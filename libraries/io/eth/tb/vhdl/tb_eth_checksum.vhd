-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for eth_checksum
-- Description:
--
--   Example from Wiki IPv4. Use Hex 45000030442240008006442e8c7c19acae241e2b
--   (20Bytes IP header) the c_exp_checksum then becomes:
--
--     4500 + 0030 + 4422 + 4000 + 8006 + 0000 + 8c7c + 19ac + ae24 + 1e2b = 2BBCF
--     2 + BBCF = BBD1 = 1011101111010001, the 1'S of sum = 0100010000101110 = 442E
--
--   Verify that checksum=c_exp_checksum when checksum_val='1'
--
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

entity tb_eth_checksum is
end tb_eth_checksum;

architecture tb of tb_eth_checksum is
  constant clk_period : time := 10 ns;  -- 100 MHz

  constant c_exp_checksum     : natural := 16#442E#;

  -- Minimum nof clk cycles between eop and sop
  constant c_checksum_latency : natural := 3;
  constant c_wait_eop_sop     : natural := 10;  -- >= c_checksum_latency-1;

  signal tb_end        : std_logic := '0';
  signal clk           : std_logic := '1';
  signal rst           : std_logic;

  signal src_out       : t_dp_sosi;

  signal checksum      : std_logic_vector(c_halfword_w - 1 downto 0);
  signal checksum_val  : std_logic;
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  p_stimuli : process
  begin
    src_out.data  <= TO_DP_DATA(0);
    src_out.valid <= '0';
    src_out.sop   <= '0';
    src_out.eop   <= '0';
    src_out.empty <= TO_DP_EMPTY(0);
    wait until rst = '0';
    wait until rising_edge(clk);

    ----------------------------------------------------------------------------
    -- First
    ----------------------------------------------------------------------------
    src_out.sop   <= '1';
    src_out.valid <= '1';
    src_out.data  <= RESIZE_DP_DATA(X"45000030");
    wait until rising_edge(clk);
    src_out.sop   <= '0';
    src_out.data  <= RESIZE_DP_DATA(X"44224000");
    wait until rising_edge(clk);
    src_out.data  <= RESIZE_DP_DATA(X"80060000");
    wait until rising_edge(clk);
    src_out.data  <= RESIZE_DP_DATA(X"8c7c19ac");
    wait until rising_edge(clk);
    src_out.eop   <= '1';
    src_out.data  <= RESIZE_DP_DATA(X"ae241e2b");
    wait until rising_edge(clk);
    src_out.data  <= (others => '0');
    src_out.valid <= '0';
    src_out.eop   <= '0';
    -- Wait latency
    for I in 0 to c_wait_eop_sop - 1 loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Again with valid low for a few cycles
    ----------------------------------------------------------------------------
    src_out.sop   <= '1';
    src_out.valid <= '1';
    src_out.data  <= RESIZE_DP_DATA(X"45000030");
    wait until rising_edge(clk);
    src_out.sop   <= '0';
    src_out.data  <= RESIZE_DP_DATA(X"44224000");
    wait until rising_edge(clk);
    src_out.data  <= RESIZE_DP_DATA(X"80060000");
    wait until rising_edge(clk);
    src_out.data  <= RESIZE_DP_DATA(X"8c7c19ac");

    -- pause
    src_out.valid <= '0';
    for I in 0 to 1 loop wait until rising_edge(clk); end loop;
    src_out.valid <= '1';

    wait until rising_edge(clk);
    src_out.eop   <= '1';
    src_out.data  <= RESIZE_DP_DATA(X"ae241e2b");
    wait until rising_edge(clk);
    src_out.data  <= (others => '0');
    src_out.valid <= '0';
    src_out.eop   <= '0';
    -- Wait latency
    for I in 0 to c_wait_eop_sop - 1 loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Again with carry in within word_sum
    ----------------------------------------------------------------------------
    src_out.sop   <= '1';
    src_out.valid <= '1';
    src_out.data  <= RESIZE_DP_DATA(X"45000030");
    wait until rising_edge(clk);
    src_out.sop   <= '0';
    src_out.data  <= RESIZE_DP_DATA(X"44224000");
--     WAIT UNTIL rising_edge(clk);
--     src_out.data  <= RESIZE_DP_DATA(X"80060000");
--     WAIT UNTIL rising_edge(clk);
--     src_out.data  <= RESIZE_DP_DATA(X"8c7c19ac");
    wait until rising_edge(clk);
    src_out.data  <= RESIZE_DP_DATA(X"80068c7c");
    wait until rising_edge(clk);
    src_out.data  <= RESIZE_DP_DATA(X"000019ac");

    -- pause
    src_out.valid <= '0';
    for I in 0 to 0 loop wait until rising_edge(clk); end loop;
    src_out.valid <= '1';

    wait until rising_edge(clk);
    src_out.eop   <= '1';
    src_out.data  <= RESIZE_DP_DATA(X"ae241e2b");
    wait until rising_edge(clk);
    src_out.data  <= (others => '0');
    src_out.valid <= '0';
    src_out.eop   <= '0';
    -- Wait latency
    for I in 0 to c_wait_eop_sop - 1 loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Again with empty = 2
    ----------------------------------------------------------------------------
    src_out.empty <= TO_DP_EMPTY(2);
    src_out.sop   <= '1';
    src_out.valid <= '1';
    src_out.data  <= RESIZE_DP_DATA(X"45000030");
    wait until rising_edge(clk);
    src_out.sop   <= '0';
    src_out.data  <= RESIZE_DP_DATA(X"44224000");
    wait until rising_edge(clk);
    src_out.data  <= RESIZE_DP_DATA(X"80061e2b");  -- overwrite these "0000" with the last 2, now empty, bytes "1e2b", to keep the seem expected result
    wait until rising_edge(clk);
    src_out.data  <= RESIZE_DP_DATA(X"8c7c19ac");
    wait until rising_edge(clk);
    src_out.eop   <= '1';
    src_out.data  <= RESIZE_DP_DATA(X"ae241e2b");
    wait until rising_edge(clk);
    src_out.data  <= (others => '0');
    src_out.valid <= '0';
    src_out.eop   <= '0';
    -- Wait latency
    for I in 0 to c_wait_eop_sop - 1 loop wait until rising_edge(clk); end loop;

    tb_end <= '1';
    assert false
      report "Simulation tb_eth_checksum finished."
      severity NOTE;
    wait;
  end process;

  p_verify : process
  begin
    wait until rising_edge(clk);
    if checksum_val = '1' then
      assert unsigned(checksum) = c_exp_checksum
        report "Wrong checksum"
        severity ERROR;
    end if;
    if tb_end = '1' then
      assert checksum_val = '1'
        report "Checksum is not valid at tb_end"
        severity ERROR;
    end if;
  end process;

  u_dut : entity work.eth_checksum
  port map (
    rst           => rst,
    clk           => clk,

    snk_in        => src_out,

    checksum      => checksum,
    checksum_val  => checksum_val
  );
end tb;
