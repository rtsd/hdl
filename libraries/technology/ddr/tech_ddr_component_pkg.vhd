-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE;
use IEEE.std_logic_1164.all;

package tech_ddr_component_pkg is
  ------------------------------------------------------------------------------
  -- ip_stratixiv
  ------------------------------------------------------------------------------

  -- Manually derived VHDL entity from Verilog module $HDL_BUILD_DIR/ip_stratixiv_ddr3_uphy_4g_800_master.v
  component ip_stratixiv_ddr3_uphy_4g_800_master is
  port (
    pll_ref_clk                : in    std_logic;  -- pll_ref_clk.clk
    global_reset_n             : in    std_logic;  -- global_reset.reset_n
    soft_reset_n               : in    std_logic;  -- soft_reset.reset_n
    afi_clk                    : out   std_logic;  -- afi_clk.clk
    afi_half_clk               : out   std_logic;  -- afi_half_clk.clk
    afi_reset_n                : out   std_logic;  -- afi_reset.reset_n
    mem_a                      : out   std_logic_vector(14 downto 0);  -- memory.mem_a
    mem_ba                     : out   std_logic_vector(2 downto 0);  -- .mem_ba
    mem_ck                     : out   std_logic_vector(1 downto 0);  -- .mem_ck
    mem_ck_n                   : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_cke                    : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n                   : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_dm                     : out   std_logic_vector(7 downto 0);  -- .mem_dm
    mem_ras_n                  : out   std_logic;  -- .mem_ras_n
    mem_cas_n                  : out   std_logic;  -- .mem_cas_n
    mem_we_n                   : out   std_logic;  -- .mem_we_n
    mem_reset_n                : out   std_logic;  -- .mem_reset_n
    mem_dq                     : inout std_logic_vector(63 downto 0);  -- .mem_dq
    mem_dqs                    : inout std_logic_vector(7 downto 0);  -- .mem_dqs
    mem_dqs_n                  : inout std_logic_vector(7 downto 0);  -- .mem_dqs_n
    mem_odt                    : out   std_logic_vector(1 downto 0);  -- .mem_odt
    avl_ready                  : out   std_logic;  -- avl.waitrequest_n
    avl_burstbegin             : in    std_logic;  -- .beginbursttransfer
    avl_addr                   : in    std_logic_vector(26 downto 0);  -- .address
    avl_rdata_valid            : out   std_logic;  -- .readdatavalid
    avl_rdata                  : out   std_logic_vector(255 downto 0);  -- .readdata
    avl_wdata                  : in    std_logic_vector(255 downto 0);  -- .writedata
    avl_be                     : in    std_logic_vector(31 downto 0);  -- .byteenable
    avl_read_req               : in    std_logic;  -- .read
    avl_write_req              : in    std_logic;  -- .write
    avl_size                   : in    std_logic_vector(6 downto 0);  -- .burstcount
    local_init_done            : out   std_logic;  -- status.local_init_done
    local_cal_success          : out   std_logic;  -- .local_cal_success
    local_cal_fail             : out   std_logic;  -- .local_cal_fail
    oct_rdn                    : in    std_logic;  -- oct.rdn
    oct_rup                    : in    std_logic;  -- .rup
    seriesterminationcontrol   : out   std_logic_vector(13 downto 0);  -- oct_sharing.seriesterminationcontrol
    parallelterminationcontrol : out   std_logic_vector(13 downto 0);  -- .parallelterminationcontrol
    pll_mem_clk                : out   std_logic;  -- pll_sharing.pll_mem_clk
    pll_write_clk              : out   std_logic;  -- .pll_write_clk
    pll_write_clk_pre_phy_clk  : out   std_logic;  -- .pll_write_clk_pre_phy_clk
    pll_addr_cmd_clk           : out   std_logic;  -- .pll_addr_cmd_clk
    pll_locked                 : out   std_logic;  -- .pll_locked
    pll_avl_clk                : out   std_logic;  -- .pll_avl_clk
    pll_config_clk             : out   std_logic;  -- .pll_config_clk
    dll_delayctrl              : out   std_logic_vector(5 downto 0)  -- dll_sharing.dll_delayctrl
  );
  end component;

  -- Manually derived VHDL entity from Verilog module $HDL_BUILD_DIR/ip_stratixiv_ddr3_uphy_4g_800_slave.v
  -- . diff with master is that only master has oct_* inputs and that the *terminationcontrol are inputs for the slave
  component ip_stratixiv_ddr3_uphy_4g_800_slave is
  port (
    pll_ref_clk                : in    std_logic;  -- pll_ref_clk.clk
    global_reset_n             : in    std_logic;  -- global_reset.reset_n
    soft_reset_n               : in    std_logic;  -- soft_reset.reset_n
    afi_clk                    : out   std_logic;  -- afi_clk.clk
    afi_half_clk               : out   std_logic;  -- afi_half_clk.clk
    afi_reset_n                : out   std_logic;  -- afi_reset.reset_n
    mem_a                      : out   std_logic_vector(14 downto 0);  -- memory.mem_a
    mem_ba                     : out   std_logic_vector(2 downto 0);  -- .mem_ba
    mem_ck                     : out   std_logic_vector(1 downto 0);  -- .mem_ck
    mem_ck_n                   : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_cke                    : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n                   : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_dm                     : out   std_logic_vector(7 downto 0);  -- .mem_dm
    mem_ras_n                  : out   std_logic;  -- .mem_ras_n
    mem_cas_n                  : out   std_logic;  -- .mem_cas_n
    mem_we_n                   : out   std_logic;  -- .mem_we_n
    mem_reset_n                : out   std_logic;  -- .mem_reset_n
    mem_dq                     : inout std_logic_vector(63 downto 0);  -- .mem_dq
    mem_dqs                    : inout std_logic_vector(7 downto 0);  -- .mem_dqs
    mem_dqs_n                  : inout std_logic_vector(7 downto 0);  -- .mem_dqs_n
    mem_odt                    : out   std_logic_vector(1 downto 0);  -- .mem_odt
    avl_ready                  : out   std_logic;  -- avl.waitrequest_n
    avl_burstbegin             : in    std_logic;  -- .beginbursttransfer
    avl_addr                   : in    std_logic_vector(26 downto 0);  -- .address
    avl_rdata_valid            : out   std_logic;  -- .readdatavalid
    avl_rdata                  : out   std_logic_vector(255 downto 0);  -- .readdata
    avl_wdata                  : in    std_logic_vector(255 downto 0);  -- .writedata
    avl_be                     : in    std_logic_vector(31 downto 0);  -- .byteenable
    avl_read_req               : in    std_logic;  -- .read
    avl_write_req              : in    std_logic;  -- .write
    avl_size                   : in    std_logic_vector(6 downto 0);  -- .burstcount
    local_init_done            : out   std_logic;  -- status.local_init_done
    local_cal_success          : out   std_logic;  -- .local_cal_success
    local_cal_fail             : out   std_logic;  -- .local_cal_fail
    seriesterminationcontrol   : in    std_logic_vector(13 downto 0);  -- oct_sharing.seriesterminationcontrol
    parallelterminationcontrol : in    std_logic_vector(13 downto 0);  -- .parallelterminationcontrol
    pll_mem_clk                : out   std_logic;  -- pll_sharing.pll_mem_clk
    pll_write_clk              : out   std_logic;  -- .pll_write_clk
    pll_write_clk_pre_phy_clk  : out   std_logic;  -- .pll_write_clk_pre_phy_clk
    pll_addr_cmd_clk           : out   std_logic;  -- .pll_addr_cmd_clk
    pll_locked                 : out   std_logic;  -- .pll_locked
    pll_avl_clk                : out   std_logic;  -- .pll_avl_clk
    pll_config_clk             : out   std_logic;  -- .pll_config_clk
    dll_delayctrl              : out   std_logic_vector(5 downto 0)  -- dll_sharing.dll_delayctrl
  );
  end component;

  -- Manually derived VHDL entity from Verilog module $HDL_BUILD_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_master.v
  component ip_stratixiv_ddr3_uphy_4g_single_rank_800_master is
  port (
    pll_ref_clk                 : in    std_logic;  -- pll_ref_clk.clk
    global_reset_n              : in    std_logic;  -- global_reset.reset_n
    soft_reset_n                : in    std_logic;  -- soft_reset.reset_n
    afi_clk                     : out   std_logic;  -- afi_clk.clk
    afi_half_clk                : out   std_logic;  -- afi_half_clk.clk
    afi_reset_n                 : out   std_logic;  -- afi_reset.reset_n
    mem_a                       : out   std_logic_vector(15 downto 0);  -- memory.mem_a
    mem_ba                      : out   std_logic_vector(2 downto 0);  -- .mem_ba
    mem_ck                      : out   std_logic_vector(1 downto 0);  -- .mem_ck
    mem_ck_n                    : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_cke                     : out   std_logic;  -- .mem_cke
    mem_cs_n                    : out   std_logic;  -- .mem_cs_n
    mem_dm                      : out   std_logic_vector(7 downto 0);  -- .mem_dm
    mem_ras_n                   : out   std_logic;  -- .mem_ras_n
    mem_cas_n                   : out   std_logic;  -- .mem_cas_n
    mem_we_n                    : out   std_logic;  -- .mem_we_n
    mem_reset_n                 : out   std_logic;  -- .mem_reset_n
    mem_dq                      : inout std_logic_vector(63 downto 0);  -- .mem_dq
    mem_dqs                     : inout std_logic_vector(7 downto 0);  -- .mem_dqs
    mem_dqs_n                   : inout std_logic_vector(7 downto 0);  -- .mem_dqs_n
    mem_odt                     : out   std_logic;  -- .mem_odt
    avl_ready                   : out   std_logic;  -- avl.waitrequest_n
    avl_burstbegin              : in    std_logic;  -- .beginbursttransfer
    avl_addr                    : in    std_logic_vector(26 downto 0);  -- .address
    avl_rdata_valid             : out   std_logic;  -- .readdatavalid
    avl_rdata                   : out   std_logic_vector(255 downto 0);  -- .readdata
    avl_wdata                   : in    std_logic_vector(255 downto 0);  -- .writedata
    avl_be                      : in    std_logic_vector(31 downto 0);  -- .byteenable
    avl_read_req                : in    std_logic;  -- .read
    avl_write_req               : in    std_logic;  -- .write
    avl_size                    : in    std_logic_vector(6 downto 0);  -- .burstcount
    local_init_done             : out   std_logic;  -- status.local_init_done
    local_cal_success           : out   std_logic;  -- .local_cal_success
    local_cal_fail              : out   std_logic;  -- .local_cal_fail
    oct_rdn                     : in    std_logic;  -- oct.rdn
    oct_rup                     : in    std_logic;  -- .rup
    seriesterminationcontrol    : out   std_logic_vector(13 downto 0);  -- oct_sharing.seriesterminationcontrol
    parallelterminationcontrol  : out   std_logic_vector(13 downto 0);  -- .parallelterminationcontrol
    pll_mem_clk                 : out   std_logic;  -- pll_sharing.pll_mem_clk
    pll_write_clk               : out   std_logic;  -- .pll_write_clk
    pll_write_clk_pre_phy_clk   : out   std_logic;  -- .pll_write_clk_pre_phy_clk
    pll_addr_cmd_clk            : out   std_logic;  -- .pll_addr_cmd_clk
    pll_locked                  : out   std_logic;  -- .pll_locked
    pll_avl_clk                 : out   std_logic;  -- .pll_avl_clk
    pll_config_clk              : out   std_logic;  -- .pll_config_clk
    dll_delayctrl               : out   std_logic_vector(5 downto 0)  -- dll_sharing.dll_delayctrl
  );
  end component;

  -- Manually derived VHDL entity from Verilog module $HDL_BUILD_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave.v
  -- . diff with master is that only master has oct_* inputs and that the *terminationcontrol are inputs for the slave
  component ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave is
  port (
    pll_ref_clk                 : in    std_logic;  -- pll_ref_clk.clk
    global_reset_n              : in    std_logic;  -- global_reset.reset_n
    soft_reset_n                : in    std_logic;  -- soft_reset.reset_n
    afi_clk                     : out   std_logic;  -- afi_clk_in.clk
    afi_half_clk                : out   std_logic;  -- afi_half_clk_in.clk
    afi_reset_n                 : out   std_logic;  -- afi_reset_in.reset_n
    mem_a                       : out   std_logic_vector(15 downto 0);  -- memory.mem_a
    mem_ba                      : out   std_logic_vector(2 downto 0);  -- .mem_ba
    mem_ck                      : out   std_logic_vector(1 downto 0);  -- .mem_ck
    mem_ck_n                    : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_cke                     : out   std_logic;  -- .mem_cke
    mem_cs_n                    : out   std_logic;  -- .mem_cs_n
    mem_dm                      : out   std_logic_vector(7 downto 0);  -- .mem_dm
    mem_ras_n                   : out   std_logic;  -- .mem_ras_n
    mem_cas_n                   : out   std_logic;  -- .mem_cas_n
    mem_we_n                    : out   std_logic;  -- .mem_we_n
    mem_reset_n                 : out   std_logic;  -- .mem_reset_n
    mem_dq                      : inout std_logic_vector(63 downto 0);  -- .mem_dq
    mem_dqs                     : inout std_logic_vector(7 downto 0);  -- .mem_dqs
    mem_dqs_n                   : inout std_logic_vector(7 downto 0);  -- .mem_dqs_n
    mem_odt                     : out   std_logic;  -- .mem_odt
    avl_ready                   : out   std_logic;  -- avl.waitrequest_n
    avl_burstbegin              : in    std_logic;  -- .beginbursttransfer
    avl_addr                    : in    std_logic_vector(26 downto 0);  -- .address
    avl_rdata_valid             : out   std_logic;  -- .readdatavalid
    avl_rdata                   : out   std_logic_vector(255 downto 0);  -- .readdata
    avl_wdata                   : in    std_logic_vector(255 downto 0);  -- .writedata
    avl_be                      : in    std_logic_vector(31 downto 0);  -- .byteenable
    avl_read_req                : in    std_logic;  -- .read
    avl_write_req               : in    std_logic;  -- .write
    avl_size                    : in    std_logic_vector(6 downto 0);  -- .burstcount
    local_init_done             : out   std_logic;  -- status.local_init_done
    local_cal_success           : out   std_logic;  -- .local_cal_success
    local_cal_fail              : out   std_logic;  -- .local_cal_fail
    seriesterminationcontrol    : in    std_logic_vector(13 downto 0);  -- oct_sharing.seriesterminationcontrol
    parallelterminationcontrol  : in    std_logic_vector(13 downto 0);  -- .parallelterminationcontrol
    pll_mem_clk                 : out   std_logic;  -- pll_sharing.pll_mem_clk
    pll_write_clk               : out   std_logic;  -- .pll_write_clk
    pll_write_clk_pre_phy_clk   : out   std_logic;  -- .pll_write_clk_pre_phy_clk
    pll_addr_cmd_clk            : out   std_logic;  -- .pll_addr_cmd_clk
    pll_locked                  : out   std_logic;  -- .pll_locked
    pll_avl_clk                 : out   std_logic;  -- .pll_avl_clk
    pll_config_clk              : out   std_logic;  -- .pll_config_clk
    dll_delayctrl               : out   std_logic_vector(5 downto 0)  -- dll_sharing.dll_delayctrl
  );
  end component;

  -- Manually derived VHDL entity from Verilog module $HDL_BUILD_DIR/ip_stratixiv_ddr3_uphy_16g_dual_rank_800.v
  component ip_stratixiv_ddr3_uphy_16g_dual_rank_800 is
  port (
    pll_ref_clk                : in    std_logic;  -- pll_ref_clk.clk
    global_reset_n             : in    std_logic;  -- global_reset.reset_n
    soft_reset_n               : in    std_logic;  -- soft_reset.reset_n
    afi_clk                    : out   std_logic;  -- afi_clk.clk
    afi_half_clk               : out   std_logic;  -- afi_half_clk.clk
    afi_reset_n                : out   std_logic;  -- afi_reset.reset_n
    mem_a                      : out   std_logic_vector(15 downto 0);  -- memory.mem_a
    mem_ba                     : out   std_logic_vector(2 downto 0);  -- .mem_ba
    mem_ck                     : out   std_logic_vector(1 downto 0);  -- .mem_ck
    mem_ck_n                   : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_cke                    : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n                   : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_dm                     : out   std_logic_vector(7 downto 0);  -- .mem_dm
    mem_ras_n                  : out   std_logic;  -- .mem_ras_n
    mem_cas_n                  : out   std_logic;  -- .mem_cas_n
    mem_we_n                   : out   std_logic;  -- .mem_we_n
    mem_reset_n                : out   std_logic;  -- .mem_reset_n
    mem_dq                     : inout std_logic_vector(63 downto 0);  -- .mem_dq
    mem_dqs                    : inout std_logic_vector(7 downto 0);  -- .mem_dqs
    mem_dqs_n                  : inout std_logic_vector(7 downto 0);  -- .mem_dqs_n
    mem_odt                    : out   std_logic_vector(1 downto 0);  -- .mem_odt
    avl_ready                  : out   std_logic;  -- avl.waitrequest_n
    avl_burstbegin             : in    std_logic;  -- .beginbursttransfer
    avl_addr                   : in    std_logic_vector(28 downto 0);  -- .address
    avl_rdata_valid            : out   std_logic;  -- .readdatavalid
    avl_rdata                  : out   std_logic_vector(255 downto 0);  -- .readdata
    avl_wdata                  : in    std_logic_vector(255 downto 0);  -- .writedata
    avl_be                     : in    std_logic_vector(31 downto 0);  -- .byteenable
    avl_read_req               : in    std_logic;  -- .read
    avl_write_req              : in    std_logic;  -- .write
    avl_size                   : in    std_logic_vector(6 downto 0);  -- .burstcount
    local_init_done            : out   std_logic;  -- status.local_init_done
    local_cal_success          : out   std_logic;  -- .local_cal_success
    local_cal_fail             : out   std_logic;  -- .local_cal_fail
    oct_rdn                    : in    std_logic;  -- oct.rdn
    oct_rup                    : in    std_logic;  -- .rup
    seriesterminationcontrol   : out   std_logic_vector(13 downto 0);  -- oct_sharing.seriesterminationcontrol
    parallelterminationcontrol : out   std_logic_vector(13 downto 0);  -- .parallelterminationcontrol
    pll_mem_clk                : out   std_logic;  -- pll_sharing.pll_mem_clk
    pll_write_clk              : out   std_logic;  -- .pll_write_clk
    pll_write_clk_pre_phy_clk  : out   std_logic;  -- .pll_write_clk_pre_phy_clk
    pll_addr_cmd_clk           : out   std_logic;  -- .pll_addr_cmd_clk
    pll_locked                 : out   std_logic;  -- .pll_locked
    pll_avl_clk                : out   std_logic;  -- .pll_avl_clk
    pll_config_clk             : out   std_logic;  -- .pll_config_clk
    dll_delayctrl              : out   std_logic_vector(5 downto 0)  -- dll_sharing.dll_delayctrl
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10
  ------------------------------------------------------------------------------

  -- Manually derived VHDL entity from VHDL file $HDL_BUILD_DIR/sim/ip_arria10_ddr4_4g_1600.vhd
  component ip_arria10_ddr4_4g_1600 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(25 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(0 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(0 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(0 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(0 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(0 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  -- Manually derived VHDL entity from VHDL file $HDL_BUILD_DIR/sim/ip_arria10_ddr4_4g_2000.vhd
  component ip_arria10_ddr4_4g_2000 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(25 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(0 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(0 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(0 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(0 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(0 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  ------------------------------------------------------------------------------

  -- Manually derived VHDL entity from VHDL file $HDL_BUILD_DIR/sim/ip_arria10_e3sge3_ddr4_4g_1600.vhd
  component ip_arria10_e3sge3_ddr4_4g_1600 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(25 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(0 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(0 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(0 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(0 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(0 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  -- Dual rank version of ip_arria10_e3sge3_ddr4_4g_1600.vhd
  component ip_arria10_e3sge3_ddr4_8g_1600 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(26 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(1 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(1 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  -- Manually derived VHDL entity from VHDL file $HDL_BUILD_DIR/sim/ip_arria10_e3sge3_ddr4_4g_2000.vhd
  component ip_arria10_e3sge3_ddr4_4g_2000 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(25 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(0 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(0 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(0 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(0 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(0 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e1sg
  ------------------------------------------------------------------------------

  -- Manually derived VHDL entity from VHDL file $HDL_BUILD_DIR/sim/ip_arria10_e1sg_ddr4_4g_1600.vhd
  component ip_arria10_e1sg_ddr4_4g_1600 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(25 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(0 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(0 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(0 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(0 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(0 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  -- Dual rank version of ip_arria10_e1sg_ddr4_4g_1600.vhd
  component ip_arria10_e1sg_ddr4_8g_1600 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(26 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(1 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(1 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  component ip_arria10_e1sg_ddr4_16g_1600 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(27 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(1 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(1 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  -- Manually derived VHDL entity from VHDL file $HDL_BUILD_DIR/sim/ip_arria10_e1sg_ddr4_4g_2000.vhd
  component ip_arria10_e1sg_ddr4_4g_2000 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(25 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(0 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(0 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(0 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(0 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(0 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  -- Dual rank version for e2sg
  component ip_arria10_e2sg_ddr4_8g_1600 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(26 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(1 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(1 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  component ip_arria10_e2sg_ddr4_8g_2400 is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(26 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(1 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(1 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  component ip_arria10_e2sg_ddr4_16g_1600_64b is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(27 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(511 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(511 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(63 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(0 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(0 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(0 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(0 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(0 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(7 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(7 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(63 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(7 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;

  component ip_arria10_e2sg_ddr4_16g_1600_72b is
  port (
    amm_ready_0         : out   std_logic;  -- ctrl_amm_avalon_slave_0.waitrequest_n
    amm_read_0          : in    std_logic                      := '0';  -- .read
    amm_write_0         : in    std_logic                      := '0';  -- .write
    amm_address_0       : in    std_logic_vector(27 downto 0)  := (others => '0');  -- .address
    amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- .readdata
    amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => '0');  -- .writedata
    amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => '0');  -- .burstcount
    amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => '0');  -- .byteenable
    amm_readdatavalid_0 : out   std_logic;  -- .readdatavalid
    emif_usr_clk        : out   std_logic;  -- emif_usr_clk_clock_source.clk
    emif_usr_reset_n    : out   std_logic;  -- emif_usr_reset_reset_source.reset_n
    global_reset_n      : in    std_logic                      := '0';  -- global_reset_reset_sink.reset_n
    mem_ck              : out   std_logic_vector(0 downto 0);  -- mem_conduit_end.mem_ck
    mem_ck_n            : out   std_logic_vector(0 downto 0);  -- .mem_ck_n
    mem_a               : out   std_logic_vector(16 downto 0);  -- .mem_a
    mem_act_n           : out   std_logic_vector(0 downto 0);  -- .mem_act_n
    mem_ba              : out   std_logic_vector(1 downto 0);  -- .mem_ba
    mem_bg              : out   std_logic_vector(1 downto 0);  -- .mem_bg
    mem_cke             : out   std_logic_vector(0 downto 0);  -- .mem_cke
    mem_cs_n            : out   std_logic_vector(0 downto 0);  -- .mem_cs_n
    mem_odt             : out   std_logic_vector(0 downto 0);  -- .mem_odt
    mem_reset_n         : out   std_logic_vector(0 downto 0);  -- .mem_reset_n
    mem_par             : out   std_logic_vector(0 downto 0);  -- .mem_par
    mem_alert_n         : in    std_logic_vector(0 downto 0)   := (others => '0');  -- .mem_alert_n
    mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs
    mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dqs_n
    mem_dq              : inout std_logic_vector(71 downto 0)  := (others => '0');  -- .mem_dq
    mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => '0');  -- .mem_dbi_n
    oct_rzqin           : in    std_logic                      := '0';  -- oct_conduit_end.oct_rzqin
    pll_ref_clk         : in    std_logic                      := '0';  -- pll_ref_clk_clock_sink.clk
    local_cal_success   : out   std_logic;  -- status_conduit_end.local_cal_success
    local_cal_fail      : out   std_logic  -- .local_cal_fail
  );
  end component;
end tech_ddr_component_pkg;

package body tech_ddr_component_pkg is
end tech_ddr_component_pkg;
