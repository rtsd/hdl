-------------------------------------------------------------------------------
--
-- Copyright (C) 2009-2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

package unb2c_board_pkg is
  -- UniBoard
  constant c_unb2c_board_nof_node             : natural := 4;  -- number of nodes on UniBoard
  constant c_unb2c_board_nof_node_w           : natural := 2;  -- = ceil_log2(c_unb2c_board_nof_node)
  constant c_unb2c_board_nof_chip             : natural := c_unb2c_board_nof_node;  -- = 4
  constant c_unb2c_board_nof_chip_w           : natural := 2;  -- = ceil_log2(c_unb2c_board_nof_chip)
  constant c_unb2c_board_nof_ddr              : natural := 2;  -- each node has 2 DDR modules

  -- Subrack : TODO: Isn't this application specific?
  constant c_unb2c_board_nof_uniboard         : natural := 4;  -- nof UniBoard in a subrack
  constant c_unb2c_board_nof_uniboard_w       : natural := 6;  -- Only 2 required for 4 boards; full width is 6.

  -- Clock frequencies
  constant c_unb2c_board_ext_clk_freq_200M    : natural := 200 * 10**6;  -- external clock, SMA clock
  constant c_unb2c_board_ext_clk_freq_256M    : natural := 256 * 10**6;  -- external clock, SMA clock
  constant c_unb2c_board_eth_clk_freq_25M     : natural :=  25 * 10**6;  -- fixed 25 MHz  ETH XO clock used as reference clock for the PLL
  constant c_unb2c_board_eth_clk_freq_125M    : natural := 125 * 10**6;  -- fixed 125 MHz ETH XO clock used as direct clock for TSE
  constant c_unb2c_board_tse_clk_freq         : natural := 125 * 10**6;  -- fixed 125 MHz TSE reference clock derived from ETH_clk by PLL
  constant c_unb2c_board_cal_clk_freq         : natural :=  40 * 10**6;  -- fixed 40 MHz IO calibration clock derived from ETH_clk by PLL
  constant c_unb2c_board_mm_clk_freq_10M      : natural :=  10 * 10**6;  -- clock when g_sim=TRUE
  constant c_unb2c_board_mm_clk_freq_25M      : natural :=  25 * 10**6;  -- clock derived from ETH_clk by PLL
  constant c_unb2c_board_mm_clk_freq_50M      : natural :=  50 * 10**6;  -- clock derived from ETH_clk by PLL
  constant c_unb2c_board_mm_clk_freq_100M     : natural := 100 * 10**6;  -- clock derived from ETH_clk by PLL
  constant c_unb2c_board_mm_clk_freq_125M     : natural := 125 * 10**6;  -- clock derived from ETH_clk by PLL

  -- ETH
  constant c_unb2c_board_nof_eth              : natural := 2;  -- number of ETH channels per node

  -- CONSTANT RECORD DECLARATIONS ---------------------------------------------

  -- c_unb2c_board_signature_* : random signature words used for unused status bits to ensure that the software reads the correct interface address
  constant c_unb2c_board_signature_eth1g_slv   : std_logic_vector(31 downto 0) := X"46e46cbc";
  constant c_unb2c_board_signature_eth10g_slv  : std_logic_vector(31 downto 0) := X"2bd2e40a";

  constant c_unb2c_board_signature_eth1g       : integer := TO_SINT(c_unb2c_board_signature_eth1g_slv  );
  constant c_unb2c_board_signature_eth10g      : integer := TO_SINT(c_unb2c_board_signature_eth10g_slv );

  -- Transceivers
  type t_c_unb2c_board_tr is record
    nof_bus                           : natural;
    bus_w                             : natural;
    i2c_w                             : natural;
  end record;

  constant c_unb2c_board_tr_back              : t_c_unb2c_board_tr := (1, 24, 0);  -- per node: 2 buses with 24 channels
  constant c_unb2c_board_tr_ring              : t_c_unb2c_board_tr := (2, 12, 0);  -- per node: 2 buses with 12 channels
  constant c_unb2c_board_tr_qsfp              : t_c_unb2c_board_tr := (6, 4,  0);  -- per node: 6 buses with 4 channels
  constant c_unb2c_board_tr_jesd204b          : t_c_unb2c_board_tr := (1, 12, 0);  -- per node: 1 buses with 12 channels
  constant c_unb2c_board_nof_tr_jesd204b      : natural := 12;  -- 12 channels used in unb2c lab tests
  constant c_unb2c_board_start_tr_jesd204b    : natural := 0;  -- First BCK transceiver used for jesd in unb2c lab tests
  constant c_unb2c_board_nof_sync_jesd204b    : natural := 4;  -- 4 channels used in unb2c lab tests, 1 for each RCU.
  constant c_unb2c_board_tr_qsfp_nof_leds     : natural := c_unb2c_board_tr_qsfp.nof_bus * 2;  -- 2 leds per qsfp

  type t_unb2c_board_qsfp_bus_2arr is array (integer range <>) of std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
  type t_unb2c_board_ring_bus_2arr is array (integer range <>) of std_logic_vector(c_unb2c_board_tr_ring.bus_w - 1 downto 0);
  type t_unb2c_board_back_bus_2arr is array (integer range <>) of std_logic_vector(c_unb2c_board_tr_back.bus_w - 1 downto 0);

  -- Auxiliary

  -- Test IO Interface
  type t_c_unb2c_board_testio is record
    tst_w                             : natural;  -- = nof tst = 2; [tst_w-1 +tst_lo : tst_lo] = [5:4],
    led_w                             : natural;  -- = nof led = 2; [led_w-1 +led_lo : led_lo] = [3:2],
    jmp_w                             : natural;  -- = nof jmp = 2; [jmp_w-1 +jmp_lo : jmp_lo] = [1:0],
    tst_lo                            : natural;  -- = 2;
    led_lo                            : natural;  -- = 2;
    jmp_lo                            : natural;  -- = 0;
  end record;

  constant c_unb2c_board_testio               : t_c_unb2c_board_testio := (2, 2, 2, 2, 2, 0);
  constant c_unb2c_board_testio_led_green     : natural := c_unb2c_board_testio.led_lo;
  constant c_unb2c_board_testio_led_red       : natural := c_unb2c_board_testio.led_lo + 1;

  type t_c_unb2c_board_aux is record
    version_w                         : natural;  -- = 2;
    id_w                              : natural;  -- = 8;  -- 6+2 bits wide = total node ID for up to 64 UniBoards in a system and 4 nodes per board
    chip_id_w                         : natural;  -- = 2;  -- board node ID for the 4 FPGA nodes on a UniBoard
    testio_w                          : natural;  -- = 6;
    testio                            : t_c_unb2c_board_testio;
  end record;

  constant c_unb2c_board_aux           : t_c_unb2c_board_aux := (2, 8, c_unb2c_board_nof_chip_w, 6, c_unb2c_board_testio);

  type t_e_unb2_board_node is (e_any);

  type t_unb2c_board_fw_version is record
    hi                                : natural;  -- = 0..15
    lo                                : natural;  -- = 0..15, firmware version is: hi.lo
  end record;

  constant c_unb2c_board_fw_version    : t_unb2c_board_fw_version := (0, 0);

  -- SIGNAL RECORD DECLARATIONS -----------------------------------------------

  -- System info
  type t_c_unb2c_board_system_info is record
    version  : natural;  -- UniBoard board HW version (2 bit value)
    id       : natural;  -- UniBoard FPGA node id (8 bit value)
                         -- Derived ID info:
    bck_id   : natural;  -- = id[7:2], ID part from back plane
    chip_id  : natural;  -- = id[1:0], ID part from UniBoard
    node_id  : natural;  -- = id[1:0], node ID: 0, 1, 2 or 3
    is_node2 : natural;  -- 1 for Node 2, else 0.
  end record;

  function func_unb2c_board_system_info(VERSION : in std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0);
                                        ID      : in std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0)) return t_c_unb2c_board_system_info;
end unb2c_board_pkg;

package body unb2c_board_pkg is
  function func_unb2c_board_system_info(VERSION : in std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0);
                                       ID      : in std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0)) return t_c_unb2c_board_system_info is
    variable v_system_info : t_c_unb2c_board_system_info;
  begin
    v_system_info.version := to_integer(unsigned(VERSION));
    v_system_info.id      := to_integer(unsigned(ID));
    v_system_info.bck_id  := to_integer(unsigned(ID(7 downto 2)));
    v_system_info.chip_id := to_integer(unsigned(ID(1 downto 0)));
    v_system_info.node_id := to_integer(unsigned(ID(1 downto 0)));
    if unsigned(ID(1 downto 0)) = 2 then v_system_info.is_node2 := 1; else v_system_info.is_node2 := 0; end if;
    return v_system_info;
  end;

end unb2c_board_pkg;
