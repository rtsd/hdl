-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Calculate the signed 'voltage' sum of input symbols during a sync
--           interval
-- Description :
--   The in_data can have g_nof_symbols_per_data >= 1 per in_data word.
--   The sync interval starts with the in_data that is valid at the in_sync.
--   The output 'voltage' sum of the previous sync interval is valid at the
--   sum_sync and held until the next sum_sync.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity aduh_mean_sum is
  generic (
    g_symbol_w             : natural := 12;
    g_nof_symbols_per_data : natural := 4;  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    g_nof_accumulations    : natural := 800 * 10**6;  -- integration time in symbols
    g_sum_truncate         : boolean := true;  -- when TRUE truncate (keep MS part) else resize (keep sign and LS part)
    g_sum_w                : natural := 32  -- typcially MM word width
  );
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;

    -- Streaming inputs
    in_data     : in  std_logic_vector(g_nof_symbols_per_data * g_symbol_w - 1 downto 0);
    in_val      : in  std_logic;
    in_sync     : in  std_logic;

    -- Accumulation outputs
    sum         : out std_logic_vector(g_sum_w - 1 downto 0);
    sum_sync    : out std_logic  -- at sum_sync there is a new sum
  );
end aduh_mean_sum;

architecture rtl of aduh_mean_sum is
  constant c_acc_w              : natural := g_symbol_w + ceil_log2(g_nof_accumulations / g_nof_symbols_per_data);
  constant c_acc_sum_nof_stages : natural := ceil_log2(g_nof_symbols_per_data);
  constant c_acc_sum_pipeline   : natural := 1;
  constant c_acc_sum_w          : natural := c_acc_w + c_acc_sum_nof_stages;

  type t_symbol_arr is array (integer range <>) of std_logic_vector(g_symbol_w - 1 downto 0);
  type t_acc_arr    is array (integer range <>) of std_logic_vector(   c_acc_w - 1 downto 0);

  signal in_sync_p    : std_logic;
  signal symbol_arr   : t_symbol_arr(0 to g_nof_symbols_per_data - 1);
  signal acc_arr      : t_acc_arr(   0 to g_nof_symbols_per_data - 1);
  signal acc_vec      : std_logic_vector(g_nof_symbols_per_data * c_acc_w - 1 downto 0);
  signal acc_sum      : std_logic_vector(c_acc_sum_w - 1 downto 0);
  signal i_sum        : std_logic_vector(g_sum_w - 1 downto 0);
  signal nxt_sum      : std_logic_vector(g_sum_w - 1 downto 0);
  signal nxt_sum_sync : std_logic;
begin
  sum <= i_sum;

  regs : process(rst, clk)
  begin
    if rst = '1' then
      i_sum    <= (others => '0');
      sum_sync <= '0';
    elsif rising_edge(clk) then
      i_sum    <= nxt_sum;
      sum_sync <= nxt_sum_sync;
    end if;
  end process;

  -- Accumulate per symbol stream in the in_data
  gen_acc : for I in 0 to g_nof_symbols_per_data - 1 generate
    symbol_arr(I) <= in_data((g_nof_symbols_per_data - I) * g_symbol_w - 1 downto (g_nof_symbols_per_data - I - 1) * g_symbol_w);  -- put big endian MS part t0 at array index 0

    u_acc : entity common_lib.common_accumulate
    generic map (
      g_representation => "SIGNED"
    )
    port map(
      rst     => rst,
      clk     => clk,
      sload   => in_sync,  -- Reload the accumlators with 0 at the sync or with the valid sample after the sync
      in_val  => in_val,
      in_dat  => symbol_arr(I),
      out_dat => acc_arr(I)
    );

    acc_vec((g_nof_symbols_per_data - I) * c_acc_w - 1 downto (g_nof_symbols_per_data - I - 1) * c_acc_w) <= acc_arr(I);  -- put array index 0 at big endian MS part t0
  end generate;

  no_tree : if g_nof_symbols_per_data = 1 generate
    -- Capture the current accumulator values at the reload
    nxt_sum      <= truncate_or_resize_svec(acc_vec, g_sum_truncate, g_sum_w) when in_sync = '1' else i_sum;
    nxt_sum_sync <= in_sync;
  end generate;

  gen_tree : if g_nof_symbols_per_data > 1 generate
    u_sum : entity common_lib.common_adder_tree
    generic map (
      g_representation => "SIGNED",
      g_pipeline       => c_acc_sum_pipeline,  -- amount of pipelining per stage
      g_nof_inputs     => g_nof_symbols_per_data,  -- >= 1, nof stages = ceil_log2(g_nof_inputs)
      g_dat_w          => c_acc_w,
      g_sum_w          => c_acc_sum_w
    )
    port map (
      clk    => clk,
      in_dat => acc_vec,
      sum    => acc_sum
    );

    u_in_sync_p : entity common_lib.common_pipeline_sl
    generic map (
      g_pipeline    => c_acc_sum_nof_stages * c_acc_sum_pipeline,  -- latency of common_adder_tree
      g_reset_value => 0
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => in_sync,
      out_dat => in_sync_p
    );

    -- Capture the current accumulator values at the reload (taking account of the latency of common_adder_tree)
    nxt_sum      <= truncate_or_resize_svec(acc_sum, g_sum_truncate, g_sum_w) when in_sync_p = '1' else i_sum;
    nxt_sum_sync <= in_sync_p;
  end generate;
end rtl;
