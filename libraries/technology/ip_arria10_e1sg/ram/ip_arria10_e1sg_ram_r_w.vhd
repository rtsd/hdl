-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- RadioHDL wrapper

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use technology_lib.technology_pkg.all;

library altera_lnsim;
use altera_lnsim.altera_lnsim_components.all;

entity ip_arria10_e1sg_ram_r_w is
  generic (
    g_inferred   : boolean := false;
    g_adr_w      : natural := 5;
    g_dat_w      : natural := 8;
    g_nof_words  : natural := 2**5;
    g_rd_latency : natural := 1;  -- choose 1 or 2
    g_init_file  : string  := "UNUSED"
  );
  port (
    clk         : in std_logic  := '1';
    data        : in std_logic_vector(g_dat_w - 1 downto 0) := (others => '0');
    rdaddress   : in std_logic_vector(g_adr_w - 1 downto 0) := (others => '0');
    wraddress   : in std_logic_vector(g_adr_w - 1 downto 0) := (others => '0');
    wren        : in std_logic  := '0';
    q           : out std_logic_vector(g_dat_w - 1 downto 0)
  );
end ip_arria10_e1sg_ram_r_w;

architecture SYN of ip_arria10_e1sg_ram_r_w is
  constant c_outdata_reg_b : string := tech_sel_a_b(g_rd_latency = 1, "UNREGISTERED", "CLOCK1");

  component altera_syncram
  generic (
          address_aclr_b  : string;
          address_reg_b  : string;
          clock_enable_input_a  : string;
          clock_enable_input_b  : string;
          clock_enable_output_b  : string;
          init_file  : string;
          intended_device_family  : string;
          lpm_type  : string;
          numwords_a  : integer;
          numwords_b  : integer;
          operation_mode  : string;
          outdata_aclr_b  : string;
          outdata_reg_b  : string;
          power_up_uninitialized  : string;
          widthad_a  : integer;
          widthad_b  : integer;
          width_a  : integer;
          width_b  : integer;
          width_byteena_a  : integer
  );
  port (
      address_a : in std_logic_vector(g_adr_w - 1 downto 0);
      address_b : in std_logic_vector(g_adr_w - 1 downto 0);
      clock0 : in std_logic;
      data_a : in std_logic_vector(g_dat_w - 1 downto 0);
      wren_a : in std_logic;
      q_b : out std_logic_vector(g_dat_w - 1 downto 0)
  );
  end component;

  signal rdaddr : natural range 0 to g_nof_words - 1;
  signal wraddr : natural range 0 to g_nof_words - 1;

  signal out_q  : std_logic_vector(g_dat_w - 1 downto 0);
  signal reg_q  : std_logic_vector(g_dat_w - 1 downto 0);
begin
  assert g_rd_latency = 1 or g_rd_latency = 2
    report "ip_arria10_e1sg_ram_r_w : read latency must be 1 (default) or 2"
    severity FAILURE;

  gen_ip : if g_inferred = false generate
    -- Copied from ip_arria10_e1sg_ram_r_w/ram_2port_140/sim/ip_arria10_e1sg_ram_r_w_ram_2port_140_hukd7xi.vhd
    u_altera_syncram : altera_syncram
    generic map (
            address_aclr_b  => "NONE",
            address_reg_b  => "CLOCK0",
            clock_enable_input_a  => "BYPASS",
            clock_enable_input_b  => "BYPASS",
            clock_enable_output_b  => "BYPASS",
            init_file  => g_init_file,
            intended_device_family  => "Arria 10",
            lpm_type  => "altera_syncram",
            numwords_a  => g_nof_words,
            numwords_b  => g_nof_words,
            operation_mode  => "DUAL_PORT",
            outdata_aclr_b  => "NONE",
            outdata_reg_b  => c_outdata_reg_b,
            power_up_uninitialized  => "FALSE",
            widthad_a  => g_adr_w,
            widthad_b  => g_adr_w,
            width_a  => g_dat_w,
            width_b  => g_dat_w,
            width_byteena_a  => 1
    )
    port map (
        address_a => wraddress,
        address_b => rdaddress,
        clock0 => clk,
        data_a => data,
        wren_a => wren,
        q_b => q
    );
  end generate;

  gen_inferred : if g_inferred = true generate
    rdaddr <= to_integer(unsigned(rdaddress));
    wraddr <= to_integer(unsigned(wraddress));

    u_mem : entity work.ip_arria10_e1sg_simple_dual_port_ram_single_clock
    generic map (
      DATA_WIDTH => g_dat_w,
      ADDR_WIDTH => g_adr_w
    )
    port map (
      clk   => clk,
      raddr => rdaddr,
      waddr => wraddr,
      data  => data,
      we    => wren,
      q     => out_q
    );

    reg_q <= out_q when rising_edge(clk);

    q <= out_q when g_rd_latency = 1 else reg_q;
  end generate;
end SYN;
