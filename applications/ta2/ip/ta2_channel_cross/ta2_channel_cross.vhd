-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Provide general I/O interface (BSP) for OpenCL kernel
-- Description:
-- . This core consists of:
--   . SOP/EOP insertion (kernel channel only carries data and valid)
--   . Dual clock FIFO
--     . Clock domain transition between kernel_clk and dp_clk
-- . Details:
--   . This core was developed for use with OpenCL IO channels.
--   . g_nof_bytes is used to indicate the number of bytes used, this should be between 1 and 64.
--   . The data field of the ST-avalon interface is also used to provide
--   . SOP, EOP and empty meta-data. The implementation of a g_nof_bytes=8 variant is shown below.
--   +-----------+---------+--------------------------------------------------------+
--   | Bit range | Name    | Description                                            |
--   +-----------+---------+--------------------------------------------------------+
--   | [0:63]    | payload | Packet payload                                         |
--   +-----------+---------+--------------------------------------------------------+
--   | 64        | sop     | Start of packet signal                                 |
--   +-----------+---------+--------------------------------------------------------+
--   | 65        | eop     | End of packet signal                                   |
--   +-----------+---------+--------------------------------------------------------+
--   | 66        | sync    | sync bit, always zero if g_use_sync = FALSE            |
--   +-----------+---------+--------------------------------------------------------+
--   | 67:68     | -       | reserved bits                                          |
--   +-----------+---------+--------------------------------------------------------+
--   | 69:71     | empty   | On EOP, this field indicates how many bytes are unused |
--   +-----------+---------+--------------------------------------------------------+
--   | 72:~      | error   | Error field, availability and size are dependent on    |
--   |           |         | generics                                               |
--   +-----------+---------+--------------------------------------------------------+
--   | ~:~       | bsn     | BSN field, availability and size are dependent on      |
--   |           |         | generics                                               |
--   +-----------+---------+--------------------------------------------------------+
--   | ~:~       | channel | channel field, availability and size are dependent on  |
--   |           |         | generics                                               |
--   +-----------+---------+--------------------------------------------------------+
-- Remark:
-- . This IP should be configured according to the corresponding IO channel in the OpenCL code.
-- . It may be nice to be able to configure a larger empty field to support g_nof_bytes > 32
--   but that would mean that the data structure in the OpenCL code must be adapted. Keep
--   in mind that IO channels must be a multiple of 8 bits (bytes).

library IEEE, common_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;

entity ta2_channel_cross is
  generic (
    g_nof_streams   : natural;
    g_nof_bytes     : positive;  -- nof bytes in payload field, Max = 32
    g_reverse_bytes : boolean := true;
    g_fifo_size     : natural := 8;
    g_use_err       : boolean := false;
    g_use_bsn       : boolean := false;
    g_use_channel   : boolean := false;
    g_use_sync      : boolean := false;
    g_err_w         : positive := 32;
    g_bsn_w         : positive := 64;
    g_channel_w     : positive := 32
  );
  port (
    dp_clk              : in  std_logic;
    dp_rst              : in  std_logic;

    dp_src_out_arr      : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    dp_src_in_arr       : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    dp_snk_out_arr      : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    dp_snk_in_arr       : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);

    kernel_clk          : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset        : in  std_logic;

    kernel_src_out_arr  : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    kernel_src_in_arr   : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    kernel_snk_out_arr  : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    kernel_snk_in_arr   : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst)

  );
end ta2_channel_cross;

architecture str of ta2_channel_cross is
  constant c_data_w    : natural := c_byte_w * g_nof_bytes;
  constant c_empty_w   : natural := ceil_log2(g_nof_bytes);
  constant c_err_w     : natural := sel_a_b(g_use_err, g_err_w, 0);
  constant c_bsn_w     : natural := sel_a_b(g_use_bsn, g_bsn_w, 0);
  constant c_channel_w : natural := sel_a_b(g_use_channel, g_channel_w, 0);

  constant c_sop_offset     : natural := g_nof_bytes * c_byte_w;
  constant c_eop_offset     : natural := g_nof_bytes * c_byte_w + 1;
  constant c_sync_offset    : natural := g_nof_bytes * c_byte_w + 2;
  constant c_empty_high     : natural := c_byte_w * (g_nof_bytes + 1);
  constant c_err_offset     : natural := c_byte_w * (g_nof_bytes + 1);
  constant c_bsn_offset     : natural := c_err_offset + c_err_w;
  constant c_channel_offset : natural := c_bsn_offset + c_bsn_w;

  signal dp_latency_adapter_tx_snk_in_arr  : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_latency_adapter_tx_snk_out_arr : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal dp_latency_adapter_tx_src_out_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_latency_adapter_tx_src_in_arr  : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_latency_adapter_rx_snk_in_arr  : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_latency_adapter_rx_snk_out_arr : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal dp_latency_adapter_rx_src_out_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_latency_adapter_rx_src_in_arr  : t_dp_siso_arr(g_nof_streams - 1 downto 0);
begin
  assert g_nof_bytes <= 32
    report "g_nof_bytes of ta2_channel_cross is configured higher than 32"
    severity ERROR;

  gen_streams: for stream in 0 to g_nof_streams - 1 generate
  -- dp_snk_in -> kernel_src_out

    ---------------------------------------------------------------------------------------
    -- TX FIFO: dp_clk -> kernel_clk
    ---------------------------------------------------------------------------------------
    u_dp_fifo_dc_tx : entity dp_lib.dp_fifo_dc
    generic map (
      g_data_w      => c_data_w,
      g_bsn_w       => c_bsn_w,
      g_empty_w     => c_empty_w,
      g_channel_w   => c_channel_w,
      g_error_w     => c_err_w,
      g_use_bsn     => g_use_bsn,
      g_use_empty   => true,
      g_use_channel => g_use_channel,
      g_use_error   => g_use_err,
      g_use_sync    => g_use_sync,
      g_fifo_size   => g_fifo_size
    )
    port map (
      wr_rst      => dp_rst,
      wr_clk      => dp_clk,
      rd_rst      => kernel_reset,
      rd_clk      => kernel_clk,

      snk_out     => dp_snk_out_arr(stream),
      snk_in      => dp_snk_in_arr(stream),

      src_in      => dp_latency_adapter_tx_snk_out_arr(stream),
      src_out     => dp_latency_adapter_tx_snk_in_arr(stream)
    );

    ----------------------------------------------------------------------------
    -- Latency adapter: adapt RL=1 (Upstream) to RL=0 (OpenCL kernel).
    ----------------------------------------------------------------------------
    u_dp_latency_adapter_tx : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 1,
      g_out_latency => 0
    )
    port map (
      clk       => kernel_clk,
      rst       => kernel_reset,

      snk_in    => dp_latency_adapter_tx_snk_in_arr(stream),
      snk_out   => dp_latency_adapter_tx_snk_out_arr(stream),

      src_out   => dp_latency_adapter_tx_src_out_arr(stream),
      src_in    => dp_latency_adapter_tx_src_in_arr(stream)
    );

    ----------------------------------------------------------------------------
    -- Data mapping
    ----------------------------------------------------------------------------
    -- Reverse byte order
    gen_reverse_rx_bytes : if g_reverse_bytes generate
      gen_rx_bytes: for I in 0 to g_nof_bytes - 1 generate
        kernel_src_out_arr(stream).data(c_byte_w * (g_nof_bytes - I) - 1 downto c_byte_w * (g_nof_bytes - 1 - I)) <= dp_latency_adapter_tx_src_out_arr(stream).data(c_byte_w * (I + 1) - 1 downto c_byte_w * I);
      end generate;
    end generate;

    gen_no_reverse_rx_bytes : if not g_reverse_bytes generate
        kernel_src_out_arr(stream).data(c_data_w - 1 downto 0) <= dp_latency_adapter_tx_src_out_arr(stream).data(c_data_w - 1 downto 0);
    end generate;

    -- Assign control signals to correct data fields.
    kernel_src_out_arr(stream).data(c_sop_offset)  <= dp_latency_adapter_tx_src_out_arr(stream).sop;
    kernel_src_out_arr(stream).data(c_eop_offset)  <= dp_latency_adapter_tx_src_out_arr(stream).eop;
    kernel_src_out_arr(stream).data(c_sync_offset) <= dp_latency_adapter_tx_src_out_arr(stream).sync when g_use_sync else '0';
    kernel_src_out_arr(stream).data(c_empty_high - 1 downto c_empty_high - c_empty_w) <= dp_latency_adapter_tx_src_out_arr(stream).empty(c_empty_w - 1 downto 0);
    kernel_src_out_arr(stream).valid <= dp_latency_adapter_tx_src_out_arr(stream).valid;
    dp_latency_adapter_tx_src_in_arr(stream).ready <= kernel_src_in_arr(stream).ready;
    dp_latency_adapter_tx_src_in_arr(stream).xon <= '1';

    -- Assign optional meta data signals to correct data fields.
    gen_err_out : if g_use_err generate
      kernel_src_out_arr(stream).data(c_err_offset + c_err_w - 1 downto c_err_offset) <= dp_latency_adapter_tx_src_out_arr(stream).err(c_err_w - 1 downto 0);
    end generate;

    gen_bsn_out : if g_use_bsn generate
      kernel_src_out_arr(stream).data(c_bsn_offset + c_bsn_w - 1 downto c_bsn_offset) <= dp_latency_adapter_tx_src_out_arr(stream).bsn(c_bsn_w - 1 downto 0);
    end generate;

    gen_channel_out : if g_use_channel generate
      kernel_src_out_arr(stream).data(c_channel_offset + c_channel_w - 1 downto c_channel_offset) <= dp_latency_adapter_tx_src_out_arr(stream).channel(c_channel_w - 1 downto 0);
    end generate;

    -- kernel_snk_in -> dp_src_out
    ----------------------------------------------------------------------------
    -- Data mapping
    ----------------------------------------------------------------------------
    -- Reverse byte order to correct for endianess
    gen_reverse_tx_bytes : if g_reverse_bytes generate
      gen_tx_bytes: for I in 0 to g_nof_bytes - 1 generate
        dp_latency_adapter_rx_snk_in_arr(stream).data(c_byte_w * (g_nof_bytes - I) - 1 downto c_byte_w * (g_nof_bytes - 1 - I)) <= kernel_snk_in_arr(stream).data(c_byte_w * (I + 1) - 1 downto c_byte_w * I);
      end generate;
    end generate;

    gen_no_reverse_tx_bytes : if not g_reverse_bytes generate
      dp_latency_adapter_rx_snk_in_arr(stream).data(c_data_w - 1 downto 0) <= kernel_snk_in_arr(stream).data(c_data_w - 1 downto 0);
    end generate;

    gen_err_in : if g_use_err generate
      dp_latency_adapter_rx_snk_in_arr(stream).err(c_err_w - 1 downto 0) <= kernel_snk_in_arr(stream).data(c_err_offset + c_err_w - 1 downto c_err_offset);
    end generate;

    gen_bsn_in : if g_use_bsn generate
      dp_latency_adapter_rx_snk_in_arr(stream).bsn(c_bsn_w - 1 downto 0) <= kernel_snk_in_arr(stream).data(c_bsn_offset + c_bsn_w - 1 downto c_bsn_offset);
    end generate;

    gen_channel_in : if g_use_channel generate
      dp_latency_adapter_rx_snk_in_arr(stream).channel(c_channel_w - 1 downto 0) <= kernel_snk_in_arr(stream).data(c_channel_offset + c_channel_w - 1 downto c_channel_offset);
    end generate;

    -- Assign correct data fields to control signals.
    dp_latency_adapter_rx_snk_in_arr(stream).sop <= kernel_snk_in_arr(stream).data(c_sop_offset);
    dp_latency_adapter_rx_snk_in_arr(stream).eop <= kernel_snk_in_arr(stream).data(c_eop_offset);
    dp_latency_adapter_rx_snk_in_arr(stream).sync <= kernel_snk_in_arr(stream).data(c_sync_offset) when g_use_sync else '0';
    dp_latency_adapter_rx_snk_in_arr(stream).empty(c_empty_w - 1 downto 0) <= kernel_snk_in_arr(stream).data(c_empty_high - 1 downto c_empty_high - c_empty_w);
    dp_latency_adapter_rx_snk_in_arr(stream).valid <= kernel_snk_in_arr(stream).valid;
    kernel_snk_out_arr(stream).ready <= dp_latency_adapter_rx_snk_out_arr(stream).ready;  -- Flow control towards source
    kernel_snk_out_arr(stream).xon <= dp_latency_adapter_rx_snk_out_arr(stream).xon;

    ----------------------------------------------------------------------------
    -- Latency adapter: adapt RL=0 (OpenCL kernel) to RL=1 (Downstream).
    ----------------------------------------------------------------------------
    u_dp_latency_adapter_rx : entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 0,
      g_out_latency => 1
    )
    port map (
      clk       => kernel_clk,
      rst       => kernel_reset,

      snk_in    => dp_latency_adapter_rx_snk_in_arr(stream),
      snk_out   => dp_latency_adapter_rx_snk_out_arr(stream),

      src_out   => dp_latency_adapter_rx_src_out_arr(stream),
      src_in    => dp_latency_adapter_rx_src_in_arr(stream)
    );

    ---------------------------------------------------------------------------------------
    -- RX FIFO: kernel_clk -> dp_clk
    ---------------------------------------------------------------------------------------
    u_dp_fifo_dc_rx : entity dp_lib.dp_fifo_dc
    generic map (
      g_data_w      => c_data_w,
      g_bsn_w       => c_bsn_w,
      g_empty_w     => c_empty_w,
      g_channel_w   => c_channel_w,
      g_error_w     => c_err_w,
      g_use_bsn     => g_use_bsn,
      g_use_empty   => true,
      g_use_channel => g_use_channel,
      g_use_error   => g_use_err,
      g_use_sync    => g_use_sync,
      g_fifo_size   => g_fifo_size
    )
    port map (
      wr_rst      => kernel_reset,
      wr_clk      => kernel_clk,
      rd_rst      => dp_rst,
      rd_clk      => dp_clk,

      snk_out     => dp_latency_adapter_rx_src_in_arr(stream),
      snk_in      => dp_latency_adapter_rx_src_out_arr(stream),

      src_in      => dp_src_in_arr(stream),
      src_out     => dp_src_out_arr(stream)
    );
 end generate;
end str;
