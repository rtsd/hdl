/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

// Positive High Voltage Hot Swap Controller with I2C Compatible Monitoring

#ifndef I2C_LTC4260_H
#define I2C_LTC4260_H

/* includes */
#include "common_types.h"

/* public constants */

// Mass write address (broadcast)
#define I2C_LTC4260_ADR_MW                  0x5F
// Alert response address
#define I2C_LTC4260_ADR_AR                  0x0C
// Other addresses
#define I2C_LTC4260_ADR_LOW_LOW_LOW         0x44

// Use SMBUS Write Byte or Read Byte to access the command registers
#define I2C_LTC4260_CMD_CONTROL             0
#define I2C_LTC4260_CMD_ALERT               1
#define I2C_LTC4260_CMD_STATUS              2
#define I2C_LTC4260_CMD_FAULT               3
#define I2C_LTC4260_CMD_SENSE               4
#define I2C_LTC4260_CMD_SOURCE              5
#define I2C_LTC4260_CMD_ADIN                6
  
// Voltage measurement units, use float for ease and accuracy or int for speed
// 0.3 mV over Rs (e.g. 10 mOhm) for current sense
#define I2C_LTC4260_V_UNIT_SENSE            ((TFloat32)0.0003)
#define I2C_LTC4260_uV_UNIT_SENSE          ((TUInt32)300)
// 400 mV supply voltage (e.g +48 V)
#define I2C_LTC4260_V_UNIT_SOURCE           ((TFloat32)0.4)
#define I2C_LTC4260_mV_UNIT_SOURCE         ((TUInt32)400)
// 10 mV ADC
#define I2C_LTC4260_V_UNIT_ADIN             ((TFloat32)0.01)
#define I2C_LTC4260_mV_UNIT_ADIN            ((TUInt32)10)
  
// 0x1B =  00 = power good
//       &  0 = disable test mode
//       &  1 = Enable massa write
//       &  1 = turn FET On
//       &  0 = Overcurrent Autoretry Disabled
//       &  1 = Undervoltage Autoretry Enabled
//       &  1 = Overvoltage Autoretry Enabled
#define I2C_LTC4260_CONTROL_DEFAULT         0x1B

/* public macros */

/* public types */

/* public variables */

/* public function protos */

#endif

