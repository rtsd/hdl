-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Clock an input slv from the in_clk domain into out_clk domain
-- Description:
--   See common_acapture and common_top.
-- Remark:
-- . A Stratix IV LAB contains 10 ALM so 20 FF. Hence common_acapture_slv can
--   fit in 1 LAB if in_dat'LENGTH <= 10.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity common_acapture_slv is
  generic (
    g_rst_level     : std_logic := '0';
    g_in_delay_len  : positive := 1;  -- = 1, typically fixed
    g_out_delay_len : positive := 1  -- >= 1, e.g. use c_meta_delay_len
  );
  port (
    in_rst  : in  std_logic := '0';
    in_clk  : in  std_logic;
    in_dat  : in  std_logic_vector;
    out_clk : in  std_logic;
    out_cap : out std_logic_vector
  );
end;

architecture str of common_acapture_slv is
  -- Provide in_cap to be able to view timing between out_clk and in_cap in Wave window
  signal in_cap : std_logic_vector(in_dat'range);
begin
  gen_slv: for I in in_dat'range generate
    u_acap : entity work.common_acapture
    generic map (
      g_rst_level     => g_rst_level,
      g_in_delay_len  => g_in_delay_len,
      g_out_delay_len => g_out_delay_len
    )
    port map (
      in_rst  => in_rst,
      in_clk  => in_clk,
      in_dat  => in_dat(I),
      in_cap  => in_cap(I),
      out_clk => out_clk,
      out_cap => out_cap(I)
    );
  end generate;
end str;
