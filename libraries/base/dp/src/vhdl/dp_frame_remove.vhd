--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_frame_remove is
  generic (
    g_data_w          : natural;
    g_symbol_w        : natural;
    g_hdr_nof_words   : natural;
    g_tail_nof_words  : natural;
    g_snk_latency     : natural := 1;
    g_internal_bypass : boolean := false  -- can be used to eliminate external GENERATE-statements/wiring
  );
  port (
    mm_rst  : in  std_logic;
    mm_clk  : in  std_logic;

    st_rst  : in  std_logic;
    st_clk  : in  std_logic;

    snk_out : out t_dp_siso;
    snk_in  : in  t_dp_sosi;

    sla_in  : in  t_mem_mosi := c_mem_mosi_rst;
    sla_out : out t_mem_miso;

    src_in  : in  t_dp_siso;
    src_out : out t_dp_sosi
  );
end dp_frame_remove;

architecture str of dp_frame_remove is
  constant c_tail_nof_symbols : natural := (g_tail_nof_words * g_symbol_w) / g_symbol_w;

  signal hdr_rem_siso   : t_dp_siso;
  signal hdr_rem_sosi   : t_dp_sosi;

  signal snk_out_rl_1   : t_dp_siso;
  signal snk_in_rl_1    : t_dp_sosi;
begin
  no_bypass : if g_internal_bypass = false generate
    u_dp_latency_adpapter: entity work.dp_latency_adapter
    generic map (
      g_in_latency  => g_snk_latency,
      g_out_latency => 1
    )
    port map (
      rst      => st_rst,
      clk      => st_clk,

      snk_out  => snk_out,
      snk_in   => snk_in,

      src_out  => snk_in_rl_1,
      src_in   => snk_out_rl_1
    );

    u_dp_hdr_remove : entity work.dp_hdr_remove
    generic map (
      g_data_w        => g_data_w,
      g_symbol_w      => g_symbol_w,
      g_hdr_nof_words => g_hdr_nof_words
    )
    port map (
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,

      st_rst      => st_rst,
      st_clk      => st_clk,

      snk_out     => snk_out_rl_1,
      snk_in      => snk_in_rl_1,

      sla_in      => sla_in,
      sla_out     => sla_out,

      src_in      => hdr_rem_siso,
      src_out     => hdr_rem_sosi
    );

    u_dp_tail_remove : entity work.dp_tail_remove
    generic map (
      g_data_w      => g_data_w,
      g_symbol_w    => g_symbol_w,
      g_nof_symbols => c_tail_nof_symbols
    )
    port map (
      st_rst  => st_rst,
      st_clk  => st_clk,

      snk_out => hdr_rem_siso,
      snk_in  => hdr_rem_sosi,

      src_in  => src_in,
      src_out => src_out
    );
  end generate;

  gen_bypass : if g_internal_bypass = true generate
    src_out <= snk_in;
    snk_out <= src_in;
  end generate;
end str;
