-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_fifo_dc is
  generic (
    -- Try FIFO settings
    g_dut_wr_clk_freq  : positive := 2;  -- normalized write clock frequency
    g_dut_rd_clk_freq  : positive := 3;  -- normalized read  clock frequency
    g_dut_use_bsn      : boolean := true;
    g_dut_use_empty    : boolean := true;
    g_dut_use_channel  : boolean := true;
    g_dut_use_sync     : boolean := true;
    g_dut_use_ctrl     : boolean := true;
    g_dut_out_latency  : natural := 1  -- selectable for dp_fifo_dc: default 1 or 0 for look ahead FIFO
  );
end tb_dp_fifo_dc;

architecture tb of tb_dp_fifo_dc is
  -- See tb_dp_pkg.vhd for explanation and run time, increase the run time by g_dut_rd_clk_freq/g_dut_wr_clk_freq if g_dut_rd_clk_freq>g_dut_wr_clk_freq

  -- DUT
  constant c_dut_fifo_size   : natural := 64;
  constant c_dut_in_latency  : natural := 1;  -- fixed for dp_fifo_dc

  -- Stimuli
  constant c_tx_latency     : natural := c_dut_in_latency;  -- TX ready latency of TB
  constant c_tx_void        : natural := sel_a_b(c_tx_latency, 1, 0);  -- used to avoid empty range VHDL warnings when c_tx_latency=0
  constant c_tx_offset_sop  : natural := 3;
  constant c_tx_period_sop  : natural := 7;  -- sop in data valid cycle 3,  10,  17, ...
  constant c_tx_offset_eop  : natural := 5;  -- eop in data valid cycle   5,  12,  19, ...
  constant c_tx_period_eop  : natural := c_tx_period_sop;
  constant c_tx_offset_sync : natural := 3;  -- sync in data valid cycle 3, 20, 37, ...
  constant c_tx_period_sync : natural := 17;
  constant c_rx_latency     : natural := g_dut_out_latency;  -- RX ready latency from DUT
  constant c_verify_en_wait : natural := 20;  -- wait some cycles before asserting verify enable

  constant c_bsn_offset     : natural := 1;
  constant c_empty_offset   : natural := 2;
  constant c_channel_offset : natural := 3;

  constant c_random_w       : natural := 19;

  signal tb_end         : std_logic := '0';
  signal wr_clk         : std_logic := '0';
  signal rd_clk         : std_logic := '0';
  signal rst            : std_logic;
  signal sync           : std_logic;
  signal lfsr1          : std_logic_vector(c_random_w - 1 downto 0) := (others => '0');
  signal lfsr2          : std_logic_vector(c_random_w   downto 0) := (others => '0');

  signal cnt_dat        : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal cnt_val        : std_logic;
  signal cnt_en         : std_logic;

  signal tx_data        : t_dp_data_arr(0 to c_tx_latency + c_tx_void)    := (others => (others => '0'));
  signal tx_val         : std_logic_vector(0 to c_tx_latency + c_tx_void) := (others => '0');

  signal in_ready       : std_logic;
  signal in_data        : std_logic_vector(c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_bsn         : std_logic_vector(c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_empty       : std_logic_vector(c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_channel     : std_logic_vector(c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_sync        : std_logic;
  signal in_val         : std_logic;
  signal in_sop         : std_logic;
  signal in_eop         : std_logic;

  signal in_siso        : t_dp_siso;
  signal in_sosi        : t_dp_sosi := c_dp_sosi_rst;
  signal out_siso       : t_dp_siso;
  signal out_sosi       : t_dp_sosi;

  signal out_ready      : std_logic;
  signal prev_out_ready : std_logic_vector(0 to c_rx_latency);
  signal out_data       : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal out_bsn        : std_logic_vector(c_dp_data_w - 1 downto 0) := (others => '0');
  signal out_empty      : std_logic_vector(c_dp_data_w - 1 downto 0) := (others => '0');
  signal out_channel    : std_logic_vector(c_dp_data_w - 1 downto 0) := (others => '0');
  signal out_sync       : std_logic;
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_eop        : std_logic;
  signal prev_out_data  : std_logic_vector(out_data'range);

  signal state          : t_dp_state_enum;

  signal verify_en      : std_logic;
  signal verify_done    : std_logic;

  signal exp_data       : std_logic_vector(c_dp_data_w - 1 downto 0) := TO_UVEC(19000 / g_dut_wr_clk_freq, c_dp_data_w);

  signal usedw          : std_logic_vector(ceil_log2(c_dut_fifo_size) - 1 downto 0);
begin
  wr_clk <= not wr_clk or tb_end after g_dut_rd_clk_freq * clk_period / 2;
  rd_clk <= not rd_clk or tb_end after g_dut_wr_clk_freq * clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  -- Sync interval
  proc_dp_sync_interval(wr_clk, sync);

  -- Input data
  cnt_val <= in_ready and cnt_en;

  proc_dp_cnt_dat(rst, wr_clk, cnt_val, cnt_dat);
  proc_dp_tx_data(c_tx_latency, rst, wr_clk, cnt_val, cnt_dat, tx_data, tx_val, in_data, in_val);
  proc_dp_tx_ctrl(c_tx_offset_sync, c_tx_period_sync, in_data, in_val, in_sync);
  proc_dp_tx_ctrl(c_tx_offset_sop, c_tx_period_sop, in_data, in_val, in_sop);
  proc_dp_tx_ctrl(c_tx_offset_eop, c_tx_period_eop, in_data, in_val, in_eop);

  in_bsn     <= INCR_UVEC(in_data, c_bsn_offset);
  in_empty   <= INCR_UVEC(in_data, c_empty_offset);
  in_channel <= INCR_UVEC(in_data, c_channel_offset);

  -- Stimuli control
  proc_dp_count_en(rst, wr_clk, sync, lfsr1, state, verify_done, tb_end, cnt_en);
  proc_dp_out_ready(rst, wr_clk, sync, lfsr2, out_ready);

  -- Output verify
  proc_dp_verify_en(c_verify_en_wait, rst, rd_clk, sync, verify_en);
  proc_dp_verify_data("out_sosi.data", c_rx_latency, rd_clk, verify_en, out_ready, out_val, out_data, prev_out_data);
  proc_dp_verify_valid(c_rx_latency, rd_clk, verify_en, out_ready, prev_out_ready, out_val);

  gen_verify_sync : if g_dut_use_sync = true generate
    proc_dp_verify_ctrl(c_tx_offset_sync, c_tx_period_sync, "sync", rd_clk, verify_en, out_data, out_val, out_sync);
  end generate;

  gen_verify_ctrl : if g_dut_use_ctrl = true generate
    proc_dp_verify_ctrl(c_tx_offset_sop, c_tx_period_sop, "sop", rd_clk, verify_en, out_data, out_val, out_sop);
    proc_dp_verify_ctrl(c_tx_offset_eop, c_tx_period_eop, "eop", rd_clk, verify_en, out_data, out_val, out_eop);
  end generate;

  gen_verify_bsn : if g_dut_use_bsn = true generate
    proc_dp_verify_other_sosi("bsn", INCR_UVEC(out_data, c_bsn_offset), rd_clk, verify_en, out_bsn);
  end generate;

  gen_verify_empty : if g_dut_use_empty = true generate
    proc_dp_verify_other_sosi("empty", INCR_UVEC(out_data, c_empty_offset), rd_clk, verify_en, out_empty);
  end generate;

  gen_verify_channel : if g_dut_use_channel = true generate
    proc_dp_verify_other_sosi("channel", INCR_UVEC(out_data, c_channel_offset), rd_clk, verify_en, out_channel);
  end generate;

  -- Check that the test has ran at all
  proc_dp_verify_value(e_at_least, rd_clk, verify_done, exp_data, out_data);

  ------------------------------------------------------------------------------
  -- DUT dp_fifo_dc
  ------------------------------------------------------------------------------

  -- map sl, slv to record
  in_ready <= in_siso.ready;  -- SISO
  in_sosi.data(c_dp_data_w - 1 downto 0) <= in_data;  -- SOSI
  in_sosi.bsn(c_dp_bsn_w - 1 downto 0)   <= in_bsn(c_dp_bsn_w - 1 downto 0);
  in_sosi.empty                        <= in_empty(c_dp_empty_w - 1 downto 0);
  in_sosi.channel                      <= in_channel(c_dp_channel_w - 1 downto 0);
  in_sosi.sync                         <= in_sync;
  in_sosi.valid                        <= in_val;
  in_sosi.sop                          <= in_sop;
  in_sosi.eop                          <= in_eop;

  out_siso.ready <= out_ready;  -- SISO
  out_data                               <= out_sosi.data(c_dp_data_w - 1 downto 0);  -- SOSI
  out_bsn(c_dp_bsn_w - 1 downto 0)         <= out_sosi.bsn(c_dp_bsn_w - 1 downto 0);
  out_empty(c_dp_empty_w - 1 downto 0)     <= out_sosi.empty;
  out_channel(c_dp_channel_w - 1 downto 0) <= out_sosi.channel;
  out_sync                               <= out_sosi.sync;
  out_val                                <= out_sosi.valid;
  out_sop                                <= out_sosi.sop;
  out_eop                                <= out_sosi.eop;

  dut : entity work.dp_fifo_dc
  generic map (
    g_data_w      => c_dp_data_w,
    g_bsn_w       => c_dp_bsn_w,
    g_empty_w     => c_dp_empty_w,
    g_channel_w   => c_dp_channel_w,
    g_error_w     => 1,
    g_use_bsn     => g_dut_use_bsn,
    g_use_empty   => g_dut_use_empty,
    g_use_channel => g_dut_use_channel,
    g_use_error   => false,
    g_use_sync    => g_dut_use_sync,
    g_use_ctrl    => g_dut_use_ctrl,
    g_fifo_size   => c_dut_fifo_size,
    g_fifo_rl     => g_dut_out_latency
  )
  port map (
    wr_rst      => rst,
    wr_clk      => wr_clk,
    rd_rst      => rst,
    rd_clk      => rd_clk,
    snk_out     => in_siso,  -- OUT = request to upstream ST source
    snk_in      => in_sosi,
    wr_usedw    => usedw,
    rd_usedw    => OPEN,
    src_in      => out_siso,  -- IN  = request from downstream ST sink
    src_out     => out_sosi
  );
end tb;
