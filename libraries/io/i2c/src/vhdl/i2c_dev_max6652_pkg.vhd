-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

package i2c_dev_max6652_pkg is
  constant MAX6652_ADR_GND                 : natural := 2#0010100#;
  constant MAX6652_ADR_VCC                 : natural := 2#0010101#;
  constant MAX6652_ADR_SDA                 : natural := 2#0010110#;
  constant MAX6652_ADR_SCL                 : natural := 2#0010111#;

  constant MAX6652_REG_READ_VIN_2_5        : natural := 16#20#;
  constant MAX6652_REG_READ_VIN_12         : natural := 16#21#;
  constant MAX6652_REG_READ_VIN_3_3        : natural := 16#22#;
  constant MAX6652_REG_READ_VCC            : natural := 16#23#;
  constant MAX6652_REG_READ_TEMP           : natural := 16#27#;

  constant MAX6652_REG_HIGH_LIMIT_VIN_2_5  : natural := 16#2B#;
  constant MAX6652_REG_LOW_LIMIT_VIN_2_5   : natural := 16#2C#;
  constant MAX6652_REG_HIGH_LIMIT_VIN_12   : natural := 16#2D#;
  constant MAX6652_REG_LOW_LIMIT_VIN_12    : natural := 16#2E#;
  constant MAX6652_REG_HIGH_LIMIT_VIN_3_3  : natural := 16#2F#;
  constant MAX6652_REG_LOW_LIMIT_VIN_3_3   : natural := 16#30#;
  constant MAX6652_REG_HIGH_LIMIT_VCC      : natural := 16#31#;
  constant MAX6652_REG_LOW_LIMIT_VCC       : natural := 16#32#;
  constant MAX6652_REG_HOT_TEMP_LIMIT      : natural := 16#39#;
  constant MAX6652_REG_HOT_TEMP_HIST       : natural := 16#3A#;

  constant MAX6652_REG_CONFIG              : natural := 16#40#;
  constant MAX6652_REG_INT_STATUS          : natural := 16#41#;
  constant MAX6652_REG_INT_MASK            : natural := 16#43#;
  constant MAX6652_REG_DEV_ADR             : natural := 16#48#;
  constant MAX6652_REG_TEMP_CONFIG         : natural := 16#4B#;

  constant MAX6652_CONFIG_START            : natural := 16#01#;
  constant MAX6652_CONFIG_INT_EN           : natural := 16#02#;
  constant MAX6652_CONFIG_INT_CLR          : natural := 16#04#;
  constant MAX6652_CONFIG_LINE_FREQ_SEL    : natural := 16#10#;
  constant MAX6652_CONFIG_SHORT_CYCLE      : natural := 16#20#;
  constant MAX6652_CONFIG_RESET            : natural := 16#80#;
end package;
