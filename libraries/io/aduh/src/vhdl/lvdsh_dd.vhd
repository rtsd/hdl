-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Handle an double data rate input interface without using a PLL
-- Description:
--   The in_clk is a double data rate clock. The DDIO elements near the pins
--   capture the in_dat using the out_clk into two parts out_dat_hi and
--   out_dat_lo. The nominal data rate out_clk is the delayed in_clk. The
--   double width output data out_dat_hi and out_dat_lo are available at the
--   output, but can also be read via a FIFO.
--   Dependend on g_rx_factor the FIFO rx_clk can run at the same rate as the
--   out_clk (= delayed in_clk) or at a lower rate. The rx_clk must be locked
--   to the in_clk or relatively faster to avoid FIFO overflow.
--   With ADU the in_clk is the 800M sample clock divided by 2 and the rx_clk
--   is the sample clock divided by 4. The phase of the in_clk depends on the
--   divider phase of the ADC and can be set by in_clk_rst. The phase of the
--   rx_clk with respect to the in_clk depends on the divide by 2 that is done
--   by the mixed width FIFO and can be set by wr_fifo_rst.
--   The in_clk and the rx_clk have a fixed but unknown phase relation. It is
--   important to maintain this phase relation inside the FPGA. This is taken
--   care of thanks to:
--   . the synchronous clock tree network inside an FPGA that is balanced such
--     that the clock has the same phase at any location in the FPGA
--   . using a DDIO register near the pin to output in_clk_rst
--   . using a LogicLock region constraint on u_acapture to have a fixed timing
--     of wr_fifo_rst
-- Remarks:
-- . The input data and clock can be delayed via the g_in_dat_delay_arr and
--   g_in_clk_delay generics. For the Stratix4 the delay can be 0..15 steps of
--   50 ps via delay element D1 in the IO input buffer. If the input buffer
--   supports dynamic delay configuration via MM interface then that requires
--   using the config_clk. The common_iobuf_in then needs to support the
--   generic delay setting as input signals. For the UniBoard back nodes it
--   appears that not all in_dat input buffers support config_clk. Therefore
--   instead of using the generics the input buffer delays are set via
--   constraints in the synthesis file.
-- . Default the rx_dat output is little endian meaning that the first input
--   data appears in the LSpart. Use g_rx_big_endian=TRUE to have big endian
--   rx_dat conform the streaming interface data.

entity lvdsh_dd is
  generic (
    g_dd_factor         : natural := 2;  -- = 2, fixed double data rate factor
    g_in_dat_w          : natural := 16;  -- nof PHY data bits
    g_in_dat_delay_arr  : t_natural_arr := array_init(0, 16);  -- nof must match g_in_dat_w
    g_in_clk_delay      : natural := 0;
    g_in_clk_rst_invert : boolean := false;
    g_use_in_delay      : boolean := false;
    g_rx_big_endian     : boolean := false;
    g_rx_factor         : natural := 1;  -- 1, 2, 4, ... must be a power of 2 because of the mixed width FIFO
    g_rx_fifo_size      : natural := 32;  -- see common_fifo_dc_lock_control for comment
    g_rx_fifo_fill      : natural := 16;  -- see common_fifo_dc_lock_control for comment
    g_rx_fifo_margin    : natural := 0  -- use 0, 1 independend of g_rx_factor, because the mixed width FIFO (g_rx_factor > 1) goes from narrow wr to wide rx, so +-1 is enough for wide side.
  );
  port (
    -- PHY input delay config clock
    config_rst    : in  std_logic := '1';
    config_clk    : in  std_logic := '1';
    config_done   : out std_logic;

    -- PHY input interface
    in_clk        : in  std_logic;
    in_dat        : in  std_logic_vector(g_in_dat_w - 1 downto 0);  -- sample [t0], [t1], [t2], [t3], [t4], [t5], [t6], [t7], ... --> time
    in_clk_rst    : out std_logic;

    -- DD domain output interface (no FIFO)
    out_clk       : out std_logic;
    out_dat_hi    : out std_logic_vector(g_in_dat_w - 1 downto 0);  -- sample [t1], [t3], [t5], [t7], ...
    out_dat_lo    : out std_logic_vector(g_in_dat_w - 1 downto 0);  -- sample [t0], [t2], [t4], [t6], ...

    -- DD --> Rx domain interface at in_clk rate or g_rx_factor lower rate (via FIFO)
    rx_rst        : in  std_logic := '1';
    rx_clk        : in  std_logic := '1';  -- default little endian rx_dat output:
    rx_dat        : out std_logic_vector(g_rx_factor * g_dd_factor * g_in_dat_w - 1 downto 0);  -- . sample [t1, t0], [t3, t2], [t5, t4], [t7, t6],  ... when g_rx_factor = 1
    rx_val        : out std_logic;  -- . sample [t3, t2, t1, t0],   [t7, t6, t5, t4],    ... when g_rx_factor = 2

    -- Rx FIFO control
    rx_locked     : out std_logic;
    rx_stable     : out std_logic;
    rx_stable_ack : in  std_logic := '0'
  );
end lvdsh_dd;

architecture str of lvdsh_dd is
  constant c_rx_fifo_lsusedw_w  : natural := true_log2(g_rx_factor);  -- nof least significant bits of wrusedw that are skipped for rdusedw due to parallelization by g_rx_factor
  constant c_rx_fifo_rdusedw_w  : natural := ceil_log2(g_rx_fifo_size);
  constant c_rx_fifo_wrusedw_w  : natural := c_rx_fifo_rdusedw_w + c_rx_fifo_lsusedw_w;

  constant c_in_dly_w        : natural := 1 + g_in_dat_w;  -- 1 extra for the in_clk
  constant c_in_delay_arr    : t_natural_arr(c_in_dly_w - 1 downto 0) := g_in_clk_delay & g_in_dat_delay_arr;  -- [16, 15:0]

  constant c_out_dat_w       : natural := g_dd_factor * g_in_dat_w;  -- hi & lo

  -- DD clock domain (in_clk = wr_clk = out_clk)
  signal in_vec              : std_logic_vector(c_in_dly_w - 1 downto 0);
  signal in_dly              : std_logic_vector(c_in_dly_w - 1 downto 0);

  signal wr_clk              : std_logic;
  signal wr_dat_hi           : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal wr_dat_lo           : std_logic_vector(g_in_dat_w - 1 downto 0);

  signal fifo_wr_ful         : std_logic;
  signal fifo_wr_req         : std_logic := '0';
  signal fifo_wr_dat         : std_logic_vector(c_out_dat_w - 1 downto 0);  -- hi & lo
  signal nxt_fifo_wr_dat     : std_logic_vector(c_out_dat_w - 1 downto 0);  -- hi & lo

  -- Cross clock domains from wr_clk to rd_clk
  signal fifo_wrusedw        : std_logic_vector(c_rx_fifo_wrusedw_w - 1 downto 0);  -- wr_clk domain
  signal fifo_rdusedw        : std_logic_vector(c_rx_fifo_rdusedw_w - 1 downto 0);  -- wr_clk domain
  signal rx_fifo_rdusedw     : std_logic_vector(c_rx_fifo_rdusedw_w - 1 downto 0);  -- rd_clk domain

  -- Cross clock domains from rd_clk to wr_clk
  signal wr_clk_rst          : std_logic_vector(0 downto 0);  -- rd_clk domain
  signal rx_clk_rst          : std_logic_vector(0 downto 0);  -- rd_clk domain
  signal i_in_clk_rst        : std_logic_vector(0 downto 0);  -- wr_clk domain

  signal dc_fifo_rst         : std_logic;  -- rd_clk domain
  signal wr_fifo_rst         : std_logic;  -- wr_clk domain

  -- Rx clock domain (rx_clk = rd_clk) for DSP
  signal rx_fifo_rd_req      : std_logic;
  signal rx_fifo_rd_dat      : std_logic_vector(rx_dat'range);
begin
  -----------------------------------------------------------------------------
  -- Optional input buffer delay control via generic
  -----------------------------------------------------------------------------

  -- Input delay
  in_vec <= in_clk & in_dat;  -- [16, 15:0]

  gen_in_dly : if g_use_in_delay = true generate
    u_buf_in : entity common_lib.common_iobuf_in
    generic map (
      g_width     => c_in_dly_w,
      g_delay_arr => c_in_delay_arr
    )
    port map (
      config_rst  => config_rst,
      config_clk  => config_clk,
      config_done => config_done,
      in_dat      => in_vec,
      out_dat     => in_dly
    );
  end generate;

  no_in_dly : if g_use_in_delay = false generate
    -- Define the required input delay via the fitter assignment settings
    config_done <= '1';
    in_dly      <= in_vec;
  end generate;

  -----------------------------------------------------------------------------
  -- Register double data rate input data
  -----------------------------------------------------------------------------

  -- Input double data rate at pin, also ensures deterministic input timing
  u_dd_in : entity common_lib.common_ddio_in
  generic map (
    g_width    => g_in_dat_w
  )
  port map (
    in_dat      => in_dly(c_in_dly_w - 2 downto 0),
    in_clk      => in_dly(c_in_dly_w - 1),
    in_clk_en   => '1',
    rst         => '0',
    out_dat_hi  => wr_dat_hi,
    out_dat_lo  => wr_dat_lo
  );

  -----------------------------------------------------------------------------
  -- Reset in_clk from rx_clk domain
  -----------------------------------------------------------------------------

  -- Use output register at pin for in_clk_rst to ensure deterministic output timing
  rx_clk_rst(0) <= wr_clk_rst(0) when g_in_clk_rst_invert = false else not wr_clk_rst(0);

  u_dd_out : entity common_lib.common_ddio_out
  generic map (
    g_width  => 1
  )
  port map (
    rst        => '0',
    in_clk     => rx_clk,
    in_clk_en  => '1',
    in_dat_hi  => rx_clk_rst,
    in_dat_lo  => rx_clk_rst,
    out_dat    => i_in_clk_rst
  );

  in_clk_rst  <= i_in_clk_rst(0);

  -----------------------------------------------------------------------------
  -- Register release of dc_fifo_rst into the wr_clk domain with 'fixed' delay
  -----------------------------------------------------------------------------

  -- Use a LogicLock region of 1 LAB on this instance to avoid variation in placement, in order to achieve a sufficiently 'fixed' data delay
  -- between the two clock domains that does not vary dependent on the size of the rest of the design.

  u_acapture_fifo_rst : entity common_lib.common_acapture
  generic map (
    g_rst_level     => '1',
    g_in_delay_len  => 1,
    g_out_delay_len => 1
  )
  port map (
    in_rst  => dc_fifo_rst,  -- need to apply dc_fifo_rst asynchronously to reset rd_usedw in case of dc lock lost due to stopped wr_clk
    in_clk  => rx_clk,
    in_dat  => '0',  -- connecting '0' is equivalent to connecting dc_fifo_rst
    out_clk => wr_clk,
    out_cap => wr_fifo_rst
  );

  -----------------------------------------------------------------------------
  -- Register wrusedw into the rd_clk domain with 'fixed' delay
  -----------------------------------------------------------------------------

  -- Use a LogicLock region of 1 LAB on this instance to avoid variation in placement, in order to achieve a sufficiently 'fixed' data delay
  -- between the two clock domains that does not vary dependent on the size of the rest of the design.

  -- Can not use the rdusedw from the FIFO, because for that the path delay depends on the size of the rest of the design. The common_acapture_slv
  -- registers the wrused into FF0 with the wr_clk and captures it with the rd_clk into FF1. For g_rx_factor>1 the LSBit(s) of the captured
  -- wrusedw can be ignored, e.g. for g_rx_factor=2 bit 0 is always '0'. By using one FF0 the timing of the rd_clk with respect to the wr_usedw(h:1)
  -- is such that the eye-window has maximum size. Using an even number of FF0 would make the rd_clk sample the wr_usedw(h:1) closer to the edge
  -- of the eye.

  u_acapture_slv_fifo_rdusedw : entity common_lib.common_acapture_slv
  generic map (
    g_rst_level     => '0',
    g_in_delay_len  => 1,
    g_out_delay_len => 1
  )
  port map (
    in_rst  => wr_fifo_rst,
    in_clk  => wr_clk,
    in_dat  => fifo_rdusedw,
    out_clk => rx_clk,
    out_cap => rx_fifo_rdusedw
  );

  ------------------------------------------------------------------------------
  -- Output direct at input clock rate and double width
  ------------------------------------------------------------------------------

  wr_clk <= in_dly(c_in_dly_w - 1);

  out_clk    <= wr_clk;
  out_dat_hi <= wr_dat_hi;
  out_dat_lo <= wr_dat_lo;

  ------------------------------------------------------------------------------
  -- Output via FIFO at same clock rate or at lower clock rate in rx_clk domain
  ------------------------------------------------------------------------------

  -- Register fifo_wr_dat to ease timing closure between DDIO and FIFO at input clock rate
  nxt_fifo_wr_dat <= wr_dat_hi & wr_dat_lo;

  u_dd_reg : entity common_lib.common_pipeline
  generic map (
    g_pipeline    => 1,
    g_in_dat_w    => c_out_dat_w,
    g_out_dat_w   => c_out_dat_w
  )
  port map (
    clk     => wr_clk,
    in_dat  => nxt_fifo_wr_dat,
    out_dat => fifo_wr_dat
  );

  -- Input FIFO dual clock lock control
  u_fifo_dc_lock_control : entity common_lib.common_fifo_dc_lock_control
  generic map (
    g_hold_wr_clk_rst  => 2,  -- >= 1, nof cycles to hold the wr_clk_rst
    g_hold_dc_fifo_rst => 31,  -- >= 1, nof cycles to hold the dc_fifo_rst, sufficiently long for wr_clk to have restarted after wr_clk_rst release
    g_rd_fill_level    => g_rx_fifo_fill,
    g_rd_fill_margin   => g_rx_fifo_margin
  )
  port map (
    -- FIFO rd_clk domain
    rd_rst        => rx_rst,
    rd_clk        => rx_clk,
    rd_usedw      => rx_fifo_rdusedw,
    rd_req        => rx_fifo_rd_req,
    wr_clk_rst    => wr_clk_rst(0),
    dc_fifo_rst   => dc_fifo_rst,

    -- MM in rd_clk domain
    rd_fill_level => g_rx_fifo_fill,
    dc_locked     => rx_locked,
    dc_stable     => rx_stable,
    dc_stable_ack => rx_stable_ack
  );

  -- No need to check on fifo_wr_ful for fifo_wr_req, because wr_init in common_fifo_dc* takes care that fifo_wr_req is only passed on after fifo_wr_ful went low after reset release.
  fifo_wr_req <= '1';

  -- Dual clock FIFO, same width
  gen_same_rate : if g_rx_factor = 1 generate
    u_fifo_dc : entity common_lib.common_fifo_dc
    generic map (
      g_dat_w     => c_out_dat_w,
      g_nof_words => g_rx_fifo_size
    )
    port map (
      rst     => wr_fifo_rst,
      wr_clk  => wr_clk,
      wr_dat  => fifo_wr_dat,
      wr_req  => fifo_wr_req,
      wr_ful  => fifo_wr_ful,
      wrusedw => fifo_wrusedw,
      rd_clk  => rx_clk,
      rd_dat  => rx_fifo_rd_dat,
      rd_req  => rx_fifo_rd_req,
      rd_emp  => OPEN,
      rdusedw => OPEN,  -- instead use wrusedw via common_acapture_slv
      rd_val  => rx_val
    );

    fifo_rdusedw <= fifo_wrusedw;
  end generate;

  -- Dual clock FIFO, mixed width
  gen_lower_rate : if g_rx_factor > 1 generate
    u_fifo_n2w : entity common_lib.common_fifo_dc_mixed_widths
    generic map (
      g_nof_words => g_rx_fifo_size * g_rx_factor,  -- FIFO size in nof wr_dat words
      g_wr_dat_w  => c_out_dat_w,
      g_rd_dat_w  => c_out_dat_w * g_rx_factor
    )
    port map (
      rst     => wr_fifo_rst,
      wr_clk  => wr_clk,
      wr_dat  => fifo_wr_dat,
      wr_req  => fifo_wr_req,
      wr_ful  => fifo_wr_ful,
      wrusedw => fifo_wrusedw,
      rd_clk  => rx_clk,
      rd_dat  => rx_fifo_rd_dat,
      rd_req  => rx_fifo_rd_req,
      rd_emp  => OPEN,
      rdusedw => OPEN,  -- instead use wrusedw via common_acapture_slv
      rd_val  => rx_val
    );

    fifo_rdusedw <= fifo_wrusedw(fifo_wrusedw'high downto c_rx_fifo_lsusedw_w);
  end generate;

  gen_little_endian : if g_rx_big_endian = false generate
    rx_dat <= rx_fifo_rd_dat;
  end generate;

  gen_big_endian : if g_rx_big_endian = true generate
    rx_dat <= hton(rx_fifo_rd_dat, g_in_dat_w, g_rx_factor * g_dd_factor);  -- rewire
  end generate;
end str;
