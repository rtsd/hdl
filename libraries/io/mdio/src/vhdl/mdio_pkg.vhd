-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
--USE common_lib.common_pkg.ALL;

package mdio_pkg is
  constant c_mdio_phy_data_width         : natural := 16;

  constant c_mdio_phy_hdr_st_len         : natural := 2;
  constant c_mdio_phy_hdr_op_addr_len    : natural := 2;
  constant c_mdio_phy_hdr_op_wr_len      : natural := 2;
  constant c_mdio_phy_hdr_op_rd_len      : natural := 2;
  constant c_mdio_phy_hdr_op_rd_incr_len : natural := 2;
  constant c_mdio_phy_hdr_prtad_len      : natural := 5;
  constant c_mdio_phy_hdr_devad_len      : natural := 5;
  constant c_mdio_phy_hdr_ta_len         : natural := 2;

  constant c_mdio_phy_hdr_total_len      : natural := c_mdio_phy_hdr_st_len        +
                                                      c_mdio_phy_hdr_op_addr_len   +
                                                      c_mdio_phy_hdr_op_wr_len     +
                                                      c_mdio_phy_hdr_op_rd_len     +
                                                      c_mdio_phy_hdr_op_rd_incr_len +
                                                      c_mdio_phy_hdr_prtad_len     +
                                                      c_mdio_phy_hdr_devad_len     +
                                                      c_mdio_phy_hdr_ta_len;

  constant c_mdio_phy_hdr_st         : std_logic_vector(c_mdio_phy_hdr_st_len         - 1 downto 0) := "00";  -- start of frame
  constant c_mdio_phy_hdr_op_addr    : std_logic_vector(c_mdio_phy_hdr_op_addr_len    - 1 downto 0) := "00";  -- operation code address
  constant c_mdio_phy_hdr_op_wr      : std_logic_vector(c_mdio_phy_hdr_op_wr_len      - 1 downto 0) := "01";  -- operation code write
  constant c_mdio_phy_hdr_op_rd      : std_logic_vector(c_mdio_phy_hdr_op_rd_len      - 1 downto 0) := "11";  -- operation code read
  constant c_mdio_phy_hdr_op_rd_incr : std_logic_vector(c_mdio_phy_hdr_op_rd_incr_len - 1 downto 0) := "10";  -- operation code read increment
  constant c_mdio_phy_hdr_prtad      : std_logic_vector(c_mdio_phy_hdr_prtad_len      - 1 downto 0) := "00000";  -- port address (example)
  constant c_mdio_phy_hdr_devad      : std_logic_vector(c_mdio_phy_hdr_devad_len      - 1 downto 0) := "00001";  -- device address (example)
  constant c_mdio_phy_hdr_ta         : std_logic_vector(c_mdio_phy_hdr_ta_len         - 1 downto 0) := "10";  -- turn around

  constant c_mdio_phy_mdc_period     : natural := 256;  -- must be a power of 2
  constant c_mdio_phy_hold_time      : natural := 10;
  constant c_mdio_phy_setup_time     : natural := 2;

  -- MDIO cmd type to perform higher level MDIO commands: write and read. These commands are interpreted and executed by the FSM in mdio_ctlr.vhd,
  -- which takes care of the lower level sub-accesses (write header, wwrite command, read done, acknowledge done, etc).
  type t_mdio_cmd is record
    wr_not_rd : std_logic;
    devadr    : std_logic_vector(c_mdio_phy_hdr_devad_len - 1 downto 0);
    devreg    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0);
    wrdata    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0);
  end record;

  -- Array of MDIO commands for auto execution by mdio_ctlr FSM.
  type t_mdio_cmd_arr is array (integer range <>) of t_mdio_cmd;

  type t_c_mdio_phy is record
    mdc_period : natural;  -- clk period * 256 /2 = mdc period, MDIO slave acts on rising edge
    hold_time  : natural;  -- clk period * 10     = write hold time, must be > 10 ns, choose some larger margin
    setup_time : natural;  -- read mdat_in is stable within 300 ns of rising mdc edge, therefore
                           -- choose g_setup_time to fit (mdc period/2) + g_setup_time > 300 ns.
                           -- typically choose mdc period > 2*300 ns, then g_setup_time can be 0.
  end record;

  constant c_mdio_phy : t_c_mdio_phy := (c_mdio_phy_mdc_period, c_mdio_phy_hold_time, c_mdio_phy_setup_time);

  function mdio_hdr_adr(prtad: std_logic_vector(c_mdio_phy_hdr_prtad_len - 1 downto 0); devad: std_logic_vector(c_mdio_phy_hdr_devad_len - 1 downto 0) ) return std_logic_vector;
  function mdio_hdr_wr (prtad: std_logic_vector(c_mdio_phy_hdr_prtad_len - 1 downto 0); devad: std_logic_vector(c_mdio_phy_hdr_devad_len - 1 downto 0) ) return std_logic_vector;
  function mdio_hdr_rd (prtad: std_logic_vector(c_mdio_phy_hdr_prtad_len - 1 downto 0); devad: std_logic_vector(c_mdio_phy_hdr_devad_len - 1 downto 0) ) return std_logic_vector;
end mdio_pkg;

package body mdio_pkg is
  function mdio_hdr_adr(prtad: std_logic_vector(c_mdio_phy_hdr_prtad_len - 1 downto 0); devad: std_logic_vector(c_mdio_phy_hdr_devad_len - 1 downto 0) ) return std_logic_vector is
  -- Return a full address header based on port address and device address.
  begin
    return c_mdio_phy_hdr_st & c_mdio_phy_hdr_op_addr & prtad & devad & c_mdio_phy_hdr_ta;
  end;

  function mdio_hdr_wr(prtad: std_logic_vector(c_mdio_phy_hdr_prtad_len - 1 downto 0); devad: std_logic_vector(c_mdio_phy_hdr_devad_len - 1 downto 0) ) return std_logic_vector is
  -- Return a full write header based on port address and device address.
  begin
    return c_mdio_phy_hdr_st & c_mdio_phy_hdr_op_wr & prtad & devad & c_mdio_phy_hdr_ta;
  end;

  function mdio_hdr_rd(prtad: std_logic_vector(c_mdio_phy_hdr_prtad_len - 1 downto 0); devad: std_logic_vector(c_mdio_phy_hdr_devad_len - 1 downto 0) ) return std_logic_vector is
  -- Return a full read header based on port address and device address.
  begin
    return c_mdio_phy_hdr_st & c_mdio_phy_hdr_op_rd & prtad & devad & c_mdio_phy_hdr_ta;
  end;
end mdio_pkg;
