###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# This QSF is sourced by other design QSF files.
# ==============================================
# Note: This file can ONLY BE SOURCED (use SOURCE_TCL_SCRIPT_FILE so it will be TCL interpreted), e.g.
# by another QSF, otherwise many TCL commands such as "$::env(HDL_WORK)" do not work.

# new in Quartus 16.0:
set_global_assignment -name NUM_PARALLEL_PROCESSORS 6


# Device:
set_global_assignment -name FAMILY "Arria 10"
set_global_assignment -name DEVICE 10AX115U4F45E3SGE3
set_global_assignment -name STRATIX_DEVICE_IO_STANDARD "1.8 V"
set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
#set_global_assignment -name MIN_CORE_JUNCTION_TEMP "-40"
set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
#set_global_assignment -name DEVICE_FILTER_SPEED_GRADE FASTEST
set_global_assignment -name DEVICE_FILTER_SPEED_GRADE ANY
#set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 256
set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 4
set_global_assignment -name ENABLE_OCT_DONE OFF
set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
set_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top
set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT -section_id Top
set_global_assignment -name PARTITION_COLOR 16764057 -section_id Top
set_global_assignment -name ENABLE_CONFIGURATION_PINS OFF
set_global_assignment -name ENABLE_NCE_PIN OFF
set_global_assignment -name ENABLE_BOOT_SEL_PIN OFF
set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "ACTIVE SERIAL X4"
#set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "ACTIVE SERIAL X1"
set_global_assignment -name STRATIXII_CONFIGURATION_DEVICE EPCQL1024
set_global_assignment -name USE_CONFIGURATION_DEVICE ON
set_global_assignment -name STRATIXIII_UPDATE_MODE REMOTE

set_global_assignment -name CRC_ERROR_OPEN_DRAIN ON
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -rise
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -fall
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -rise
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -fall

set_global_assignment -name CONFIGURATION_VCCIO_LEVEL 1.8V
set_global_assignment -name ON_CHIP_BITSTREAM_DECOMPRESSION ON
#set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_12_5MHZ
set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_25MHZ
#set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_100MHZ

set_global_assignment -name USER_START_UP_CLOCK OFF

set_global_assignment -name DEVICE_FILTER_PACKAGE FBGA
set_global_assignment -name DEVICE_FILTER_PIN_COUNT 1932

# OFF is default; specify ON if needed:
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[12]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[13]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[14]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[15]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[16]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[17]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[18]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[19]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[20]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[21]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[22]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[23]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[24]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[25]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[26]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[27]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[28]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[29]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[30]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[31]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[32]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[33]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[34]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[35]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[36]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[37]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[38]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[39]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[40]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[41]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[42]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[43]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[44]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[45]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[46]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_RX[47]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[12]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[13]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[14]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[15]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[16]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[17]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[18]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[19]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[20]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[21]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[22]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[23]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[24]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[25]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[26]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[27]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[28]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[29]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[30]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[31]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[32]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[33]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[34]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[35]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[36]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[37]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[38]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[39]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[40]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[41]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[42]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[43]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[44]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[45]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[46]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to BCK_TX[47]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[3]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_RX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_0_TX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_RX[11]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[4]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[5]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[6]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[7]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[8]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[9]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[10]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to RING_1_TX[11]




set_global_assignment -name SOURCE_FILE quartus.ini

#set_global_assignment -name EDA_BOARD_DESIGN_SIGNAL_INTEGRITY_TOOL "IBIS (Signal Integrity)"
#set_global_assignment -name EDA_OUTPUT_DATA_FORMAT IBIS -section_id eda_board_design_signal_integrity
#set_global_assignment -name EDA_IBIS_SPECIFICATION_VERSION 5P0 -section_id eda_board_design_signal_integrity
set_instance_assignment -name PARTITION_HIERARCHY root_partition -to | -section_id Top

set_global_assignment -name LAST_QUARTUS_VERSION 16.0.1

# Optimize for performance:
set_global_assignment -name OPTIMIZATION_TECHNIQUE SPEED
set_global_assignment -name SYNTH_TIMING_DRIVEN_SYNTHESIS ON
set_global_assignment -name OPTIMIZE_HOLD_TIMING "ALL PATHS"
set_global_assignment -name OPTIMIZE_MULTI_CORNER_TIMING ON
set_global_assignment -name FITTER_EFFORT "STANDARD FIT"
set_global_assignment -name ROUTER_CLOCKING_TOPOLOGY_ANALYSIS ON
#set_global_assignment -name PHYSICAL_SYNTHESIS_REGISTER_RETIMING ON
set_global_assignment -name ROUTER_TIMING_OPTIMIZATION_LEVEL MAXIMUM

# To set a location assignment for a PLL, do the following:
# - after compilation, open the chip planner
# - hover over the ATX PLL block (left side or right side)
# - Right click and click "Copy tooltip"
# - Paste text in here and edit
#set_location_assignment HSSIPMALCPLL_X0_Y33_N29 -to "unb2_test:u_revision|unb2_board_10gbe:\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\gen_ip_arria10:u0|ip_arria10_transceiver_pll_10g:\gen_phy_24:u_ip_arria10_transceiver_pll_10g_0|altera_xcvr_atx_pll_a10:xcvr_atx_pll_a10_0|a10_xcvr_atx_pll:a10_xcvr_atx_pll_inst|twentynm_atx_pll_inst"



#set_parameter -name dbg_user_identifier 1 -to "\\Generate_XCVR_LANE_INSTANCES:1:xcvr_lane_inst|xcvr_txrx_inst|xcvr_native_a10_0"
#set_parameter -name dbg_user_identifier 0 -to "\\Generate_XCVR_LANE_INSTANCES:0:xcvr_lane_inst|xcvr_pll_inst|xcvr_atx_pll_a10_0"
#set_parameter -name dbg_user_identifier 1 -to "\\Generate_XCVR_LANE_INSTANCES:1:xcvr_lane_inst|xcvr_pll_inst|xcvr_atx_pll_a10_0"

set_parameter -name dbg_user_identifier 1 -to "unb2_test:u_revision|unb2_board_10gbe:\\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE_0|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\\gen_ip_arria10:u0|ip_arria10_phy_10gbase_r_12:\\gen_phy_12:u_ip_arria10_phy_10gbase_r_12"
set_parameter -name dbg_user_identifier 1 -to "unb2_test:u_revision|unb2_board_10gbe:\\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE_1|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\\gen_ip_arria10:u0|ip_arria10_phy_10gbase_r_12:\\gen_phy_12:u_ip_arria10_phy_10gbase_r_12"

set_parameter -name dbg_user_identifier 1 -to "unb2_test:u_revision|unb2_board_10gbe:\\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE_0|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\\gen_ip_arria10:u0|ip_arria10_transceiver_pll_10g:u_ip_arria10_transceiver_pll_10g|altera_xcvr_atx_pll_a10:xcvr_atx_pll_a10_0"
set_parameter -name dbg_user_identifier 1 -to "unb2_test:u_revision|unb2_board_10gbe:\\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|tr_10GbE:u_tr_10GbE_1|tech_eth_10g:u_tech_eth_10g|tech_eth_10g_arria10:\\gen_ip_arria10:u0|tech_10gbase_r:u_tech_10gbase_r|tech_10gbase_r_arria10:\\gen_ip_arria10:u0|ip_arria10_transceiver_pll_10g:u_ip_arria10_transceiver_pll_10g|altera_xcvr_atx_pll_a10:xcvr_atx_pll_a10_0"


# Pass compile stamps as generics (passed to top-level when $UNB_COMPILE_STAMPS is set)
if { [info exists ::env(UNB_COMPILE_STAMPS) ] } {
  set_parameter -name g_stamp_date [clock format [clock seconds] -format {%Y%m%d}]
  set_parameter -name g_stamp_time [clock format [clock seconds] -format {%H%M%S}]
  post_message -type info "RADIOHDL: using SVN $::env(HDL_GIT_REVISION)"
  set_parameter -name g_stamp_svn [regsub -all {[^0-9]} [exec echo $::env(HDL_GIT_REVISION)] ""] 
}

#set_instance_assignment -name FAST_OUTPUT_REGISTER ON -to "ctrl_unb2_board:u_ctrl|eth:\\gen_eth:u_eth|tech_tse:u_tech_tse|tech_tse_arria10_e3sge3:\\gen_ip_arria10_e3sge3:u0|ip_arria10_e3sge3_tse_sgmii_lvds:\\u_LVDS_tse:u_tse|ip_arria10_e3sge3_tse_sgmii_lvds_altera_eth_tse_151_6kz2wlq:eth_tse_0|altera_eth_tse_pcs_pma_nf_lvds:i_tse_pcs_0|tbi_tx_d"
#set_instance_assignment -name FAST_INPUT_REGISTER ON -to "ctrl_unb2_board:u_ctrl|eth:\\gen_eth:u_eth|tech_tse:u_tech_tse|tech_tse_arria10_e3sge3:\\gen_ip_arria10_e3sge3:u0|ip_arria10_e3sge3_tse_sgmii_lvds:\\u_LVDS_tse:u_tse|ip_arria10_e3sge3_tse_sgmii_lvds_altera_eth_tse_151_6kz2wlq:eth_tse_0|altera_eth_tse_pcs_pma_nf_lvds:i_tse_pcs_0|tbi_rx_d"
#set_instance_assignment -name GLOBAL_SIGNAL "REGIONAL CLOCK" -to "ctrl_unb2_board:u_ctrl|eth:\\gen_eth:u_eth|tech_tse:u_tech_tse|tech_tse_arria10_e3sge3:\\gen_ip_arria10_e3sge3:u0|ip_arria10_e3sge3_tse_sgmii_lvds:\\u_LVDS_tse:u_tse|ip_arria10_e3sge3_tse_sgmii_lvds_altera_eth_tse_151_6kz2wlq:eth_tse_0|altera_eth_tse_pcs_pma_nf_lvds:i_tse_pcs_0|tx_clk"
#set_instance_assignment -name GLOBAL_SIGNAL "REGIONAL CLOCK" -to "ctrl_unb2_board:u_ctrl|eth:\\gen_eth:u_eth|tech_tse:u_tech_tse|tech_tse_arria10_e3sge3:\\gen_ip_arria10_e3sge3:u0|ip_arria10_e3sge3_tse_sgmii_lvds:\\u_LVDS_tse:u_tse|ip_arria10_e3sge3_tse_sgmii_lvds_altera_eth_tse_151_6kz2wlq:eth_tse_0|altera_eth_tse_mac:i_tse_mac|rx_clk"
#set_instance_assignment -name GLOBAL_SIGNAL "REGIONAL CLOCK" -to "ctrl_unb2_board:u_ctrl|eth:\\gen_eth:u_eth|tech_tse:u_tech_tse|tech_tse_arria10_e3sge3:\\gen_ip_arria10_e3sge3:u0|ip_arria10_e3sge3_tse_sgmii_lvds:\\u_LVDS_tse:u_tse|ip_arria10_e3sge3_tse_sgmii_lvds_altera_eth_tse_151_6kz2wlq:eth_tse_0|altera_eth_tse_pcs_pma_nf_lvds:i_tse_pcs_0|rx_clk"
#set_instance_assignment -name GLOBAL_SIGNAL "REGIONAL CLOCK" -to "ctrl_unb2_board:u_ctrl|eth:\\gen_eth:u_eth|tech_tse:u_tech_tse|tech_tse_arria10_e3sge3:\\gen_ip_arria10_e3sge3:u0|ip_arria10_e3sge3_tse_sgmii_lvds:\\u_LVDS_tse:u_tse|ip_arria10_e3sge3_tse_sgmii_lvds_altera_eth_tse_151_6kz2wlq:eth_tse_0|altera_eth_tse_mac:i_tse_mac|tx_clk"
