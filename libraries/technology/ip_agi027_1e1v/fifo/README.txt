README.txt for $HDL_WORK/libraries/technology/ip_agi027_1e1v/fifo

Contents:

1) FIFO components
2) Agilex7 IP
3) Implementation options (LUTs or block RAM)
4) Synthesis trials
5) Issues


1) FIFO components:

  ip_agi027_1e1v_fifo_sc              = Single clock FIFO
  ip_agi027_1e1v_fifo_dc              = Dual clock FIFO
  ip_agi027_1e1v_fifo_dc_mixed_widths = Dual clock FIFO with different read and write data widths (ratio power of 2)
  

2) Agilex7 IP

  The IP was ported from arria10_e2sg by:
  
   - methode A:
    . copy original ip_arria_e2sg_<fifo_name>.vhd and ip_arria_e2sg_<fifo_name>.ip files.
    . rename ip_arria_e2sg_<fifo_name>.ip and .vhd into ip_agi027_1e1v_<fifo_name>.ip and .vhd (also replace name inside the .vhd file)
    . open in to Quartus 23.2.0 build 94, set device family to Agilex7 and device part to AGIB027R31A1I1VB. 
      Finish automatically convert to "new" IP, note differences such as version.
    . then generate HDL (select VHDL for both sim and synth) using the Quartus tool or generate HDL in the build directory using the 
      terminal command generate_ip_libs <buildset> and finish to save the changes.
    . compare the generated files to the copied .vhd file for version, using the same library, generics, and ports. Make adjustments if 
      necessary to make it work.
    . git commit also the ip_agi027_1e1v_<fifo_name>.ip to preserve the original in case it needs to be modified.

   - methode B:
    . copy original ip_arria_e2sg_<fifo_name>.vhd file.
    . rename ip_arria_e2sg_<fifo_name>.vhd into ip_agi027_1e1v_<fifo_name>.vhd (also replace name inside the .vhd file).
    . open ip_arria_e2sg_<fifo_name>.ip file in Quartus 19.4.0 build 64. No device family and device part need to be set.
    . open also Quartus 23.2.0 build 94, set device family to Agilex7 and device part to AGIB027R31A1I1VB.
    . select the corresponding IP in the IP catalog in Quartus 23.2.0 and provide the filename as ip_agi027_1e1v_<fifo_name>.ip
      Finish automatically convert to IP, note differences such as version.
    . save the changes and then generate HDL (select VHDL for both sim and synth) using the Quartus tool or generate HDL in the build 
      directory using the terminal command generate_ip_libs <buildset> to finish it.
    . compare the generated files to the copied .vhd file for version, using the same library, generics, and ports. Make adjustments if 
      necessary to make it work.
    . git commit also the ip_agi027_1e1v_<fifo_name>.ip to preserve the original in case it needs to be modified.


  this yields:
  
    ip_agi027_1e1v_fifo_sc.ip
    ip_agi027_1e1v_fifo_dc.ip
    ip_agi027_1e1v_fifo_dc_mixed_widths.ip
  
  The Agilex7 FIFO IP still uses the altera_mf package (so not the altera_lnsim package as with the block RAM). The
  FIFOs map to the altera_mf components to scfifo, dcfifo and dcfifo_mixed_widths.
  
  The IP only needs to be generated with generate_ip_libs <buildset> if it need to be modified, because the ip_agi027_1e1v_fifo_*.vhd
  directly instantiates the altera_mf component. The buildset for the agi027_1e1v is iwave.
  
  The instantiation is copied manually from the ip_agi027_1e1v_fifo_*/fifo_1921/sim/ip_agi027_1e1vg_fifo_*.vhd and 
  saved in the ip_agi027_1e1v_<fifo_name>.vhd file. So then the generated HDL files are no longer needed, because it could easily be derived 
  from the IP file and the files will be generated in the build directory (under iwave/qsys-generate/) when using the terminal commando 
  generate_ip_libs <buildset>. 
   

3) Implementation options (LUTs or block RAM)

  The IP FIFO can be set to use LUTs (MLAB) or block RAM (M20K and M144K) via g_use_eab.
  

4) Synthesis trials

  The quartus/fifo.qsf could be derived from the ip_arria10/fifo/ folder and changed to only the following assignments:
    set_global_assignment -name FAMILY "Agilex 7"
    set_global_assignment -name DEVICE AGIB027R31A1I1VB
    set_global_assignment -name LAST_QUARTUS_VERSION "23.2.0 Pro Edition"
    set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 256
    set_global_assignment -name MIN_CORE_JUNCTION_TEMP "-40"
    set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
    set_global_assignment -name PWRMGT_VOLTAGE_OUTPUT_FORMAT "LINEAR FORMAT"
    set_global_assignment -name PWRMGT_LINEAR_FORMAT_N "-12"
    set_global_assignment -name POWER_APPLY_THERMAL_MARGIN ADDITIONAL
  quartus_qsf_files = $HDL_WORK/libraries/technology/ip_agi027_1e1v/fifo/quartus/fifo.qsf could be added to the hdllib.cfg under
  [quartus_project_file]. Use the terminal command quartus_config <buildset> to create/update all the projectfiles for iwave.
  The Quartus project ip_agi027_1e1v_fifo.qpf from $HDL_BUILD_DIR/iwave/quartus/ip_agi027_1e1v_fifo/ was used to verify that the FIFO IP 
  actually synthesise to the appropriate FPGA resources. Use the Quartus GUI to manually select a top level component for synthesis e.g. 
  by right clicking the entity vhd file in the file tab of the Quartus project navigator window. For the (default) testcondition the 
  generics are set to 1024 words deep and 20 bits wide. Then check the resource usage in the synthesis and fitter reports.
  The most important information from these reports is (found under Place Stage > Resource Usage Summary / Resource Utilazation by Entity):
      . for g_nof_words is 1024 and for g_wrdat_w is 20, exactly fills one M20k block ram. 20 * 1024 = 20480 block memory bits.
      . the total M20K blocks is 13272. Thus the total block memory bits that is available 13272 * 20480 = 271810560 when optimal use.
      . no dsp blocks are used.
      . the ALM needed [=A-B+C] for dc_mixed_widths is 119.
      . the ALM needed [=A-B+C] for dc is 113.
      . the ALM needed [=A-B+C] for sc is 30.
      . the total ALMs on device is 912800.


5) Issues

  No issues.
