--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------
-- Author: Eric Kooistra
-- Purpose:
--   Select shift in or load in version of dp_repack_data_in.
-- Description:
--   Use g_use_in_load = true when snk_in.data valid has no gaps.
--
library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity dp_repack_data_in is
  generic (
    g_use_in_load  : boolean := false;
    g_bypass       : boolean := false;
    g_in_dat_w     : natural;
    g_in_nof_words : natural;
    g_in_symbol_w  : natural := 1  -- default 1 for snk_in.empty in nof bits, else use power of 2
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    snk_out          : out t_dp_siso;
    snk_in           : in  t_dp_sosi;

    src_in           : in  t_dp_siso;
    src_out          : out t_dp_sosi;
    src_out_data     : out std_logic_vector(g_in_dat_w * g_in_nof_words - 1 downto 0)
  );
end dp_repack_data_in;

architecture str of dp_repack_data_in is
begin
  gen_shift : if not g_use_in_load generate
    u_shift : entity work.dp_repack_data_in_shift
    generic map (
      g_bypass       => g_bypass,
      g_in_dat_w     => g_in_dat_w,
      g_in_nof_words => g_in_nof_words,
      g_in_symbol_w  => g_in_symbol_w
    )
    port map (
      rst          => rst,
      clk          => clk,
      snk_out      => snk_out,
      snk_in       => snk_in,
      src_in       => src_in,
      src_out      => src_out,
      src_out_data => src_out_data
    );
  end generate;

  gen_load : if g_use_in_load generate
    u_load : entity work.dp_repack_data_in_load
    generic map (
      g_bypass       => g_bypass,
      g_in_dat_w     => g_in_dat_w,
      g_in_nof_words => g_in_nof_words,
      g_in_symbol_w  => g_in_symbol_w
    )
    port map (
      rst          => rst,
      clk          => clk,
      snk_out      => snk_out,
      snk_in       => snk_in,
      src_in       => src_in,
      src_out      => src_out,
      src_out_data => src_out_data
    );
  end generate;
end str;
