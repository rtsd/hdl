README

The twiddlePkg.vhd can be generated with dsp/rTwoSDF/src/matlab/twiddles.m
Several twiddlePkg_w*.vhd have been pre generated in dsp/rTwoSDF/src/vhdl/pkg/
Default dsp/rTwoSDF/src/vhdl/twiddlesPkg.vhd = dsp/rTwoSDF/src/vhdl/pkg/twiddlesPkg_w18.vhd
To try another twiddle width do: cp pkg/twiddlesPkg_w18.vhd twiddlesPkg.vhd
