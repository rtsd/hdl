#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2016
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Create mif files for the coeffcient RAM memories per tap and for the wide
   band factor range. The coeffients in the input dat file must be in normal
   impulse response order and the nof taps and nof points must match. The
   output mif files contain the FIR coefficients in flipped order per tap.
   The FIR coefficients order is first flipped per tap and after that they 
   are output into the mif files. The output mif files are numbered in
   range(wb_factor*nof_taps), first all tap files for wb index 0, then all
   tap files for wb index 1 etc.
   
   The FIR coefficients for wb factor > 1 are created in big endian order, so
   pfir.wb_big_endian = True, because internally fil_ppf_wide.vhd uses fixed
   big endian time [0,1,2,3] to wideband P [3,2,1,0] index mapping for the
   input data. This makes that the same set of mif files can be used for
   fil_ppf_wide.vhd, independent of g_big_endian_wb_in in fil_ppf_wide.vhd.
                    
   The output MIF files use the input dat filename as prefix and append to
   that the wb_factor information and the MIF file index. 
   
   The coefficient values are stored in the MIF. The width of the data is  
   specified by pfir.coef_w. The pfir.coef_w is obtained via the command 
   line argument and is only used to identify the input dat file.
   
   A pfir_coeff_*.dat file can be created using Matlab:
   > $HDL_WORK/applications/apertif/matlab/run_pfir_coef.m
   
   The result is then (dependend on the actual settings in run_pfir_coef.m):
   > $HDL_WORK/applications/apertif/matlab/data/pfir_coeff_incrementing_8taps_64points_16b.dat
   
   This coefficients dat file needs to be copied to the local ../hex directory,
   because both the dat and the MIF files sare used in the VHDL testbenches.
   
   Usage:
   > python fil_ppf_create_mifs.py -h
   > python fil_ppf_create_mifs.py -f ../hex/pfir_coeff_incrementing_8taps_64points_16b.dat -t 8 -p 64 -w 1 -c 16
   
   The output MIF files will then be:
   ../hex/pfir_coeff_incrementing_8taps_64points_16b_1wb_0.mif
   ../hex/pfir_coeff_incrementing_8taps_64points_16b_1wb_1.mif
   ../hex/pfir_coeff_incrementing_8taps_64points_16b_1wb_2.mif
   ../hex/pfir_coeff_incrementing_8taps_64points_16b_1wb_3.mif
   ../hex/pfir_coeff_incrementing_8taps_64points_16b_1wb_4.mif
   ../hex/pfir_coeff_incrementing_8taps_64points_16b_1wb_5.mif
   ../hex/pfir_coeff_incrementing_8taps_64points_16b_1wb_6.mif
   ../hex/pfir_coeff_incrementing_8taps_64points_16b_1wb_7.mif 

   The script is defined as a single run() function that can be ran standalone
   as main (typical usage) or be imported and called by another script.
"""

import sys
import getopt
import os.path
from tools import *
from common import *

def run(argv):   
    # Arguments
    class pfir:
        filepathname = ''
        filename = ''
        nof_taps = 0
        nof_points = 0
        wb_factor = 0
        coef_w = 0
        wb_big_endian = False   # fixed use False to create mif files for fil_ppf_wide.vhd and for apertif_unb1_bn_filterbank
    
    usage_str = 'USAGE: fil_ppf_create_mifs.py -f <path and file name> -t <nof taps> -p <nof points> -w <wb factor> -c <coef width>\n'
    try:
        opts, _ = getopt.getopt(sys.argv[1:], 'hf:t:p:w:c:')
    except getopt.GetoptError:
        print usage_str
        sys.exit(2)
        
    #print sys.argv
    for opt, arg in opts:
        if opt == '-h':
           print usage_str
           sys.exit()
        elif opt == '-f':
           pfir.filepathname = arg
           pfir.filename = os.path.basename(arg)
           pfir.name = os.path.splitext(pfir.filename)[0]
        elif opt == '-t':
           pfir.nof_taps = int(arg)
        elif opt == '-p':
           pfir.nof_points = int(arg)
        elif opt == '-w':
           pfir.wb_factor = int(arg)
        elif opt == '-c':
           pfir.coef_w = int(arg)
    if pfir.filepathname == '' or pfir.nof_taps == 0 or pfir.nof_points == 0 or pfir.wb_factor == 0 or pfir.coef_w == 0:
        print 'Missing arguments!'
        print usage_str
        sys.exit()
    
    c_nof_mif_files = pfir.wb_factor * pfir.nof_taps;
    c_mif_nof_points = pfir.nof_points/pfir.wb_factor;
    print 'Creating wb_factor * nof_taps (= %d*%d) = %d MIF-files for PFIR with coefficients from input file %s.' % (pfir.wb_factor, pfir.nof_taps, c_nof_mif_files, pfir.filepathname)
    print '. With %d points per tap' % pfir.nof_points
    print '. With %d points per MIF-file' % c_mif_nof_points
    print '. With %d bit coefficient in RAM' % (pfir.coef_w)
    print '\n'
    
    # Read coefficients from PFIR file
    c_nof_coefs = pfir.nof_taps * pfir.nof_points;
    pfir_coefs =[]
    with open(pfir.filepathname, 'r') as fp:
        for line in fp:
            if line.strip()!='':                  # skip empty line
                s = int(line)                     # one coef per line
                s = s & (2**pfir.coef_w-1)
                pfir_coefs.append(s)
    #print pfir_coefs
    
    # Flip the order of the coefficients per tap
    pfir_coefs_flip = []
    for j in range(pfir.nof_taps):
        for i in range(pfir.nof_points):
            pfir_coefs_flip.append(pfir_coefs[j*pfir.nof_points + pfir.nof_points-1-i])
    #print pfir_coefs_flip
    
    # Use PFIR input filename as MIF file prefix, this filename contains already nof_taps, nof_points and coef_w
    c_mif_prefix = '../hex/%s_' % pfir.name
    
    for k in range(pfir.wb_factor):
        if pfir.wb_big_endian == True:
            kk = pfir.wb_factor-1-k  # reverse, to fit big endian time to wideband input data mapping t[0,1,2,3] = P[3,2,1,0]
        else:
            kk = k                   # keep, to fit little endian time to wideband input data mapping t[3,2,1,0] = P[3,2,1,0]
            
        for j in range(pfir.nof_taps):
            mifpathname = c_mif_prefix + '%dwb_%d.mif' % (pfir.wb_factor, k*pfir.nof_taps+j)  # append MIF index in range(c_nof_mif_files)
            with open(mifpathname, 'w') as fp:
                s = 'WIDTH=%d;\n' % pfir.coef_w
                fp.write(s)
                s = 'DEPTH=%d;\n' % c_mif_nof_points
                fp.write(s)
                s = 'ADDRESS_RADIX=HEX;\n'
                fp.write(s)
                s = 'DATA_RADIX=HEX;\n'
                fp.write(s)
                s = 'CONTENT BEGIN\n'
                fp.write(s)
    
                for i in range(c_mif_nof_points):
                    s = ' %x   :  %x ; \n' % (i, pfir_coefs_flip[j*pfir.nof_points+i*pfir.wb_factor+kk])  # use kk
                    fp.write(s)
    
                s = 'END;\n'
                fp.write(s)

if __name__ == "__main__":  
   run(sys.argv[1:])