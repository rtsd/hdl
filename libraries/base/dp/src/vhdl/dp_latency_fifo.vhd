-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose: Simple DP fifo in registers.
-- Description:
--   The fifo can store up to g_fifo_size words. When the FIFO is full then
--   the snk_out.ready is '0', so wr_ful is equivalent and equal to NOT
--   snk_out.ready.
--
-- Remark:
-- . Uses a dp_latency_adapter as FIFO.
-- . Choose g_fifo_size > g_output_rl - g_input_rl to ensure that
--   c_adapter_input_rl > c_adapter_output_rl, because only then the FIFO
--   logic will be instantiated inside the dp_latency_adapter

entity dp_latency_fifo is
  generic (
    g_bypass       : boolean := false;  -- for clarity rather use g_bypass=TRUE than equivalent g_fifo_size=0
    g_input_rl     : natural := 1;  -- input ready latency
    g_output_rl    : natural := 1;  -- output ready latency
    g_fifo_size    : natural := 1  -- must be g_fifo_size > g_output_rl - g_input_rl
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- Monitor FIFO filling
    usedw        : out std_logic_vector(ceil_log2(2 + g_input_rl + g_fifo_size) - 1 downto 0);  -- +3 to match dp_latency_adapter usedw width
    wr_ful       : out std_logic;
    rd_emp       : out std_logic;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso;
    src_out      : out t_dp_sosi
  );
end dp_latency_fifo;

architecture rtl of dp_latency_fifo is
  constant c_adapter_input_rl  : natural := g_input_rl + g_fifo_size;
  constant c_adapter_output_rl : natural := g_output_rl;

  signal i_snk_out    : t_dp_siso;
  signal fifo_snk_out : t_dp_siso;
  signal i_usedw      : std_logic_vector(usedw'range);
begin
  gen_bypass : if g_bypass = true generate
    snk_out <= src_in;
    src_out <= snk_in;
  end generate;

  no_bypass : if g_bypass = false generate
    snk_out <= i_snk_out;
    usedw   <= i_usedw;

    -- pass on frame level flow control
    i_snk_out.xon <= src_in.xon;

    -- pass on sample level flow control similar as in dp_latency_adapter but now with extra margin of g_fifo_size
    p_snk_out_ready : process(i_usedw, fifo_snk_out, snk_in)  -- equivalent to p_snk_out_ready in dp_latency_adapter
    begin
      i_snk_out.ready <= '0';
      if fifo_snk_out.ready = '1' then  -- equivalent to ff_siso.ready in dp_latency_adapter
        -- Default snk_out ready when the source is ready.
        i_snk_out.ready <= '1';
      else
        -- Extra snk_out ready to look ahead for fifo_snk_out RL = 0 and to fill the FIFO.
        if TO_UINT(i_usedw) < c_adapter_input_rl then  -- equivalent to fifo_reg(0).valid='0' in dp_latency_adapter
          i_snk_out.ready <= '1';
        elsif TO_UINT(i_usedw) = c_adapter_input_rl then  -- equivalent to fifo_reg(1).valid='0' in dp_latency_adapter
          i_snk_out.ready <= not(snk_in.valid);
        end if;
      end if;
    end process;

    -- define FIFO full as not ready for new input
    wr_ful <= not i_snk_out.ready;

    u_dp_latency_adapter : entity work.dp_latency_adapter
    generic map (
      g_in_latency  => c_adapter_input_rl,
      g_out_latency => c_adapter_output_rl
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- Monitor internal FIFO filling
      fifo_usedw   => i_usedw,
      fifo_ful     => OPEN,
      fifo_emp     => rd_emp,
      -- ST sink
      snk_out      => fifo_snk_out,
      snk_in       => snk_in,
      -- ST source
      src_in       => src_in,
      src_out      => src_out
    );
  end generate;
end rtl;
