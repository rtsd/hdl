-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Reuse from LOFAR rad_frame_status.vhd and rad_frame_status(rtl).vhd

-- Purpose:
--   This function the status of the frame processing in RAD. It counts the number of received frames and
--   reports whether there occured brc errors and whether frames were discarded due to misalignment. The
--   status is reset for every sync interval.
-- Description:
--   The status output is aggregate input discarded & frame sync & aggregate brc & count nof rx frames. The
--   status applies to the interval before the previous sync interval, due to the data processing latency.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity dp_frame_status is
  generic (
    g_dat_w    : positive := 32;
    g_hdr_w    : positive := 16;
    g_cnt_w    : positive := 18;
    g_status_w : positive := 32
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    sync       : in  std_logic;

    in_dat     : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val     : in  std_logic;
    in_sof     : in  std_logic;
    in_eof     : in  std_logic;
    in_dis     : in  std_logic;

    status     : out std_logic_vector(g_status_w - 1 downto 0)
  );
end dp_frame_status;

architecture rtl of dp_frame_status is
  signal i_status      : std_logic_vector(status'range);
  signal debug_status  : std_logic_vector(status'range);
  signal debug_dis     : std_logic;
  signal debug_fsync   : std_logic;
  signal debug_brc     : std_logic;
  signal debug_cnt     : std_logic_vector(g_cnt_w - 1 downto 0);

  signal sync_dly      : std_logic;  -- fdelay sync one cycle to compensate for payevent delay in ei_evt.

  signal fsync         : std_logic;  -- frame sync from in_dat header at in_sof
  signal fsync_det     : std_logic;
  signal fsync_hld     : std_logic;
  signal nxt_fsync_hld : std_logic;

  signal brc           : std_logic;  -- frame brc from in_dat brc at in_eof
  signal brc_det       : std_logic;  -- aggregate brc
  signal brc_hld       : std_logic;
  signal nxt_brc_hld   : std_logic;

  signal dis_det       : std_logic;  -- frame got discarded due to mis alignment
  signal dis_hld       : std_logic;  -- aggregate discarded
  signal nxt_dis_hld   : std_logic;

  signal cnt           : std_logic_vector(g_cnt_w - 1 downto 0);  -- frame count
  signal nxt_cnt       : std_logic_vector(cnt'range);
  signal cnt_hld       : std_logic_vector(cnt'range);
  signal nxt_cnt_hld   : std_logic_vector(cnt'range);
begin
  status   <= i_status;
  i_status <= hton(RESIZE_UVEC(dis_hld & fsync_hld & brc_hld & cnt_hld, g_status_w), c_word_sz);
  -- synthesis translate_off
  -- Signals for simulation wave window
  debug_status <= hton(i_status, c_word_sz);
  debug_dis    <= debug_status(g_cnt_w + 2);
  debug_fsync  <= debug_status(g_cnt_w + 1);
  debug_brc    <= debug_status(g_cnt_w);
  debug_cnt    <= debug_status(g_cnt_w - 1 downto 0);
  -- synthesis translate_on

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      sync_dly  <= '0';
      fsync_hld <= '0';
      brc_hld   <= '0';
      dis_hld   <= '0';
      cnt       <= (others => '0');
      cnt_hld   <= (others => '0');
    elsif rising_edge(clk) then
      sync_dly  <= sync;
      fsync_hld <= nxt_fsync_hld;
      brc_hld   <= nxt_brc_hld;
      dis_hld   <= nxt_dis_hld;
      cnt       <= nxt_cnt;
      cnt_hld   <= nxt_cnt_hld;
    end if;
  end process;

  fsync <= '1' when in_dat(g_hdr_w - 1) = '1' and in_sof = '1' else '0';
  brc   <= '1' when in_dat(0)        = '1' and in_eof = '1' else '0';

  -- use sync_dly also for fsync_hld to let all status fields apply to the same sync interval
  nxt_fsync_hld <= fsync_det                                     when sync_dly = '1' else fsync_hld;
  nxt_brc_hld   <= brc_det   when fsync = '1' else            '0'  when sync_dly = '1' else brc_hld;
  nxt_dis_hld   <= dis_det   when fsync = '1' else            '0'  when sync_dly = '1' else dis_hld;
  nxt_cnt_hld   <= cnt       when fsync = '1' else (others => '0') when sync_dly = '1' else cnt_hld;

  -- frame sync detection
  u_fsync_det : entity common_lib.common_switch
  port map (
    clk         => clk,
    rst         => rst,
    switch_high => fsync,  -- frame sync
    switch_low  => sync_dly,  -- system sync
    out_level   => fsync_det
  );

  -- any frame brc over fsync interval
  u_brc_det : entity common_lib.common_switch
  port map (
    clk         => clk,
    rst         => rst,
    switch_high => brc,  -- frame brc
    switch_low  => fsync,  -- frame sync
    out_level   => brc_det
  );

  -- any frame discarded over fsync interval
  u_dis_det : entity common_lib.common_switch
  port map (
    clk         => clk,
    rst         => rst,
    switch_high => in_dis,  -- frame discarded
    switch_low  => fsync,  -- frame sync
    out_level   => dis_det
  );

  -- frame count over fsync interval
  p_frame_cnt : process(cnt, fsync, in_eof)
  begin
    nxt_cnt <= cnt;
    if fsync = '1' then
      nxt_cnt <= (others => '0');
    elsif in_eof = '1' then
      nxt_cnt <= std_logic_vector(unsigned(cnt) + 1);
    end if;
  end process;
end architecture;
