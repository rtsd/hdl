-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.tech_fpga_voltage_sens_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

library ip_arria10_voltage_sense_altera_voltage_sense_150;
library ip_arria10_e3sge3_voltage_sense_altera_voltage_sense_151;
library ip_arria10_e1sg_voltage_sense_altera_voltage_sense_180;
library ip_arria10_e2sg_voltage_sense_altera_voltage_sense_1910;

entity tech_fpga_voltage_sens is
  generic (
    g_technology : natural := c_tech_select_default
  );
  port (
    clock_clk                    : in  std_logic := '0';
    reset_sink_reset             : in  std_logic;
    controller_csr_address       : in  std_logic := '0';
    controller_csr_read          : in  std_logic := '0';
    controller_csr_write         : in  std_logic := '0';
    controller_csr_writedata     : in  std_logic_vector(31 downto 0) := (others => '0');
    controller_csr_readdata      : out std_logic_vector(31 downto 0) := (others => '0');
    sample_store_csr_address     : in  std_logic_vector(3 downto 0) := "0000";
    sample_store_csr_read        : in  std_logic := '0';
    sample_store_csr_write       : in  std_logic := '0';
    sample_store_csr_writedata   : in  std_logic_vector(31 downto 0) := (others => '0');
    sample_store_csr_readdata      : out std_logic_vector(31 downto 0) := (others => '0');
    sample_store_irq_irq         : out std_logic
  );
end tech_fpga_voltage_sens;

architecture str of tech_fpga_voltage_sens is
begin
  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_voltage_sense
      port map (
        clock_clk                  => clock_clk,
        reset_sink_reset           => reset_sink_reset,
        controller_csr_address     => controller_csr_address,
        controller_csr_read        => controller_csr_read,
        controller_csr_write       => controller_csr_write,
        controller_csr_writedata   => controller_csr_writedata,
        controller_csr_readdata    => controller_csr_readdata,
        sample_store_csr_address   => sample_store_csr_address,
        sample_store_csr_read      => sample_store_csr_read,
        sample_store_csr_write     => sample_store_csr_write,
        sample_store_csr_writedata => sample_store_csr_writedata,
        sample_store_csr_readdata  => sample_store_csr_readdata,
        sample_store_irq_irq       => sample_store_irq_irq
      );
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_voltage_sense
      port map (
        clock_clk                  => clock_clk,
        reset_sink_reset           => reset_sink_reset,
        controller_csr_address     => controller_csr_address,
        controller_csr_read        => controller_csr_read,
        controller_csr_write       => controller_csr_write,
        controller_csr_writedata   => controller_csr_writedata,
        controller_csr_readdata    => controller_csr_readdata,
        sample_store_csr_address   => sample_store_csr_address,
        sample_store_csr_read      => sample_store_csr_read,
        sample_store_csr_write     => sample_store_csr_write,
        sample_store_csr_writedata => sample_store_csr_writedata,
        sample_store_csr_readdata  => sample_store_csr_readdata,
        sample_store_irq_irq       => sample_store_irq_irq
      );
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_voltage_sense
      port map (
        clock_clk                  => clock_clk,
        reset_sink_reset           => reset_sink_reset,
        controller_csr_address     => controller_csr_address,
        controller_csr_read        => controller_csr_read,
        controller_csr_write       => controller_csr_write,
        controller_csr_writedata   => controller_csr_writedata,
        controller_csr_readdata    => controller_csr_readdata,
        sample_store_csr_address   => sample_store_csr_address,
        sample_store_csr_read      => sample_store_csr_read,
        sample_store_csr_write     => sample_store_csr_write,
        sample_store_csr_writedata => sample_store_csr_writedata,
        sample_store_csr_readdata  => sample_store_csr_readdata,
        sample_store_irq_irq       => sample_store_irq_irq
      );
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_voltage_sense
      port map (
        clock_clk                  => clock_clk,
        reset_sink_reset           => reset_sink_reset,
        controller_csr_address     => controller_csr_address,
        controller_csr_read        => controller_csr_read,
        controller_csr_write       => controller_csr_write,
        controller_csr_writedata   => controller_csr_writedata,
        controller_csr_readdata    => controller_csr_readdata,
        sample_store_csr_address   => sample_store_csr_address,
        sample_store_csr_read      => sample_store_csr_read,
        sample_store_csr_write     => sample_store_csr_write,
        sample_store_csr_writedata => sample_store_csr_writedata,
        sample_store_csr_readdata  => sample_store_csr_readdata,
        sample_store_irq_irq       => sample_store_irq_irq
      );
  end generate;
end architecture;
