-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

-- Status:
-- . Compiles OK, but still needs to be functionally verified with a test bench.
-- . Add support for out_dat width is g_dat_w+1
-- . Perhaps u_stage_add_input can be merged into gen_stage

-- Purpose: Multi stage adder
-- Description:
--   E.g. a N stage adder will need N pipeline stages and each pipeline stage
--   has N parallel sections. The N stages are needed to ripple trhough the
--   carry of the N sections. Each section is an instantiation of
--   common_add_sub, so in total N**2 sections.
-- Remarks:
-- . Synthesizing common_top.vhd shows that even a 64b adder can run at 500 MHz
--   on Stratix IV so this multi stage adder is not needed.

entity common_adder_staged is
  generic (
    g_dat_w            : natural;
    g_adder_w          : natural;  -- g_adder_w internal adder width
    g_pipeline_input   : natural;  -- 0 no input registers, else register input
    g_pipeline_output  : natural  -- pipeline for the adder, must be >= ceil(g_dat_w / g_adder_w) to allow g_adder_w < g_dat_w
  );
  port (
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    in_dat_a    : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_dat_b    : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val      : in  std_logic := '1';
    out_dat     : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val     : out std_logic
  );
end common_adder_staged;

architecture str of common_adder_staged is
  constant c_pipeline  : natural := g_pipeline_input + g_pipeline_output;

  constant c_nof_adder : natural := g_dat_w / g_adder_w + sel_a_b(g_dat_w mod g_adder_w = 0, 0, 1);

  type t_inp_matrix is array (integer range <>, integer range <>) of std_logic_vector(g_adder_w - 1 downto 0);  -- (STAGE, SECTION)
  type t_sum_matrix is array (integer range <>, integer range <>) of std_logic_vector(g_adder_w   downto 0);  -- (STAGE, SECTION), width +1 for carry bit

  -- Input signals
  signal reg_dat_a      : std_logic_vector(g_dat_w - 1 downto 0) := (others => '0');
  signal reg_dat_b      : std_logic_vector(g_dat_w - 1 downto 0) := (others => '0');

  signal vec_dat_a      : std_logic_vector(c_nof_adder * g_adder_w - 1 downto 0);
  signal vec_dat_b      : std_logic_vector(c_nof_adder * g_adder_w - 1 downto 0);

  -- Internal sub adders
  signal m_a            : t_inp_matrix(0 to c_nof_adder - 1,  0 to c_nof_adder - 1);
  signal m_b            : t_inp_matrix(0 to c_nof_adder - 1,  0 to c_nof_adder - 1);
  signal m_sum          : t_sum_matrix(0 to c_nof_adder - 1, -1 to c_nof_adder - 1);  -- section index -1 for first zero carry input

  signal vec_add        : std_logic_vector(c_nof_adder * g_adder_w - 1 downto 0);

  -- Pipeline control signals, map to slv to be able to use common_pipeline
  signal in_val_slv     : std_logic_vector(0 downto 0);
  signal out_val_slv    : std_logic_vector(0 downto 0);
begin
  assert not(g_pipeline_output < c_nof_adder and g_adder_w < g_dat_w)
    report "common_adder_staged: internal adder width < output adder width is only possible for pipeline >= nof adder"
    severity FAILURE;

  ------------------------------------------------------------------------------
  -- Input
  ------------------------------------------------------------------------------

  u_pipe_a : entity common_lib.common_pipeline
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => g_pipeline_input,
    g_reset_value    => 0,
    g_in_dat_w       => g_dat_w,
    g_out_dat_w      => g_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => in_dat_a,
    out_dat => reg_dat_a
  );

  u_pipe_b : entity common_lib.common_pipeline
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => g_pipeline_input,
    g_reset_value    => 0,
    g_in_dat_w       => g_dat_w,
    g_out_dat_w      => g_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => in_dat_b,
    out_dat => reg_dat_b
  );

  ------------------------------------------------------------------------------
  -- Multiple adder sections (g_adder_w < g_dat_w)
  --
  -- . The number of sections depends on the g_dat_w / g_adder_w, the g_adder_w
  --   has to be small enough to ease timing closure for synthesis
  -- . The first register stage adds the input data and the subsequent stages
  --   add the carry from the preceding stage
  -- . The number of stages equals the number of sections.
  -- . The example below shows a case where all stages are needed to propagate
  --   the carry:
  --
  --          adder sections
  --             2    1    0   -1
  --           876  543  210       -- g_dat_w = 9, g_adder_w = 3
  -- STAGE 0:
  --           111  111  111       -- m_a(  0, SECTION) = in_dat_a
  --           000  000  001       -- m_b(  0, SECTION) = in_dat_b
  --          0111 0111 1000    0  -- m_sum(0, SECTION)
  -- STAGE 1:
  --           111  111  000       -- m_a(  1, SECTION) = m_sum(0, SECTION  )(2:0)
  --             0    1    0       -- m_b(  1, SECTION) = m_sum(0, SECTION-1)(3)   = carry
  --          0111 1000 0000    0  -- m_sum(1, SECTION)
  -- STAGE 2:
  --           111  000  000       -- m_a(  2, SECTION) = m_sum(1, SECTION  )(2:0)
  --             1    0    0       -- m_b(  2, SECTION) = m_sum(1, SECTION-1)(3)   = carry
  --          1000 0000 0000    0  -- m_sum(2, SECTION)
  --
  --           000  000  000                            = out_dat
  ------------------------------------------------------------------------------

  gen_multi : if g_pipeline_output >= c_nof_adder and g_adder_w < g_dat_w generate
    -- resize input length to multiple of g_adder_w
    vec_dat_a <= RESIZE_SVEC(reg_dat_a, c_nof_adder * g_adder_w);
    vec_dat_b <= RESIZE_SVEC(reg_dat_b, c_nof_adder * g_adder_w);

    -- initialize the first carry input for each stage to zero
    init_carry : for STAGE in 0 to c_nof_adder - 1 generate
      m_sum(STAGE, -1) <= (others => '0');
    end generate;

    gen_sections : for SECTION in 0 to c_nof_adder - 1 generate
      -- Map the input slv to adder stage 0
      m_a(0, SECTION) <= vec_dat_a((SECTION + 1) * g_adder_w - 1 downto SECTION * g_adder_w);
      m_b(0, SECTION) <= vec_dat_b((SECTION + 1) * g_adder_w - 1 downto SECTION * g_adder_w);

      u_stage_add_input : entity common_lib.common_add_sub
      generic map (
        g_direction       => "ADD",
        g_representation  => "UNSIGNED",  -- must treat the sections as unsigned
        g_pipeline_input  => 0,
        g_pipeline_output => 1,
        g_in_dat_w        => g_adder_w,
        g_out_dat_w       => g_adder_w + 1
      )
      port map (
        clk     => clk,
        clken   => clken,
        in_a    => m_a(0, SECTION),
        in_b    => m_b(0, SECTION),
        result  => m_sum(0, SECTION)
      );

      gen_stage : for STAGE in 1 to c_nof_adder - 1 generate
        m_a(STAGE, SECTION) <=             m_sum(STAGE-1, SECTION  )(g_adder_w - 1 downto 0);  -- sum from preceding stage
        m_b(STAGE, SECTION) <= RESIZE_UVEC(m_sum(STAGE-1, SECTION - 1)(g_adder_w), g_adder_w);  -- carry from less significant section in preceding stage

        -- Adder stages to add and propagate the carry for each section
        u_add_carry : entity common_lib.common_add_sub
        generic map (
          g_direction       => "ADD",
          g_representation  => "UNSIGNED",  -- must treat the sections as unsigned
          g_pipeline_input  => 0,
          g_pipeline_output => 1,
          g_in_dat_w        => g_adder_w,
          g_out_dat_w       => g_adder_w + 1
        )
        port map (
          clk     => clk,
          clken   => clken,
          in_a    => m_a(STAGE, SECTION),
          in_b    => m_b(STAGE, SECTION),  -- + carry 0 or 1 from the less significant adder section
          result  => m_sum(STAGE, SECTION)
        );
      end generate;

      -- map the adder sections from the last stage to the output to slv
      vec_add((SECTION + 1) * g_adder_w - 1 downto SECTION * g_adder_w) <= m_sum(c_nof_adder - 1, SECTION)(g_adder_w - 1 downto 0);
    end generate;

    -- Rest output pipeline
    u_out_val : entity common_lib.common_pipeline
    generic map (
      g_representation => "UNSIGNED",
      g_pipeline       => g_pipeline_output - c_nof_adder,
      g_reset_value    => 0,
      g_in_dat_w       => g_dat_w,
      g_out_dat_w      => g_dat_w
    )
    port map (
      clk     => clk,
      clken   => clken,
      in_dat  => vec_add(g_dat_w - 1 downto 0),  -- resize length of multiple g_adder_w back to g_dat_w width
      out_dat => out_dat
    );
  end generate;

  ------------------------------------------------------------------------------
  -- Parallel output control pipeline
  ------------------------------------------------------------------------------

  u_out_val : entity common_lib.common_pipeline
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => 1,
    g_out_dat_w      => 1
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => in_val_slv,
    out_dat => out_val_slv
  );

  in_val_slv(0) <= in_val;
  out_val       <= out_val_slv(0);
end str;
