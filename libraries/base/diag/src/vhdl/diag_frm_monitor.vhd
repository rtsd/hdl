--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Monitor a stream of frames with test sequence data.
-- Description:
--   Each frame is marked by an active in_eop. The in_sop is not needed and the
--   in_dat test data in the frame is ignored. The monitor does not need to
--   explicitly check the frame data, because it relies on the PHY level frame
--   error detection by e.g. and CRC check that was already done before this
--   monitor. This PHY level frame error information needs to be passed on to
--   the monitor via the in_error input.
--   The frame monitor is enabled by diag_en. When enabled the monitor is
--   always ready to receive frames.
--   Based on the in_eop it counts the number of received frames. Based on the
--   in_error signal that is valid during the in_eop it counts the number of
--   frames that have an error.
-- Remark:
-- . This component can be easily mapped to standard MM and ST interfaces.

entity diag_frm_monitor is
  generic (
    g_frame_cnt_w : natural := 32
  );
  port (
    rst             : in  std_logic;
    clk             : in  std_logic;
    clken           : in  std_logic := '1';

    -- Static control input (connect via MM)
    diag_en         : in  std_logic;
    diag_frame_cnt  : out std_logic_vector(g_frame_cnt_w - 1 downto 0);
    diag_error_cnt  : out std_logic_vector(g_frame_cnt_w - 1 downto 0);

    -- ST input
    in_eop          : in  std_logic;  -- '1' for last valid frame data
    in_error        : in  std_logic  -- '1' when received frame contained an error, '0' when OK
  );
end diag_frm_monitor;

architecture str of diag_frm_monitor is
  signal diag_en_revt       : std_logic;

  signal frame_cnt          : std_logic_vector(diag_frame_cnt'range);
  signal nxt_diag_frame_cnt : std_logic_vector(diag_frame_cnt'range);
  signal error_cnt          : std_logic_vector(diag_frame_cnt'range);
  signal nxt_diag_error_cnt : std_logic_vector(diag_frame_cnt'range);

  signal frm_eop            : std_logic;
  signal frm_error          : std_logic;
begin
  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      diag_frame_cnt <= (others => '0');
      diag_error_cnt <= (others => '0');
    elsif rising_edge(clk) then
      if clken = '1' then
        diag_frame_cnt <= nxt_diag_frame_cnt;
        diag_error_cnt <= nxt_diag_error_cnt;
      end if;
    end if;
  end process;

  -- Signal begin of diag_en
  u_diag_en_revt : entity common_lib.common_evt
  generic map (
    g_evt_type => "RISING",
    g_out_reg  => false
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    in_sig  => diag_en,
    out_evt => diag_en_revt
  );

  -- Hold last frame and error counts also when the monitor is disabled, clear when it is restarted
  nxt_diag_frame_cnt <= (others => '0') when diag_en_revt = '1' else frame_cnt;
  nxt_diag_error_cnt <= (others => '0') when diag_en_revt = '1' else error_cnt;

  -- Count all received frames
  u_frm_cnt : entity common_lib.common_counter
  generic map (
    g_width     => g_frame_cnt_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    cnt_clr => diag_en_revt,
    cnt_en  => in_eop,
    count   => frame_cnt
  );

  -- Count the received frames that had an error
  frm_error <= in_eop and in_error;

  u_err_cnt : entity common_lib.common_counter
  generic map (
    g_width     => g_frame_cnt_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    cnt_clr => diag_en_revt,
    cnt_en  => frm_error,
    count   => error_cnt
  );
end str;
