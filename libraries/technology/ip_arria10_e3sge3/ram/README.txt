README.txt for $HDL_WORK/libraries/technology/ip_arria10/ram

Contents:

1) RAM components
2) ROM components
3) Arria10 IP
4) Inferred IP
5) Memory initialisation file
6) Implementation options (LUTs or block RAM)
7) Synthesis trials


1) RAM components:

  ip_arria10_ram_crwk_crw = Two read/write ports each port with own port clock and with power of two ratio between port widths
  ip_arria10_ram_crw_crw  = Two read/write ports each port with own port clock and with same address and data width on both ports
  ip_arria10_ram_cr_cw    = One read port with clock and one write port with clock and with same address and data width on both ports
  ip_arria10_ram_r_w      = Single clock, one read port and one write port and with same address and data width on both ports
  

2) ROM components:
  ip_arria10_rom_r_w      = Not available and not needed, because the ip_arria10_ram_r_w can be used for ROM IP by not connecting the
                            write port.


3) Arria10 IP

  The IP only needs to be generated with
  
   ./generate_ip.sh
   
  if it need to be modified, because the ip_arria10_ram_*.vhd directly instantiates the altera_syncram component.
  
  The instantiation is copied manually from the ip_arria10_ram_*/ram_2port_140/sim/ip_arria10_ram_*.vhd.
  
  It appears that the altera_syncram component can be synthesized even though it comes from the altera_lnsim package, 
  that is a simulation package. However it resembles how it worked for Straix IV with altera_mf.


4) Inferred IP

  The inferred Altera code was obtained using template insert with Quartus 14.0a10.
  The ram_crwk_crw can not be inferred.
  For the other RAM the g_inferred generic is set to FALSE because the inferred instances do not yet support g_init_file.
  It is possible to init the RAM using a function e.g.:
 
      function init_ram
        return memory_t is 
        variable tmp : memory_t := (others => (others => '0'));
      begin 
        for addr_pos in 0 to 2**ADDR_WIDTH - 1 loop 
          -- Initialize each address with the address itself
          tmp(addr_pos) := std_logic_vector(to_unsigned(addr_pos, DATA_WIDTH));
        end loop;
        return tmp;
      end init_ram;  
    
      -- Declare the RAM signal and specify a default value.  Quartus II
      -- will create a memory initialization file (.mif) based on the 
      -- default value.
      signal ram : memory_t := init_ram;

5) Memory initialisation file

  To support the g_init_file requires first reading the file in a certain format. For us an integer format or SLV format
  with one value per line (line number = address) would be fine. Using SLV format is necessary if the RAM data is wider
  than 32 bit, because VHDL integer range is only 2**32. The tb_common_pkg has functiosn to read such a file. Quartus
  creates a mif file from this when it infers the RAM. However our current UniBoard1 designs provide a mif file that fits
  the RAM IP. Therefore it is easier initially to also use the RAM IP for Arria10. In future for RadioHDL a generic
  RAM init file format is preferrable though.
 

6) Implementation options (LUTs or block RAM)

  The IP and inferred RAM can be set to use LUTs (MLAB) or block RAM (M20K), however this is not supported yet.
  
  . For IP RAM this would imply adding a generic to set the appropriate parameter in the altera_syncram
  . For inferred RAM is would imply adding a generic to be used for the syntype attribute. 
    From http://www.alterawiki.com/wiki/Mapping_SRLs_to_registers,_MLABs,_or_Block_RAMs:  

      entity
          g_ramstyle : STRING := "MLAB,no_rw_check"
      architecture  
          attribute ramstyle : string;
    
          signal ram : memory_t := init_ram;
          attribute ramstyle of ram : signal is g_ramstyle; 
  

7) Synthesis trials

  The quartus/ram.qpf Quartus project was used to verify that the inferred RAM and the block RAM IP actually synthesise
  to the appropriate FPGA resources.
  Use the Quartus GUI to manually select a top level component for synthesis e.g. by right clicking the entity vhd file
  in the file tab of the Quartus project navigator window.
  Then check the resource usage in the synthesis and fitter reports.
