--------------------------------------------------------------------------------
--
-- Copyright (C) 2009-2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.unb2_board_pkg.all;
use technology_lib.technology_pkg.all;

-- Keep the UniBoard system info knowledge in this HDL entity and in the
-- corresponding software functions in unb_common.c,h. This avoids having to
-- define named constants for indexing the fields in the info word.

entity unb2_board_system_info is
  generic (
    g_sim        : boolean := false;
    g_fw_version : t_unb2_board_fw_version := c_unb2_board_fw_version;  -- firmware version x.y (4b.4b)
    g_aux        : t_c_unb2_board_aux := c_unb2_board_aux;  -- aux contains the hardware version
    g_rom_version: natural := 1;
    g_technology : natural := c_tech_arria10
  );
  port (
    clk         : in  std_logic;
    hw_version  : in  std_logic_vector(g_aux.version_w - 1 downto 0);
    id          : in  std_logic_vector(g_aux.id_w - 1 downto 0);
    info        : out std_logic_vector(c_word_w - 1 downto 0);
    bck_id      : out std_logic_vector(c_unb2_board_nof_uniboard_w - 1 downto 0);  -- ID[7:2]
    chip_id     : out std_logic_vector(c_unb2_board_nof_chip_w - 1 downto 0);  -- ID[1:0]
    node_id     : out std_logic_vector(c_unb2_board_nof_node_w - 1 downto 0);  -- ID[1:0]
    is_node2    : out std_logic  -- '1' for Node 2, else '0'.
  );
end unb2_board_system_info;

architecture str of unb2_board_system_info is
  signal cs_sim         : std_logic;

  signal hw_version_reg : std_logic_vector(hw_version'range);
  signal id_reg         : std_logic_vector(id'range);
  signal nxt_info       : std_logic_vector(info'range);

  signal nxt_bck_id     : std_logic_vector(bck_id'range);
  signal nxt_chip_id    : std_logic_vector(chip_id'range);
  signal nxt_node_id    : std_logic_vector(node_id'range);
  signal nxt_is_node2   : std_logic;
begin
  p_reg : process(clk)
  begin
    if rising_edge(clk) then
      -- inputs
      hw_version_reg <= hw_version;
      id_reg         <= id;
      -- output
      info           <= nxt_info;
      bck_id         <= nxt_bck_id;
      chip_id        <= nxt_chip_id;
      node_id        <= nxt_node_id;
      is_node2       <= nxt_is_node2;
    end if;
  end process;

  cs_sim <= is_true(g_sim);

  p_info : process(cs_sim, hw_version_reg, id_reg)
  begin
    nxt_info(31 downto 27) <= TO_UVEC(g_technology, 5);
    nxt_info(26 downto 24) <= TO_UVEC(g_rom_version, 3);
    nxt_info(23 downto 20) <= TO_UVEC(g_fw_version.hi, 4);
    nxt_info(19 downto 16) <= TO_UVEC(g_fw_version.lo, 4);
    nxt_info(10)           <= cs_sim;
    nxt_info(9 downto 8)   <= hw_version_reg;
    nxt_info(7 downto 0)   <= id_reg;
  end process;

  nxt_bck_id   <= id_reg(7 downto 2);
  nxt_chip_id  <= id_reg(1 downto 0);
  nxt_node_id  <= id_reg(1 downto 0);
  nxt_is_node2 <= '1' when TO_UINT(id_reg(1 downto 0)) = 2 else '0';
end str;
