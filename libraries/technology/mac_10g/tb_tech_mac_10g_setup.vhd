-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Reusable test bench stimuli to setup tech_mac_10g via MM
-- Description:

library IEEE, technology_lib, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.tb_common_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.tb_tech_mac_10g_pkg.all;
use work.tech_mac_10g_component_pkg.all;

entity tb_tech_mac_10g_setup is
  generic (
    g_technology    : natural := c_tech_select_default;
    g_nof_macs      : positive := 1;
    g_src_mac       : std_logic_vector(c_network_eth_mac_slv'range) := X"123456789ABC"  -- = 12-34-56-78-9A-BC
  );
  port (
    tb_end    : in  std_logic := '0';
    mm_clk    : out std_logic;
    mm_rst    : out std_logic;
    mm_init   : out std_logic;
    mac_mosi  : out t_mem_mosi;
    mac_miso  : in  t_mem_miso
  );
end tb_tech_mac_10g_setup;

architecture tb of tb_tech_mac_10g_setup is
  constant mm_clk_period        : time := 20 ns;  -- 50 MHz

  -- Clocks and reset
  signal i_mm_clk          : std_logic := '0';  -- memory-mapped bus clock
  signal i_mm_rst          : std_logic;  -- reset synchronous with mm_clk
begin
  mm_clk <= i_mm_clk;
  mm_rst <= i_mm_rst;

  i_mm_clk <= not i_mm_clk or tb_end after mm_clk_period / 2;  -- MM clock
  i_mm_rst <= '1', '0' after mm_clk_period * 10;

  p_mm_setup : process
    variable v_mm_offset : natural;
  begin
    mm_init  <= '1';
    mac_mosi.wr <= '0';
    mac_mosi.rd <= '0';

    -- wait until after reset release
    proc_common_wait_until_low(i_mm_clk, i_mm_rst);
    proc_common_wait_some_cycles(i_mm_clk, 10);

    for I in 0 to g_nof_macs - 1 loop
      v_mm_offset := I * 2**func_tech_mac_10g_csr_addr_w(g_technology);
      proc_tech_mac_10g_setup(g_technology,
                              g_src_mac,
                              v_mm_offset,
                              i_mm_clk, mac_miso, mac_mosi);
    end loop;
    mm_init <= '0';
    wait;
  end process;
end tb;
