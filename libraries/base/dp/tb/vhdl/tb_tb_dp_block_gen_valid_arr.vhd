-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi test bench for dp_block_gen_valid_arr
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_block_gen_valid_arr is
end tb_tb_dp_block_gen_valid_arr;

architecture tb of tb_tb_dp_block_gen_valid_arr is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --  g_nof_streams           : POSITIVE := 1;
  --  g_nof_data_per_block    : POSITIVE := 11;
  --  g_nof_blk_per_sync      : POSITIVE := 8;
  --  g_check_input_sync      : BOOLEAN := FALSE;
  --  g_nof_pages_bsn         : NATURAL := 0;
  --  g_restore_global_bsn    : BOOLEAN := FALSE;
  --  g_enable                : t_dp_flow_control_enum := e_active;  -- always e_active or e_pulse block generator enable
  --  g_flow_control          : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control for input valid
  --  g_nof_repeat            : NATURAL := 200

  u_input_bsn                   : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 8, false, 0, false, e_active, e_active, 100);
  u_input_bsn_check_sync        : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 9,  true, 0, false, e_active, e_active, 100);
  u_buffer_input_bsn            : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 5, false, 1, false, e_active, e_active, 100);

  u_en_input_bsn                : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 8, false, 0, false, e_pulse, e_active, 500);
  u_en_input_bsn_check_sync     : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 9,  true, 0, false, e_pulse, e_active, 500);
  u_en_buffer_input_bsn         : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 5, false, 1, false, e_pulse, e_active, 500);

  u_en_rnd_input_bsn            : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 8, false, 0, false, e_pulse, e_random, 500);
  u_en_rnd_input_bsn_check_sync : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 9,  true, 0, false, e_pulse, e_random, 500);
  u_en_rnd_buffer_input_bsn     : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 5, false, 1, false, e_pulse, e_random, 500);

  u_global_input_bsn            : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 8, false, 0,  true, e_active, e_active, 100);
  u_global_input_bsn_check_sync : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 9,  true, 0,  true, e_active, e_active, 100);
  u_buffer_global_input_bsn     : entity work.tb_dp_block_gen_valid_arr generic map (1, 11, 5, false, 1,  true, e_active, e_active, 100);
end tb;
