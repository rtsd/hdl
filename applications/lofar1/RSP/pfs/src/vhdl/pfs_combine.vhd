library IEEE;
use IEEE.std_logic_1164.all;

entity pfs_combine is
  generic (
    g_nof_fir         : integer;
    g_data_w          : integer
  );
  port (
    in_dat_x          : in  std_logic_vector(g_nof_fir * g_data_w - 1 downto 0);
    in_dat_y          : in  std_logic_vector(g_nof_fir * g_data_w - 1 downto 0);
    in_val            : in  std_logic_vector(g_nof_fir - 1 downto 0);
    in_sync           : in  std_logic_vector(g_nof_fir - 1 downto 0);
    out_dat_x         : out std_logic_vector(g_data_w - 1 downto 0);
    out_dat_y         : out std_logic_vector(g_data_w - 1 downto 0);
    out_val           : out std_logic;
    out_sync          : out std_logic;
    clk               : in  std_logic;
    rst               : in  std_logic
  );
end pfs_combine;
