-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- Purpose:
--   Model a transceiver link.
-- Description:
--   The latency of the link is modeled by delay g_latency.
--   If the real link has e.g. 8b/10b coding, then it can transmit fill words.
--   This is modeled by g_valid_support = TRUE. When in_val is low then a fill
--   word is transmitted that can not occur in the user data. At the receiver
--   the fill words can be passed on through out_val low. If the real link has
--   no coding, then this is modeled by g_valid_support = FALSE and out_val
--   will be high always.

entity dp_phy_link is
  generic (
    g_latency        : time := 1 us;
    g_valid_support  : boolean := true
  );
  port (
    in_dat           : in  std_logic_vector;
    in_val           : in  std_logic := '1';

    out_dat          : out std_logic_vector;
    out_val          : out std_logic
  );
end dp_phy_link;

architecture beh of dp_phy_link is
begin
  out_dat <= transport in_dat after g_latency;

  gen_valid : if g_valid_support = true generate
    out_val <= transport in_val after g_latency;
  end generate;

  no_valid : if g_valid_support = false generate
    out_val <= '1';
  end generate;
end beh;
