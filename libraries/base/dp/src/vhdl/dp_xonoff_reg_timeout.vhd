-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for dp_xonoff with timeout
-- Description:
--
--  Read/write register for stream enable/disable, disables <g_default_value> seconds after last enable write
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |                                                                     |X|  0
--  |-----------------------------------------------------------------------|
--
--
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity dp_xonoff_reg_timeout is
  generic (
    g_default_value : std_logic := '1';
    g_mm_timeout    : natural   := 10;  -- 10 Seconds, Max = 42
    g_sim           : boolean   := false  -- When True, use micro seconds instead of seconds
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;
    st_rst            : in  std_logic;
    st_clk            : in  std_logic;

    -- Memory Mapped Slave in mm_clk domain
    sla_in            : in  t_mem_mosi;
    sla_out           : out t_mem_miso;

    -- MM registers in st_clk domain
    xonoff_reg        : out std_logic_vector(0 downto 0)
  );
end dp_xonoff_reg_timeout;

architecture str of dp_xonoff_reg_timeout is
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => 1,
                                  dat_w    => c_word_w,
                                  nof_dat  => 1,
                                  init_sl  => g_default_value);
  constant c_mm_max_counter : natural := sel_a_b(g_sim, g_mm_timeout * 50, g_mm_timeout * (50 * 10**6));  -- @50MHz

  signal mm_xonoff_reg     : std_logic_vector(0 downto 0);
  signal mm_xonoff_reg_out : std_logic_vector(0 downto 0);
  signal counter           : std_logic_vector(c_word_w - 1 downto 0);
  signal cnt_clr           : std_logic := '0';
  signal cnt_en            : std_logic := '1';
begin
  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out   <= c_mem_miso_rst;
      -- Write access, register values
      mm_xonoff_reg(0) <= g_default_value;

    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';
      cnt_clr <= '0';
      -- Check if timeout is reached
      if TO_UINT(counter) >= c_mm_max_counter then
        cnt_en <= '0';
        mm_xonoff_reg_out(0) <= '0';
      else
        mm_xonoff_reg_out <= mm_xonoff_reg;
      end if;

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            mm_xonoff_reg(0) <= sla_in.wrdata(0);
            if sla_in.wrdata(0) = '1' then
               cnt_clr <= '1';
               cnt_en  <= '1';
            end if;

          when others => null;  -- not used MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Read Block Sync
          when 0 =>
            sla_out.rddata(0) <= mm_xonoff_reg(0);

          when others => null;
        end case;
      end if;
    end if;
  end process;

  u_counter : entity common_lib.common_counter
  generic map (
    g_latency  => 0
  )
  port map (
    rst        => mm_rst,
    clk        => mm_clk,
    cnt_clr    => cnt_clr,
    cnt_en     => cnt_en,
    count      => counter
  );

  u_reg_cross_domain : entity common_lib.common_reg_cross_domain
  port map (
    in_rst     => mm_rst,
    in_clk     => mm_clk,

    in_dat     => mm_xonoff_reg_out,

    out_rst    => st_rst,
    out_clk    => st_clk,

    out_dat    => xonoff_reg
  );
end str;
