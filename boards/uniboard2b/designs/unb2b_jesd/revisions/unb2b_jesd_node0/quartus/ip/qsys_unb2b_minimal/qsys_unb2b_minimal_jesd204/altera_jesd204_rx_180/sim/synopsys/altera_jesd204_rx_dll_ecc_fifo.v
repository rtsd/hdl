// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
PYZ3iz3BtKtsF1fhzUatETu75d/AEIPF6P/T+jjbj8ndzyobW62Vbynmq26K8ltS
kDF+Sks/V+P/nBDteeB74SM6wagFRrzAudBi3Q1jVzEBhSYexz3PTqPn3N8Caatg
3oiMXuIx1jUWg3wJiDWa/ONtRzgBJJBpYQVcWYs0xgg=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 1312 )
`pragma protect data_block
gEN4qKKn3Qk+zr4aID5vn5+AeqWIYCYupUYGrbAcqdsavL2PNYez0dstwbFvN5YJ
I4AD/bJxRrTWSqUos9uH4Aux912RxWcVOFecdx8/rxi4wURx244HtlqtInt0RmyA
mK0Wkj4eQzeZb/4pncM4oZ62J7KBj+B67+CiS7CR03eSy8yJp87bUPTfNWNrCwgN
9R8pAeWW3QOFUgOqlQKZs4OieNu9+0sMbXuOP8JuCA/3w4qJT3eqn9x7Brl7hbn5
tU3u9QvOv8LTnsMZmT6DpFZ029MYZo3vkbWKN2PJxERSF5KM0yUyluQGM1i40bGg
UldaHD8vt2KpBQ++lCQFcuc+w0wunJ51QWYRoNJNEC5xIqo+mjRK5W5ZWO0HxLyU
gnbzZHj+MJrlUJyrj8b7WMaFB4tNRzHV9LfjVv2xmNWrgmZXMSkM2FAHF32VqdmM
iDBU1s1FXSu5qHz9OWvKPMuJTqpnf8o4ha/597KuhHmfzL7BoJ8YVkzZ0MIs8tU4
rvKgGqj0PVhzEuDc8ixjnh/vDll9U9jfCAohUDg0OxEvujr1lnN/XoV5lyL6hSt/
vf8nYJIPCa9zlTvqEePpdBHzzoGcMspv1RT/psClMMur/uIOP7RsliRqFmBzdQyS
QOKGKTjE3cBKCCciyFSbZHMh4CrkWF9Ae6ucF1yl+LRFcGS28K/FP0AJ3+eiY0PO
6J7jU/lJMppzUX7Ks1DDorGxC4lKGzJyryet8/UJlNoxkVmAQcKX0cYS43w3UfNi
40sT8II7U9W4wYaSVwzpdRBoI97dNiGDZHXQOoRWsK3XGauUdisGDjOIKNtGc6z8
EVT3mmqSzZcdoa26RHbJfVNox79XDCxY4+AbxenIYtgal1faRz187fwiHW5xgcqe
dGoAxjiNAfcNUm6jXdysj+5/S9kQWDUcjsvAI1d4JAOKyxQBYjXhYtAptfyD3t4c
D12FFu1UxS1vNO//0fA7bjRUnFOLAi8W0zuMicuZofMKIolzbczbX4SEtt2PtYAL
3iR5feTmcywaQvIK5dHD/7vc6GSpoQd5AkfpMExivr8VBMbfJoIUC8Y0ICK3nOAH
qfEm2KKvecGOph177d9ctauvmh5PTO+JTz1nqjz6lH0dNiYbBu7K74y88THAJvsV
QKGpHBBIrPY364c9bCbbaC5YLpfbjIuZ5u5vBMvYwgPrPaYYboIEEjMxN4uJbAGC
8H0DTY7UVmW4Y7JtCUxY6jHFoNJWKLMrNQP1PEI6lN4FqUlvITdYnNty8Qrwus3i
gVTl365bpAMCSenULvgeKiA+wnqmtR+67kP5QX9k5ImHpEJ+g1d2gBn2yz5j2atE
2ZX2HP3gkEhgPWMEmFICU9qeH815+T7/c3/+bDM6kVuRe5VR7MquaYeZz1iznLZG
67wdgJU6MWcPA9TnqgDRlh/jxocqHCPLkLKQ7ifV+RV1Pg0UNN0sGr0/h+gcpzeF
r7HlTAMDHyi8xPPN9Ap9JLF+vlo95sPsb5j1KY1GsPfxsSoIxAzmQteE9hHzF0KY
FmmgMaSQKBqmh1V2UmKplWrd+xm38oY5P7mPY131/74e70UdhSwU6t4KSglTpT0j
dkoOzp+7U1o+19SRqNsDcoySmyvj9/xQiXBmLtyfVo/Z1gZXFcZJsGrzLtJWkbhI
EFb0wPKH6aITLLKYnPjKBk9GDRPrvroycRPVPi0BNvAdezThRZP+jd/36kX3jUz5
/LoeF+QZPkmUDsk4NHJTTg==

`pragma protect end_protected
