-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Status: Legacy, do not use in new designs
-- . This dp_offload_tx should not be used for new designs, instead use
--   dp_concat_field_blk.vhd. In dp_offload_tx.vhd the alignment of
--   snk_in_arr and hdr_fields_in_arr is problematic to maintain after the
--   dp_split and dp_packet_merge functions. By removing these functions
--   the input interface becomes clear and that is what dp_concat_field_blk
--   does. If tail remove (by dp_split, but preferrably use dp_block_resize)
--   and or multiple blocks per packet (by dp_packet_merge) are needed,
--   then this functions should be applied before dp_concat_field_blk, so
--   before hdr_fields_in_arr.
-- Author: Daniel van der Schuur
--         Eric Kooistra
-- Purpose:
-- . Reduce data rate and/or packet rate and add a user-defined header
-- Description:
-- . From each incoming streams, dp_offload_tx allows the user to set:
--   . The number of words per incoming frame that is selected (word 0..g_nof_words_per_block-1)
--   . The numer of blocks of these selected words that are put into one new frame (g_nof_blocks_per_packet)
-- . The header contents can be controlled dynamically by data path or MM control (selected by g_hdr_field_sel)
-- Remarks:

library IEEE, common_lib, technology_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_offload_tx is
  generic (
    g_technology                : natural := c_tech_select_default;
    g_nof_streams               : natural;
    g_data_w                    : natural;
    g_use_complex               : boolean;  -- TRUE uses re(0..g_data_w/2 -1) & im(0..g_data_w/2-1) as input instead of data(0..g_data_w-1).
    g_nof_words_per_block       : natural;
    g_nof_blocks_per_packet     : natural;
    g_hdr_field_arr             : t_common_field_arr;  -- User defined header fields
    g_hdr_field_sel             : std_logic_vector;  -- For each header field, select the source: 0=data path, 1=MM controlled
    g_use_post_split_fifo       : boolean := true;
    g_pkt_merge_align_at_sync   : boolean := false
  );
  port (
    mm_rst               : in  std_logic;
    mm_clk               : in  std_logic;

    dp_rst               : in  std_logic;
    dp_clk               : in  std_logic;

    reg_hdr_dat_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_hdr_dat_miso     : out t_mem_miso;

    snk_in_arr           : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    snk_out_arr          : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    src_out_arr          : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_in_arr           : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    hdr_fields_in_arr    : in  t_slv_1024_arr(g_nof_streams - 1 downto 0)  -- hdr_fields_in_arr(i) is considered valid @ snk_in_arr(i).sop
  );
end dp_offload_tx;

architecture str of dp_offload_tx is
  constant c_dp_split_val_latency               : natural := 1;
  constant c_dp_packet_merge_val_latency        : natural := 2;
  constant c_nof_header_words                   : natural := field_nof_words(g_hdr_field_arr, g_data_w);
  constant c_nof_merged_pkt_words               : natural := g_nof_words_per_block * g_nof_blocks_per_packet;
  constant c_dp_field_blk_snk_data_w            : natural := field_slv_out_len(field_arr_set_mode(g_hdr_field_arr,  "RW"));
  constant c_dp_field_blk_src_data_w            : natural := g_data_w;

  -- Internal FIFO fill level: one merged packet (to provide an uninterrupted output packet)
  constant c_dp_fifo_fill                       : natural := c_nof_merged_pkt_words;
  -- Total internat FIFO size: one merged packet + one header (we need to be able to hold the data path
  -- during header insertion) + margin
  constant c_dp_fifo_size                       : natural := c_dp_fifo_fill + c_nof_header_words + 10;

  signal dp_split_snk_in_arr                    : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_split_hdr_fields_snk_in_arr         : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);  -- init to avoid 'X' in unused record fields

  signal dp_split_snk_out_arr                   : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_split_src_out_2arr                  : t_dp_sosi_2arr_2(g_nof_streams - 1 downto 0);
  signal dp_split_hdr_fields_src_out_arr        : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal dp_split_src_in_2arr                   : t_dp_siso_2arr_2(g_nof_streams - 1 downto 0);
  signal dp_split_hdr_fields_src_in_arr         : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_packet_merge_src_out_arr            : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_packet_merge_hdr_fields_src_out_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal dp_packet_merge_src_in_arr             : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal dp_packet_merge_hdr_fields_src_in_arr  : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_fifo_fill_src_out_arr               : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_fifo_fill_src_in_arr                : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_field_blk_snk_in_arr                : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_field_blk_snk_out_arr               : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_concat_snk_in_2arr                  : t_dp_sosi_2arr_2(g_nof_streams - 1 downto 0);
  signal dp_concat_snk_out_2arr                 : t_dp_siso_2arr_2(g_nof_streams - 1 downto 0);

  signal reg_hdr_dat_mosi_arr                   : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_hdr_dat_miso_arr                   : t_mem_miso_arr(g_nof_streams - 1 downto 0);
begin
  ---------------------------------------------------------------------------------------
  -- Wire inputs to dp_split inputs
  ---------------------------------------------------------------------------------------
  -- use complex or data field
  gen_wires_complex : if g_use_complex = true generate
    p_wires_complex : process(snk_in_arr)
    begin
      for i in 0 to g_nof_streams - 1 loop
        dp_split_snk_in_arr(i) <= snk_in_arr(i);
        dp_split_snk_in_arr(i).data(g_data_w - 1 downto 0) <= snk_in_arr(i).re(g_data_w / 2 - 1 downto 0) & snk_in_arr(i).im(g_data_w / 2 - 1 downto 0);
      end loop;
    end process;
  end generate;

  gen_wires_data : if g_use_complex = false generate
    dp_split_snk_in_arr <= snk_in_arr;
  end generate;

  -- hdr_fields_in_arr(i) is considered valid at snk_in_arr(i).sop
  gen_dp_pipeline_hdr_fields_in_arr : for i in 0 to g_nof_streams - 1 generate
    dp_split_hdr_fields_snk_in_arr(i).data(field_slv_len(g_hdr_field_arr) - 1 downto 0) <= hdr_fields_in_arr(i)(field_slv_len(g_hdr_field_arr) - 1 downto 0);
    dp_split_hdr_fields_snk_in_arr(i).valid                                           <= snk_in_arr(i).sop;
  end generate;

  ---------------------------------------------------------------------------------------
  -- Split nof_words_per_block (forward only the first nof_words_per_block of each block)
  ---------------------------------------------------------------------------------------
  gen_dp_split : for i in 0 to g_nof_streams - 1 generate
    snk_out_arr(i).ready <= dp_split_snk_out_arr(i).ready;
    snk_out_arr(i).xon   <= src_in_arr(i).xon;  -- Pass on XON from source side

    u_dp_split : entity work.dp_split
    generic map (
      g_data_w         => g_data_w,
      g_symbol_w       => g_data_w,
      g_nof_symbols    => g_nof_words_per_block
    )
    port map (
      rst             => dp_rst,
      clk             => dp_clk,

      snk_out         => dp_split_snk_out_arr(i),
      snk_in          => dp_split_snk_in_arr(i),

      src_in_arr      => dp_split_src_in_2arr(i),
      src_out_arr     => dp_split_src_out_2arr(i)
    );

    dp_split_src_in_2arr(i)(0) <= c_dp_siso_rdy;  -- Always ready to throw away the tail
  end generate;

  -- Introduce the same delay (as dp_plit) on the corresponding header fields
  u_dp_pipeline_arr_dp_split : entity work.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_streams,
    g_pipeline    => c_dp_split_val_latency
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,

    snk_in_arr  => dp_split_hdr_fields_snk_in_arr,
    snk_out_arr => OPEN,  -- Flow control is already taken care of by dp_split

    src_out_arr => dp_split_hdr_fields_src_out_arr,
    src_in_arr  => dp_split_hdr_fields_src_in_arr
  );

  ---------------------------------------------------------------------------------------
  -- Merge nof_blocks_per_packet
  ---------------------------------------------------------------------------------------
  gen_dp_packet_merge : for i in 0 to g_nof_streams - 1 generate
    u_dp_packet_merge : entity work.dp_packet_merge
    generic map (
      g_nof_pkt       => g_nof_blocks_per_packet,
      g_align_at_sync => g_pkt_merge_align_at_sync
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,

      snk_out     => dp_split_src_in_2arr(i)(1),
      snk_in      => dp_split_src_out_2arr(i)(1),

      src_in      => dp_packet_merge_src_in_arr(i),
      src_out     => dp_packet_merge_src_out_arr(i)
    );
  end generate;

  -- Introduce the same delay (as dp_packet_merge) on the corresponding header fields
  u_dp_pipeline_arr_dp_packet_merge : entity work.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_streams,
    g_pipeline    => c_dp_packet_merge_val_latency
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,

    snk_in_arr  => dp_split_hdr_fields_src_out_arr,
    snk_out_arr => dp_split_hdr_fields_src_in_arr,

    src_out_arr => dp_packet_merge_hdr_fields_src_out_arr,
    src_in_arr  => dp_packet_merge_hdr_fields_src_in_arr
  );

  -- dp_packet_merge_hdr_fields_src_out_arr contains a valid header for each block that was merged
  -- into one packet. We want only the first valid header per merged block to be forwarded to
  -- dp_field_blk.
  -- . From here on, we won't need any more pipelining for the header as we now have one header
  --   in sync with each merged packet.
  --   . dp_field_blk has flow control and dp_concat will hold the header until the payload
  --     also arrives via the fill fifo.
  p_wire_valid : process(dp_packet_merge_src_out_arr, dp_packet_merge_hdr_fields_src_out_arr)
  begin
    for i in 0 to g_nof_streams - 1 loop
      dp_field_blk_snk_in_arr(i) <= dp_packet_merge_hdr_fields_src_out_arr(i);
      dp_field_blk_snk_in_arr(i).valid <= dp_packet_merge_src_out_arr(i).sop;
    end loop;
  end process;

  dp_packet_merge_hdr_fields_src_in_arr <= dp_field_blk_snk_out_arr;

  ---------------------------------------------------------------------------------------
  -- Use this FIFO when the gaps in the input stream are not large enoung for the header
  -- to be insterted.
  -- . This FIFO buffers the merged packets; the corresponding headers go around it.
  -- . This FIFO sits behind dp_split so data rate here is reduced (if
  --   g_nof_words_per_block < block length)
  -- . Use a FILL fifo variant here so the output frames can be read out as fast the sink
  --   (most likely an Ethernet core) would like
  ---------------------------------------------------------------------------------------
  gen_dp_fifo_fill : for i in 0 to sel_a_b(g_use_post_split_fifo, g_nof_streams, 0) - 1 generate
    u_dp_fifo_fill : entity work.dp_fifo_fill
    generic map (
      g_technology => g_technology,
      g_data_w    => g_data_w,
      g_fifo_fill => c_dp_fifo_fill,
      g_fifo_size => c_dp_fifo_size
     )
    port map (
      rst        => dp_rst,
      clk        => dp_clk,

      snk_in     => dp_packet_merge_src_out_arr(i),
      snk_out    => dp_packet_merge_src_in_arr(i),

      src_out    => dp_fifo_fill_src_out_arr(i),
      src_in     => dp_fifo_fill_src_in_arr(i)
    );

    dp_fifo_fill_src_in_arr(i).ready <= dp_concat_snk_out_2arr(i)(0).ready;
    dp_fifo_fill_src_in_arr(i).xon   <= '1';  -- Prevents flushing of frames

    dp_concat_snk_in_2arr(i)(0) <= dp_fifo_fill_src_out_arr(i);
  end generate;

  no_dp_fifo_fill : for i in 0 to sel_a_b(g_use_post_split_fifo, 0, g_nof_streams) - 1 generate
    dp_concat_snk_in_2arr(i)(0)   <= dp_packet_merge_src_out_arr(i);
    dp_packet_merge_src_in_arr(i) <= dp_concat_snk_out_2arr(i)(0);
  end generate;

  ---------------------------------------------------------------------------------------
  -- Create header block & concatenate header to offload stream.
  ---------------------------------------------------------------------------------------
  gen_dp_field_blk : for i in 0 to g_nof_streams - 1 generate
    -- Create multi-cycle header block from single-cycle wide header SLV
    u_dp_field_blk : entity work.dp_field_blk
    generic map (
      g_field_arr   => field_arr_set_mode(g_hdr_field_arr,  "RW"),
      g_field_sel   => g_hdr_field_sel,
      g_snk_data_w  => c_dp_field_blk_snk_data_w,
      g_src_data_w  => c_dp_field_blk_src_data_w
    )
    port map (
      dp_rst       => dp_rst,
      dp_clk       => dp_clk,

      mm_rst       => mm_rst,
      mm_clk       => mm_clk,

      snk_in       => dp_field_blk_snk_in_arr(i),
      snk_out      => dp_field_blk_snk_out_arr(i),

      src_in       => dp_concat_snk_out_2arr(i)(1),
      src_out      => dp_concat_snk_in_2arr(i)(1),

      reg_slv_mosi => reg_hdr_dat_mosi_arr(i),
      reg_slv_miso => reg_hdr_dat_miso_arr(i)
    );
  end generate;

  gen_dp_concat : for i in 0 to g_nof_streams - 1 generate
    u_dp_concat : entity work.dp_concat
    generic map (
      g_data_w    => g_data_w,
      g_symbol_w  => 1
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,

      snk_out_arr => dp_concat_snk_out_2arr(i),
      snk_in_arr  => dp_concat_snk_in_2arr(i),

      src_in      => src_in_arr(i),
      src_out     => src_out_arr(i)
    );
  end generate;

  ---------------------------------------------------------------------------------------
  -- MM control & monitoring
  ---------------------------------------------------------------------------------------
  u_common_mem_mux_hdr_dat : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => ceil_log2(field_nof_words(g_hdr_field_arr, c_word_w))
  )
  port map (
    mosi     => reg_hdr_dat_mosi,
    miso     => reg_hdr_dat_miso,
    mosi_arr => reg_hdr_dat_mosi_arr,
    miso_arr => reg_hdr_dat_miso_arr
  );
end str;
