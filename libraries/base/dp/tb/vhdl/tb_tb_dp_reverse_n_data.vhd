-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra, 14 feb 2023
-- Purpose: Verify multiple variations of tb_dp_reverse_n_data
-- Description:
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_reverse_n_data is
end tb_tb_dp_reverse_n_data;

architecture tb of tb_tb_dp_reverse_n_data is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  constant c_rep_act   : natural :=   3;
begin
-- g_pipeline               : NATURAL := 1;  -- 0 for combinatorial, > 0 for registers
-- g_nof_repeat             : NATURAL := 5;
-- g_reverse_len            : NATURAL := 4;
-- g_pkt_gap                : NATURAL := 0

--    g_pipeline
--    |  g_reverse_len
--    |  | g_pkt_gap
--    |  | |
  u_pipe_1_0  : entity work.tb_dp_reverse_n_data generic map(1, c_rep_act, 1, 0);
  u_comb_2_0  : entity work.tb_dp_reverse_n_data generic map(0, c_rep_act, 2, 0);
  u_pipe_2_0  : entity work.tb_dp_reverse_n_data generic map(1, c_rep_act, 2, 0);
  u_comb_3_5  : entity work.tb_dp_reverse_n_data generic map(0, c_rep_act, 3, 5);
  u_pipe_3_5  : entity work.tb_dp_reverse_n_data generic map(1, c_rep_act, 3, 5);
end tb;
