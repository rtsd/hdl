--Legal Notice: (C)2019 Altera Corporation. All rights reserved.  Your
--use of Altera Corporation's design tools, logic functions and other
--software and tools, and its AMPP partner logic functions, and any
--output files any of the foregoing (including device programming or
--simulation files), and any associated documentation or information are
--expressly subject to the terms and conditions of the Altera Program
--License Subscription Agreement or other applicable license agreement,
--including, without limitation, that your use is for the sole purpose
--of programming logic devices manufactured by Altera and sold by Altera
--or its authorized distributors.  Please refer to the applicable
--agreement for further details.

-- turn off superfluous VHDL processor warnings
-- altera message_level Level1
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 13469 16735 16788

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_w is
        port (
              -- inputs:
                 signal clk : in std_logic;
                 signal fifo_wdata : in std_logic_vector(7 downto 0);
                 signal fifo_wr : in std_logic;

              -- outputs:
                 signal fifo_FF : out std_logic;
                 signal r_dat : out std_logic_vector(7 downto 0);
                 signal wfifo_empty : out std_logic;
                 signal wfifo_used : out std_logic_vector(5 downto 0)
              );
end entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_w;

architecture europa of qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_w is
begin
--synthesis translate_off
    process (clk)
    variable write_line : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(fifo_wr) = '1' then
          write(write_line, character'val(CONV_INTEGER(fifo_wdata)));
          write(write_line, string'(""));
          write(output, write_line.all);
          deallocate (write_line);
        end if;
      end if;
    end process;

    wfifo_used <= A_REP(std_logic'('0'), 6);
    r_dat <= A_REP(std_logic'('0'), 8);
    fifo_FF <= std_logic'('0');
    wfifo_empty <= std_logic'('1');
--synthesis translate_on
end europa;

-- turn off superfluous VHDL processor warnings
-- altera message_level Level1
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 13469 16735 16788

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library lpm;
use lpm.all;

entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_w is
        port (
              -- inputs:
                 signal clk : in std_logic;
                 signal fifo_clear : in std_logic;
                 signal fifo_wdata : in std_logic_vector(7 downto 0);
                 signal fifo_wr : in std_logic;
                 signal rd_wfifo : in std_logic;

              -- outputs:
                 signal fifo_FF : out std_logic;
                 signal r_dat : out std_logic_vector(7 downto 0);
                 signal wfifo_empty : out std_logic;
                 signal wfifo_used : out std_logic_vector(5 downto 0)
              );
end entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_w;

architecture europa of qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_w is
--synthesis translate_off
component qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_w is
           port (
                 -- inputs:
                    signal clk : in std_logic;
                    signal fifo_wdata : in std_logic_vector(7 downto 0);
                    signal fifo_wr : in std_logic;

                 -- outputs:
                    signal fifo_FF : out std_logic;
                    signal r_dat : out std_logic_vector(7 downto 0);
                    signal wfifo_empty : out std_logic;
                    signal wfifo_used : out std_logic_vector(5 downto 0)
                 );
end component qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_w;

--synthesis translate_on
--synthesis read_comments_as_HDL on
--  component scfifo is
--GENERIC (
--      lpm_hint : STRING;
--        lpm_numwords : NATURAL;
--        lpm_showahead : STRING;
--        lpm_type : STRING;
--        lpm_width : NATURAL;
--        lpm_widthu : NATURAL;
--        overflow_checking : STRING;
--        underflow_checking : STRING;
--        use_eab : STRING
--      );
--    PORT (
--    signal full : OUT STD_LOGIC;
--        signal q : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
--        signal usedw : OUT STD_LOGIC_VECTOR (5 DOWNTO 0);
--        signal empty : OUT STD_LOGIC;
--        signal rdreq : IN STD_LOGIC;
--        signal aclr : IN STD_LOGIC;
--        signal data : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
--        signal clock : IN STD_LOGIC;
--        signal wrreq : IN STD_LOGIC
--      );
--  end component scfifo;
--synthesis read_comments_as_HDL off
                signal internal_fifo_FF :  std_logic;
                signal internal_r_dat :  std_logic_vector(7 downto 0);
                signal internal_wfifo_empty :  std_logic;
                signal internal_wfifo_used :  std_logic_vector(5 downto 0);
begin
  --vhdl renameroo for output signals
  fifo_FF <= internal_fifo_FF;
  --vhdl renameroo for output signals
  r_dat <= internal_r_dat;
  --vhdl renameroo for output signals
  wfifo_empty <= internal_wfifo_empty;
  --vhdl renameroo for output signals
  wfifo_used <= internal_wfifo_used;
--synthesis translate_off
    --the_qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_w, which is an e_instance
    the_qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_w : qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_w
      port map(
        fifo_FF => internal_fifo_FF,
        r_dat => internal_r_dat,
        wfifo_empty => internal_wfifo_empty,
        wfifo_used => internal_wfifo_used,
        clk => clk,
        fifo_wdata => fifo_wdata,
        fifo_wr => fifo_wr
      );

--synthesis translate_on
--synthesis read_comments_as_HDL on
--    wfifo : scfifo
--      generic map(
--        lpm_hint => "RAM_BLOCK_TYPE=AUTO",
--        lpm_numwords => 64,
--        lpm_showahead => "OFF",
--        lpm_type => "scfifo",
--        lpm_width => 8,
--        lpm_widthu => 6,
--        overflow_checking => "OFF",
--        underflow_checking => "OFF",
--        use_eab => "ON"
--      )
--      port map(
--                aclr => fifo_clear,
--                clock => clk,
--                data => fifo_wdata,
--                empty => internal_wfifo_empty,
--                full => internal_fifo_FF,
--                q => internal_r_dat,
--                rdreq => rd_wfifo,
--                usedw => internal_wfifo_used,
--                wrreq => fifo_wr
--      );
--
--synthesis read_comments_as_HDL off
end europa;

-- turn off superfluous VHDL processor warnings
-- altera message_level Level1
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 13469 16735 16788

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_r is
        port (
              -- inputs:
                 signal clk : in std_logic;
                 signal fifo_rd : in std_logic;
                 signal rst_n : in std_logic;

              -- outputs:
                 signal fifo_EF : out std_logic;
                 signal fifo_rdata : out std_logic_vector(7 downto 0);
                 signal rfifo_full : out std_logic;
                 signal rfifo_used : out std_logic_vector(5 downto 0)
              );
end entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_r;

architecture europa of qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_r is
                signal bytes_left :  std_logic_vector(31 downto 0);
                signal fifo_rd_d :  std_logic;
                signal internal_rfifo_full1 :  std_logic;
                signal new_rom :  std_logic;
                signal num_bytes :  std_logic_vector(31 downto 0);
                signal rfifo_entries :  std_logic_vector(6 downto 0);
begin
  --vhdl renameroo for output signals
  rfifo_full <= internal_rfifo_full1;
--synthesis translate_off
    -- Generate rfifo_entries for simulation
    process (clk, rst_n)
    begin
      if rst_n = '0' then
        bytes_left <= std_logic_vector'("00000000000000000000000000000000");
        fifo_rd_d <= std_logic'('0');
      elsif clk'event and clk = '1' then
        fifo_rd_d <= fifo_rd;
        -- decrement on read
        if std_logic'(fifo_rd_d) = '1' then
          bytes_left <= A_EXT (((std_logic_vector'("0") & (bytes_left)) - (std_logic_vector'("00000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(std_logic'('1'))))), 32);
        end if;
        -- catch new contents
        if std_logic'(new_rom) = '1' then
          bytes_left <= num_bytes;
        end if;
      end if;
    end process;

    fifo_EF <= to_std_logic((bytes_left = std_logic_vector'("00000000000000000000000000000000")));
    internal_rfifo_full1 <= to_std_logic((bytes_left > std_logic_vector'("00000000000000000000000001000000")));
    rfifo_entries <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_rfifo_full1)) = '1'), std_logic_vector'("00000000000000000000000001000000"), bytes_left), 7);
    rfifo_used <= rfifo_entries(5 downto 0);
    new_rom <= std_logic'('0');
    num_bytes <= std_logic_vector'("00000000000000000000000000000000");
    fifo_rdata <= std_logic_vector'("00000000");
--synthesis translate_on
end europa;

-- turn off superfluous VHDL processor warnings
-- altera message_level Level1
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 13469 16735 16788

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library lpm;
use lpm.all;

entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_r is
        port (
              -- inputs:
                 signal clk : in std_logic;
                 signal fifo_clear : in std_logic;
                 signal fifo_rd : in std_logic;
                 signal rst_n : in std_logic;
                 signal t_dat : in std_logic_vector(7 downto 0);
                 signal wr_rfifo : in std_logic;

              -- outputs:
                 signal fifo_EF : out std_logic;
                 signal fifo_rdata : out std_logic_vector(7 downto 0);
                 signal rfifo_full : out std_logic;
                 signal rfifo_used : out std_logic_vector(5 downto 0)
              );
end entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_r;

architecture europa of qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_r is
--synthesis translate_off
component qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_r is
           port (
                 -- inputs:
                    signal clk : in std_logic;
                    signal fifo_rd : in std_logic;
                    signal rst_n : in std_logic;

                 -- outputs:
                    signal fifo_EF : out std_logic;
                    signal fifo_rdata : out std_logic_vector(7 downto 0);
                    signal rfifo_full : out std_logic;
                    signal rfifo_used : out std_logic_vector(5 downto 0)
                 );
end component qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_r;

--synthesis translate_on
--synthesis read_comments_as_HDL on
--  component scfifo is
--GENERIC (
--      lpm_hint : STRING;
--        lpm_numwords : NATURAL;
--        lpm_showahead : STRING;
--        lpm_type : STRING;
--        lpm_width : NATURAL;
--        lpm_widthu : NATURAL;
--        overflow_checking : STRING;
--        underflow_checking : STRING;
--        use_eab : STRING
--      );
--    PORT (
--    signal full : OUT STD_LOGIC;
--        signal q : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
--        signal usedw : OUT STD_LOGIC_VECTOR (5 DOWNTO 0);
--        signal empty : OUT STD_LOGIC;
--        signal rdreq : IN STD_LOGIC;
--        signal aclr : IN STD_LOGIC;
--        signal data : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
--        signal clock : IN STD_LOGIC;
--        signal wrreq : IN STD_LOGIC
--      );
--  end component scfifo;
--synthesis read_comments_as_HDL off
                signal internal_fifo_EF :  std_logic;
                signal internal_fifo_rdata :  std_logic_vector(7 downto 0);
                signal internal_rfifo_full :  std_logic;
                signal internal_rfifo_used :  std_logic_vector(5 downto 0);
begin
  --vhdl renameroo for output signals
  fifo_EF <= internal_fifo_EF;
  --vhdl renameroo for output signals
  fifo_rdata <= internal_fifo_rdata;
  --vhdl renameroo for output signals
  rfifo_full <= internal_rfifo_full;
  --vhdl renameroo for output signals
  rfifo_used <= internal_rfifo_used;
--synthesis translate_off
    --the_qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_r, which is an e_instance
    the_qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_r : qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_sim_scfifo_r
      port map(
        fifo_EF => internal_fifo_EF,
        fifo_rdata => internal_fifo_rdata,
        rfifo_full => internal_rfifo_full,
        rfifo_used => internal_rfifo_used,
        clk => clk,
        fifo_rd => fifo_rd,
        rst_n => rst_n
      );

--synthesis translate_on
--synthesis read_comments_as_HDL on
--    rfifo : scfifo
--      generic map(
--        lpm_hint => "RAM_BLOCK_TYPE=AUTO",
--        lpm_numwords => 64,
--        lpm_showahead => "OFF",
--        lpm_type => "scfifo",
--        lpm_width => 8,
--        lpm_widthu => 6,
--        overflow_checking => "OFF",
--        underflow_checking => "OFF",
--        use_eab => "ON"
--      )
--      port map(
--                aclr => fifo_clear,
--                clock => clk,
--                data => t_dat,
--                empty => internal_fifo_EF,
--                full => internal_rfifo_full,
--                q => internal_fifo_rdata,
--                rdreq => fifo_rd,
--                usedw => internal_rfifo_used,
--                wrreq => wr_rfifo
--      );
--
--synthesis read_comments_as_HDL off
end europa;

-- turn off superfluous VHDL processor warnings
-- altera message_level Level1
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 13469 16735 16788

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library lpm;
use lpm.all;

entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi is
        port (
              -- inputs:
                 signal av_address : in std_logic;
                 signal av_chipselect : in std_logic;
                 signal av_read_n : in std_logic;
                 signal av_write_n : in std_logic;
                 signal av_writedata : in std_logic_vector(31 downto 0);
                 signal clk : in std_logic;
                 signal rst_n : in std_logic;

              -- outputs:
                 signal av_irq : out std_logic;
                 signal av_readdata : out std_logic_vector(31 downto 0);
                 signal av_waitrequest : out std_logic;
                 signal dataavailable : out std_logic;
                 signal readyfordata : out std_logic
              );
attribute ALTERA_ATTRIBUTE : string;
attribute ALTERA_ATTRIBUTE of qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi : entity is "SUPPRESS_DA_RULE_INTERNAL=" "R101,C106,D101,D103" "";
end entity qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi;

architecture europa of qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi is
component qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_w is
           port (
                 -- inputs:
                    signal clk : in std_logic;
                    signal fifo_clear : in std_logic;
                    signal fifo_wdata : in std_logic_vector(7 downto 0);
                    signal fifo_wr : in std_logic;
                    signal rd_wfifo : in std_logic;

                 -- outputs:
                    signal fifo_FF : out std_logic;
                    signal r_dat : out std_logic_vector(7 downto 0);
                    signal wfifo_empty : out std_logic;
                    signal wfifo_used : out std_logic_vector(5 downto 0)
                 );
end component qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_w;

component qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_r is
           port (
                 -- inputs:
                    signal clk : in std_logic;
                    signal fifo_clear : in std_logic;
                    signal fifo_rd : in std_logic;
                    signal rst_n : in std_logic;
                    signal t_dat : in std_logic_vector(7 downto 0);
                    signal wr_rfifo : in std_logic;

                 -- outputs:
                    signal fifo_EF : out std_logic;
                    signal fifo_rdata : out std_logic_vector(7 downto 0);
                    signal rfifo_full : out std_logic;
                    signal rfifo_used : out std_logic_vector(5 downto 0)
                 );
end component qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_r;

--synthesis read_comments_as_HDL on
--  component alt_jtag_atlantic is
--GENERIC (
--      INSTANCE_ID : NATURAL;
--        LOG2_RXFIFO_DEPTH : NATURAL;
--        LOG2_TXFIFO_DEPTH : NATURAL;
--        SLD_AUTO_INSTANCE_INDEX : STRING
--      );
--    PORT (
--    signal t_pause : OUT STD_LOGIC;
--        signal r_ena : OUT STD_LOGIC;
--        signal t_ena : OUT STD_LOGIC;
--        signal t_dat : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
--        signal t_dav : IN STD_LOGIC;
--        signal rst_n : IN STD_LOGIC;
--        signal r_dat : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
--        signal r_val : IN STD_LOGIC;
--        signal clk : IN STD_LOGIC
--      );
--  end component alt_jtag_atlantic;
--synthesis read_comments_as_HDL off
                signal ac :  std_logic;
                signal activity :  std_logic;
                signal fifo_AE :  std_logic;
                signal fifo_AF :  std_logic;
                signal fifo_EF :  std_logic;
                signal fifo_FF :  std_logic;
                signal fifo_clear :  std_logic;
                signal fifo_rd :  std_logic;
                signal fifo_rdata :  std_logic_vector(7 downto 0);
                signal fifo_wdata :  std_logic_vector(7 downto 0);
                signal fifo_wr :  std_logic;
                signal ien_AE :  std_logic;
                signal ien_AF :  std_logic;
                signal internal_av_waitrequest :  std_logic;
                signal ipen_AE :  std_logic;
                signal ipen_AF :  std_logic;
                signal pause_irq :  std_logic;
                signal r_dat :  std_logic_vector(7 downto 0);
                signal r_ena :  std_logic;
                signal r_val :  std_logic;
                signal rd_wfifo :  std_logic;
                signal read_0 :  std_logic;
                signal rfifo_full :  std_logic;
                signal rfifo_used :  std_logic_vector(5 downto 0);
                signal rvalid :  std_logic;
                signal sim_r_ena :  std_logic;
                signal sim_t_dat :  std_logic;
                signal sim_t_ena :  std_logic;
                signal sim_t_pause :  std_logic;
                signal t_dat :  std_logic_vector(7 downto 0);
                signal t_dav :  std_logic;
                signal t_ena :  std_logic;
                signal t_pause :  std_logic;
                signal wfifo_empty :  std_logic;
                signal wfifo_used :  std_logic_vector(5 downto 0);
                signal woverflow :  std_logic;
                signal wr_rfifo :  std_logic;
begin
  --avalon_jtag_slave, which is an e_avalon_slave
  rd_wfifo <= r_ena and not wfifo_empty;
  wr_rfifo <= t_ena and not rfifo_full;
  fifo_clear <= not rst_n;
  --the_qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_w, which is an e_instance
  the_qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_w : qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_w
    port map(
      fifo_FF => fifo_FF,
      r_dat => r_dat,
      wfifo_empty => wfifo_empty,
      wfifo_used => wfifo_used,
      clk => clk,
      fifo_clear => fifo_clear,
      fifo_wdata => fifo_wdata,
      fifo_wr => fifo_wr,
      rd_wfifo => rd_wfifo
    );

  --the_qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_r, which is an e_instance
  the_qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_r : qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_scfifo_r
    port map(
      fifo_EF => fifo_EF,
      fifo_rdata => fifo_rdata,
      rfifo_full => rfifo_full,
      rfifo_used => rfifo_used,
      clk => clk,
      fifo_clear => fifo_clear,
      fifo_rd => fifo_rd,
      rst_n => rst_n,
      t_dat => t_dat,
      wr_rfifo => wr_rfifo
    );

  ipen_AE <= ien_AE and fifo_AE;
  ipen_AF <= ien_AF and ((pause_irq or fifo_AF));
  av_irq <= ipen_AE or ipen_AF;
  activity <= t_pause or t_ena;
  process (clk, rst_n)
  begin
    if rst_n = '0' then
      pause_irq <= std_logic'('0');
    elsif clk'event and clk = '1' then
      -- only if fifo is not empty...
      if std_logic'((t_pause and not fifo_EF)) = '1' then
        pause_irq <= std_logic'('1');
      elsif std_logic'(read_0) = '1' then
        pause_irq <= std_logic'('0');
      end if;
    end if;
  end process;

  process (clk, rst_n)
  begin
    if rst_n = '0' then
      r_val <= std_logic'('0');
      t_dav <= std_logic'('1');
    elsif clk'event and clk = '1' then
      r_val <= r_ena and not wfifo_empty;
      t_dav <= not rfifo_full;
    end if;
  end process;

  process (clk, rst_n)
  begin
    if rst_n = '0' then
      fifo_AE <= std_logic'('0');
      fifo_AF <= std_logic'('0');
      fifo_wr <= std_logic'('0');
      rvalid <= std_logic'('0');
      read_0 <= std_logic'('0');
      ien_AE <= std_logic'('0');
      ien_AF <= std_logic'('0');
      ac <= std_logic'('0');
      woverflow <= std_logic'('0');
      internal_av_waitrequest <= std_logic'('1');
    elsif clk'event and clk = '1' then
      fifo_AE <= to_std_logic(((std_logic_vector'("0000000000000000000000000") & (std_logic_vector'(A_ToStdLogicVector(fifo_FF) & wfifo_used))) <= std_logic_vector'("00000000000000000000000000001000")));
      fifo_AF <= to_std_logic(((std_logic_vector'("000000000000000000000000") & (((std_logic_vector'("01000000") - (std_logic_vector'("0") & (std_logic_vector'(A_ToStdLogicVector(rfifo_full) & rfifo_used))))))) <= std_logic_vector'("00000000000000000000000000001000")));
      fifo_wr <= std_logic'('0');
      read_0 <= std_logic'('0');
      internal_av_waitrequest <= not (((av_chipselect and ((not av_write_n or not av_read_n))) and internal_av_waitrequest));
      if std_logic'(activity) = '1' then
        ac <= std_logic'('1');
      end if;
      -- write
      if std_logic'(((av_chipselect and not av_write_n) and internal_av_waitrequest)) = '1' then
        -- addr 1 is control; addr 0 is data
        if std_logic'(av_address) = '1' then
          ien_AF <= av_writedata(0);
          ien_AE <= av_writedata(1);
          if std_logic'((av_writedata(10) and not activity)) = '1' then
            ac <= std_logic'('0');
          end if;
        else
          fifo_wr <= not fifo_FF;
          woverflow <= fifo_FF;
        end if;
      end if;
      -- read
      if std_logic'(((av_chipselect and not av_read_n) and internal_av_waitrequest)) = '1' then
        -- addr 1 is interrupt; addr 0 is data
        if std_logic'(not av_address) = '1' then
          rvalid <= not fifo_EF;
        end if;
        read_0 <= not av_address;
      end if;
    end if;
  end process;

  fifo_wdata <= av_writedata(7 downto 0);
  fifo_rd <= A_WE_StdLogic((std_logic'(((((av_chipselect and not av_read_n) and internal_av_waitrequest) and not av_address))) = '1'), not fifo_EF, std_logic'('0'));
  av_readdata <= A_EXT (A_WE_StdLogicVector((std_logic'(read_0) = '1'), (std_logic_vector'("0") & ((A_REP(std_logic'('0'), 9) & A_ToStdLogicVector(rfifo_full) & rfifo_used & A_ToStdLogicVector(rvalid) & A_ToStdLogicVector(woverflow) & A_ToStdLogicVector(not fifo_FF) & A_ToStdLogicVector(not fifo_EF) & A_ToStdLogicVector(std_logic'('0')) & A_ToStdLogicVector(ac) & A_ToStdLogicVector(ipen_AE) & A_ToStdLogicVector(ipen_AF) & fifo_rdata))), (A_REP(std_logic'('0'), 9) & ((std_logic_vector'("01000000") - (std_logic_vector'("0") & (std_logic_vector'(A_ToStdLogicVector(fifo_FF) & wfifo_used))))) & A_ToStdLogicVector(rvalid) & A_ToStdLogicVector(woverflow) & A_ToStdLogicVector(not fifo_FF) & A_ToStdLogicVector(not fifo_EF) & A_ToStdLogicVector(std_logic'('0')) & A_ToStdLogicVector(ac) & A_ToStdLogicVector(ipen_AE) & A_ToStdLogicVector(ipen_AF) & A_REP(std_logic'('0'), 6) & A_ToStdLogicVector(ien_AE) & A_ToStdLogicVector(ien_AF))), 32);
  process (clk, rst_n)
  begin
    if rst_n = '0' then
      readyfordata <= std_logic'('0');
    elsif clk'event and clk = '1' then
      readyfordata <= not fifo_FF;
    end if;
  end process;

  --vhdl renameroo for output signals
  av_waitrequest <= internal_av_waitrequest;
--synthesis translate_off
    -- Tie off Atlantic Interface signals not used for simulation
    process (clk)
    begin
      if clk'event and clk = '1' then
        sim_t_pause <= std_logic'('0');
        sim_t_ena <= std_logic'('0');
        sim_t_dat <= Vector_To_Std_Logic(A_WE_StdLogicVector((std_logic'(t_dav) = '1'), r_dat, A_REP(r_val, 8)));
        sim_r_ena <= std_logic'('0');
      end if;
    end process;

    r_ena <= sim_r_ena;
    t_ena <= sim_t_ena;
    t_dat <= std_logic_vector'("0000000") & (A_TOSTDLOGICVECTOR(sim_t_dat));
    t_pause <= sim_t_pause;
    process (fifo_EF)
    begin
        dataavailable <= not fifo_EF;
    end process;

--synthesis translate_on
--synthesis read_comments_as_HDL on
--    qsys_unb2b_minimal_jtag_uart_0_altera_avalon_jtag_uart_180_tj65noi_alt_jtag_atlantic : alt_jtag_atlantic
--      generic map(
--        INSTANCE_ID => 0,
--        LOG2_RXFIFO_DEPTH => 6,
--        LOG2_TXFIFO_DEPTH => 6,
--        SLD_AUTO_INSTANCE_INDEX => "YES"
--      )
--      port map(
--                clk => clk,
--                r_dat => r_dat,
--                r_ena => r_ena,
--                r_val => r_val,
--                rst_n => rst_n,
--                t_dat => t_dat,
--                t_dav => t_dav,
--                t_ena => t_ena,
--                t_pause => t_pause
--      );
--
--    process (clk, rst_n)
--    begin
--      if rst_n = '0' then
--        dataavailable <= std_logic'('0');
--      elsif clk'event and clk = '1' then
--        dataavailable <= NOT fifo_EF;
--      end if;
--
--    end process;
--
--synthesis read_comments_as_HDL off
end europa;
