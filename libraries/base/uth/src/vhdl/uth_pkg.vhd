--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, easics_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

use easics_lib.PCK_CRC8_D8.all;
use easics_lib.PCK_CRC16_D16.all;
use easics_lib.PCK_CRC32_D32.all;
use easics_lib.PCK_CRC64_D64.all;

package uth_pkg is
  --<constants>--
  constant c_uth_data_max_w     : natural := 256;
  constant c_uth_crc64          : natural := 64;  -- internally use CRC-8, 16, 32 or 64, use CRC-64 for data wider than 32 bits
  constant c_uth_nof_overhead   : natural := 4;  -- header: idle, sfd, tlen and tail: crc, so total 4 words frame overhead
  constant c_uth_gap_min        : natural := c_uth_nof_overhead;  -- minimal gap between valid data blocks (to be able to insert the frame overhead into the stream)
  constant c_uth_idle           : std_logic_vector(c_uth_data_max_w - 1 downto 0) := c_slv1( c_uth_data_max_w - 1 downto 0);  -- = 0xF..FFFF
  constant c_uth_preamble       : std_logic_vector(c_uth_data_max_w - 1 downto 0) := c_slv10(c_uth_data_max_w - 1 downto 0);  -- = 0xA..AAAA
  constant c_uth_sfd            : std_logic_vector(c_uth_data_max_w - 1 downto 0) := c_slv10(c_uth_data_max_w - 1 downto 1) & '1';  -- = 0xA..AAAB
  constant c_uth_payload_min    : natural := 1;  -- an empty payload is not allowed because we need at least one data to signal sop and eop

  constant c_uth_crc_ok         : natural := 0;
  constant c_uth_crc_err        : natural := 1;
  constant c_uth_crc_err_w      : natural := 1;  -- one bit CRC status field to fit c_uth_crc_ok=0 or c_uth_crc_err=1

  --<types>--

  --<local functions>--
  function func_uth_parallel_crc(data, crc : std_logic_vector) return std_logic_vector;

  --<global functions>--
  function func_uth_crc_w(c_data_w : natural) return natural;
  function func_uth_next_crc(data, crc : std_logic_vector) return std_logic_vector;
end uth_pkg;

package body uth_pkg is
  --<functions>--
  -- Calculate parallel CRC for wide data
  function func_uth_parallel_crc(data, crc : std_logic_vector) return std_logic_vector is
    constant c_data_w  : natural := data'length;
    constant c_crc_w   : natural := crc'length;
    constant c_nof_crc : natural := ceil_div(c_data_w, c_uth_crc64);
    variable v_data    : std_logic_vector(c_crc_w - 1 downto 0);
    variable v_crc     : std_logic_vector(c_crc_w - 1 downto 0);
  begin
    v_data := RESIZE_UVEC(data, c_crc_w);
    for I in 0 to c_nof_crc - 1 loop
      v_crc((I + 1) * c_uth_crc64 - 1 downto I * c_uth_crc64) := nextCRC64_D64(v_data((I + 1) * c_uth_crc64 - 1 downto I * c_uth_crc64), crc((I + 1) * c_uth_crc64 - 1 downto I * c_uth_crc64));
    end loop;

    return v_crc;
  end func_uth_parallel_crc;

  ------------------------------------------------------------------------------
  -- Calculate CRC width based on input data width
  ------------------------------------------------------------------------------

  function func_uth_crc_w(c_data_w : natural) return natural is
    constant c_nof_crc : natural := ceil_div(c_data_w, c_uth_crc64);
    variable v_crc_w   : natural;
  begin
    -- Support CRC-8, 16, 32 and 64
    case c_data_w is
      when  0        => report "UTH data width must be >= 1" severity FAILURE;
      when  1 to 8   => v_crc_w := 8;  -- use CRC-8
      when  9 to 16  => v_crc_w := 16;  -- use CRC-16
      when 17 to 32  => v_crc_w := 32;  -- use CRC-32
      when 33 to 255 => v_crc_w := 64 * c_nof_crc;  -- for data wider than 33 use one or more CRC-64 in parallel (c_uth_crc64 = 64)
      when others    => report "UTH data width must be <= c_uth_data_max_w = 256" severity FAILURE;
    end case;
    return v_crc_w;
  end func_uth_crc_w;

  ------------------------------------------------------------------------------
  -- Calculate next CRC for this data
  ------------------------------------------------------------------------------

  function func_uth_next_crc(data, crc : std_logic_vector) return std_logic_vector is
    constant c_data_w   : natural := data'length;
    constant c_crc_w    : natural := crc'length;
    constant c_nof_crc  : natural := ceil_div(c_data_w, c_uth_crc64);
    variable nxt_crc    : std_logic_vector(crc'range);
  begin
    case c_data_w is
      when  0        => report "UTH data width must be >= 1" severity FAILURE;
      when  1 to 8   => nxt_crc := nextCRC8_D8(  RESIZE_UVEC(data,  8), crc);  -- CRC-8  = ATM HEC,  polynomial x^(0 1 2 8)
      when  9 to 16  => nxt_crc := nextCRC16_D16(RESIZE_UVEC(data, 16), crc);  -- CRC-16 = USB DATA, polynomial x^(0 2 15 16)
      when 17 to 32  => nxt_crc := nextCRC32_D32(RESIZE_UVEC(data, 32), crc);  -- CRC-32 = Ethernet, polynomial x^(0 1 2 4 5 7 8 10 11 12 16 22 23 26 32)
      when 33 to 255 => nxt_crc := func_uth_parallel_crc(data, crc);  -- CRC-64 = EMCA-182, polynomial x^(0 1 4 7 9 10 12 13 17 19 21 22 23 24 27 29 31 32 33 35 37 38 39 40 45 46 47 52 53 54 55 57 62 64)
      when others    => report "UTH data width must be <= c_uth_data_max_w = 256" severity FAILURE;
    end case;
    return nxt_crc;
  end func_uth_next_crc;
end uth_pkg;
