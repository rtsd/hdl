-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.dp_packetizing_pkg.all;

entity tb_dp_frame_scheduler is
  generic (
    g_dut_verify_mux : boolean := true;  -- when FALSE verify dp_frame_scheduler else verify dp_mux
    g_dut_fifo_rl    : natural := 0  -- setting for dp_fifo_fill in dp_frame_scheduler, fixed    1 for dp_mux via c_dut_fifo_rl
  );
end tb_dp_frame_scheduler;

-- run 12 ms, enough to let mark_in_sof occur at least twice

architecture tb of tb_dp_frame_scheduler is
  constant c_clk_period         : time    := 5 ns;
  constant c_rst_delay          : natural := 4;
  constant c_dut_delay          : natural := 11;

  constant c_dut_fifo_rl        : natural := sel_a_b(g_dut_verify_mux,    1, g_dut_fifo_rl);  -- fixed for dp_mux

  constant c_nof_input          : natural := 2;  -- two input streams, crosslets (x) and beamlets (b)
  constant c_nof_input_w        : natural := ceil_log2(c_nof_input);

  constant c_packet_size_x      : natural := 147;  -- 3/2 *  96 dual pol antennes * 4 word/let / 4 lanes + 3 header
  constant c_packet_size_b      : natural := 375;  -- 3/2 * 248          beamlets * 4 word/let / 4 lanes + 3 header
  constant c_slice_size         : natural := 1024;

  constant c_idle_size_x        : natural := c_slice_size - c_packet_size_x;
  constant c_idle_size_b        : natural := c_slice_size - c_packet_size_b;

  constant c_sfd_x              : std_logic_vector(c_dp_max_w - 1 downto 0) := "0101010101010101010101010101010101010101010101010101010101010100";  -- 0x..5554;
  constant c_sfd_b              : std_logic_vector(c_dp_max_w - 1 downto 0) := "0101010101010101010101010101010101010101010101010101010101010011";  -- 0x..5553;

  constant c_fsn_x              : natural := 16#C000#;  -- Crosslet packet
  constant c_fsn_b              : natural := 16#B000#;  -- Beamlet packet
  constant c_fsn_max            : natural := 16#1000#;  -- Reserve [15:12] for X ID or B ID

  constant c_burst_nof_packets  : natural := 2;  -- nof frames to store to handle SERDES lane bursts
  constant c_x_scheduler_size   : natural := c_burst_nof_packets * c_packet_size_x;  -- = 294;
  constant c_b_scheduler_size   : natural := c_burst_nof_packets * c_packet_size_b;  -- = 750;
  constant c_x_scheduler_fill   : natural := sel_a_b(g_dut_fifo_rl, 0, 1);  -- need to fill > 0 to force use of dp_fifo_fill with the RL adapter
  constant c_b_scheduler_fill   : natural := sel_a_b(g_dut_fifo_rl, 0, 1);  -- when fill=0 then the dp_fifo_fill used fixed RL=1

  constant c_lane_dat_w         : natural := 16;  -- = c_rsp_lane_dat_w = 16

  signal rst_dut         : std_logic := '1';
  signal rst             : std_logic := '1';
  signal clk             : std_logic := '0';

  signal scheduler_dat   : std_logic_vector(c_nof_input * c_lane_dat_w - 1 downto 0);
  signal scheduler_val   : std_logic_vector(c_nof_input             - 1 downto 0);
  signal scheduler_sof   : std_logic_vector(c_nof_input             - 1 downto 0);
  signal scheduler_eof   : std_logic_vector(c_nof_input             - 1 downto 0);
  signal scheduler_sosi  : t_dp_sosi_arr(0 to c_nof_input - 1);

  signal lane_tx_xdat    : std_logic_vector(c_lane_dat_w - 1 downto 0);
  signal lane_tx_xval    : std_logic;
  signal lane_tx_xsof    : std_logic;
  signal lane_tx_xeof    : std_logic;

  signal lane_tx_bdat    : std_logic_vector(c_lane_dat_w - 1 downto 0);
  signal lane_tx_bval    : std_logic;
  signal lane_tx_bsof    : std_logic;
  signal lane_tx_beof    : std_logic;

  signal lane_tx_dat     : std_logic_vector(c_lane_dat_w - 1 downto 0);
  signal lane_tx_val     : std_logic;
  signal lane_tx_sof     : std_logic;
  signal lane_tx_eof     : std_logic;
  signal lane_tx_channel : std_logic_vector(c_nof_input_w - 1 downto 0);
  signal lane_tx_sosi    : t_dp_sosi;

  signal fsn_x           : natural := c_fsn_x;  -- Init crosslet packet count with crosslet ID
  signal fsn_b           : natural := c_fsn_b;  -- Init beamlet  packet count with beamlet  ID

  signal mark_slice_sof  : std_logic := '0';
  signal mark_in_sof     : std_logic := '0';

  signal mark_out_sof         : std_logic := '0';
  signal mark_out_sof_hld     : std_logic := '0';
  signal mark_out_sof_hld_d   : std_logic := '0';
  signal mark_out_sfd         : std_logic := '0';
  signal mark_out_fsn         : std_logic := '0';
  signal mark_out_eof         : std_logic := '0';
  signal mark_out_data        : std_logic := '0';
  signal mark_out_frame       : std_logic := '0';

  signal mark_out_sfd_x       : std_logic := '0';
  signal mark_out_sfd_x_hld   : std_logic := '0';
  signal mark_out_sfd_x_hld_d : std_logic := '0';
  signal mark_out_fsn_x       : std_logic := '0';
  signal mark_out_fsn_x_hld   : std_logic := '0';
  signal mark_out_fsn_x_hld_d : std_logic := '0';
  signal mark_out_data_x_0    : std_logic := '0';
  signal mark_out_eof_x       : std_logic := '0';
  signal mark_out_eof_x_d     : std_logic := '0';
  signal mark_out_data_x      : std_logic := '0';

  signal mark_out_sfd_b       : std_logic := '0';
  signal mark_out_sfd_b_hld   : std_logic := '0';
  signal mark_out_sfd_b_hld_d : std_logic := '0';
  signal mark_out_fsn_b       : std_logic := '0';
  signal mark_out_fsn_b_hld   : std_logic := '0';
  signal mark_out_fsn_b_hld_d : std_logic := '0';
  signal mark_out_data_b_0    : std_logic := '0';
  signal mark_out_eof_b       : std_logic := '0';
  signal mark_out_eof_b_d     : std_logic := '0';
  signal mark_out_data_b      : std_logic := '0';

  signal expected_dat    : natural;
  signal expected_fsn_x  : natural;
  signal expected_fsn_b  : natural;

  procedure proc_tx_packet(constant c_packet_size : in    natural;
                           constant c_sfd         : in    std_logic_vector;
                           constant c_fsn_word    : in    natural;
                           signal   rst           : in    std_logic;
                           signal   clk           : in    std_logic;
                           signal   out_sof       : out   std_logic;
                           signal   out_eof       : out   std_logic;
                           signal   out_val       : out   std_logic;
                           signal   out_dat       : inout std_logic_vector(c_lane_dat_w - 1 downto 0)) is
  begin
    out_sof <= '0';
    out_eof <= '0';
    out_val <= '0';
    out_dat <= (others => '0');
    if rst = '1' then
      wait until rising_edge(clk);
    else
      -- Transmit a packet (conform dp_frame_tx)
      out_sof <= '1';
      out_val <= '1';
      out_dat <= c_dp_idle(out_dat'range);
      wait until rising_edge(clk);  -- start frame with idle word
      out_sof <= '0';
      out_dat <= c_sfd(out_dat'range);
      wait until rising_edge(clk);  -- then the frame delimiter word
      out_dat <= std_logic_vector(to_unsigned(c_fsn_word, out_dat'length));
      wait until rising_edge(clk);  -- then the frame sequence number
      out_dat <= std_logic_vector(to_unsigned(0, out_dat'length));
      wait until rising_edge(clk);  -- then the first counter data word (= 0)
      for I in 4 to c_packet_size-2 loop
        out_dat <= std_logic_vector(unsigned(out_dat) + 1);
        wait until rising_edge(clk);  -- then loop from 4 because idle, sfd, fsn and first data word have been issued already
      end loop;  -- and loop until 1 but last to be able to issue the end of frame
      out_eof <= '1';
      out_dat <= std_logic_vector(unsigned(out_dat) + 1);
      wait until rising_edge(clk);  -- end frame with last counter data word
      out_val <= '0';
      out_eof <= '0';
    end if;
  end proc_tx_packet;

  procedure proc_idle(constant c_idle_size : in natural;
                      signal   rst         : in std_logic;
                      signal   clk         : in std_logic) is
  begin
    if rst = '1' then
      wait until rising_edge(clk);
    else
      for I in 0 to c_idle_size-1 loop
        wait until rising_edge(clk);
      end loop;
    end if;
  end proc_idle;

  procedure proc_cnt(constant c_max : in    natural;
                     signal   rst   : in    std_logic;
                     signal   cnt   : inout natural) is
  begin
    if rst = '0' then
      cnt <= cnt + 1;  -- increment packet counter
      if cnt = c_max - 1 then
        cnt <= 0;
      end if;
    end if;
  end proc_cnt;
begin
  ------------------------------------------------------------------------------
  -- Clock, reset and sync
  ------------------------------------------------------------------------------

  clk <= not clk after c_clk_period / 2;

  p_reset : process
  begin
    rst_dut <= '1';
    rst     <= '1';
    for I in 0 to c_rst_delay loop wait until rising_edge(clk); end loop;
    rst_dut <= '0';
    for I in 0 to c_dut_delay loop wait until rising_edge(clk); end loop;
    rst <= '0';
    wait;
  end process;

  p_slice_sof : process
  begin
    if rst = '1' then
      mark_slice_sof <= '0';
      wait until rising_edge(clk);
    else
      mark_slice_sof <= '1';
      wait until rising_edge(clk);
      mark_slice_sof <= '0';
      for I in 1 to c_slice_size-1 loop wait until rising_edge(clk); end loop;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Generate crosslet and beamlet packets, about one per slice
  ------------------------------------------------------------------------------

  -- Slide the crosslet frames along the slice interval and keep the beamlet
  -- frames fixed in the slice interval, so that after c_slice_size nof frames
  -- all relative phases between the crosslet and beamlet frames have occured.

  p_tx_crosslets : process
  begin
    -- Send a crosslet packets
    proc_tx_packet(c_packet_size_x,
                   c_sfd_x,
                   fsn_x,
                   rst,
                   clk,
                   lane_tx_xsof,
                   lane_tx_xeof,
                   lane_tx_xval,
                   lane_tx_xdat);
    proc_cnt(c_fsn_max, rst, fsn_x);
    proc_idle(c_idle_size_x - 1,  -- min 1 to tx one cycle earlier in every slice
              rst,
              clk);
  end process;

  p_tx_beamlets : process
  begin
    -- Send a beamlet packet at start of every slice
    proc_tx_packet(c_packet_size_b,
                   c_sfd_b,
                   fsn_b,
                   rst,
                   clk,
                   lane_tx_bsof,
                   lane_tx_beof,
                   lane_tx_bval,
                   lane_tx_bdat);
    proc_cnt(c_fsn_max, rst, fsn_b);
    proc_idle(c_idle_size_b,  -- tx at start of every slice
              rst,
              clk);
  end process;

  ------------------------------------------------------------------------------
  -- DUT : The dp_frame_scheduler
  ------------------------------------------------------------------------------

  scheduler_dat <= lane_tx_xdat & lane_tx_bdat;
  scheduler_val <= lane_tx_xval & lane_tx_bval;
  scheduler_sof <= lane_tx_xsof & lane_tx_bsof;
  scheduler_eof <= lane_tx_xeof & lane_tx_beof;

  gen_scheduler : if g_dut_verify_mux = false generate
    dut : entity work.dp_frame_scheduler
    generic map (
      g_dat_w      => c_lane_dat_w,
      g_nof_input  => c_nof_input,
      g_fifo_rl    => c_dut_fifo_rl,
      g_fifo_size  => (c_x_scheduler_size, c_b_scheduler_size),  -- 1 DOWNTO 0
      g_fifo_fill  => (c_x_scheduler_fill, c_b_scheduler_fill)  -- 1 DOWNTO 0
    )
    port map (
      rst        => rst_dut,
      clk        => clk,
      in_dat     => scheduler_dat,
      in_val     => scheduler_val,
      in_sof     => scheduler_sof,
      in_eof     => scheduler_eof,
      out_dat    => lane_tx_dat,
      out_val    => lane_tx_val,
      out_sof    => lane_tx_sof,
      out_eof    => lane_tx_eof
    );
  end generate;

  gen_mux : if g_dut_verify_mux = true generate
    p_scheduler_sosi : process(scheduler_dat, scheduler_val, scheduler_sof, scheduler_eof)
    begin
      for I in 0 to c_nof_input - 1 loop
        scheduler_sosi(I).data(c_lane_dat_w - 1 downto 0) <= scheduler_dat((I + 1) * c_lane_dat_w - 1 downto I * c_lane_dat_w);
        scheduler_sosi(I).valid                         <= scheduler_val(I);
        scheduler_sosi(I).sop                           <= scheduler_sof(I);
        scheduler_sosi(I).eop                           <= scheduler_eof(I);
      end loop;
    end process;

    lane_tx_dat     <= lane_tx_sosi.data(c_lane_dat_w - 1 downto 0);
    lane_tx_val     <= lane_tx_sosi.valid;
    lane_tx_sof     <= lane_tx_sosi.sop;
    lane_tx_eof     <= lane_tx_sosi.eop;
    lane_tx_channel <= lane_tx_sosi.channel(c_nof_input_w - 1 downto 0);

    dut : entity work.dp_mux
    generic map (
      g_data_w          => c_lane_dat_w,
      g_empty_w         => 1,  -- not used
      g_in_channel_w    => 1,  -- not used
      g_error_w         => 1,  -- not used
      g_use_empty       => false,
      g_use_in_channel  => false,
      g_use_error       => false,
      g_nof_input       => c_nof_input,
      g_use_fifo        => true,
      g_fifo_size       => (c_b_scheduler_size, c_x_scheduler_size),  -- 0 TO 1
      g_fifo_fill       => (c_b_scheduler_fill, c_x_scheduler_fill)  -- 0 TO 1
    )
    port map (
      rst         => rst_dut,
      clk         => clk,
      -- ST sinks
      snk_out_arr => OPEN,  -- OUT = request to upstream ST source
      snk_in_arr  => scheduler_sosi,
      -- ST source
      src_in      => c_dp_siso_rdy,  -- IN  = request from downstream ST sink
      src_out     => lane_tx_sosi
    );
  end generate;

  ------------------------------------------------------------------------------
  -- Useful auxiliary signals
  ------------------------------------------------------------------------------

  -- DUT input
  mark_in_sof <= lane_tx_xsof and lane_tx_bsof;  -- signal when crosslet and beamlet frame are in phase

  -- DUT output

  -- For c_dut_fifo_rl=1 it is not necessary to check lane_tx_val, because then the data remains valid during a frame.
  -- For c_dut_fifo_rl=0 there does occur an invalid data cycle at the beginning of the frame due to the RL adapter,
  -- therefor it is necessary to check lane_tx_val by means of the mark_out_**_hld signals.

  mark_out_sof       <= lane_tx_sof;
  mark_out_sof_hld   <= '1' when mark_out_sof = '1' else '0' when lane_tx_val = '1';
  mark_out_sof_hld_d <= mark_out_sof_hld when rising_edge(clk);

  -- mark the SFD
  mark_out_sfd         <= mark_out_sof_hld_d and lane_tx_val;
  mark_out_sfd_x       <= '1' when lane_tx_dat = c_sfd_x(lane_tx_dat'range) and mark_out_sfd = '1' else '0';
  mark_out_sfd_x_hld   <= '1' when mark_out_sfd_x = '1' else '0' when lane_tx_val = '1';
  mark_out_sfd_x_hld_d <= mark_out_sfd_x_hld when rising_edge(clk);
  mark_out_sfd_b       <= '1' when lane_tx_dat = c_sfd_b(lane_tx_dat'range) and mark_out_sfd = '1' else '0';
  mark_out_sfd_b_hld   <= '1' when mark_out_sfd_b = '1' else '0' when lane_tx_val = '1';
  mark_out_sfd_b_hld_d <= mark_out_sfd_b_hld when rising_edge(clk);

  -- mark the FSN
  mark_out_fsn_x       <= mark_out_sfd_x_hld_d and lane_tx_val;
  mark_out_fsn_x_hld   <= '1' when mark_out_fsn_x = '1' else '0' when lane_tx_val = '1';
  mark_out_fsn_x_hld_d <= mark_out_fsn_x_hld when rising_edge(clk);
  mark_out_fsn_b       <= mark_out_sfd_b_hld_d and lane_tx_val;
  mark_out_fsn_b_hld   <= '1' when mark_out_fsn_b = '1' else '0' when lane_tx_val = '1';
  mark_out_fsn_b_hld_d <= mark_out_fsn_b_hld when rising_edge(clk);
  mark_out_fsn         <= mark_out_fsn_x or mark_out_fsn_b;

  -- mark the DATA[0..n-1-3]
  mark_out_data_x_0 <= mark_out_fsn_x_hld_d and lane_tx_val;
  mark_out_data_b_0 <= mark_out_fsn_b_hld_d and lane_tx_val;

  mark_out_eof_x <= '1' when unsigned(lane_tx_dat) = c_packet_size_x - 1 - 3 and mark_out_data_x = '1' and lane_tx_val = '1' else '0';  -- data 0..n-1 -3 to account for: idle, sfd, fsn
  mark_out_eof_b <= '1' when unsigned(lane_tx_dat) = c_packet_size_b - 1 - 3 and mark_out_data_b = '1' and lane_tx_val = '1' else '0';  -- data 0..n-1 -3 to account for: idle, sfd, fsn
  mark_out_eof   <= mark_out_eof_x or mark_out_eof_b;

  mark_out_eof_x_d <= mark_out_eof_x when rising_edge(clk);
  mark_out_eof_b_d <= mark_out_eof_b when rising_edge(clk);

  mark_out_data_x <= '1' when mark_out_data_x_0 = '1' else '0' when mark_out_eof_x_d = '1';
  mark_out_data_b <= '1' when mark_out_data_b_0 = '1' else '0' when mark_out_eof_b_d = '1';
  mark_out_data <= mark_out_data_x or mark_out_data_b;

  -- mark all frames
  mark_out_frame <= mark_out_sof or mark_out_sfd or mark_out_fsn or mark_out_data;

  ------------------------------------------------------------------------------
  -- Verify DUT output
  ------------------------------------------------------------------------------

  p_verify_out_dat : process(rst, clk)
  begin
    if rst = '1' then
      expected_dat <= 0;
    elsif rising_edge(clk) then
      expected_dat <= 0;
      if mark_out_data = '1' then
        if unsigned(lane_tx_dat) /= expected_dat then
          report "Scheduler out_dat error"
            severity FAILURE;
        end if;
        expected_dat <= expected_dat + 1;
      end if;
    end if;
  end process;

  p_verify_out_fsn : process(rst, clk)
  begin
    if rst = '1' then
      expected_fsn_x <= c_fsn_x;
      expected_fsn_b <= c_fsn_b;
    elsif rising_edge(clk) then
      if mark_out_fsn_x = '1' then
        if unsigned(lane_tx_dat) /= expected_fsn_x then
          report "Scheduler crosslet out_fsn error"
            severity FAILURE;
        end if;
        proc_cnt(c_fsn_max, rst, expected_fsn_x);  -- increment for next packet
      end if;
      if mark_out_fsn_b = '1' then
        if unsigned(lane_tx_dat) /= expected_fsn_b then
          report "Scheduler beamlet out_fsn error"
            severity FAILURE;
        end if;
        proc_cnt(c_fsn_max, rst, expected_fsn_b);  -- increment for next packet
      end if;
    end if;
  end process;

  p_verify_out_val : process(rst, clk)
  begin
    if rising_edge(clk) then
      if mark_out_frame = '1' then
        if lane_tx_val /= '1' then
          if c_dut_fifo_rl = 1 then
            report "Scheduler out_val missing error"
              severity FAILURE;
          end if;
        end if;
      else
        if lane_tx_val /= '0' then
          report "Scheduler out_val unexpected error"
            severity FAILURE;
        end if;
      end if;
    end if;
  end process;

  p_verify_out_eof : process(rst, clk)
  begin
    if rising_edge(clk) then
      if mark_out_eof = '1' then
        if lane_tx_eof /= '1' then
          report "Scheduler out_eof missing error"
            severity FAILURE;
        end if;
      else
        if lane_tx_eof /= '0' then
          report "Scheduler out_eof unexpected error"
            severity FAILURE;
        end if;
      end if;
    end if;
  end process;
end tb;
