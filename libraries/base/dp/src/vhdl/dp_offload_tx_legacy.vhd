-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, work;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_offload_tx_legacy is
  generic (
    g_nof_streams             : natural := 1;
    g_data_w                  : natural;
    g_block_size              : natural := 256;  -- The number of words per input block
    g_block_nof_sel_words     : natural := 20;  -- The maximum number of words we select from each input block.
    g_nof_words_per_pkt       : natural := 360;  -- The maximum number of words per output Tx packet
    g_hdr_nof_words           : natural;  -- The number of words (of width g_data_w) to be added as header for each TX packet, e.g. c_eth_total_header_nof_words
    g_use_complex             : boolean := false;  -- TRUE uses re(0..g_data_w/2 -1) & im(0..g_data_w/2-1) as input instead of data(0..g_data_w-1).
    g_use_input_fifo          : boolean := false;  -- TRUE instantiates a shallow input FIFO, e.g. in case source has no flow control
    g_use_output_fifo         : boolean := true;  -- TRUE to instantiate an output FIFO that buffers an entire packet
    g_input_fifo_bsn_w        : natural := c_dp_stream_bsn_w;
    g_input_fifo_empty_w      : natural := c_dp_stream_empty_w;
    g_input_fifo_channel_w    : natural := c_dp_stream_channel_w;
    g_input_fifo_error_w      : natural := c_dp_stream_error_w;
    g_input_fifo_use_bsn      : boolean := true;
    g_input_fifo_use_empty    : boolean := true;
    g_input_fifo_use_channel  : boolean := true;
    g_input_fifo_use_error    : boolean := true;
    g_input_fifo_use_sync     : boolean := true
  );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    st_rst                   : in  std_logic;
    st_clk                   : in  std_logic;

    -- MM registers
    reg_hdr_insert_mosi      : in  t_mem_mosi;
    ram_hdr_insert_mosi      : in  t_mem_mosi;

    reg_dp_split_mosi        : in  t_mem_mosi;
    reg_dp_split_miso        : out t_mem_miso;

    reg_dp_pkt_merge_mosi    : in  t_mem_mosi;
    reg_dp_pkt_merge_miso    : out t_mem_miso;

    dp_sosi_arr              : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    dp_siso_arr              : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    tx_sosi_arr              : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    tx_siso_arr              : in  t_dp_siso_arr(g_nof_streams - 1 downto 0)
  );
end dp_offload_tx_legacy;

architecture str of dp_offload_tx_legacy is
  constant c_fifo_margin               : natural := 10;
  constant c_dp_pkt_overhead_nof_words : natural := 4 + 1;

  constant c_hdr_insert_reg_addr_w     : natural := 1;  -- Only 1 register used. A width of 1 still yields 2 addresses/instance though.
  constant c_hdr_insert_ram_addr_w     : natural := ceil_log2( g_hdr_nof_words * (g_data_w / c_word_w) );

  -- In case of an uninterruptable stream, the dp_hdr_insert's ready deassertion (during header insertion)
  -- has to be compensated with this input FIFO buffer:
  constant c_input_fifo_size    : natural := g_hdr_nof_words + c_dp_pkt_overhead_nof_words + c_fifo_margin;

  constant c_output_fifo_fill   : natural := g_hdr_nof_words + c_dp_pkt_overhead_nof_words + g_nof_words_per_pkt;
  constant c_output_fifo_size   : natural := c_output_fifo_fill + c_fifo_margin;

  signal dp_sel_sosi_arr              : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal dp_to_split_sosi_arr         : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_to_split_siso_arr         : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_from_split_sosi_2arr      : t_dp_sosi_2arr_2(g_nof_streams - 1 downto 0);
  signal dp_from_split_siso_2arr      : t_dp_siso_2arr_2(g_nof_streams - 1 downto 0);

  signal dp_to_pkt_merge_sosi_arr     : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_to_pkt_merge_siso_arr     : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal dp_merged_sosi_arr           : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_merged_siso_arr           : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal udp_tx_pkt_sosi_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal udp_tx_pkt_siso_arr          : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal udp_tx_hdr_pkt_sosi_arr      : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal udp_tx_hdr_pkt_siso_arr      : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal udp_tx_hdr_pkt_fifo_sosi_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal udp_tx_hdr_pkt_fifo_siso_arr : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal reg_hdr_insert_mosi_arr      : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal ram_hdr_insert_mosi_arr      : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
begin
  u_common_mem_mux_hdr_reg : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_hdr_insert_reg_addr_w
  )
  port map (
    mosi     => reg_hdr_insert_mosi,
    mosi_arr => reg_hdr_insert_mosi_arr
  );

  u_common_mem_mux_hdr_ram : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_hdr_insert_ram_addr_w
  )
  port map (
    mosi     => ram_hdr_insert_mosi,
    mosi_arr => ram_hdr_insert_mosi_arr
  );

  gen_nof_streams0: for i in 0 to g_nof_streams - 1 generate
    ---------------------------------------------------------------------------------------
    -- Select complex input if required
    ---------------------------------------------------------------------------------------
    gen_complex_in : if g_use_complex = true generate
      p_connect : process(dp_sosi_arr(i))
      begin
        dp_sel_sosi_arr(i) <= dp_sosi_arr(i);
        dp_sel_sosi_arr(i).data(g_data_w - 1 downto 0) <= dp_sosi_arr(i).re(g_data_w / 2 - 1 downto 0) & dp_sosi_arr(i).im(g_data_w / 2 - 1 downto 0);
      end process;
    end generate;

    gen_data_in : if g_use_complex = false generate
      dp_sel_sosi_arr(i) <= dp_sosi_arr(i);
    end generate;

    ---------------------------------------------------------------------------------------
    -- Use a FIFO to buffer only 1 word
    -- Required as dp_split needs to deassert its ready signal periodically
    ---------------------------------------------------------------------------------------
    gen_input_buffer : if g_use_input_fifo = true generate
      u_buf : entity work.dp_fifo_sc
      generic map (
        g_data_w      => g_data_w,
        g_bsn_w       => g_input_fifo_bsn_w,
        g_empty_w     => g_input_fifo_empty_w,
        g_channel_w   => g_input_fifo_channel_w,
        g_error_w     => g_input_fifo_error_w,
        g_use_bsn     => g_input_fifo_use_bsn,
        g_use_empty   => g_input_fifo_use_empty,
        g_use_channel => g_input_fifo_use_channel,
        g_use_error   => g_input_fifo_use_error,
        g_use_sync    => g_input_fifo_use_sync,
        g_use_ctrl    => true,
        g_fifo_size   => 10  -- Use 10 as there's a FIFO margin
      )
      port map (
        rst         => st_rst,
        clk         => st_clk,

        snk_out     => dp_siso_arr(i),
        snk_in      => dp_sel_sosi_arr(i),

        src_in      => dp_to_split_siso_arr(i),
        src_out     => dp_to_split_sosi_arr(i)
      );
    end generate;

  end generate;

  ---------------------------------------------------------------------------------------
  -- Throw away words 0..g_block_nof_sel_words-1 of block (0..g_block_size-1)
  ---------------------------------------------------------------------------------------
  u_mms_dp_split : entity work.mms_dp_split
  generic map (
    g_nof_streams     => g_nof_streams,
    g_data_w          => g_data_w,
    g_symbol_w        => g_data_w,
    g_nof_symbols_max => g_block_nof_sel_words * (g_data_w / c_byte_w)
  )
  port map (
    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    dp_rst       => st_rst,
    dp_clk       => st_clk,

    snk_out_arr  => dp_to_split_siso_arr,
    snk_in_arr   => dp_to_split_sosi_arr,

    src_in_2arr  => dp_from_split_siso_2arr,
    src_out_2arr => dp_from_split_sosi_2arr,

    reg_mosi     => reg_dp_split_mosi,
    reg_miso     => reg_dp_split_miso
  );

  gen_nof_streams1: for i in 0 to g_nof_streams - 1 generate
    dp_from_split_siso_2arr(i)(0) <= c_dp_siso_rdy;
  end generate;

  gen_nof_streams2: for i in 0 to g_nof_streams - 1 generate
    ---------------------------------------------------------------------------------------
    -- Use a FIFO when source is uninterrupable: we need to buffer the input during packet
    -- encoding and header insertion
    ---------------------------------------------------------------------------------------
    gen_input_fifo : if g_use_input_fifo = true generate
      u_input_fifo : entity work.dp_fifo_sc
      generic map (
        g_data_w      => g_data_w,
        g_bsn_w       => g_input_fifo_bsn_w,
        g_empty_w     => g_input_fifo_empty_w,
        g_channel_w   => g_input_fifo_channel_w,
        g_error_w     => g_input_fifo_error_w,
        g_use_bsn     => g_input_fifo_use_bsn,
        g_use_empty   => g_input_fifo_use_empty,
        g_use_channel => g_input_fifo_use_channel,
        g_use_error   => g_input_fifo_use_error,
        g_use_sync    => g_input_fifo_use_sync,
        g_use_ctrl    => true,
        g_fifo_size   => c_input_fifo_size
      )
      port map (
        rst         => st_rst,
        clk         => st_clk,

        snk_out     => dp_from_split_siso_2arr(i)(1),
        snk_in      => dp_from_split_sosi_2arr(i)(1),

        src_in      => dp_to_pkt_merge_siso_arr(i),
        src_out     => dp_to_pkt_merge_sosi_arr(i)
      );
    end generate;

    no_input_fifo : if g_use_input_fifo = false generate
      dp_to_pkt_merge_sosi_arr(i) <= dp_from_split_sosi_2arr(i)(1);
      dp_siso_arr(i)    <= dp_to_pkt_merge_siso_arr(i);
    end generate;

  end generate;

  ---------------------------------------------------------------------------------------
  -- The packet rate has to be brought down to what the targeted sink can handle.
  -- (e.g. PC; ~80k per second max.)
  -- G_nof_pkt packets are merged into 1.
  ---------------------------------------------------------------------------------------
  u_dp_packet_merge : entity work.mms_dp_packet_merge
  generic map (
    g_nof_streams => g_nof_streams,
    g_nof_pkt     => g_nof_words_per_pkt  -- Support merging 360*1 word
  )
  port map (
    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    dp_rst       => st_rst,
    dp_clk       => st_clk,

    snk_out_arr  => dp_to_pkt_merge_siso_arr,
    snk_in_arr   => dp_to_pkt_merge_sosi_arr,

    src_in_arr   => dp_merged_siso_arr,
    src_out_arr  => dp_merged_sosi_arr,

    reg_mosi     => reg_dp_pkt_merge_mosi,
    reg_miso     => reg_dp_pkt_merge_miso
  );

  gen_nof_streams3: for i in 0 to g_nof_streams - 1 generate
    ---------------------------------------------------------------------------------------
    -- Convert DP to packetized DP
    ---------------------------------------------------------------------------------------
    u_dp_packet_enc : entity work.dp_packet_enc
    generic map (
      g_data_w => g_data_w
    )
    port map (
      rst       => st_rst,
      clk       => st_clk,

      snk_out   => dp_merged_siso_arr(i),
      snk_in    => dp_merged_sosi_arr(i),

      src_in    => udp_tx_pkt_siso_arr(i),
      src_out   => udp_tx_pkt_sosi_arr(i)
    );

    ---------------------------------------------------------------------------------------
    -- Add header to DP packet
    ---------------------------------------------------------------------------------------
    u_hdr_insert : entity work.dp_hdr_insert
    generic map (
      g_data_w        => g_data_w,
      g_symbol_w      => c_byte_w,
      g_hdr_nof_words => g_hdr_nof_words
    )
    port map (
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,

      st_rst      => st_rst,
      st_clk      => st_clk,

      reg_mosi    => reg_hdr_insert_mosi_arr(i),
      ram_mosi    => ram_hdr_insert_mosi_arr(i),

      snk_out     => udp_tx_pkt_siso_arr(i),
      snk_in      => udp_tx_pkt_sosi_arr(i),

      src_in      => udp_tx_hdr_pkt_siso_arr(i),
      src_out     => udp_tx_hdr_pkt_sosi_arr(i)
    );

    ---------------------------------------------------------------------------------------
    -- FIFO so we can deliver packets to the ETH module fast enough
    ---------------------------------------------------------------------------------------
    gen_output_fifo: if g_use_output_fifo = true generate
      u_dp_fifo_fill : entity work.dp_fifo_fill
      generic map (
        g_data_w      => g_data_w,
        g_bsn_w       => 0,
        g_empty_w     => 0,
        g_channel_w   => 0,
        g_error_w     => 0,
        g_use_bsn     => false,  -- Don't forward these as all have been encoded by dp_packet_enc
        g_use_empty   => false,
        g_use_channel => false,
        g_use_error   => false,
        g_use_sync    => false,
        g_fifo_fill   => c_output_fifo_fill,  -- Release packet only when available
        g_fifo_size   => c_output_fifo_size,
        g_fifo_rl     => 1
      )
      port map (
        rst         => st_rst,
        clk         => st_clk,

        snk_out     => udp_tx_hdr_pkt_siso_arr(i),
        snk_in      => udp_tx_hdr_pkt_sosi_arr(i),

        src_in      => udp_tx_hdr_pkt_fifo_siso_arr(i),
        src_out     => udp_tx_hdr_pkt_fifo_sosi_arr(i)
      );

      udp_tx_hdr_pkt_fifo_siso_arr(i) <= tx_siso_arr(i);
      tx_sosi_arr(i) <= udp_tx_hdr_pkt_fifo_sosi_arr(i);
    end generate;

    gen_no_output_fifo: if g_use_output_fifo = false generate
      udp_tx_hdr_pkt_siso_arr(i) <= tx_siso_arr(i);
      tx_sosi_arr(i) <= udp_tx_hdr_pkt_sosi_arr(i);
    end generate;

  end generate;
end str;
