#! /usr/bin/env python3
###############################################################################
#
# Copyright 2022
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

###############################################################################

# Author: Eric Kooistra
# Date: jan 2023
# Purpose:
#   Try wrapping in a summator
# Description:
#   Usage:
#   > python3 try_wrap.py -N 10
#

import argparse
import textwrap

import random

import common as cm

# Parse arguments to derive user parameters
_parser = argparse.ArgumentParser(
    description="".join(textwrap.dedent("""\
        The int_wrap function keeps w LSbits and remove MSbits from an integer,
        so that the result is in range range -2**(w-1) to + 2**(w-1)-1. This
        try_wrap.py show that wrap(sum) = wrap(sum(wrap)), so intermediate
        wrapping in a summator does not change the final sum.

        The wrap function is distributive, similar to modulo [1]:

            (a + b) mod n = [(a mod n) + (b mod n)] mod n
                 ab mod n = [(a mod n)(b mod n)] mod n

        > python try_wrap.py -N 10 --w_inp 10 --w_sum 4

        References:
        [1] https://en.wikipedia.org/wiki/Modulo_operation
        \n""")),
    formatter_class=argparse.RawTextHelpFormatter)
_parser.add_argument('-N', default=1000, type=int, help='Number of input samples')
_parser.add_argument('--w_inp', default=10, type=int, help='Number bits of input samples')
_parser.add_argument('--w_sum', default=4, type=int, help='Number bits of output sum')
args = _parser.parse_args()

N_samples = args.N
W_input = args.w_inp
W_sum = args.w_sum

lo = -1 * 2**(W_input - 1)
hi = 2**(W_input - 1) - 1
x = [random.randint(lo, hi) for i in range(N_samples)]
x_wrap = [cm.int_wrap(i, W_sum) for i in x]
x_wrap_sum_wrap = cm.int_wrap(cm.add_list_elements(x_wrap), W_sum)
x_wrap_sum      = cm.int_wrap(cm.add_list_elements(x), W_sum)

print(x)
print()
print(x_wrap)
print()

if x_wrap_sum_wrap == x_wrap_sum:
    print('OK')
else:
    print('Error:')
    print(x_wrap_sum_wrap)
    print(x_wrap_sum)
