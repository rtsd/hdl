-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Reusable test bench verification for the tech_mac_10g
-- Description:
--   Based on the transmitted data at eop verify the received data at eop.

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use WORK.tb_tech_mac_10g_pkg.all;

entity tb_tech_mac_10_verify_rx_at_eop is
  generic (
    g_no_padding     : boolean := false;  -- default FALSE to verify the data from MAC that is padded to minimal length, else use TRUE to verify the data of any length
    g_pkt_length_arr : t_nat_natural_arr
  );
  port (
    tx_clk      : in  std_logic;
    tx_sosi     : in  t_dp_sosi;
    rx_clk      : in  std_logic;
    rx_sosi     : in  t_dp_sosi
  );
end tb_tech_mac_10_verify_rx_at_eop;

architecture tb of tb_tech_mac_10_verify_rx_at_eop is
  constant c_nof_pkt       : natural := g_pkt_length_arr'length;

  signal expected_sosi_arr : t_dp_sosi_arr(0 to c_nof_pkt - 1);
  signal rx_sosi_reg       : t_dp_sosi;
begin
  p_ff_store_tx_at_eop : process(tx_clk)
    variable vI : natural := 0;
  begin
    if rising_edge(tx_clk) then
      if tx_sosi.eop = '1' then
        expected_sosi_arr(vI) <= tx_sosi;
        vI := vI + 1;
      end if;
    end if;
  end process;

  p_ff_verify_rx_at_eop : process(rx_clk)
    variable vI   : natural := 0;
    variable vLow : natural := 0;
  begin
    if rising_edge(rx_clk) then
      rx_sosi_reg <= rx_sosi;  -- use rx_sosi_reg for verification at eop to account for once cycle latency in expected_sosi_arr()
      if rx_sosi_reg.eop = '1' then
        if g_no_padding = false and g_pkt_length_arr(vI) < 64 - 14 - 20 - 8 - 4 then  -- = minimum frame 64 - ETH 14 - IP 20 - UDP 8 - CRC 4
          -- frame shorter than 64 get padded by MAC so empty after stripping the Rx CRC is fixed 4, which becomes 6 due to pre header padding for UDP word align
          if TO_UINT(rx_sosi_reg.empty) /= 6 then
            report "RX at eop: Wrong padded empty"
              severity ERROR;
          end if;
        else
          if rx_sosi_reg.empty /= expected_sosi_arr(vI).empty then
            report "RX at eop: Wrong empty"
              severity ERROR;
          else
            vLow := TO_UINT(rx_sosi_reg.empty) * 8;
            assert rx_sosi_reg.data(63 downto vLow) = expected_sosi_arr(vI).data(63 downto vLow)
              report "RX at eop: Wrong data"
              severity ERROR;
          end if;
        end if;
        vI := vI + 1;
      end if;
    end if;
  end process;
end tb;
