-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : * Insert extra sync pulses that copy the BSN of the incoming sync until a new sync pulse is availabel at the input
--           * The blocksize of the output stream can also be changed.
-- Description:
--   The BSN is captured on the incoming sync pulse and stored.
--   The incoming data is repacked into output blocks of size g_nof_data_per_blk.
--   The output control signals sop and eop are created. Every g_nof_blk_per_sync block
--   a sync pulse is created at the output. Allong with this sync pulse the stored BSN is
--   set on the output. The BSN of the non-synced blocks are simply incremented starting
--   from 1 and resetted every outgoing sync pulse.
--
-- Remarks:
-- . There is no support for back pressure.
-- . It does not compensate for missing data or extra data. There is NO reset function. It assumes that the
--   incoming data is perfectly aligned. Use a dp_sync_checker to assure the incoming data is perfect.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_sync_insert is
  generic (
    g_nof_data_per_blk : natural := 128;
    g_nof_blk_per_sync : natural := 12500
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    -- Streaming sink
    snk_in     : in  t_dp_sosi := c_dp_sosi_rst;
    -- Streaming source
    src_out    : out t_dp_sosi
  );
end dp_sync_insert;

architecture rtl of dp_sync_insert is
  type t_reg is record  -- local registers
    data_cnt      : natural range 0 to g_nof_data_per_blk;
    blk_cnt       : natural range 0 to g_nof_blk_per_sync;
    sync_out_bsn  : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
    snk_in_reg    : t_dp_sosi;
    src_out       : t_dp_sosi;
    sync_detected : std_logic;
  end record;

  constant c_reg_rst  : t_reg := ( 0, 0, (others => '0'), c_dp_sosi_rst, c_dp_sosi_rst, '0');

  -- Define the local registers in t_reg record
  signal r         : t_reg;
  signal nxt_r     : t_reg;
begin
  src_out <= r.src_out;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(clk) then
      r <= nxt_r;
    end if;
  end process;

  p_comb : process(r, snk_in)
    variable v : t_reg;
  begin
    v := r;
    v.src_out := func_dp_stream_reset_control(v.src_out);
    v.snk_in_reg := snk_in;

    if snk_in.sync = '1' then
      v.sync_out_bsn := snk_in.bsn;
      if r.sync_detected = '0' then
        v.sync_detected := '1';
      end if;
    end if;

    if r.snk_in_reg.valid = '1' and r.sync_detected = '1' then
      v.src_out.data  := r.snk_in_reg.data;
      v.src_out.re    := r.snk_in_reg.re;
      v.src_out.im    := r.snk_in_reg.im;
      v.src_out.valid := '1';

      v.data_cnt      := r.data_cnt + 1;

      if r.data_cnt = 0 and r.blk_cnt = 0 then
        v.src_out.sync := '1';
        v.src_out.bsn  := r.sync_out_bsn;
        v.src_out.sop  := '1';
        v.blk_cnt      := r.blk_cnt + 1;
      elsif r.data_cnt = 0 then
        v.src_out.sop  := '1';
        v.src_out.bsn  := TO_DP_BSN(r.blk_cnt);
        v.blk_cnt      := r.blk_cnt + 1;
      elsif r.data_cnt = g_nof_data_per_blk - 1 then
        v.data_cnt    := 0;
        v.src_out.eop := '1';
      end if;

      if r.blk_cnt = g_nof_blk_per_sync then
        v.blk_cnt := 0;
      end if;
    end if;

    nxt_r <= v;
  end process;
end rtl;
