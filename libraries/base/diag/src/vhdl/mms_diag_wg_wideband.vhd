-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provides a wideband WG by using multiple diag_wg
-- Description:
-- . See diag.wg.vhd.
-- Remarks:
-- . For g_wideband_factor=1 this diag_wg_wideband defaults to diag_wg. Hence
--   no need to make a mms_diag_wg.vhd.
--

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.diag_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_diag_wg_wideband is
  generic (
    g_technology         : natural := c_tech_select_default;
    -- Use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_cross_clock_domain : boolean := true;

    -- Use g_buf_dir to be able to have different path to waveform file for sim and for synth
    g_buf_dir            : string := "data/";

    -- Wideband parameters
    g_wideband_factor    : natural := 4;  -- Wideband rate factor >= 1 for unit frequency of g_wideband_factor * Fs

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w          : natural := 18;
    g_buf_addr_w         : natural := 11;
    g_calc_support       : boolean := true;
    g_calc_gain_w        : natural := 1;
    g_calc_dat_w         : natural := 12
  );
  port (
    -- Memory-mapped clock domain
    mm_rst               : in  std_logic;
    mm_clk               : in  std_logic;

    reg_mosi             : in  t_mem_mosi;
    reg_miso             : out t_mem_miso;

    buf_mosi             : in  t_mem_mosi;
    buf_miso             : out t_mem_miso;

    -- Streaming clock domain
    st_rst               : in  std_logic;
    st_clk               : in  std_logic;
    st_restart           : in  std_logic := '0';

    out_ovr              : out std_logic_vector(g_wideband_factor            - 1 downto 0);  -- big endian, so first output sample in MSBit, MSData
    out_dat              : out std_logic_vector(g_wideband_factor * g_buf_dat_w - 1 downto 0);
    out_val              : out std_logic_vector(g_wideband_factor            - 1 downto 0);
    out_sync             : out std_logic_vector(g_wideband_factor            - 1 downto 0)
  );
end mms_diag_wg_wideband;

architecture str of mms_diag_wg_wideband is
  signal st_wg_ctrl      : t_diag_wg;  -- write
  signal st_mon_ctrl     : t_diag_wg;  -- read
begin
  u_mm_reg : entity work.diag_wg_wideband_reg
  generic map (
    g_cross_clock_domain => g_cross_clock_domain
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => st_rst,
    st_clk      => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => reg_mosi,
    sla_out     => reg_miso,

    -- MM registers in st_clk domain
    st_wg_ctrl  => st_wg_ctrl,
    st_mon_ctrl => st_mon_ctrl
  );

  u_wg_wideband : entity work.diag_wg_wideband
  generic map (
    g_technology        => g_technology,
    -- Use g_buf_dir to be able to have different path to waveform file for sim and for synth
    g_buf_dir           => g_buf_dir,

    -- Wideband parameters
    g_wideband_factor   => g_wideband_factor,

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w         => g_buf_dat_w,
    g_buf_addr_w        => g_buf_addr_w,
    g_calc_support      => g_calc_support,
    g_calc_gain_w       => g_calc_gain_w,
    g_calc_dat_w        => g_calc_dat_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,

    mm_wrdata            => buf_mosi.wrdata(g_buf_dat_w - 1 downto 0),
    mm_address           => buf_mosi.address(g_buf_addr_w - 1 downto 0),
    mm_wr                => buf_mosi.wr,
    mm_rd                => buf_mosi.rd,
    mm_rdval             => buf_miso.rdval,
    mm_rddata            => buf_miso.rddata(g_buf_dat_w - 1 downto 0),

    -- Streaming clock domain
    st_rst               => st_rst,
    st_clk               => st_clk,
    st_restart           => st_restart,

    st_ctrl              => st_wg_ctrl,
    st_mon_ctrl          => st_mon_ctrl,

    out_ovr              => out_ovr,
    out_dat              => out_dat,
    out_val              => out_val,
    out_sync             => out_sync
  );
end str;
