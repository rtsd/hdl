-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_frame_rd is
end tb_dp_frame_rd;

architecture tb of tb_dp_frame_rd is
  -- > as 5
  -- > run 10 us, at 4335 ns expect Warning: Frame with valid data but no sof.

  constant clk_period         : time := 10 ns;  -- 100 MHz

  constant c_init_length      : natural := 100;
  constant c_empty_length     : natural := 50;
  constant c_wait_length      : natural := 10;
  constant c_nof_frames       : natural := 2;
  constant c_frame_length     : natural := 30;
  constant c_gap_length       : natural := 10;
  constant c_data_w           : natural := 16;

  constant c_fifo_nof_words   : natural := 1024;
  constant c_fifo_dat_w       : natural := 1 + 1 + c_data_w;  -- = 1+1+32=34
  constant c_throttle_num     : natural := 1;  -- numerator <= g_throttle_den
  constant c_throttle_den     : natural := 4;  -- denominator (use 1 for full speed, i.e no output throttling)
  --CONSTANT c_throttle_sof     : BOOLEAN := TRUE;  -- when false immediately do request next data after sof
  constant c_throttle_sof     : boolean := false;
  --CONSTANT c_throttle_eof     : BOOLEAN := TRUE;  -- when false immediately continue request next frame after eof
  constant c_throttle_eof     : boolean := false;
  constant c_frame_request    : std_logic := '1';  -- when '1' then always request, else only in state s_request when not frm_ack
                                                   -- use '1' to verify c_throttle_eof=FALSE

  type t_state_enum is (s_request, s_eof, s_err);

  signal clk           : std_logic := '0';
  signal rst           : std_logic;

  signal in_data       : std_logic_vector(c_data_w - 1 downto 0);
  signal in_val        : std_logic;
  signal in_sof        : std_logic;
  signal in_eof        : std_logic;

  signal in_sosi       : t_dp_sosi;
  signal rd_siso       : t_dp_siso;
  signal rd_sosi       : t_dp_sosi;

  signal fifo_rd_data  : std_logic_vector(c_data_w - 1 downto 0);
  signal fifo_rd_val   : std_logic;
  signal fifo_rd_sof   : std_logic;
  signal fifo_rd_eof   : std_logic;

  signal fifo_rd_req   : std_logic;
  signal fifo_usedw    : std_logic_vector(ceil_log2(c_fifo_nof_words) - 1 downto 0);

  signal frm_req       : std_logic;
  signal frm_flush     : std_logic;
  signal frm_ack       : std_logic;
  signal frm_busy      : std_logic;
  signal frm_err       : std_logic;
  signal frm_done      : std_logic;

  signal state         : t_state_enum;

  signal out_data      : std_logic_vector(c_data_w - 1 downto 0);
  signal out_val       : std_logic;
  signal out_sof       : std_logic;
  signal out_eof       : std_logic;
  signal prev_out_data : std_logic_vector(out_data'range) := (others => '0');

  signal verify_en     : std_logic;
  signal verify_done   : std_logic;

  signal exp_data      : std_logic_vector(c_data_w - 1 downto 0) := TO_UVEC(100, c_data_w);

  procedure proc_frame(constant in_sof       : in  std_logic;
                       constant in_data      : in  std_logic_vector(c_data_w - 1 downto 0);
                       constant empty_length : in  natural;
                       signal   clk          : in  std_logic;
                       signal   frm_data     : out std_logic_vector(c_data_w - 1 downto 0);
                       signal   frm_val      : out std_logic;
                       signal   frm_sof      : out std_logic;
                       signal   frm_eof      : out std_logic) is
    variable v_frm_data : std_logic_vector(frm_data'range);
  begin
    frm_val    <= '1';
    -- sof
    frm_sof    <= in_sof;
    v_frm_data := INCR_UVEC(in_data, 1);
    frm_data   <= v_frm_data;
    wait until rising_edge(clk);
    frm_sof  <= '0';
    for I in 2 to c_frame_length / 2 - 1 loop
      v_frm_data := INCR_UVEC(v_frm_data, 1);
      frm_data   <= v_frm_data;
      wait until rising_edge(clk);
    end loop;
    frm_val    <= '0';
    for I in 1 to empty_length loop wait until rising_edge(clk); end loop;
    frm_val    <= '1';
    for I in c_frame_length / 2 to c_frame_length - 1 loop
      v_frm_data := INCR_UVEC(v_frm_data, 1);
      frm_data   <= v_frm_data;
      wait until rising_edge(clk);
    end loop;
    -- eof
    frm_eof    <= '1';
    v_frm_data := INCR_UVEC(v_frm_data, 1);
    frm_data   <= v_frm_data;
    wait until rising_edge(clk);
    frm_val    <= '0';
    frm_sof    <= '0';
    frm_eof    <= '0';
    -- gap
    for I in 1 to c_gap_length loop wait until rising_edge(clk); end loop;
  end proc_frame;
begin
  clk <= not clk after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  verify_en <= '1';

  p_input_stimuli : process
    variable v_data  : std_logic_vector(c_data_w - 1 downto 0);
    variable v_sof   : std_logic;
  begin
    verify_done <= '0';
    v_data  := (others => '0');
    v_sof   := '0';
    in_data <= (others => '0');
    in_val  <= '0';
    in_sof  <= '0';
    in_eof  <= '0';
    wait until rst = '0';
    for I in 1 to c_init_length loop wait until rising_edge(clk); end loop;

    -- Some proper frames
    v_sof := '1';
    for I in 1 to c_nof_frames loop
      proc_frame(v_sof, v_data, 0, clk, in_data, in_val, in_sof, in_eof);
      v_data := in_data;
    end loop;

    -- Frame without sof
    v_sof := '0';
    proc_frame(v_sof, v_data, 0, clk, in_data, in_val, in_sof, in_eof);
    --v_data := in_data;  -- commented to fit p_verify

    -- Some proper frames with FIFO running empty
    v_sof := '1';
    for I in 1 to c_nof_frames loop
      proc_frame(v_sof, v_data, c_empty_length, clk, in_data, in_val, in_sof, in_eof);
      v_data := in_data;
    end loop;

    -- Signal verify done after waiting for some latency in dut
    wait for 500 * clk_period;
    wait until rising_edge(clk);
    verify_done <= '1';
    wait until rising_edge(clk);
    verify_done <= '0';
    wait;
  end process;

  -- map sl, slv to record
  in_sosi.data(c_data_w - 1 downto 0) <= in_data;  -- SOSI
  in_sosi.valid                     <= in_val;
  in_sosi.sop                       <= in_sof;
  in_sosi.eop                       <= in_eof;

  rd_siso.ready <= fifo_rd_req;  -- SISO
  fifo_rd_data  <= rd_sosi.data(c_data_w - 1 downto 0);  -- SOSI
  fifo_rd_val   <= rd_sosi.valid;
  fifo_rd_sof   <= rd_sosi.sop;
  fifo_rd_eof   <= rd_sosi.eop;

  u_fifo : entity work.dp_fifo_sc
  generic map (
    g_data_w      => c_data_w,
    g_empty_w     => 1,
    g_channel_w   => 1,
    g_error_w     => 1,
    g_use_empty   => false,
    g_use_channel => false,
    g_use_error   => false,
    g_use_ctrl    => true,
    g_fifo_size   => c_fifo_nof_words,
    g_fifo_rl     => 1
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_out     => OPEN,  -- OUT = request to upstream ST source
    snk_in      => in_sosi,
    usedw       => fifo_usedw,
    src_in      => rd_siso,  -- IN  = request from downstream ST sink
    src_out     => rd_sosi
  );

  u_dut : entity work.dp_frame_rd
  generic map (
    g_dat_w          => c_data_w,
    g_frm_cnt_max    => 1,
    g_throttle_num   => c_throttle_num,
    g_throttle_den   => c_throttle_den,
    g_throttle_sof   => c_throttle_sof,
    g_throttle_eof   => c_throttle_eof
  )
  port map (
    rst              => rst,
    clk              => clk,

    frm_req          => frm_req,
    frm_flush        => frm_flush,
    frm_ack          => frm_ack,
    frm_busy         => frm_busy,
    frm_err          => frm_err,
    frm_done         => frm_done,

    rd_req           => fifo_rd_req,
    rd_dat           => fifo_rd_data,
    rd_val           => fifo_rd_val,
    rd_sof           => fifo_rd_sof,
    rd_eof           => fifo_rd_eof,

    out_dat          => out_data,
    out_val          => out_val,
    out_sof          => out_sof,
    out_eof          => out_eof
  );

  p_output_stimuli : process
  begin
    frm_req   <= '0';
    frm_flush <= '0';
    state     <= s_request;

    for I in 1 to c_init_length    loop wait until rising_edge(clk); end loop;
    for I in 1 to c_wait_length * 10 loop wait until rising_edge(clk); end loop;

    while true loop
      frm_req   <= c_frame_request;
      frm_flush <= '0';
      case state is
        when s_request =>
          if frm_ack = '0' then
            frm_req <= '1';
          elsif frm_err = '1' then
            state <= s_err;  -- frame with valid data but no sof
          elsif frm_busy = '1' then
            state <= s_eof;  -- frame with sof, read rest of frame
          else
            state <= s_request;  -- no frame, request again
          end if;
        when s_eof =>
          if frm_done = '1' then
            state <= s_request;
          end if;
        when others =>  -- s_err
          report "Frame with valid data but no sof"
            severity WARNING;
          state <= s_request;
      end case;
      wait until rising_edge(clk);
    end loop;
  end process;

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if out_val = '1' then
        prev_out_data <= out_data;
        if verify_en = '1' and unsigned(out_data) /= unsigned(prev_out_data) + 1 then
           report "Wrong out_data count"
             severity ERROR;
        end if;
      end if;
    end if;
  end process;

  -- Check that the test has ran at all
  proc_dp_verify_value(e_at_least, clk, verify_done, exp_data, out_data);
end tb;
