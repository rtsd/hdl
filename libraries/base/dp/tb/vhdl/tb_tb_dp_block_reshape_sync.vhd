-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra, 1 May 2018
-- Purpose: Verify pipelining or no pipelining of dp_block_reshape_sync

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_block_reshape_sync is
end tb_tb_dp_block_reshape_sync;

architecture tb of tb_tb_dp_block_reshape_sync is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 5
  -- > run -all                 --> OK

  -- g_input_nof_data_per_blk   : NATURAL :=  9;
  -- g_input_nof_blk_per_sync   : NATURAL := 12;  --  6 * 108 = 12 * 9
  -- g_reshape_nof_data_per_blk : NATURAL := 18;
  -- g_reshape_nof_blk_per_sync : NATURAL :=  3;  -- 12 *  54 =  3 * 18
  -- g_reshape_bsn              : BOOLEAN := TRUE;  -- when TRUE reshape BSN else when FALSE pass on BSN
  -- g_pipeline_src_out         : NATURAL := 0;
  -- g_pipeline_src_in          : NATURAL := 0

  -- nof reshape sync > nof input sync, so reshape sync intervals are shorter
  u_more_comb                      : entity work.tb_dp_block_reshape_sync generic map (9, 12, 18,  3, false, 0, 0);
  u_more_pipe                      : entity work.tb_dp_block_reshape_sync generic map (9, 12, 18,  3, false, 0, 0);
  u_more_bsn_channel_comb          : entity work.tb_dp_block_reshape_sync generic map (9, 12, 18,  3,  true, 0, 0);
  u_more_bsn_channel_pipe_src_out  : entity work.tb_dp_block_reshape_sync generic map (9, 12, 18,  3,  true, 1, 0);
  u_more_bsn_channel_pipe_src_in   : entity work.tb_dp_block_reshape_sync generic map (9, 12, 18,  3,  true, 0, 1);

  -- nof reshape sync < nof input sync, so reshape sync intervals are longer
  u_less_bsn_channel_pipe_src_out  : entity work.tb_dp_block_reshape_sync generic map (9, 12, 18, 12,  true, 1, 0);
end tb;
