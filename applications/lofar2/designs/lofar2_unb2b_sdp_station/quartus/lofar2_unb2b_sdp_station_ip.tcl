###############################################################################
#
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

set IP_PATH $::env(HDL_BUILD_DIR)/unb2b/quartus/lofar2_unb2b_sdp_station/ip/qsys_lofar2_unb2b_sdp_station

set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_avs_common_mm_0.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_avs_common_mm_1.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_avs_eth_0.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_clk_0.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_cpu_0.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_jesd204b.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_jtag_uart_0.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_onchip_memory2_0.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_pio_jesd_ctrl.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_pio_pps.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_pio_system_info.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_pio_wdi.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_bf_weights.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_diag_data_buffer_bsn.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_equalizer_gains.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_equalizer_gains_cross.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_fil_coefs.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_scrap.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_ss_ss_wide.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_st_bst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_st_histogram.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_st_sst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_st_xsq.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_ram_wg.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_aduh_monitor.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bf_scale.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bdo_destinations.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_align_v2_bf.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_align_v2_xsub.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_input.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_aligned_bf.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_aligned_xsub.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_beamlet_output.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_bst_offload.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_ring_rx_bf.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_ring_rx_xst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_ring_tx_bf.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_ring_tx_xst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_rx_align_bf.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_rx_align_xsub.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_sst_offload.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_monitor_v2_xst_offload.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_scheduler.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_source_v2.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_bsn_sync_scheduler_xsub.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_crosslets_info.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_diag_data_buffer_bsn.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dp_block_validate_bsn_at_sync_bf.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dp_block_validate_bsn_at_sync_xst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dp_block_validate_err_bf.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dp_block_validate_err_xst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dpmm_ctrl.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dpmm_data.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dp_selector.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dp_shiftram.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_dp_xonoff.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_epcs.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_fpga_temp_sens.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_fpga_voltage_sens.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_hdr_dat.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_mmdp_ctrl.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_mmdp_data.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_nof_crosslets.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_nw_10gbe_eth10g.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_nw_10gbe_mac.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_remu.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_ring_info.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_ring_lane_info_bf.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_ring_lane_info_xst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_sdp_info.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_si.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_stat_enable_bst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_stat_enable_sst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_stat_enable_xst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_stat_hdr_dat_bst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_stat_hdr_dat_sst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_stat_hdr_dat_xst.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_tr_10gbe_eth10g.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_tr_10gbe_mac.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_unb_pmbus.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_unb_sens.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_wdi.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_reg_wg.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_rom_system_info.ip
set_global_assignment -name IP_FILE $IP_PATH/qsys_lofar2_unb2b_sdp_station_timer_0.ip

