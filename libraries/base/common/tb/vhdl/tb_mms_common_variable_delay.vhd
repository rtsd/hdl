-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker
-- Purpose:
-- . test bench for mms_common_variable_delay.vhd to test enable by signal mm interface
-- Description:
-- . see common_variable_delay.vhd
-- . Only verifies mm control of enable.
-- . Detailde verification of trigger has been done already in tb_common_variable_delay.vhd.
-- . Here it is sufficient to use Wave window to view effect of enable on trigger.
-- --------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_str_pkg.all;
use work.tb_common_pkg.all;
use work.common_mem_pkg.all;
use work.tb_common_mem_pkg.all;

entity tb_mms_common_variable_delay is
end tb_mms_common_variable_delay;

architecture tb of tb_mms_common_variable_delay is
  constant c_clk_period       : time    := 10 ns;
  constant c_trigger_interval : natural := 40;  -- in clk's
  constant c_mm_addr_enable   : natural := 0;
  constant c_cross_clock_domain_latency : natural := 40;

  signal tb_end : std_logic := '0';
  signal rst    : std_logic;
  signal clk    : std_logic := '0';

  signal delay       : natural   := 0;
  signal trigger     : std_logic := '0';
  signal trigger_dly : std_logic := '0';

  signal mm_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal mm_miso     : t_mem_miso;

  signal enable      : natural;
begin
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 4;

  p_mm_stimuli : process
  begin
    wait until rst = '0';
    proc_common_wait_some_cycles(clk, 10);

    proc_mem_mm_bus_wr(c_mm_addr_enable, 1, clk, mm_miso, mm_mosi);
    proc_common_wait_some_cycles(clk, c_cross_clock_domain_latency);
    proc_mem_mm_bus_rd(c_mm_addr_enable, clk, mm_miso, mm_mosi);
    proc_mem_mm_bus_rd_latency(1, clk);
    enable <= TO_UINT(mm_miso.rddata(1 downto 0));
    proc_common_wait_some_cycles(clk, 1);
    assert enable = 1
      report "enable not on"
      severity ERROR;

    proc_mem_mm_bus_wr(c_mm_addr_enable, 0, clk, mm_miso, mm_mosi);
    proc_common_wait_some_cycles(clk, c_cross_clock_domain_latency);
    proc_mem_mm_bus_rd(c_mm_addr_enable, clk, mm_miso, mm_mosi);
    proc_mem_mm_bus_rd_latency(1, clk);
    enable <= TO_UINT(mm_miso.rddata(0 downto 0));
    proc_common_wait_some_cycles(clk, 1);
    assert enable = 0
      report "enable not off"
      severity ERROR;

    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  -- generate trigger pulse signal
  proc_common_gen_pulse(1, c_trigger_interval, '1', rst, clk, trigger);

  -- device under test
  u_dut : entity work.mms_common_variable_delay
  port map (
    mm_rst     => rst,
    mm_clk     => clk,
    dp_rst     => rst,
    dp_clk     => clk,

    reg_enable_mosi => mm_mosi,
    reg_enable_miso => mm_miso,

    delay       => delay,
    trigger     => trigger,
    trigger_dly => trigger_dly
  );
end tb;
