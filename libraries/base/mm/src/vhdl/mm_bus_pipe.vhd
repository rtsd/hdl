-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Provide pipelining to the combinatorial mm_bus_comb
-- Description:
--   The mm_bus_comb is combinatorial, so there is no pipelining between
--   the master interface and the slave interfaces. If possible do not
--   use pipelining of mosi and miso to avoid extra logic and to avoid
--   increasing the read latency. Instead first try achieve timing closure
--   by lower clock rate for the MM bus. Pipelining the MM bus can be
--   necessary to achieve timing closure. Thanks to mm_bus_comb the
--   pipelining is clearly separated from the MM bus multiplexer. The
--   pipelining is placed at the output of the bus, so at the slave side
--   for mosi and at the master side for miso:
--
--                                   FOR g_nof_slaves:
--      g_pipeline_miso_rdval        g_pipeline_mosi
--      g_pipeline_miso_wait              |   g_pipeline_miso_wait
--               |                        |           |
--               v       ________     ____v___     ___v___
--   <-- p_miso_pipe <--| mm_bus |<--|mm      |<--|mm     |<--------
--   ------------------>|  comb  |-->|pipeline|-->|latency|-------->
--     .              . |________| . |________|   |adapter| .    .
--   master_miso      .            .            . |_______| . slave_miso_arr
--   master_mosi      .            .            .           . slave_mosi_arr
--                  m_miso   bus_miso_arr  pipe_miso_arr  adapt_miso_arr
--                  m_mosi   bus_mosi_arr  pipe_mosi_arr  adapt_mosi_arr
--
--   The MM bus pipelining is defined by:
--
--   * g_pipeline_mosi
--     Pipelining mosi write accesses introduces an extra latency from master
--     to slave, which is typically not a problem. Pipelining mosi read
--     accesses increases the read latency between accessing the slave and
--     getting the rddata. Using a different pipelining for the wr and the rd
--     pulse would yield a different pipelining of the address for write and
--     for read, which is akward. Therefore both mosi write and mosi read
--     use the same g_pipeline_mosi pipelining.
--
--   * g_pipeline_miso_rdval
--     Pipelining the miso read data increases the read latency.
--
--   * g_pipeline_miso_wait
--     Pipelining the miso waitrequest increases the write and read latency
--     for slaves that need MM flow control. Only applies to slave that
--     have g_waitrequest_arr is TRUE.
--
--   The pipelining generics are defined as BOOLEAN (rather than NATURAL),
--   because the pipelining only needs to be 0 or 1.
--
--   The total write latency from master to slave is 1 when either
--   g_pipeline_mosi or g_pipeline_miso_wait.
--   The total read latency from master via slave back to master is
--   write latency + g_rd_latency_arr of the selected slave + 1 or 0
--   dependend on g_pipeline_miso_rdval.
--
-- Usage:
--   See mm_bus.vhd
--
-- Remark:
--   * It is not allowed to simultaneously use g_pipeline_miso_wait = TRUE
--     and g_pipeline_mosi = TRUE, because this leads to a combinatorial loop
--     of the miso.waitrequest that is used at the output of the mm_pipeline
--     and at the input of the mm_latency adapter:
--     - at the mm_pipeline output the waitrequest gates the mosi.wr and rd
--     - at the mm_latency_adapter input in common_rl_decrease the wr or
--       rd strobe is used to set the waitrequest.
--     This combinatorial loop seems unavoidable when the interface between
--     mm_pipeline and mm_latency_adpater is at RL = 0. A solution could be
--     to increase the RL at the output of the mm_pipeline to RL = 1 by
--     registering the waitrequest from the mm_latency_adapter. The total
--     RL for the input of the MM latency adapter then becomes RL = 2, so
--     then the mm_latency_adapter needs to adapt from RL = 2 to 0.
--     Currently the mm_latency_adapter only supports RL 1 to 0. Is possible
--     to extent this to RL = N to 0, similar as in dp_latency_adapter.
--     However fortunately it is not necessary to support g_pipeline_mosi =
--     TRUE when g_pipeline_miso_wait = TRUE, because g_pipeline_miso_wait =
--     TRUE by itself already also pipeplines the mosi.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mm_bus_pipe is
  generic (
    g_nof_slaves          : positive;  -- Number of MM slave interfaces on the bus
    g_base_arr            : t_nat_natural_arr;  -- Address base per slave
    g_width_arr           : t_nat_natural_arr;  -- Address width per slave
    g_rd_latency_arr      : t_nat_natural_arr;  -- Read latency per slave
    g_waitrequest_arr     : t_nat_boolean_arr;  -- Enable waitrequest flow control per slave, else fixed '0'
    g_pipeline_mosi       : boolean := false;  -- Pipeline MM access (wr, rd)
    g_pipeline_miso_rdval : boolean := false;  -- Pipeline MM read (rdval)
    g_pipeline_miso_wait  : boolean := false  -- Pipeline MM access flow control (waitrequest)
  );
  port (
    mm_rst         : in  std_logic;
    mm_clk         : in  std_logic;
    master_mosi    : in  t_mem_mosi;
    master_miso    : out t_mem_miso;
    slave_mosi_arr : out t_mem_mosi_arr(0 to g_nof_slaves - 1);
    slave_miso_arr : in  t_mem_miso_arr(0 to g_nof_slaves - 1) := (others => c_mem_miso_rst)
  );
end mm_bus_pipe;

architecture str of mm_bus_pipe is
  signal m_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal m_miso         : t_mem_miso := c_mem_miso_rst;
  signal m_miso_reg     : t_mem_miso := c_mem_miso_rst;

  signal bus_mosi_arr   : t_mem_mosi_arr(0 to g_nof_slaves - 1);
  signal bus_miso_arr   : t_mem_miso_arr(0 to g_nof_slaves - 1);
  signal pipe_mosi_arr  : t_mem_mosi_arr(0 to g_nof_slaves - 1);
  signal pipe_miso_arr  : t_mem_miso_arr(0 to g_nof_slaves - 1);
  signal adapt_mosi_arr : t_mem_mosi_arr(0 to g_nof_slaves - 1);
  signal adapt_miso_arr : t_mem_miso_arr(0 to g_nof_slaves - 1);
begin
  assert not(g_pipeline_miso_wait = true and g_pipeline_mosi = true)
    report "Do not use g_pipeline_mosi = true if g_pipeline_miso_wait = TRUE"
    severity FAILURE;

  -- Master side
  m_mosi <= master_mosi;

  m_miso_reg <= m_miso when rising_edge(mm_clk);

  p_miso_pipe : process(m_miso, m_miso_reg)
  begin
    -- Default no miso pipelining
    master_miso <= m_miso;
    -- Use pipelining
    if g_pipeline_miso_rdval then
      master_miso.rddata <= m_miso_reg.rddata;
      master_miso.rdval  <= m_miso_reg.rdval;
    end if;
    if g_pipeline_miso_wait then
      master_miso.waitrequest <= m_miso_reg.waitrequest;
    end if;
  end process;

  -- MM bus
  u_mm_bus_comb : entity work.mm_bus_comb
  generic map (
    g_nof_slaves     => g_nof_slaves,
    g_base_arr       => g_base_arr,
    g_width_arr      => g_width_arr,
    g_rd_latency_arr => g_rd_latency_arr
  )
  port map (
    mm_clk         => mm_clk,
    master_mosi    => m_mosi,
    master_miso    => m_miso,
    slave_mosi_arr => bus_mosi_arr,
    slave_miso_arr => bus_miso_arr
  );

  -- Slaves side
  gen_slave_pipes : for I in 0 to g_nof_slaves - 1 generate
    u_slave_pipe_mosi : entity work.mm_pipeline
    generic map (
      g_pipeline => g_pipeline_mosi
    )
    port map (
      mm_rst        => mm_rst,
      mm_clk        => mm_clk,
      in_mosi       => bus_mosi_arr(I),
      in_miso       => bus_miso_arr(I),
      out_mosi      => pipe_mosi_arr(I),
      out_miso      => pipe_miso_arr(I)
    );

    gen_wires : if g_waitrequest_arr(I) = false generate
      adapt_mosi_arr(I) <= pipe_mosi_arr(I);
      pipe_miso_arr(I)  <= adapt_miso_arr(I);
    end generate;

    gen_slave_latency_adapter : if g_waitrequest_arr(I) = true generate
      u_slave_latency_adapter : entity work.mm_latency_adapter
      generic map (
        g_adapt => g_pipeline_miso_wait
      )
      port map (
        mm_rst        => mm_rst,
        mm_clk        => mm_clk,
        -- MM input RL = 1
        in_mosi       => pipe_mosi_arr(I),
        in_miso       => pipe_miso_arr(I),
        -- MM output RL = 0
        out_mosi      => adapt_mosi_arr(I),
        out_miso      => adapt_miso_arr(I)
      );
    end generate;
  end generate;

  slave_mosi_arr <= adapt_mosi_arr;
  adapt_miso_arr <= slave_miso_arr;
end str;
