-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Multi test bench for mm_bus.vhd
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity tb_tb_mm_bus is
end tb_tb_mm_bus;

architecture tb of tb_tb_mm_bus is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Usage:
  -- > as 4
  -- > run -all

  -- g_nof_slaves          : POSITIVE := 2;       -- Number of slave memory interfaces on the MM bus array.
  -- g_base_offset         : NATURAL := 0;        -- Address of first slave on the MM bus
  -- g_width_w             : POSITIVE := 4;       -- Address width of each slave memory in the MM bus array.
  -- g_rd_latency          : NATURAL := 1;        -- Read latency of the slaves slave
  -- g_waitrequest         : BOOLEAN := FALSE;    -- When TRUE model waitrequest by MM slaves, else fixed '0'
  -- g_pipeline_mosi       : BOOLEAN := FALSE;
  -- g_pipeline_miso_rdval : BOOLEAN := TRUE;
  -- g_pipeline_miso_wait  : BOOLEAN := FALSE

  u_no_pipe                          : entity work.tb_mm_bus generic map (16,      0, 3, 1, false, false, false, false);
  u_no_pipe_base_offset              : entity work.tb_mm_bus generic map (16, 3 * 2**4, 4, 1, false, false, false, false);
  u_pipe_mosi                        : entity work.tb_mm_bus generic map ( 3,      0, 4, 1, false,  true, false, false);
  u_pipe_mosi_miso_rdval             : entity work.tb_mm_bus generic map ( 3,      0, 4, 1, false,  true,  true, false);
  u_waitrequest_no_pipe              : entity work.tb_mm_bus generic map ( 3,      0, 4, 1,  true, false, false, false);
  u_waitrequest_pipe_miso_rdval      : entity work.tb_mm_bus generic map ( 3,      0, 4, 1,  true, false,  true, false);
  u_waitrequest_pipe_miso_rdval2     : entity work.tb_mm_bus generic map ( 3,      0, 4, 2,  true, false,  true, false);
  u_waitrequest_pipe_miso_wait       : entity work.tb_mm_bus generic map ( 2,      0, 4, 1,  true, false, false,  true);
  u_waitrequest_pipe_mosi_one        : entity work.tb_mm_bus generic map ( 1,      0, 4, 1,  true,  true, false, false);
  u_waitrequest_pipe_mosi            : entity work.tb_mm_bus generic map ( 2,      0, 4, 1,  true,  true, false, false);
  u_waitrequest_pipe_mosi_miso_rdval : entity work.tb_mm_bus generic map ( 2,      0, 4, 1,  true,  true,  true, false);

  -- Do not support simultaneous g_pipeline_mosi = TRUE and g_pipeline_miso_wait = TRUE, see mm_bus_pipe.vhd.
  --u_waitrequest_pipe_mosi_miso_wait  : ENTITY work.tb_mm_bus GENERIC MAP ( 2,      0, 4, 1,  TRUE,  TRUE, FALSE,  TRUE);
  --u_waitrequest_pipe_all             : ENTITY work.tb_mm_bus GENERIC MAP ( 2,      0, 4, 1,  TRUE,  TRUE,  TRUE,  TRUE);
end tb;
