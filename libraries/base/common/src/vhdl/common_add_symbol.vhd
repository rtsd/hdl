-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

-- Purpose: Per symbol add of the two input data stream
-- Description:
--   The in_a, in_b and out_data are slv with g_nof_symbols-1:0 concatenated
--   symbols. The out_data contains the sum of each pair of symbols in in_a
--   and in_b. The symbol width is g_symbol_w. The output can be pipelined via
--   g_pipeline.
-- Remarks:
-- . No need for g_representation = "SIGNED" or "UNSIGNED", because that is
--   only important if output width > input width, and not relevant here where
--   both output width and input width are g_symbol_w

entity common_add_symbol is
  generic (
    g_pipeline     : natural := 0;
    g_nof_symbols  : natural := 4;
    g_symbol_w     : natural := 16
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;

    in_a       : in  std_logic_vector(g_nof_symbols * g_symbol_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_symbols * g_symbol_w - 1 downto 0);
    in_val     : in  std_logic := 'X';
    in_sop     : in  std_logic := 'X';
    in_eop     : in  std_logic := 'X';

    out_data   : out std_logic_vector(g_nof_symbols * g_symbol_w - 1 downto 0);
    out_val    : out std_logic;  -- pipelined in_val
    out_sop    : out std_logic;  -- pipelined in_sop
    out_eop    : out std_logic  -- pipelined in_eop
  );
end common_add_symbol;

architecture str of common_add_symbol is
  type t_symbol_arr  is array (integer range <>) of std_logic_vector(g_symbol_w - 1 downto 0);

  signal in_a_arr     : t_symbol_arr(g_nof_symbols - 1 downto 0);
  signal in_b_arr     : t_symbol_arr(g_nof_symbols - 1 downto 0);
  signal sum_dat_arr  : t_symbol_arr(g_nof_symbols - 1 downto 0);
  signal sum_data     : std_logic_vector(g_nof_symbols * g_symbol_w - 1 downto 0);
begin
  gen_symbols : for I in g_nof_symbols - 1 downto 0 generate
    -- map input vector to arr
    in_a_arr(I) <= in_a((I + 1) * g_symbol_w - 1 downto I * g_symbol_w);
    in_b_arr(I) <= in_b((I + 1) * g_symbol_w - 1 downto I * g_symbol_w);

    -- add per symbol
    sum_dat_arr(I) <= ADD_UVEC(in_a_arr(I), in_b_arr(I));

    -- map arr to output vector
    sum_data((I + 1) * g_symbol_w - 1 downto I * g_symbol_w) <= sum_dat_arr(I);
  end generate;

  -- pipeline data output
  u_out_data : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline,
    g_in_dat_w  => g_nof_symbols * g_symbol_w,
    g_out_dat_w => g_nof_symbols * g_symbol_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => sum_data,
    out_dat => out_data
  );

  -- pipeline control output
  u_out_val : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_val,
    out_dat => out_val
  );

  u_out_sop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sop,
    out_dat => out_sop
  );

  u_out_eop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_eop,
    out_dat => out_eop
  );
end str;
