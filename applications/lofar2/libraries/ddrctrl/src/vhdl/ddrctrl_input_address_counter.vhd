-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: Creates address by counting input valids
--
-- Description:
--  The counter starts on the first valid = '1' clockcylce, the counter stops
--  when valid = '0'.
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
--  The maximum value of the address is determend by g_tech_ddr.

library IEEE, technology_lib, tech_ddr_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use technology_lib.technology_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity ddrctrl_input_address_counter is
  generic (
    g_tech_ddr            : t_c_tech_ddr;  -- type of memory
    g_max_adr             : natural
  );
  port (
    clk                   : in  std_logic;
    rst                   : in  std_logic;
    in_sosi               : in  t_dp_sosi;  -- input data
    in_data_stopped       : in  std_logic;
    out_sosi              : out t_dp_sosi                         := c_dp_sosi_init;  -- output data
    out_adr               : out natural;
    out_bsn_adr           : out natural;
    out_data_stopped      : out std_logic
  );
end ddrctrl_input_address_counter;

architecture rtl of ddrctrl_input_address_counter is
  -- constants for readability
  constant c_data_w       : natural                               := func_tech_ddr_ctlr_data_w( g_tech_ddr );  -- the with of the input data and output data, 576

  -- type for statemachine
  type t_state is (RESET, COUNTING, MAX, IDLE);

  -- record for readability
  type t_reg is record
  state                   : t_state;
  bsn_passed              : std_logic;
  out_sosi                : t_dp_sosi;
  out_bsn_adr             : natural;
  out_data_stopped        : std_logic;
  s_in_sosi               : t_dp_sosi;
  s_in_data_stopped       : std_logic;
  s_adr                   : natural;
  end record;

  constant  c_t_reg_init  : t_reg                                 := (RESET, '0', c_dp_sosi_init, 0, '0', c_dp_sosi_init, '0', 0);

  -- signals for readability
  signal d_reg            : t_reg                                 := c_t_reg_init;
  signal q_reg            : t_reg                                 := c_t_reg_init;
begin
  q_reg <= d_reg when rising_edge(clk);

  -- Increments the address each time in_sosi.valid = '1', if address = g_max_adr the address is reset to 0.
  p_adr : process(rst, in_sosi, in_data_stopped, q_reg)

  variable v              : t_reg;
  begin
    v                                                             := q_reg;

    -- compensate for delay in ddrctrl_input_address_counter
    v.out_sosi                                                    := q_reg.s_in_sosi;
    v.out_data_stopped                                            := q_reg.s_in_data_stopped;
    v.s_in_sosi                                                   := in_sosi;
    v.s_in_data_stopped                                           := in_data_stopped;

    case q_reg.state is
    when RESET =>
      v := c_t_reg_init;

      if q_reg.s_in_sosi.sop = '1' then
        v.out_bsn_adr := v.s_adr;
      end if;

    when COUNTING =>
      v.s_adr := q_reg.s_adr + 1;

      if q_reg.s_in_sosi.sop = '1' then
        v.out_bsn_adr := v.s_adr;
      end if;

    when MAX =>
      v.s_adr := 0;

      if q_reg.s_in_sosi.sop = '1' then
        v.out_bsn_adr := v.s_adr;
      end if;

    when IDLE =>
    -- after a reset wait for a sop so the memory will be filled with whole blocks.
    if in_sosi.sop = '1' then
      v.bsn_passed := '1';
    end if;
    end case;

    if rst = '1' then
      v.state := RESET;
    elsif q_reg.s_adr = g_max_adr - 1 and in_sosi.valid = '1' and q_reg.bsn_passed = '1' then
      v.state := MAX;
    elsif in_sosi.valid = '1' and q_reg.bsn_passed = '1' then
      v.state := COUNTING;
    else
      v.state := IDLE;
    end if;

    d_reg <= v;
  end process;

  -- fill outputs
  out_sosi          <= q_reg.out_sosi;
  out_adr           <= q_reg.s_adr;
  out_bsn_adr       <= q_reg.out_bsn_adr;
  out_sosi.bsn      <= q_reg.out_sosi.bsn;
  out_data_stopped  <= q_reg.out_data_stopped;
end rtl;
