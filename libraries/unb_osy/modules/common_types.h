/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H

/* includes */
#include "alt_types.h"


/* public constants */

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

#define HALF_WORD_SZ       2
#define WORD_SZ            4
#define LONG_WORD_SZ       8
#define NIBBLE_W           4
#define BYTE_W             8
#define OCTET_W            8
#define HALF_WORD_W       (BYTE_W * HALF_WORD_SZ)
#define WORD_W            (BYTE_W * WORD_SZ)
#define LONG_WORD_W       (BYTE_W * LONG_WORD_SZ)


/* public macros */


/* public types */

/* Map to
 * %QUARTUS_ROOTDIR%/../ip/altera/nios2_ip/altera_nios2/HAL/inc/alt_type.h
 */
typedef alt_32             TBool;        // b
typedef alt_8              TInt8;        // c  : signed char
typedef alt_16             TInt16;       // i  : signed short
typedef alt_32             TInt32;       // l  : signed long
typedef alt_64             TInt64;       // h  : signed long long
typedef alt_u8             TUInt8;       // uc : unsigned char
typedef alt_u16            TUInt16;      // ui : unsigned short
typedef alt_u32            TUInt32;      // ul : unsigned long
typedef alt_u64            TUInt64;      // uh : unsigned long long

typedef float              TFloat32;     // f
typedef double             TFloat64;     // d
typedef char               TStr8[9];     // sz
typedef char               TStr32[33];   // sz
typedef char               TStr64[65];   // sz
typedef char               TStr128[129]; // sz
typedef char               TStr256[257]; // sz
typedef char               TStr512[513]; // sz


/* public variables */

/* public function protos */

#endif

