-------------------------------------------------------------------------------
--
-- Copyright (C) 2012-2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use i2c_lib.i2c_smbus_pkg.all;
use i2c_lib.i2c_dev_unb2_pkg.all;
use common_lib.common_pkg.all;

entity unb2b_board_pmbus_ctrl is
  generic (
    g_sim        : boolean := false;
    g_nof_result : natural := 42;
    g_temp_high  : natural := 85
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    start      : in  std_logic;  -- pulse to start the I2C sequence to read out the sensors
    out_dat    : out std_logic_vector(c_byte_w - 1 downto 0);
    out_val    : out std_logic;
    in_dat     : in  std_logic_vector(c_byte_w - 1 downto 0);
    in_val     : in  std_logic;
    in_err     : in  std_logic;
    in_ack     : in  std_logic;
    in_end     : in  std_logic;
    result_val : out std_logic;
    result_err : out std_logic;
    result_dat : out t_slv_8_arr(0 to g_nof_result - 1)
  );
end entity;

architecture rtl of unb2b_board_pmbus_ctrl is
  type t_SEQUENCE is array (natural range <>) of natural;

  -- The I2C bit rate is c_i2c_bit_rate = 50 [kbps], so 20 us period. Hence 20 us wait time for SDA is enough
  -- Assume clk <= 200 MHz, so 5 ns period. Hence timeout of 4000 is enough.
  constant c_timeout_sda : natural := sel_a_b(g_sim, 0, 16);  -- wait 16 * 256 = 4096 clk periods

  constant c_SEQ : t_SEQUENCE := (
    SMBUS_READ_BYTE,  I2C_UNB2_PMB_TCVR0_BMR461_ADR, PMBUS_REG_READ_VOUT_MODE,  -- RX supply
    SMBUS_READ_WORD,  I2C_UNB2_PMB_TCVR0_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_TCVR0_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_TCVR0_BMR461_ADR, PMBUS_REG_READ_TEMP,

    SMBUS_READ_BYTE,  I2C_UNB2_PMB_TCVR1_BMR461_ADR, PMBUS_REG_READ_VOUT_MODE,  -- TX supply
    SMBUS_READ_WORD,  I2C_UNB2_PMB_TCVR1_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_TCVR1_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_TCVR1_BMR461_ADR, PMBUS_REG_READ_TEMP,

    SMBUS_READ_BYTE,  I2C_UNB2_PMB_CORE_BMR464_ADR, PMBUS_REG_READ_VOUT_MODE,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_CORE_BMR464_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_CORE_BMR464_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_CORE_BMR464_ADR, PMBUS_REG_READ_TEMP,

    SMBUS_READ_BYTE,  I2C_UNB2_PMB_CTRL_BMR461_ADR, PMBUS_REG_READ_VOUT_MODE,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_CTRL_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_CTRL_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_CTRL_BMR461_ADR, PMBUS_REG_READ_TEMP,

    SMBUS_READ_BYTE,  I2C_UNB2_PMB_FPGAIO_BMR461_ADR, PMBUS_REG_READ_VOUT_MODE,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_FPGAIO_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_FPGAIO_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_FPGAIO_BMR461_ADR, PMBUS_REG_READ_TEMP,

    SMBUS_READ_BYTE,  I2C_UNB2_PMB_VCCRAM_BMR461_ADR, PMBUS_REG_READ_VOUT_MODE,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_VCCRAM_BMR461_ADR, PMBUS_REG_READ_VOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_VCCRAM_BMR461_ADR, PMBUS_REG_READ_IOUT,
    SMBUS_READ_WORD,  I2C_UNB2_PMB_VCCRAM_BMR461_ADR, PMBUS_REG_READ_TEMP,

    SMBUS_C_SAMPLE_SDA, 0, c_timeout_sda, 0, 0,
    SMBUS_C_END,
    SMBUS_C_NOP
  );

  constant c_seq_len : natural := c_SEQ'length - 1;

  -- The protocol list c_SEQ yields a list of result bytes (result_dat)
  -- make sure that g_nof_result matches the number of result bytes

  signal start_reg       : std_logic;

  signal seq_cnt         : natural range 0 to c_seq_len := c_seq_len;
  signal nxt_seq_cnt     : natural;

  signal rx_cnt          : natural range 0 to g_nof_result;
  signal nxt_rx_cnt      : natural;

  signal rx_val          : std_logic;
  signal nxt_rx_val      : std_logic;
  signal rx_err          : std_logic;
  signal nxt_rx_err      : std_logic;
  signal rx_dat          : t_slv_8_arr(result_dat'range);
  signal nxt_rx_dat      : t_slv_8_arr(result_dat'range);
  signal nxt_result_val  : std_logic;
  signal nxt_result_err  : std_logic;
  signal i_result_dat    : t_slv_8_arr(result_dat'range);
  signal nxt_result_dat  : t_slv_8_arr(result_dat'range);
begin
  result_dat <= i_result_dat;

  regs: process(rst, clk)
  begin
    if rst = '1' then
      start_reg     <= '0';
      seq_cnt       <= c_seq_len;
      rx_cnt        <= 0;
      rx_val        <= '0';
      rx_err        <= '0';
      rx_dat        <= (others => (others => '0'));
      result_val    <= '0';
      result_err    <= '0';
      i_result_dat  <= (others => (others => '0'));
    elsif rising_edge(clk) then
      start_reg     <= start;
      seq_cnt       <= nxt_seq_cnt;
      rx_cnt        <= nxt_rx_cnt;
      rx_val        <= nxt_rx_val;
      rx_err        <= nxt_rx_err;
      rx_dat        <= nxt_rx_dat;
      result_val    <= nxt_result_val;
      result_err    <= nxt_result_err;
      i_result_dat  <= nxt_result_dat;
    end if;
  end process;

  -- Issue the protocol list
  p_seq_cnt : process(seq_cnt, start_reg, in_ack)
  begin
    nxt_seq_cnt <= seq_cnt;
    if start_reg = '1' then
      nxt_seq_cnt <= 0;
    elsif seq_cnt < c_seq_len and in_ack = '1' then
      nxt_seq_cnt <= seq_cnt + 1;
    end if;
  end process;

  out_dat <= std_logic_vector(TO_UVEC(c_SEQ(seq_cnt), c_byte_w));
  out_val <= '1' when seq_cnt < c_seq_len else '0';

  -- Fill the rx_dat byte array
  p_rx_dat : process(start_reg, rx_err, in_err, rx_dat, rx_cnt, in_dat, in_val)
  begin
    nxt_rx_err <= rx_err;
    if start_reg = '1' then
      nxt_rx_err <= '0';
    elsif in_err = '1' then
      nxt_rx_err <= '1';
    end if;

    nxt_rx_dat <= rx_dat;
    nxt_rx_cnt <= rx_cnt;
    if start_reg = '1' then
      nxt_rx_dat <= (others => (others => '0'));
      nxt_rx_cnt <= 0;
    elsif in_val = '1' then
      nxt_rx_dat(rx_cnt) <= in_dat;
      nxt_rx_cnt         <= rx_cnt + 1;
    end if;
  end process;

  nxt_rx_val <= in_end;

  -- Capture the complete rx_dat byte array
  nxt_result_val <= rx_val;
  nxt_result_err <= rx_err;
  nxt_result_dat <= rx_dat when rx_val = '1' else i_result_dat;
end rtl;
