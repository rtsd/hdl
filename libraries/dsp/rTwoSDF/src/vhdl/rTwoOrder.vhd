--------------------------------------------------------------------------------
--   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
--   Copyright (C) 2009-2011
--   ASTRON (Netherlands Institute for Radio Astronomy)
--   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
--   This file is part of the UniBoard software suite.
--   The file is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

library ieee, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity rTwoOrder is
  generic   (
    g_nof_points  : natural := 8;
    g_bit_flip    : boolean := true;
    g_nof_chan    : natural := 0
  );
  port (
    clk     : in  std_logic;
    rst     : in  std_logic;
    in_dat  : in  std_logic_vector;
    in_val  : in  std_logic;
    out_dat : out std_logic_vector;
    out_val : out std_logic
  );
end entity rTwoOrder;

architecture rtl of rTwoOrder is
  constant c_nof_channels : natural := 2**g_nof_chan;
  constant c_dat_w        : natural := in_dat'length;
  constant c_page_size    : natural := g_nof_points * c_nof_channels;
  constant c_adr_points_w : natural := ceil_log2(g_nof_points);
  constant c_adr_chan_w   : natural := g_nof_chan;
  constant c_adr_tot_w    : natural := c_adr_points_w + c_adr_chan_w;

  signal adr_points_cnt   : std_logic_vector(c_adr_points_w - 1 downto 0);
  signal adr_chan_cnt     : std_logic_vector(c_adr_chan_w   - 1 downto 0);
  signal adr_tot_cnt      : std_logic_vector(c_adr_tot_w    - 1 downto 0);

  signal in_init          : std_logic;
  signal nxt_in_init      : std_logic;
  signal in_en            : std_logic;

  signal cnt_ena   : std_logic;

  signal next_page        : std_logic;

  signal wr_en            : std_logic;
  signal wr_adr           : std_logic_vector(c_adr_tot_w - 1 downto 0);
  signal wr_dat           : std_logic_vector(c_dat_w - 1 downto 0);

  signal rd_en            : std_logic;
  signal rd_adr           : std_logic_vector(c_adr_tot_w - 1 downto 0);
  signal rd_dat           : std_logic_vector(c_dat_w - 1 downto 0);
  signal rd_val           : std_logic;
begin
  out_dat <= rd_dat;
  out_val <= rd_val;

  p_clk : process(clk, rst)
  begin
    if rst = '1' then
      in_init      <= '1';
    elsif rising_edge(clk) then
      in_init      <= nxt_in_init;
    end if;
  end process;

  nxt_in_init <= '0' when next_page = '1' else in_init;  -- keep in_init active until the first block has been written

  in_en <= not in_init;  -- disable reading of the first block for convenience in verification, because it contains undefined values

  wr_dat   <= in_dat;
  wr_en    <= in_val;
  rd_en    <= in_val and in_en;

  next_page <= '1' when unsigned(adr_tot_cnt) = c_page_size-1  and wr_en = '1' else '0';

  adr_tot_cnt <= adr_chan_cnt & adr_points_cnt;

  gen_bit_flip : if g_bit_flip = true generate
    wr_adr <= adr_chan_cnt & flip(adr_points_cnt);  -- flip the addresses to perform the reorder
  end generate;

  no_bit_flip : if g_bit_flip = false generate
    wr_adr <= adr_tot_cnt;  -- do not flip the addresses for easier debugging with tb_rTwoOrder
  end generate;

  rd_adr <= adr_tot_cnt;

  u_adr_point_cnt : entity common_lib.common_counter
  generic map(
    g_latency   => 1,
    g_init      => 0,
    g_width     => ceil_log2(g_nof_points)
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_en  => cnt_ena,
    count   => adr_points_cnt
  );

  -- Generate on c_nof_channels to avoid simulation warnings on TO_UINT(adr_chan_cnt) when adr_chan_cnt is a NULL array
  one_chan : if c_nof_channels = 1 generate
    cnt_ena <= '1' when in_val = '1' else '0';
  end generate;

  more_chan : if c_nof_channels > 1 generate
    cnt_ena <= '1' when in_val = '1' and TO_UINT(adr_chan_cnt) = c_nof_channels - 1 else '0';
  end generate;

  u_adr_chan_cnt : entity common_lib.common_counter
  generic map(
    g_latency   => 1,
    g_init      => 0,
    g_width     => g_nof_chan
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_en  => in_val,
    count   => adr_chan_cnt
  );

  u_buff : entity common_lib.common_paged_ram_r_w
  generic map (
    g_str             => "use_adr",
    g_data_w          => c_dat_w,
    g_nof_pages       => 2,
    g_page_sz         => c_page_size,
    g_wr_start_page   => 0,
    g_rd_start_page   => 1,
    g_rd_latency      => 1
  )
  port map (
    rst          => rst,
    clk          => clk,
    wr_next_page => next_page,
    wr_adr       => wr_adr,
    wr_en        => wr_en,
    wr_dat       => wr_dat,
    rd_next_page => next_page,
    rd_adr       => rd_adr,
    rd_en        => rd_en,
    rd_dat       => rd_dat,
    rd_val       => rd_val
  );
end rtl;
