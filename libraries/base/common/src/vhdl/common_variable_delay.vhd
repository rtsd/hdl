-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker, Eric Kooistra
-- Purpose:
-- . Delay input pulse by delay number of cycles
-- Description:
-- . The actual pulse delay will be delay + 1, due to implementation latency
--   of 1 clk cycle
-- --------------------------------------------------------------------------

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_variable_delay is
  generic (
    g_max_delay : natural := 200 * 10**6
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;

    delay        : in  natural range 0 to g_max_delay := 0;
    enable       : in  std_logic := '0';
    in_pulse     : in  std_logic;
    out_pulse    : out std_logic
  );
end common_variable_delay;

architecture rtl of common_variable_delay is
  signal i_out_pulse       : std_logic;
  signal nxt_out_pulse     : std_logic;
  signal cnt_en            : std_logic;
  signal nxt_cnt_en        : std_logic;
  signal delay_cnt         : natural;
  signal nxt_delay_cnt     : natural;
begin
  out_pulse <= i_out_pulse;

  p_delay: process(enable, in_pulse, delay, cnt_en, delay_cnt)
  begin
    nxt_out_pulse <= '0';
    nxt_cnt_en    <= '0';
    nxt_delay_cnt <= 0;

    if enable = '1' then
      -- Use cnt_en to avoid that toggling enable causes an out_pulse
      nxt_cnt_en <= cnt_en;
      if cnt_en = '1' then
        nxt_delay_cnt <= delay_cnt + 1;
        if delay_cnt + 1 = delay then
          nxt_out_pulse <= '1';
          nxt_cnt_en <= '0';
        end if;
      else
        -- Accept new in_pulse when idle, ignore new in_pulse when busy
        if in_pulse = '1' then
          if delay = 0 then
            nxt_out_pulse <= '1';  -- out_pulse immediately
          else
            nxt_cnt_en <= '1';  -- apply out_pulse after delay
          end if;
        end if;
      end if;
    end if;
  end process;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      i_out_pulse <= '0';
      cnt_en      <= '0';
      delay_cnt   <= 0;
    elsif rising_edge(clk) then
      i_out_pulse <= nxt_out_pulse;
      cnt_en      <= nxt_cnt_en;
      delay_cnt   <= nxt_delay_cnt;
    end if;
  end process;
end rtl;
