README.txt for $HDL_WORK/libraries/technology/ip_arria10/complex_mult

1) Porting
2) IP component
3) Compilation, simulation and verification
4) Synthesis
5) Remarks


1) Porting

The complex_mult IP was ported manually from Quartus v11.1 for Stratix IV  to Quartus 15.0 for Arria10 by creating it in Qsys using
the same parameter settings.


2) IP component

The generated IP is not kept in SVN, only the Qsys source file:

  ip_arria10_complex_mult.qsys

Therefore first the IP needs to be generated using:

  ./generate_ip.sh
  

3) Compilation, simulation and verification

The generated IP also contains a msim_setup.tcl file that was used to manually create:

  compile_ip.tcl
  
This compile_ip.tcl is in the hdllib.cfg and gets compiled before the other code.


4) Synthesis

No synthesis trials were done, because this will implicitely be done when the IP is used in a design. The QIP file:

  ip_arria10_complex_mult.qip

is included in the hdllib.cfg and contains what is needed to synthesize the IP.


5) Remarks

a) Use generated IP specific library clause name

  The generated ip_arria10_<lib_name>.vhd uses an IP specific library name. Therefore the hdllib.cfg uses the IP
  specific library as library clause name to make it known:
  
    hdl_lib_name = ip_arria10_<lib_name>
    hdl_library_clause_name = ip_arria10_<lib_name>_<ip_specific>
    
 