--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;

-- Purpose: Transmit a frame with PRSG or COUNTER test sequence data.
-- Description:
--   The test data can be PRSG or COUNTER dependent on diag_sel.
--   A frame generation is enabled (started) by diag_sop. It does not need
--   diag_eop to end the frame generation, because the generator ends itself
--   when it has generated g_frame_len words of out_dat.
--   After a test frame has finished then diag_ready with RL=0 is active to
--   indicate that a new diag_sof can be handled. The next frame will then
--   immediatly follow the previous frame, so out_sop of new frame direct after
--   out_eop of previous frame, if allowed by out_ready from the sink.
--   It is allowed to keep diag_sop continue high or to make diag_sop =
--   diag_ready, both will result in maximum frame rate.
--   The out_ready acts as a data request. During a frame the out_dat is valid
--   for every active out_ready, to support streaming flow control.
-- Remark:
-- . This component can be easily mapped to standard MM and ST interfaces.

entity diag_tx_frm is
  generic (
    g_sel       : std_logic := '1';  -- '0' = PRSG, '1' = COUNTER
    g_init      : natural := 0;  -- default test sequence init value
    g_frame_len : natural := 2;  -- >= 2, test frame length
    g_dat_w     : natural := 16  -- >= 1, test data width
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;
    clken          : in  std_logic := '1';

    -- Static control input (connect via MM or leave open to use default)
    diag_sel       : in  std_logic := g_sel;  -- '0' = PRSG sequence data, '1' = COUNTER sequence data
    diag_dc        : in  std_logic := '0';  -- '0' = output diag_sel sequence data, '1' = output constant diag_init data
    diag_frame_len : in  std_logic_vector(ceil_log2(g_frame_len) - 1 downto 0) := TO_UVEC(g_frame_len, ceil_log2(g_frame_len));

    -- Dynamic control input (connect via MM or via ST input or leave open to use defaults)
    diag_ready     : out std_logic;  -- '1' when the generator can accept a new diag_sop
    diag_init      : in  std_logic_vector(g_dat_w - 1 downto 0) := TO_UVEC(g_init, g_dat_w);  -- init data for out_dat when diag_sop = '1'
    diag_sop       : in  std_logic := '1';  -- '1' start a frame generation

    -- ST output
    out_ready      : in  std_logic := '1';  -- '1' = request ST test data output, '0' = halt ST test data output
    out_dat        : out std_logic_vector(g_dat_w - 1 downto 0);  -- test sequence data
    out_val        : out std_logic;  -- '1' when out_data is valid
    out_sop        : out std_logic;  -- '1' for first valid frame data
    out_eop        : out std_logic  -- '1' for last  valid frame data
  );
end diag_tx_frm;

architecture rtl of diag_tx_frm is
  constant c_lfsr_nr          : natural := g_dat_w - c_common_lfsr_first;

  signal i_diag_ready    : std_logic;

  signal frm_sop         : std_logic;
  signal frm_busy        : std_logic;
  signal nxt_frm_busy    : std_logic;

  signal diag_en         : std_logic;

  signal cnt             : std_logic_vector(diag_frame_len'range);
  signal cnt_clr         : std_logic;
  signal cnt_en          : std_logic;
  signal cnt_done        : std_logic;

  signal nxt_out_val     : std_logic;
  signal nxt_out_sop     : std_logic;
  signal nxt_out_eop     : std_logic;
begin
  diag_ready <= i_diag_ready;

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      frm_busy        <= '0';
      out_val         <= '0';
      out_sop         <= '0';
      out_eop         <= '0';
    elsif rising_edge(clk) then
      if clken = '1' then
        frm_busy        <= nxt_frm_busy;
        out_val         <= nxt_out_val;
        out_sop         <= nxt_out_sop;
        out_eop         <= nxt_out_eop;
      end if;
    end if;
  end process;

  i_diag_ready <= not diag_en;

  -- Use frm_sop to support diag_sop pulse for frame rate control and also
  -- diag_sop continue high for maximum frame rate.
  frm_sop <= i_diag_ready and diag_sop;

  nxt_frm_busy <= '1' when frm_sop  = '1' else
                  '0' when cnt_done = '1' else frm_busy;

  cnt_clr     <= frm_sop;
  cnt_en      <= '1' when frm_busy = '1' and out_ready = '1' and unsigned(cnt) <= g_frame_len - 1 else '0';
  cnt_done    <= '1' when frm_busy = '1' and out_ready = '1' and unsigned(cnt) = g_frame_len - 1 else '0';

  -- Distinguish between frm_busy and diag_en to be able to generated frames
  -- that are output without idle cycles in between. The diag_en therefore
  -- make uses of the feature of diag_tx_seq that the next sequence data is
  -- already available at its output when out_val is still '0'.
  diag_en     <= frm_busy and not cnt_done;

  nxt_out_val <= cnt_en;
  nxt_out_sop <= '1' when frm_busy = '1' and out_ready = '1' and unsigned(cnt) = 0             else '0';
  nxt_out_eop <= cnt_done;

  u_cnt_len : entity common_lib.common_counter
  generic map (
    g_width => diag_frame_len'length
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    cnt_clr => cnt_clr,
    cnt_en  => cnt_en,
    count   => cnt
  );

  u_frame_dat : entity work.diag_tx_seq
  generic map (
    g_dat_w    => g_dat_w
  )
  port map (
    rst        => rst,
    clk        => clk,
    clken      => clken,
    diag_en    => diag_en,
    diag_sel   => diag_sel,
    diag_dc    => diag_dc,
    diag_init  => diag_init,
    diag_req   => out_ready,
    out_dat    => out_dat,
    out_val    => open
  );
end rtl;
