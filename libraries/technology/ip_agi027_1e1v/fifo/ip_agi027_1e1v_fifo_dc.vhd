-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author: D.F. Brouwer
-- Purpose:
--   RadioHDL wrapper / Instantiate FIFO IP with generics
-- Description:
--   Copied component declaration and instance example from generated/fifo_1921/sim/ip_agi027_1e1v_fifo_dc_fifo_1921_kss5lzq.vhd

library ieee;
use ieee.std_logic_1164.all;

library technology_lib;
use technology_lib.technology_pkg.all;

library altera_mf;
use altera_mf.all;

entity ip_agi027_1e1v_fifo_dc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
end ip_agi027_1e1v_fifo_dc;

architecture SYN of ip_agi027_1e1v_fifo_dc is
    component  dcfifo
    generic (
        -- enable_ecc  : string;
        intended_device_family  : string;
        -- lpm_hint  : string;
        lpm_numwords  : natural;
        lpm_showahead  : string;
        lpm_type  : string;
        lpm_width  : natural;
        lpm_widthu  : natural;
        overflow_checking  : string;
        rdsync_delaypipe  : natural;
        read_aclr_synch  : string;
        underflow_checking  : string;
        use_eab  : string;
        write_aclr_synch  : string;
        wrsync_delaypipe  : natural
    );
    port (
        aclr   : in std_logic;
        data   : in std_logic_vector(g_dat_w - 1 downto 0);
        rdclk   : in std_logic;
        rdreq   : in std_logic;
        wrclk   : in std_logic;
        wrreq   : in std_logic;
        q   : out std_logic_vector(g_dat_w - 1 downto 0);
        rdempty   : out std_logic;
        rdusedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0);
        wrfull   : out std_logic;
        wrusedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
    );
    end component;
begin
  u_dcfifo : dcfifo
  generic map (
    -- enable_ecc  => "FALSE",
    intended_device_family  => "Agilex 7",
    -- lpm_hint  => "DISABLE_DCFIFO_EMBEDDED_TIMING_CONSTRAINT=TRUE",
    lpm_numwords  => g_nof_words,
    lpm_showahead  => "OFF",
    lpm_type  => "dcfifo",
    lpm_width  => g_dat_w,
    lpm_widthu  => tech_ceil_log2(g_nof_words),
    overflow_checking  => "ON",
    rdsync_delaypipe  => 5,
    read_aclr_synch  => "OFF",
    underflow_checking  => "ON",
    use_eab  => g_use_eab,
    write_aclr_synch  => "ON",
    wrsync_delaypipe  => 5
  )
  port map (
    aclr => aclr,
    data => data,
    rdclk => rdclk,
    rdreq => rdreq,
    wrclk => wrclk,
    wrreq => wrreq,
    q => q,
    rdempty => rdempty,
    rdusedw => rdusedw,
    wrfull => wrfull,
    wrusedw => wrusedw
  );
end SYN;
