-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify dp_block_gen
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_block_gen is
  generic (
    g_use_src_in         : boolean := false;
    g_nof_data_per_block : natural := 11;
    g_nof_blk_per_sync   : natural := 5;
    g_enable             : t_dp_flow_control_enum := e_pulse;  -- always e_active or e_pulse block generator enable
    g_out_ready          : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    g_nof_repeat         : natural := 100
  );
end tb_dp_block_gen;

architecture tb of tb_dp_block_gen is
  constant c_rl                       : natural := 1;
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  constant c_bsn_init                 : natural := 3;

  signal tb_end             : std_logic := '0';
  signal clk                : std_logic := '1';
  signal rst                : std_logic := '1';
  signal sl1                : std_logic := '1';

  signal random             : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse              : std_logic;
  signal pulse_en           : std_logic := '1';

  signal enable             : std_logic;
  signal ready              : std_logic;

  signal ref_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal out_siso           : t_dp_siso := c_dp_siso_rdy;
  signal out_sosi           : t_dp_sosi;
  signal prev_out_sosi      : t_dp_sosi;
  signal prev_out_ready     : std_logic_vector(0 to c_rl);
  signal out_gap            : std_logic := '0';
  signal hold_sop           : std_logic := '0';
  signal exp_size           : natural := g_nof_data_per_block;
  signal cnt_size           : natural := 0;

  signal verify_en          : std_logic := '0';
  signal verify_bsn_en      : std_logic := '0';
  signal verify_done        : std_logic := '0';
  signal verify_valid       : std_logic := '0';
  signal verify_sop         : std_logic := '0';
  signal verify_eop         : std_logic := '0';
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random <= func_common_random(random) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period, '1', rst, clk, pulse_en, pulse);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  ready <= '1'                 when g_out_ready = e_active else
           random(random'high) when g_out_ready = e_random else
           pulse               when g_out_ready = e_pulse;

  ------------------------------------------------------------------------------
  -- STIMULI
  ------------------------------------------------------------------------------

  p_stimuli : process
  begin
    enable <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);
    for I in 0 to g_nof_repeat - 1 loop
      enable <= '1';
      proc_common_wait_some_cycles(clk, 1);
      if g_enable = e_pulse then
        if I mod c_pulse_period = 0 then
          enable <= '0';
        end if;
      end if;
      proc_common_wait_some_cycles(clk, 4 * g_nof_data_per_block);
    end loop;
    enable <= '0';

    -- End of stimuli
    proc_common_wait_some_cycles(clk, 100);
    proc_common_gen_pulse(clk, verify_done);
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  -- input data
  ref_sosi.data  <= INCR_UVEC(ref_sosi.data, 1) when out_siso.ready = '1' and rising_edge(clk);
  ref_sosi.re    <= INCR_UVEC(ref_sosi.re,   2) when out_siso.ready = '1' and rising_edge(clk);
  ref_sosi.im    <= INCR_UVEC(ref_sosi.im,   3) when out_siso.ready = '1' and rising_edge(clk);

  -- flow control via input valid or via output ready
  ref_sosi.valid <= '0' when g_use_src_in = true  else ready;
  out_siso.ready <= '1' when g_use_src_in = false else ready;

  ------------------------------------------------------------------------------
  -- VERIFICATION
  ------------------------------------------------------------------------------

  verify_en <= '1';

  p_verify_bsn : process
  begin
    verify_bsn_en <= '0';
    proc_common_wait_until_high(clk, enable);
    proc_common_wait_some_cycles(clk, 10);  -- first sop will have occured, so start verification for subsequent BSN
    while enable = '1' loop
      verify_bsn_en <= '1';
      wait until rising_edge(clk);
    end loop;
  end process;

  -- Verify that the stimuli have been applied at all
  verify_valid <= '1' when out_sosi.valid = '1' and rising_edge(clk);
  verify_sop   <= '1' when out_sosi.sop = '1'   and rising_edge(clk);
  verify_eop   <= '1' when out_sosi.eop = '1'   and rising_edge(clk);
  proc_dp_verify_value("out_sosi.valid", clk, verify_done, sl1, verify_valid);
  proc_dp_verify_value("out_sosi.sop",   clk, verify_done, sl1, verify_sop);
  proc_dp_verify_value("out_sosi.eop",   clk, verify_done, sl1, verify_eop);

  -- Verify some general streaming interface properties
  proc_dp_verify_valid(c_rl, clk, verify_en, out_siso.ready, prev_out_ready, out_sosi.valid);  -- Verify that the out_sosi valid fits with the ready latency
  proc_dp_verify_gap_invalid(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, out_gap);  -- Verify that the out_sosi valid is low between blocks
  proc_dp_verify_data("BSN", clk, verify_bsn_en, out_sosi.sop, out_sosi.bsn, prev_out_sosi.bsn);

  -- Verify intervals
  proc_dp_verify_sync(g_nof_blk_per_sync, c_bsn_init, clk, verify_en, out_sosi.sync, out_sosi.sop, out_sosi.bsn);

  proc_dp_verify_sop_and_eop(c_rl, clk, out_siso.ready, out_sosi.valid, out_sosi.sop, out_sosi.eop, hold_sop);

  proc_dp_verify_block_size(exp_size, clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, cnt_size);

  ------------------------------------------------------------------------------
  -- DUT : dp_block_gen
  ------------------------------------------------------------------------------

  u_dut: entity work.dp_block_gen
  generic map (
    g_use_src_in         => g_use_src_in,
    g_nof_data           => g_nof_data_per_block,
    g_nof_blk_per_sync   => g_nof_blk_per_sync,
    g_empty              => 1,
    g_channel            => 2,
    g_error              => 3,
    g_bsn                => c_bsn_init,
    g_preserve_sync      => false,
    g_preserve_bsn       => false
  )
  port map (
    rst        => rst,
    clk        => clk,
    -- Streaming sink
    snk_in     => ref_sosi,
    -- Streaming source
    src_in     => out_siso,
    src_out    => out_sosi,
    -- MM control
    en         => enable
  );
end tb;
