-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use technology_lib.technology_pkg.all;
use work.tech_mac_10g_component_pkg.all;

package tb_tech_mac_10g_pkg is
  -- Test bench supported packet data types
  constant c_tb_tech_mac_10g_data_type_symbols : natural := 0;
  constant c_tb_tech_mac_10g_data_type_counter : natural := 1;
  constant c_tb_tech_mac_10g_data_type_arp     : natural := 2;
  constant c_tb_tech_mac_10g_data_type_ping    : natural := 3;  -- over IP/ICMP
  constant c_tb_tech_mac_10g_data_type_udp     : natural := 4;  -- over IP

  function func_tech_mac_10g_header_size(data_type : natural) return natural;  -- raw ethernet: 4 header words, protocol ethernet: 11 header words

  -- Configure the 10G MAC
  procedure proc_tech_mac_10g_setup(constant c_technology        : in  natural;
                                    constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                    constant mm_offset           : in  natural;
                                    signal   mm_clk              : in  std_logic;
                                    signal   mm_miso             : in  t_mem_miso;
                                    signal   mm_mosi             : out t_mem_mosi);

  procedure proc_tech_mac_10g_setup(constant c_technology        : in  natural;
                                    constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                    signal   mm_clk              : in  std_logic;
                                    signal   mm_miso             : in  t_mem_miso;
                                    signal   mm_mosi             : out t_mem_mosi);

  procedure proc_tech_mac_10g_setup_stratixiv(constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                              constant mm_offset           : in  natural;
                                              signal   mm_clk              : in  std_logic;
                                              signal   mm_miso             : in  t_mem_miso;
                                              signal   mm_mosi             : out t_mem_mosi);

  procedure proc_tech_mac_10g_setup_arria10(constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                            constant mm_offset           : in  natural;
                                            signal   mm_clk              : in  std_logic;
                                            signal   mm_miso             : in  t_mem_miso;
                                            signal   mm_mosi             : out t_mem_mosi);

  procedure proc_tech_mac_10g_tx_packet(constant total_header    : in  t_network_total_header;
                                        constant data_len        : in  natural;  -- in symbols = octets = bytes
                                        constant c_data_type     : in  natural;  -- c_tb_tech_mac_10g_data_type_*
                                        constant c_ready_latency : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                                        constant c_nof_not_valid : in  natural;  -- when > 0 then pull tx valid low for c_nof_not_valid beats during tx
                                        signal   ff_clk          : in  std_logic;
                                        signal   ff_en           : in  std_logic;  -- similar purpose as c_nof_not_valid, but not used so pass on signal '1'
                                        signal   ff_src_in       : in  t_dp_siso;
                                        signal   ff_src_out      : out t_dp_sosi);

  -- Receive and verify packet from the 10G MAC
  procedure proc_tech_mac_10g_rx_packet(constant total_header : in  t_network_total_header;
                                        constant c_data_type  : in  natural;  -- c_tb_tech_mac_10g_data_type_*
                                        signal   ff_clk       : in  std_logic;
                                        signal   ff_snk_in    : in  t_dp_sosi;
                                        signal   ff_snk_out   : out t_dp_siso);
end tb_tech_mac_10g_pkg;

package body tb_tech_mac_10g_pkg is
  ------------------------------------------------------------------------------
  -- LOCAL ITEMS
  ------------------------------------------------------------------------------

  constant c_nof_eth_beats  : natural := c_network_total_header_64b_eth_nof_words;  -- = 3 (2.5), nof words in eth part of the header
  constant c_nof_hdr_beats  : natural := c_network_total_header_64b_nof_words;  -- = 6, nof words in the total header

  ------------------------------------------------------------------------------
  -- GLOBAL ITEMS
  ------------------------------------------------------------------------------

  function func_tech_mac_10g_header_size(data_type : natural) return natural is
  begin
    case data_type is
      when c_tb_tech_mac_10g_data_type_symbols => return c_network_total_header_64b_eth_nof_words;
      when c_tb_tech_mac_10g_data_type_counter => return c_network_total_header_64b_eth_nof_words;
      when others => null;
    end case;

    return c_network_total_header_64b_nof_words;
  end func_tech_mac_10g_header_size;

  -- Configure the 10G MAC
  procedure proc_tech_mac_10g_setup(constant c_technology        : in  natural;
                                    constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                    constant mm_offset           : in  natural;
                                    signal   mm_clk              : in  std_logic;
                                    signal   mm_miso             : in  t_mem_miso;
                                    signal   mm_mosi             : out t_mem_mosi) is
  begin
    case c_technology is
      when c_tech_stratixiv => proc_tech_mac_10g_setup_stratixiv(src_mac, mm_offset, mm_clk, mm_miso, mm_mosi);
      when c_tech_arria10_proto   => proc_tech_mac_10g_setup_arria10(  src_mac, mm_offset, mm_clk, mm_miso, mm_mosi);
      when others           => proc_tech_mac_10g_setup_stratixiv(src_mac, mm_offset, mm_clk, mm_miso, mm_mosi);  -- default to c_tech_stratixiv
    end case;
  end proc_tech_mac_10g_setup;

  procedure proc_tech_mac_10g_setup(constant c_technology        : in  natural;
                                    constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                    signal   mm_clk              : in  std_logic;
                                    signal   mm_miso             : in  t_mem_miso;
                                    signal   mm_mosi             : out t_mem_mosi) is
  begin
    proc_tech_mac_10g_setup(c_technology, src_mac, 0, mm_clk, mm_miso, mm_mosi);
  end proc_tech_mac_10g_setup;

  -- . The src_mac[47:0] = 0x123456789ABC for MAC address 12-34-56-78-9A-BC
  procedure proc_tech_mac_10g_setup_stratixiv(constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                              constant mm_offset           : in  natural;
                                              signal   mm_clk              : in  std_logic;
                                              signal   mm_miso             : in  t_mem_miso;
                                              signal   mm_mosi             : out t_mem_mosi) is
    --CONSTANT c_mac0       : INTEGER := TO_SINT(hton(src_mac(31 DOWNTO  0), 4));
    --CONSTANT c_mac1       : INTEGER := TO_SINT(hton(src_mac(47 DOWNTO 32), 2));
    constant c_mac0       : integer := TO_SINT(src_mac(31 downto  0));
    constant c_mac1       : integer := TO_SINT(src_mac(47 downto 32));
  begin
    -- MAC control
    -- . rx read
    proc_mem_mm_bus_rd(mm_offset + 16#0040#,               mm_clk, mm_miso, mm_mosi);  -- RW, rx_padcrc_control = 0x1, remove CRC (use 0x3 to also remove padding)
    proc_mem_mm_bus_rd(mm_offset + 16#0080#,               mm_clk, mm_miso, mm_mosi);  -- RW, rx_padcheck_control = 0x2, check CRC
    proc_mem_mm_bus_rd(mm_offset + 16#0800#,               mm_clk, mm_miso, mm_mosi);  -- RW, rx_frame_control = 0x3, for promiscuous (transparent) mode
                                                                                     --     [0] = 0 only receive SRC_MAC, 1 accept all unicast
                                                                                     --     [1] = 0 drop multi cast, 1 to accept all multicast
    proc_mem_mm_bus_rd(mm_offset + 16#0801#,               mm_clk, mm_miso, mm_mosi);  -- RW, rx_frame_maxlength = 1518 = 0x5EE
    proc_mem_mm_bus_rd(mm_offset + 16#0802#,               mm_clk, mm_miso, mm_mosi);  -- RW, rx_frame_addr0 = 0, e.g. 0x56789ABC,
    proc_mem_mm_bus_rd(mm_offset + 16#0803#,               mm_clk, mm_miso, mm_mosi);  -- RW, rx_frame_addr1 = 0, e.g. 0x1234, for primary SRC_MAC = 12-34-56-78-9A-BC
    proc_common_wait_some_cycles(mm_clk, 10);

    ---- . tx write
    --proc_mem_mm_bus_wr(mm_offset+16#1120#,            3, mm_clk, mm_miso, mm_mosi);  -- RW, tx_unidir_control = 3 to enable unidir and disable remote fault sequence generation
    --proc_common_wait_some_cycles(mm_clk, 10);

    -- . tx read
    proc_mem_mm_bus_rd(mm_offset + 16#1040#,               mm_clk, mm_miso, mm_mosi);  -- RW, tx_padins_control = 1, insert padding to ensure 64 bytes including CRC
    proc_mem_mm_bus_rd(mm_offset + 16#1080#,               mm_clk, mm_miso, mm_mosi);  -- RW, tx_crcins_control = 3, compute and insert CRC
    proc_mem_mm_bus_rd(mm_offset + 16#1120#,               mm_clk, mm_miso, mm_mosi);  -- RW, tx_unidir_control = 0, use 0x1 to enable unidirectional mode
    proc_mem_mm_bus_rd(mm_offset + 16#1200#,               mm_clk, mm_miso, mm_mosi);  -- RW, tx_addrins_control = 0, use 0x1 to overwrite frame SRC_MAC with tx_addrins_macaddr0,1
    proc_mem_mm_bus_rd(mm_offset + 16#1201#,               mm_clk, mm_miso, mm_mosi);  -- RW, tx_addrins_macaddr0 = 0, e.g. 0x56789ABC,
    proc_mem_mm_bus_rd(mm_offset + 16#1202#,               mm_clk, mm_miso, mm_mosi);  -- RW, tx_addrins_macaddr1 = 0, e.g. 0x1234, for frame SRC_MAC = 12-34-56-78-9A-BC
    proc_mem_mm_bus_rd(mm_offset + 16#1801#,               mm_clk, mm_miso, mm_mosi);  -- RW, tx_frame_maxlength = 1518 = 0x5EE
    proc_common_wait_some_cycles(mm_clk, 10);

    ---- . rx set primary SRC_MAC
    --proc_mem_mm_bus_wr(mm_offset+16#0802#,       c_mac0, mm_clk, mm_miso, mm_mosi);  -- src_mac(31 DOWNTO  0)
    --proc_mem_mm_bus_wr(mm_offset+16#0803#,       c_mac1, mm_clk, mm_miso, mm_mosi);  -- src_mac(47 DOWNTO 32) <-- primary SRC_MAC = 12-34-56-78-9A-BC
    --proc_mem_mm_bus_rd(mm_offset+16#0802#,               mm_clk, mm_miso, mm_mosi);  -- src_mac(31 DOWNTO  0)
    --proc_mem_mm_bus_rd(mm_offset+16#0803#,               mm_clk, mm_miso, mm_mosi);  -- src_mac(47 DOWNTO 32)
    --proc_common_wait_some_cycles(mm_clk, 10);
    --
    ---- . tx insert frame SRC_MAC
    --proc_mem_mm_bus_wr(mm_offset+16#1200#,            1, mm_clk, mm_miso, mm_mosi);  -- RW, tx_addrins_control = 1 to overwrite frame SRC_MAC with tx_addrins_macaddr0,1
    --proc_mem_mm_bus_wr(mm_offset+16#1201#,       c_mac0, mm_clk, mm_miso, mm_mosi);  -- src_mac(31 DOWNTO  0)
    --proc_mem_mm_bus_wr(mm_offset+16#1202#,       c_mac1, mm_clk, mm_miso, mm_mosi);  -- src_mac(47 DOWNTO 32) <-- frame SRC_MAC = 12-34-56-78-9A-BC
    --proc_common_wait_some_cycles(mm_clk, 10);

    wait until rising_edge(mm_clk);
  end proc_tech_mac_10g_setup_stratixiv;

  procedure proc_tech_mac_10g_setup_arria10(constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                            constant mm_offset           : in  natural;
                                            signal   mm_clk              : in  std_logic;
                                            signal   mm_miso             : in  t_mem_miso;
                                            signal   mm_mosi             : out t_mem_mosi) is
  begin
    -- The Low Latency Ethernet 10G MAC uses legacy MAC MM interface, so the register offsets of the 10Gbps Ethernet MAC still apply
    proc_tech_mac_10g_setup_stratixiv(src_mac, mm_offset, mm_clk, mm_miso, mm_mosi);
  end proc_tech_mac_10g_setup_arria10;

  -- Transmit user packet
  -- . Use word aligned payload data, so with padding inserted before the 14 byte header
  -- . Packets can be send immediately after eachother so new sop directly after last eop
  -- . The word rate is controlled by respecting ready from the MAC
  procedure proc_tech_mac_10g_tx_packet(constant total_header    : in  t_network_total_header;
                                        constant data_len        : in  natural;  -- in symbols = octets = bytes
                                        constant c_data_type     : in  natural;  -- c_tb_tech_mac_10g_data_type_*
                                        constant c_ready_latency : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                                        constant c_nof_not_valid : in  natural;  -- when > 0 then pull tx valid low for c_nof_not_valid beats during tx
                                        signal   ff_clk          : in  std_logic;
                                        signal   ff_en           : in  std_logic;  -- similar purpose as c_nof_not_valid, but not used so pass on signal '1'
                                        signal   ff_src_in       : in  t_dp_siso;
                                        signal   ff_src_out      : out t_dp_sosi) is
    constant c_arp_words_arr  : t_network_total_header_64b_arr := func_network_total_header_construct_arp( total_header.eth, total_header.arp);
    constant c_icmp_words_arr : t_network_total_header_64b_arr := func_network_total_header_construct_icmp(total_header.eth, total_header.ip, total_header.icmp);
    constant c_udp_words_arr  : t_network_total_header_64b_arr := func_network_total_header_construct_udp( total_header.eth, total_header.ip, total_header.udp);

    constant c_mod            : natural := data_len mod c_tech_mac_10g_symbols_per_beat;
    constant c_nof_data_beats : natural := data_len   / c_tech_mac_10g_symbols_per_beat + sel_a_b(c_mod, 1, 0);
    constant c_empty          : natural := sel_a_b(c_mod, c_tech_mac_10g_symbols_per_beat - c_mod, 0);
    variable v_sym            : unsigned(c_tech_mac_10g_symbol_w - 1 downto 0) := (others => '0');
    variable v_num            : unsigned(c_tech_mac_10g_data_w - 1 downto 0) := (others => '0');
    variable v_hdr_words_arr  : t_network_total_header_64b_arr;
  begin
    ff_src_out <= c_dp_sosi_rst;
    -- Wait until XON is active
    proc_common_wait_until_high(ff_clk, ff_src_in.xon);
    -- Generate this packet

    -- Select header
    case c_data_type is
      when c_tb_tech_mac_10g_data_type_arp  => v_hdr_words_arr := c_arp_words_arr;
      when c_tb_tech_mac_10g_data_type_ping => v_hdr_words_arr := c_icmp_words_arr;
      when others                           => v_hdr_words_arr := c_udp_words_arr;  -- default via UDP
    end case;

    ff_src_out.empty <= TO_DP_EMPTY(0);
    if c_data_type = c_tb_tech_mac_10g_data_type_arp then

      ----------------------------------------------------------------------------
      -- Header only frame
      ----------------------------------------------------------------------------
      -- . sop
      ff_src_out.data <= RESIZE_DP_DATA(v_hdr_words_arr(0));
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '1', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
      ff_src_out.data <= RESIZE_DP_DATA(v_hdr_words_arr(1));  -- prepare data before loop, so proc_dp_stream_ready_latency can be called at start of the loops
      for I in 2 to c_nof_hdr_beats - 2 loop
        proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
        ff_src_out.data <= RESIZE_DP_DATA(v_hdr_words_arr(I));
      end loop;
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
      -- . eop
      ff_src_out.data <= RESIZE_DP_DATA(v_hdr_words_arr(c_nof_hdr_beats - 1));
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '1', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);

    else

      ----------------------------------------------------------------------------
      -- Header for frame with payload
      ----------------------------------------------------------------------------

      -- Header
      -- . sop
      ff_src_out.data <= RESIZE_DP_DATA(v_hdr_words_arr(0));
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '1', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
      ff_src_out.data <= RESIZE_DP_DATA(v_hdr_words_arr(1));  -- prepare data before loop, so proc_dp_stream_ready_latency can be called at start of the loops
      for I in 2 to c_nof_hdr_beats - 1 loop
        proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
        ff_src_out.data <= RESIZE_DP_DATA(v_hdr_words_arr(I));
      end loop;

      -- Payload
      for I in 0 to c_nof_data_beats - 1 loop
        proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
        case c_data_type is
          when c_tb_tech_mac_10g_data_type_counter =>
            -- data : X"00000000_00000001", X"00000000_00000002", X"00000000_00000003", etc
            v_num := v_num + 1;
            ff_src_out.data <= RESIZE_DP_DATA(std_logic_vector(v_num));
          when others =>
            -- data : X"01020304_05060708", X"090A0B0C_0D0E0F10", X"11121314_15161718", etc
            for J in c_tech_mac_10g_symbols_per_beat - 1 downto 0 loop
              v_sym := v_sym + 1;
              ff_src_out.data((J + 1) * c_tech_mac_10g_symbol_w - 1 downto J * c_tech_mac_10g_symbol_w) <= std_logic_vector(v_sym);
            end loop;
        end case;
        -- tb : pull valid low for some time during the middle of the payload
        if c_nof_not_valid > 0 and I = c_nof_data_beats / 2 then
          ff_src_out.valid <= '0';
          for I in 0 to c_nof_not_valid loop wait until rising_edge(ff_clk); end loop;
          ff_src_out.valid <= '1';
        end if;
      end loop;

      -- Last data
      if c_empty > 0 then
        -- Overwrite empty data
        ff_src_out.empty <= TO_DP_EMPTY(c_empty);
        for J in c_empty - 1 downto 0 loop
          ff_src_out.data((J + 1) * c_tech_mac_10g_symbol_w - 1 downto J * c_tech_mac_10g_symbol_w) <= (others => '0');
        end loop;
      end if;
      -- . eop
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '1', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);

    end if;
  end proc_tech_mac_10g_tx_packet;

  -- Receive packet
  -- . Use word aligned payload data, so with padding inserted before the 14 byte header
  -- . Packets can be always be received, assume the user application is always ready
  -- . The CRC32 is also passed on to the user at eop.
  -- . Note that when empty/=0 then the CRC32 is not word aligned, so therefore use prev_data to be able
  --   to handle part of last data word in case empty/=0 at eop
  procedure proc_tech_mac_10g_rx_packet(constant total_header : in  t_network_total_header;
                                        constant c_data_type  : in  natural;  -- c_tb_tech_mac_10g_data_type_*
                                        signal   ff_clk       : in  std_logic;
                                        signal   ff_snk_in    : in  t_dp_sosi;
                                        signal   ff_snk_out   : out t_dp_siso) is
    constant c_eth_header     : t_network_eth_header := total_header.eth;
    constant c_arp_words_arr  : t_network_total_header_64b_arr := func_network_total_header_construct_arp( total_header.eth, total_header.arp);
    constant c_icmp_words_arr : t_network_total_header_64b_arr := func_network_total_header_construct_icmp(total_header.eth, total_header.ip, total_header.icmp);
    constant c_udp_words_arr  : t_network_total_header_64b_arr := func_network_total_header_construct_udp( total_header.eth, total_header.ip, total_header.udp);
    variable v_sym            : unsigned(c_tech_mac_10g_symbol_w - 1 downto 0) := (others => '0');
    variable v_num            : unsigned(c_tech_mac_10g_data_w - 1 downto 0) := (others => '0');
    variable v_empty          : natural;
    variable v_first          : boolean := true;
    variable v_data           : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);
    variable v_prev_data      : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);
    variable v_hdr_string     : string(1 to 11);
    variable v_hdr_words_arr  : t_network_total_header_64b_arr;
  begin
    -- Select header
    case c_data_type is
      when c_tb_tech_mac_10g_data_type_arp  => v_hdr_string := "ETH/ARP    "; v_hdr_words_arr := c_arp_words_arr;
      when c_tb_tech_mac_10g_data_type_ping => v_hdr_string := "ETH/IP/ICMP"; v_hdr_words_arr := c_icmp_words_arr;
      when others                           => v_hdr_string := "ETH/IP/UDP "; v_hdr_words_arr := c_udp_words_arr;  -- default via UDP
    end case;

    -- Keep ff_rx_snk_out.ready='1' and ff_rx_snk_out.xon='1' all the time
    ff_snk_out <= c_dp_siso_rdy;

    ----------------------------------------------------------------------------
    -- Verify ETH header part
    ----------------------------------------------------------------------------
    -- . wait for sop
    proc_dp_stream_valid_sop(ff_clk, ff_snk_in.valid, ff_snk_in.sop);
    assert ff_snk_in.data(63 downto 16) = X"00000000_0000"
      report "RX: Wrong ETH alignment padding not zero"
      severity ERROR;
    assert ff_snk_in.data(15 downto 0) = c_eth_header.dst_mac(47 downto 32)
      report "RX: Wrong ETH dst_mac_addr(47 downto 32)"
      severity ERROR;
    proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
    assert ff_snk_in.data(63 downto 32) = c_eth_header.dst_mac(31 downto 0)
      report "RX: Wrong ETH dst_mac_addr(31 downto 0)"
      severity ERROR;
    assert ff_snk_in.data(31 downto 0) = c_eth_header.src_mac(47 downto 16)
      report "RX: Wrong ETH src_mac_addr(47 downto 16)"
      severity ERROR;
    proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
    assert ff_snk_in.data(63 downto 48) = c_eth_header.src_mac(15 downto 0)
      report "RX: Wrong ETH src_mac_addr(15 downto 0)"
      severity ERROR;
    assert ff_snk_in.data(47 downto 32) = c_eth_header.eth_type
      report "RX: Wrong ETH ethertype"
      severity ERROR;

    -- . verify remaining half word
    assert ff_snk_in.data(31 downto 0) = v_hdr_words_arr(c_nof_eth_beats - 1)(31 downto 0)
      report "RX: Wrong " & v_hdr_string & " response half word"
      severity ERROR;

    if c_data_type = c_tb_tech_mac_10g_data_type_arp then

      ----------------------------------------------------------------------------
      -- Verify header only frame
      ----------------------------------------------------------------------------

      for I in c_nof_eth_beats to c_nof_hdr_beats - 1 loop
        proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
        assert ff_snk_in.data(63 downto 0) = v_hdr_words_arr(I)
          report "RX: Wrong " & v_hdr_string & " response word"
          severity ERROR;
      end loop;
      -- . continue to eop
      while ff_snk_in.eop /= '1' loop
        proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
      end loop;

    else

      ----------------------------------------------------------------------------
      -- Verify header and payload
      ----------------------------------------------------------------------------

      -- Header
      for I in c_nof_eth_beats to c_nof_hdr_beats - 1 loop
        proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
        if I /= c_network_total_header_64b_ip_header_checksum_wi then  -- do not verify tx ip header checksum
          assert ff_snk_in.data(63 downto 0) = v_hdr_words_arr(I)
            report "RX: Wrong " & v_hdr_string & " response word"
            severity ERROR;
        end if;
      end loop;

      if ff_snk_in.eop = '1' then
        -- Header only, no padding and no CRC (eg. for ST loopback already before MAC)
        null;
      else
        -- Payload
        -- . continue to eop
        v_first := true;
        v_data  := (others => '0');  -- proc_tech_mac_10g_tx_packet() data increments from 0
        proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
        while ff_snk_in.eop /= '1' loop
          v_prev_data := v_data;
          v_data      := ff_snk_in.data(63 downto 0);
          if v_first = false then
            case c_data_type is
              when c_tb_tech_mac_10g_data_type_counter =>
                -- data : X"00000000_00000001", X"00000000_00000002", X"00000000_00000003", etc
                v_num := v_num + 1;
                if unsigned(v_prev_data) /= 0 then  -- do not verify zero padding
                  assert unsigned(v_prev_data) = v_num
                    report "RX: Wrong data word"
                    severity ERROR;
                end if;
              when others =>
                -- data : X"01020304_05060708", X"090A0B0C_0D0E0F10", X"11121314_15161718", etc
                for J in c_tech_mac_10g_symbols_per_beat - 1 downto 0 loop
                  v_sym := v_sym + 1;
                  if unsigned(v_prev_data((J + 1) * c_tech_mac_10g_symbol_w - 1 downto J * c_tech_mac_10g_symbol_w)) /= 0 then  -- do not verify zero padding
                    assert unsigned(v_prev_data((J + 1) * c_tech_mac_10g_symbol_w - 1 downto J * c_tech_mac_10g_symbol_w)) = v_sym
                      report "RX: Wrong data symbol"
                      severity ERROR;
                    v_sym := v_sym + J;
                    exit;
                  end if;
                end loop;
            end case;
          end if;
          v_first := false;
          proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
        end loop;

        -- Verify last data and CRC32 if empty<4 else the last word is only the CRC32
        v_prev_data := v_data;
        v_data      := ff_snk_in.data(63 downto 0);
        v_empty     := to_integer(unsigned(ff_snk_in.empty(c_tech_mac_10g_empty_w - 1 downto 0)));
        --IF v_empty < 4 THEN
          for J in v_empty - 1 downto 0 loop
            v_prev_data((J + 1) * c_tech_mac_10g_symbol_w - 1 downto J * c_tech_mac_10g_symbol_w) := (others => '0');
          end loop;
          case c_data_type is
            when c_tb_tech_mac_10g_data_type_counter =>
              -- data : X"00000000_00000001", X"00000000_00000002", X"00000000_00000003", etc
              v_num := v_num + 1;
              for J in v_empty - 1 downto 0 loop
                v_num((J + 1) * c_tech_mac_10g_symbol_w - 1 downto J * c_tech_mac_10g_symbol_w) := (others => '0');  -- force CRC32 symbols in last data word to 0
              end loop;
              if unsigned(v_prev_data) /= 0 then  -- do not verify zero padding
                assert unsigned(v_prev_data) = v_num
                  report "RX: Wrong empty data word"
                  severity ERROR;
              end if;
            when others =>
              -- data : X"01020304_05060708", X"090A0B0C_0D0E0F10", X"11121314_15161718", etc
              for J in c_tech_mac_10g_symbols_per_beat - 1 downto v_empty loop  -- ignore CRC32 symbols in last data word
                v_sym := v_sym + 1;
                if unsigned(v_prev_data((J + 1) * c_tech_mac_10g_symbol_w - 1 downto J * c_tech_mac_10g_symbol_w)) /= 0 then  -- do not verify zero padding
                  assert unsigned(v_prev_data((J + 1) * c_tech_mac_10g_symbol_w - 1 downto J * c_tech_mac_10g_symbol_w)) = v_sym
                    report "RX: Wrong empty data symbol"
                    severity ERROR;
                  v_sym := v_sym + J;
                  exit;
                end if;
              end loop;
          end case;
        --END IF;
      end if;
      -- No verify on CRC32 word
    end if;
  end proc_tech_mac_10g_rx_packet;
end tb_tech_mac_10g_pkg;
