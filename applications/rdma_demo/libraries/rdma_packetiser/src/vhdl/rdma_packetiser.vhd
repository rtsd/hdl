-------------------------------------------------------------------------------
--
-- Copyright 2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Packetizes an incoming data stream as RDMA "WRITE" packets.
-- Description: see https://support.astron.nl/confluence/x/urT-Bg
-- Note: Due to the extra headers being prepended and the latency
--       that is introduced, a gap size of 10 clock cycles between
--       packets is necessary.
library IEEE, common_lib, dp_lib, eth_lib, rdma_icrc_external_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use dp_lib.dp_components_pkg.all;
  use eth_lib.eth_pkg.all;
  use eth_lib.eth_tester_pkg.all;
  use work.rdma_packetiser_pkg.all;

entity rdma_packetiser is
  generic (
    g_cross_clock_domain : boolean := false;
    g_sync_timeout       : natural := c_dp_sync_timeout
  );
  port (
    -- Clocks and reset
    dp_clk             : in  std_logic;
    dp_rst             : in  std_logic;

    mm_clk             : in  std_logic;
    mm_rst             : in  std_logic;

    reg_hdr_dat_copi   : in  t_mem_copi := c_mem_copi_rst;
    reg_hdr_dat_cipo   : out t_mem_cipo;

    snk_in             : in  t_dp_sosi := c_dp_sosi_rst;
    snk_out            : out t_dp_siso := c_dp_siso_rdy;

    src_out            : out t_dp_sosi := c_dp_sosi_rst;
    src_in             : in  t_dp_siso := c_dp_siso_rdy;

    block_len          : in  std_logic_vector(c_halfword_w - 1 downto 0) -- in octets
  );
end rdma_packetiser;

architecture str of rdma_packetiser is
  constant c_nof_byte              : natural := 64;
  constant c_data_w                : natural := c_nof_byte * c_byte_w;
  constant c_max_packet_size       : natural := ceil_div(9000, c_nof_byte);
  -- dp_offload_tx_v3 needs 8 clock cycles (minimum) to prepend headers
  -- even if the header is < 8 words
  constant c_latency_dp_offload_tx : natural := 8;
  constant c_dp_fifo_latency       : natural := 3;
  constant c_fifo_size             : natural := c_dp_fifo_latency + c_latency_dp_offload_tx;

  signal eth_hdr              : std_logic_vector(1023 downto 0) := (others => '0');
  signal dp_fifo_data_sosi    : t_dp_sosi := c_dp_sosi_rst;
  signal dp_fifo_data_siso    : t_dp_siso := c_dp_siso_rdy;
  signal dp_fifo_eth_hdr_sosi : t_dp_sosi := c_dp_sosi_rst;
  signal dp_fifo_eth_hdr_siso : t_dp_siso := c_dp_siso_rdy;
  signal assemble_header_sosi : t_dp_sosi := c_dp_sosi_rst;
  signal assemble_header_siso : t_dp_siso := c_dp_siso_rdy;
  signal icrc_sosi            : t_dp_sosi := c_dp_sosi_rst;
  signal icrc_siso            : t_dp_siso := c_dp_siso_rdy;

begin
  -- dp_fifo for prepending RDMA headers. dp_offload_tx_v3 needs 8 clock cycles
  -- (minimum) to prepend headers even if the header is < 8 words.
  u_dp_fifo_data : entity dp_lib.dp_fifo_sc
  generic map (
    g_data_w         => c_data_w,
    g_empty_w        => c_byte_w,
    g_bsn_w          => 64,
    g_use_empty      => true,
    g_use_bsn        => true,
    g_use_sync       => true,
    g_fifo_size      => c_fifo_size,
    g_fifo_rl        => 1,
    g_fifo_af_margin => 0,
    g_fifo_af_xon    => 0
  )
  port map (
    clk     => dp_clk,
    rst     => dp_rst,
    snk_in  => snk_in,
    snk_out => snk_out,
    src_out => dp_fifo_data_sosi,
    src_in  => dp_fifo_data_siso
  );

  u_rdma_assemble_header : entity work.rdma_packetiser_assemble_header
  generic map (
    g_data_w => c_data_w
  )
  port map (
    -- Clocks and reset
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,

    -- Streaming interfaces
    snk_in           => dp_fifo_data_sosi,
    snk_out          => dp_fifo_data_siso,
    src_out          => assemble_header_sosi,
    src_in           => assemble_header_siso,

    -- Memory Mapped Peripherals
    reg_hdr_dat_copi => reg_hdr_dat_copi,
    reg_hdr_dat_cipo => reg_hdr_dat_cipo,

    -- block length in nof octets
    block_len        => block_len,

    -- ETH header to be prepended after icrc computation
    eth_hdr          => eth_hdr
  );

  u_icrc_append : entity rdma_icrc_external_lib.append_crc_dp_wrapper
  port map (
    dp_clk => dp_clk,
    dp_rst => dp_rst,

    in_dp_sosi  => assemble_header_sosi,
    in_dp_siso  => assemble_header_siso,

    out_dp_sosi => icrc_sosi,
    out_dp_siso => icrc_siso
  );

  -- dp_fifo after icrc as icrc_append cannot handle backpressure.
  -- also buffer for prepending the eth header, .
  u_dp_fifo_eth_hdr : entity dp_lib.dp_fifo_sc
  generic map (
    g_data_w         => c_data_w,
    g_empty_w        => c_byte_w,
    g_bsn_w          => 64,
    g_use_empty      => true,
    g_use_bsn        => true,
    g_use_sync       => true,
    g_fifo_size      => c_fifo_size,
    g_fifo_rl        => 1,
    g_fifo_af_margin => 0,
    g_fifo_af_xon    => 0
  )
  port map (
    clk     => dp_clk,
    rst     => dp_rst,
    snk_in  => icrc_sosi,
    snk_out => icrc_siso,
    src_out => dp_fifo_eth_hdr_sosi,
    src_in  => dp_fifo_eth_hdr_siso
  );

  -- dp_offload_tx_v3
  u_dp_offload_tx : entity dp_lib.dp_offload_tx_v3
  generic map (
    g_nof_streams    => 1,
    g_data_w         => c_data_w,
    g_symbol_w       => c_octet_w,
    g_hdr_field_arr  => c_rdma_packetiser_eth_hdr_field_arr,
    g_hdr_field_sel  => c_rdma_packetiser_eth_hdr_field_sel,
    g_pipeline_ready => true
  )
  port map (
    dp_rst                => dp_rst,
    dp_clk                => dp_clk,
    snk_in_arr(0)         => dp_fifo_eth_hdr_sosi,
    snk_out_arr(0)        => dp_fifo_eth_hdr_siso,
    src_out_arr(0)        => src_out,
    src_in_arr(0)         => src_in,
    hdr_fields_in_arr(0)  => eth_hdr
  );
end str;
