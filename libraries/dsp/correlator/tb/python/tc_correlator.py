#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from common import *
import matplotlib                                                                                                                       
matplotlib.use('TkAgg') 
from common_dsp import *

import test_case
import node_io
import pi_diag_data_buffer

# Purpose:
# Description:
# Usage:

NOF_INPUTS = 10 
NOF_VISIBILITIES = (NOF_INPUTS*(NOF_INPUTS+1))/2
BUFFER_WIDTH = 64

tc = test_case.Testcase('TB - ', '')
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)
db = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, nofStreams=1, ramSizePerStream=2*NOF_VISIBILITIES)

###############################################################################
# Read data buffer
# . Read NOF_VISIBILITIES accumulator values
###############################################################################
data = db.read_data_buffer(streamNr=0, n=2*NOF_VISIBILITIES, radix='uns', width=BUFFER_WIDTH, nofColumns=12)[0]

###############################################################################
# Convert the unsigned words to complex
###############################################################################
channel_data = data
for index,word in enumerate(channel_data):
    word_bits = CommonBits(word, BUFFER_WIDTH)
    im = word_bits[BUFFER_WIDTH-1:BUFFER_WIDTH/2]
    re = word_bits[BUFFER_WIDTH/2-1:0]
    print 're,im', index, re, im
    channel_data[index] = complex(re, im)

###############################################################################
# Convert binomials to complex phasor notation
###############################################################################
for index,word in enumerate(channel_data):
    channel_data[index] = complex_binomial_to_phasor(word)

###############################################################################
# Extract the phases and amplitudes from the complex data
###############################################################################
phases = []
amplitudes = []
for word in channel_data:
    amplitudes.append(word[0])
    phases.append(word[1])

################################################################################
# Re-shape the flat list into a matrix and plot the matrix
################################################################################
mat = unique_vis_to_full_matrix(phases)    
plot_matrix_color([[mat]])

