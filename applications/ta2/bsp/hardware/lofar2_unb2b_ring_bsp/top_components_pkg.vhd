-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Reinier vd Walle
-- Purpose:
-- . VHDL package for top.vhd
-- Description:
-- . Contains components instantiated by top.vhd
-- --------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

package top_components_pkg is
    component board is
        port (
            avs_eth_0_clk_export                     : out std_logic;  -- export
            avs_eth_0_irq_export                     : in  std_logic                     := 'X';  -- export
            avs_eth_0_ram_address_export             : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_ram_read_export                : out std_logic;  -- export
            avs_eth_0_ram_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_ram_write_export               : out std_logic;  -- export
            avs_eth_0_ram_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reg_address_export             : out std_logic_vector(3 downto 0);  -- export
            avs_eth_0_reg_read_export                : out std_logic;  -- export
            avs_eth_0_reg_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_reg_write_export               : out std_logic;  -- export
            avs_eth_0_reg_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reset_export                   : out std_logic;  -- export
            avs_eth_0_tse_address_export             : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_tse_read_export                : out std_logic;  -- export
            avs_eth_0_tse_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_tse_waitrequest_export         : in  std_logic                     := 'X';  -- export
            avs_eth_0_tse_write_export               : out std_logic;  -- export
            avs_eth_0_tse_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            clk_clk                                  : in  std_logic                     := 'X';  -- clk
            reset_reset_n                            : in  std_logic                     := 'X';  -- reset_n
            kernel_clk_clk                           : out std_logic;  -- clk
            kernel_reset_reset_n                     : out std_logic;  -- reset_n
            kernel_clk2x_clk                         : out std_logic;  -- clk
            kernel_cra_waitrequest                   : in  std_logic                     := 'X';  -- waitrequest
            kernel_cra_readdata                      : in  std_logic_vector(63 downto 0) := (others => 'X');  -- readdata
            kernel_cra_readdatavalid                 : in  std_logic                     := 'X';  -- readdatavalid
            kernel_cra_burstcount                    : out std_logic_vector(0 downto 0);  -- burstcount
            kernel_cra_writedata                     : out std_logic_vector(63 downto 0);  -- writedata
            kernel_cra_address                       : out std_logic_vector(29 downto 0);  -- address
            kernel_cra_write                         : out std_logic;  -- write
            kernel_cra_read                          : out std_logic;  -- read
            kernel_cra_byteenable                    : out std_logic_vector(7 downto 0);  -- byteenable
            kernel_cra_debugaccess                   : out std_logic;  -- debugaccess
            kernel_irq_irq                           : in  std_logic_vector(0 downto 0)  := (others => 'X');  -- irq
            kernel_interface_sw_reset_in_reset       : in  std_logic                     := 'X';  -- reset
            pio_pps_address_export                   : out std_logic_vector(0 downto 0);  -- export
            pio_pps_clk_export                       : out std_logic;  -- export
            pio_pps_read_export                      : out std_logic;  -- export
            pio_pps_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_pps_reset_export                     : out std_logic;  -- export
            pio_pps_write_export                     : out std_logic;  -- export
            pio_pps_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            pio_system_info_address_export           : out std_logic_vector(4 downto 0);  -- export
            pio_system_info_clk_export               : out std_logic;  -- export
            pio_system_info_read_export              : out std_logic;  -- export
            pio_system_info_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_system_info_reset_export             : out std_logic;  -- export
            pio_system_info_write_export             : out std_logic;  -- export
            pio_system_info_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            pio_wdi_external_connection_export       : out std_logic;  -- export
            reg_bsn_monitor_v2_rx_reset_export       : out std_logic;  -- export
            reg_bsn_monitor_v2_rx_clk_export         : out std_logic;  -- export
            reg_bsn_monitor_v2_rx_address_export     : out std_logic_vector(9 downto 0);  -- export
            reg_bsn_monitor_v2_rx_write_export       : out std_logic;  -- export
            reg_bsn_monitor_v2_rx_writedata_export   : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_v2_rx_read_export        : out std_logic;  -- export
            reg_bsn_monitor_v2_rx_readdata_export    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_v2_tx_reset_export       : out std_logic;  -- export
            reg_bsn_monitor_v2_tx_clk_export         : out std_logic;  -- export
            reg_bsn_monitor_v2_tx_address_export     : out std_logic_vector(9 downto 0);  -- export
            reg_bsn_monitor_v2_tx_write_export       : out std_logic;  -- export
            reg_bsn_monitor_v2_tx_writedata_export   : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_v2_tx_read_export        : out std_logic;  -- export
            reg_bsn_monitor_v2_tx_readdata_export    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_bg_ring_reset_export            : out std_logic;  -- export
            ram_diag_bg_ring_clk_export              : out std_logic;  -- export
            ram_diag_bg_ring_address_export          : out std_logic_vector(9 downto 0);  -- export
            ram_diag_bg_ring_write_export            : out std_logic;  -- export
            ram_diag_bg_ring_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            ram_diag_bg_ring_read_export             : out std_logic;  -- export
            ram_diag_bg_ring_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_scrap_reset_export                   : out std_logic;  -- export
            ram_scrap_clk_export                     : out std_logic;  -- export
            ram_scrap_address_export                 : out std_logic_vector(8 downto 0);  -- export
            ram_scrap_write_export                   : out std_logic;  -- export
            ram_scrap_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            ram_scrap_read_export                    : out std_logic;  -- export
            ram_scrap_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_bg_ring_reset_export            : out std_logic;  -- export
            reg_diag_bg_ring_clk_export              : out std_logic;  -- export
            reg_diag_bg_ring_address_export          : out std_logic_vector(2 downto 0);  -- export
            reg_diag_bg_ring_write_export            : out std_logic;  -- export
            reg_diag_bg_ring_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_diag_bg_ring_read_export             : out std_logic;  -- export
            reg_diag_bg_ring_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_xonoff_bg_reset_export            : out std_logic;  -- export
            reg_dp_xonoff_bg_clk_export              : out std_logic;  -- export
            reg_dp_xonoff_bg_address_export          : out std_logic_vector(2 downto 0);  -- export
            reg_dp_xonoff_bg_write_export            : out std_logic;  -- export
            reg_dp_xonoff_bg_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_dp_xonoff_bg_read_export             : out std_logic;  -- export
            reg_dp_xonoff_bg_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_xonoff_from_lane_reset_export     : out std_logic;  -- export
            reg_dp_xonoff_from_lane_clk_export       : out std_logic;  -- export
            reg_dp_xonoff_from_lane_address_export   : out std_logic_vector(2 downto 0);  -- export
            reg_dp_xonoff_from_lane_write_export     : out std_logic;  -- export
            reg_dp_xonoff_from_lane_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_dp_xonoff_from_lane_read_export      : out std_logic;  -- export
            reg_dp_xonoff_from_lane_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_sdp_info_address_export              : out std_logic_vector(3 downto 0);  -- export
            reg_sdp_info_clk_export                  : out std_logic;  -- export
            reg_sdp_info_read_export                 : out std_logic;  -- export
            reg_sdp_info_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_sdp_info_reset_export                : out std_logic;  -- export
            reg_sdp_info_write_export                : out std_logic;  -- export
            reg_sdp_info_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_ctrl_address_export             : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_ctrl_clk_export                 : out std_logic;  -- export
            reg_dpmm_ctrl_read_export                : out std_logic;  -- export
            reg_dpmm_ctrl_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_ctrl_reset_export               : out std_logic;  -- export
            reg_dpmm_ctrl_write_export               : out std_logic;  -- export
            reg_dpmm_ctrl_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_data_address_export             : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_data_clk_export                 : out std_logic;  -- export
            reg_dpmm_data_read_export                : out std_logic;  -- export
            reg_dpmm_data_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_data_reset_export               : out std_logic;  -- export
            reg_dpmm_data_write_export               : out std_logic;  -- export
            reg_dpmm_data_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_epcs_address_export                  : out std_logic_vector(2 downto 0);  -- export
            reg_epcs_clk_export                      : out std_logic;  -- export
            reg_epcs_read_export                     : out std_logic;  -- export
            reg_epcs_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_epcs_reset_export                    : out std_logic;  -- export
            reg_epcs_write_export                    : out std_logic;  -- export
            reg_epcs_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_temp_sens_address_export        : out std_logic_vector(2 downto 0);  -- export
            reg_fpga_temp_sens_clk_export            : out std_logic;  -- export
            reg_fpga_temp_sens_read_export           : out std_logic;  -- export
            reg_fpga_temp_sens_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_temp_sens_reset_export          : out std_logic;  -- export
            reg_fpga_temp_sens_write_export          : out std_logic;  -- export
            reg_fpga_temp_sens_writedata_export      : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_voltage_sens_address_export     : out std_logic_vector(3 downto 0);  -- export
            reg_fpga_voltage_sens_clk_export         : out std_logic;  -- export
            reg_fpga_voltage_sens_read_export        : out std_logic;  -- export
            reg_fpga_voltage_sens_readdata_export    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_voltage_sens_reset_export       : out std_logic;  -- export
            reg_fpga_voltage_sens_write_export       : out std_logic;  -- export
            reg_fpga_voltage_sens_writedata_export   : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_ctrl_address_export             : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_ctrl_clk_export                 : out std_logic;  -- export
            reg_mmdp_ctrl_read_export                : out std_logic;  -- export
            reg_mmdp_ctrl_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_ctrl_reset_export               : out std_logic;  -- export
            reg_mmdp_ctrl_write_export               : out std_logic;  -- export
            reg_mmdp_ctrl_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_data_address_export             : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_data_clk_export                 : out std_logic;  -- export
            reg_mmdp_data_read_export                : out std_logic;  -- export
            reg_mmdp_data_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_data_reset_export               : out std_logic;  -- export
            reg_mmdp_data_write_export               : out std_logic;  -- export
            reg_mmdp_data_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_remu_address_export                  : out std_logic_vector(2 downto 0);  -- export
            reg_remu_clk_export                      : out std_logic;  -- export
            reg_remu_read_export                     : out std_logic;  -- export
            reg_remu_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_remu_reset_export                    : out std_logic;  -- export
            reg_remu_write_export                    : out std_logic;  -- export
            reg_remu_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_ta2_unb2b_mm_io_reset_export         : out std_logic;  -- export
            reg_ta2_unb2b_mm_io_clk_export           : out std_logic;  -- export
            reg_ta2_unb2b_mm_io_address_export       : out std_logic_vector(7 downto 0);  -- export
            reg_ta2_unb2b_mm_io_write_export         : out std_logic;  -- export
            reg_ta2_unb2b_mm_io_writedata_export     : out std_logic_vector(31 downto 0);  -- export
            reg_ta2_unb2b_mm_io_read_export          : out std_logic;  -- export
            reg_ta2_unb2b_mm_io_readdata_export      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_ta2_unb2b_mm_io_waitrequest_export   : in  std_logic                     := 'X';  -- export
            reg_unb_pmbus_address_export             : out std_logic_vector(5 downto 0);  -- export
            reg_unb_pmbus_clk_export                 : out std_logic;  -- export
            reg_unb_pmbus_read_export                : out std_logic;  -- export
            reg_unb_pmbus_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_pmbus_reset_export               : out std_logic;  -- export
            reg_unb_pmbus_write_export               : out std_logic;  -- export
            reg_unb_pmbus_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_unb_sens_address_export              : out std_logic_vector(5 downto 0);  -- export
            reg_unb_sens_clk_export                  : out std_logic;  -- export
            reg_unb_sens_read_export                 : out std_logic;  -- export
            reg_unb_sens_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_sens_reset_export                : out std_logic;  -- export
            reg_unb_sens_write_export                : out std_logic;  -- export
            reg_unb_sens_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_wdi_address_export                   : out std_logic_vector(0 downto 0);  -- export
            reg_wdi_clk_export                       : out std_logic;  -- export
            reg_wdi_read_export                      : out std_logic;  -- export
            reg_wdi_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wdi_reset_export                     : out std_logic;  -- export
            reg_wdi_write_export                     : out std_logic;  -- export
            reg_wdi_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            rom_system_info_address_export           : out std_logic_vector(9 downto 0);  -- export
            rom_system_info_clk_export               : out std_logic;  -- export
            rom_system_info_read_export              : out std_logic;  -- export
            rom_system_info_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            rom_system_info_reset_export             : out std_logic;  -- export
            rom_system_info_write_export             : out std_logic;  -- export
            rom_system_info_writedata_export         : out std_logic_vector(31 downto 0)  -- export
        );
    end component board;

  component freeze_wrapper is
    port (
      board_kernel_clk_clk               : in   std_logic;  -- input
      board_kernel_clk2x_clk             : in   std_logic;  -- input
      board_kernel_reset_reset_n         : in   std_logic;  -- input
      board_kernel_irq_irq               : out  std_logic_vector(0 downto 0);  -- output [0:0]
      board_kernel_cra_waitrequest       : out  std_logic;  -- output
      board_kernel_cra_readdata          : out  std_logic_vector(63 downto 0);  -- output [63:0]
      board_kernel_cra_readdatavalid     : out  std_logic;  -- output
      board_kernel_cra_burstcount        : in   std_logic_vector(0 downto 0);  -- input [0:0]
      board_kernel_cra_writedata         : in   std_logic_vector(63 downto 0);  -- input [63:0]
      board_kernel_cra_address           : in   std_logic_vector(29 downto 0);  -- input [29:0]
      board_kernel_cra_write             : in   std_logic;  -- input
      board_kernel_cra_read              : in   std_logic;  -- input
      board_kernel_cra_byteenable        : in   std_logic_vector(7 downto 0);  -- input [7:0]
      board_kernel_cra_debugaccess       : in   std_logic;  -- input

      board_kernel_register_mem_address    : out std_logic_vector(6 downto 0);  -- := (others => 'X'); -- address
      board_kernel_register_mem_clken      : out std_logic;  -- := 'X';             -- clken
      board_kernel_register_mem_chipselect : out std_logic;  -- := 'X';             -- chipselect
      board_kernel_register_mem_write      : out std_logic;  -- := 'X';             -- write
      board_kernel_register_mem_readdata   : in  std_logic_vector(255 downto 0);  -- readdata
      board_kernel_register_mem_writedata  : out std_logic_vector(255 downto 0);  -- := (others => 'X'); -- writedata
      board_kernel_register_mem_byteenable : out std_logic_vector(31 downto 0);  -- := (others => 'X'); -- byteenable

      board_kernel_stream_src_10GbE_ring_0_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_ring_0_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_ring_0_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_0_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_ring_0_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_0_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_ring_1_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_ring_1_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_ring_1_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_1_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_ring_1_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_1_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_ring_2_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_ring_2_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_ring_2_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_2_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_ring_2_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_2_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_ring_3_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_ring_3_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_ring_3_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_3_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_ring_3_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_3_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_ring_4_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_ring_4_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_ring_4_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_4_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_ring_4_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_4_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_ring_5_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_ring_5_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_ring_5_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_5_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_ring_5_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_5_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_ring_6_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_ring_6_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_ring_6_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_6_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_ring_6_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_6_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_ring_7_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_ring_7_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_ring_7_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_7_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_ring_7_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_ring_7_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_qsfp_0_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_qsfp_0_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_qsfp_0_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_qsfp_0_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_qsfp_0_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_qsfp_0_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_qsfp_1_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_qsfp_1_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_qsfp_1_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_qsfp_1_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_qsfp_1_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_qsfp_1_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_qsfp_2_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_qsfp_2_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_qsfp_2_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_qsfp_2_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_qsfp_2_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_qsfp_2_ready  : in  std_logic;

      board_kernel_stream_src_10GbE_qsfp_3_data   : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_10GbE_qsfp_3_valid  : in  std_logic;
      board_kernel_stream_src_10GbE_qsfp_3_ready  : out std_logic;
      board_kernel_stream_snk_10GbE_qsfp_3_data   : out std_logic_vector(103 downto 0);
      board_kernel_stream_snk_10GbE_qsfp_3_valid  : out std_logic;
      board_kernel_stream_snk_10GbE_qsfp_3_ready  : in  std_logic;

      board_kernel_stream_src_lane_0_data   : in  std_logic_vector(167 downto 0);
      board_kernel_stream_src_lane_0_valid  : in  std_logic;
      board_kernel_stream_src_lane_0_ready  : out std_logic;
      board_kernel_stream_snk_lane_0_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_lane_0_valid  : out std_logic;
      board_kernel_stream_snk_lane_0_ready  : in  std_logic;

      board_kernel_stream_src_lane_1_data   : in  std_logic_vector(167 downto 0);
      board_kernel_stream_src_lane_1_valid  : in  std_logic;
      board_kernel_stream_src_lane_1_ready  : out std_logic;
      board_kernel_stream_snk_lane_1_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_lane_1_valid  : out std_logic;
      board_kernel_stream_snk_lane_1_ready  : in  std_logic;

      board_kernel_stream_src_lane_2_data   : in  std_logic_vector(167 downto 0);
      board_kernel_stream_src_lane_2_valid  : in  std_logic;
      board_kernel_stream_src_lane_2_ready  : out std_logic;
      board_kernel_stream_snk_lane_2_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_lane_2_valid  : out std_logic;
      board_kernel_stream_snk_lane_2_ready  : in  std_logic;

      board_kernel_stream_src_lane_3_data   : in  std_logic_vector(167 downto 0);
      board_kernel_stream_src_lane_3_valid  : in  std_logic;
      board_kernel_stream_src_lane_3_ready  : out std_logic;
      board_kernel_stream_snk_lane_3_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_lane_3_valid  : out std_logic;
      board_kernel_stream_snk_lane_3_ready  : in  std_logic;

      board_kernel_stream_src_lane_4_data   : in  std_logic_vector(167 downto 0);
      board_kernel_stream_src_lane_4_valid  : in  std_logic;
      board_kernel_stream_src_lane_4_ready  : out std_logic;
      board_kernel_stream_snk_lane_4_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_lane_4_valid  : out std_logic;
      board_kernel_stream_snk_lane_4_ready  : in  std_logic;

      board_kernel_stream_src_lane_5_data   : in  std_logic_vector(167 downto 0);
      board_kernel_stream_src_lane_5_valid  : in  std_logic;
      board_kernel_stream_src_lane_5_ready  : out std_logic;
      board_kernel_stream_snk_lane_5_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_lane_5_valid  : out std_logic;
      board_kernel_stream_snk_lane_5_ready  : in  std_logic;

      board_kernel_stream_src_lane_6_data   : in  std_logic_vector(167 downto 0);
      board_kernel_stream_src_lane_6_valid  : in  std_logic;
      board_kernel_stream_src_lane_6_ready  : out std_logic;
      board_kernel_stream_snk_lane_6_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_lane_6_valid  : out std_logic;
      board_kernel_stream_snk_lane_6_ready  : in  std_logic;

      board_kernel_stream_src_lane_7_data   : in  std_logic_vector(167 downto 0);
      board_kernel_stream_src_lane_7_valid  : in  std_logic;
      board_kernel_stream_src_lane_7_ready  : out std_logic;
      board_kernel_stream_snk_lane_7_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_lane_7_valid  : out std_logic;
      board_kernel_stream_snk_lane_7_ready  : in  std_logic;

      board_kernel_stream_snk_rx_monitor_0_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_rx_monitor_0_valid  : out std_logic;
      board_kernel_stream_snk_rx_monitor_0_ready  : in  std_logic;
      board_kernel_stream_snk_tx_monitor_0_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_tx_monitor_0_valid  : out std_logic;
      board_kernel_stream_snk_tx_monitor_0_ready  : in  std_logic;

      board_kernel_stream_snk_rx_monitor_1_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_rx_monitor_1_valid  : out std_logic;
      board_kernel_stream_snk_rx_monitor_1_ready  : in  std_logic;
      board_kernel_stream_snk_tx_monitor_1_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_tx_monitor_1_valid  : out std_logic;
      board_kernel_stream_snk_tx_monitor_1_ready  : in  std_logic;

      board_kernel_stream_snk_rx_monitor_2_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_rx_monitor_2_valid  : out std_logic;
      board_kernel_stream_snk_rx_monitor_2_ready  : in  std_logic;
      board_kernel_stream_snk_tx_monitor_2_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_tx_monitor_2_valid  : out std_logic;
      board_kernel_stream_snk_tx_monitor_2_ready  : in  std_logic;

      board_kernel_stream_snk_rx_monitor_3_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_rx_monitor_3_valid  : out std_logic;
      board_kernel_stream_snk_rx_monitor_3_ready  : in  std_logic;
      board_kernel_stream_snk_tx_monitor_3_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_tx_monitor_3_valid  : out std_logic;
      board_kernel_stream_snk_tx_monitor_3_ready  : in  std_logic;

      board_kernel_stream_snk_rx_monitor_4_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_rx_monitor_4_valid  : out std_logic;
      board_kernel_stream_snk_rx_monitor_4_ready  : in  std_logic;
      board_kernel_stream_snk_tx_monitor_4_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_tx_monitor_4_valid  : out std_logic;
      board_kernel_stream_snk_tx_monitor_4_ready  : in  std_logic;

      board_kernel_stream_snk_rx_monitor_5_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_rx_monitor_5_valid  : out std_logic;
      board_kernel_stream_snk_rx_monitor_5_ready  : in  std_logic;
      board_kernel_stream_snk_tx_monitor_5_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_tx_monitor_5_valid  : out std_logic;
      board_kernel_stream_snk_tx_monitor_5_ready  : in  std_logic;

      board_kernel_stream_snk_rx_monitor_6_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_rx_monitor_6_valid  : out std_logic;
      board_kernel_stream_snk_rx_monitor_6_ready  : in  std_logic;
      board_kernel_stream_snk_tx_monitor_6_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_tx_monitor_6_valid  : out std_logic;
      board_kernel_stream_snk_tx_monitor_6_ready  : in  std_logic;

      board_kernel_stream_snk_rx_monitor_7_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_rx_monitor_7_valid  : out std_logic;
      board_kernel_stream_snk_rx_monitor_7_ready  : in  std_logic;
      board_kernel_stream_snk_tx_monitor_7_data   : out std_logic_vector(167 downto 0);
      board_kernel_stream_snk_tx_monitor_7_valid  : out std_logic;
      board_kernel_stream_snk_tx_monitor_7_ready  : in  std_logic;

      board_kernel_stream_src_bs_data       : in  std_logic_vector(103 downto 0);
      board_kernel_stream_src_bs_valid      : in  std_logic;
      board_kernel_stream_src_bs_ready      : out std_logic;

      board_kernel_stream_src_mm_io_data   : in  std_logic_vector(71 downto 0);
      board_kernel_stream_src_mm_io_valid  : in  std_logic;
      board_kernel_stream_src_mm_io_ready  : out std_logic;
      board_kernel_stream_snk_mm_io_data   : out std_logic_vector(31 downto 0);
      board_kernel_stream_snk_mm_io_valid  : out std_logic;
      board_kernel_stream_snk_mm_io_ready  : out std_logic
   );
  end component freeze_wrapper;
end top_components_pkg;
