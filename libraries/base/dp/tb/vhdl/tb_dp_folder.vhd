-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author
-- . Daniel van der Schuur
-- Purpose
-- . Verify stream through dp_unfolder->dp_folder stages

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_folder is
end tb_dp_folder;

architecture tb of tb_dp_folder is
  constant c_nof_inputs                  : natural := 1;
  constant c_nof_unfolds                 : natural := 1;  -- 5 folds = 1->2->4->8->16->32 unfolded streams
  constant c_nof_unfolded_streams        : natural := c_nof_inputs * pow2(c_nof_unfolds);

  constant c_data_w                      : natural := 32;
  constant c_nof_packets                 : natural := 10;
  constant c_packet_len                  : natural := 20;
  constant c_packet_gap                  : natural := 0;

  constant c_clk_period                  : time := 5 ns;  -- 200 MHz
  constant c_mm_clk_period               : time := 20 ns;  -- 50 MHz

  signal clk                             : std_logic := '1';
  signal rst                             : std_logic;

  signal proc_dp_gen_block_data_in_en    : std_logic := '1';
  signal proc_dp_gen_block_data_src_in   : t_dp_siso := c_dp_siso_rdy;

  signal proc_dp_gen_block_data_src_out  : t_dp_sosi;

  signal dp_unfolder_snk_in_arr          : t_dp_sosi_arr(c_nof_inputs - 1 downto 0);
  signal dp_folder_snk_in_arr            : t_dp_sosi_arr(c_nof_unfolded_streams - 1 downto 0);
  signal dp_folder_src_out_arr           : t_dp_sosi_arr(c_nof_inputs - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Clock,reset generation
  -----------------------------------------------------------------------------
  clk <= not clk after c_clk_period / 2;
  rst <= '1', '0'   after c_clk_period * 7;

  -----------------------------------------------------------------------------
  -- Generate packet stream
  -----------------------------------------------------------------------------
  p_generate_packets : process
  begin
    proc_dp_gen_block_data_src_out <= c_dp_sosi_rst;

    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    for i in 0 to c_nof_packets - 1 loop
      -- Generate single packet
      proc_dp_gen_block_data(c_data_w, 0, c_packet_len, 0, 0, '0', "0", clk, proc_dp_gen_block_data_in_en, proc_dp_gen_block_data_src_in, proc_dp_gen_block_data_src_out);

      -- Insert optional gap between the packets
      proc_common_wait_some_cycles(clk, c_packet_gap);
    end loop;
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- Unfold the single stream into multiple streams, the fold it back into one again
  -----------------------------------------------------------------------------
  dp_unfolder_snk_in_arr(0) <= proc_dp_gen_block_data_src_out;

  u_dp_unfolder : entity work.dp_unfolder
  generic map (
    g_nof_inputs  => c_nof_inputs,
    g_nof_unfolds => c_nof_unfolds,
    g_output_align => false  -- We're going to fold these outputs again, so don't align them!
  )
  port map (
    clk         => clk,
    rst         => rst,

    snk_in_arr  => dp_unfolder_snk_in_arr,
    src_out_arr => dp_folder_snk_in_arr
  );

  u_dp_folder : entity work.dp_folder
  generic map (
    g_nof_inputs => c_nof_unfolded_streams,
    g_nof_folds  => -1  -- Fold until 1 output remains,
--    g_output_block_size => c_packet_len
  )
  port map (
    clk         => clk,
    rst         => rst,

    snk_in_arr  => dp_folder_snk_in_arr,
    src_out_arr => dp_folder_src_out_arr
  );
end tb;
