-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Select and/or reorder data on multiple streams.
--
-- Description:
--   Array of reorder_col_select instances.
--
-- Remarks:
--

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity reorder_col_wide_select is
  generic (
    g_nof_inputs         : natural := 6;
    g_dsp_data_w         : natural := 18;  -- complex data width, = c_data_w / 2
    g_nof_ch_in          : natural := 1024;
    g_nof_ch_sel         : natural := 12;  -- g_nof_ch_sel < g_nof_ch_in
    g_use_complex        : boolean := true
  );
  port (
    dp_rst              : in  std_logic;
    dp_clk              : in  std_logic;

    -- Dynamic reorder block size control
    nof_ch_in           : in natural range 0 to g_nof_ch_in := g_nof_ch_in;
    nof_ch_sel          : in natural range 0 to g_nof_ch_sel := g_nof_ch_sel;

    -- Memory Mapped
    col_select_mosi     : in  t_mem_mosi;  -- channel select control
    col_select_miso     : out t_mem_miso;  -- only used for waitrequest

    -- Streaming
    input_sosi_arr      : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    output_sosi_arr     : out t_dp_sosi_arr(g_nof_inputs - 1 downto 0)
  );
end reorder_col_wide_select;

architecture str of reorder_col_wide_select is
  signal col_select_miso_arr : t_mem_miso_arr(g_nof_inputs - 1 downto 0);
begin
  col_select_miso <= col_select_miso_arr(0);  -- All inputs have the same mosi/miso

  -- Instantiate reorder_col_select for multiple streams in parallel, to
  -- reorder blocks in each stream in time. The same reordering is applied
  -- by col_select_mosi to each stream.
  gen_nof_input : for I in 0 to g_nof_inputs - 1 generate
    u_reorder_col_select : entity work.reorder_col_select
    generic map (
      g_dsp_data_w         => g_dsp_data_w,
      g_nof_ch_in          => g_nof_ch_in,
      g_nof_ch_sel         => g_nof_ch_sel,
      g_use_complex        => g_use_complex
    )
    port map (
      dp_rst         => dp_rst,
      dp_clk         => dp_clk,

      -- Dynamic reorder block size control, same for all streams
      nof_ch_in      => nof_ch_in,
      nof_ch_sel     => nof_ch_sel,

      -- Memory Mapped
      col_select_mosi => col_select_mosi,
      col_select_miso => col_select_miso_arr(I),

      -- Streaming
      input_sosi     => input_sosi_arr(I),
      output_sosi    => output_sosi_arr(I)
    );
  end generate;
end str;
