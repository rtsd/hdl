-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for unb2a_led.
-- Description:
--   The DUT can be targeted at unb 0, node 3 with the same Python scripts
--   that are used on hardware.
-- Usage:
--   On command line do:
--     > run_modelsim & (to start Modeslim)
--
--   In Modelsim do:
--     > lp unb2a_led
--     > mk clean all (only first time to clean all libraries)
--     > mk all (to compile all libraries that are needed for unb2a_led)
--     . load tb_unb2a_led simulation by double clicking the tb_unb2a_led icon
--     > as 10 (to view signals in Wave Window)
--     > run 100 us (or run -all)
--
--

library IEEE, common_lib, unb2a_board_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2a_board_lib.unb2_board_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_unb2a_led is
    generic (
      g_design_name : string  := "unb2a_led";
      g_sim_unb_nr  : natural := 0;  -- UniBoard 0
      g_sim_node_nr : natural := 3  -- Node 3
    );
end tb_unb2a_led;

architecture tb of tb_unb2a_led is
  constant c_sim             : boolean := true;

  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 3;  -- Node 3

  constant c_eth_clk_period  : time := 8 ns;  -- 125 MHz XO on UniBoard

  -- DUT
  signal eth_clk             : std_logic := '0';
  signal TESTIO              : std_logic_vector(c_unb2_board_aux.testio_w - 1 downto 0);
  signal qsfp_led            : std_logic_vector(c_unb2_board_tr_qsfp_nof_leds - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (25 MHz)

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_unb2a_led : entity work.unb2a_led
    generic map (
      g_sim         => c_sim,
      g_sim_unb_nr  => c_unb_nr,
      g_sim_node_nr => c_node_nr,
      g_design_name => g_design_name
    )
    port map (
      -- GENERAL
      ETH_CLK     => eth_clk,
      TESTIO      => TESTIO,
      QSFP_LED    => qsfp_led
    );
end tb;
