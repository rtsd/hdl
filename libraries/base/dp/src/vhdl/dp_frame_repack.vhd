--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Reuse from LOFAR rad_frame_repack.vhd and rad_frame_repack(rtl).vhd

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity dp_frame_repack is
  generic (
    g_in_dat_w        : natural;
    g_in_nof_words    : natural;
    g_out_dat_w       : natural;
    g_out_nof_words   : natural
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;
    in_dat           : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val           : in  std_logic;
    in_sof           : in  std_logic;
    in_eof           : in  std_logic;
    out_dat          : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val          : out std_logic;
    out_sof          : out std_logic;
    out_eof          : out std_logic
  );
end dp_frame_repack;

architecture str of dp_frame_repack is
  signal pack_fsn       : std_logic_vector(in_dat'range);
  signal pack_dat       : std_logic_vector(in_dat'range);
  signal pack_val       : std_logic;
  signal pack_sof       : std_logic;
  signal pack_eof       : std_logic;
  signal pack_err       : std_logic;

  signal repack_fsn     : std_logic_vector(out_dat'range);
  signal repack_dat     : std_logic_vector(out_dat'range);
  signal repack_val     : std_logic;
  signal repack_sof     : std_logic;
  signal repack_eof     : std_logic;
begin
  no_pack : if g_in_nof_words = g_out_nof_words generate
    out_dat <= RESIZE_UVEC(in_dat, out_dat'length);  -- any extra bits will get stripped again by dp_repack at the other end,
                                                     -- typically g_out_dat_w=g_in_dat_w
    out_val <= in_val;
    out_sof <= in_sof;
    out_eof <= in_eof;
  end generate;

  gen_pack : if g_in_nof_words /= g_out_nof_words generate
    unframe : entity work.dp_unframe
    generic map (
      g_dat_w        => g_in_dat_w,
      g_fsn_w        => g_in_dat_w
    )
    port map (
      rst            => rst,
      clk            => clk,
      in_dat         => in_dat,
      in_val         => in_val,
      in_sof         => in_sof,
      in_eof         => in_eof,
      out_fsn        => pack_fsn,
      out_sync       => OPEN,
      out_dat        => pack_dat,
      out_val        => pack_val,
      out_sof        => pack_sof,
      out_eof        => pack_eof,
      out_err        => pack_err
    );

    repack : entity work.dp_repack_legacy
    generic map (
      g_in_dat_w      => g_in_dat_w,
      g_in_nof_words  => g_in_nof_words,
      g_out_dat_w     => g_out_dat_w,
      g_out_nof_words => g_out_nof_words
    )
    port map (
      rst            => rst,
      clk            => clk,
      in_dat         => pack_dat,
      in_val         => pack_val,
      in_sof         => pack_sof,
      in_eof         => pack_eof,
      out_dat        => repack_dat,
      out_val        => repack_val,
      out_sof        => repack_sof,
      out_eof        => repack_eof
    );

    repack_fsn <= RESIZE_SVEC(pack_fsn, repack_fsn'length);  -- pack_fsn remains valid

    frame : entity work.dp_frame
    generic map (
      g_dat_w        => g_out_dat_w,
      g_fsn_w        => g_out_dat_w
    )
    port map (
      rst            => rst,
      clk            => clk,
      in_fsn         => repack_fsn,
      in_dat         => repack_dat,
      in_val         => repack_val,
      in_sof         => repack_sof,
      in_eof         => repack_eof,
      in_err         => pack_err,  -- pack_err remains valid
      out_dat        => out_dat,
      out_val        => out_val,
      out_sof        => out_sof,
      out_eof        => out_eof
    );
  end generate;
end str;
