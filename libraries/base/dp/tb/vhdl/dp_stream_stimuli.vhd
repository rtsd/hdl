-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . The dp_stream_stimuli generates as stream of packets with counter data.
-- Description:
--
-- Remark:
-- . The stimuli empty = 0 because the data in proc_dp_gen_block_data() is
--   generated with one symbol per data (because symbol_w = data_w).
--
-- Usage:
-- . See tb_dp_example_no_dut for usage example
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity dp_stream_stimuli is
  generic (
    g_instance_nr    : natural := 0;
    -- flow control
    g_random_w       : natural := 15;  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   : natural := 1;
    g_pulse_period   : natural := 2;
    g_flow_control   : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    : natural := 10;
    g_sync_offset    : natural := 0;
    g_use_complex    : boolean := false;
    g_data_init      : natural := 0;  -- choose some easy to recognize and unique value, data will increment at every valid
    g_re_init        : natural := 0;  -- choose some easy to recognize and unique value, data will increment at every valid
    g_im_init        : natural := 0;  -- choose some easy to recognize and unique value, data will increment at every valid
    g_bsn_init       : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := TO_DP_BSN(0);  -- X"0877665544332211", bsn will increment at every sop
    g_err_init       : natural := 247;  -- choose some easy to recognize and unique value
    g_err_incr       : natural := 1;  -- when 0 keep fixed at init value, when 1 increment at every sop
    g_channel_init   : natural := 5;  -- choose some easy to recognize and unique value
    g_channel_incr   : natural := 1;  -- when 0 keep fixed at init value, when 1 increment at every sop
    -- specific
    g_in_dat_w       : natural := 32;
    g_nof_repeat     : natural := 5;
    g_pkt_len        : natural := 16;
    g_pkt_gap        : natural := 4;
    g_wait_last_evt  : natural := 100  -- number of clk cycles to wait with last_snk_in_evt after finishing the stimuli
  );
  port (
    rst               : in  std_logic;
    clk               : in  std_logic;

    -- Generate stimuli
    src_in            : in  t_dp_siso := c_dp_siso_rdy;
    src_out           : out t_dp_sosi;

    -- End of stimuli
    last_snk_in       : out t_dp_sosi;  -- expected verify_snk_in after end of stimuli
    last_snk_in_evt   : out std_logic;  -- trigger verify to verify the last_snk_in
    tb_end            : out std_logic  -- signal end of tb as far as this dp_stream_stimuli is concerned
  );
end dp_stream_stimuli;

architecture str of dp_stream_stimuli is
  signal random          : std_logic_vector(g_random_w - 1 downto 0) := TO_UVEC(g_instance_nr, g_random_w);  -- use different initialization to have different random sequences per stream
  signal pulse           : std_logic;
  signal pulse_en        : std_logic := '1';

  signal stimuli_en      : std_logic := '1';
  signal src_out_data    : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal i_src_out       : t_dp_sosi;
begin
  src_out <= i_src_out;

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  random <= func_common_random(random) when rising_edge(clk);

  proc_common_gen_duty_pulse(g_pulse_active, g_pulse_period, '1', rst, clk, pulse_en, pulse);

  stimuli_en <= '1'                 when g_flow_control = e_active else
                random(random'high) when g_flow_control = e_random else
                pulse               when g_flow_control = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli_st : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
    variable v_last : t_dp_sosi := c_dp_sosi_rst;
  begin
    -- Initialisations
    last_snk_in <= c_dp_sosi_rst;
    last_snk_in_evt <= '0';
    tb_end <= '0';

    -- Adjust initial sosi field values by -1 to compensate for auto increment
    v_sosi.bsn     := INCR_UVEC(g_bsn_init,                    -1);
    v_sosi.channel := INCR_UVEC(TO_DP_CHANNEL(g_channel_init), -g_channel_incr);
    v_sosi.data    := INCR_UVEC(TO_DP_DATA(g_data_init),       -g_pkt_len);
    v_sosi.re      := INCR_UVEC(TO_DP_DSP_DATA(g_re_init),     -g_pkt_len);
    v_sosi.im      := INCR_UVEC(TO_DP_DSP_DATA(g_im_init),     -g_pkt_len);
    v_sosi.err     := INCR_UVEC(TO_DP_ERROR(g_err_init),       -g_err_incr);

    i_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Generate g_nof_repeat packets
    for I in 0 to g_nof_repeat - 1 loop
      -- Auto increment v_sosi field values for this packet
      v_sosi.bsn     := INCR_UVEC(v_sosi.bsn, 1);
      v_sosi.sync    := sel_a_b((unsigned(v_sosi.bsn) mod g_sync_period) = g_sync_offset, '1', '0');  -- insert sync starting at BSN=g_sync_offset and with period g_sync_period
      v_sosi.channel := INCR_UVEC(v_sosi.channel, g_channel_incr);
      v_sosi.err     := INCR_UVEC(v_sosi.err, g_err_incr);
      if g_use_complex = false then
        v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len);
        v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
      else
        v_sosi.re      := INCR_UVEC(v_sosi.re, g_pkt_len);
        v_sosi.re      := RESIZE_DP_DSP_UDATA(v_sosi.re(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
        v_sosi.im      := INCR_UVEC(v_sosi.im, g_pkt_len);
        v_sosi.im      := RESIZE_DP_DSP_UDATA(v_sosi.im(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
      end if;
      -- Send packet
      if g_use_complex = false then
        proc_dp_gen_block_data(g_in_dat_w, TO_UINT(v_sosi.data),
                               g_pkt_len, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn,
                               clk, stimuli_en, src_in, i_src_out);
      else
        proc_dp_gen_block_complex(g_in_dat_w, TO_UINT(v_sosi.re), TO_UINT(v_sosi.im),
                                  g_pkt_len, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn,
                                  clk, stimuli_en, src_in, i_src_out);
      end if;
      -- Insert optional gap between the packets
      proc_common_wait_some_cycles(clk, g_pkt_gap);

      -- Update v_last.sync
      if v_sosi.sync = '1' then v_last.sync := '1'; end if;
    end loop;

    -- Update v_last control
    if g_nof_repeat > 0 then
      v_last.sop := '1';
      v_last.eop := '1';
      v_last.valid := '1';
    end if;

    -- Determine and keep last expected sosi field values after end of stimuli
    -- . e_qual
    v_last.bsn     := std_logic_vector( unsigned(g_bsn_init) + g_nof_repeat - 1);
    v_last.channel := TO_DP_CHANNEL(g_channel_init + (g_nof_repeat - 1) * g_channel_incr);
    v_last.err     := TO_DP_ERROR(g_err_init + (g_nof_repeat - 1) * g_err_incr);
    -- . account for g_pkt_len
    if g_use_complex = false then
      v_last.data := INCR_UVEC(v_sosi.data, g_pkt_len - 1);
      v_last.data := RESIZE_DP_DATA(v_last.data(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
    else
      v_last.re := INCR_UVEC(v_sosi.re, g_pkt_len - 1);
      v_last.re := RESIZE_DP_DSP_DATA(v_last.re(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
      v_last.im := INCR_UVEC(v_sosi.im, g_pkt_len - 1);
      v_last.im := RESIZE_DP_DSP_DATA(v_last.im(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
    end if;
    last_snk_in <= v_last;

    -- Signal end of stimuli
    -- . latency from stimuli to verify depends on the flow control, so wait sufficiently long for last packet to have passed through
    proc_common_wait_some_cycles(clk, g_wait_last_evt);
    proc_common_gen_pulse(clk, last_snk_in_evt);
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Auxiliary
  ------------------------------------------------------------------------------

  -- Map to slv to ease monitoring in wave window
  src_out_data <= i_src_out.data(g_in_dat_w - 1 downto 0);
end str;
