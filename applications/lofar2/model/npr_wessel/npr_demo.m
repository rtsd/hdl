% Near perfect reconstruction polyphase filter bank demo.
%
% (c) 2007-2022 Wessel Lubberhuizen
% number of taps per channel
L=64;
% number of channels
N=256;
% number of slices
M=8192;
% whether to use matlab's own chirp function
use_matlab_chirp=false;
fprintf('designing prototype filter...\n');
c=npr_coeff(N,L);
fprintf('generating a test signal...\n');
% time series
t=(0:M*N-1)/(M*N);
% generate a linear chirp as a test signal.
if use_matlab_chirp
    % Note that matlab's own chirp function exhibits phase noise with
    % increasing magnitude, apparently due to roundoff errors in t.
    x = complex(chirp(t,0,1,M*N,'linear',0),chirp(t,0,1,M*N,'linear',90));
else
    % Custom low noise linear chirp, using phase instead of t.
    dphi=2*pi*t;
    phi=zeros(size(dphi));
    for i=2:length(dphi)
        phi(i) = mod(phi(i-1)+dphi(i-1),2*pi);
    end
    x = exp(-1i*phi);
end
% You can add some white noise if you like
%x = awgn(x,200);
% run it through the npr filterbank
fprintf('processing...\n');
time1=cputime;
y=npr_analysis(c,x);
z=npr_synthesis(c,y);
time2=cputime;
fprintf('processing rate = %3.2f MSamples / second\n',length(x)/(time2-time1)/1E6);
% compare the input and the output
delay=N*(L-1)/2;
padding=zeros(1,delay);
z=z(1+delay:length(z));
x=x(1:length(x)-delay);
e=z-x;
fprintf('average reconstruction error = %f dB\n',20*log10(norm(e,2)/norm(x,2)));
%% Figure 1
figure(1);
subplot(2,1,1);
plot(real([x' z']));
title('input / output signals');
xlabel('sample');
ylabel('signal value');
grid on;
subplot(2,1,2);
plot(real(e'));
xlabel('time (sample)');
ylabel('error value');
grid on;
%% Figure 2
figure(2);
plot(20*log10(abs(e')));
title('reconstruction error');
xlabel('time (sample)');
ylabel('power (dB)');
grid on;
%% Figure 3
figure(3);
subplot(2,1,1);
img=20*log10(abs(y)/max(abs(y(:))));
imagesc(img,[-300 0]);
colorbar;
title('npr\_analysis (dB)');
xlabel('time (slice)');
ylabel('frequency (channel)');
% compare with matlabs spectrogram function
window=blackmanharris(N,'periodic');
overlap=0.5*N;
nfft=2*overlap;
s=spectrogram(x,window,overlap,nfft);
s=flipud(s);
img2=20*log10(abs(s)/max(abs(s(:))));
subplot(2,1,2);
imagesc(img2,[-300 0]);
colorbar;
title('spectrogram (dB)');
xlabel('time (slice)');
ylabel('frequency (channel)');
