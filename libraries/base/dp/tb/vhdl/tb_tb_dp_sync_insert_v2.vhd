-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Verify multiple variations of tb_dp_sync_insert_v2
-- Description:
-- Usage:
-- > as 3
-- > run -all
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_sync_insert_v2 is
end tb_tb_dp_sync_insert_v2;

architecture tb of tb_tb_dp_sync_insert_v2 is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--    g_nof_streams            : NATURAL := 2;
--    g_block_size_input       : NATURAL := 16;
--    g_nof_blk_per_sync_input : NATURAL := 32;
--    g_gap_size_during_block  : NATURAL := 0;
--    g_gap_size_between_block : NATURAL := 0;
--    g_nof_blk_per_sync       : NATURAL := 8;
--    g_nof_blk_per_sync_min   : NATURAL := 2;
--    g_nof_repeat             : NATURAL := 14

  u_no_gaps            : entity work.tb_dp_sync_insert_v2 generic map(2, 16, 32, 0, 0, 8, 2, 14);
  u_gap                : entity work.tb_dp_sync_insert_v2 generic map(2, 16, 32, 1, 3, 8, 2, 14);
  u_large_blk_per_sync : entity work.tb_dp_sync_insert_v2 generic map(2, 16, 32, 0, 0, 999, 2, 14);
  u_fract_blk_per_sync : entity work.tb_dp_sync_insert_v2 generic map(2, 16, 32, 0, 0, 7, 2, 14);
end tb;
