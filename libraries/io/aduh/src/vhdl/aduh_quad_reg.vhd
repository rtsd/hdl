-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave read only register for aduh_quad
-- Description:
-- . Report the locked status for ADU-AB and CD
-- . Report pattern verification result for each ADC [A,B,C,D]
-- . Control for dp_phs_clk_en_vec[g_nof_dp_phs_clk-1:0]
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |         xxx                              ab_stable & ab_locked = [1:0]|  0
--  |-----------------------------------------------------------------------|
--  |         xxx                              cd_stable & cd_locked = [1:0]|  1
--  |-----------------------------------------------------------------------|
--  |         xxx            a_verify_res_val[12]|   xxx   a_verify_res[8:0]|  2
--  |-----------------------------------------------------------------------|
--  |         xxx            b_verify_res_val[12]|   xxx   b_verify_res[8:0]|  3
--  |-----------------------------------------------------------------------|
--  |         xxx            c_verify_res_val[12]|   xxx   c_verify_res[8:0]|  4
--  |-----------------------------------------------------------------------|
--  |         xxx            d_verify_res_val[12]|   xxx   d_verify_res[8:0]|  5
--  |-----------------------------------------------------------------------|
--  |                                                       ab_control[31:0]|  6
--  |-----------------------------------------------------------------------|
--  |                                                       cd_control[31:0]|  7
--  |-----------------------------------------------------------------------|
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity aduh_quad_reg is
  generic (
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_nof_dp_phs_clk     : natural := 1  -- nof dp_phs_clk that can be used to detect the word phase
  );
  port (
    -- Clocks and reset
    mm_rst                   : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk                   : in  std_logic;  -- memory-mapped bus clock
    st_rst                   : in  std_logic;  -- reset synchronous with st_clk
    st_clk                   : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in                   : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out                  : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    st_aduh_ab_status        : in  std_logic_vector(c_word_w - 1 downto 0);
    st_aduh_ab_locked        : in  std_logic;
    st_aduh_ab_stable        : in  std_logic;
    st_aduh_ab_stable_ack    : out std_logic;
    st_aduh_ab_control       : out std_logic_vector(c_word_w - 1 downto 0);

    st_aduh_cd_status        : in  std_logic_vector(c_word_w - 1 downto 0);
    st_aduh_cd_locked        : in  std_logic;
    st_aduh_cd_stable        : in  std_logic;
    st_aduh_cd_stable_ack    : out std_logic;
    st_aduh_cd_control       : out std_logic_vector(c_word_w - 1 downto 0);

    st_aduh_a_verify_res     : in  std_logic_vector(c_word_w - 1 downto 0);
    st_aduh_a_verify_res_val : in  std_logic;
    st_aduh_a_verify_res_ack : out std_logic;

    st_aduh_b_verify_res     : in  std_logic_vector(c_word_w - 1 downto 0);
    st_aduh_b_verify_res_val : in  std_logic;
    st_aduh_b_verify_res_ack : out std_logic;

    st_aduh_c_verify_res     : in  std_logic_vector(c_word_w - 1 downto 0);
    st_aduh_c_verify_res_val : in  std_logic;
    st_aduh_c_verify_res_ack : out std_logic;

    st_aduh_d_verify_res     : in  std_logic_vector(c_word_w - 1 downto 0);
    st_aduh_d_verify_res_val : in  std_logic;
    st_aduh_d_verify_res_ack : out std_logic
  );
end aduh_quad_reg;

architecture rtl of aduh_quad_reg is
  -- Define the actual size of the MM slave register
  constant c_nof_dat : natural := 8;
  constant c_mm_reg  : t_c_mem := (latency  => 1,
                                   adr_w    => ceil_log2(c_nof_dat),
                                   dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                   nof_dat  => c_nof_dat,
                                   init_sl  => '0');

  -- Register access control signal in mm_clk domain
  signal mm_aduh_ab_stable_ack    : std_logic;
  signal mm_aduh_cd_stable_ack    : std_logic;

  signal mm_aduh_a_verify_res_ack : std_logic;
  signal mm_aduh_b_verify_res_ack : std_logic;
  signal mm_aduh_c_verify_res_ack : std_logic;
  signal mm_aduh_d_verify_res_ack : std_logic;

  -- Registers in mm_clk domain
  signal mm_aduh_ab_status        : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_aduh_ab_locked        : std_logic;
  signal mm_aduh_ab_stable        : std_logic;
  signal mm_aduh_ab_control       : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_aduh_cd_status        : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_aduh_cd_locked        : std_logic;
  signal mm_aduh_cd_stable        : std_logic;
  signal mm_aduh_cd_control       : std_logic_vector(c_word_w - 1 downto 0);

  signal mm_aduh_a_verify_res     : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_aduh_a_verify_res_val : std_logic;
  signal mm_aduh_b_verify_res     : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_aduh_b_verify_res_val : std_logic;
  signal mm_aduh_c_verify_res     : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_aduh_c_verify_res_val : std_logic;
  signal mm_aduh_d_verify_res     : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_aduh_d_verify_res_val : std_logic;
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out                  <= c_mem_miso_rst;

      -- Access event, register values
      mm_aduh_ab_stable_ack    <= '0';
      mm_aduh_cd_stable_ack    <= '0';
      mm_aduh_ab_control                              <= (others => '0');  -- default reset all unused control bits
      mm_aduh_cd_control                              <= (others => '0');  -- default reset all unused control bits
      mm_aduh_ab_control(g_nof_dp_phs_clk - 1 downto 0) <= (others => '1');  -- default enable all dp_phs_clk for ADU-AB
      mm_aduh_cd_control(g_nof_dp_phs_clk - 1 downto 0) <= (others => '1');  -- default enable all dp_phs_clk for ADU-CD

      mm_aduh_a_verify_res_ack <= '0';
      mm_aduh_b_verify_res_ack <= '0';
      mm_aduh_c_verify_res_ack <= '0';
      mm_aduh_d_verify_res_ack <= '0';

    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Access event defaults
      mm_aduh_ab_stable_ack <= '0';
      mm_aduh_cd_stable_ack <= '0';

      mm_aduh_a_verify_res_ack <= '0';
      mm_aduh_b_verify_res_ack <= '0';
      mm_aduh_c_verify_res_ack <= '0';
      mm_aduh_d_verify_res_ack <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Write dp_phs_clk select control
          when 6 =>
            mm_aduh_ab_control <= sla_in.wrdata(31 downto 0);
          when 7 =>
            mm_aduh_cd_control <= sla_in.wrdata(31 downto 0);
          when others => null;  -- not used MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Read ADUH locked status
          when 0 =>
            mm_aduh_ab_stable_ack        <= '1';
            sla_out.rddata(0)            <= mm_aduh_ab_locked;  -- ADU AB
            sla_out.rddata(1)            <= mm_aduh_ab_stable;
            sla_out.rddata(31 downto 2)  <= mm_aduh_ab_status(31 downto 2);  -- extra status info for debug
          when 1 =>
            mm_aduh_cd_stable_ack        <= '1';
            sla_out.rddata(0)            <= mm_aduh_cd_locked;  -- ADU CD
            sla_out.rddata(1)            <= mm_aduh_cd_stable;
            sla_out.rddata(31 downto 2)  <= mm_aduh_cd_status(31 downto 2);  -- extra status info for debug

          -- Read ADUH ADC verify test pattern status
          when 2 =>
            mm_aduh_a_verify_res_ack     <= '1';
            sla_out.rddata(8 downto 0)   <= mm_aduh_a_verify_res(8 downto 0);  -- ADC A
            sla_out.rddata(12)           <= mm_aduh_a_verify_res_val;
          when 3 =>
            mm_aduh_b_verify_res_ack     <= '1';
            sla_out.rddata(8 downto 0)   <= mm_aduh_b_verify_res(8 downto 0);  -- ADC B
            sla_out.rddata(12)           <= mm_aduh_b_verify_res_val;
          when 4 =>
            mm_aduh_c_verify_res_ack     <= '1';
            sla_out.rddata(8 downto 0)   <= mm_aduh_c_verify_res(8 downto 0);  -- ADC C
            sla_out.rddata(12)           <= mm_aduh_c_verify_res_val;
          when 5 =>
            mm_aduh_d_verify_res_ack     <= '1';
            sla_out.rddata(8 downto 0)   <= mm_aduh_d_verify_res(8 downto 0);  -- ADC D
            sla_out.rddata(12)           <= mm_aduh_d_verify_res_val;

          when 6 =>
            -- Read back dp_phs_clk select control
            sla_out.rddata(31 downto 0)  <= mm_aduh_ab_control;
          when 7 =>
            -- Read back dp_phs_clk select control
            sla_out.rddata(31 downto 0)  <= mm_aduh_cd_control;
          when others => null;  -- not used MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and st_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the save side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  no_cross : if g_cross_clock_domain = false generate  -- so mm_clk = st_clk
    mm_aduh_ab_status        <= st_aduh_ab_status;
    mm_aduh_ab_locked        <= st_aduh_ab_locked;
    mm_aduh_ab_stable        <= st_aduh_ab_stable;
    st_aduh_ab_stable_ack    <= mm_aduh_ab_stable_ack;
    st_aduh_ab_control       <= mm_aduh_ab_control;

    mm_aduh_cd_status        <= st_aduh_cd_status;
    mm_aduh_cd_locked        <= st_aduh_cd_locked;
    mm_aduh_cd_stable        <= st_aduh_cd_stable;
    st_aduh_cd_stable_ack    <= mm_aduh_cd_stable_ack;
    st_aduh_cd_control       <= mm_aduh_cd_control;

    mm_aduh_a_verify_res     <= st_aduh_a_verify_res;
    mm_aduh_a_verify_res_val <= st_aduh_a_verify_res_val;
    st_aduh_a_verify_res_ack <= mm_aduh_a_verify_res_ack;

    mm_aduh_b_verify_res     <= st_aduh_b_verify_res;
    mm_aduh_b_verify_res_val <= st_aduh_b_verify_res_val;
    st_aduh_b_verify_res_ack <= mm_aduh_b_verify_res_ack;

    mm_aduh_c_verify_res     <= st_aduh_c_verify_res;
    mm_aduh_c_verify_res_val <= st_aduh_c_verify_res_val;
    st_aduh_c_verify_res_ack <= mm_aduh_c_verify_res_ack;

    mm_aduh_d_verify_res     <= st_aduh_d_verify_res;
    mm_aduh_d_verify_res_val <= st_aduh_d_verify_res_val;
    st_aduh_d_verify_res_ack <= mm_aduh_d_verify_res_ack;
  end generate;  -- no_cross

  gen_cross : if g_cross_clock_domain = true generate
    -- ADUH extra status registers
    -- . no need to use in_new, continuously cross the clock domain
    u_aduh_ab_status : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_aduh_ab_status,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_aduh_ab_status
    );

    u_aduh_cd_status : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_aduh_cd_status,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_aduh_cd_status
    );

    -- ADUH locked registers
    u_aduh_ab_locked : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_aduh_ab_locked,
      dout => mm_aduh_ab_locked
    );

    u_aduh_ab_stable : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_aduh_ab_stable,
      dout => mm_aduh_ab_stable
    );

    u_aduh_ab_stable_ack : entity common_lib.common_spulse
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_pulse  => mm_aduh_ab_stable_ack,
      in_busy   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_pulse => st_aduh_ab_stable_ack
    );

    u_aduh_ab_control : entity common_lib.common_reg_cross_domain
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_dat    => mm_aduh_ab_control,
      in_done   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_dat   => st_aduh_ab_control,
      out_new   => open
    );

    u_aduh_cd_locked : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_aduh_cd_locked,
      dout => mm_aduh_cd_locked
    );

    u_aduh_cd_stable : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_aduh_cd_stable,
      dout => mm_aduh_cd_stable
    );

    u_aduh_cd_stable_ack : entity common_lib.common_spulse
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_pulse  => mm_aduh_cd_stable_ack,
      in_busy   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_pulse => st_aduh_cd_stable_ack
    );

    u_aduh_cd_control : entity common_lib.common_reg_cross_domain
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_dat    => mm_aduh_cd_control,
      in_done   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_dat   => st_aduh_cd_control,
      out_new   => open
    );

    -- ADUH ADC verification registers
    u_aduh_a_verify_res : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_aduh_a_verify_res,
      in_done     => OPEN,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_aduh_a_verify_res,
      out_new     => open
    );

    u_aduh_a_verify_res_val : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_aduh_a_verify_res_val,
      dout => mm_aduh_a_verify_res_val
    );

    u_aduh_a_verify_res_ack : entity common_lib.common_spulse
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_pulse  => mm_aduh_a_verify_res_ack,
      in_busy   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_pulse => st_aduh_a_verify_res_ack
    );

    u_aduh_b_verify_res : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_aduh_b_verify_res,
      in_done     => OPEN,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_aduh_b_verify_res,
      out_new     => open
    );

    u_aduh_b_verify_res_val : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_aduh_b_verify_res_val,
      dout => mm_aduh_b_verify_res_val
    );

    u_aduh_b_verify_res_ack : entity common_lib.common_spulse
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_pulse  => mm_aduh_b_verify_res_ack,
      in_busy   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_pulse => st_aduh_b_verify_res_ack
    );

    u_aduh_c_verify_res : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_aduh_c_verify_res,
      in_done     => OPEN,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_aduh_c_verify_res,
      out_new     => open
    );

    u_aduh_c_verify_res_val : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_aduh_c_verify_res_val,
      dout => mm_aduh_c_verify_res_val
    );

    u_aduh_c_verify_res_ack : entity common_lib.common_spulse
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_pulse  => mm_aduh_c_verify_res_ack,
      in_busy   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_pulse => st_aduh_c_verify_res_ack
    );

    u_aduh_d_verify_res : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_aduh_d_verify_res,
      in_done     => OPEN,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_aduh_d_verify_res,
      out_new     => open
    );

    u_aduh_d_verify_res_val : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_aduh_d_verify_res_val,
      dout => mm_aduh_d_verify_res_val
    );

    u_aduh_d_verify_res_ack : entity common_lib.common_spulse
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_pulse  => mm_aduh_d_verify_res_ack,
      in_busy   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_pulse => st_aduh_d_verify_res_ack
    );
  end generate;  -- gen_cross
end rtl;
