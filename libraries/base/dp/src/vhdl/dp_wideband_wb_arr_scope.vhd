-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Scope component to show the arrayed DP SOSI data at the SCLK
--          sample rate
-- Description:
--   The SCLK rate is g_wideband_factor faster than the DCLK rate. The input
--   is one wideband stream that is carried by an array of g_wideband_factor
--   sosi streams at the DCLK rate. The output is a single sosi integer stream
--   at the SCLK rate.
-- Remark:
-- . Only for simulation.
-- . When g_use_sclk=TRUE then the input SCLK is used. Else the SCLK is derived
--   from the DCLK so that it does not have to be applied via an input. This
--   eases the use of this scope within a design.
-- . In this dp_wideband_wb_arr_scope the input is only one wideband stream
--   and the input sosi array has size g_wideband_factor, so the wideband
--   data is carried via the sosi array dimension.
--   In dp_wideband_sp_arr_scope the input is one or more wideband streams
--   and the input sosi array has size g_nof_streams, so there the wideband
--   data is carried by g_wideband_factor concatenated symbols in the data
--   field or in the (re, im) fields.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_wideband_wb_arr_scope is
  generic (
    g_sim                 : boolean := false;
    g_use_sclk            : boolean := true;
    g_wideband_factor     : natural := 4;  -- Wideband rate factor = 4 for dp_clk processing frequency is 200 MHz frequency and SCLK sample frequency Fs is 800 MHz
    g_wideband_big_endian : boolean := false;  -- When true wb_sosi_arr[3:0] = sample[t0,t1,t2,t3], else when false : wb_sosi_arr[3:0] = sample[t3,t2,t1,t0]
    g_dat_w               : natural := 8  -- Actual width of the data field or of the re field, im field
  );
  port (
    -- Digital processing clk
    DCLK         : in std_logic := '0';

    -- Sampling clk, for simulation only
    SCLK         : in std_logic := '0';  -- SCLK rate = g_wideband_factor * DCLK rate

    -- Streaming input samples for one stream
    wb_sosi_arr  : in t_dp_sosi_arr(g_wideband_factor - 1 downto 0);  -- = [3:0] = Signal Path time samples [t3,t2,t1,t0]

    -- Scope output samples for one stream
    scope_sosi   : out t_dp_sosi_integer
  );
end dp_wideband_wb_arr_scope;

architecture beh of dp_wideband_wb_arr_scope is
  signal SCLKi        : std_logic;  -- sampling clk, for simulation only
  signal sample_cnt   : natural range 0 to g_wideband_factor - 1 := 0;
  signal st_sosi      : t_dp_sosi;
begin
  sim_only : if g_sim = true generate
    use_sclk : if g_use_sclk = true generate
      SCLKi <= SCLK;  -- no worry about the delta cycle delay from SCLK to SCLKi
    end generate;

    gen_sclk : if g_use_sclk = false generate
      proc_common_dclk_generate_sclk(g_wideband_factor, DCLK, SCLKi);
    end generate;

    -- View wb_sosi_arr at the sample rate using st_sosi
    p_st_sosi : process(SCLKi)
    begin
      if rising_edge(SCLKi) then
        if g_wideband_big_endian = true then
          st_sosi <= wb_sosi_arr(g_wideband_factor - 1 - sample_cnt);
        else
          st_sosi <= wb_sosi_arr(sample_cnt);
        end if;
        sample_cnt <= 0;
        if wb_sosi_arr(0).valid = '1' and sample_cnt < g_wideband_factor - 1 then  -- all wb_sosi_arr().valid are the same, so use (0)
          sample_cnt <= sample_cnt + 1;
        end if;
      end if;
    end process;

    -- Map sosi to SLV of actual g_dat_w to allow observation in Wave Window in analogue format
    scope_sosi <= func_dp_stream_slv_to_integer(st_sosi, g_dat_w);
  end generate;
end beh;
