-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.tb_common_mem_pkg.all;

entity tb_mms_dp_fields is
end tb_mms_dp_fields;

architecture tb of tb_mms_dp_fields is
  constant clk_period   : time := 10 ns;
  constant c_delay_len  : natural := 3;

  constant c_field_arr : t_common_field_arr(2 downto 0) := (( field_name_pad("test_field_2"), "RW", 36, field_default(x"BADC0DE56") ),  -- 0xCAFEDEADB
                                                            ( field_name_pad("test_field_1"), "RO",  8, field_default(x"AA") ),  -- 0xEE
                                                            ( field_name_pad("test_field_0"), "RO",  4, field_default(x"B") ));  -- 0xF

  constant c_field_arr2: t_common_field_arr(2 downto 0) := (( field_name_pad("test_field_2"), "RO", 4,  field_default(x"B")),  -- 0xF
                                                            ( field_name_pad("test_field_1"), "RO", 8,  field_default(x"AA")),  -- 0xEE
                                                            ( field_name_pad("test_field_0"), "RW", 36, field_default(x"BADC0DE56")));  -- 0xCAFEDEADB

  signal clk     : std_logic := '0';
  signal rst     : std_logic := '1';

  signal my_slv  : std_logic_vector(field_slv_in_len( c_field_arr) - 1 downto 0) := x"EEF";
  signal slv_out : std_logic_vector(field_slv_out_len(c_field_arr) - 1 downto 0);

  signal mm_mosi : t_mem_mosi;
  signal mm_miso : t_mem_miso;
begin
  clk  <= not clk after clk_period / 2;
  rst <= '0' after 100 ns;

  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    g_field_arr => c_field_arr2
  )
  port map (
    mm_clk  => clk,
    mm_rst  => rst,

    mm_mosi => mm_mosi,
    mm_miso => mm_miso,

    slv_clk => clk,
    slv_rst => rst,

    slv_in  => my_slv,
    slv_out => slv_out
  );

  p_stim: process
  begin
     wait until rst = '0';
     proc_mem_mm_bus_rd(0, clk, mm_mosi);
     proc_mem_mm_bus_rd(1, clk, mm_mosi);
     proc_mem_mm_bus_rd(2, clk, mm_mosi);
     proc_mem_mm_bus_rd(3, clk, mm_mosi);

     -- c_field_arr write
--   proc_mem_mm_bus_wr(2, x"AFEDEADB", clk, mm_mosi);
--   proc_mem_mm_bus_wr(3, x"0000000C", clk, mm_mosi);

     -- c_field_arr2 write
     proc_mem_mm_bus_wr(0, x"AFEDEADB", clk, mm_mosi);
     proc_mem_mm_bus_wr(1, x"0000000C", clk, mm_mosi);

     -- Wait until words have been written
     wait for 250 ns;

     proc_mem_mm_bus_rd(0, clk, mm_mosi);
     proc_mem_mm_bus_rd(1, clk, mm_mosi);
     proc_mem_mm_bus_rd(2, clk, mm_mosi);
     proc_mem_mm_bus_rd(3, clk, mm_mosi);

     wait;
   end process;
end tb;
