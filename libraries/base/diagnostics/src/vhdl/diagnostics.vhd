--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- PURPOSE
-- =======
-- Generate and verify an array of streams with g_dat_w -bits data.
-- Any data widths exceeding 32 (max=256) will create a number of
-- substreams within a stream.
-- For instance: g_dat_w = 64 and g_nof_streams = 8 will create:
--   - an array of 8 streams
--   - each stream contains a 64-bit data stream (counter or PRBS) made up of 2 substreams of 32 bits each.
--
-- All control inputs and status outputs apply to an individual stream.

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity diagnostics is
  generic (
    g_dat_w         : natural;  -- data width per stream (8..256)
    g_block_len     : natural := 0;  -- 0: data+valid only; >0 add sop, eop
    g_nof_streams   : natural := 1;
    g_src_latency   : natural := 1;
    g_snk_latency   : natural := 1;
    g_separate_clk  : boolean := false  -- Use separate SRC and SNK clock domains
   );
  port (
    rst              : in  std_logic := '0';
    clk              : in  std_logic := '0';

    src_rst          : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
    src_clk          : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');

    snk_rst          : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
    snk_clk          : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');

    snk_out_arr      : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr       : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    snk_diag_en      : in  std_logic_vector(g_nof_streams - 1 downto 0);
    snk_diag_md      : in  std_logic_vector(g_nof_streams - 1 downto 0);
    snk_diag_res     : out std_logic_vector(g_nof_streams - 1 downto 0);
    snk_diag_res_val : out std_logic_vector(g_nof_streams - 1 downto 0);
    snk_val_cnt      : out t_slv_32_arr(g_nof_streams - 1 downto 0);
    snk_val_cnt_clr  : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');

    src_in_arr       : in  t_dp_siso_arr(g_nof_streams - 1 downto 0);
    src_out_arr      : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_diag_en      : in  std_logic_vector(g_nof_streams - 1 downto 0);
    src_diag_md      : in  std_logic_vector(g_nof_streams - 1 downto 0);
    src_val_cnt      : out t_slv_32_arr(g_nof_streams - 1 downto 0);
    src_val_cnt_clr  : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0')
  );
end diagnostics;

architecture str of diagnostics is
  constant c_min_dat_w        : natural := 8;
  constant c_max_dat_w        : natural := 32;
  constant c_sub_stream_dat_w : natural := split_w(g_dat_w, c_min_dat_w, c_max_dat_w);  -- Calculate appropriate data width of internal streams
  constant c_nof_substreams   : natural := g_dat_w / c_sub_stream_dat_w;

  type t_streams_x_substreams_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(c_nof_substreams - 1 downto 0);

  signal substream_snk_diag_res       : t_streams_x_substreams_arr;
  signal substream_snk_diag_res_val   : t_streams_x_substreams_arr;

  signal tx_diag_req          : std_logic_vector(g_nof_streams - 1 downto 0);
  signal tx_siso_arr          : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal tx_sosi_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal tx_bg_siso_arr       : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal tx_bg_sosi_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal rx_siso_arr          : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal rx_sosi_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal tx_dpmon_siso_arr    : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal tx_dpmon_sosi_arr    : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal rx_dpmon_siso_arr    : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal rx_dpmon_sosi_arr    : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal tx_seq_out_val       : t_streams_x_substreams_arr;
  signal tb_clken             : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '1');  -- TB signal - can be forced to '0' to pause valid output and to '1' again to continue

  signal tx_clk               : std_logic_vector(g_nof_streams - 1 downto 0);
  signal tx_rst               : std_logic_vector(g_nof_streams - 1 downto 0);

  signal rx_clk               : std_logic_vector(g_nof_streams - 1 downto 0);
  signal rx_rst               : std_logic_vector(g_nof_streams - 1 downto 0);
begin
  gen_separate_clk : if g_separate_clk = true generate
    tx_clk <= src_clk;
    tx_rst <= src_rst;

    rx_clk <= snk_clk;
    rx_rst <= snk_rst;
  end generate;

  gen_nof_streams : for i in 0 to g_nof_streams - 1 generate
    gen_one_clk : if g_separate_clk = false generate
      tx_clk(i) <= clk;
      tx_rst(i) <= rst;

      rx_clk(i) <= clk;
      rx_rst(i) <= rst;
    end generate;

    snk_diag_res(i)     <= orv(substream_snk_diag_res(i));  -- Create a one-bit diag_res per stream
    snk_diag_res_val(i) <= andv(substream_snk_diag_res_val(i));  -- If all substream diag results are valid, the stream's diag_res is valid.

    u_tx_latency_adpt: entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => 1,
      g_out_latency => g_src_latency
      )
    port map (
      rst      => tx_rst(i),
      clk      => tx_clk(i),

      snk_out  => tx_dpmon_siso_arr(i),
      snk_in   => tx_dpmon_sosi_arr(i),

      src_out  => src_out_arr(i),
      src_in   => src_in_arr(i)
    );

    u_rx_latency_adpt: entity dp_lib.dp_latency_adapter
    generic map (
      g_in_latency  => g_snk_latency,
      g_out_latency => 1
    )
    port map (
      rst      => rx_rst(i),
      clk      => rx_clk(i),

      snk_out  => snk_out_arr(i),
      snk_in   => snk_in_arr(i),

      src_out  => rx_dpmon_sosi_arr(i),
      src_in   => rx_dpmon_siso_arr(i)
    );

    tx_diag_req(i) <= tx_siso_arr(i).ready and tx_siso_arr(i).xon;

    gen_bg : if g_block_len > 0 generate
      u_dp_block_gen: entity dp_lib.dp_block_gen
      generic map (
        g_nof_data => g_block_len,
        g_empty    => 0,
        g_channel  => 0,
        g_error    => 0
      )
      port map (
        rst        => tx_rst(i),
        clk        => tx_clk(i),

        src_in     => tx_bg_siso_arr(i),
        src_out    => tx_bg_sosi_arr(i),

        en         => src_diag_en(i)
      );

      tx_sosi_arr(i).sop      <= tx_bg_sosi_arr(i).sop;
      tx_sosi_arr(i).eop      <= tx_bg_sosi_arr(i).eop;
      tx_sosi_arr(i).empty    <= tx_bg_sosi_arr(i).empty;
      tx_sosi_arr(i).err      <= tx_bg_sosi_arr(i).err;
      tx_sosi_arr(i).channel  <= tx_bg_sosi_arr(i).channel;

      tx_bg_siso_arr(i).ready <= tx_siso_arr(i).ready;
    end generate;

    gen_nof_substreams : for j in c_nof_substreams - 1 downto 0 generate
      u_diag_tx_seq: entity diag_lib.diag_tx_seq
      generic map (
        g_dat_w  => c_sub_stream_dat_w
        )
      port map (
        rst      => tx_rst(i),
        clk      => tx_clk(i),
        clken    => tb_clken(i),

        diag_en  => src_diag_en(i),
        diag_sel => src_diag_md(i),
        diag_init => (others => '0'),

        diag_req => tx_diag_req(i),
        out_dat  => tx_sosi_arr(i).data( (j + 1) * c_sub_stream_dat_w - 1 downto (j + 1) * c_sub_stream_dat_w - c_sub_stream_dat_w ),
        out_val  => tx_seq_out_val(i)(j)
      );

      u_diag_rx_seq: entity diag_lib.diag_rx_seq
      generic map (
        g_dat_w      => c_sub_stream_dat_w,
        g_diag_res_w => c_sub_stream_dat_w
        )
      port map (
        rst          => rx_rst(i),
        clk          => rx_clk(i),

        diag_en      => snk_diag_en(i),
        diag_sel     => snk_diag_md(i),

        orv(diag_res) => substream_snk_diag_res(i)(j),  -- vector-wise OR to create a one-bit diag_result per substream
        diag_res_val => substream_snk_diag_res_val(i)(j),

        in_dat       => rx_sosi_arr(i).data( (j + 1) * c_sub_stream_dat_w - 1 downto (j + 1) * c_sub_stream_dat_w - c_sub_stream_dat_w),
        in_val       => rx_sosi_arr(i).valid
      );
    end generate;

    tx_sosi_arr(i).valid <= tx_seq_out_val(i)(0);

    rx_siso_arr(i).ready <= '1';  -- snk_diag_en(i);
    rx_siso_arr(i).xon   <= '1';  -- snk_diag_en(i);

    u_tx_dpmon : entity dp_lib.dp_mon
    generic map (
      g_latency  => 1
    )
    port map (
      rst      => tx_rst(i),
      clk      => tx_clk(i),
      en       => src_diag_en(i),

      snk_out  => tx_siso_arr(i),
      snk_in   => tx_sosi_arr(i),

      src_in   => tx_dpmon_siso_arr(i),
      src_out  => tx_dpmon_sosi_arr(i),

      clr      => src_val_cnt_clr(i),
      word_cnt => src_val_cnt(i)
    );

    u_rx_dpmon : entity dp_lib.dp_mon
    generic map (
      g_latency  => 1
    )
    port map (
      rst      => rx_rst(i),
      clk      => rx_clk(i),
      en       => snk_diag_en(i),

      snk_out  => rx_dpmon_siso_arr(i),
      snk_in   => rx_dpmon_sosi_arr(i),

      src_in   => rx_siso_arr(i),
      src_out  => rx_sosi_arr(i),

      clr      => snk_val_cnt_clr(i),
      word_cnt => snk_val_cnt(i)
    );
  end generate;
end str;
