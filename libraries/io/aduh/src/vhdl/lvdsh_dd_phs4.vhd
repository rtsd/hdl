-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Author: Eric Kooistra
-- Purpose: Detect the wide band factor phase 0, 1, 2 or 3 of the 200 MHz
--          dp_clk using the rising and falling edge of the double data rate
--          400 MHz in_clk clock. The aim is to detect same sample phase
--          after each restart of the ADC, without the need for calibration.
-- Description:
--   The edges of the in_clk occur at the 800 MHz sample rate of the ADC. The
--   in_clk is a double data rate (DDR) 400 MHz clock as indicated by
--   c_dd_factor=2. The DDIO elements near the pins capture the in_dat using
--   the in_clk into two  parts in_dat_hi and in_dat_lo.
--   The dp_clk runs at a c_rx_factor=2 lower rate than the in_clk so at 200
--   MHz. The dp_clk is locked to the in_clk. The dp_clk phase detector detects
--   the g_wb_factor=4 sample phase uncertainty that can occur between the
--   sample clock and the processing clock. With ADU the in_clk is the 800M
--   sample clock divided by c_dd_factor=2 and the dp_clk is the sample clock
--   divided by g_wb_factor=4.
--   The phase of the in_clk depends on the divider phase of the ADC. The phase
--   of the dp_clk with respect to the in_clk depends on the divide by 2 that
--   packs the dual sample words of the single data rate (SDR) rising edge
--   in_clk domain into 4 sample words for the dp_clk domain. The in_clk and
--   the dp_clk have a fixed but unknown phase relation between 0 and 3 =
--   g_wb_factor-1 samples.
--   The sample phase alignment within a g_wb_factor=4 sample word is done by
--   lvdsh_dd_phs4_align. After that the word phase is also aligned by
--   adjusting the roundtrip latency between wb_sync and dp_sync which are both
--   derived from the selected dp_phs_clk.
--   Both the sample phase alignment and the word phase alignment use the
--   dp_phs_clk as reference. For the sample phase alignment within a word the
--   dp_phs_clk could run at the same 200 MHz rate as the dp_clk. However to
--   support a roundtrip word latency of about 16 dp_clk cycles the dp_phs_clk
--   are divided by 32 at the PLL and only their rising edge is used to do the
--   alignments.
--   The g_nof_dp_phs_clk>= 1 and maximum 6. When g_nof_dp_phs_clk=6 then
--   typically each dp_phs_clk is shifted by 11.25 or 22.5 degrees where 90
--   degrees corresponds to 1.25 ns so 1 800MHz sample or a quarter dp_clk
--   period. By selecting one suitable out of a range of phase shifted
--   dp_phs_clk it is possible to find a proper dp_phs_clk reference for all
--   signal paths in the different nodes.
--   The whole sample phase realignment scheme relies on fixed clock
--   distribution delays on the boards and in the FPGA. Therefore a clock tree
--   network is used for the dp_phs_clk as well, even though the dp_phs_clk
--   does not clock any logic. The dp_phs_clk gets sampled by the dp_clk to
--   get wb_sync and by the in_clk to get dd_sync. The assumption is that the
--   setup time for this signal is small and nearly constant independent of
--   the size of the design and independent of the clock trees that are used.
--   The dd_sync is passed on to the dp_clk domain via a dual clock FIFO to
--   become dp_sync.
-- Remark:
-- . Support fixed raw data mode via g_nof_dp_phs_clk=0.
-- . Support dynamic MM selection between realigned data mode and raw data
--   mode via r_dp.dp_phs_align_en that depends on dp_phs_clk_en_vec/=0.
-- . The maximum g_nof_dp_phs_clk=6 because otherwise it does not fit in PLL_L3
--   that is near the input CLK pin. It seems preferable to use PLL_L3 with
--   CLK pin rather than a more remote PLL.
-- . The 6 dp_phs_clk at 200/32 MHz do not take extra power (< 0.1W) as was
--   measured for 4 BN on a UniBoard.
-- . On the Apertif subrack hardware with 16 BN and 8 ADU (so 64 SP = signal
--   paths) stable lock is always found and delta of 2 samples or multiple
--   of 4 samples (= 1 word @ 200MHz) between signal paths do not occur.
--   However there can still occur delta of 1 sample between signal paths. It
--   is important that the selected dp_phs_clk_vec phase always falls in the
--   same sample period, because otherwise there arises again an uncertainty
--   of 1 sample in the sample phase. A suitable ADU lock mechanism may be
--   either:
--   1) using 2 phase clocks with delta 22.5 degrees and that the phase is
--      found automatically, or
--   2) using 4 or 6 phase clocks with delta 22.5 or 11.25 degrees to cover
--      an entire sample period and via MM control one phase is selected per
--      BN- ADU pair. The MM control is then needed to select always the same
--      phase in case not always the same phase is found automatically.
-- . With multiple Apertif subracks it appears that the word alignment found
--   for all ADC paths is not always the same for all ADC paths. This cannot
--   be avoided with this lvdsh_dd_phs4, therefore any residual sample phase
--   uncertainty needs to be accounted for by calibration. However for most
--   ADC dp_dat paths this lvdsh_dd_phs4 will ensure a proper word alignment
--   that makes it feasible to perform coherent beamforming.
-- . On Apertif subracks it appears that sometimes over hours an initial word
--   alignment will get adjusted and this then causes a brief disturbance in
--   the dp_dat values or it may even cause a sample shift that remains. Such
--   a sample shift would harm the remainder of the observation. By setting
--   in_maintain_phs='1' after word alignment it can be ensured that this word
--   alignment maintained as long as the ADC in_clk remains active (so in
--   practise for as long as the observation will last or the Apertif frontend
--   and subracks remain on). The maintain lock feature is enabled when
--   g_maintain_phs = TRUE.
--

entity lvdsh_dd_phs4 is
  generic (
    g_sim                : boolean := false;
    g_wb_factor          : natural := 4;  -- fixed wideband factor 4 = c_rx_factor*c_dd_factor
    g_dp_phs_clk_period  : natural := 32;  -- number of dp_clk periods per dp_phs_clk period, must match g_clk*_divide_by in unb_clk200_pll
    g_nof_dp_phs_clk     : natural := 2;  -- nof dp_phs_clk that can be used to detect lock
    g_maintain_phs       : boolean := true;  -- when TRUE maintain the stable lock once it is found, else keep on checking whether the stable lock is still stable
    g_wb_use_rising_edge : boolean := false;  -- when TRUE using rising edge of dp_clk domain to capture wb_sync as reference for dp_sync from in_clk domain, else use falling edge
    g_in_dat_w           : natural := 8  -- nof PHY data bits
  );
  port (
    -- PHY input interface
    in_clk               : in  std_logic := '1';
    in_dat               : in  std_logic_vector(g_in_dat_w - 1 downto 0);  -- input samples [t0], [t1], [t2], [t3], [t4], [t5], [t6], [t7], ... --> time

    -- DD --> Rx domain interface at in_clk rate or g_wb_factor lower rate (via FIFO)
    dp_rst               : in  std_logic := '1';
    dp_clk               : in  std_logic := '1';
    dp_phs_clk_vec       : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);
    dp_phs_clk_en_vec    : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);  -- only the enabled dp_phs_clk will be used
    dp_dat               : out std_logic_vector(g_wb_factor * g_in_dat_w - 1 downto 0);  -- big endian output samples [t0, t1, t2, t3], [t4, t5, t6, t7], ...
    dp_val               : out std_logic;

    -- Rx status monitor
    out_status           : out std_logic_vector(c_word_w - 1 downto 0);  -- extra status information for debug
    out_phs_locked       : out std_logic;
    out_word_locked      : out std_logic;
    out_word_stable      : out std_logic;
    out_word_stable_ack  : in  std_logic := '0'
  );
end lvdsh_dd_phs4;

architecture str of lvdsh_dd_phs4 is
  constant c_word_align_en          : boolean := true;

  constant c_dd_factor              : natural := 2;  -- fixed double data rate factor
  constant c_rx_factor              : natural := 2;  -- fixed for g_wb_factor = c_rx_factor*c_dd_factor = 4
  constant c_dp_phs_align_en        : boolean := g_nof_dp_phs_clk > 0;  -- if there are no dp_phs_clk then fall back to raw data mode
  constant c_nof_dp_phs_clk         : natural := sel_a_b(c_dp_phs_align_en, g_nof_dp_phs_clk, 1);  -- map to dummy 1 when g_nof_dp_phs_clk=0 to avoid compile error on NATURAL RANGE 0 TO -1

  constant c_in_period_w            : natural := 3;  -- large enough to account for dd_factor and small to be able to detect short in_clk inactive period
  constant c_dp_detect_period       : natural := 2**c_in_period_w / c_dd_factor;

  constant c_raw_phs_w              : natural := g_wb_factor;  -- = 4 * 1b = 4b
  constant c_sync_w                 : natural := 1;
  constant c_dp_dat_w               : natural := g_wb_factor * g_in_dat_w;  -- = 4 * 8b = 32b
  constant c_fifo_dat_w             : natural := c_raw_phs_w + c_sync_w + c_dp_dat_w;
  constant c_fifo_size              : natural := 256;  -- 36b * 256 = 1 M9K
  constant c_fifo_size_w            : natural := ceil_log2(c_fifo_size);
  constant c_fifo_wr_pipeline       : natural := 1;  -- 0 or 1 optionally to pipeline FIFO wr_req to have mor margin with dp_clk edge
  constant c_in_rst_delay_len       : natural := 16;
  constant c_delay_len              : natural := c_meta_delay_len;
  constant c_word_req_lat           : natural := 4;
  constant c_dp_phs_align_restart_w : natural := 5;

  constant c_in_dd_phs_locked_w     : natural := sel_a_b(g_sim, 12, 30);  -- used to ensure that dd_phs_locked is only declared if dd_phs is detected ok for at least 2**(g_dd_phs_locked_w-1) in_clk cycles
  constant c_dp_dd_phs_timeout_w    : natural := c_in_dd_phs_locked_w;  -- dd_phs locked timeout in dp_clk domain, so c_dd_factor=2 longer than c_in_dd_phs_locked_w, because c_in_dd_phs_locked_w runs
                                                                            -- in the in_clk domain. This is enough to find stable lock if it is possible to have stable lock at the selected dp_phs_clk.

  constant c_wb_sync_period         : natural := g_dp_phs_clk_period;  -- nof dp_clk cycles for wb_sync period, must be > c_wb_sync_roundtrip
  constant c_wb_cnt_w               : natural := ceil_log2(c_wb_sync_period);
  constant c_wb_sync_in_latency     : natural := 5;  -- estimated nof dp_clk cycles latency of wb_sync transfer from dp_clk to in_clk domain and the latency in lvdsh_dd_phs4_align
  constant c_wb_fifo_fill_latency   : natural := 5;  -- estimated nof dp_clk cycles latency of fifo_rdusedw - fifo_wrused
  constant c_wb_fifo_fill_level     : integer := 12;  -- set expected FIFO fill level on read side  (mean level +- 1 can occur)
  constant c_wb_sync_roundtrip        : natural := c_wb_sync_in_latency + c_wb_fifo_fill_latency + c_wb_fifo_fill_level;  -- achieve this nof dp_clk cycles from getting wb_sync back via dp_sync
  constant c_wb_sync_roundtrip_margin : natural := 2;
  constant c_wb_sync_roundtrip_max    : natural := c_wb_sync_roundtrip + c_wb_sync_roundtrip_margin;
  constant c_wb_sync_roundtrip_min    : natural := c_wb_sync_roundtrip - c_wb_sync_roundtrip_margin;
  constant c_wb_fifo_fill_margin    : natural := 2;  -- some symmetrical FIFO filled margin >= 0, use 0 in theory, use 1 to allow some timing uncertainty in rdusedw of dual clock FIFO
  constant c_wb_fifo_fill_margin_p  : natural := c_wb_fifo_fill_margin;  -- some FIFO more filled margin >= 0, use 0 or 1 to allow 8 and 9
  constant c_wb_fifo_fill_margin_n  : natural := c_wb_fifo_fill_margin;  -- some FIFO less filled margin >= 0, use 1 or 2 to allow 7 and 6
  constant c_wb_fifo_fill_max       : natural := c_wb_fifo_fill_level + c_wb_fifo_fill_margin_p;  -- maximum FIFO fill level at any time
  constant c_wb_fifo_fill_min       : natural := c_wb_fifo_fill_level - c_wb_fifo_fill_margin_n;  -- minimum FIFO fill level during lock

  -- view debug signals in Wave Window
  signal dbg_c_dp_phs_align_en      : boolean := c_dp_phs_align_en;
  signal dbg_c_wb_sync_period       : natural := c_wb_sync_period;
  signal dbg_c_wb_sync_in_latency   : natural := c_wb_sync_in_latency;
  signal dbg_c_wb_fifo_fill_latency : natural := c_wb_fifo_fill_latency;
  signal dbg_c_wb_fifo_fill_max     : natural := c_wb_fifo_fill_max;
  signal dbg_c_wb_fifo_fill_level   : natural := c_wb_fifo_fill_level;  -- must be > c_word_req_lat to ensure that fifo_rdusedw > 0
  signal dbg_c_wb_fifo_fill_min     : natural := c_wb_fifo_fill_min;
  signal dbg_c_wb_sync_roundtrip    : natural := c_wb_sync_roundtrip;
  signal dbg_c_wb_sync_roundtrip_max: natural := c_wb_sync_roundtrip_max;
  signal dbg_c_wb_sync_roundtrip_min: natural := c_wb_sync_roundtrip_min;

  signal dbg_status_0_out_word_locked       : std_logic;
  signal dbg_status_1_out_word_stable       : std_logic;
  signal dbg_status_3_dp_fifo_fill_stable   : std_logic;
  signal dbg_status_4_dp_phs_stable         : std_logic;
  signal dbg_status_5_wb_roundtrip_stable   : std_logic;
  signal dbg_status_6_dp_in_clk_detected    : std_logic;
  signal dbg_status_7_dp_in_clk_stable      : std_logic;
  signal dbg_status_15_8_fifo_rdusedw       : std_logic_vector(7 downto 0);
  signal dbg_status_23_16_wb_cnt_roundtrip  : std_logic_vector(7 downto 0);
  signal dbg_status_27_24_dp_phs_clk_select : std_logic_vector(3 downto 0);
  signal dbg_status_31_28_dp_raw_phs        : std_logic_vector(3 downto 0);

  -- in_clk domain
  type t_in_reg is record
    -- data with realignment
    dd_sync_locked      : std_logic;
    -- data without realignment
    in_dat_hi           : std_logic_vector(g_in_dat_w - 1 downto 0);
    in_dat_lo           : std_logic_vector(g_in_dat_w - 1 downto 0);
    dd_in_data          : std_logic_vector(c_dp_dat_w - 1 downto 0);
    dd_in_val           : std_logic;
    -- write FIFO
    fifo_wr_dat         : std_logic_vector(c_fifo_dat_w - 1 downto 0);
    fifo_wr_dat_p       : std_logic_vector(c_fifo_dat_w - 1 downto 0);
    fifo_wr_req         : std_logic;
    fifo_wr_req_p       : std_logic;
  end record;

  signal i_out_status        : std_logic_vector(c_word_w - 1 downto 0);
  signal i_out_word_stable   : std_logic;

  signal in_rst              : std_logic;
  signal in_fifo_wr_dat      : std_logic_vector(c_fifo_dat_w - 1 downto 0);
  signal in_fifo_wr_req      : std_logic;
  signal in_phs_align_en     : std_logic;
  signal in_maintain_phs     : std_logic := '0';

  signal r_in                : t_in_reg;
  signal nxt_r_in            : t_in_reg;

  signal in_dat_hi           : std_logic_vector(g_in_dat_w - 1 downto 0);  -- input samples [t1], [t3], [t5], [t7], ... --> time
  signal in_dat_lo           : std_logic_vector(g_in_dat_w - 1 downto 0);  -- input samples [t0], [t2], [t4], [t6], ... --> time

  signal raw_phs             : std_logic_vector(g_wb_factor - 1 downto 0);  -- measured dd phase before realignment
  signal dd_phs_locked       : std_logic;
  signal dd_sync             : std_logic_vector(g_wb_factor - 1 downto 0);
  signal dd_sync_sl          : std_logic;
  signal dd_dat              : std_logic_vector(c_dp_dat_w - 1 downto 0);
  signal dd_val              : std_logic;

  signal fifo_wrusedw        : std_logic_vector(c_fifo_size_w - 1 downto 0);

  -- dp_clk domain
  type t_dp_reg is record
    wb_sync_cap          : std_logic;
    wb_sync              : std_logic;
    dp_in_rst_req        : std_logic;
    fifo_rd_req          : std_logic;
    le_sync              : std_logic;
    le_raw_phs           : std_logic_vector(g_wb_factor - 1 downto 0);
    le_dat               : std_logic_vector(c_dp_dat_w - 1 downto 0);
    le_val               : std_logic;
    dp_raw_phs           : std_logic_vector(g_wb_factor - 1 downto 0);
    dp_dat               : std_logic_vector(g_wb_factor * g_in_dat_w - 1 downto 0);  -- big endian output samples [t0, t1, t2, t3], [t4, t5, t6, t7], ...
    dp_val               : std_logic;
    dp_phs_locked        : std_logic;
    dp_phs_align_en      : std_logic;
    dp_phs_clk_en_vec    : std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);
    dp_phs_clk_select    : natural range 0 to c_nof_dp_phs_clk - 1;
    dp_sync_cap          : std_logic;
    prev_dp_sync_cap     : std_logic;
    dp_sync              : std_logic;
    wb_cnt_clr           : std_logic;
    wb_cnt_roundtrip     : std_logic_vector(c_wb_cnt_w - 1 downto 0);
    wb_cnt_roundtrip_hold: std_logic_vector(c_wb_cnt_w - 1 downto 0);
    dp_word_req          : std_logic;
    dp_word_req_dly      : std_logic_vector(c_word_req_lat - 1 downto 0);
    dp_word_locked       : std_logic;
    dp_word_lock_failed  : std_logic;
    dp_maintain_phs      : std_logic;
  end record;

  signal r_dp                        : t_dp_reg;
  signal nxt_r_dp                    : t_dp_reg;

  signal dp_in_clk_stopped           : std_logic;
  signal dp_in_clk_detected          : std_logic;
  signal dp_in_clk_stable            : std_logic;
  signal dp_in_enabled               : std_logic;

  signal fifo_rd_dat                 : std_logic_vector(c_fifo_dat_w - 1 downto 0);
  signal fifo_rd_val                 : std_logic;
  signal fifo_rd_emp                 : std_logic;
  signal fifo_rdusedw                : std_logic_vector(c_fifo_size_w - 1 downto 0);
  signal dp_fifo_fill_lock           : std_logic;
  signal dp_fifo_fill_stable         : std_logic;

  signal dp_phs_clk_en_vec_evt       : std_logic;
  signal dp_phs_timeout_cnt          : std_logic_vector(c_dp_dd_phs_timeout_w - 1 downto 0);
  signal dp_phs_timeout_cnt_clr      : std_logic;
  signal dp_phs_timeout              : std_logic;
  signal dp_phs_val                  : std_logic;
  signal dp_phs_lock_lost            : std_logic;
  signal dp_phs_stable               : std_logic;

  signal dp_phs_align_restart        : std_logic;
  signal dp_phs_align_restart_extend : std_logic;
  signal dp_phs_align_restart_revt   : std_logic;

  signal wb_phs_clk                  : std_logic;
  signal wb_sync_cap                 : std_logic;
  signal wb_cnt                      : std_logic_vector(c_wb_cnt_w - 1 downto 0);
  signal wb_roundtrip_expected       : std_logic;
  signal wb_roundtrip_lock           : std_logic;
  signal wb_roundtrip_stable         : std_logic;
begin
  ------------------------------------------------------------------------------
  -- Reset input section when lock is lost
  ------------------------------------------------------------------------------

  --  Multiple triggers for dp_phs_align_restart
  --
  --  . The dp_phs_align_restart is an input to the dp_in_rst_req that drives in_rst for lvdsh_dd_phs4_align
  --  . When out_word_stable='1' then dp_maintain_phs='1' if g_maintain_phs=TRUE. The dp_phs_timeout and dp_phs_lock_lost can then
  --    no longer occur, because the lvdsh_dd_phs4_align then continues on the current phs using common_toggle_align.
  --    The dp_word_lock_failed can still occur, but only if the course check on wb_cnt_roundtrip latency and fifo_rdusedw FIFO fill
  --    level fails by more than a +-1 margin.
  --dp_phs_align_restart <= dp_phs_timeout OR dp_phs_lock_lost OR r_dp.dp_word_lock_failed;     -- a) should be robust enough to never detect loss lock as long as in_clk stays active
  dp_phs_align_restart <= dp_phs_timeout or dp_phs_lock_lost or (r_dp.dp_word_lock_failed and not r_dp.dp_maintain_phs);  -- b) even a bit stronger then a)
  --dp_phs_align_restart <= (dp_phs_timeout OR dp_phs_lock_lost OR r_dp.dp_word_lock_failed) AND (NOT r_dp.dp_maintain_phs);     -- c) is equivalent to b) so should not make a difference

  -- Extend dp_phs_align_restart to filter out any subsequent restart triggers
  u_common_pulse_extend : entity common_lib.common_pulse_extend
  generic map (
    g_rst_level    => '0',
    g_p_in_level   => '1',
    g_ep_out_level => '1',
    g_extend_w     => c_dp_phs_align_restart_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    p_in    => dp_phs_align_restart,
    ep_out  => dp_phs_align_restart_extend
  );

  -- Ensure dp_phs_align_restart will cause only a single increment for dp_phs_clk_select
  u_common_evt_0 : entity common_lib.common_evt
  generic map (
    g_evt_type   => "RISING",
    g_out_reg    => false  -- if TRUE then the output is registered, else it is not
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    in_sig  => dp_phs_align_restart_extend,
    out_evt => dp_phs_align_restart_revt
  );

  -- Ensure that the FIFO is not read when it is reset, to avoid dp_dat becoming 0 which would complicate the verification in the tb
  dp_in_enabled <= '1' when dp_phs_align_restart = '0' and dp_phs_align_restart_extend = '0' else '0';

  -- Reset the input when:
  -- . the in_clk has stopped, or
  -- . the dp_phs_clk_en_vec was changed via MM, or
  -- . an internal dp_phs_align_restart request occured
  -- When out_word_stable='1' then dp_maintain_phs='1' if g_maintain_phs=TRUE. The dp_in_clk_stopped and dp_phs_clk_en_vec_evt
  -- can the then still cause a reset. When dp_maintain_phs='1' the dp_phs_align_restart will effectively not occur anymore,
  -- because both dp_clk and in_clk are active and in lock and any jitter is ignored.
  nxt_r_dp.dp_in_rst_req <= dp_in_clk_stopped or dp_phs_clk_en_vec_evt or dp_phs_align_restart_extend;

  u_common_async_in_rst : entity common_lib.common_async
  generic map (
    g_rst_level => '1',
    g_delay_len => c_in_rst_delay_len
  )
  port map (
    rst  => r_dp.dp_in_rst_req,  -- asynchronous rst ensures that it will take effect also when in_clk is not running
    clk  => in_clk,
    din  => '0',
    dout => in_rst
  );

  ------------------------------------------------------------------------------
  -- Detect whether the in_clk is active
  ------------------------------------------------------------------------------

  u_common_clock_active_detector : entity common_lib.common_clock_active_detector
  generic map (
    g_in_period_w       => c_in_period_w,
    g_dp_detect_period  => c_dp_detect_period,
    g_dp_detect_margin  => 1
  )
  port map (
    -- PHY input interface
    in_clk               => in_clk,
    dp_clk               => dp_clk,
    dp_in_clk_detected   => dp_in_clk_detected,
    dp_in_clk_stable     => dp_in_clk_stable,
    dp_in_clk_stable_ack => out_word_stable_ack
  );

  dp_in_clk_stopped <= not dp_in_clk_detected;

  ------------------------------------------------------------------------------
  -- Capture the double data rate input sample data
  ------------------------------------------------------------------------------

  -- Double data rate input cell at pin, also ensures deterministic input timing
  u_common_ddio_in : entity common_lib.common_ddio_in
  generic map (
    g_width => g_in_dat_w
  )
  port map (
    in_dat      => in_dat,
    in_clk      => in_clk,
    out_dat_hi  => in_dat_hi,
    out_dat_lo  => in_dat_lo
  );

  nxt_r_in.in_dat_hi <= in_dat_hi;
  nxt_r_in.in_dat_lo <= in_dat_lo;

  gen_dp_phs_align_en_1 : if c_dp_phs_align_en = true generate
    ------------------------------------------------------------------------------
    -- Capture the '0' to '1' transition of the dp_phs_clk
    ------------------------------------------------------------------------------

    wb_phs_clk <= dp_phs_clk_vec(0);

    u_common_async_wb_sync : entity common_lib.common_async
    generic map (
      g_rising_edge => g_wb_use_rising_edge,
      g_rst_level   => '0',
      g_delay_len   => c_delay_len  -- typically 1 should be sufficient, but more is fine too
    )
    port map (
      rst  => dp_rst,
      clk  => dp_clk,
      din  => wb_phs_clk,
      dout => wb_sync_cap
    );

    nxt_r_dp.wb_sync_cap <= wb_sync_cap;

    nxt_r_dp.wb_sync <= '1' when wb_sync_cap = '1' and r_dp.wb_sync_cap = '0' else '0';

    ------------------------------------------------------------------------------
    -- Detect and align to the dp_clk phase
    ------------------------------------------------------------------------------

    u_lvdsh_dd_phs4_align : entity work.lvdsh_dd_phs4_align
    generic map (
      g_wb_factor         => g_wb_factor,
      g_nof_dp_phs_clk    => g_nof_dp_phs_clk,
      g_dp_phs_clk_period => g_dp_phs_clk_period,
      g_dd_phs_locked_w   => c_in_dd_phs_locked_w,
      g_in_dat_w          => g_in_dat_w
    )
    port map (
      -- DP clock reference for word alignment
      dp_phs_clk_vec     => dp_phs_clk_vec,
      dp_phs_clk_select  => r_dp.dp_phs_clk_select,

      -- PHY input interface
      in_rst             => in_rst,
      in_clk             => in_clk,
      in_dat_hi          => in_dat_hi,
      in_dat_lo          => in_dat_lo,
      in_maintain_phs    => in_maintain_phs,

      raw_phs            => raw_phs,
      out_phs_locked     => dd_phs_locked,
      out_sync           => dd_sync,
      out_dat            => dd_dat,
      out_val            => dd_val
    );

    dd_sync_sl <= andv(dd_sync);  -- after sample phase realignment the dd_sync derived from dp_clk_phs in the in_clk domain toggles between 0x0 and 0xF.

    nxt_r_in.dd_sync_locked <= '0' when dd_phs_locked = '0' else
                               '1' when dd_sync_sl = '1' else r_in.dd_sync_locked;  -- dd_phs_locked goes '1' when dd_sync_sl='0', so dd_sync_locked starts after next dd_sync_sl rising event
  end generate;  -- gen_dp_phs_align_en_1

  ------------------------------------------------------------------------------
  -- Support raw data without realignment
  ------------------------------------------------------------------------------

  u_common_async_in_phs_align_en : entity common_lib.common_async
  generic map (
    g_delay_len   => c_delay_len
  )
  port map (
    rst  => '0',
    clk  => in_clk,
    din  => r_dp.dp_phs_align_en,
    dout => in_phs_align_en
  );

  nxt_r_in.dd_in_data <= in_dat_hi & in_dat_lo & r_in.in_dat_hi & r_in.in_dat_lo;
  nxt_r_in.dd_in_val  <= not r_in.dd_in_val;  -- toggle to implement c_dd_factor=2 divider

  ------------------------------------------------------------------------------
  -- Use a dual clock FIFO to bring the data to the dp_clk domain
  ------------------------------------------------------------------------------

  -- Register fifo_wr_dat to ease timing closure between DDIO and FIFO at input clock rate
  -- . dd_val toggles due dd_factor between in_clk rate and dp_clk rate
  -- . use dd_phs_locked seems not needed, but could help to ensure the FIFO does not get filled with spurious initial samples
  -- . use dd_sync_locked to align start of fifo_wr_req seems not needed, but could help to ensure proper word aligment between nodes
  nxt_r_in.fifo_wr_dat <= raw_phs        &        dd_sync_sl     &   dd_dat when in_phs_align_en = '1' else "1111" & '1' & r_in.dd_in_data;
  nxt_r_in.fifo_wr_req <=  dd_phs_locked and r_in.dd_sync_locked and dd_val when in_phs_align_en = '1' else                r_in.dd_in_val;

  nxt_r_in.fifo_wr_dat_p <= r_in.fifo_wr_dat;
  nxt_r_in.fifo_wr_req_p <= r_in.fifo_wr_req;

  in_fifo_wr_dat <= r_in.fifo_wr_dat when c_fifo_wr_pipeline = 0 else r_in.fifo_wr_dat_p;
  in_fifo_wr_req <= r_in.fifo_wr_req when c_fifo_wr_pipeline = 0 else r_in.fifo_wr_req_p;

  -- Dual clock FIFO
  u_common_fifo_dc : entity common_lib.common_fifo_dc
  generic map (
    g_dat_w      => c_fifo_dat_w,
    g_nof_words  => c_fifo_size
  )
  port map (
    rst     => in_rst,
    wr_clk  => in_clk,
    wr_dat  => in_fifo_wr_dat,
    wr_req  => in_fifo_wr_req,
    wr_ful  => OPEN,
    wrusedw => fifo_wrusedw,
    rd_clk  => dp_clk,
    rd_dat  => fifo_rd_dat,
    rd_req  => r_dp.fifo_rd_req,
    rd_emp  => fifo_rd_emp,
    rdusedw => fifo_rdusedw,
    rd_val  => fifo_rd_val
  );

  nxt_r_dp.le_raw_phs      <= fifo_rd_dat(c_raw_phs_w + c_sync_w + c_dp_dat_w - 1 downto c_sync_w + c_dp_dat_w);  -- 4b
  nxt_r_dp.le_sync         <= fifo_rd_dat(                                                  c_dp_dat_w);  -- 1b
  nxt_r_dp.le_dat          <= fifo_rd_dat(                     c_dp_dat_w - 1 downto                   0);  -- 32b
  nxt_r_dp.le_val          <= fifo_rd_val;

  nxt_r_dp.dp_raw_phs      <=      r_dp.le_raw_phs                       when r_dp.le_val = '1' else r_dp.dp_raw_phs;  -- Keep measured phase in little endian order
  nxt_r_dp.dp_sync_cap     <=      r_dp.le_sync                          when r_dp.le_val = '1' else r_dp.dp_sync_cap;  -- Hold to avoid false dp_sync_cap due to fifo_rd_req='0'
  nxt_r_dp.dp_dat          <= hton(r_dp.le_dat, g_in_dat_w, g_wb_factor) when r_dp.le_val = '1' else r_dp.dp_dat;  -- Rewire le_dat data words with [t3, t2, t1, t0] to big endian dp_dat data words with sample order [t0, t1, t2, t3]
  nxt_r_dp.dp_val          <=      r_dp.le_val;

  nxt_r_dp.prev_dp_sync_cap <= r_dp.dp_sync_cap;
  nxt_r_dp.dp_sync <= '1' when r_dp.dp_sync_cap = '1' and r_dp.prev_dp_sync_cap = '0' else '0';

  dp_dat <= r_dp.dp_dat;
  dp_val <= r_dp.dp_val;

  dp_phs_val <= '1' when r_dp.dp_val = '1' or r_dp.dp_word_req_dly(c_word_req_lat - 1) = '0' else '0';  -- Mask the gaps in dp_val due to fifo_rd_req='0'

  nxt_r_dp.dp_phs_locked <= dp_phs_val;  -- The input phase is locked when the FIFO does not run empty, so the in_clk is running active AND that dd_phs=c_exp_phs.

  out_phs_locked <= r_dp.dp_phs_locked;

  u_common_evt_1 : entity common_lib.common_evt
  generic map (
    g_evt_type => "FALLING"
  )
  port map (
    rst      => dp_rst,
    clk      => dp_clk,
    in_sig   => r_dp.dp_phs_locked,
    out_evt  => dp_phs_lock_lost
  );

  ------------------------------------------------------------------------------
  -- Select dp_phs_clk
  ------------------------------------------------------------------------------

  -- Detect dp_phs_clk_en_vec=0 to disable realignment
  nxt_r_dp.dp_phs_align_en <= '1' when c_dp_phs_align_en = true and unsigned(dp_phs_clk_en_vec) /= 0 else '0';

  -- Detect MM change in dp_phs_clk_en_vec
  nxt_r_dp.dp_phs_clk_en_vec <= dp_phs_clk_en_vec;

  dp_phs_clk_en_vec_evt <= '1' when c_dp_phs_align_en = true and unsigned(r_dp.dp_phs_clk_en_vec) /= unsigned(dp_phs_clk_en_vec) else '0';

  gen_dp_phs_align_en_2 : if c_dp_phs_align_en = true generate
    -- Control dp_phs_clk_select
    p_dp_phs_clk_select : process (r_dp, dp_in_clk_stopped, dp_phs_clk_en_vec_evt, dp_phs_align_restart_revt, dp_phs_clk_en_vec)
    begin
      nxt_r_dp.dp_phs_clk_select <= r_dp.dp_phs_clk_select;

      if dp_in_clk_stopped = '1' or dp_phs_clk_en_vec_evt = '1' then
        -- in_clk inactive        : reset to initial dp_clk_phs
        -- phs_clk_en_vec changed : restart from initial dp_clk_phs
        nxt_r_dp.dp_phs_clk_select  <= 0;
      else
        -- in_clk active: select next dp_clk_phs
        -- . dp_phs_align_restart_revt:
        --   . dp_phs_timeout      : cannot achieve lock with in_clk on this dp_clk_phs, so try next dp_clk_phs
        --   . dp_phs_lock_lost    : lock lost due to meta stable timing error between in_clk and this dp_clk_phs, so try next dp_clk_phs
        --   . dp_word_lock_failed : word lock lost, avoid remaining unable to achieve word lock on this dp_clk_phs, so try next dp_clk_phs
        -- . dp_phs_clk_en_vec     : if dp_phs_clk is not enabled then select next dp_phs_clk
        if dp_phs_align_restart_revt = '1' or dp_phs_clk_en_vec(r_dp.dp_phs_clk_select) = '0' then
          if r_dp.dp_phs_clk_select = g_nof_dp_phs_clk - 1 then
            nxt_r_dp.dp_phs_clk_select <= 0;
          else
            nxt_r_dp.dp_phs_clk_select <= r_dp.dp_phs_clk_select + 1;
          end if;
        end if;
      end if;
    end process;
  end generate;  -- gen_dp_phs_align_en_2

  ------------------------------------------------------------------------------
  -- DD phase lock timeout
  ------------------------------------------------------------------------------
  u_common_counter_dp_phs_timeout_cnt : entity common_lib.common_counter
  generic map (
    g_width => c_dp_dd_phs_timeout_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    cnt_clr => dp_phs_timeout_cnt_clr,
    count   => dp_phs_timeout_cnt
  );

  dp_phs_timeout_cnt_clr <= dp_in_clk_stopped or r_dp.dp_phs_locked or dp_phs_timeout;  -- clear the dp_phs_timeout timer when there is no active in_clk or when the dp_phs is locked
  dp_phs_timeout <= dp_phs_timeout_cnt(dp_phs_timeout_cnt'high);  -- dp_phs_timeout pulse also restarts the timer

  ------------------------------------------------------------------------------
  -- Determine DP word lock based on the wb_sync - dp_sync roundtrip latency via in_clk domain
  ------------------------------------------------------------------------------

  -- Measure and align the wb_sync to dp_sync roundtrip latency
  u_common_counter_wb_cnt : entity common_lib.common_counter
  generic map (
    g_width => c_wb_cnt_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    cnt_clr => r_dp.wb_cnt_clr,
    count   => wb_cnt
  );

  -- Default read when not empty, so the FIFO cannot run full
  nxt_r_dp.fifo_rd_req <= not fifo_rd_emp and dp_in_enabled and r_dp.dp_word_req;

  nxt_r_dp.dp_word_req_dly(c_word_req_lat - 1 downto 0) <= r_dp.dp_word_req_dly(c_word_req_lat - 2 downto 0) & r_dp.dp_word_req;

  -- Detect expected wb_cnt of roundtrip latency
  wb_roundtrip_expected <= '1' when unsigned(r_dp.wb_cnt_roundtrip) = c_wb_sync_roundtrip else '0';

  p_dp_word_lock : process (r_dp, wb_cnt, wb_roundtrip_expected, fifo_rd_emp, fifo_rdusedw)
  begin
    nxt_r_dp.wb_cnt_clr            <= '1';
    nxt_r_dp.wb_cnt_roundtrip      <= (others => '0');
    nxt_r_dp.wb_cnt_roundtrip_hold <= (others => '0');

    nxt_r_dp.dp_word_req         <= '1';
    nxt_r_dp.dp_word_locked      <= '0';
    nxt_r_dp.dp_word_lock_failed <= '0';

    if r_dp.dp_phs_align_en = '1' then
      --------------------------------------------------------------------------
      -- Realignment enabled
      --------------------------------------------------------------------------
      if r_dp.dp_phs_locked = '1' then
        -- Measure wb_sync - dp_sync roundtrip latency
        nxt_r_dp.wb_cnt_clr            <= r_dp.wb_cnt_clr;
        nxt_r_dp.wb_cnt_roundtrip      <= r_dp.wb_cnt_roundtrip;
        nxt_r_dp.wb_cnt_roundtrip_hold <= r_dp.wb_cnt_roundtrip_hold;
        if r_dp.wb_sync = '1' then
          nxt_r_dp.wb_cnt_clr <= '0';
        elsif r_dp.dp_sync = '1' then
          nxt_r_dp.wb_cnt_clr <= '1';
          nxt_r_dp.wb_cnt_roundtrip <= wb_cnt;
          nxt_r_dp.wb_cnt_roundtrip_hold <= r_dp.wb_cnt_roundtrip;
        end if;

        -- Word lock control
        -- . Note: In practise it appears necessary to add dp_word_lock_failed check, because it is not sufficient to rely on dp_phs_lock_lost alone
        nxt_r_dp.dp_word_locked <= r_dp.dp_word_locked;
        if r_dp.dp_word_locked = '0' then
          -- Word lock acquisition
          if r_dp.dp_sync = '1' then  -- dp_sync implies also that the FIFO is not empty, so no need to check fifo_rd_emp to avoid false dp_phs_val via dp_word_req_dly
            -- Adjust wb_sync - dp_sync roundtrip latency to become c_wb_sync_roundtrip
            if unsigned(wb_cnt) < c_wb_sync_roundtrip then
              nxt_r_dp.dp_word_req <= '0';  -- too early dp_sync so slip 1 word
            elsif unsigned(wb_cnt) = c_wb_sync_roundtrip then
              nxt_r_dp.dp_word_locked <= '1';  -- declare word locked

              -- Fine check FIFO fill level when lock is declared
              if unsigned(fifo_rdusedw) < c_wb_fifo_fill_min then
                nxt_r_dp.dp_word_lock_failed <= '1';  -- unexpected change in FIFO fill level, so recover via in_rst
              end if;
            end if;

            -- Fine check that latency is incrementing properly during word lock acquisition
            if unsigned(r_dp.wb_cnt_roundtrip_hold) > 0 then
              if unsigned(r_dp.wb_cnt_roundtrip_hold) /= unsigned(r_dp.wb_cnt_roundtrip) - 1 then
                nxt_r_dp.dp_word_lock_failed <= '1';  -- wrong roundtrip latency increment, so recover via in_rst
              end if;
            end if;
          end if;
        else
          -- *** Word locked, this is the correct end state for the ALIGNED data processing ***

          -- Fine check roundtrip latency during stable word lock
          if wb_roundtrip_expected = '0' and r_dp.dp_maintain_phs = '0' then
            -- After achieving dp_word_locked, but while dp_maintain_phs is still '0' the wb_cnt_roundtrip must exactly match c_wb_sync_roundtrip.
            nxt_r_dp.dp_word_lock_failed <= '1';  -- word lock lost, so recover via in_rst
          end if;
          -- Fine check FIFO fill level during stable word lock
          if unsigned(fifo_rdusedw) < c_wb_fifo_fill_min then
            nxt_r_dp.dp_word_lock_failed <= '1';  -- unexpected change in FIFO fill level, so recover via in_rst
          end if;
        end if;
      end if;

    else
      --------------------------------------------------------------------------
      -- Realignment disabled : raw data
      --------------------------------------------------------------------------
      if r_dp.dp_phs_locked = '1' then
        -- Word lock control
        nxt_r_dp.dp_word_locked <= r_dp.dp_word_locked;
        if r_dp.dp_word_locked = '0' then
          -- Word lock acquisition
          if fifo_rd_emp = '0' then  -- no dp_sync in raw mode, so need to use fifo_rd_emp to check that FIFO is not empty to avoid false dp_phs_val due to dp_word_req_dly
            if unsigned(fifo_rdusedw) <= c_wb_fifo_fill_level - c_word_req_lat then
              nxt_r_dp.dp_word_req <= '0';  -- too few words in FIFO so slip 1 word
            elsif unsigned(fifo_rdusedw) >= c_wb_fifo_fill_min then
              nxt_r_dp.dp_word_locked <= '1';  -- declare word locked
            end if;
          end if;
        else
          -- *** Word locked, this is the correct end state for the RAW data processing ***

          -- Fine check FIFO fill level during stable word lock
          if unsigned(fifo_rdusedw) < c_wb_fifo_fill_min then
            nxt_r_dp.dp_word_lock_failed <= '1';  -- unexpected change in FIFO fill level, so recover via in_rst
          end if;
        end if;
      end if;
    end if;

    -- Course check roundtrip latency and FIFO fill level at any time
    if unsigned(wb_cnt) > c_wb_sync_roundtrip_max then
      nxt_r_dp.dp_word_lock_failed <= '1';  -- timeout dp_sync, so recover via in_rst
    end if;
    if unsigned(fifo_rdusedw) > c_wb_fifo_fill_max then
      nxt_r_dp.dp_word_lock_failed <= '1';  -- unexpected change in FIFO fill level, so recover via in_rst
    end if;
  end process;

  -- Monitor wb_cnt of roundtrip latency, use >min and <max instead of >=min and <=max to over constrain it compared to the checks in p_dp_word_lock
  wb_roundtrip_lock <= '1' when unsigned(r_dp.wb_cnt_roundtrip) >= c_wb_sync_roundtrip_min and unsigned(r_dp.wb_cnt_roundtrip) <= c_wb_sync_roundtrip_max else '0';

  -- Monitor fifo_rdusedw, use >min and <max instead of >=min and <=max to over constrain it compared to the checks in p_dp_word_lock
  dp_fifo_fill_lock <= '1' when unsigned(fifo_rdusedw) > c_wb_fifo_fill_min and unsigned(fifo_rdusedw) < c_wb_fifo_fill_max else '0';

  ------------------------------------------------------------------------------
  -- Monitor the dp_clk phase alignment
  ------------------------------------------------------------------------------

  p_out_status : process(dp_fifo_fill_stable, dp_phs_stable, wb_roundtrip_stable, dp_in_clk_stable, dp_in_clk_detected, fifo_rdusedw, r_dp)
  begin
    -- Debug monitor status
    i_out_status               <= (others => '0');
    i_out_status(           3) <= dp_fifo_fill_stable;  -- 1 bit
    i_out_status(           4) <= dp_phs_stable;  -- 1 bit
    i_out_status(           5) <= wb_roundtrip_stable;  -- 1 bit
    i_out_status( 7 downto  6) <= dp_in_clk_stable & dp_in_clk_detected;  -- 2 bit
    i_out_status(15 downto  8) <= fifo_rdusedw;  -- c_fifo_size_w = 8
    i_out_status(23 downto 16) <= RESIZE_UVEC(r_dp.wb_cnt_roundtrip, 8);  -- c_wb_cnt_w <= 8
    i_out_status(27 downto 24) <= TO_UVEC(r_dp.dp_phs_clk_select, 4);  -- g_nof_dp_phs_clk <= 6, fits in 4 bit
    i_out_status(31 downto 28) <= r_dp.dp_raw_phs;  -- g_wb_factor = 4
  end process;

  out_status <= i_out_status;

  sim_dbg_status : if g_sim = true generate
    dbg_status_0_out_word_locked       <= r_dp.dp_word_locked;
    dbg_status_1_out_word_stable       <= i_out_word_stable;
    dbg_status_3_dp_fifo_fill_stable   <= i_out_status(3);
    dbg_status_4_dp_phs_stable         <= i_out_status(4);
    dbg_status_5_wb_roundtrip_stable   <= i_out_status(5);
    dbg_status_6_dp_in_clk_detected    <= i_out_status(6);
    dbg_status_7_dp_in_clk_stable      <= i_out_status(7);
    dbg_status_15_8_fifo_rdusedw       <= i_out_status(15 downto 8);
    dbg_status_23_16_wb_cnt_roundtrip  <= i_out_status(23 downto 16);
    dbg_status_27_24_dp_phs_clk_select <= i_out_status(27 downto 24);
    dbg_status_31_28_dp_raw_phs        <= i_out_status(31 downto 28) when r_dp.dp_val = '1' and r_dp.le_val = '1';  -- latch to view only yhe raw_phs when in lock
  end generate;

  ------------------------------------------------------------------------------
  -- Stable monitors for out_status
  ------------------------------------------------------------------------------

  u_common_stable_monitor_word_lock : entity common_lib.common_stable_monitor
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- MM
    r_in         => r_dp.dp_word_locked,
    r_stable     => i_out_word_stable,  -- monitors lvdsh_dd_phs4 overalll data output stable (both phs and word)
    r_stable_ack => out_word_stable_ack
  );

  u_common_stable_monitor_phs_lock : entity common_lib.common_stable_monitor
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- MM
    r_in         => r_dp.dp_phs_locked,
    r_stable     => dp_phs_stable,  -- monitors lvdsh_dd_phs4_align data output stable
    r_stable_ack => out_word_stable_ack
  );

  u_common_stable_monitor_fifo_fill_lock : entity common_lib.common_stable_monitor
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- MM
    r_in         => dp_fifo_fill_lock,
    r_stable     => dp_fifo_fill_stable,  -- monitors FIFO fill level between min and max
    r_stable_ack => out_word_stable_ack
  );

  u_common_stable_monitor_wb_cnt_roundtrip : entity common_lib.common_stable_monitor
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- MM
    r_in         => wb_roundtrip_lock,
    r_stable     => wb_roundtrip_stable,  -- monitors roundtrip latency of wb_sync --> dp_sync via in_clk domain between +-1
    r_stable_ack => out_word_stable_ack
  );

  out_word_locked <= r_dp.dp_word_locked;
  out_word_stable <= i_out_word_stable;

  ------------------------------------------------------------------------------
  -- Enable alignment or maintain current alignment once out_word_stable is achieved
  ------------------------------------------------------------------------------
  nxt_r_dp.dp_maintain_phs <= i_out_word_stable when g_maintain_phs = true else '0';

  u_common_async_in_maintain_phs : entity common_lib.common_async
  generic map (
    g_rst_level => '1',
    g_delay_len => c_delay_len
  )
  port map (
    rst  => '0',
    clk  => in_clk,
    din  => r_dp.dp_maintain_phs,
    dout => in_maintain_phs
  );

  ------------------------------------------------------------------------------
  -- Registers
  ------------------------------------------------------------------------------

  p_in_reg : process(in_rst, in_clk)
  begin
    if in_rst = '1' then
      r_in.dd_sync_locked <= '0';
      r_in.dd_in_val      <= '0';
      r_in.fifo_wr_req    <= '0';
      r_in.fifo_wr_req_p  <= '0';
    elsif rising_edge(in_clk) then
      r_in <= nxt_r_in;
    end if;
  end process;

  p_dp_reg : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      r_dp.wb_sync_cap         <= '0';
      r_dp.wb_sync             <= '0';
      r_dp.dp_in_rst_req       <= '1';
      r_dp.fifo_rd_req         <= '0';
      r_dp.le_sync             <= '0';
      r_dp.le_raw_phs          <= (others => '0');
      r_dp.le_val              <= '0';
      r_dp.dp_raw_phs          <= (others => '0');
      r_dp.dp_dat              <= (others => '0');  -- to avoid Warning: NUMERIC_STD.TO_INTEGER: metavalue detected, returning 0 in simulation
      r_dp.dp_val              <= '0';
      r_dp.dp_phs_locked       <= '0';
      r_dp.dp_phs_align_en     <= '0';
      r_dp.dp_phs_clk_en_vec   <= (others => '1');
      r_dp.dp_phs_clk_select   <= 0;
      r_dp.dp_sync_cap         <= '0';
      r_dp.prev_dp_sync_cap    <= '0';
      r_dp.dp_sync             <= '0';
      r_dp.wb_cnt_clr          <= '1';
      r_dp.wb_cnt_roundtrip    <= (others => '1');
      r_dp.dp_word_req         <= '1';
      r_dp.dp_word_req_dly     <= (others => '1');
      r_dp.dp_word_locked      <= '0';
      r_dp.dp_word_lock_failed <= '0';
      r_dp.dp_maintain_phs     <= '0';
    elsif rising_edge(dp_clk) then
      r_dp <= nxt_r_dp;
    end if;
  end process;
end str;
