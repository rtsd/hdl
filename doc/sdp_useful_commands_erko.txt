cd git
Statistics offload indices bug:

1) Summary

- XST show duplicate subband index for LiLi cell when nof_crosslets > 1. Goes always wrong.
- SST show duplicate signal input indices that correspond to missing indices that can be
  somwhat lower or even higher. GFoes wrong for about 1.5 % of the SST packets, more often
  on higher nodes but seen all all nodes 2 - 15, not yet seen on node 0, 1.

2) DTS-outside

http://dop496.nfra.nl:8888/notebooks/Demo_and_test_scripts/PPK/XSTcaptureRaw.ipynb

xst_rx_align_stream_enable = [1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0] --> should be 2**P_sq - 1 = 3

- step = 0
- nofXST = 1 --> ok
- nofXST > 1 --> first cross-correlation on each node has wrong subband index
- nofXST = 2
0 subband: 78 baseline (0, 0)
1 subband: 78 baseline (0, 0)
2 subband: 71 baseline (0, 36) zero
3 subband: 78 baseline (0, 36) zero
4 subband: 71 baseline (0, 24) zero
5 subband: 78 baseline (0, 24) zero
6 subband: 78 baseline (12, 12)
7 subband: 78 baseline (12, 12)
8 subband: 71 baseline (12, 0) zero
9 subband: 78 baseline (12, 0) zero
10 subband: 71 baseline (12, 36) zero
11 subband: 78 baseline (12, 36) zero
12 subband: 78 baseline (24, 24)
13 subband: 78 baseline (24, 24)
14 subband: 71 baseline (24, 12) zero
15 subband: 78 baseline (24, 12) zero
16 subband: 71 baseline (24, 0) zero
17 subband: 78 baseline (24, 0) zero
18 subband: 78 baseline (36, 36) zero
19 subband: 78 baseline (36, 36) zero
20 subband: 71 baseline (36, 24) zero
21 subband: 78 baseline (36, 24) zero
22 subband: 71 baseline (36, 12) zero
23 subband: 78 baseline (36, 12) zero

24 subband: 78 baseline (0, 0)
25 subband: 78 baseline (0, 0)
26 subband: 71 baseline (0, 36) zero
27 subband: 78 baseline (0, 36) zero
28 subband: 71 baseline (0, 24) zero
29 subband: 78 baseline (0, 24) zero
30 subband: 78 baseline (12, 12)
31 subband: 78 baseline (12, 12)
32 subband: 71 baseline (12, 0) zero
33 subband: 78 baseline (12, 0) zero
34 subband: 71 baseline (12, 36) zero
35 subband: 78 baseline (12, 36) zero
36 subband: 78 baseline (24, 24)
37 subband: 78 baseline (24, 24)
38 subband: 71 baseline (24, 12) zero
39 subband: 78 baseline (24, 12) zero
40 subband: 71 baseline (24, 0) zero
41 subband: 78 baseline (24, 0) zero
42 subband: 78 baseline (36, 36) zero
43 subband: 78 baseline (36, 36) zero
44 subband: 71 baseline (36, 24) zero
45 subband: 78 baseline (36, 24) zero
46 subband: 71 baseline (36, 12) zero
47 subband: 78 baseline (36, 12) zero

48 subband: 78 baseline (0, 0)
49 subband: 78 baseline (0, 0)
50 subband: 71 baseline (0, 36) zero
51 subband: 78 baseline (0, 36) zero
52 subband: 71 baseline (0, 24) zero
53 subband: 78 baseline (0, 24) zero
54 subband: 78 baseline (12, 12)
55 subband: 78 baseline (12, 12)
56 subband: 71 baseline (12, 0) zero
57 subband: 78 baseline (12, 0) zero
58 subband: 71 baseline (12, 36) zero
59 subband: 78 baseline (12, 36) zero
60 subband: 78 baseline (24, 24)
61 subband: 78 baseline (24, 24)
62 subband: 71 baseline (24, 12) zero
63 subband: 78 baseline (24, 12) zero
64 subband: 71 baseline (24, 0) zero
65 subband: 78 baseline (24, 0) zero
66 subband: 78 baseline (36, 36) zero
67 subband: 78 baseline (36, 36) zero
68 subband: 71 baseline (36, 24) zero
69 subband: 78 baseline (36, 24) zero
70 subband: 71 baseline (36, 12) zero
------------78 baseline (36, 12) zero is missing

71 subband: 78 baseline (0, 0)
72 subband: 78 baseline (0, 0)
73 subband: 71 baseline (0, 36) zero
74 subband: 78 baseline (0, 36) zero
75 subband: 71 baseline (0, 24) zero
76 subband: 78 baseline (0, 24) zero
77 subband: 78 baseline (12, 12)
78 subband: 78 baseline (12, 12)
79 subband: 71 baseline (12, 0) zero
80 subband: 78 baseline (12, 0) zero
81 subband: 71 baseline (12, 36) zero
82 subband: 78 baseline (12, 36) zero
83 subband: 78 baseline (24, 24)
84 subband: 78 baseline (24, 24)
85 subband: 71 baseline (24, 12) zero
86 subband: 78 baseline (24, 12) zero
87 subband: 71 baseline (24, 0) zero
88 subband: 78 baseline (24, 0) zero
89 subband: 78 baseline (36, 36) zero
90 subband: 78 baseline (36, 36) zero
91 subband: 71 baseline (36, 24) zero
92 subband: 78 baseline (36, 24) zero
93 subband: 71 baseline (36, 12) zero
94 subband: 78 baseline (36, 12) zero

95 subband: 78 baseline (0, 0)
96 subband: 78 baseline (0, 0)
97 subband: 71 baseline (0, 36) zero
98 subband: 78 baseline (0, 36) zero
99 subband: 71 baseline (0, 24) zero


3) RW

L2SDP-700 : gunzip ./temp/tcpdump.txt.gz
header:
    marker, version 5805
    subband c4 - ca = 196 - 202
    signal input A = 0
    signal input B = 0, 54, 48, 3c, 30, 24, 18,  c
                   = 0, 84, 72, 60, 48, 36, 24, 12
    block period 1400 = 5120
    integration interval 2faf0 = 195312
    nof_signal_inputs, nof_statistics_per_packet 0c08

> cat temp/tcpdump.txt |grep 0x0030 |more --> c4 is correct, not reported as c5


4) SDP-ARTS

Signal input indices:

  dec: 0, 12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144, 156, 168, 180
  hex: 0,  c, 18, 24, 30, 3c, 48, 54, 60,  6c,  78,  84,  90,  9c,  a8,  b4


> stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup --mtime 5 --test-header
> stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup --mtime 5 --plot
> stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup --mtime 5 --headers > x.txt

> stat_stream_xst.py --host 10.99.0.250 --port 4842 -h
> stat_stream_xst.py --host 10.99.0.250 --port 4842 --headers
> stat_stream_xst.py --host 10.99.0.250 --port 4842 --headers --stream ON
> stat_stream_xst.py --host 10.99.0.250 --port 4842 --setup --ip dop386 --mac dop386

> vi test/py/base/statistics_stream_packet.py

> sdp_rw.py --host 10.99.0.250 --port 4842 -l
> sdp_rw.py --host 10.99.0.250 --port 4842 -l pps
> sdp_rw.py --host 10.99.0.250 --port 4842 -r pps_capture_cnt

> sdp_rw.py --host 10.99.0.250 --port 4842 -r sdp_config_first_fpga_nr
> sdp_rw.py --host 10.99.0.250 --port 4842 -r sdp_config_nof_fpgas

> sdp_rw.py --host 10.99.0.250 --port 4842 -w fpga_mask [True]*16
> sdp_rw.py --host 10.99.0.250 --port 4842 -r fpga_mask
> sdp_rw.py --host 10.99.0.250 --port 4842 -r global_node_index

> sdp_rw.py --host 10.99.0.250 --port 4842 -r scrap
> sdp_rw.py --host 10.99.0.250 --port 4842 -w scrap [1]*16
> sdp_rw.py --host 10.99.0.250 --port 4842 -w scrap [1]*8192
> sdp_rw.py --host 10.99.0.250 --port 4842 -r scrap

> sdp_rw.py --host 10.99.0.250 --port 4842 -w processing_enable [False]*16
> sdp_rw.py --host 10.99.0.250 --port 4842 -r processing_enable

> sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_hdr_eth_destination_mac
> sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_hdr_ip_destination_address
> sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_integration_interval
> sdp_rw.py --host 10.99.0.250 --port 4842 -r signal_input_bsn

> sudo tcpdump -vvXXSnelfi enp5s0 port 5001 > tcpdump.txt ==> SST has duplicates ~2% : e.g. in 5 sec:
       130 is duplicate, 128 is missing --> delta = -2
       130 is duplicate, 122 is missing --> delta = -8
       125 is duplicate, 126 is missing --> delta = +1
        93 is duplicate,  92 is missing --> delta = -1
       166 is duplicate, 164 is missing --> delta = -2
        79 is duplicate,  75 is missing --> delta = -4
        83 is duplicate,  80 is missing --> delta = -3
       165 is duplicate, 163 is missing --> delta = -2
       175 is duplicate, 172 is missing --> delta = -3
       179 is duplicate, 176 is missing --> delta = -3
        95 is duplicate,  94 is missing --> delta = -1


5) SDP-ARTS XST indices

sdp_rw.py --host 10.99.0.250 --port 4842 -r firmware_version

    https://git.astron.nl/rtsd/hdl/-/merge_requests/241 met statistics offload fix was op 15 april 2022
    sdp-arts: 2022-04-13T08.41.35_209979741_lofar2_unb2b_sdp_station_full_wg
    dts-outside: 2022-04-12T10.56.45_b8464ee23_lofar2_unb2c_sdp_station_full
    dts-lcu: 2022-04-29T10.19.39_2c3958e1f_lofar2_unb2c_sdp_station_full

sdp_rw.py --host 10.99.0.250 --port 4842 -l xst

sdp_rw.py --host 10.99.0.250 --port 4840 -w fpga_mask [True]*16
sdp_rw.py --host 10.99.0.250 --port 4840 -r fpga_mask

sdp_rw.py --host 10.99.0.250 --port 4842 -r firmware_version
sdp_rw.py --host 10.99.0.250 --port 4842 -r sdp_config_first_fpga_nr  # sdp-arts = 64
sdp_rw.py --host 10.99.0.250 --port 4842 -r sdp_config_nof_fpgas  # sdp-arts = 16

stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup --mtime 1

SDP processing:
> sdp_rw.py --host 10.99.0.250 --port 4842 -r processing_enable

XST ring GN 0-15:
sdp_rw.py --host 10.99.0.250 --port 4842 -w ring_node_offset [0]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_node_offset
sdp_rw.py --host 10.99.0.250 --port 4842 -w ring_nof_nodes [16]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_nof_nodes

sdp_rw.py --host 10.99.0.250 --port 4842 -w ring_use_cable_to_next_rn [False,False,False,True,False,False,False,True,False,False,False,True,False,False,False,True]
sdp_rw.py --host 10.99.0.250 --port 4842 -w ring_use_cable_to_previous_rn [True,False,False,False,True,False,False,False,True,False,False,False,True,False,False,False]
sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_use_cable_to_next_rn
sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_use_cable_to_previous_rn

sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_ring_nof_transport_hops [8]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_ring_nof_transport_hops

XST setup:
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_hdr_eth_destination_mac  # dop386 = 00:15:17:98:5f:bf
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_hdr_ip_destination_address  # dop386 = 10.99.0.253
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_hdr_udp_destination_port  # 5003

sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_subband_select [0,10,11,12,13,14,15,16]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_subband_select  # updated after xst_processing_enable

sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_integration_interval [1.0]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_integration_interval  # updated after xst_processing_enable

sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_offload_nof_crosslets [2]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_nof_crosslets  # updated immediately

sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_processing_enable [False]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_processing_enable [True]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_processing_enable

sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_subband_select  # updated after xst_processing_enable
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_integration_interval  # updated after xst_processing_enable
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_nof_crosslets  # updated immediately

XST offload:
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_enable
sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_offload_enable [True]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_enable

sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_offload_enable [False]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_enable

sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_nof_packets

sudo tcpdump -vvXXSnelfi enp5s0 port 5003 > tcpdump.txt
cat tcpdump.txt |grep 0x0030 |more   # ==> select T_int block and then find next T_int blocks


6) DTS-LAB XST indices

sdp_rw.py --host 10.99.0.250 --port 4840 -l xst

sdp_rw.py --host 10.99.0.250 --port 4840 -w fpga_mask [False,True,True,True,False,False,False,False,False,False,False,False,False,False,False,False]
sdp_rw.py --host 10.99.0.250 --port 4840 -r fpga_mask

sdp_rw.py --host 10.99.0.250 --port 4840 -r firmware_version
sdp_rw.py --host 10.99.0.250 --port 4840 -r sdp_config_first_fpga_nr  # dts-lab = 0
sdp_rw.py --host 10.99.0.250 --port 4840 -r sdp_config_nof_fpgas  # dts-lab = 16

stat_stream_xst.py --host 10.99.0.250 --port 4840 --ip dop421 --mac dop421 --setup --mtime 1

SDP processing GN 1-3:
sdp_rw.py --host 10.99.0.250 --port 4840 -w processing_enable [False,True,True,True,False,False,False,False,False,False,False,False,False,False,False,False]
sdp_rw.py --host 10.99.0.250 --port 4840 -r processing_enable

Ring GN 1-3:
sdp_rw.py --host 10.99.0.250 --port 4842 -w ring_node_offset [1]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_node_offset
sdp_rw.py --host 10.99.0.250 --port 4842 -w ring_nof_nodes [3]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_nof_nodes

sdp_rw.py --host 10.99.0.250 --port 4840 -w ring_use_cable_to_next_rn [False,False,False,True,False,False,False,False,False,False,False,False,False,False,False,False]
sdp_rw.py --host 10.99.0.250 --port 4840 -w ring_use_cable_to_previous_rn [False,True,False,False,False,False,False,False,False,False,False,False,False,False,False,False]
sdp_rw.py --host 10.99.0.250 --port 4840 -r ring_use_cable_to_next_rn
sdp_rw.py --host 10.99.0.250 --port 4840 -r ring_use_cable_to_previous_rn


XST setup:
sdp_rw.py --host 10.99.0.250 --port 4840 -w xst_ring_nof_transport_hops [2]*16  # N_rn = 3 --> P_sq = N_rn // 2 + 1 = 2
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_ring_nof_transport_hops

sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_rx_align_stream_enable

sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_hdr_eth_destination_mac  # dop421 = 00:15:17:aa:22:9c
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_hdr_ip_destination_address  # dop421 = 10.99.0.254
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_hdr_udp_destination_port  # 5003

sdp_rw.py --host 10.99.0.250 --port 4840 -w xst_subband_select [0,10,11,12,13,14,15,16]*16
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_subband_select  # updated after xst_processing_enable

sdp_rw.py --host 10.99.0.250 --port 4840 -w xst_integration_interval [1.0]*16
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_integration_interval  # updated after xst_processing_enable

sdp_rw.py --host 10.99.0.250 --port 4840 -w xst_offload_nof_crosslets [2]*16
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_nof_crosslets  # updated immediately

sdp_rw.py --host 10.99.0.250 --port 4840 -w xst_processing_enable [False]*16
sdp_rw.py --host 10.99.0.250 --port 4840 -w xst_processing_enable [True]*16
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_processing_enable

sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_subband_select  # updated after xst_processing_enable
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_integration_interval  # updated after xst_processing_enable
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_nof_crosslets  # updated immediately

XST monitor:
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_ring_rx_total_nof_sync_discarded
sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_ring_rx_total_nof_sync_received

XST offload:
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_enable
sdp_rw.py --host 10.99.0.250 --port 4840 -w xst_offload_enable [True]*16
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_enable

sdp_rw.py --host 10.99.0.250 --port 4840 -w xst_offload_enable [False]*16
sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_enable

for i in {1..5}
do
  sdp_rw.py --host 10.99.0.250 --port 4840 -r xst_offload_nof_packets
  sleep 1
done

sudo tcpdump -vvXXSnelfi enp67s0f1 port 5003 > tcpdump.txt
cat tcpdump.txt |grep 0x0030 |more   # ==> select T_int block and then find next T_int blocks


6) SST indices

a)
example on PN1:

=== ERROR ===   bsn 322519378320312,  19 duplicate,  17 missing --> delta = -2
=== ERROR ===   bsn 322519378320312,  23 duplicate,  20 missing --> delta = -3
=== ERROR ===   bsn 322519378320312,  95 duplicate,  93 missing --> delta = -3
=== ERROR ===   bsn 322519378320312, 148 duplicate, 147 missing --> delta = -1
=== ERROR ===   bsn 322519378320312, 151 duplicate, 149 missing --> delta = -2
=== ERROR ===   bsn 322519378320312, 155 duplicate, 154 missing --> delta = -1

from:

kooistra@dop386:~/git$ stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup --mtime 120 --test-header
start
start udp server
udp server opened
=== ERROR ===   bsn 322519374804687, index 119 is duplicate
=== ERROR ===   bsn 322519374804687, index 191 is duplicate
=== ERROR ===   bsn 322519375000000, index 45 is duplicate
=== ERROR ===   bsn 322519375000000, index 47 is duplicate
=== ERROR ===   bsn 322519375000000, index 94 is duplicate
=== ERROR ===   bsn 322519375000000, index 136 is duplicate
...
=== ERROR ===   bsn 322519397070312, index 163 is duplicate
=== ERROR ===   bsn 322519397070312, index 167 is duplicate
=== ERROR ===   bsn 322519397265625, index 71 is duplicate
=== ERROR ===   bsn 322519397265625, index 150 is duplicate
=== ERROR ===   bsn 322519397265625, index 154 is duplicate
=== ERROR ===   bsn 322519397460937, index 107 is duplicate
=== ERROR ===   bsn 322519397656250, index 103 is duplicate
=== ERROR ===   bsn 322519397656250, index 107 is duplicate
socket.timeout
valid packets=22688
SUCCES

received bsn numbers [322519374609375, 322519374804687, 322519375000000, 322519375195312, 322519375390625, 322519375585937, 322519375781250, 322519375976562, 322519376171875, 322519376367187, 322519376562500, 322519376757812, 322519376953125, 322519377148437, 322519377343750, 322519377539062, 322519377734375, 322519377929687, 322519378125000, 322519378320312, 322519378515625, 322519378710937, 322519378906250, 322519379101562, 322519379296875, 322519379492187, 322519379687500, 322519379882812, 322519380078125, 322519380273437, 322519380468750, 322519380664062, 322519380859375, 322519381054687, 322519381250000, 322519381445312, 322519381640625, 322519381835937, 322519382031250, 322519382226562, 322519382421875, 322519382617187, 322519382812500, 322519383007812, 322519383203125, 322519383398437, 322519383593750, 322519383789062, 322519383984375, 322519384179687, 322519384375000, 322519384570312, 322519384765625, 322519384960937, 322519385156250, 322519385351562, 322519385546875, 322519385742187, 322519385937500, 322519386132812, 322519386328125, 322519386523437, 322519386718750, 322519386914062, 322519387109375, 322519387304687, 322519387500000, 322519387695312, 322519387890625, 322519388085937, 322519388281250, 322519388476562, 322519388671875, 322519388867187, 322519389062500, 322519389257812, 322519389453125, 322519389648437, 322519389843750, 322519390039062, 322519390234375, 322519390429687, 322519390625000, 322519390820312, 322519391015625, 322519391210937, 322519391406250, 322519391601562, 322519391796875, 322519391992187, 322519392187500, 322519392382812, 322519392578125, 322519392773437, 322519392968750, 322519393164062, 322519393359375, 322519393554687, 322519393750000, 322519393945312, 322519394140625, 322519394335937, 322519394531250, 322519394726562, 322519394921875, 322519395117187, 322519395312500, 322519395507812, 322519395703125, 322519395898437, 322519396093750, 322519396289062, 322519396484375, 322519396679687, 322519396875000, 322519397070312, 322519397265625, 322519397460937, 322519397656250, 322519397851562]
- signal_input_index 117 not in bsn 322519374804687
- signal_input_index 189 not in bsn 322519374804687
- signal_input_index 40 not in bsn 322519375000000
- signal_input_index 46 not in bsn 322519375000000
- signal_input_index 92 not in bsn 322519375000000
- signal_input_index 135 not in bsn 322519375000000
- signal_input_index 137 not in bsn 322519375000000
- signal_input_index 142 not in bsn 322519375000000
- signal_input_index 146 not in bsn 322519375000000
- signal_input_index 151 not in bsn 322519375000000
- signal_input_index 154 not in bsn 322519375000000
- signal_input_index 137 not in bsn 322519375195312
- signal_input_index 142 not in bsn 322519375195312
- signal_input_index 154 not in bsn 322519375390625
- signal_input_index 162 not in bsn 322519375390625
- signal_input_index 165 not in bsn 322519375390625
- signal_input_index 170 not in bsn 322519375390625
- signal_input_index 173 not in bsn 322519375390625
- signal_input_index 178 not in bsn 322519375390625
- signal_input_index 116 not in bsn 322519375585937
- signal_input_index 118 not in bsn 322519375781250
- signal_input_index 126 not in bsn 322519375781250
- signal_input_index 130 not in bsn 322519375781250
- signal_input_index 154 not in bsn 322519375781250
- signal_input_index 102 not in bsn 322519375976562
...
- signal_input_index 106 not in bsn 322519397460937
- signal_input_index 100 not in bsn 322519397656250
- signal_input_index 104 not in bsn 322519397656250

stream data status:
- received 23040 packets ==> = 120 * 192 is OK
- n_valid 22688 packets
- n_duplicate 352 packets ==> 352 / 23040 = 1.5 %

7) Reboot, flash

# Alle fpgas schrijven duurt 2m28s:
> sdp_firmware.py --host 10.99.0.250 --write --image USER --file /home/donker/images/lofar2_unb2c_sdp_station_full-r70484fd08.rb

# Alle fpgas schrijven terug lezen en checken duurt 6m15s
sdp_firmware.py --host 10.99.0.250 --write --read --verify --image USER --file /home/donker/images/lofar2_unb2c_sdp_station_full-r70484fd08.rb

# Wil je ook een reboot doen, dan ook nog --reboot toevoegen.

[0] = factory
[1] = user image
sdp_rw.py --host 10.99.0.250 --port 4842 -w boot_image [1]*16

__pycache__ dir met gecompileerde pyc deleten

# login on regtest@dop349
mystep
regtest
ll ~/bitstream/lofar2_unb2b_sdp_station_full_wg-r*

# flash image
scp -p regtest@10.87.6.144:~/bitstream/lofar2*wg* /home/kooistra/Downloads/      # dop349 = 10.87.6.144

sdp_firmware.py --host 10.99.0.250 --port 4842 -h
sdp_rw.py --host 10.99.0.250 --port 4842 -r firmware_version
sdp_firmware.py --host 10.99.0.250 --port 4842 --version
sdp_firmware.py --host 10.99.0.250 --port 4842 --image FACT --reboot
sdp_firmware.py --host 10.99.0.250 --port 4842 --version
sdp_firmware.py --host 10.99.0.250 --port 4842 -n 64:79 --image USER --file ~/Downloads/lofar2_unb2b_sdp_station_full_wg-rce96e0f1d.rbf --write
sdp_firmware.py --host 10.99.0.250 --port 4842 --image USER --reboot
sdp_firmware.py --host 10.99.0.250 --port 4842 --version



8) PD 26 aug 2022
Op een vers syteem gaat het goed,  maar na een aantal keer testen gaan xst_ring_tx_nof_packets en xst_ring_rx_nof_packets  rare waardes geven.

sdp_rw.py --host 10.99.0.250 --port 4842 -w boot_image [1]*16
stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-cm --test-header
test-cp: PASSED
test-mp: PASSED
test-header: PASSED

verder is alles wat --plot en --headers nodig heeft standaard ingebouwd, onderstaande werkt dus zonder dat eerst wat anders moet worden gezet. plot kan worden beeindigd door in de terminal op Enter te drukken.

stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --plots

stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --headers





9) BF
sdp_rw.py --host 10.99.0.250 --port 4842 -w bf_ring_nof_transport_hops [1]*16
sdp_rw.py --host 10.99.0.250 --port 4842 -r bf_ring_nof_transport_hops

sdp_rw.py --host 10.99.0.250 --port 4842 -r bst_offload_enable

sdp_rw.py --host 10.99.0.250 --port 4842 -r bf_ring_rx_total_nof_sync_discarded
sdp_rw.py --host 10.99.0.250 --port 4842 -r bf_ring_rx_total_nof_sync_received

SST tests with: stat_stream_sst

# --bsn-monitor of SST offload
stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream ON --bsn-monitor --mtime 3 -vv
stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream OFF --bsn-monitor --mtime 3 -vv

# --headers --> print headers during mtime
stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --headers --mtime 3

# --test-header --> valid packets = mtime * 192 packets
stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-header --mtime 3

# --test-data --> verify expected SST for ampl in [1.0, 0.1, 0.01, 0.001]
stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-data

# --plots
# use ctrl-C in terminal to stop plots
stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --plots
# use wg.py in other terminal to control WG
wg.py  --host 10.99.0.250 --port 4842 --setphase 0 --setfreq 19921875 --setampl 0.5 --enable
wg.py  --host 10.99.0.250 --port 4842 --disable


10) DTS-lab (feb 2023)

kooistra@dop421:~/git/sdptr$ sdp_rw.py --host 10.99.0.250 -r firmware_version
read firmware_version:
node  0:  2022-11-21T11.15.47_193f87e53_lofar2_unb2c_sdp_station_full
node  1:  2022-11-21T11.15.47_193f87e53_lofar2_unb2c_sdp_station_full
node  2:
node  3:  2022-11-21T11.15.47_193f87e53_lofar2_unb2c_sdp_station_full


11) L2TS sep 2023

# LBA - 4840
# HBA0 - 4842
# HBA1 - 4844
sdp_rw.py --host 10.151.255.1 --port 4842 -r firmware_version
sdp_rw.py --host 10.151.255.1 --port 4842 -r jesd204b_rx_err0
sdp_rw.py --host 10.151.255.1 --port 4842 -r jesd204b_rx_err1
sdp_rw.py --host 10.151.255.1 --port 4842 -r jesd204b_csr_dev_syncn
sdp_rw.py --host 10.151.255.1 --port 4842 -r jesd204b_csr_rbd_count
sdp_rw.py --host 10.151.255.1 --port 4842 -r signal_input_bsn
sdp_rw.py --host 10.151.255.1 --port 4842 -r signal_input_mean
sdp_rw.py --host 10.151.255.1 --port 4842 -r signal_input_std
sdp_rw.py --host 10.151.255.1 --port 4842 -r signal_input_samples_delay
sdp_rw.py --host 10.151.255.1 --port 4842 -r signal_input_data_buffer

sdp_rw.py --host 10.151.255.1 --port 4842 -r xst_integration_interval
sdp_rw.py --host 10.151.255.1 --port 4842 -r xst_ring_rx_bsn
sdp_rw.py --host 10.151.255.1 --port 4842 -r xst_aligned_bsn
sdp_rw.py --host 10.151.255.1 --port 4842 -r xst_ring_rx_latency
sdp_rw.py --host 10.151.255.1 --port 4842 -r xst_ring_rx_total_nof_packets_discarded
sdp_rw.py --host 10.151.255.1 --port 4842 -r xst_ring_rx_total_nof_packets_received
sdp_rw.py --host 10.151.255.1 --port 4842 -r xst_ring_rx_total_nof_sync_discarded
sdp_rw.py --host 10.151.255.1 --port 4842 -r xst_ring_rx_total_nof_sync_received
