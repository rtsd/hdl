library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use common_lib.common_pkg.all;

architecture rtl of pfs_tapsbuf is
type RamType is array(0 to 2**g_addr_w) of std_logic_vector(g_data_w - 1 downto 0);

-- pfs_tapsbuf_1024.hex is empty (all zeros)
signal RAM : RamType := (others => (others => '0'));

signal read_addrb : std_logic_vector(g_addr_w - 1 downto 0);
begin
---------------------------------------------------------------
process (clk)
begin
  if (clk'event and clk = '1') then
    if (wren = '1') then
      RAM (conv_integer(wraddr)) <= wrdata;
    end if;
    read_addrb <= rdaddr;
      rddata <= RAM(conv_integer(read_addrb));
  end if;
end process;

---------------------------------------------------------------
---------------------------------------------------------------
end rtl;
