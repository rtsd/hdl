-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

entity eth_control is
  port (
    -- Clocks and reset
    rst               : in  std_logic;
    clk               : in  std_logic;

    -- Control registers
    reg_config        : in  t_eth_mm_reg_config;
    reg_control       : in  t_eth_mm_reg_control;
    reg_continue_wr   : in  std_logic;  -- used to know that control reg has been written and ETH module can continue from TX_PENDING state
    reg_status        : out t_eth_mm_reg_status;
    reg_status_wr     : in  std_logic;  -- used to know that status reg has been read and can be cleared to remove the sla_interrupt

    -- Streaming sink Rx frame
    -- . The rcv_rd, rcv_ack and rcv_done act instead of rcv_out.ready to have rcv_out ready per frame instead of per data word
    rcv_rd            : out std_logic;
    rcv_ack           : in  std_logic;
    rcv_done          : in  std_logic;
    rcv_in            : in  t_dp_sosi;  -- packet memory is always ready for packet data
    rcv_hdr_words_arr : in  t_network_total_header_32b_arr;
    rcv_hdr_status    : in  t_eth_hdr_status;

    -- Streaming source Tx frame
    xmt_in            : in  t_dp_siso;
    xmt_out           : out t_dp_sosi;

    -- MM frame memory
    mem_in            : out t_mem_mosi;
    mem_out           : in  t_mem_miso
  );
end eth_control;

architecture rtl of eth_control is
  -- Internal source ready latency of this component
  constant c_this_src_latency : natural := 1;  -- xmt_in, xmt_out

  type t_state_enum is (
    s_idle,
    s_rx_request,
    s_rx_frame,
    s_hdr_response,
    s_tx_pending,
    s_tx_frame,
    s_tx_done
  );

  signal nxt_rcv_rd               : std_logic;
  signal rcv_in_eop_dly           : std_logic;

  signal hdr_response_arr         : t_network_total_header_32b_arr;
  signal nxt_hdr_response_arr     : t_network_total_header_32b_arr;
  signal hdr_word_cnt             : natural range 0 to c_network_total_header_32b_nof_words := c_network_total_header_32b_nof_words;
  signal nxt_hdr_word_cnt         : natural;
  signal hdr_en                   : std_logic;
  signal nxt_hdr_en               : std_logic;
  signal hdr_done                 : std_logic;
  signal nxt_hdr_done             : std_logic;

  signal tx_insert                : std_logic;
  signal nxt_tx_insert            : std_logic;

  signal i_reg_status             : t_eth_mm_reg_status;
  signal nxt_reg_status           : t_eth_mm_reg_status;

  signal xmt_start                : std_logic;
  signal nxt_xmt_start            : std_logic;
  signal xmt_en                   : std_logic;
  signal nxt_xmt_en               : std_logic;
  signal xmt_done                 : std_logic;
  signal nxt_xmt_done             : std_logic;
  signal xmt_word_cnt             : natural range 0 to c_eth_frame_nof_words;
  signal nxt_xmt_word_cnt         : natural;
  signal tx_nof_words             : std_logic_vector(c_eth_frame_nof_words_w - 1 downto 0);
  signal nxt_tx_nof_words         : std_logic_vector(c_eth_frame_nof_words_w - 1 downto 0);
  signal tx_empty                 : std_logic_vector(c_eth_empty_w - 1 downto 0);
  signal nxt_tx_empty             : std_logic_vector(c_eth_empty_w - 1 downto 0);

  signal i_mem_in                 : t_mem_mosi;
  signal nxt_mem_in               : t_mem_mosi;
  signal rd_val                   : std_logic_vector(0 to c_mem_ram_rd_latency);  -- use [0] to combinatorially store rd (= rd_en)
  signal nxt_rd_val               : std_logic_vector(1 to c_mem_ram_rd_latency);
  signal rd_sop                   : std_logic_vector(    rd_val'range);
  signal nxt_rd_sop               : std_logic_vector(nxt_rd_val'range);
  signal rd_eop                   : std_logic_vector(    rd_val'range);
  signal nxt_rd_eop               : std_logic_vector(nxt_rd_val'range);

  signal xmt_siso                 : t_dp_siso;
  signal xmt_sosi                 : t_dp_sosi;

  signal i_xmt_out                : t_dp_sosi;

  -- ST->MM state machine
  signal state                    : t_state_enum;
  signal nxt_state                : t_state_enum;
begin
  reg_status <= i_reg_status;
  mem_in     <= i_mem_in;
  xmt_out    <= i_xmt_out;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      rcv_rd                   <= '0';
      rcv_in_eop_dly           <= '0';
      hdr_word_cnt             <= c_network_total_header_32b_nof_words;
      hdr_en                   <= '0';
      hdr_done                 <= '0';
      tx_insert                <= '0';
      xmt_start                <= '0';
      i_reg_status             <= c_eth_mm_reg_status_rst;
      tx_nof_words             <= (others => '0');
      tx_empty                 <= (others => '0');
      xmt_word_cnt             <= 0;
      xmt_en                   <= '0';
      xmt_done                 <= '0';
      i_mem_in                 <= c_mem_mosi_rst;
      rd_val(nxt_rd_val'range) <= (others => '0');
      rd_sop(nxt_rd_val'range) <= (others => '0');
      rd_eop(nxt_rd_val'range) <= (others => '0');
      state                    <= s_idle;
    elsif rising_edge(clk) then
      rcv_rd                   <= nxt_rcv_rd;
      rcv_in_eop_dly           <= rcv_in.eop;
      hdr_response_arr         <= nxt_hdr_response_arr;
      hdr_word_cnt             <= nxt_hdr_word_cnt;
      hdr_en                   <= nxt_hdr_en;
      hdr_done                 <= nxt_hdr_done;
      tx_insert                <= nxt_tx_insert;
      xmt_start                <= nxt_xmt_start;
      i_reg_status             <= nxt_reg_status;
      tx_nof_words             <= nxt_tx_nof_words;
      tx_empty                 <= nxt_tx_empty;
      xmt_word_cnt             <= nxt_xmt_word_cnt;
      xmt_en                   <= nxt_xmt_en;
      xmt_done                 <= nxt_xmt_done;
      i_mem_in                 <= nxt_mem_in;
      rd_val(nxt_rd_val'range) <= nxt_rd_val;
      rd_sop(nxt_rd_val'range) <= nxt_rd_sop;
      rd_eop(nxt_rd_val'range) <= nxt_rd_eop;
      state                    <= nxt_state;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Response header
  ------------------------------------------------------------------------------

  p_hdr_response_arr : process(rcv_hdr_words_arr, rcv_hdr_status, reg_config)
  begin
    nxt_hdr_response_arr   <= func_network_total_header_response_eth(     rcv_hdr_words_arr, reg_config.mac_address);
    if rcv_hdr_status.is_arp = '1' then
      nxt_hdr_response_arr <= func_network_total_header_response_arp( rcv_hdr_words_arr, reg_config.mac_address, reg_config.ip_address);
    elsif rcv_hdr_status.is_icmp = '1' then
      nxt_hdr_response_arr <= func_network_total_header_response_icmp(rcv_hdr_words_arr, reg_config.mac_address);
      -- Calculate icmp checksum = original checksum + 0x0800.
      nxt_hdr_response_arr(9)(c_halfword_w - 1 downto 0) <= TO_UVEC( 2048 + TO_UINT(rcv_hdr_words_arr(9)(c_halfword_w - 1 downto 0)), c_halfword_w);
    elsif rcv_hdr_status.is_udp = '1' then
      nxt_hdr_response_arr <= func_network_total_header_response_udp( rcv_hdr_words_arr, reg_config.mac_address);
    elsif rcv_hdr_status.is_ip = '1' then
      -- If it is another protocol over IP than ICMP or UDP, then prepare only IP response
      nxt_hdr_response_arr <= func_network_total_header_response_ip(  rcv_hdr_words_arr, reg_config.mac_address);
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Frame memory access
  ------------------------------------------------------------------------------

  p_mem_access : process(i_mem_in, rcv_in, hdr_en, hdr_word_cnt, hdr_response_arr, xmt_en, xmt_siso, xmt_word_cnt)
  begin
    nxt_mem_in.address <= i_mem_in.address;
    nxt_mem_in.wrdata  <= i_mem_in.wrdata;
    nxt_mem_in.wr      <= '0';
    nxt_mem_in.rd      <= '0';
    nxt_hdr_word_cnt   <= 0;
    nxt_xmt_word_cnt   <= 0;
    -- Rx frame
    if rcv_in.valid = '1' then
      nxt_mem_in.address <= INCR_UVEC(i_mem_in.address, 1);
      nxt_mem_in.wrdata  <= RESIZE_MEM_DATA(rcv_in.data);
      nxt_mem_in.wr      <= '1';
    end if;
    if rcv_in.sop = '1' then
      nxt_mem_in.address <= TO_MEM_ADDRESS(c_eth_ram_rx_offset);  -- rx buffer starts at address 0
    end if;
    -- Prepare the Tx header
    if hdr_en = '1' then  -- tx buffer starts at address 380
      nxt_mem_in.address <= TO_MEM_ADDRESS(c_eth_ram_tx_offset + hdr_word_cnt);
      nxt_mem_in.wrdata  <= RESIZE_MEM_DATA(hdr_response_arr(hdr_word_cnt));
      nxt_mem_in.wr      <= '1';
      nxt_hdr_word_cnt   <= hdr_word_cnt + 1;
    end if;
    -- Tx frame
    if xmt_en = '1' then
      nxt_mem_in.address <= TO_MEM_ADDRESS(c_eth_ram_tx_offset + xmt_word_cnt);
      nxt_xmt_word_cnt   <= xmt_word_cnt;
      if xmt_siso.ready = '1' then
        nxt_mem_in.rd    <= '1';
        nxt_xmt_word_cnt <= xmt_word_cnt + 1;  -- rd_en side counter
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Tx frame output control
  ------------------------------------------------------------------------------

  rd_val(0) <= i_mem_in.rd;
  rd_sop(0) <= i_mem_in.rd when xmt_word_cnt = 1                      else '0';
  rd_eop(0) <= i_mem_in.rd when xmt_word_cnt = unsigned(tx_nof_words) else '0';

  -- rd_val side after read latency
  nxt_rd_val <= rd_val(0 to rd_val'high - 1);
  nxt_rd_sop <= rd_sop(0 to rd_val'high - 1);
  nxt_rd_eop <= rd_eop(0 to rd_val'high - 1);

  xmt_sosi.data  <= RESIZE_DP_DATA(mem_out.rddata);
  xmt_sosi.valid <= rd_val(rd_val'high);
  xmt_sosi.sop   <= rd_sop(rd_val'high);
  xmt_sosi.eop   <= rd_eop(rd_val'high);
  xmt_sosi.empty <= RESIZE_DP_EMPTY(tx_empty);

  -- adapt mem read latency to xmt ready latency
  u_xmt_out_adapter : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency   => c_mem_ram_rd_latency + 1,  -- = 2 + 1 latency for xmt_siso.ready -> nxt_mem_in.rd
    g_out_latency  => c_this_src_latency  -- = 1
  )
  port map (
    rst     => rst,
    clk     => clk,
    -- ST sink
    snk_out => xmt_siso,
    snk_in  => xmt_sosi,
    -- ST source
    src_in  => xmt_in,
    src_out => i_xmt_out
  );

  nxt_xmt_done <= i_xmt_out.eop;

  ------------------------------------------------------------------------------
  -- Master-slave handshake control via status register
  ------------------------------------------------------------------------------

  -- Gather Rx and Tx status into the status register
  p_reg_status : process(i_reg_status, reg_status_wr, rcv_in, rcv_in_eop_dly, i_mem_in, tx_insert, hdr_done, xmt_done)
  begin
    nxt_reg_status <= i_reg_status;
    -- MM write access to the status address clears the status register, which removes the interrupt
    -- using write instead of read access to remove the interrupt allows for polling reg_status
    if reg_status_wr = '1'    then nxt_reg_status          <= c_eth_mm_reg_status_rst; end if;
    -- Events that set the status register
    if rcv_in.eop = '1'       then nxt_reg_status.rx_empty <= rcv_in.empty(c_eth_empty_w - 1 downto 0); end if;
    if rcv_in_eop_dly = '1'   then
      nxt_reg_status.rx_nof_words                                     <= (others => '0');
      nxt_reg_status.rx_nof_words(c_eth_frame_nof_words_w - 1 downto 0) <= INCR_UVEC(i_mem_in.address(c_eth_frame_nof_words_w - 1 downto 0), 1);
    end if;
    if tx_insert = '1'        then nxt_reg_status.tx_avail <= '1'; end if;
    if hdr_done = '1'         then nxt_reg_status.rx_avail <= '1'; end if;
    if xmt_done = '1'         then nxt_reg_status.tx_done  <= '1'; end if;
  end process;

  -- Capture the control register tx fields for a new transmit
  nxt_tx_nof_words <= reg_control.tx_nof_words(c_eth_frame_nof_words_w - 1 downto 0) when xmt_start = '1' else tx_nof_words;
  nxt_tx_empty     <= reg_control.tx_empty                                         when xmt_start = '1' else tx_empty;

  ------------------------------------------------------------------------------
  -- State machine
  ------------------------------------------------------------------------------

  p_state : process(state, reg_control, reg_continue_wr, rcv_in, rcv_ack, rcv_done, hdr_word_cnt, xmt_siso, xmt_word_cnt, tx_nof_words, xmt_done)
  begin
    nxt_rcv_rd     <= '0';  -- read frame start pulse
    nxt_hdr_en     <= '0';  -- prepare response header enable
    nxt_hdr_done   <= '0';  -- prepare response header done
    nxt_tx_insert  <= '0';  -- accept tx insert request
    nxt_xmt_start  <= '0';  -- write frame start pulse
    nxt_xmt_en     <= '0';  -- write frame enable

    nxt_state <= state;
    case state is
      when s_idle =>
        if reg_control.tx_request = '1' then  -- MM master requests to insert an extra tx frame
          nxt_tx_insert <= '1';
          nxt_state <= s_tx_pending;
        end if;
        if reg_control.rx_en = '1' then  -- MM master enables default Rx-Tx operation so request a new rx frame
          nxt_rcv_rd <= '1';
          nxt_state <= s_rx_request;
        end if;
      when s_rx_request =>
        if rcv_in.sop = '1' then  -- busy receiving a new frame (best check for sop, so no need to check rcv_busy, rcv_err in eth_rx_frame)
          nxt_state <= s_rx_frame;
        elsif rcv_ack = '1' then  -- rcv ack with no rcv sop, so there was no rx frame pending, try request again
          nxt_state <= s_idle;
        end if;
      when s_rx_frame =>
        if rcv_done = '1' then  -- frame received
          nxt_state <= s_hdr_response;
        end if;
      when s_hdr_response =>  -- prepare tx header for rx response
        if hdr_word_cnt < c_network_total_header_32b_nof_words - 1 then
          nxt_hdr_en <= '1';
        else
          nxt_hdr_done <= '1';  -- new frame received and response prepared
          nxt_state <= s_tx_pending;
        end if;
      when s_tx_pending =>  -- wait for MM master
        if reg_continue_wr = '1' then  -- MM master done
          if reg_control.tx_en = '1' then  -- continue with response tx frame
            nxt_xmt_start <= '1';
            nxt_state <= s_tx_frame;
          else  -- do not response tx frame
            nxt_state <= s_idle;
          end if;
        end if;
      when s_tx_frame =>
        if xmt_siso.ready = '0' or xmt_word_cnt < unsigned(tx_nof_words) - 1 then
          nxt_xmt_en <= '1';
        else
          nxt_state <= s_tx_done;  -- frame transmitted
        end if;
      when others =>  -- s_tx_done
        if xmt_done = '1' then
          nxt_state <= s_idle;  -- frame transmitted, to be safe wait for xmt_done
        end if;
    end case;
  end process;
end rtl;
