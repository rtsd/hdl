-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the bf_unit.
--           To be used in conjunction with python testscript: ../python/tc_bf_unit.py
--
-- The testbench can be used in two modes: auto-mode and non-auto-mode.

--
-- Usage in auto-mode
--   > Run python script in separate terminal: "python bf_unit.py "
--
-- Usage in non-auto-mode
--   > as 5
--   > run 500 ns
--   > run python script in separate terminal: "python tc_bf_unit.py --unb 0 --fn 0 --sim"
--   > run 20 us or run -all
--   > Check the results of the python script.
--   > Stop the simulation manually in Modelsim by pressing the stop-button.
--   > Evalute the WAVE window.

library IEEE, common_lib, mm_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use work.bf_pkg.all;

entity tb_bf_unit is
  generic(
    g_nof_signal_paths      : positive :=  64;  -- = 64
    g_nof_input_streams     : positive :=  16;  -- = 16
    g_nof_subbands          : positive :=  24;  -- = 24
    g_nof_weights           : positive := 256;  -- = 256
    g_nof_bf_units          : positive :=   4;  -- = 4
    g_in_dat_w              : positive :=  16;  -- = 16
    g_in_weight_w           : positive :=  16;  -- = 16
    g_bst_gain_w            : integer  :=   1;  -- = 1
    g_bst_dat_w             : positive :=  16;  -- = 16
    g_out_gain_w            : integer  :=  -5;  -- = -5
    g_out_dat_w             : positive :=   8;  -- = 8
    g_stat_data_w           : positive :=  56;  -- = 56
    g_stat_data_sz          : positive :=   2;  -- = 2
    g_bf_weights_file_name  : string   := "data/weights";  -- "../../../src/hex/weights";   -- "UNUSED" or relative path to e.g. the bf/build/data/weights hex file for adr_w=8 and dat_w=32
    g_ss_wide_file_prefix   : string   := "data/ss_wide"  -- "../../../src/hex/ss_wide"       -- path_to_file
  );
end tb_bf_unit;

architecture tb of tb_bf_unit is
  constant c_sim                : boolean := true;

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period      : time := 200 ps;
  constant c_dp_clk_period      : time := 5 ns;
  constant c_sclk_period        : time := 1250 ps;
  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  signal SCLK                   : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  signal reg_diag_bg_mosi          : t_mem_mosi;
  signal reg_diag_bg_miso          : t_mem_miso;

  signal ram_diag_bg_mosi          : t_mem_mosi;
  signal ram_diag_bg_miso          : t_mem_miso;

  signal ram_diag_data_buf_re_mosi : t_mem_mosi;
  signal ram_diag_data_buf_re_miso : t_mem_miso;

  signal reg_diag_data_buf_re_mosi : t_mem_mosi;
  signal reg_diag_data_buf_re_miso : t_mem_miso;

  signal ram_diag_data_buf_im_mosi : t_mem_mosi;
  signal ram_diag_data_buf_im_miso : t_mem_miso;

  signal reg_diag_data_buf_im_mosi : t_mem_mosi;
  signal reg_diag_data_buf_im_miso : t_mem_miso;

  -- DUT
  signal ram_ss_ss_wide_mosi       : t_mem_mosi := c_mem_mosi_rst;
  signal ram_ss_ss_wide_miso       : t_mem_miso := c_mem_miso_rst;

  signal ram_bf_weights_mosi       : t_mem_mosi := c_mem_mosi_rst;
  signal ram_bf_weights_miso       : t_mem_miso := c_mem_miso_rst;

  signal ram_st_sst_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_sst_miso           : t_mem_miso := c_mem_miso_rst;

  signal reg_st_sst_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal reg_st_sst_miso           : t_mem_miso := c_mem_miso_rst;

  -- Compose the record with generics for the bf_units.
  constant c_bf                     : t_c_bf  := (g_nof_signal_paths, g_nof_input_streams, g_nof_subbands,
                                                  g_nof_weights, g_nof_bf_units, g_in_dat_w, g_in_weight_w, g_bst_gain_w,
                                                  g_bst_dat_w, g_out_gain_w, g_out_dat_w, g_stat_data_w, g_stat_data_sz);

  -- Custom definitions of constants
  constant c_bg_block_len           : natural  := c_bf.nof_subbands * c_bf.nof_signal_paths / c_bf.nof_input_streams;
  constant c_db_block_len           : natural  := c_bf.nof_weights;

  -- Configuration of the block generator:
  constant c_bg_nof_output_streams  : positive := c_bf.nof_input_streams;
  constant c_bg_buf_dat_w           : positive := c_nof_complex * c_bf.in_dat_w;
  constant c_bg_buf_adr_w           : positive := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_prefix    : string   := "data/tb_bg_dat";  -- "../../../src/hex/tb_bg_dat";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, c_bf.nof_input_streams, 1);

  -- Configuration of the databuffers:
  constant c_db_nof_streams         : positive := 1;
  constant c_db_data_w              : positive := c_diag_db_max_data_w;
  constant c_db_buf_nof_data        : positive := c_db_block_len;
  constant c_db_buf_use_sync        : boolean  := false;
  constant c_db_data_type_re        : t_diag_data_type_enum := e_real;
  constant c_db_data_type_im        : t_diag_data_type_enum := e_imag;

  signal bg_siso_arr                : t_dp_siso_arr(c_bf.nof_input_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(c_bf.nof_input_streams - 1 downto 0);

  signal out_sosi_arr               : t_dp_sosi_arr(1 - 1 downto 0) := (others => c_dp_sosi_rst);
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  SCLK   <= not SCLK after c_sclk_period / 2;
  dp_clk <= not dp_clk after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  u_mm_file_reg_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_BG")
                                           port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

  u_mm_file_ram_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_BG")
                                           port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

  u_mm_file_ram_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_re_mosi, ram_diag_data_buf_re_miso);

  u_mm_file_reg_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_re_mosi, reg_diag_data_buf_re_miso);

  u_mm_file_ram_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_im_mosi, ram_diag_data_buf_im_miso);

  u_mm_file_reg_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_im_mosi, reg_diag_data_buf_im_miso);

  -- DUT
  u_mm_file_ram_ss_ss_wide       : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_SS_SS_WIDE")
                                           port map(mm_rst, mm_clk, ram_ss_ss_wide_mosi, ram_ss_ss_wide_miso);

  u_mm_file_ram_bf_weights       : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_BF_WEIGHTS")
                                           port map(mm_rst, mm_clk, ram_bf_weights_mosi, ram_bf_weights_miso);

  u_mm_file_ram_st_sst           : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_ST_SST")
                                           port map(mm_rst, mm_clk, ram_st_sst_mosi, ram_st_sst_miso);

  u_mm_file_reg_st_sst           : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_ST_SST")
                                           port map(mm_rst, mm_clk, reg_st_sst_mosi, reg_st_sst_miso);

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_bg_nof_output_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr         => bg_siso_arr,
    out_sosi_arr         => bg_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut : entity work.bf_unit
  generic map (
    g_bf                    => c_bf,
    g_bf_weights_file_name  => g_bf_weights_file_name,
    g_ss_wide_file_prefix   => g_ss_wide_file_prefix,
    g_weights_write_only    => false
    --g_weights_write_only    => TRUE
  )
  port map (
    -- System
    dp_rst                 =>  dp_rst,
    dp_clk                 =>  dp_clk,
    mm_rst                 =>  mm_rst,
    mm_clk                 =>  mm_clk,

    -- MM interface
    ram_ss_ss_wide_mosi     => ram_ss_ss_wide_mosi,
    ram_ss_ss_wide_miso     => ram_ss_ss_wide_miso,
    ram_bf_weights_mosi     => ram_bf_weights_mosi,
    ram_bf_weights_miso     => ram_bf_weights_miso,
    ram_st_sst_mosi         => ram_st_sst_mosi,
    ram_st_sst_miso         => ram_st_sst_miso,
    reg_st_sst_mosi         => reg_st_sst_mosi,
    reg_st_sst_miso         => reg_st_sst_miso,

    -- ST interface
    in_sosi_arr            => bg_sosi_arr,
    out_raw_sosi           => out_sosi_arr(0),  -- original    raw beamlets output with c_sum_w bits.
    out_bst_sosi           => open,  -- requantized 16b beamlets output that is also used for internal BST.
    out_qua_sosi           => open  -- requantized  8b beamlets output.
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer real
  ----------------------------------------------------------------------------
  u_data_buf_re : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams     => c_db_nof_streams,
    g_data_type       => c_db_data_type_re,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
     -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_re_mosi,
    ram_data_buf_miso => ram_diag_data_buf_re_miso,
    reg_data_buf_mosi => reg_diag_data_buf_re_mosi,
    reg_data_buf_miso => reg_diag_data_buf_re_miso,
    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer imag
  ----------------------------------------------------------------------------
  u_data_buf_im : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams     => c_db_nof_streams,
    g_data_type       => c_db_data_type_im,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_im_mosi,
    ram_data_buf_miso => ram_diag_data_buf_im_miso,
    reg_data_buf_mosi => reg_diag_data_buf_im_mosi,
    reg_data_buf_miso => reg_diag_data_buf_im_miso,
    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );
end tb;
