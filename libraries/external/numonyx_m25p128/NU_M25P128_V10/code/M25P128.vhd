--
--        _/_/           _/_/_/
--      _/_/_/_/         _/_/_/
--      _/_/_/_/_/       _/_/_/              ____________________________________________
--      _/_/_/_/_/       _/_/_/             /                                           /
--      _/_/_/_/_/       _/_/_/            /                                 M25P128   /
--      _/_/_/_/_/       _/_/_/           /                                           /
--      _/_/_/_/_/_/     _/_/_/          /                                  128Mbit  /
--      _/_/_/_/_/_/     _/_/_/         /                   Low Voltage, SPI, 50Mhz /
--      _/_/_/ _/_/_/    _/_/_/        /                                           /
--      _/_/_/  _/_/_/   _/_/_/       /                     VHDL Behavioral Model /
--      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.0 /
--      _/_/_/    _/_/_/ _/_/_/     /                                           /
--      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2008 Numonyx B.V. /
--      _/_/_/      _/_/_/_/_/    /___________________________________________/
--      _/_/_/       _/_/_/_/
--      _/_/          _/_/_/
--
--
--             NUMONYX
--
--



Library IEEE;
  Use IEEE.std_logic_1164.all;
  Use IEEE.Std_Logic_signed.all;

Library Work;
  Use work.StringLib.all;
  Use work.Def.all;
  use work.TimingData.all;
  Use work.Data.all;


Entity TimingCheck_entity is 
    Generic(
            TimingCheckOn : boolean
            );
    Port(		
            C    : in std_logic ;
            D    : in std_logic ;
            S    : in std_logic ; 
            W    : in std_logic ;
            HOLD : in std_logic 
            );
End TimingCheck_entity;


Architecture behavior of TimingCheck_entity is 



 Begin



  -----------------------------------
  --   TIMING CHECK PROCESS
  -----------------------------------

  timingCheck_process : Process ( S, C, D, W, HOLD )

  variable  C_HighTime, C_LowTime, 
            S_HighTime, S_LowTime, 
            HOLD_LowTime, HOLD_HighTime,
            W_LowTime, W_HighTime,
            D_ValidTime : time := 0 ns; 


  begin

    
  if (TimingCheckOn) then
    
       
    ----  S event  ----
        
    if (S'event and S='0') then
       
        assert (now - C_HighTime >= tCHSL)
        report "#TIMING CHECK WARNING !!!#  tCHSL violation" severity warning;
        
        assert (now - S_HighTime >= tSHSL)
        report "#TIMING CHECK WARNING !!!#  tSHSL violation" severity warning;
        
        assert (now - W_HighTime >= tWHSL)
        report "#TIMING CHECK WARNING !!!#  tWHSL violation" severity warning;
        
        S_LowTime := now;
    
    elsif (S'event and S='1') then

        assert (now - C_HighTime >= tCHSH)
        report "#TIMING CHECK WARNING !!!#  tCHSH violation" severity warning;
        
        S_HighTime := now;


    ----  C event  ----
    
    elsif (C'event and C='0') then

        assert (now - C_LowTime >= tCL)
        report "#TIMING CHECK WARNING !!!#  tCL violation" severity warning;
        
       
        C_LowTime := now;

    elsif (C'event and C='1') then

        assert (now - C_HighTime >= tCH)
        report "#TIMING CHECK WARNING !!!#  tCH violation" severity warning;
        
      
        assert (now - S_LowTime >= tSLCH)
        report "#TIMING CHECK WARNING !!!#  tSLCH violation" severity warning;

        assert (now - S_HighTime >= tSHCH)
        report "#TIMING CHECK WARNING !!!#  tSHCH violation" severity warning;

        assert (now - HOLD_LowTime >= tHLCH)
        report "#TIMING CHECK WARNING !!!#  tHLCH violation" severity warning;

        assert (now - HOLD_HighTime >= tHHCH)
        report "#TIMING CHECK WARNING !!!#  tHHCH violation" severity warning;

        assert (now - D_ValidTime >= tDVCH)
        report "#TIMING CHECK WARNING !!!#  tDVCH violation" severity warning;
        
        C_HighTime := now;



    ---- D event  ----

    elsif (D'event) then

        assert (now - C_HighTime >= tCHDX)
        report "#TIMING CHECK WARNING !!!#  tCHDX violation" severity warning;
        
        if (D='0' or D='1') then
            D_ValidTime := now;
        end if;

    
    ---- W event  ----

    elsif (W'event and W='0') then

        assert (now - S_HighTime >= tSHWL)
        report "#TIMING CHECK WARNING !!!#  tSHWL violation" severity warning;
        
        W_LowTime := now;


    ---- HOLD event  ----

    elsif (HOLD'event and HOLD='0') then

        assert (now - C_HighTime >= tCHHL)
        report "#TIMING CHECK WARNING !!!#  tCHHL violation" severity warning;

        HOLD_LowTime := now;

    elsif (HOLD'event and HOLD='1') then

        assert (now - C_HighTime >= tCHHH)
        report "#TIMING CHECK WARNING !!!#  tCHHH violation" severity warning;

        HOLD_HighTime := now;   

    end if; 

  
  
  end if;

  end process; 
End behavior;
-------------------------------------------------------------------------------
--                          Block Lock ENTITY                                --
-------------------------------------------------------------------------------
LIbrary IEEE;
  Use IEEE.std_logic_1164.all;
  Use IEEE.Std_Logic_arith.all;

Library Work;
  Use work.def.all;
  Use work.CUIcommandData.all;
  Use work.data.all;
  Use work.StringLib.all;
  Use work.BlockLib.all;

Entity BlockLock_entity is 
            Port(
                  BlockLock_task           : inout BlockLockTask_type;
                  Kernel_BlockLockComplete : out TimeEvent;
                  Reset                    : in Boolean
                );
End BlockLock_entity;

Architecture behavior of BlockLock_entity is 

-- Default Value of Start Block Address after power-up or Hardware/Software Reset
Type BlockLock_type is array (BlockDim_range) of Lock_type;

Signal init                     : Boolean := true;
Shared Variable BlockLockArray  : BlockLock_type;

Procedure initBlockLock(Variable BlockLock: out BlockLock_type) is 
   Variable iBlock : BlockDim_range;
   begin 
      for iBlock in BlockDim_range loop
          BlockLockArray(iBlock):= UNLOCK;
      end loop;
end procedure;
         
begin


  Process (BlockLock_task, Reset)
  
  Variable iBlock, j             : BlockDim_range;
  Variable startBlock, endBlock  : BlockDim_range;
  Variable Status                : Message_type := NoError_msg;
  Variable isError               : boolean := false;
  Variable isLocked              : boolean := false;

  Begin 
        if (init) then  
        
                      initBlock(BlockBoundary);
                      
                      init <= false;
                
        elsif (Reset'event and Reset = true) then

            else 
         
              Case (BlockLock_task.task) is  
              
               When None    =>

                When getStatusByBlock =>
                        iBlock := BlockLock_task.startBlock;
               
                                 isLocked := BlockLockArray(iBlock);
                                 BlockLock_task.isLocked <= isLocked;
                                 BlockLock_task.isUnLocked <= not(isLocked);
                              
                     Status:=NoError_msg;

                     Kernel_BlockLockComplete  <= now + 1 ps;

                When getStatusByAddress =>
                       
                                iBlock := getBlock(BlockBoundary, BlockLock_task.address);

                                isLocked := BlockLockArray(iBlock);
                                BlockLock_task.isLocked <= isLocked;
                                BlockLock_task.isUnLocked <= not(isLocked);

                     Status:=NoError_msg;

                     Kernel_BlockLockComplete  <= now + 1 ps;
                     
                When putUnLock =>   -- Unlock a sequence of Blocks
                
                                iBlock := BlockLock_task.startBlock;
                                BlockLockArray(iBlock) := UNLOCK; 

                     Status:=NoError_msg;
                     Kernel_BlockLockComplete  <= now;
                     
                When  putLock =>   -- Lock all blocks

                                iBlock := BlockLock_task.startBlock;
                                BlockLockArray(iBlock) := LOCK; 

                     Status:=NoError_msg;
                     Kernel_BlockLockComplete  <= now;

                When SetLockByBP =>
                                PrintString("[" & time2str(now) & "]  Block Protect Status changed now is: " & slv2str(to_stdlogicvector(BlockLock_task.BP))); 
                                case BlockLock_task.BP is 
                                   When "000" => 
                                         for iBlock in 0 to 63 loop
                                              BlockLockArray(iBlock) := UNLOCK; 
                                         end loop;

                                   When "001" => 
                                         for iBlock in 0 to 62 loop
                                              BlockLockArray(iBlock) := UNLOCK; 
                                         end loop;
                                              BlockLockArray(63) := LOCK; 

                                   When "010" => 
                                         for iBlock in 0 to 61 loop
                                              BlockLockArray(iBlock) := UNLOCK; 
                                         end loop;
                                         for iBlock in 62 to 63 loop
                                              BlockLockArray(iBlock) := LOCK; 
                                         end loop;
                                   When "011" => 
                                         for iBlock in 0 to 59 loop
                                              BlockLockArray(iBlock) := UNLOCK; 
                                         end loop;
                                         for iBlock in 60 to 63 loop
                                              BlockLockArray(iBlock) := LOCK; 
                                         end loop;
                                   When "100" => 
                                         for iBlock in 0 to 55 loop
                                              BlockLockArray(iBlock) := UNLOCK; 
                                         end loop;
                                         for iBlock in 56 to 63 loop
                                              BlockLockArray(iBlock) := LOCK; 
                                         end loop;
                                   When "101" => 
                                         for iBlock in 0 to 47 loop
                                              BlockLockArray(iBlock) := UNLOCK; 
                                         end loop;
                                         for iBlock in 48 to 63 loop
                                              BlockLockArray(iBlock) := LOCK; 
                                         end loop;
                                    When "110" => 
                                         for iBlock in 0 to 31 loop
                                              BlockLockArray(iBlock) := UNLOCK; 
                                         end loop;
                                         for iBlock in 31 to 63 loop
                                              BlockLockArray(iBlock) := LOCK; 
                                         end loop;
                                   When "111" => 
                                         for iBlock in 0 to 63 loop
                                              BlockLockArray(iBlock) := LOCK; 
                                         end loop;

                                   end case;

                     Status:=NoError_msg;
                     Kernel_BlockLockComplete  <= now;
                When Others =>

                             PrintString("+++ General Error +++ Task of BlockLock Entity not recognized");
                             Status := GENERIC_ERROR_msg;
                             isError:= true;
                             Kernel_BlockLockComplete  <= now;

              End Case;
              
              
      
      end if;
      
    End Process;

 

End behavior;


--------------------------------------------------------------------------------
--                      Mass Memory Entity                                    --
--------------------------------------------------------------------------------
Library IEEE;
  Use IEEE.std_logic_1164.all;
Library Work;
  Use work.def.all;
  Use work.data.all;
  Use work.StringLib.all;
  Use work.MemoryLib.all;

Entity Memory_entity is 
  generic(
        MemoryFileName       : string
        );
  port (
        Memory_Status        : out Status_type;
        Memory_Task          : inout MemoryTask_type;
        Kernel_MemComplete   : out TimeEvent;
        Reset                : in Boolean
        );
End Memory_entity;

Architecture behavior of Memory_entity is 
Begin

  Process 
    Variable Memory      : memory_rec;
    Variable Status      : Status_type;
    Variable hold_address     : AddrMem_type;
    Variable address     : AddrMem_type;
    Variable addressEnd  : AddrMem_type;
    Variable iAddr       : AddrMem_type;
    Variable i           : Natural;
    Variable numData     : Integer;
    Variable data        : DataMem_type;
    Variable dataFF    : DataMem_type := (Others => '1' );
    Variable PowerUp     : Boolean := false;
    Variable isAll1 : Boolean;
    Variable len         : Integer; 
    -- Page 
    Variable LenPage     : Integer; 
    Variable iPage       : Integer; 
    Variable Page        : page_rec;
    Variable addAddrPage : Integer; 
    
    Begin
    if not(PowerUp) then 
                  InitMemory(Memory);
                  if (MemoryFileName="") then 
                        PrintString("[" & time2str(now) & "] Memory File not found: Init All Array with FF value");
                                         else 
                        LoadMemoryFile(MemoryFileName, Memory);
                        PrintString("[" & time2str(now) & "] Load Memory File: " & MemoryFileName);
                  end if;
                  PowerUp:=true;
    end if;

    wait until Memory_Task'event or Reset'event;
    if (reset) then
               Status.Message := NoError_msg;
               iPage := 0;
               for i in Page_range loop Page(i) := dataFF; end loop;
               LenPage := 0;
    else 
        case Memory_task.task is 
            
             when InitPage =>
                      LenPage:=0;
                      iPage :=0;
                      hold_address := Memory_task.address;
                      Status.Message := NoError_msg;

             when addByteInPage =>
                      data := Memory_task.data;
                      Page(iPage) := data;
                      if iPage + 1 = Page_dim then 
                                        iPage := 0;     
                                        LenPage := Page_dim;
                                   else iPage := iPage + 1;
                                        if LenPage < Page_dim  then 
                                                LenPage := LenPage + 1;
                                        end if;
                      end if;
                      Status.Message := NoError_msg;
                      Memory_task.eventTime <= now;

             when putPage =>
                      if LenPage>0 then 
                              for i in 0 to LenPage - 1 loop
                                  addAddrPage := hold_address + i;
                                  putMemory(Memory,addAddrPage, Page(i));
                              end loop;
                      end if;
                      Status.Message := NoError_msg;

             when put =>
 
                      data := Memory_task.data;
                      putMemory(Memory, address, data);
                      Memory_task.eventTime <= now;

             when get =>
                      address := Memory_task.address;
                      getMemory(Memory, address, data);
                      Memory_task.data <= data;
                      Memory_task.eventTime <= now + 1 ps;
                      wait for 1 ps;
                      Status.Message := NoError_msg;

             when BlockErase =>
                      Address    := Memory_task.address;
                      AddressEnd := Memory_task.addressEnd;
                      if Address > AddressEnd then 
                                  PrintString("+++ General Error +++ in Mass Memory Entity: Address is greater than AddressEnd" );
                         else 
                                  For i in Address to AddressEnd loop
                                   iAddr := i;
                                   putMemory(Memory, iAddr, dataFF);
                                  End Loop;
                      end if;
                      Status.Message := NoError_msg;
             when getBlock =>
             when putBlock =>
             when readIFall1 => 
                      Address := Memory_task.address;
                      AddressEnd := Memory_task.addressEnd;
                      Len := AddressEnd - Address + 1;
                      if Address > AddressEnd then 
                                  PrintString("+++ General Error +++ in Mass Memory Entity: Address is greater than AddressEnd" );
                         else 
                              for i in Address to AddressEnd loop
                                       iAddr := i;
                                       getMemory(Memory, iAddr, data);
                                       if (data/=dataFF) then exit;end if;
                              end loop;
                              if (data/=dataFF) then isAll1:= false;
                                                  else isAll1:= true;
                              end if;
                      Memory_task.AddressEnd <= iAddr;
                      Memory_task.isAll1 <= isAll1;
                      Memory_task.eventTime <= now + 1 ps; 
                      wait for 1 ps;
                      end if;

             when load =>
             when save =>
             when printList =>
                      PrintList(Memory);
             when printData =>
                      PrintData(address, data);
             when others =>
        end case;
    Memory_Status <= Status;  
    Kernel_MemComplete <= now;
 
    end if;
  End Process;
End behavior;
--------------------------------------------------------------------------------
--                      Program Entity                                       --
--------------------------------------------------------------------------------
Library IEEE;
  Use IEEE.std_logic_1164.all;
Library Work;
  Use work.def.all;
  Use work.data.all;
  use work.TimingData.all;
  Use work.StringLib.all;
  use work.BlockLib.all;
  Use work.MemoryLib.all;
  
Entity Program_entity is 
  port (
        Program_Status          : out Status_type;
        Program_Task            : in ProgramTask_type;
        BlockLock_task          : inout BlockLockTask_type;
        Kernel_BlockLockComplete: in TimeEvent;
        Memory_Task             : inout MemoryTask_type;
        Kernel_MemComplete      : in TimeEvent;
        StatusRegister_task     : out StatusRegisterTask_type;
        VppH                    : in Boolean;
        Reset                   : in Boolean
        );
End Program_entity;
  
Architecture behavior of Program_entity is 
        Function EventTimeGuarded(V: vectorTimeEvent_type) Return TimeEvent is
        Variable i:Integer;
        Variable big : Time;
          Begin 
          big := V(V'LOW);
          For i in V'range Loop
              If V(i)>big Then 
                        big:=V(i);
                      End If;
          End Loop;
        Return big; 
        end Function; 

  Signal Program_Event            : boolean := false;
  Signal ErrorCheck_Event         : boolean := false;
  Signal ErrorCheckDisable_Event  : EventTimeGuarded TimeEvent register := 0 ns;
  Shared Variable Error_Event     : boolean := false;
  Shared Variable Busy            : boolean := false;
  Shared Variable hold_address    : AddrMem_type := 0;
  Shared Variable startTime       : time := 0 ns;
  Shared Variable delayTime       : time := PageProgram_time;
  Shared Variable Status          : message_type := NoError_msg;
  Begin
  Process
    begin
    wait until (Program_Task'event or Reset'event);
    if Reset'event and reset then
             Status := NoError_msg;
             hold_address := 0;
             startTime := 0 ns; 
             delayTime := PageProgram_time;
    else Case (Program_Task.task) is
         When Program =>
              hold_address := Program_Task.address;
              startTime := now;
              Busy := True;
              Status:=Busy_msg;
              if (VppH) then delayTime := FastPageProgram_time;
                        else delayTime := PageProgram_time;
              end if;
              ErrorCheck_event <= not ErrorCheck_event;
              Program_Event <= not(Program_Event) after delayTime; 
         When Others =>
         End Case;
    end if;
    End process;

    Program_process: Process 
      Variable temp_Data : DataMem_type;
     
      constant dataFF: DataMem_type:= X"FF"; 
    begin
    wait until (Program_Event'event or Reset'event );
    if (reset'event) then 
              if reset then 
                       Memory_task.task <= none;
              end if;
    else
      if (Status = Busy_msg) then 
               Memory_task.task <= putPage;
               Memory_task.address <= hold_address;
               Memory_task.eventTime <= now;
                 wait for 1 fs;
          busy:=false;
          ErrorCheckDisable_event <= now;
      end if;
    end if;
    End Process Program_process; 
    
    ErrorCheck_Process: Process
      Variable isBlockProtect: Boolean; 
      begin 
      wait until ErrorCheck_event'event;
      if (Status=Busy_msg) then 
            BlockLock_task.task <= getStatusByAddress;
            BlockLock_task.address <= hold_address;
            BlockLock_task.eventTime <= now;
            wait until Kernel_BlockLockComplete'event;

--******************************************************************************** 
                 isBlockProtect:=BlockLock_task.isLocked; 
--********************************************************************************                   
              
            if (isBlockProtect) then
                        Status := BlockLock_msg;
                   else 
                        Program_Status.message <= Status;
                        Program_Status.busy <= busy;
                        Program_Status.address <= hold_address;
                            wait until ErrorCheckDisable_event'event; 
                        if Status=Busy_msg  then Status := NoError_msg;end if; -- Program Complete
            end if;
      if (Status/=Busy_msg) then busy:=false;
                               StatusRegister_task.task <= reset_WEL;
                               StatusRegister_task.SR <= (Others =>  '0'); -- NP
                               StatusRegister_task.eventTime <= now;
                               end if;
      Program_Status.busy <= busy;
      Program_Status.message <= Status;

      end if;
      end Process ErrorCheck_Process;
end behavior;

--------------------------------------------------------------------------------
--                      Erase Entity                                       --
--------------------------------------------------------------------------------
Library IEEE;
  Use IEEE.std_logic_1164.all;
Library Work;
  Use work.def.all;
  Use work.data.all;
  use work.TimingData.all;
  Use work.StringLib.all;
  use work.BlockLib.all;
  Use work.MemoryLib.all;
  
Entity Erase_entity is 
  port (
        Erase_Status            : out Status_type;
        Erase_Task              : in EraseTask_type;
        BlockLock_task          : inout BlockLockTask_type;
        Kernel_BlockLockComplete: in TimeEvent;
        Memory_Task             : inout MemoryTask_type;
        Kernel_MemComplete      : in TimeEvent;
        StatusRegister_task     : out StatusRegisterTask_type;
        VppH                    : in Boolean;
        Reset                   : in Boolean
        );
End Erase_entity;
  
Architecture behavior of Erase_entity is 
        Function EventTimeGuarded(V: vectorTimeEvent_type) Return TimeEvent is
        Variable i:Integer;
        Variable big : Time;
          Begin 
          big := V(V'LOW);
          For i in V'range Loop
              If V(i)>big Then 
                        big:=V(i);
                      End If;
          End Loop;
        Return big; 
        end Function; 

  Signal Erase_Event            : boolean := false;
  Signal ErrorCheck_Event         : boolean := false;
  Signal ErrorCheckDisable_Event  : EventTimeGuarded TimeEvent register := 0 ns;
  Shared Variable Error_Event     : boolean := false;
  Shared Variable Busy            : boolean := false;
  Shared Variable HoldAddress     : AddrMem_type := 0;
  Shared Variable HoldAddress_BlockStart: AddrMem_type := 0;
  Shared Variable HoldAddress_BlockEnd : AddrMem_type := 0;
  Shared Variable startTime       : time := 0 ns;
  Shared Variable delayTime       : time := BlockErase_time;
  Shared Variable Status          : message_type := NoError_msg;
  Shared Variable isBulkErase     : Boolean := false;
  Begin
  Process
    begin
    wait until (Erase_Task'event or Reset'event);
    if Reset'event and reset then
             Status := NoError_msg;
             HoldAddress := 0;
             startTime := 0 ns; 
             delayTime := BlockErase_time;
    else Case (Erase_Task.task) is

         When BlockErase =>
               HoldAddress            := Erase_Task.address;
               HoldAddress_BlockStart := getAddrBlockStart( BlockBoundary, Erase_Task.address);  --, mode16bit );
               HoldAddress_BlockEnd   := getAddrBlockEnd (  BlockBoundary, Erase_Task.address);  --, mode16bit );
               startTime := now;
               Busy := True;isBulkErase:= false;
               ErrorCheck_event <= not ErrorCheck_event;
               if (VppH) then delayTime := FastBlockErase_time;
                         else delayTime := BlockErase_time;
               end if;
               Erase_Event <= not(Erase_Event) after delayTime; 
               Status:=Busy_msg;
                  
         When BulkErase =>
               HoldAddress            := Erase_Task.address;
               HoldAddress_BlockStart := getAddrBlockStart( BlockBoundary, 0);               -- First Block;
               HoldAddress_BlockEnd   := getAddrBlockEnd (  BlockBoundary, Memory_dim - 1);  -- Last Block;
               Busy := True; isBulkErase:= true;
               ErrorCheck_event <= not ErrorCheck_event;
               if (VppH) then delayTime := FastBulkErase_time;
                         else delayTime := BulkErase_time;
               end if;
               Erase_Event <= not(Erase_Event) after delayTime; 
               Status:=Busy_msg;
                  
         When Others =>
         End Case;
    end if;
    End process;

    Erase_process: Process 
      Variable temp_Data : DataMem_type;
     
      constant dataFF: DataMem_type:= X"FF"; 
    begin
    wait until (Erase_Event'event or Reset'event );
    if (reset'event) then 
              if reset then 
                       Memory_task.task <= none;
              end if;
    else
      if (Status = Busy_msg) then 
          Memory_task.task <= BlockErase;
          Memory_task.address <= HoldAddress_BlockStart;
          Memory_task.addressEnd <= HoldAddress_BlockEnd;
          Memory_task.eventTime <= now;
              Wait Until Kernel_MemComplete'event;

          busy:=false;
          ErrorCheckDisable_event <= now;
      end if;
    end if;
    End Process Erase_process; 
    
    ErrorCheck_Process: Process
      Variable isBlockProtect: Boolean; 
      begin 
      wait until ErrorCheck_event'event;
      if (Status=Busy_msg) then 
            if isBulkErase then 
               isBlockProtect := false;
            else 
               BlockLock_task.task <= getStatusByAddress;
               BlockLock_task.address <= HoldAddress;
               BlockLock_task.eventTime <= now;
                  wait until Kernel_BlockLockComplete'event;
               isBlockProtect:=BlockLock_task.isLocked; 
            end if; 
            if (isBlockProtect) then
                        Status := BlockLock_msg;
                   else 
                        Erase_Status.message <= Status;
                        Erase_Status.busy <= busy;
                        Erase_Status.address <= HoldAddress;
                            wait until ErrorCheckDisable_event'event; 
                        if Status=Busy_msg  then Status := NoError_msg;end if; -- Erase Complete
            end if;
      if (Status/=Busy_msg) then busy:=false;
                               StatusRegister_task.task <= reset_WEL;
                               StatusRegister_task.SR <= (Others =>  '0'); -- NP
                               StatusRegister_task.eventTime <= now;
                               end if;

      Erase_Status.busy <= busy;
      Erase_Status.message <= Status;

      end if;
      end Process ErrorCheck_Process;
end behavior;

--------------------------------------------------------------------------------
--                      Status Register Entity                                --
--------------------------------------------------------------------------------
Library IEEE;
  Use IEEE.std_logic_1164.all;

Library Work;
  Use work.def.all;
  Use work.data.all;
  Use work.TimingData.all;
  Use work.StringLib.all;
  Use work.BlockLib.all;
  use work.CUIcommandData.all;

  
Entity StatusRegister_entity is 
  port (
        StatusRegister_status    : out Status_type;
        SRWD_flag                : out Std_Logic;
        WEL_flag                 : out Std_Logic;
        Kernel_status            : in Status_type;
        BlockLock_task           : out BlockLockTask_type;
        Kernel_BlockLockComplete : in TimeEvent;
        StatusRegister_task      : inout StatusRegisterTask_type;
        SR_reg                   : inout DataStatusReg_type ;
        StatusRegister_report    : out report_type;
        Reset                    : in Boolean
        );
End StatusRegister_entity;
  
Architecture behavior of StatusRegister_entity is 


  Signal SRWD_bit          : bit; -- 7
  Signal BP2_bit           : bit; -- 4
  Signal BP1_bit           : bit; -- 3
  Signal BP0_bit           : bit; -- 2
  Signal WEL_bit           : bit; -- 1
  Signal WIP_bit           : bit; -- 0



  Signal Program_Event           : boolean := false;
  Signal ErrorCheck_Event        : boolean := false;
  Signal ErrorCheckDisable_Event : TimeEvent := 0 ns;
  Signal SR_WriteCompleteEvent   : TimeEvent := 0 ns;
  Shared Variable Status         : Message_type := NoError_msg;
  Shared Variable Busy           : Boolean := false;

Begin
  
  
  
  SR_reg <= (   7 => SRWD_bit,
                6 => '0',         -- Bit 6 is reserved             
                5 => '0',         -- Bit 5 is reserved 
                4 => BP2_bit,
                3 => BP1_bit,                    
                2 => BP0_bit,       
                1 => WEL_bit,                  
                0 => WIP_bit   
             );
  
  
  SRWD_flag <= '1' when SRWD_bit = '1'  else '0';
  WEL_flag  <= '1' when WEL_bit  = '1'  else '0';
  
  StatusRegisterTASK_process: Process 
               Variable Message : Message_type := none_msg;
           Begin
               wait until StatusRegister_task'event or Reset'Event;
           if (Reset'event and not(reset)) then 

                            BP2_bit <= '0';
                            BP1_bit <= '0';
                            BP0_bit <= '0';
                            SRWD_bit<= '0';
           else 
              Case (StatusRegister_task.task) is 
                When Read   => 

                            StatusRegister_task.SR <= SR_reg; 
                            Message := none_msg;
                            Status := NoError_msg;

                When Write  => 


                            Status := Busy_msg;
                            Busy   := true;
                            ErrorCheck_event <= not ErrorCheck_event;
                            Program_Event <= not(Program_Event) after WriteStatusRegister_time;

                               wait until SR_WriteCompleteEvent'event;

                            SRWD_bit <= StatusRegister_task.SR(7); 
                            BP2_bit  <= StatusRegister_task.SR(4);
                            BP1_bit  <= StatusRegister_task.SR(3);
                            BP0_bit  <= StatusRegister_task.SR(2);

                            Message  := setWRSR_msg;
                            
                            BlockLock_task.task <= SetLockByBP;
                            BlockLock_task.BP <=  (0 => StatusRegister_task.SR(2), 
                                                   1 => StatusRegister_task.SR(3), 
                                                   2 => StatusRegister_task.SR(4)
                                                   );
                            BlockLock_task.eventTime <= now;
                            
                            wait until Kernel_BlockLockComplete'event;
                            


                When ReadProtectAreaSizes => 
                            StatusRegister_task.SR <= ( 2 => BP2_bit,
                                                        1 => BP1_bit,
                                                        0 => BP0_bit,
                                                        others => '0');
                            Message := none_msg;
                            Status := NoError_msg;

                When Set_SRWD =>
                            SRWD_bit <= '1'; 
                            Message := setSRWDbit_msg;
                            Status := NoError_msg;


                When Reset_SRWD =>
                            SRWD_bit <= '0'; 
                            Message := resetSRWDbit_msg;
                            Status := NoError_msg;


                When Read_SRWD =>
                            StatusRegister_task.SR(0) <= SRWD_bit;
                            Message := none_msg;
                            Status := NoError_msg;

                When Set_WEL =>
                            WEL_bit <= '1';
                            Message := setWELbit_msg;
                            Status := NoError_msg;


                When Reset_WEL =>
                            WEL_bit <= '0';
                            Message := resetWELbit_msg;
                            Status := NoError_msg;
                            
                When Read_WEL =>
                            StatusRegister_task.SR(0) <= WEL_bit;
                            Message := none_msg;
                            Status := NoError_msg;

                When Others =>  

              End Case;
              
           StatusRegister_task.eventTime <= now;
           StatusRegister_report.message <= Message;
           StatusRegister_report.eventTime <= now + 1 ps;
           end if;
  End Process;


  Program_process: Process 
             begin 
             wait until (Program_Event'event or Reset'event );
             if (Status = Busy_msg) then 
                 busy := False;
                 ErrorCheckDisable_event <= now;
                 SR_WriteCompleteEvent <= now;
             end if;
    end process;

  ErrorCheck_Process: Process
  Begin
      wait until ErrorCheck_event'event;

        StatusRegister_status.message <= Status;
        StatusRegister_status.busy    <= Busy;

            wait until ErrorCheckDisable_event'event or Reset'event;

        if (reset'event and reset) then Status:=CmdAbtByRst_msg;
                                   else Status:=NoError_msg;
        end if;
        Busy := False;

        StatusRegister_status.message <= Status;
        StatusRegister_status.busy    <= Busy;

  End Process;
      
  UpDate_WithOutError: Process(Kernel_Status, reset) 
          begin
                If ((Reset'event and not(reset)) ) then 
                           WIP_bit <= '0';
                Else
                   if (Kernel_status'event) then 
                           if (Kernel_status.busy) then WIP_bit <='1';
                                                   else WIP_bit <='0';
                           end if;
                   end if;
                End if; 
  end process UpDate_WithOutError;
  
  print_SR: Postponed Process (SR_reg) 
            Variable SR_info : DataStatusReg_type;
            Variable last_time : time := 0 ns;
            Begin
                -- Print SR
                if (now/=last_time) then 
                                SR_info := SR_reg;
                                PrintString("[" & time2str(now) & "]  Status Register Update: " & slv2str(to_stdlogicvector(SR_info)) & "; hex = " & slv2hex(to_stdlogicvector(SR_info)));
                last_time := now;
                end if;
 end process;

End behavior;

---------------------- KERNEL ENTITY ----------------------------------------------------------------------

Library IEEE;
   Use IEEE.std_logic_1164.all;
Library Work;
  Use work.def.all;
  use work.TimingData.all;
  Use work.Data.all;
  Use work.StringLib.all;
  use work.CUIcommandData.all;

Entity Kernel_entity is 
  port( 
        Program_Status       : in Status_type;
        Erase_Status         : in Status_type;
        StatusRegister_status: in Status_type;
        Kernel_Status        : out Status_type;
        Kernel_Report        : in Report_type;
        Kernel_VerifyEvent   : out TimeEvent;
        Kernel_ErrorEvent    : in ErrorEvent;
        Kernel_CommandDecode : in command_type;
        Reset                : in boolean
      );
end Kernel_entity;

Architecture behavior of Kernel_entity is 
  Begin
  StatusUpDate: Process (Program_Status, Erase_Status, Reset) begin
                  If (Reset'event and Reset) then
                       Kernel_Status.message <= NoError_msg;
                       Kernel_Status.busy    <= false;
                       Kernel_status.suspended <= false;
                       Kernel_status.address <= 0;
                  Else 
                       if Program_Status'event    then  Kernel_Status.message <= Program_Status.message;  end if;
                       --if ProtectReg_Status'event then  Kernel_Status.message <= ProtectReg_Status.message;  end if;
                       if   Erase_Status'event    then  Kernel_Status.message <= Erase_Status.message;    end if;
                       if   StatusRegister_status'event then Kernel_Status.message <= StatusRegister_status.message;    end if;
                       Kernel_Status.busy      <= Program_Status.busy or Erase_Status.busy or StatusRegister_status.busy;  
                       Kernel_status.suspended <= Program_Status.suspended or Erase_Status.suspended;
                       if Program_Status.busy or Program_Status.suspended then 
                                         Kernel_status.address <= Program_Status.address;
                       end if;
                       if Erase_Status.busy or Erase_Status.suspended then 
                                         Kernel_status.address <= Erase_Status.address;
                       end if;
                  End If;
  End Process; 
  KernelReport_Process: Process (Kernel_Report) begin 
                        If not(Reset) then
                               Case (Kernel_Report.message) is 
                                     when none_msg        => -- None message
                                     when NoError_msg        => 
                                     when setSRWDbit_msg =>
                                                PrintString(" $Info: Status Register -> SRWD bit as been Set"); 
                                     when resetSRWDbit_msg =>
                                                PrintString(" $Info: Status Register -> SRWD bit as been Reset"); 
                                     when setWELbit_msg =>
                                                PrintString(" $Info: Status Register -> WEL bit as been Set"); 
                                     when resetWELbit_msg =>
                                                PrintString(" $Info: Status Register -> WEL bit as been Reset"); 
                                     when NoWRSR_msg => 
                                                PrintString(" #Warning: [SR Write Locked]  Status Register -> Write Access Denied"); 
                                     when NoWrite_msg =>
                                                PrintString(" #Warning: [Write Operation Locked]  No Write Operation is allowed: PP, SE, BE, WRSR");
                                     when WIP_msg =>
                                                PrintString(" #Warning: [Invalid Command]  Device is Busy! The command will be Ignored");

                                     when setWRSR_msg =>
                                                PrintString(" $Info: Status Register -> Block Protect and SRWD bit are UpDate"); 

                                     when BlockLock_msg      => 
                                                PrintString(" #Warning: [Locked Warning]   Cannot complete operation when the block is locked "); 
                                     when CmdAbtByRst_msg   => 
                                                PrintString(" #Warning: [Command Aborted]   Command Aborted by Reset "); 
                                     when BkEraseProt_msg =>
                                                PrintString(" #Warning: [Bulk Locked]  Bulk Erase Command not allowed when BP bits are not all 0 "); 
-----------------------------------------------------------------------------------------------------------------------------------------------------
                                     when CmdSeq_msg         => 
                                                PrintString(" !Error:   [Invalid Command]  Sequence Command Unknown"); 
                                     when SuspCmd_msg        => 
                                                PrintString(" !Error:   [Invalid Command]  Cannot execute this command during suspend"); 
                                     when SuspAcc_msg        => 
                                                PrintString(" !Error:   [Invalid Command]  Cannot access this address due to suspend"); 
                                     when AddrRange_msg      => 
                                                PrintString(" !Error:   [Invalid Address]  Address out of range"); 
                                     when AddrTog_msg        => 
                                                PrintString(" !Error:   [Program Buffer]   Cannot change block address during command sequence"); 
                                     when BuffSize_msg       => 
                                                PrintString(" !Error:   [Program Buffer]   Buffer size is too large (Max Size is ProgramBuffer_dim"); 
                                     when InvVDD_msg         => 
                                                PrintString(" !Error:   [Invalid Supply]   Voltage Supply must be: VDD>VDDmin or VDD<VDDmax "); 
                                     when InvVPP1_msg         => 
                                                PrintString(" !Error:   [Invalid P/E Supply]   Program/Erase Voltage Supply must be: VPP>VPP1min");

                                     when InvVPPH_msg         => 
                                                PrintString(" !Error:   [Invalid Fast Program Supply]  Program Voltage Supply for Fast operation must be: VPP>VPPmin or VPP<VPPmax "); 
                                     when ByteToggle_msg     => 
                                                PrintString(" !Error:   [BYTE_N Toggled]   Cannot toggle BYTE_N while busy"); 
                                     when BlkBuffer_msg      => 
                                                PrintString(" !Error:   [Program Buffer]   Program Buffer cannot cross block boundary"); 
                                     when PreProg_msg        => 
                                                PrintString(" !Error:   [Program Failure]  Program Failure due to cell failure"); 
                                     when PRegPreProg_msg    =>
                                                PrintString(" !Error:   [Program Failure]  Protection Register is Locked");
                                     when OTPlock_msg        => 
                                                PrintString(" !Error:   [Program Failure]  One-Time-Programmable segment of the Protection Register is locked");
                                     when UPlock_msg         => 
                                                PrintString(" !Error:   [Program Failure]  User Programmable Register is locked"); 
                                     when SuspAccWarn_msg    => 
                                                PrintString(" #Warning: [Invalid Access]   It isn't possible access this address due to suspend"); 
                                     when IncProgSeq_msg     =>    
                                                PrintString(" !Error:   [Program Failure]   Incorrect Word Program Command sequence, address out of block"); 

                                     when IncEraseSeq_msg    => 
                                                PrintString(" !Error:   [Erase Failure]     Incorrect Block Erase Command sequence, address out of block"); 

                                     when BlockOverbound_msg         => 
                                                PrintString(" !Error:   [Program Failure]   One or more address past the block boundaries"); 

                                     when AddrCFI_msg        => 
                                                PrintString(" #Warning: [Invalid CFI Address]   CFI Address out of range");
                                     when NoBusy_msg         => 
                                                PrintString(" #Warning: [NO Busy]      Device is not Busy");
                                     when NoSusp_msg         => 
                                                PrintString(" #Warning: [NO Suspend]   Nothing previus suspend command");
                                     when NoOPsameBlock_msg  =>
                                                PrintString(" #Warning: [Invalid Block] Operation not Allowed in the Block that is being erased or programmed");
                                     when NoOPguaranteed_msg  =>
                                                PrintString(" #Warning: [Operation not guaranteed] The Read Array command is accepted but the data output in not guaranteed");
                                     when NoOPallowed_msg    =>
                                                PrintString(" !Error: [Invalid Command] Invalid Dual Operation");
                                     when GENERIC_ERROR_msg  =>
                                                Assert (false)
                                                Severity ERROR;
                                                Report "-- GENERAL ERROR CODE -- Don't Continue";
 
                                     when Suspend_msg        => 
                                     when Busy_msg           => 
                                     when others =>                
                               End case;
                        
                        end if;
  End Process KernelReport_Process;

                


End behavior;

-------------------------------------------------------------------------------
--                      CUI DECODER ENTITY                                   --
-------------------------------------------------------------------------------
LIbrary IEEE;
  Use IEEE.std_logic_1164.all;
  Use IEEE.Std_Logic_arith.all;
Library Work;
  Use work.def.all;
  --use work.TimingsLib.all;
  Use work.data.all;
  Use work.StringLib.all;
  Use work.CUIcommandData.all;
  
Entity CUIdecoder_entity is 
  generic (
        Command     : CommandList_type;
        DataSeq     : DataLatch_type
       );
  port (
        DataIn               : in  DataLatch_type;
        Kernel_CUIcommandEvent    : in  TimeEvent;
        Kernel_VerifyEvent   : out TimeEvent;
        Kernel_CompleteEvent : in  TimeEvent;
        Kernel_ErrorEvent    : in  Std_Logic;
        Kernel_CommandDecode : out Command_type
        );
End CUIdecoder_entity;


Architecture behavior of CUIdecoder_entity is 
  Begin

  decode: Process
    Variable iSeq: Integer := 1;
    Variable checkData: Boolean;
    Variable checkAddr: Boolean; 
    variable i : Integer;

    
    -- "45EC" = "45XC"; "2314" /= "2324"; "A231" = "XXXX"
    Function isMatchSLV(d:string; s:string) Return boolean is 
       Variable i:Integer := 1;
       Variable dc,sc: Character;
    begin 
       loop
          If i<=d'right  Then dc:=d(i); Else dc:=NUL; End If;
          If i<=s'right  Then sc:=s(i); Else sc:=NUL; End If;

          If ((dc/=sc and sc/='X') or dc=NUL) Then
               Return (character'pos(dc) - character'pos(sc)=0);
          Else
               i:=i+1;
          End If;
             
       end Loop;
    end isMatchSLV;
    
  Begin 
  Wait Until Kernel_CUIcommandEvent'event;  

          checkData := isMatchSLV( slv2hex(DataIn) , slv2hex(DataSeq) );

          if (checkData) then -- Sequence confirmed
          
              Kernel_VerifyEvent <= now;    -- No Error

              Kernel_CommandDecode.cmd <= Command;
              Kernel_CommandDecode.eventTime <= now;
              

              else 
                   
                   if false then PrintString("[" & time2str(now) & "]  Waiting for complete intruction ");end if;
                   wait until Kernel_CompleteEvent'event or Kernel_ErrorEvent'event;
                   if false then PrintString("[" & time2str(now) & "]  Complete Intruction ");end if;
        
    end if;
  End Process decode;

End behavior;

LIBRARY IEEE;
    Use IEEE.std_logic_1164.all;
    Use IEEE.std_logic_TextIO.all;
    Use IEEE.Std_Logic_arith.all;

LIBRARY WORK;
    Use work.def.all;
    Use work.data.all;
    Use work.TimingData.all;
    Use work.BlockLib.all;
    Use work.StringLib.all;
    Use work.CUIcommandData.all;
-----------------------------------------------------
--			ENTITY	
-----------------------------------------------------
Entity M25P128 is

Generic(
                MemoryFileName  : string;
                TimingCheckOn   : boolean
                );
Port(		VCC  : in real ;
		C    : in std_logic ;
                D    : in std_logic ;
                S    : in std_logic ; 
                W_Vpp: in real;
                HOLD : in std_logic ;
		Q    : out std_logic
                );

End M25P128;


Architecture behavior of M25P128 is

Signal W         : Std_Logic;
Signal VppH      : Boolean;
Signal DataOut   : Std_Logic;
Signal DataIn    : Std_Logic_Vector(7 downto 0);
Signal hold_address : AddrMem_type;
Signal isDataIn  : Boolean;
Signal hold_cond : Boolean;
Signal raz       : Boolean;
Signal Power_up  : Boolean := true;
Signal select_ok : Boolean := true;
Signal c_int     : Std_Logic;
Signal cpt       : Integer := 0;
Signal byte_cpt  : Integer :=0;
Signal reset     : Boolean;
Signal ChipSelect_allowed  : Boolean := false;
Signal ReadAccess_allowed  : Boolean := false;
Signal WriteAccess_allowed : Boolean := false; 

-- Resolved Function 
Function EventTimeGuarded(V: vectorTimeEvent_type) Return TimeEvent is
Variable i:Integer;
Variable iBig:Integer;
Variable big : Time;
  Begin 
  big := V(V'LOW);
  for i in V'range loop
      if V(i)>big then 
                big:=V(i);
                iBig := i;
      end if;
  end loop;
return big; 
End Function;

Function CommandGuarded(VET: vectorCommand_type) Return Command_Type is
    Variable i : Integer; 
    Variable bigET : Command_Type;
    Variable MTN : Command_Type;
  Begin 
  bigET  := VET(VET'LOW);
  for i in VET'range loop
      MTN:= VET(i);
      if VET(i).eventTime > bigET.eventTime then bigET := VET(i); end if;
      
  end loop;
return bigET; 
End Function;

Function ReportGuarded(VKR: vectorReport_type) Return Report_type is 
Variable i:Integer;
Variable bigKR : Report_type;
Variable KRN   : Report_type;
  Begin 
  bigKR  := VKR(VKR'LOW);
  
  for i in VKR'low + 1 to VKR'high loop
      KRN:= VKR(i);
      if VKR(i).eventTime > bigKR.eventTime then bigKR := VKR(i); end if;
      
  end loop;
Return bigKR; 
End Function;

Function StatusRegisterTaskGuarded(VET: vectorStatusRegisterTask_type) Return StatusRegisterTask_type is 
Variable i:Integer;
Variable bigET : StatusRegisterTask_type;
Variable MTN : StatusRegisterTask_type;
  Begin 
  bigET  := VET(VET'LOW);
  
  for i in VET'range loop
      MTN:= VET(i);
      if VET(i).eventTime > bigET.eventTime then bigET := VET(i); end if;
      
  end loop;
return bigET; 
End Function;

Function ReadStatusGuarded(VET: vectorReadStatus_type) Return ReadStatus_type is 
Variable i:Integer;
Variable bigET : ReadStatus_type;
  Begin 
  bigET  := VET(VET'LOW);
  
  for i in VET'range loop
      if VET(i).eventTime > bigET.eventTime then bigET := VET(i); end if;
      
  end loop;
return bigET; 
End Function;

Function DataOutBufferGuarded(VET: vectorDataOutBuffer_type) Return DataOutBuffer_type is 
Variable i:Integer;
Variable bigET : DataOutBuffer_type;
  Begin 
  bigET  := VET(VET'LOW);
  
  for i in VET'range loop
      if VET(i).eventTime > bigET.eventTime then bigET := VET(i); end if;
      
  end loop;
return bigET; 
End Function;



Function ProgramTaskGuarded(VPT: vectorProgramTask_type) Return ProgramTask_type is 
Variable i:Integer;
Variable bigPT : ProgramTask_type;
  Begin 
  bigPT  := VPT(VPT'LOW);
  
  for i in VPT'range loop
      if VPT(i).eventTime > bigPT.eventTime then bigPT := VPT(i); end if;
      
  end loop;
return bigPT; 
End Function;

Function EraseTaskGuarded(VPT: vectorEraseTask_type) Return EraseTask_type is 
Variable i:Integer;
Variable bigPT : EraseTask_type;
  Begin 
  bigPT  := VPT(VPT'LOW);
  
  for i in VPT'range loop
      if VPT(i).eventTime > bigPT.eventTime then bigPT := VPT(i); end if;
      
  end loop;
return bigPT; 
End Function;

Function MemoryTaskGuarded(VMT: vectorMemoryTask_type) Return MemoryTask_type is 
Variable i:Integer;
Variable bigMT : MemoryTask_type;
Variable MTN : MemoryTask_type;
  Begin 
  bigMT  := VMT(VMT'LOW);
  
  for i in VMT'low + 1 to VMT'high loop
      MTN:= VMT(i);
      if VMT(i).eventTime > bigMT.eventTime then bigMT := VMT(i); end if;
      
  end loop;
Return bigMT; 
End Function;


Function BlockLockTaskGuarded(VET: vectorBlockLockTask_type) Return BlockLockTask_type is 
Variable i:Integer;
Variable bigET : BlockLockTask_type;
  Begin 
  bigET  := VET(VET'LOW);
  
  for i in VET'range loop
      if VET(i).eventTime > bigET.eventTime then bigET := VET(i); end if;
  end loop;
return bigET; 
End Function;

Function isWriteAccessAllowed(Signal isW: in Boolean; t: in time) Return Boolean is
  begin 
  if not(isW) then 
              PrintString("[" & time2str(t) & "] #Warning: Program,Erase,Write Status Register Operations are Inhibit");
              Return false;
         Else 
              Return true;
  end if;
End Function;

 Function isReadAccessAllowed(Signal isR: in Boolean; t: in time) Return Boolean is
  begin 
  if not(isR) then 
              PrintString("[" & time2str(t) & "] #Warning: Read Operations are Inhibit");
              Return false;
         Else 
              Return true;
  end if;
End Function;

      
--==============================================================================
--------------------------------------------------------------------------------
Signal Kernel_CUIcommandEvent : TimeEvent;
Signal Kernel_VerifyEvent     : EventTimeGuarded TimeEvent register := 0 ns;
Signal Kernel_CompleteEvent   : EventTimeGuarded TimeEvent register := 0 ns;
Signal Kernel_ErrorEvent      : Std_Logic;
Signal Kernel_CommandDecode   : CommandGuarded Command_type register;
Signal Kernel_Status          : Status_type;
Signal Kernel_Report          : ReportGuarded Report_type register;

-- ========================================================================== -- 
--------------------------------------------------------------------------------
Signal SRWD_flag              : Std_Logic := '0';
Signal WEL_flag               : Std_Logic := '0';
--------------------------------------------------------------------------------
-- Read Signal and variable
Signal  Read_Status             : ReadStatusGuarded ReadStatus_type register;
Signal  isCountDisable          : Boolean := false;
Signal  DataOutBuffer           : DataOutBufferGuarded DataOutBuffer_type register;
Signal  DataOutEventComplete    : TimeEvent;
Signal  Kernel_ReadComplete     : TimeEvent;
-- Program Signal and variable 
Signal  Program_task            : ProgramTaskGuarded ProgramTask_type register;
Signal  Program_Status          : Status_type;
------------------------------
-- Erase Signal and variable 
Signal  Erase_task              : EraseTaskGuarded EraseTask_type register;
Signal  Erase_Status            : Status_type;
-- Memory Signal
Signal  Memory_Status           : Status_type;
Signal  Memory_Task             : MemoryTaskGuarded MemoryTask_type register;
Signal  Kernel_MemComplete      : EventTimeGuarded TimeEvent register := 0 ns;
-- Status Register --
Signal  StatusRegister_status   : Status_type;
Signal  StatusRegister_task      : StatusRegisterTaskGuarded StatusRegisterTask_type register;
Signal  SR_reg : DataStatusReg_type:= (others => '0');
Signal  StatusRegister_report    : report_type;
-- Block Lock Signal
Signal  BlockLock_task          : BlockLockTaskGuarded BlockLockTask_type register;
Signal  Kernel_BlockLockComplete: EventTimeGuarded TimeEvent register := 0 ns;
Signal  BlockLock_Status        : Status_type;
-- Protect Signal
Signal  isWRITEprotect         : Boolean := false;
Signal  isWIPprotect           : Boolean := false;
Signal  isSRprotect            : Boolean := false;
-- Generic Variable --
Shared Variable Message       : Message_type := None_msg;
--------------------------------------------------------------------------------

-------------------------------
----- CUI DECODER Component --
-------------------------------

Component CUIdecoder_entity
  generic(
         Command     : CommandList_type;
         DataSeq     : DataLatch_type
        );
   port( 
         DataIn                 : in DataLatch_type;
         Kernel_CUIcommandEvent : in  TimeEvent;
         Kernel_VerifyEvent     : out TimeEvent;
         Kernel_CompleteEvent   : in  TimeEvent;
         Kernel_ErrorEvent      : in  Std_Logic;
         Kernel_CommandDecode   : out Command_type
        );
End Component;

-------------------------------
-- -- Manager Declaration -- --                      
-------------------------------

component Kernel_entity is 
  port( 
        Program_Status        : in Status_type;
        Erase_Status          : in Status_type;
        StatusRegister_Status : in Status_type;
        Kernel_Status         : out Status_type;
        Kernel_Report         : in Report_type;
        Kernel_VerifyEvent    : out TimeEvent;
        Kernel_ErrorEvent     : in ErrorEvent;
        Kernel_CommandDecode  : in command_type;
        Reset                 : in boolean
      );
end Component ;



Component StatusRegister_entity is 
  port ( 
        StatusRegister_status : out Status_type;
        SRWD_flag             : out Std_Logic;
        WEL_flag              : out Std_Logic;
        Kernel_status         : in Status_type;
        BlockLock_task        : out BlockLockTask_type;
        Kernel_BlockLockComplete : in TimeEvent;
        StatusRegister_task   : inout StatusRegisterTask_type;
        SR_reg                   : inout DataStatusReg_type ;
        StatusRegister_report : out report_type; 
        Reset                 : in Boolean
        );
End component;


Component Memory_entity is 
  Generic(
        MemoryFileName       : string
        );
  port (
        Memory_Status        : out Status_type;
        Memory_Task          : inout MemoryTask_type;
        Kernel_MemComplete   : out TimeEvent;
        Reset                : in Boolean
        );
End Component;



Component BlockLock_entity is 
   Port(
        BlockLock_task           : inout BlockLockTask_type;
        Kernel_BlockLockComplete : out TimeEvent;
        Reset                    : in Boolean
        );
End Component;

Component Program_entity is 
  port (
        Program_Status          : out Status_type;
        Program_Task            : in ProgramTask_type;
        BlockLock_task          : inout BlockLockTask_type;
        Kernel_BlockLockComplete: in TimeEvent;
        Memory_Task             : inout MemoryTask_type;
        Kernel_MemComplete      : in TimeEvent;
        StatusRegister_task     : out StatusRegisterTask_type;
        VppH                    : in Boolean;
        Reset                   : in Boolean
        );
End Component;
  
Component Erase_entity is 
  port (
        Erase_Status            : out Status_type;
        Erase_Task              : in EraseTask_type;
        BlockLock_task          : inout BlockLockTask_type;
        Kernel_BlockLockComplete: in TimeEvent;
        Memory_Task             : inout MemoryTask_type;
        Kernel_MemComplete      : in TimeEvent;
        StatusRegister_task     : out StatusRegisterTask_type;
        VppH                    : in Boolean;
        Reset                   : in Boolean
        );
End Component;
  
Component TimingCheck_entity is 
    Generic(
            TimingCheckOn : boolean
            );
    Port(		
            C    : in std_logic ;
            D    : in std_logic ;
            S    : in std_logic ; 
            W    : in std_logic ;
            HOLD : in std_logic 
            );
End Component;

  
Begin


PowerUp_process : process (Vcc)
Constant tPUW_mid_time : time := (tPUW_max - tPUW_min) / 2;
Variable Vcc_VwiLow_time : time := 0 ns ;
Variable Vcc_VwiHigh_time : time := 0 ns;
Variable t_time : time := 0 ns;
Variable Vcc_old : real := 0.0;
Begin
if (Vcc<Vwi_low) then 
                   Reset <= true;
                   ChipSelect_allowed <= false;
                   ReadAccess_allowed <= false; 
                   WriteAccess_allowed <= false; 
                   Vcc_VwiLow_time := now;
end if;                 
if (Vcc>=Vwi_low and Vcc<Vcc_low) then 
                   Reset <= false;
                   ChipSelect_allowed <= true;
                   ReadAccess_allowed <= false; 
                   WriteAccess_allowed <= false;
                   Vcc_VwiLow_time := now;
end if;
if (Vcc>Vcc_low) then
                   Reset <= false;
                   ChipSelect_allowed <= true;
                   ReadAccess_allowed <= true after tVSL;
                   if (now <= Vcc_VwiLow_time ) then t_time := 0 ns;
                                                else t_time := now - Vcc_VwiLow_time;
                   end if;
                   if t_time >= tPUW_mid_time then
                                        WriteAccess_allowed <= true;
                                 else 
                                        WriteAccess_allowed <= true after tPUW_mid_time - t_time;
                   end if;
                   
end if;
Vcc_old := Vcc;
end process PowerUp_process;

EhancedProgramVoltage_process : process(W_VPP) begin 
if (W_Vpp >= 0.0 and W_Vpp < Vcc_low) then
     PrintString("[" & time2str(now) & "]  -- W Signal: LOW --" );
     W<='0';
     VppH<=false;
   elsif (W_Vpp >= Vcc_low and W_Vpp <= Vcc_High) then
     PrintString("[" & time2str(now) & "]  -- W Signal: VCC range -> Normal PP/SE/BE/WRSR Operations are allowed --" );
     W<='1';
     VppH<=false;
   elsif (W_Vpp>=Vpp_Low and W_Vpp<=Vpp_High) then 
     PrintString("[" & time2str(now) & "]  -- W Signal: VPP range -> Fast PP/SE/BE/WRSR Operations are allowed --" );
     W<='1';
     VppH<=true;
   else 
     PrintString("[" & time2str(now) & "]  !Error: W out of range!" );
end if;

end process EhancedProgramVoltage_process;

----------------------------------------------------------------------------------------
-- This process checks select and deselect conditions. Some other conditions are tested:
-- prog cycle, deep power down mode and read electronic signature.
 pin_S: PROCESS
----------------------------------------------------------------------------------------
 BEGIN
 WAIT ON S;
 if not(ChipSelect_allowed) then 
              PrintString("[" & time2str(now) & "] #Warning: Chip Select are Inhibit; No Operation are allowed");
              select_ok<=false;
    elsif (s='0') then
        
        select_ok<=true;
        PrintString("[" & time2str(now) & "]  -- S Signal: LOW  -> The Chip is Selected --");
   else 

        select_ok<=false;
        PrintString("[" & time2str(now) & "]  -- S Signal: HIGH -> The Chip is NOT selected --");

end if;
END PROCESS pin_S;

-------------------------------------------------------------
-- This process generates the Hold condition when it is valid
 hold_com: PROCESS 
-------------------------------------------------------------
BEGIN
WAIT ON HOLD;
IF (HOLD = '0' AND S='0') THEN
	IF (C='0') THEN
		hold_cond <= true;

                PrintString("[" & time2str(now) & "]  -- Hold Signal -> Communication PAUSED --");
	ELSE WAIT ON C,hold;
		IF (C='0') THEN 
		hold_cond <= true;
                PrintString("[" & time2str(now) & "]  -- Hold Signal -> Communication PAUSED --");
		END IF;
	END IF;
ELSIF (HOLD = '1') THEN
	IF (C='0') THEN
		hold_cond <= false;	     
                PrintString("[" & time2str(now) & "]  -- Hold Signal -> Communication STARTS --");
	ELSE WAIT ON C,hold;
		IF (C='0') THEN
		   hold_cond <= false;	     
                   PrintString("[" & time2str(now) & "]  -- Hold Signal -> Communication STARTS --");
		END IF;
	END IF;
END IF;
END PROCESS hold_com ;

------------------------------------------------------------------------
-- This process inhibits the internal clock when hold condition is valid
Horloge: PROCESS
------------------------------------------------------------------------
Begin
   WAIT ON C;
   IF (NOT hold_cond) THEN
        C_int<=C;
   ELSIF (hold_cond) THEN
        C_int<='0';
   END IF;
END PROCESS Horloge;

------------------------------------------------------------
-- This process increments 2 counters:  one bit counter (cpt)
--					one byte counter (byte_cpt)
count_bit: PROCESS
------------------------------------------------------------
Variable count_enable: boolean := false;
Begin
Wait on C_int,raz; 

If ((raz or NOT select_ok) or isCountDisable) Then
	cpt <= 0;
	byte_cpt <= 0;
	count_enable := false;
Else
	IF (C_int = '1') THEN 
	-- count enable is an intermediate variable which allows cpt to be
	-- constant during a whole period
		count_enable := true;
	END IF;
	
	IF(count_enable AND C_int'event AND C_int = '0')THEN 
		cpt <= (cpt +1)  MOD 8;
	END IF;

	IF(C_int = '0' AND isDataIn)THEN 
		byte_cpt <= (byte_cpt+1);
	END IF;
END IF;
END PROCESS count_bit;


-----------------------------------------------------------------------
-- This process latches every byte of data received and returns isDataIn 
DataIn_process: PROCESS
-----------------------------------------------------------------------
VARIABLE data: std_logic_vector (7 downto 0):="00000000";
BEGIN
  Wait on C_int,Select_ok;
	IF (NOT select_ok) then 
		raz<=true;
		isDataIn<=false;
		DataIn<="00000000";
		data := "00000000";
	ELSIF (C_int'event and C_int='1') THEN
			raz<=false;
			IF (cpt=0) THEN
				DataIn<="00000000";
				isDataIn<=false;
			END IF;
			data(7-cpt):=D;
                IF (cpt=7) THEN 
				isDataIn<=true;
				DataIn<=data;
			End if;
	End if;
  END PROCESS DataIn_Process;

-----------------------------------------------------------------
-- This process inhibits data output when hold condition is valid
DataOutput_process: PROCESS
-----------------------------------------------------------------
  Begin
 -- Wait on hold_cond,DataOut,S,Reset;
  Wait on hold_cond,DataOut,S,Reset, C_int ;
  if (reset'event) and reset then 
                       Q<='Z'; 
  elsif (S'event and S='1') then 
                Q<='Z' after tSHQZ;
  elsif (hold_cond'event) THEN
  	IF (hold_cond) THEN
  		Q<='Z' after tHLQZ;
  	else 
  		Q<=DataOut after tHHQX;
  	END IF;
--  elsif (DataOut'event and not(hold_cond)) then
  elsif ((DataOut'event and not(hold_cond)) or (isCountDisable and not(hold_cond))) then
  	Q<=DataOut;
  else 
        Q<='Z';
  end if;
  
  End Process DataOutput_process;
 
DataOut_buffer: Process
Variable i: Integer;
begin 
   wait until DataOutBuffer'event or reset'event;
  If (reset'event and reset) then 
        DataOut <= 'Z' ;
  Elsif  (DataOutBuffer'event and S='0') then 
        isCountDisable<=true;
        for i in DataOutBuffer.data'range loop
              wait until (C_int'event and C_int='0') or (S'event and S='1');
              exit when  (S'event and S='1');
        DataOut <= DataOutBuffer.data(i);
        End Loop;
        DataOutEventComplete <= now;
        isCountDisable<=false;
   End if;
 End Process DataOut_buffer;
 
--------------------------------------------------------------------------------
-- Process for protect 
StatusRegisterProtect_process : process(SRWD_flag,W) begin 
        if (W='0' and SRWD_flag='1') then isSRprotect<=true;
                                     else isSRprotect<=false;
        end if;
end process StatusRegisterProtect_process; 
                              
isWRITEprotect <= false when WEL_flag = '1' else true; 

--------------------------------------------------------------------------------

-- Command Decoder 
  WriteEnable_decode  :  CUIdecoder_entity
      generic map (WriteEnable_cmd, WriteEnable_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  WriteDisable_decode  :  CUIdecoder_entity
      generic map (WriteDisable_cmd, WriteDisable_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  ReadIdentification_decode:  CUIdecoder_entity
      generic map (ReadIdentification_cmd, ReadIdentification_cmd_SeqData)
      port map    (DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  ReadStatusRegister_decode  :  CUIdecoder_entity
      generic map (ReadStatusRegister_cmd, ReadStatusRegister_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  WriteStatusRegister_decode  :  CUIdecoder_entity
      generic map (WriteStatusRegister_cmd, WriteStatusRegister_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  ReadDataByte_decode  :  CUIdecoder_entity
      generic map (ReadDataBytes_cmd, ReadDataBytes_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  ReadDataByte_HS_decode  :  CUIdecoder_entity
      generic map (ReadDataBytes_HS_cmd, ReadDataBytes_HS_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  PageProgram_decode  :  CUIdecoder_entity
      generic map (PageProgram_cmd, PageProgram_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  SectorErase_decode  :  CUIdecoder_entity
      generic map (SectorErase_cmd, SectorErase_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);

  BulkErase_decode  :  CUIdecoder_entity
      generic map (BulkErase_cmd, BulkErase_cmd_SeqData)
      port map (  DataIn, Kernel_CUIcommandEvent, Kernel_VerifyEvent, Kernel_CompleteEvent, Kernel_ErrorEvent, Kernel_CommandDecode);


-- Manager Instance --
  BlockLock_manager : BlockLock_entity 
      port map(BlockLock_task, Kernel_BlockLockComplete, Reset);

  StatusRegister_manager : StatusRegister_entity 
      port map ( StatusRegister_status, SRWD_flag, WEL_flag, Kernel_status, BlockLock_task, Kernel_BlockLockComplete, StatusRegister_task, SR_reg, StatusRegister_report, Reset);

  Kernel_manager : Kernel_entity 
      port map ( Program_Status, Erase_Status, StatusRegister_status, Kernel_Status, Kernel_Report, Kernel_VerifyEvent, Kernel_ErrorEvent, Kernel_CommandDecode, Reset);

  Memory_manager : Memory_entity  
      generic map ( MemoryFileName )
      port map( Memory_Status, Memory_Task, Kernel_MemComplete, Reset);

  Program_Manager : Program_entity 
      port map ( Program_Status, Program_Task, BlockLock_task, Kernel_BlockLockComplete, Memory_Task, Kernel_MemComplete, StatusRegister_task, VppH, Reset);
  
  Erase_Manager : Erase_entity 
      port map ( Erase_Status, Erase_Task, BlockLock_task, Kernel_BlockLockComplete, Memory_Task, Kernel_MemComplete, StatusRegister_task, VppH, Reset);
  

  TimingCheck_manager : TimingCheck_entity
        generic map(TimingCheckOn)
        port map(C, D, S, W, HOLD);

-- Check Command --
  CheckCommand_process :  Process Begin
                          Wait Until (isDataIn'event and isDataIn=true);
                                Kernel_CUIcommandEvent <= now;
                                   Wait Until Kernel_CompleteEvent'event or Kernel_ErrorEvent'event;
  End Process CheckCommand_process;


  Verify_Error : Process begin
                 Wait Until Kernel_CUIcommandEvent'event;
                      Kernel_ErrorEvent <= '1' after 1 fs; --Error if not verify event at same delta time
                      Wait Until Kernel_VerifyEvent'event;
                         Kernel_ErrorEvent <= 'X'; -- Error Suppressed
                        
  End Process Verify_Error;

  ErrorReport : Process(Kernel_ErrorEvent)
               Begin 
               if (Kernel_ErrorEvent = '1') Then 
                        PrintString("[" & time2str(now) & "]  !ERROR: Command not Recognized" );
                        Kernel_VerifyEvent <= now;
               end if;
  End Process ErrorReport;

  Read_Manager: Process 
                    Constant HexStr_dim : Integer :=  (AddrBus_Dim / 4);
                    Variable Address: AddrBus_type;
                    Variable AddressMem: AddrMem_type;
                    Variable Address_STDlogic : Std_Logic_vector(AddrBus_dim-1 downto 0);
                    Variable Address_STRlogic : string(1 to AddrBus_dim);
                    Variable AddrHEXstr : string(1 to HexStr_dim) := (Others => '0');
                    Variable goRead : Boolean := true;
                    Variable MSB,LSB: Integer;
                begin
                wait until Read_status'event or reset'event;
                if (reset'event and reset) then
                     DataOutBuffer.data <= (Others => 'Z');
                     DataOutBuffer.eventTime <= now;
                elsif (Read_Status'event) then
                     Case (Read_status.mode) is
                       when ReadArray_status | FastReadArray_status =>
                                  -- Latch Address
                                   for i in 3 downto 1 loop
                                      Wait Until (isDataIn'event and isDataIn=true);
                                      MSB:= (i * 8) - 1; LSB := MSB - 7;
                                      Address(MSB downto LSB) := DataIn;
                                       
                                   end loop;
                                   AddressMem := slv2int(Address);
                                   -- Fast Read Array -- Waiting for Dummy Byte --
                                   if (Read_status.mode = FastReadArray_status)  then 
                                             Wait Until (isDataIn'event and isDataIn=true);
                                   end if;

                                   Memory_task.task <= get;
                                   Memory_task.address <= AddressMem; 
                                   Memory_task.data <= (Others => '0' ); --NP;
                                   Memory_task.addressEnd <= 0; --NP
                                   Memory_task.isAll1 <= false; --NP
                                   Memory_task.eventTime <= now;    
                                      wait until Kernel_MemComplete'event;
                                        -----------------------------------------------------------------------
                                         Address_stdlogic :=  conv_std_logic_vector(AddressMem, AddrMem_dim);
                                         Address_STRlogic := Slv2Str(Address_stdlogic);
                                         AddrHEXstr    := Str2Hex(Address_STRlogic);
                                         PrintString("[" & time2str(now) & "]  Data Read result: Memory[" & AddrHEXstr & "] = " & Str2Hex(Slv2Str(to_stdlogicvector(Memory_task.data))) );
                                        -----------------------------------------------------------------------
                                   DataOutBuffer.data <= to_stdlogicvector(Memory_task.data);
                                   DataOutBuffer.eventTime <= now;
                                   goRead:=true;
                                   ReadLoop : 
                                   while (goRead=true) loop
                                         wait until ((S'event and S='1') or DataOutEventComplete'event);
                                      if (S'event and S='1') then goRead:=false;
                                          else 
                                          if (AddressMem + 1 = Memory_dim ) then AddressMem := 0;
                                                                            else AddressMem := AddressMem  + 1;
                                                                            end if;
                                          Memory_task.task <= get;
                                          Memory_task.address <= AddressMem;
                                          Memory_task.data <= (Others => '0' ); -- NP
                                          Memory_task.addressEnd <= 0; --NP
                                          Memory_task.isAll1 <= false; --NP
                                          Memory_task.eventTime <= now;    
                                              wait until Kernel_MemComplete'event;
               
                                          DataOutBuffer.data <= to_stdlogicvector(Memory_task.data);
                                          DataOutBuffer.eventTime <= now;
                                        -----------------------------------------------------------------------
                                         Address_stdlogic :=  conv_std_logic_vector(AddressMem, AddrMem_dim);
                                         Address_STRlogic := Slv2Str(Address_stdlogic);
                                         AddrHEXstr    := Str2Hex(Address_STRlogic);
                                         PrintString("[" & time2str(now) & "]  Data Read result: Memory[" & AddrHEXstr & "] = " & Str2Hex(Slv2Str(to_stdlogicvector(Memory_task.data))) );
                                        -----------------------------------------------------------------------

                                      end if;
                                   end loop ReadLoop;
                                      
                       when ReadStatusReg_status => 

                             StatusRegister_task.task <= Read;
                             StatusRegister_task.SR <= SR_reg ; -- NP
                             StatusRegister_task.eventTime <= now;
                                   wait until StatusRegister_report'event;
                             DataOutBuffer.data <=  to_stdlogicvector(StatusRegister_task.SR);
                             DataOutBuffer.eventTime <= now;
                                 PrintString("[" & time2str(now) & "]  Data Read result: SR = " & (slv2str(to_stdlogicvector(StatusRegister_task.SR))) & "; hex = " & Str2Hex(Slv2Str(to_stdlogicvector(StatusRegister_task.SR))) );
                             goRead:=true;
                             SRReadLoop : 
                             while (goRead=true) loop
                                   wait until ((S'event and S='1') or DataOutEventComplete'event);
                                   if (S'event and S='1') then goRead:=false;
                                      else 
                                      StatusRegister_task.task <= Read;
                                      StatusRegister_task.SR <= SR_reg ;
                                      StatusRegister_task.eventTime <= now;
                                           wait until StatusRegister_report'event;
                                      DataOutBuffer.data <=  to_stdlogicvector(StatusRegister_task.SR);
                                      DataOutBuffer.eventTime <= now;
                                          PrintString("[" & time2str(now) & "]  Data Read result: SR = " & (slv2str(to_stdlogicvector(StatusRegister_task.SR))) & "; hex = " & Str2Hex(Slv2Str(to_stdlogicvector(StatusRegister_task.SR))) );
                                   end if;
                             end loop SRReadLoop;
 
                       when ReadID_status => 

                             DataOutBuffer.data <= ManufCode_id;
                             DataOutBuffer.eventTime <= now;
                             PrintString("[" & time2str(now) & "]  Electronic Signature: Manufacturer Code = " & Str2Hex(Slv2Str(ManufCode_id)) & ";");

                                wait until DataOutEventComplete'event;

                             DataOutBuffer.data <= MemoryType_id;
                             DataOutBuffer.eventTime <= now;
                             PrintString("[" & time2str(now) & "]  Electronic Signature: Memory Type = " & Str2Hex(Slv2Str(MemoryType_id)) & ";");
                                wait until DataOutEventComplete'event;

                             DataOutBuffer.data <= MemoryCapacity_id;
                             DataOutBuffer.eventTime <= now;
                             PrintString("[" & time2str(now) & "]  Electronic Signature: Memory Capacity = " & Str2Hex(Slv2Str(MemoryCapacity_id)) & ";");
                                
                       when others =>

                     End Case;

              Kernel_ReadComplete <= now;
              end if;
  end Process Read_Manager;

-- Command Execute -- 

 WriteEnable_command : Process begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd = WriteEnable_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Write Enable");

                    If isWriteAccessAllowed(WriteAccess_allowed,now) then 

                       StatusRegister_task.task <= set_WEL;
                       StatusRegister_task.SR <= SR_reg ;
                       StatusRegister_task.eventTime <= now;

                         wait until StatusRegister_report'event;

                     Kernel_Report.message <= StatusRegister_Report.message;
                     Kernel_Report.eventTime <= now;
                   end if;                    
                Kernel_CompleteEvent <= now;        
                End If;
 End process WriteEnable_command; 


 WriteDisable_command : Process begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd = WriteDisable_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Write Disable");

                    If isWriteAccessAllowed(WriteAccess_allowed,now) then 

                       StatusRegister_task.task <= reset_WEL;
                       StatusRegister_task.SR <= SR_reg ;
                       StatusRegister_task.eventTime <= now;

                        wait until StatusRegister_report'event;

                       Kernel_Report.message <= StatusRegister_Report.message;
                       Kernel_Report.eventTime <= now;
                    End if;

                Kernel_CompleteEvent <= now;        
                End If;
  End process WriteDisable_command; 

  ReadIdentification_command : Process begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd  = ReadIdentification_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Read Identification");


                    If isReadAccessAllowed(ReadAccess_allowed,now) then 

                        Read_status.mode <= ReadID_status;
                        Read_status.eventTime <= now;

                        Wait until Kernel_ReadComplete'event;

                    end if;    

                Kernel_CompleteEvent <= now;        
                End If;
End process ReadIdentification_command; 

  ReadStatusRegister_command : Process begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd = ReadStatusRegister_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Read Status Register");
                        
                    If isReadAccessAllowed(ReadAccess_allowed,now) then 
                        Read_status.mode <= ReadStatusReg_status;
                        Read_status.eventTime <= now;

                        Wait until Kernel_ReadComplete'event;

                    End if;
                 Kernel_CompleteEvent <= now;        
                End If;
End process ReadStatusRegister_command; 

  WriteStatusRegister_command : Process begin 
                Wait Until Kernel_CommandDecode'event;   

                If (Kernel_CommandDecode.cmd = WriteStatusRegister_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Write Status Register");

                 If isWriteAccessAllowed(WriteAccess_allowed,now) then 
                    
                   If (isWRITEprotect) then Message := NoWrite_msg;
                     elsif (isSRprotect) then Message := NoWRSR_msg;
                     elsif (Kernel_Status.busy) then Message := WIP_msg;
                     else  
                     
                          Wait Until (isDataIn'event and isDataIn=true);

                     StatusRegister_task.task <= Write;
                     StatusRegister_task.SR <= to_bitvector(DataIn);
                     StatusRegister_task.eventTime <= now;

                          wait until StatusRegister_report'event;

                     Message := StatusRegister_Report.message;
 
                     End If;

                     Kernel_Report.message <= Message;
                     Kernel_Report.eventTime <= now;
                 end if;
                 Kernel_CompleteEvent <= now;        
                End If;

End process WriteStatusRegister_command; 

ReadDataBytes_command : Process begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd = ReadDataBytes_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Read Data Bytes");

                    If isReadAccessAllowed(ReadAccess_allowed,now) then 
                        Read_status.mode <= ReadArray_status;
                        Read_status.eventTime <= now;
                        
                        Wait until Kernel_ReadComplete'event;

                    end if;

                Kernel_CompleteEvent <= now;        
                End If;
End process ReadDataBytes_command; 

FastReadDataBytes_command : Process begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd = ReadDataBytes_HS_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Fast Read Data Bytes");

                    If isReadAccessAllowed(ReadAccess_allowed,now) then 

                        Read_status.mode <= FastReadArray_status;
                        Read_status.eventTime <= now;
 
                        Wait until Kernel_ReadComplete'event;

                    end if;

                Kernel_CompleteEvent <= now;        
                End If;
End process FastReadDataBytes_command; 

PageProgram_command : Process 
                    Constant HexStr_dim : Integer :=  (AddrBus_Dim / 4);
                   Variable MSB,LSB: Integer;
                   Variable goProgram : Boolean:=false;
                   Variable goReset   : Boolean:=false;
                   Variable Address   : AddrBus_type;
                   Variable AddressMem: AddrMem_type;
                   Variable Address_STRlogic : string(1 to AddrBus_dim);
                   Variable AddrHEXstr : string(1 to HexStr_dim) := (Others => '0');

                begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd = PageProgram_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Page Program");
                Message:=None_msg;
                If isWriteAccessAllowed(WriteAccess_allowed,now) then 
                        If (isWRITEprotect) then Message := NoWrite_msg;
                           elsif (Kernel_Status.busy) then Message := WIP_msg;
                              else  
                                   -- Latch Address
                                   for i in 3 downto 1 loop
                                      Wait Until (isDataIn'event and isDataIn=true);
                                      MSB:= (i * 8) - 1; LSB := MSB - 7;
                                      Address(MSB downto LSB) := DataIn;
                                       
                                   end loop; 
                                         Address_STRlogic := Slv2Str(Address);
                                         AddrHEXstr    := Str2Hex(Address_STRlogic);
                                         PrintString("[" & time2str(now) & "]  Address Latched: " & AddrHEXstr);

                                   Memory_task.task <= InitPage;
                                   Memory_task.address <= slv2int(Address);
                                   Memory_task.data <= (Others => '0' ); -- NP
                                   Memory_task.addressEnd <= 0; --NP
                                   Memory_task.isAll1 <= false; --NP
                                   Memory_task.eventTime <= now;    
                                      wait until Kernel_MemComplete'event;
                                   goProgram:=false;goReset := false;
                                   ProgramLoop : while (goProgram=false and goReset=false) loop
                                        Wait Until ((isDataIn'event and isDataIn=true) or (S'event and S='1') ) or (Reset'event and Reset);
                                        if (S'event and S='1') then goProgram:=true;
                                        elsif (Reset'event and Reset) then goReset:=true;
                                           else 
                                                
                                                Memory_task.task <= addByteInPage;
                                                Memory_task.data <= to_bitvector(DataIn);
                                                Memory_task.eventTime <= now;    
                                                Memory_task.address <= 0;  --NP
                                                Memory_task.addressEnd <= 0; --NP
                                                Memory_task.isAll1 <= false; --NP
                                                wait until Kernel_MemComplete'event;
                                                PrintString("[" & time2str(now) & "]  Data Input: " & Str2Hex(Slv2Str(DataIn)));
                                                
                                        end if;
                                   end loop ProgramLoop;
                                   if goReset=true then Message := CmdAbtByRst_msg;
                                      else 
                                           Program_Task.task <= Program;
                                           Program_task.address <= slv2int(Address);-- used only to check if the block is locked
                                           Program_task.data<= (Others => '0' ); --NP
                                           Program_task.status<= NoError_msg; --NP
                                           Program_task.eventTime <= now;    

                                              wait until Program_Status.message'event;
                                           Message := Program_Status.message;
                                   end if;
                        end if;
                   Kernel_Report.message <= Message;
                   Kernel_Report.eventTime <= now;
                 end if;  
                 Kernel_CompleteEvent <= now;        
                End If;
End process PageProgram_command;  

SectorErase_command : Process 
                   Variable Address   : AddrBus_type;
                   Variable MSB,LSB: Integer;
                begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd = SectorErase_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Sector Erase");

                 If isWriteAccessAllowed(WriteAccess_allowed,now) then 

                   If (isWRITEprotect) then Message := NoWrite_msg;
                      elsif (Kernel_Status.busy) then Message := WIP_msg;
                      else  
                       -- Latch Address
                       for i in 3 downto 1 loop
                            Wait Until (isDataIn'event and isDataIn=true);
                            MSB:= (i * 8) - 1; LSB := MSB - 7;
                            Address(MSB downto LSB) := DataIn;
                       end loop;
                       Wait Until (S'event and S='1') or (Reset'event and Reset);
                       if (Reset'event and Reset) then Message := CmdAbtByRst_msg;
                            else 
                            Erase_Task.task <= BlockErase;
                            Erase_task.address <= slv2int(Address);
                            Erase_task.status<= NoError_msg; --NP
                            Erase_task.eventTime <= now;    

                                 wait until Erase_Status.message'event;
                                   
                            Message := Erase_Status.message;
                       end if;       
                   end if;  
                   Kernel_Report.message <= Message;
                   Kernel_Report.eventTime <= now;
                 end if;
                Kernel_CompleteEvent <= now;        
                End If;
End process SectorErase_command; 

BulkErase_command : Process 
                   Variable BP : bit_vector(2 downto 0);
                   Variable Address   : AddrBus_type;
                   Variable MSB,LSB: Integer;
                begin 
                Wait Until Kernel_CommandDecode'event;   
                If (Kernel_CommandDecode.cmd = BulkErase_cmd) then
                        PrintString("[" & time2str(now) & "]  Command Issued: Bulk Erase");

                 If isWriteAccessAllowed(WriteAccess_allowed,now) then 

                     StatusRegister_task.task <= ReadProtectAreaSizes;
                     StatusRegister_task.SR <= SR_reg ;
                     StatusRegister_task.eventTime <= now;

                          wait until StatusRegister_report'event;

                     BP := StatusRegister_task.SR(2 downto 0); 

                   If (isWRITEprotect) then Message := NoWrite_msg;
                      elsif (Kernel_Status.busy) then Message := WIP_msg;
                      elsif ( BP/="000" )  then Message := BkEraseProt_msg;
                      else  
                       -- Latch Address
                            Erase_Task.task <= BulkErase;
                            Erase_task.address <= 0;         --NP
                            Erase_task.status<= NoError_msg; --NP
                            Erase_task.eventTime <= now;    

                                 wait until Erase_Status.message'event;
                                   
                            Message := Erase_Status.message;
                  end if;  
                  Kernel_Report.message <= Message;
                  Kernel_Report.eventTime <= now;
                end if;
                Kernel_CompleteEvent <= now;        
                End If;
End process BulkErase_command; 

End behavior ;

