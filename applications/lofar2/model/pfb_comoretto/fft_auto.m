%% fft_auto.m
% spectrum of (complex) array x, with frequency resolution n
% Convolves input data with cosine tapering, FFT, square module, and 
% accumulation in time
%
%% Input
% x  column vector (complex) with data samples
% n  Number of frequency bins (2-sided)
%
%% Output
%
% auto: real column vector with power spectrum. 
%       FFT order (positive then negative, DC at index 1)
%
%% Code
function auto = fft_auto(x,n)
  auto  = zeros(n,1);
  taper = cos(((0:(n-1))'-n/2)*pi/(n));
  start = 1;
  nspectra = 0;
  npoints = size(x,1);
  while (start + n) <= npoints
    spt1 = fft(x(start:(start+n-1)).* taper,n);
    auto = auto + real(spt1(1:n).*conj(spt1(1:n)));
    start = start + n/2;   % not n2, segments overlap
    nspectra = nspectra + 1;
  end
  norm = 1./(nspectra*n);
  auto = auto*norm;
end
