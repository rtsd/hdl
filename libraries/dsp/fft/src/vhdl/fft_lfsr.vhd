-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: Scramble quantization noise crosstalk between two real inputs
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark:
-- . Copy from applications/lofar1/RSP/pft2/src/vhdl/pft_lfsr.vhd
-- . The g_seed must be > 0 for LFSR period is 2**c_fft_lfsr_len - 1. Value
--   g_seed = 0 causes the LFSR to remain stuck at 0.

library IEEE;
use IEEE.std_logic_1164.all;
use work.fft_pkg.all;

entity fft_lfsr is
  generic (
    g_seed1 : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := c_fft_switch_seed1;
    g_seed2 : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := c_fft_switch_seed2
  );
  port (
    in_en          : in  std_logic;
    out_bit1       : out std_logic;
    out_bit2       : out std_logic;
    clk            : in  std_logic;
    rst            : in  std_logic
  );
end fft_lfsr;

architecture rtl of fft_lfsr is
  -- uses preferred pair of pritive trinomials
  -- x^41 + x^20 + 1  and x^41 + x^3 + 1
  -- see XAPP217

  constant c_len : natural := c_fft_lfsr_len;  -- = 41, same for both trinomials
  constant c1    : natural := 20;
  constant c2    : natural := 3;

  signal s1      : std_logic_vector(c_len - 1 downto 0);
  signal nxt_s1  : std_logic_vector(c_len - 1 downto 0);

  signal s2      : std_logic_vector(c_len - 1 downto 0);
  signal nxt_s2  : std_logic_vector(c_len - 1 downto 0);
begin
  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      s1 <= g_seed1;
      s2 <= g_seed2;
    elsif rising_edge(clk) then
      s1 <= nxt_s1;
      s2 <= nxt_s2;
    end if;
  end process;

  out_bit1 <= s1(s1'high);
  out_bit2 <= s2(s2'high);

  p_seed : process(in_en, s1, s2)
  begin
    nxt_s1 <= s1;
    nxt_s2 <= s2;
    if in_en = '1' then
      -- shift
      nxt_s1(c_len - 1 downto 1) <= s1(c_len - 2 downto 0);
      nxt_s2(c_len - 1 downto 1) <= s2(c_len - 2 downto 0);

      -- feedback 1
      nxt_s1(0) <= s1(c_len - 1);
      nxt_s2(0) <= s2(c_len - 1);

      -- feedback 2
      nxt_s1(c1) <= s1(c_len - 1) xor s1(c1 - 1);
      nxt_s2(c2) <= s2(c_len - 1) xor s2(c2 - 1);
    end if;
  end process;
end rtl;
