-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Self-checking testbench for simulating lofar2_unb2b_ring using BG data.
--
-- Description:
-- See, https://support.astron.nl/confluence/x/jyu7Ag
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   > run -a
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2b_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, tech_pll_lib, tr_10GbE_lib, ring_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;
use ring_lib.ring_pkg.all;
use work.lofar2_unb2b_ring_pkg.all;

entity tb_lofar2_unb2b_ring is
  generic (
    g_multi_tb            : boolean              := false;
    g_unb_nr              : natural              := 0;
    g_design_name         : string               := "lofar2_unb2b_ring_full";
    g_nof_rn              : natural              := 3;
    g_nof_block_per_sync  : natural              := 3;
    g_access_scheme       : integer range 1 to 3 := 1
  );
  port (
    tb_end : out std_logic := '0'  -- For multi tb
  );
end tb_lofar2_unb2b_ring;

architecture tb of tb_lofar2_unb2b_ring is
  constant c_sim             : boolean := true;
  constant c_node_nr         : natural := 0;
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2b_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_sa_clk_period       : time := tech_pll_clk_644_period;  -- 644MHz
  constant c_pps_period          : natural := 1000;

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C
  constant c_cable_delay         : time := 12 ns;

  constant c_revision_select     : t_lofar2_unb2b_ring_config := func_sel_revision_rec(g_design_name);
  constant c_nof_lanes           : natural := c_revision_select.N_ring_lanes;

  constant c_block_period        : natural := 1024;
  constant c_blocksize           : natural := c_sdp_V_ring_pkt_len_max - c_ring_dp_hdr_field_size;
  constant c_gapsize             : natural := c_block_period - c_blocksize;
  constant c_sync_timeout        : natural := c_block_period * g_nof_block_per_sync + 10;  -- +10 for extra slack
  constant c_exp_bsn_at_sync     : natural := g_nof_block_per_sync;
  constant c_exp_nof_sop         : natural := g_nof_block_per_sync;
  constant c_exp_nof_valid       : natural := g_nof_block_per_sync * c_blocksize;

  -- MM
  constant c_mm_file_reg_ring_info               : string := mmf_unb_file_prefix(g_unb_nr, c_node_nr) & "REG_RING_INFO";
  constant c_mm_file_reg_ring_lane_info          : string := mmf_unb_file_prefix(g_unb_nr, c_node_nr) & "REG_RING_LANE_INFO";
  constant c_mm_file_reg_diag_bg                 : string := mmf_unb_file_prefix(g_unb_nr, c_node_nr) & "REG_DIAG_BG";
  constant c_mm_file_ram_diag_bg                 : string := mmf_unb_file_prefix(g_unb_nr, c_node_nr) & "RAM_DIAG_BG";
  constant c_mm_file_reg_dp_xonoff_lane          : string := mmf_unb_file_prefix(g_unb_nr, c_node_nr) & "REG_DP_XONOFF_LANE";
  constant c_mm_file_reg_dp_xonoff_local         : string := mmf_unb_file_prefix(g_unb_nr, c_node_nr) & "REG_DP_XONOFF_LOCAL";
  constant c_mm_file_reg_bsn_monitor_v2_ring_rx  : string := mmf_unb_file_prefix(g_unb_nr, c_node_nr) & "REG_BSN_MONITOR_V2_RING_RX";
  constant c_mm_file_reg_bsn_monitor_v2_ring_tx  : string := mmf_unb_file_prefix(g_unb_nr, c_node_nr) & "REG_BSN_MONITOR_V2_RING_TX";

  -- Tb
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal i_tb_end            : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0);

  signal i_QSFP_0_TX         : t_unb2b_board_qsfp_bus_2arr(g_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_QSFP_0_RX         : t_unb2b_board_qsfp_bus_2arr(g_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_RING_0_TX         : t_unb2b_board_qsfp_bus_2arr(g_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_RING_0_RX         : t_unb2b_board_qsfp_bus_2arr(g_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_RING_1_TX         : t_unb2b_board_qsfp_bus_2arr(g_nof_rn - 1 downto 0) := (others => (others => '0'));
  signal i_RING_1_RX         : t_unb2b_board_qsfp_bus_2arr(g_nof_rn - 1 downto 0) := (others => (others => '0'));

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal ext_pps             : std_logic := '0';
  signal pps_rst             : std_logic := '1';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal pmbus_scl           : std_logic;
  signal pmbus_sda           : std_logic;

  signal SA_CLK              : std_logic := '1';
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  tb_clk <= not tb_clk or i_tb_end after c_tb_clk_period / 2;  -- Testbench MM clock

  ext_clk <= not ext_clk or i_tb_end after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk or i_tb_end after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  SA_CLK <= not SA_CLK or i_tb_end after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)

  pps_rst <= '0' after c_ext_clk_period * 2;

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(5, c_pps_period, '1', pps_rst, ext_clk, pps);
  ext_pps <= pps;

  ------------------------------------------------------------------------------
  -- DUTs
  ------------------------------------------------------------------------------
  gen_dut : for RN in 0 to g_nof_rn - 1 generate
    u_lofar_unb2b_ring : entity work.lofar2_unb2b_ring
    generic map (
      g_design_name            => g_design_name,
      g_design_note            => "",
      g_sim                    => c_sim,
      g_sim_unb_nr             => g_unb_nr + (RN / c_quad),
      g_sim_node_nr            => RN mod c_quad,
      g_sim_sync_timeout       => c_sync_timeout
    )
    port map (
      -- GENERAL
      CLK          => ext_clk,
      PPS          => pps,
      WDI          => WDI,
      INTA         => INTA,
      INTB         => INTB,

      -- Others
      VERSION      => c_version,
      ID           => ( TO_UVEC(RN / c_quad, c_unb2b_board_nof_uniboard_w) & TO_UVEC(RN mod c_quad, c_unb2b_board_nof_chip_w) ),
      TESTIO       => open,

      -- I2C Interface to Sensors
      SENS_SC      => sens_scl,
      SENS_SD      => sens_sda,

      PMBUS_SC     => pmbus_scl,
      PMBUS_SD     => pmbus_sda,
      PMBUS_ALERT  => open,

      -- 1GbE Control Interface
      ETH_CLK      => eth_clk,
      ETH_SGIN     => eth_rxp,
      ETH_SGOUT    => eth_txp,

      -- Transceiver clocks
      SA_CLK       => SA_CLK,
      -- front transceivers
      QSFP_0_RX    => i_QSFP_0_RX(RN),
      QSFP_0_TX    => i_QSFP_0_TX(RN),

      -- ring transceivers
      RING_0_RX    => i_RING_0_RX(RN),
      RING_0_TX    => i_RING_0_TX(RN),
      RING_1_RX    => i_RING_1_RX(RN),
      RING_1_TX    => i_RING_1_TX(RN),
      -- LEDs
      QSFP_LED     => open

    );
  end generate;

  -- Ring connections
  gen_ring : for I in 0 to g_nof_rn - 2 generate
    -- Connect consecutive nodes with RING interfaces (PCB)
    i_RING_0_RX(I + 1) <= i_RING_1_TX(I);
    i_RING_1_RX(I)   <= i_RING_0_TX(I + 1);
  end generate;

  -- Connect first and last nodes with QSFP interface.
  i_QSFP_0_RX(0)          <= i_QSFP_0_TX(g_nof_rn - 1);
  i_QSFP_0_RX(g_nof_rn - 1) <= i_QSFP_0_TX(0);

  ------------------------------------------------------------------------------
  -- MM peripeheral accesses via file IO
  ------------------------------------------------------------------------------
  p_mm_stimuli : process
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    proc_common_wait_until_hi_lo(ext_clk, ext_pps);

    -- Write ring configuration to all nodes.
    for RN in 0 to g_nof_rn - 1 loop
      mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_RING_INFO", 2, g_nof_rn, tb_clk);  -- N_rn
      mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_RING_INFO", 3, 0,        tb_clk);  -- O_rn
    end loop;

    -- Start node specific settings
    mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr, 0) & "REG_RING_INFO", 0, 1, tb_clk);  -- use_ring_to_previous_rn = 1
    mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr, 0) & "REG_RING_INFO", 1, 0, tb_clk);  -- use_ring_to_next_rn = 0

    -- End node specific settings
    mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + ((g_nof_rn - 1) / c_quad), (g_nof_rn - 1) mod c_quad) & "REG_RING_INFO", 0, 0, tb_clk);  -- use_ring_to_previous_rn = 0
    mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + ((g_nof_rn - 1) / c_quad), (g_nof_rn - 1) mod c_quad) & "REG_RING_INFO", 1, 1, tb_clk);  -- use_ring_to_next_rn = 1

    ----------------------------------------------------------------------------
    -- Access scheme 1. A source RN creates the packets and sends them along the ring.
    ----------------------------------------------------------------------------
    if g_access_scheme = 1 then
      for I in 0 to c_nof_lanes - 1 loop
        -- Select local input (= 1) on  start node on all lanes.
        mmf_mm_bus_wr(c_mm_file_reg_dp_xonoff_lane,  I * 2, 0, tb_clk);  -- Disable input from lane
        mmf_mm_bus_wr(c_mm_file_reg_dp_xonoff_local, I * 2, 1, tb_clk);  -- Enable local input

        -- Set transport_nof_hops to N_rn on start node for a full transfer around the ring.
        mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr, 0) & "REG_RING_LANE_INFO", I * 2 + 1, g_nof_rn, tb_clk);
      end loop;

    ----------------------------------------------------------------------------
    -- Access scheme 2, 3. Each RN creates packets and sends them along the ring.
    ----------------------------------------------------------------------------
    else
      for RN in 0 to g_nof_rn - 1 loop
        for I in 0 to c_nof_lanes - 1 loop
          -- Select both local and remote input on all nodes on all lanes.
          mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_DP_XONOFF_LANE",  I * 2, 1, tb_clk);  -- Enable input from lane
          mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_DP_XONOFF_LOCAL", I * 2, 1, tb_clk);  -- Enable local input

          -- Set transport_nof_hops to N_rn-1 on all nodes.
          mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_RING_LANE_INFO", I * 2 + 1, g_nof_rn - 1, tb_clk);
        end loop;
      end loop;
    end if;
    ----------------------------------------------------------------------------
    -- Enable BG on all nodes (for bs_sosi)
    ----------------------------------------------------------------------------
    for RN in 0 to g_nof_rn - 1 loop
      mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_DIAG_BG", 1,          c_blocksize, tb_clk);  -- samples per packet
      mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_DIAG_BG", 2, g_nof_block_per_sync, tb_clk);  -- blocks per sync
      mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_DIAG_BG", 3,            c_gapsize, tb_clk);  -- gapsize
      mmf_mm_bus_wr(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_DIAG_BG", 0,                    3, tb_clk);  -- enable at sync
    end loop;
    ----------------------------------------------------------------------------
    -- Verify Access scheme 1 by reading rx / tx monitors on source RN
    ----------------------------------------------------------------------------
    if g_access_scheme = 1 then
      -- Wait for bsn monitor to have received a sync period.
      mmf_mm_wait_until_value(c_mm_file_reg_bsn_monitor_v2_ring_rx, 4,  -- read nof valid
                              "SIGNED", rd_data, ">", 0,  -- this is the wait until condition
                              1 us, tb_clk);  -- read every 1 us

      for I in 0 to c_nof_lanes - 1 loop
        mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_v2_ring_tx, I * c_sdp_N_pn_max * 8 + 1, rd_data, tb_clk);  -- bsn at sync
        assert TO_UINT(rd_data) = c_exp_bsn_at_sync
          report "Wrong bsn_at_sync value from bsn_monitor_v2_ring_tx on source node in access scheme 1."
          severity ERROR;
        mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_v2_ring_tx, I * c_sdp_N_pn_max * 8 + 3, rd_data, tb_clk);  -- nof_sop
        assert TO_UINT(rd_data) = c_exp_nof_sop
          report "Wrong nof_sop value from bsn_monitor_v2_ring_tx on source node in access scheme 1."
          severity ERROR;
        mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_v2_ring_tx, I * c_sdp_N_pn_max * 8 + 4, rd_data, tb_clk);  -- nof_valid
        assert TO_UINT(rd_data) = c_exp_nof_valid
          report "Wrong nof_valid value from bsn_monitor_v2_ring_tx on source node in access scheme 1."
          severity ERROR;
        mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_v2_ring_tx, I * c_sdp_N_pn_max * 8 + 5, rd_data, tb_clk);  -- nof_err
        assert TO_UINT(rd_data) = 0
          report "Wrong nof_err value from bsn_monitor_v2_ring_tx on source node in access scheme 1."
          severity ERROR;

        mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_v2_ring_rx, I * c_sdp_N_pn_max * 8 + 1, rd_data, tb_clk);  -- bsn at sync
        assert TO_UINT(rd_data) = c_exp_bsn_at_sync
          report "Wrong bsn_at_sync value from bsn_monitor_v2_ring_rx on source node in access scheme 1."
          severity ERROR;
        mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_v2_ring_rx, I * c_sdp_N_pn_max * 8 + 3, rd_data, tb_clk);  -- nof_sop
        assert TO_UINT(rd_data) = c_exp_nof_sop
          report "Wrong nof_sop value from bsn_monitor_v2_ring_rx on source node in access scheme 1."
          severity ERROR;
        mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_v2_ring_rx, I * c_sdp_N_pn_max * 8 + 4, rd_data, tb_clk);  -- nof_valid
        assert TO_UINT(rd_data) = c_exp_nof_valid
          report "Wrong nof_valid value from bsn_monitor_v2_ring_rx on source node in access scheme 1."
          severity ERROR;
        mmf_mm_bus_rd(c_mm_file_reg_bsn_monitor_v2_ring_rx, I * c_sdp_N_pn_max * 8 + 5, rd_data, tb_clk);  -- nof_err
        assert TO_UINT(rd_data) = 0
          report "Wrong nof_err value from bsn_monitor_v2_ring_rx on source node in access scheme 1."
          severity ERROR;
      end loop;

    ----------------------------------------------------------------------------
    -- Verify Access scheme 2,3 by reading rx / tx monitors on all RN
    ----------------------------------------------------------------------------
    else
    -- Wait for bsn monitor to have received a sync period.
    mmf_mm_wait_until_value(mmf_unb_file_prefix(g_unb_nr + ((g_nof_rn - 1) / c_quad), (g_nof_rn - 1) mod c_quad) & "REG_BSN_MONITOR_V2_RING_RX", 4,  -- read nof valid
                            "SIGNED", rd_data, ">", 0,  -- this is the wait until condition
                            1 us, tb_clk);  -- read every 1 us

      for RN in 0 to g_nof_rn - 1 loop
        for I in 0 to c_nof_lanes - 1 loop  -- lane index
          for J in 0 to g_nof_rn - 1 loop  -- bsn_monitor index
            -- No packets transmitted from next RN (this_rn + 1 for even lanes, this_rn - 1 for odd lanes) as this RN should have removed it from the ring.
            if (I mod 2 = 0 and (RN + 1) mod g_nof_rn = J) or (I mod 2 = 1 and (RN + g_nof_rn - 1) mod g_nof_rn = J) then
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_TX", (I * c_sdp_N_pn_max + J) * 8 + 0, rd_data, tb_clk);  -- status bits
              assert rd_data(2) = '1'
                report "Wrong sync_timout, expected 1, got 0. From bsn_monitor_v2_ring_tx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
            else
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_TX", (I * c_sdp_N_pn_max + J) * 8 + 0, rd_data, tb_clk);  -- status bits
              assert rd_data(2) = '0'
                report "Wrong sync_timout, expected 0, got 1. From bsn_monitor_v2_ring_tx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_TX", (I * c_sdp_N_pn_max + J) * 8 + 1, rd_data, tb_clk);  -- bsn at sync
              assert TO_UINT(rd_data) = c_exp_bsn_at_sync
                report "Wrong bsn_at_sync value from bsn_monitor_v2_ring_tx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_TX", (I * c_sdp_N_pn_max + J) * 8 + 3, rd_data, tb_clk);  -- nof_sop
              assert TO_UINT(rd_data) = c_exp_nof_sop
                report "Wrong nof_sop value from bsn_monitor_v2_ring_tx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_TX", (I * c_sdp_N_pn_max + J) * 8 + 4, rd_data, tb_clk);  -- nof_valid
              assert TO_UINT(rd_data) = c_exp_nof_valid
                report "Wrong nof_valid value from bsn_monitor_v2_ring_tx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_TX", (I * c_sdp_N_pn_max + J) * 8 + 5, rd_data, tb_clk);  -- nof_err
              assert TO_UINT(rd_data) = 0
                report "Wrong nof_err value from bsn_monitor_v2_ring_tx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
            end if;
            if RN = J then  -- No packets received from itself as the previous RN should have removed it from the ring.
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_RX", (I * c_sdp_N_pn_max + J) * 8 + 0, rd_data, tb_clk);  -- status bits
              assert rd_data(2) = '1'
                report "Wrong sync_timout, expected 1, got 0. From bsn_monitor_v2_ring_rx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
            else
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_RX", (I * c_sdp_N_pn_max + J) * 8 + 0, rd_data, tb_clk);  -- status bits
              assert rd_data(2) = '0'
                report "Wrong sync_timout, expected 0, got 1. From bsn_monitor_v2_ring_rx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_RX", (I * c_sdp_N_pn_max + J) * 8 + 1, rd_data, tb_clk);  -- bsn at sync
              assert TO_UINT(rd_data) = c_exp_bsn_at_sync
                report "Wrong bsn_at_sync value from bsn_monitor_v2_ring_rx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_RX", (I * c_sdp_N_pn_max + J) * 8 + 3, rd_data, tb_clk);  -- nof_sop
              assert TO_UINT(rd_data) = c_exp_nof_sop
                report "Wrong nof_sop value from bsn_monitor_v2_ring_rx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_RX", (I * c_sdp_N_pn_max + J) * 8 + 4, rd_data, tb_clk);  -- nof_valid
              assert TO_UINT(rd_data) = c_exp_nof_valid
                report "Wrong nof_valid value from bsn_monitor_v2_ring_rx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
              mmf_mm_bus_rd(mmf_unb_file_prefix(g_unb_nr + (RN / c_quad), RN mod c_quad) & "REG_BSN_MONITOR_V2_RING_RX", (I * c_sdp_N_pn_max + J) * 8 + 5, rd_data, tb_clk);  -- nof_err
              assert TO_UINT(rd_data) = 0
                report "Wrong nof_err value from bsn_monitor_v2_ring_rx on RN_" & integer'image(RN) & " in access scheme 2/3."
                severity ERROR;
            end if;
          end loop;
        end loop;
      end loop;
    end if;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(not g_multi_tb, ext_clk, sim_done, i_tb_end);
    wait;
  end process;

  tb_end <= i_tb_end;
end tb;
