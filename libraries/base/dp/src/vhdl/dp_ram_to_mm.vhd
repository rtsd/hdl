--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_ram_to_mm is
  generic (
    g_technology       : natural := c_tech_select_default;
    g_ram_rd_nof_words : natural;
    g_ram_wr_dat_w     : natural
  );
  port (
    mm_rst   : in  std_logic;
    mm_clk   : in  std_logic;

    st_rst   : in  std_logic;
    st_clk   : in  std_logic;

    sla_in   : in  t_mem_mosi;
    sla_out  : out t_mem_miso;

    snk_in   : in  t_dp_sosi;
    snk_out  : out t_dp_siso
  );
end dp_ram_to_mm;

architecture rtl of dp_ram_to_mm is
  constant c_mm_ram_rd  : t_c_mem := (latency  => 1,
                                      adr_w    => ceil_log2(g_ram_rd_nof_words),
                                      dat_w    => c_word_w,
                                      nof_dat  => g_ram_rd_nof_words,
                                      init_sl  => '0');

  constant c_ram_wr_nof_words : natural := (c_word_w * g_ram_rd_nof_words) / g_ram_wr_dat_w;

  constant c_mm_ram_wr  : t_c_mem := (latency  => 1,
                                      adr_w    => ceil_log2(c_ram_wr_nof_words),
                                      dat_w    => g_ram_wr_dat_w,
                                      nof_dat  => c_ram_wr_nof_words,
                                      init_sl  => '0');

  type t_state_enum is (s_init, s_wait_for_sop, s_write);

  signal state         : t_state_enum;
  signal nxt_state     : t_state_enum;

  signal nxt_snk_out  : t_dp_siso;

  signal wr_en       : std_logic;
  signal wr_data     : std_logic_vector(c_mm_ram_wr.dat_w - 1 downto 0);
  signal wr_addr     : std_logic_vector(c_mm_ram_wr.adr_w - 1 downto 0);
  signal nxt_wr_addr : std_logic_vector(c_mm_ram_wr.adr_w - 1 downto 0);
begin
  p_state : process(state, wr_addr, snk_in, wr_data)
  begin
    nxt_state   <= state;
    nxt_wr_addr <= wr_addr;
    wr_en <= '0';
    nxt_snk_out <= c_dp_siso_rst;

    case state is
      when s_init =>
        nxt_state <= s_wait_for_sop;

      when s_wait_for_sop =>
        nxt_snk_out <= c_dp_siso_rdy;
        if snk_in.sop = '1' then
          wr_en <= '1';
          nxt_state <= s_write;
          nxt_wr_addr <= INCR_UVEC(wr_addr, 1);
          if unsigned(wr_addr) = c_ram_wr_nof_words - 1 then
            nxt_state   <= s_wait_for_sop;
            nxt_wr_addr <= (others => '0');
          end if;
        end if;

      when s_write =>
        nxt_snk_out <= c_dp_siso_rdy;
        if snk_in.valid = '1' then
          wr_en <= '1';
          nxt_wr_addr <= INCR_UVEC(wr_addr, 1);
          if unsigned(wr_addr) = c_ram_wr_nof_words - 1 then
            nxt_state   <= s_wait_for_sop;
            nxt_wr_addr <= (others => '0');
          end if;
        end if;

      when others =>
        nxt_state <= s_init;
    end case;
  end process;

  p_clk : process(st_rst, st_clk)
  begin
    if st_rst = '1' then
      state   <= s_init;
      wr_addr <= (others => '0');
      snk_out <= c_dp_siso_rst;
    elsif rising_edge(st_clk) then
      state   <= nxt_state;
      wr_addr <= nxt_wr_addr;
      snk_out <= nxt_snk_out;
    end if;
  end process;

  u_ram : entity common_lib.common_ram_cr_cw_ratio
  generic map (
    g_technology => g_technology,
    g_ram_wr    => c_mm_ram_wr,
    g_ram_rd    => c_mm_ram_rd,
    g_init_file => "UNUSED"
  )
  port map (
    -- Write port clock domain
    wr_rst    => st_rst,
    wr_clk    => st_clk,
    wr_en     => snk_in.valid,
    wr_adr    => wr_addr,
    wr_dat    => snk_in.data(c_mm_ram_wr.dat_w - 1 downto 0),
    -- Read port clock domain
    rd_rst    => mm_rst,
    rd_clk    => mm_clk,
    rd_en     => sla_in.rd,
    rd_adr    => sla_in.address(c_mm_ram_rd.adr_w - 1 downto 0),
    rd_dat    => sla_out.rddata(c_mm_ram_rd.dat_w - 1 downto 0),
    rd_val    => sla_out.rdval
  );
end rtl;
