--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Combine mac_10g and 10gbase_r for c_tech_arria10_proto
-- Description
--
--   The clocks come from an external central fPLL:
--
--     tx_ref_clk_644 --> fPLL --> clk_312
--                                 clk_156, rst_156
--   Blockdiagram:
--
--                          312 156 644
--                _________   |  |  |   ____________
--                |       |   |  |  |   |          |
--                |       |<--/  |  \-->|          |
--                |       |<-----+----->|          |
--                |       |             |          |
--                |       |    XGMII    |          |
--     tx_snk --->|tech_  |------------>|tech_     |---> serial_tx
--     rx_src <---|mac_10g|<------------|10gbase_r |<--- serial_rx
--                |       |             |          |
--                |_______|--\       /--|__________|
--                    |      |       |
--                  mac_mm   |       |
--                           |       v
--                       (   v    xgmii_tx_ready)
--     tx_snk_out.xon <--(xgmii_link_status[1:0])
--
-- . g_direction:
--   "TX_RX" = Default support bidir
--   "TX_ONLY" = Uses a bidir MAC and connects the MAC Tx to the MAC RX.
--   "RX_ONLY" = Same as "TX_RX"
--   See tech_eth_10g_stratixiv.vhd for more details.
--
-- Remarks:
-- . xgmii_link_status:
--   When the xgmii_tx_ready from the 10gbase_r and the xgmii_link_status from
--   the mac_10g are both be OK then the tx_snk.xon is asserted to allow the
--   user data transmission.
--   The tb_tech_eth_10g reveals that xgmii_tx_ready goes high after some power
--   up time and then remains active independent of link_fault.
--   A link fault eg. due to rx disconnect is detected by the link fault status:
--     0 = OK
--     1 = local fault
--     2 = remote fault
--
--   From google search:
--     Link fault Operation
--     1) Device B detects loss of signal. Local fault is signaled by PHY of Device B to Device B.
--     2) Device B ceases transmission of MAC frames and transmits remote fault to Device A.
--     3) Device A receives remote fault from Device B.
--     4) Device A stops sending frames, continuously generates Idle.
--
--   Hence when the xgmii_link_status is OK then the other side is also OK so
--   then it is also appropriate to release tx_snk.xon.
--
--   The XGMII link status can be monitored via the reg_eth10 MM register:
--
--     addr  data[31:0]
--      0      [0] = tx_snk_out_arr(I).xon
--             [1] = xgmii_tx_ready_arr(I)
--           [3:2] = xgmii_link_status_arr(I)
--

library IEEE, common_lib, dp_lib, technology_lib, tech_10gbase_r_lib, tech_mac_10g_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;

entity ip_arria10_eth_10g is
  generic (
    g_sim                 : boolean := false;
    g_sim_level           : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        : natural := 1;
    g_direction           : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_pre_header_padding  : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R

    -- Data clocks
    clk_312          : in  std_logic := '0';
    clk_156          : in  std_logic := '0';
    rst_156          : in  std_logic := '0';

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    reg_eth10g_mosi  : in  t_mem_mosi;  -- ETH10G (link status register)
    reg_eth10g_miso  : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- Serial
    serial_tx_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    serial_rx_arr    : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
end ip_arria10_eth_10g;

architecture str of ip_arria10_eth_10g is
  -- Enable or disable the conditions that must be ok to release tx_snk_out_arr xon
  constant c_check_link_status       : boolean := g_direction /= "TX_ONLY";
  constant c_check_xgmii_tx_ready    : boolean := g_direction /= "RX_ONLY";

  signal i_tx_snk_out_arr      : t_dp_siso_arr(g_nof_channels - 1 downto 0);

  -- MAG_10G control status registers
  signal mac_mosi_arr          : t_mem_mosi_arr(g_nof_channels - 1 downto 0);
  signal mac_miso_arr          : t_mem_miso_arr(g_nof_channels - 1 downto 0);

  -- XON control
  signal mac_snk_out_arr       : t_dp_siso_arr(g_nof_channels - 1 downto 0);

  -- XGMII
  signal xgmii_link_status_arr : t_tech_mac_10g_xgmii_status_arr(g_nof_channels - 1 downto 0);  -- 2 bit, from MAC_10g
  signal xgmii_tx_ready_arr    : std_logic_vector(g_nof_channels - 1 downto 0);  -- 1 bit, from PHY 10gbase_r
  signal xgmii_tx_dc_arr       : t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit
  signal xgmii_rx_dc_arr       : t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit
  signal xgmii_internal_dc_arr : t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit

  -- Link status monitor
  constant c_mem_reg_eth10g_adr_w     : natural := 1;
  constant c_mem_reg_eth10g_dat_w     : natural := 32;
  constant c_mem_reg_eth10g_nof_data  : natural := 1;
  constant c_mem_reg_eth10g           : t_c_mem := (c_mem_reg_rd_latency, c_mem_reg_eth10g_adr_w,  c_mem_reg_eth10g_dat_w,  c_mem_reg_eth10g_nof_data, 'X');

  signal reg_eth10g_mosi_arr          : t_mem_mosi_arr(g_nof_channels - 1 downto 0);
  signal reg_eth10g_miso_arr          : t_mem_miso_arr(g_nof_channels - 1 downto 0);

  signal mm_reg_eth10g_arr            : t_slv_32_arr(g_nof_channels - 1 downto 0);
begin
  tx_snk_out_arr <= i_tx_snk_out_arr;

  gen_mac : for I in 0 to g_nof_channels - 1 generate
    i_tx_snk_out_arr(I).ready <= mac_snk_out_arr(I).ready;  -- pass on MAC cycle accurate backpressure

    p_xon_flow_control : process(clk_156)
      variable v_xgmii_link_status     : std_logic_vector(c_tech_mac_10g_link_status_w - 1 downto 0) := "00";
      variable v_xgmii_tx_ready        : std_logic := '1';
    begin
      if rising_edge(clk_156) then
        i_tx_snk_out_arr(I).xon <= '0';

        -- First gather all conditions that are enabled to affect xon. The default value is such that it enables xon when the condition is not checked.
        if c_check_link_status   = true then v_xgmii_link_status := xgmii_link_status_arr(I); end if;  -- check both remote fault [1] and local fault [0]
        if c_check_xgmii_tx_ready = true then v_xgmii_tx_ready    := xgmii_tx_ready_arr(I);    end if;

        -- Now apply the conditions to xon
        if v_xgmii_tx_ready = '1' and v_xgmii_link_status = "00" then
          i_tx_snk_out_arr(I).xon <= '1';  -- XON when Tx PHY is ready and XGMII is ok
        end if;
      end if;
    end process;

    u_tech_mac_10g : entity tech_mac_10g_lib.tech_mac_10g
    generic map (
      g_technology          => c_tech_arria10_proto,
      g_pre_header_padding  => g_pre_header_padding
    )
    port map (
      -- MM
      mm_clk            => mm_clk,
      mm_rst            => mm_rst,
      csr_mosi          => mac_mosi_arr(I),
      csr_miso          => mac_miso_arr(I),

      -- ST
      tx_clk_312        => clk_312,
      tx_clk_156        => clk_156,
      tx_rst            => rst_156,
      tx_snk_in         => tx_snk_in_arr(I),  -- 64 bit data
      tx_snk_out        => mac_snk_out_arr(I),

      rx_clk_312        => clk_312,
      rx_clk_156        => clk_156,
      rx_rst            => rst_156,
      rx_src_out        => rx_src_out_arr(I),  -- 64 bit data
      rx_src_in         => rx_src_in_arr(I),

      -- XGMII
      xgmii_link_status => xgmii_link_status_arr(I),
      xgmii_tx_data     => xgmii_tx_dc_arr(I),
      xgmii_rx_data     => xgmii_internal_dc_arr(I)
    );
  end generate;

  xgmii_internal_dc_arr <= xgmii_tx_dc_arr when g_direction = "TX_ONLY" else xgmii_rx_dc_arr;

  u_tech_10gbase_r: entity tech_10gbase_r_lib.tech_10gbase_r
  generic map (
    g_technology     => c_tech_arria10_proto,
    g_sim            => g_sim,
    g_sim_level      => g_sim_level,
    g_nof_channels   => g_nof_channels
  )
  port map (
    -- Transceiver PLL reference clock
    tr_ref_clk_644     => tr_ref_clk_644,

    -- XGMII clocks
    clk_156            => clk_156,
    rst_156            => rst_156,

    -- XGMII interface
    xgmii_tx_ready_arr => xgmii_tx_ready_arr,
    xgmii_rx_ready_arr => OPEN,
    xgmii_tx_dc_arr    => xgmii_tx_dc_arr,
    xgmii_rx_dc_arr    => xgmii_rx_dc_arr,

    -- PHY serial IO
    tx_serial_arr      => serial_tx_arr,
    rx_serial_arr      => serial_rx_arr
  );

  gen_reg_eth10g : for I in 0 to g_nof_channels - 1 generate
    mm_reg_eth10g_arr(I) <= RESIZE_UVEC(xgmii_link_status_arr(I) & xgmii_tx_ready_arr(I) & i_tx_snk_out_arr(I).xon, c_mem_reg_eth10g_dat_w);

    u_reg_map : entity common_lib.common_reg_r_w_dc
    generic map (
      g_cross_clock_domain => true,
      g_in_new_latency     => 0,
      g_readback           => false,
      g_reg                => c_mem_reg_eth10g,
      g_init_reg           => (others => '0')
    )
    port map (
      -- Clocks and reset
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,
      st_rst      => rst_156,
      st_clk      => clk_156,

      -- Memory Mapped Slave in mm_clk domain
      sla_in      => reg_eth10g_mosi_arr(I),
      sla_out     => reg_eth10g_miso_arr(I),

      -- MM registers in st_clk domain
      reg_wr_arr  => OPEN,
      reg_rd_arr  => OPEN,
      in_new      => '1',
      in_reg      => mm_reg_eth10g_arr(I),
      out_reg     => open
    );
  end generate;

  -----------------------------------------------------------------------------
  -- MM bus mux
  -----------------------------------------------------------------------------
  u_common_mem_mux_mac : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_channels,
    g_mult_addr_w => func_tech_mac_10g_csr_addr_w(c_tech_arria10_proto)
  )
  port map (
    mosi     => mac_mosi,
    miso     => mac_miso,
    mosi_arr => mac_mosi_arr,
    miso_arr => mac_miso_arr
  );

  u_common_mem_mux_eth10g : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_channels,
    g_mult_addr_w => c_mem_reg_eth10g_adr_w
  )
  port map (
    mosi     => reg_eth10g_mosi,
    miso     => reg_eth10g_miso,
    mosi_arr => reg_eth10g_mosi_arr,
    miso_arr => reg_eth10g_miso_arr
  );
end str;
