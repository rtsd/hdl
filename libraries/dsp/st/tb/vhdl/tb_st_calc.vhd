-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_st_calc is
  generic (
    g_in_dat_w     : natural := 16;
    g_out_dat_w    : natural := 32
  );
end tb_st_calc;

architecture tb of tb_st_calc is
  constant clk_period             : time := 10 ns;

  constant c_nof_sync             : natural := 3;
  constant c_nof_stat             : natural := 100;
  constant c_out_adr_w            : natural := ceil_log2(c_nof_stat);
  constant c_gap_size             : natural := 2**c_out_adr_w - c_nof_stat;

  constant c_nof_accum_per_sync   : natural := 5;  -- integration time

  signal tb_end          : std_logic := '0';
  signal clk             : std_logic := '0';
  signal rst             : std_logic;

  signal in_sync         : std_logic;
  signal in_val          : std_logic;
  signal in_dat          : std_logic_vector(g_in_dat_w - 1 downto 0);

  signal in_a_re         : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_a_im         : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_b_re         : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_b_im         : std_logic_vector(g_in_dat_w - 1 downto 0);

  signal out_adr         : std_logic_vector(c_out_adr_w - 1 downto 0);
  signal out_re          : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal out_im          : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal out_val         : std_logic;
begin
  clk  <= not clk or tb_end after clk_period / 2;

  in_a_re <= in_dat;
  in_a_im <= in_dat;
  in_b_re <= in_dat;
  in_b_im <= in_dat;

  in_dat <= (others => '0') when rst = '1' else INCR_UVEC(in_dat, 1) when rising_edge(clk) and in_val = '1';

  -- run 1 us
  p_stimuli : process
  begin
    rst <= '1';
    in_sync <= '0';
    in_val <= '0';
    wait until rising_edge(clk);
    for I in 0 to 9 loop wait until rising_edge(clk); end loop;
    rst <= '0';
    for I in 0 to 9 loop wait until rising_edge(clk); end loop;

    for I in 0 to c_nof_sync - 1 loop
      in_sync <= '1';
      wait until rising_edge(clk);
      in_sync <= '0';

      for J in 0 to c_nof_accum_per_sync - 1 loop
        in_val <= '1';
        for I in 0 to c_nof_stat - 1 loop wait until rising_edge(clk); end loop;
        in_val <= '0';
        for I in 0 to c_gap_size-1 loop wait until rising_edge(clk); end loop;
      end loop;
    end loop;
    for I in 0 to 9 loop wait until rising_edge(clk); end loop;
    tb_end <= '1';
    wait;
  end process;

  u_dut : entity work.st_calc
  generic map (
    g_nof_mux       => 1,
    g_nof_stat      => c_nof_stat,
    g_in_dat_w      => g_in_dat_w,
    g_out_dat_w     => g_out_dat_w,
    g_out_adr_w     => c_out_adr_w,
    g_complex       => false
  )
  port map (
    rst        => rst,
    clk        => clk,
    clken      => '1',
    in_ar      => in_a_re,
    in_ai      => in_a_im,
    in_br      => in_b_re,
    in_bi      => in_b_im,
    in_val     => in_val,
    in_sync    => in_sync,
    out_adr    => out_adr,
    out_re     => out_re,
    out_im     => out_im,
    out_val    => out_val,
    out_val_m  => open
  );
end tb;
