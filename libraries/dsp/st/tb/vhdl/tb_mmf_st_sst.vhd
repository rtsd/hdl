-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the st_sst unit.
--           To be used in conjunction with python script: ../python/tc_mmf_st_sst.py
--
--
-- Usage in non-auto-mode (c_modelsim_start = 0 in python):
--   > as 5
--   > run -all
--   > Run python script in separate terminal: "python tc_mmf_st_xst.py --unb 0 --bn 0 --sim"
--   > Check the results of the python script.
--   > Stop the simulation manually in Modelsim by pressing the stop-button.
--   > Evalute the WAVE window.

library IEEE, common_lib, mm_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;

entity tb_mmf_st_sst is
  generic(
    g_nof_stat      : natural := 8;  -- nof accumulators
    g_xst_enable    : boolean := true;
    g_in_data_w     : natural := 16;
    g_stat_data_w   : natural := 56;  -- statistics accumulator width
    g_stat_data_sz  : natural := 2;  -- statistics word width >= statistics accumulator width and fit in a power of 2 multiple 32b MM words
    g_nof_instances : natural := 4;  -- The number of st_sst instances in parallel.
    g_nof_frames    : natural := 1
  );
end tb_mmf_st_sst;

architecture tb of tb_mmf_st_sst is
  constant c_sim                : boolean := true;

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period      : time := 100 ps;
  constant c_dp_clk_period      : time := 2 ns;
  constant c_sclk_period        : time := 1250 ps;
  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  signal reg_diag_bg_mosi       : t_mem_mosi;
  signal reg_diag_bg_miso       : t_mem_miso;

  signal ram_diag_bg_mosi       : t_mem_mosi;
  signal ram_diag_bg_miso       : t_mem_miso;

  signal ram_st_sst_mosi        : t_mem_mosi;
  signal ram_st_sst_miso        : t_mem_miso;

  signal reg_st_sst_mosi        : t_mem_mosi;
  signal reg_st_sst_miso        : t_mem_miso;

  signal ram_st_sst_mosi_arr    : t_mem_mosi_arr(g_nof_instances - 1 downto 0);
  signal ram_st_sst_miso_arr    : t_mem_miso_arr(g_nof_instances - 1 downto 0);

  signal reg_st_sst_mosi_arr    : t_mem_mosi_arr(g_nof_instances - 1 downto 0);
  signal reg_st_sst_miso_arr    : t_mem_miso_arr(g_nof_instances - 1 downto 0);

  -- Custom definitions of constants
  constant c_bg_block_len           : natural  := g_nof_stat * g_nof_frames;
  constant c_complex_factor         : natural  := sel_a_b(g_xst_enable, c_nof_complex, 1);
  constant c_ram_addr_w             : natural  := ceil_log2(g_stat_data_sz * g_nof_stat * c_complex_factor);

  -- Configuration of the block generator:
  constant c_bg_nof_output_streams  : positive := g_nof_instances;
  constant c_bg_buf_dat_w           : positive := c_nof_complex * g_in_data_w;
  constant c_bg_buf_adr_w           : positive := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_prefix    : string   := "UNUSED";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, g_nof_instances, 1);

  -- Signal declarations to connect block generator to the DUT
  signal bg_siso_arr                : t_dp_siso_arr(c_bg_nof_output_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(c_bg_nof_output_streams - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  u_mm_file_reg_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_BG")
                                           port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

  u_mm_file_ram_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_BG")
                                           port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

  u_mm_file_ram_st_sst           : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_ST_SST")
                                           port map(mm_rst, mm_clk, ram_st_sst_mosi, ram_st_sst_miso);

  u_mm_file_reg_st_sst           : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_ST_SST")
                                           port map(mm_rst, mm_clk, reg_st_sst_mosi, reg_st_sst_miso);

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_bg_nof_output_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr         => bg_siso_arr,
    out_sosi_arr         => bg_sosi_arr
  );

  -- Combine the internal array of mm interfaces for the beamlet statistics to one array that is connected to the port of bf
  u_mem_mux_ram_sst : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_instances,
    g_mult_addr_w => c_ram_addr_w
  )
  port map (
    mosi     => ram_st_sst_mosi,
    miso     => ram_st_sst_miso,
    mosi_arr => ram_st_sst_mosi_arr,
    miso_arr => ram_st_sst_miso_arr
  );

  u_mem_mux_reg_sst : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_instances,
    g_mult_addr_w => 1
  )
  port map (
    mosi     => reg_st_sst_mosi,
    miso     => reg_st_sst_miso,
    mosi_arr => reg_st_sst_mosi_arr,
    miso_arr => reg_st_sst_miso_arr
  );

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  gen_duts : for I in 0 to g_nof_instances - 1 generate
    u_dut : entity work.st_sst
    generic map(
      g_nof_stat       => g_nof_stat,
      g_xst_enable     => g_xst_enable,
      g_in_data_w      => g_in_data_w,
      g_stat_data_w    => g_stat_data_w,
      g_stat_data_sz   => g_stat_data_sz
    )
    port map(
      mm_rst           =>  mm_rst,
      mm_clk           =>  mm_clk,
      dp_rst           =>  dp_rst,
      dp_clk           =>  dp_clk,

      -- Streaming
      in_complex       => bg_sosi_arr(I),

      -- Memory Mapped
      ram_st_sst_mosi  => ram_st_sst_mosi_arr(I),
      ram_st_sst_miso  => ram_st_sst_miso_arr(I),
      reg_st_sst_mosi  => reg_st_sst_mosi_arr(I),
      reg_st_sst_miso  => reg_st_sst_miso_arr(I)
    );
  end generate;
end tb;
