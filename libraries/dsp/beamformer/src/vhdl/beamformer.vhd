-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Structural beamformer using weights RAM, multiplier and adder stage
-- Description:
-- . This beamformer multiplies each valid input sample of snk_in_arr by
--   weights read from RAM, and adds the resulting products of all streams
--   together into a single output stream.
-- . The weight is taken from the RAM that corresponds to the snk_in stream
--   index.
-- . To keep this beamformer generic and easily reusable the user can wrap
--   this beamformer with simple application specific logic to control
--   weight_addr.
--   . Input/output reordering and quantization are also application
--     specific and should be done in the wrapper.
-- . The input array snk_in_arr must be synchronous.
-- . If RAM is not desired, set g_use_weight_reg or g_use_weight_arr.
--   . The addressing remains the same (done using weight_addr) but the
--     source of the weights can be selected that way.
-- . Note: Only set 1 of g_use_weight_ram, g_use_weight_reg, g_use_weight_arr
--         to TRUE.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library common_lib, common_mult_lib, technology_lib, dp_lib;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity beamformer is
  generic (
    g_technology            : natural := c_tech_select_default;
    g_nof_inputs            : natural;
    g_data_w                : natural;
    g_nof_weights           : natural;
    g_weights_w             : natural := 16;
    g_weights_file          : string  := "hex/beamformer_weights";
    g_weights_ram_dual_port : boolean := true;  -- FIXME rename this to readback_weights
    g_mult_variant          : string := "IP";
    g_use_weight_ram        : boolean := true;  -- Use weights RAM (default)
    g_use_weight_reg        : boolean := false;  -- Use a weights register instead of RAM
    g_use_weight_arr        : boolean := false  -- Use the input weights array instead of weights RAM of Registers
  );
  port (
    dp_clk      : in  std_logic;
    dp_rst      : in  std_logic;

    mm_clk      : in  std_logic;
    mm_rst      : in  std_logic;

    ram_mosi    : in  t_mem_mosi := c_mem_mosi_rst;  -- MM interface to upload weights to RAM or REG
    ram_miso    : out t_mem_miso;

    weight_addr : in  std_logic_vector(ceil_log2(g_nof_weights) - 1 downto 0);  -- Weight address
    weight_arr  : in  t_slv_32_arr(g_nof_inputs - 1 downto 0) := (others => (others => '0'));

    snk_in_arr  : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);  -- All streams must be synchronous
    src_out     : out t_dp_sosi
  );
end beamformer;

architecture str of beamformer is
  ------------------------------------------------------------------------------
  -- Weights RAM
  ------------------------------------------------------------------------------
  constant c_common_ram_crw_crw_ram : t_c_mem := (latency  => 1,
                                                  adr_w    => ceil_log2(g_nof_weights),
                                                  dat_w    => c_nof_complex * g_weights_w,
                                                  nof_dat  => g_nof_weights,
                                                  init_sl  => '0');

  type t_common_ram_crw_crw_adr_b_arr is array(g_nof_inputs - 1 downto 0) of std_logic_vector(ceil_log2(g_nof_weights) - 1 downto 0);
  type t_common_ram_crw_crw_rd_dat_b_arr is array(g_nof_inputs - 1 downto 0) of std_logic_vector(c_nof_complex * g_weights_w - 1 downto 0);

  signal ram_mosi_arr : t_mem_mosi_arr(g_nof_inputs - 1 downto 0);
  signal ram_miso_arr : t_mem_miso_arr(g_nof_inputs - 1 downto 0);

  signal common_ram_crw_crw_adr_b_arr    : t_common_ram_crw_crw_adr_b_arr;
  signal common_ram_crw_crw_rd_en_b_arr  : std_logic_vector(g_nof_inputs - 1 downto 0);
  signal common_ram_crw_crw_rd_dat_b_arr : t_common_ram_crw_crw_rd_dat_b_arr;
  signal common_ram_crw_crw_rd_val_b_arr : std_logic_vector(g_nof_inputs - 1 downto 0);

  signal common_ram_crw_crw_src_out_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  ------------------------------------------------------------------------------
  -- Weights REG
  ------------------------------------------------------------------------------
  type t_common_reg_r_d_dc_out_slv_arr is array(g_nof_inputs - 1 downto 0) of std_logic_vector(c_common_ram_crw_crw_ram.dat_w * c_common_ram_crw_crw_ram.nof_dat - 1 downto 0);
  type t_common_reg_r_d_dc_out_arr     is array(g_nof_weights - 1 downto 0) of std_logic_vector(c_common_ram_crw_crw_ram.dat_w - 1 downto 0);
  type t_common_reg_r_d_dc_out_2arr    is array(g_nof_inputs - 1 downto 0) of t_common_reg_r_d_dc_out_arr;

  signal common_reg_r_w_dc_in_reg_slv_arr  : t_common_reg_r_d_dc_out_slv_arr;
  signal common_reg_r_w_dc_out_reg_slv_arr : t_common_reg_r_d_dc_out_slv_arr;
  signal common_reg_r_w_dc_out_reg_2arr    : t_common_reg_r_d_dc_out_2arr;

  -- The register outputs the weight 1 cycle too soon relative to the RAM, so register the address
  signal reg_weight_addr :  std_logic_vector(ceil_log2(g_nof_weights) - 1 downto 0);

  ------------------------------------------------------------------------------
  -- Pipeline
  ------------------------------------------------------------------------------
  signal dp_pipeline_arr_src_out_arr   : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  ------------------------------------------------------------------------------
  -- Multiplier stage
  ------------------------------------------------------------------------------
  signal dp_complex_mult_snk_in_2arr_2 : t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);
  signal dp_complex_mult_src_out_arr   : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Weights RAM
  ------------------------------------------------------------------------------
  gen_weight_ram : if g_use_weight_ram = true generate
    gen_common_ram_crw_crw : for i in 0 to g_nof_inputs - 1 generate
      -- Read request on every incoming valid cycle
      common_ram_crw_crw_rd_en_b_arr(i) <= snk_in_arr(i).valid;

      -- Use entity input for read address
      common_ram_crw_crw_adr_b_arr(i) <= weight_addr;

      -- Dual clock RAM
      u_common_ram_crw_crw : entity common_lib.common_ram_crw_crw
      generic map (
        g_technology     => g_technology,
        g_ram            => c_common_ram_crw_crw_ram,
        g_init_file      => sel_a_b(g_weights_file = "UNUSED", "UNUSED", g_weights_file & "_" & natural'image(i) & ".hex"),
        g_true_dual_port => g_weights_ram_dual_port
      )
      port map (
        rst_a     => mm_rst,
        clk_a     => mm_clk,
        wr_en_a   => ram_mosi_arr(i).wr,
        wr_dat_a  => ram_mosi_arr(i).wrdata(c_common_ram_crw_crw_ram.dat_w - 1 downto 0),
        adr_a     => ram_mosi_arr(i).address(c_common_ram_crw_crw_ram.adr_w - 1 downto 0),
        rd_en_a   => ram_mosi_arr(i).rd,
        rd_dat_a  => ram_miso_arr(i).rddata(c_common_ram_crw_crw_ram.dat_w - 1 downto 0),
        rd_val_a  => ram_miso_arr(i).rdval,

        rst_b     => dp_rst,
        clk_b     => dp_clk,
        wr_en_b   => '0',
        wr_dat_b  => (others => '0'),
        adr_b     => common_ram_crw_crw_adr_b_arr(i),
        rd_en_b   => common_ram_crw_crw_rd_en_b_arr(i),
        rd_dat_b  => common_ram_crw_crw_rd_dat_b_arr(i),
        rd_val_b  => common_ram_crw_crw_rd_val_b_arr(i)
      );

      -- RAM output rewired to SOSI array
      common_ram_crw_crw_src_out_arr(i).re <= RESIZE_DP_DSP_DATA(common_ram_crw_crw_rd_dat_b_arr(i)(  g_weights_w - 1 downto 0));
      common_ram_crw_crw_src_out_arr(i).im <= RESIZE_DP_DSP_DATA(common_ram_crw_crw_rd_dat_b_arr(i)(2 * g_weights_w - 1 downto g_weights_w));
      common_ram_crw_crw_src_out_arr(i).valid <= common_ram_crw_crw_rd_val_b_arr(i);
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- Weights register
  ------------------------------------------------------------------------------
  gen_weight_reg : if g_use_weight_reg = true generate
    gen_common_reg_r_w_dc : for i in 0 to g_nof_inputs - 1 generate
      u_common_reg_r_w_dc : entity common_lib.common_reg_r_w_dc
      generic map (
        g_cross_clock_domain => true,
        g_readback           => g_weights_ram_dual_port,
        g_reg                => c_common_ram_crw_crw_ram
      )
      port map (
        -- Clocks and reset
        mm_rst         => mm_rst,
        mm_clk         => mm_clk,
        st_rst         => dp_rst,
        st_clk         => dp_clk,

        -- Memory Mapped Slave in mm_clk domain
        sla_in         => ram_mosi_arr(i),
        sla_out        => ram_miso_arr(i),

        -- MM registers in st_clk domain
        reg_wr_arr     => open,
        reg_rd_arr     => open,
        in_reg         => common_reg_r_w_dc_in_reg_slv_arr(i),
        out_reg        => common_reg_r_w_dc_out_reg_slv_arr(i)
      );

      -- Rewire the concatenated SLV array to something we can index properly [g_nof_inputs][g_nof_weights]
      gen_common_reg_r_w_dc_out_reg_2arr : for j in 0 to g_nof_weights - 1 generate
        common_reg_r_w_dc_out_reg_2arr(i)(j) <= common_reg_r_w_dc_out_reg_slv_arr(i)(j * 2 * g_weights_w + 2 * g_weights_w - 1 downto j * 2 * g_weights_w);
      end generate;

      -- REG output rewired to 'RAM' SOSI array. The weight is indexed by reg_weight_addr.
      common_ram_crw_crw_src_out_arr(i).re <= RESIZE_DP_DSP_DATA(common_reg_r_w_dc_out_reg_2arr(i)(TO_UINT(reg_weight_addr))(  g_weights_w - 1 downto 0));
      common_ram_crw_crw_src_out_arr(i).im <= RESIZE_DP_DSP_DATA(common_reg_r_w_dc_out_reg_2arr(i)(TO_UINT(reg_weight_addr))(2 * g_weights_w - 1 downto g_weights_w));
    end generate;

    gen_readback: if g_weights_ram_dual_port = true generate
      common_reg_r_w_dc_in_reg_slv_arr <= common_reg_r_w_dc_out_reg_slv_arr;
    end generate;

  end generate;

  ------------------------------------------------------------------------------
  -- Combine the individual MM buses into one
  ------------------------------------------------------------------------------
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_inputs,
    g_mult_addr_w => ceil_log2(g_nof_weights)
  )
  port map (
    mosi     => ram_mosi,
    miso     => ram_miso,
    mosi_arr => ram_mosi_arr,
    miso_arr => ram_miso_arr
  );

  ------------------------------------------------------------------------------
  -- Pipeline to align snk_in_arr with dp_ram_src_out_arr
  ------------------------------------------------------------------------------
  u_dp_pipeline_arr : entity dp_lib.dp_pipeline_arr
  generic map(
    g_nof_streams => g_nof_inputs,
    g_pipeline    => 1
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,

    snk_in_arr   => snk_in_arr,

    src_out_arr  => dp_pipeline_arr_src_out_arr
  );

  ------------------------------------------------------------------------------
  -- Multiplier stage
  ------------------------------------------------------------------------------
  gen_mult_inputs: for i in 0 to g_nof_inputs - 1 generate
    dp_complex_mult_snk_in_2arr_2(i)(0) <= dp_pipeline_arr_src_out_arr(i);  -- Streaming data
    dp_complex_mult_snk_in_2arr_2(i)(1) <= common_ram_crw_crw_src_out_arr(i);  -- Weights RAM
  end generate;

  u_dp_complex_mult : entity dp_lib.dp_complex_mult
  generic map (
    g_nof_multipliers => g_nof_inputs,
    g_data_w          => g_data_w,
    g_variant         => g_mult_variant
  )
  port map (
    clk           => dp_clk,
    rst           => dp_rst,

    snk_in_2arr_2 => dp_complex_mult_snk_in_2arr_2,
    src_out_arr   => dp_complex_mult_src_out_arr
  );

  ------------------------------------------------------------------------------
  -- Adder stage
  ------------------------------------------------------------------------------
  u_dp_complex_add : entity dp_lib.dp_complex_add
  generic map (
    g_nof_inputs => g_nof_inputs,
    g_data_w     => 2 * g_data_w
  )
  port map (
    clk        => dp_clk,
    rst        => dp_rst,

    snk_in_arr => dp_complex_mult_src_out_arr,
    src_out    => src_out
  );

  ------------------------------------------------------------------------------
  -- Registers
  ------------------------------------------------------------------------------
  p_clk : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      reg_weight_addr <= (others => '0');
    elsif rising_edge(dp_clk) then
      reg_weight_addr <= weight_addr;
    end if;
  end process;
end str;
