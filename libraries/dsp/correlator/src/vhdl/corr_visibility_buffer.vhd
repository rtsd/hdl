--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Buffer the visibilities during folding
-- Description:
-- . Consists of an array of FIFOs that are read out periodically.
-- . The output data rate is such that all output streams can be folded into
--   one stream (valid cycles/stream never occur in parallel)
-- . In addition, the FIFOs are read out such that the folder outputs channel
--   blocks of visibilities.
-- . Finally, the output stream is toggled to produce a constant flow
--   instead of a short burst (to the extent that it is allowed by the
--   integration time - a short integration time leaves little headroom).

entity corr_visibility_buffer is
  generic (
    g_nof_inputs          : natural;  -- Number of input streams
    g_buffer_depth        : natural;  -- Number of words to buffer
    g_data_w              : natural;  -- Complex input data width
    g_nof_pre_mult_folds  : natural;  -- Nof times the data has been folded
    g_inter_channel_delay : natural  -- Nof delay cycles between output channels
   );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    src_out_arr    : out t_dp_sosi_arr(g_nof_inputs - 1 downto 0)
  );
end corr_visibility_buffer;

architecture str of corr_visibility_buffer is
  ------------------------------------------------------------------------------
  -- Example input: 6 visibilities, non-folded (g_nof_inputs = 6,
  -- g_nof_pre_mult_folds = 0)
  -- The improtance of timing the FIFOs is depiced below:
  -- ====================
  -- Untimed FIFO output:
  -- ====================
  --  dp_fifo_sc_src_out_arr(0) -> [ch0, ch1, .. chN-1]
  --  dp_fifo_sc_src_out_arr(1) -> [ch0, ch1, .. chN-1]
  --  dp_fifo_sc_src_out_arr(2) -> [ch0, ch1, .. chN-1]
  --  dp_fifo_sc_src_out_arr(3) -> [ch0, ch1, .. chN-1]
  --  dp_fifo_sc_src_out_arr(4) -> [ch0, ch1, .. chN-1]
  --  dp_fifo_sc_src_out_arr(5) -> [ch0, ch1, .. chN-1]
  -- ====================
  -- Timed FIFO output:
  -- ====================
  --  dp_fifo_sc_src_out_arr(0) -> [ch0]                   [ch1]
  --  dp_fifo_sc_src_out_arr(1) ->     [ch0]                   [ch1]
  --  dp_fifo_sc_src_out_arr(2) ->         [ch0]                   [ch1]
  --  dp_fifo_sc_src_out_arr(3) ->             [ch0]                   [ch1]
  --  dp_fifo_sc_src_out_arr(4) ->                 [ch0]                   [ch1]
  --  dp_fifo_sc_src_out_arr(5) ->                     [ch0]                   [ch1]
  --
  -- The above timed FIFO output allows the 6 streams to be interleaved into
  -- one stream, putting the channels back-to-back in the right order.

  -- The number of visibilities interleaved onto one input stream
  constant c_vis_block_len        : natural := pow2(g_nof_pre_mult_folds);

  -- The minimum period to wait so the folder does not miss data
  constant c_channel_block_length : natural := g_nof_inputs * c_vis_block_len;

  signal dp_fifo_sc_src_out_arr   : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal dp_fifo_sc_src_in_arr    : t_dp_siso_arr(g_nof_inputs - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Buffer the visibilities before the folding stage.
  -- . All accumulators output blocks of visibilities at the same time. We need
  --   to buffer the visibilities to source them to the folder at the right
  --   pace.
  ------------------------------------------------------------------------------
  gen_dp_fifo_sc : for i in 0 to g_nof_inputs - 1 generate
    u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
    generic map (
      g_data_w         => 2 * g_data_w,
      g_use_ctrl       => false,
      g_use_sync       => sel_a_b(i = 0, true, false),  -- Pass on sync of stream 0.
      g_use_complex    => true,
      g_fifo_size      => g_buffer_depth,
      g_fifo_af_margin => 0
    )
    port map (
      rst         => rst,
      clk         => clk,

      wr_ful      => OPEN,
      usedw       => OPEN,
      rd_emp      => OPEN,

      snk_out     => OPEN,
      snk_in      => snk_in_arr(i),
      src_in      => dp_fifo_sc_src_in_arr(i),
      src_out     => dp_fifo_sc_src_out_arr(i)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- Read out the FIFOs such that dp_fifo_sc_src_out_arr only carries one valid
  -- word per cycle. This allows folding (multiplexing) into one stream without
  -- losing data (example for 16 streams with c_vis_block_len=1):
  --
  --     |<-----------------------g_block_period-------------------->|
  --     |<---c_channel_block_length---->|<--g_inter_channel_delay-->|
  --  0) [0]                             .                           [1]
  --  1) | [0]                           .                           . [1]
  --  2) |   [0]                         .                           .   [1]
  --  3) |     [0]                       .                           .     [1]
  --  4) |       [0]                     .                           .       [1]
  --  5) |         [0]                   .                           .        ..
  --  6) |           [0]                 .                           .
  --  7) |             [0]               .                           .
  --  8) |               [0]             .                           .
  --  9) |                 [0]           .                           .
  -- 10) |                   [0]         .                           .
  -- 11) |                     [0]       .                           .
  -- 12) |                       [0]     .                           .
  -- 13) |                         [0]   .                           .
  -- 14) |                           [0] .                           .
  -- 15) |<---g_init_valid_delay(15)-->[0]                           .
  ------------------------------------------------------------------------------
  gen_dp_src_out_timer : for i in 0 to g_nof_inputs - 1 generate
    u_dp_src_out_timer : entity dp_lib.dp_src_out_timer
    generic map (
      g_init_valid_delay => i * c_vis_block_len,  -- relative to dp_fifo_sc_src_out_arr(i).valid
      g_block_period     => c_channel_block_length + g_inter_channel_delay,
      g_block_len        => c_vis_block_len
    )
    port map (
      rst                  => rst,
      clk                  => clk,

      init_valid_delay_ref => snk_in_arr(i).valid,

      snk_in               => dp_fifo_sc_src_out_arr(i),
      snk_out              => dp_fifo_sc_src_in_arr(i)
    );
  end generate;

  src_out_arr <= dp_fifo_sc_src_out_arr;
end str;
