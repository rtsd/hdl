
set_location_assignment PIN_K15 -to CLK
set_location_assignment PIN_J15 -to "CLK(n)"
set_location_assignment PIN_N12 -to ETH_CLK
set_location_assignment PIN_K14 -to PPS
set_location_assignment PIN_J14 -to "PPS(n)"
set_location_assignment PIN_Y36 -to SA_CLK
set_location_assignment PIN_Y35 -to "SA_CLK(n)"
set_location_assignment PIN_AH9 -to SB_CLK
set_location_assignment PIN_AH10 -to "SB_CLK(n)"


set_location_assignment PIN_A29 -to emif_0_example_design_mem_mem_a[0]
set_location_assignment PIN_B29 -to emif_0_example_design_mem_mem_a[1]
set_location_assignment PIN_H29 -to emif_0_example_design_mem_mem_a[2]
set_location_assignment PIN_G29 -to emif_0_example_design_mem_mem_a[3]
set_location_assignment PIN_D29 -to emif_0_example_design_mem_mem_a[4]
set_location_assignment PIN_E29 -to emif_0_example_design_mem_mem_a[5]
set_location_assignment PIN_C29 -to emif_0_example_design_mem_mem_a[6]
set_location_assignment PIN_C28 -to emif_0_example_design_mem_mem_a[7]
set_location_assignment PIN_E30 -to emif_0_example_design_mem_mem_a[8]
set_location_assignment PIN_D30 -to emif_0_example_design_mem_mem_a[9]
set_location_assignment PIN_B28 -to emif_0_example_design_mem_mem_a[10]
set_location_assignment PIN_A28 -to emif_0_example_design_mem_mem_a[11]
set_location_assignment PIN_H27 -to emif_0_example_design_mem_mem_a[12]
set_location_assignment PIN_E28 -to emif_0_example_design_mem_mem_a[13]

set_location_assignment PIN_K28 -to emif_0_example_design_mem_mem_act_n[0]
set_location_assignment PIN_C16 -to emif_0_example_design_mem_mem_alert_n[0]
set_location_assignment PIN_C27 -to emif_0_example_design_mem_mem_ba[0]
set_location_assignment PIN_A27 -to emif_0_example_design_mem_mem_ba[1]
set_location_assignment PIN_B26 -to emif_0_example_design_mem_mem_bg[0]
set_location_assignment PIN_L27 -to emif_0_example_design_mem_mem_bg[1]
set_location_assignment PIN_F28 -to emif_0_example_design_mem_mem_a[15]

set_location_assignment PIN_E24 -to emif_0_example_design_mem_mem_dq[64]     ;# was: MB_II_CB[0]
set_location_assignment PIN_J25 -to emif_0_example_design_mem_mem_dq[65]     ;# was: MB_II_CB[1]
set_location_assignment PIN_A25 -to emif_0_example_design_mem_mem_dq[66]     ;# was: MB_II_CB[2]
set_location_assignment PIN_G25 -to emif_0_example_design_mem_mem_dq[67]     ;# was: MB_II_CB[3]
set_location_assignment PIN_D25 -to emif_0_example_design_mem_mem_dq[68]     ;# was: MB_II_CB[4]
set_location_assignment PIN_K25 -to emif_0_example_design_mem_mem_dq[69]     ;# was: MB_II_CB[5]
set_location_assignment PIN_D24 -to emif_0_example_design_mem_mem_dq[70]     ;# was: MB_II_CB[6]
set_location_assignment PIN_F25 -to emif_0_example_design_mem_mem_dq[71]     ;# was: MB_II_CB[7]

set_location_assignment PIN_N27 -to emif_0_example_design_mem_mem_ck[0]      ;# was: MB_II_CK[0]  
set_location_assignment PIN_M28 -to emif_0_example_design_mem_mem_ck_n[0]    ;# was: MB_II_CK_n[0]
set_location_assignment PIN_K27 -to emif_0_example_design_mem_mem_CK[1]      ;# was: MB_II_CK[1]  
set_location_assignment PIN_J26 -to emif_0_example_design_mem_mem_CK_n[1]    ;# was: MB_II_CK_n[1]
set_location_assignment PIN_N28 -to emif_0_example_design_mem_mem_cke[0]     ;# was: MB_II_CKE[0] 
set_location_assignment PIN_P26 -to emif_0_example_design_mem_mem_cke[1]     ;# was: MB_II_CKE[1] 
#set_location_assignment PIN_P26 -to cke1_export
set_instance_assignment -name IO_STANDARD "SSTL-12" -to cke1_export

set_location_assignment PIN_K29 -to emif_0_example_design_mem_mem_cs_n[0]    ;# was: MB_II_CS[0]  
set_location_assignment PIN_H26 -to emif_0_example_design_mem_mem_cs_n[1]
#set_location_assignment PIN_H26 -to cs1_export
set_instance_assignment -name IO_STANDARD "SSTL-12" -to cs1_export


set_location_assignment PIN_A16 -to emif_0_example_design_mem_mem_dbi_n[0]   ;# was: MB_II_DM[0]
set_location_assignment PIN_M21 -to emif_0_example_design_mem_mem_dbi_n[1]   ;# was: MB_II_DM[1]
set_location_assignment PIN_K22 -to emif_0_example_design_mem_mem_dbi_n[2]   ;# was: MB_II_DM[2]
set_location_assignment PIN_D19 -to emif_0_example_design_mem_mem_dbi_n[3]   ;# was: MB_II_DM[3]
set_location_assignment PIN_G30 -to emif_0_example_design_mem_mem_dbi_n[4]   ;# was: MB_II_DM[4]
set_location_assignment PIN_R32 -to emif_0_example_design_mem_mem_dbi_n[5]   ;# was: MB_II_DM[5]
set_location_assignment PIN_G32 -to emif_0_example_design_mem_mem_dbi_n[6]   ;# was: MB_II_DM[6]
set_location_assignment PIN_AC32 -to emif_0_example_design_mem_mem_dbi_n[7]  ;# was: MB_II_DM[7]
set_location_assignment PIN_E25 -to emif_0_example_design_mem_mem_dbi_n[8]   ;# was: MB_II_DM[8]

set_location_assignment PIN_F17 -to emif_0_example_design_mem_mem_dqs[0]
set_location_assignment PIN_L20 -to emif_0_example_design_mem_mem_dqs[1]
set_location_assignment PIN_J22 -to emif_0_example_design_mem_mem_dqs[2]
set_location_assignment PIN_B19 -to emif_0_example_design_mem_mem_dqs[3]
set_location_assignment PIN_L31 -to emif_0_example_design_mem_mem_dqs[4]
set_location_assignment PIN_P31 -to emif_0_example_design_mem_mem_dqs[5]
set_location_assignment PIN_N33 -to emif_0_example_design_mem_mem_dqs[6]
set_location_assignment PIN_T33 -to emif_0_example_design_mem_mem_dqs[7]
set_location_assignment PIN_A26 -to emif_0_example_design_mem_mem_dqs[8]

set_location_assignment PIN_K30 -to emif_0_example_design_mem_mem_odt[0]

set_location_assignment PIN_R27 -to emif_0_example_design_mem_mem_odt[1]
#set_location_assignment PIN_R27 -to odt1_export
set_instance_assignment -name IO_STANDARD "SSTL-12" -to odt1_export

set_location_assignment PIN_R28 -to emif_0_example_design_mem_mem_par[0]
set_location_assignment PIN_G28 -to emif_0_example_design_mem_mem_a[16]

set_location_assignment PIN_J29 -to emif_0_example_design_pll_ref_clk_clk

set_location_assignment PIN_L28 -to emif_0_example_design_mem_mem_reset_n[0]

set_location_assignment PIN_J27 -to oct_oct_rzqin
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to oct_oct_rzqin
#set_instance_assignment -name IO_STANDARD "SSTL-12" -to oct_oct_rzqin

set_location_assignment PIN_F27 -to emif_0_example_design_mem_mem_a[14]
# qsfp leds
set_location_assignment PIN_BA33 -to emif_0_example_design_status_local_cal_fail
set_location_assignment PIN_BB33 -to emif_0_example_design_status_local_cal_success
set_location_assignment PIN_AV32 -to emif_0_example_design_tg_0_traffic_gen_fail
set_location_assignment PIN_AP31 -to emif_0_example_design_tg_0_traffic_gen_pass
set_location_assignment PIN_AT33 -to emif_0_example_design_tg_0_traffic_gen_timeout
set_location_assignment PIN_AF32 -to pll_locked_pll_locked
# testio(0)
set_location_assignment PIN_AN32 -to global_reset_reset_n 
# testio(1)
set_location_assignment PIN_AP32 -to testio1 
set_instance_assignment -name IO_STANDARD "1.8 V" -to testio1

# IO Standard Assignments from Gijs (excluding memory)
set_instance_assignment -name IO_STANDARD "1.8 V" -to ETH_CLK
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[1](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[1](n)"
set_instance_assignment -name IO_STANDARD LVDS -to SA_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "SA_CLK(n)"
set_instance_assignment -name IO_STANDARD LVDS -to SB_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "SB_CLK(n)"
set_instance_assignment -name IO_STANDARD LVDS -to BCK_REF_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "BCK_REF_CLK(n)"
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to WDI

#set_location_assignment PIN_AN32 -to TESTIO[0]
set_location_assignment PIN_AP32 -to TESTIO[1]
set_location_assignment PIN_AT30 -to TESTIO[2]
set_location_assignment PIN_BD31 -to TESTIO[3]
set_location_assignment PIN_AU30 -to TESTIO[4]
set_location_assignment PIN_BD30 -to TESTIO[5]

set_location_assignment PIN_AB12 -to VERSION[0]
set_location_assignment PIN_AB13 -to VERSION[1]
set_location_assignment PIN_BB30 -to WDI

# locations changed 30 sept
set_location_assignment PIN_K12 -to ETH_SGIN[0]
set_location_assignment PIN_J12 -to "ETH_SGIN[0](n)"
set_location_assignment PIN_AF33 -to ETH_SGIN[1]
set_location_assignment PIN_AE33 -to "ETH_SGIN[1](n)"
set_location_assignment PIN_H13 -to ETH_SGOUT[0]
set_location_assignment PIN_H12 -to "ETH_SGOUT[0](n)"
set_location_assignment PIN_AW31 -to ETH_SGOUT[1]
set_location_assignment PIN_AV31 -to "ETH_SGOUT[1](n)"

set_instance_assignment -name IO_STANDARD LVDS -to PPS
set_instance_assignment -name IO_STANDARD LVDS -to "PPS(n)"
set_instance_assignment -name IO_STANDARD LVDS -to CLK
set_instance_assignment -name IO_STANDARD LVDS -to "CLK(n)"

# Enable internal termination for LVDS inputs
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to PPS
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to CLK
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[1]

set_location_assignment PIN_V9 -to BCK_REF_CLK
set_location_assignment PIN_V10 -to "BCK_REF_CLK(n)"
set_location_assignment PIN_AL32 -to CLKUSR
set_location_assignment PIN_M16 -to MB_EVENT



set_location_assignment PIN_A17 -to emif_0_example_design_mem_mem_dq[0]
set_location_assignment PIN_B16 -to emif_0_example_design_mem_mem_dq[1]
set_location_assignment PIN_D16 -to emif_0_example_design_mem_mem_dq[2]
set_location_assignment PIN_A18 -to emif_0_example_design_mem_mem_dq[3]
set_location_assignment PIN_B18 -to emif_0_example_design_mem_mem_dq[4]
set_location_assignment PIN_C17 -to emif_0_example_design_mem_mem_dq[5]
set_location_assignment PIN_E18 -to emif_0_example_design_mem_mem_dq[6]
set_location_assignment PIN_F18 -to emif_0_example_design_mem_mem_dq[7]
set_location_assignment PIN_R22 -to emif_0_example_design_mem_mem_dq[8]
set_location_assignment PIN_J20 -to emif_0_example_design_mem_mem_dq[9]
set_location_assignment PIN_L21 -to emif_0_example_design_mem_mem_dq[10]
set_location_assignment PIN_M20 -to emif_0_example_design_mem_mem_dq[11]
set_location_assignment PIN_J21 -to emif_0_example_design_mem_mem_dq[12]
set_location_assignment PIN_P21 -to emif_0_example_design_mem_mem_dq[13]
set_location_assignment PIN_R20 -to emif_0_example_design_mem_mem_dq[14]
set_location_assignment PIN_N21 -to emif_0_example_design_mem_mem_dq[15]
set_location_assignment PIN_L22 -to emif_0_example_design_mem_mem_dq[16]
set_location_assignment PIN_G20 -to emif_0_example_design_mem_mem_dq[17]
set_location_assignment PIN_H21 -to emif_0_example_design_mem_mem_dq[18]
set_location_assignment PIN_N22 -to emif_0_example_design_mem_mem_dq[19]
set_location_assignment PIN_P22 -to emif_0_example_design_mem_mem_dq[20]
set_location_assignment PIN_F20 -to emif_0_example_design_mem_mem_dq[21]
set_location_assignment PIN_G21 -to emif_0_example_design_mem_mem_dq[22]
set_location_assignment PIN_F21 -to emif_0_example_design_mem_mem_dq[23]
set_location_assignment PIN_E19 -to emif_0_example_design_mem_mem_dq[24]
set_location_assignment PIN_B20 -to emif_0_example_design_mem_mem_dq[25]
set_location_assignment PIN_A20 -to emif_0_example_design_mem_mem_dq[26]
set_location_assignment PIN_G19 -to emif_0_example_design_mem_mem_dq[27]
set_location_assignment PIN_D20 -to emif_0_example_design_mem_mem_dq[28]
set_location_assignment PIN_E20 -to emif_0_example_design_mem_mem_dq[29]
set_location_assignment PIN_D17 -to emif_0_example_design_mem_mem_dq[30]
set_location_assignment PIN_C18 -to emif_0_example_design_mem_mem_dq[31]
set_location_assignment PIN_F30 -to emif_0_example_design_mem_mem_dq[32]
set_location_assignment PIN_L30 -to emif_0_example_design_mem_mem_dq[33]
set_location_assignment PIN_M30 -to emif_0_example_design_mem_mem_dq[34]
set_location_assignment PIN_C31 -to emif_0_example_design_mem_mem_dq[35]
set_location_assignment PIN_D31 -to emif_0_example_design_mem_mem_dq[36]
set_location_assignment PIN_H31 -to emif_0_example_design_mem_mem_dq[37]
set_location_assignment PIN_J31 -to emif_0_example_design_mem_mem_dq[38]
set_location_assignment PIN_F31 -to emif_0_example_design_mem_mem_dq[39]
set_location_assignment PIN_P32 -to emif_0_example_design_mem_mem_dq[40]
set_location_assignment PIN_R30 -to emif_0_example_design_mem_mem_dq[41]
set_location_assignment PIN_U31 -to emif_0_example_design_mem_mem_dq[42]
set_location_assignment PIN_W31 -to emif_0_example_design_mem_mem_dq[43]
set_location_assignment PIN_P29 -to emif_0_example_design_mem_mem_dq[44]
set_location_assignment PIN_P30 -to emif_0_example_design_mem_mem_dq[45]
set_location_assignment PIN_V31 -to emif_0_example_design_mem_mem_dq[46]
set_location_assignment PIN_R29 -to emif_0_example_design_mem_mem_dq[47]
set_location_assignment PIN_M33 -to emif_0_example_design_mem_mem_dq[48]
set_location_assignment PIN_J33 -to emif_0_example_design_mem_mem_dq[49]
set_location_assignment PIN_H33 -to emif_0_example_design_mem_mem_dq[50]
set_location_assignment PIN_H32 -to emif_0_example_design_mem_mem_dq[51]
set_location_assignment PIN_J32 -to emif_0_example_design_mem_mem_dq[52]
set_location_assignment PIN_K33 -to emif_0_example_design_mem_mem_dq[53]
set_location_assignment PIN_K32 -to emif_0_example_design_mem_mem_dq[54]
set_location_assignment PIN_L32 -to emif_0_example_design_mem_mem_dq[55]
set_location_assignment PIN_AB33 -to emif_0_example_design_mem_mem_dq[56]
set_location_assignment PIN_AA32 -to emif_0_example_design_mem_mem_dq[57]
set_location_assignment PIN_W32 -to emif_0_example_design_mem_mem_dq[58]
set_location_assignment PIN_U33 -to emif_0_example_design_mem_mem_dq[59]
set_location_assignment PIN_Y33 -to emif_0_example_design_mem_mem_dq[60]
set_location_assignment PIN_AA33 -to emif_0_example_design_mem_mem_dq[61]
set_location_assignment PIN_V33 -to emif_0_example_design_mem_mem_dq[62]
set_location_assignment PIN_Y32 -to emif_0_example_design_mem_mem_dq[63]

set_location_assignment PIN_E17 -to emif_0_example_design_mem_mem_dqs_n[0]
set_location_assignment PIN_K20 -to emif_0_example_design_mem_mem_dqs_n[1]
set_location_assignment PIN_H22 -to emif_0_example_design_mem_mem_dqs_n[2]
set_location_assignment PIN_C19 -to emif_0_example_design_mem_mem_dqs_n[3]
set_location_assignment PIN_M31 -to emif_0_example_design_mem_mem_dqs_n[4]
set_location_assignment PIN_N31 -to emif_0_example_design_mem_mem_dqs_n[5]
set_location_assignment PIN_P33 -to emif_0_example_design_mem_mem_dqs_n[6]
set_location_assignment PIN_T32 -to emif_0_example_design_mem_mem_dqs_n[7]
set_location_assignment PIN_B25 -to emif_0_example_design_mem_mem_dqs_n[8]


set_location_assignment PIN_AJ31 -to altera_reserved_tck
set_location_assignment PIN_AK18 -to altera_reserved_tdi
set_location_assignment PIN_AH31 -to altera_reserved_ntrst
set_location_assignment PIN_AM29 -to altera_reserved_tdo
#set_location_assignment PIN_AV33 -to ~ALTERA_DATA0~



set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[1]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[2]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[3]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[4]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[5]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[6]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[7]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[8]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[9]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[10]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[11]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[12]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[13]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_act_n[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_ba[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_ba[1]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_bg[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_bg[1]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[15]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V SSTL" -to emif_0_example_design_mem_mem_ck[0]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V SSTL" -to emif_0_example_design_mem_mem_CK[1]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_cke[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_cs_n[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_par[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[16]
#
#set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_pll_ref_clk_clk
set_instance_assignment -name IO_STANDARD "1.2 V" -to emif_0_example_design_pll_ref_clk_clk

set_instance_assignment -name IO_STANDARD "1.2 V" -to emif_0_example_design_mem_mem_reset_n[0]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_a[14]
set_instance_assignment -name IO_STANDARD "SSTL-12" -to emif_0_example_design_mem_mem_odt[0]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_alert_n[0]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[64]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[65]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[66]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[67]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[68]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[69]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[70]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[71]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[0]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[1]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[2]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[3]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[4]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[5]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[6]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[7]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dbi_n[8]

set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[0]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[1]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[2]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[3]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[4]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[5]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[6]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[7]
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.2-V POD" -to emif_0_example_design_mem_mem_dqs[8]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[0]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[1]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[2]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[3]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[4]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[5]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[6]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[7]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[8]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[9]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[10]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[11]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[12]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[13]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[14]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[15]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[16]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[17]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[18]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[19]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[20]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[21]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[22]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[23]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[24]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[25]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[26]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[27]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[28]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[29]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[30]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[31]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[32]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[33]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[34]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[35]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[36]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[37]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[38]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[39]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[40]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[41]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[42]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[43]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[44]

set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[45]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[46]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[47]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[48]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[49]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[50]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[51]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[52]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[53]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[54]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[55]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[56]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[57]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[58]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[59]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[60]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[61]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[62]
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to emif_0_example_design_mem_mem_dq[63]

