-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for the DDIO in and out simulation models
-- Description:
--               _   _   _   _
--     inclock  | |_| |_| |_| |_
--     datain    0 1 2 3 4 5 6 7
--     data_h        1   3   5
--     data_l        0   2   4
--     dataout           0 1 2 3 4 5 6 7
--
-- Usage:
--   The tb is self checking (p_verify) and self stopping (tb_end)
--
--   . Load the simulation by right mouse selecting the entity in library work
--   > as 3
--   > run -a

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_ip_arria10_e3sge3_ddio_1 is
end tb_ip_arria10_e3sge3_ddio_1;

architecture tb of tb_ip_arria10_e3sge3_ddio_1 is
  constant c_clk_period : time := 10 ns;

  signal tb_end      : std_logic := '0';
  signal clk         : std_logic := '1';
  signal in_dat      : std_logic;
  signal in_data     : std_logic_vector(0 downto 0);
  signal data_h      : std_logic_vector(0 downto 0);
  signal data_l      : std_logic_vector(0 downto 0);
  signal out_data    : std_logic_vector(0 downto 0);
  signal out_dat     : std_logic;
  signal out_dat_exp : std_logic;
begin
  tb_end <= '0', '1' after 100 * c_clk_period;

  clk <= not clk or tb_end after c_clk_period / 2;

  p_in : process
  begin
    -- 0
    in_dat <= '0';
    wait until falling_edge(clk);
    in_dat <= '0';
    wait until rising_edge(clk);
    -- 1
    in_dat <= '0';
    wait until falling_edge(clk);
    in_dat <= '1';
    wait until rising_edge(clk);
    -- 2
    in_dat <= '1';
    wait until falling_edge(clk);
    in_dat <= '0';
    wait until rising_edge(clk);
    -- 3
    in_dat <= '1';
    wait until falling_edge(clk);
    in_dat <= '1';
    wait until rising_edge(clk);
    -- 2
    in_dat <= '1';
    wait until falling_edge(clk);
    in_dat <= '0';
    wait until rising_edge(clk);
  end process;

  in_data(0) <= in_dat;

  u_ddio_in : entity work.ip_arria10_e3sge3_ddio_in_1
  port map (
    datain    => in_data,
    inclock   => clk,
    dataout_h => data_h,
    dataout_l => data_l
  );

  u_ddio_out : entity work.ip_arria10_e3sge3_ddio_out_1
  port map (
    dataout  => out_data,
    outclock => clk,
    datain_h => data_h,
    datain_l => data_l
  );

  out_dat <= out_data(0);

  out_dat_exp <= transport in_dat after c_clk_period * 1.5 + 1 ps;

  p_verify : process(clk)
  begin
    if falling_edge(clk) then
      assert out_dat = out_dat_exp
        report "tb_ip_arria10_e3sge3_ddio_1: Error, unexpeced data at falling edge";
    end if;
    if rising_edge(clk) then
      assert out_dat = out_dat_exp
        report "tb_ip_arria10_e3sge3_ddio_1: Error, unexpeced data at rising edge";
    end if;
  end process;
end tb;
