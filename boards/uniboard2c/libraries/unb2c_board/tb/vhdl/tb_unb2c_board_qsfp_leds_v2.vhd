-------------------------------------------------------------------------------
--
-- Copyright (C) 2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Test bench for unb2c_board_qsfp_leds
-- Description:
--   The test bench is self-stopping but not self-checking. Manually obeserve
--   in the wave window that:
--   1) factory image:
--      - green led is off
--      - red led toggles
--   2) user image
--      - red led is off
--      - green led toggles when any xon='0'
--      - green led is on continously when any xon='1'
--      - green led goes briefly off when any sop='1'
-- Usage:
--   > as 3
--   > run -a

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tb_unb2c_board_qsfp_leds_v2 is
end tb_unb2c_board_qsfp_leds_v2;

architecture tb of tb_unb2c_board_qsfp_leds_v2 is
  constant c_sim_factor        : natural := 100;  -- same as in unb2c_board_qsfp_leds_v2.vhd

  constant c_mm_clk_freq_hz    : natural := 125 * 10**6;
  constant c_mm_clk_period_ns  : natural := 10**9 / c_mm_clk_freq_hz;
  constant c_nof_mm_clk_per_us : natural := 1000 / c_mm_clk_period_ns;

  constant c_dp_clk_freq_hz    : natural := 125 * 10**6;
  constant c_dp_clk_period_ns  : natural := 10**9 / c_dp_clk_freq_hz;
  constant c_nof_dp_clk_per_us : natural := 1000 / c_dp_clk_period_ns;

  constant mm_clk_period       : time := c_mm_clk_period_ns * 1 ns;
  constant dp_clk_period       : time := c_dp_clk_period_ns * 1 ns;

  constant c_nof_qsfp       : natural := 6;
  constant c_nof_lanes      : natural := c_nof_qsfp * c_quad;

  signal tb_end                  : std_logic := '0';
  signal mm_rst                  : std_logic := '1';
  signal mm_clk                  : std_logic := '0';
  signal mm_pulse_ms             : std_logic := '0';
  signal dp_rst                  : std_logic := '1';
  signal dp_clk                  : std_logic := '0';
  signal dp_pps                  : std_logic := '0';

  signal ddr_I_cal_ok            : std_logic := '0';
  signal ddr_II_cal_ok           : std_logic := '0';

  signal tx_siso_arr             : t_dp_siso_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_siso_rst);
  signal tx_sosi_arr             : t_dp_sosi_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal rx_sosi_arr             : t_dp_sosi_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);

  signal dbg_xon_arr             : std_logic_vector(c_nof_lanes - 1 downto 0);
  signal dbg_tx_sop_arr          : std_logic_vector(c_nof_lanes - 1 downto 0);
  signal dbg_rx_sop_arr          : std_logic_vector(c_nof_lanes - 1 downto 0);

  signal factory_green_led_arr   : std_logic_vector(c_nof_qsfp - 1 downto 0);
  signal factory_red_led_arr     : std_logic_vector(c_nof_qsfp - 1 downto 0);

  signal user_green_led_arr      : std_logic_vector(c_nof_qsfp - 1 downto 0);
  signal user_red_led_arr        : std_logic_vector(c_nof_qsfp - 1 downto 0);

  -- Cannot use proc_common_gen_pulse() to create sop in array.
  -- proc_common_gen_pulse() works for dbg_sop, dbg_sosi.sop but not for dbg_sop_slv(I) or for tx_sosi_arr(I).sop.
  -- The compiler then gives Error: "(vcom-1450) Actual (indexed name) for formal "pulse" is not a static signal name"
  -- It does work if the array index is from a GENERATE statement, but it does not work when it is from a LOOP statement.
  signal dbg_sop      : std_logic;
  signal dbg_sop_slv  : std_logic_vector(c_nof_lanes - 1 downto 0);
  signal dbg_sosi : t_dp_sosi;
begin
  mm_clk <= not mm_clk or tb_end after mm_clk_period / 2;
  mm_rst <= '1', '0' after mm_clk_period * 7;
  dp_clk <= not dp_clk or tb_end after dp_clk_period / 2;
  dp_rst <= '1', '0' after dp_clk_period * 7;

  proc_common_gen_pulse(1, (c_nof_mm_clk_per_us * 10**3) / c_sim_factor, '1', mm_rst, mm_clk, mm_pulse_ms);
  proc_common_gen_pulse(1, (c_nof_dp_clk_per_us * 10**6) / c_sim_factor**2, '1', dp_rst, dp_clk, dp_pps);

  -- Ease observation of record fields in Wave window, by mapping them to a SLV
  dbg_xon_arr    <= func_dp_stream_arr_get(tx_siso_arr, "XON");
  dbg_tx_sop_arr <= func_dp_stream_arr_get(tx_sosi_arr, "SOP");
  dbg_rx_sop_arr <= func_dp_stream_arr_get(rx_sosi_arr, "SOP");

  p_stimuli : process
  begin
    tx_siso_arr <= (others => c_dp_siso_rst);
    tx_sosi_arr <= (others => c_dp_sosi_rst);
    rx_sosi_arr <= (others => c_dp_sosi_rst);
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 50);

    -- Toggle ddr calibration status
    ddr_I_cal_ok <= '1';
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 5);
    ddr_II_cal_ok <= '1';
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 50);
    ddr_I_cal_ok <= '0';
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 5);
    ddr_II_cal_ok <= '0';

    -- Switch on each lane
    for I in 0 to c_nof_lanes - 1 loop
      tx_siso_arr(I).xon <= '1';
      proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 10);
    end loop;
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 50);

    -- Issue the sop of a Tx packet on each lane
    for I in 0 to c_nof_lanes - 1 loop
      -- Cannot use proc_common_gen_pulse(), because index I in a LOOP is not static
      tx_sosi_arr(I).sop <= '1';
      wait until rising_edge(mm_clk);
      tx_sosi_arr(I).sop <= '0';
      proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 10);
    end loop;
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 50);

    -- Issue the sop of an Rx packet on each lane
    for I in 0 to c_nof_lanes - 1 loop
      -- Cannot use proc_common_gen_pulse(), because index I in a LOOP is not static
      rx_sosi_arr(I).sop <= '1';
      wait until rising_edge(mm_clk);
      rx_sosi_arr(I).sop <= '0';
      proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 10);
    end loop;
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 50);

    -- Switch off each lane
    for I in 0 to c_nof_lanes - 1 loop
      tx_siso_arr(I).xon <= '0';
      proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 10);
    end loop;
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 50);

    tb_end <= '1';
    proc_common_wait_some_pulses(mm_clk, mm_pulse_ms, 10);
    wait;
  end process;

  u_unb2c_factory_qsfp_leds_v2 : entity work.unb2c_board_qsfp_leds_v2
  generic map (
    g_sim             => true,  -- when true speed up led toggling in simulation
    g_factory_image   => true,  -- distinguish factory image and user images
    g_nof_qsfp        => c_nof_qsfp,  -- number of QSFP cages each with one dual led that can light red or green (or amber = red + green)
    g_mm_pulse_us     => c_nof_mm_clk_per_us,  -- nof mm_clk cycles to get us period<
    g_dp_pulse_us     => c_nof_dp_clk_per_us   -- nof dp_clk cycles to get us period<
  )
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
    dp_pps            => dp_pps,
    -- ddr status
    ddr_I_cal_ok      => ddr_I_cal_ok,
    ddr_II_cal_ok     => ddr_II_cal_ok,
    -- leds
    green_led_arr     => factory_green_led_arr,
    red_led_arr       => factory_red_led_arr
  );

  u_unb2c_user_qsfp_leds_v2 : entity work.unb2c_board_qsfp_leds_v2
  generic map (
    g_sim             => true,  -- when true speed up led toggling in simulation
    g_factory_image   => false,  -- distinguish factory image and user images
    g_nof_qsfp        => c_nof_qsfp,  -- number of QSFP cages each with one dual led that can light red or green (or amber = red + green)
    g_mm_pulse_us     => c_nof_mm_clk_per_us,  -- nof mm_clk cycles to get us period<
    g_dp_pulse_us     => c_nof_dp_clk_per_us   -- nof dp_clk cycles to get us period<
  )
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
    dp_pps            => dp_pps,
    -- lane status
    tx_siso_arr       => tx_siso_arr,
    tx_sosi_arr       => tx_sosi_arr,
    rx_sosi_arr       => rx_sosi_arr,
    -- leds
    green_led_arr     => user_green_led_arr,
    red_led_arr       => user_red_led_arr
  );
end tb;
