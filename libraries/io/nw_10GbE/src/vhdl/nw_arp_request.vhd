-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Generate ARP request packets at a predefined interval.
-- Description:
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;

entity nw_arp_request is
  generic (
    g_period_s  : natural := 30
  );
  port (
    dp_pps  : in std_logic;
    dp_clk  : in std_logic;
    dp_rst  : in std_logic;

    src_in  : in  t_dp_siso := c_dp_siso_rst;
    src_out : out t_dp_sosi := c_dp_sosi_rst;

    arp_sha : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    arp_spa : in std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    arp_tpa : in std_logic_vector(c_network_ip_addr_w - 1 downto 0)

  );
end nw_arp_request;

architecture rtl of nw_arp_request is
  constant c_data_w : natural := c_xgmii_data_w;
  constant c_nof_hdr_fields : natural := 12;
  constant c_hdr_field_sel  : std_logic_vector(c_nof_hdr_fields - 1 downto 0) := (others => '0');
  constant c_hdr_field_arr : t_common_field_arr(c_nof_hdr_fields - 1 downto 0) := ( ( field_name_pad("eth_dst_mac"       ), "RW", 48, field_default(c_network_eth_bc_mac) ),  -- broadcast address
                                                                                  ( field_name_pad("eth_src_mac"       ), "RW", 48, field_default(0) ),  -- same as arp_sha
                                                                                  ( field_name_pad("eth_type"          ), "RW", 16, field_default(x"0806") ),  -- ARP type
                                                                                  ( field_name_pad("arp_htype"         ), "RW", 16, field_default(1) ),  -- eth type
                                                                                  ( field_name_pad("arp_ptype"         ), "RW", 16, field_default(x"0800") ),  -- IP type
                                                                                  ( field_name_pad("arp_hlen"          ), "RW",  8, field_default(6) ),  -- Mac length is 6 bytes
                                                                                  ( field_name_pad("arp_plen"          ), "RW",  8, field_default(4) ),  -- IP length is 4 bytes
                                                                                  ( field_name_pad("arp_operation"     ), "RW", 16, field_default(1) ),  -- operation is request
                                                                                  ( field_name_pad("arp_sha"           ), "RW", 48, field_default(0) ),  -- set later
                                                                                  ( field_name_pad("arp_spa"           ), "RW", 32, field_default(0) ),  -- set later
                                                                                  ( field_name_pad("arp_tha"           ), "RW", 48, field_default(0) ),  -- set to 0
                                                                                  ( field_name_pad("arp_tpa"           ), "RW", 32, field_default(0) ));  -- set later

  constant c_dp_field_blk_snk_data_w : natural := field_slv_len(c_hdr_field_arr);
  constant c_dp_field_blk_src_data_w : natural := c_data_w;

  signal valid_sha : std_logic := '0';
  signal valid_spa : std_logic := '0';
  signal valid_tpa : std_logic := '0';
  signal valid_arp : std_logic := '0';
  signal pps_cnt   : natural   := 0;
  signal trigger   : boolean   := false;

  signal dp_field_blk_snk_in      : t_dp_sosi := c_dp_sosi_rst;
  signal nxt_dp_field_blk_snk_in  : t_dp_sosi := c_dp_sosi_rst;
  signal dp_field_blk_snk_out     : t_dp_siso := c_dp_siso_rdy;
begin
  p_field_wires : process (arp_sha, arp_spa, arp_tpa)
  begin
    -- Set defaults
    loop_field_wires: for i in c_hdr_field_arr'range loop
      nxt_dp_field_blk_snk_in.data(field_hi(c_hdr_field_arr, i) downto field_lo(c_hdr_field_arr, i)) <= c_hdr_field_arr(i).default(c_hdr_field_arr(i).size-1 downto 0);
    end loop;
    -- Overwrite fields comming from inputs
    nxt_dp_field_blk_snk_in.data(field_hi(c_hdr_field_arr, "eth_src_mac") downto field_lo(c_hdr_field_arr, "eth_src_mac")) <= arp_sha;
    nxt_dp_field_blk_snk_in.data(field_hi(c_hdr_field_arr, "arp_sha")     downto field_lo(c_hdr_field_arr, "arp_sha")) <= arp_sha;
    nxt_dp_field_blk_snk_in.data(field_hi(c_hdr_field_arr, "arp_spa")     downto field_lo(c_hdr_field_arr, "arp_spa")) <= arp_spa;
    nxt_dp_field_blk_snk_in.data(field_hi(c_hdr_field_arr, "arp_tpa")     downto field_lo(c_hdr_field_arr, "arp_tpa")) <= arp_tpa;
  end process;

  -- pps counter process setting sop, eop and valid
  p_dp_pps : process (dp_clk, dp_rst)
  begin
    if dp_rst = '1' then
      valid_sha <= '0';
      valid_spa <= '0';
      valid_tpa <= '0';
      valid_arp <= '0';
      trigger <= false;
      pps_cnt <= 0;
      dp_field_blk_snk_in <= c_dp_sosi_rst;
    elsif rising_edge(dp_clk) then
      -- Use signal to indicate if ARP can be enabled.
      valid_sha <= vector_or(arp_sha);
      valid_spa <= vector_or(arp_spa);
      valid_tpa <= vector_or(arp_tpa);
      valid_arp <= valid_sha and valid_spa and valid_tpa;
      if valid_arp = '1' then
        dp_field_blk_snk_in <= nxt_dp_field_blk_snk_in;
        if trigger and dp_field_blk_snk_out.ready = '1' and dp_field_blk_snk_out.xon = '1' then
          trigger <= false;
          dp_field_blk_snk_in.sop   <= '1';
          dp_field_blk_snk_in.eop   <= '1';
          dp_field_blk_snk_in.valid <= '1';
        end if;

        if dp_pps = '1' then
          if pps_cnt < g_period_s - 1 then
            pps_cnt <= pps_cnt + 1;
          else
            pps_cnt <= 0;
            trigger <= true;
          end if;
        end if;
      else
        trigger <= false;
        pps_cnt <= 0;
        dp_field_blk_snk_in <= c_dp_sosi_rst;
      end if;
    end if;
  end process;

  -- dp_field_blk to convert the ARP packet SLV to multi-cycle
  u_dp_field_blk : entity dp_lib.dp_field_blk
  generic map (
    g_field_arr      => c_hdr_field_arr,
    g_field_sel      => c_hdr_field_sel,
    g_snk_data_w     => c_dp_field_blk_snk_data_w,
    g_src_data_w     => c_dp_field_blk_src_data_w,
    g_in_symbol_w    => c_byte_w,
    g_out_symbol_w   => c_byte_w
  )
  port map (
    dp_clk       => dp_clk,
    dp_rst       => dp_rst,

    mm_clk       => '0',
    mm_rst       => '0',

    snk_in       => dp_field_blk_snk_in,
    snk_out      => dp_field_blk_snk_out,

    src_out      => src_out,
    src_in       => src_in

  );
end rtl;
