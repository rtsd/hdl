-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Generate the sosi control for a block of data under flow control
-- Description:
--   When enabled a block of g_nof_data words is output via src_out under flow
--   control by the ready. The ready depends on g_use_src_in:
--
--   . If g_use_src_in=TRUE then the src_in.ready is used and snk_in is
--     ignored. This can be used as a block reference input parallel to a
--     group of user inputs.
--     The g_preserve_* generics do not apply, because the snk_in is ignored.
--
--   . If g_use_src_in=FALSE then the src_in is not used and the snk_in data,
--     re, im are passed on and the snk_in.valid is used as ready. This can
--     be used as a BSN source that creates sync, bsn, sop and eop from
--     snk_in.valid for snk_in.data that has data not valid gaps. The
--     dp_bsn_source is similar but only supports data that is always valid.
--     The g_preserve_* generics can be used to preserve the corresponding
--     snk_in fields or to use the local generated values for this fields.
--
--   The first active ready starts the dp_block_gen. The first output block
--   will have a src_out.sync and every g_nof_blk_per_sync another
--   src_out.sync. Each block is marked by src_out.sop and src_out.eop. The
--   sop also marks the BSN. The BSN is the block sequence number that
--   increments for every block.
--
--   The snk_in.sop and snk_in.eop are always ignored, because g_nof_data
--   will set the src_out.sop/eop blocks.
--
--   If g_preserve_sync=TRUE then src_out.sync = snk_in.sync, else the
--   src_out.sync is depends on the first ready and on g_nof_blk_per_sync.
--
--   If g_preserve_bsn=TRUE then src_out.bsn = snk_in.bsn, else the
--   src_out.bsn is depends on the first ready and the initial g_bsn. If
--   g_preserve_bsn=TRUE then g_nof_data needs to match the snk_in.sop/eop
--   blocks.
--
--   If g_preserve_channel=TRUE then src_out.channel = snk_in.channel, else
--   the src_out.channel = g_channel.
--
-- Remarks:
-- . Ready latency (RL) = 1
-- . Alternatively consider using dp_block_gen_valid_arr.vhd or
--   dp_block_reshape.vhd.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_block_gen is
  generic (
    g_use_src_in         : boolean := true;  -- when true use src_in.ready else use snk_in.valid for flow control
    g_nof_data           : positive := 1;  -- nof data per block
    g_nof_blk_per_sync   : positive := 8;
    g_empty              : natural := 0;
    g_channel            : natural := 0;
    g_error              : natural := 0;
    g_bsn                : natural := 0;
    g_preserve_sync      : boolean := false;
    g_preserve_bsn       : boolean := false;
    g_preserve_channel   : boolean := false
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    -- Streaming sink
    snk_out    : out t_dp_siso;  -- pass on src_in.xon, pass on or force snk_out.ready dependend on g_use_src_in
    snk_in     : in  t_dp_sosi := c_dp_sosi_rst;
    -- Streaming source
    src_in     : in  t_dp_siso := c_dp_siso_rdy;
    src_out    : out t_dp_sosi;
    -- MM control
    en         : in  std_logic := '1'
  );
end dp_block_gen;

architecture rtl of dp_block_gen is
  type t_state is (s_sop, s_data, s_eop);

  type t_reg is record  -- local registers
    state     : t_state;
    data_cnt  : natural range 0 to g_nof_data;
    blk_cnt   : natural range 0 to g_nof_blk_per_sync;
    bsn       : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
    src_out   : t_dp_sosi;
  end record;

  constant c_reg_rst  : t_reg := (s_sop, 0, 0, TO_DP_BSN(g_bsn), c_dp_sosi_rst);

  signal ready     : std_logic;

  -- Define the local registers in t_reg record
  signal r         : t_reg;
  signal nxt_r     : t_reg;
begin
  snk_out.ready <= src_in.ready when g_use_src_in = true else '1';  -- force snk_out.ready = '1' when src_in.ready is not used
  snk_out.xon   <= src_in.xon;  -- always pass on siso.xon

  src_out <= r.src_out;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(clk) then
      r <= nxt_r;
    end if;
  end process;

  ready <= src_in.ready when g_use_src_in = true else snk_in.valid;

  p_state : process(r, en, ready, snk_in)
  begin
    nxt_r <= r;

    if g_use_src_in = false then
      nxt_r.src_out.data  <= snk_in.data;
      nxt_r.src_out.re    <= snk_in.re;
      nxt_r.src_out.im    <= snk_in.im;
    end if;

    if g_preserve_sync = false then
      nxt_r.src_out.sync  <= '0';
    else
      nxt_r.src_out.sync  <= snk_in.sync;
    end if;

    nxt_r.src_out.valid <= '0';
    nxt_r.src_out.sop   <= '0';
    nxt_r.src_out.eop   <= '0';

    if g_preserve_bsn = true then
      nxt_r.src_out.bsn <= snk_in.bsn;
    end if;

    if g_preserve_channel = true then
      nxt_r.src_out.channel <= snk_in.channel;
    end if;

    case r.state is
      when s_sop =>
        nxt_r.data_cnt <= 0;  -- for clarity init data count to 0 (because it will become 1 anyway at sop)
        if en = '0' then  -- if disabled then reset block generator and remain in this state
          nxt_r.blk_cnt <= 0;
          nxt_r.bsn     <= TO_DP_BSN(g_bsn);
        else  -- enabled block generator
          if ready = '1' then  -- once enabled the complete block will be output dependent on the flow control
            -- use input sync or create local sync
            if g_preserve_sync = false then
              if r.blk_cnt = 0 then
                nxt_r.src_out.sync  <= '1';  -- use local sync for this block, local sync starts at first ready
              end if;
              if r.blk_cnt >= g_nof_blk_per_sync - 1 then  -- maintain local sync interval
                nxt_r.blk_cnt <= 0;
              else
                nxt_r.blk_cnt <= r.blk_cnt + 1;
              end if;
            end if;
            nxt_r.src_out.valid   <= '1';
            nxt_r.src_out.sop     <= '1';

            -- use input bsn or create local bsn
            if g_preserve_bsn = false then
              nxt_r.bsn             <= INCR_UVEC(r.bsn, 1);  -- increment local bsn for next block
              nxt_r.src_out.bsn     <= r.bsn;  -- use local bsn for this block
            end if;

            -- use input channel or create fixed local channel
            if g_preserve_channel = false then
              nxt_r.src_out.channel <= TO_DP_CHANNEL(g_channel);
            end if;

            if g_nof_data = 1 then
              nxt_r.src_out.eop   <= '1';  -- single word block
              nxt_r.src_out.empty <= TO_DP_EMPTY(g_empty);
              nxt_r.src_out.err   <= TO_DP_ERROR(g_error);
            elsif g_nof_data = 2 then
              nxt_r.data_cnt <= 1;  -- start of two word block
              nxt_r.state <= s_eop;
            else
              nxt_r.data_cnt <= 1;  -- start of multi word block
              nxt_r.state <= s_data;
            end if;
          end if;
        end if;
      when s_data =>
        if ready = '1' then
          nxt_r.data_cnt <= r.data_cnt + 1;
          nxt_r.src_out.valid <= '1';
          if r.data_cnt = g_nof_data - 2 then
            nxt_r.state <= s_eop;
          end if;
        end if;
      when others =>  -- s_eop
        if ready = '1' then
          nxt_r.src_out.valid <= '1';
          nxt_r.src_out.eop   <= '1';
          nxt_r.src_out.empty <= TO_DP_EMPTY(g_empty);
          nxt_r.src_out.err   <= TO_DP_ERROR(g_error);
          nxt_r.state <= s_sop;
        end if;
    end case;
  end process;
end rtl;
