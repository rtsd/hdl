#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist                                                                                        

vmap  altera_iopll_1930           ./work/

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e2sg_pll_clk25/sim"
  vlog  "$IP_DIR/../altera_iopll_1930/sim/ip_arria10_e2sg_pll_clk25_altera_iopll_1930_4orwjna.vo" -work altera_iopll_1930

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e2sg_pll_clk125/sim"
  vlog  "$IP_DIR/../altera_iopll_1930/sim/ip_arria10_e2sg_pll_clk125_altera_iopll_1930_rnfwfny.vo" -work altera_iopll_1930

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e2sg_pll_clk200/sim"
  vlog  "$IP_DIR/../altera_iopll_1930/sim/ip_arria10_e2sg_pll_clk200_altera_iopll_1930_f63mvhq.vo" -work altera_iopll_1930

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e2sg_jesd204b_rx_core_pll_200MHz/sim"
  vlog  "$IP_DIR/../altera_iopll_1930/sim/ip_arria10_e2sg_jesd204b_rx_core_pll_200MHz_altera_iopll_1930_ibzmqny.vo" -work altera_iopll_1930


