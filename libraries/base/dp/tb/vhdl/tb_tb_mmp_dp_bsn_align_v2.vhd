-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 2 march 2022
-- Purpose: Regression multi tb for mmp_dp_bsn_align_v2
-- Description:
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_mmp_dp_bsn_align_v2 is
end tb_tb_mmp_dp_bsn_align_v2;

architecture tb of tb_tb_mmp_dp_bsn_align_v2 is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_lost_input          : BOOLEAN := FALSE

  u_no_lost_input      : entity work.tb_mmp_dp_bsn_align_v2 generic map (false);
  u_one_lost_input     : entity work.tb_mmp_dp_bsn_align_v2 generic map (true);
end tb;
