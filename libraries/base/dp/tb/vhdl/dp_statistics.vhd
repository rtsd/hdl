-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Ndperlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Ndperlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Provide a generic DP bus checking stage for simulation.
-- Remark:
-- . EK 17 dec 2021:
--   . removed g_disable_failures, to simplify the component, because default
--     ERROR level is used in  practise
--   . removed g_check_data_rate_mbps, to simplify the component, because
--     measuring the average data rate  exactly is difficult and requires
--     receiving at least 2 snk_in.sops, to be able to derive mpbs from
--     valid_count / cycle_count between 2 snk_in.sops.
--   . I changed dp_done to end at eop (instead of at next sop), so that an
--     extra next packet is not needed, which reduces the sim time.
--

library IEEE, common_lib, work, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_statistics is
  generic (
    g_runtime_nof_packets      : natural;  -- Run the test bench for nof_packets before asserting tb_end
    g_runtime_timeout          : time;  -- Report Failure if g_runtime_nof_packets is not reached before this time
    g_check_nof_valid          : boolean := false;  -- True enables valid count checking at dp_done. Reports Failure in case of mismatch.
    g_check_nof_valid_ref      : natural := 0;  -- Reference (= expected) valid count
    g_dp_word_w                : natural := 32  -- Used to calculate data rate
   );
  port (
    dp_clk : in  std_logic := '0';
    dp_rst : in  std_logic;

    snk_in : in  t_dp_sosi;
    tb_end : out std_logic  -- To be used to stop test-bench generated clocks
  );
end dp_statistics;

architecture str of dp_statistics is
  signal packet_count         : natural := 0;
  signal nxt_packet_count     : natural := 0;

  signal valid_count          : natural := 0;
  signal nxt_valid_count      : natural := 0;

  signal timeout              : std_logic;

  signal dp_done               : std_logic := '0';
  signal nxt_dp_done           : std_logic;
begin
  -- Make sure tb_end will not cause dp_clk to be stopped before dp_done has
  -- been evaluated in p_dp_done_check
  tb_end <= dp_done when rising_edge(dp_clk);

  ------------------------------------------------------------------------------
  -- Counters
  ------------------------------------------------------------------------------
  nxt_packet_count <= packet_count + 1 when snk_in.sop = '1'  else packet_count;
  nxt_valid_count  <= valid_count + 1  when snk_in.valid = '1'ELSE valid_count;

  ------------------------------------------------------------------------------
  -- Assert timeout after user specified time
  -- . Note: Using AFTER [time] results in the simulation actually running up to
  --         the runtime. This is okay as tb_end='1' makes sure no signals are
  --         changing anymore so this does not take extra sim time.
  ------------------------------------------------------------------------------
  timeout <= '0', '1' after g_runtime_timeout;

  ------------------------------------------------------------------------------
  -- Assert dp_done if we've seen g_runtime_nof_packets packets
  ------------------------------------------------------------------------------
  nxt_dp_done <= '1' when packet_count = g_runtime_nof_packets and snk_in.eop = '1' else '0';

  ------------------------------------------------------------------------------
  -- On dp_done; do the checks defined in the generics
  ------------------------------------------------------------------------------
  p_dp_done_check: process(dp_clk)
  begin
    if timeout = '1' and dp_done = '0' then
      report "[dp_statistics] Timeout occured!"
        severity ERROR;  -- report ERROR to have Error in log
      report "[dp_statistics] Timeout occured!"
        severity FAILURE;  -- report FAILURE to stop simulation
    elsif dp_done = '1' then
      if rising_edge(dp_clk) then
        -- report valid count
        if g_check_nof_valid = true then
          if valid_count /= g_check_nof_valid_ref then
            report "[dp_statistics] Valid count " & integer'image(valid_count) & " does not match reference " & integer'image(g_check_nof_valid_ref)
              severity ERROR;
          else
            report "[dp_statistics] Valid count " & integer'image(valid_count) & " is OK"
              severity NOTE;  -- Note to show that the check indeed did happen
          end if;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Registers
  ------------------------------------------------------------------------------
  p_dp_clk: process(dp_clk, dp_rst)
  begin
    if dp_rst = '1' then
      packet_count <= 0;
      valid_count  <= 0;
      dp_done      <= '0';
    elsif(rising_edge(dp_clk)) then
      packet_count <= nxt_packet_count;
      valid_count  <= nxt_valid_count;
      dp_done      <= nxt_dp_done;
    end if;
  end process;
end str;
