-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use technology_lib.technology_pkg.all;
use WORK.tech_tse_pkg.all;

package tb_tech_tse_pkg is
  -- Test bench supported packet data types
  constant c_tb_tech_tse_data_type_symbols : natural := 0;
  constant c_tb_tech_tse_data_type_counter : natural := 1;
  constant c_tb_tech_tse_data_type_arp     : natural := 2;
  constant c_tb_tech_tse_data_type_ping    : natural := 3;  -- over IP/ICMP
  constant c_tb_tech_tse_data_type_udp     : natural := 4;  -- over IP

  function func_tech_tse_header_size(data_type : natural) return natural;  -- raw ethernet: 4 header words, protocol ethernet: 11 header words

  -- Configure the TSE MAC
  procedure proc_tech_tse_setup(constant c_technology        : in  natural;
                                constant c_promis_en         : in  boolean;
                                constant c_tse_tx_fifo_depth : in  natural;
                                constant c_tse_rx_fifo_depth : in  natural;
                                constant c_tx_ready_latency  : in  natural;
                                constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                signal   psc_access          : out std_logic;
                                signal   mm_clk              : in  std_logic;
                                signal   mm_miso             : in  t_mem_miso;
                                signal   mm_mosi             : out t_mem_mosi);

  procedure proc_tech_tse_setup_stratixiv(constant c_promis_en         : in  boolean;
                                          constant c_tse_tx_fifo_depth : in  natural;
                                          constant c_tse_rx_fifo_depth : in  natural;
                                          constant c_tx_ready_latency  : in  natural;
                                          constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                          signal   psc_access          : out std_logic;
                                          signal   mm_clk              : in  std_logic;
                                          signal   mm_miso             : in  t_mem_miso;
                                          signal   mm_mosi             : out t_mem_mosi);

  procedure proc_tech_tse_setup_arria10(constant c_promis_en         : in  boolean;
                                        constant c_tse_tx_fifo_depth : in  natural;
                                        constant c_tse_rx_fifo_depth : in  natural;
                                        constant c_tx_ready_latency  : in  natural;
                                        constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                        signal   psc_access          : out std_logic;
                                        signal   mm_clk              : in  std_logic;
                                        signal   mm_miso             : in  t_mem_miso;
                                        signal   mm_mosi             : out t_mem_mosi);

  procedure proc_tech_tse_tx_packet(constant total_header    : in  t_network_total_header;
                                    constant data_len        : in  natural;  -- in symbols = octets = bytes
                                    constant c_data_type     : in  natural;  -- c_tb_tech_tse_data_type_*
                                    constant c_ready_latency : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                                    constant c_nof_not_valid : in  natural;  -- when > 0 then pull tx valid low for c_nof_not_valid beats during tx
                                    signal   ff_clk          : in  std_logic;
                                    signal   ff_en           : in  std_logic;  -- similar purpose as c_nof_not_valid, but not used so pass on signal '1'
                                    signal   ff_src_in       : in  t_dp_siso;
                                    signal   ff_src_out      : out t_dp_sosi);

  -- Receive and verify packet from the TSE MAC
  procedure proc_tech_tse_rx_packet(constant total_header : in  t_network_total_header;
                                    constant c_data_type  : in  natural;  -- c_tb_tech_tse_data_type_*
                                    signal   ff_clk       : in  std_logic;
                                    signal   ff_snk_in    : in  t_dp_sosi;
                                    signal   ff_snk_out   : out t_dp_siso);
end tb_tech_tse_pkg;

package body tb_tech_tse_pkg is
  ------------------------------------------------------------------------------
  -- LOCAL ITEMS
  ------------------------------------------------------------------------------

  constant c_nof_eth_beats  : natural := c_network_total_header_32b_eth_nof_words;  -- nof words in eth part of the header
  constant c_nof_hdr_beats  : natural := c_network_total_header_32b_nof_words;  -- nof words in the total header

  -- Use default word addressing for MAC registers according to table 4.8, 4.9
  -- Use halfword addressing for PCS register to match table 4.17
  function func_map_pcs_addr(pcs_addr : natural) return natural is
  begin
    return pcs_addr * 2 + c_tech_tse_byte_addr_pcs_offset;
  end func_map_pcs_addr;

  ------------------------------------------------------------------------------
  -- GLOBAL ITEMS
  ------------------------------------------------------------------------------

  function func_tech_tse_header_size(data_type : natural) return natural is
  begin
    case data_type is
      when c_tb_tech_tse_data_type_symbols => return c_network_total_header_32b_eth_nof_words;
      when c_tb_tech_tse_data_type_counter => return c_network_total_header_32b_eth_nof_words;
      when others => null;
    end case;
    return c_network_total_header_32b_nof_words;
  end func_tech_tse_header_size;

  -- Configure the TSE MAC
  procedure proc_tech_tse_setup(constant c_technology        : in  natural;
                                constant c_promis_en         : in  boolean;
                                constant c_tse_tx_fifo_depth : in  natural;
                                constant c_tse_rx_fifo_depth : in  natural;
                                constant c_tx_ready_latency  : in  natural;
                                constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                signal   psc_access          : out std_logic;
                                signal   mm_clk              : in  std_logic;
                                signal   mm_miso             : in  t_mem_miso;
                                signal   mm_mosi             : out t_mem_mosi) is
  begin
    case c_technology is
      when c_tech_stratixiv      => proc_tech_tse_setup_stratixiv(c_promis_en, c_tse_tx_fifo_depth, c_tse_rx_fifo_depth, c_tx_ready_latency, src_mac, psc_access, mm_clk, mm_miso, mm_mosi);
      when c_tech_arria10_proto  => proc_tech_tse_setup_arria10(c_promis_en, c_tse_tx_fifo_depth, c_tse_rx_fifo_depth, c_tx_ready_latency, src_mac, psc_access, mm_clk, mm_miso, mm_mosi);
      when c_tech_arria10_e3sge3 => proc_tech_tse_setup_arria10(c_promis_en, c_tse_tx_fifo_depth, c_tse_rx_fifo_depth, c_tx_ready_latency, src_mac, psc_access, mm_clk, mm_miso, mm_mosi);
      when c_tech_arria10_e1sg   => proc_tech_tse_setup_arria10(c_promis_en, c_tse_tx_fifo_depth, c_tse_rx_fifo_depth, c_tx_ready_latency, src_mac, psc_access, mm_clk, mm_miso, mm_mosi);
      when c_tech_arria10_e2sg   => proc_tech_tse_setup_arria10(c_promis_en, c_tse_tx_fifo_depth, c_tse_rx_fifo_depth, c_tx_ready_latency, src_mac, psc_access, mm_clk, mm_miso, mm_mosi);
      when others                => proc_tech_tse_setup_stratixiv(c_promis_en, c_tse_tx_fifo_depth, c_tse_rx_fifo_depth, c_tx_ready_latency, src_mac, psc_access, mm_clk, mm_miso, mm_mosi);  -- default to c_tech_stratixiv
    end case;
  end proc_tech_tse_setup;

  -- . The src_mac[47:0] = 0x123456789ABC for MAC address 12-34-56-78-9A-BC
  procedure proc_tech_tse_setup_stratixiv(constant c_promis_en         : in  boolean;
                                          constant c_tse_tx_fifo_depth : in  natural;
                                          constant c_tse_rx_fifo_depth : in  natural;
                                          constant c_tx_ready_latency  : in  natural;
                                          constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                          signal   psc_access          : out std_logic;
                                          signal   mm_clk              : in  std_logic;
                                          signal   mm_miso             : in  t_mem_miso;
                                          signal   mm_mosi             : out t_mem_mosi) is
    constant c_mac0       : integer := TO_SINT(hton(src_mac(47 downto 16), 4));
    constant c_mac1       : integer := TO_SINT(hton(src_mac(15 downto  0), 2));
  begin
    -- PSC control
    psc_access <= '1';
    proc_mem_mm_bus_rd(func_map_pcs_addr(16#22#),           mm_clk, mm_miso, mm_mosi);  -- REV --> 0x0901
    proc_mem_mm_bus_wr(func_map_pcs_addr(16#28#), 16#0008#, mm_clk, mm_miso, mm_mosi);  -- IF_MODE <-- Force 1GbE, no autonegatiation
    proc_mem_mm_bus_rd(func_map_pcs_addr(16#00#),           mm_clk, mm_miso, mm_mosi);  -- CONTROL --> 0x1140
    proc_mem_mm_bus_rd(func_map_pcs_addr(16#02#),           mm_clk, mm_miso, mm_mosi);  -- STATUS --> 0x000D
    proc_mem_mm_bus_wr(func_map_pcs_addr(16#00#), 16#0140#, mm_clk, mm_miso, mm_mosi);  -- CONTROL <-- Auto negotiate disable
    psc_access <= '0';

    -- MAC control
    proc_mem_mm_bus_rd(16#000#, mm_clk, mm_miso, mm_mosi);  -- REV --> CUST_VERSION & 0x0901
    if c_promis_en = false then
      proc_mem_mm_bus_wr(16#008#, 16#0100004B#, mm_clk, mm_miso, mm_mosi);
    else
      proc_mem_mm_bus_wr(16#008#, 16#0100005B#, mm_clk, mm_miso, mm_mosi);
    end if;
      -- COMMAND_CONFIG <--
      -- Only the bits relevant to UniBoard are explained here, others are 0
      -- [    0] = TX_ENA             = 1, enable tx datapath
      -- [    1] = RX_ENA             = 1, enable rx datapath
      -- [    2] = XON_GEN            = 0
      -- [    3] = ETH_SPEED          = 1, enable 1GbE operation
      -- [    4] = PROMIS_EN          = 0, when 1 then receive all frames
      -- [    5] = PAD_EN             = 0, when 1 enable receive padding removal (requires ethertype=payload length)
      -- [    6] = CRC_FWD            = 1, enable receive CRC forward
      -- [    7] = PAUSE_FWD          = 0
      -- [    8] = PAUSE_IGNORE       = 0
      -- [    9] = TX_ADDR_INS        = 0, when 1 then MAX overwrites tx SRC MAC with mac_0,1 or one of the supplemental mac
      -- [   10] = HD_ENA             = 0
      -- [   11] = EXCESS_COL         = 0
      -- [   12] = LATE_COL           = 0
      -- [   13] = SW_RESET           = 0, when 1 MAC disables tx and rx, clear statistics and flushes receive FIFO
      -- [   14] = MHAS_SEL           = 0, select multicast address resolutions hash-code mode
      -- [   15] = LOOP_ENA           = 0
      -- [18-16] = TX_ADDR_SEL[2:0]   = 000, TX_ADDR_INS insert mac_0,1 or one of the supplemental mac
      -- [   19] = MAGIC_EN           = 0
      -- [   20] = SLEEP              = 0
      -- [   21] = WAKEUP             = 0
      -- [   22] = XOFF_GEN           = 0
      -- [   23] = CNT_FRM_ENA        = 0
      -- [   24] = NO_LGTH_CHECK      = 1, when 0 then check payload length of received frames (requires ethertype=payload length)
      -- [   25] = ENA_10             = 0
      -- [   26] = RX_ERR_DISC        = 0, when 1 then discard erroneous frames (requires store and forward mode, so rx_section_full=0)
      --                                   when 0 then pass on with rx_err[0]=1
      -- [   27] = DISABLE_RD_TIMEOUT = 0
      -- [30-28] = RSVD               = 000
      -- [   31] = CNT_RESET          = 0, when 1 clear statistics
    proc_mem_mm_bus_wr(16#00C#,       c_mac0, mm_clk, mm_miso, mm_mosi);  -- MAC_0
    proc_mem_mm_bus_wr(16#010#,       c_mac1, mm_clk, mm_miso, mm_mosi);  -- MAC_1 <-- SRC_MAC = 12-34-56-78-9A-BC
    proc_mem_mm_bus_wr(16#05C#, 16#0000000C#, mm_clk, mm_miso, mm_mosi);  -- TX_IPG_LENGTH <-- interpacket gap = 12
    --proc_mem_mm_bus_wr(16#014#, 16#000005EE#, mm_clk, mm_miso, mm_mosi);  -- FRM_LENGTH <-- receive max frame length = 1518
    proc_mem_mm_bus_wr(16#014#, 16#0000233A#, mm_clk, mm_miso, mm_mosi);  -- FRM_LENGTH <-- receive max frame length = 9018

    -- FIFO legenda:
    -- . Tx section full  = There is enough data in the FIFO to start reading it, when 0 then store and forward.
    -- . Rx section full  = There is enough data in the FIFO to start reading it, when 0 then store and forward.
    -- . Tx section empty = There is not much empty space anymore in the FIFO, warn user via ff_tx_septy
    -- . Rx section empty = There is not much empty space anymore in the FIFO, inform remote device via XOFF flow control
    -- . Tx almost full   = Assert ff_tx_a_full and deassert ff_tx_rdy. Furthermore TX_ALMOST_FULL = c_tx_ready_latency+3,
    --                      so choose 3 for zero tx ready latency
    -- . Rx almost full   = Assert ff_rx_a_full and if the user is not ready ff_rx_rdy then:
    --                      --> break off the reception with an error to avoid FIFO overflow
    -- . Tx almost empty  = Assert ff_tx_a_empty and if the FIFO does not contain a eop yet then:
    --                      --> break off the transmission with an error to avoid FIFO underflow
    -- . Rx almost empty  = Assert ff_rx_a_empty
    -- Typical FIFO values:
    -- . TX_SECTION_FULL  = 16   > 8   = TX_ALMOST_EMPTY
    -- . RX_SECTION_FULL  = 16   > 8   = RX_ALMOST_EMPTY
    -- . TX_SECTION_EMPTY = D-16 < D-3 = Tx FIFO depth - TX_ALMOST_FULL
    -- . RX_SECTION_EMPTY = D-16 < D-8 = Rx FIFO depth - RX_ALMOST_FULL
    -- . c_tse_tx_fifo_depth = 1 M9K = 256*32b = 1k * 8b is sufficient when the Tx user respects ff_tx_rdy, to store a complete
    --                         ETH packet would require 1518 byte, so 2 M9K = 2k * 8b
    -- . c_tse_rx_fifo_depth = 1 M9K = 256*32b = 1k * 8b is sufficient when the Rx user ff_rx_rdy is sufficiently active
    proc_mem_mm_bus_wr(16#01C#, c_tse_rx_fifo_depth - 16, mm_clk, mm_miso, mm_mosi);  -- RX_SECTION_EMPTY <-- default FIFO depth - 16, >3
    proc_mem_mm_bus_wr(16#020#,                     16, mm_clk, mm_miso, mm_mosi);  -- RX_SECTION_FULL  <-- default 16
    proc_mem_mm_bus_wr(16#024#, c_tse_tx_fifo_depth - 16, mm_clk, mm_miso, mm_mosi);  -- TX_SECTION_EMPTY <-- default FIFO depth - 16, >3
    proc_mem_mm_bus_wr(16#028#,                     16, mm_clk, mm_miso, mm_mosi);  -- TX_SECTION_FULL  <-- default 16, >~ 8 otherwise no tx
    proc_mem_mm_bus_wr(16#02C#,                      8, mm_clk, mm_miso, mm_mosi);  -- RX_ALMOST_EMPTY  <-- default 8
    proc_mem_mm_bus_wr(16#030#,                      8, mm_clk, mm_miso, mm_mosi);  -- RX_ALMOST_FULL   <-- default 8
    proc_mem_mm_bus_wr(16#034#,                      8, mm_clk, mm_miso, mm_mosi);  -- TX_ALMOST_EMPTY  <-- default 8
    proc_mem_mm_bus_wr(16#038#,   c_tx_ready_latency + 3, mm_clk, mm_miso, mm_mosi);  -- TX_ALMOST_FULL   <-- default 3

    proc_mem_mm_bus_rd(16#0E8#, mm_clk, mm_miso, mm_mosi);  -- TX_CMD_STAT --> 0x00040000 : [18]=1 TX_SHIFT16, [17]=0 OMIT_CRC
    proc_mem_mm_bus_rd(16#0EC#, mm_clk, mm_miso, mm_mosi);  -- RX_CMD_STAT --> 0x02000000 : [25]=1 RX_SHIFT16

    wait until rising_edge(mm_clk);
  end proc_tech_tse_setup_stratixiv;

  -- It is noticed that the arria10 variant needs longer setup time.
  procedure proc_tech_tse_setup_arria10(constant c_promis_en         : in  boolean;
                                        constant c_tse_tx_fifo_depth : in  natural;
                                        constant c_tse_rx_fifo_depth : in  natural;
                                        constant c_tx_ready_latency  : in  natural;
                                        constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                        signal   psc_access          : out std_logic;
                                        signal   mm_clk              : in  std_logic;
                                        signal   mm_miso             : in  t_mem_miso;
                                        signal   mm_mosi             : out t_mem_mosi) is
  begin
    proc_tech_tse_setup_stratixiv(c_promis_en, c_tse_tx_fifo_depth, c_tse_rx_fifo_depth, c_tx_ready_latency, src_mac, psc_access, mm_clk, mm_miso, mm_mosi);
    wait for 10 us;
    wait until rising_edge(mm_clk);
  end proc_tech_tse_setup_arria10;

  -- Transmit user packet
  -- . Use word aligned payload data, so with half word inserted before the 14 byte header
  -- . Packets can be send immediately after eachother so new sop directly after last eop
  -- . The word rate is controlled by respecting ready from the MAC
  procedure proc_tech_tse_tx_packet(constant total_header    : in  t_network_total_header;
                                    constant data_len        : in  natural;  -- in symbols = octets = bytes
                                    constant c_data_type     : in  natural;  -- c_tb_tech_tse_data_type_*
                                    constant c_ready_latency : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
                                    constant c_nof_not_valid : in  natural;  -- when > 0 then pull tx valid low for c_nof_not_valid beats during tx
                                    signal   ff_clk          : in  std_logic;
                                    signal   ff_en           : in  std_logic;  -- similar purpose as c_nof_not_valid, but not used so pass on signal '1'
                                    signal   ff_src_in       : in  t_dp_siso;
                                    signal   ff_src_out      : out t_dp_sosi) is
    constant c_eth_header     : t_network_eth_header := total_header.eth;
    constant c_arp_words_arr  : t_network_total_header_32b_arr := func_network_total_header_construct_arp( total_header.eth, total_header.arp);
    constant c_icmp_words_arr : t_network_total_header_32b_arr := func_network_total_header_construct_icmp(total_header.eth, total_header.ip, total_header.icmp);
    constant c_udp_words_arr  : t_network_total_header_32b_arr := func_network_total_header_construct_udp( total_header.eth, total_header.ip, total_header.udp);

    constant c_mod            : natural := data_len mod c_tech_tse_symbols_per_beat;
    constant c_nof_data_beats : natural := data_len   / c_tech_tse_symbols_per_beat + sel_a_b(c_mod, 1, 0);
    constant c_empty          : natural := sel_a_b(c_mod, c_tech_tse_symbols_per_beat - c_mod, 0);
    variable v_sym            : unsigned(c_tech_tse_symbol_w - 1 downto 0) := (others => '0');
    variable v_num            : unsigned(c_tech_tse_data_w - 1 downto 0) := (others => '0');
  begin
    ff_src_out.empty <= TO_DP_EMPTY(0);
    ----------------------------------------------------------------------------
    -- ETH Header
    -- . sop
    ff_src_out.data <= RESIZE_DP_DATA(c_udp_words_arr(0));  -- all arp, icmp and udp contain the same eth header, so it is ok to use c_udp_words_arr
    proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '1', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
    ff_src_out.data <= RESIZE_DP_DATA(c_udp_words_arr(1));  -- prepare data before loop, so proc_dp_stream_ready_latency can be called at start of the loops
    for I in 2 to c_nof_eth_beats - 1 loop
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
      ff_src_out.data <= RESIZE_DP_DATA(c_udp_words_arr(I));
    end loop;
    ----------------------------------------------------------------------------
    -- ETH higher layer headers
    if c_data_type = c_tb_tech_tse_data_type_arp then
      for I in c_nof_eth_beats to c_nof_hdr_beats - 2 loop
        proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
        ff_src_out.data <= RESIZE_DP_DATA(c_arp_words_arr(I));
      end loop;
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
      -- . eop
      ff_src_out.data <= RESIZE_DP_DATA(c_arp_words_arr(c_nof_hdr_beats - 1));
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '1', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
    elsif c_data_type = c_tb_tech_tse_data_type_ping or c_data_type = c_tb_tech_tse_data_type_udp then
      for I in c_nof_eth_beats to c_nof_hdr_beats - 1 loop
        proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
        case c_data_type is
          when c_tb_tech_tse_data_type_ping => ff_src_out.data <= RESIZE_DP_DATA(c_icmp_words_arr(I));
          when c_tb_tech_tse_data_type_udp =>  ff_src_out.data <= RESIZE_DP_DATA(c_udp_words_arr(I));
          when others => null;
        end case;
      end loop;
    end if;
    ----------------------------------------------------------------------------
    -- Data
    if c_data_type /= c_tb_tech_tse_data_type_arp then
      for I in 0 to c_nof_data_beats - 1 loop
        proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '0', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
        case c_data_type is
          when c_tb_tech_tse_data_type_counter =>
            -- data : X"00000001", X"00000002", X"00000003", etc
            v_num := v_num + 1;
            ff_src_out.data <= RESIZE_DP_DATA(std_logic_vector(v_num));
          when others =>
            -- data : X"01020304", X"05060708", X"090A0B0C", etc
            for J in c_tech_tse_symbols_per_beat - 1 downto 0 loop
              v_sym := v_sym + 1;
              ff_src_out.data((J + 1) * c_tech_tse_symbol_w - 1 downto J * c_tech_tse_symbol_w) <= std_logic_vector(v_sym);
            end loop;
        end case;
        -- tb : pull valid low for some time during the middle of the payload
        if c_nof_not_valid > 0 and I = c_nof_data_beats / 2 then
          ff_src_out.valid <= '0';
          for I in 0 to c_nof_not_valid loop wait until rising_edge(ff_clk); end loop;
          ff_src_out.valid <= '1';
        end if;
      end loop;
      --------------------------------------------------------------------------
      -- Last data
      if c_empty > 0 then
        -- Overwrite empty data
        ff_src_out.empty <= TO_DP_EMPTY(c_empty);
        for J in c_empty - 1 downto 0 loop
          ff_src_out.data((J + 1) * c_tech_tse_symbol_w - 1 downto J * c_tech_tse_symbol_w) <= (others => '0');
        end loop;
      end if;
      -- . eop
      proc_dp_stream_ready_latency(c_ready_latency, ff_clk, ff_src_in.ready, ff_en, '0', '1', '0', '1', ff_src_out.sync, ff_src_out.valid, ff_src_out.sop, ff_src_out.eop);
    end if;
    ----------------------------------------------------------------------------
    -- Initialize for next tx packet
    ff_src_out.data  <= TO_DP_DATA(0);
    ff_src_out.valid <= '0';
    ff_src_out.eop   <= '0';
    ff_src_out.empty <= TO_DP_EMPTY(0);
  end proc_tech_tse_tx_packet;

  -- Receive packet
  -- . Use word aligned payload data, so with half word inserted before the 14 byte header
  -- . Packets can be always be received, assume the user application is always ready
  -- . The CRC32 is also passed on to the user at eop.
  -- . Note that when empty/=0 then the CRC32 is not word aligned, so therefore use prev_data to be able
  --   to handle part of last data word in case empty/=0 at eop
  procedure proc_tech_tse_rx_packet(constant total_header : in  t_network_total_header;
                                    constant c_data_type  : in  natural;  -- c_tb_tech_tse_data_type_*
                                    signal   ff_clk       : in  std_logic;
                                    signal   ff_snk_in    : in  t_dp_sosi;
                                    signal   ff_snk_out   : out t_dp_siso) is
    constant c_eth_header     : t_network_eth_header := total_header.eth;
    constant c_arp_words_arr  : t_network_total_header_32b_arr := func_network_total_header_construct_arp( total_header.eth, total_header.arp);
    constant c_icmp_words_arr : t_network_total_header_32b_arr := func_network_total_header_construct_icmp(total_header.eth, total_header.ip, total_header.icmp);
    constant c_udp_words_arr  : t_network_total_header_32b_arr := func_network_total_header_construct_udp( total_header.eth, total_header.ip, total_header.udp);
    variable v_sym            : unsigned(c_tech_tse_symbol_w - 1 downto 0) := (others => '0');
    variable v_num            : unsigned(c_tech_tse_data_w - 1 downto 0) := (others => '0');
    variable v_empty          : natural;
    variable v_first          : boolean := true;
    variable v_data           : std_logic_vector(c_tech_tse_data_w - 1 downto 0);
    variable v_prev_data      : std_logic_vector(c_tech_tse_data_w - 1 downto 0);
  begin
    -- Keep ff_rx_snk_out.ready='1' and ff_rx_snk_out.xon='1' all the time
    ff_snk_out <= c_dp_siso_rdy;
    ----------------------------------------------------------------------------
    -- Verify ETH Header
    -- . wait for sop
    proc_dp_stream_valid_sop(ff_clk, ff_snk_in.valid, ff_snk_in.sop);
    assert ff_snk_in.data(31 downto 16) = X"0000"
      report "RX: Wrong ETH alignment half word not zero"
      severity ERROR;
    assert ff_snk_in.data(15 downto 0) = c_eth_header.dst_mac(47 downto 32)
      report "RX: Wrong ETH dst_mac_addr(47 downto 32)"
      severity ERROR;
    proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
    assert ff_snk_in.data(31 downto 0) = c_eth_header.dst_mac(31 downto 0)
      report "RX: Wrong ETH dst_mac_addr(31 downto 0)"
      severity ERROR;
    proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
    assert ff_snk_in.data(31 downto 0) = c_eth_header.src_mac(47 downto 16)
      report "RX: Wrong ETH src_mac_addr(47 downto 16)"
      severity ERROR;
    proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
    assert ff_snk_in.data(31 downto 16) = c_eth_header.src_mac(15 downto 0)
      report "RX: Wrong ETH src_mac_addr(15 downto 0)"
      severity ERROR;
    assert ff_snk_in.data(15 downto 0) = c_eth_header.eth_type
      report "RX: Wrong ETH ethertype"
      severity ERROR;
    ----------------------------------------------------------------------------
    -- Verify ETH higher layer headers
    if c_data_type = c_tb_tech_tse_data_type_arp then
      for I in c_nof_eth_beats to c_nof_hdr_beats - 1 loop
        proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
        assert ff_snk_in.data(31 downto 0) = c_arp_words_arr(I)
          report "RX: Wrong ARP response word"
          severity ERROR;
      end loop;
      -- . continue to eop
      while ff_snk_in.eop /= '1' loop
        proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
      end loop;
    elsif c_data_type = c_tb_tech_tse_data_type_ping or c_data_type = c_tb_tech_tse_data_type_udp then
      for I in c_nof_eth_beats to c_nof_hdr_beats - 1 loop
        proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
        if I /= c_network_total_header_32b_ip_header_checksum_wi then  -- do not verify tx ip header checksum
          case c_data_type is
            when c_tb_tech_tse_data_type_ping => assert ff_snk_in.data(31 downto 0) = c_icmp_words_arr(I) report "RX: Wrong IP/ICMP = PING response word" severity ERROR;
            when c_tb_tech_tse_data_type_udp =>  assert ff_snk_in.data(31 downto 0) = c_udp_words_arr(I)  report "RX: Wrong IP/UDP response word" severity ERROR;
            when others => null;
          end case;
        end if;
      end loop;
    end if;
    ----------------------------------------------------------------------------
    -- Verify DATA
    if c_data_type /= c_tb_tech_tse_data_type_arp then
      -- . continue to eop
      v_first := true;
      proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
      while ff_snk_in.eop /= '1' loop
        v_prev_data := v_data;
        v_data      := ff_snk_in.data(c_tech_tse_data_w - 1 downto 0);
        if v_first = false then
          case c_data_type is
            when c_tb_tech_tse_data_type_counter =>
              -- data : X"00000001", X"00000002", X"00000003", etc
              v_num := v_num + 1;
              if unsigned(v_prev_data) /= 0 then  -- do not verify zero padding
                assert unsigned(v_prev_data) = v_num
                  report "RX: Wrong data word"
                  severity ERROR;
              end if;
            when others =>
              -- data : X"01020304", X"05060708", X"090A0B0C", etc
              for J in c_tech_tse_symbols_per_beat - 1 downto 0 loop
                v_sym := v_sym + 1;
                if unsigned(v_prev_data((J + 1) * c_tech_tse_symbol_w - 1 downto J * c_tech_tse_symbol_w)) /= 0 then  -- do not verify zero padding
                  assert unsigned(v_prev_data((J + 1) * c_tech_tse_symbol_w - 1 downto J * c_tech_tse_symbol_w)) = v_sym
                    report "RX: Wrong data symbol"
                    severity ERROR;
                end if;
              end loop;
          end case;
        end if;
        v_first := false;
        proc_dp_stream_valid(ff_clk, ff_snk_in.valid);
      end loop;
      --------------------------------------------------------------------------
      -- Verify last DATA and CRC32 if empty/=0 else the last word is only the CRC32
      v_prev_data := v_data;
      v_data      := ff_snk_in.data(c_tech_tse_data_w - 1 downto 0);
      v_empty     := to_integer(unsigned(ff_snk_in.empty(c_tech_tse_empty_w - 1 downto 0)));
      if v_empty > 0 then
        for J in v_empty - 1 downto 0 loop
          v_prev_data((J + 1) * c_tech_tse_symbol_w - 1 downto J * c_tech_tse_symbol_w) := (others => '0');
        end loop;
        case c_data_type is
          when c_tb_tech_tse_data_type_counter =>
            -- data : X"00000001", X"00000002", X"00000003", etc
            v_num := v_num + 1;
            for J in v_empty - 1 downto 0 loop
              v_num((J + 1) * c_tech_tse_symbol_w - 1 downto J * c_tech_tse_symbol_w) := (others => '0');  -- force CRC32 symbols in last data word to 0
            end loop;
            if unsigned(v_prev_data) /= 0 then  -- do not verify zero padding
              assert unsigned(v_prev_data) = v_num
                report "RX: Wrong empty data word"
                severity ERROR;
            end if;
          when others =>
            -- data : X"01020304", X"05060708", X"090A0B0C", etc
            for J in c_tech_tse_symbols_per_beat - 1 downto v_empty loop  -- ignore CRC32 symbols in last data word
              v_sym := v_sym + 1;
              if unsigned(v_prev_data((J + 1) * c_tech_tse_symbol_w - 1 downto J * c_tech_tse_symbol_w)) /= 0 then  -- do not verify zero padding
                assert unsigned(v_prev_data((J + 1) * c_tech_tse_symbol_w - 1 downto J * c_tech_tse_symbol_w)) = v_sym
                  report "RX: Wrong empty data symbol"
                  severity ERROR;
              end if;
            end loop;
        end case;
      end if;
    end if;
    -- No verify on CRC32 word
  end proc_tech_tse_rx_packet;
end tb_tech_tse_pkg;
