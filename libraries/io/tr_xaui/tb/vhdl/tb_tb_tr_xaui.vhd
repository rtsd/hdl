-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi testbench for tr_xaui the 10G XAUI IO module
-- Description:
--   The multi-tb finishes when all instances have signaled tb_end.
-- Usage:
--   > as 5
--   > run -all

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_select_pkg.all;

entity tb_tb_tr_xaui is
end tb_tb_tr_xaui;

architecture tb of tb_tb_tr_xaui is
  constant c_tb_end_vec : std_logic_vector(31 downto 0) := (others => '1');
  signal   tb_end_vec   : std_logic_vector(31 downto 0) := c_tb_end_vec;  -- sufficiently long to fit all tb instances
  signal   tb_end       : std_logic := '0';
begin
-- g_technology         : NATURAL := c_tech_stratixiv;
-- g_tb_end             : BOOLEAN := TRUE;   -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
-- g_sim_level          : NATURAL := 0;      -- 0 = use IP; 1 = use fast serdes model

  u_tr_xaui_sim_level_0   : entity work.tb_tr_xaui generic map (c_tech_select_default, false, 0) port map (tb_end_vec(0));
  u_tr_xaui_sim_level_1   : entity work.tb_tr_xaui generic map (c_tech_select_default, false, 1) port map (tb_end_vec(1));

  tb_end <= '1' when tb_end_vec = c_tb_end_vec else '0';

  p_tb_end : process
  begin
    wait until tb_end = '1';
    wait for 1 ns;
    report "Multi tb simulation finished."
      severity FAILURE;
    wait;
  end process;
end tb;
