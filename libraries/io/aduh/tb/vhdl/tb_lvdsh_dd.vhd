-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_lvdsh_dd is
end tb_lvdsh_dd;

-- Usage:
-- > as 10
-- > run 10 us

architecture tb of tb_lvdsh_dd is
  constant c_config_clk_period  : time := 10 ns;
  constant c_sp_clk_period      : time := 1250 ps;  -- 800 MSps sample clock
  constant c_in_clk_period      : time := c_sp_clk_period * 2;  -- 400 MHz double data rate clock

  constant c_in_dat_w           : natural := 8;
  constant c_in_dat_delay_arr   : t_natural_arr(0 to c_in_dat_w - 1) := array_init(0, c_in_dat_w, 1);  -- nof must match g_dat_w
  constant c_in_clk_delay       : natural := c_in_dat_delay_arr(c_in_dat_w - 1) + 1;
  constant c_out_dat_w          : natural := 2 * c_in_dat_w;  -- hi & lo
  constant c_rx_big_endian      : boolean := true;
  constant c_rx_factor          : natural := 2;  -- 1, 2, 4, ... must be a power of 2 because of the mixed width FIFO
  constant c_rx_fifo_size       : natural := 32;  -- see common_fifo_dc_lock_control used in lvds_dd for comment
  constant c_rx_fifo_fill       : natural := 16;  -- see common_fifo_dc_lock_control used in lvds_dd for comment

  constant c_rx_clk_period      : time := c_rx_factor * c_in_clk_period;  -- 200 MHz data path processing clock

  signal tb_end        : std_logic := '0';
  signal config_rst    : std_logic := '1';
  signal config_clk    : std_logic := '1';

  signal sp_clk        : std_logic := '1';
  signal in_clk        : std_logic := '1';
  signal in_clk_en     : std_logic;
  signal in_clk_act    : std_logic;
  signal in_dat        : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal in_clk_rst    : std_logic;

  signal out_clk       : std_logic;
  signal out_dat_hi    : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal out_dat_lo    : std_logic_vector(c_in_dat_w - 1 downto 0);

  signal rx_rst        : std_logic := '1';
  signal rx_clk        : std_logic := '1';
  signal rx_dat        : std_logic_vector(c_rx_factor * c_out_dat_w - 1 downto 0);
  signal rx_val        : std_logic;

  signal rx_locked     : std_logic;
  signal rx_stable     : std_logic;
  signal rx_stable_ack : std_logic := '0';
begin
  config_clk <= not config_clk or tb_end after c_config_clk_period / 2;
  config_rst <= '1', '0' after c_config_clk_period * 7;

  sp_clk <=  not sp_clk                     or tb_end after c_sp_clk_period / 2;
  in_clk <= (not in_clk and not in_clk_rst) or tb_end after c_in_clk_period / 2;

  -- Briefly disable the in_clk to verify rx_locked and rx_stable
  tb_end <= '0',    '1' after c_in_clk_period * 10000;
  in_clk_en <= '1', '0' after c_in_clk_period * 1000,
                    '1' after c_in_clk_period * 1100,
                    '0' after c_in_clk_period * 1200,
                    '1' after c_in_clk_period * 1700,
                    '0' after c_in_clk_period * 2300,
                    '1' after c_in_clk_period * 2400;
  in_clk_act <= in_clk and in_clk_en;

  -- Pulse rx_stable_ack to acknowledge rx_stable and re-enable it
  rx_stable_ack <= '0', '1' after c_rx_clk_period * 100,
                        '0' after c_rx_clk_period * 101,
                        '1' after c_rx_clk_period * 1250,
                        '0' after c_rx_clk_period * 1251,
                        '1' after c_rx_clk_period * 3000,
                        '0' after c_rx_clk_period * 3001;

  rx_clk <= not rx_clk or tb_end after c_rx_clk_period / 2;
  rx_rst <= '1', '0' after c_rx_clk_period * 7;

  in_dat <= INCR_UVEC(in_dat, 1) when rising_edge(sp_clk);

  u_dut : entity work.lvdsh_dd
  generic map (
    g_in_dat_w         => c_in_dat_w,
    g_in_dat_delay_arr => c_in_dat_delay_arr,
    g_in_clk_delay     => c_in_clk_delay,
    g_rx_big_endian    => c_rx_big_endian,
    g_rx_factor        => c_rx_factor,
    g_rx_fifo_size     => c_rx_fifo_size,
    g_rx_fifo_fill     => c_rx_fifo_fill
  )
  port map (
    -- PHY input delay config clock
    config_rst    => config_rst,
    config_clk    => config_clk,

    -- PHY input interface
    in_clk        => in_clk_act,
    in_dat        => in_dat,
    in_clk_rst    => in_clk_rst,

    -- DD domain output interface (no FIFO)
    out_clk       => out_clk,
    out_dat_hi    => out_dat_hi,
    out_dat_lo    => out_dat_lo,

    -- DD --> Rx domain interface at in_clk rate or g_rx_factor lower rate (via FIFO)
    rx_rst        => rx_rst,
    rx_clk        => rx_clk,
    rx_dat        => rx_dat,
    rx_val        => rx_val,

    rx_locked     => rx_locked,
    rx_stable     => rx_stable,
    rx_stable_ack => rx_stable_ack
  );
end tb;
