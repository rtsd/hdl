--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Combine mac_10g and 10gbase_r for c_tech_arria10_e3sge3
-- Description
--
--   The clocks come from an external central fPLL:
--
--     tx_ref_clk_644 --> fPLL --> clk_312
--                                 clk_156, rst_156
--   Blockdiagram:
--
--                          312 156 644
--                _________   |  |  |   ____________
--                |       |   |  |  |   |          |
--                |       |<--/  |  \-->|          |
--                |       |<-----+----->|          |
--                |       |             |          |
--                |       |    XGMII    |          |
--     tx_snk --->|tech_  |------------>|tech_     |---> serial_tx
--     rx_src <---|mac_10g|<------------|10gbase_r |<--- serial_rx
--                |       |             |          |
--                |_______|--\       /--|__________|
--                    |      |       |
--                  mac_mm   |       |
--                           |       v
--                       (   v    xgmii_tx_ready)
--     tx_snk_out.xon <--(xgmii_link_status[1:0])
--
-- . g_direction:
--   "TX_RX" = Default support bidir
--   "TX_ONLY" = Uses a bidir MAC and connects the MAC Tx to the MAC RX.
--   "RX_ONLY" = Same as "TX_RX"
--   See tech_eth_10g_stratixiv.vhd for more details.
--
-- Remarks:
-- . xgmii_link_status:
--   When the xgmii_tx_ready from the 10gbase_r and the xgmii_link_status from
--   the mac_10g are both be OK then the tx_snk.xon is asserted to allow the
--   user data transmission.
--   The tb_tech_eth_10g reveals that xgmii_tx_ready goes high after some power
--   up time and then remains active independent of link_fault.
--   A link fault eg. due to rx disconnect is detected by the link fault status:
--     0 = OK
--     1 = local fault
--     2 = remote fault
--
--   From google search:
--     Link fault Operation
--     1) Device B detects loss of signal. Local fault is signaled by PHY of Device B to Device B.
--     2) Device B ceases transmission of MAC frames and transmits remote fault to Device A.
--     3) Device A receives remote fault from Device B.
--     4) Device A stops sending frames, continuously generates Idle.
--
--   Hence when the xgmii_link_status is OK then the other side is also OK so
--   then it is also appropriate to release tx_snk.xon.
--
--   The XGMII link status can be monitored via the reg_eth10 MM register:
--
--     addr  data[31:0]
--      0      [0] = tx_snk_out_arr(I).xon
--             [1] = xgmii_tx_ready_arr(I)
--           [3:2] = xgmii_link_status_arr(I)
--

library IEEE, common_lib, dp_lib, technology_lib, tech_mac_10g_lib, ip_arria10_e3sge3_eth_10g_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;
use work.tech_eth_10g_component_pkg.all;

entity tech_eth_10g_arria10_e3sge3 is
  generic (
    g_sim                 : boolean := false;
    g_sim_level           : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels        : natural := 1;
    g_direction           : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_pre_header_padding  : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_644   : in  std_logic := '0';  -- 644.531250 MHz for 10GBASE-R

    -- Data clocks
    clk_312          : in  std_logic := '0';
    clk_156          : in  std_logic := '0';
    rst_156          : in  std_logic := '0';

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    reg_eth10g_mosi  : in  t_mem_mosi;  -- ETH10G (link status register)
    reg_eth10g_miso  : out t_mem_miso;

    reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso   : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ clk_156
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- Serial
    serial_tx_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    serial_rx_arr    : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
end tech_eth_10g_arria10_e3sge3;

architecture str of tech_eth_10g_arria10_e3sge3 is
begin
  u_ip_arria10_e3sge3_eth_10g : ip_arria10_e3sge3_eth_10g
  generic map(
    g_sim                => g_sim,
    g_sim_level          => g_sim_level,
    g_nof_channels       => g_nof_channels,
    g_direction          => g_direction,
    g_pre_header_padding => g_pre_header_padding
  )
  port map(
    -- Transceiver PLL reference clock
    tr_ref_clk_644  => tr_ref_clk_644,

    -- Data clocks
    clk_312         => clk_312,
    clk_156         => clk_156,
    rst_156         => rst_156,

    -- MM
    mm_clk          => mm_clk,
    mm_rst          => mm_rst,

    mac_mosi        => mac_mosi,
    mac_miso        => mac_miso,

    reg_eth10g_mosi => reg_eth10g_mosi,
    reg_eth10g_miso => reg_eth10g_miso,

    reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi => reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi,
    reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso => reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso,

    -- ST
    tx_snk_in_arr  => tx_snk_in_arr,
    tx_snk_out_arr => tx_snk_out_arr,

    rx_src_out_arr => rx_src_out_arr,
    rx_src_in_arr  => rx_src_in_arr,

    -- Serial
    serial_tx_arr  => serial_tx_arr,
    serial_rx_arr  => serial_rx_arr
  );
end str;
