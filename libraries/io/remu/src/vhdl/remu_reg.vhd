-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

--  RO  read only  (no VHDL present to access HW in write mode)
--  WO  write only (no VHDL present to access HW in read mode)
--  WE  write event (=WO)
--  WR  write control, read control
--  RW  read status, write control
--  RC  read, clear on read
--  FR  FIFO read
--  FW  FIFO write
--
--  wi  Bits    R/W Name          Default  Description                 |REG_EPCS|
--  =============================================================================
--  0   [31..0] WO  reconfigure   0x0
--  1   [2..0]  WO  param
--  2   [0]     WE  read_param
--  3   [0]     WE  write_param
--  4   [23..0] RO  data_out
--  5   [23..0] WO  data_in
--  6   [0]     RO  busy
--  =============================================================================

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity remu_reg is
  generic (
    g_data_w : natural := 24
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;

    epcs_rst          : in  std_logic;
    epcs_clk          : in  std_logic;

    -- Memory Mapped Slave
    sla_in            : in  t_mem_mosi;
    sla_out           : out t_mem_miso;

    reconfigure       : out std_logic;
    read_param        : out std_logic;
    write_param       : out std_logic;
    param             : out std_logic_vector(2 downto 0);
    busy              : in  std_logic;
    data_out          : in  std_logic_vector(g_data_w - 1 downto 0);
    data_in           : out std_logic_vector(g_data_w - 1 downto 0)
    );
end remu_reg;

architecture rtl of remu_reg is
  constant c_mm_reg        : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(7),
                                         dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                         nof_dat  => 7,
                                         init_sl  => '0');

  -- For safety, some commands require specific words to be written
  constant c_cmd_reconfigure : std_logic_vector(c_word_w - 1 downto 0 ) := x"B007FAC7";  -- "Boot factory"

  signal mm_reconfigure      : std_logic;
  signal mm_read_param       : std_logic;
  signal mm_write_param      : std_logic;
  signal mm_param            : std_logic_vector(2 downto 0);
  signal mm_busy             : std_logic;
  signal mm_data_out         : std_logic_vector(g_data_w - 1 downto 0);
  signal mm_data_in          : std_logic_vector(g_data_w - 1 downto 0);
begin
  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out   <= c_mem_miso_rst;
      -- Write access, register values
      mm_reconfigure  <= '0';
      mm_read_param   <= '0';
      mm_write_param  <= '0';
      mm_param        <= (others => '0');
      mm_data_in      <= (others => '0');

    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write event defaults
      mm_read_param  <= '0';
      mm_write_param <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            if sla_in.wrdata(c_word_w - 1 downto 0) = c_cmd_reconfigure then
              mm_reconfigure <= '1';
            else
              mm_reconfigure <= '0';
            end if;
          when 1 =>
            mm_param <= sla_in.wrdata(2 downto 0);
          when 2 =>
            mm_read_param <= sla_in.wrdata(0);
          when 3 =>
            mm_write_param <= sla_in.wrdata(0);
          when 5 =>
            mm_data_in <= sla_in.wrdata(g_data_w - 1 downto 0);
          when others => null;  -- unused MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 4 =>
            sla_out.rddata(g_data_w - 1 downto 0) <= mm_data_out;
          when 6 =>
            sla_out.rddata(0) <= mm_busy;
          when others => null;  -- unused MM addresses
        end case;
      end if;
    end if;
  end process;

  u_spulse_write_param : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_write_param,
    in_busy   => OPEN,
    out_rst   => epcs_rst,
    out_clk   => epcs_clk,
    out_pulse => write_param
  );

  u_spulse_read_param : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_read_param,
    in_busy   => OPEN,
    out_rst   => epcs_rst,
    out_clk   => epcs_clk,
    out_pulse => read_param
  );

  u_spulse_reconfigure : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_reconfigure,
    in_busy   => OPEN,
    out_rst   => epcs_rst,
    out_clk   => epcs_clk,
    out_pulse => reconfigure
  );

  u_async_busy : entity common_lib.common_async
  generic map (
    g_rst_level => '0'
  )
  port map (
    rst  => mm_rst,
    clk  => mm_clk,
    din  => busy,
    dout => mm_busy
  );

  u_cross_param : entity common_lib.common_reg_cross_domain
  port map (
    in_rst      => mm_rst,
    in_clk      => mm_clk,
    in_dat      => mm_param,
    in_done     => OPEN,
    out_rst     => epcs_rst,
    out_clk     => epcs_clk,
    out_dat     => param,
    out_new     => open
  );

  u_cross_data_in : entity common_lib.common_reg_cross_domain
  port map (
    in_rst      => mm_rst,
    in_clk      => mm_clk,
    in_dat      => mm_data_in,
    in_done     => OPEN,
    out_rst     => epcs_rst,
    out_clk     => epcs_clk,
    out_dat     => data_in,
    out_new     => open
  );

  u_cross_data_out : entity common_lib.common_reg_cross_domain
  port map (
    in_rst      => epcs_rst,
    in_clk      => epcs_clk,
    in_dat      => data_out,
    in_done     => OPEN,
    out_rst     => mm_rst,
    out_clk     => mm_clk,
    out_dat     => mm_data_out,
    out_new     => open
  );
end rtl;
