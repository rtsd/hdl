-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_fanout_tree
-- Usage:
--   > as 10
--   > run -all
-- . The tb is self checking and asserts when the fanout data are not equal and
--   when the fanout data is not incrementing.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_lfsr_sequences_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_fanout_tree is
  generic (
    -- TB control
    g_random_in_en              : boolean := false;
    g_random_in_val             : boolean := false;
    -- DUT settings
    g_nof_stages                : positive := 2;  -- >= 1
    g_nof_output_per_cell       : positive := 2;  -- >= 1
    g_nof_output                : positive := 3;  -- >= 1 and <= g_nof_output_per_cell**g_nof_stages
    g_cell_pipeline_factor_arr  : t_natural_arr := (1, 2);  -- range: g_nof_stages-1 DOWNTO 0, stage g_nof_stages-1 is output stage. Value: stage factor to multiply with g_cell_pipeline_arr
    g_cell_pipeline_arr         : t_natural_arr := (1, 0)  -- range: g_nof_output_per_cell-1 DOWNTO 0. Value: 0 for wires, >0 for register stages
  );
end tb_common_fanout_tree;

architecture tb of tb_common_fanout_tree is
  constant clk_period          : time := 10 ns;

  constant c_rl                : natural := 1;
  constant c_init              : natural := 0;
  constant c_dat_w             : natural := 8;

  constant c_nof_output        : natural := g_nof_output_per_cell**g_nof_stages;
  constant c_tree_pipeline_arr : t_natural_arr(c_nof_output - 1 downto 0) := func_common_fanout_tree_pipelining(g_nof_stages, g_nof_output_per_cell, c_nof_output,
                                                                                                              g_cell_pipeline_factor_arr, g_cell_pipeline_arr);
  constant c_tree_pipeline_max : natural := largest(c_tree_pipeline_arr);

  type t_data_arr is array (integer range <>) of std_logic_vector(c_dat_w - 1 downto 0);

  signal tb_end              : std_logic := '0';
  signal rst                 : std_logic := '1';
  signal clk                 : std_logic := '1';
  signal ready               : std_logic := '1';
  signal verify_en           : std_logic := '0';
  signal random_0            : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1            : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences

  signal in_en               : std_logic := '1';
  signal cnt_en              : std_logic := '1';
  signal in_val              : std_logic := '0';
  signal in_dat              : std_logic_vector(c_dat_w - 1 downto 0);
  signal out_en_vec          : std_logic_vector(g_nof_output        - 1 downto 0);
  signal out_val_vec         : std_logic_vector(g_nof_output        - 1 downto 0);
  signal out_dat_vec         : std_logic_vector(g_nof_output * c_dat_w - 1 downto 0);
  signal out_dat_arr         : t_data_arr(g_nof_output - 1 downto 0);
  signal prev_out_dat_arr    : t_data_arr(g_nof_output - 1 downto 0);

  signal dbg_c_tree_pipeline_arr : t_natural_arr(c_nof_output - 1 downto 0) := c_tree_pipeline_arr;
  signal ref_en_vec             : std_logic_vector(g_nof_output - 1 downto 0);
  signal ref_val_vec            : std_logic_vector(g_nof_output - 1 downto 0);
  signal ref_dat_arr            : t_data_arr(      g_nof_output - 1 downto 0);
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  p_stimuli : process
  begin
    proc_common_wait_until_high(clk, in_en);
    proc_common_wait_until_high(clk, in_val);
    proc_common_wait_some_cycles(clk, c_tree_pipeline_max);

    verify_en <= '1';
    proc_common_wait_some_cycles(clk, 2**c_dat_w);

    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  in_en  <= '1' when g_random_in_en = false else random_0(random_0'high);
  cnt_en <= '1' when g_random_in_val = false else random_1(random_1'high);

  proc_common_gen_data(c_rl, c_init, rst, clk, cnt_en, ready, in_dat, in_val);

  dut : entity work.common_fanout_tree
  generic map (
    g_nof_stages               => g_nof_stages,
    g_nof_output_per_cell      => g_nof_output_per_cell,
    g_nof_output               => g_nof_output,
    g_cell_pipeline_factor_arr => g_cell_pipeline_factor_arr,
    g_cell_pipeline_arr        => g_cell_pipeline_arr,
    g_dat_w                    => c_dat_w
  )
  port map (
    clk           => clk,
    in_en         => in_en,
    in_dat        => in_dat,
    in_val        => in_val,
    out_en_vec    => out_en_vec,
    out_dat_vec   => out_dat_vec,
    out_val_vec   => out_val_vec
  );

  -- Verify data for fanout output 0
  proc_common_verify_data(c_rl, clk, verify_en, ready, out_val_vec(0), out_dat_arr(0), prev_out_dat_arr(0));

  gen_verify_tree : for i in g_nof_output - 1 downto 0 generate
    out_dat_arr(i) <= out_dat_vec((i + 1) * c_dat_w - 1 downto i * c_dat_w);

    -- Reference pipeline compensate
    ref_en_vec(i)  <= out_en_vec( i)'delayed((c_tree_pipeline_max - c_tree_pipeline_arr(i)) * clk_period);
    ref_val_vec(i) <= out_val_vec(i)'delayed((c_tree_pipeline_max - c_tree_pipeline_arr(i)) * clk_period);
    ref_dat_arr(i) <= out_dat_arr(i)'delayed((c_tree_pipeline_max - c_tree_pipeline_arr(i)) * clk_period);

    -- Verify fanout tree by comparing all fanout with output 0
    p_verify_out_dat_arr : process(clk)
    begin
      if rising_edge(clk) then
        if verify_en = '1' then
          assert ref_en_vec(i) = ref_en_vec(0)
            report "Error: wrong fanout enable result"
            severity ERROR;
          assert ref_val_vec(i) = ref_val_vec(0)
            report "Error: wrong fanout valid result"
            severity ERROR;
          assert ref_dat_arr(i) = ref_dat_arr(0)
            report "Error: wrong fanout data result"
            severity ERROR;
        end if;
      end if;
    end process;
  end generate;
end tb;
