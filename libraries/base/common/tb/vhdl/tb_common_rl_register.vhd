-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_rl_register.
--   The tb also uses a common_fifo_sc to investigate the relation between RL
--   and FIFO full to find out what almost full margin is needed to avoid FIFO
--   overflow.
-- Description:
--   The common_rl_register decreases RL=1 --> 0 and then increases RL=0 -->1.
--   The DUT consists of a FIFO with RL=1 and then common_rl_increase to
--   register the FIFO rd_req. The tb uses fifo_almost_full to force
--   rl_register_in_ready='1' to avoid FIFO overflow.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_lfsr_sequences_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_rl_register is
  generic (
    g_nof_blocks               : natural := 1000;  -- nof blocks to simulate
    g_random_enable            : boolean := true;  -- use TRUE for random input enable flow control
    g_random_ready             : boolean := true;  -- use TRUE for random output ready flow control
    g_fifo_size                : natural := 64;
    g_rl_register_en           : boolean := true;
    g_rl_register_hold_dat_en  : boolean := true
  );
end tb_common_rl_register;

-- Run 20 us, observe src_out_dat in wave window

architecture tb of tb_common_rl_register is
  constant clk_period         : time := 10 ns;

  constant c_dat_w            : natural := 16;
  constant c_random_w         : natural := 61;

  constant c_gen_rl           : natural := 1;  -- input generator has fixed RL = 1
  constant c_fifo_rl          : natural := 1;  -- FIFO has RL = 1 fixed
  constant c_rl_register_rl   : natural := 1;  -- common_rl_register has RL = 1 fixed

  constant c_fifo_af_margin   : natural := c_fifo_rl + sel_a_b(g_rl_register_en, 1, 0);

  signal rst                        : std_logic;
  signal clk                        : std_logic := '0';
  signal tb_end                     : std_logic := '0';

  signal fifo_out_ready             : std_logic;
  signal fifo_in_dat                : std_logic_vector(c_dat_w - 1 downto 0);
  signal fifo_in_val                : std_logic;

  signal fifo_emp                   : std_logic;
  signal fifo_ful                   : std_logic;
  signal fifo_almost_full           : std_logic := '0';
  signal fifo_usedw                 : std_logic_vector(ceil_log2(g_fifo_size) - 1 downto 0);

  signal fifo_in_ready              : std_logic;
  signal fifo_out_dat               : std_logic_vector(c_dat_w - 1 downto 0);
  signal fifo_out_val               : std_logic;

  signal rl_register_in_ready       : std_logic;
  signal rl_register_out_dat        : std_logic_vector(c_dat_w - 1 downto 0);
  signal rl_register_out_val        : std_logic;

  signal enable                     : std_logic := '1';
  signal random0                    : std_logic_vector(c_random_w - 1 downto 0) := (others => '0');
  signal random1                    : std_logic_vector(c_random_w   downto 0) := (others => '0');
  signal verify_en                  : std_logic := '1';

  signal prev_fifo_in_ready         : std_logic;
  signal prev_fifo_out_dat          : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev_rl_register_in_ready  : std_logic;
  signal prev_rl_register_out_dat   : std_logic_vector(c_dat_w - 1 downto 0);
begin
  rst <= '1', '0' after clk_period * 7;
  clk <= not clk or tb_end after clk_period / 2;

  tb_end    <= '0', '1' after clk_period * g_nof_blocks * g_fifo_size;
  verify_en <= '0', '1' after clk_period * (10 + c_random_w);

  random0 <= func_common_random(random0) when rising_edge(clk);
  random1 <= func_common_random(random1) when rising_edge(clk);

  ------------------------------------------------------------------------------
  -- Stimuli
  ------------------------------------------------------------------------------
  -- Flow control
  enable               <= random0(random0'high)                     when g_random_enable = true else '1';
  rl_register_in_ready <= random1(random1'high) or fifo_almost_full when g_random_ready = true else '1';

  -- Generate FIFO input with c_gen_rl = 1 and counter data starting at 0
  proc_common_gen_data(c_gen_rl, 0, rst, clk, enable, fifo_out_ready, fifo_in_dat, fifo_in_val);

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------
  -- Verify dut output incrementing data
  proc_common_verify_data(c_fifo_rl,        clk, verify_en, fifo_in_ready,        fifo_out_val,        fifo_out_dat,        prev_fifo_out_dat);
  proc_common_verify_data(c_rl_register_rl, clk, verify_en, rl_register_in_ready, rl_register_out_val, rl_register_out_dat, prev_rl_register_out_dat);

  -- Verify dut output stream ready - valid relation
  proc_common_verify_valid(c_fifo_rl,        clk, verify_en, fifo_in_ready,        prev_fifo_in_ready,        fifo_out_val);
  proc_common_verify_valid(c_rl_register_rl, clk, verify_en, rl_register_in_ready, prev_rl_register_in_ready, rl_register_out_val);

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  -- model input without ready flow control (anyway can not use NOT fifo_ful, because that stimuli come with RL=1 so will stop 1 cycle too late when FIFO is full)
  fifo_out_ready <= '1';

  u_fifo_sc : entity work.common_fifo_sc
  generic map (
    g_note_is_ful => true,  -- when TRUE report NOTE when FIFO goes full, fifo overflow is always reported as FAILURE
    g_dat_w       => c_dat_w,
    g_nof_words   => g_fifo_size,
    g_af_margin   => c_fifo_af_margin
  )
  port map (
    rst      => rst,
    clk      => clk,
    wr_dat   => fifo_in_dat,
    wr_req   => fifo_in_val,
    wr_ful   => fifo_ful,
    wr_aful  => fifo_almost_full,  -- get FIFO almost full to be used to force rl_register_in_ready
    rd_dat   => fifo_out_dat,
    rd_req   => fifo_in_ready,
    rd_emp   => fifo_emp,
    rd_val   => fifo_out_val,
    usedw    => fifo_usedw
  );

  -- RL 1 --> 0 --> 1
  u_rl_register : entity work.common_rl_register
  generic map (
    g_adapt       => g_rl_register_en,
    g_hold_dat_en => g_rl_register_hold_dat_en,
    g_dat_w       => c_dat_w
  )
  port map (
    rst           => rst,
    clk           => clk,
    -- ST sink: RL = 1
    snk_out_ready => fifo_in_ready,
    snk_in_dat    => fifo_out_dat,
    snk_in_val    => fifo_out_val,
    -- ST source: RL = 0
    src_in_ready  => rl_register_in_ready,
    src_out_dat   => rl_register_out_dat,
    src_out_val   => rl_register_out_val
  );
end tb;
