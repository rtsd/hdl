-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra

This readme describes how the PFB (= pfs + pft2) code of LOFAR1 on RSP that
was created in 2004 by W. Lubberhuizen / W. Poiesz, was ported to git for
LOFAR2.0/SDP on UniBoard2 in 2021 by E. Kooistra.

Contents
1) Comparison of LOFAR1 and APERTIF polyphase filterbank (PFB)
2) Porting LOFAR1 PFB code to LOFAR2.0
  a) pfs
  b) pft2
  c) Simulating tb/vhdl/tb_pft2.vhd
3) Create pfb2_unit.vhd that can replace wpfb_unit_dev.vhd
4) Remove () from filenames
5) Add generics


References:
[1] LOFAR1 RSP firmware SVN repository https://svn.astron.nl/Station/trunk/RSP/
[2] LOFAR1 pft2 reference files https://svn.astron.nl/Station/trunk/RSP/rsp/tb/tc/5.%20Datapath/5.2%20PFT/
[3] LOFAR1 firmware ported to LOFAR2.0 GIT repository https://git.astron.nl/rtsd/hdl/-/tree/master/applications/lofar1
[4] APERTIF DSP firmware (rTwoSDF, filter, fft, wpfb) in LOFAR2.0 GIT repository https://git.astron.nl/rtsd/hdl/-/tree/master/libraries/dsp
[5] APERTIF PFB MATLAB code in APERTIF firmware SVN repository https://svn.astron.nl/UniBoard_FP7/RadioHDL/trunk/applications/apertif/matlab/apertif_matlab_readme.txt



1) Comparison of LOFAR1 and APERTIF polyphase filterbank (PFB)
- Advantages of LOFAR1 PFB (= pfs + pft2) are:
  . used in LOFAR1
  . pft2 uses multipliers 100% whereas fft_r2_pipe in wpfb_unit_dev achieves 50%
  . pft2 has pft_switch to decorrelate the fft_separate crosstalk between the two real signal inputs
- Advantages of Apertif PFB (= wpfb_unit_dev) are:
  . used in APERTIF
  . already available in LOFAR2.0 SDP


  
2) Porting LOFAR1 PFB code to LOFAR2.0
- Porting from [1] to [3]

a) pfs
  * hdllib.cfg : copy simulation files to data/
  * src/vhdl/
    - pfs_coefsbuf(str).vhd :
      . use c_coefs_rom : t_c_mem for common_rom from common_lib.
      . use g_init_file => "data/pfs_coefsbuf_1024.hex" = Coeffs16384Kaiser-quant.dat
    - pfs_filter(rtl).vhd : use ported common_mult_add.vhd from common_mult_lib
  * tb/vhdl/tb_pfs.vhd : ==> simulates OK
    . added usage comment
    
b) pft2
  * hdllib.cfg : copy simulation files to data/
  * src/vhdl/
    - pft_bf(rtl).vhd :
      . use common_complex_add_sub from common_lib instead of common_caddsub
      . use common_fifo_sc from common_lib
    - pft_bf_fw(rtl).vhd : use common_add_sub from common_lib instead of common_caddsub
    - pft_buffer(rtl).vhd : use common_ram_r_w from common_lib instead of common_dpram
    - pft_separate(rtl).vhd : use common_add_sub from common_lib instead of common_caddsub
    - pft_stage(str).vhd : use common_complex_round from common_lib instead of common_cround
    - pft_tmult(rtl).vhd :
      . use c_twid_file = "data/twiddle_" & NATURAL'IMAGE(c_coeff_w) & "_" & NATURAL'IMAGE(g_index) & ".hex"
      . use c_twid_rom : t_c_mem for common_rom from common_lib.
      . use common_complex_round from common_lib instead of common_cround
      . use common_complex_mult from common_lib instead of common_cmult
    - pft_top(str).vhd : removed option c_use_coregen_pfft
  * tb/data
    . copied stimuli and expected results files from [2] to data/,
      the tc.tcl can create *.sig files that tc.m can use to created expected *.re and *.im files
  * tb/vhdl/tb_pft2.vhd : ==> compiles OK, input *.sig files not recreated yet.
    . g_tst_data_dir = "data/"

c) Simulating tb/vhdl/tb_pft2.vhd
  * Derived tc_sig.tcl from tc.tcl and copied constants.tcl and wg.tcl to tcl/ to generate the *sig files
  * Made tb_pft2.vhd self stopping and changed generics to g_pft_mode, g_name_x, g_name_y
  * Added multi tb_tb_pft2.vhd to verify ptf2 with all available *sig files.
  ==> tb_tb_pft2.vhd simulates OK using c_diff_max = 20 like in data/tc.tcl
  
  
  
3) Create pfb2_unit.vhd that can replace wpfb_unit_dev.vhd
  * pfb2.vhd = pfs + pft2
  * pfb2_unit.vhd = multiple instances of pfb2 + sst, similar as wpfb_unit_dev.vhd for wideband factor wb = 1.

  

4) Remove () from filenames
  * The () in the files names cause the 'mk' command in Modelsim to fail, 'mk compile' can
    handle () in file names, but 'mk' is also needed for efficient compilation.
  * Put entity and architecture in same file without () in file name.
  * Use _pkg instead of (pkg) in package file name to avoid () in file name.
  * Keep only the actually used files in the hdllib.cfg

  
  
5) Add generics and package constant defaults
  * pfs_pkg.vhd:
    - g_pfs_coef_dat_w, default is c_pfs_coef_w = 16, should match coefs in g_pfs_coefs_file
    - g_pfs_coefs_file, default is c_pfs_coefs_file = "data/pfs_coefsbuf_1024.hex"

  * pft2_pkg.vhd:
    - c_coeff_w in pft_tmult.vhd default is c_pft_twiddle_w = 16
    - g_stage_dat_w in pft.vhd, default is c_pft_stage_dat_w = 20
    - verified range of g_stage_dat_w in tb_pft2
    - verified switch_en input in tb_pft2
    
  * pfb2.vhd:
    - g_switch_en
    - g_stage_dat_w
    - g_pfs_bypass
