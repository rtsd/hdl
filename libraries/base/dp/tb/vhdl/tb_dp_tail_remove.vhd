-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_tail_remove is
  generic (
    g_data_w               : natural := 32;
    g_symbol_w             : natural := c_byte_w
  );
end tb_dp_tail_remove;

architecture tb of tb_dp_tail_remove is
  constant c_clk_period           : time    := 100 ns;
  constant c_rl                   : natural := 1;

  constant c_nof_frames : natural := 3;
  constant c_nof_symbols_per_frame : natural := 80;
  constant c_nof_symbols_per_tail  : natural := 12;

  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '0';

  signal in_en          : std_logic_vector(0 to 1) := (others => '1');
  signal in_siso_arr    : t_dp_siso_arr(0 to 1) := (others => c_dp_siso_rdy);
  signal in_sosi_arr    : t_dp_sosi_arr(0 to 1) := (others => c_dp_sosi_rst);

  signal concat_siso       : t_dp_siso := c_dp_siso_rdy;
  signal concat_sosi       : t_dp_sosi;

  signal detailed_siso       : t_dp_siso := c_dp_siso_rdy;
  signal detailed_sosi       : t_dp_sosi;
begin
  rst  <= '1', '0' after c_clk_period * 7;
  clk  <=  not clk or tb_end after c_clk_period / 2;

  -- Generate two streams:
  -- Stream 0 will mimic regular data stream
  -- Stream 1 will be the the tail that we're going to remove

  p_stream_0 : process
    variable v_bsn : natural := 0;
    variable v_symbol_init : natural := 0;
  begin
    wait for 10 * c_clk_period;
    wait until rising_edge(clk);

      for i in 0 to c_nof_frames loop
        proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w, v_symbol_init, c_nof_symbols_per_frame, v_bsn, '0', clk, in_en(0), in_siso_arr(0), in_sosi_arr(0));
        v_symbol_init := v_symbol_init + c_nof_symbols_per_frame;
        v_bsn := v_bsn + 1;
      end loop;

    wait;
  end process;

  p_stream_1 : process
    variable v_bsn : natural := 0;
    variable v_symbol_init : natural := 40;
  begin
    wait for 15 * c_clk_period;
    wait until rising_edge(clk);

      for i in 0 to c_nof_frames loop
        proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w, v_symbol_init, c_nof_symbols_per_tail, v_bsn, '0', clk, in_en(1), in_siso_arr(1), in_sosi_arr(1));
        v_symbol_init := v_symbol_init + c_nof_symbols_per_tail;
        v_bsn := v_bsn + 1;
      end loop;

    -- As the tail frame will allways be last we can end the TB here:
    tb_end <= '1';

    wait;
  end process;

  -- Concatenate the two streams so we can remove stream 1 (='tail')
  u_dp_concat : entity work.dp_concat
  generic map (
    g_data_w    => g_data_w,
    g_symbol_w  => g_symbol_w
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_out_arr => in_siso_arr,
    snk_in_arr  => in_sosi_arr,
    src_in      => concat_siso,
    src_out     => concat_sosi
  );

  -- Now feed the concatenated streams into dp_tail_remove to remove the tail
  u_dp_tail_remove : entity work.dp_tail_remove
  generic map (
    g_data_w    => g_data_w,
    g_symbol_w  => g_symbol_w,
    g_nof_symbols => c_nof_symbols_per_tail
  )
  port map (
    st_rst      => rst,
    st_clk      => clk,

    snk_out => concat_siso,
    snk_in  => concat_sosi,

    src_in => detailed_siso,
    src_out => detailed_sosi
  );
end tb;
