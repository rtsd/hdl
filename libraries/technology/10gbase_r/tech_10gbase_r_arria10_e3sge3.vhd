--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_arria10_e3sge3_phy_10gbase_r_altera_xcvr_native_a10_151;
library ip_arria10_e3sge3_phy_10gbase_r_4_altera_xcvr_native_a10_151;
library ip_arria10_e3sge3_phy_10gbase_r_12_altera_xcvr_native_a10_151;
library ip_arria10_e3sge3_phy_10gbase_r_24_altera_xcvr_native_a10_151;
library ip_arria10_e3sge3_phy_10gbase_r_48_altera_xcvr_native_a10_151;
library ip_arria10_e3sge3_transceiver_pll_10g_altera_xcvr_atx_pll_a10_151;
library ip_arria10_e3sge3_transceiver_reset_controller_1_altera_xcvr_reset_control_151;
library ip_arria10_e3sge3_transceiver_reset_controller_4_altera_xcvr_reset_control_151;
library ip_arria10_e3sge3_transceiver_reset_controller_12_altera_xcvr_reset_control_151;
library ip_arria10_e3sge3_transceiver_reset_controller_24_altera_xcvr_reset_control_151;
library ip_arria10_e3sge3_transceiver_reset_controller_48_altera_xcvr_reset_control_151;

library IEEE, tech_pll_lib, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;
use work.tech_10gbase_r_component_pkg.all;

entity tech_10gbase_r_arria10_e3sge3 is
  generic (
    g_sim                 : boolean := false;
    g_nof_channels        : natural := 1
  );
  port (
    -- MM
    mm_clk            : in  std_logic := '0';
    mm_rst            : in  std_logic := '0';
    reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso   : out t_mem_miso;

    -- Transceiver ATX PLL reference clock
    tr_ref_clk_644          : in  std_logic;  -- 644.531250 MHz

    -- XGMII clocksi
    clk_156                 : in std_logic;  -- 156.25 MHz
    rst_156                 : in std_logic;

    -- XGMII interface
    xgmii_tx_ready_arr      : out std_logic_vector(g_nof_channels - 1 downto 0);  -- can be used for xon flow control
    xgmii_rx_ready_arr      : out std_logic_vector(g_nof_channels - 1 downto 0);  -- typically leave not connected
    xgmii_tx_dc_arr         : in  t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit
    xgmii_rx_dc_arr         : out t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit

    -- PHY serial IO
    tx_serial_arr           : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_serial_arr           : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
end tech_10gbase_r_arria10_e3sge3;

architecture str of tech_10gbase_r_arria10_e3sge3 is
  -- FIXME check selection of g_nof_channels to be 1,4,12 or 24

  --CONSTANT c_nof_channels_per_ip : NATURAL := sel_a_b(g_nof_channels=24, 24, 1); -- only support single 1 or block of 12
  --CONSTANT c_nof_channels_per_ip : NATURAL := 24 WHEN g_nof_channels=24 ELSE 12 WHEN g_nof_channels=12 ELSE 1;
  constant c_nof_channels_per_ip : natural := g_nof_channels;

  constant IP_SIZE               : natural := c_nof_channels_per_ip;  -- short constant name alias to improve index readability
  constant IP_SIZE_DATA          : natural := IP_SIZE * c_xgmii_data_w;
  constant IP_SIZE_CONTROL       : natural := IP_SIZE * c_xgmii_nof_lanes;

  signal tx_serial_clk          : std_logic_vector(0 downto 0);

  signal tr_coreclkin           : std_logic_vector(0 downto 0);

  signal tx_parallel_data_arr   : t_xgmii_d_arr(g_nof_channels - 1 downto 0);  -- 64 bit
  signal rx_parallel_data_arr   : t_xgmii_d_arr(g_nof_channels - 1 downto 0);  -- 64 bit
  signal tx_control_arr         : t_xgmii_c_arr(g_nof_channels - 1 downto 0);  -- 8 bit
  signal rx_control_arr         : t_xgmii_c_arr(g_nof_channels - 1 downto 0);  -- 8 bit

  -- IP block SLV signals
  signal tx_serial_clk_slv        : std_logic_vector(g_nof_channels - 1 downto 0);
  signal tr_coreclkin_slv         : std_logic_vector(g_nof_channels - 1 downto 0);
  signal tx_parallel_data_arr_slv : std_logic_vector(g_nof_channels * c_xgmii_data_w - 1 downto 0);  -- 64 bit
  signal rx_parallel_data_arr_slv : std_logic_vector(g_nof_channels * c_xgmii_data_w - 1 downto 0);  -- 64 bit
  signal tx_control_arr_slv       : std_logic_vector(g_nof_channels * c_xgmii_nof_lanes - 1 downto 0);  -- 8 bit
  signal rx_control_arr_slv       : std_logic_vector(g_nof_channels * c_xgmii_nof_lanes - 1 downto 0);  -- 8 bit

  -- transceiver reset controller
  signal tx_analogreset_arr     : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '1');
  signal tx_digitalreset_arr    : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '1');
  signal rx_analogreset_arr     : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '1');
  signal rx_digitalreset_arr    : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '1');
  signal tx_cal_busy_arr        : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');
  signal rx_cal_busy_arr        : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');
  signal rx_is_lockedtodata_arr : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');

  signal cal_busy_arr           : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');

  -- transceiver ATX PLL for 10G
  signal atx_pll_powerdown_arr  : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');
  signal atx_pll_locked_arr     : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');
  signal atx_pll_locked         : std_logic;
  signal atx_pll_cal_busy       : std_logic;
begin
  -- Clocks
  tr_coreclkin(0) <= clk_156;

  gen_glue_logic : for I in 0 to g_nof_channels - 1 generate
    -- Reset controller
    cal_busy_arr(I) <= tx_cal_busy_arr(I) or atx_pll_cal_busy;

    -- On hardware use atx_pll_locked for all channels, in simulation model some timing difference between the channels
    gen_hw : if g_sim = false generate
      atx_pll_locked_arr(I) <= atx_pll_locked;
    end generate;

    gen_sim : if g_sim = true generate
      atx_pll_locked_arr(I) <= transport atx_pll_locked after tech_pll_clk_156_period * I;
    end generate;

    -- XGMII
    tx_parallel_data_arr(I) <= func_xgmii_d(xgmii_tx_dc_arr(I));
    tx_control_arr(I)       <= func_xgmii_c(xgmii_tx_dc_arr(I));

    xgmii_rx_dc_arr(I)      <= func_xgmii_dc(rx_parallel_data_arr(I), rx_control_arr(I));
  end generate;

  gen_phy_1 : if c_nof_channels_per_ip = 1 generate
    gen_channels : for I in 0 to g_nof_channels - 1 generate
      u_ip_arria10_e3sge3_phy_10gbase_r : ip_arria10_e3sge3_phy_10gbase_r
      port map (
        tx_analogreset          => tx_analogreset_arr(I downto I),
        tx_digitalreset         => tx_digitalreset_arr(I downto I),
        rx_analogreset          => rx_analogreset_arr(I downto I),
        rx_digitalreset         => rx_digitalreset_arr(I downto I),
        tx_cal_busy             => tx_cal_busy_arr(I downto I),
        rx_cal_busy             => rx_cal_busy_arr(I downto I),

        tx_serial_clk0          => tx_serial_clk,
        rx_cdr_refclk0          => tr_ref_clk_644,
        tx_serial_data          => tx_serial_arr(I downto I),
        rx_serial_data          => rx_serial_arr(I downto I),

        rx_is_lockedtoref       => OPEN,
        rx_is_lockedtodata      => rx_is_lockedtodata_arr(I downto I),

        tx_coreclkin            => tr_coreclkin,  -- 156.25 MHz
        rx_coreclkin            => tr_coreclkin,  -- 156.25 MHz

        tx_parallel_data        => tx_parallel_data_arr(I),
        rx_parallel_data        => rx_parallel_data_arr(I),
        tx_control              => tx_control_arr(I),
        rx_control              => rx_control_arr(I)

        --tx_err_ins              : in  std_logic                     := '0';             --              tx_err_ins.tx_err_ins
        --tx_enh_data_valid       : in  std_logic_vector(0 downto 0)  := (others => '0'); --       tx_enh_data_valid.tx_enh_data_valid
        --tx_enh_fifo_full        : out std_logic_vector(0 downto 0);                     --        tx_enh_fifo_full.tx_enh_fifo_full
        --tx_enh_fifo_pfull       : out std_logic_vector(0 downto 0);                     --       tx_enh_fifo_pfull.tx_enh_fifo_pfull
        --tx_enh_fifo_empty       : out std_logic_vector(0 downto 0);                     --       tx_enh_fifo_empty.tx_enh_fifo_empty
        --tx_enh_fifo_pempty      : out std_logic_vector(0 downto 0);                     --      tx_enh_fifo_pempty.tx_enh_fifo_pempty

        --rx_enh_data_valid       : out std_logic_vector(0 downto 0);                     --       rx_enh_data_valid.rx_enh_data_valid
        --rx_enh_fifo_full        : out std_logic_vector(0 downto 0);                     --        rx_enh_fifo_full.rx_enh_fifo_full
        --rx_enh_fifo_empty       : out std_logic_vector(0 downto 0);                     --       rx_enh_fifo_empty.rx_enh_fifo_empty
        --rx_enh_fifo_del         : out std_logic_vector(0 downto 0);                     --         rx_enh_fifo_del.rx_enh_fifo_del
        --rx_enh_fifo_insert      : out std_logic_vector(0 downto 0);                     --      rx_enh_fifo_insert.rx_enh_fifo_insert
        --rx_enh_highber          : out std_logic_vector(0 downto 0);                     --          rx_enh_highber.rx_enh_highber
        --rx_enh_blk_lock         : out std_logic_vector(0 downto 0);                     --         rx_enh_blk_lock.rx_enh_blk_lock

        --unused_tx_parallel_data : in  std_logic_vector(63 downto 0) := (others => '0'); -- unused_tx_parallel_data.unused_tx_parallel_data
        --unused_tx_control       : in  std_logic_vector(8 downto 0)  := (others => '0'); --       unused_tx_control.unused_tx_control
        --unused_rx_parallel_data : out std_logic_vector(63 downto 0);                    -- unused_rx_parallel_data.unused_rx_parallel_data
        --unused_rx_control       : out std_logic_vector(11 downto 0);                    --       unused_rx_control.unused_rx_control
      );

      u_ip_arria10_e3sge3_transceiver_reset_controller_1 : ip_arria10_e3sge3_transceiver_reset_controller_1
      port map (
        clock              => clk_156,
        reset              => rst_156,
        pll_powerdown      => atx_pll_powerdown_arr(I downto I),
        tx_analogreset     => tx_analogreset_arr(I downto I),
        tx_digitalreset    => tx_digitalreset_arr(I downto I),
        tx_ready           => xgmii_tx_ready_arr(I downto I),
        pll_locked         => atx_pll_locked_arr(I downto I),
        pll_select         => "0",  -- set to zero when using one PLL
        tx_cal_busy        => cal_busy_arr(I downto I),
        rx_analogreset     => rx_analogreset_arr(I downto I),
        rx_digitalreset    => rx_digitalreset_arr(I downto I),
        rx_ready           => xgmii_rx_ready_arr(I downto I),
        rx_is_lockedtodata => rx_is_lockedtodata_arr(I downto I),
        rx_cal_busy        => rx_cal_busy_arr(I downto I)
      );
    end generate;
  end generate;

  gen_phy_4 : if c_nof_channels_per_ip = 4 generate
    tx_serial_clk_slv <= (others => tx_serial_clk(0));
    tr_coreclkin_slv  <= (others => tr_coreclkin(0));

    gen_channels : for I in 0 to g_nof_channels - 1 generate
      tx_parallel_data_arr_slv((I + 1) * c_xgmii_data_w - 1 downto I * c_xgmii_data_w) <= tx_parallel_data_arr(I);
      tx_control_arr_slv((I + 1) * c_xgmii_nof_lanes - 1 downto I * c_xgmii_nof_lanes) <= tx_control_arr(I);

      rx_parallel_data_arr(I) <= rx_parallel_data_arr_slv((I + 1) * c_xgmii_data_w - 1 downto I * c_xgmii_data_w);
      rx_control_arr(I) <= rx_control_arr_slv((I + 1) * c_xgmii_nof_lanes - 1 downto I * c_xgmii_nof_lanes);
    end generate;

    u_ip_arria10_e3sge3_phy_10gbase_r_4 : ip_arria10_e3sge3_phy_10gbase_r_4
    port map (
      tx_analogreset          => tx_analogreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          tx_analogreset.tx_analogreset
      tx_digitalreset         => tx_digitalreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --         tx_digitalreset.tx_digitalreset
      rx_analogreset          => rx_analogreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          rx_analogreset.rx_analogreset
      rx_digitalreset         => rx_digitalreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --         rx_digitalreset.rx_digitalreset
      tx_cal_busy             => tx_cal_busy_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --             tx_cal_busy.tx_cal_busy
      rx_cal_busy             => rx_cal_busy_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --             rx_cal_busy.rx_cal_busy

      tx_serial_clk0          => tx_serial_clk_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          tx_serial_clk0.clk
      rx_cdr_refclk0          => tr_ref_clk_644,  -- in  std_logic                       := '0';             --          rx_cdr_refclk0.clk
      tx_serial_data          => tx_serial_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --          tx_serial_data.tx_serial_data
      rx_serial_data          => rx_serial_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          rx_serial_data.rx_serial_data

      --rx_is_lockedtoref       : out std_logic_vector(11 downto 0);                      --       rx_is_lockedtoref.rx_is_lockedtoref
      rx_is_lockedtodata      => rx_is_lockedtodata_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --      rx_is_lockedtodata.rx_is_lockedtodata

      tx_coreclkin            => tr_coreclkin_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --            tx_coreclkin.clk
      rx_coreclkin            => tr_coreclkin_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --            rx_coreclkin.clk

      tx_parallel_data        => tx_parallel_data_arr_slv(IP_SIZE_DATA - 1 downto 0),  -- in  std_logic_vector(1535 downto 0) := (others => '0'); --        tx_parallel_data.tx_parallel_data
      rx_parallel_data        => rx_parallel_data_arr_slv(IP_SIZE_DATA - 1 downto 0),  -- out std_logic_vector(1535 downto 0);                    --        rx_parallel_data.rx_parallel_data
      tx_control              => tx_control_arr_slv(IP_SIZE_CONTROL - 1 downto 0),  -- in  std_logic_vector(191 downto 0)  := (others => '0'); --              tx_control.tx_control
      rx_control              => rx_control_arr_slv(IP_SIZE_CONTROL - 1 downto 0)  -- out std_logic_vector(191 downto 0);                     --              rx_control.rx_control

      --tx_clkout               : out std_logic_vector(11 downto 0);                      --               tx_clkout.clk
      --rx_clkout               : out std_logic_vector(11 downto 0);                      --               rx_clkout.clk

      --tx_err_ins              : in  std_logic_vector(11 downto 0)   := (others => '0'); --              tx_err_ins.tx_err_ins
      --tx_enh_data_valid       : in  std_logic_vector(11 downto 0)   := (others => '0'); --       tx_enh_data_valid.tx_enh_data_valid
      --tx_enh_fifo_empty       : out std_logic_vector(11 downto 0);                      --       tx_enh_fifo_empty.tx_enh_fifo_empty
      --tx_enh_fifo_full        : out std_logic_vector(11 downto 0);                      --        tx_enh_fifo_full.tx_enh_fifo_full
      --tx_enh_fifo_pempty      : out std_logic_vector(11 downto 0);                      --      tx_enh_fifo_pempty.tx_enh_fifo_pempty
      --tx_enh_fifo_pfull       : out std_logic_vector(11 downto 0);                      --       tx_enh_fifo_pfull.tx_enh_fifo_pfull
      --tx_pma_div_clkout       : out std_logic_vector(11 downto 0);                      --       tx_pma_div_clkout.clk

      --rx_enh_blk_lock         : out std_logic_vector(11 downto 0);                      --         rx_enh_blk_lock.rx_enh_blk_lock
      --rx_enh_data_valid       : out std_logic_vector(11 downto 0);                      --       rx_enh_data_valid.rx_enh_data_valid
      --rx_enh_fifo_del         : out std_logic_vector(11 downto 0);                      --         rx_enh_fifo_del.rx_enh_fifo_del
      --rx_enh_fifo_empty       : out std_logic_vector(11 downto 0);                      --       rx_enh_fifo_empty.rx_enh_fifo_empty
      --rx_enh_fifo_full        : out std_logic_vector(11 downto 0);                      --        rx_enh_fifo_full.rx_enh_fifo_full
      --rx_enh_fifo_insert      : out std_logic_vector(11 downto 0);                      --      rx_enh_fifo_insert.rx_enh_fifo_insert
      --rx_enh_highber          : out std_logic_vector(11 downto 0);                      --          rx_enh_highber.rx_enh_highber

      --unused_rx_control       : out std_logic_vector(287 downto 0);                     --       unused_rx_control.unused_rx_control
      --unused_rx_parallel_data : out std_logic_vector(1535 downto 0);                    -- unused_rx_parallel_data.unused_rx_parallel_data
      --unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => '0'); --       unused_tx_control.unused_tx_control
      --unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
    );

    u_ip_arria10_e3sge3_transceiver_reset_controller_4 : ip_arria10_e3sge3_transceiver_reset_controller_4
    port map (
      clock              => clk_156,  -- : in  std_logic                     := '0';             --              clock.clk
      pll_locked         => atx_pll_locked_arr(0 downto 0),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --         pll_locked.pll_locked
      pll_powerdown      => atx_pll_powerdown_arr(0 downto 0),  -- : out std_logic_vector(0 downto 0);                     --      pll_powerdown.pll_powerdown
      pll_select         => "0",  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --         pll_select.pll_select
      reset              => rst_156,  -- : in  std_logic                     := '0';             --              reset.reset
      rx_analogreset     => rx_analogreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --     rx_analogreset.rx_analogreset
      rx_cal_busy        => rx_cal_busy_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); --        rx_cal_busy.rx_cal_busy
      rx_digitalreset    => rx_digitalreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --    rx_digitalreset.rx_digitalreset
      rx_is_lockedtodata => rx_is_lockedtodata_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); -- rx_is_lockedtodata.rx_is_lockedtodata
      rx_ready           => xgmii_rx_ready_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --           rx_ready.rx_ready
      tx_analogreset     => tx_analogreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --     tx_analogreset.tx_analogreset
      tx_cal_busy        => cal_busy_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); --        tx_cal_busy.tx_cal_busy
      tx_digitalreset    => tx_digitalreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --    tx_digitalreset.tx_digitalreset
      tx_ready           => xgmii_tx_ready_arr(IP_SIZE-1 downto 0)  -- : out std_logic_vector(11 downto 0)                     --           tx_ready.tx_ready
    );
  end generate;

  gen_phy_12 : if c_nof_channels_per_ip = 12 generate
    tx_serial_clk_slv <= (others => tx_serial_clk(0));
    tr_coreclkin_slv  <= (others => tr_coreclkin(0));

    gen_channels : for I in 0 to g_nof_channels - 1 generate
      tx_parallel_data_arr_slv((I + 1) * c_xgmii_data_w - 1 downto I * c_xgmii_data_w) <= tx_parallel_data_arr(I);
      tx_control_arr_slv((I + 1) * c_xgmii_nof_lanes - 1 downto I * c_xgmii_nof_lanes) <= tx_control_arr(I);

      rx_parallel_data_arr(I) <= rx_parallel_data_arr_slv((I + 1) * c_xgmii_data_w - 1 downto I * c_xgmii_data_w);
      rx_control_arr(I) <= rx_control_arr_slv((I + 1) * c_xgmii_nof_lanes - 1 downto I * c_xgmii_nof_lanes);
    end generate;

    u_ip_arria10_e3sge3_phy_10gbase_r_12 : ip_arria10_e3sge3_phy_10gbase_r_12
    port map (
      tx_analogreset          => tx_analogreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          tx_analogreset.tx_analogreset
      tx_digitalreset         => tx_digitalreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --         tx_digitalreset.tx_digitalreset
      rx_analogreset          => rx_analogreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          rx_analogreset.rx_analogreset
      rx_digitalreset         => rx_digitalreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --         rx_digitalreset.rx_digitalreset
      tx_cal_busy             => tx_cal_busy_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --             tx_cal_busy.tx_cal_busy
      rx_cal_busy             => rx_cal_busy_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --             rx_cal_busy.rx_cal_busy

      tx_serial_clk0          => tx_serial_clk_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          tx_serial_clk0.clk
      rx_cdr_refclk0          => tr_ref_clk_644,  -- in  std_logic                       := '0';             --          rx_cdr_refclk0.clk
      tx_serial_data          => tx_serial_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --          tx_serial_data.tx_serial_data
      rx_serial_data          => rx_serial_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          rx_serial_data.rx_serial_data

      --rx_is_lockedtoref       : out std_logic_vector(11 downto 0);                      --       rx_is_lockedtoref.rx_is_lockedtoref
      rx_is_lockedtodata      => rx_is_lockedtodata_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --      rx_is_lockedtodata.rx_is_lockedtodata

      tx_coreclkin            => tr_coreclkin_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --            tx_coreclkin.clk
      rx_coreclkin            => tr_coreclkin_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --            rx_coreclkin.clk

      tx_parallel_data        => tx_parallel_data_arr_slv(IP_SIZE_DATA - 1 downto 0),  -- in  std_logic_vector(1535 downto 0) := (others => '0'); --        tx_parallel_data.tx_parallel_data
      rx_parallel_data        => rx_parallel_data_arr_slv(IP_SIZE_DATA - 1 downto 0),  -- out std_logic_vector(1535 downto 0);                    --        rx_parallel_data.rx_parallel_data
      tx_control              => tx_control_arr_slv(IP_SIZE_CONTROL - 1 downto 0),  -- in  std_logic_vector(191 downto 0)  := (others => '0'); --              tx_control.tx_control
      rx_control              => rx_control_arr_slv(IP_SIZE_CONTROL - 1 downto 0)  -- out std_logic_vector(191 downto 0);                     --              rx_control.rx_control

      --tx_clkout               : out std_logic_vector(11 downto 0);                      --               tx_clkout.clk
      --rx_clkout               : out std_logic_vector(11 downto 0);                      --               rx_clkout.clk

      --tx_err_ins              : in  std_logic_vector(11 downto 0)   := (others => '0'); --              tx_err_ins.tx_err_ins
      --tx_enh_data_valid       : in  std_logic_vector(11 downto 0)   := (others => '0'); --       tx_enh_data_valid.tx_enh_data_valid
      --tx_enh_fifo_empty       : out std_logic_vector(11 downto 0);                      --       tx_enh_fifo_empty.tx_enh_fifo_empty
      --tx_enh_fifo_full        : out std_logic_vector(11 downto 0);                      --        tx_enh_fifo_full.tx_enh_fifo_full
      --tx_enh_fifo_pempty      : out std_logic_vector(11 downto 0);                      --      tx_enh_fifo_pempty.tx_enh_fifo_pempty
      --tx_enh_fifo_pfull       : out std_logic_vector(11 downto 0);                      --       tx_enh_fifo_pfull.tx_enh_fifo_pfull
      --tx_pma_div_clkout       : out std_logic_vector(11 downto 0);                      --       tx_pma_div_clkout.clk

      --rx_enh_blk_lock         : out std_logic_vector(11 downto 0);                      --         rx_enh_blk_lock.rx_enh_blk_lock
      --rx_enh_data_valid       : out std_logic_vector(11 downto 0);                      --       rx_enh_data_valid.rx_enh_data_valid
      --rx_enh_fifo_del         : out std_logic_vector(11 downto 0);                      --         rx_enh_fifo_del.rx_enh_fifo_del
      --rx_enh_fifo_empty       : out std_logic_vector(11 downto 0);                      --       rx_enh_fifo_empty.rx_enh_fifo_empty
      --rx_enh_fifo_full        : out std_logic_vector(11 downto 0);                      --        rx_enh_fifo_full.rx_enh_fifo_full
      --rx_enh_fifo_insert      : out std_logic_vector(11 downto 0);                      --      rx_enh_fifo_insert.rx_enh_fifo_insert
      --rx_enh_highber          : out std_logic_vector(11 downto 0);                      --          rx_enh_highber.rx_enh_highber

      --unused_rx_control       : out std_logic_vector(287 downto 0);                     --       unused_rx_control.unused_rx_control
      --unused_rx_parallel_data : out std_logic_vector(1535 downto 0);                    -- unused_rx_parallel_data.unused_rx_parallel_data
      --unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => '0'); --       unused_tx_control.unused_tx_control
      --unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
    );

    u_ip_arria10_e3sge3_transceiver_reset_controller_12 : ip_arria10_e3sge3_transceiver_reset_controller_12
    port map (
      clock              => clk_156,  -- : in  std_logic                     := '0';             --              clock.clk
      pll_locked         => atx_pll_locked_arr(0 downto 0),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --         pll_locked.pll_locked
      pll_powerdown      => atx_pll_powerdown_arr(0 downto 0),  -- : out std_logic_vector(0 downto 0);                     --      pll_powerdown.pll_powerdown
      pll_select         => "0",  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --         pll_select.pll_select
      reset              => rst_156,  -- : in  std_logic                     := '0';             --              reset.reset
      rx_analogreset     => rx_analogreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --     rx_analogreset.rx_analogreset
      rx_cal_busy        => rx_cal_busy_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); --        rx_cal_busy.rx_cal_busy
      rx_digitalreset    => rx_digitalreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --    rx_digitalreset.rx_digitalreset
      rx_is_lockedtodata => rx_is_lockedtodata_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); -- rx_is_lockedtodata.rx_is_lockedtodata
      rx_ready           => xgmii_rx_ready_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --           rx_ready.rx_ready
      tx_analogreset     => tx_analogreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --     tx_analogreset.tx_analogreset
      tx_cal_busy        => cal_busy_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); --        tx_cal_busy.tx_cal_busy
      tx_digitalreset    => tx_digitalreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --    tx_digitalreset.tx_digitalreset
      tx_ready           => xgmii_tx_ready_arr(IP_SIZE-1 downto 0)  -- : out std_logic_vector(11 downto 0)                     --           tx_ready.tx_ready
    );
  end generate;

  gen_phy_24 : if c_nof_channels_per_ip = 24 generate
    tx_serial_clk_slv <= (others => tx_serial_clk(0));
    tr_coreclkin_slv  <= (others => tr_coreclkin(0));

    gen_channels : for I in 0 to g_nof_channels - 1 generate
      tx_parallel_data_arr_slv((I + 1) * c_xgmii_data_w - 1 downto I * c_xgmii_data_w) <= tx_parallel_data_arr(I);
      tx_control_arr_slv((I + 1) * c_xgmii_nof_lanes - 1 downto I * c_xgmii_nof_lanes) <= tx_control_arr(I);

      rx_parallel_data_arr(I) <= rx_parallel_data_arr_slv((I + 1) * c_xgmii_data_w - 1 downto I * c_xgmii_data_w);
      rx_control_arr(I) <= rx_control_arr_slv((I + 1) * c_xgmii_nof_lanes - 1 downto I * c_xgmii_nof_lanes);
    end generate;

    u_ip_arria10_e3sge3_phy_10gbase_r_24 : ip_arria10_e3sge3_phy_10gbase_r_24
    port map (
      reconfig_write(0)       => reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi.wr,  -- in  std_logic_vector(0 downto 0)
      reconfig_read(0)        => reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi.rd,  -- in  std_logic_vector(0 downto 0)
      reconfig_address        => reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi.address(14 downto 0),  -- in  std_logic_vector(14 downto 0)
      reconfig_writedata      => reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi.wrdata(c_word_w - 1 downto 0),  -- in  std_logic_vector(31 downto 0)
      reconfig_readdata       => reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso.rddata(c_word_w - 1 downto 0),  -- out std_logic_vector(31 downto 0);
      reconfig_waitrequest(0) => reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso.waitrequest,  -- out std_logic_vector(0 downto 0);
      reconfig_clk(0)         => mm_clk,  -- in  std_logic_vector(0 downto 0)
      reconfig_reset(0)       => mm_rst,  -- in  std_logic_vector(0 downto 0)

      tx_analogreset          => tx_analogreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          tx_analogreset.tx_analogreset
      tx_digitalreset         => tx_digitalreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --         tx_digitalreset.tx_digitalreset
      rx_analogreset          => rx_analogreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          rx_analogreset.rx_analogreset
      rx_digitalreset         => rx_digitalreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --         rx_digitalreset.rx_digitalreset
      tx_cal_busy             => tx_cal_busy_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --             tx_cal_busy.tx_cal_busy
      rx_cal_busy             => rx_cal_busy_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --             rx_cal_busy.rx_cal_busy

      tx_serial_clk0          => tx_serial_clk_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          tx_serial_clk0.clk
      rx_cdr_refclk0          => tr_ref_clk_644,  -- in  std_logic                       := '0';             --          rx_cdr_refclk0.clk
      tx_serial_data          => tx_serial_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --          tx_serial_data.tx_serial_data
      rx_serial_data          => rx_serial_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          rx_serial_data.rx_serial_data

      --rx_is_lockedtoref       : out std_logic_vector(11 downto 0);                      --       rx_is_lockedtoref.rx_is_lockedtoref
      rx_is_lockedtodata      => rx_is_lockedtodata_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --      rx_is_lockedtodata.rx_is_lockedtodata

      tx_coreclkin            => tr_coreclkin_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --            tx_coreclkin.clk
      rx_coreclkin            => tr_coreclkin_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --            rx_coreclkin.clk

      tx_parallel_data        => tx_parallel_data_arr_slv(IP_SIZE_DATA - 1 downto 0),  -- in  std_logic_vector(1535 downto 0) := (others => '0'); --        tx_parallel_data.tx_parallel_data
      rx_parallel_data        => rx_parallel_data_arr_slv(IP_SIZE_DATA - 1 downto 0),  -- out std_logic_vector(1535 downto 0);                    --        rx_parallel_data.rx_parallel_data
      tx_control              => tx_control_arr_slv(IP_SIZE_CONTROL - 1 downto 0),  -- in  std_logic_vector(191 downto 0)  := (others => '0'); --              tx_control.tx_control
      rx_control              => rx_control_arr_slv(IP_SIZE_CONTROL - 1 downto 0)  -- out std_logic_vector(191 downto 0);                     --              rx_control.rx_control

      --tx_clkout               : out std_logic_vector(11 downto 0);                      --               tx_clkout.clk
      --rx_clkout               : out std_logic_vector(11 downto 0);                      --               rx_clkout.clk

      --tx_err_ins              : in  std_logic_vector(11 downto 0)   := (others => '0'); --              tx_err_ins.tx_err_ins
      --tx_enh_data_valid       : in  std_logic_vector(11 downto 0)   := (others => '0'); --       tx_enh_data_valid.tx_enh_data_valid
      --tx_enh_fifo_empty       : out std_logic_vector(11 downto 0);                      --       tx_enh_fifo_empty.tx_enh_fifo_empty
      --tx_enh_fifo_full        : out std_logic_vector(11 downto 0);                      --        tx_enh_fifo_full.tx_enh_fifo_full
      --tx_enh_fifo_pempty      : out std_logic_vector(11 downto 0);                      --      tx_enh_fifo_pempty.tx_enh_fifo_pempty
      --tx_enh_fifo_pfull       : out std_logic_vector(11 downto 0);                      --       tx_enh_fifo_pfull.tx_enh_fifo_pfull
      --tx_pma_div_clkout       : out std_logic_vector(11 downto 0);                      --       tx_pma_div_clkout.clk

      --rx_enh_blk_lock         : out std_logic_vector(11 downto 0);                      --         rx_enh_blk_lock.rx_enh_blk_lock
      --rx_enh_data_valid       : out std_logic_vector(11 downto 0);                      --       rx_enh_data_valid.rx_enh_data_valid
      --rx_enh_fifo_del         : out std_logic_vector(11 downto 0);                      --         rx_enh_fifo_del.rx_enh_fifo_del
      --rx_enh_fifo_empty       : out std_logic_vector(11 downto 0);                      --       rx_enh_fifo_empty.rx_enh_fifo_empty
      --rx_enh_fifo_full        : out std_logic_vector(11 downto 0);                      --        rx_enh_fifo_full.rx_enh_fifo_full
      --rx_enh_fifo_insert      : out std_logic_vector(11 downto 0);                      --      rx_enh_fifo_insert.rx_enh_fifo_insert
      --rx_enh_highber          : out std_logic_vector(11 downto 0);                      --          rx_enh_highber.rx_enh_highber

      --unused_rx_control       : out std_logic_vector(287 downto 0);                     --       unused_rx_control.unused_rx_control
      --unused_rx_parallel_data : out std_logic_vector(1535 downto 0);                    -- unused_rx_parallel_data.unused_rx_parallel_data
      --unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => '0'); --       unused_tx_control.unused_tx_control
      --unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
    );

    u_ip_arria10_e3sge3_transceiver_reset_controller_24 : ip_arria10_e3sge3_transceiver_reset_controller_24
    port map (
      clock              => clk_156,  -- : in  std_logic                     := '0';             --              clock.clk
      pll_locked         => atx_pll_locked_arr(0 downto 0),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --         pll_locked.pll_locked
      pll_powerdown      => atx_pll_powerdown_arr(0 downto 0),  -- : out std_logic_vector(0 downto 0);                     --      pll_powerdown.pll_powerdown
      pll_select         => "0",  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --         pll_select.pll_select
      reset              => rst_156,  -- : in  std_logic                     := '0';             --              reset.reset
      rx_analogreset     => rx_analogreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --     rx_analogreset.rx_analogreset
      rx_cal_busy        => rx_cal_busy_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); --        rx_cal_busy.rx_cal_busy
      rx_digitalreset    => rx_digitalreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --    rx_digitalreset.rx_digitalreset
      rx_is_lockedtodata => rx_is_lockedtodata_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); -- rx_is_lockedtodata.rx_is_lockedtodata
      rx_ready           => xgmii_rx_ready_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --           rx_ready.rx_ready
      tx_analogreset     => tx_analogreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --     tx_analogreset.tx_analogreset
      tx_cal_busy        => cal_busy_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); --        tx_cal_busy.tx_cal_busy
      tx_digitalreset    => tx_digitalreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --    tx_digitalreset.tx_digitalreset
      tx_ready           => xgmii_tx_ready_arr(IP_SIZE-1 downto 0)  -- : out std_logic_vector(11 downto 0)                     --           tx_ready.tx_ready
    );
  end generate;

  gen_phy_48 : if c_nof_channels_per_ip = 48 generate
    tx_serial_clk_slv <= (others => tx_serial_clk(0));
    tr_coreclkin_slv  <= (others => tr_coreclkin(0));

    gen_channels : for I in 0 to g_nof_channels - 1 generate
      tx_parallel_data_arr_slv((I + 1) * c_xgmii_data_w - 1 downto I * c_xgmii_data_w) <= tx_parallel_data_arr(I);
      tx_control_arr_slv((I + 1) * c_xgmii_nof_lanes - 1 downto I * c_xgmii_nof_lanes) <= tx_control_arr(I);

      rx_parallel_data_arr(I) <= rx_parallel_data_arr_slv((I + 1) * c_xgmii_data_w - 1 downto I * c_xgmii_data_w);
      rx_control_arr(I) <= rx_control_arr_slv((I + 1) * c_xgmii_nof_lanes - 1 downto I * c_xgmii_nof_lanes);
    end generate;

    u_ip_arria10_e3sge3_phy_10gbase_r_48 : ip_arria10_e3sge3_phy_10gbase_r_48
    port map (
      tx_analogreset          => tx_analogreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          tx_analogreset.tx_analogreset
      tx_digitalreset         => tx_digitalreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --         tx_digitalreset.tx_digitalreset
      rx_analogreset          => rx_analogreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          rx_analogreset.rx_analogreset
      rx_digitalreset         => rx_digitalreset_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --         rx_digitalreset.rx_digitalreset
      tx_cal_busy             => tx_cal_busy_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --             tx_cal_busy.tx_cal_busy
      rx_cal_busy             => rx_cal_busy_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --             rx_cal_busy.rx_cal_busy

      tx_serial_clk0          => tx_serial_clk_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          tx_serial_clk0.clk
      rx_cdr_refclk0          => tr_ref_clk_644,  -- in  std_logic                       := '0';             --          rx_cdr_refclk0.clk
      tx_serial_data          => tx_serial_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --          tx_serial_data.tx_serial_data
      rx_serial_data          => rx_serial_arr(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --          rx_serial_data.rx_serial_data

      --rx_is_lockedtoref       : out std_logic_vector(11 downto 0);                      --       rx_is_lockedtoref.rx_is_lockedtoref
      rx_is_lockedtodata      => rx_is_lockedtodata_arr(IP_SIZE-1 downto 0),  -- out std_logic_vector(11 downto 0);                      --      rx_is_lockedtodata.rx_is_lockedtodata

      tx_coreclkin            => tr_coreclkin_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --            tx_coreclkin.clk
      rx_coreclkin            => tr_coreclkin_slv(IP_SIZE-1 downto 0),  -- in  std_logic_vector(11 downto 0)   := (others => '0'); --            rx_coreclkin.clk

      tx_parallel_data        => tx_parallel_data_arr_slv(IP_SIZE_DATA - 1 downto 0),  -- in  std_logic_vector(1535 downto 0) := (others => '0'); --        tx_parallel_data.tx_parallel_data
      rx_parallel_data        => rx_parallel_data_arr_slv(IP_SIZE_DATA - 1 downto 0),  -- out std_logic_vector(1535 downto 0);                    --        rx_parallel_data.rx_parallel_data
      tx_control              => tx_control_arr_slv(IP_SIZE_CONTROL - 1 downto 0),  -- in  std_logic_vector(191 downto 0)  := (others => '0'); --              tx_control.tx_control
      rx_control              => rx_control_arr_slv(IP_SIZE_CONTROL - 1 downto 0)  -- out std_logic_vector(191 downto 0);                     --              rx_control.rx_control

      --tx_clkout               : out std_logic_vector(11 downto 0);                      --               tx_clkout.clk
      --rx_clkout               : out std_logic_vector(11 downto 0);                      --               rx_clkout.clk

      --tx_err_ins              : in  std_logic_vector(11 downto 0)   := (others => '0'); --              tx_err_ins.tx_err_ins
      --tx_enh_data_valid       : in  std_logic_vector(11 downto 0)   := (others => '0'); --       tx_enh_data_valid.tx_enh_data_valid
      --tx_enh_fifo_empty       : out std_logic_vector(11 downto 0);                      --       tx_enh_fifo_empty.tx_enh_fifo_empty
      --tx_enh_fifo_full        : out std_logic_vector(11 downto 0);                      --        tx_enh_fifo_full.tx_enh_fifo_full
      --tx_enh_fifo_pempty      : out std_logic_vector(11 downto 0);                      --      tx_enh_fifo_pempty.tx_enh_fifo_pempty
      --tx_enh_fifo_pfull       : out std_logic_vector(11 downto 0);                      --       tx_enh_fifo_pfull.tx_enh_fifo_pfull
      --tx_pma_div_clkout       : out std_logic_vector(11 downto 0);                      --       tx_pma_div_clkout.clk

      --rx_enh_blk_lock         : out std_logic_vector(11 downto 0);                      --         rx_enh_blk_lock.rx_enh_blk_lock
      --rx_enh_data_valid       : out std_logic_vector(11 downto 0);                      --       rx_enh_data_valid.rx_enh_data_valid
      --rx_enh_fifo_del         : out std_logic_vector(11 downto 0);                      --         rx_enh_fifo_del.rx_enh_fifo_del
      --rx_enh_fifo_empty       : out std_logic_vector(11 downto 0);                      --       rx_enh_fifo_empty.rx_enh_fifo_empty
      --rx_enh_fifo_full        : out std_logic_vector(11 downto 0);                      --        rx_enh_fifo_full.rx_enh_fifo_full
      --rx_enh_fifo_insert      : out std_logic_vector(11 downto 0);                      --      rx_enh_fifo_insert.rx_enh_fifo_insert
      --rx_enh_highber          : out std_logic_vector(11 downto 0);                      --          rx_enh_highber.rx_enh_highber

      --unused_rx_control       : out std_logic_vector(287 downto 0);                     --       unused_rx_control.unused_rx_control
      --unused_rx_parallel_data : out std_logic_vector(1535 downto 0);                    -- unused_rx_parallel_data.unused_rx_parallel_data
      --unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => '0'); --       unused_tx_control.unused_tx_control
      --unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
    );

    u_ip_arria10_e3sge3_transceiver_reset_controller_48 : ip_arria10_e3sge3_transceiver_reset_controller_48
    port map (
      clock              => clk_156,  -- : in  std_logic                     := '0';             --              clock.clk
      pll_locked         => atx_pll_locked_arr(0 downto 0),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --         pll_locked.pll_locked
      pll_powerdown      => atx_pll_powerdown_arr(0 downto 0),  -- : out std_logic_vector(0 downto 0);                     --      pll_powerdown.pll_powerdown
      pll_select         => "0",  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --         pll_select.pll_select
      reset              => rst_156,  -- : in  std_logic                     := '0';             --              reset.reset
      rx_analogreset     => rx_analogreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --     rx_analogreset.rx_analogreset
      rx_cal_busy        => rx_cal_busy_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); --        rx_cal_busy.rx_cal_busy
      rx_digitalreset    => rx_digitalreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --    rx_digitalreset.rx_digitalreset
      rx_is_lockedtodata => rx_is_lockedtodata_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); -- rx_is_lockedtodata.rx_is_lockedtodata
      rx_ready           => xgmii_rx_ready_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --           rx_ready.rx_ready
      tx_analogreset     => tx_analogreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --     tx_analogreset.tx_analogreset
      tx_cal_busy        => cal_busy_arr(IP_SIZE-1 downto 0),  -- : in  std_logic_vector(11 downto 0) := (others => '0'); --        tx_cal_busy.tx_cal_busy
      tx_digitalreset    => tx_digitalreset_arr(IP_SIZE-1 downto 0),  -- : out std_logic_vector(11 downto 0);                    --    tx_digitalreset.tx_digitalreset
      tx_ready           => xgmii_tx_ready_arr(IP_SIZE-1 downto 0)  -- : out std_logic_vector(11 downto 0)                     --           tx_ready.tx_ready
    );
  end generate;

  -- ATX PLL
  u_ip_arria10_e3sge3_transceiver_pll_10g : ip_arria10_e3sge3_transceiver_pll_10g
  port map (
    pll_powerdown   => atx_pll_powerdown_arr(0),  -- only use reset controller 0 for ATX PLL power down, leave others not used
    pll_refclk0     => tr_ref_clk_644,
    pll_locked      => atx_pll_locked,
    pll_cal_busy    => atx_pll_cal_busy,
    mcgb_rst        => atx_pll_powerdown_arr(0),
    mcgb_serial_clk => tx_serial_clk(0)
  );
end str;
