-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Author: E. Kooistra, 30 mar 2017
-- Purpose: Verify mms_dp_gain_serial_arr
-- Description:
-- Usage:
-- > as 10
-- > run -all
-- The tb is self stopping and self checking.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_mms_dp_gain_serial_arr is
  generic (
    g_technology             : natural := c_tech_select_default;
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active or random stimuli valid flow control
    g_nof_streams            : natural := 3;  -- >= 1
    g_nof_gains              : natural := 117;  -- number of gains in series per stream
    g_complex_data           : boolean := false;
    g_complex_gain           : boolean := false
  );
end tb_mms_dp_gain_serial_arr;

architecture tb of tb_mms_dp_gain_serial_arr is
  constant c_mm_clk_period              : time := 20 ns;
  constant c_dp_clk_period              : time := 10 ns;
  constant c_cross_clock_domain_latency : natural := 20;
  constant c_dut_latency                : natural := 5;  -- = 3 for the real or complex multiplier + 2 for the RAM read latency

  constant c_real_multiply              : boolean := g_complex_data = false and g_complex_gain = false;
  constant c_nof_gains_w                : natural := ceil_log2(g_nof_gains);
  constant c_nof_gains_span             : natural := 2**c_nof_gains_w;
  constant c_gain_w                     : natural := 8;  -- from MM : <= 32 for real, <= 16 for complex to fit 32b MM data
  constant c_gain_max                   : integer :=  2**(c_gain_w - 1) - 1;
  constant c_gain_min                   : integer := -2**(c_gain_w - 1) + 1;  -- do not use most negative value to allow skipping double sign in gain*in_dat product

  constant c_in_dat_w                   : natural := 9;  -- streaming data
  constant c_in_length                  : integer :=  2**c_in_dat_w;
  constant c_in_max                     : integer :=  2**(c_in_dat_w - 1) - 1;
  constant c_in_min                     : integer := -2**(c_in_dat_w - 1);
  constant c_out_dat_w                  : natural := c_gain_w + c_in_dat_w - 1;  -- skip double sign bit and assume complex gain amplitude <= 1

  constant c_int_gain_re                : integer := c_gain_max - 1;
  constant c_int_gain_im                : integer :=           -1;
  constant c_slv_gain_re                : std_logic_vector(c_gain_w - 1 downto 0) := TO_SVEC(c_int_gain_re, c_gain_w);
  constant c_slv_gain_im                : std_logic_vector(c_gain_w - 1 downto 0) := TO_SVEC(c_int_gain_im, c_gain_w);
  constant c_slv_gain_complex           : std_logic_vector(c_nof_complex * c_gain_w - 1 downto 0) := c_slv_gain_im & c_slv_gain_re;
  constant c_int_gain_complex           : integer := TO_SINT(c_slv_gain_complex);

  signal dbg_nof_streams      : natural := g_nof_streams;
  signal dbg_complex_data     : boolean := g_complex_data;
  signal dbg_complex_gain     : boolean := g_complex_gain;

  signal tb_end               : std_logic := '0';
  signal mm_clk               : std_logic := '1';
  signal mm_rst               : std_logic := '1';
  signal dp_clk               : std_logic := '1';
  signal dp_rst               : std_logic := '1';

  signal random               : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal stimuli_en           : std_logic := '0';
  signal in_en                : std_logic := '0';
  signal verify_en            : std_logic := '0';

  signal cnt_re               : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal cnt_im               : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal cnt_val              : std_logic;

  signal in_sosi              : t_dp_sosi;
  signal in_sosi_dly          : t_dp_sosi;
  signal in_sosi_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal out_sosi_arr         : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal exp_sosi_arr         : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal gains_re_arr         : t_integer_arr(g_nof_streams - 1 downto 0) := (others => c_int_gain_re);  -- default gain for all parallel streams and serial addresses
  signal gains_im_arr         : t_integer_arr(g_nof_streams - 1 downto 0) := (others => c_int_gain_im);  -- default gain for all parallel streams and serial addresses

  signal ram_gains_mosi       : t_mem_mosi := c_mem_mosi_rst;  -- MM write side, im&re
  signal ram_gains_miso       : t_mem_miso;
  signal gains_rd_address     : std_logic_vector(c_nof_gains_w - 1 downto 0) := TO_UVEC(0, c_nof_gains_w);  -- DP read side, same read address for all streams
  signal gains_rd_address_dly : std_logic_vector(c_nof_gains_w - 1 downto 0);
begin
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  random <= func_common_random(random) when rising_edge(dp_clk);

  in_en <= stimuli_en and random(random'high) when g_flow_control_stimuli = e_random else stimuli_en;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------
  proc_dp_cnt_dat(dp_rst, dp_clk, in_en, cnt_val, cnt_re);

  -- derive cnt_im linearly from cnt_re such that abs(complex(cnt_re, cnt_im)) <= amplitude
  -- the principle is: suppose re normalized to 1 then im = 1-re when re>=0 else im = -1-re when re<0
  cnt_im <= TO_SVEC(sel_a_b(TO_SINT(cnt_re) >= 0, c_in_max, c_in_min) - TO_SINT(cnt_re), c_in_dat_w);

  in_sosi.data  <= RESIZE_DP_SDATA(cnt_re);
  in_sosi.re    <= RESIZE_DP_DSP_DATA(cnt_re);
  in_sosi.im    <= RESIZE_DP_DSP_DATA(cnt_im);
  in_sosi.valid <= cnt_val;

  in_sosi_arr <= (others => in_sosi);  -- use same data on all input streams

  p_clk : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if cnt_val = '1' then
        if TO_UINT(gains_rd_address) < g_nof_gains - 1 then
          gains_rd_address <= INCR_UVEC(gains_rd_address, 1);
        else
          gains_rd_address <= TO_UVEC(0, c_nof_gains_w);
        end if;
      end if;
    end if;
  end process;

  u_in_sosi_dly : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_dut_latency
  )
  port map (
    rst      => dp_rst,
    clk      => dp_clk,
    snk_in   => in_sosi,
    src_out  => in_sosi_dly
  );

  u_gains_rd_address_dly : entity common_lib.common_pipeline
  generic map (
    g_pipeline  => c_dut_latency,
    g_in_dat_w  => c_nof_gains_w,
    g_out_dat_w => c_nof_gains_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    in_dat  => gains_rd_address,
    out_dat => gains_rd_address_dly
  );

  p_mm_stimuli : process
  begin
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(dp_clk, 5);

    -- Use same default serial gains for all parallel streams and serial addresses to keep the tb simple
    gains_re_arr <= (others => c_int_gain_re);
    if g_complex_gain = false then
      gains_im_arr <= (others => c_int_gain_re);
    else
      gains_im_arr <= (others => c_int_gain_im);
    end if;

    for I in 0 to g_nof_streams - 1 loop
      for J in 0 to g_nof_gains - 1 loop
        if J = I then
          -- for one address in stream set gain to zero, to have some distinction between the streams and between the addresses
          proc_mem_mm_bus_wr(I * c_nof_gains_span + J, 0, mm_clk, ram_gains_miso, ram_gains_mosi);
        else
          -- all other addresses have same default gain
          proc_mem_mm_bus_wr(I * c_nof_gains_span + J, c_int_gain_complex, mm_clk, ram_gains_miso, ram_gains_mosi);
        end if;
      end loop;
    end loop;
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);
    stimuli_en <= '1';
    verify_en <= '1';
    proc_common_wait_some_cycles(dp_clk, c_in_length * 3);

    tb_end <= '1';
    wait;
  end process;

  -- Verify out_sosi_arr assuming that the gain and c_out_dat_w are set such that the output equals the input
  p_exp_sosi_arr : process(gains_re_arr, gains_im_arr, in_sosi_dly)
  begin
    for I in 0 to g_nof_streams - 1 loop
      if c_real_multiply = true then
        exp_sosi_arr(I).data <= TO_DP_SDATA(gains_re_arr(I) * TO_SINT(in_sosi_dly.data));
        if I = TO_UINT(gains_rd_address_dly) then
          exp_sosi_arr(I).data <= TO_DP_SDATA(0);
        end if;
      else
        if g_complex_gain = false then
          exp_sosi_arr(I).re <= TO_DP_DSP_DATA(gains_re_arr(I) * TO_SINT(in_sosi_dly.re));
          exp_sosi_arr(I).im <= TO_DP_DSP_DATA(gains_re_arr(I) * TO_SINT(in_sosi_dly.im));
        else
          exp_sosi_arr(I).re <= TO_DP_DSP_DATA(gains_re_arr(I) * TO_SINT(in_sosi_dly.re) - gains_im_arr(I) * TO_SINT(in_sosi_dly.im));
          exp_sosi_arr(I).im <= TO_DP_DSP_DATA(gains_re_arr(I) * TO_SINT(in_sosi_dly.im) + gains_im_arr(I) * TO_SINT(in_sosi_dly.re));
        end if;
        if I = TO_UINT(gains_rd_address_dly) then
          exp_sosi_arr(I).re <= TO_DP_DSP_DATA(0);
          exp_sosi_arr(I).im <= TO_DP_DSP_DATA(0);
        end if;
      end if;
    end loop;
  end process;

  p_verify : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if verify_en = '1' then
        for I in 0 to g_nof_streams - 1 loop
          if c_real_multiply = true then
            assert signed(out_sosi_arr(I).data) = signed(exp_sosi_arr(I).data)
              report "Unexpected real data"
              severity ERROR;
          else
            if g_complex_gain = false then
              assert signed(out_sosi_arr(I).re) = signed(exp_sosi_arr(I).re)
                report "Unexpected complex real data, with real gain"
                severity ERROR;
              assert signed(out_sosi_arr(I).im) = signed(exp_sosi_arr(I).im)
                report "Unexpected complex imag data, with real gain"
                severity ERROR;
            else
              assert signed(out_sosi_arr(I).re) = signed(exp_sosi_arr(I).re)
                report "Unexpected complex real data, with complex gain"
                severity ERROR;
              assert signed(out_sosi_arr(I).im) = signed(exp_sosi_arr(I).im)
                report "Unexpected complex imag data, with complex gain"
                severity ERROR;
            end if;
          end if;
        end loop;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  gen_one : if g_nof_streams = 1 generate
    u_dut_one : entity work.mms_dp_gain_serial
    generic map (
      g_technology        => c_tech_select_default,
      g_nof_gains         => g_nof_gains,  -- number of gains in series per stream
      g_complex_data      => g_complex_data,
      g_complex_gain      => g_complex_gain,
      g_gain_w            => c_gain_w,
      g_in_dat_w          => c_in_dat_w,
      g_out_dat_w         => c_out_dat_w,
      g_gains_file_name   => "UNUSED",  -- "UNUSED" or relative path to some "gains_#.hex" file, where # is the stream index
      g_gains_write_only  => false  -- When set to TRUE the M9K blocks are forced to Simple Dual Port mode to save memory. When FALSE it is True Dual Port.
    )
    port map (
      -- System
      mm_rst                  => mm_rst,
      mm_clk                  => mm_clk,
      dp_rst                  => dp_rst,
      dp_clk                  => dp_clk,

      -- MM interface
      ram_gains_mosi          => ram_gains_mosi,  -- write side
      ram_gains_miso          => ram_gains_miso,

      -- ST interface
      gains_rd_address        => gains_rd_address,  -- read side, same read address for all streams

      in_sosi                 => in_sosi_arr(0),
      out_sosi                => out_sosi_arr(0)
    );
  end generate;

  gen_par : if g_nof_streams > 1 generate
    u_dut_par : entity work.mms_dp_gain_serial_arr
    generic map (
      g_technology        => c_tech_select_default,
      g_nof_streams       => g_nof_streams,
      g_nof_gains         => g_nof_gains,  -- number of gains in series per stream
      g_complex_data      => g_complex_data,
      g_complex_gain      => g_complex_gain,
      g_gain_w            => c_gain_w,
      g_in_dat_w          => c_in_dat_w,
      g_out_dat_w         => c_out_dat_w,
      g_gains_file_name   => "UNUSED",  -- "UNUSED" or relative path to some "gains_#.hex" file, where # is the stream index
      g_gains_write_only  => false  -- When set to TRUE the M9K blocks are forced to Simple Dual Port mode to save memory. When FALSE it is True Dual Port.
    )
    port map (
      -- System
      mm_rst                  => mm_rst,
      mm_clk                  => mm_clk,
      dp_rst                  => dp_rst,
      dp_clk                  => dp_clk,

      -- MM interface
      ram_gains_mosi          => ram_gains_mosi,  -- write side
      ram_gains_miso          => ram_gains_miso,

      -- ST interface
      gains_rd_address        => gains_rd_address,  -- read side, same read address for all streams

      in_sosi_arr             => in_sosi_arr,
      out_sosi_arr            => out_sosi_arr
    );
  end generate;
end tb;
