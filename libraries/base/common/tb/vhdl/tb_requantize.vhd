-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, tst_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose: Test bench for common_requantize.vhd
-- Usage:
-- > do wave_requantize.do
-- > run 1 us
-- . Do a diff with the golden reference output files for the DUTs. These files
--   are created using run 1 us and:
--   . c_in_dat_w           = 6
--   . c_out_dat_w          = 3
--   . c_lsb_w              = 2
--   . c_lsb_round_clip     = TRUE
--   . c_lsb_round_even     = FALSE
--   . c_msb_clip_symmetric = TRUE
-- . Observe reg_dat with respect to the out_s_*_*.dat and out_u_*_*.dat
-- . Try also c_lsb_round_clip=FALSE
-- . Try also c_msb_clip_symmetric=FALSE

entity tb_requantize is
end tb_requantize;

architecture tb of tb_requantize is
  constant clk_period            : time := 10 ns;
  constant c_output_file_dir     : string := "data/";

  constant c_nof_dut             : natural := 4;
  constant g_pipeline_remove_lsb : natural := 0;
  constant g_pipeline_remove_msb : natural := 1;
  constant c_pipeline            : natural := g_pipeline_remove_lsb + g_pipeline_remove_msb;
  constant c_in_dat_w            : natural := 6;
  constant c_out_dat_w           : natural := 3;
  constant c_lsb_w               : natural := 2;
  constant c_lsb_round_clip      : boolean := true;  -- FALSE;
  constant c_lsb_round_even      : boolean := false;  -- golden reference data is for round half away from zero
  constant c_msb_clip_symmetric  : boolean := true;  -- FALSE;

  -- Stimuli
  signal in_val         : std_logic;
  signal in_dat         : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal in_vec         : std_logic_vector(c_in_dat_w   downto 0);
  signal reg_vec        : std_logic_vector(c_in_dat_w   downto 0);
  signal reg_val        : std_logic;
  signal reg_dat        : std_logic_vector(c_in_dat_w - 1 downto 0);

  -- DUT output
  signal out_s_r_c_dat  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- src = signed round and clip
  signal out_s_r_c_ovr  : std_logic;
  signal out_s_r_w_dat  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- srw = signed round and wrap
  signal out_s_r_w_ovr  : std_logic;
  signal out_s_t_c_dat  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- stc = signed truncate and clip
  signal out_s_t_c_ovr  : std_logic;
  signal out_s_t_w_dat  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- stw = signed truncate and wrap
  signal out_s_t_w_ovr  : std_logic;

  signal out_u_r_c_dat  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- urc = unsigned round and clip
  signal out_u_r_c_ovr  : std_logic;
  signal out_u_r_w_dat  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- urw = unsigned round and wrap
  signal out_u_r_w_ovr  : std_logic;
  signal out_u_t_c_dat  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- utc = unsigned truncate and clip
  signal out_u_t_c_ovr  : std_logic;
  signal out_u_t_w_dat  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- utw = unsigned truncate and wrap
  signal out_u_t_w_ovr  : std_logic;

  -- Verification by means of writing output files that can be compared with stored golden reference files
  signal out_s_dat_vec  : std_logic_vector(c_out_dat_w * c_nof_dut - 1 downto 0);
  signal ref_s_dat_vec  : std_logic_vector(c_out_dat_w * c_nof_dut - 1 downto 0);
  signal out_s_ovr_vec  : std_logic_vector(            c_nof_dut - 1 downto 0);
  signal ref_s_ovr_vec  : std_logic_vector(            c_nof_dut - 1 downto 0);
  signal out_u_dat_vec  : std_logic_vector(c_out_dat_w * c_nof_dut - 1 downto 0);
  signal ref_u_dat_vec  : std_logic_vector(c_out_dat_w * c_nof_dut - 1 downto 0);
  signal out_u_ovr_vec  : std_logic_vector(            c_nof_dut - 1 downto 0);
  signal ref_u_ovr_vec  : std_logic_vector(            c_nof_dut - 1 downto 0);
  signal ref_val        : std_logic;
  signal ref_eof        : std_logic;

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '1';
  signal rst            : std_logic;

  constant c_init       : std_logic_vector(in_dat'range) := (others => '0');
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  -- Testbench end
  p_tb_end : process
  begin
    tb_end <= '0';
    wait until ref_val = '1';
    wait until rising_edge(clk);
    wait until ref_val = '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    tb_end <= '1';
    wait;
  end process;

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      in_val      <= '0';
      in_dat      <= c_init;
    elsif rising_edge(clk) then
      in_val      <= '1';
      in_dat      <= std_logic_vector(signed(in_dat) + 1);
    end if;
  end process;

  -- Delay input as much as DUT output
  in_vec <= in_val & in_dat;

  u_pipe : entity work.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_in_dat_w       => c_in_dat_w + 1,
    g_out_dat_w      => c_in_dat_w + 1
  )
  port map (
    clk     => clk,
    in_dat  => in_vec,
    out_dat => reg_vec
  );

  reg_val <= reg_vec(c_in_dat_w);
  reg_dat <= reg_vec(c_in_dat_w - 1 downto 0);

  -- DUT for "SIGNED"
  u_s_r_c : entity work.common_requantize
  generic map (
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => c_lsb_round_clip,
    g_lsb_round_even      => c_lsb_round_even,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => c_msb_clip_symmetric,
    g_pipeline_remove_lsb => g_pipeline_remove_lsb,
    g_pipeline_remove_msb => g_pipeline_remove_msb,
    g_in_dat_w            => c_in_dat_w,
    g_out_dat_w           => c_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_s_r_c_dat,
    out_ovr        => out_s_r_c_ovr
  );

  u_s_r_w : entity work.common_requantize
  generic map (
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => c_lsb_round_clip,
    g_lsb_round_even      => c_lsb_round_even,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => c_msb_clip_symmetric,
    g_pipeline_remove_lsb => g_pipeline_remove_lsb,
    g_pipeline_remove_msb => g_pipeline_remove_msb,
    g_in_dat_w            => c_in_dat_w,
    g_out_dat_w           => c_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_s_r_w_dat,
    out_ovr        => out_s_r_w_ovr
  );

  u_s_t_c : entity work.common_requantize
  generic map (
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => false,
    g_lsb_round_clip      => c_lsb_round_clip,
    g_lsb_round_even      => c_lsb_round_even,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => c_msb_clip_symmetric,
    g_pipeline_remove_lsb => g_pipeline_remove_lsb,
    g_pipeline_remove_msb => g_pipeline_remove_msb,
    g_in_dat_w            => c_in_dat_w,
    g_out_dat_w           => c_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_s_t_c_dat,
    out_ovr        => out_s_t_c_ovr
  );

  u_s_t_w : entity work.common_requantize
  generic map (
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => false,
    g_lsb_round_clip      => c_lsb_round_clip,
    g_lsb_round_even      => c_lsb_round_even,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => c_msb_clip_symmetric,
    g_pipeline_remove_lsb => g_pipeline_remove_lsb,
    g_pipeline_remove_msb => g_pipeline_remove_msb,
    g_in_dat_w            => c_in_dat_w,
    g_out_dat_w           => c_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_s_t_w_dat,
    out_ovr        => out_s_t_w_ovr
  );

  -- DUT for "UNSIGNED"
  u_u_r_c : entity work.common_requantize
  generic map (
    g_representation      => "UNSIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => c_lsb_round_clip,
    g_lsb_round_even      => c_lsb_round_even,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => c_msb_clip_symmetric,
    g_pipeline_remove_lsb => g_pipeline_remove_lsb,
    g_pipeline_remove_msb => g_pipeline_remove_msb,
    g_in_dat_w            => c_in_dat_w,
    g_out_dat_w           => c_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_u_r_c_dat,
    out_ovr        => out_u_r_c_ovr
  );

  u_u_r_w : entity work.common_requantize
  generic map (
    g_representation      => "UNSIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => c_lsb_round_clip,
    g_lsb_round_even      => c_lsb_round_even,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => c_msb_clip_symmetric,
    g_pipeline_remove_lsb => g_pipeline_remove_lsb,
    g_pipeline_remove_msb => g_pipeline_remove_msb,
    g_in_dat_w            => c_in_dat_w,
    g_out_dat_w           => c_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_u_r_w_dat,
    out_ovr        => out_u_r_w_ovr
  );

  u_u_t_c : entity work.common_requantize
  generic map (
    g_representation      => "UNSIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => false,
    g_lsb_round_clip      => c_lsb_round_clip,
    g_lsb_round_even      => c_lsb_round_even,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => c_msb_clip_symmetric,
    g_pipeline_remove_lsb => g_pipeline_remove_lsb,
    g_pipeline_remove_msb => g_pipeline_remove_msb,
    g_in_dat_w            => c_in_dat_w,
    g_out_dat_w           => c_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_u_t_c_dat,
    out_ovr        => out_u_t_c_ovr
  );

  u_u_t_w : entity work.common_requantize
  generic map (
    g_representation      => "UNSIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => false,
    g_lsb_round_clip      => c_lsb_round_clip,
    g_lsb_round_even      => c_lsb_round_even,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => c_msb_clip_symmetric,
    g_pipeline_remove_lsb => g_pipeline_remove_lsb,
    g_pipeline_remove_msb => g_pipeline_remove_msb,
    g_in_dat_w            => c_in_dat_w,
    g_out_dat_w           => c_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_u_t_w_dat,
    out_ovr        => out_u_t_w_ovr
  );

  -- Verification usign golden results from file
  p_verify : process
  begin
    wait until rising_edge(clk);
    if ref_val = '1' then
      if out_s_dat_vec /= ref_s_dat_vec then report "Mismatch in signed requantize data"       severity ERROR; end if;
      if out_s_ovr_vec /= ref_s_ovr_vec then report "Mismatch in signed requantize overflow"   severity ERROR; end if;
      if out_u_dat_vec /= ref_u_dat_vec then report "Mismatch in unsigned requantize data"     severity ERROR; end if;
      if out_u_ovr_vec /= ref_u_ovr_vec then report "Mismatch in unsigned requantize overflow" severity ERROR; end if;
    end if;
  end process;

  out_s_dat_vec <= out_s_r_c_dat & out_s_r_w_dat & out_s_t_c_dat & out_s_t_w_dat;
  out_s_ovr_vec <= out_s_r_c_ovr & out_s_r_w_ovr & out_s_t_c_ovr & out_s_t_w_ovr;
  out_u_dat_vec <= out_u_r_c_dat & out_u_r_w_dat & out_u_t_c_dat & out_u_t_w_dat;
  out_u_ovr_vec <= out_u_r_c_ovr & out_u_r_w_ovr & out_u_t_c_ovr & out_u_t_w_ovr;

  u_output_file_s_dat : entity tst_lib.tst_output
  generic map (
    g_file_name   => c_output_file_dir & "tb_requantize_s_dat.out",
    g_nof_data    => c_nof_dut,
    g_data_width  => c_out_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    in_dat   => out_s_dat_vec,
    in_val   => reg_val
  );

  u_ref_file_s_dat : entity tst_lib.tst_input
  generic map (
    g_file_name   => c_output_file_dir & "tb_requantize_s_dat.gold",
    g_file_repeat => 1,
    g_nof_data    => c_nof_dut,
    g_data_width  => c_out_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    en       => in_val,
    out_dat  => ref_s_dat_vec,
    out_val  => ref_val,
    out_eof  => ref_eof
  );

  u_output_file_s_ovr : entity tst_lib.tst_output
  generic map (
    g_file_name   => c_output_file_dir & "tb_requantize_s_ovr.out",
    g_nof_data    => c_nof_dut,
    g_data_width  => 1,
    g_data_type   => "UNSIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    in_dat   => out_s_ovr_vec,
    in_val   => reg_val
  );

  u_ref_file_s_ovr : entity tst_lib.tst_input
  generic map (
    g_file_name   => c_output_file_dir & "tb_requantize_s_ovr.gold",
    g_file_repeat => 1,
    g_nof_data    => c_nof_dut,
    g_data_width  => 1,
    g_data_type   => "SIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    en       => in_val,
    out_dat  => ref_s_ovr_vec,
    out_val  => OPEN,
    out_eof  => open
  );

  u_output_file_u_dat : entity tst_lib.tst_output
  generic map (
    g_file_name   => c_output_file_dir & "tb_requantize_u_dat.out",
    g_nof_data    => c_nof_dut,
    g_data_width  => c_out_dat_w,
    g_data_type   => "UNSIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    in_dat   => out_u_dat_vec,
    in_val   => reg_val
  );

  u_ref_file_u_dat : entity tst_lib.tst_input
  generic map (
    g_file_name   => c_output_file_dir & "tb_requantize_u_dat.gold",
    g_file_repeat => 1,
    g_nof_data    => c_nof_dut,
    g_data_width  => c_out_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    en       => in_val,
    out_dat  => ref_u_dat_vec,
    out_val  => OPEN,
    out_eof  => open
  );

  u_output_file_u_ovr : entity tst_lib.tst_output
  generic map (
    g_file_name   => c_output_file_dir & "tb_requantize_u_ovr.out",
    g_nof_data    => c_nof_dut,
    g_data_width  => 1,
    g_data_type   => "UNSIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    in_dat   => out_u_ovr_vec,
    in_val   => reg_val
  );

  u_ref_file_u_ovr : entity tst_lib.tst_input
  generic map (
    g_file_name   => c_output_file_dir & "tb_requantize_u_ovr.gold",
    g_file_repeat => 1,
    g_nof_data    => c_nof_dut,
    g_data_width  => 1,
    g_data_type   => "SIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    en       => in_val,
    out_dat  => ref_u_ovr_vec,
    out_val  => OPEN,
    out_eof  => open
  );
end tb;
