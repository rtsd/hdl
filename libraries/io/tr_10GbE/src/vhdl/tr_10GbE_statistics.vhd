-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur, Reinier van der Walle
-- Purpose:
-- . 10GbE wrapper for dp_statistics

library IEEE, common_lib, work, technology_lib, dp_lib, tr_10GbE_lib, tech_pll_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tr_10GbE_statistics is
  generic (
    g_technology               : natural := c_tech_select_default;
    g_dp_clk_freq_khz          : natural := 200000;  -- default dp_clk 200 MHz, 5 ns period
    g_runtime_nof_packets      : natural;  -- Run the test bench for nof_packets before asserting tb_end
    g_runtime_timeout          : time;  -- Report Failure if g_runtime_nof_packets is not reached before this time
    g_check_nof_valid          : boolean := false;  -- True enables valid count checking at tb_end. Reports Failure in case of mismatch.
    g_check_nof_valid_ref      : natural := 0  -- Reference (= expected) valid count
  );
  port (
    xaui_in   : in  std_logic_vector(3 downto 0) := (others => '0');
    serial_in : in  std_logic := '0';
    tb_end    : out std_logic  -- To be used to stop test-bench generated clocks
  );
end tr_10GbE_statistics;

architecture str of tr_10GbE_statistics is
  constant c_eth_clk_freq_khz   : natural := 156250;  -- not used, because dp_statistics operates in dp_clk domain
  constant c_eth_word_w         : natural := 64;
  constant c_dp_word_w          : natural := c_eth_word_w;

  constant c_dp_clk_period      : time := 10**9 / g_dp_clk_freq_khz * 1 ps;  -- 200MHz

  constant c_eth_clk_period     : time := 6.4 ns;
  constant c_cal_rec_clk_period : time := 25 ns;  -- 40MHz

  constant c_mm_clk_period      : time := 20 ns;  -- 50MHz
  constant c_sa_clk_period      : time := tech_pll_clk_644_period;

  signal dp_clk                 : std_logic := '1';
  signal dp_rst                 : std_logic;

  signal eth_clk                : std_logic := '1';  -- Start high to match sim model generated clock
  signal eth_rst                : std_logic := '0';
  signal cal_rec_clk            : std_logic := '0';

  signal mm_clk                 : std_logic := '1';
  signal mm_rst                 : std_logic := '0';
  signal sa_clk                 : std_logic := '1';

  signal tr_ref_clk_312         : std_logic := '0';
  signal tr_ref_clk_156         : std_logic := '0';
  signal tr_ref_rst_156         : std_logic := '0';

  signal tr_10GbE_src_out       : t_dp_sosi;

  signal i_tb_end               : std_logic;
begin
  ------------------------------------------------------------------------------
  -- We're using the tb_end output locally
  ------------------------------------------------------------------------------
  tb_end <= i_tb_end;

  ------------------------------------------------------------------------------
  -- Generate dp_clk and dp_rst
  ------------------------------------------------------------------------------
  dp_clk <= not dp_clk or i_tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 2;

  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    ----------------------------------------------------------------------------
    -- We're generating a reference clock locally, just like the sim model in
    -- tr_10GbE
    ----------------------------------------------------------------------------
    eth_clk <= not eth_clk or i_tb_end after c_eth_clk_period / 2;
    eth_rst <= '1', '0' after c_eth_clk_period * 2;

    cal_rec_clk <= not cal_rec_clk or i_tb_end after c_cal_rec_clk_period / 2;
    ----------------------------------------------------------------------------
    -- Use tr_10GbE with internal simulation model as Ethernet receiver
    ----------------------------------------------------------------------------
    u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
    generic map (
      g_sim           => true,
      g_sim_level     => 1,
      g_technology    => g_technology,
      g_nof_macs      => 1,
      g_use_mdio      => false
    )
    port map (
      tr_ref_clk_156      => eth_clk,
      tr_ref_rst_156      => eth_rst,

      cal_rec_clk         => cal_rec_clk,

      mm_rst              => '0',
      mm_clk              => '0',

      dp_rst              => dp_rst,
      dp_clk              => dp_clk,

      xaui_rx_arr(0)      => xaui_in,

      src_out_arr(0)      => tr_10GbE_src_out
    );
  end generate;

  gen_ip_arria10 : if g_technology /= c_tech_stratixiv generate
    mm_clk <= not mm_clk or i_tb_end after c_mm_clk_period / 2;
    mm_rst <= '1', '0' after c_mm_clk_period * 2;
    sa_clk <= not sa_clk or i_tb_end after c_sa_clk_period / 2;

    u_unb2_board_clk644_pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
    generic map (
      g_technology => g_technology
    )
    port map (
      refclk_644 => sa_clk,
      rst_in     => mm_rst,
      clk_156    => tr_ref_clk_156,
      clk_312    => tr_ref_clk_312,
      rst_156    => tr_ref_rst_156,
      rst_312    => open
    );

    u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
    generic map (
      g_sim           => true,
      g_sim_level     => 1,
      g_technology    => g_technology,
      g_nof_macs      => 1,
      g_use_mdio      => false
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644      => sa_clk,
      tr_ref_clk_312      => tr_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
      tr_ref_clk_156      => tr_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
      tr_ref_rst_156      => tr_ref_rst_156,  -- for 10GBASE-R or for XAUI

      -- MM interface
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,

      -- DP interface
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,

      serial_rx_arr(0)    => serial_in,

      src_out_arr(0)      => tr_10GbE_src_out

    );
  end generate;

  ------------------------------------------------------------------------------
  -- dp_statistics
  ------------------------------------------------------------------------------
  u_dp_statistics : entity dp_lib.dp_statistics
  generic map (
    g_runtime_nof_packets      => g_runtime_nof_packets,
    g_runtime_timeout          => g_runtime_timeout,
    g_check_nof_valid          => g_check_nof_valid,
    g_check_nof_valid_ref      => g_check_nof_valid_ref,
    g_dp_word_w                => c_dp_word_w
    )
  port map (
    dp_clk => dp_clk,
    dp_rst => dp_rst,

    snk_in => tr_10GbE_src_out,

    tb_end => i_tb_end
  );
end str;
