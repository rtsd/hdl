--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Reuse from LOFAR rad_frame_hdr.vhd and rad_frame_hdr(rtl).vhd

-- Purpose:
--   Determine start and end of data blocks and introduce the data path (dp)
--   frame sequence number (fsn) with sync and sequence number information.
-- Description:
--   The input data are the data path signals:
--     . sync
--     . val
--     . dat[]
--   The output data are the data paths signals:
--     . val
--     . dat[]
--     . sof
--     . eof
--     . fsn[]
--   The blocks have size g_block_size. There must be gaps between data blocks
--   to allow space for dp_frame and dp_frame_tx which insert >= 1 idle, sfd,
--   fsn and brc/crc into the stream. Therefore in_val must go low for at
--   least 4 cycles between data blocks. The sof marks the start of block and
--   the eof marks the end of the data block.
--   For the first block after a sync the msbit of the fsn is '1' and the frame
--   sequence number is forced to restart at 0. The fsn increments for the
--   subsequent data blocks. The fsn may wrap several times during a sync
--   interval. The fsn word is valid at the sof and during the frame. The new
--   fsn becomes valid directly after the eof or after an in_sync, that
--   restarts the fsn. The in_sync may be high for an entire data block. The
--   falling edge of in_sync is used internally and therefor the first cycle
--   after the last active in_sync must have in_val low (i.e. fall in the gap
--   between data blocks).
--   If g_use_sync is FALSE then the periodic in_sync is not used and the
--   function starts packetizing the data at the first valid input and will
--   remain in state s_data forever.
--   The function verifies that subsequent in_sync occur on a block boundary,
--   if not then the output flushes the rest of the block and then disables and
--   tries to recover at the next in_sync. In practise such an block error can
--   not occur, because the input signals come from internal circuitry in the
--   FPGA so if that goes wrong, then the entire FPGA logic state is undefined.
--   If the input data can be reset independent of this function then a
--   fractional data block can occur, so then the flush capability becomes
--   useful. Furthermore flush capability ensures that only full blocks are
--   output or nothing, similar as for dp_frame_rx. The error is output via
--   out_err which is valid in the cycle directly after out_eof. The g_fsn_w
--   must be <= g_dat_w.

entity dp_frame_fsn is
  generic (
    g_fsn_w        : positive := 16;
    g_dat_w        : positive := 16;
    g_use_sync     : boolean  := true;
    g_block_size   : positive := 1000
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;
    in_sync        : in  std_logic := '0';
    in_val         : in  std_logic;
    in_dat         : in  std_logic_vector(g_dat_w - 1 downto 0);
    out_fsn        : out std_logic_vector(g_fsn_w - 1 downto 0);
    out_dat        : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val        : out std_logic;
    out_sof        : out std_logic;
    out_eof        : out std_logic;
    out_err        : out std_logic
  );
end dp_frame_fsn;

architecture rtl of dp_frame_fsn is
  type state_type is (s_idle, s_data, s_flush);

  -- Use unregistered input to save logic, use registered input to ease achieving timing constrains.
  constant c_input_reg     : boolean := false;

  -- Registered inputs (see c_input_reg)
  signal in_sync_reg       : std_logic;
  signal in_val_reg        : std_logic;
  signal in_dat_reg        : std_logic_vector(in_dat'range);

  signal nxt_out_dat       : std_logic_vector(out_dat'range);
  signal nxt_out_val       : std_logic;

  signal state             : state_type;
  signal nxt_state         : state_type;
  signal rst_state         : state_type;

  -- sync
  signal prev_sync         : std_logic;
  signal sync_fevt         : std_logic;

  -- frame sync and frame sequence number
  signal fsync             : std_logic;
  signal nxt_fsync         : std_logic;
  signal fsn               : std_logic_vector(g_fsn_w - 2 downto 0);  -- MSbit of out_fsn will hold fsync
  signal nxt_fsn           : std_logic_vector(fsn'range);

  -- sample counter
  signal cnt               : natural range 0 to g_block_size-1;
  signal nxt_cnt           : natural;
  signal cnt_is_zero       : std_logic;
  signal cnt_is_block_size : std_logic;

  -- start, end of frame
  signal nxt_out_sof       : std_logic;
  signal i_out_eof         : std_logic;
  signal nxt_out_eof       : std_logic;
  signal nxt_out_err       : std_logic;
begin
  out_eof <= i_out_eof;

  -- Frame sequence number is composed of fsync and fsn
  out_fsn <= fsync & fsn;

  gen_input_reg : if c_input_reg = true generate
    p_reg : process (clk, rst)
    begin
      if rst = '1' then
        in_sync_reg <= '0';
        in_val_reg  <= '0';
        in_dat_reg  <= (others => '0');
      elsif rising_edge(clk) then
        in_sync_reg <= in_sync;
        in_val_reg  <= in_val;
        in_dat_reg  <= in_dat;
      end if;
    end process;
  end generate;

  no_input_reg : if c_input_reg = false generate
    in_sync_reg <= in_sync;
    in_val_reg  <= in_val;
    in_dat_reg  <= in_dat;
  end generate;

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      fsync     <= '0';
      fsn       <= (others => '0');
      prev_sync <= '0';
      cnt       <= 0;
      out_dat   <= (others => '0');
      out_val   <= '0';
      out_sof   <= '0';
      i_out_eof <= '0';
      out_err   <= '0';
      state     <= rst_state;
    elsif rising_edge(clk) then
      fsync     <= nxt_fsync;
      fsn       <= nxt_fsn;
      prev_sync <= in_sync_reg;
      cnt       <= nxt_cnt;
      out_dat   <= nxt_out_dat;
      out_val   <= nxt_out_val;
      out_sof   <= nxt_out_sof;
      i_out_eof <= nxt_out_eof;
      out_err   <= nxt_out_err;
      state     <= nxt_state;
    end if;
  end process;

  rst_state <= s_idle when g_use_sync = true else s_data;

  sync_fevt <= prev_sync and not in_sync_reg;

  cnt_is_zero       <= '1' when cnt = 0              else '0';
  cnt_is_block_size <= '1' when cnt = g_block_size-1 else '0';

  p_fsn: process(sync_fevt, fsync, fsn, i_out_eof)
  begin
    nxt_fsync <= fsync;
    nxt_fsn   <= fsn;
    if sync_fevt = '1' then
      nxt_fsync <= '1';  -- store sync
      nxt_fsn   <= (others => '0');  -- restart fsn
    elsif i_out_eof = '1' then
      nxt_fsync <= '0';
      nxt_fsn   <= std_logic_vector(unsigned(fsn) + 1);
    end if;
  end process;

  p_state: process(state, cnt, cnt_is_zero, cnt_is_block_size, sync_fevt, in_dat_reg, in_val_reg)
  begin
    nxt_out_dat <= in_dat_reg;
    nxt_out_val <= in_val_reg;
    nxt_out_sof <= '0';
    nxt_out_eof <= '0';
    nxt_out_err <= '0';
    nxt_state   <= state;
    nxt_cnt     <= cnt;
    if in_val_reg = '1' then
      nxt_cnt <= cnt + 1;
      if cnt_is_block_size = '1' then
        nxt_cnt <= 0;
      end if;
    end if;

    case state is
      when s_idle =>  -- no output until in_sync_reg goes inactive
        nxt_out_val <= '0';
        nxt_cnt <= 0;
        if sync_fevt = '1' then
          nxt_state <= s_data;
        end if;
      when s_data =>
        if in_val_reg = '1' then  -- block dat sof and eof
          if cnt_is_zero = '1' then
            nxt_out_sof <= '1';
          elsif cnt_is_block_size = '1' then
            nxt_out_eof <= '1';
          end if;
        end if;
        if sync_fevt = '1' and cnt_is_zero /= '1' then
          nxt_state <= s_flush;  -- fractional nof data blocks in sync interval
        end if;
      when others =>  -- s_flush
        nxt_out_val <= '1';
        nxt_cnt <= cnt + 1;  -- flush full block with undefined out_dat[]
        if cnt_is_block_size = '1' then
          nxt_out_err <= '1';  -- signal error, will propagate via brc, crc
          nxt_out_eof <= '1';
          nxt_cnt <= 0;
          nxt_state <= s_idle;  -- try to recover at next sync_fevt
        end if;
    end case;
  end process;
end rtl;
