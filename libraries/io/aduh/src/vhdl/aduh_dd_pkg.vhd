-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

package aduh_dd_pkg is
  -- ADU Interface
  type t_c_aduh_delays is record  -- Input de-skew buffer delays: unit 50 ps, range 0..15
    clk_delay_a       : natural;
    clk_delay_d       : natural;
    dat_delay_arr_a   : t_natural_arr(7 downto 0);  -- width is c_aduh_dd_ai.port_w = 8
    dat_delay_arr_b   : t_natural_arr(7 downto 0);
    dat_delay_arr_c   : t_natural_arr(7 downto 0);
    dat_delay_arr_d   : t_natural_arr(7 downto 0);
  end record;

  constant c_aduh_delays : t_c_aduh_delays := (0, 0, (others => 0), (others => 0), (others => 0), (others => 0));

  type t_c_aduh_dd_ai is record
    nof_sp         : natural;  -- = 4;     -- Fixed support 4 signal paths A,B,C,D, whether they contain active data depends on nof_adu
    nof_adu        : natural;  -- = 2;     -- When 2 ADUs then use all 4 ports A,B,C,D, one ADU on ports A,B and one ADU on ports C,D,
                                           -- when 1 ADU then only use ports C,D
    nof_ports      : natural;  -- = 2;     -- Fixed 2 ADC BI ports per ADU
    port_w         : natural;  -- = 8;     -- Fixed 8 bit ADC BI port width, the ADC sample width is also 8 bit
    dd_factor      : natural;  -- = 2;     -- Fixed double data rate factor for lvds data (800 MSps) and lvds clock (400 MHz)
    rx_factor      : natural;  -- = 2;     -- when 1 then the data path processing clock frequency is 400 MHz (= lvds clock / 1)
                                           -- when 2 then the data path processing clock frequency is 200 MHz (= lvds clock / 2)
    clk_rst_enable : boolean;  -- = TRUE;  -- default TRUE for initial DCLK_RST pulse to control the ADC DCLK phase, else FALSE for no DCLK_RST pulse
    clk_rst_invert : boolean;  -- = FALSE; -- default FALSE because DCLK_RST pulse on ADC is active high, use TRUE for active low pulse to compensate for P/N cross
    deskew         : t_c_aduh_delays;  -- Input de-skew buffer delays
  end record;

  constant c_aduh_dd_ai  : t_c_aduh_dd_ai := (4, 2, 2, 8, 2, 2, true, false, c_aduh_delays);
end aduh_dd_pkg;

package body aduh_dd_pkg is
end aduh_dd_pkg;
