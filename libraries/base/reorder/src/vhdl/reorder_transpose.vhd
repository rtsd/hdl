--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Performing a transpose (reordering data) on one or more streaming
--          inputs using external memory.
--
-- Description:
--   The input of multiple streams (g_nof_streams) is concatenated into a
--   single stream. Either the data field or the complex fields can be
--   used, based on the g_use_complex generic.
--
--   Data on this single stream is than transposed according to the settings
--   of g_frame_size_in and g_reorder_seq.
--   The actual transpose is divided in two stages. The first stage (pre_transpose)
--   is done using a subband select module (reorder_col in RadioHDL).
--   The second stage is done in external memory (DDR3, DDR4,...).
--
--   Stage 1: Pre Transpose
--   This stage is used to reorder data with a resolution as high as a single
--   sample, because the second stage (if using DDR3 for instance) has a resolution
--   of 16 or more samples. The ss_ss_transp mm interface can be used to specify
--   the desired reordering for the pre transpose.
--
--   Stage 2: Reorder Sequencer
--   After the pre transpose the data is send to the external memory
--   interface (to_mem_src_out). The reorder sequencer module provides the address
--   and control signals for the external memory. Writing and reading is done in a
--   alternating way. Data from the external memory is received via the
--   from_mem_snk_in interface. The sequencers rhythm is based on the settings of
--   the g_reorder_seq generic.
--
--   At the output the data from the single stream is split up in the original
--   g_nof_streams again. A block_gen module is used to generate the SYNC, SOP and EOP
--   signals.
--
--   SYNC and BSN
--   At the input the BSN number at every SYNC is written to a fifo. This BSN number
--   is inserted in the output data again when a SYNC is applied at the output.
--
--   SYNC Period
--   The SYNC period (the number of blocks per sync interval) is monitored with a counter.
--   In case the number of blocks within a syncperiod is not equal to the specified
--   g_reorder_seq.nof_blocks the sequencer will reset and start again when the number of
--   received blocksdoes match the g_reorder_seq.nof_blocks.
--
--   Complex or real data:
--   The the width of snk_in.data, re, im is g_in_dat_w. When g_use_complex = FALSE then
--   snk_in.data is passed on internally, so then the internal sosi.data width is also
--   g_in_dat_w. When g_use_complex = TRUE then snk_in.im and snk_in.re are concatenated
--   into an internal sosi.data that has then width 2*g_in_dat_w. The g_nof_streams are
--   also concatenated into the sosi.data.
--   . g_use_complex = FALSE : sosi.data =  data[g_nof_streams-1] & ... &  data[1] &  data[0]
--   . g_use_complex =  TRUE : sosi.data = im&re[g_nof_streams-1] & ... & im&re[1] & im&re[0]
--   Hence:
--   . g_use_complex = FALSE : c_data_w = g_nof_streams*g_in_dat_w
--   . g_use_complex =  TRUE : c_data_w = g_nof_streams*g_in_dat_w*c_nof_complex
-- Remarks:

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.reorder_pkg.all;

entity reorder_transpose is
  generic(
    g_pipeline_input : natural := 0;  -- choose 0 to have orignal version -r < 19416, choose 1 does not seem to help timing closure (erko)
    g_pipeline_output: natural := 0;  -- choose 0 to have orignal version -r < 19416, choose 1 does not seem to help timing closure (erko)
    g_nof_streams    : natural := 4;
    g_in_dat_w       : natural := 8;
    g_frame_size_in  : natural := 256;
    g_frame_size_out : natural := 256;
    g_use_complex    : boolean := true;
    g_mem_dat_w      : natural := 256;  -- The data width to the attached memory.
    g_ena_pre_transp : boolean := true;
    g_reorder_seq    : t_reorder_seq := c_reorder_seq;
    g_select_file    : string := "UNUSED"
  );
  port (
    mm_rst                   : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk                   : in  std_logic;  -- memory-mapped bus clock
    dp_clk                   : in  std_logic;
    dp_rst                   : in  std_logic;

    -- ST sinks
    snk_out_arr              : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr               : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr               : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr              : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    -- Memory Mapped
    ram_ss_ss_transp_mosi    : in  t_mem_mosi;  -- channel select control
    ram_ss_ss_transp_miso    : out t_mem_miso;
    reg_dp_sync_checker_mosi : in  t_mem_mosi;
    reg_dp_sync_checker_miso : out t_mem_miso;

    -- Control interface to the external memory
    dvr_miso                 : in  t_mem_ctlr_miso;
    dvr_mosi                 : out t_mem_ctlr_mosi;

    -- Data interface to the external memory
    to_mem_src_out           : out t_dp_sosi;
    to_mem_src_in            : in  t_dp_siso;

    from_mem_snk_in          : in  t_dp_sosi;
    from_mem_snk_out         : out t_dp_siso := c_dp_siso_rdy
   );
end reorder_transpose;

architecture str of reorder_transpose is
  constant c_blocksize        : positive := g_reorder_seq.wr_chunksize + g_reorder_seq.gapsize;
  constant c_pagesize         : positive := g_reorder_seq.nof_blocks * c_blocksize;
  constant c_mem_size         : positive := 2 * c_pagesize;
  constant c_mem_size_w       : positive := ceil_log2(c_mem_size);

  constant c_total_data_w     : natural  := g_nof_streams * g_in_dat_w;
  constant c_complex_data_w   : natural  := c_total_data_w * c_nof_complex;
  constant c_data_w           : natural  := sel_a_b(g_use_complex, c_complex_data_w, c_total_data_w);
  constant c_data_w_pre       : natural  := sel_a_b(g_use_complex, c_total_data_w, c_total_data_w / 2);  -- Datawidth for pre-transpose defines Re or Im part.
  constant c_sdata_w          : natural  := c_data_w / g_nof_streams;

  constant c_nof_ch_in        : natural  := g_frame_size_in * g_reorder_seq.rd_chunksize;
  constant c_nof_ch_sel       : natural  := g_reorder_seq.wr_chunksize * g_reorder_seq.rd_chunksize;

  constant c_data_w_ratio     : positive := g_mem_dat_w / c_data_w;

  signal merged_snk_in        : t_dp_sosi;
  signal merged_snk_out       : t_dp_siso := c_dp_siso_rdy;

  signal pipe_merged_snk_in   : t_dp_sosi;
  signal pipe_merged_snk_out  : t_dp_siso := c_dp_siso_rdy;

  signal sync_checked_src_out : t_dp_sosi;
  signal sync_checked_src_in  : t_dp_siso;

  signal ss_in_sosi           : t_dp_sosi;
  signal ss_in_siso           : t_dp_siso := c_dp_siso_rdy;

  -- ctrl & status DDR3 driver
  signal dvr_done             : std_logic;
  signal dvr_en               : std_logic;
  signal dvr_wr_not_rd        : std_logic;
  signal dvr_start_address    : std_logic_vector(c_mem_size_w - 1 downto 0);
  signal dvr_nof_data         : std_logic_vector(c_mem_size_w - 1 downto 0);
  signal dvr_wr_flush_en      : std_logic;

  signal i_to_mem_src_out     : t_dp_sosi;

  signal block_gen_out_sosi   : t_dp_sosi;
  signal from_mem_out_sosi    : t_dp_sosi;

  signal sync_bsn             : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal rd_req_i             : std_logic;
  signal rd_dat_i             : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal rd_val_i             : std_logic;

  signal merge_src_out_arr    : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal i_src_out_arr        : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  -- debug signals
  -- . g_use_complex = FALSE : view sosi.data
  signal dbg_snk_in_data_concat           : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_to_mem_src_out_data_concat   : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_from_mem_snk_in_data_concat  : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_src_out_data_concat          : std_logic_vector(c_total_data_w - 1 downto 0);
  -- . g_use_complex = TRUE : view sosi.re and sosi.im
  signal dbg_snk_in                       : t_dp_sosi;
  signal dbg_snk_in_re_concat             : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_snk_in_im_concat             : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_to_mem_src_out               : t_dp_sosi;
  signal dbg_to_mem_src_out_re_concat     : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_to_mem_src_out_im_concat     : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_from_mem_snk_in              : t_dp_sosi;
  signal dbg_from_mem_snk_in_re_concat    : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_from_mem_snk_in_im_concat    : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_src_out                      : t_dp_sosi;
  signal dbg_src_out_re_concat            : std_logic_vector(c_total_data_w - 1 downto 0);
  signal dbg_src_out_im_concat            : std_logic_vector(c_total_data_w - 1 downto 0);
begin
  to_mem_src_out <= i_to_mem_src_out;
  src_out_arr <= i_src_out_arr;

  -- Internal transport is done via sosi.data, for debug view sosi.data or view sosi.re and sosi.im dependent on g_use_complex
  dbg_snk_in          <= snk_in_arr(0);
  dbg_to_mem_src_out  <= i_to_mem_src_out;
  dbg_from_mem_snk_in <= from_mem_snk_in;
  dbg_src_out         <= i_src_out_arr(0);

  gen_debug : for I in 0 to g_nof_streams - 1 generate
    -- g_use_complex = FALSE : view sosi.data
    dbg_snk_in_data_concat(         (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <=    snk_in_arr(I).data(      g_in_dat_w - 1 downto 0);
    dbg_to_mem_src_out_data_concat( (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= i_to_mem_src_out.data((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w);
    dbg_from_mem_snk_in_data_concat((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <=  from_mem_snk_in.data((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w);
    dbg_src_out_data_concat(        (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= i_src_out_arr(I).data(      g_in_dat_w - 1 downto 0);

    -- g_use_complex = TRUE : view sosi.re and sosi.im (transported via sosi.data, factor 2 = c_nof_complex)
    dbg_snk_in_re_concat(         (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <=    snk_in_arr(I).re(          g_in_dat_w - 1 downto 0);
    dbg_snk_in_im_concat(         (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <=    snk_in_arr(I).im(          g_in_dat_w - 1 downto 0);
    dbg_to_mem_src_out_re_concat( (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= i_to_mem_src_out.data((2 * I + 1) * g_in_dat_w - 1 downto (2 * I + 0) * g_in_dat_w);
    dbg_to_mem_src_out_im_concat( (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= i_to_mem_src_out.data((2 * I + 2) * g_in_dat_w - 1 downto (2 * I + 1) * g_in_dat_w);
    dbg_from_mem_snk_in_re_concat((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <=  from_mem_snk_in.data((2 * I + 1) * g_in_dat_w - 1 downto (2 * I + 0) * g_in_dat_w);
    dbg_from_mem_snk_in_im_concat((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <=  from_mem_snk_in.data((2 * I + 2) * g_in_dat_w - 1 downto (2 * I + 1) * g_in_dat_w);
    dbg_src_out_re_concat(        (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= i_src_out_arr(I).re(          g_in_dat_w - 1 downto 0);
    dbg_src_out_im_concat(        (I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= i_src_out_arr(I).im(          g_in_dat_w - 1 downto 0);
  end generate;

  g_merge_in_complex : if g_use_complex = true generate
    process(snk_in_arr)
    begin
      merged_snk_in <= snk_in_arr(0);
      for i in 0 to g_nof_streams - 1 loop
        merged_snk_in.data((2 * i + 1) * g_in_dat_w - 1 downto 2 * i * g_in_dat_w)     <= snk_in_arr(i).re(g_in_dat_w - 1 downto 0);
        merged_snk_in.data((2 * i + 2) * g_in_dat_w - 1 downto (2 * i + 1) * g_in_dat_w) <= snk_in_arr(i).im(g_in_dat_w - 1 downto 0);
      end loop;
    end process;
  end generate;

  g_merge_in_data : if g_use_complex = false generate
    process(snk_in_arr)
    begin
      merged_snk_in <= snk_in_arr(0);
      for i in 0 to g_nof_streams - 1 loop
        merged_snk_in.data((i + 1) * g_in_dat_w - 1 downto i * g_in_dat_w) <= snk_in_arr(i).data(g_in_dat_w - 1 downto 0);
      end loop;
    end process;
  end generate;

  g_siso : for J in 0 to g_nof_streams - 1 generate
    snk_out_arr(J) <= merged_snk_out;
  end generate;

  -- Pipeline merged input
  -- . to easy timing closure by decoupling the external memory interface and
  --   the streaming input interface somewhat more (erko)
  u_dp_pipeline_input : entity dp_lib.dp_pipeline
  generic map(
    g_pipeline => g_pipeline_input
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_out      => merged_snk_out,
    snk_in       => merged_snk_in,
    -- ST source
    src_in       => pipe_merged_snk_out,
    src_out      => pipe_merged_snk_in
  );

  u_sync_check_and_restore : entity dp_lib.mms_dp_sync_checker
  generic map(
    g_nof_blk_per_sync   => g_reorder_seq.nof_blocks
  )
  port map(
    mm_rst  => mm_rst,
    mm_clk  => mm_clk,
    dp_rst  => dp_rst,
    dp_clk  => dp_clk,
    snk_out => pipe_merged_snk_out,
    snk_in  => pipe_merged_snk_in,
    src_in  => sync_checked_src_in,
    src_out => sync_checked_src_out,
    reg_dp_sync_checker_mosi => reg_dp_sync_checker_mosi,
    reg_dp_sync_checker_miso => reg_dp_sync_checker_miso
  );

  gen_pre_transpose : if g_ena_pre_transp = true generate
    -- Packet merge is required for reorder_col.
    u_dp_packet_merge : entity dp_lib.dp_packet_merge
    generic map (
      g_nof_pkt => g_reorder_seq.rd_chunksize
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,

      snk_out     => sync_checked_src_in,
      snk_in      => sync_checked_src_out,

      src_in      => ss_in_siso,
      src_out     => ss_in_sosi
    );

    u_single_ss : entity work.reorder_col
    generic map (
      g_dsp_data_w         => c_data_w_pre,
      g_nof_ch_in          => c_nof_ch_in,
      g_nof_ch_sel         => c_nof_ch_sel,
      g_select_file_name   => g_select_file,
      g_use_complex        => false
    )
    port map (
      mm_rst         => mm_rst,
      mm_clk         => mm_clk,
      dp_rst         => dp_rst,
      dp_clk         => dp_clk,

      -- Memory Mapped
      ram_ss_ss_mosi => ram_ss_ss_transp_mosi,
      ram_ss_ss_miso => ram_ss_ss_transp_miso,

      -- Streaming
      input_sosi     => ss_in_sosi,
      input_siso     => OPEN,  -- Don't allow backpressure.

      output_sosi    => i_to_mem_src_out,
      output_siso    => to_mem_src_in
    );
  end generate;

  gen_not_pre_transpose : if g_ena_pre_transp = false generate
    i_to_mem_src_out    <= sync_checked_src_out;
    sync_checked_src_in <= to_mem_src_in;
  end generate;

  -- Map original dvr interface signals to t_mem_ctlr_mosi/miso
  dvr_done            <= dvr_miso.done;  -- Requested wr or rd sequence is done
  dvr_mosi.burstbegin <= dvr_en;
  dvr_mosi.wr         <= dvr_wr_not_rd;  -- No need to use dvr_mosi.rd
  dvr_mosi.address    <= RESIZE_MEM_CTLR_ADDRESS(dvr_start_address);
  dvr_mosi.burstsize  <= RESIZE_MEM_CTLR_BURSTSIZE(dvr_nof_data);
  dvr_mosi.flush      <= dvr_wr_flush_en;
  dvr_wr_flush_en     <= '0';

  u_ddr_sequencer: entity work.reorder_sequencer
  generic map(
    g_reorder_seq  => g_reorder_seq,
    g_data_w_ratio => c_data_w_ratio
  )
  port map (
    dp_rst    => dp_rst,
    dp_clk    => dp_clk,

    en_evt    => dvr_en,
    wr_not_rd => dvr_wr_not_rd,

    address   => dvr_start_address,
    burstsize => dvr_nof_data,

    done      => dvr_done
  );

  ---------------------------------------------------------------
  -- FIFO FOR SYNC-BSN
  ---------------------------------------------------------------
  u_sync_bsn_fifo : entity common_lib.common_fifo_sc
  generic map (
    g_use_lut   => true,  -- Make this FIFO in logic, since it's only 2 words deep.
    g_dat_w     => c_dp_stream_bsn_w,
    g_nof_words => 4  -- 2 sync intervals should be sufficient, choose 4 to be safe (erko)
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    wr_dat  => pipe_merged_snk_in.bsn,
    wr_req  => pipe_merged_snk_in.sync,
    wr_ful  => open,
    rd_dat  => rd_dat_i,
    rd_req  => rd_req_i,
    rd_emp  => open,
    rd_val  => rd_val_i,
    usedw   => open
  );

  ---------------------------------------------------------------
  -- CREATE READ-AHEAD FIFO INTERFACE FOR SYNC-BSN
  ---------------------------------------------------------------
  u_fifo_adapter : entity common_lib.common_fifo_rd
  generic map (
    g_dat_w => c_dp_stream_bsn_w
  )
  port map(
    rst        => dp_rst,
    clk        => dp_clk,
    -- ST sink: RL = 1
    fifo_req   => rd_req_i,
    fifo_dat   => rd_dat_i,
    fifo_val   => rd_val_i,
    -- ST source: RL = 0
    rd_req     => block_gen_out_sosi.sync,
    rd_dat     => sync_bsn,
    rd_val     => open
  );

  -----------------------
  -- Pipeline to match latency of dp_block_gen
  -----------------------
  u_dp_pipeline : entity dp_lib.dp_pipeline
  generic map(
    g_pipeline => 1
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_out      => OPEN,
    snk_in       => from_mem_snk_in,
    -- ST source
    src_in       => OPEN,
    src_out      => from_mem_out_sosi
  );

  --------------------
  -- DP BLOCK GEN (providing sop/eop)
  --------------------
  u_dp_block_gen : entity dp_lib.dp_block_gen
  generic map(
    g_use_src_in       => false,
    g_nof_data         => g_frame_size_out,
    g_nof_blk_per_sync => g_reorder_seq.nof_blocks,
    g_empty            => 0,
    g_channel          => 0,
    g_error            => 0
  )
  port map(
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => from_mem_snk_in,

    -- Use incoming data to generate more data
    src_out    => block_gen_out_sosi,
    en         => '1'
  );

  from_mem_snk_out <= src_in_arr(0);

  g_merge_out_complex : if g_use_complex = true generate
    gen_merge_out : process(block_gen_out_sosi, from_mem_out_sosi, sync_bsn)
    begin
      for i in 0 to g_nof_streams - 1 loop
        merge_src_out_arr(i)       <= block_gen_out_sosi;
        merge_src_out_arr(i).valid <= from_mem_out_sosi.valid;
        merge_src_out_arr(i).re    <= RESIZE_DP_DSP_DATA(from_mem_out_sosi.data((2 * i + 1) * g_in_dat_w - 1 downto 2 * i * g_in_dat_w));
        merge_src_out_arr(i).im    <= RESIZE_DP_DSP_DATA(from_mem_out_sosi.data((2 * i + 2) * g_in_dat_w - 1 downto (2 * i + 1) * g_in_dat_w));
        if (block_gen_out_sosi.sync = '1') then
          merge_src_out_arr(i).bsn <= sync_bsn;
        end if;
      end loop;
    end process;
  end generate;

  g_merge_out_data : if g_use_complex = false generate
    gen_merge_out : process(block_gen_out_sosi, from_mem_out_sosi)
    begin
      for i in 0 to g_nof_streams - 1 loop
        merge_src_out_arr(i)       <= block_gen_out_sosi;
        merge_src_out_arr(i).valid <= from_mem_out_sosi.valid;
        merge_src_out_arr(i).data  <= RESIZE_DP_DATA(from_mem_out_sosi.data((i + 1) * g_in_dat_w - 1 downto i * g_in_dat_w));
        if (block_gen_out_sosi.sync = '1') then
          merge_src_out_arr(i).bsn <= sync_bsn;
        end if;
      end loop;
    end process;
  end generate;

  -----------------------
  -- Pipeline merged output arr
  -- . to easy timing closure by decoupling the external memory interface and
  --   the streaming output interface somewhat more (erko)
  -----------------------
  u_dp_pipeline_arr : entity dp_lib.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_streams,
    g_pipeline    => g_pipeline_output  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in_arr   => merge_src_out_arr,
    -- ST source
    src_out_arr  => i_src_out_arr
  );
end str;
