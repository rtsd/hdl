###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

source $::env(HDL_WORK)/boards/uniboard2b/libraries/unb2b_board/quartus/pinning/unb2b_minimal_pins.tcl
# unb2b_jesd204_pins contains undesired QSFP pinning
#source $::env(HDL_WORK)/boards/uniboard2b/libraries/unb2b_board/quartus/pinning/unb2b_jesd204b_pins.tcl
#
#=====================
# QSFP pins
# ====================

set_location_assignment PIN_AL32 -to CLKUSR

set_location_assignment PIN_Y36 -to SA_CLK
set_instance_assignment -name IO_STANDARD LVDS -to SA_CLK
# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SA_CLK

set_location_assignment PIN_AH9 -to SB_CLK
set_instance_assignment -name IO_STANDARD LVDS -to SB_CLK
# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SB_CLK


set_global_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON


set_location_assignment PIN_AT31 -to QSFP_RST

set_location_assignment PIN_AY33 -to QSFP_SCL[0]
set_location_assignment PIN_AY32 -to QSFP_SCL[1]
set_location_assignment PIN_AY30 -to QSFP_SCL[2]
set_location_assignment PIN_AN33 -to QSFP_SCL[3]
set_location_assignment PIN_AN31 -to QSFP_SCL[4]
set_location_assignment PIN_AJ33 -to QSFP_SCL[5]
set_location_assignment PIN_BA32 -to QSFP_SDA[0]
set_location_assignment PIN_BA31 -to QSFP_SDA[1]
set_location_assignment PIN_AP33 -to QSFP_SDA[2]
set_location_assignment PIN_AM33 -to QSFP_SDA[3]
set_location_assignment PIN_AK33 -to QSFP_SDA[4]
set_location_assignment PIN_AH32 -to QSFP_SDA[5]

set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_RST

# QSFP_0_RX
set_location_assignment PIN_AN38 -to QSFP_0_RX[0]
set_location_assignment PIN_AM40 -to QSFP_0_RX[1]
set_location_assignment PIN_AK40 -to QSFP_0_RX[2]
set_location_assignment PIN_AJ38 -to QSFP_0_RX[3]

# QSFP_0_TX
set_location_assignment PIN_AN42 -to QSFP_0_TX[0]
set_location_assignment PIN_AM44 -to QSFP_0_TX[1]
set_location_assignment PIN_AK44 -to QSFP_0_TX[2]
set_location_assignment PIN_AJ42 -to QSFP_0_TX[3]


### QSFP_1_RX
set_location_assignment PIN_AC38 -to QSFP_1_RX[0]
set_location_assignment PIN_AD40 -to QSFP_1_RX[1]
set_location_assignment PIN_AF40 -to QSFP_1_RX[2]
set_location_assignment PIN_AG38 -to QSFP_1_RX[3]

### QSFP_1_TX
set_location_assignment PIN_AC42 -to QSFP_1_TX[0]
set_location_assignment PIN_AD44 -to QSFP_1_TX[1]
set_location_assignment PIN_AF44 -to QSFP_1_TX[2]
set_location_assignment PIN_AG42 -to QSFP_1_TX[3]

# RING pinning location
set_location_assignment PIN_AP40 -to RING_0_RX[0]
set_location_assignment PIN_AR38 -to RING_0_RX[1]
set_location_assignment PIN_AT40 -to RING_0_RX[2]
set_location_assignment PIN_AU38 -to RING_0_RX[3]
set_location_assignment PIN_AP44 -to RING_0_TX[0]
set_location_assignment PIN_AR42 -to RING_0_TX[1]
set_location_assignment PIN_AT44 -to RING_0_TX[2]
set_location_assignment PIN_AU42 -to RING_0_TX[3]
set_location_assignment PIN_H40 -to RING_1_RX[0]
set_location_assignment PIN_J38 -to RING_1_RX[1]
set_location_assignment PIN_F40 -to RING_1_RX[2]
set_location_assignment PIN_G38 -to RING_1_RX[3]
set_location_assignment PIN_H44 -to RING_1_TX[0]
set_location_assignment PIN_J42 -to RING_1_TX[1]
set_location_assignment PIN_G42 -to RING_1_TX[2]
set_location_assignment PIN_F44 -to RING_1_TX[3]



