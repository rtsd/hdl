-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra,  20 jul 2017,   Created
-- Purpose: Align output to the toggling input and maintain this toggling
-- Description:
--   The in_toggle has a toggle period of g_nof_clk_per_period clk cycles.
--
--   - When in_align = '1' then the out_toggle follows the in_toggle.
--   - When in_align = '0' then the out_toggle maintains the current toggling
--     independent of in_toggle
--
--   This compenent can be used to in_align to a certain input phase that is
--   defined by the toggling in_toggle and then maintain this input phase
--   even when in_toggle stops toggling or change phase for some reason.

library IEEE;
use IEEE.std_logic_1164.all;

entity common_toggle_align is
  generic (
    g_pipeline            : natural := 1;  -- 0 for combinatorial to have out_toggle in phase with in_toggle, > 0 for pipelined out_toggle
    g_reset_value         : natural := 0;  -- 0 or 1, bit reset value for out_toggle
    g_nof_clk_per_period  : natural := 4  -- must be even and >= 2
  );
  port (
    rst         : in  std_logic := '0';
    clk         : in  std_logic;
    in_align    : in  std_logic := '1';
    in_toggle   : in  std_logic;
    out_toggle  : out std_logic
  );
end;

architecture rtl of common_toggle_align is
  signal prev_in_toggle       : std_logic;
  signal in_toggle_revt       : std_logic;

  signal cnt_clr              : std_logic;
  signal cnt                  : natural range 0 to g_nof_clk_per_period - 1;
  signal nxt_cnt              : natural;

  signal nxt_out_toggle       : std_logic;
begin
  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      cnt            <= 0;
      prev_in_toggle <= '1';  -- use '1' to avoid any initial in_toggle_revt (this is not critical though)
    elsif rising_edge(clk) then
      cnt            <= nxt_cnt;
      prev_in_toggle <= in_toggle;
    end if;
  end process;

  -- detect rising edge of in_toggle period
  in_toggle_revt <= '1' when prev_in_toggle = '0' and in_toggle = '1' else '0';

  cnt_clr <= in_align and in_toggle_revt;

  nxt_cnt <= 0 when cnt_clr = '1' or cnt = g_nof_clk_per_period - 1 else cnt + 1;

  -- align to input toggle or maintain output toggle
  p_out_toggle : process (in_align, in_toggle, nxt_cnt)
  begin
    if in_align = '1' then
      -- align to incomming in_toggle
      nxt_out_toggle <= in_toggle;
    else
      -- maintain phase and period of incomming in_toggle that it was aligned to
      if nxt_cnt < g_nof_clk_per_period / 2 then
        nxt_out_toggle <= '1';
      else
        nxt_out_toggle <= '0';
      end if;
    end if;
  end process;

  u_common_pipeline_sl : entity work.common_pipeline_sl
  generic map (
    g_pipeline    => g_pipeline,
    g_reset_value => g_reset_value
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => nxt_out_toggle,
    out_dat => out_toggle
  );
end rtl;
