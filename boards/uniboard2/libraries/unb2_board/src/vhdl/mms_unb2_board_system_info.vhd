-------------------------------------------------------------------------------
--
-- Copyright (C) 2012-2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.unb2_board_pkg.all;
use technology_lib.technology_pkg.all;

entity mms_unb2_board_system_info is
  generic (
    g_sim         : boolean := false;
    g_technology  : natural := c_tech_arria10;
    g_design_name : string;
    g_fw_version  : t_unb2_board_fw_version := c_unb2_board_fw_version;  -- firmware version x.y
    g_stamp_date  : natural := 0;
    g_stamp_time  : natural := 0;
    g_stamp_svn   : natural := 0;
    g_design_note : string  := "";
    g_rom_version : natural := 1;
    g_aux         : t_c_unb2_board_aux := c_unb2_board_aux  -- aux contains the hardware version
  );
  port (
    mm_rst          : in    std_logic;
    mm_clk          : in    std_logic;

    -- MM registers
    reg_mosi        : in    t_mem_mosi := c_mem_mosi_rst;
    reg_miso        : out   t_mem_miso;

    rom_mosi        : in    t_mem_mosi := c_mem_mosi_rst;
    rom_miso        : out   t_mem_miso;

    hw_version      : in  std_logic_vector(g_aux.version_w - 1 downto 0);
    id              : in  std_logic_vector(g_aux.id_w - 1 downto 0);

    chip_id         : out std_logic_vector(c_unb2_board_nof_chip_w - 1 downto 0);
    bck_id          : out std_logic_vector(c_unb2_board_nof_uniboard_w - 1 downto 0);

    -- Info output still supported for older designs
    info            : out std_logic_vector(c_word_w - 1 downto 0)
    );
end mms_unb2_board_system_info;

architecture str of mms_unb2_board_system_info is
  -- Provide different prefixes (absolute and relative) for the same path. ModelSim understands $UNB, Quartus does not.
  -- Required because the work paths of ModelSim and Quartus are different.
  constant c_quartus_path_prefix  : string := "";
  constant c_modelsim_path_prefix : string := "$UNB/Firmware/designs/" & g_design_name & "/build/synth/quartus/";
  constant c_path_prefix          : string := sel_a_b(g_sim, c_modelsim_path_prefix, c_quartus_path_prefix);

-- No longer supporting MIF files in sim as non-$UNB (e.g. $AARTFAAC) designs will cause path error.
--  CONSTANT c_mif_name    : STRING := sel_a_b((g_design_name="UNUSED"), g_design_name, c_path_prefix & g_design_name & ".mif");
  constant c_mif_name    : string :=  sel_a_b(g_sim, "UNUSED", sel_a_b((g_design_name = "UNUSED"), g_design_name, c_path_prefix & g_design_name & ".mif"));

  constant c_rom_addr_w  : natural := 10;  -- 2^10 = 1024 addresses * 32 bits = 4 kiB

  constant c_mm_rom      : t_c_mem := (latency  => 1,
                                      adr_w    => c_rom_addr_w,
                                      dat_w    => c_word_w,
                                      nof_dat  => 2**c_rom_addr_w,  -- = 2**adr_w
                                      init_sl  => '0');

  signal i_info          : std_logic_vector(c_word_w - 1 downto 0);
begin
 info <= i_info;

  u_unb2_board_system_info: entity work.unb2_board_system_info
  generic map (
    g_sim        => g_sim,
    g_fw_version => g_fw_version,
    g_rom_version => g_rom_version,
    g_technology  => g_technology
  )
  port map (
    clk        => mm_clk,
    hw_version => hw_version,
    id         => id,
    info       => i_info,
    chip_id    => chip_id,
    bck_id     => bck_id
   );

  u_unb2_board_system_info_reg: entity work.unb2_board_system_info_reg
  generic map (
    g_design_name => g_design_name,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_stamp_svn   => g_stamp_svn,
    g_design_note => g_design_note
  )
  port map (
    mm_rst    => mm_rst,
    mm_clk    => mm_clk,

    sla_in    => reg_mosi,
    sla_out   => reg_miso,

    info      => i_info
  );

  u_common_rom : entity common_lib.common_rom
  generic map (
    g_technology => g_technology,
    g_ram       => c_mm_rom,
    g_init_file => c_mif_name
  )
  port map (
    rst     => mm_rst,
    clk     => mm_clk,
    rd_en   => rom_mosi.rd,
    rd_adr  => rom_mosi.address(c_mm_rom.adr_w - 1 downto 0),
    rd_dat  => rom_miso.rddata(c_mm_rom.dat_w - 1 downto 0),
    rd_val  => rom_miso.rdval
  );
end str;
