-------------------------------------------------------------------------------
--
-- Copyright (C) 2012-2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for unb2_fpga_sens
--

library IEEE, common_lib, technology_lib, fpga_sense_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;

entity unb2_fpga_sens_reg is
  generic (
    g_sim             : boolean;
    g_technology      : natural := c_tech_arria10;
    g_sens_nof_result : natural := 1;
    g_temp_high       : natural := 85
  );
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock
    start      : in  std_logic;

    -- Memory Mapped Slave in mm_clk domain
    sla_temp_in     : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_temp_out    : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    sla_voltage_in     : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_voltage_out    : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers
    --sens_err   : IN  STD_LOGIC := '0';
    --sens_data  : IN  t_slv_8_arr(0 TO g_sens_nof_result-1); -- FIXME should be OUT

    -- Max temp output
    temp_high  : out std_logic_vector(6 downto 0)
  );
end unb2_fpga_sens_reg;

architecture str of unb2_fpga_sens_reg is
  --SIGNAL i_temp_high    : STD_LOGIC_VECTOR(6 DOWNTO 0);
begin
  temp_high <= (others => '0');  -- i_temp_high;

  u_fpga_sense: entity fpga_sense_lib.fpga_sense
  generic map (
    g_technology => g_technology,
    g_sim        => g_sim
  )
  port map (
    mm_clk      => mm_clk,
    mm_rst      => mm_rst,

    start_sense => start,

    reg_temp_mosi    => sla_temp_in,
    reg_temp_miso    => sla_temp_out,

    reg_voltage_store_mosi    => sla_voltage_in,
    reg_voltage_store_miso    => sla_voltage_out
  );
end str;
