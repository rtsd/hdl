-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Authors : J Hargreaves, L Hiemstra
-- Purpose:
--   AIT - ADC (Jesd) receiver, input, timing and associated diagnostic blocks
-- Description:
--   Unb2c version for lab testing
--   Contains all the signal processing blocks to receive and time the ADC input data
--   See https://support.astron.nl/confluence/display/STAT/L5+SDPFW+DD%3A+ADC+data+input+and+timestamp

library IEEE, common_lib, unb2c_board_lib, technology_lib, diag_lib, dp_lib, tech_jesd204b_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use unb2c_board_lib.unb2c_board_peripherals_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity node_adc_input_and_timing_nowg is
  generic (
    g_technology              : natural := c_tech_arria10_e2sg;
    g_jesd_freq               : string  := "200MHz";
    g_buf_nof_data            : natural := 131072;  -- 8192; --1024;
    g_nof_streams             : natural := 12;
    g_nof_sync_n              : natural := c_unb2c_board_nof_sync_jesd204b;  -- n ADCs per RCU share a sync
    g_bsn_sync_timeout        : natural := 200000000;  -- Default 200M, overide for short simulation
    g_sim                     : boolean := false
  );
  port (
    -- clocks and resets
    mm_clk                         : in std_logic;
    mm_rst                         : in std_logic;
    dp_clk                         : in std_logic;
    dp_rst                         : in std_logic;

    -- mm control buses
    -- JESD
    jesd204b_mosi                  : in  t_mem_mosi := c_mem_mosi_rst;
    jesd204b_miso                  : out t_mem_miso := c_mem_miso_rst;

    -- bsn source
    reg_bsn_source_mosi            : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_source_miso            : out t_mem_miso := c_mem_miso_rst;

    -- BSN MONITOR
    reg_bsn_monitor_input_mosi     : in  t_mem_mosi;
    reg_bsn_monitor_input_miso     : out t_mem_miso;

    -- Data buffer for framed samples (variable depth)
    ram_diag_data_buf_bsn_mosi     : in  t_mem_mosi;
    ram_diag_data_buf_bsn_miso     : out t_mem_miso;
    reg_diag_data_buf_bsn_mosi     : in  t_mem_mosi;
    reg_diag_data_buf_bsn_miso     : out t_mem_miso;

    -- JESD control
    jesd_ctrl_mosi                 : in  t_mem_mosi;
    jesd_ctrl_miso                 : out t_mem_miso;

    -- JESD io signals
    jesd204b_serial_data           : in    std_logic_vector((c_unb2c_board_tr_jesd204b.bus_w * c_unb2c_board_tr_jesd204b.nof_bus) - 1 downto 0);
    jesd204b_refclk                : in    std_logic;
    jesd204b_sysref                : in    std_logic;
    jesd204b_sync_n                : out   std_logic_vector(g_nof_sync_n - 1 downto 0)
  );
end node_adc_input_and_timing_nowg;

architecture str of node_adc_input_and_timing_nowg is
  constant c_mm_jesd_ctrl_reg       : t_c_mem := (latency  => 1,
                                                  adr_w    => 1,
                                                  dat_w    => c_word_w,
                                                  nof_dat  => 1,
                                                  init_sl  => '0');

  -- Frame parameters TBC
  constant c_bs_bsn_w               : natural := 64;  -- 51;
  constant c_bs_block_size          : natural := 1024;
  constant c_bs_nof_block_per_sync  : natural := 390625;  -- generate a sync every 2s for testing
  constant c_data_w                 : natural := 16;

  -- JESD signals
  signal rx_clk                     : std_logic;  -- formerly jesd204b_frame_clk
  signal rx_rst                     : std_logic;
  signal rx_sysref                  : std_logic;

  -- Sosis and sosi arrays
  signal rx_sosi_arr                : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal bs_sosi                    : t_dp_sosi;
  signal mux_sosi_arr               : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal st_sosi_arr                : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal mm_rst_internal            : std_logic;
  signal mm_jesd_ctrl_reg           : std_logic_vector(c_word_w - 1 downto 0);
  signal jesd204b_disable_arr       : std_logic_vector(g_nof_streams - 1 downto 0);
  signal jesd204b_reset             : std_logic;
begin
  -- The node AIT is reset at power up by mm_rst and under software control by jesd204b_reset.
  -- The mm_rst internal will cause a reset on the rx_rst by the reset sequencer in the u_jesd204b.
  -- The MM jesd204b_reset is intended for node AIT resynchronisation tests of the u_jesd204b.
  -- The MM jesd204b_reset should not be applied in an SDP application, because this will cause
  -- a disturbance in the block timing of the out_sosi_arr(i).sync,bsn,sop,eop. The other logic
  -- in an SDP application assumes that the block timing of the out_sosi_arr(i) only contains
  -- complete blocks, so from sop to eop.

  mm_rst_internal <= mm_rst or mm_jesd_ctrl_reg(31);

  gen_jesd_disable : for I in 0 to g_nof_streams - 1 generate
    jesd204b_disable_arr(i) <= mm_jesd_ctrl_reg(i);
  end generate;

  -----------------------------------------------------------------------------
  -- JESD204B IP (ADC Handler)
  -----------------------------------------------------------------------------

  u_jesd204b: entity tech_jesd204b_lib.tech_jesd204b
  generic map(
    g_sim                => g_sim,
    g_technology         => g_technology,
    g_nof_streams        => g_nof_streams,
    g_nof_sync_n         => g_nof_sync_n,
    g_jesd_freq          => g_jesd_freq
  )
  port map(
    jesd204b_refclk      => jesd204b_refclk,
    jesd204b_sysref      => jesd204b_sysref,
    jesd204b_sync_n_arr  => jesd204b_sync_n,

    rx_sosi_arr          => rx_sosi_arr,
    rx_clk               => rx_clk,
    rx_rst               => rx_rst,
    rx_sysref            => rx_sysref,

    jesd204b_disable_arr  => jesd204b_disable_arr,

    -- MM
    mm_clk               => mm_clk,
    mm_rst               => mm_rst_internal,

    jesd204b_mosi        => jesd204b_mosi,
    jesd204b_miso        => jesd204b_miso,

     -- Serial
    serial_tx_arr        => open,
    serial_rx_arr        => jesd204b_serial_data(g_nof_streams - 1 downto 0)
  );

  -----------------------------------------------------------------------------
  -- Timestamp
  -----------------------------------------------------------------------------
  u_bsn_source : entity dp_lib.mms_dp_bsn_source
  generic map (
    g_cross_clock_domain     => true,
    g_block_size             => c_bs_block_size,
    g_nof_block_per_sync     => c_bs_nof_block_per_sync,
    g_bsn_w                  => c_bs_bsn_w
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst_internal,
    mm_clk            => mm_clk,
    dp_rst            => rx_rst,
    dp_clk            => rx_clk,
    dp_pps            => rx_sysref,

    -- Memory-mapped clock domain
    reg_mosi          => reg_bsn_source_mosi,
    reg_miso          => reg_bsn_source_miso,

    -- Streaming clock domain
    bs_sosi           => bs_sosi
  );

  mux_sosi_arr  <= rx_sosi_arr when rising_edge(rx_clk);

  -----------------------------------------------------------------------------
  -- Concatenate muxed data streams with bsn framing
  -----------------------------------------------------------------------------

  gen_concat : for I in 0 to g_nof_streams - 1 generate
    p_sosi : process(mux_sosi_arr(I), bs_sosi)
    begin
      st_sosi_arr(I)       <= bs_sosi;
      st_sosi_arr(I).data  <= mux_sosi_arr(I).data;
    end process;
  end generate;

  ---------------------------------------------------------------------------------------
  -- Diagnostics on the bsn-framed data
  --   . BSN Monitor (ToDo: can be removed as not part of the spec)
  --   . Aduh monitor
  --   . Data Buffer (variable depth from 1k-128k)
  ---------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------
  -- BSN monitor (Block Checker)
  ---------------------------------------------------------------------------------------
  u_bsn_monitor : entity dp_lib.mms_dp_bsn_monitor
  generic map (
    g_nof_streams        => 1,  -- They're all the same
    g_sync_timeout       => g_bsn_sync_timeout,
    g_bsn_w              => c_bs_bsn_w,
    g_log_first_bsn      => false
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst_internal,
    mm_clk      => mm_clk,
    reg_mosi    => reg_bsn_monitor_input_mosi,
    reg_miso    => reg_bsn_monitor_input_miso,

    -- Streaming clock domain
    dp_rst      => rx_rst,
    dp_clk      => rx_clk,
    in_sosi_arr => st_sosi_arr(0 downto 0)
  );

 -----------------------------------------------------------------------------
-- Diagnostic Data Buffer
  -----------------------------------------------------------------------------

  u_diag_data_buffer_bsn : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_technology   => g_technology,
    g_nof_streams  => g_nof_streams,
    g_data_w       => c_data_w,
    g_buf_nof_data => g_buf_nof_data,
    g_buf_use_sync => true  -- when TRUE start filling the buffer at the in_sync, else after the last word was read
  )
  port map (
    mm_rst            => mm_rst_internal,
    mm_clk            => mm_clk,
    dp_rst            => rx_rst,
    dp_clk            => rx_clk,

    ram_data_buf_mosi => ram_diag_data_buf_bsn_mosi,
    ram_data_buf_miso => ram_diag_data_buf_bsn_miso,
    reg_data_buf_mosi => reg_diag_data_buf_bsn_mosi,
    reg_data_buf_miso => reg_diag_data_buf_bsn_miso,

    in_sosi_arr       => st_sosi_arr,
    in_sync           => st_sosi_arr(0).sync
  );

  -----------------------------------------------------------------------------
  -- JESD Control register
  -----------------------------------------------------------------------------
  u_mm_jesd_ctrl_reg : entity common_lib.common_reg_r_w
  generic map (
    g_reg       => c_mm_jesd_ctrl_reg,
    g_init_reg  => (others => '0')
  )
  port map (
    rst       => mm_rst,
    clk       => mm_clk,
    -- control side
    wr_en     => jesd_ctrl_mosi.wr,
    wr_adr    => jesd_ctrl_mosi.address(c_mm_jesd_ctrl_reg.adr_w - 1 downto 0),
    wr_dat    => jesd_ctrl_mosi.wrdata(c_mm_jesd_ctrl_reg.dat_w - 1 downto 0),
    rd_en     => jesd_ctrl_mosi.rd,
    rd_adr    => jesd_ctrl_mosi.address(c_mm_jesd_ctrl_reg.adr_w - 1 downto 0),
    rd_dat    => jesd_ctrl_miso.rddata(c_mm_jesd_ctrl_reg.dat_w - 1 downto 0),
    rd_val    => OPEN,
    -- data side
    out_reg   => mm_jesd_ctrl_reg,
    in_reg    => mm_jesd_ctrl_reg
  );
end str;
