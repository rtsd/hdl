------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity unb1_bn_terminal_bg is
  generic (
    -- General
    g_sim           : boolean := false;
    g_rev_multi_unb : boolean := false;  -- Set to TRUE by Quartus revision bn_terminal_bg_rev_multi
    g_design_name   : string  := "unb1_bn_terminal_bg";  -- Set to "bn_terminal_bg_rev_multi_unb" by revision bn_terminal_bg_rev_multi
    g_design_note   : string  := "UNUSED";
    g_stamp_date    : natural := 0;  -- Date (YYYYMMDD)
    g_stamp_time    : natural := 0;  -- Time (HHMMSS)
    g_stamp_svn     : natural := 0  -- SVN revision
  );
  port (
   -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc      : inout std_logic;
    sens_sd      : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk      : in    std_logic;
    ETH_SGIN     : in    std_logic;
    ETH_SGOUT    : out   std_logic;

    -- Transceiver clocks
    SA_CLK       : in  std_logic := '0';  -- TR clock BN-BI (backplane)
    SB_CLK       : in  std_logic := '0';  -- TR clock FN-BN (mesh)

    -- Serial I/O
    FN_BN_0_TX   : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_0_RX   : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_1_TX   : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_1_RX   : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_2_TX   : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_2_RX   : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_3_TX   : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_3_RX   : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');

    BN_BI_0_TX   : out std_logic_vector(c_unb1_board_tr_back.bus_w - 1 downto 0);
    BN_BI_0_RX   : in  std_logic_vector(c_unb1_board_tr_back.bus_w - 1 downto 0);
    BN_BI_1_TX   : out std_logic_vector(c_unb1_board_tr_back.bus_w - 1 downto 0);
    BN_BI_1_RX   : in  std_logic_vector(c_unb1_board_tr_back.bus_w - 1 downto 0);
    BN_BI_2_TX   : out std_logic_vector(c_unb1_board_tr_back.bus_w - 1 downto 0);
    BN_BI_2_RX   : in  std_logic_vector(c_unb1_board_tr_back.bus_w - 1 downto 0)
  );
end unb1_bn_terminal_bg;

architecture str of unb1_bn_terminal_bg is
  constant c_fw_version    : t_unb1_board_fw_version := (1, 0);  -- firmware version x.y
    -- Use PHY Interface
    -- TYPE t_c_unb_use_phy IS RECORD
    --   eth1g   : NATURAL;
    --   tr_front: NATURAL;
    --   tr_mesh : NATURAL;
    --   tr_back : NATURAL;
    --   ddr3_I  : NATURAL;
    --   ddr3_II : NATURAL;
    --   adc     : NATURAL;
    --   wdi     : NATURAL;
    -- END RECORD;
  constant c_use_phy                : t_c_unb1_board_use_phy := (1, 0, 1, 1, 0, 0, 0, 1);
  -- Transceivers Interface
  constant c_tr_mesh                : t_c_unb1_board_tr := c_unb1_board_tr_mesh;
  constant c_tr_back                : t_c_unb1_board_tr := c_unb1_board_tr_back;
  -- Auxiliary Interface
  constant c_aux                    : t_c_unb1_board_aux := c_unb1_board_aux;

  constant c_usr_nof_streams        : natural := 16;
  constant c_usr_block_len          : natural := 96;
  constant c_ram_diag_bg_addr_w     : natural := ceil_log2(c_usr_nof_streams * pow2(ceil_log2(c_usr_block_len)));  -- Total addressable nof words required: 16 streams * (128 words / block_gen).

  -- Mesh settings
  constant c_use_mesh               : boolean := c_use_phy.tr_mesh = 1;
  constant c_mesh_nof_serial        : natural := 3;  -- default only use the 3 full featured transceivers in the mesh (out of maximal 4), using only 2 is not enough
  constant c_mesh_use_rx            : boolean := true;  -- can be FALSE for BG functionality, use TRUE to support for bidirectional TR diagnostics
  constant c_mesh_gx_mbps           : natural := 5000;

  constant c_mesh_mon_select        : natural := 5;  -- > 0 = enable SOSI data buffers monitor via MM
  constant c_mesh_mon_nof_words     : natural := c_unb1_board_peripherals_mm_reg_default.ram_diag_db_buf_size;  -- = 1024
  constant c_mesh_mon_use_sync      : boolean := true;  -- when TRUE use dp_pps to trigger the data buffer capture, else new data capture after read access of last data word

  -- System
  signal cs_sim                     : std_logic;
  signal xo_clk                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal cal_clk                    : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_locked                  : std_logic;
  signal mm_rst                     : std_logic;

  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;
  signal dp_pps                     : std_logic;

  signal this_chip_id               : std_logic_vector(c_unb1_board_nof_chip_w - 1 downto 0);  -- ID[2:0], so range 0-3 for FN and range 4-7 BN
  signal this_bck_id                : std_logic_vector(c_unb1_board_nof_uniboard_w - 1 downto 0);  -- ID[7:3]

  -- PIOs
  signal pout_debug_wave            : std_logic_vector(c_word_w - 1 downto 0);
  signal pout_wdi                   : std_logic;
  signal pin_pps                    : std_logic_vector(c_word_w - 1 downto 0);
  signal pin_intab                  : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
  signal pout_intab                 : std_logic_vector(c_word_w - 1 downto 0);

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

 -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;  -- mms_unb_sens registers
  signal reg_unb_sens_miso          : t_mem_miso;

  -- eth1g
  signal eth1g_tse_clk              : std_logic;
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- tr_mesh
  signal mesh_tx_serial_2arr        : t_unb1_board_mesh_sl_2arr;  -- Tx
  signal mesh_rx_serial_2arr        : t_unb1_board_mesh_sl_2arr;  -- Rx support for diagnostics

  -- tr_back
  signal back_tx_serial_2arr        : t_unb1_board_back_sl_2arr;  -- Tx
  signal back_rx_serial_2arr        : t_unb1_board_back_sl_2arr;  -- Rx

  -- MM block generator register for BN-FN streams[0:15] = [0,4,8,12, 1,5,9,13, 2,6,10,14, 3,7,11,15]
  signal reg_diag_bg_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diag_bg_miso           : t_mem_miso;
  signal ram_diag_bg_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal ram_diag_bg_miso           : t_mem_miso;

  -- MM tr_nonbonded with diagnostics, mesh
  signal reg_mesh_tr_nonbonded_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_mesh_tr_nonbonded_miso : t_mem_miso;
  signal reg_mesh_diagnostics_mosi  : t_mem_mosi := c_mem_mosi_rst;
  signal reg_mesh_diagnostics_miso  : t_mem_miso;

  -- MM diag_data_buffer, mesh
  signal ram_mesh_diag_data_buf_mosi: t_mem_mosi := c_mem_mosi_rst;
  signal ram_mesh_diag_data_buf_miso: t_mem_miso;

  -- MM tr_nonbonded with diagnostics, back
  signal reg_back_tr_nonbonded_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_back_tr_nonbonded_miso : t_mem_miso;
  signal reg_back_diagnostics_mosi  : t_mem_mosi := c_mem_mosi_rst;
  signal reg_back_diagnostics_miso  : t_mem_miso;

  -- STREAM
  signal in_sosi_arr                : t_dp_sosi_arr(c_usr_nof_streams - 1 downto 0);
  signal in_siso_arr                : t_dp_siso_arr(c_usr_nof_streams - 1 downto 0) := (others => c_dp_siso_rst);
begin
  -----------------------------------------------------------------------------
  -- SOPC system
  -----------------------------------------------------------------------------

  u_sopc : entity work.sopc_unb1_bn_terminal_bg
  port map (
    -- 1) global signals:
    clk_0                                         => xo_clk,  -- PLL reference = 25 MHz from ETH_clk pin
    reset_n                                       => xo_rst_n,
    mm_clk                                        => mm_clk,  -- PLL clk[0] = 125 MHz system clock that the NIOS2 and the MM bus run on
    cal_clk                                       => cal_clk,  -- PLL clk[1] =  40 MHz calibration clock for the IO reconfiguration
    tse_clk                                       => eth1g_tse_clk,  -- PLL clk[2] = 125 MHz dedicated clock for the 1 Gbit Ethernet unit

    -- the_altpll_0
    areset_to_the_altpll_0                        => '0',
    locked_from_the_altpll_0                      => mm_locked,
    phasedone_from_the_altpll_0                   => OPEN,

    -- the_avs_eth_0
    coe_clk_export_from_the_avs_eth_0             => OPEN,
    coe_reset_export_from_the_avs_eth_0           => eth1g_mm_rst,
    coe_tse_address_export_from_the_avs_eth_0     => eth1g_tse_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
    coe_tse_write_export_from_the_avs_eth_0       => eth1g_tse_mosi.wr,
    coe_tse_writedata_export_from_the_avs_eth_0   => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
    coe_tse_read_export_from_the_avs_eth_0        => eth1g_tse_mosi.rd,
    coe_tse_readdata_export_to_the_avs_eth_0      => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
    coe_tse_waitrequest_export_to_the_avs_eth_0   => eth1g_tse_miso.waitrequest,
    coe_reg_address_export_from_the_avs_eth_0     => eth1g_reg_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
    coe_reg_write_export_from_the_avs_eth_0       => eth1g_reg_mosi.wr,
    coe_reg_writedata_export_from_the_avs_eth_0   => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
    coe_reg_read_export_from_the_avs_eth_0        => eth1g_reg_mosi.rd,
    coe_reg_readdata_export_to_the_avs_eth_0      => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
    coe_irq_export_to_the_avs_eth_0               => eth1g_reg_interrupt,
    coe_ram_address_export_from_the_avs_eth_0     => eth1g_ram_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
    coe_ram_write_export_from_the_avs_eth_0       => eth1g_ram_mosi.wr,
    coe_ram_writedata_export_from_the_avs_eth_0   => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
    coe_ram_read_export_from_the_avs_eth_0        => eth1g_ram_mosi.rd,
    coe_ram_readdata_export_to_the_avs_eth_0      => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),

    -- the_reg_unb_sens
    coe_address_export_from_the_reg_unb_sens      => reg_unb_sens_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
    coe_clk_export_from_the_reg_unb_sens          => OPEN,
    coe_read_export_from_the_reg_unb_sens         => reg_unb_sens_mosi.rd,
    coe_readdata_export_to_the_reg_unb_sens       => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
    coe_reset_export_from_the_reg_unb_sens        => OPEN,
    coe_write_export_from_the_reg_unb_sens        => reg_unb_sens_mosi.wr,
    coe_writedata_export_from_the_reg_unb_sens    => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_diag_bg
    coe_address_export_from_the_reg_diag_bg       => reg_diag_bg_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_bg_adr_w - 1 downto 0),
    coe_clk_export_from_the_reg_diag_bg           => OPEN,
    coe_read_export_from_the_reg_diag_bg          => reg_diag_bg_mosi.rd,
    coe_readdata_export_to_the_reg_diag_bg        => reg_diag_bg_miso.rddata(c_word_w - 1 downto 0),
    coe_reset_export_from_the_reg_diag_bg         => OPEN,
    coe_write_export_from_the_reg_diag_bg         => reg_diag_bg_mosi.wr,
    coe_writedata_export_from_the_reg_diag_bg     => reg_diag_bg_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_ram_diag_bg
    coe_address_export_from_the_ram_diag_bg       => ram_diag_bg_mosi.address(c_ram_diag_bg_addr_w - 1 downto 0),
    coe_clk_export_from_the_ram_diag_bg           => OPEN,
    coe_read_export_from_the_ram_diag_bg          => ram_diag_bg_mosi.rd,
    coe_readdata_export_to_the_ram_diag_bg        => ram_diag_bg_miso.rddata(c_word_w - 1 downto 0),
    coe_reset_export_from_the_ram_diag_bg         => OPEN,
    coe_write_export_from_the_ram_diag_bg         => ram_diag_bg_mosi.wr,
    coe_writedata_export_from_the_ram_diag_bg     => ram_diag_bg_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_tr_nonbonded_mesh
    coe_address_export_from_the_reg_tr_nonbonded_mesh   => reg_mesh_tr_nonbonded_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tr_nonbonded_adr_w - 1 downto 0),
    coe_clk_export_from_the_reg_tr_nonbonded_mesh       => OPEN,
    coe_read_export_from_the_reg_tr_nonbonded_mesh      => reg_mesh_tr_nonbonded_mosi.rd,
    coe_readdata_export_to_the_reg_tr_nonbonded_mesh    => reg_mesh_tr_nonbonded_miso.rddata(c_word_w - 1 downto 0),
    coe_reset_export_from_the_reg_tr_nonbonded_mesh     => OPEN,
    coe_write_export_from_the_reg_tr_nonbonded_mesh     => reg_mesh_tr_nonbonded_mosi.wr,
    coe_writedata_export_from_the_reg_tr_nonbonded_mesh => reg_mesh_tr_nonbonded_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_diagnostics_mesh
    coe_address_export_from_the_reg_diagnostics_mesh   => reg_mesh_diagnostics_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diagnostics_adr_w - 1 downto 0),
    coe_clk_export_from_the_reg_diagnostics_mesh       => OPEN,
    coe_read_export_from_the_reg_diagnostics_mesh      => reg_mesh_diagnostics_mosi.rd,
    coe_readdata_export_to_the_reg_diagnostics_mesh    => reg_mesh_diagnostics_miso.rddata(c_word_w - 1 downto 0),
    coe_reset_export_from_the_reg_diagnostics_mesh     => OPEN,
    coe_write_export_from_the_reg_diagnostics_mesh     => reg_mesh_diagnostics_mosi.wr,
    coe_writedata_export_from_the_reg_diagnostics_mesh => reg_mesh_diagnostics_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_ram_diag_data_buffer
    coe_address_export_from_the_ram_diag_data_buffer   => ram_mesh_diag_data_buf_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_diag_db_adr_w - 1 downto 0),
    coe_clk_export_from_the_ram_diag_data_buffer       => OPEN,
    coe_read_export_from_the_ram_diag_data_buffer      => ram_mesh_diag_data_buf_mosi.rd,
    coe_readdata_export_to_the_ram_diag_data_buffer    => ram_mesh_diag_data_buf_miso.rddata(c_word_w - 1 downto 0),
    coe_reset_export_from_the_ram_diag_data_buffer     => OPEN,
    coe_write_export_from_the_ram_diag_data_buffer     => ram_mesh_diag_data_buf_mosi.wr,
    coe_writedata_export_from_the_ram_diag_data_buffer => ram_mesh_diag_data_buf_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_tr_nonbonded_back
    coe_address_export_from_the_reg_tr_nonbonded_back   => reg_back_tr_nonbonded_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tr_nonbonded_adr_w - 1 downto 0),
    coe_clk_export_from_the_reg_tr_nonbonded_back       => OPEN,
    coe_read_export_from_the_reg_tr_nonbonded_back      => reg_back_tr_nonbonded_mosi.rd,
    coe_readdata_export_to_the_reg_tr_nonbonded_back    => reg_back_tr_nonbonded_miso.rddata(c_word_w - 1 downto 0),
    coe_reset_export_from_the_reg_tr_nonbonded_back     => OPEN,
    coe_write_export_from_the_reg_tr_nonbonded_back     => reg_back_tr_nonbonded_mosi.wr,
    coe_writedata_export_from_the_reg_tr_nonbonded_back => reg_back_tr_nonbonded_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_diagnostics_back
    coe_address_export_from_the_reg_diagnostics_back   => reg_back_diagnostics_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diagnostics_adr_w - 1 downto 0),
    coe_clk_export_from_the_reg_diagnostics_back       => OPEN,
    coe_read_export_from_the_reg_diagnostics_back      => reg_back_diagnostics_mosi.rd,
    coe_readdata_export_to_the_reg_diagnostics_back    => reg_back_diagnostics_miso.rddata(c_word_w - 1 downto 0),
    coe_reset_export_from_the_reg_diagnostics_back     => OPEN,
    coe_write_export_from_the_reg_diagnostics_back     => reg_back_diagnostics_mosi.wr,
    coe_writedata_export_from_the_reg_diagnostics_back => reg_back_diagnostics_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_pio_debug_wave
    out_port_from_the_pio_debug_wave            => pout_debug_wave,

    -- the_pio_pps
    in_port_to_the_pio_pps                      => pin_pps,

    -- the_pio_system_info: actually a avs_common_mm instance
    coe_clk_export_from_the_pio_system_info             => OPEN,
    coe_reset_export_from_the_pio_system_info           => OPEN,
    coe_address_export_from_the_pio_system_info         => reg_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
    coe_read_export_from_the_pio_system_info            => reg_unb_system_info_mosi.rd,
    coe_readdata_export_to_the_pio_system_info          => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_pio_system_info           => reg_unb_system_info_mosi.wr,
    coe_writedata_export_from_the_pio_system_info       => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_rom_system_info
    coe_clk_export_from_the_rom_system_info             => OPEN,
    coe_reset_export_from_the_rom_system_info           => OPEN,
    coe_address_export_from_the_rom_system_info         => rom_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
    coe_read_export_from_the_rom_system_info            => rom_unb_system_info_mosi.rd,
    coe_readdata_export_to_the_rom_system_info          => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_rom_system_info           => rom_unb_system_info_mosi.wr,
    coe_writedata_export_from_the_rom_system_info       => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_pio_wdi
    out_port_from_the_pio_wdi                   => pout_wdi,

    -- the_reg_wdi: Manual WDI override; causes FPGA reconfiguration if WDI is enabled (g_use_phy).
    coe_clk_export_from_the_reg_wdi               => OPEN,
    coe_reset_export_from_the_reg_wdi             => OPEN,
    coe_address_export_from_the_reg_wdi           => reg_wdi_mosi.address(0),
    coe_read_export_from_the_reg_wdi              => reg_wdi_mosi.rd,
    coe_readdata_export_to_the_reg_wdi            => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_wdi             => reg_wdi_mosi.wr,
    coe_writedata_export_from_the_reg_wdi         => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0)
  );

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim           => g_sim,
    g_design_name   => g_design_name,
    g_design_note   => g_design_note,
    g_stamp_date    => g_stamp_date,
    g_stamp_time    => g_stamp_time,
    g_stamp_svn     => g_stamp_svn,
    g_fw_version    => c_fw_version,
    g_mm_clk_freq   => c_unb1_board_mm_clk_freq_50M,
    g_use_phy       => c_use_phy,
    g_aux           => c_aux
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    this_chip_id             => this_chip_id,
    this_bck_id              => this_bck_id,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,  -- 125 MHz from xo_clk PLL in SOPC system
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    sens_sc                  => sens_sc,
    sens_sd                  => sens_sd,
    -- . 1GbE Control Interface
    ETH_clk                  => ETH_clk,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- Design function
  -----------------------------------------------------------------------------

  u_node_bn_terminal_bg : entity work.node_unb1_bn_terminal_bg
  generic map(
    g_sim                     => g_sim,
    -- Application interface
    g_use_bg                  => true,
    g_usr_nof_streams         => c_usr_nof_streams,
    g_usr_block_len           => c_usr_block_len,
    -- Terminals interface
    g_use_mesh                => c_use_mesh,
    g_use_back                => g_rev_multi_unb,
    g_mesh_nof_serial         => c_mesh_nof_serial,
    g_mesh_use_rx             => c_mesh_use_rx,
    g_mesh_gx_mbps            => c_mesh_gx_mbps,
    g_mesh_mon_select         => c_mesh_mon_select,
    g_mesh_mon_nof_words      => c_mesh_mon_nof_words,
    g_mesh_mon_use_sync       => c_mesh_mon_use_sync,
    g_mesh_ena_reorder        => true,
    -- Auxiliary Interface
    g_aux                     => c_aux
  )
  port map(
    -- System
    mm_rst                 => mm_rst,
    mm_clk                 => mm_clk,
    dp_rst                 => dp_rst,
    dp_clk                 => dp_clk,
    dp_pps                 => dp_pps,
    tr_mesh_clk            => SB_CLK,
    tr_back_clk            => SA_CLK,
    cal_clk                => cal_clk,

    chip_id                => this_chip_id,
    bck_id                 => this_bck_id,

    in_sosi_arr            => in_sosi_arr,
    in_siso_arr            => in_siso_arr,

    -- MM interface
    -- . block generator
    reg_diag_bg_mosi       => reg_diag_bg_mosi,
    reg_diag_bg_miso       => reg_diag_bg_miso,
    ram_diag_bg_mosi       => ram_diag_bg_mosi,
    ram_diag_bg_miso       => ram_diag_bg_miso,
    -- . tr_nonbonded mesh
    reg_mesh_tr_nonbonded_mosi  => reg_mesh_tr_nonbonded_mosi,
    reg_mesh_tr_nonbonded_miso  => reg_mesh_tr_nonbonded_miso,
    reg_mesh_diagnostics_mosi   => reg_mesh_diagnostics_mosi,
    reg_mesh_diagnostics_miso   => reg_mesh_diagnostics_miso,

    -- . tr_nonbonded back
    reg_back_tr_nonbonded_mosi  => reg_back_tr_nonbonded_mosi,
    reg_back_tr_nonbonded_miso  => reg_back_tr_nonbonded_miso,
    reg_back_diagnostics_mosi   => reg_back_diagnostics_mosi,
    reg_back_diagnostics_miso   => reg_back_diagnostics_miso,

    -- . diag_data_buffer mesh
    ram_mesh_diag_data_buf_mosi => ram_mesh_diag_data_buf_mosi,
    ram_mesh_diag_data_buf_miso => ram_mesh_diag_data_buf_miso,

    -- Mesh interface
    mesh_tx_serial_2arr         => mesh_tx_serial_2arr,
    mesh_rx_serial_2arr         => mesh_rx_serial_2arr,

    -- Back interface
    back_tx_serial_2arr         => back_tx_serial_2arr,
    back_rx_serial_2arr         => back_rx_serial_2arr
  );

  -----------------------------------------------------------------------------
  -- Wires
  -----------------------------------------------------------------------------

  no_tr_mesh : if c_use_phy.tr_mesh = 0 generate
    mesh_rx_serial_2arr <= (others => (others => '0'));
  end generate;

  gen_tr_mesh : if c_use_phy.tr_mesh /= 0 generate
    u_mesh_io : entity unb1_board_lib.unb1_board_mesh_io
    generic map (
      g_bus_w => c_tr_mesh.bus_w
    )
    port map (
      tx_serial_2arr => mesh_tx_serial_2arr,
      rx_serial_2arr => mesh_rx_serial_2arr,

      -- Serial I/O
      FN_BN_0_TX     => FN_BN_0_TX,
      FN_BN_0_RX     => FN_BN_0_RX,
      FN_BN_1_TX     => FN_BN_1_TX,
      FN_BN_1_RX     => FN_BN_1_RX,
      FN_BN_2_TX     => FN_BN_2_TX,
      FN_BN_2_RX     => FN_BN_2_RX,
      FN_BN_3_TX     => FN_BN_3_TX,
      FN_BN_3_RX     => FN_BN_3_RX
    );
  end generate;

  gen_tr_back : if c_use_phy.tr_back /= 0 generate
    u_back_io : entity unb1_board_lib.unb1_board_back_io
    generic map (
      g_bus_w => c_tr_back.bus_w
    )
    port map (
      tx_serial_2arr => back_tx_serial_2arr,
      rx_serial_2arr => back_rx_serial_2arr,

      -- Serial I/O
      BN_BI_0_TX     => BN_BI_0_TX,
      BN_BI_0_RX     => BN_BI_0_RX,
      BN_BI_1_TX     => BN_BI_1_TX,
      BN_BI_1_RX     => BN_BI_1_RX,
      BN_BI_2_TX     => BN_BI_2_TX,
      BN_BI_2_RX     => BN_BI_2_RX
    );
  end generate;
end;
