--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity mm_rx_logger_trig is
  port (
    clk                : in  std_logic;
    rst                : in  std_logic;
    trig_on            : in  std_logic;
    one_shot           : in  std_logic;  -- Enable this if you want to be absolutely positive that you've logged the first occurrence
    log_en_evt         : in  std_logic;
    log_dis_evt        : in  std_logic;
    log_nof_words      : in  std_logic_vector(c_word_w - 1 downto 0);  -- unlimited (as long as there's no dis_evt) when zero (log FIFO keeps flushing then)
    log_cnt            : out std_logic_vector(c_word_w - 1 downto 0);
    log_en             : out std_logic
   );
end mm_rx_logger_trig;

architecture str of mm_rx_logger_trig is
  signal i_log_cnt     : std_logic_vector(c_word_w - 1 downto 0);
  signal nxt_log_cnt   : std_logic_vector(c_word_w - 1 downto 0);

  type t_state_enum is (s_init, s_idle, s_logging, s_one_shot);

  signal state                  : t_state_enum;
  signal nxt_state              : t_state_enum;
  signal prev_state             : t_state_enum;
begin
  log_cnt <= i_log_cnt;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      state       <= s_init;
      i_log_cnt     <= (others => '0');
    elsif rising_edge(clk) then
      state       <= nxt_state;
      i_log_cnt     <= nxt_log_cnt;
    end if;
  end process;

  p_state : process(state, i_log_cnt, log_dis_evt, log_en_evt, trig_on, one_shot, log_nof_words)
  begin
    nxt_state   <= state;
    nxt_log_cnt <= i_log_cnt;

    case state is

      when s_logging =>
        log_en      <= '1';
        if log_dis_evt = '1' or i_log_cnt = log_nof_words then  -- As we start with a log_cnt of 1, a log_nof_words of 0 will not disable logging.
          log_en    <= '0';
          if one_shot = '1' then
            nxt_state <= s_one_shot;
          else
            nxt_state <= s_idle;
          end if;
        else
          nxt_log_cnt <= INCR_UVEC(i_log_cnt, 1);
        end if;

      when s_one_shot =>  -- Remain in one-shot state until one_shot is set to '0'.
        log_en    <= '0';
        if one_shot = '1' then
          nxt_state <= s_idle;
        end if;

      when s_idle =>
        log_en <= '0';
        if trig_on = '1' then
          if log_en_evt = '1' then
            nxt_log_cnt <= x"00000001";  -- As we're enabling logging here already, we'll have logged 1 word on the next cycle.
            log_en      <= '1';
            nxt_state   <= s_logging;
          end if;
        end if;

      when others =>  -- s_init
        log_en    <= '0';
        nxt_state <= s_idle;
    end case;
  end process;
end str;
