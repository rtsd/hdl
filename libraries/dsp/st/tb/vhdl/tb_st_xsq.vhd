-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:  Testbench for the st_xsq unit.
--
-- Usage in non-auto-mode (c_modelsim_start = 0 in python):
--   > as 5
--   > run -all
-- Description:
-- The tb generates random data to feed into st_xsq. The output is compared to
-- a pre-calculated expected array of xsq values.
-- Remark:
-- . More detail can be found in:
--   https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+Correlator

library IEEE, common_lib, mm_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_math_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.tb_st_pkg.all;

entity tb_st_xsq is
  generic(
    g_nof_crosslets      : natural := 2;
    g_nof_signal_inputs  : natural := 12;
    g_in_data_w          : natural := 16;
    g_nof_sync           : natural := 3;
    g_stat_data_w        : natural := 64;  -- statistics accumulator width
    g_stat_data_sz       : natural := 2;  -- statistics word width >= statistics accumulator width and fit in a power of 2 multiple 32b MM words
    g_nof_block_per_sync : natural := 5;
    g_nof_clk_per_blk    : natural := 1024
  );
end tb_st_xsq;

architecture tb of tb_st_xsq is
  constant c_sim            : boolean := true;
  constant c_rl             : natural := 1;
  constant c_block_size     : natural := g_nof_crosslets * g_nof_signal_inputs;
  constant c_random_data_w  : natural := 8;
  constant c_xsq            : natural := g_nof_signal_inputs * g_nof_signal_inputs;
  constant c_nof_statistics : natural := g_nof_crosslets * c_xsq;
  constant c_gap_size       : natural := g_nof_clk_per_blk - c_nof_statistics;

  constant c_random_in_a_re : t_integer_arr(0 to c_block_size-1) := common_math_create_random_arr(c_block_size, c_random_data_w, 100);
  constant c_random_in_a_im : t_integer_arr(0 to c_block_size-1) := common_math_create_random_arr(c_block_size, c_random_data_w, 101);
  constant c_random_in_b_re : t_integer_arr(0 to c_block_size-1) := common_math_create_random_arr(c_block_size, c_random_data_w, 102);
  constant c_random_in_b_im : t_integer_arr(0 to c_block_size-1) := common_math_create_random_arr(c_block_size, c_random_data_w, 103);

  constant c_expected_xsq   : t_integer_arr(0 to c_nof_statistics * c_nof_complex - 1) := func_st_calculate_expected_xsq(c_random_in_a_re, c_random_in_a_im, c_random_in_b_re, c_random_in_b_im, g_nof_crosslets, g_nof_block_per_sync);
  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period  : time := 100 ps;
  constant c_dp_clk_period  : time := 5 ns;
  constant c_dp_pps_period  : natural := 64;

  signal tb_end             : std_logic;
  signal dp_pps             : std_logic;

  signal mm_rst             : std_logic := '1';
  signal mm_clk             : std_logic := '1';

  signal dp_rst             : std_logic;
  signal dp_clk             : std_logic := '1';

  signal st_en              : std_logic := '1';
  signal st_siso            : t_dp_siso := c_dp_siso_rdy;
  signal st_sosi            : t_dp_sosi := c_dp_sosi_rst;
  signal in_sosi_a          : t_dp_sosi := c_dp_sosi_rst;
  signal in_sosi_b          : t_dp_sosi := c_dp_sosi_rst;

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  signal ram_st_xsq_mosi    : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_xsq_miso    : t_mem_miso := c_mem_miso_rst;
  signal st_xsq_out_arr     : t_slv_32_arr(0 to c_nof_statistics * c_nof_complex * g_stat_data_sz - 1) := (others => (others => '0'));
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- MM Stimuli
  ------------------------------------------------------------------------------
  p_mm_stimuli : process
  begin
    for I in 0 to g_nof_sync - 1 loop
      proc_common_wait_until_lo_hi(dp_clk, st_sosi.sync);
    end loop;
    proc_common_wait_some_cycles(dp_clk, g_nof_clk_per_blk);
    proc_common_wait_some_cycles(dp_clk, 20);
    -- read ram
    proc_mem_read_ram(0, c_nof_statistics * c_nof_complex * g_stat_data_sz, mm_clk, ram_st_xsq_mosi, ram_st_xsq_miso, st_xsq_out_arr);
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Data blocks
  ------------------------------------------------------------------------------
  p_st_stimuli : process
    variable v_re  : natural := 0;
    variable v_im  : natural := 0;
  begin
    tb_end <= '0';
    st_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(dp_clk, dp_rst);

    -- Run some sync intervals with DSP counter data for the real and imag fields
    wait until rising_edge(dp_clk);
    proc_common_wait_some_cycles(dp_clk, 7);
    for I in 0 to g_nof_sync - 1 loop
      proc_dp_gen_block_data(c_rl, false, g_in_data_w, g_in_data_w, 0, v_re, v_im, c_nof_statistics, 0, 0, '1', "0", dp_clk, st_en, st_siso, st_sosi);  -- next sync
      st_sosi <= c_dp_sosi_rst;
      proc_common_wait_some_cycles(dp_clk, c_gap_size);
      for J in 0 to g_nof_block_per_sync - 2 loop  -- provide sop and eop for block reference
        proc_dp_gen_block_data(c_rl, false, g_in_data_w, g_in_data_w, 0, v_re, v_im, c_nof_statistics, 0, 0, '0', "0", dp_clk, st_en, st_siso, st_sosi);  -- no sync
        st_sosi <= c_dp_sosi_rst;
        proc_common_wait_some_cycles(dp_clk, c_gap_size);
      end loop;
    end loop;
    st_sosi <= c_dp_sosi_rst;
    proc_common_wait_some_cycles(dp_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  in_sosi_a.sync   <= st_sosi.sync;
  in_sosi_b.sync   <= st_sosi.sync;
  in_sosi_a.sop    <= st_sosi.sop;
  in_sosi_b.sop    <= st_sosi.sop;
  in_sosi_a.eop    <= st_sosi.eop;
  in_sosi_b.eop    <= st_sosi.eop;
  in_sosi_a.valid  <= st_sosi.valid;
  in_sosi_b.valid  <= st_sosi.valid;

  p_random_stimuli : process
  begin
    for S in 0 to g_nof_sync * g_nof_block_per_sync - 1 loop
      wait until rising_edge(st_sosi.sop);
      for N in 0 to g_nof_crosslets - 1 loop
        for I in 0 to g_nof_signal_inputs - 1 loop
          for J in 0 to g_nof_signal_inputs - 1 loop
            in_sosi_a.re <= TO_DP_DSP_DATA(c_random_in_a_re(N * g_nof_signal_inputs + I));
            in_sosi_a.im <= TO_DP_DSP_DATA(c_random_in_a_im(N * g_nof_signal_inputs + I));
            in_sosi_b.re <= TO_DP_DSP_DATA(c_random_in_b_re(N * g_nof_signal_inputs + J));
            in_sosi_b.im <= TO_DP_DSP_DATA(c_random_in_b_im(N * g_nof_signal_inputs + J));
            proc_common_wait_some_cycles(dp_clk, 1);
          end loop;
        end loop;
      end loop;
    end loop;
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------
  p_verify : process
  begin
    proc_common_wait_until_high(mm_clk, ram_st_xsq_miso.rdval);
    proc_common_wait_some_cycles(mm_clk, 2 * c_nof_statistics * c_nof_complex * g_stat_data_sz + 10);
    for I in 0 to c_nof_statistics * c_nof_complex - 1 loop
      assert TO_SINT(st_xsq_out_arr(I * 2)) = c_expected_xsq(I)
        report "WRONG XSQ DATA"
        severity ERROR;  -- Only read low part of statistic
    end loop;
    wait;
  end process;

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut : entity work.st_xsq
  generic map(
    g_nof_signal_inputs  => g_nof_signal_inputs,
    g_nof_crosslets      => g_nof_crosslets,
    g_in_data_w          => g_in_data_w,
    g_stat_data_w        => g_stat_data_w,
    g_stat_data_sz       => g_stat_data_sz
  )
  port map(
    mm_rst           =>  mm_rst,
    mm_clk           =>  mm_clk,
    dp_rst           =>  dp_rst,
    dp_clk           =>  dp_clk,

    -- Streaming
    in_a             => in_sosi_a,
    in_b             => in_sosi_b,

    -- Memory Mapped
    ram_st_xsq_mosi  => ram_st_xsq_mosi,
    ram_st_xsq_miso  => ram_st_xsq_miso
  );
end tb;
