-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_fifo_dc is
end tb_tb_dp_fifo_dc;

architecture tb of tb_tb_dp_fifo_dc is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 10
  -- > run g_dut_rd_clk_freq * 330 us                 --> OK

  -- Try FIFO settings : GENERIC MAP (g_dut_wr_clk_freq, g_dut_rd_clk_freq, g_dut_use_bsn, g_dut_use_empty, g_dut_use_channel, g_dut_use_sync, g_dut_use_ctrl, g_dut_out_latency)

  u_use_all_rl_0          : entity work.tb_dp_fifo_dc generic map (1, 1, true,  true,  true,  true,   true,  0);
  u_use_all_rl_0_clk_2_1  : entity work.tb_dp_fifo_dc generic map (2, 1, true,  true,  true,  true,   true,  0);
  u_use_all_rl_0_clk_1_2  : entity work.tb_dp_fifo_dc generic map (1, 2, true,  true,  true,  true,   true,  0);
  u_use_all_rl_0_clk_3_2  : entity work.tb_dp_fifo_dc generic map (3, 2, true,  true,  true,  true,   true,  0);
  u_use_all_rl_0_clk_2_3  : entity work.tb_dp_fifo_dc generic map (2, 3, true,  true,  true,  true,   true,  0);

  u_use_all               : entity work.tb_dp_fifo_dc generic map (1, 1, true,  true,  true,  true,   true,  1);
  u_use_all_clk_3_1       : entity work.tb_dp_fifo_dc generic map (3, 1, true,  true,  true,  true,   true,  1);
  u_use_all_clk_1_3       : entity work.tb_dp_fifo_dc generic map (1, 3, true,  true,  true,  true,   true,  1);

  u_use_ctrl_rl_0         : entity work.tb_dp_fifo_dc generic map (1, 1, false, false, false, false,  true,  0);
  u_use_ctrl_rl_0_clk_1_3 : entity work.tb_dp_fifo_dc generic map (1, 3, false, false, false, false,  true,  0);
  u_use_ctrl_rl_0_clk_3_1 : entity work.tb_dp_fifo_dc generic map (3, 1, false, false, false, false,  true,  0);
  u_use_ctrl              : entity work.tb_dp_fifo_dc generic map (1, 1, false, false, false, false,  true,  1);
  u_use_ctrl_clk_1_2      : entity work.tb_dp_fifo_dc generic map (1, 2, false, false, false, false,  true,  1);
  u_use_ctrl_clk_2_1      : entity work.tb_dp_fifo_dc generic map (2, 1, false, false, false, false,  true,  1);

  u_no_bsn                : entity work.tb_dp_fifo_dc generic map (1, 1, false, true,  true,  true,   true,  1);
  u_no_empty              : entity work.tb_dp_fifo_dc generic map (1, 1, true,  false, true,  true,   true,  1);
  u_no_channel            : entity work.tb_dp_fifo_dc generic map (1, 1, true,  true,  false, true,   true,  1);
  u_no_sync               : entity work.tb_dp_fifo_dc generic map (1, 1, true,  true,  true,  false,  true,  1);
  u_no_ctrl               : entity work.tb_dp_fifo_dc generic map (1, 1, true,  true,  true,  true,   false, 1);
end tb;
