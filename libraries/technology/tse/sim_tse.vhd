--------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Drop-in behavioral simulation model for tech_tse.vhd.
-- Description:
-- . The simulation model is based on tech_transceiver_lib.sim_transceiver_gx
--   and is about a factor 4 faster than the IP simulation model.
-- Remark:
-- . Default use g_tx_crc=TRUE, to model TSE IP in ETH on UniBoard1, UniBoard2
-- . Connect eth_txp/eth_rxp directly to host rxp/txp without a TRANSPORT delay,
--   because the sim_transceiver_gx model requries that both sides of a link
--   are in phase.

library IEEE, common_lib, dp_lib, tech_transceiver_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.tech_tse_pkg.all;

entity sim_tse is
  generic(
    g_tx         : boolean;
    g_tx_crc     : boolean := true;  -- model append CRC by TSE MAC, CRC value = 0
    g_rx         : boolean
  );
  port(
    -- Clocks and reset
    mm_rst         : in  std_logic;  -- unused
    mm_clk         : in  std_logic;  -- unused
    eth_clk        : in  std_logic;  -- 125 MHz
    tx_snk_clk     : in  std_logic;  -- DP
    rx_src_clk     : in  std_logic;  -- DP

    -- Memory Mapped Slave
    mm_sla_in      : in  t_mem_mosi;
    mm_sla_out     : out t_mem_miso;

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      : in  t_dp_sosi;
    tx_snk_out     : out t_dp_siso;
    -- . MAC specific
    tx_mac_in      : in  t_tech_tse_tx_mac;
    tx_mac_out     : out t_tech_tse_tx_mac;

    -- MAC receive interface
    -- . ST Source
    rx_src_in      : in  t_dp_siso;
    rx_src_out     : out t_dp_sosi;
    -- . MAC specific
    rx_mac_out     : out t_tech_tse_rx_mac;

    -- PHY interface
    eth_txp        : out std_logic;
    eth_rxp        : in  std_logic;

    tse_led        : out t_tech_tse_led
  );
end sim_tse;

architecture str of sim_tse is
  constant c_crc_sz : natural := 4;  -- CRC word has 4 octets

  signal tr_clk     : std_logic;
  signal tr_rst     : std_logic;

  signal tx_snk_rst : std_logic;
  signal rx_src_rst : std_logic;

  signal tx_fifo_sosi  : t_dp_sosi := c_dp_sosi_rst;
  signal tx_fifo_siso  : t_dp_siso := c_dp_siso_hold;

  type t_reg is record
    crc_sosi : t_dp_sosi;
    crc_cnt  : natural range 0 to c_crc_sz;
  end record;

  signal crc_siso      : t_dp_siso := c_dp_siso_hold;
  signal r             : t_reg;
  signal nxt_r         : t_reg;

  signal tx_pkt_sosi   : t_dp_sosi := c_dp_sosi_rst;
  signal tx_pkt_siso   : t_dp_siso := c_dp_siso_hold;

  signal gx_tx_snk_in_arr  : t_dp_sosi_arr(0 downto 0);
  signal gx_tx_snk_out_arr : t_dp_siso_arr(0 downto 0);

  signal gx_rx_src_out_arr : t_dp_sosi_arr(0 downto 0);
  signal gx_rx_src_in_arr  : t_dp_siso_arr(0 downto 0);
begin
  -------------------------------------------------------------------------------
  -- TX FIFO
  -- . tx_snk_clk (dp_clk) -> tr_clk
  -- . User data width (32b) -> transceiver PCS data width (8b)
  -------------------------------------------------------------------------------
  u_common_areset_tx : entity common_lib.common_areset
  generic map(
    g_rst_level => '1'
  )
  port map(
    clk     => tx_snk_clk,
    in_rst  => '0',
    out_rst => tx_snk_rst
  );

  u_dp_fifo_dc_mixed_widths_tx : entity dp_lib.dp_fifo_dc_mixed_widths
  generic map (
    g_wr_data_w => c_tech_tse_data_w,
    g_rd_data_w => c_byte_w,
    g_use_ctrl  => true,  -- SOP, EOP support
    g_wr_fifo_size => 50,
    g_rd_fifo_rl   => 1
  )
  port map (
    wr_rst      => tx_snk_rst,
    wr_clk      => tx_snk_clk,
    rd_rst      => tr_rst,
    rd_clk      => tr_clk,

    snk_in      => tx_snk_in,
    snk_out     => tx_snk_out,

    src_out     => tx_fifo_sosi,
    src_in      => tx_fifo_siso
  );

  no_tx_crc : if not g_tx_crc generate
    gx_tx_snk_in_arr(0) <= tx_fifo_sosi;
    tx_fifo_siso        <= gx_tx_snk_out_arr(0);
  end generate;

  gen_tx_crc : if g_tx_crc generate
    -----------------------------------------------------------------------------
    -- Model Tx CRC by appending four zero octets at end of Tx packet
    -----------------------------------------------------------------------------
    --
    -- The p_crc_comb implementation is based on the following timing diagram:
    --                        _   _   _   _   _   _   _   _   _
    --   tr_clk             _| |_| |_| |_| |_| |_| |_| |_| |_|
    --                      ___________________________________
    --   tx_fifo_siso.ready
    --                      _________
    --   tx_fifo_sosi.valid          |_________________________
    --                            ___
    --   tx_fifo_sosi.eop   _____|   |_________________________
    --                      _____                 _____________
    --   crc_siso.ready          |_______________|
    --                                _______________
    --   crc_sosi.valid     _________|               |_________
    --                                            ___
    --   crc_sosi.eop       _____________________|   |_________
    --
    --   crc_cnt             | 0 | 0 | 1 | 2 | 3 | 0 | 0 | 0 |
    --                      _________________________
    --   tx_pkt_sosi.valid                           |_________
    --                                            ___
    --   tx_pkt_sosi.eop    _____________________|   |_________
    --

    tx_fifo_siso.ready <= tx_pkt_siso.ready and crc_siso.ready;

    p_tx_pkt_sosi : process(tx_fifo_sosi, r)
    begin
      -- start with tx_fifo_sosi packet
      tx_pkt_sosi <= tx_fifo_sosi;
      -- append CRC = 0 at end of tx_fifo_sosi packet
      if r.crc_sosi.valid = '1' then
        tx_pkt_sosi.data <= TO_DP_DATA(0);
      end if;
      tx_pkt_sosi.valid <= tx_fifo_sosi.valid or r.crc_sosi.valid;
      tx_pkt_sosi.eop   <= r.crc_sosi.eop;
    end process;

    p_crc_comb : process(tx_fifo_sosi, r)
      variable v : t_reg;
    begin
      crc_siso.ready <= '1';
      v := r;
      v.crc_sosi.valid := '0';
      v.crc_sosi.eop := '0';
      if tx_fifo_sosi.eop = '1' then
        crc_siso.ready <= '0';
        v.crc_sosi.valid := '1';
        v.crc_cnt := 1;
      end if;
      if r.crc_cnt > 0 then
        crc_siso.ready <= '0';
        v.crc_sosi.valid := '1';
        v.crc_cnt := r.crc_cnt + 1;
      end if;
      if r.crc_cnt = c_crc_sz - 1 then
        v.crc_sosi.eop := '1';
        v.crc_cnt := 0;
      end if;
      nxt_r <= v;
    end process;

    p_crc_reg : process(tr_rst, tr_clk)
    begin
      if tr_rst = '1' then
        r <= (c_dp_sosi_rst, 0);
      elsif rising_edge(tr_clk) then
        r <= nxt_r;
      end if;
    end process;

    gx_tx_snk_in_arr(0) <= tx_pkt_sosi;
    tx_pkt_siso         <= gx_tx_snk_out_arr(0);
  end generate;

  -------------------------------------------------------------------------------
  -- Transceiver sim model
  -- . Inside this model, tr_clk = tx_clk = rx_clk. We're using its output
  --   tx_clk,tx_rst as local tr_clk,tr_rst to prevent delta delay issues.
  -------------------------------------------------------------------------------
  u_sim_transceiver_gx : entity tech_transceiver_lib.sim_transceiver_gx
  generic map(
    g_data_w => c_byte_w,
    g_nof_gx => 1,
    g_mbps   => 1250,
    g_rx     => g_rx,
    g_tx     => g_tx
  )
  port map (
    tb_end          => '0',

    tr_clk          => eth_clk,

    tx_clk(0)       => tr_clk,
    tx_rst(0)       => tr_rst,

    tx_sosi_arr     => gx_tx_snk_in_arr,
    tx_siso_arr     => gx_tx_snk_out_arr,
    tx_dataout(0)   => eth_txp,

    rx_datain(0)    => eth_rxp,
    rx_sosi_arr     => gx_rx_src_out_arr,
    rx_siso_arr     => gx_rx_src_in_arr
  );

  -------------------------------------------------------------------------------
  -- RX FIFO
  -- . tr_clk => rx_src_clk (dp_clk)
  -- . transceiver PCS data width (8b) -> User data width (32b)
  -------------------------------------------------------------------------------
  u_common_areset_rx : entity common_lib.common_areset
  generic map(
    g_rst_level => '1'
  )
  port map(
    clk     => rx_src_clk,
    in_rst  => '0',
    out_rst => rx_src_rst
  );

  u_dp_fifo_dc_mixed_widths_rx : entity dp_lib.dp_fifo_dc_mixed_widths
  generic map (
    g_wr_data_w => c_byte_w,
    g_rd_data_w => c_tech_tse_data_w,
    g_use_ctrl  => true,  -- SOP, EOP support
    g_wr_fifo_size => 50,
    g_rd_fifo_rl   => 1
  )
  port map (
    wr_rst      => tr_rst,
    wr_clk      => tr_clk,
    rd_rst      => rx_src_rst,
    rd_clk      => rx_src_clk,

    snk_in      => gx_rx_src_out_arr(0),
    snk_out     => gx_rx_src_in_arr(0),

    src_out     => rx_src_out,
    src_in      => rx_src_in
  );
end str;
