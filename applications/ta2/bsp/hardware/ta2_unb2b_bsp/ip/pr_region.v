// (C) 1992-2018 Intel Corporation.                            
// Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
// and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
// and/or other countries. Other marks and brands may be claimed as the property  
// of others. See Trademarks on intel.com for full list of Intel trademarks or    
// the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
// Your use of Intel Corporation's design tools, logic functions and other        
// software and tools, and its AMPP partner logic functions, and any output       
// files any of the foregoing (including device programming or simulation         
// files), and any associated documentation or information are expressly subject  
// to the terms and conditions of the Altera Program License Subscription         
// Agreement, Intel MegaCore Function License Agreement, or other applicable      
// license agreement, including, without limitation, that your use is for the     
// sole purpose of programming logic devices manufactured by Intel and sold by    
// Intel or its authorized distributors.  Please refer to the applicable          
// agreement for further details.                                                 

module pr_region (
//  input  wire [30:0]  cc_snoop_data,
//  input  wire         cc_snoop_valid,
//  output wire         cc_snoop_ready,
//  input  wire         cc_snoop_clk_clk,
  input  wire         clock_reset_clk,
  input  wire         clock_reset2x_clk,
  input  wire         clock_reset_reset_reset_n,
  output wire         kernel_cra_waitrequest,
  output wire [63:0]  kernel_cra_readdata,
  output wire         kernel_cra_readdatavalid,
  input  wire [0:0]   kernel_cra_burstcount,
  input  wire [63:0]  kernel_cra_writedata,
  input  wire [29:0]  kernel_cra_address,
  input  wire         kernel_cra_write,
  input  wire         kernel_cra_read,
  input  wire [7:0]   kernel_cra_byteenable,
  input  wire         kernel_cra_debugaccess,
  output wire         kernel_irq_irq,

  input  wire         kernel_mem0_waitrequest,
  input  wire [511:0] kernel_mem0_readdata,
  input  wire         kernel_mem0_readdatavalid,
  output wire [4:0]   kernel_mem0_burstcount,
  output wire [511:0] kernel_mem0_writedata,
  output wire [32:0]  kernel_mem0_address,
  output wire         kernel_mem0_write,
  output wire         kernel_mem0_read,
  output wire [63:0]  kernel_mem0_byteenable,

  output  wire [6:0]   kernel_register_mem_address,                
  output  wire         kernel_register_mem_clken,      
  output  wire         kernel_register_mem_chipselect, 
  output  wire         kernel_register_mem_write,      
  input   wire [255:0]  kernel_register_mem_readdata,   
  output  wire [255:0]  kernel_register_mem_writedata,  
  output  wire [31:0]   kernel_register_mem_byteenable, 

  input  wire [15:0]  kernel_stream_src_ADC_data,
  input  wire         kernel_stream_src_ADC_valid,
  output wire         kernel_stream_src_ADC_ready,

  input  wire [71:0]  kernel_stream_src_mm_io_data,
  input  wire         kernel_stream_src_mm_io_valid,
  output wire         kernel_stream_src_mm_io_ready,
  output wire [31:0]  kernel_stream_snk_mm_io_data,
  output wire         kernel_stream_snk_mm_io_valid,
  input  wire         kernel_stream_snk_mm_io_ready,

  input  wire [39:0]  kernel_stream_src_1GbE_data,
  input  wire         kernel_stream_src_1GbE_valid,
  output wire         kernel_stream_src_1GbE_ready,
  output wire [39:0]  kernel_stream_snk_1GbE_data,
  output wire         kernel_stream_snk_1GbE_valid,
  input  wire         kernel_stream_snk_1GbE_ready,

  input  wire [71:0]  kernel_stream_src_10GbE_data,
  input  wire         kernel_stream_src_10GbE_valid,
  output wire         kernel_stream_src_10GbE_ready,
  output wire [71:0]  kernel_stream_snk_10GbE_data,
  output wire         kernel_stream_snk_10GbE_valid,
  input  wire         kernel_stream_snk_10GbE_ready,

  input  wire [263:0] kernel_stream_src_40GbE_data,
  input  wire         kernel_stream_src_40GbE_valid,
  output wire         kernel_stream_src_40GbE_ready,
  output wire [263:0] kernel_stream_snk_40GbE_data,
  output wire         kernel_stream_snk_40GbE_valid,
  input  wire         kernel_stream_snk_40GbE_ready,

  input  wire [263:0] kernel_stream_src_40GbE_ring_0_data,
  input  wire         kernel_stream_src_40GbE_ring_0_valid,
  output wire         kernel_stream_src_40GbE_ring_0_ready,
  output wire [263:0] kernel_stream_snk_40GbE_ring_0_data,
  output wire         kernel_stream_snk_40GbE_ring_0_valid,
  input  wire         kernel_stream_snk_40GbE_ring_0_ready,

  input  wire [263:0] kernel_stream_src_40GbE_ring_1_data,
  input  wire         kernel_stream_src_40GbE_ring_1_valid,
  output wire         kernel_stream_src_40GbE_ring_1_ready,
  output wire [263:0] kernel_stream_snk_40GbE_ring_1_data,
  output wire         kernel_stream_snk_40GbE_ring_1_valid,
  input  wire         kernel_stream_snk_40GbE_ring_1_ready


//  input  wire         kernel_mem0_waitrequest,
//  input  wire [511:0] kernel_mem0_readdata,
//  input  wire         kernel_mem0_readdatavalid,
//  output wire [4:0]   kernel_mem0_burstcount,
//  output wire [511:0] kernel_mem0_writedata,
//  output wire [30:0]  kernel_mem0_address,
//  output wire         kernel_mem0_write,
//  output wire         kernel_mem0_read,
//  output wire [63:0]  kernel_mem0_byteenable
);

  wire          pipelined_kernel_mem0_s0_waitrequest;
  wire  [511:0] pipelined_kernel_mem0_s0_readdata;
  wire          pipelined_kernel_mem0_s0_readdatavalid;
  wire    [4:0] pipelined_kernel_mem0_s0_burstcount;
  wire  [511:0] pipelined_kernel_mem0_s0_writedata;
  wire   [32:0] pipelined_kernel_mem0_s0_address;
  wire          pipelined_kernel_mem0_s0_write;
  wire          pipelined_kernel_mem0_s0_read;
  wire   [63:0] pipelined_kernel_mem0_s0_byteenable;


  wire [11:0] kernel_system_register_mem_address;
  wire        kernel_system_register_mem_write;
  wire        kernel_system_register_mem_read;
  reg         kernel_system_register_mem_readdatavalid;
  
  assign kernel_register_mem_address = kernel_system_register_mem_address[11:5];
  assign kernel_register_mem_chipselect = 1'b1;
  assign kernel_register_mem_clken = kernel_system_register_mem_write|kernel_system_register_mem_read;
  assign kernel_register_mem_write = kernel_system_register_mem_write;
  
  always @( posedge clock_reset_clk or negedge clock_reset_reset_reset_n )
  begin
    if (clock_reset_reset_reset_n  == 1'b0 ) begin
      kernel_system_register_mem_readdatavalid <= 1'b0;
    end else begin
       kernel_system_register_mem_readdatavalid <= kernel_system_register_mem_read;
    end
  end

//=======================================================
//  kernel_mem pipeline stage instantiation
//=======================================================
kernel_mem_mm_bridge_0 kernel_mem_inst(
  .clk(clock_reset_clk), 
  .m0_waitrequest(kernel_mem0_waitrequest),
  .m0_readdata(kernel_mem0_readdata),
  .m0_readdatavalid(kernel_mem0_readdatavalid),
  .m0_burstcount(kernel_mem0_burstcount),
  .m0_writedata(kernel_mem0_writedata),
  .m0_address(kernel_mem0_address),
  .m0_write(kernel_mem0_write),
  .m0_read(kernel_mem0_read),
  .m0_byteenable(kernel_mem0_byteenable),
  .reset(~clock_reset_reset_reset_n),
  .s0_waitrequest(pipelined_kernel_mem0_s0_waitrequest),
  .s0_readdata(pipelined_kernel_mem0_s0_readdata),
  .s0_readdatavalid(pipelined_kernel_mem0_s0_readdatavalid),
  .s0_burstcount(pipelined_kernel_mem0_s0_burstcount),
  .s0_writedata(pipelined_kernel_mem0_s0_writedata),
  .s0_address(pipelined_kernel_mem0_s0_address),
  .s0_write(pipelined_kernel_mem0_s0_write),
  .s0_read(pipelined_kernel_mem0_s0_read),
  .s0_byteenable(pipelined_kernel_mem0_s0_byteenable)
);

//=======================================================
//  kernel_system instantiation
//=======================================================
kernel_system kernel_system_inst
(
  // kernel_system ports
  .clock_reset_clk(clock_reset_clk),
  .clock_reset2x_clk(clock_reset2x_clk),
  .clock_reset_reset_reset_n(clock_reset_reset_reset_n),
  .kernel_irq_irq(kernel_irq_irq),
  .cc_snoop_clk_clk(clock_reset_clk),
//  .cc_snoop_data(cc_snoop_data),
//  .cc_snoop_valid(cc_snoop_valid),
//  .cc_snoop_ready(cc_snoop_ready),
  .kernel_cra_waitrequest(kernel_cra_waitrequest),
  .kernel_cra_readdata(kernel_cra_readdata),
  .kernel_cra_readdatavalid(kernel_cra_readdatavalid),
  .kernel_cra_burstcount(kernel_cra_burstcount),
  .kernel_cra_writedata(kernel_cra_writedata),
  .kernel_cra_address(kernel_cra_address),
  .kernel_cra_write(kernel_cra_write),
  .kernel_cra_read(kernel_cra_read),
  .kernel_cra_byteenable(kernel_cra_byteenable),
  .kernel_cra_debugaccess(kernel_cra_debugaccess),

  .kernel_register_mem_read(kernel_system_register_mem_read),
  .kernel_register_mem_write(kernel_system_register_mem_write),
  .kernel_register_mem_address(kernel_system_register_mem_address),
  .kernel_register_mem_writedata(kernel_register_mem_writedata),
  .kernel_register_mem_byteenable(kernel_register_mem_byteenable),
  .kernel_register_mem_readdata(kernel_register_mem_readdata),
  .kernel_register_mem_readdatavalid(kernel_system_register_mem_readdatavalid),


  .kernel_input_40GbE_data(kernel_stream_src_40GbE_data),
  .kernel_input_40GbE_ready(kernel_stream_src_40GbE_ready),
  .kernel_input_40GbE_valid(kernel_stream_src_40GbE_valid),

  .kernel_output_40GbE_data(kernel_stream_snk_40GbE_data),
  .kernel_output_40GbE_ready(kernel_stream_snk_40GbE_ready),
  .kernel_output_40GbE_valid(kernel_stream_snk_40GbE_valid),


  .kernel_input_40GbE_ring_0_data(kernel_stream_src_40GbE_ring_0_data),
  .kernel_input_40GbE_ring_0_ready(kernel_stream_src_40GbE_ring_0_ready),
  .kernel_input_40GbE_ring_0_valid(kernel_stream_src_40GbE_ring_0_valid),

  .kernel_output_40GbE_ring_0_data(kernel_stream_snk_40GbE_ring_0_data),
  .kernel_output_40GbE_ring_0_ready(kernel_stream_snk_40GbE_ring_0_ready),
  .kernel_output_40GbE_ring_0_valid(kernel_stream_snk_40GbE_ring_0_valid),


  .kernel_input_40GbE_ring_1_data(kernel_stream_src_40GbE_ring_1_data),
  .kernel_input_40GbE_ring_1_ready(kernel_stream_src_40GbE_ring_1_ready),
  .kernel_input_40GbE_ring_1_valid(kernel_stream_src_40GbE_ring_1_valid),

  .kernel_output_40GbE_ring_1_data(kernel_stream_snk_40GbE_ring_1_data),
  .kernel_output_40GbE_ring_1_ready(kernel_stream_snk_40GbE_ring_1_ready),
  .kernel_output_40GbE_ring_1_valid(kernel_stream_snk_40GbE_ring_1_valid),


  .kernel_input_10GbE_data(kernel_stream_src_10GbE_data),
  .kernel_input_10GbE_ready(kernel_stream_src_10GbE_ready),
  .kernel_input_10GbE_valid(kernel_stream_src_10GbE_valid),

  .kernel_output_10GbE_data(kernel_stream_snk_10GbE_data),
  .kernel_output_10GbE_ready(kernel_stream_snk_10GbE_ready),
  .kernel_output_10GbE_valid(kernel_stream_snk_10GbE_valid),


  .kernel_input_1GbE_data(kernel_stream_src_1GbE_data),
  .kernel_input_1GbE_ready(kernel_stream_src_1GbE_ready),
  .kernel_input_1GbE_valid(kernel_stream_src_1GbE_valid),

  .kernel_output_1GbE_data(kernel_stream_snk_1GbE_data),
  .kernel_output_1GbE_ready(kernel_stream_snk_1GbE_ready),
  .kernel_output_1GbE_valid(kernel_stream_snk_1GbE_valid),

  .kernel_input_ADC_data(kernel_stream_src_ADC_data),
  .kernel_input_ADC_ready(kernel_stream_src_ADC_ready),
  .kernel_input_ADC_valid(kernel_stream_src_ADC_valid),


  .kernel_input_mm_data(kernel_stream_src_mm_io_data),
  .kernel_input_mm_ready(kernel_stream_src_mm_io_ready),
  .kernel_input_mm_valid(kernel_stream_src_mm_io_valid),

  .kernel_output_mm_data(kernel_stream_snk_mm_io_data),
  .kernel_output_mm_ready(kernel_stream_snk_mm_io_ready),
  .kernel_output_mm_valid(kernel_stream_snk_mm_io_valid),

  .kernel_mem0_address(pipelined_kernel_mem0_s0_address),
  .kernel_mem0_read(pipelined_kernel_mem0_s0_read),
  .kernel_mem0_write(pipelined_kernel_mem0_s0_write),
  .kernel_mem0_burstcount(pipelined_kernel_mem0_s0_burstcount),
  .kernel_mem0_writedata(pipelined_kernel_mem0_s0_writedata),
  .kernel_mem0_byteenable(pipelined_kernel_mem0_s0_byteenable),
  .kernel_mem0_readdata(pipelined_kernel_mem0_s0_readdata),
  .kernel_mem0_waitrequest(pipelined_kernel_mem0_s0_waitrequest),
  .kernel_mem0_readdatavalid(pipelined_kernel_mem0_s0_readdatavalid)
);

endmodule
