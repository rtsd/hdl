-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Usage:
-- > as 10
-- > run -all
-- . Observe in_data_arr_p and the expected result and the result of the DUT in the Wave window
-- . This TB verifies the DUT architecture that was compile last. Default after a fresh mk the (str)
--   is compiled last, to simulate the (recursive) manually compile it and the simulate again.
--   Within the recursive architecture it is not possible to explicitely configure it to recursively
--   use the recursive architecture using FOR ALL : ENTITY because the instance label is within a
--   generate block.
-- . The p_verify makes the tb self checking and asserts when the results are not equal

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_adder_tree is
  generic (
    g_representation : string  := "SIGNED";
    g_pipeline       : natural := 1;  -- amount of pipelining per stage
    g_nof_inputs     : natural := 31;  -- >= 1
    g_symbol_w       : natural := 8;
    g_sum_w          : natural := 8  -- worst case bit growth requires g_symbol_w + ceil_log2(g_nof_inputs);
  );
end tb_common_adder_tree;

architecture tb of tb_common_adder_tree is
  constant clk_period      : time := 10 ns;

  constant c_data_vec_w    : natural := g_nof_inputs * g_symbol_w;
  constant c_nof_stages    : natural := ceil_log2(g_nof_inputs);

  constant c_pipeline_tree : natural := g_pipeline * c_nof_stages;

  type t_symbol_arr is array (integer range <>) of std_logic_vector(g_symbol_w - 1 downto 0);

  -- Use the same symbol value g_nof_inputs time in the data_vec
  function func_data_vec(symbol : integer) return std_logic_vector is
    variable v_data_vec : std_logic_vector(c_data_vec_w - 1 downto 0);
  begin
    for I in 0 to g_nof_inputs - 1 loop
      v_data_vec((I + 1) * g_symbol_w - 1 downto I * g_symbol_w) := TO_UVEC(symbol, g_symbol_w);
    end loop;
    return v_data_vec;
  end;

  -- Calculate the expected result of the sum of the symbols in the data_vec
  function func_result(data_vec : std_logic_vector) return std_logic_vector is
    variable v_result : integer;
  begin
    v_result := 0;
    if g_representation = "SIGNED" then
      for I in 0 to g_nof_inputs - 1 loop
        v_result := v_result + TO_SINT(data_vec((I + 1) * g_symbol_w - 1 downto I * g_symbol_w));
      end loop;
      v_result := RESIZE_SINT(v_result, g_sum_w);
      return TO_SVEC(v_result, g_sum_w);
    else
      for I in 0 to g_nof_inputs - 1 loop
        v_result := v_result + TO_UINT(data_vec((I + 1) * g_symbol_w - 1 downto I * g_symbol_w));
      end loop;
      v_result := RESIZE_UINT(v_result, g_sum_w);
      return TO_UVEC(v_result, g_sum_w);
    end if;
  end;

  signal rst                : std_logic;
  signal clk                : std_logic := '1';
  signal tb_end             : std_logic := '0';

  signal result_comb        : std_logic_vector(g_sum_w - 1 downto 0);  -- expected combinatorial sum

  signal in_data_vec        : std_logic_vector(c_data_vec_w - 1 downto 0) := (others => '0');
  signal in_data_vec_p      : std_logic_vector(c_data_vec_w - 1 downto 0);
  signal in_data_arr_p      : t_symbol_arr(0 to g_nof_inputs - 1);

  signal result_expected    : std_logic_vector(g_sum_w - 1 downto 0);  -- expected pipelined sum
  signal result_dut         : std_logic_vector(g_sum_w - 1 downto 0);  -- DUT sum
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 3;

  p_stimuli : process
  begin
    in_data_vec <= (others => '0');
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Apply equal symbol value inputs
    for I in 0 to 2**g_symbol_w - 1 loop
      in_data_vec <= func_data_vec(I);
      proc_common_wait_some_cycles(clk, 1);
    end loop;
    in_data_vec <= (others => '0');
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';

    wait;
  end process;

  -- For easier manual analysis in the wave window:
  -- . Pipeline the in_data_vec to align with the result
  -- . Map the concatenated symbols in in_data_vec into an in_data_arr_p array
  u_data_vec_p : entity work.common_pipeline
  generic map (
    g_representation => g_representation,
    g_pipeline       => c_pipeline_tree,
    g_reset_value    => 0,
    g_in_dat_w       => c_data_vec_w,
    g_out_dat_w      => c_data_vec_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => in_data_vec,
    out_dat => in_data_vec_p
  );

  p_data_arr : process(in_data_vec_p)
  begin
    for I in 0 to g_nof_inputs - 1 loop
      in_data_arr_p(I) <= in_data_vec_p((I + 1) * g_symbol_w - 1 downto I * g_symbol_w);
    end loop;
  end process;

  result_comb <= func_result(in_data_vec);

  u_result : entity work.common_pipeline
  generic map (
    g_representation => g_representation,
    g_pipeline       => c_pipeline_tree,
    g_reset_value    => 0,
    g_in_dat_w       => g_sum_w,
    g_out_dat_w      => g_sum_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => result_comb,
    out_dat => result_expected
  );

  -- Using work.common_adder_tree(recursive) will only invoke the recursive architecture once, because the next recursive level will default to using the last compiled architecture
  -- Therefore only instatiatiate the DUT once in this tb and use compile order to influence which architecture is used.
  dut : entity work.common_adder_tree  -- uses last compile architecture
  generic map (
    g_representation => g_representation,
    g_pipeline       => g_pipeline,
    g_nof_inputs     => g_nof_inputs,
    g_dat_w          => g_symbol_w,
    g_sum_w          => g_sum_w
  )
  port map (
    clk    => clk,
    in_dat => in_data_vec,
    sum    => result_dut
  );

  p_verify : process(rst, clk)
  begin
    if rst = '0' then
      if rising_edge(clk) then
        assert result_dut = result_expected
          report "Error: wrong result_dut"
          severity ERROR;
      end if;
    end if;
  end process;
end tb;
