--------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Checks the sync for input 0 and pass on the entire input array.
-- Description:
--   See mms_dp_sync_checker. The dp_sync_checker for input 0 causes a c_latency
--   of 2. Therefore the other inputs of the array are pipelined by the same
--   c_latency. The prerequisite is that all inputs in the array are aligned and
--   share the same sync as input 0, as is the case e.g. after a dp_bsn_aligner.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_sync_checker_arr is
  generic(
    g_cross_clock_domain : boolean := true;
    g_nof_blk_per_sync   : natural := 16;
    g_nof_streams        : natural := 1
  );
  port (
    mm_rst                   : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk                   : in  std_logic;  -- memory-mapped bus clock

    dp_clk                   : in  std_logic;
    dp_rst                   : in  std_logic;

    -- ST sinks
    snk_out_arr              : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr               : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr               : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr              : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    -- Memory Mapped
    reg_dp_sync_checker_mosi : in  t_mem_mosi;
    reg_dp_sync_checker_miso : out t_mem_miso
  );
end mms_dp_sync_checker_arr;

architecture str of mms_dp_sync_checker_arr is
  constant c_latency : natural := 2;

  signal sync_checker_snk_out : t_dp_siso;
  signal sync_checker_src_out : t_dp_sosi;
  signal pipeline_src_out_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
begin
  -- Copy flow control from output stream 0 via sync_checker to all input streams, ignore flow control from other output streams
  snk_out_arr <= (others => sync_checker_snk_out);

  -- Check sync on input stream 0
  u_mms_dp_sync_checker : entity work.mms_dp_sync_checker
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_nof_blk_per_sync   => g_nof_blk_per_sync
  )
  port map (
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    dp_clk                   => dp_clk,
    dp_rst                   => dp_rst,

    -- ST sinks
    snk_out                  => sync_checker_snk_out,
    snk_in                   => snk_in_arr(0),
    -- ST source
    src_in                   => src_in_arr(0),
    src_out                  => sync_checker_src_out,

    -- Memory Mapped
    reg_dp_sync_checker_mosi => reg_dp_sync_checker_mosi,
    reg_dp_sync_checker_miso => reg_dp_sync_checker_miso
  );

  -- Pipeline all input streams with same latency as mms_dp_sync_checker
  u_dp_pipeline_arr : entity dp_lib.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_streams,
    g_pipeline    => c_latency
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in_arr   => snk_in_arr,
    -- ST source
    src_out_arr  => pipeline_src_out_arr
  );

  -- copy sync_checker control to all output streams, pass on the pipelined data
  p_copy_sync_checker_controls : process(sync_checker_src_out, pipeline_src_out_arr)
  begin
    for i in 0 to g_nof_streams - 1 loop
      src_out_arr(i)      <= sync_checker_src_out;
      src_out_arr(i).data <= pipeline_src_out_arr(i).data;
      src_out_arr(i).re   <= pipeline_src_out_arr(i).re;
      src_out_arr(i).im   <= pipeline_src_out_arr(i).im;
    end loop;
  end process;
end str;
