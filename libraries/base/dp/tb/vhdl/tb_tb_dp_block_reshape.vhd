-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra, 9 Jan 2019
-- Purpose: Verify pipelining or no pipelining of dp_block_reshape

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_block_reshape is
end tb_tb_dp_block_reshape;

architecture tb of tb_tb_dp_block_reshape is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 5
  -- > run -all                 --> OK

--    g_pipeline   : NATURAL := 1   -- use 0 or 1

  u_comb   : entity work.tb_dp_block_reshape generic map (0);
  u_pipe   : entity work.tb_dp_block_reshape generic map (1);
end tb;
