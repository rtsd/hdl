--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, easics_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

use easics_lib.RAD_CRC16_D16.all;
use easics_lib.RAD_CRC18_D18.all;
use easics_lib.RAD_CRC20_D20.all;

use easics_lib.PCK_CRC16_D4.all;
use easics_lib.PCK_CRC16_D8.all;
use easics_lib.PCK_CRC16_D9.all;
use easics_lib.PCK_CRC16_D10.all;
use easics_lib.PCK_CRC16_D16.all;
use easics_lib.PCK_CRC16_D18.all;
use easics_lib.PCK_CRC16_D20.all;
use easics_lib.PCK_CRC16_D32.all;
use easics_lib.PCK_CRC16_D36.all;
use easics_lib.PCK_CRC16_D48.all;
use easics_lib.PCK_CRC16_D64.all;
use easics_lib.PCK_CRC16_D72.all;

use easics_lib.PCK_CRC32_D4.all;
use easics_lib.PCK_CRC32_D8.all;
use easics_lib.PCK_CRC32_D9.all;
use easics_lib.PCK_CRC32_D10.all;
use easics_lib.PCK_CRC32_D16.all;
use easics_lib.PCK_CRC32_D18.all;
use easics_lib.PCK_CRC32_D20.all;
use easics_lib.PCK_CRC32_D32.all;
use easics_lib.PCK_CRC32_D36.all;
use easics_lib.PCK_CRC32_D48.all;
use easics_lib.PCK_CRC32_D64.all;
use easics_lib.PCK_CRC32_D72.all;

package dp_packetizing_pkg is
  --<types>--

  --<functions>--
  function func_dp_next_crc(c_lofar : boolean; dat, crc : std_logic_vector) return std_logic_vector;

  --<constants>--
  constant c_dp_max_w          : natural := 64;
  constant c_dp_gap_min        : natural := 4;  -- minimal gap between valid data blocks (to insert >= 1 idle, sfd, fsn and brc/crc into the stream)
  constant c_dp_idle           : std_logic_vector(c_dp_max_w - 1 downto 0) := "0101010101010101010101010101010101010101010101010101010101010101";  -- 0x..5555
  constant c_dp_sfd            : std_logic_vector(c_dp_max_w - 1 downto 0) := "0101010101010101010101010101010101010101010101010101010101010100";  -- 0x..5554;
  -- define more c_dp_sfd_* to have multiple independent dp frame streams per link, e.g. as was used for LOFAR:
  constant c_dp_sfd_lofar      : std_logic_vector(c_dp_max_w - 1 downto 0) := "0101010101010101010101010101010101010101010101010001000011111010";  -- 0x..10FA;

  constant c_dp_brc_ok         : natural := 0;
  constant c_dp_brc_err        : natural := 1;

  constant c_dp_crc_w          : natural := 16;

  constant c_dp_mode_w         : natural := 2;
  constant c_dp_mode_disable   : std_logic_vector(c_dp_mode_w - 1 downto 0) := "01";
  constant c_dp_mode_buf       : std_logic_vector(c_dp_mode_w - 1 downto 0) := "00";
  constant c_dp_mode_stream    : std_logic_vector(c_dp_mode_w - 1 downto 0) := "11";
  constant c_dp_mode_combine   : std_logic_vector(c_dp_mode_w - 1 downto 0) := "10";
end dp_packetizing_pkg;

package body dp_packetizing_pkg is
  --<functions>--

  ------------------------------------------------------------------------------
  -- FUNCTION: Calculate next CRC for this data
  ------------------------------------------------------------------------------

  function func_dp_next_crc(c_lofar : boolean; dat, crc : std_logic_vector) return std_logic_vector is
    variable nxt_crc : std_logic_vector(crc'range);
  begin
    if c_lofar = true then
      case dat'length is
        when 16 => nxt_crc := nextRAD_CRC16_D16(RESIZE_UVEC(dat, 16), crc);
        when 18 => nxt_crc := nextRAD_CRC18_D18(RESIZE_UVEC(dat, 18), crc);
        when 20 => nxt_crc := nextRAD_CRC20_D20(RESIZE_UVEC(dat, 20), crc);
        when others   => report "DP data width not supported in easics_lib RAD for LOFAR" severity FAILURE;
      end case;

    else
      -- CRC-16
      if c_dp_crc_w = 16 then
        case dat'length is
          when 1  to 4  => nxt_crc := nextCRC16_D4( RESIZE_UVEC(dat,  4), crc);
          when 5  to 8  => nxt_crc := nextCRC16_D8( RESIZE_UVEC(dat,  8), crc);
          when 9        => nxt_crc := nextCRC16_D9(             dat,      crc);
          when 10       => nxt_crc := nextCRC16_D10(            dat,      crc);
          when 11 to 16 => nxt_crc := nextCRC16_D16(RESIZE_UVEC(dat, 16), crc);
          when 17 to 18 => nxt_crc := nextCRC16_D18(RESIZE_UVEC(dat, 18), crc);
          when 19 to 20 => nxt_crc := nextCRC16_D20(RESIZE_UVEC(dat, 20), crc);
          when 21 to 32 => nxt_crc := nextCRC16_D32(RESIZE_UVEC(dat, 32), crc);
          when 33 to 36 => nxt_crc := nextCRC16_D36(RESIZE_UVEC(dat, 36), crc);
          when 37 to 48 => nxt_crc := nextCRC16_D48(RESIZE_UVEC(dat, 48), crc);
          when 49 to 64 => nxt_crc := nextCRC16_D64(RESIZE_UVEC(dat, 64), crc);
          when 65 to 72 => nxt_crc := nextCRC16_D72(RESIZE_UVEC(dat, 72), crc);
          when others   => report "DP data width not supported in easics_lib CRC-16" severity FAILURE;
        end case;
      end if;
      -- CRC-32
      if c_dp_crc_w = 32 then
        case dat'length is
          when 1  to 4  => nxt_crc := nextCRC32_D4( RESIZE_UVEC(dat,  4), crc);
          when 5  to 8  => nxt_crc := nextCRC32_D8( RESIZE_UVEC(dat,  8), crc);
          when 9        => nxt_crc := nextCRC32_D9(             dat,      crc);
          when 10       => nxt_crc := nextCRC32_D10(            dat,      crc);
          when 11 to 16 => nxt_crc := nextCRC32_D16(RESIZE_UVEC(dat, 16), crc);
          when 17 to 18 => nxt_crc := nextCRC32_D18(RESIZE_UVEC(dat, 18), crc);
          when 19 to 20 => nxt_crc := nextCRC32_D20(RESIZE_UVEC(dat, 20), crc);
          when 21 to 32 => nxt_crc := nextCRC32_D32(RESIZE_UVEC(dat, 32), crc);
          when 33 to 36 => nxt_crc := nextCRC32_D36(RESIZE_UVEC(dat, 36), crc);
          when 37 to 48 => nxt_crc := nextCRC32_D48(RESIZE_UVEC(dat, 48), crc);
          when 49 to 64 => nxt_crc := nextCRC32_D64(RESIZE_UVEC(dat, 64), crc);
          when 65 to 72 => nxt_crc := nextCRC32_D72(RESIZE_UVEC(dat, 72), crc);
          when others   => report "DP data width not supported in easics_lib CRC-32" severity FAILURE;
        end case;
      end if;
    end if;

    return nxt_crc;
  end func_dp_next_crc;
end dp_packetizing_pkg;
