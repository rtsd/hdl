-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity tb_avs_i2c_master is
end tb_avs_i2c_master;

architecture tb of tb_avs_i2c_master is
  component avs_i2c_master is
    generic (
      g_control_adr_w  : natural := 1;
      g_protocol_adr_w : natural := 10;
      g_result_adr_w   : natural := 10;
      g_clk_cnt        : natural := 399;
      g_comma_w        : natural := 0
    );
    port (
      coe_gs_sim_export      : in    std_logic                     := 'X';  -- gs_sim.export
      coe_sync_export        : in    std_logic                     := 'X';  -- sync.export
      coe_i2c_scl_export     : inout std_logic                     := 'X';  -- i2c_scl.export
      coe_i2c_sda_export     : inout std_logic                     := 'X';  -- i2c_sda.export
      csi_system_reset       : in    std_logic                     := 'X';  -- system.reset
      csi_system_clk         : in    std_logic                     := 'X';  -- .clk
      avs_control_address    : in    std_logic                     := 'X';  -- control.address
      avs_control_write      : in    std_logic                     := 'X';  -- .write
      avs_control_read       : in    std_logic                     := 'X';  -- .read
      avs_control_writedata  : in    std_logic_vector(31 downto 0) := (others => 'X');  -- .writedata
      avs_control_readdata   : out   std_logic_vector(31 downto 0);  -- .readdata
      avs_protocol_address   : in    std_logic_vector(9 downto 0)  := (others => 'X');  -- protocol.address
      avs_protocol_write     : in    std_logic                     := 'X';  -- .write
      avs_protocol_read      : in    std_logic                     := 'X';  -- .read
      avs_protocol_writedata : in    std_logic_vector(7 downto 0)  := (others => 'X');  -- .writedata
      avs_protocol_readdata  : out   std_logic_vector(7 downto 0);  -- .readdata
      avs_result_address     : in    std_logic_vector(9 downto 0)  := (others => 'X');  -- result.address
      avs_result_write       : in    std_logic                     := 'X';  -- .write
      avs_result_read        : in    std_logic                     := 'X';  -- .read
      avs_result_writedata   : in    std_logic_vector(7 downto 0)  := (others => 'X');  -- .writedata
      avs_result_readdata    : out   std_logic_vector(7 downto 0);  -- .readdata
      ins_interrupt_irq      : out   std_logic  -- interrupt.irq
    );
  end component avs_i2c_master;

  constant c_clk_period : time := 5 ns;

  signal reset    : std_logic;
  signal clk      : std_logic := '0';

  signal i2c_scl  : std_logic;
  signal i2c_sda  : std_logic;
begin
  reset <= '1', '0' after c_clk_period * 10;
  clk <= not clk after c_clk_period / 2;

  --u_avs_i2c_m : ENTITY work.avs_i2c_master
  u_avs_i2c_m : component avs_i2c_master
  generic map (
    -- g_i2c_mm
    g_control_adr_w    => 1,
    g_protocol_adr_w   => 10,
    g_result_adr_w     => 10,
    -- g_i2c_phy
    g_clk_cnt          => 399,
    g_comma_w          => 0
  )
  port map (
    ---------------------------------------------------------------------------
    -- Avalon Conduit interfaces: coe_*_export
    ---------------------------------------------------------------------------
    -- GENERIC Signal
    coe_gs_sim_export        => '1',

    -- System
    coe_sync_export          => '1',

    -- I2C interface
    coe_i2c_scl_export       => i2c_scl,
    coe_i2c_sda_export       => i2c_sda,

    ---------------------------------------------------------------------------
    -- Avalon Clock Input interface: csi_*
    ---------------------------------------------------------------------------
    csi_system_reset         => reset,
    csi_system_clk           => clk,

    ---------------------------------------------------------------------------
    -- Avalon Memory Mapped Slave interfaces: avs_*
    ---------------------------------------------------------------------------
    -- MM slave I2C control register
    avs_control_address      => c_sl0,
    avs_control_write        => c_sl0,
    avs_control_read         => c_sl0,
    avs_control_writedata    => c_slv0(31 downto 0),
    avs_control_readdata     => OPEN,
    -- MM slave I2C protocol register
    avs_protocol_address     => c_slv0(9 downto 0),
    avs_protocol_write       => c_sl0,
    avs_protocol_read        => c_sl0,
    avs_protocol_writedata   => c_slv0(7 downto 0),
    avs_protocol_readdata    => OPEN,
    -- MM slave I2C result register
    avs_result_address       => c_slv0(9 downto 0),
    avs_result_write         => c_sl0,
    avs_result_read          => c_sl0,
    avs_result_writedata     => c_slv0(7 downto 0),
    avs_result_readdata      => OPEN,

    ---------------------------------------------------------------------------
    -- Avalon Interrupt Sender interface: ins_*
    ---------------------------------------------------------------------------
    ins_interrupt_irq        => open
  );
end;
