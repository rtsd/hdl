--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose: Verify the adc08d1020 test pattern on ADU for one signal path
-- Description:
--   The adc08d1020 has two ADCs I and Q, so it outputs two signal paths. Both
--   use different test patterns, therefore via pattern_sel one can select
--   whether to verify for the I or the Q signal path. The test pattern data is
--   periodic over 10 samples:
--
--        TP_I TP_Q  TP_OV
--     T0 02h  01h   0
--     T1 FDh  FEh   1
--     T2 02h  01h   0
--     T3 02h  01h   0
--     T4 FDh  FEh   1
--     T5 FDh  FEh   1
--     T6 02h  01h   0
--     T7 02h  01h   0
--     T8 FDh  FEh   1
--     T9 02h  01h   0
--
--   The verification is always ready to accept data, therefore it has no
--   in_siso.ready output. The verification is always enabled. After reset and
--   when verify_res_ack pulses then verify_res_val = '0'.
--   The verification needs two words to initialize its local reference pattern
--   generator and then the next words can be verified. At the third valid
--   input word the verify_res_val goes active '1' and remains active until the
--   next verify_res_ack pulse. If the received data is a mismatch with the
--   local reference pattern then the verify_res goes high '1' and remains '1'
--   until the next verify_res_ack pulse.
--   The verify_res[8] contains the matching result for the aggregate symbol
--   values, and verify_res[7:0] contains the result per corresponding symbol
--   bit [7:0]. Via verify_res[7:0] the skew between LVDS input lines can be
--   measured. Via verify_res[8] it becomes clear whether the skew is
--   sufficiently small to have an open sampling eye for the entire symbol
--   value.
--   The duration of the verification interval depends on verify_res_ack, each
--   time verify_res_ack pulses a new verification interval starts.
--
-- Remarks:
-- . The overflow bit is not verified
-- . The TP_I and TP_Q test symbols effectively only contain two values (0x02,
--   0xFD) or (0x01, 0xFE) respectively. Hence these can be mapped on single
--   bit values '0' and '1' as is doen via the signal symb.
-- . The TP_I and TP_Q symbols are verified per bit and for the entire symbol
--   via the mapped symb signal. The 8 symbol bits and the mapped symb signal
--   all have the test pattern of 10 values: 0 1 0 0 1 1 0 0 1 0.
-- . The data arrives with g_nof_symbols_per_data=4 symbols per data, so a
--   sequence of two test patterns (2*10 divides by 4) can appear at 10
--   different phases as:
--
--   Phase Pattern     Pattern        Nibble hex values
--     0   0100 1100 1001 0011 0010 = 4 C 9 3 2
--     1   1001 1001 0010 0110 0100 = 9 9 2 6 4
--     2   0011 0010 0100 1100 1001 = 3 2 4 C 9
--     3   0110 0100 1001 1001 0010 = 6 4 9 9 2
--     4   1100 1001 0011 0010 0100 = C 9 3 2 4
--     5   1001 0010 0110 0100 1001 = 9 2 6 4 9
--     6   0010 0100 1100 1001 0011 = 2 4 C 9 3
--     7   0100 1001 1001 0010 0110 = 4 9 9 2 6
--     8   1001 0011 0010 0100 1100 = 9 3 2 4 C
--     9   0010 0110 0100 1001 1001 = 2 6 4 9 9
--
--   Hence for phase 0 to 9 the 4-bit nibbles can either be repeated
--   <4 C 9 3 2> or <9 9 2 6 4>. E.g. if the first two data words map to 4 C
--   then the next expected data word is 9. One data word (i.e. 4 symbols of
--   the 10) is not enough to know the next test pattern data word. Two data
--   words (i.e. 8 symbols of the 10) are sufficient to know the next test
--   pattern data word. This is implemented by func_tp_seq in aduh_verify_bit.

entity aduh_verify is
  generic (
    g_symbol_w             : natural := 8;  -- Fixed
    g_nof_symbols_per_data : natural := 4  -- Fixed, big endian in_sosi.data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    -- ST input
    in_sosi        : in  t_dp_sosi;  -- signal path data with 4 800MHz 8b samples in time per one 32b word @ 200MHz

    -- Static control input (connect via MM or leave open to use default)
    pattern_sel    : in  natural range 0 to 1 := 0;  -- 0 = DI, 1 = DQ
    verify_res     : out std_logic_vector(g_symbol_w downto 0);
    verify_res_val : out std_logic;
    verify_res_ack : in  std_logic
  );
end aduh_verify;

architecture rtl of aduh_verify is
  constant c_tp_symbol   : t_slv_8_arr(0 to 1) := (X"02", X"01");  -- = (I, Q), use patter_sel to select

  type t_nibble_arr is array (integer range <>) of std_logic_vector(0 to g_nof_symbols_per_data - 1);  -- here use index [0:3] for the big endian nibbles

  signal symbols             : t_slv_8_arr(0 to g_nof_symbols_per_data - 1);
  signal symb                : std_logic_vector(0 to g_nof_symbols_per_data - 1);  -- here use index [0:3] for the big endian nibbles
  signal symb_err            : std_logic_vector(0 to g_nof_symbols_per_data - 1);
  signal bits                : t_nibble_arr(g_symbol_w - 1 downto 0);

  signal in_val              : std_logic;
  signal nxt_in_val          : std_logic;
  signal in_symb             : std_logic_vector(0 to g_nof_symbols_per_data - 1);  -- here use index [0:3] for the big endian nibbles
  signal nxt_in_symb         : std_logic_vector(0 to g_nof_symbols_per_data - 1);
  signal in_symb_err         : std_logic;
  signal nxt_in_symb_err     : std_logic;
  signal in_bits             : t_nibble_arr(g_symbol_w - 1 downto 0);
  signal nxt_in_bits         : t_nibble_arr(g_symbol_w - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- 1) Prepare the nibble (width g_nof_symbols_per_data) streams for:
  -- . the aggregate symbol bit (in_symb)
  -- . the g_symbol_w = 8 individual bits (in_bits)
  ------------------------------------------------------------------------------

  p_reg : process (rst, clk)
  begin
    if rst = '1' then
      in_val           <= '0';
      in_symb          <= (others => '0');
      in_symb_err      <= '1';
      in_bits          <= (others => (others => '0'));
    elsif rising_edge(clk) then
      in_val           <= nxt_in_val;
      in_symb          <= nxt_in_symb;
      in_symb_err      <= nxt_in_symb_err;
      in_bits          <= nxt_in_bits;
    end if;
  end process;

  p_symbols : process(in_sosi, pattern_sel)
  begin
    for I in 0 to g_nof_symbols_per_data - 1 loop
      -- Big endian symbols in data. After applying the I or Q mask (from c_tp_symbol) the symbol can be 0x00 or 0xFF if it is a test symbol.
      symbols(I) <= offset_binary(in_sosi.data((g_nof_symbols_per_data - I) * g_symbol_w - 1 downto (g_nof_symbols_per_data - I - 1) * g_symbol_w)) xor c_tp_symbol(pattern_sel);
    end loop;
  end process;

  p_symb : process(symbols)
  begin
    -- If the symbol is a test symbol 0x00 and 0xFF then these result in symb() is '0' and '1' respectively, and with symb_err() = '0'.
    -- If the symbol is not a test symbol this is indicated via symb_err() = '1'.
    for I in 0 to g_nof_symbols_per_data - 1 loop
      if unsigned(symbols(I)) = 0 then
        symb(I)     <= '0';
        symb_err(I) <= '0';
      elsif unsigned(symbols(I)) = 16#FF# then
        symb(I)     <= '1';
        symb_err(I) <= '0';
      else
        symb(I)     <= '0';
        symb_err(I) <= '1';
      end if;
    end loop;
  end process;

  p_bits : process(symbols)
  begin
    for I in g_symbol_w - 1 downto 0 loop
      for J in 0 to g_nof_symbols_per_data - 1 loop
        bits(I)(J) <= symbols(J)(I);
      end loop;
    end loop;
  end process;

  nxt_in_val       <= in_sosi.valid;
  nxt_in_symb      <= symb;  -- 1 nibbles stream for the mapped symbols
  nxt_in_symb_err  <= vector_or(symb_err);
  nxt_in_bits      <= bits;  -- 8 nibbles streams, one per symbol bit

  ------------------------------------------------------------------------------
  -- 2) Verify the test pattern for:
  -- . the aggregate symbol bit (in_symb)
  -- . the g_symbol_w = 8 individual bits (in_bits)
  ------------------------------------------------------------------------------

  u_verify_symb : entity work.aduh_verify_bit
  generic map (
    g_nof_symbols_per_data => g_nof_symbols_per_data
  )
  port map (
    rst            => rst,
    clk            => clk,

    -- ST input
    in_val         => in_val,
    in_dat         => in_symb,
    in_dat_err     => in_symb_err,

    -- Static control input (connect via MM or leave open to use default)
    verify_res     => verify_res(g_symbol_w),
    verify_res_val => verify_res_val,
    verify_res_ack => verify_res_ack
  );

  gen_verify : for I in g_symbol_w - 1 downto 0 generate
    u_bit : entity work.aduh_verify_bit
    generic map (
      g_nof_symbols_per_data => g_nof_symbols_per_data
    )
    port map (
      rst            => rst,
      clk            => clk,

      -- ST input
      in_val         => in_val,
      in_dat         => in_bits(I),
      in_dat_err     => '0',

      -- Static control input (connect via MM or leave open to use default)
      verify_res     => verify_res(I),
      verify_res_val => OPEN,
      verify_res_ack => verify_res_ack
    );
  end generate;
end rtl;
