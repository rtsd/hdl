--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
-- . Control a source's output rate by toggling its ready input.
-- Description:
-- . The following diagram shows how src_out depends on the generics (only
--   two src_out samples shown):
--                                            |<--g_block_period-->|
--  src_out) |<-----g_init_valid_delay----->[0]                             [1]
--           ^
--           |
--           init_valid_delay_ref='1'

entity dp_src_out_timer is
  generic (
    g_init_valid_delay       : natural := 0;
    g_block_period  : natural := 1;  -- Block period >1
    g_block_len              : natural := 1  -- Nof words per valid block
  );
  port (
    rst                  : in  std_logic;
    clk                  : in  std_logic;

    init_valid_delay_ref : in std_logic := '1';

    snk_in               : in  t_dp_sosi;
    snk_out              : out t_dp_siso  -- rl=1
  );
end dp_src_out_timer;

architecture rtl of dp_src_out_timer is
  type t_state is (s_init, s_wait, s_delay, s_ready, s_not_ready);
  signal state             : t_state;
  signal nxt_state         : t_state;

  constant c_max_count     : natural := largest(largest(1, g_init_valid_delay), g_block_period);

  signal cycle_cnt         : std_logic_vector(ceil_log2(c_max_count) - 1 downto 0);
  signal nxt_cycle_cnt     : std_logic_vector(ceil_log2(c_max_count) - 1 downto 0);

  signal prv_snk_out_ready : std_logic;
  signal snk_out_ready     : std_logic;
  signal nxt_snk_out_ready : std_logic;
begin
  snk_out.ready <= snk_out_ready;
  snk_out.xon   <= '1';

  -----------------------------------------------------------------------------
  -- FSM that controls ready signal
  -----------------------------------------------------------------------------
  p_state : process(state, init_valid_delay_ref, cycle_cnt, snk_out_ready, prv_snk_out_ready, snk_in.valid)
  begin
    nxt_state         <= state;
    nxt_cycle_cnt     <= cycle_cnt;
    nxt_snk_out_ready <= snk_out_ready;

    case state is

      when s_wait =>
        -- Wait for our delay reference input to go high
        if init_valid_delay_ref = '1' then
          nxt_state <= s_delay;
        end if;

      when s_delay =>
        -- Delay for X cycles after reference input went high
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if TO_UINT(cycle_cnt) >= g_init_valid_delay then
          nxt_state     <= s_ready;
          nxt_cycle_cnt <= (others => '0');
          nxt_snk_out_ready <= '1';
        end if;

      when s_ready =>
        nxt_snk_out_ready <= '1';

        -- Clock out g_block_len words
        if g_block_period > 1 then
          nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
          if TO_UINT(cycle_cnt) >= g_block_len - 1 then
            nxt_state         <= s_not_ready;
            nxt_snk_out_ready <= '0';
          end if;
        end if;
        if prv_snk_out_ready /= snk_in.valid then
          -- We're no longer provided with valid data to time, so we lost timing. Reset.
          nxt_state <= s_init;
        end if;

      when s_not_ready =>
        nxt_snk_out_ready <= '0';
        -- De-assert ready to get some invalid cycles if required
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if TO_UINT(cycle_cnt) >= g_block_period - 1 then
          nxt_state         <= s_ready;
          nxt_snk_out_ready <= '1';
          nxt_cycle_cnt     <= (others => '0');
        end if;
        if prv_snk_out_ready /= snk_in.valid then
          -- We're no longer provided with valid data to time, so we lost timing. Reset.
          nxt_state <= s_init;
        end if;

      when others =>  -- s_init
        nxt_state         <= s_wait;
        nxt_snk_out_ready <= '0';
        nxt_cycle_cnt     <= (others => '0');
    end case;
  end process;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      state             <= s_init;
      snk_out_ready     <= '0';
      prv_snk_out_ready <= '0';
      cycle_cnt         <= (others => '0');
    elsif rising_edge(clk) then
      state             <= nxt_state;
      snk_out_ready     <= nxt_snk_out_ready;
      prv_snk_out_ready <= snk_out_ready;
      cycle_cnt         <= nxt_cycle_cnt;
    end if;
  end process;
end rtl;
