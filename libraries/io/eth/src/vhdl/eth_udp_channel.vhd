-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

entity eth_udp_channel is
  port (
    -- Clocks and reset
    rst           : in  std_logic;
    clk           : in  std_logic;

    -- Streaming Sink
    snk_in        : in  t_dp_sosi;
    snk_out       : out t_dp_siso;

    -- Streaming Source
    src_in        : in  t_dp_siso;
    src_out       : out t_dp_sosi;

    -- Demux control
    reg_demux     : in  t_eth_mm_reg_demux;
    hdr_status    : in  t_eth_hdr_status
  );
end eth_udp_channel;

architecture rtl of eth_udp_channel is
  -- ETH channel number
  signal channel     : natural range 0 to c_eth_nof_channels - 1;
  signal nxt_channel : natural;
begin
  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      channel <= 0;
    elsif rising_edge(clk) then
      channel <= nxt_channel;
    end if;
  end process;

  -- Determine the demux UDP stream channel number
  p_udp_ports : process(hdr_status, reg_demux)
  begin
    nxt_channel <= 0;  -- default use control via channel 0
    for I in 1 to c_eth_nof_udp_ports loop
      if hdr_status.is_udp = '1' and reg_demux.udp_ports_en(I) = '1' then
        if unsigned(hdr_status.udp_port) = unsigned(reg_demux.udp_ports(I)) then
          nxt_channel <= I;  -- UDP offload channels : channel>0
        end if;
      end if;
    end loop;
  end process;

  -- Pass on the siso
  snk_out <= src_in;

  -- Fill in the channel field in the stream t_dp_stream
  src_out <= func_dp_stream_channel_set(snk_in, channel);
end rtl;
