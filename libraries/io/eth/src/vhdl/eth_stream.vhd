-------------------------------------------------------------------------------
--
-- Copyright (C) 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra
-- Purpose:
--   Provide Ethernet access to a node for one UDP stream via TSE.
-- Description:
-- * This eth_stream.vhd is a stripped down version of eth.vhd to offload and
--   onload one UDP stream.
--   . see eth_stream_udp.vhd for UDP offload/onload stream details
--   . contains the TSE
--   . sets up TSE in state machnine and then switch to external mm_ctlr,
--     when setup_done = '1', to allow external monitoring of the TSE
--   . use g_jumbo_en = FALSE to support 1500 octet frames 1518 like in
--     unb_osy/unbos_eth.h, or use g_jumbo_en = TRUE to support 9000 octet
--     frames. With g_jumbo_en = FALSE a 9000 octet packet is received
--     properly, but has rx_src_out.err = 3 indicating invalid length.
--     Use c_jumbo_en = TRUE to avoid invalid length.
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+ETH+tester+unit+for+1GbE

library IEEE, common_lib, dp_lib, technology_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use work.eth_pkg.all;
use technology_lib.technology_select_pkg.all;

entity eth_stream is
  generic (
    g_technology   : natural := c_tech_select_default;
    g_ETH_PHY      : string  := "LVDS";  -- "LVDS" (default): uses LVDS IOs for ctrl_unb_common, "XCVR": uses tranceiver PHY
    g_rx_udp_port  : natural := TO_UINT(c_eth_rx_udp_port);
    g_jumbo_en     : boolean := true;
    g_sim          : boolean := false;
    g_sim_level    : natural := 0  -- 0 = use IP model (equivalent to g_sim = FALSE); 1 = use fast serdes model;
  );
  port (
    -- Clocks and reset
    mm_rst             : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk             : in  std_logic;  -- memory-mapped bus clock
    eth_clk            : in  std_logic;  -- ethernet phy reference clock
    st_rst             : in  std_logic;  -- reset synchronous with st_clk
    st_clk             : in  std_logic;  -- packet stream clock

    cal_rec_clk        : in  std_logic := '0';  -- Calibration & reconfig clock when using XCVR

    -- TSE setup
    src_mac            : in std_logic_vector(c_48 - 1 downto 0);
    setup_done         : out std_logic;

    -- UDP transmit interface
    udp_tx_snk_in      : in  t_dp_sosi := c_dp_sosi_rst;
    udp_tx_snk_out     : out t_dp_siso;

    -- UDP receive interface
    udp_rx_src_in      : in  t_dp_siso := c_dp_siso_rdy;
    udp_rx_src_out     : out t_dp_sosi;

    -- Memory Mapped Slaves
    tse_ctlr_copi      : in  t_mem_mosi;  -- ETH TSE MAC registers
    tse_ctlr_cipo      : out t_mem_miso;

    -- PHY interface
    eth_txp            : out std_logic;
    eth_rxp            : in  std_logic;

    -- LED interface
    tse_led            : out t_tech_tse_led
  );
end eth_stream;

architecture str of eth_stream is
  -- Tx UDP offload stream to TSE
  signal tse_tx_sosi   : t_dp_sosi;
  signal tse_tx_siso   : t_dp_siso;

  -- Rx stream from TSE (contains the UDP onload packets and possibly other
  -- network packets)
  signal tse_rx_sosi   : t_dp_sosi;
  signal tse_rx_siso   : t_dp_siso;
begin
  u_eth_stream_udp : entity work.eth_stream_udp
  generic map (
    g_rx_udp_port => g_rx_udp_port
  )
  port map (
    -- Clocks and reset
    st_rst        => st_rst,
    st_clk        => st_clk,

    -- User UDP interface
    -- . Tx
    udp_tx_sosi   => udp_tx_snk_in,
    udp_tx_siso   => udp_tx_snk_out,
    -- . Rx
    udp_rx_sosi   => udp_rx_src_out,
    udp_rx_siso   => udp_rx_src_in,

    -- PHY interface
    -- . Tx
    tse_tx_sosi   => tse_tx_sosi,
    tse_tx_siso   => tse_tx_siso,
    -- . Rx
    tse_rx_sosi   => tse_rx_sosi,
    tse_rx_siso   => tse_rx_siso
  );

  u_tech_tse_with_setup : entity tech_tse_lib.tech_tse_with_setup
  generic map (
    g_technology   => g_technology,
    g_ETH_PHY      => g_ETH_PHY,
    g_jumbo_en     => g_jumbo_en,
    g_sim          => g_sim,
    g_sim_level    => g_sim_level
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,  -- MM
    eth_clk        => eth_clk,  -- 125 MHz
    tx_snk_clk     => st_clk,  -- DP
    rx_src_clk     => st_clk,  -- DP

    -- TSE setup
    src_mac        => src_mac,
    setup_done     => setup_done,

    -- Calibration & reconfig clock
    cal_rec_clk    => cal_rec_clk,

    -- Memory Mapped Peripheral
    mm_ctlr_copi   => tse_ctlr_copi,
    mm_ctlr_cipo   => tse_ctlr_cipo,

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      => tse_tx_sosi,
    tx_snk_out     => tse_tx_siso,

    -- MAC receive interface
    -- . ST Source
    rx_src_in      => tse_rx_siso,
    rx_src_out     => tse_rx_sosi,

    -- PHY interface
    eth_txp        => eth_txp,
    eth_rxp        => eth_rxp,

    tse_led        => tse_led
  );
end str;
