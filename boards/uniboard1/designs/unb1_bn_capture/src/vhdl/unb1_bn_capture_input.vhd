-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Capture input from two ADU or use WG data and attach timestamp.

library IEEE, common_lib, unb1_board_lib, dp_lib, diag_lib, ppsh_lib, aduh_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.unb1_bn_capture_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use aduh_lib.aduh_dd_pkg.all;

entity unb1_bn_capture_input is
  generic (
    -- General
    g_sim         : boolean := false;
    -- BN capture specific
    g_bn_capture  : t_c_bn_capture := c_bn_capture;
    -- Use PHY Interface
    g_use_phy     : t_c_unb1_board_use_phy := (1, 0, 0, 0, 0, 0, 1, 1);
    -- ADC Interface
    g_nof_dp_phs_clk : natural := 1;  -- nof dp_phs_clk that can be used to detect the word phase
    g_ai             : t_c_aduh_dd_ai := c_aduh_dd_ai
  );
  port (
    -- ADC Interface
    -- . ADU_AB
    ADC_BI_A                  : in  std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_B                  : in  std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_A_CLK              : in  std_logic;
    ADC_BI_A_CLK_RST          : out std_logic;

    -- . ADU_CD
    ADC_BI_C                  : in  std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_D                  : in  std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_D_CLK              : in  std_logic;
    ADC_BI_D_CLK_RST          : out std_logic;

    -- Clocks and reset
    mm_rst                    : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk                    : in  std_logic;  -- memory-mapped bus clock
    dp_rst                    : in  std_logic;  -- reset synchronous with st_clk
    dp_clk                    : in  std_logic;  -- streaming clock domain clock
    dp_phs_clk_vec            : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);  -- divided and phase shifted dp_clk
    dp_pps                    : in  std_logic;  -- external PPS in captured in dp_clk domain

    -- MM bsn source
    reg_bsn_source_mosi       : in  t_mem_mosi;  -- Start a BSN timestamp
    reg_bsn_source_miso       : out t_mem_miso;

    -- MM bsn scheduler for WG
    reg_bsn_scheduler_wg_mosi : in  t_mem_mosi;  -- Schedule WG restart at a BSN, read current BSN
    reg_bsn_scheduler_wg_miso : out t_mem_miso;

    -- MM aduh quad
    reg_adc_quad_mosi         : in  t_mem_mosi;  -- ADUH locked status and pattern verify for two half ADUs so 4 ADC inputs
    reg_adc_quad_miso         : out t_mem_miso;

    -- MM wideband waveform generator ports [A, B, C, D]
    reg_wg_mosi_arr           : in  t_mem_mosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = WG control ports [A,B,C,D]
    reg_wg_miso_arr           : out t_mem_miso_arr(0 to g_ai.nof_sp - 1);
    ram_wg_mosi_arr           : in  t_mem_mosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = WG buffer ports [A,B,C,D]
    ram_wg_miso_arr           : out t_mem_miso_arr(0 to g_ai.nof_sp - 1);

    -- MM shiftram
    reg_dp_shiftram_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_shiftram_miso      : out t_mem_miso;

    -- MM signal path monitor buffers for [A, B, C, D]
    reg_mon_mosi_arr          : in  t_mem_mosi_arr(0 to g_ai.nof_sp - 1);  -- read only access to the data monitor mean sum and power sum
    reg_mon_miso_arr          : out t_mem_miso_arr(0 to g_ai.nof_sp - 1);
    ram_mon_mosi_arr          : in  t_mem_mosi_arr(0 to g_ai.nof_sp - 1);  -- read and overwrite access to the data monitor buffers
    ram_mon_miso_arr          : out t_mem_miso_arr(0 to g_ai.nof_sp - 1);

    -- Streaming output (can be from ADU or from internal WG)
    sp_sosi_arr               : out t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = Signal Paths [A,B,C,D]
    sp_siso_arr               : in  t_dp_siso_arr(0 to g_ai.nof_sp - 1)
  );
end unb1_bn_capture_input;

architecture str of unb1_bn_capture_input is
  constant c_dp_factor                    : natural := g_ai.rx_factor * g_ai.dd_factor;
  constant c_wideband_factor              : natural := c_dp_factor;  -- Wideband rate factor = 4 for dp_clk is 200 MHz frequency and sample frequency Fs is 800 MHz

  constant c_bs_block_size                : natural := g_bn_capture.sp.nof_samples_per_block / c_wideband_factor;
  constant c_bs_nof_block_per_sync        : natural := g_bn_capture.sp.nof_samples_per_sync / g_bn_capture.sp.nof_samples_per_block;
  constant c_bs_bsn_w                     : natural := c_dp_stream_bsn_w;  -- = 48

  constant c_wg_buf_directory             : string := "data/";
  constant c_wg_buf_dat_w                 : natural := c_unb1_board_peripherals_mm_reg_default.ram_diag_wg_dat_w;
  constant c_wg_buf_addr_w                : natural := c_unb1_board_peripherals_mm_reg_default.ram_diag_wg_adr_w;

  -- DP streaming
  signal bs_sosi                 : t_dp_sosi;
  signal dp_bsn_trigger_wg       : std_logic;

  signal aduh_sosi_arr           : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = ADC_BI ports [A,B,C,D]

  signal wg_ovr                  : std_logic_vector(g_ai.nof_sp * c_wideband_factor               - 1 downto 0);  -- big endian, so first output sample in MSBit, MSData
  signal wg_dat                  : std_logic_vector(g_ai.nof_sp * c_wideband_factor * c_wg_buf_dat_w - 1 downto 0);
  signal wg_val                  : std_logic_vector(g_ai.nof_sp * c_wideband_factor               - 1 downto 0);
  signal wg_sync                 : std_logic_vector(g_ai.nof_sp * c_wideband_factor               - 1 downto 0);
  signal wg_sosi_arr             : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = WG ports [A,B,C,D]

  signal mux_sosi_arr            : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);  -- = [0:3] = Signal Paths [A,B,C,D]
  signal nxt_mux_sosi_arr        : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);

  signal dp_shiftram_src_out_arr : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);

  signal dp_shiftram_src_out_timestamped_arr     : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);
  signal nxt_dp_shiftram_src_out_timestamped_arr : t_dp_sosi_arr(0 to g_ai.nof_sp - 1);
begin
  -----------------------------------------------------------------------------
  -- Input samples from ADUH or WG
  -----------------------------------------------------------------------------
  no_adc : if g_use_phy.adc = 0 generate
    -- No ADU interface, so only use WG
    reg_adc_quad_miso <= c_mem_miso_rst;
    aduh_sosi_arr     <= (others => c_dp_sosi_rst);
  end generate;

  use_adc : if g_use_phy.adc /= 0 generate
    u_aduh_quad : entity aduh_lib.mms_aduh_quad
    generic map (
      -- General
      g_sim                => g_sim,
      g_cross_clock_domain => c_bn_capture_mm_cross_clock_domain,
      -- ADC Interface
      g_nof_dp_phs_clk     => g_nof_dp_phs_clk,
      g_ai                 => g_ai
    )
    port map (
      -- ADC Interface
      -- . ADU_AB
      ADC_BI_A         => ADC_BI_A,
      ADC_BI_B         => ADC_BI_B,
      ADC_BI_A_CLK     => ADC_BI_A_CLK,  -- lvds clock from ADU_AB
      ADC_BI_A_CLK_RST => ADC_BI_A_CLK_RST,  -- release synchronises ADU_AB DCLK divider

      -- . ADU_CD
      ADC_BI_C         => ADC_BI_C,
      ADC_BI_D         => ADC_BI_D,
      ADC_BI_D_CLK     => ADC_BI_D_CLK,  -- lvds clock from ADU_CD
      ADC_BI_D_CLK_RST => ADC_BI_D_CLK_RST,  -- release synchronises ADU_CD DCLK divider

      -- MM clock domain
      mm_rst           => mm_rst,
      mm_clk           => mm_clk,

      reg_mosi         => reg_adc_quad_mosi,
      reg_miso         => reg_adc_quad_miso,

      -- Streaming clock domain
      dp_rst           => dp_rst,
      dp_clk           => dp_clk,
      dp_phs_clk_vec   => dp_phs_clk_vec,

      -- . data
      aduh_sosi_arr    => aduh_sosi_arr
    );
  end generate;

  gen_wg : for I in 0 to g_ai.nof_sp - 1 generate
    u_sp : entity diag_lib.mms_diag_wg_wideband
    generic map (
      g_cross_clock_domain => c_bn_capture_mm_cross_clock_domain,
      g_buf_dir            => c_wg_buf_directory,

      -- Wideband parameters
      g_wideband_factor    => c_wideband_factor,

      -- Basic WG parameters, see diag_wg.vhd for their meaning
      g_buf_dat_w          => c_wg_buf_dat_w,
      g_buf_addr_w         => c_wg_buf_addr_w,
      g_calc_support       => true,
      g_calc_gain_w        => 1,
      g_calc_dat_w         => c_wg_buf_dat_w
    )
    port map (
      -- Memory-mapped clock domain
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,

      reg_mosi            => reg_wg_mosi_arr(I),
      reg_miso            => reg_wg_miso_arr(I),

      buf_mosi            => ram_wg_mosi_arr(I),
      buf_miso            => ram_wg_miso_arr(I),

      -- Streaming clock domain
      st_rst              => dp_rst,
      st_clk              => dp_clk,
      st_restart          => dp_bsn_trigger_wg,

      out_ovr             => wg_ovr( (I + 1) * c_wideband_factor               - 1 downto I * c_wideband_factor               ),
      out_dat             => wg_dat( (I + 1) * c_wideband_factor * c_wg_buf_dat_w - 1 downto I * c_wideband_factor * c_wg_buf_dat_w),
      out_val             => wg_val( (I + 1) * c_wideband_factor               - 1 downto I * c_wideband_factor               ),
      out_sync            => wg_sync((I + 1) * c_wideband_factor               - 1 downto I * c_wideband_factor               )
    );

    -- wires
    -- . all wideband samples will be valid in parallel, so using vector_or() or vector_and() is fine
    -- . if one of the wideband sample has overflow, then set the overflow error, so use vector_or()
    wg_sosi_arr(I).data  <= RESIZE_DP_SDATA(wg_dat( (I + 1) * c_wideband_factor * c_wg_buf_dat_w - 1 downto I * c_wideband_factor * c_wg_buf_dat_w));
    wg_sosi_arr(I).valid <=       vector_or(wg_val( (I + 1) * c_wideband_factor               - 1 downto I * c_wideband_factor               ));
    wg_sosi_arr(I).sync  <=       vector_or(wg_sync((I + 1) * c_wideband_factor               - 1 downto I * c_wideband_factor               ));
    wg_sosi_arr(I).err   <= TO_DP_ERROR(c_unb1_board_ok) when
                                  vector_or(wg_ovr( (I + 1) * c_wideband_factor               - 1 downto I * c_wideband_factor               )) = '0' else
                            TO_DP_ERROR(2**c_unb1_board_error_adc_bi);  -- pass ADC or WG overflow info on as an error signal
  end generate;

  -----------------------------------------------------------------------------
  -- WG / ADU mux
  -----------------------------------------------------------------------------
  gen_mux : for I in 0 to g_ai.nof_sp - 1 generate
    p_sosi : process(aduh_sosi_arr, wg_sosi_arr)
    begin
      -- Valid is forced to '1' here for dp_shiftram.
      nxt_mux_sosi_arr(I).valid <= '1';

      -- Default use the ADUH data
      nxt_mux_sosi_arr(I).data  <= aduh_sosi_arr(I).data;
      if wg_sosi_arr(I).valid = '1' then
        -- Valid WG data overrules ADUH data
        nxt_mux_sosi_arr(I).data <= wg_sosi_arr(I).data;
      end if;
    end process;
  end generate;

  p_reg_mux : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      mux_sosi_arr  <= (others => c_dp_sosi_rst);
    elsif rising_edge(dp_clk) then
      mux_sosi_arr  <= nxt_mux_sosi_arr;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Time delay: dp_shiftram
  -- . dp_shiftram uses DOWNTO range arrays internally vs. TO ranges used here.
  --   func_dp_stream_arr_reverse_range is used to reverse the ranges so
  --   the MM control stream indices are correct.
  -- . The 4 streams are treated individually by the shiftram. This is fine,
  --   because it leaves full control to the LCU via the MM interface.
  --   For Apertif typically the LCU will write the same delay setting to all
  --   4 streams and in fact to all 64 streams in the PAF.
  -- . The written delay setting must be applied synchronously at the sync, this
  --   is ensured by g_use_sync_in=TRUE and bs_sosi.sync.
  -----------------------------------------------------------------------------
  u_dp_shiftram : entity dp_lib.dp_shiftram
  generic map (
    g_nof_streams => g_ai.nof_sp,  -- 4 signal paths
    g_nof_words   => 2048,
    g_data_w      => c_wideband_factor * g_ai.port_w,  -- 4 concatenated timesamples
    g_use_sync_in => true
  )
  port map (
    dp_rst   => dp_rst,
    dp_clk   => dp_clk,

    mm_rst   => mm_rst,
    mm_clk   => mm_clk,

    sync_in  => bs_sosi.sync,

    reg_mosi => reg_dp_shiftram_mosi,
    reg_miso => reg_dp_shiftram_miso,

    snk_in_arr => func_dp_stream_arr_reverse_range(mux_sosi_arr),

    func_dp_stream_arr_reverse_range(src_out_arr) => dp_shiftram_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Timestamp
  -----------------------------------------------------------------------------
  u_bsn_sosi : entity dp_lib.mms_dp_bsn_source
  generic map (
    g_cross_clock_domain     => c_bn_capture_mm_cross_clock_domain,
    g_block_size             => c_bs_block_size,
    g_nof_block_per_sync     => c_bs_nof_block_per_sync,
    g_bsn_w                  => c_bs_bsn_w
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
    dp_pps            => dp_pps,

    -- Memory-mapped clock domain
    reg_mosi          => reg_bsn_source_mosi,
    reg_miso          => reg_bsn_source_miso,

    -- Streaming clock domain
    bs_sosi           => bs_sosi
  );

  u_bsn_trigger_wg : entity dp_lib.mms_dp_bsn_scheduler
  generic map (
    g_cross_clock_domain => c_bn_capture_mm_cross_clock_domain,
    g_bsn_w              => c_bs_bsn_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    reg_mosi    => reg_bsn_scheduler_wg_mosi,
    reg_miso    => reg_bsn_scheduler_wg_miso,

    -- Streaming clock domain
    dp_rst      => dp_rst,
    dp_clk      => dp_clk,

    snk_in      => bs_sosi,  -- only uses eop (= block sync), bsn[]
    trigger_out => dp_bsn_trigger_wg
  );

  gen_sosi_ctrl : for I in 0 to g_ai.nof_sp - 1 generate
    p_sosi : process(dp_shiftram_src_out_arr, bs_sosi)
    begin
      -- BS sets the sosi sync, bsn, valid, sop and eop for all signal paths
      nxt_dp_shiftram_src_out_timestamped_arr(I)       <= bs_sosi;
      nxt_dp_shiftram_src_out_timestamped_arr(I).data  <= dp_shiftram_src_out_arr(I).data;
    end process;
  end generate;

  p_reg_timestamps : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      dp_shiftram_src_out_timestamped_arr  <= (others => c_dp_sosi_rst);
    elsif rising_edge(dp_clk) then
      dp_shiftram_src_out_timestamped_arr  <= nxt_dp_shiftram_src_out_timestamped_arr;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Add SISO flow control to otherwise uninterruptible mux output
  -----------------------------------------------------------------------------
  gen_sp_siso_rdy : for I in 0 to g_ai.nof_sp - 1 generate
    u_dp_ready : entity dp_lib.dp_ready
     generic map (
      g_ready_latency => 1
      )
    port map (
      rst     => dp_rst,
      clk     => dp_clk,

      snk_in  => dp_shiftram_src_out_timestamped_arr(I),

      src_out => sp_sosi_arr(I),
      src_in  => sp_siso_arr(I)
      );
  end generate;

  -----------------------------------------------------------------------------
  -- Monitor ADU/WG output
  -----------------------------------------------------------------------------
  gen_mon : for I in 0 to g_ai.nof_sp - 1 generate
    u_sp : entity aduh_lib.mms_aduh_monitor
    generic map (
      g_cross_clock_domain   => c_bn_capture_mm_cross_clock_domain,
      g_symbol_w             => g_ai.port_w,
      g_nof_symbols_per_data => c_wideband_factor,  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
      g_nof_accumulations    => g_bn_capture.sp.nof_samples_per_sync,  -- integration time in symbols, defines internal accumulator widths
      g_buffer_nof_symbols   => g_bn_capture.sp.monitor_buffer_nof_samples,
      g_buffer_use_sync      => g_bn_capture.sp.monitor_buffer_use_sync
    )
    port map (
      -- Memory-mapped clock domain
      mm_rst         => mm_rst,
      mm_clk         => mm_clk,

      reg_mosi       => reg_mon_mosi_arr(I),  -- read only access to the signal path data mean sum and power sum registers
      reg_miso       => reg_mon_miso_arr(I),
      buf_mosi       => ram_mon_mosi_arr(I),  -- read and overwrite access to the signal path data buffers
      buf_miso       => ram_mon_miso_arr(I),

      -- Streaming clock domain
      st_rst         => dp_rst,
      st_clk         => dp_clk,

      in_sosi        => dp_shiftram_src_out_timestamped_arr(I)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Scope monitor for Wave Window
  -----------------------------------------------------------------------------
  u_quad_scope : entity aduh_lib.aduh_quad_scope
  generic map (
    g_sim  => g_sim,
    g_ai   => g_ai
  )
  port map (
    DCLK        => dp_clk,
    sp_sosi_arr => dp_shiftram_src_out_timestamped_arr
  );
end str;
