-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Provide 1G ethernet I/O interface (BSP) for OpenCL kernel on Arria10
-- Description:
-- . This core consists of glue logic between the OpenCL kernel IO channel and dp sosi/siso interface to ctrl_unb2b_board:
-- . Details:
--   . This core was developed for use on the Uniboard2b.
--   .
--   . The data field of the ST-avalon interface is also used to provide
--   . SOP, EOP and empty meta-data. The implementation of this is shown below.
--   +-----------+---------+--------------------------------------------------------+
--   | Bit range | Name    | Description                                            |
--   +-----------+---------+--------------------------------------------------------+
--   | [0:31]    | payload | Packet payload                                         |
--   +-----------+---------+--------------------------------------------------------+
--   | 32        | sop     | Start of packet signal                                 |
--   +-----------+---------+--------------------------------------------------------+
--   | 33        | eop     | End of packet signal                                   |
--   +-----------+---------+--------------------------------------------------------+
--   | [34:37]   | -       | reserved bits                                          |
--   +-----------+---------+--------------------------------------------------------+
--   | [38:39]   | empty   | On EOP, this field indicates how many bytes are unused |
--   +-----------+---------+--------------------------------------------------------+
library IEEE, common_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity ta2_unb2b_1GbE is
  port (
    st_clk             : in std_logic;
    st_rst             : in std_logic;

    -- eth1g UDP streaming ports
    udp_tx_sosi        : out  t_dp_sosi;
    udp_tx_siso        : in t_dp_siso;
    udp_rx_sosi        : in  t_dp_sosi;
    udp_rx_siso        : out t_dp_siso;

    kernel_clk         : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset       : in  std_logic;

    src_out            : out t_dp_sosi;
    src_in             : in  t_dp_siso;
    snk_out            : out t_dp_siso;
    snk_in             : in  t_dp_sosi
  );
end ta2_unb2b_1GbE;

architecture str of ta2_unb2b_1GbE is
  constant c_sim                           : boolean := false;
  constant c_empty_w                       : natural := 2;
  constant c_tx_fifo_size                  : natural := 10;  -- Can be small as flow control is enabled
  constant c_rx_fifo_size                  : natural := 10;  -- Can be small as flow control is enabled

  signal dp_latency_adapter_tx_src_out     : t_dp_sosi;
  signal dp_latency_adapter_tx_src_in      : t_dp_siso;

  signal dp_latency_adapter_tx_snk_in      : t_dp_sosi;
  signal dp_latency_adapter_tx_snk_out     : t_dp_siso;

  signal dp_fifo_dc_rx_src_out             : t_dp_sosi;
  signal dp_fifo_dc_rx_src_in              : t_dp_siso;

  signal dp_latency_adapter_rx_src_out     : t_dp_sosi;
  signal dp_latency_adapter_rx_src_in      : t_dp_siso;

  signal dp_xonoff_src_out                 : t_dp_sosi;
  signal dp_xonoff_src_in                  : t_dp_siso;
begin
-------------------------------------------------------
 -- Mapping Data from OpenCL kernel to 1GbE Interface --
  -------------------------------------------------------

  ----------------------------------------------------------------------------
  -- Data mapping
  ----------------------------------------------------------------------------
  -- Reverse byte order
  gen_tx_bytes: for I in 0 to 3 generate
    dp_latency_adapter_tx_snk_in.data(c_byte_w * (4 - I) - 1  downto c_byte_w * (3 - I)) <= snk_in.data(c_byte_w * (I + 1) - 1 downto c_byte_w * I);
  end generate;

  -- Assign correct data fields to control signals.
  dp_latency_adapter_tx_snk_in.sop <= snk_in.data(32);
  dp_latency_adapter_tx_snk_in.eop <= snk_in.data(33);
  dp_latency_adapter_tx_snk_in.empty(1 downto 0) <= snk_in.data(39 downto 38);

  dp_latency_adapter_tx_snk_in.valid <= snk_in.valid;
  snk_out.ready <= dp_latency_adapter_tx_snk_out.ready;  -- Flow control towards source (kernel)

  ----------------------------------------------------------------------------
  -- Latency adapter: adapt RL=0 (OpenCL kernel) to RL=1 (Downstream).
  ----------------------------------------------------------------------------
  u_dp_latency_adapter_tx : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency  => 0,
    g_out_latency => 1
  )
  port map (
    clk       => kernel_clk,
    rst       => kernel_reset,

    snk_in    => dp_latency_adapter_tx_snk_in,
    snk_out   => dp_latency_adapter_tx_snk_out,

    src_out   => dp_latency_adapter_tx_src_out,
    src_in    => dp_latency_adapter_tx_src_in
  );

  -----------------------------------------------------------------------------
  -- TX XON frame control
  -----------------------------------------------------------------------------

  u_dp_xonoff : entity dp_lib.dp_xonoff
  port map (
    rst      => kernel_reset,
    clk      => kernel_clk,

    in_siso  => dp_latency_adapter_tx_src_in,
    in_sosi  => dp_latency_adapter_tx_src_out,

    out_siso => dp_xonoff_src_in,
    out_sosi => dp_xonoff_src_out
  );

  -----------------------------------------------------------------------------
  -- TX dual clock FIFO
  -----------------------------------------------------------------------------

    u_dp_fifo_dc_tx : entity dp_lib.dp_fifo_dc
    generic map (
      g_technology  => c_tech_arria10_e1sg,
      g_data_w      => c_word_w,
      g_empty_w     => c_empty_w,
      g_use_empty   => true,
      g_fifo_size   => c_tx_fifo_size
    )
    port map (
      wr_rst      => kernel_reset,
      wr_clk      => kernel_clk,
      rd_rst      => st_rst,
      rd_clk      => st_clk,

      snk_out     => dp_xonoff_src_in,
      snk_in      => dp_xonoff_src_out,

      src_in      => udp_tx_siso,
      src_out     => udp_tx_sosi
    );

-------------------------------------------------------
 -- Mapping Data from 1GbE Interface to OpenCL kernel --
  -------------------------------------------------------
  -----------------------------------------------------------------------------
  -- TX dual clock FIFO
  -----------------------------------------------------------------------------

    u_dp_fifo_dc_rx : entity dp_lib.dp_fifo_dc
    generic map (
      g_technology  => c_tech_arria10_e1sg,
      g_data_w      => c_word_w,
      g_empty_w     => c_empty_w,
      g_use_empty   => true,
      g_fifo_size   => c_rx_fifo_size
    )
    port map (
      wr_rst      => st_rst,
      wr_clk      => st_clk,
      rd_rst      => kernel_reset,
      rd_clk      => kernel_clk,

      snk_out     => udp_rx_siso,
      snk_in      => udp_rx_sosi,

      src_in      => dp_fifo_dc_rx_src_in,
      src_out     => dp_fifo_dc_rx_src_out
    );

  ----------------------------------------------------------------------------
  -- Latency adapter: adapt RL=1 (Upstream) to RL=0 (OpenCL kernel).
  ----------------------------------------------------------------------------
  u_dp_latency_adapter_rx : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency  => 1,
    g_out_latency => 0
  )
  port map (
    clk       => kernel_clk,
    rst       => kernel_reset,

    snk_in    => dp_fifo_dc_rx_src_out,
    snk_out   => dp_fifo_dc_rx_src_in,

    src_out   => dp_latency_adapter_rx_src_out,
    src_in    => dp_latency_adapter_rx_src_in
  );

  ----------------------------------------------------------------------------
  -- Data mapping
  ----------------------------------------------------------------------------
  -- Reverse byte order
  gen_rx_bytes: for I in 0 to 3 generate
    src_out.data(c_byte_w * (4 - I) - 1  downto c_byte_w * (3 - I)) <= dp_latency_adapter_rx_src_out.data(c_byte_w * (I + 1) - 1 downto c_byte_w * I);
  end generate;

  -- Assign control signals to correct data fields.
  src_out.data(32) <= dp_latency_adapter_rx_src_out.sop;
  src_out.data(33) <= dp_latency_adapter_rx_src_out.eop;
  src_out.data(39 downto 38) <= dp_latency_adapter_rx_src_out.empty(1 downto 0);

  src_out.valid <= dp_latency_adapter_rx_src_out.valid;
  dp_latency_adapter_rx_src_in.ready <= src_in.ready;
  dp_latency_adapter_rx_src_in.xon <= '1';
end str;
