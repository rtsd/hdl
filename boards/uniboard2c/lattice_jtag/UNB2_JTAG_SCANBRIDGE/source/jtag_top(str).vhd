-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

architecture str of jtag_top is
    component bscan2 is
    -- enter the number of BSCAN2 blocks to create.  This is the only place that
    -- needs to be modified to control the number of local scan ports created.
      generic (
    bscan_ports     :     positive := 2

      );
      port (
        TDI, TCK, TMS   : in  std_logic;
        TRST            : in  std_logic;
        -- Turn on slow slew in fitter for output signals
        TDO             : out std_logic;
         -- OE control for MSP ports (Active high)
         ENABLE_MSP     : in  std_logic;
         MSPTCK         : out std_logic_vector(4 * bscan_ports - 1 downto 0);
         MSPTDI         : in  std_logic_vector(4 * bscan_ports - 1 downto 0);
         MSPTDO         : out std_logic_vector(4 * bscan_ports - 1 downto 0);
         MSPTMS         : out std_logic_vector(4 * bscan_ports - 1 downto 0);
         MSPTRST        : out std_logic_vector(4 * bscan_ports - 1 downto 0);
         -- one set of addresses to check for device
         IDN            : in std_logic_vector(3 downto 0)
      );
    end component bscan2;

-- internal enable signal for tri-stating the scanbridge
    constant jtag_chains    : natural := 5;
    signal ENABLE_SB        : std_logic;
    signal TDO_BSCAN        : std_logic;
    signal TDA              : std_logic;
    signal TDB              : std_logic;
    signal TDC              : std_logic;
    signal TDD              : std_logic;
    signal MSPTDO_BSCAN     : std_logic_vector(jtag_chains - 1 downto 0);
    signal MSPTCK_BSCAN     : std_logic_vector(jtag_chains - 1 downto 0);
    signal MSPTMS_BSCAN     : std_logic_vector(jtag_chains - 1 downto 0);
    signal MSPTRST_BSCAN    : std_logic_vector(jtag_chains - 1 downto 0);
    begin
      bscan : component bscan2
        port map (
           TDI                              => TDI,
           TCK                              => TCK,
           TMS                              => TMS,
           TRST                             => TRST,
           TDO                              => TDO_BSCAN,
           ENABLE_MSP                       => ENABLE_SB,
           MSPTCK(jtag_chains - 1 downto 0)   => MSPTCK_BSCAN,
           MSPTDI(jtag_chains - 1 downto 0)   => MSPTDI,
           MSPTDO(jtag_chains - 1 downto 0)   => MSPTDO_BSCAN,
           MSPTMS(jtag_chains - 1 downto 0)   => MSPTMS_BSCAN,
           MSPTRST(jtag_chains - 1 downto 0)  => MSPTRST_BSCAN,
           IDN                              => "0000"
        );

      p_jtagselect:  process(TDI, MSPTDI(jtag_chains - 1 downto 0), TCK, TMS, TRST)
      begin
          ENABLE_SB  <= '0';
          MSPTDO(jtag_chains - 1 downto 0) <= "ZZZZZ";
          MSPTCK(jtag_chains - 1 downto 0) <= "ZZZZZ";
          MSPTMS(jtag_chains - 1 downto 0) <= "ZZZZZ";
          MSPTRST(jtag_chains - 1 downto 0) <= "ZZZZZ";

          if CTRL(1) = '1' then
            ENABLE_SB  <= '1';
            MSPTDO     <= MSPTDO_BSCAN;
            TDO        <= TDO_BSCAN;
            MSPTCK     <= MSPTCK_BSCAN;
            MSPTMS     <= MSPTMS_BSCAN;
            MSPTRST    <= MSPTRST_BSCAN;
          else
            if LPSEL(0) = '0' then
              MSPTDO(0)  <= TDI;
              TDA        <= MSPTDI(0);
              MSPTCK(0)  <= TCK;
              MSPTMS(0)  <= TMS;
              MSPTRST(0) <= TRST;
            else
              TDA        <= TDI;
            end if;

            if LPSEL(1) = '0' then
              MSPTDO(1)  <= TDA;
              TDB        <= MSPTDI(1);
              MSPTCK(1)  <= TCK;
              MSPTMS(1)  <= TMS;
              MSPTRST(1) <= TRST;
            else
              TDB        <= TDA;
            end if;

            if LPSEL(2) = '0' then
              MSPTDO(2)  <= TDB;
              TDC        <= MSPTDI(2);
              MSPTCK(2)  <= TCK;
              MSPTMS(2)  <= TMS;
              MSPTRST(2) <= TRST;
            else
              TDC        <= TDB;
            end if;

            if LPSEL(3) = '0' then
              MSPTDO(3)  <= TDC;
              TDD        <= MSPTDI(3);
              MSPTCK(3)  <= TCK;
              MSPTMS(3)  <= TMS;
              MSPTRST(3) <= TRST;
            else
              TDD        <= TDC;
            end if;

            if LPSEL(4) = '0' then
              MSPTDO(4)  <= TDD;
              TDO        <= MSPTDI(4);
              MSPTCK(4)  <= TCK;
              MSPTMS(4)  <= TMS;
              MSPTRST(4) <= TRST;
            else
              TDO        <= TDD;
            end if;
         end if;
  end process;
end str;
