-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

-- Purpose: Transpose of in_data to out_data
-- Description:
--   The in_data is a concatenation of g_nof_data input streams, that are each
--   g_data_w bits wide. The data in each input stream consists of a
--   concatenation of g_nof_data symbols, that are g_data_w/g_nof_data bits
--   wide. The out_data contains the transpose of in_data. For example for
--   g_nof_data=2 this becomes:
--
--                          |- b[1,0] ---> 1[b,a] -|
--                          |          \/          |
--                          |          /\          |
--   in_data[b[1,0,a[1,0]] -+- a[1,0] ---> 0[b,a] -+-> out_data[1[b,a],0[b,a]]
--
--   Idem for g_nof_data=4:
--
--   in_data[d[3:0],c[3:0],b[3:0],a[3:0]] -> out_data[3[d:a],2[d:a],1[d:a],0[d:a]]
--
-- Remarks:
-- . The transpose assumes that the in_data is square, so
--   c_nof_symbols = g_nof_data

entity common_transpose_symbol is
  generic (
    g_pipeline  : natural := 0;
    g_nof_data  : natural := 4;
    g_data_w    : natural := 16  -- must be multiple of g_nof_data
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;

    in_data    : in  std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
    in_val     : in  std_logic := '0';
    in_sop     : in  std_logic := '0';
    in_eop     : in  std_logic := '0';

    out_data   : out std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
    out_val    : out std_logic;  -- pipelined in_val
    out_sop    : out std_logic;  -- pipelined in_sop
    out_eop    : out std_logic  -- pipelined in_eop
  );
end common_transpose_symbol;

architecture rtl of common_transpose_symbol is
  constant c_nof_symbols : natural := g_nof_data;
  constant c_symbol_w    : natural := g_data_w / c_nof_symbols;

  type t_symbol_arr  is array (integer range <>) of std_logic_vector(c_symbol_w - 1 downto 0);
  type t_symbol_2arr is array (integer range <>) of t_symbol_arr(c_nof_symbols - 1 downto 0);

  signal in_symbol_2arr    : t_symbol_2arr(g_nof_data - 1 downto 0);
  signal trans_symbol_2arr : t_symbol_2arr(g_nof_data - 1 downto 0);

  signal trans_data        : std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
begin
  no_transpose : if g_nof_data = 1 generate
    trans_data <= in_data;
  end generate;

  gen_transpose : if g_nof_data > 1 generate
    gen_data : for I in g_nof_data - 1 downto 0 generate
      gen_symbols : for J in c_nof_symbols - 1 downto 0 generate
        -- map input vector to 2arr
        in_symbol_2arr(I)(J) <= in_data((J + 1) * c_symbol_w + I * g_data_w - 1 downto J * c_symbol_w + I * g_data_w);

        -- transpose
        trans_symbol_2arr(J)(I) <= in_symbol_2arr(I)(J);

        -- map 2arr to output vector
        trans_data((J + 1) * c_symbol_w + I * g_data_w - 1 downto J * c_symbol_w + I * g_data_w) <= trans_symbol_2arr(I)(J);
      end generate;
    end generate;
  end generate;

  -- pipeline data output
  u_out_data : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline,
    g_in_dat_w  => g_nof_data * g_data_w,
    g_out_dat_w => g_nof_data * g_data_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => trans_data,
    out_dat => out_data
  );

  -- pipeline control output
  u_out_val : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_val,
    out_dat => out_val
  );

  u_out_sop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sop,
    out_dat => out_sop
  );

  u_out_eop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_eop,
    out_dat => out_eop
  );
end rtl;
