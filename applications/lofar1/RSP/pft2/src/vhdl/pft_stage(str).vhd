-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library common_lib, pft2_lib;
use common_lib.all;

architecture str of pft_stage is
  constant c_round_pipeline_in  : natural := 1;
  constant c_round_pipeline_out : natural := 1;
  constant c_round_pipeline     : natural := c_round_pipeline_in + c_round_pipeline_out;

  constant c_bf1_out_w   : natural := g_in_dat_w + 1;
  constant c_bf2_out_w   : natural := g_in_dat_w + 2;

  signal bf1_re    : std_logic_vector(c_bf1_out_w - 1 downto 0);
  signal bf1_im    : std_logic_vector(c_bf1_out_w - 1 downto 0);
  signal bf1_val   : std_logic;
  signal bf1_sync  : std_logic;

  signal bf2_re    : std_logic_vector(c_bf2_out_w - 1 downto 0);
  signal bf2_im    : std_logic_vector(c_bf2_out_w - 1 downto 0);
  signal bf2_val   : std_logic;
  signal bf2_sync  : std_logic;
begin
  gen_middle: if g_index > 0 generate
    bf1 : entity pft2_lib.pft_bf
    generic map (
      g_index          => 2 * g_index + 1,
      g_in_dat_w       => g_in_dat_w,
      g_out_dat_w      => c_bf1_out_w,
      g_bf_name        => "bf1"
    )
    port map (
      in_re            => in_re,
      in_im            => in_im,
      in_val           => in_val,
      in_sync          => in_sync,
      out_re           => bf1_re,
      out_im           => bf1_im,
      out_val          => bf1_val,
      out_sync         => bf1_sync,
      clk              => clk,
      rst              => rst
    );

    bf2 : entity pft2_lib.pft_bf
    generic map (
      g_index          => 2 * g_index,
      g_in_dat_w       => c_bf1_out_w,
      g_out_dat_w      => c_bf2_out_w,
      g_bf_name        => "bf2"
    )
    port map (
      in_re            => bf1_re,
      in_im            => bf1_im,
      in_val           => bf1_val,
      in_sync          => bf1_sync,
      out_re           => bf2_re,
      out_im           => bf2_im,
      out_val          => bf2_val,
      out_sync         => bf2_sync,
      clk              => clk,
      rst              => rst
    );

    tmult : entity pft2_lib.pft_tmult
    generic map (
      g_in_dat_w     => c_bf2_out_w,
      g_out_dat_w    => g_out_dat_w,
      g_index        => g_index
    )
    port map (
      in_re          => bf2_re,
      in_im          => bf2_im,
      in_val         => bf2_val,
      in_sync        => bf2_sync,
      out_re         => out_re,
      out_im         => out_im,
      out_val        => out_val,
      out_sync       => out_sync,
      clk            => clk,
      rst            => rst
    );
  end generate;

  gen_last: if g_index = 0 generate
    signal reg_val  : std_logic;
    signal reg_sync : std_logic;

  begin

    bf1_fw : entity pft2_lib.pft_bf_fw
    generic map (
      g_index          => 2 * g_index + 1,
      g_in_dat_w       => g_in_dat_w,
      g_out_dat_w      => c_bf1_out_w,
      g_bf_name        => "bf1"
    )
    port map (
      in_re            => in_re,
      in_im            => in_im,
      in_val           => in_val,
      in_sync          => in_sync,
      out_re           => bf1_re,
      out_im           => bf1_im,
      out_val          => bf1_val,
      out_sync         => bf1_sync,
      clk              => clk,
      rst              => rst
    );

    bf2_fw : entity pft2_lib.pft_bf_fw
    generic map (
      g_index          => 2 * g_index,
      g_in_dat_w       => c_bf1_out_w,
      g_out_dat_w      => c_bf2_out_w,
      g_bf_name        => "bf2"
    )
    port map (
      in_re            => bf1_re,
      in_im            => bf1_im,
      in_val           => bf1_val,
      in_sync          => bf1_sync,
      out_re           => bf2_re,
      out_im           => bf2_im,
      out_val          => bf2_val,
      out_sync         => bf2_sync,
      clk              => clk,
      rst              => rst
    );

    u_rnd : entity common_lib.common_complex_round
    generic map (
      g_representation  => "SIGNED",
      g_round           => true,
      g_round_clip      => false,
      g_pipeline_input  => c_round_pipeline_in,
      g_pipeline_output => c_round_pipeline_out,
      g_in_dat_w        => c_bf2_out_w,
      g_out_dat_w       => g_out_dat_w
    )
    port map (
      in_re          => bf2_re,
      in_im          => bf2_im,
      out_re         => out_re,
      out_im         => out_im,
      clk            => clk
    );

    p_regs: process(clk, rst)
    begin
      if rst = '1' then
        reg_val  <= '0';
        reg_sync <= '0';
        out_val  <= '0';
        out_sync <= '0';
      elsif rising_edge(clk) then
        out_val  <= reg_val;
        out_sync <= reg_sync;
        reg_val  <= bf2_val;
        reg_sync <= bf2_sync;
      end if;
    end process;

  end generate;
end str;
