--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author: J Hargreaves, E. Kooistra
-- Purpose: Wrapper for the Intel Arria 10 e1sg (unb2b, unb2c) tecnology version of the
--   JESD204b interface for ADCs and DACs
-- Description
--   Current configuration supports 12 channels receive only
--
-- . v2 uses FIFO in IP to get from rxlink_clk at 100 MHz to dp_clk at 200 MHz

library IEEE, common_lib, dp_lib, technology_lib, ip_arria10_e1sg_jesd204b_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.tech_jesd204b_component_pkg.all;

entity tech_jesd204b_arria10_e1sg_v2 is
  generic (
    g_sim                 : boolean := false;
    g_nof_streams         : natural := 12;
    g_nof_sync_n          : natural := 12;
    g_direction           : string  := "RX_ONLY";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_jesd_freq           : string  := "200MHz"
  );
  port (
    -- JESD204B external signals
    jesd204b_refclk       : in std_logic := '0';  -- Reference clock. For AD9683 use 200MHz direct from clock reference pin
    jesd204b_sysref       : in std_logic := '0';  -- SYSREF should drive ADC and FPGA with correct phase wrt jesd204b_device_clk
    jesd204b_sync_n_arr   : out std_logic_vector(g_nof_sync_n - 1 downto 0);  -- output to control ADC initialization/syncronization phase

    jesd204b_disable_arr  : in std_logic_vector(g_nof_streams - 1 downto 0);

    -- Data to fabric
    dp_clk                : in std_logic;
    dp_rst                : in std_logic;
    dp_sysref             : out std_logic;  -- = dp_sosi_arr(0).sync, all dp_sosi_arr().sync carry the dp_sysref
    dp_sosi_arr           : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);  -- Parallel data and sync to fabric

    -- MM Control
    mm_clk                : in  std_logic;
    mm_rst                : in  std_logic;

    jesd204b_mosi         : in  t_mem_mosi;  -- mm control
    jesd204b_miso         : out t_mem_miso;

    -- Serial connections to transceiver pins
    serial_tx_arr         : out std_logic_vector(g_nof_streams - 1 downto 0);  -- Not used for ADC
    serial_rx_arr         : in  std_logic_vector(g_nof_streams - 1 downto 0)
  );
end tech_jesd204b_arria10_e1sg_v2;

architecture str of tech_jesd204b_arria10_e1sg_v2 is
begin
  u_ip_arria10_e1sg_jesd204b_v2 : ip_arria10_e1sg_jesd204b_v2
  generic map(
    g_sim                => g_sim,
    g_nof_streams        => g_nof_streams,
    g_nof_sync_n         => g_nof_sync_n,
    g_direction          => g_direction,
    g_jesd_freq          => g_jesd_freq
  )
  port map(
    jesd204b_refclk      => jesd204b_refclk,
    jesd204b_sysref      => jesd204b_sysref,
    jesd204b_sync_n_arr  => jesd204b_sync_n_arr,

    jesd204b_disable_arr => jesd204b_disable_arr,

    dp_clk               => dp_clk,
    dp_rst               => dp_rst,
    dp_sysref            => dp_sysref,
    dp_sosi_arr          => dp_sosi_arr,

    -- MM
    mm_clk               => mm_clk,
    mm_rst               => mm_rst,

    jesd204b_mosi        => jesd204b_mosi,
    jesd204b_miso        => jesd204b_miso,

     -- Serial
    serial_tx_arr        => serial_tx_arr,
    serial_rx_arr        => serial_rx_arr
  );
end str;
