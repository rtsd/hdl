#include "sys/alt_irq.h"
//#include "osx_irq.h"

#include "osx_timer.h"
#include "altera_avalon_timer_regs.h"

#include "alt_types.h"
#include "unbosx_types.h"

volatile clock_t _osx_nticks = 0;

clock_t osx_clock( void ) {
	return _osx_nticks;
}
/* 
 * osx_timer_sc_irq() is the interrupt handler used for the system 
 * clock. This is called periodically when a timer interrupt occurs. The 
 * function first clears the interrupt condition, and then calls the 
 * alt_tick() function to notify the system that a timer tick has occurred.
 *
 * alt_tick() increments the system tick count, and updates any registered 
 * alarms, see alt_tick.c for further details.
 */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void osx_timer_sc_irq (void* base)
#else
static void osx_timer_sc_irq (void* base, alt_u32 id)
#endif
{
  /* clear the interrupt */
  IOWR_ALTERA_AVALON_TIMER_STATUS (base, 0);
  
  /* 
   * Dummy read to ensure IRQ is negated before the ISR returns.
   * The control register is read because reading the status
   * register has side-effects per the register map documentation.
   */
  IORD_ALTERA_AVALON_TIMER_CONTROL (base);

  _osx_nticks++;
}

/*
 * osx_timer_sc_init() is called to initialise the timer that will be 
 * used to provide the periodic system clock. This is called from the 
 * auto-generated alt_sys_init() function.
 */

void osx_timer_sc_init (void* base, alt_u32 irq_controller_id, 
                                alt_u32 irq, alt_u32 freq)
{
  /* set to free running mode */
  
  IOWR_ALTERA_AVALON_TIMER_CONTROL (base, 
            ALTERA_AVALON_TIMER_CONTROL_ITO_MSK  |
            ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
            ALTERA_AVALON_TIMER_CONTROL_START_MSK);

  /* register the interrupt handler, and enable the interrupt */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
  alt_ic_isr_register(irq_controller_id, irq, osx_timer_sc_irq, 
                      base, NULL);
#else
  alt_irq_register (irq, base, osx_timer_sc_irq);
#endif  
}
