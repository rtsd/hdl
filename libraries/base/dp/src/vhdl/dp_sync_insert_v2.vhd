-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose : * Insert extra sync pulses.
-- Description:
--   Every nof_blk_per_sync block a sync pulse is created at the output. The block
--   counter resets if a sync arrives at the input or when nof_blk_per_sync is reached.
--   nof_blk_per_sync is controllable using M&C.
--
-- Remarks:
-- . There is no support for back pressure.
-- . It does not compensate for missing data or extra data. There is NO reset function. It assumes that the
--   incoming data is perfectly aligned. Use a dp_sync_checker to assure the incoming data is perfect.
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_sync_insert_v2 is
  generic (
    g_nof_streams          : natural := 1;
    g_nof_blk_per_sync     : natural := 200000;
    g_nof_blk_per_sync_min : natural := 19530
  );
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;
    mm_clk     : in  std_logic;
    dp_rst     : in  std_logic;
    dp_clk     : in  std_logic;

    -- MM bus access in memory-mapped clock domain
    reg_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_miso   : out t_mem_miso := c_mem_miso_rst;

    -- Streaming sink
    in_sosi_arr : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
    -- Streaming source
    out_sosi_arr: out t_dp_sosi_arr(g_nof_streams - 1 downto 0)

  );
end dp_sync_insert_v2;

architecture rtl of dp_sync_insert_v2 is
  type t_reg is record  -- local registers
    blk_cnt          : natural range 0 to g_nof_blk_per_sync;
    nof_blk_per_sync : natural range 0 to g_nof_blk_per_sync;
    out_sosi_arr     : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  end record;

  constant c_reg_rst  : t_reg := ( 0, 0, (others => c_dp_sosi_rst));
  constant c_mm_reg_w : natural := ceil_log2(g_nof_blk_per_sync + 1);
  constant c_nof_blk_per_sync_mm_reg   : t_c_mem := (1, 1, c_mm_reg_w, 1, 'X');
  constant c_init_reg : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := TO_UVEC(g_nof_blk_per_sync, c_mem_reg_init_w);

  -- Define the local registers in t_reg record
  signal r         : t_reg;
  signal nxt_r     : t_reg;

  signal reg_nof_blk_per_sync : std_logic_vector(c_mm_reg_w - 1 downto 0);
begin
  out_sosi_arr <= r.out_sosi_arr;

  p_clk : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(dp_clk) then
      r <= nxt_r;
    end if;
  end process;

  p_comb : process(r, in_sosi_arr, reg_nof_blk_per_sync)
    variable v : t_reg;
  begin
    v := r;
    v.out_sosi_arr := in_sosi_arr;

    v.nof_blk_per_sync := TO_UINT(reg_nof_blk_per_sync);
    if TO_UINT(reg_nof_blk_per_sync) < g_nof_blk_per_sync_min then
      v.nof_blk_per_sync := g_nof_blk_per_sync_min;
    end if;

    if in_sosi_arr(0).sop = '1' then
      v.blk_cnt  := r.blk_cnt + 1;

      if r.blk_cnt = r.nof_blk_per_sync - 1 or in_sosi_arr(0).sync = '1' then
        v.blk_cnt := 0;
        for I in 0 to g_nof_streams - 1 loop
          v.out_sosi_arr(I).sync := '1';
        end loop;
      end if;
    end if;

    nxt_r <= v;
  end process;

  u_common_reg_r_w_dc : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => true,
    g_readback           => false,
    g_reg                => c_nof_blk_per_sync_mm_reg,
    g_init_reg           => c_init_reg
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in         => reg_mosi,
    sla_out        => reg_miso,

    -- MM registers in st_clk domain
    reg_wr_arr     => OPEN,
    reg_rd_arr     => OPEN,
    in_reg         => reg_nof_blk_per_sync,
    out_reg        => reg_nof_blk_per_sync
  );
end rtl;
