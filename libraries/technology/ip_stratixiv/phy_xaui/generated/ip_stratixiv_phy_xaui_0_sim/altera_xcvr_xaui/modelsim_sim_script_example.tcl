# (C) 2001-2012 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and other 
# software and tools, and its AMPP partner logic functions, and any output 
# files any of the foregoing (including device programming or simulation 
# files), and any associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License Subscription 
# Agreement, Altera MegaCore Function License Agreement, or other applicable 
# license agreement, including, without limitation, that your use is for the 
# sole purpose of programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the applicable 
# agreement for further details.


###########################################
#
# This is an example file for compilation/simulation of 10GBASER-R PHY instance in Modelsim 
#
# You can modify this script and use it to compile/simulate your design depending on  
# the following Modelsim license options:
# (1) Mixed language license 
#    - Top-level PHY IP variant can be in Verilog or VHDL
#    - Underlying PHY IP files are in plaintext Verilog 
#    - Testbench can be in Verilog or VHDL
#      
# (2) Non-mixed language license   
#     (a) Verilog Only:
#        - Top-level PHY IP variant should be in Verilog
#        - Underlying PHY IP files are in plaintext Verilog
#        - Testbench should be in Verilog
#
#     (b) VHDL Only:
#        - Top-level PHY IP variant should be in VHDL
#        - Underlying PHY IP files are in encrypted Verilog
#        - Testbench should be in VHDL
#
# (3) Quartus encrypted some files for copyright. Those file will be encrypted for different simulator
# 
# Please do the following changes in this script in order to use it:
#  - Set the language (verilog or vhdl) of the generated PHY IP variant  
#  - Set your top-level PHY IP variant name
#  - Set your top-level testbench name
#  - Compile your other design files and your testbench. You would use vlog or vcom according 
#    to the language of your design files/testbench and your Modelsim license. 
# 
# Note that the PHY IP files are correctly compiled in this script according to the the 
# "language" variable you set.
#
##############################################################################################

set QUARTUS_ROOTDIR $env(QUARTUS_ROOTDIR)
#################################################################################
##
## Set your language, simulator and top level design name here
## e.g. vsim -c -do "do ./test_sim/modelsim_example_script.tcl verilog stratixiv modelsim test test_tst hard"
## 
#################################################################################
# language = verilog (verilog variant of the PHY IP) or vhdl (vhdl variant of the PHY IP)
# defaulted to verilog
set language $1
if {$language != "verilog" && $language != "vhdl"} {
	echo "invalid setting for language: $language. valid settings are verilog and vhdl"
	exit
}

# family = 
# defaulted to stratixv
set family $2
if {$family != "stratixiv" && $family != "stratixv" && $family != "cycloneiv" && $family != "arriaiigx" && $family != "arriaiigz" && $family != "hardcopyiv"} {
	echo "invalid setting for family: $family. valid settings are stratixiv,stratixv,cycloneiv,arriaiigx,arriaiigz,hardcopyiv"
	exit
}

# simulator = modelsim or VCS
# defaulted to modelsim
# This file not support VCS yet, you can use this file as reference for VCS
set simulator $3
if {$simulator != "modelsim"} {
	echo "invalid setting for simulator: $simulator. valid settings are modelsim"
	exit
}
## Set your top level design name here
##set dut_name <top level design name as generted in Quartus or Qsys>
set dut_name $4
if {$dut_name == ""} {
	echo "Missing top level design name: $dut_name"
}
## Set your test bench name here
##set tb_name <test bench>
set tb_name $5
if {$tb_name == ""} {
	echo "Missing test bench name: $tb_name"
}

## Set your XAUI type
##set xaui_type soft or hard
set xaui_type $6
if {$xaui_type == ""} {
	echo "Missing test xaui_type: $xaui_type"
}

puts " simulator=$simulator"
puts " language=$language"
puts " family=$family"
#################################################################################
## Set directory path according to simulator
## unenc_file_dir for Quartus unecrypted files
## enc_file_dir is for Quartus ecrypted files
## This file only support modelsim simulation. You can use the following path
## as reference of other simulator
#################################################################################
## Modelsim Verilog/Modelsim VHDL with Mixed language license 
if { $simulator == "modelsim" } {
	set enc_file_dir ./${dut_name}_sim/altera_xcvr_xaui/mentor  
	if {$language == "verilog"} {
    		set unenc_file_dir ./${dut_name}_sim/altera_xcvr_xaui 
	} else {
		## Modelsim-AE / Models
		set unenc_file_dir ./${dut_name}_sim/altera_xcvr_xaui/mentor
	}
	
} elseif { $simulator == "VCS" } {
		set unenc_file_dir ${dut_name}_sim 
		set enc_file_dir $dut_name_sim/synopsys   

} elseif { $simulator == "Aldec" } {
		set unenc_file_dir ${dut_name}_sim 
		set enc_file_dir $dut_name_sim/Aldec   
		
} elseif { $simulator == "VCS" } {
		set unenc_file_dir ./${dut_name}_sim 
		set enc_file_dir ./${dut_name}_sim/synopsys   

} elseif { $simulator == "NCSIM" } {
		set unenc_file_dir ./${dut_name}_sim
		set enc_file_dir ./${dut_name}_sim/cadence
		 
}

exec rm -rf work
vlib work
puts " unenc_file_dir=$unenc_file_dir"
puts " enc_file_dir=$enc_file_dir"

###########################################
# Library files 
###########################################
if {$language == "verilog"} {
       vlog $QUARTUS_ROOTDIR/eda/sim_lib/220model.v
       vlog $QUARTUS_ROOTDIR/eda/sim_lib/altera_primitives.v
       vlog $QUARTUS_ROOTDIR/eda/sim_lib/sgate.v
       vlog $QUARTUS_ROOTDIR/eda/sim_lib/altera_mf.v
       vlog $QUARTUS_ROOTDIR/eda/sim_lib/stratixiv_hssi_atoms.v
       vlog $QUARTUS_ROOTDIR/eda/sim_lib/stratixii_atoms.v

       if {$family == "stratixiv" || $family == "arriaiigx" || $family == "arriaiigz" || $family == "hardcopyiv"} {
	  ###########################################
	  # Stratix IV library files 
	  ###########################################
	  vlog $QUARTUS_ROOTDIR/eda/sim_lib/stratixiv_hssi_atoms.v
          vlog $QUARTUS_ROOTDIR/eda/sim_lib/stratixiv_atoms.v
          vlog $QUARTUS_ROOTDIR/libraries/megafunctions/alt_cal_c3gxb.v
	} 
	if {$family == "cycloneiv"} {
	  ###########################################
	  # Cyclone IV library files 
	  ###########################################
	  vlog $QUARTUS_ROOTDIR/eda/sim_lib/cycloneiv_hssi_atoms.v
          vlog $QUARTUS_ROOTDIR/eda/sim_lib/cycloneiv_atoms.v
          vlog $QUARTUS_ROOTDIR/libraries/megafunctions/alt_cal_c3gxb.v
	} 
	if {$family == "stratixv"} {
	  ###########################################
	  # Stratix V library files 
	  ###########################################
	vlog -sv $QUARTUS_ROOTDIR/eda/sim_lib/altera_lnsim.sv
	vlog $QUARTUS_ROOTDIR/eda/sim_lib/mentor/stratixv_atoms_ncrypt.v
	vlog $QUARTUS_ROOTDIR/eda/sim_lib/stratixv_atoms.v
	vlog $QUARTUS_ROOTDIR/eda/sim_lib/mentor/stratixv_hssi_atoms_ncrypt.v
	vlog $QUARTUS_ROOTDIR/eda/sim_lib/stratixv_hssi_atoms.v
        }
} else {
	vmap altera work
	vmap altera_mf work
	vmap lpm work
	vmap sgate work
	vmap stratixiv_hssi work
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/220pack.vhd
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/220model.vhd
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/altera_primitives_components.vhd
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/altera_primitives.vhd
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/sgate_pack.vhd
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/sgate.vhd
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/altera_mf_components.vhd
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/altera_mf.vhd
        vcom $QUARTUS_ROOTDIR/eda/sim_lib/stratixii_atoms.vhd
  
       if {$family == "stratixiv" || $family == "arriaiigx" || $family == "arriaiigz"} {
	  ###########################################
	  # Stratix IV library files 
	  ###########################################
          vcom $QUARTUS_ROOTDIR/eda/sim_lib/stratixiv_hssi_components.vhd
          vcom $QUARTUS_ROOTDIR/eda/sim_lib/stratixiv_hssi_atoms.vhd
          vcom $QUARTUS_ROOTDIR/libraries/megafunctions/alt_cal_c3gxb.vhd
	} 
	if {$family == "cycloneiv"} {
	  ###########################################
	  # Cyclone IV library files 
	  ###########################################
          vcom $QUARTUS_ROOTDIR/eda/sim_lib/cycloneiv_hssi_components.vhd
          vcom $QUARTUS_ROOTDIR/eda/sim_lib/cycloneiv_hssi_atoms.vhd
          vcom $QUARTUS_ROOTDIR/libraries/megafunctions/alt_cal_c3gxb.vhd
	 } 
	 if {$family == "stratixv"} {
	  ###########################################
	  # Stratix V library files 
	  ###########################################
	  vlog $QUARTUS_ROOTDIR/eda/sim_lib/mentor/altera_lnsim_for_vhdl.sv
	  vlog $QUARTUS_ROOTDIR/eda/sim_lib/mentor/stratixv_atoms_for_vhdl.v
	  vlog $QUARTUS_ROOTDIR/eda/sim_lib/mentor/stratixv_hssi_atoms_for_vhdl.v
         }
}

if {$xaui_type == "hard" } {
  vlog -sv $unenc_file_dir/alt_xcvr_csr_common_h.sv
  vlog -sv $unenc_file_dir/alt_xcvr_csr_common.sv
  vlog $unenc_file_dir/alt_dprio.v
  vlog $unenc_file_dir/alt_mutex_acq.v
  vlog $unenc_file_dir/alt_pma_ch_controller_tgx.v
  vlog $unenc_file_dir/alt_pma_controller_tgx.v
  vlog -sv $unenc_file_dir/altera_xcvr_functions.sv
  vlog -sv $unenc_file_dir/alt_reset_ctrl_lego.sv
  vlog -sv $unenc_file_dir/alt_reset_ctrl_tgx_cdrauto.sv
  vlog -sv $unenc_file_dir/alt_xcvr_resync.sv
  vlog -sv $unenc_file_dir/altera_xcvr_xaui.sv
  vlog -sv $unenc_file_dir/alt_xcvr_arbiter.sv
  vlog -sv $unenc_file_dir/alt_xcvr_m2s.sv
  vlog -sv $unenc_file_dir/alt_xcvr_mgmt2dec_phyreconfig.sv
  vlog -sv $unenc_file_dir/alt_xcvr_mgmt2dec_xaui.sv
  vlog -sv $unenc_file_dir/alt_xcvr_csr_pcs8g_h.sv
  vlog -sv $unenc_file_dir/alt_xcvr_csr_pcs8g.sv
  vlog -sv $unenc_file_dir/alt_xcvr_csr_selector.sv
  vlog $unenc_file_dir/altera_wait_generate.v
  vlog $unenc_file_dir/hxaui.v
  vlog $unenc_file_dir/hxaui_csr_h.sv
  vlog +incdir+$unenc_file_dir $unenc_file_dir/hxaui_csr.sv
  if {$family == "stratixiv" || $family == "arriaiigx" || $family == "arriaiigz" || $family == "hardcopyiv"} {
          ###########################################
          # Use this section for Stratix IV/Arria II GX Hard XAUI
          ###########################################
          vlog -sv $unenc_file_dir/siv_xcvr_xaui.sv
          vlog $unenc_file_dir/alt_dprio.v
          vlog $unenc_file_dir/alt_xcvr_reconfig*.v
           vlog -sv $unenc_file_dir/alt_xcvr_reconfig*.sv
           vlog $unenc_file_dir/hxaui_alt4gxb.v
    } 
    if {$family == "cycloneiv"} {
          ###########################################
          # Use this section for Cyclone IV GX Hard XAUI
          ###########################################
          vlog $unenc_file_dir/civ_xcvr_xaui.v
          vlog $unenc_file_dir/alt_xcvr_reconfig*.v
          vlog -sv $unenc_file_dir/alt_xcvr_reconfig*.sv
          vlog $unenc_file_dir/hxaui_alt_c3gxb.v
      }
 } else {         
      vlog -sv $unenc_file_dir/alt_xcvr_csr_common_h.sv
      vlog -sv $unenc_file_dir/alt_xcvr_csr_common.sv
      vlog -sv $unenc_file_dir/alt_xcvr_csr_pcs8g_h.sv
      vlog -sv $unenc_file_dir/alt_xcvr_csr_pcs8g.sv
      vlog -sv $unenc_file_dir/alt_xcvr_csr_selector.sv
      vlog -sv $unenc_file_dir/alt_xcvr_m2s.sv
      vlog -sv $unenc_file_dir/alt_xcvr_mgmt2dec_phyreconfig.sv
      vlog -sv $unenc_file_dir/alt_xcvr_mgmt2dec_xaui.sv
      vlog -sv $unenc_file_dir/alt_xcvr_arbiter.sv
      vlog -sv $unenc_file_dir/altera_xcvr_functions.sv
      vlog $enc_file_dir/alt_soft_xaui_pcs.v
      vlog $enc_file_dir/alt_soft_xaui_reset.v
      vlog $enc_file_dir/alt_soft_xaui_rx.v
      vlog $enc_file_dir/alt_soft_xaui_rx_8b10b_dec.v
      vlog $enc_file_dir/alt_soft_xaui_rx_channel_synch.v
      vlog $enc_file_dir/alt_soft_xaui_rx_deskew.v
      vlog $enc_file_dir/alt_soft_xaui_rx_deskew_channel.v
      vlog $enc_file_dir/alt_soft_xaui_rx_deskew_ram.v
      vlog $enc_file_dir/alt_soft_xaui_rx_invalid_code_det.v
      vlog $enc_file_dir/alt_soft_xaui_rx_parity.v
      vlog $enc_file_dir/alt_soft_xaui_rx_parity_4b.v
      vlog $enc_file_dir/alt_soft_xaui_rx_parity_6b.v
      vlog $enc_file_dir/alt_soft_xaui_rx_rate_match.v
      vlog $enc_file_dir/alt_soft_xaui_rx_rate_match_ram.v
      vlog $enc_file_dir/alt_soft_xaui_rx_rl_chk_6g.v
      vlog $enc_file_dir/alt_soft_xaui_rx_sm.v
      vlog $enc_file_dir/alt_soft_xaui_tx.v
      vlog $enc_file_dir/alt_soft_xaui_tx_8b10b_enc.v
      vlog $enc_file_dir/alt_soft_xaui_tx_idle_conv.v
      vlog $enc_file_dir/l_modules.v
      vlog $unenc_file_dir/hxaui_csr_h.sv
      vlog +incdir+$unenc_file_dir $unenc_file_dir/hxaui_csr.sv
      vlog $unenc_file_dir/sxaui.v
      vlog -sv $unenc_file_dir/alt_reset_ctrl_lego.sv
      vlog $unenc_file_dir/altera_wait_generate.v
      vlog -sv $unenc_file_dir/altera_xcvr_xaui.sv
      vlog -sv $unenc_file_dir/alt_reset_ctrl_tgx_cdrauto.sv
      vlog -sv $unenc_file_dir/alt_xcvr_resync.sv
      if {$family == "stratixiv" } {
          ###########################################
          # Use this section for Stratix IV Soft XAUI
          ###########################################
          vlog -sv $unenc_file_dir/siv_xcvr_xaui.sv
          vlog $unenc_file_dir/alt4gxb_vo.v
          vlog $unenc_file_dir/alt_mutex_acq.v
          vlog $unenc_file_dir/alt_pma_ch_controller_tgx.v
          vlog $unenc_file_dir/alt_pma_controller_tgx.v
          vlog -sv $unenc_file_dir/alt_pma_functions.sv
          vlog -sv $unenc_file_dir/siv_xcvr_low_latency_phy_nr.sv
          vlog $unenc_file_dir/alt_dprio.v
          vlog $unenc_file_dir/alt_xcvr_reconfig*.v
          vlog -sv $unenc_file_dir/alt_xcvr_reconfig*.sv
    } else {
          
           ###########################################
           # Use this section for Stratix V Soft XAUI
           ###########################################
		   
           vlog -sv $unenc_file_dir/sv_xcvr_h.sv
           vlog -sv $unenc_file_dir/sv_xcvr_xaui.sv
           vlog -sv $unenc_file_dir/sv_xcvr_plls.sv
           vlog $unenc_file_dir/alt_pma_ch_controller_tgx.v
           vlog $unenc_file_dir/alt_pma_controller_tgx.v
           vlog -sv $unenc_file_dir/alt_pma_functions.sv
           vlog -sv $unenc_file_dir/sv_xcvr_low_latency_phy_nr.sv
           vlog -sv $unenc_file_dir/sv_xcvr_avmm.sv
           vlog -sv $unenc_file_dir/sv_xcvr_avmm_csr.sv
           vlog -sv $unenc_file_dir/sv_xcvr_avmm_dcd.sv
           vlog -sv $unenc_file_dir/sv_xcvr_custom_native.sv
           vlog -sv $unenc_file_dir/sv_xcvr_data_adapter.sv
           vlog -sv $unenc_file_dir/sv_xcvr_native.sv
           vlog -sv $unenc_file_dir/sv_reconfig_bundle_to_xcvr.sv
           vlog -sv $unenc_file_dir/sv_reconfig_bundle_merger.sv
           vlog -sv $unenc_file_dir/sv_reconfig_bundle_to_ip.sv
           vlog -sv $unenc_file_dir/sv_pcs.sv
           vlog -sv $unenc_file_dir/sv_pcs_ch.sv
           vlog -sv $unenc_file_dir/sv_pma.sv
           vlog -sv $unenc_file_dir/sv_rx_pma.sv
           vlog -sv $unenc_file_dir/sv_tx_pma.sv
           vlog -sv $unenc_file_dir/sv_tx_pma_ch.sv
           vlog -sv $unenc_file_dir/sv_pcs_ch.sv
           vlog -sv $unenc_file_dir/sv_pcs_ch.sv
           vlog -sv $unenc_file_dir/stratixv_hssi_8g_rx_pcs_rbc.sv
           vlog -sv $unenc_file_dir/stratixv_hssi_8g_tx_pcs_rbc.sv
           vlog -sv $unenc_file_dir/stratixv_hssi_common_pcs_pma_interface_rbc.sv
           vlog -sv $unenc_file_dir/stratixv_hssi_common_pld_pcs_interface_rbc.sv
           vlog -sv $unenc_file_dir/stratixv_hssi_rx_pcs_pma_interface_rbc.sv
           vlog -sv $unenc_file_dir/stratixv_hssi_rx_pld_pcs_interface_rbc.sv
           vlog -sv $unenc_file_dir/stratixv_hssi_tx_pcs_pma_interface_rbc.sv
           vlog -sv $unenc_file_dir/stratixv_hssi_tx_pld_pcs_interface_rbc.sv
      }
 }

 
#################################################################
# Add your custom library compilation here and testbench
#################################################################
set custom_compilation custom_lib.tcl
if {[file exists $custom_compilation]} {
	source $custom_compilation
}

###########################################
# Add your test bench here
###########################################
vlog ${dut_name}.v
vsim -c -t 1ps $tb_name -novopt


###########################################
# Run and Quit your simulator
###########################################
run -all
quit -sim
exit
