// Copyright (C) Altera Corporation. All rights reserved. 
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 11.1sp2 linux32 Build 259 01/26/2012
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6c"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
kzr+GHJF6c8r2Ktqgeo7jbHeqse+1y5i2fQWwEb1cSP+08nAoqAy6OVmlxJGDc1r
mLOImcyfb3bMjBHbWqCUhgC7ivwxiUDQ/YrqbTCJ9zQxKz5T0/qQAzZ4v17YY2zM
00Re0NHMdK06PlP+94zS3uLQr1Ofr0PoWzK8LhRxtCc=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3056)
SzCAkWI7b3ZFTGmksrDtDQ0wLcju7q2fp2/Mw6CG3DZMIshQRZkZ+cM8cn3Z/igW
s10cOWQIqgS+PfM6Tnrnye/oYaD9suyxCDti4te871zOnJx5lbbT6hQQBqARebve
eeIM8kHim6fee8vz0Rw5oHwLTlOs3TIbVUQFw3M2Uq22fP6OggHP2kqXGjFg6gcN
ZWeBV0Zodf+26IJEfkUjURTmt0AV5wXeOGXMjXXr7ANqlUIz9E7fhU4bPfmDCCwE
PPXdEWT99eFuj3wsDZf6BhJL5Kjr7unnrLVPlx4rOcDSkE/7PbKRpH7NJeCTuSuR
jeBmE/7A6OQ4Q59Xq1t3aCipr3BUgLC3swW9uyFzVDLvLS0YSf9xJbCb/FDL9cAQ
7g13ZX2dElSpo4ZS0cf3MttEiw/59x3aSWC5b5vIkrd4FCSEvR2+8lGHsdDJpmYR
X/pghfNVuftKksS7OCpKqAQJphBT1QrBwvZldbuvA7qw2Sn3qnKAIOntAV8giU02
j1+B+fyA9C6v0rH7ijX6I1uRAHIIE30mBdqrbmb3DFQRKk9kLkvXAbidK5j74GVV
BjGg59htMFRp6Khi7uI5rzTx0loqH9/96LUi8C/x/W1SXxL9Y/9D3r71JPL4Hp/J
uODAg9baPdSLHEAI7Td4d0YzUlM9FyXdNB7x0WvkUq4hlpQEXunT7b+5M89RPs8H
xCxA0Dh5mfNH+suwNCsx38uRehLk7fNYRlqc1lGoqoYhbnbd3cNZaF8xGy5SOzxE
Nr+OSOVPKhLqOM3UTBrrqk57VaLyadKfFA2gaIPuhlHI290tYrfLkIcK11kAHMle
zi1CPuGjOmfAkLwHOD+3s5fC9xUW4GN8rkkWrwQlVsJM83YgGlWiHFJZob4Khe1D
4M6ZU8DBtkR5yXhoDKcVd1UR+QTKYM0kO5HPhQNLYyNRplIcnDVv4LHyofWV/Rx1
VuFglabCoPLsYmZ17/orKBKk1ww+ENYwkW6NxWgelPnU7v/JA0IRgPT9Br1BB2K0
16AUPG8a4MlngcNrk+5KxSapTW1kHUvWPgzYdreYX7jvZ9Idjci80UCHrAcui1kN
lS1w+O/eVyt06KWrb3L17CdQY2y8F9ghaRI74RlBTgX8RArgTs9SBM2rjLkRYo9u
MjLre47nV1gn78Y6H+bn5PXh6SkJ+2QVXeCMcPwggY+Xtb2dU6aDnDiqipt8Y4c7
edtOlUZZJIGiC5jtAvbISFx7jjZjSiYqBCoVHESP+q9zENfDWTUJE/xnxL+tgGUW
LCqw/RZ0Q3QyX9iGZ5nNGL4DhKBNPg0BOdjB0xE9qhAmj7BvROJgdKDUAcS7+yAQ
Xcy2UYH0OcIMFY0HED7xc6AVOj5K3WRhc6DUoKfWAywavnMejOrBbCxf8xlFQWeL
Z9T7IIDfxd4h0cAXnrhAqEip5rl9bazj9jqpQ4g8r4Ah0DPLDqep9F8GVEHZ+3ct
6Wq4Dn/zwftSlBFrohTXbgRTjy1pPnCQdcZj01f3E2VEQFWe+nI6ao/Ca0vcxZyH
h8VG+hPmKqaHloXJJo7a7ChcYDRN1E/Dzc3lG8mbdSZ/nE/PVBcK/VXSTK33wTF6
oIc3jKbuZ/isOPlmOwyWHOTVdcjyMiUXn7p4rmUiS5xrlZ6Jm3RvG9D5sPUAQnpL
MwHgFtFFXp//5GKEYQPoTtTrD/E/yTOuXKSggGgycGJgM+1xX9ILTpkWeMPVZR1W
mbNYr5EMF96x8clGD+1+m6o+8B4sX8Hw4E1j9QGesnXSdmem+/0LpuIZSEJ0VW7n
eg+oDQKvxuBltUC7AdKTSeybELqkw64Ci9I6gFi14GWmM0LgNFr0eiPNt/UKsA3l
mxu5553zRCDz6RLKePITEYWxjMnHyDTa0RV2i+B9/4Vsj0nVGa/pzZdCOZtYhxin
spD6RBbiJFQ+HAGJKEsTELA3r53Y1lH1osGcXwZhkR4Rk1a1/EW+tdP4DCvllDvk
xPA3JRiWQU87LqM71rXDpf+DdsRLWB7gC06/lAcdcUkUeJ5k5U/h2W+ah2WKUF7U
ZGXv7R638d4iyvND/RRRQjekklGqyv/yN1cC187Z1Kz1C4p1PkQheLWOWw7f2GpR
W2P4OyhxfHHQhNFo7XuNzmtHqNmNU4vXWCRMENOceH6+YT2oub7xMmO15MqiQhnb
WVatkjQY0w4tHhnNGD8vN7kkwCFm+DGFe6zTWuAkNpLbaCSojzAnHyxmOFPfV4WF
OTThh3alLzMw7L2x1FzBTWVppcYyTkxwhE8nu3JdmjJoMjk7RCChxOKWVCrKYa51
761BQa3rnlNIY1Jug39lXzE85yBsOgr6KS6RCfwlKK2G4RLY1InYT8zdDjONc3fa
VqAGUQmpB2WVoEwPrSRHOJ0++iR7EgfMWXHH+tjN+KfK/yOzmSTi8+DPzDC1c/3P
QIwQgW1n8mgo2pPEr4WkoTSenpq5C4BiPBIM1g1uc5cXlIjYjQYegSJAfYMsBZes
Bl9YFbrPqokNIQR29md4CFHchwtNU3hLBwc01Y1G50O7ipYdL9f3hq1H7FbMZIiB
y0Pnek/n9/qLthvnABcQBXmNZ3+9xX6WfbGa9a535ov2K8HwCko8/pjrvfXXBHJE
8x2r9JiBSGeaaoVtif8JD9iGMht0shcihPQbsvXsdrfBaecnzN0HWRaaSNIn4ZR+
FHNGJICJ8ujiARUMTR5oQCSZRNF/21O7UTavMwRgggGAQ3MK6YGwkMHH6W0tPWA4
60FJTFMSW9/+6v7HcvDVbWtbwshbMXRkbOMUsykNWIQ96EvZN/u7TV+flHYqRpaK
WAIQh79XmZTE2qecuXsv0GRfsBaO135rlFu55y6rWI1b2lJZIAKb/tmArR1cP8lo
1Zu50M3R9ANsgA01zNzLDP1rZZH0aqusgoLtTvMPVNxUCeHjrT7WLltOwhpqyd67
hMlW9Ccbpx2bAUNVUdaAFuBS1/KW1Z8ZPVU/A0BG/Vskm4gOHxsQ9WEZEiNTFHpl
TGHltVIlDdhwVQ84WbTK7okpu0oF5kTwJ/7+e3pQtn/8vbSfS2mBgwk6o5nj0QwG
Nvj2SNr5WIFKvxx4bY5TwI2qxYC3cSQoLQnJwvkGPdETqEuIaAnr4myi1fn2s1Lo
zq3rXQ0jrbJ8EXjs/cdYLLT+EA54+xqDWY4y79S0KNUIB+XLy1BitG17a6EHSSX4
UYnr+K/GLh31VXe87/pSr1zLT43/86ogA1aOBQmGlbfxs4QLESHz8Q8laqIMF+M6
WxrSiEq1SqPC36s1aCx5oZT/sIQ08uoDWnsWvT88sDEKVpnjphqnEs1j6J9pXrlD
iccpG3UBSvztTX0Wt6wn2fKZPcSLyrbByp6n3T6FLaM3b1t5ULzBS2KKQCZj4137
refPry9SBiq1VOhL3r9JnXawcfQvAEQnkBKEDWGJhP7uQ19UkrJ2KD+9rlUJjUYO
V9juj/7CCcDmTuBaJLFc9YwWHLazJwp3IQi5K5ip5tBsmTj73OBNIHRo9Qcy1Fnn
6Yej3f9xyU+Hxrce8yk20+0dsE9MAsp9CwSZIWtd98QHLUWD8+6kcd9J3P52dtHJ
V2e7Lwq772ygh5ex6Cui351FNU/vu3gQbQfc2DkBZjyeHD7Q1OOnpYY76NNxnGxy
yMqElmeZ0wpfFa8tvOk2JOJhWlRG0OfaK+3o+r0FFU+9K/06+VYlmtNmFhhZHt4/
atYljYrOBIe4PeanipfMDChV+EW1ozutF9zFBXNEIS3tJ9UB0vDhcTzXoUK6PQxP
7UAEFSljkY0zst43+bG0PekuMwzTU6F1eUcN4l3FiKPZ9h267G8EiT7/M5YFyBNu
yKl3jlJE3nYJyeIkiy0QRuh2o4nlOBet3ec/xGyJopbXfHHkd7Dnz7rwnW6wZI7K
r7jXx6f7PjYXj+eaZEGntvBcvAkRrWb3zuSICQ9ackcsMsH7O3baTXPJ4idKigMx
pMGzhtM0UeTbdV2tRjeJX9ZlOb8NqPiaklpur8gjxNVKSbHhel/r7Ub7Wfv/EdUX
aL3Ui9weC4EsnZ1FC/9DG/Low/lsOsu2t4c+h9C9nEo=
`pragma protect end_protected
