-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Capture the external PPS reliably in the clk clock domain
-- Description:
--   The assumption is that the external PPS is synchronous to the clk clock
--   domain, but with an unknown skew. Therefore it must be possible to
--   reliably capture the external PPS always on the same clk clock edge. The
--   capture_cnt allows monitoring this, because when the external PPS is in
--   lock with the clk then the capture_cnt should remain equal to g_clk_freq.
--
--   The PPS capture can make use of:
--
--    1) input delay elements (IDELAY for Xilinx, ALTIO_BUF for Altera)
--    2) rising edge or falling edge clock so phase 0 and 180 (preferably via
--       DDR input buffer)
--    3) phases of a PLL so 8 or 16 phases from 0
--    4) one bit wide LVDS_RX with DPA
--
--   For LOFAR scheme 1) and 2) were used using Xilinx Virtex4. For UniBoard
--   scheme 1 can be implemented using the D1 delay element in the Alters
--   StratixIV. However this only allows 16 steps of 50 ps each and requires
--   a serial scheme to program. Therefore initially using only scheme 2 for
--   UniBoard PPS capture seems enough. Possibly in combination with using a
--   fixed 0 or > 0 delay setting for D1.
--
--   The common_ddio_in DDR element wraps the FPGA specific IP. It is better
--   to use an input DDR element then to use rising_edge/ falling_edge, to
--   ensure that both edges capture the pps_ext in dedicated logic near the
--   pin.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use technology_lib.technology_select_pkg.all;

entity ppsh is
  generic (
    g_technology    : natural := c_tech_select_default;
    g_clk_freq      : natural := 200 * 10**6  -- clock frequency of clk in Hz
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;
    -- PPS
    pps_ext        : in  std_logic;  -- with unknown but constant phase to clk
    pps_sys        : out std_logic;  -- pps pulse in clk domain (i.e. 1 clk cycle pulse per second)
    -- MM control
    pps_toggle     : out std_logic;  -- pps toggle level signal in clk domain (i.e. 0.5 Hz square wave)
    pps_stable     : out std_logic;  -- pps stable signal in clk domain
    capture_cnt    : out std_logic_vector(ceil_log2(g_clk_freq) - 1 downto 0);  -- counts the number of clk clock cycles between subsequent pps_ext pulses
    offset_cnt     : out std_logic_vector(ceil_log2(g_clk_freq) - 1 downto 0);  -- counts the number of clk clock cycles between now and last pps_ext pulse
    pps_stable_ack : in  std_logic := '0';  -- pps stable acknowledge in clk domain
    capture_edge   : in  std_logic := '0';  -- when '0' then clock pps_ext on rising edge of clk, else use falling edge of clk
    expected_cnt   : in  std_logic_vector(ceil_log2(g_clk_freq) - 1 downto 0) := (others => '1')  -- expected number of clk clock cycles between subsequent pps_ext pulses
 );
end ppsh;

architecture rtl of ppsh is
  constant c_pipeline_output : natural := 100;

  signal pps_ext_delayed   : std_logic_vector(0 downto 0);
  signal pps_ext_falling   : std_logic_vector(0 downto 0);
  signal pps_ext_rising    : std_logic_vector(0 downto 0);

  signal pps_ext_cap       : std_logic;
  signal nxt_pps_ext_cap   : std_logic;

  signal pps_ext_sync      : std_logic;

  signal pps_ext_revt      : std_logic;

  signal i_capture_cnt     : std_logic_vector(capture_cnt'range) := (others => '1');

  signal pps_sys_buf       : std_logic;
  signal i_pps_toggle      : std_logic;
  signal nxt_pps_toggle    : std_logic;

  signal pps_locked        : std_logic;
  signal nxt_pps_locked    : std_logic;
begin
  capture_cnt <= i_capture_cnt;
  pps_toggle  <= i_pps_toggle;

  pps_ext_delayed(0) <= pps_ext;  -- no input delay support

  u_in : entity common_lib.common_ddio_in
  generic map(
    g_technology => g_technology,
    g_width     => 1
  )
  port map(
    in_dat      => pps_ext_delayed,
    in_clk      => clk,
    rst         => '0',  -- no need to use rst, this eases timing closure
    out_dat_hi  => pps_ext_rising,
    out_dat_lo  => pps_ext_falling
  );

  nxt_pps_ext_cap <= pps_ext_rising(0) when capture_edge = '0' else pps_ext_falling(0);  -- captured ext_sync

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      pps_ext_cap    <= '0';
      i_pps_toggle   <= '0';
      pps_locked     <= '0';
    elsif rising_edge(clk) then
      pps_ext_cap    <= nxt_pps_ext_cap;
      i_pps_toggle   <= nxt_pps_toggle;
      pps_locked     <= nxt_pps_locked;
    end if;
  end process;

  -- Conquer potential meta-stability
  u_pps_sync : entity common_lib.common_async
  generic map (
    g_rst_level => '0',
    g_delay_len => c_meta_delay_len
  )
  port map (
    rst  => rst,
    clk  => clk,
    din  => pps_ext_cap,
    dout => pps_ext_sync
  );

  -- Maintain the capture interval counter
  u_pps_revt : entity common_lib.common_evt
  generic map (
    g_evt_type => "RISING",
    g_out_reg  => true
  )
  port map (
    rst      => rst,
    clk      => clk,
    in_sig   => pps_ext_sync,
    out_evt  => pps_ext_revt
  );

  u_capture_cnt : entity common_lib.common_interval_monitor
  generic map (
    g_interval_cnt_w => capture_cnt'length
  )
  port map (
    rst           => rst,
    clk           => clk,
    -- ST
    in_val        => '1',
    in_evt        => pps_ext_revt,
    -- MM
    interval_cnt  => i_capture_cnt,
    clk_cnt       => offset_cnt
  );

  -- Output the pps_sys with extra pipelining to ease timing of pps_sys fan out
  u_pps_sys : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline       => c_pipeline_output,
    g_reset_value    => 0,
    g_out_invert     => false
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => pps_ext_revt,
    out_dat => pps_sys_buf
  );

  pps_sys <= pps_sys_buf;

  -- Output the pps_toggle
  nxt_pps_toggle <= not i_pps_toggle when pps_sys_buf = '1' else i_pps_toggle;

  -- PPS locked (stable pps capture count)
  nxt_pps_locked <= '1' when unsigned(i_capture_cnt) = unsigned(expected_cnt) else '0';

  u_pps_stable : entity common_lib.common_stable_monitor
  port map (
    rst          => rst,
    clk          => clk,
    -- MM
    r_in         => pps_locked,
    r_stable     => pps_stable,
    r_stable_ack => pps_stable_ack
  );
end rtl;
