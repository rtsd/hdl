--IP Functional Simulation Model
--VERSION_BEGIN 11.1SP2 cbx_mgl 2012:01:25:21:26:09:SJ cbx_simgen 2012:01:25:21:25:27:SJ  VERSION_END


-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

 LIBRARY sgate;
 USE sgate.sgate_pack.all;

--synthesis_resources = lut 14 mux21 14 oper_add 2 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  altera_merlin_traffic_limiter_0002 IS 
	 PORT 
	 ( 
		 clk	:	IN  STD_LOGIC;
		 cmd_sink_channel	:	IN  STD_LOGIC_VECTOR (7 DOWNTO 0);
		 cmd_sink_data	:	IN  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 cmd_sink_endofpacket	:	IN  STD_LOGIC;
		 cmd_sink_ready	:	OUT  STD_LOGIC;
		 cmd_sink_startofpacket	:	IN  STD_LOGIC;
		 cmd_sink_valid	:	IN  STD_LOGIC;
		 cmd_src_channel	:	OUT  STD_LOGIC_VECTOR (7 DOWNTO 0);
		 cmd_src_data	:	OUT  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 cmd_src_endofpacket	:	OUT  STD_LOGIC;
		 cmd_src_ready	:	IN  STD_LOGIC;
		 cmd_src_startofpacket	:	OUT  STD_LOGIC;
		 cmd_src_valid	:	OUT  STD_LOGIC_VECTOR (7 DOWNTO 0);
		 reset	:	IN  STD_LOGIC;
		 rsp_sink_channel	:	IN  STD_LOGIC_VECTOR (7 DOWNTO 0);
		 rsp_sink_data	:	IN  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 rsp_sink_endofpacket	:	IN  STD_LOGIC;
		 rsp_sink_ready	:	OUT  STD_LOGIC;
		 rsp_sink_startofpacket	:	IN  STD_LOGIC;
		 rsp_sink_valid	:	IN  STD_LOGIC;
		 rsp_src_channel	:	OUT  STD_LOGIC_VECTOR (7 DOWNTO 0);
		 rsp_src_data	:	OUT  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 rsp_src_endofpacket	:	OUT  STD_LOGIC;
		 rsp_src_ready	:	IN  STD_LOGIC;
		 rsp_src_startofpacket	:	OUT  STD_LOGIC;
		 rsp_src_valid	:	OUT  STD_LOGIC
	 ); 
 END altera_merlin_traffic_limiter_0002;

 ARCHITECTURE RTL OF altera_merlin_traffic_limiter_0002 IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_0_405q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_1_404q	:	STD_LOGIC := '0';
	 SIGNAL  wire_ni_w_lg_w478w486w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w_lg_w478w479w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w485w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w478w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_has_pending_responses_370q	:	STD_LOGIC := '0';
	 SIGNAL  wire_nl_w483w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_0_401q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_1_369q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_2_368q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_3_367q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_4_366q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_5_365q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_6_364q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_7_363q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_0_362q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_1_361q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_2_360q	:	STD_LOGIC := '0';
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_sink_ready_408m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_0_433m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_1_432m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_2_431m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_3_430m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_4_429m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_5_428m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_6_427m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_7_426m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_internal_valid_409m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_381m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_382m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_384m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_385m_dataout	:	STD_LOGIC;
	 SIGNAL  wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_a	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_b	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
	 SIGNAL  wire_gnd	:	STD_LOGIC;
	 SIGNAL  wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_o	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_a	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_b	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_o	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_w_lg_w477w481w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_lg_w_cmd_sink_data_range154w472w490w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_cmd_sink_valid474w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w487w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w480w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_lg_w477w481w482w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_reset476w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w520w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w477w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w484w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w473w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_cmd_sink_data_range154w472w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_always1_386_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_410_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_411_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_412_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_413_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_414_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_415_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_416_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_417_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_nonposted_cmd_accepted_377_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_response_accepted_379_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_save_dest_id_338_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_407_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_vcc :	STD_LOGIC;
	 SIGNAL  wire_w_cmd_sink_data_range154w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
 BEGIN

	wire_gnd <= '0';
	wire_w_lg_w477w481w(0) <= wire_w477w(0) AND wire_w480w(0);
	wire_w_lg_w_lg_w_cmd_sink_data_range154w472w490w(0) <= wire_w_lg_w_cmd_sink_data_range154w472w(0) AND wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_internal_valid_409m_dataout;
	wire_w_lg_cmd_sink_valid474w(0) <= cmd_sink_valid AND wire_w473w(0);
	wire_w487w(0) <= s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_nonposted_cmd_accepted_377_dataout AND wire_ni_w_lg_w478w486w(0);
	wire_w480w(0) <= s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_response_accepted_379_dataout AND wire_ni_w_lg_w478w479w(0);
	wire_w_lg_w_lg_w477w481w482w(0) <= NOT wire_w_lg_w477w481w(0);
	wire_w_lg_reset476w(0) <= NOT reset;
	wire_w520w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_always1_386_dataout;
	wire_w477w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_nonposted_cmd_accepted_377_dataout;
	wire_w484w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_response_accepted_379_dataout;
	wire_w473w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_407_dataout;
	wire_w_lg_w_cmd_sink_data_range154w472w(0) <= NOT wire_w_cmd_sink_data_range154w(0);
	cmd_sink_ready <= wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_sink_ready_408m_dataout;
	cmd_src_channel <= ( cmd_sink_channel(7 DOWNTO 0));
	cmd_src_data <= ( cmd_sink_data(68 DOWNTO 0));
	cmd_src_endofpacket <= cmd_sink_endofpacket;
	cmd_src_startofpacket <= cmd_sink_startofpacket;
	cmd_src_valid <= ( wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_7_426m_dataout & wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_6_427m_dataout & wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_5_428m_dataout & wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_4_429m_dataout & wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_3_430m_dataout & wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_2_431m_dataout & wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_1_432m_dataout & wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_0_433m_dataout);
	rsp_sink_ready <= rsp_src_ready;
	rsp_src_channel <= ( rsp_sink_channel(7 DOWNTO 0));
	rsp_src_data <= ( rsp_sink_data(68 DOWNTO 0));
	rsp_src_endofpacket <= rsp_sink_endofpacket;
	rsp_src_startofpacket <= rsp_sink_startofpacket;
	rsp_src_valid <= rsp_sink_valid;
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_always1_386_dataout <= (s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_nonposted_cmd_accepted_377_dataout AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_response_accepted_379_dataout);
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_410_dataout <= (cmd_sink_valid AND cmd_sink_channel(0));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_411_dataout <= (cmd_sink_valid AND cmd_sink_channel(1));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_412_dataout <= (cmd_sink_valid AND cmd_sink_channel(2));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_413_dataout <= (cmd_sink_valid AND cmd_sink_channel(3));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_414_dataout <= (cmd_sink_valid AND cmd_sink_channel(4));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_415_dataout <= (cmd_sink_valid AND cmd_sink_channel(5));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_416_dataout <= (cmd_sink_valid AND cmd_sink_channel(6));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_417_dataout <= (cmd_sink_valid AND cmd_sink_channel(7));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_nonposted_cmd_accepted_377_dataout <= (wire_w_lg_w_lg_w_cmd_sink_data_range154w472w490w(0) AND (cmd_sink_endofpacket AND cmd_src_ready));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_response_accepted_379_dataout <= (rsp_sink_endofpacket AND (rsp_sink_valid AND rsp_src_ready));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_save_dest_id_338_dataout <= (wire_w_lg_w_cmd_sink_data_range154w472w(0) AND wire_w_lg_cmd_sink_valid474w(0));
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout <= (wire_w_lg_w_cmd_sink_data_range154w472w(0) AND altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_has_pending_responses_370q);
	s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_407_dataout <= (s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout AND (NOT (((NOT (cmd_sink_data(65) XOR altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_0_362q)) AND (NOT (cmd_sink_data(66) XOR altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_1_361q))) AND (NOT (cmd_sink_data(67) XOR altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_2_360q)))));
	s_wire_vcc <= '1';
	wire_w_cmd_sink_data_range154w(0) <= cmd_sink_data(51);
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_0_405q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_1_404q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
			IF (s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_always1_386_dataout = '0') THEN
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_0_405q <= wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_385m_dataout;
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_1_404q <= wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_384m_dataout;
			END IF;
		END IF;
	END PROCESS;
	wire_ni_w_lg_w478w486w(0) <= wire_ni_w478w(0) AND wire_ni_w485w(0);
	wire_ni_w_lg_w478w479w(0) <= wire_ni_w478w(0) AND altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_0_405q;
	wire_ni_w485w(0) <= NOT altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_0_405q;
	wire_ni_w478w(0) <= NOT altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_1_404q;
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_has_pending_responses_370q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_has_pending_responses_370q <= (wire_nl_w483w(0) OR (wire_w484w(0) AND wire_w487w(0)));
		END IF;
	END PROCESS;
	wire_nl_w483w(0) <= altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_has_pending_responses_370q AND wire_w_lg_w_lg_w477w481w482w(0);
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_0_401q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_1_369q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_2_368q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_3_367q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_4_366q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_5_365q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_6_364q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_7_363q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_0_362q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_1_361q <= '0';
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_2_360q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
			IF (s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_save_dest_id_338_dataout = '1') THEN
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_0_401q <= cmd_sink_channel(0);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_1_369q <= cmd_sink_channel(1);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_2_368q <= cmd_sink_channel(2);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_3_367q <= cmd_sink_channel(3);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_4_366q <= cmd_sink_channel(4);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_5_365q <= cmd_sink_channel(5);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_6_364q <= cmd_sink_channel(6);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_7_363q <= cmd_sink_channel(7);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_0_362q <= cmd_sink_data(65);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_1_361q <= cmd_sink_data(66);
				altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_dest_id_2_360q <= cmd_sink_data(67);
			END IF;
		END IF;
	END PROCESS;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_sink_ready_408m_dataout <= cmd_src_ready AND NOT(s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_407_dataout);
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_0_433m_dataout <= (altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_0_401q AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_410_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_410_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_1_432m_dataout <= (altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_1_369q AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_411_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_411_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_2_431m_dataout <= (altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_2_368q AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_412_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_412_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_3_430m_dataout <= (altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_3_367q AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_413_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_413_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_4_429m_dataout <= (altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_4_366q AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_414_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_414_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_5_428m_dataout <= (altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_5_365q AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_415_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_415_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_6_427m_dataout <= (altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_6_364q AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_416_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_416_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_7_426m_dataout <= (altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_last_channel_7_363q AND s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_417_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_406_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_cmd_src_valid_417_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_internal_valid_409m_dataout <= cmd_sink_valid AND NOT(s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_suppress_407_dataout);
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_381m_dataout <= wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_o(1) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_nonposted_cmd_accepted_377_dataout = '1'  ELSE altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_1_404q;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_382m_dataout <= wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_o(0) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_nonposted_cmd_accepted_377_dataout = '1'  ELSE altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_0_405q;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_384m_dataout <= wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_o(2) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_response_accepted_379_dataout = '1'  ELSE wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_381m_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_385m_dataout <= wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_o(1) WHEN s_wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_response_accepted_379_dataout = '1'  ELSE wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_next_pending_response_count_382m_dataout;
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_a <= ( altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_1_404q & altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_0_405q);
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_b <= ( "0" & "1");
	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380 :  oper_add
	  GENERIC MAP (
		sgate_representation => 0,
		width_a => 2,
		width_b => 2,
		width_o => 2
	  )
	  PORT MAP ( 
		a => wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_a,
		b => wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_b,
		cin => wire_gnd,
		o => wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add0_380_o
	  );
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_a <= ( altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_1_404q & altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_pending_response_count_0_405q & "1");
	wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_b <= ( "1" & "0" & "1");
	altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383 :  oper_add
	  GENERIC MAP (
		sgate_representation => 0,
		width_a => 3,
		width_b => 3,
		width_o => 3
	  )
	  PORT MAP ( 
		a => wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_a,
		b => wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_b,
		cin => wire_gnd,
		o => wire_altera_merlin_traffic_limiter_0002_altera_merlin_traffic_limiter_limiter_001_add1_383_o
	  );

 END RTL; --altera_merlin_traffic_limiter_0002
--synopsys translate_on
--VALID FILE
