Hardware:
. On development machine:
  $ python $SVN/RadioHDL/trunk/tools/oneclick/base/quartus_config.py
  $ run_all_sopc unb1 unb1_dp_offload
  $ run_rbf unb1 unb1_dp_offload
  Copy the resulting unb1_dp_offload.rbf from the build directory to your UniBoard LCU (e.g. home dir).
. On the UniBoard LCU:
  $ python $UPE_GEAR/peripherals/util_system_info.py --unb 0 --fn 0:3 --bn 0:3 -n 2                 # show all designs running on board
  $ python $UPE_GEAR/peripherals/util_system_info.py --unb 0 --fn 0:3 --bn 0:3 -n 4                 # Get memory maps of all designs
  $ python $UPE_GEAR/peripherals/util_wdi.py         --unb 0 --fn 0:3 --bn 0:3 -n 0                 # Revert all FPGAs to unb_factory
  $ python $UPE_GEAR/peripherals/util_system_info.py --unb 0 --fn 0:3 --bn 0:3 -n 4                 # Get memory maps of all designs
  $ python $UPE_GEAR/peripherals/util_epcs.py        --unb 0 --fn 0:1 -n 4 -s ~/unb1_dp_offload.rbf # Program RBF to flash user section of 2 FPGAs
  $ python $UPE_GEAR/peripherals/util_epcs.py        --unb 0 --fn 0:1 -n 8                          # Load the user image on the 2 FPGAs
  $ python $UPE_GEAR/peripherals/util_system_info.py --unb 0 --fn 0:3 --bn 0:3 -n 4                 # Get memory maps of all FPGAs
  
  Finally, run the test case:

  $ python $SVN/RadioHDL/trunk/libraries/base/dp/designs/unb1_dp_offload/tb/python/tc_unb1_dp_offload.py --unb 0 --fn 0:1 

  Example output:

    user@LCU(08:45:52):~$ python $SVN/RadioHDL/trunk/libraries/base/dp/designs/unb1_dp_offload/tb/python/tc_unb1_dp_offload.py --unb 0 --fn 0:1
    [2015:10:07 08:45:54] - (3) TB - >>>
    [2015:10:07 08:45:54] - (1) TB - >>> Title : Test case for design unb_dp_offload. Targets: UNB-[0], FN-[0, 1]: 
    [2015:10:07 08:45:54] - (3) TB - >>>
    [2015:10:07 08:45:54] - (3) TB - 
    [2015:10:07 08:45:54] - (3) TB - SI - UNB-0, FN-0:     Design = unb1_dp_offload
    [2015:10:07 08:45:54] - (3) TB - SI - UNB-0, FN-1:     Design = unb1_dp_offload
    UDP port enable 0
    [2015:10:07 08:45:54] - (5) TB - ETH - UNB-0, FN-0: write_udp_port_en = 69536
    [2015:10:07 08:45:54] - (5) TB - ETH - UNB-0, FN-1: write_udp_port_en = 69536
    [2015:10:07 08:45:54] - (5) TB - DPOTXH[0]UNB-0, FN-0: write eth_dst_mac = 2248671233
    [2015:10:07 08:45:54] - (5) TB - DPOTXH[0]UNB-0, FN-0: write eth_dst_mac = 34
    [2015:10:07 08:45:54] - (5) TB - DPOTXH[0]UNB-0, FN-1: write eth_dst_mac = 2248671232
    [2015:10:07 08:45:54] - (5) TB - DPOTXH[0]UNB-0, FN-1: write eth_dst_mac = 34
    [2015:10:07 08:45:54] - (5) TB - BG - UNB-0, FN-0: write_enable
    [2015:10:07 08:45:54] - (5) TB - BG - UNB-0, FN-1: write_enable
    [2015:10:07 08:45:54] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 08:45:54] - (5) TB - UNB-0, FN-1: BSN[0] nof_sop.1   
    [2015:10:07 08:45:55] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.100000   
    [2015:10:07 08:45:55] - (5) TB - UNB-0, FN-1: BSN[0] nof_sop.100000   
    [2015:10:07 08:45:55] - (5) TB - UNB-0, FN-0: BSN[0] xon_stable...................1   ready_stable.................1   
    [2015:10:07 08:45:55] - (5) TB -                     sync_timeout.................0   bsn_at_sync.............200000   
    [2015:10:07 08:45:55] - (5) TB -                     nof_sop.................100000   nof_valid..............2200000   
    [2015:10:07 08:45:55] - (5) TB -                     nof_err......................0   bsn_first....................0   
    [2015:10:07 08:45:55] - (5) TB -                     bsn_first_cycle_cnt.2578168288   block_len...................22   
    [2015:10:07 08:45:55] - (5) TB -                     nof_sync.....................2   
    [2015:10:07 08:45:55] - (5) TB - UNB-0, FN-1: BSN[0] xon_stable...................1   ready_stable.................1   
    [2015:10:07 08:45:55] - (5) TB -                     sync_timeout.................0   bsn_at_sync.............200000   
    [2015:10:07 08:45:55] - (5) TB -                     nof_sop.................100000   nof_valid..............2200000   
    [2015:10:07 08:45:55] - (5) TB -                     nof_err......................0   bsn_first....................0   
    [2015:10:07 08:45:55] - (5) TB -                     bsn_first_cycle_cnt.2573338865   block_len...................22   
    [2015:10:07 08:45:55] - (5) TB -                     nof_sync.....................2   
    [2015:10:07 08:45:55] - (0) TB - >>> Test Case result: PASSED
    PASSED

Simulation:
. On development machine:
  $ python $SVN/RadioHDL/trunk/tools/oneclick/base/modelsim_config.py
  $ run_sopc unb1 unb1_dp_offload         # not required if you've already run run_all_sopc for hardware 
  $ run_modelsim unb1
  VSIM #> lp unb1_dp_offload
  VSIM #> mk all
  (double click tb_unb1_dp_offload in the Project tab)
  VSIM #> as 8
  VSIM #> run 2us
  $ python $SVN/RadioHDL/trunk/libraries/base/dp/designs/unb1_dp_offload/tb/python/tc_unb1_dp_offload.py --unb 0 --fn 0 --sim
  VSIM #> run -a

  Example terminal output:

    user@DEV(09:21:34):~$ python $SVN/RadioHDL/trunk/libraries/base/dp/designs/unb1_dp_offload/tb/python/tc_unb1_dp_offload.py --unb 0 --fn 0 --sim
    [2015:10:07 09:21:48] - (3) TB - >>>
    [2015:10:07 09:21:48] - (1) TB - >>> Title : Test case for design unb_dp_offload. Targets: UNB-[0], FN-[0]: 
    [2015:10:07 09:21:48] - (3) TB - >>>
    [2015:10:07 09:21:48] - (3) TB - 
    [2015:10:07 09:22:14] - (3) TB - SI - UNB-0, FN-0:     Design = unb1_dp_offload
    UDP port enable 0
    [2015:10:07 09:22:14] - (5) TB - ETH - UNB-0, FN-0: write_udp_port_en = 69536
    [2015:10:07 09:22:14] - (5) TB - BG - UNB-0, FN-0: write_enable
    [2015:10:07 09:22:14] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.0   
    [2015:10:07 09:22:15] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.0   
    [2015:10:07 09:22:16] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.0   
    [2015:10:07 09:22:17] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:18] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:19] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:20] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:21] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:22] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:23] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:24] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:26] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:27] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:28] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.1   
    [2015:10:07 09:22:29] - (5) TB - UNB-0, FN-0: BSN[0] nof_sop.5   
    [2015:10:07 09:22:29] - (5) TB - UNB-0, FN-0: BSN[0] xon_stable.............1   ready_stable...........1   sync_timeout...........0   bsn_at_sync...........10   nof_sop................5   
    [2015:10:07 09:22:29] - (5) TB -                     nof_valid............110   nof_err................0   bsn_first..............0   bsn_first_cycle_cnt.3085   block_len.............22   
    [2015:10:07 09:22:29] - (5) TB -                     nof_sync...............2   
    [2015:10:07 09:22:29] - (0) TB - >>> Test Case result: PASSED
    PASSED

  
