-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Switch output high or low
-- Description:
-- . The output goes high when switch_high='1' and low when switch_low='1'.
-- . If g_or_high is true then the output follows the switch_high immediately,
--   else it goes high in the next clk cycle.
-- . If g_and_low is true then the output follows the switch_low immediately,
--   else it goes low in the next clk cycle.
--   The g_priority_lo defines which input has priority when switch_high and
--   switch_low are active simultaneously.

library ieee;
use ieee.std_logic_1164.all;

entity common_switch is
  generic (
    g_rst_level    : std_logic := '0';  -- Defines the output level at reset.
    g_priority_lo  : boolean := true;  -- When TRUE then input switch_low has priority, else switch_high. Don't care when switch_high and switch_low are pulses that do not occur simultaneously.
    g_or_high      : boolean := false;  -- When TRUE and priority hi then the registered switch_level is OR-ed with the input switch_high to get out_level, else out_level is the registered switch_level
    g_and_low      : boolean := false  -- When TRUE and priority lo then the registered switch_level is AND-ed with the input switch_low to get out_level, else out_level is the registered switch_level
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    switch_high : in  std_logic;  -- A pulse on switch_high makes the out_level go high
    switch_low  : in  std_logic;  -- A pulse on switch_low makes the out_level go low
    out_level   : out std_logic
  );
end;

architecture rtl of common_switch is
  signal switch_level         : std_logic := g_rst_level;
  signal nxt_switch_level     : std_logic;
begin
  gen_wire : if g_or_high = false and g_and_low = false generate
    out_level <= switch_level;
  end generate;

  gen_or : if g_or_high = true and g_and_low = false generate
    out_level <= switch_level or switch_high;
  end generate;

  gen_and : if g_or_high = false and g_and_low = true generate
    out_level <= switch_level and (not switch_low);
  end generate;

  gen_or_and : if g_or_high = true and g_and_low = true generate
    out_level <= (switch_level or switch_high) and (not switch_low);
  end generate;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      switch_level <= g_rst_level;
    elsif rising_edge(clk) then
      if clken = '1' then
        switch_level <= nxt_switch_level;
      end if;
    end if;
  end process;

  p_switch_level : process(switch_level, switch_low, switch_high)
  begin
    nxt_switch_level <= switch_level;
    if g_priority_lo = true then
      if switch_low = '1' then
        nxt_switch_level <= '0';
      elsif switch_high = '1' then
        nxt_switch_level <= '1';
      end if;
    else
      if switch_high = '1' then
        nxt_switch_level <= '1';
      elsif switch_low = '1' then
        nxt_switch_level <= '0';
      end if;
    end if;
  end process;
end rtl;
