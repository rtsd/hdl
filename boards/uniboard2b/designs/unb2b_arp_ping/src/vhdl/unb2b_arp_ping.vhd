-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: P. Donker
-- Purpose: Support ARP response and ping response via 1GE on UniBoard2
-- Description:
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, technology_lib, dp_lib, eth_lib, eth1g_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use technology_lib.technology_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use eth_lib.eth_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity unb2b_arp_ping is
  generic (
    g_design_name       : string  := "unb2b_arp_ping";
    g_design_note       : string  := "UNUSED";
    g_technology        : natural := c_tech_arria10_e1sg;
    g_sim               : boolean := false;  -- Overridden by TB
    g_sim_level         : natural := 0;  -- 0 = use IP; 1 = use fast serdes model;
    g_stamp_date        : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time        : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id       : string  := "";  -- revision id     -- set by QSF
    g_factory_image     : boolean := false;  -- TRUE;
    g_protect_addr_range: boolean := false
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

    QSFP_LED     : out   std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end unb2b_arp_ping;

architecture str of unb2b_arp_ping is
  constant c_mm_clk_freq            : natural := c_unb2b_board_mm_clk_freq_50M;

  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic;

  signal st_rst                     : std_logic;
  signal st_clk                     : std_logic;

  signal app_led_red                : std_logic := '1';
  signal app_led_green              : std_logic := '0';

  -- PIOs
  signal pout_wdi                   : std_logic := '0';
  signal wdi_cnt                    : integer := 0;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi := c_mem_mosi_rst;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi := c_mem_mosi_rst;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- pm bus
  signal reg_unb_pmbus_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal reg_unb_pmbus_miso         : t_mem_miso;

  -- FPGA sensors
  signal reg_fpga_temp_sens_mosi    : t_mem_mosi := c_mem_mosi_rst;
  signal reg_fpga_temp_sens_miso    : t_mem_miso;
  signal reg_fpga_voltage_sens_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_fpga_voltage_sens_miso : t_mem_miso;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- EPCS read
  signal reg_dpmm_data_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dpmm_data_miso         : t_mem_miso;
  signal reg_dpmm_ctrl_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dpmm_ctrl_miso         : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal reg_mmdp_data_miso         : t_mem_miso;
  signal reg_mmdp_ctrl_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal reg_mmdp_ctrl_miso         : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal reg_epcs_miso              : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal reg_remu_miso              : t_mem_miso;

  -- QSFP leds
  signal qsfp_green_led_arr         : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr           : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);

  -- Node info
  -- . Base address as used by unb_osy
  constant c_base_ip                : std_logic_vector(c_16 - 1 downto 0) := X"0A63";  -- Base IP address used by unb_osy: 10.99.xx.yy
  constant c_base_mac               : std_logic_vector(c_32 - 1 downto 0) := X"00228608";  -- Base MAC address used by unb_osy:

  signal system_info                : t_c_unb2b_board_system_info;
  signal back_id                    : std_logic_vector(c_8 - 1 downto 0);
  signal node_id_ip                 : std_logic_vector(c_8 - 1 downto 0);
  signal node_id_mac                : std_logic_vector(c_8 - 1 downto 0);
  signal src_ip                     : std_logic_vector(c_network_ip_addr_slv'range);
  signal src_mac                    : std_logic_vector(c_network_eth_mac_slv'range);
begin
  system_info  <= func_unb2b_board_system_info(VERSION, ID);
  back_id      <= to_uvec(system_info.bck_id, c_8);  -- xx = bck_id
  node_id_ip   <= to_uvec(system_info.node_id + 1, c_8);  -- yy = node_id +1 to avoid reserved 00
  node_id_mac  <= to_uvec(system_info.node_id, c_8);  -- yy = node_id
  src_ip       <= c_base_ip & back_id & node_id_ip;
  src_mac      <= c_base_mac & back_id & node_id_mac;

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb2b_board_lib.ctrl_unb2b_board
  generic map (
    g_sim                     => g_sim,
    g_sim_level               => g_sim_level,
    g_technology              => g_technology,
    g_base_ip                 => c_base_ip,
    g_design_name             => g_design_name,
    g_design_note             => g_design_note,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_revision_id             => g_revision_id,
    g_mm_clk_freq             => c_mm_clk_freq,
    g_eth_clk_freq            => c_unb2b_board_eth_clk_freq_125M,
    g_aux                     => c_unb2b_board_aux,
    g_factory_image           => g_factory_image,
    g_udp_offload             => g_sim,  -- use g_udp_offload to enable ETH instance in simulation
    g_udp_offload_nof_streams => 3,  -- use g_udp_offload, but no UDP offload streams
    g_protect_addr_range      => g_protect_addr_range,
    g_app_led_red             => true,
    g_app_led_green           => true
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => st_rst,
    dp_clk                   => st_clk,
    dp_pps                   => OPEN,
    dp_rst_in                => st_rst,
    dp_clk_in                => st_clk,

    app_led_red              => app_led_red,
    app_led_green            => app_led_green,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    SENS_SC                  => SENS_SC,
    SENS_SD                  => SENS_SD,
    -- PM bus
    PMBUS_SC                 => PMBUS_SC,
    PMBUS_SD                 => PMBUS_SD,
    PMBUS_ALERT              => PMBUS_ALERT,

    -- . 1GbE Control Interface
    ETH_clk                  => ETH_CLK,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -- normaly done by unb_os
  p_wdi : process(mm_clk)
  begin
    if rising_edge(mm_clk) then
      if wdi_cnt = 5000 then
        pout_wdi <= not pout_wdi;
        wdi_cnt <= 0;
      else
        wdi_cnt <= wdi_cnt + 1;
      end if;
    end if;
  end process;

  eth1g_mm_rst <= mm_rst;

  -- led control
  p_led : process(eth1g_reg_interrupt)
  begin
    if rising_edge(eth1g_reg_interrupt) then
      app_led_red <= '0';
      app_led_green <= not app_led_green;
    end if;
  end process;

  --u_eth1g_master : ENTITY eth1g_lib.eth1g_master(beh)
  u_eth1g_master : entity eth1g_lib.eth1g_master(rtl)
  generic map (
    g_sim         => g_sim
  )
  port map (
    mm_rst        => mm_rst,
    mm_clk        => mm_clk,

    tse_mosi      => eth1g_tse_mosi,
    tse_miso      => eth1g_tse_miso,
    reg_interrupt => eth1g_reg_interrupt,
    reg_mosi      => eth1g_reg_mosi,
    reg_miso      => eth1g_reg_miso,
    ram_mosi      => eth1g_ram_mosi,
    ram_miso      => eth1g_ram_miso,

    src_mac       => src_mac,
    src_ip        => src_ip
  );

  u_front_led : entity unb2b_board_lib.unb2b_board_qsfp_leds
  generic map (
    g_sim           => g_sim,
    g_factory_image => g_factory_image,
    g_nof_qsfp      => c_unb2b_board_tr_qsfp.nof_bus,
    g_pulse_us      => 1000 / (10**9 / c_mm_clk_freq)  -- nof clk cycles to get us period
  )
  port map (
    rst             => mm_rst,
    clk             => mm_clk,
    green_led_arr   => qsfp_green_led_arr,
    red_led_arr     => qsfp_red_led_arr
  );

  u_front_io : entity unb2b_board_lib.unb2b_board_front_io
  generic map (
    g_nof_qsfp_bus => c_unb2b_board_tr_qsfp.nof_bus
  )
  port map (
    green_led_arr => qsfp_green_led_arr,
    red_led_arr   => qsfp_red_led_arr,
    QSFP_LED      => QSFP_LED
  );
end str;
