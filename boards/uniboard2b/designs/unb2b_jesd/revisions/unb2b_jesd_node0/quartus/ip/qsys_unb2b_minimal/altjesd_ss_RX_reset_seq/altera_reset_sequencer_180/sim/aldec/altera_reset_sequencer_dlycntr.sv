// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent= "Aldec protectip", encrypt_agent_info= "Riviera-PRO 2015.06.92"
`pragma protect data_method= "aes128-cbc"
`pragma protect key_keyowner= "Aldec", key_keyname= "ALDEC15_001", key_method= "rsa"
`pragma protect key_block encoding= (enctype="base64", line_length= 76, bytes= 256)
lrJCkygbtA47Fd3D3MCc3Ae+4bGWBQqUzxwmt4JVqCwhY9e3hjCW9eOYMNYFuDZPdJnUxwjKohCo
q/ZX1hDsdonF5Zy0igrcaFVGRme548iIKtJpcO1Ev3jyJsziko1+taIj84LLsYxSA8sS7DCoMGVZ
0mTu1kXz0iJkYG9QrC7bho0aDNerjKADF/1SSIXfeXPyPSdYlcYGGY43KYqjX21Li9CjLrK/b4mI
Ls+HEL8YtLJI2SYIANySRV2ja98RU3iY9KmBj52Lp40jAfrz6h3l2vMsNVic14P+ViaZxpKXz1aG
G9rspyPeoxQjQSIeXaV4iJdlGU7456O1hJpWIg==
`pragma protect data_keyowner= "altera", data_keyname= "altera"
`pragma protect data_block encoding= (enctype="base64", line_length= 76, bytes= 2192)
K/DCkPWQDQlFkAnfyYlaQvxIY/1zKWHi6DCYLFG+JfkDXqQ+i3+dGg09+PNB6bEbIvyi3kHzqN9N
/nQonTHU+e2bP0tGW88ZovS0bUlUC0SeHBDfRm0zT/L9xkmUJ5WRfNrLDf/3WmU04f2sM3743DGO
0vwd/4M+6RDSia/mbDCaQVPhiRStO4UUMvwSwZC7W8lmEL1lFq7XfQFRftZn1Fo0FiZkOcqYdyej
j+6F0HaepDCDtKkWzoomYlOB8XHAIovdGEj1SUenRF/k+ak1nYN8w2tzbM9ErEhsJjI6WaV5qmOS
RwKYkLYMn1bDPoatGSG+QJT0EnWWEz1SLfXSEOWatjsZCz+eexClx/AdcM+Nl+3FqU2j0h542Kpi
Ha8e1jU4BupXG3aUyHMDau3C3MzM7C3JRYKVTq0AYiY1oktiNwnBqPLFqd0of3by1PtHr8WTYbOi
xodVIQ+KQ9+5ah6KhL2pR0KAceZzmFUJvso+rOIo/oENEPt68FRWJNUwvjNBwXBdAxjzpNR5O0Pe
cFnk6TdNr4lZY38IValis7MbG8YqdV8tPJViE/L2pDbWsKZMnby8gBeqDgUIjs6FWY9SYFu1qD2T
YAoFTFdqvX6wNXflNKIBFH6dhRt7f0rnYRK+uIQsmWldtRWwWzNG14/MrcLRoQjIub1s4hZKNNOi
N+Ez3RRQclFAGbvxuFwtY6udo5I7kzai2jlTfGhKuBK+uenXLsIg+dSn9RQ6tS40wE8S8TsD6qQk
A5Xvx37RYsWdJsUWPkjb4V2C4/UnNzeVLeyp7jhktLDuleIncZwAoUrTFCJK2JIrDisOXPKWNeFU
utzpen5cPE2TCVgm5H0FwgA9s8TiQFZhVWyEwy3LYOYVwkxL8CsAR2AYsJTrrhggnr9r4Kw+ODAg
chslXesgfUEsI1KSABBUWk/8B1jq8JiyXN3eyrDL1CGBZPDbKyo+cqTNVWaWYKMQX1l5Gugm5b1G
Zgs0rbgc+B0Y72KakTwgpyqCWZkyBRsrmEKz9Dxhj4l0V+A5d8nsF7hZ4K4eIAiGUZVCLzjhGBYd
L0WwsRFhvtBIBpdTZzphXw9mHESSGIAMzwDIcJHpYse7Qam9fnGJ/tGJcp/gTfkWmtAaQOpwdMvo
mYBn1cyd1x1anLe+flZMq97W18xbym04mLMetMJ1DbLFoXtxjllsbIpJ/XNfrK7pWUg8SLCl5SUC
MdefCRLg00IyBOY2daQXr2HMCWG3VBqa0MHPvmzrzOdK/dW2pCRCbbZv/OjnGuqgZ4EV//z564Qd
C3LwZ91OXy1rZJMP+AE2uk5VvlIKftcZmmzUeJ4Okw+aJ+SpH6jikKyKLImYYYyomGFXlZiZ6ZDX
9pwkXa+NJZGoIZz3Xupx8uM6e7ANyiewifEGmmMh1ijyDBABXWX3mGttHzepgJtGGo51kJkqQc6i
HkN7bT46jAhv3xtllj0vOUnYqzxp1HxhvmDYkS4QnKBZKuGqyph7ldK0Pa4xX1uDnysQYGSUw0ym
PtNt2kCMOixttGSV9DEoKFLanx7TICsMeI+ZkYFyv7sJtxBDvEWg85ih3nO46xiqbclf5NnM4XdJ
/wBF8s+2vCeXfON2/Z9aydcz+RucYCVIlujPr4+85OSj1m5G1dDNe4gRQJDdM6uptHfHOF1c1fY4
mH5fJh81gNmUjJY1TZPMeRqdaimF3r5yoflUmG9RpPc0kTFKlMvooEtMYuJBBjo6c3NVmKIBhVfG
PEDbNxAUS/Rh25VZ2p/OEG1AeSfTCeecQMrL5nk/JWEMxrMXjFIuWocoOw4umfUHzhWxxvXj2jBl
4rHnvs4PKSonuMahtp2xz/VVeQoiAYqT/CQfKHzfz4+helOUVTpaGEz2hJb+mH53qBHuKhaJPC4X
3nQD1o7sBoaSJIWRfN1dKAb4QFoOFgC3wzPMJBwNNPhwpLtVqB0dxZfJn0S7gigho0dPmrH5Hhds
BtrnO4hETcRqjEW9X6BOhYkPDTKLnntbhCRHw22u5B8vBM8sDcpVBuCCR7wg5AbHkstMcdA/i8VM
V2FYPHcUwO5zFmrANA/vDNdZDr9fhpl7BcY24LfnC8xs1k1ifjvWSjsWcPQadBPvbSkgt/17land
QYWL4YjCDjwSkhanwuv1X2PsEyZaq/AgpFwFS8QQuKHaz+Ghzl4LdugFmH/qgaUYRtTn47d9A7ao
nSxEYuyu9jHjA4wdivE0SZtJi/0YXsk/81xd/6cLl+pq7pX1CqZHG7RKb4WVMUVy32tYu/fVm3g9
PgMW7+fwEOgQ1tVV78epH2DU8G292cXhe7fKsFnocea3+E29QRcfMaUERbMVQJX9C+I0IE3KXGE6
QiCbCk+U3eAamZLVFplLyIAYBOyYL6HwmdtmKRkLytP4eVCviowwvYPN0qZ5olh4mN+1I7xthR0H
Id+eqOEF4xfDF9ZNGGjlTHuemZ/2IkAJnEq9IgeTLp5arw2WYtb/xJH7VXtk7Hxb44/NPT/W38CP
DiJGb/kqrvLEDPiEldw59uD5JJotsr3JPPse23EcdaI6Wu9JVQNTBwanZQ3kbwdmRSiWqLItaxCs
VErwA76hrItapq+15AborfzSqit6jM6D8y+/NkX+GrZVBNgZnU3t8CbyLFLrOB0u6Kl3vQKPoef/
d6sDTO0Mp0n2obpAROHpe6+Mzeu3Ptf0AETBwOeaeY1odaB6SciY0e3F7etNyH2PhfpuPqUoh59J
0fQ7XNOPSG5OjSd1m4QDip8YzJFrLLk4OCfLD6vvCPn8dtwbarkK1zQoT7/UU1CeQW/TQPTqQKps
bYUjZRcqFh+wCGHVqHHbn7O6LXGIxXRAzE3zNyqwTc1RrG2gOjkUMyEWWqbgF+6vBoHjrRM72S0R
4o8wBslxuf6oVKPfUk51rSTC3+n8fBgb+DI=
`pragma protect end_protected
