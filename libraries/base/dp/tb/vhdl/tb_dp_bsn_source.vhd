-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Usage:
-- > as 10
-- > run -all
-- . sop, eop are verified automatically
-- . sync and bsn are verified automatically
-- and then manually verify on/off in Wave window

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_bsn_source is
end tb_dp_bsn_source;

architecture tb of tb_dp_bsn_source is
  constant c_clk_period   : time    := 10 ns;

  constant c_block_size   : natural := 31;
  --CONSTANT c_block_size   : NATURAL := 32;
  constant c_bsn_w        : natural := 16;
  constant c_sync_period  : natural := 7;
  constant c_sync_offset  : natural := 3;  -- must be < c_sync_period for proc_dp_verify_sync

  -- The state name tells what kind of test is being done
  type t_state_enum is (
    s_disable,
    s_start,
    s_pps_start
  );

  signal tb_state     : t_state_enum;

  signal tb_end       : std_logic := '0';
  signal rst          : std_logic := '1';
  signal clk          : std_logic := '1';
  signal pps          : std_logic;

  -- DUT
  signal dp_on        : std_logic := '0';
  signal dp_on_pps    : std_logic := '0';
  signal init_bsn     : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '0');

  signal bs_sosi      : t_dp_sosi;

  -- Verify
  signal verify_sync  : std_logic := '1';
  signal hold_bs_sop  : std_logic;
begin
  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------
  rst <= '1', '0' after c_clk_period * 7;
  clk <= (not clk) or tb_end after c_clk_period / 2;

  -- MM control
  p_mm : process
  begin
    tb_end   <= '0';
    tb_state <= s_disable;
    pps      <= '0';

    dp_on   <= '0';
    dp_on_pps  <= '0';
    init_bsn <= TO_UVEC(c_sync_offset, c_bsn_w);

    -- Get synchronous to clk
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 500);

    -- Start by making dp_on high
    tb_state <= s_start;
    dp_on <= '1';
    proc_common_wait_some_cycles(clk, 2000);

    -- Stop by making dp_on low
    tb_state <= s_disable;
    dp_on <= '0';
    proc_common_wait_some_cycles(clk, 1000);

    -- Now start on PPS
    tb_state <= s_pps_start;
    dp_on_pps <= '1';
    dp_on     <= '1';
    proc_common_wait_some_cycles(clk, 10);
    pps <= '1';
    proc_common_wait_some_cycles(clk, 1);
    pps <= '0';
    proc_common_wait_some_cycles(clk, 1000);

    -- Stop by making dp_on low
    tb_state <= s_disable;
    dp_on <= '0';
    dp_on_pps <= '0';  -- No PPS trigger next time
    proc_common_wait_some_cycles(clk, 1000);

    -- Start by making dp_on high
    tb_state <= s_start;
    dp_on <= '1';
    proc_common_wait_some_cycles(clk, 2500);

    -- Stop by making dp_on low
    tb_state <= s_disable;
    dp_on <= '0';
    dp_on_pps <= '0';
    proc_common_wait_some_cycles(clk, 1000);

    -- Now start on next PPS and continue forever
    init_bsn <= TO_UVEC(c_sync_offset, c_bsn_w);
    tb_state <= s_pps_start;
    dp_on_pps <= '1';
    dp_on     <= '1';
    proc_common_wait_some_cycles(clk, 20);
    pps <= '1';
    proc_common_wait_some_cycles(clk, 1);
    pps <= '0';

    proc_common_wait_some_cycles(clk, 10000);
    tb_end   <= '1';
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- Verification
  -----------------------------------------------------------------------------
  proc_dp_verify_sop_and_eop(clk, bs_sosi.valid, bs_sosi.sop, bs_sosi.eop, hold_bs_sop);  -- Verify that sop and eop come in pairs
  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_sync, bs_sosi.sync, bs_sosi.sop, bs_sosi.bsn);  -- Verify sync at sop and at expected BSN

  -----------------------------------------------------------------------------
  -- DUT: dp_bsn_source
  -----------------------------------------------------------------------------

  dut : entity work.dp_bsn_source
  generic map (
    g_block_size         => c_block_size,
    g_nof_block_per_sync => c_sync_period,
    g_bsn_w              => c_bsn_w
  )
  port map (
    rst       => rst,
    clk       => clk,
    pps       => pps,
    -- MM control
    dp_on     => dp_on,
    dp_on_pps => dp_on_pps,
    init_bsn  => init_bsn,
    -- Streaming
    src_out   => bs_sosi
  );
end tb;
