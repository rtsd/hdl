--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------
-- Purpose: Tb for WG
-- Description: Verifies all WG modes.
-- Usage:
-- > as 10
-- > run -all
-- . Use select rigth mouse in Wave window on wg_dat and choose radix -->
--   decimal and format --> analogue (automatic)
-- . Observe state in diag_wg(0).

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use work.diag_pkg.all;

entity tb_diag_wg_wideband is
  generic (
    -- Wideband parameters
    g_wideband_factor  : natural := 4;  -- Wideband rate factor >= 1 for unit frequency of g_wideband_factor * Fs
                                            -- Take care that the g_wideband_factor and c_clk_freq match the simulation time resolution of integer 1 ps
    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_addr_w       : natural := 10;
    g_buf_dat_w        : natural := 8;
    g_wg_dat_w         : natural := 8
  );
end tb_diag_wg_wideband;

architecture tb of tb_diag_wg_wideband is
  constant c_clk_freq       : natural := 200;  -- MHz
  constant c_clk_period     : time    := (10**6 / c_clk_freq) * 1 ps;

  -- Default WG settings
  constant c_buf_nof_dat    : natural := 2**g_buf_addr_w;

  constant c_wg_gain_w      : natural := 1;  -- Normalized range [0 1>  maps to fixed point range [0:2**c_diag_wg_ampl_w>
                                              -- . use gain 2**0             = 1 to have fulle scale without clipping
                                              -- . use gain 2**g_calc_gain_w > 1 to cause clipping

  constant c_buf_full_scale : natural := 2**(g_buf_dat_w - 1) - 1;  -- The stored waveform range should also be [-c_buf_full_scale +c_buf_full_scale], so not including -c_buf_full_scale-1
  constant c_wg_full_scale  : natural := 2**(g_wg_dat_w - 1) - 1;
  constant c_ampl_norm      : real := sel_a_b(g_wg_dat_w < g_buf_dat_w, real(c_wg_full_scale) / real(c_wg_full_scale+1), 1.0);
  --CONSTANT c_ampl_norm    : REAL := REAL(c_wg_full_scale)/REAL(c_wg_full_scale+1);     -- Use this if g_wg_dat_w < g_buf_dat_w, to avoid clipping
  --CONSTANT c_ampl_norm    : REAL := 1.0;                                               -- Use this if g_wg_dat_w = g_buf_dat_w, because the stored waveform range is already -+c_buf_full_scale
  --CONSTANT c_ampl_norm    : REAL := REAL(c_buf_full_scale)/REAL(c_buf_full_scale+1);   -- No need to use this, because the stored waveform range is already -+c_buf_full_scale

  constant c_freq_unit      : real := c_diag_wg_freq_unit;  -- ^= c_clk_freq = Fs (sample frequency), assuming one sinus waveform in the buffer
  constant c_ampl_unit      : real := c_diag_wg_ampl_unit * c_ampl_norm;  -- ^= Full Scale range [-c_wg_full_scale +c_wg_full_scale] without clipping
  constant c_phase_unit     : real := c_diag_wg_phase_unit;  -- ^= 1 degree

  -- Wideband WG settings
  constant c_sample_period   : time    := (10**6 / (c_clk_freq * g_wideband_factor)) * 1 ps;

  type t_buf_dat_arr is array (natural range <>) of std_logic_vector(g_buf_dat_w - 1 downto 0);

  signal tb_end         : std_logic;
  signal rst            : std_logic;
  signal clk            : std_logic := '1';
  signal restart        : std_logic;

  signal wg_ctrl        : t_diag_wg;
  signal cur_ctrl       : t_diag_wg;
  signal mon_ctrl       : t_diag_wg;

  signal wg_mode        : natural;
  signal wg_freq        : natural;
  signal wg_ampl        : natural;
  signal wg_nof_samples : natural;
  signal wg_phase       : natural;

  -- Wideband WG output is big endian, so first output sample in MSBit, MSData
  signal out_ovr        : std_logic_vector(g_wideband_factor            - 1 downto 0);
  signal out_dat        : std_logic_vector(g_wideband_factor * g_buf_dat_w - 1 downto 0);
  signal out_val        : std_logic_vector(g_wideband_factor            - 1 downto 0);
  signal out_sync       : std_logic_vector(g_wideband_factor            - 1 downto 0);

  signal wg_ovr         : std_logic_vector(0 to g_wideband_factor - 1);
  signal wg_dat         : t_buf_dat_arr(0 to g_wideband_factor - 1);
  signal wg_val         : std_logic_vector(0 to g_wideband_factor - 1);
  signal wg_sync        : std_logic_vector(0 to g_wideband_factor - 1);

  signal sample_clk     : std_logic := '1';
  signal sample_cnt     : natural range 0 to g_wideband_factor - 1;
  signal sample_ovr     : std_logic;
  signal sample_dat     : std_logic_vector(g_buf_dat_w - 1 downto 0);
  signal sample_val     : std_logic;
  signal sample_sync    : std_logic;
begin
  rst <= '1', '0' after c_clk_period / 10;
  clk <= not clk or tb_end after c_clk_period / 2;

  sample_clk <= not sample_clk or tb_end after c_sample_period / 2;

  wg_ctrl.mode        <= TO_UVEC(wg_mode,        c_diag_wg_mode_w);
  wg_ctrl.freq        <= TO_UVEC(wg_freq,        c_diag_wg_freq_w);
  wg_ctrl.ampl        <= TO_UVEC(wg_ampl,        c_diag_wg_ampl_w);
  wg_ctrl.nof_samples <= TO_UVEC(wg_nof_samples, c_diag_wg_nofsamples_w);
  wg_ctrl.phase       <= TO_UVEC(wg_phase,       c_diag_wg_phase_w);

  p_mm : process
  begin
    tb_end         <= '0';
    restart        <= '0';
    wg_mode        <= c_diag_wg_mode_off;

    ---------------------------------------------------------------------------
    -- >>> Single, repeat mode
    wg_nof_samples <= c_buf_nof_dat;  -- must be <= c_buf_nof_dat

    -- >>> CALC mode
    -- Cosinus with frequency Fs/2 (note sinus Fs/2 yields DC = 0)
--     wg_freq        <= INTEGER(0.5 * c_freq_unit);
--     wg_phase       <= INTEGER(90.0 * c_phase_unit);

    -- Choosing 1.0*Fs to select Fs which is equivalent to DC, hence the DC value is then determined by phase
    -- this also applies to 2.0, 3.0, 4.0 etc
--     wg_freq        <= INTEGER(1.0 * c_freq_unit);
--     wg_phase       <= INTEGER(45.0 * c_phase_unit);

    -- Sinus Fs/16
    wg_freq        <= integer(0.0625 * c_freq_unit);
    --wg_freq        <= INTEGER(511.0/512.0 * c_freq_unit);
    wg_freq        <= integer(1.0 / 512.0 * c_freq_unit);
    --wg_freq        <= INTEGER(1.0);  -- minimum value, yields Fs/c_freq_unit Hz sinus
    wg_phase       <= integer(0.0 * c_phase_unit);

    -- Sinus Fs/17
--     wg_freq        <= INTEGER(1.0/17.0 * c_freq_unit);
--     wg_phase       <= INTEGER(0.0 * c_phase_unit);

    wg_ampl        <= integer(1.0 * c_ampl_unit);  -- yields amplitude of c_wg_full_scale
--     wg_ampl        <= INTEGER(1.0/REAL(c_wg_full_scale) * c_ampl_unit);  -- yields amplitude of 1
--     wg_ampl        <= INTEGER(3.0/REAL(c_wg_full_scale) * c_ampl_unit);  -- yields amplitude of 3

    wait until rising_edge(clk);  -- align to rising edge
    cur_ctrl       <= wg_ctrl;
    wait for c_clk_period * 200;

    ---------------------------------------------------------------------------
    -- >>> Select the different modes

    -- CALC mode
    wg_mode        <= c_diag_wg_mode_calc;
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected hold OFF)"
      severity ERROR;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    cur_ctrl       <= wg_ctrl;
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected new CALC)"
      severity ERROR;

    wait for c_clk_period * 3000;

    wg_ampl        <= integer(0.5 * c_ampl_unit);  -- change ampl immediately
    wait for c_clk_period * 1;
    cur_ctrl       <= wg_ctrl;
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected new CALC ampl)"
      severity ERROR;

    wait for c_clk_period * 3000;

    wg_phase       <= integer(90.0 * c_phase_unit);  -- no change phase without restart
    wg_freq        <= integer(0.5 / 512.0 * c_freq_unit);  -- no change freq without restart
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected hold CALC phase and freq)"
      severity ERROR;

    wait for c_clk_period * 3000;

    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    cur_ctrl       <= wg_ctrl;
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected new CALC phase and freq)"
      severity ERROR;

    wait for c_clk_period * 3000;

    -- OFF mode
    wg_mode        <= c_diag_wg_mode_off;
    wait for c_clk_period * 1;
    cur_ctrl       <= wg_ctrl;  -- OFF mode takes effect immediately
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected immediately OFF)"
      severity ERROR;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected still OFF)"
      severity ERROR;

    wait for c_clk_period * 200;

    -- SINGLE mode
    wg_mode        <= c_diag_wg_mode_single;
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected hold OFF)"
      severity ERROR;
    for I in 0 to 3 loop
      restart      <= '1';
      wait for c_clk_period * 1;
      restart      <= '0';
      cur_ctrl     <= wg_ctrl;
      wait for c_clk_period * 10;
      assert mon_ctrl = cur_ctrl
        report "Wrong mon_ctrl (expected new SINGLE)"
        severity ERROR;
      wait for c_clk_period * c_buf_nof_dat;
      wait for c_clk_period * 300;
    end loop;

    -- REPEAT mode
    wg_mode        <= c_diag_wg_mode_repeat;
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected hold SINGLE)"
      severity ERROR;
    for I in 0 to 1 loop
      restart        <= '1';
      wait for c_clk_period * 1;
      restart        <= '0';
      cur_ctrl       <= wg_ctrl;
      wait for c_clk_period * 10;
      assert mon_ctrl = cur_ctrl
        report "Wrong mon_ctrl (expected new REPEAT)"
        severity ERROR;
      wait for c_clk_period * c_buf_nof_dat * 5;
      wait for c_clk_period * 200;
    end loop;

    -- OFF mode
    wg_mode        <= c_diag_wg_mode_off;
    wait for c_clk_period * 1;
    cur_ctrl       <= wg_ctrl;  -- OFF mode takes effect immediately, no need for restart
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected immediately OFF)"
      severity ERROR;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    wait for c_clk_period * 10;
    assert mon_ctrl = cur_ctrl
      report "Wrong mon_ctrl (expected still OFF)"
      severity ERROR;

    wait for c_clk_period * 200;

    wait for c_clk_period * 100;
    tb_end         <= '1';
    wait;
  end process;

  u_wideband_wg : entity work.diag_wg_wideband
  generic map (
    -- Wideband parameters
    g_wideband_factor   => g_wideband_factor,
    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w         => g_buf_dat_w,
    g_buf_addr_w        => g_buf_addr_w,
    g_calc_support      => true,
    g_calc_gain_w       => c_wg_gain_w,
    g_calc_dat_w        => g_wg_dat_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst               => '0',
    mm_clk               => '0',

    mm_wrdata            => (others => '0'),
    mm_address           => (others => '0'),
    mm_wr                => '0',
    mm_rd                => '0',
    mm_rdval             => OPEN,
    mm_rddata            => OPEN,

    -- Streaming clock domain
    st_rst               => rst,
    st_clk               => clk,
    st_restart           => restart,

    st_ctrl              => wg_ctrl,
    st_mon_ctrl          => mon_ctrl,

    out_ovr              => out_ovr,
    out_dat              => out_dat,
    out_val              => out_val,
    out_sync             => out_sync
  );

  -- Map wideband WG out_* slv to wg_* arrays to ease interpretation in wave window
  gen_wires : for I in 0 to g_wideband_factor - 1 generate
    wg_ovr(I)  <= out_ovr(                                            g_wideband_factor - I - 1);
    wg_dat(I)  <= out_dat((g_wideband_factor - I) * g_buf_dat_w - 1 downto (g_wideband_factor - I - 1) * g_buf_dat_w);
    wg_val(I)  <= out_val(                                            g_wideband_factor - I - 1);
    wg_sync(I) <= out_sync(                                           g_wideband_factor - I - 1);
  end generate;

  -- View WG output at the sample rate
  p_sample : process(sample_clk)
  begin
    if rising_edge(sample_clk) then
      sample_cnt <= 0;
      -- Count samples for any wg_val, in WG single mode the not all wg_val are active at the end of the block if nof_samples MOD g_wideband_factor /= 0
      if vector_or(wg_val) = '1' and sample_cnt < g_wideband_factor - 1 then
        sample_cnt <= sample_cnt + 1;
      end if;
      sample_ovr  <= wg_ovr(sample_cnt);
      sample_dat  <= wg_dat(sample_cnt);
      sample_val  <= wg_val(sample_cnt);
      sample_sync <= wg_sync(sample_cnt);
    end if;
  end process;
end tb;
