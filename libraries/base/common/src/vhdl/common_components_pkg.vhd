-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;

-- Purpose: Component declarations to check positional mapping
-- Description:
-- Remarks:

package common_components_pkg is
  component common_pipeline is
  generic (
    g_representation : string  := "SIGNED";  -- or "UNSIGNED"
    g_pipeline       : natural := 1;  -- 0 for wires, > 0 for registers,
    g_reset_value    : integer := 0;
    g_in_dat_w       : natural := 8;
    g_out_dat_w      : natural := 9
  );
  port (
    rst     : in  std_logic := '0';
    clk     : in  std_logic;
    clken   : in  std_logic := '1';
    in_clr  : in  std_logic := '0';
    in_en   : in  std_logic := '1';
    in_dat  : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    out_dat : out std_logic_vector(g_out_dat_w - 1 downto 0)
  );
  end component;

  component common_pipeline_sl is
    generic (
      g_pipeline       : natural := 1;  -- 0 for wires, > 0 for registers,
      g_reset_value    : natural := 0;  -- 0 or 1, bit reset value,
      g_out_invert     : boolean := false
    );
    port (
      rst     : in  std_logic := '0';
      clk     : in  std_logic;
      clken   : in  std_logic := '1';
      in_clr  : in  std_logic := '0';
      in_en   : in  std_logic := '1';
      in_dat  : in  std_logic;
      out_dat : out std_logic
    );
  end component;
end common_components_pkg;

package body common_components_pkg is
end common_components_pkg;
