-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, tst_lib, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Test bench for common_reorder_symbol.vhd
-- Usage:
-- > as 3
-- > run -all
--   p_verify self-checks the output of the reorder by using a inverse reorder
-- Remark:
-- . The verification assumes that the reorderings can be inversed, so all
--   inputs are output. The inverse reorder to get back to the original input
--   order can be derived from the DUT reorder(stage 1:c_N) using two reorder
--   instances:
--                            u_inverse_in:          u_inverse_out:
--   c_N = odd  --> inverse = reorder(stage 1:c_N) + identity
--   c_N = even --> inverse = reorder(stage c_N:2) + reorder(stage 1)
-- . An alternative approach is to derive the select settings from a specified
--   input to output mapping list. Manually it is relatively easy to find a
--   suitable select_arr setting. A simple algorithm to find a select_arr
--   setting is to try all possible select_arr settings, which is feasible
--   for practical c_N.

entity tb_common_reorder_symbol is
  generic (
--     g_nof_input    : NATURAL := 3;
--     g_nof_output   : NATURAL := 3;
--     g_symbol_w     : NATURAL := 8;
--     g_select_arr   : t_natural_arr := (3, 3, 3);  --array_init(3, 6)  -- range must fit [c_N*(c_N-1)/2-1:0]
--     g_pipeline_arr : t_natural_arr := (0,0,0,0)  --array_init(0, 5)  -- range must fit [0:c_N]
    g_nof_input    : natural := 5;
    g_nof_output   : natural := 5;
    g_symbol_w     : natural := 8;
    g_select_arr   : t_natural_arr := (3, 3, 3, 3, 3, 3, 3, 3, 3, 3);  -- array_init(3, 6)  -- range must fit [c_N*(c_N-1)/2-1:0]
    g_pipeline_arr : t_natural_arr := (0, 0, 0, 0, 0, 0)  -- array_init(0, 5)  -- range must fit [0:c_N]
  );
end tb_common_reorder_symbol;

architecture tb of tb_common_reorder_symbol is
  constant clk_period        : time := 10 ns;

  -- Stimuli constants
  constant c_frame_len       : natural := 17;
  constant c_frame_sop       : natural := 1;
  constant c_frame_eop       : natural := (c_frame_sop + c_frame_len - 1) mod c_frame_len;

  -- DUT constants
  constant c_N               : natural := largest(g_nof_input, g_nof_output);

  constant c_dut_pipeline    : natural := func_sum(g_pipeline_arr);
  constant c_total_pipeline  : natural := c_dut_pipeline * 3;  -- factor for DUT, inverse DUT in and out

  constant c_nof_select      : natural := c_N * (c_N - 1) / 2;
  constant c_select_w        : natural := 2;  -- fixed 2 bit per X select

  -- Stimuli
  signal tb_end             : std_logic := '0';
  signal rst                : std_logic;
  signal clk                : std_logic := '1';

  -- DUT input
  signal in_select_arr      : t_natural_arr(c_nof_select - 1 downto 0) := g_select_arr;  -- force range [c_N*(c_N-1)/2-1:0]
  signal in_select_vec      : std_logic_vector(c_nof_select * c_select_w - 1 downto 0);
  signal in_data_vec        : std_logic_vector(g_nof_input * g_symbol_w - 1 downto 0);
  signal in_dat             : std_logic_vector(            g_symbol_w - 1 downto 0) := (others => '0');
  signal in_val             : std_logic;
  signal in_sop             : std_logic;
  signal in_eop             : std_logic;
  signal in_sync            : std_logic;

  -- DUT output
  signal reorder_data_vec   : std_logic_vector(g_nof_output * g_symbol_w - 1 downto 0);
  signal reorder_val        : std_logic;
  signal reorder_sop        : std_logic;
  signal reorder_eop        : std_logic;
  signal reorder_sync       : std_logic;

  -- inverse output
  signal inverse_select_arr : t_natural_arr(2 * c_nof_select - 1 downto 0) :=  (others => 0);  -- default reorder2 select is 0 for pass on
  signal inverse_select_vec : std_logic_vector(2 * c_nof_select * c_select_w - 1 downto 0) := (others => '0');
  signal inverse_data_vec   : std_logic_vector(g_nof_output * g_symbol_w - 1 downto 0);
  signal inverse_val        : std_logic;
  signal inverse_sop        : std_logic;
  signal inverse_eop        : std_logic;
  signal inverse_sync       : std_logic;

  -- Verify output
  signal out_data_vec       : std_logic_vector(g_nof_input * g_symbol_w - 1 downto 0);
  signal out_val            : std_logic;
  signal out_sop            : std_logic;
  signal out_eop            : std_logic;
  signal out_sync           : std_logic;

  -- Verify
  signal exp_data_vec       : std_logic_vector(g_nof_input * g_symbol_w - 1 downto 0);
  signal exp_val            : std_logic;
  signal exp_sop            : std_logic;
  signal exp_eop            : std_logic;
  signal exp_sync           : std_logic;
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  tb_end <= '1' when signed(in_dat) = -1 else '0';

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      in_val <= '0';
      in_dat <= (others => '0');
    elsif rising_edge(clk) then
      in_val <= '1';
      in_dat <= std_logic_vector(signed(in_dat) + 1);
    end if;
  end process;

  gen_in_data_vec: for I in g_nof_input - 1 downto 0 generate
    in_data_vec((I + 1) * g_symbol_w - 1 downto I * g_symbol_w) <= INCR_UVEC(in_dat, I);
  end generate;

  in_sop  <= in_val when TO_UINT(in_dat) mod c_frame_len = c_frame_sop else '0';
  in_eop  <= in_val when TO_UINT(in_dat) mod c_frame_len = c_frame_eop else '0';
  in_sync <= '0';

  gen_in_select_vec: for K in c_nof_select - 1 downto 0 generate
    in_select_vec((K + 1) * c_select_w - 1 downto K * c_select_w) <= TO_UVEC(in_select_arr(K), c_select_w);
  end generate;

  -- DUT
  u_reorder_in : entity work.common_reorder_symbol
  generic map (
    g_nof_input    => g_nof_input,
    g_nof_output   => g_nof_output,
    g_symbol_w     => g_symbol_w,
    g_select_w     => c_select_w,
    g_nof_select   => c_nof_select,
    g_pipeline_arr => g_pipeline_arr
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => in_data_vec,
    in_val     => in_val,
    in_sop     => in_sop,
    in_eop     => in_eop,
    in_sync    => in_sync,

    in_select  => in_select_vec,

    out_data   => reorder_data_vec,
    out_val    => reorder_val,
    out_sop    => reorder_sop,
    out_eop    => reorder_eop,
    out_sync   => reorder_sync
  );

  -- inverse DUT
  inverse_select_arr <= func_common_reorder2_inverse_select(c_N, in_select_arr);

  gen_inverse_select_vec: for K in 2 * c_nof_select - 1 downto 0 generate
    inverse_select_vec((K + 1) * c_select_w - 1 downto K * c_select_w) <= TO_UVEC(inverse_select_arr(K), c_select_w);
  end generate;

  u_inverse_in : entity work.common_reorder_symbol
  generic map (
    g_nof_input    => g_nof_output,
    g_nof_output   => g_nof_input,
    g_symbol_w     => g_symbol_w,
    g_select_w     => c_select_w,
    g_nof_select   => c_nof_select,
    g_pipeline_arr => g_pipeline_arr
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => reorder_data_vec,
    in_val     => reorder_val,
    in_sop     => reorder_sop,
    in_eop     => reorder_eop,
    in_sync    => reorder_sync,

    in_select  => inverse_select_vec(c_nof_select * c_select_w - 1 downto 0),

    out_data   => inverse_data_vec,
    out_val    => inverse_val,
    out_sop    => inverse_sop,
    out_eop    => inverse_eop,
    out_sync   => inverse_sync
  );

  u_inverse_out : entity work.common_reorder_symbol
  generic map (
    g_nof_input    => g_nof_output,
    g_nof_output   => g_nof_input,
    g_symbol_w     => g_symbol_w,
    g_select_w     => c_select_w,
    g_nof_select   => c_nof_select,
    g_pipeline_arr => g_pipeline_arr
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => inverse_data_vec,
    in_val     => inverse_val,
    in_sop     => inverse_sop,
    in_eop     => inverse_eop,
    in_sync    => inverse_sync,

    in_select  => inverse_select_vec(2 * c_nof_select * c_select_w - 1 downto c_nof_select * c_select_w),

    out_data   => out_data_vec,
    out_val    => out_val,
    out_sop    => out_sop,
    out_eop    => out_eop,
    out_sync   => out_sync
  );

  -- Verification
  p_verify : process(rst, clk)
  begin
    if rst = '1' then
    elsif rising_edge(clk) then
      if exp_data_vec /= out_data_vec then report "Unexpected out_data_vec" severity ERROR; end if;
      if exp_val      /= out_val      then report "Unexpected out_val"      severity ERROR; end if;
      if exp_sop      /= out_sop      then report "Unexpected out_sop"      severity ERROR; end if;
      if exp_eop      /= out_eop      then report "Unexpected out_eop"      severity ERROR; end if;
      if exp_sync     /= out_sync     then report "Unexpected out_sync"     severity ERROR; end if;
    end if;
  end process;

  -- pipeline data input
  u_out_dat : entity work.common_pipeline
  generic map (
    g_pipeline  => c_total_pipeline,
    g_in_dat_w  => g_nof_input * g_symbol_w,
    g_out_dat_w => g_nof_input * g_symbol_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_data_vec,
    out_dat => exp_data_vec
  );

  -- pipeline control input
  u_out_val : entity work.common_pipeline_sl
  generic map (
    g_pipeline => c_total_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_val,
    out_dat => exp_val
  );

  u_out_sop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => c_total_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sop,
    out_dat => exp_sop
  );

  u_out_eop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => c_total_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_eop,
    out_dat => exp_eop
  );

  u_out_sync : entity work.common_pipeline_sl
  generic map (
    g_pipeline => c_total_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sync,
    out_dat => exp_sync
  );
end tb;
