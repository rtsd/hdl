/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

/* Compile switches. */


/* includes */
#include <stdio.h>
#include "common_types.h"
#include "common_util.h"


/* private constants (#define) */

/* private types */

/* private function protos */

/* private constants (const) */

/* private variables */

/* public variables */

/* public function implements */

/*-----------------------------------------------------------------------------
 * Function name:  UTIL_swapl
 * Parameters:     in_dat  = 4 byte long input word
 * Return:         out_dat = byte swapped word
 * Description:
 *-----------------------------------------------------------------------------
 */
TUInt32 UTIL_swapl(TUInt32 in_dat)
{
  TUInt32  out_dat;
  TUInt8  *pInByte;
  TUInt8  *pOutByte;

  pInByte  = (TUInt8 *)&in_dat;
  pOutByte = (TUInt8 *)&out_dat + 3;

  *pOutByte-- = *pInByte++;
  *pOutByte-- = *pInByte++;
  *pOutByte-- = *pInByte++;
  *pOutByte   = *pInByte;

  return out_dat;
}


/*-----------------------------------------------------------------------------
 * Function name:  UTIL_swaps
 * Parameters:     in_dat  = 2 byte long input half word
 * Return:         out_dat = byte swapped half word
 * Description:
 *-----------------------------------------------------------------------------
 */
TUInt16 UTIL_swaps(TUInt16 in_dat)
{
  TUInt16  out_dat;
  TUInt8  *pInByte;
  TUInt8  *pOutByte;

  pInByte  = (TUInt8 *)&in_dat;
  pOutByte = (TUInt8 *)&out_dat + 1;

  *pOutByte-- = *pInByte++;
  *pOutByte   = *pInByte;

  return out_dat;
}


/* private function implements */
