%-----------------------------------------------------------------------------
%
% Copyright (C) 2018
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, Apr 2018
%
% pfir_coeff_dc_adjust - Adjust the DC response per poly phase of an
%   existing set of PFIR coefficients
%
%   The input coeffIn is an array of N*L integer PFIR coefficients that fit 
%   in nof_bits. The number of polyphase is N and the number of taps per
%   polyphase is L. The order of the input coeffIn is first all L
%   coefficients for phase 0 then for phase 1, and last for phase N-1.
%
%   The coeffIn values are adjusted such that each of the N polyphase FIR
%   sections with L taps each has the same DC response. This then ensures
%   that for DC input the output of all N polyphases will also be DC. The
%   FFT in a polyphase filterbank will then transform this DC to bin 0, and
%   there will be  no affect on the other bins.
%
%   The DC adjustment algorithm first determines the median of the sums
%   per polyphase. For each polyphase the L taps are then DC adjusted
%   to this dc_polyphases_median. If the DC adjustment is > L then first
%   an equal coarse adjustment is made to all L taps. The remining DC
%   adjustment is then < L and applied by adjusting the outer taps by 1.
%   
%   The DC adjusted FIR coefficients are returned via coeffNoDC. The DC
%   adjustment must not cause coefficient overflow. The nof_bits
%   defines the maximum integer coefficient range. If the coeffNoDC does
%   not fit, than the function reduces the input coeffIn by a factor
%   and then recalculates coeffNoDC, until the coefficients all fit within
%   nof_bits.
%
%   The run_pfir_coeff.m shows how this pfir_coeff_adjust.m can be used.
function coeffNoDC = pfir_coeff_dc_adjust(coeffIn, N, L, nof_bits)

iteration = 0;
while true
    iteration = iteration + 1;
    % Automatically equalize the DC response of the polyphases to have flat DC level at FFT input
    % Reshape the coefficients in N polyphases with L taps each.
    q_coeff = reshape(coeffIn, N, L).';
    % The DC response is the sum of the FIR coefficients per polyphase.
    dc_polyphases = sum(q_coeff);
    % The median will be used as aim for the dc adjustment, because most polyphases are already
    % close to the median. For larger N the first and last polyphases deviate the most.
    dc_polyphases_median = median(dc_polyphases);
    % DC adjust each polyphase to the DC median
    for I = 1:N
        polyphase = q_coeff(:,I);
        dc_polyphase = sum(polyphase);
        dc_adjust = dc_polyphase - dc_polyphases_median;
        % First equally adjust all L coefficients by an equal amount of
        % dc_offset >= 0
        dc_offset = floor(abs(dc_adjust)/L);
        % Then adjust the dc_remainder < L, over dc_adjust number of
        % coefficients from the outer to the inner taps of the polyphase
        % by 1
        dc_remainder = abs(dc_adjust) - L*dc_offset;
        % Treat dc_adjust < 0, > 0 separately, nothing to do when dc_adjust = 0
        if dc_adjust > 0
            polyphase = polyphase - dc_offset;
            if dc_remainder > 0
                for J = 1:dc_remainder
                    if mod(J,2)==0
                        polyphase(J) = polyphase(J) - 1;
                    else
                        polyphase(L+1-J) = polyphase(L+1-J) - 1;
                    end
                end
            end
        end
        if dc_adjust < 0
            polyphase = polyphase + dc_offset;
            if dc_remainder > 0
                for J = 1:dc_remainder
                    if mod(J,2)==0
                        polyphase(J) = polyphase(J) + 1;
                    else
                        polyphase(L+1-J) = polyphase(L+1-J) + 1;
                    end
                end
            end
        end
        dc_polyphase = sum(polyphase);
        dc_adjusted = dc_polyphase - dc_polyphases_median;
        if dc_adjusted ~= 0
            % Return with Error messages when dc_adjusted for a polyphase is not 0
            disp(sprintf('Error polyphase %4d : dc_adjust = %4d, dc_offset= %4d, dc_remainder = %4d, dc_adjusted = %4d must be 0', I, dc_adjust, dc_offset, dc_remainder, dc_adjusted));
            return
        end
        q_coeff(:,I) = polyphase;
    end
    coeffNoDC = reshape(q_coeff.', 1, N*L);

    % Check whether the q_coeff are still in range q_min : q_max
    q_full_scale = 2^(nof_bits-1);
    q_max = q_full_scale-1;
    q_no_dc_max = max(coeffNoDC);
    if q_no_dc_max > q_max
        coeffIn = round(coeffIn * q_max / q_no_dc_max);  % Scale coeff with more positive margin to fit DC adjustment
    else
        break;  % Finished DC adjustment of the coefficients
    end
end
disp(sprintf('DC adjusted in %d iteration(s)', iteration));

end
