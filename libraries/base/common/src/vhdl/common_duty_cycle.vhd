-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

--            :<-----------period----------->:
--            :__________                    :_________ .............g_act_lvl
-- dc_out ____|          |___________________|         |________ ...!g_act_lvl
--            :          :
--            :<-active->:

--            :<-s_idle->:<-----s_idle------>:
--            :__________:                   :_________
-- dc_out ____|          |___________________|         |________
--            ^          ^
--            |          |
--            |          s_deassert
--            s_assert

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.common_pkg.all;

entity common_duty_cycle is
  generic (
    g_rst_lvl : std_logic := '0';  -- dc_out level on reset
    g_dis_lvl : std_logic := '0';  -- dc_out level when disabled
    g_act_lvl : std_logic := '1';  -- Level that's DC controlled
    g_per_cnt : positive;  -- Nof clk cycles per period. Note: if the dc_per_cnt input is used, this generic sets the maximum period.
    g_act_cnt : natural  -- Nof clk cycles/period active level
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    dc_per_cnt  : in  std_logic_vector(ceil_log2(g_per_cnt + 1) - 1 downto 0) := TO_UVEC(g_per_cnt, ceil_log2(g_per_cnt + 1));
    dc_act_cnt  : in  std_logic_vector(ceil_log2(g_per_cnt + 1) - 1 downto 0) := TO_UVEC(g_act_cnt, ceil_log2(g_per_cnt + 1));

    dc_out_en   : in  std_logic := '1';
    dc_out      : out std_logic
  );
end;

architecture rtl of common_duty_cycle is
  constant c_cycle_cnt_w : natural := ceil_log2(g_per_cnt + 1);

  type t_state_enum is (s_idle, s_assert, s_deassert);

  type t_reg is record
    state     : t_state_enum;
    cycle_cnt : natural range 0 to g_per_cnt;
    dc_pulse  : std_logic;
  end record;

  signal r, nxt_r : t_reg;
begin
  p_comb : process(rst, dc_out_en, dc_per_cnt, dc_act_cnt, r)
    variable v : t_reg;
  begin
    v := r;

    if TO_UVEC(r.cycle_cnt, c_cycle_cnt_w) = dc_per_cnt then
      v.cycle_cnt := 1;
    else
      v.cycle_cnt := r.cycle_cnt + 1;
    end if;

    case r.state is

      when s_idle     => v.state := s_idle;
                         if TO_UVEC(r.cycle_cnt, c_cycle_cnt_w) = dc_act_cnt or dc_act_cnt = TO_UVEC(0, c_cycle_cnt_w) then
                           v.state := s_deassert;
                         elsif TO_UVEC(r.cycle_cnt, c_cycle_cnt_w) = dc_per_cnt or dc_act_cnt = dc_per_cnt then
                           v.state := s_assert;
                         end if;

      when s_assert   => v.dc_pulse := g_act_lvl;
                         if TO_UVEC(r.cycle_cnt, c_cycle_cnt_w) = dc_act_cnt and dc_act_cnt < dc_per_cnt then
                           v.state := s_deassert;
                         end if;

      when s_deassert => v.dc_pulse := not(g_act_lvl);
                         if TO_UVEC(r.cycle_cnt, c_cycle_cnt_w) = dc_per_cnt and dc_act_cnt /= TO_UVEC(0, c_cycle_cnt_w) then
                           v.state := s_assert;
                         end if;
    end case;

    if rst = '1' then
      v.state     := s_idle;
      v.cycle_cnt := 0;
      v.dc_pulse  := g_rst_lvl;
    end if;

    nxt_r <= v;
  end process;

  p_seq : process(clk)
  begin
    if rising_edge(clk) then r <= nxt_r; end if;
  end process;

  dc_out <= r.dc_pulse when dc_out_en = '1' else g_dis_lvl when rst = '0' else g_rst_lvl;
end rtl;
