-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Adapt miso.waitrequest latency from 1 to 0, to support pipelining
--          of the waitrequest flow control
-- Description:
--   Wraps common_rl_decrease.vhd.
--   The common_rl_decrease.vhd latency adapter FIFO buffers the in_mosi, to
--   create time to compensate for the pipeline of the in_mosi.waitrequest.
--   When the in_mosi.waitrequest goes high, then this FIFO buffer can hold
--   the in_mosi input that may still arrive, due to that the master at the
--   input only notices the in_mosi.waitrequest from the output slave one
--   cycle later due to the pipelining.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mm_latency_adapter is
  generic (
    g_adapt       : boolean := true  -- default when TRUE then decrease sink RL 1 to source RL 0, else then implement wires
  );
  port (
    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;
    -- MM input RL = 1
    in_mosi       : in  t_mem_mosi;
    in_miso       : out t_mem_miso;
    -- MM output RL = 0
    out_mosi      : out t_mem_mosi;
    out_miso      : in  t_mem_miso
  );
end mm_latency_adapter;

architecture str of mm_latency_adapter is
  -- Sum of all t_mem_mosi fields widths (synthesis will optimize away unused address and data bits)
  constant c_data_w  : natural := c_mem_address_w +  c_mem_data_w + 2;  -- 32 + 72 + 1 (wr) + 1 (rd) = 106

  signal in_waitrequest  : std_logic;
  signal in_data         : std_logic_vector(c_data_w - 1 downto 0);
  signal in_val          : std_logic;
  signal in_ready        : std_logic;
  signal out_ready       : std_logic;
  signal out_data        : std_logic_vector(c_data_w - 1 downto 0);
  signal out_val         : std_logic;
begin
  in_data <= func_slv_concat(in_mosi.address, in_mosi.wrdata, slv(in_mosi.wr), slv(in_mosi.rd));
  in_val <= in_mosi.wr or in_mosi.rd;

  p_miso : process(out_miso, in_waitrequest)
  begin
    in_miso <= out_miso;
    --in_miso.rdval <= out_miso.rdval AND NOT in_waitrequest;
    in_miso.waitrequest <= in_waitrequest;
  end process;

  -- Account for opposite meaning of waitrequest and ready
  in_waitrequest <= not in_ready;
  out_ready      <= not out_miso.waitrequest;

  u_rl : entity common_lib.common_rl_decrease
  generic map (
    g_adapt   => g_adapt,
    g_dat_w   => c_data_w
  )
  port map (
    rst           => mm_rst,
    clk           => mm_clk,
    -- ST sink: RL = 1
    snk_out_ready => in_ready,
    snk_in_dat    => in_data,
    snk_in_val    => in_val,
    -- ST source: RL = 0
    src_in_ready  => out_ready,
    src_out_dat   => out_data,
    src_out_val   => out_val
  );

  out_mosi.address <=    func_slv_extract(c_mem_address_w, c_mem_data_w, 1, 1, out_data, 0);
  out_mosi.wrdata  <=    func_slv_extract(c_mem_address_w, c_mem_data_w, 1, 1, out_data, 1);
  out_mosi.wr      <= sl(func_slv_extract(c_mem_address_w, c_mem_data_w, 1, 1, out_data, 2));
  out_mosi.rd      <= sl(func_slv_extract(c_mem_address_w, c_mem_data_w, 1, 1, out_data, 3));
end str;
