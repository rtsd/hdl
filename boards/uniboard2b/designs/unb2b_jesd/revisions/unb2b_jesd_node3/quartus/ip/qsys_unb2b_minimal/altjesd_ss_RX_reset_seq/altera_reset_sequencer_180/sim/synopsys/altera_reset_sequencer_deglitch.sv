// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
VKqND1sTGonjnmWrfwhEPhC9HEe+QcqrecbxuDaU2cxEZKDXQSzeJgABOV+zawOW
SUS0g/pGA4O5GrGFk02iKH4wYQaAT25zD2Q+ekPDDuJUhzlJtyB7zjb3CDVROv6N
lELCe1lhYvCIbDIln084B51162w5MZimlppzQ0uZfxU=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 1792 )
`pragma protect data_block
2jwmNBqVrBnovMC6xMJX8Kkv+QYYnnGSVLLCvi3UTk1zEGTjbiqBKfpN9ujYn2Dn
+r1TQnnAjqo+iIw8QcX9lSq300MJdSAYFOm/yC/8CPP9PqAUMdF8FvFG8THGCX36
xP95bvs4zPPeVG88wgJtVpgUMb09IyHqaAfmvfB9gaHAqZFXr8iyPTiDF+Yy+0EP
IRNv9Gft6prIbKm5e8B1hK3rNQ9uGso6mpZWH/wdCkRj59kFcRO8xdep4HYUQWXX
pu3PEoyZDQmLARedL5gxK8ZqiF4s2VC5KAclEp3cf5ixu66QNmQQQ/UDhhQiemlM
i1tRJGVTBVVFvrPSSw/K/2ooC0DOeCVXNHfWh4LBBDTzhVOlEK7RaicqsUhQmLYb
FMY1RrfnDLUm8EAXfrNnRkyl7V/sYEQi7JkvqMRljXkUaXGBALTr2Dmxrer13qFV
itCPflZX2Zz9V7FNuVwkaO3HcE28aUL1qdwrDY1+2llnpVOexNlKkAze+ZYh5gp3
jHNNyEh2OVmLRt9YVs6A2WOh1nR9iN4raDWSEAj/R0LENb+20XvgCWwyIMyDQ3ab
QwpFuyn08TBQ8mUvK+NLUhI+zBVzEFfF+qxbhJSZTgpVsR8Smw+OIkPwglTzYwzV
wgq3NZuQ2z0W5AUax6ZiA6g0+PytVan3e1OGTra7+yrIqeTEf83RuHJxTuep3F85
tMp7JdT4t3gEe8L1ugs1PLavpN02IiAzar8QdHd857I8yNdT8by9R71ZVyLyzdLA
rKhiLCH0RMXyo7wJ/vguSBmmMgDOn4fj1ForHvDkQ9zXKN8sIILN+5O9JX45CfN3
1qi/HAuEYlvzB3WcUGlXS32hMmHc8yzdaoJB7j6Msj6VOGLWGHvmrKtTfFN39rxe
wiNTjFFE7V6cpEWo8rFI0WtQK4pJcVTvG+LcFk+AKtP4uhhAq0vNb4Nzxhk0nIOh
6zKRjibnn1ZMm8BcGnSuAcUlW9Fce0DoMlGRc1zh+l3cXl/LH4GBLtaQaOv+68f5
CUlIAHw4zcuIWEGmMGV+FGXzqGcg8WumIbl9C2ZiVhlc3gmKIma0rWU2fd1vgeSw
uc4O1eiUQ1mDfxURiKenOGwSE+ntQTWvQPlJkhxET/SwcN+U+0MbaQ05/t789AYl
kTdof2sgOedtP5p7jXAmovY0xOwBBjtaGUxDbdBDe1ly1mk2wsnD0hVVu2i/AL+a
cRhfe/aWAL5UXDOgA9FH0jo+hd0JjJqNKOIlL7tzn+0o4urToL2Df6TGvChmSflZ
lKsyO1ryjaV5EPD3ZLUysCaOKLBRnqDFW/M+MvBwxoiFVYHlMoSpkIdx8Fb4S29H
9kQlVWCe+Hc1o+Igl86go0nXTUWlzAVt35jzEGn/fl05r3OQKjkMY+SARSCDoeWi
kR3yM3hSq51nyFwFVDbX/9Yv6UmTiR3MGqaEt4POCOW/VdDo/ZFD2aOKcqCyjkNV
SCdSgQA8e51yAdm+6teao1/xeFj+2J/mOEmPGR7pgbBlwjdTiD6pYwDmhCOXttBt
/hi7p/SxGbes/cDO8BPtD9VVo1urxhf+VzX5sYXGtPJHDYzUA4u9Y6huL1B3EwUh
QhMBsHnJwoFSnWPetUies1lHXS6Je2gitBK8i6j9YgioadB/6r7phZPWTfv+H2sK
IlowH+pMEbiqKkBnBl6tG5j1CfE27+yYsq1htnxXBrwFUbaPyScmokHpmSu+BtRN
kzjvtPBN5Ld3KIfHYVBDVPa+EnfGJ8txA2rw9v4e6yDDZAAQeywkSrQHGA8CQ8s7
OSSSyYNDEi+CPoBl6pN51KsijWPNbzLvmvuIQM8h4s/2X0KFvVPD+V8uBiKkYq3V
PVPLU0V/sW13QimMYtINZXc51poKET1gTrmP0oGS4P10XHYVIGClmhjsNaS96JLW
wG7nkUaxhJuro2vdlvUyrRotT+i3ox01BuRKAxYXtOX/eLhkTE+vF9EZBSc+QJtk
sMmb2XnIJkU58skP3ggIgKdMKzAR1lpMaC1PHJB/u5Az41e1IQZ7Of1mfD61hIgX
vFTPW87JOu5/XYY+KoLT7DbDal/15MB7z9efSO3KJQ1d0FIBD7cdBLwICXufvRha
Dc+5SAUiAlyucKheGpLm8/AkqSpMIcUjCWk1EEYn3ZwdzQPPySKqXvHNJT1ZNdk7
cCBoqFjMaWKgfFoYIQj9dg66dRASdXSDRvX3vhJb0pQoWRMO/ZHLEcHy5us9Ccw2
qVbcURXY4LDGd2w/Whu7v1PC/JIEBBOpzADEgZyLBUDdrL3CLxUyXnG6UnTo/kLJ
S/5QErCBOQMmGMf28sSm3dmcBSybwYI7PZqSB+PGQboOTjHWS5CCGmqinmtVHuBH
oHCEovQgqA6rMkQL4WGLuw==

`pragma protect end_protected
