-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: P. Donker

-- Purpose:
-- . SDP info register
-- Description:
--
-- https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD
--
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.sdp_pkg.all;

entity sdp_info_reg is
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock

    dp_clk     : in  std_logic;
    dp_rst     : in  std_logic;

    reg_mosi : in  t_mem_mosi;
    reg_miso : out t_mem_miso;

    -- sdp info
    sdp_info_ro : in  t_sdp_info;  -- ro = read only
    sdp_info    : out t_sdp_info
  );
end sdp_info_reg;

architecture str of sdp_info_reg is
  constant c_field_arr : t_common_field_arr(8 downto 0) :=
      ( (field_name_pad("antenna_field_index"),     "RW",  6, field_default(0)),  -- = station_info[15:10]
        (field_name_pad("station_id"),              "RW", 10, field_default(0)),  -- = station_info[9:0]
        (field_name_pad("antenna_band_index"),      "RW",  1, field_default(0)),
        (field_name_pad("observation_id"),          "RW", 32, field_default(0)),
        (field_name_pad("nyquist_zone_index"),      "RW",  2, field_default(0)),
        (field_name_pad("f_adc"),                   "RO",  1, field_default(0)),
        (field_name_pad("fsub_type"),               "RO",  1, field_default(0)),
        (field_name_pad("beam_repositioning_flag"), "RW",  1, field_default(0)),
        (field_name_pad("block_period"),            "RO", 16, field_default(0)) );

  signal mm_fields_in  : std_logic_vector(field_slv_in_len(c_field_arr) - 1 downto 0);
  signal mm_fields_out : std_logic_vector(field_slv_out_len(c_field_arr) - 1 downto 0);

  signal sdp_info_rd : t_sdp_info;
  signal sdp_info_wr : t_sdp_info;
begin
  sdp_info    <= sdp_info_rd;

  p_sdp_info_rd : process(sdp_info_wr, sdp_info_ro)
  begin
    -- default write assign all fields
    sdp_info_rd <= sdp_info_wr;

    -- overrule the read only fields
    sdp_info_rd.f_adc              <= sdp_info_ro.f_adc;
    sdp_info_rd.fsub_type          <= sdp_info_ro.fsub_type;
    sdp_info_rd.block_period       <= sdp_info_ro.block_period;
  end process;

  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    g_use_slv_in_val  => false,  -- use FALSE to save logic when always slv_in_val='1'
    g_field_arr       => c_field_arr
  )
  port map (
    mm_clk     => mm_clk,
    mm_rst     => mm_rst,

    mm_mosi    => reg_mosi,
    mm_miso    => reg_miso,

    slv_clk    => dp_clk,
    slv_rst    => dp_rst,

    slv_in     => mm_fields_in,
    slv_in_val => '1',

    slv_out    => mm_fields_out
  );

  -- add "RO" fields to mm_fields
  mm_fields_in(field_hi(c_field_arr, "f_adc") downto
               field_lo(c_field_arr, "f_adc")) <= slv(sdp_info_rd.f_adc);
  mm_fields_in(field_hi(c_field_arr, "fsub_type") downto
               field_lo(c_field_arr, "fsub_type")) <= slv(sdp_info_rd.fsub_type);
  mm_fields_in(field_hi(c_field_arr, "block_period") downto
               field_lo(c_field_arr, "block_period")) <= sdp_info_rd.block_period;

  -- get "RW" fields from mm_fields
  sdp_info_wr.antenna_field_index     <= mm_fields_out(field_hi(c_field_arr, "antenna_field_index") downto
                                                       field_lo(c_field_arr, "antenna_field_index"));
  sdp_info_wr.station_id              <= mm_fields_out(field_hi(c_field_arr, "station_id") downto
                                                       field_lo(c_field_arr, "station_id"));
  sdp_info_wr.antenna_band_index      <= sl(mm_fields_out(field_hi(c_field_arr, "antenna_band_index") downto
                                                          field_lo(c_field_arr, "antenna_band_index")));
  sdp_info_wr.observation_id          <= mm_fields_out(field_hi(c_field_arr, "observation_id") downto
                                                       field_lo(c_field_arr, "observation_id"));
  sdp_info_wr.nyquist_zone_index      <= mm_fields_out(field_hi(c_field_arr, "nyquist_zone_index") downto
                                                       field_lo(c_field_arr, "nyquist_zone_index"));
  sdp_info_wr.beam_repositioning_flag <= sl(mm_fields_out(field_hi(c_field_arr, "beam_repositioning_flag") downto
                                                          field_lo(c_field_arr, "beam_repositioning_flag")));
end str;
