-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- Test bench for dp_block_validate_bsn_at_sync.
-- Description:
-- Verifies the output sosi of the DUT with the expected sosi.
-- The TB also reads the register values via MM and verifies them against the
-- expected values.
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_block_validate_bsn_at_sync is
  generic (
    g_nof_blocks_per_sync  : natural := 5;
    g_nof_data_per_blk     : natural := 6;
    g_bsn_init             : natural := 7  -- >= g_nof_blocks_per_sync for discarded sync, < g_nof_blocks_per_sync for no discarded sync.
  );
end tb_dp_block_validate_bsn_at_sync;

architecture tb of tb_dp_block_validate_bsn_at_sync is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_dp_clk_period  : time := 5 ns;
  constant c_mm_clk_period  : time := 10 ns;

  constant c_dut_pipeline   : natural := 1;
  constant c_gap_size       : natural := 4;
  constant c_nof_sync       : natural := 5;
  constant c_nof_blk        : natural := g_nof_blocks_per_sync * c_nof_sync;
  -- The u_stimuli_in creates counter data in stimuli_sosi.channel. Therefore
  -- choose some c_check_channel value (> 0, < c_nof_blk) that will occur in
  -- the future. Hence this c_check_channel value will occur once in the tb,
  -- because stimuli_sosi.channel keeps incrementing. Choosing c_check_channel
  -- such that it corresponds with a channel from stimuli on a sync pulse.
  constant c_check_channel  : natural := g_nof_blocks_per_sync;  -- can be c_check_channel MOD g_nof_blocks_per_sync = 0 but not larger than c_nof_blk.

  signal dp_clk             : std_logic := '1';
  signal mm_clk             : std_logic := '1';
  signal rst                : std_logic := '1';
  signal tb_end             : std_logic := '0';

  signal stimuli_end        : std_logic;
  signal stimuli_sosi       : t_dp_sosi;
  signal stimuli_siso       : t_dp_siso := c_dp_siso_rdy;
  signal bs_sosi            : t_dp_sosi;
  signal bs_siso            : t_dp_siso := c_dp_siso_rdy;
  signal stimuli_cnt_reg    : natural;
  signal stimuli_cnt        : natural;
  signal verify_sosi        : t_dp_sosi;
  signal verify_siso        : t_dp_siso := c_dp_siso_rdy;
  signal reference_cnt      : natural;
  signal reference_cnt_reg  : natural;
  signal reference_sosi     : t_dp_sosi;
  signal reference_siso     : t_dp_siso := c_dp_siso_rdy;
  signal reg_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso           : t_mem_miso := c_mem_miso_rst;
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  rst <= '1', '0' after c_dp_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  -- Generate in_sosi with data frames
  u_stimuli_in : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => g_nof_blocks_per_sync,
    g_nof_repeat  => c_nof_blk,
    g_pkt_len     => g_nof_data_per_blk,
    g_pkt_gap     => c_gap_size,
    g_channel_init => 0,
    g_bsn_init    => TO_DP_BSN(0)
  )
  port map (
    rst               => rst,
    clk               => dp_clk,

    -- Generate stimuli
    src_in            => stimuli_siso,
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  -- Generate bs_sosi with data frames
  u_stimuli_bs : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => g_nof_blocks_per_sync,
    g_nof_repeat  => c_nof_blk,
    g_pkt_len     => g_nof_data_per_blk,
    g_pkt_gap     => c_gap_size,
    g_channel_init => 0,
    g_bsn_init    => TO_DP_BSN(g_bsn_init)
  )
  port map (
    rst               => rst,
    clk               => dp_clk,

    -- Generate stimuli
    src_in            => bs_siso,
    src_out           => bs_sosi,

    -- End of stimuli
    tb_end            => open
  );
  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dut : entity work.dp_block_validate_bsn_at_sync
  generic map (
    g_check_channel => c_check_channel
  )
  port map (
    dp_rst       => rst,
    dp_clk       => dp_clk,

    mm_rst       => rst,
    mm_clk       => mm_clk,
    -- ST sink
    in_sosi      => stimuli_sosi,
    bs_sosi      => bs_sosi,
    -- ST source
    out_sosi     => verify_sosi,

    reg_mosi     => reg_mosi,
    reg_miso     => reg_miso
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_dut_pipeline
  )
  port map (
    rst         => rst,
    clk         => dp_clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => stimuli_sosi,
    -- ST source
    src_in      => reference_siso,
    src_out     => reference_sosi
  );

  reference_cnt_reg <= reference_cnt when rising_edge(dp_clk);
  reference_cnt     <= reference_cnt_reg + 1 when reference_sosi.sync = '1' else reference_cnt_reg;

  p_verify : process(dp_clk)
  variable v_valid_blk : boolean := true;
  begin
    if rising_edge(dp_clk) then
      if reference_sosi.sop = '1' then  -- Decide for each block if it should be valid.
        -- A block can only be discarded if its channel field corresponds with the check_channel.
        if g_bsn_init >= g_nof_blocks_per_sync and TO_UINT(reference_sosi.channel) = c_check_channel then
          v_valid_blk := false;
        else
          v_valid_blk := true;
        end if;
      end if;

      if v_valid_blk then  -- we expect a block
        assert verify_sosi = reference_sosi
          report "Unexpected difference between in / out sosi"
          severity ERROR;
      else  -- we expect no block
        assert verify_sosi.valid = '0'
          report "Wrong, valid is not '0' which is unexpected."
          severity ERROR;
        assert verify_sosi.sop = '0'
          report "Wrong, sop is not '0' which is unexpected."
          severity ERROR;
        assert verify_sosi.eop = '0'
          report "Wrong, eop is not '0' which is unexpected."
          severity ERROR;
      end if;
    end if;
  end process;

  p_verify_mm : process
  begin
    proc_common_wait_until_lo_hi(dp_clk, stimuli_end);
    proc_common_wait_some_cycles(dp_clk, c_dut_pipeline + 1);
    proc_common_wait_some_cycles(mm_clk, 1);
    proc_mem_mm_bus_rd(1, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    assert c_nof_sync = TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0))
      report "Wrong total sync count"
      severity ERROR;

    proc_mem_mm_bus_rd(0, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    if g_bsn_init >= g_nof_blocks_per_sync then  -- should have 1 discarded sync
      assert 1 = TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0))
        report "Wrong discarded sync count"
        severity ERROR;
    else  -- 0 discarded sync
      assert 0 = TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0))
        report "Wrong discarded sync count"
        severity ERROR;
    end if;
    proc_common_wait_some_cycles(dp_clk, 100);
    tb_end <= '1';
    wait;
  end process;
end tb;
