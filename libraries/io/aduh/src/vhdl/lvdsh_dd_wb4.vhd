-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Handle the ADU-BN double data rate LVDS input interface with auto
--          sample phase adjust for fixed g_wb_factor=4.
-- Description:
--   The in_clk is a double data rate clock as indicated by c_dd_factor=2. The
--   DDIO elements near the pins capture the in_dat using the in_clk into two
--   parts in_dat_hi and in_dat_lo.
--   The dp_clk runs at a c_rx_factor=2 lower rate than the in_clk. A mixed
--   width FIFO doubles the read data width of dp_dat.
--   The dp_clk must be locked to the in_clk to avoid FIFO overflow and to be
--   able to adjust for the g_wb_factor=4 sample phase uncertainty. With ADU
--   the in_clk is the 800M sample clock divided by c_dd_factor=2 and the
--   dp_clk is the sample clock divided by g_wb_factor=4.
--   The phase of the in_clk depends on the divider phase of the ADC. The phase
--   of the dp_clk with respect to the in_clk depends on the divide by 2 that
--   is done by the mixed width FIFO. The in_clk and the dp_clk have a fixed
--   but unknown phase relation between 0 and 3 = g_wb_factor-1 samples. The
--   clock domain crossing from in_clk domain to dp_clk domain via the dual
--   clock FIFO can also cause an extra uncertainty of 1 dp_clk cycle, so 4
--   samples.
--   An wb_sync pulse created in the dp_clk domain and read back via the FIFO
--   is used to measure the actual in_clk - dp_clk phase relation (0, 1, 2
--   or 3 sampes) and to measure the FIFO latency (integer multiple of 4
--   samples). The phase offset and FIFO latency are then compensated for to
--   ensure the same sample time t0 is captured by the same dp_clk cycle in all
--   nodes. If the FIFO has the same latency for wb_sync - dp_sync then that
--   means that t0 is aligned across all nodes, because dp_clk has the same
--   phase in all nodes. The phase relation becomes fixed independent of the
--   design that synthesizes it and also independent of system (ADU-BN) power
--   up timing.
--   It is important to maintain this phase relation inside the FPGA. This is
--   taken care of thanks to the synchronous clock tree network inside an FPGA
--   that is balanced such that the clock has the same phase at any location
--   in the FPGA. The wb_sync pulse setup time from the dp_clk domain to the
--   in_clk domain needs to be as fixed as possible, independent of how it was
--   synthesized. This can be achieved by placing a logic lock region on the
--   common_ddreg instance that takes care of the clock domain crossing. By
--   forcing the logic to be placed within one ALM the setup time becomes
--   sufficiently constant (delta ~< 100 ps) because the physical paths as
--   short as feasible.
-- Remarks:
-- . Input buffer delays need to be set via constraints in the synthesis file.
-- . The dp_dat output is big endian meaning that the first input data sample
--   appears in the MS symbol. Using big endian format is conform the DP
--   streaming interface.
-- . The lvdsh_dd_wb4 avoids using a state machine with more than 2 states,
--   because the more complex the state machine is the more difficult it is
--   to ensure that it can not get into a dead-lock situation. Instead
--   implementation uses independ processes that take care of a single task.

entity lvdsh_dd_wb4 is
  generic (
    g_sim               : boolean := false;
    g_sim_phase         : natural := 0;  -- range 0:3 (= g_wb_factor-1)
    g_use_in_clk_rst    : boolean := false;
    g_wb_factor         : natural := 4;  -- fixed wideband factor 4 = c_rx_factor*c_dd_factor
    g_in_dat_w          : natural := 8  -- nof PHY data bits
  );
  port (
    -- PHY input interface
    in_clk_rst    : out std_logic;
    in_clk        : in  std_logic;
    in_dat        : in  std_logic_vector(g_in_dat_w - 1 downto 0);  -- input samples [t0], [t1], [t2], [t3], [t4], [t5], [t6], [t7], ... --> time

    -- DD --> Rx domain interface at in_clk rate or g_dp_factor lower rate (via FIFO)
    dp_rst        : in  std_logic := '1';
    dp_clk        : in  std_logic := '1';
    dp_clkq       : in  std_logic := '0';
    dp_dat        : out std_logic_vector(g_wb_factor * g_in_dat_w - 1 downto 0);  -- big endian output samples [t0, t1, t2, t3], [t4, t5, t6, t7], ...
    dp_val        : out std_logic;

    -- Rx status monitor
    dp_sync_phase : out natural range 0 to 2 * g_wb_factor - 1;  -- valid dclk phases are 0:3 because g_wb_factor=4, however the detection need twice this range
    dp_status     : out std_logic_vector(c_word_w - 1 downto 0);  -- extra status information for debug
    dp_locked     : out std_logic;
    dp_stable     : out std_logic;
    dp_stable_ack : in  std_logic := '0'
  );
end lvdsh_dd_wb4;

architecture str of lvdsh_dd_wb4 is
  -- Debug constants for development only, default FALSE for functional
  constant c_use_dbg_dat        : boolean := false;
  constant c_swap_in_hi_lo      : boolean := false;
  constant c_tsetup_delay_in_hi : boolean := false;
  constant c_tsetup_delay_in_lo : boolean := false;

  constant c_dd_factor          : natural := 2;  -- fixed double data rate factor
  constant c_rx_factor          : natural := 2;  -- fixed for g_wb_factor = c_rx_factor*c_dd_factor = 4
  constant c_wb_sync_latency    : natural := 16;  -- nof dp_clk cycles from getting wb_sync back via dp_sync
  constant c_wb_sync_period     : natural := 32;  -- nof dp_clk cycles for wb_sync period, must be > c_wb_sync_latency
  constant c_wb_sync_timeout    : natural := 1024;  -- nof dp_clk cycles for wb_sync timeout, must be >>> c_wb_sync_latency
  constant c_dp_fifo_margin     : natural := 16;  -- rd side, almost full or almost empty margin
  constant c_dp_fifo_size       : natural := c_wb_sync_latency + c_dp_fifo_margin;  -- rd side
  constant c_in_fifo_size       : natural := c_rx_factor * c_dp_fifo_size;  -- wr side

  constant c_wb_cnt_w           : natural := ceil_log2(c_wb_sync_timeout + 1);

  constant c_wb_sync_w          : natural := 1;
  constant c_sync_in_dat_w      : natural := c_wb_sync_w + g_in_dat_w;

  constant c_in_rst_extend_w    : natural := 8;
  constant c_in_rst_delay_len   : natural := 4;  -- choose even value to fit expected g_sim_phase

  constant c_fifo_wr_dat_w      : natural := c_dd_factor * c_sync_in_dat_w;
  constant c_fifo_rd_dat_w      : natural := c_rx_factor * c_fifo_wr_dat_w;

  constant c_rx_sync_w          : natural := c_rx_factor * c_dd_factor * c_wb_sync_w;  -- = 4*1
  constant c_rx_dat_w           : natural := c_rx_factor * c_dd_factor * g_in_dat_w;

  type t_reg is record
    lock_state        : natural range 0 to 255;
    status            : std_logic_vector(c_word_w - 1 downto 0);
    be_sync           : std_logic_vector(c_rx_sync_w - 1 downto 0);
    be_dat            : std_logic_vector(c_rx_dat_w - 1 downto 0);
    sync_phase        : natural range 0 to 2 * g_wb_factor - 1;  -- valid dclk phases are 0:3 because g_wb_factor=4, however the detection need twice this range
    dat_phase         : natural range 0 to g_wb_factor - 1;  -- valid dclk phases are 0:3 because g_wb_factor=4
    rx_sync           : std_logic_vector(c_rx_sync_w - 1 downto 0);
    rx_dat            : std_logic_vector(c_rx_dat_w - 1 downto 0);
    rx_val            : std_logic;
    dp_sync           : std_logic;
    dp_fifo_rd_req    : std_logic;
    dp_in_rst_req     : std_logic;
    dp_locked         : std_logic;
  end record;

  -- DD clock domain (in_clk)
  signal in_sync_hi_cap      : std_logic;
  signal in_sync_lo_cap      : std_logic;
  signal in_sync_hi          : std_logic;
  signal in_sync_lo          : std_logic;

  signal in_dat_hi           : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_dat_lo           : std_logic_vector(g_in_dat_w - 1 downto 0);

  signal in_fifo_wr_req      : std_logic := '0';
  signal in_fifo_wr_dat      : std_logic_vector(c_fifo_wr_dat_w - 1 downto 0);
  signal in_fifo_wrusedw     : std_logic_vector(ceil_log2(c_in_fifo_size) - 1 downto 0);
  signal dp_fifo_rdusedw     : std_logic_vector(ceil_log2(c_dp_fifo_size) - 1 downto 0);

  -- Rx-DD-Rx clock domain
  signal dp_in_clk_rst       : std_logic_vector(0 downto 0);
  signal i_in_clk_rst        : std_logic_vector(0 downto 0);

  signal dp_in_rst_ext       : std_logic;
  signal in_rst_cap          : std_logic;
  signal in_rst              : std_logic;
  signal dp_rst_cap          : std_logic;
  signal dp_cnt_rst          : std_logic;
  signal dp_cnt_clr          : std_logic;
  signal wb_cnt_clr          : std_logic;
  signal wb_cnt              : std_logic_vector(c_wb_cnt_w - 1 downto 0);
  signal wb_sync             : std_logic;

  -- Rx clock domain (dp_clk = rd_clk) for DSP
  signal r, nxt_r            : t_reg;

  signal dp_fifo_rd_dat      : std_logic_vector(c_fifo_rd_dat_w - 1 downto 0);
  signal dp_fifo_rd_val      : std_logic;
  signal le_sync             : std_logic_vector(c_rx_sync_w - 1 downto 0);
  signal le_dat              : std_logic_vector(c_rx_dat_w - 1 downto 0);
  signal be_sync             : std_logic_vector(c_rx_sync_w - 1 downto 0);
  signal be_dat              : std_logic_vector(c_rx_dat_w - 1 downto 0);

  signal dbg_dat             : std_logic_vector(c_rx_dat_w - 1 downto 0);

  -- DD phase measurement
  signal dd_phase            : std_logic;
  signal prev_dd_phase       : std_logic;
  signal dd_phase_det        : std_logic;
begin
  -- Map outputs
  in_clk_rst <= i_in_clk_rst(0) when g_use_in_clk_rst = true else '0';

  dp_sync_phase <= r.sync_phase;
  dp_status     <= r.status;
  dp_locked     <= r.dp_locked;
  dp_dat        <= r.rx_dat when c_use_dbg_dat = false else dbg_dat;
  dp_val        <= r.rx_val;

  p_dbg_dat : process(r, wb_cnt)
  begin
    dbg_dat <= r.rx_dat;
    dbg_dat( 7 downto  0) <= offset_binary(be_sync & TO_UVEC(r.sync_phase, 4));
    dbg_dat(15 downto  8) <= offset_binary(TO_UVEC(r.lock_state, 8));
    dbg_dat(23 downto 16) <= offset_binary(            wb_cnt(           7 downto 0));
    dbg_dat(31 downto 24) <= offset_binary(RESIZE_UVEC(wb_cnt(c_wb_cnt_w - 1 downto 8), 8));
  end process;

  -- Registers
  p_dp_clk : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      r.lock_state        <= 0;
      r.status            <= (others => '0');
      r.be_sync           <= (others => '0');
      r.be_dat            <= (others => '0');
      r.sync_phase        <= 0;
      r.dat_phase         <= 0;
      r.rx_sync           <= (others => '0');
      r.rx_dat            <= (others => '0');
      r.rx_val            <= '0';
      r.dp_sync           <= '0';
      r.dp_fifo_rd_req    <= '1';
      r.dp_in_rst_req     <= '1';
      r.dp_locked         <= '0';
    elsif rising_edge(dp_clk) then
      r <= nxt_r;
    end if;
  end process;

  -- Reset input section when lock is lost
  u_common_pulse_extend : entity common_lib.common_pulse_extend
  generic map (
    g_rst_level    => '1',
    g_p_in_level   => '1',
    g_ep_out_level => '1',
    g_extend_w     => c_in_rst_extend_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    p_in    => r.dp_in_rst_req,
    ep_out  => dp_in_rst_ext
  );

  u_common_async_in : entity common_lib.common_async
  generic map (
    g_rst_level => '1',
    g_delay_len => c_in_rst_delay_len
  )
  port map (
    rst  => dp_in_rst_ext,
    clk  => in_clk,
    din  => '0',
    dout => in_rst_cap
  );

  u_common_async_dp : entity common_lib.common_async
  generic map (
    g_rst_level => '1',
    g_delay_len => c_in_rst_delay_len
  )
  port map (
    rst  => in_rst_cap,
    clk  => dp_clk,
    din  => '0',
    dout => dp_rst_cap
  );

  gen_hw : if g_sim = false generate
    in_rst <= in_rst_cap;
  end generate;

  gen_sim : if g_sim = true generate
    -- Model in_clk to dp_clk divide by 2 uncertainty by delaying the in_rst one in_clk cycle in case g_sim_phase is even
    gen_even : if g_sim_phase = 0 or g_sim_phase = 1 generate in_rst <= in_rst_cap when rising_edge(in_clk); end generate;
    gen_odd  : if g_sim_phase = 2 or g_sim_phase = 3 generate in_rst <= in_rst_cap;                          end generate;
  end generate;

  -- Double data rate input cell at pin, also ensures deterministic input timing
  u_common_ddio_in : entity common_lib.common_ddio_in
  generic map (
    g_width => g_in_dat_w
  )
  port map (
    in_dat      => in_dat,
    in_clk      => in_clk,
    rst         => in_rst,
    out_dat_hi  => in_dat_hi,
    out_dat_lo  => in_dat_lo
  );

  u_common_ddreg : entity common_lib.common_ddreg
  generic map (
    g_in_delay_len    => 1,
    g_out_delay_len   => 2 + c_meta_delay_len,
    g_tsetup_delay_hi => c_tsetup_delay_in_hi,
    g_tsetup_delay_lo => c_tsetup_delay_in_lo
  )
  port map (
    in_clk      => dp_clk,
    in_dat      => wb_sync,
    rst         => in_rst,
    out_clk     => in_clk,
    out_dat_hi  => in_sync_hi_cap,
    out_dat_lo  => in_sync_lo_cap
  );

  in_sync_hi <= in_sync_hi_cap when c_swap_in_hi_lo = false else in_sync_lo_cap;
  in_sync_lo <= in_sync_lo_cap when c_swap_in_hi_lo = false else in_sync_hi_cap;

  -- Register fifo_wr_dat to ease timing closure between DDIO and FIFO at input clock rate
  in_fifo_wr_dat <= (in_sync_hi & in_dat_hi) & (in_sync_lo & in_dat_lo) when rising_edge(in_clk);
  in_fifo_wr_req <= '1';

  -- Dual clock FIFO, mixed width
  u_common_fifo_dc_mixed_widths : entity common_lib.common_fifo_dc_mixed_widths
  generic map (
    g_nof_words => c_in_fifo_size,  -- FIFO size in nof wr_dat words
    g_wr_dat_w  => c_fifo_wr_dat_w,
    g_rd_dat_w  => c_fifo_rd_dat_w
  )
  port map (
    rst     => in_rst,
    wr_clk  => in_clk,
    wr_dat  => in_fifo_wr_dat,
    wr_req  => in_fifo_wr_req,
    wr_ful  => OPEN,
    wrusedw => in_fifo_wrusedw,
    rd_clk  => dp_clk,
    rd_dat  => dp_fifo_rd_dat,
    rd_req  => r.dp_fifo_rd_req,
    rd_emp  => OPEN,
    rdusedw => dp_fifo_rdusedw,
    rd_val  => dp_fifo_rd_val
  );

  -- Double data rate capture dp_clk phase using wb_sync
  u_common_pulser : entity common_lib.common_pulser
  generic map (
    g_pulse_period => c_wb_sync_period
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,
    pulse_out => wb_sync
  );

  -- Extract sync and data. The FIFO output is in little endian order, symbol index [3:0] = sample [t3 t2 t1 t0]
  gen_le : for I in 0 to g_wb_factor - 1 generate
    le_sync(                         I           ) <= dp_fifo_rd_dat(g_in_dat_w + I * c_sync_in_dat_w) and dp_fifo_rd_val;  -- only accept sync when there is valid FIFO read data
    le_dat((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= dp_fifo_rd_dat(g_in_dat_w + I * c_sync_in_dat_w - 1 downto I * c_sync_in_dat_w);
  end generate;

  -- Rewire to big endian sample order, symbol index [3:0] = sample [t0 t1 t2 t3]
  be_sync <= hton(le_sync, c_wb_sync_w, g_wb_factor);
  be_dat  <= hton(le_dat,   g_in_dat_w, g_wb_factor);

  -- Determine in_clk - dp_clk phase
  nxt_r.be_sync <= be_sync;
  nxt_r.be_dat  <= be_dat;

  p_phase_detect : process(r, be_sync, wb_cnt)
  begin
    -- It is not necessary to have:
    -- . a phase_val signal that is active when a valid be_sync is detected for every c_wb_sync_latency period,
    -- . a phase_evt signal that pulses when the detected phase changes,
    -- because these cases of a missed phase sync and a changed phase are handled in p_locked.

    -- Set phase based on where the valid be_sync "1111" is detected in two cycles
    -- Signal illegal be_sync combinations that can occur if the in_clk and dp_clk edges are too close
    nxt_r.sync_phase <= r.sync_phase;
    if r.be_sync = "1111" and be_sync = "0000" then nxt_r.sync_phase <= 0; end if;  -- F0
    if r.be_sync = "0111" and be_sync = "1000" then nxt_r.sync_phase <= 1; end if;  -- 78
    if r.be_sync = "0011" and be_sync = "1100" then nxt_r.sync_phase <= 2; end if;  -- 3C
    if r.be_sync = "0001" and be_sync = "1110" then nxt_r.sync_phase <= 3; end if;  -- 1E
    if r.be_sync = "1011" and be_sync = "0100" then nxt_r.sync_phase <= 5; end if;  -- B4 = swap hi lo of 78, so map to phase 4+1=5
    if r.be_sync = "0010" and be_sync = "1101" then nxt_r.sync_phase <= 7; end if;  -- 2D = swap hi lo of 1E, so map to phase 4+3=7
                                                                                -- F0 = swap hi lo of F0, so phase 4 cannot be distinghuised from phase 0
                                                                                -- 3C = swap hi lo of 3C, so phase 6 cannot be distinghuised from phase 2
    -- Map sync_phase 0:3 and 5:7 on dat_phase 0:3
    if r.sync_phase < g_wb_factor then
      nxt_r.dat_phase <= r.sync_phase;
    else
      nxt_r.dat_phase <= r.sync_phase - g_wb_factor;
    end if;
  end process;

  p_debug_status : process(r, be_sync, wb_cnt, dd_phase)
  begin
    -- Debug status
    nxt_r.status <= r.status;
    nxt_r.status(7 downto 4) <= TO_UVEC(r.sync_phase, 4);
    nxt_r.status(7 downto 4) <= "000" & dd_phase;
    if r.be_sync = "1111" or (r.be_sync /= "0000" and be_sync /= "0000") then
      nxt_r.status(15 downto 8) <= r.be_sync & be_sync;
    end if;
    if be_sync /= "0000" then
      nxt_r.status(23 downto 16) <= r.be_sync & be_sync;
    elsif unsigned(wb_cnt) > c_wb_sync_latency then
      nxt_r.status(23 downto 16) <= (others => '0');
    end if;
    nxt_r.status(31 downto 24) <= TO_UVEC(r.lock_state, 8);
  end process;

  p_phase_adjust : process(r, be_sync, be_dat)
    variable v_rsync : std_logic_vector(c_rx_sync_w - 1 downto 0);
    variable v_sync  : std_logic_vector(c_rx_sync_w - 1 downto 0);
  begin
    v_rsync := r.be_sync(2) & r.be_sync(3) & r.be_sync(0) & r.be_sync(1);  -- swap hi lo
    v_sync  :=   be_sync(2) &   be_sync(3) &   be_sync(0) &   be_sync(1);  -- swap hi lo

    nxt_r.rx_sync <= (others => '0');
    case r.sync_phase is
      when 0      => nxt_r.rx_sync <= r.be_sync;
      when 1      => nxt_r.rx_sync <= r.be_sync(2 downto 0) & be_sync(3);
      when 2      => nxt_r.rx_sync <= r.be_sync(1 downto 0) & be_sync(3 downto 2);
      when 3      => nxt_r.rx_sync <= r.be_sync(0)          & be_sync(3 downto 1);
      when 5      => nxt_r.rx_sync <=   v_rsync(2 downto 0) &  v_sync(3);  -- as phase 1
      when 7      => nxt_r.rx_sync <=   v_rsync(0)          &  v_sync(3 downto 1);  -- as phase 3
      when others => null;
    end case;

    nxt_r.rx_dat <= r.be_dat;
    case r.dat_phase is
      when 0      => nxt_r.rx_dat <= r.be_dat;
      when 1      => nxt_r.rx_dat <= r.be_dat(3 * g_in_dat_w - 1 downto 0) & be_dat(4 * g_in_dat_w - 1 downto 3 * g_in_dat_w);
      when 2      => nxt_r.rx_dat <= r.be_dat(2 * g_in_dat_w - 1 downto 0) & be_dat(4 * g_in_dat_w - 1 downto 2 * g_in_dat_w);
      when 3      => nxt_r.rx_dat <= r.be_dat(1 * g_in_dat_w - 1 downto 0) & be_dat(4 * g_in_dat_w - 1 downto 1 * g_in_dat_w);
      when others => null;
    end case;
  end process;

  nxt_r.rx_val <= dp_fifo_rd_val;

  -- By means of a lock region on u_common_ddreg and a suitable PLL phase offset for the dp_clk it can be ensured that both:
  -- . be_sync_hi = be_sync(3) & be_sync(1) captured by in_clk rising edge, and
  -- . be_sync_lo = be_sync(2) & be_sync(0) captured by in_clk falling edge are "11" at the same time and "00" otherwise.
  -- Both be_sync_hi and be_sync_lo must have reliably captured the wb_sync, because we need both to detect the g_wb_factor=4 possible phases for r.sync_phase.
  -- For example with only be_sync_hi the phase detect would yield:
  --   IF r.be_sync_hi="11" AND be_sync="00" THEN nxt_r.sync_phase <= 0; END IF;
  --   IF r.be_sync_hi="01" AND be_sync="10" THEN nxt_r.sync_phase <= 1; END IF;
  --   IF r.be_sync_hi="01" AND be_sync="10" THEN nxt_r.sync_phase <= 2; END IF;   -- 1=2, and
  --   IF r.be_sync_hi="00" AND be_sync="11" THEN nxt_r.sync_phase <= 3; END IF;   -- 0=3, so using only be_sync_hi or only be_sync_lo is not enough
  -- Therefore the recovered dp_sync is only detected when rx_sync="1111" or "0000".
  nxt_r.dp_sync <= vector_and(r.rx_sync);

  -- Measure wb_sync to dp_sync latency
  u_common_counter : entity common_lib.common_counter
  generic map (
    g_width => c_wb_cnt_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    cnt_clr => wb_cnt_clr,
    count   => wb_cnt
  );

  wb_cnt_clr <= dp_cnt_clr or dp_cnt_rst;

  u_common_switch_dp_cnt_rst : entity common_lib.common_switch
  generic map (
    g_rst_level    => '1',  -- Defines the output level at reset.
    g_priority_lo  => false,  -- When TRUE then input switch_low has priority, else switch_high. Don't care when switch_high and switch_low are pulses that do not occur simultaneously.
    g_or_high      => false,  -- When TRUE and priority hi then the registered switch_level is OR-ed with the input switch_high to get out_level, else out_level is the registered switch_level
    g_and_low      => false  -- When TRUE and priority lo then the registered switch_level is AND-ed with the input switch_low to get out_level, else out_level is the registered switch_level
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,
    switch_high => dp_rst_cap,
    switch_low  => wb_sync,
    out_level   => dp_cnt_rst
  );

  u_common_switch_dp_cnt_clr : entity common_lib.common_switch
  generic map (
    g_rst_level    => '1',  -- Defines the output level at reset.
    g_priority_lo  => true,  -- When TRUE then input switch_low has priority, else switch_high. Don't care when switch_high and switch_low are pulses that do not occur simultaneously.
    g_or_high      => true,  -- When TRUE and priority hi then the registered switch_level is OR-ed with the input switch_high to get out_level, else out_level is the registered switch_level
    g_and_low      => false  -- When TRUE and priority lo then the registered switch_level is AND-ed with the input switch_low to get out_level, else out_level is the registered switch_level
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,
    switch_high => r.dp_sync,
    switch_low  => wb_sync,
    out_level   => dp_cnt_clr
  );

  p_locked : process(r, wb_cnt)
  begin
    nxt_r.dp_fifo_rd_req <= '1';
    nxt_r.dp_in_rst_req  <= '0';
    nxt_r.dp_locked      <= r.dp_locked;
    nxt_r.lock_state     <= r.lock_state;
    if unsigned(wb_cnt) < c_wb_sync_timeout then
      if r.dp_locked = '0' then
        -- Find lock
        nxt_r.lock_state <= 1;
        if r.dp_sync = '1' then
          if unsigned(wb_cnt) < c_wb_sync_latency then
            -- too early r.dp_sync so slip 1 cycle to adjust the FIFO latency to c_wb_sync_latency
            nxt_r.dp_fifo_rd_req <= '0';
            nxt_r.lock_state <= 2;
          elsif unsigned(wb_cnt) = c_wb_sync_latency then
            -- aligned r.dp_sync so declare locked
            nxt_r.dp_locked <= '1';
            nxt_r.lock_state <= 3;
          else
            -- too late r.dp_sync
            nxt_r.dp_in_rst_req <= '1';  -- recover by resetting the input
            nxt_r.lock_state <= 4;
          end if;
        end if;
      else
        -- Maintain lock
        -- . do not accept unexpected sync
        -- . do allow missing an r.dp_sync, but require at least one r.dp_sync per c_wb_sync_timeout)
        if r.dp_sync = '1' then
          if unsigned(wb_cnt) /= c_wb_sync_latency then
            -- unexpected r.dp_sync
            nxt_r.dp_in_rst_req <= '1';  -- recover by resetting the input,
            nxt_r.dp_locked     <= '0';  -- and finding lock again
            nxt_r.lock_state <= 5;
          end if;
        end if;
      end if;
    else
      -- Timeout r.dp_sync
      nxt_r.dp_in_rst_req <= '1';  -- recover by resetting the input,
      nxt_r.dp_locked     <= '0';  -- and finding lock again
      nxt_r.lock_state <= 6;
    end if;
  end process;

  u_common_clock_phase_detector : entity common_lib.common_clock_phase_detector
  generic map (
    g_rising_edge    => true,
    g_meta_delay_len => c_meta_delay_len
  )
  port map (
    in_clk    => in_clk,  -- used as data input for dp_clk
    rst       => dp_rst,
    clk       => dp_clk,
    --clk       => dp_clkq,
    phase     => dd_phase,
    phase_det => open
  );

  prev_dd_phase <= dd_phase when rising_edge(dp_clk);
  dd_phase_det <= '1' when prev_dd_phase = dd_phase else '0';

  u_common_stable_monitor : entity common_lib.common_stable_monitor
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- MM
    r_in         => dd_phase_det,
    --r_in         => r.dp_locked,
    r_stable     => dp_stable,
    r_stable_ack => dp_stable_ack
  );

  dp_in_clk_rst(0) <= r.dp_in_rst_req;

  u_common_ddio_out : entity common_lib.common_ddio_out
  generic map (
    g_width  => 1
  )
  port map (
    rst        => '0',
    in_clk     => dp_clk,
    in_dat_hi  => dp_in_clk_rst,
    in_dat_lo  => dp_in_clk_rst,
    out_dat    => i_in_clk_rst
  );
end str;
