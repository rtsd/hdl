--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_packetizing_pkg.all;

-- Reuse from LOFAR rad_frame_tx.vhd and rad_frame_tx(rtl).vhd

-- Purpose:
--   Pass on the input data path frame:
--
--                -----------------------
--                | fsn | dat ... | brc |
--                -----------------------
--                  sof             eof
--                  val   val ...   val
--
--   into an output data path PHY frame:
--
--          ----------------------------------
--     idle |idle| sfd | fsn | dat ... | crc | idle ...
--          ----------------------------------
--            sof                        eof
--            val  val   val   val ...   val
--
-- Description:
--   An input internal frame consists of fsn, data and brc. The in_sof marks
--   fsn and the in_eof marks the brc. When in_val is '0' then c_dp_idle data
--   is output.
--   The dp PHY output frame suits point it point links only. A valid CRC is
--   inserted when the brc is OK else insert NOT crc to propagate the brc error
--   at dp PHY level. For dat'LENGTH < c_dp_crc_w the transmitted crc word gets
--   extended. For dat'LENGTH > c_dp_crc_w the transmitted crc word gets
--   truncated.
--   The dp PHY frame is prepended with at least 1 idle word and a start of
--   frame delimiter (sfd) word. Different sfd words can be used to have
--   multiple independent dp frame streams via one dp PHY link. The dat block
--   size is fixed per dp frame stream.
--   More idle words are sent between frames if necessary. The out_val, out_sof
--   and out_eof signals are available to ease subsequent tx frame scheduling,
--   but they are not used on the actual dp PHY link. The idle word just before
--   the sfd is also marked valid and indicated by the out_sof to ensure that
--   there is at least one idle word between frames.

entity dp_frame_tx is
  generic (
    g_sfd        : std_logic_vector := c_dp_sfd;
    g_lofar      : boolean := false;  -- when TRUE use same CRC as for LOFAR
    g_dat_w      : natural := c_dp_max_w
  );
  port (
    rst      : in  std_logic;
    clk      : in  std_logic;
    in_dat   : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val   : in  std_logic;
    in_sof   : in  std_logic;
    in_eof   : in  std_logic;
    out_dat  : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val  : out std_logic;
    out_sof  : out std_logic;
    out_eof  : out std_logic
  );
end dp_frame_tx;

architecture rtl of dp_frame_tx is
  -- Use unregistered input to save logic, use registered input to ease achieving timing constrains.
  constant c_input_reg   : boolean := false;

  constant c_idle        : std_logic_vector := c_dp_idle(in_dat'range);
  constant c_sfd         : std_logic_vector := g_sfd(in_dat'range);
  constant c_brc_ok      : std_logic_vector := std_logic_vector(to_unsigned(c_dp_brc_ok, g_dat_w));

  signal crc             : std_logic_vector(c_dp_crc_w - 1 downto 0);
  signal nxt_crc         : std_logic_vector(crc'range);

  -- Registered inputs (see c_input_reg)
  signal in_dat_reg      : std_logic_vector(in_dat'range);
  signal in_val_reg      : std_logic;
  signal in_sof_reg      : std_logic;
  signal in_eof_reg      : std_logic;

  -- Delayed inputs to create space to insert idle and sfd word
  signal in_dat_dly      : std_logic_vector(in_dat'range);
  signal in_val_dly      : std_logic;
  signal in_sof_dly      : std_logic;
  signal in_eof_dly      : std_logic;
  signal in_dat_dly2     : std_logic_vector(in_dat'range);
  signal in_val_dly2     : std_logic;
  signal in_eof_dly2     : std_logic;

  signal frame_busy      : std_logic;
  signal nxt_frame_busy  : std_logic;

  signal nxt_out_dat     : std_logic_vector(out_dat'range);
  signal nxt_out_val     : std_logic;
  signal nxt_out_sof     : std_logic;
  signal nxt_out_eof     : std_logic;
begin
  gen_input_reg : if c_input_reg = true generate
    p_reg : process (clk, rst)
    begin
      if rst = '1' then
        in_dat_reg <= (others => '0');
        in_val_reg <= '0';
        in_sof_reg <= '0';
        in_eof_reg <= '0';
      elsif rising_edge(clk) then
        in_dat_reg <= in_dat;
        in_val_reg <= in_val;
        in_sof_reg <= in_sof;
        in_eof_reg <= in_eof;
      end if;
    end process;
  end generate;

  no_input_reg : if c_input_reg = false generate
    in_dat_reg <= in_dat;
    in_val_reg <= in_val;
    in_sof_reg <= in_sof;
    in_eof_reg <= in_eof;
  end generate;

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      in_dat_dly  <= (others => '0');
      in_val_dly  <= '0';
      in_sof_dly  <= '0';
      in_eof_dly  <= '0';
      in_dat_dly2 <= (others => '0');
      in_val_dly2 <= '0';
      in_eof_dly2 <= '0';
      crc         <= (others => '1');
      out_dat     <= (others => '0');
      out_val     <= '0';
      out_sof     <= '0';
      out_eof     <= '0';
      frame_busy  <= '0';
    elsif rising_edge(clk) then
      in_dat_dly  <= in_dat_reg;
      in_val_dly  <= in_val_reg;
      in_sof_dly  <= in_sof_reg;
      in_eof_dly  <= in_eof_reg;
      in_dat_dly2 <= in_dat_dly;
      in_val_dly2 <= in_val_dly;
      in_eof_dly2 <= in_eof_dly;
      crc         <= nxt_crc;
      out_dat     <= nxt_out_dat;
      out_val     <= nxt_out_val;
      out_sof     <= nxt_out_sof;
      out_eof     <= nxt_out_eof;
      frame_busy  <= nxt_frame_busy;
    end if;
  end process;

  p_output: process(crc, in_dat_dly2, in_val_dly2, in_sof_reg, in_sof_dly, in_eof_dly2, frame_busy)
  begin
    -- set default nxt_crc
    nxt_crc <= crc;
    if in_val_dly2 = '1' then
      nxt_crc <= func_dp_next_crc(g_lofar, in_dat_dly2, crc);
    end if;

    -- set default nxt_out_dat
    nxt_out_dat <= in_dat_dly2;  -- pass on input dat
    nxt_out_val <= in_val_dly2;
    nxt_out_sof <= '0';
    nxt_out_eof <= '0';

    nxt_frame_busy <= frame_busy;  -- hold to know when a frame is being transmitted or not

    -- overrule default nxt_crc, nxt_out_dat
    if in_sof_reg = '1' then
      nxt_crc     <= (others => '1');  -- init crc
      nxt_out_dat <= c_idle;  -- prepend idle word
      nxt_out_val <= '1';
      nxt_out_sof <= '1';
      nxt_frame_busy <= '1';
    elsif in_sof_dly = '1' then
      nxt_crc     <= (others => '1');  -- init crc
      nxt_out_dat <= c_sfd;  -- prepend sfd word
      nxt_out_val <= '1';
    elsif in_eof_dly2 = '1' then
      nxt_frame_busy <= '0';
      nxt_out_dat <= RESIZE_UVEC(crc, g_dat_w);  -- fill in calculated tx crc and account for brc
      if in_dat_dly2 /= c_brc_ok then
        nxt_out_dat <= not RESIZE_UVEC(crc, g_dat_w);
      end if;
      nxt_out_eof <= '1';
    elsif in_val_dly2 = '0' then
      if frame_busy = '0' then
        nxt_out_dat <= c_idle;  -- insert idle word when out_val is inactive between frames
      --ELSE
      -- do not transmit idle when out_val is inactive during a frame to reduce potential false sync at receiver
      end if;
    end if;
  end process;
end rtl;
