-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for dp_bsn_align
-- Description:
--
--  Read/Write registers for all streams. The number of registers dpeneds on the
--  g_nof_inputs that are used. Each input has it's own enable register.
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |                                                   enable_input0 = [0] |  0
--  |-----------------------------------------------------------------------|
--  |                                                   enable_input1 = [0] |  1
--  |-----------------------------------------------------------------------|
--  |                                                   enable_input2 = [0] |  2
--  |-----------------------------------------------------------------------|
--                                      |
--                                      |
--  |-----------------------------------------------------------------------|
--  |                                                  enable_input.. = [0] |  g_nof_inputs-1
--  |-----------------------------------------------------------------------|

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity reorder_rewire_reg is
  generic (
    g_nof_streams        : positive := 8;
    g_sel_in_w           : positive := 3;
    g_cross_clock_domain : boolean  := true  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
  );
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock
    st_rst     : in  std_logic;  -- reset synchronous with st_clk
    st_clk     : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in     : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out    : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    sel_reg    : out  std_logic_vector
  );
end reorder_rewire_reg;

architecture str of reorder_rewire_reg is
  constant c_nof_required_bits      : natural := g_nof_streams * g_sel_in_w;  -- 24
  constant c_nof_required_registers : natural := 1;  -- c_nof_required_bits MOD c_word_w;

  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => ceil_log2(c_nof_required_registers),
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => c_nof_required_registers,
                                  init_sl  => '0');

  --  FUNCTION array_init(init : NATURAL;   nof, width : NATURAL) RETURN STD_LOGIC_VECTOR;  -- useful to init an unconstrained std_logic_vector with repetitive content
  constant c_reg_init    : std_logic_vector(g_nof_streams * g_sel_in_w - 1 downto 0) := array_init(0, g_nof_streams, g_sel_in_w, 1);

  -- Registers in st_clk domain
  signal sel_in_reg  : std_logic_vector(c_word_w - 1 downto 0);
begin
  u_reg : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_in_new_latency     => 1,
    g_readback           => true,
    g_reg                => c_mm_reg,
    g_init_reg           => RESIZE_UVEC(c_reg_init, c_mem_reg_init_w)  -- RESIZE_UVEC(TO_UVEC(1, c_word_w), c_mem_reg_init_w)
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => st_rst,
    st_clk      => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => sla_in,
    sla_out     => sla_out,

    -- MM registers in st_clk domain
    reg_wr_arr  => OPEN,
    reg_rd_arr  => OPEN,
    in_new      => OPEN,
    in_reg      => sel_in_reg,  -- read
    out_reg     => sel_in_reg,  -- write
    out_new     => open
  );

  sel_reg <= sel_in_reg(g_nof_streams * g_sel_in_w - 1 downto 0);
end str;
