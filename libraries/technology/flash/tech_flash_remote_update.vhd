-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Remote update from flash device

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_flash_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_flash_lib;
library ip_arria10_remote_update_altera_remote_update_150;
library ip_arria10_e3sge3_remote_update_altera_remote_update_151;
--LIBRARY ip_arria10_e1sg_remote_update_altera_remote_update_180;
--LIBRARY ip_arria10_e2sg_remote_update_altera_remote_update_1910;

entity tech_flash_remote_update is
  generic (
    g_technology : natural := c_tech_select_default
  );
  port (
    clock       : in std_logic;
    data_in     : in std_logic_vector(tech_flash_data_w(g_technology) - 1 downto 0);
    param       : in std_logic_vector(2 downto 0);
    read_param  : in std_logic;
    reconfig    : in std_logic;
    reset       : in std_logic;
    reset_timer : in std_logic;
    write_param : in std_logic;
    busy        : out std_logic;
    data_out    : out std_logic_vector(tech_flash_data_w(g_technology) - 1 downto 0)
  );
end tech_flash_remote_update;

architecture str of tech_flash_remote_update is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_remote_update
    port map (clock, data_in, param, read_param, reconfig, reset, reset_timer, write_param, busy, data_out);
  end generate;

  -- note 1: data_in and data_out must increase to 32 bits
  -- note 2: EPCQ-L1024 not yet supported in IP editor

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_remote_update
    port map (clock, data_in, param, read_param, reconfig, reset, reset_timer, write_param, busy, data_out);
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_remote_update
    port map (clock, data_in, param, read_param, reconfig, reset, reset_timer, write_param, busy, data_out);
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_remote_update
    port map (clock, data_in, param, read_param, reconfig, reset, reset_timer, write_param, busy, data_out);
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_remote_update
    port map (clock, data_in, param, read_param, reconfig, reset, reset_timer, write_param, busy, data_out);
  end generate;
end architecture;
