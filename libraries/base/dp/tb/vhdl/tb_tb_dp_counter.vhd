-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_counter
-- Description:
-- Usage:
-- > as 6
-- > run -all

entity tb_tb_dp_counter is
end tb_tb_dp_counter;

architecture tb of tb_tb_dp_counter is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- general
  -- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;   -- always active, random or pulse flow control
  -- g_flow_control_verify    : t_dp_flow_control_enum := e_pulse;  -- always active, random or pulse flow control
  -- -- dut
  -- g_pipeline_src_out : NATURAL := 1; -- Pipeline source outputs (data,valid,sop,eop etc)
  -- g_pipeline_src_in  : NATURAL := 0  -- Pipeline source inputs (ready,xon). This will also pipeline src_out.
  -- g_nof_counters     : NATURAL := 2;
  -- -- min range = [0,2,1] => (0,1) 'the Python way'
  -- g_range_start      : t_nat_natural_arr(9 DOWNTO 0) := (0,0,0,0,0,0,0, 1, 0, 0);
  -- g_range_stop       : t_nat_natural_arr(9 DOWNTO 0) := (2,2,2,2,2,2,7,16,16,16);
  -- g_range_step       : t_nat_natural_arr(9 DOWNTO 0) := (1,1,1,1,1,1,2, 2, 2, 1);

  u_act_act_comb     : entity work.tb_dp_counter generic map (e_active, e_active, 0, 0, 3);
  u_act_act_pipe_out : entity work.tb_dp_counter generic map (e_active, e_active, 1, 0, 3);
  u_act_act_pipe_in  : entity work.tb_dp_counter generic map (e_active, e_active, 0, 1, 3);

  u_rnd_rnd_comb     : entity work.tb_dp_counter generic map (e_random, e_random, 0, 0, 3);
  u_rnd_rnd_pipe_out : entity work.tb_dp_counter generic map (e_random, e_random, 1, 0, 3);
  u_rnd_rnd_pipe_in  : entity work.tb_dp_counter generic map (e_random, e_random, 0, 1, 3);
end tb;
