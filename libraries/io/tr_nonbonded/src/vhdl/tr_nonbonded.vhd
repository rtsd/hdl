--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, technology_lib, tech_transceiver_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tr_nonbonded is
  generic(
    g_sim                 : boolean := false;
    g_sim_level           : natural := 0;
    g_technology          : natural := c_tech_select_default;
    g_data_w              : natural := 32;
    g_nof_gx              : natural := 12;
    g_mbps                : natural := 6250;  -- Supported: 6250, 5000, 3125, 2500
    ------------------------------------------------------------------------------------------------
    -- | g_sim | g_sim_level | Description
    -- |--------------------------------------------------------------------------------------------
    -- | FALSE | X           | Use ALTGX IP; Use long initialization delay for alignment on HW
    -- |--------------------------------------------------------------------------------------------
    -- | TRUE  | 0           | Use ALTGX IP; Use short initialization delay for alignment in sim
    -- | TRUE  | 1           | Use fast sim-only behavioural serdes model
    -- |--------------------------------------------------------------------------------------------
    -- | X     | >1          | Not supported
    ------------------------------------------------------------------------------------------------
    g_tx                  : boolean := true;
    g_rx                  : boolean := true;
    g_fifos               : boolean := false;  -- When TRUE use dp_clk and clock domain crossing FIFO for dp->tx and for rx->dp, when FALSE use rx_clk stream and tx_clk stream
    g_tx_fifo_depth       : natural := c_bram_m9k_fifo_depth;  -- = 256 * 32b = 1 M9K, because g_data_w=32b, functionally a depth of c_meta_fifo_depth=16 is sufficient to cross the clock domain
    g_rx_fifo_depth       : natural := c_bram_m9k_fifo_depth  -- = 256 * 32b = 1 M9K, because g_data_w=32b, functionally a depth of c_meta_fifo_depth=16 is sufficient to cross the clock domain
  );
  port (
    tb_end                : in  std_logic := '0';  -- in simulation stop internal clocks when tb_end='1' to support 'run -all'

    -- DP clock domain (only used when g_fifos=TRUE)
    st_rst                : in  std_logic;
    st_clk                : in  std_logic;

    -- PHY clocks
    tr_clk                : in  std_logic;
    cal_rec_clk           : in  std_logic;

    -- FIFO monitoring
    gp_out                : out std_logic_vector(2 * g_nof_gx - 1 downto 0);

    -- Serial data I/O
    tx_dataout            : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_datain             : in  std_logic_vector(g_nof_gx - 1 downto 0);

    -- Parallel data I/O with fabric
    -- . Use g_fifos=FALSE for data receive directly in rx_clk domain
    rx_rst                : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_clk                : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_sosi_arr           : out t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    rx_siso_arr           : in  t_dp_siso_arr(g_nof_gx - 1 downto 0);

    -- . Use g_fifos=FALSE for data transmit directly in tx_clk domain
    tx_rst                : out std_logic_vector(g_nof_gx - 1 downto 0);
    tx_clk                : out std_logic_vector(g_nof_gx - 1 downto 0);
    tx_sosi_arr           : in  t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    tx_siso_arr           : out t_dp_siso_arr(g_nof_gx - 1 downto 0);

    -- . Use g_fifos=TRUE for data transmit and receive in dp_clk domain
    dp_rx_sosi_arr        : out t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    dp_rx_siso_arr        : in  t_dp_siso_arr(g_nof_gx - 1 downto 0);
    dp_tx_sosi_arr        : in  t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    dp_tx_siso_arr        : out t_dp_siso_arr(g_nof_gx - 1 downto 0);

    -- Control and monitoring
    tx_state              : out std_logic_vector(2 * g_nof_gx - 1 downto 0);
    tx_align_en           : in  std_logic_vector(g_nof_gx - 1 downto 0);

    rx_state              : out std_logic_vector(2 * g_nof_gx - 1 downto 0);
    rx_align_en           : in  std_logic_vector(g_nof_gx - 1 downto 0);

    rx_dataout            : out t_slv_32_arr(g_nof_gx - 1 downto 0) := (others => (others => '0'))
  );
end tr_nonbonded;

architecture str of tr_nonbonded is
  signal i_rx_clk           : std_logic_vector(g_nof_gx - 1 downto 0);
  signal i_rx_rst           : std_logic_vector(g_nof_gx - 1 downto 0);

  signal i_tx_clk           : std_logic_vector(g_nof_gx - 1 downto 0);
  signal i_tx_rst           : std_logic_vector(g_nof_gx - 1 downto 0);

  signal rx_fifo_sosi_arr   : t_dp_sosi_arr(g_nof_gx - 1 downto 0);
  signal rx_fifo_siso_arr   : t_dp_siso_arr(g_nof_gx - 1 downto 0);

  signal tx_fifo_sosi_arr   : t_dp_sosi_arr(g_nof_gx - 1 downto 0);
  signal tx_fifo_siso_arr   : t_dp_siso_arr(g_nof_gx - 1 downto 0);

  signal gp_out_tx          : std_logic_vector(g_nof_gx - 1 downto 0) := (others => '0');
  signal gp_out_rx          : std_logic_vector(g_nof_gx - 1 downto 0) := (others => '0');
begin
  -- FIFO monitoring
  gp_out <= gp_out_tx & gp_out_rx;

  gen_phy: if g_sim = false or g_sim_level = 0 generate
    -- PHY IP
    u_tech_transceiver_gx: entity tech_transceiver_lib.tech_transceiver_gx
    generic map (
      g_technology    => g_technology,
      g_data_w        => g_data_w,
      g_nof_gx        => g_nof_gx,
      g_mbps          => g_mbps,
      g_tx            => g_tx,
      g_rx            => g_rx,
      g_sim           => g_sim
      )
    port map (
      tr_clk          => tr_clk,

      cal_rec_clk     => cal_rec_clk,

      rx_clk          => i_rx_clk,
      rx_rst          => i_rx_rst,

      rx_sosi_arr     => rx_fifo_sosi_arr,
      rx_siso_arr     => rx_fifo_siso_arr,

      tx_clk          => i_tx_clk,
      tx_rst          => i_tx_rst,

      tx_sosi_arr     => tx_fifo_sosi_arr,
      tx_siso_arr     => tx_fifo_siso_arr,

      tx_dataout      => tx_dataout,
      rx_datain       => rx_datain,

      tx_state        => tx_state,
      tx_align_en     => tx_align_en,

      rx_state        => rx_state,
      rx_align_en     => rx_align_en
    );
  end generate;

  gen_sim: if g_sim = true and g_sim_level = 1 generate
    -- Behavioural serdes model (fast)
    u_sim_gx: entity tech_transceiver_lib.sim_transceiver_gx
    generic map (
      g_data_w        => g_data_w,
      g_nof_gx        => g_nof_gx,
      g_mbps          => g_mbps,
      g_tx            => g_tx,
      g_rx            => g_rx
      )
    port map (
      tb_end          => tb_end,

      tr_clk          => tr_clk,

      rx_clk          => i_rx_clk,
      rx_rst          => i_rx_rst,

      rx_sosi_arr     => rx_fifo_sosi_arr,
      rx_siso_arr     => rx_fifo_siso_arr,

      tx_clk          => i_tx_clk,
      tx_rst          => i_tx_rst,

      tx_sosi_arr     => tx_fifo_sosi_arr,
      tx_siso_arr     => tx_fifo_siso_arr,

      tx_dataout      => tx_dataout,
      rx_datain       => rx_datain
    );
  end generate;

  --  === TX === TX === TX === TX === TX === TX === TX === TX === TX === TX === TX === TX === TX === TX === TX === TX
  gen_tx : if g_tx = true generate
    tx_rst <= i_tx_rst;
    tx_clk <= i_tx_clk;

    gen_i : for i in 0 to g_nof_gx - 1 generate
      no_tx_fifo : if g_fifos = false generate
        tx_fifo_sosi_arr(i) <= tx_sosi_arr(i);  -- for tx_fifo_sosi_arr it is needed to select between tx_sosi_arr and dp_tx_sosi_arr
        tx_siso_arr(i)      <= tx_fifo_siso_arr(i);  -- could default connect tx_siso_arr, but for clarity only connect tx_siso_arr when g_fifos = FALSE, else leave it 'X'
      end generate;

      gen_tx_fifo : if g_fifos = true generate
        u_tx_fifo : entity dp_lib.dp_fifo_dc
        generic map (
          g_technology => g_technology,
          g_data_w    => g_data_w,
          g_use_ctrl  => false,
          g_fifo_size => g_tx_fifo_depth,
          g_fifo_rl   => 1
        )
        port map (
          wr_rst      => st_rst,
          wr_clk      => st_clk,
          rd_rst      => i_tx_rst(i),
          rd_clk      => i_tx_clk(i),
          -- ST sink
          snk_out     => dp_tx_siso_arr(i),
          snk_in      => dp_tx_sosi_arr(i),
          -- Monitor FIFO filling
          wr_ful      => gp_out_tx(i),
          wr_usedw    => OPEN,
          rd_usedw    => OPEN,
          rd_emp      => OPEN,
          -- ST source
          src_in      => tx_fifo_siso_arr(i),
          src_out     => tx_fifo_sosi_arr(i)
        );
      end generate;  -- gen_tx_fifo
    end generate;  -- gen_i
  end generate;  -- gen_tx

  -- === RX === RX === RX === RX === RX === RX === RX === RX === RX === RX === RX === RX === RX === RX === RX === RX
  gen_rx : if g_rx = true generate
    rx_rst <= i_rx_rst;
    rx_clk <= i_rx_clk;

    gen_i : for i in 0 to g_nof_gx - 1 generate
      rx_dataout(i)(g_data_w - 1 downto 0) <= rx_fifo_sosi_arr(i).data(g_data_w - 1 downto 0);

      no_rx_fifo : if g_fifos = false generate
        rx_sosi_arr(i)      <= rx_fifo_sosi_arr(i);  -- could default connect rx_sosi_arr, but for clarity only connect rx_sosi_arr when g_fifos = FALSE, else leave it 'X'
        rx_fifo_siso_arr(i) <= rx_siso_arr(i);  -- for rx_fifo_siso_arr it is needed to select between rx_siso_arr and dp_rx_siso_arr
      end generate;

      gen_rx_fifo : if g_fifos = true generate
        u_rx_fifo : entity dp_lib.dp_fifo_dc
        generic map (
          g_technology => g_technology,
          g_data_w    => g_data_w,
          g_use_ctrl  => false,
          g_fifo_size => g_rx_fifo_depth,
          g_fifo_rl   => 1
        )
        port map (
          wr_rst      => i_rx_rst(i),
          wr_clk      => i_rx_clk(i),
          rd_rst      => st_rst,
          rd_clk      => st_clk,
          -- ST sink
          snk_out     => rx_fifo_siso_arr(i),
          snk_in      => rx_fifo_sosi_arr(i),
          -- Monitor FIFO filling
          wr_ful      => gp_out_rx(i),
          wr_usedw    => OPEN,
          rd_usedw    => OPEN,
          rd_emp      => OPEN,
          -- ST source
          src_in      => dp_rx_siso_arr(i),
          src_out     => dp_rx_sosi_arr(i)
        );
      end generate;  -- gen_rx_fifo
    end generate;  -- gen_i
  end generate;  -- gen_rx
end str;
