###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_general_pins.tcl
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_other_pins.tcl
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_1Gbe_pins.tcl
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_sensor_pins.tcl
# -- Front Interface (10GbE)
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/FRONT_NODE_tr_cntrl_pins.tcl

set_location_assignment PIN_AA2 -to SA_CLK
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SA_CLK

set_location_assignment PIN_M4 -to SI_FN_0_TX[0]
set_location_assignment PIN_K4 -to SI_FN_0_TX[1]
set_location_assignment PIN_D4 -to SI_FN_0_TX[2]
set_location_assignment PIN_B4 -to SI_FN_0_TX[3]
set_location_assignment PIN_N2 -to SI_FN_0_RX[0]
set_location_assignment PIN_L2 -to SI_FN_0_RX[1]
set_location_assignment PIN_E2 -to SI_FN_0_RX[2]
set_location_assignment PIN_C2 -to SI_FN_0_RX[3]

set_location_assignment PIN_AD4 -to SI_FN_1_TX[0]
set_location_assignment PIN_AB4 -to SI_FN_1_TX[1]
set_location_assignment PIN_T4 -to SI_FN_1_TX[2]
set_location_assignment PIN_P4 -to SI_FN_1_TX[3]
set_location_assignment PIN_AE2 -to SI_FN_1_RX[0]
set_location_assignment PIN_AC2 -to SI_FN_1_RX[1]
set_location_assignment PIN_U2 -to SI_FN_1_RX[2]
set_location_assignment PIN_R2 -to SI_FN_1_RX[3]

set_location_assignment PIN_AT4 -to SI_FN_2_TX[0]
set_location_assignment PIN_AP4 -to SI_FN_2_TX[1]
set_location_assignment PIN_AH4 -to SI_FN_2_TX[2]
set_location_assignment PIN_AF4 -to SI_FN_2_TX[3]
set_location_assignment PIN_AU2 -to SI_FN_2_RX[0]
set_location_assignment PIN_AR2 -to SI_FN_2_RX[1]
set_location_assignment PIN_AJ2 -to SI_FN_2_RX[2]
set_location_assignment PIN_AG2 -to SI_FN_2_RX[3]

set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_TX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_TX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_TX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_TX[3]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_RX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_RX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_RX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_RX[3]

set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_1_TX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_1_TX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_1_TX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_1_TX[3]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_1_RX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_1_RX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_1_RX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_1_RX[3]

set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_2_TX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_2_TX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_2_TX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_2_TX[3]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_2_RX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_2_RX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_2_RX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_2_RX[3]


# -- include ddr3 pins
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/src/tcl/COMMON_NODE_ddr_I_rec_pins.tcl
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/src/tcl/COMMON_NODE_ddr_II_rec_pins.tcl

