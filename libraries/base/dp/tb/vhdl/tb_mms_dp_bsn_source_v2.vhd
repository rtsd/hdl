-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Purpose:
-- . Test bench for mms_dp_bsn_source_v2
-- Description:
-- . Verify DP on, off via MM
-- . Verify captured BSN via MM during a sync interval
-- . Verify changing of BSN_time_offset
--
-- Usage:
-- > as 10
-- > run -all
-- > view expanded bs_sosi in Wave window

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_mms_dp_bsn_source_v2 is
end tb_mms_dp_bsn_source_v2;

architecture tb of tb_mms_dp_bsn_source_v2 is
  constant c_bsn_time_offset_w          : natural := 10;

  constant c_clk_period                 : time := 10 ns;

  constant c_pps_interval               : natural := 1000;
  constant c_cross_clock_domain_latency : natural := 20;

  constant c_block_size                 : natural := 100;
  constant c_nof_block_per_sync         : natural := 15;
  constant c_nof_clk_per_sync           : natural := c_nof_block_per_sync * c_block_size;
  constant c_bsn_init                   : natural := 7;

  constant c_mm_addr_dp_on              : natural := 0;
  constant c_mm_addr_nof_clk_per_sync   : natural := 1;
  constant c_mm_addr_bsn_lo             : natural := 2;
  constant c_mm_addr_bsn_hi             : natural := 3;
  constant c_mm_addr_bsn_time_offset    : natural := 4;

  constant c_mm_dp_off                  : natural := 0;  -- DP off after finishing current block
  constant c_mm_dp_on_immediate         : natural := 1;  -- DP on immediate by setting bit 0
  constant c_mm_dp_on_at_pps            : natural := 3;  -- DP on at next PPS by setting bits 1,0

  signal tb_end               : std_logic := '0';
  signal clk                  : std_logic := '1';
  signal rst                  : std_logic := '1';
  signal pps                  : std_logic := '0';

  signal bs_sosi              : t_dp_sosi;

  signal mm_dp_on_status      : natural;
  signal mm_bsn               : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := (others => '0');
  signal mm_bsn_time_offset   : std_logic_vector(c_bsn_time_offset_w - 1 downto 0) := (others => '0');

  signal mm_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal mm_miso              : t_mem_miso;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  proc_common_gen_pulse(1, c_pps_interval, '1', rst, clk, pps);

  p_mm_stimuli : process
  begin
     wait until rst = '0';
     proc_common_wait_some_cycles(clk, 10);

     -- Write initial BSN and number of block per sync interval
     -- .   -- must also write hi part to trigger transfer across clock domain
     proc_mem_mm_bus_wr(c_mm_addr_bsn_lo,                   c_bsn_init, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_wr(c_mm_addr_bsn_hi,                            0, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_wr(c_mm_addr_nof_clk_per_sync, c_nof_clk_per_sync, clk, mm_miso, mm_mosi);
     proc_common_wait_some_cycles(clk, c_cross_clock_domain_latency);

     --------------------------------------------------------------------------
     -- DP on immediate
     --------------------------------------------------------------------------
     -- Wait until after PPS
     proc_common_wait_until_hi_lo(clk, pps);

     -- Write DP on immediate
     proc_mem_mm_bus_wr(c_mm_addr_dp_on, c_mm_dp_on_immediate, clk, mm_miso, mm_mosi);
     proc_common_wait_some_cycles(clk, c_cross_clock_domain_latency);

     -- Read dp on status
     proc_mem_mm_bus_rd(c_mm_addr_dp_on, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_rd_latency(1, clk);
     mm_dp_on_status <= TO_UINT(mm_miso.rddata(1 downto 0));
     proc_common_wait_some_cycles(clk, 1);
     assert mm_dp_on_status = c_mm_dp_on_immediate
       report "Wrong DP on status, expected DP on immediate."
       severity ERROR;

     -- Read BSN in first sync interval
     proc_common_wait_some_cycles(clk, c_nof_clk_per_sync / 2);
     proc_mem_mm_bus_rd(c_mm_addr_bsn_lo, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_rd_latency(1, clk);
     mm_bsn(c_word_w - 1 downto 0) <= mm_miso.rddata(c_word_w - 1 downto 0);
     proc_mem_mm_bus_rd(c_mm_addr_bsn_hi, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_rd_latency(1, clk);
     mm_bsn(2 * c_word_w - 1 downto c_word_w) <= mm_miso.rddata(c_word_w - 1 downto 0);
     proc_common_wait_some_cycles(clk, 1);
     assert TO_UINT(mm_bsn) = c_bsn_init
       report "Wrong BSN at sync in first interval."
       severity ERROR;

     -- Read BSN in second sync interval
     proc_common_wait_some_cycles(clk, c_nof_clk_per_sync);
     proc_mem_mm_bus_rd(c_mm_addr_bsn_lo, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_rd_latency(1, clk);
     mm_bsn(c_word_w - 1 downto 0) <= mm_miso.rddata(c_word_w - 1 downto 0);
     proc_mem_mm_bus_rd(c_mm_addr_bsn_hi, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_rd_latency(1, clk);
     mm_bsn(2 * c_word_w - 1 downto c_word_w) <= mm_miso.rddata(c_word_w - 1 downto 0);
     proc_common_wait_some_cycles(clk, 1);
     assert TO_UINT(mm_bsn) = c_bsn_init + c_nof_block_per_sync
       report "Wrong BSN at sync in second interval."
       severity ERROR;

     -- Run few sync intervals
     proc_common_wait_some_cycles(clk, 3 * c_nof_clk_per_sync);

     -- Write DP off
     proc_mem_mm_bus_wr(c_mm_addr_dp_on, c_mm_dp_off, clk, mm_miso, mm_mosi);
     proc_common_wait_some_cycles(clk, c_block_size);

     -- Read dp on status
     proc_mem_mm_bus_rd(c_mm_addr_dp_on, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_rd_latency(1, clk);
     mm_dp_on_status <= TO_UINT(mm_miso.rddata(1 downto 0));
     proc_common_wait_some_cycles(clk, 1);
     assert mm_dp_on_status = c_mm_dp_off
       report "Wrong DP on status, expected DP off."
       severity ERROR;

     -- Set bsn_time_offset and read back 2 times 0 and 5
     proc_mem_mm_bus_wr(c_mm_addr_bsn_time_offset, 0, clk, mm_miso, mm_mosi);
     proc_common_wait_some_cycles(clk, 2 * c_cross_clock_domain_latency);

     proc_mem_mm_bus_rd(c_mm_addr_bsn_time_offset, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_rd_latency(1, clk);
     mm_bsn_time_offset(c_bsn_time_offset_w - 1 downto 0) <= mm_miso.rddata(c_bsn_time_offset_w - 1 downto 0);
     proc_common_wait_some_cycles(clk, 1);
     assert TO_UINT(mm_bsn_time_offset(c_bsn_time_offset_w - 1 downto 0)) = 0
       report "Wrong offset, expected 0"
       severity ERROR;

     proc_mem_mm_bus_wr(c_mm_addr_bsn_time_offset, 5, clk, mm_miso, mm_mosi);
     proc_common_wait_some_cycles(clk, 2 * c_cross_clock_domain_latency);

     proc_mem_mm_bus_rd(c_mm_addr_bsn_time_offset, clk, mm_miso, mm_mosi);
     proc_mem_mm_bus_rd_latency(1, clk);
     mm_bsn_time_offset(c_bsn_time_offset_w - 1 downto 0) <= mm_miso.rddata(c_bsn_time_offset_w - 1 downto 0);
     proc_common_wait_some_cycles(clk, 1);
     assert TO_UINT(mm_bsn_time_offset(c_bsn_time_offset_w - 1 downto 0)) = 5
       report "Wrong offset, expected 5"
       severity ERROR;

     proc_common_wait_some_cycles(clk, c_nof_clk_per_sync);
     tb_end <= '1';
     wait;
  end process;

  u_dut : entity work.mms_dp_bsn_source_v2
  generic map (
    g_cross_clock_domain => true,
    g_block_size         => c_block_size,
    g_nof_clk_per_sync   => 200 * 10**6,  -- overrule via MM write
    g_bsn_w              => c_dp_stream_bsn_w
  )
  port map (
    -- Clocks and reset
    mm_rst            => rst,
    mm_clk            => clk,
    dp_rst            => rst,
    dp_clk            => clk,
    dp_pps            => pps,

    -- Memory-mapped clock domain
    reg_mosi          => mm_mosi,  -- actual ranges defined by c_mm_reg in dp_bsn_source_reg
    reg_miso          => mm_miso,  -- actual ranges defined by c_mm_reg in dp_bsn_source_reg

    -- Streaming clock domain
    bs_sosi           => bs_sosi
  );
end tb;
