# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and other 
# software and tools, and its AMPP partner logic functions, and any output 
# files any of the foregoing (including device programming or simulation 
# files), and any associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License Subscription 
# Agreement, Altera MegaCore Function License Agreement, or other applicable 
# license agreement, including, without limitation, that your use is for the 
# sole purpose of programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the applicable 
# agreement for further details.



proc error_and_exit {msg} {
   post_message -type error "SCRIPT_ABORTED!!!"
   foreach line [split $msg "\n"] {
      post_message -type error $line
   }
   qexit -error
}

proc show_usage_and_exit {argv0} {
   post_message -type error  "USAGE: $argv0 \[VERILOG|VHDL\]"
   qexit -error
}

set argv0 "quartus_sh -t [info script]"
set args $quartus(args)

if {[llength $args] == 1 } {
   set lang [string toupper [string trim [lindex $args 0]]]
   if {$lang != "VERILOG" && $lang != "VHDL"} {
      show_usage_and_exit $argv0
   }
} else {
   set lang "VERILOG"
}

if {[llength $args] > 1} {
   show_usage_and_exit $argv0
}

if {[is_project_open]} {
   post_message "Closing currently opened project..."
   project_close
}

set script_path [file dirname [file normalize [info script]]]

source "$script_path/params.tcl"

set ex_design_path         "$script_path/sim"
set system_name            $ed_params(SIM_QSYS_NAME)
set qsys_file              "${system_name}.qsys"
set family                 $ip_params(SYS_INFO_DEVICE_FAMILY)
set device                 $ed_params(DEFAULT_DEVICE)

post_message " "
post_message "*************************************************************************"
post_message "Altera External Memory Interface IP Example Design Builder"
post_message " "
post_message "Type    : Simulation Design"
post_message "Family  : $family"
post_message "Language: $lang"
post_message " "
post_message "This script takes ~1 minute to execute..."
post_message "*************************************************************************"
post_message " "

if {!$ip_params(MEM_HAS_SIM_SUPPORT)} {
   error_and_exit "Simulation for this memory interface configuration is not supported by the current IP version."
}

if {[file isdirectory $ex_design_path]} {
   error_and_exit "Directory $ex_design_path has already been generated.\nThis script cannot overwrite generated example designs.\nIf you would like to regenerate the design by re-running the script, please remove the directory."
}

file mkdir $ex_design_path
file copy -force "${script_path}/$qsys_file" "${ex_design_path}/$qsys_file"

if {[file exists "${script_path}/quartus.ini"]} {
   file copy -force "${script_path}/quartus.ini" "${ex_design_path}/quartus.ini"
}

post_message "Generating example design files..."

set ip_generate_exe_path "$::env(QUARTUS_ROOTDIR)/sopc_builder/bin/ip-generate"
set ip_make_simscript_exe_path "$::env(QUARTUS_ROOTDIR)/sopc_builder/bin/ip-make-simscript"

cd $ex_design_path
exec -ignorestderr $ip_generate_exe_path --remove-qsys-generate-warning --file-set=SIM_${lang} --system-info=DEVICE=$device --output-directory=. --report-file=spd $qsys_file >>& ip_generate.out
exec -ignorestderr $ip_make_simscript_exe_path --spd=ed_sim.spd >>& make_simscript.out

file delete -force ed_sim.spd
file delete -force make_simscript.out
file delete -force ip_generate.out

post_message "Finalizing..."

set sim_scripts [list]
lappend sim_scripts "${ex_design_path}/synopsys/vcs/vcs_setup.sh"
lappend sim_scripts "${ex_design_path}/synopsys/vcsmx/vcsmx_setup.sh"
lappend sim_scripts "${ex_design_path}/cadence/ncsim_setup.sh"

foreach sim_script $sim_scripts {
   if {[file exists $sim_script]} {
      set fh [open $sim_script r]
      set file_data [read $fh]
      close $fh

      set fh [open $sim_script w]
      foreach line [split $file_data "\n"] {
         if {[regexp -- {USER_DEFINED_SIM_OPTIONS\s*=.*\+vcs\+finish\+100} $line]} {
            regsub -- {\+vcs\+finish\+100} $line {} line
         }
         if {[regexp -- {USER_DEFINED_SIM_OPTIONS\s*=.*\-input \\\"\@run 100; exit\\\"} $line]} {
            regsub -- {\-input \\\"\@run 100; exit\\\"} $line {} line
         }
         puts $fh $line
      }
      close $fh
   }
}
if {$ip_params(DIAG_USE_ABSTRACT_PHY) == "true" } {
   source "$script_path/make_abphy_sim_script.tcl"
}

post_message " "
post_message "*************************************************************************"
post_message "Successfully generated example design at the following location:"
post_message " "
post_message "   $ex_design_path"
post_message " "
post_message "*************************************************************************"
post_message " "
