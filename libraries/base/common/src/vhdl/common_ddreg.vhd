-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Capture internal signal at double data rate
-- Description:
-- . Function:
--   The double data rate in_dat samples that arrive at time series t0, t1, t2,
--   ... get output with samples t0, t2, ... in out_dat_lo and samples t1, t3,
--   ... in out_dat_hi. Hence out_dat = out_dat_hi & out_dat_lo contains the
--   time series samples in little endian format with the first sample in the
--   LSpart. This is similar to common_ddio_in.
--
-- . Schematic:
--   The in_dat is first registered by in_clk and then captured by out_clk.
--   There is a common_ddreg_r for capturing at rising edge of out_clk and
--   a common_ddreg_f for capturing at falling edge of out_clk. These are
--   separate components to allow placing each of them in a single ALM using
--   a logic lock region. Together they do not fit in a single ALM, because
--   one ALM can fit only two clocks, so in_clk and out_clk-r or in_clk and
--   out_clk-f, but not in_clk and out_clk-r and out_clk-f. After
--   common_ddreg_f a seperate common_ddreg_fr is used to carry the data from
--   the falling edge flipflop to the rising edge flipflop in the out_clk
--   clock domain.
--
--             ------------------------------------
--   in_dat -->| ddreg_r                          |----> out_dat_hi
--   in_clk -->| async     async                  |<---- out_clk
--             ------------------------------------
--
--             -------------------     ------------
--   in_dat -->| ddreg_f         |---->| ddreg_fr |----> out_dat_lo
--   in_clk -->| async     async |<-\  | fFF  rFF |<-+-- out_clk
--             -------------------  |  ------------  |
--                                  |                |
--                                  \----------------/
--
--   The common_async in common_ddreg_r and common_ddreg_f has a preserve
--   register synthesis attribute to ensure that the input register clocked
--   by in_clk does not get optimized away because it exists in both
--   common_ddreg_r and common_ddreg_f.
--   In this way the clock domain transition from in_clk --> out_clk-r for
--   in_dat to out_dat_hi is kept within 1 ALM, to restrict the data path delay
--   and to make it as synthesis design indepentent as possible. Similar or
--   the clock domain transition from in_clk --> out_clk-f for in_dat to
--   out_dat_lo.
-- Remark:
-- . Similar to common_ddio_in but for internal signal instead of FPGA input
--   pin signal.

--------------------------------------------------------------------------------
-- common_ddreg_r
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_ddreg_r is
  generic (
    g_in_delay_len    : positive := 1;
    g_out_delay_len   : positive := c_meta_delay_len;
    g_tsetup_delay_hi : boolean := false
  );
  port (
    in_clk      : in  std_logic;
    in_dat      : in  std_logic;
    rst         : in  std_logic := '0';
    out_clk     : in  std_logic;
    out_dat_r   : out std_logic
  );
end common_ddreg_r;

architecture str of common_ddreg_r is
  signal in_dat_r   : std_logic;
  signal in_dat_d   : std_logic;
begin
  u_in : entity work.common_async
  generic map (
    g_delay_len => g_in_delay_len
  )
  port map (
    rst  => rst,
    clk  => in_clk,
    din  => in_dat,
    dout => in_dat_r
  );

  in_dat_d <= in_dat_r when g_tsetup_delay_hi = false else in_dat_r when rising_edge(out_clk);

  -- Output at rising edge
  u_out_hi : entity work.common_async
  generic map (
    g_delay_len => g_out_delay_len
  )
  port map (
    rst  => rst,
    clk  => out_clk,
    din  => in_dat_d,
    dout => out_dat_r
  );
end str;

--------------------------------------------------------------------------------
-- common_ddreg_f
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_ddreg_f is
  generic (
    g_in_delay_len    : positive := 1;
    g_out_delay_len   : positive := c_meta_delay_len;
    g_tsetup_delay_lo : boolean := false
  );
  port (
    in_clk      : in  std_logic;
    in_dat      : in  std_logic;
    rst         : in  std_logic := '0';
    out_clk     : in  std_logic;
    out_dat_f   : out std_logic  -- clocked at falling edge of out_clk
  );
end common_ddreg_f;

architecture str of common_ddreg_f is
  signal in_dat_r   : std_logic;
  signal in_dat_d   : std_logic;
begin
  u_in : entity work.common_async
  generic map (
    g_delay_len => g_in_delay_len
  )
  port map (
    rst  => rst,
    clk  => in_clk,
    din  => in_dat,
    dout => in_dat_r
  );

  in_dat_d <= in_dat_r when g_tsetup_delay_lo = false else in_dat_r when falling_edge(out_clk);

  -- Capture input at falling edge
  u_fall : entity work.common_async
  generic map (
    g_rising_edge => false,
    g_delay_len   => g_out_delay_len
  )
  port map (
    rst  => rst,
    clk  => out_clk,
    din  => in_dat_d,
    dout => out_dat_f
  );
end str;

--------------------------------------------------------------------------------
-- common_ddreg_fr
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_ddreg_fr is
  port (
    rst         : in  std_logic := '0';
    clk         : in  std_logic;
    in_dat_f    : in  std_logic;  -- clocked at falling edge of clk
    out_dat_r   : out std_logic  -- clocked at rising edge of clk
  );
end common_ddreg_fr;

architecture str of common_ddreg_fr is
  signal in_dat_d   : std_logic;
begin
  in_dat_d  <= in_dat_f when falling_edge(clk);  -- input at falling edge
  out_dat_r <= in_dat_d when rising_edge(clk);  -- Output at rising edge
end str;

--------------------------------------------------------------------------------
-- common_ddreg
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_ddreg is
  generic (
    g_in_delay_len    : positive := 1;
    g_out_delay_len   : positive := c_meta_delay_len;
    g_tsetup_delay_hi : boolean := false;
    g_tsetup_delay_lo : boolean := false
  );
  port (
    in_clk      : in  std_logic;
    in_dat      : in  std_logic;
    rst         : in  std_logic := '0';
    out_clk     : in  std_logic;
    out_dat_hi  : out std_logic;
    out_dat_lo  : out std_logic
  );
end common_ddreg;

architecture str of common_ddreg is
  signal out_dat_f   : std_logic;
begin
  -- out_dat_hi
  u_ddreg_hi : entity work.common_ddreg_r
  generic map (
    g_in_delay_len    => g_in_delay_len,
    g_out_delay_len   => g_out_delay_len,
    g_tsetup_delay_hi => g_tsetup_delay_hi
  )
  port map (
    in_clk     => in_clk,
    in_dat     => in_dat,
    rst        => rst,
    out_clk    => out_clk,
    out_dat_r  => out_dat_hi
  );

  -- out_dat_lo
  u_ddreg_fall : entity work.common_ddreg_f
  generic map (
    g_in_delay_len    => g_in_delay_len,
    g_out_delay_len   => g_out_delay_len - 1,
    g_tsetup_delay_lo => g_tsetup_delay_lo
  )
  port map (
    in_clk     => in_clk,
    in_dat     => in_dat,
    rst        => rst,
    out_clk    => out_clk,
    out_dat_f  => out_dat_f  -- clocked at falling edge of out_clk
  );

  u_ddreg_lo : entity work.common_ddreg_fr
  port map (
    rst         => rst,
    clk         => out_clk,
    in_dat_f    => out_dat_f,
    out_dat_r   => out_dat_lo  -- clocked at rising edge of out_clk
  );
end str;
