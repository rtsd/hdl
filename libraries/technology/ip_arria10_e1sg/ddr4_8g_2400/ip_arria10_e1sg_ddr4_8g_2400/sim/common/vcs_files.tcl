
namespace eval ip_arria10_e1sg_ddr4_8g_2400 {
  proc get_memory_files {QSYS_SIMDIR} {
    set memory_files [list]
    lappend memory_files "$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_seq_params_sim.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_seq_params_synth.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_seq_cal.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_avalon_onchip_memory2_180/sim/seq_cal_soft_m20k.hex"
    return $memory_files
  }
  
  proc get_common_design_files {QSYS_SIMDIR} {
    set design_files [dict create]
    return $design_files
  }
  
  proc get_design_files {QSYS_SIMDIR} {
    set design_files [dict create]
    error "Skipping VCS script generation since VHDL file $QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i.vhd is required for simulation"
  }
  
  proc get_elab_options {SIMULATOR_TOOL_BITNESS} {
    set ELAB_OPTIONS ""
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ELAB_OPTIONS
  }
  
  
  proc get_sim_options {SIMULATOR_TOOL_BITNESS} {
    set SIM_OPTIONS ""
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $SIM_OPTIONS
  }
  
  
  proc get_env_variables {SIMULATOR_TOOL_BITNESS} {
    set ENV_VARIABLES [dict create]
    set LD_LIBRARY_PATH [dict create]
    dict set ENV_VARIABLES "LD_LIBRARY_PATH" $LD_LIBRARY_PATH
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ENV_VARIABLES
  }
  
  
}
