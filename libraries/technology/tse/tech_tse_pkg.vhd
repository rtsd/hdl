-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

package tech_tse_pkg is
  constant c_tech_tse_reg_addr_w           : natural := 8;  -- = max 256 MAC registers
  constant c_tech_tse_byte_addr_w          : natural := c_tech_tse_reg_addr_w + 2;
  constant c_tech_tse_byte_addr_pcs_offset : natural := 16#200#;  -- table 4.8, 4.9 in ug_ethernet.pdf
  constant c_tech_tse_data_w               : natural := c_word_w;  -- = 32

  constant c_tech_tse_symbol_w             : natural := c_byte_w;  -- = 8
  constant c_tech_tse_symbol_max           : natural := 2**c_tech_tse_symbol_w - 1;  -- = 255
  constant c_tech_tse_symbols_per_beat     : natural := c_tech_tse_data_w / c_tech_tse_symbol_w;  -- = 4

  constant c_tech_tse_pcs_reg_addr_w       : natural := 5;  -- = max 32 PCS registers
  constant c_tech_tse_pcs_halfword_addr_w  : natural := c_tech_tse_pcs_reg_addr_w + 1;  -- table 4.17 in ug_ethernet.pdf
  constant c_tech_tse_pcs_byte_addr_w      : natural := c_tech_tse_pcs_reg_addr_w + 2;
  constant c_tech_tse_pcs_data_w           : natural := c_halfword_w;  -- = 16;

  constant c_tech_tse_empty_w              : natural := 2;
  constant c_tech_tse_tx_error_w           : natural := 1;
  constant c_tech_tse_rx_error_w           : natural := 6;
  constant c_tech_tse_error_w              : natural := largest(c_tech_tse_tx_error_w, c_tech_tse_rx_error_w);
  constant c_tech_tse_err_stat_w           : natural := 18;
  constant c_tech_tse_frm_type_w           : natural := 4;

  constant c_tech_tse_rx_ready_latency     : natural := 2;  -- 2 = default when FIFO is used
  constant c_tech_tse_tx_ready_latency     : natural := 1;  -- c_tech_tse_tx_ready_latency + 3 = TX_ALMOST_FULL

  constant c_tech_tse_tx_fifo_depth        : natural := 256;  -- nof words for Tx FIFO
  constant c_tech_tse_rx_fifo_depth        : natural := 256;  -- nof words for Rx FIFO

  type t_tech_tse_tx_mac is record
    -- Tx MAC inputs
    crc_fwd  : std_logic;
    -- Tx MAC outputs
    septy    : std_logic;
    a_full   : std_logic;
    a_empty  : std_logic;
    uflow    : std_logic;
  end record;

  type t_tech_tse_rx_mac is record
    -- Rx MAC inputs
    -- Rx MAC outputs
    ethertype: std_logic_vector(c_tech_tse_err_stat_w - 1 downto 0);
    frm_type : std_logic_vector(c_tech_tse_frm_type_w - 1 downto 0);
    dsav     : std_logic;
    a_full   : std_logic;
    a_empty  : std_logic;
  end record;

  type t_tech_tse_led is record
    an       : std_logic;
    link     : std_logic;
    disp_err : std_logic;
    char_err : std_logic;
    crs      : std_logic;
    col      : std_logic;
  end record;

  function func_tech_tse_map_pcs_addr(pcs_addr : natural) return natural;
end tech_tse_pkg;

package body tech_tse_pkg is
function func_tech_tse_map_pcs_addr(pcs_addr : natural) return natural is
begin
  return pcs_addr * 2 + c_tech_tse_byte_addr_pcs_offset;
end func_tech_tse_map_pcs_addr;
end tech_tse_pkg;
