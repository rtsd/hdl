--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_packetizing_pkg.all;

-- Reuse from LOFAR rad_frame.vhd and rad_frame(rtl).vhd

-- Purpose:
--   Puts the input data path signals:
--     . val
--     . dat[]
--     . sof
--     . eof
--     . fsn[]
--   into the internal data path (dp) frame data format:
--
--     -----------------------
--     | fsn | dat ... | brc |
--     -----------------------
--       sof             eof
--       val   val ...   val
--
--   To make an external frame with sfd and crc instead of brc use dp_frame_tx.
--
-- Description:
--   The input sof and eof mark the first respectively the last valid word of a
--   block of input data. The output sof indicates the fsn word. After that
--   follows the block of input data. At the end a brc is appended indicated by
--   the output eof. If necessary invalid data is inserted between frames.
--   The brc is a boolean redundancy check and will be 0 indicating OK when
--   in_err='0', else 1. The in_err is valid during the active in_eof. The brc
--   can act as a place holder for a true crc to be inserted when the frame
--   is sent outside the FPGA using dp_frame_tx and dp_frame_rx. Alternatively
--   the brc can be kept in the dp frame in addition to a PHY level CRC.
--   The width of the fsn and brc is set by the dat width. The fsn is sign
--   extended to g_dat_w, in this way all bits g_dat_w:g_fsn_w-1 indicate
--   fsync, so a signed resize suits for every g_dat_w >= g_fsn_w.

entity dp_frame is
  generic (
    g_fsn_w          : positive := c_dp_max_w;
    g_dat_w          : positive := c_dp_max_w
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    in_fsn           : in  std_logic_vector(g_fsn_w - 1 downto 0);
    in_dat           : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val           : in  std_logic;
    in_sof           : in  std_logic;
    in_eof           : in  std_logic;
    in_err           : in  std_logic;

    out_dat          : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val          : out std_logic;
    out_sof          : out std_logic;
    out_eof          : out std_logic
  );
end dp_frame;

architecture rtl of dp_frame is
  -- Use unregistered input to save logic, use registered input to ease achieving timing constrains.
  constant c_input_reg   : boolean := false;

  constant c_brc_ok      : std_logic_vector(in_dat'range) := std_logic_vector(to_unsigned(c_dp_brc_ok, in_dat'length));
  constant c_brc_err     : std_logic_vector(in_dat'range) := std_logic_vector(to_unsigned(c_dp_brc_err, in_dat'length));

  -- Registered inputs (see c_input_reg)
  signal in_fsn_reg      : std_logic_vector(in_fsn'range);
  signal in_dat_reg      : std_logic_vector(in_dat'range);
  signal in_val_reg      : std_logic;
  signal in_sof_reg      : std_logic;
  signal in_eof_reg      : std_logic;
  signal in_err_reg      : std_logic;

  -- Delayed inputs to create space to insert fsn word
  signal in_dat_dly      : std_logic_vector(in_dat'range);
  signal in_val_dly      : std_logic;
  signal in_eof_dly      : std_logic;
  signal in_err_dly      : std_logic;

  -- Delay input eof and err one more to append brc word
  signal in_eof_dly2     : std_logic;
  signal in_err_dly2     : std_logic;

  signal i_out_dat       : std_logic_vector(out_dat'range);
  signal nxt_out_dat     : std_logic_vector(out_dat'range);
  signal nxt_out_val     : std_logic;
  signal nxt_out_sof     : std_logic;
  signal nxt_out_eof     : std_logic;
begin
  out_dat <= i_out_dat;

  gen_input_reg : if c_input_reg = true generate
    p_reg : process (clk, rst)
    begin
      if rst = '1' then
        in_fsn_reg <= (others => '0');
        in_dat_reg <= (others => '0');
        in_val_reg <= '0';
        in_sof_reg <= '0';
        in_eof_reg <= '0';
        in_err_reg <= '0';
      elsif rising_edge(clk) then
        in_fsn_reg <= in_fsn;
        in_dat_reg <= in_dat;
        in_val_reg <= in_val;
        in_sof_reg <= in_sof;
        in_eof_reg <= in_eof;
        in_err_reg <= in_err;
      end if;
    end process;
  end generate;

  no_input_reg : if c_input_reg = false generate
    in_fsn_reg <= in_fsn;
    in_dat_reg <= in_dat;
    in_val_reg <= in_val;
    in_sof_reg <= in_sof;
    in_eof_reg <= in_eof;
    in_err_reg <= in_err;
  end generate;

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      in_dat_dly  <= (others => '0');
      in_val_dly  <= '0';
      in_eof_dly  <= '0';
      in_err_dly  <= '0';
      in_eof_dly2 <= '0';
      in_err_dly2 <= '0';
      i_out_dat   <= (others => '0');
      out_val     <= '0';
      out_sof     <= '0';
      out_eof     <= '0';
    elsif rising_edge(clk) then
      in_dat_dly  <= in_dat_reg;
      in_val_dly  <= in_val_reg;
      in_eof_dly  <= in_eof_reg;
      in_err_dly  <= in_err_reg;
      in_eof_dly2 <= in_eof_dly;
      in_err_dly2 <= in_err_dly;
      i_out_dat   <= nxt_out_dat;
      out_val     <= nxt_out_val;
      out_sof     <= nxt_out_sof;
      out_eof     <= nxt_out_eof;
    end if;
  end process;

  p_output: process(i_out_dat, in_fsn_reg, in_dat_dly, in_val_dly, in_sof_reg, in_eof_dly2, in_err_dly2)
  begin
    -- set default nxt_out_dat
    nxt_out_dat <= i_out_dat;  -- hold output dat when input is inactive
    nxt_out_val <= '0';
    nxt_out_sof <= '0';
    nxt_out_eof <= '0';

    -- overrule default nxt_out_dat
    if in_sof_reg = '1' then  -- insert fsn, no need to delay input fsn, because it holds
      nxt_out_dat <= RESIZE_SVEC(in_fsn_reg, out_dat'length);  -- sign extend is fsync,
      nxt_out_val <= '1';
      nxt_out_sof <= '1';
    elsif in_eof_dly2 = '1' then
      nxt_out_dat <= c_brc_ok;  -- fill in brc, account for input err
      if in_err_dly2 = '1' then
        nxt_out_dat <= c_brc_err;
      end if;
      nxt_out_val <= '1';
      nxt_out_eof <= '1';
    elsif in_val_dly = '1' then
      nxt_out_dat <= in_dat_dly;  -- pass on input dat
      nxt_out_val <= '1';
    end if;
  end process;
end rtl;
