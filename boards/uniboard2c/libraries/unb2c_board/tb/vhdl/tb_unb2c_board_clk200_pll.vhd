-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Simulate phase behaviour of PLL in normal mode
-- Description:
-- Usage:
-- > as 3
-- > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity tb_unb2c_board_clk200_pll is
end tb_unb2c_board_clk200_pll;

architecture tb of tb_unb2c_board_clk200_pll is
  constant c_ext_clk_period    : time := 5 ns;  -- 200 MHz
  constant c_clk_vec_w         : natural := 6;
  constant c_clk_div           : natural := 32;

  signal tb_end         : std_logic := '0';
  signal ext_clk        : std_logic := '0';
  signal ext_rst        : std_logic;

  signal st_clk200_0    : std_logic;
  signal st_rst200_0    : std_logic;

  signal st_clk200p0    : std_logic;
  signal st_rst200p0    : std_logic;

  signal st_clk200_45   : std_logic;
  signal st_rst200_45   : std_logic;

  signal st_clk200p45   : std_logic;
  signal st_rst200p45   : std_logic;

  signal st_clk400      : std_logic;
  signal st_rst400      : std_logic;

  signal dp_clk200      : std_logic;
  signal dp_rst200      : std_logic;
begin
  tb_end <= '0', '1' after c_ext_clk_period * 5000;

  ext_clk <= not ext_clk or tb_end after c_ext_clk_period / 2;
  ext_rst <= '1', '0' after c_ext_clk_period * 7;

  dut_0 : entity work.unb2c_board_clk200_pll
  generic map (
    g_clk200_phase_shift  => "0"
  )
  port map (
    arst       => ext_rst,
    clk200     => ext_clk,
    st_clk200  => st_clk200_0,
    st_rst200  => st_rst200_0,
    st_clk200p => st_clk200p0,
    st_rst200p => st_rst200p0,
    st_clk400  => st_clk400,
    st_rst400  => st_rst400
  );

  dut_45 : entity work.unb2c_board_clk200_pll
  generic map (
    g_clk200_phase_shift  => "625",
    g_clk200p_phase_shift => "625"
  )
  port map (
    arst       => ext_rst,
    clk200     => ext_clk,
    st_clk200  => st_clk200_45,
    st_rst200  => st_rst200_45,
    st_clk200p => st_clk200p45,
    st_rst200p => st_rst200p45,
    st_clk400  => OPEN,
    st_rst400  => open
  );

  dut_p6 : entity work.unb2c_board_clk200_pll
  generic map (
    g_clk200_phase_shift  => "0"
  )
  port map (
    arst       => ext_rst,
    clk200     => ext_clk,
    st_clk200  => dp_clk200,
    st_rst200  => dp_rst200
  );
end tb;
