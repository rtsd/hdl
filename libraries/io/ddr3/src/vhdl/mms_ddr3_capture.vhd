-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use work.ddr3_pkg.all;

entity mms_ddr3_capture is
  generic (
    g_sim                     : boolean := false;
    g_ddr                     : t_c_ddr3_phy := c_ddr3_phy_4g;
    g_mts                     : natural := 800;
    g_wr_data_w               : natural := c_ddr3_ctlr_data_w;
    g_wr_use_ctrl             : boolean := false;
    g_wr_fifo_depth           : natural := 128;
    g_rd_fifo_depth           : natural := 256;
    g_rd_data_w               : natural := c_ddr3_ctlr_data_w;
    g_flush_wr_fifo           : boolean := false;
    g_flush_sop               : boolean := false;
    g_flush_sop_channel       : boolean := false;
    g_flush_sop_start_channel : natural := 0;
    g_flush_nof_channels      : natural := 0;
    g_use_bsn_scheduler       : boolean := false
  );
  port (
    mm_rst          : in    std_logic;
    mm_clk          : in    std_logic;

    ctlr_ref_clk    : in    std_logic;
    ctlr_rst        : in    std_logic;  -- asynchronous reset input to controller

    ctlr_gen_clk    : out   std_logic;  -- Controller generated clock
    ctlr_gen_rst    : out   std_logic;

    wr_clk          : in    std_logic;
    wr_rst          : in    std_logic;

    wr_sosi         : in    t_dp_sosi;
    wr_siso         : out   t_dp_siso;

    flush_ena       : in    std_logic := '0';

    -- MM registers
    ddr3_mosi       : in    t_mem_mosi := c_mem_mosi_rst;
    ddr3_miso       : out   t_mem_miso;

    dpmm_ctrl_mosi  : in    t_mem_mosi := c_mem_mosi_rst;
    dpmm_ctrl_miso  : out   t_mem_miso := c_mem_miso_rst;

    dpmm_data_mosi  : in    t_mem_mosi := c_mem_mosi_rst;
    dpmm_data_miso  : out   t_mem_miso := c_mem_miso_rst;

    -- SO-DIMM Memory Bank
    ddr3_in         : in    t_tech_ddr3_phy_in;
    ddr3_io         : inout t_tech_ddr3_phy_io;
    ddr3_ou         : out   t_tech_ddr3_phy_ou
  );
end mms_ddr3_capture;

architecture str of mms_ddr3_capture is
  signal rd_sosi    : t_dp_sosi;
  signal rd_siso    : t_dp_siso;

  signal rd_clk     : std_logic;
  signal rd_rst     : std_logic;

  signal rd_fifo_usedw : std_logic_vector(ceil_log2(g_rd_fifo_depth * (c_ddr3_ctlr_data_w / g_rd_data_w) ) - 1 downto 0);
begin
  u_mms_ddr3: entity work.mms_ddr3
  generic map(
    g_sim                      => g_sim,
    g_ddr                      => g_ddr,
    g_mts                      => g_mts,
    g_wr_data_w                => g_wr_data_w,
    g_wr_use_ctrl              => g_wr_use_ctrl,
    g_wr_fifo_depth            => g_wr_fifo_depth,
    g_rd_fifo_depth            => g_rd_fifo_depth,
    g_rd_data_w                => c_word_w,
    g_flush_wr_fifo            => g_flush_wr_fifo,
    g_flush_sop                => g_flush_sop,
    g_flush_sop_channel        => g_flush_sop_channel,
    g_flush_sop_start_channel  => g_flush_sop_start_channel,
    g_flush_nof_channels       => g_flush_nof_channels
  )
  port map (
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    ctlr_ref_clk       => ctlr_ref_clk,
    ctlr_rst           => ctlr_rst,

    ctlr_gen_clk       => ctlr_gen_clk,
    ctlr_gen_rst       => ctlr_gen_rst,

    wr_clk             => wr_clk,
    wr_rst             => wr_rst,

    wr_sosi            => wr_sosi,
    wr_siso            => wr_siso,

    flush_ena          => flush_ena,

    rd_sosi            => rd_sosi,
    rd_siso            => rd_siso,

    rd_clk             => mm_clk,
    rd_rst             => mm_rst,

    rd_fifo_usedw      => rd_fifo_usedw,  -- relative to FIFO wr side

    ctrl_mosi          => ddr3_mosi,
    ctrl_miso          => ddr3_miso,

    ddr3_in            => ddr3_in,
    ddr3_io            => ddr3_io,
    ddr3_ou            => ddr3_ou
  );

  u_mms_dp_fifo_to_mm: entity dp_lib.mms_dp_fifo_to_mm
  generic map(
    g_rd_fifo_depth => g_rd_fifo_depth * (c_ddr3_ctlr_data_w / g_rd_data_w)
  )
  port map (
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    rd_sosi            => rd_sosi,
    rd_siso            => rd_siso,

    ctrl_mosi          => dpmm_ctrl_mosi,
    ctrl_miso          => dpmm_ctrl_miso,

    data_mosi          => dpmm_data_mosi,
    data_miso          => dpmm_data_miso,

    rd_usedw           => rd_fifo_usedw

  );
end str;
