--
--        _/_/           _/_/_/
--      _/_/_/_/         _/_/_/
--      _/_/_/_/_/       _/_/_/              ____________________________________________
--      _/_/_/_/_/       _/_/_/             /                                           /
--      _/_/_/_/_/       _/_/_/            /                                 M25P128   /
--      _/_/_/_/_/       _/_/_/           /                                           /
--      _/_/_/_/_/_/     _/_/_/          /                                  128Mbit  /
--      _/_/_/_/_/_/     _/_/_/         /                   Low Voltage, SPI, 50Mhz /
--      _/_/_/ _/_/_/    _/_/_/        /                                           /
--      _/_/_/  _/_/_/   _/_/_/       /                     VHDL Behavioral Model /
--      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.0 /
--      _/_/_/    _/_/_/ _/_/_/     /                                           /
--      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2008 Numonyx B.V. /
--      _/_/_/      _/_/_/_/_/    /___________________________________________/
--      _/_/_/       _/_/_/_/
--      _/_/          _/_/_/
--
--
--             NUMONYX
--
--
----------------------------------------------------------------------------------------
--                                    MEMORY Array PACKAGE                            --
----------------------------------------------------------------------------------------
--                                                                                    --
--                            Implements the memory array by a vector                 --
--                                                                                    -- 
--                                                                                    -- 
--                                                                                    -- 
----------------------------------------------------------------------------------------

library IEEE; 
   use IEEE.std_logic_1164.all;
   use IEEE.std_logic_TextIO.all;
   use IEEE.Std_Logic_Arith.all;

library Std;
   use STD.TextIO.all;

library Work;
   use work.def.all;
   use work.data.all;
   use work.StringLib.all;

package MemoryLib is

   type memory_rec is array (Memory_dim - 1  downto 0) of DataMem_type;
   type page_rec is array (Page_range) of DataMem_type;
   
   constant FF: DataMem_type:= (Others => '1');


   ------------------------------------------------------------------------
   -------  Public Procedures: they can be used in the model  -------------
   ------------------------------------------------------------------------

   -- Used to init the memory list
   procedure InitMemory(variable memory: inout memory_rec);    

   -- Used to write or to erase the memory
   procedure putMemory(variable memory: inout memory_rec; variable add: in AddrMem_type; variable data: in DataMem_type);

   -- Used to read a memory cell
   procedure getMemory(variable memory: in memory_rec; variable add: in AddrMem_type; variable data: out DataMem_type);

   -- Used to read a vector of memory cells
   --procedure getMemoryBuffer(variable memory: in memory_rec; variable firstAdd: in AddrMem_type; variable numData: in natural); --variable dataVector: out MemBuffer_type);
   
   -- Used to load a memory file in the memory list
   procedure LoadMemoryFile(FileName: in String; variable memory: inout memory_rec);

   -- Used to save into a file the memory list
   procedure SaveMemoryFile(FileName: in String; variable memory: in memory_rec);

   -- Used to display the memory list
   procedure PrintList(variable memory: in memory_rec);

   -- Used to display the two passed values
   procedure PrintData(variable add:in AddrMem_type; variable data: in DataMem_type);

end MemoryLib;


----------------------------------------------------------------
--------  Package Body - procedures implementation  ------------
----------------------------------------------------------------

package body MemoryLib is


   ------------------------------------------------
   -------  Initialization of the memory ----------
   ------------------------------------------------
   
   procedure InitMemory(variable memory: inout memory_rec) is
   variable i: Integer ;
   begin
     for i in 0 to Memory_dim - 1   loop
     memory(i):= FF;
     end loop;
   end;

   ----------------------------------------------------
   ---------  Print only a value of the Memory  ---------
   ----------------------------------------------------

   procedure PrintData(variable add: in AddrMem_type; variable data: in DataMem_type) is
   variable msg: line;
   begin

     write(msg, String'(" address =  "));
     write(msg, add);
     
     write(msg, String'("      "));

     write(msg, String'(" data =  "));
     write(msg, data);
     writeline(output, msg);

   end;


   ----------------------------------------------------
   ---------  Print the values of the list  -----------
   ----------------------------------------------------

   procedure PrintList(variable memory: in memory_rec) is
   begin


   end;

   ---------------------------------------------------------------
   ---------  Read the data in a memory cell  --------------------
   ---------------------------------------------------------------

   procedure getMemory(variable memory: in memory_rec; variable add: in AddrMem_type; variable data: out DataMem_type) is
   begin
      data:=memory(add);
   end;


  
   ---------------------------------------------------------------
   ------  Add or remove a cell to the memory list  --------------
   ---------------------------------------------------------------

   procedure putMemory(variable memory: inout memory_rec; variable add: in AddrMem_type; variable data: in DataMem_type) is
   begin
      memory(add):=data;

   end;

   ---------------------------------------------------------------------
   ----------------  Erase a cell in the memory list  ------------------
   ---------------------------------------------------------------------

   procedure BlockErase(variable memory: inout memory_rec; Variable Addr1: in AddrMem_type; Variable Addr2: in AddrMem_type) is
   variable i: AddrMem_type;
   begin
      for i in Addr1 to Addr2 loop
          memory(i) := FF;
      end loop;  
   end;
----------------------------------------------------------------------------
--------  Load the memory file in the memory list  -------------------------
----------------------------------------------------------------------------

procedure LoadMemoryFile(FileName: in String; variable memory: inout memory_rec) is

    file p_file : text open Read_Mode is FileName;
    variable l : line;
    variable ch : character;
    variable address : AddrMem_type;
    variable data : DataMem_type;
    variable index_addr : integer;
    variable index_data : integer;
    Constant dataHIGH : integer := data'high;
  begin

    InitMemory(memory);
    if (FileName/="")  then

       while not endfile (p_file) loop

            readline  (p_file,l);

            if (l'length>0) then

                address := 0;
                index_addr := AddrMem_Dim/4;
                if ((AddrMem_Dim rem 4) = 0) then
                   index_addr := index_addr - 1;
                end if;
                index_data := 0;
                read (l, ch);
  
                while (not(ch = '/')) loop  --- lettura e calcolo address
                        address := address + slv2int(char2quad_bits(ch))*(16**index_addr);
                        read (l, ch);
                        index_addr := index_addr - 1;
                end loop;
  
                read (l, ch);

                while (not(ch = ';')) loop  --- lettura dato
                  for i in 0 to 3 loop
                    data(DataBus_Dim - 1 - i - 4*index_data) := To_BitVector(char2quad_bits(ch))(3-i);
                   
                  end loop;
                  read (l, ch);
                  
                  index_data := index_data + 1;
                end loop;
                putMemory(memory, address, data);
            end if;  
  
       end loop;
    end if;
  end LoadMemoryFile;

  
------------------------------------------------------------------------------
-------------  Save the memory list into a memory file  ----------------------
------------------------------------------------------------------------------

procedure SaveMemoryFile(FileName: in String; variable memory: in memory_rec) is


  begin
  end SaveMemoryFile;

end MemoryLib;

