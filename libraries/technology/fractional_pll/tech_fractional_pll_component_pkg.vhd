-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE;
use IEEE.std_logic_1164.all;

package tech_fractional_pll_component_pkg is
  -----------------------------------------------------------------------------
  -- ip_arria10
  -----------------------------------------------------------------------------

  component ip_arria10_fractional_pll_clk200 is
  port
  (
    outclk0       : out std_logic;  -- outclk0.clk
    outclk1       : out std_logic;  -- outclk1.clk
    outclk2       : out std_logic;  -- outclk2.clk
    pll_cal_busy  : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown : in  std_logic := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0   : in  std_logic := '0'  -- pll_refclk0.clk
  );
  end component;

  component ip_arria10_fractional_pll_clk125 is
  port
  (
    outclk0       : out std_logic;  -- outclk0.clk
    outclk1       : out std_logic;  -- outclk1.clk
    outclk2       : out std_logic;  -- outclk2.clk
    outclk3       : out std_logic;  -- outclk2.clk
    pll_cal_busy  : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown : in  std_logic := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0   : in  std_logic := '0'  -- pll_refclk0.clk
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  -----------------------------------------------------------------------------

  component ip_arria10_e3sge3_fractional_pll_clk200 is
  port
  (
    outclk0       : out std_logic;  -- outclk0.clk
    outclk1       : out std_logic;  -- outclk1.clk
    outclk2       : out std_logic;  -- outclk2.clk
    pll_cal_busy  : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown : in  std_logic := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0   : in  std_logic := '0'  -- pll_refclk0.clk
  );
  end component;

  component ip_arria10_e3sge3_fractional_pll_clk125 is
  port
  (
    outclk0       : out std_logic;  -- outclk0.clk
    outclk1       : out std_logic;  -- outclk1.clk
    outclk2       : out std_logic;  -- outclk2.clk
    outclk3       : out std_logic;  -- outclk2.clk
    pll_cal_busy  : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown : in  std_logic := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0   : in  std_logic := '0'  -- pll_refclk0.clk
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e1sg
  -----------------------------------------------------------------------------

  component ip_arria10_e1sg_fractional_pll_clk200 is
  port
  (
    outclk0       : out std_logic;  -- outclk0.clk
    outclk1       : out std_logic;  -- outclk1.clk
    outclk2       : out std_logic;  -- outclk2.clk
    pll_cal_busy  : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown : in  std_logic := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0   : in  std_logic := '0'  -- pll_refclk0.clk
  );
  end component;

  component ip_arria10_e1sg_fractional_pll_clk125 is
  port
  (
    outclk0       : out std_logic;  -- outclk0.clk
    outclk1       : out std_logic;  -- outclk1.clk
    outclk2       : out std_logic;  -- outclk2.clk
    outclk3       : out std_logic;  -- outclk2.clk
    pll_cal_busy  : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown : in  std_logic := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0   : in  std_logic := '0'  -- pll_refclk0.clk
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e2sg
  -----------------------------------------------------------------------------

  component ip_arria10_e2sg_fractional_pll_clk200 is
  port
  (
    outclk0       : out std_logic;  -- outclk0.clk
    outclk1       : out std_logic;  -- outclk1.clk
    outclk2       : out std_logic;  -- outclk2.clk
    pll_cal_busy  : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown : in  std_logic := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0   : in  std_logic := '0'  -- pll_refclk0.clk
  );
  end component;

  component ip_arria10_e2sg_fractional_pll_clk125 is
  port
  (
    outclk0       : out std_logic;  -- outclk0.clk
    outclk1       : out std_logic;  -- outclk1.clk
    outclk2       : out std_logic;  -- outclk2.clk
    outclk3       : out std_logic;  -- outclk2.clk
    pll_cal_busy  : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked    : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown : in  std_logic := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0   : in  std_logic := '0'  -- pll_refclk0.clk
  );
  end component;
end tech_fractional_pll_component_pkg;
