-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle

-- Purpose:
-- . Scope component to show the desired time index (beamlet/subband) from
-- the sp_sosi_arr input in the wave window based on g_selection.
-- Description:
-- First deinterleaves the input then feed that to dp_wideband_sp_arr_scope.
-- Remark:
-- . Only for simulation.
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.sdp_pkg.all;

entity sdp_scope is
  generic (
    g_sim                 : boolean := false;
    g_selection           : natural := 0;  -- Time index selection (subband/beamlet)
    g_nof_input           : natural := 2;  -- nof input streams
    g_n_deinterleave      : natural := 2;  -- deinterleave factor for each input stream
    g_dat_w               : natural := 18  -- Data width
  );
  port (
    clk : in std_logic;
    rst : in std_logic;

    -- Streaming input for complex streams
    sp_sosi_arr     : in t_dp_sosi_arr(g_nof_input - 1 downto 0);

    -- Scope output for deinterleaved streams
    scope_sosi_arr  : out t_dp_sosi_integer_arr(g_nof_input * g_n_deinterleave-1 downto 0)
  );
end sdp_scope;

architecture str of sdp_scope is
  type t_dp_sosi_2arr_n is array (integer range <>) of t_dp_sosi_arr(g_n_deinterleave-1 downto 0);

  signal cnt                       : natural;
  signal deinterleaved_sosi_2arr_n : t_dp_sosi_2arr_n(g_nof_input - 1 downto 0);
  signal deinterleaved_sosi_arr    : t_dp_sosi_arr(g_nof_input * g_n_deinterleave-1 downto 0);
  signal selected_sosi_arr         : t_dp_sosi_arr(g_nof_input * g_n_deinterleave-1 downto 0) :=
                                         (others => c_dp_sosi_rst);
begin
  sim_only : if g_sim = true generate
    gen_deinterleave : for I in 0 to g_nof_input - 1 generate
      u_dp_deinterleave : entity dp_lib.dp_deinterleave_one_to_n
      generic map(
        g_pipeline => 0,
        g_nof_outputs => g_n_deinterleave
      )
      port map(
        rst   => rst,
        clk   => clk,

        snk_in => sp_sosi_arr(I),
        src_out_arr => deinterleaved_sosi_2arr_n(I)
      );

      gen_flat : for J in 0 to g_n_deinterleave-1 generate
        deinterleaved_sosi_arr(g_n_deinterleave * I + J) <= deinterleaved_sosi_2arr_n(I)(J);
      end generate;
    end generate;

    p_cnt : process(rst, clk)
    begin
      if rst = '1' then
        cnt <= 0;
      elsif rising_edge(clk) then
        if deinterleaved_sosi_arr(0).valid = '1' then
          if deinterleaved_sosi_arr(0).eop = '1' then
            cnt <= 0;
          else
            cnt <= cnt + 1;
          end if;
        end if;
      end if;
    end process;

    -- Select desired index.
    selected_sosi_arr <= deinterleaved_sosi_arr when cnt = g_selection;

    ---------------------------------------------------------------
    -- SIGNAL SCOPE
    ---------------------------------------------------------------
    u_dp_wideband_sp_arr_scope : entity dp_lib.dp_wideband_sp_arr_scope
    generic map (
      g_sim             => g_sim,
      g_use_sclk        => false,
      g_complex         => true,
      g_nof_streams     => g_nof_input * g_n_deinterleave,
      g_wideband_factor => 1,
      g_dat_w           => g_dat_w
    )
    port map (
      DCLK           => clk,
      sp_sosi_arr    => selected_sosi_arr,
      scope_sosi_arr => scope_sosi_arr
    );
  end generate;
end str;
