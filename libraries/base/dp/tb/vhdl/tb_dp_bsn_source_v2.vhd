-------------------------------------------------------------------------------
--
-- Copyright (C) 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: P. Donker, E. Kooistra
-- Purpose: Tb to verify that the BS source can be started in any PPS interval
--          to create the BSN grid and sync interval as defined in Fig. 3.1 in
--          [1].
-- Decsription:
-- * Start/Stop BSN source tests:
--   1) test once asynchronously (dp_on_pps='0') without automatic check, check
--      visualy in wave window.
--   2) test c_nof_repeat synchronously (dp_on_pps='1') with automatic check.
-- . Verify if bs_sosi.eop and bs_sosi.sop come in pairs
-- . Verify that bs_sosi.sync is at bs_sosi.sop
-- . Verify that bs_sosi has fixed latency with respect to ref_grid
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+Timing+in+Station
-- [2] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+BSN+source+with+offset
--
-- Usage:
-- > as 8, e.g. view bs_sosi, exp_grid and unexpected_bs_sync
-- > run -all
-- . sop, eop are verified automatically
-- . sync and bsn are verified automatically using the ref_grid

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_bsn_source_v2 is
  generic (
    g_pps_interval : natural := 16;  -- 101;
    g_block_size   : natural := 5  -- 23
  );
end tb_dp_bsn_source_v2;

architecture tb of tb_dp_bsn_source_v2 is
  -- The nof block per sync interval will be the same after every
  -- c_min_nof_pps_interval. The c_gcd is the greatest common divider of
  -- g_pps_interval and g_block_size, so g_block_size / c_gcd yields an
  -- integer. When g_pps_interval and g_block_size are relative prime,
  -- then c_gcd = 1, and then it takes g_block_size nof g_pps_interval
  -- for the pattern of c_nof_block_per_sync_lo and c_nof_block_per_sync_hi
  -- to repeat. If c_gcd = g_block_size, then c_nof_block_per_sync_lo =
  -- c_nof_block_per_sync_hi, so then the nof block per sync interval is
  -- the same in every pps interval.
  constant c_gcd                   : natural := gcd(g_pps_interval, g_block_size);
  constant c_min_nof_pps_interval  : natural := g_block_size / c_gcd;

  constant c_nof_block_per_sync_lo : natural := g_pps_interval / g_block_size;
  constant c_nof_block_per_sync_hi : natural := ceil_div(g_pps_interval, g_block_size);

  -- choose c_nof_pps and c_nof_repeat > c_min_nof_pps_interval, because the
  -- fractional sync pattern will repeat every c_min_nof_pps_interval number
  -- of g_pps_intervals.
  constant c_factor            : natural := 3;
  constant c_nof_pps           : natural := c_min_nof_pps_interval * c_factor;
  constant c_nof_repeat        : natural := c_min_nof_pps_interval * c_factor;

  constant c_clk_period        : time    := 10 ns;
  constant c_bsn_w             : natural := 31;
  constant c_bsn_time_offset_w : natural := ceil_log2(g_block_size);

  -- Minimum latency between sync and PPS, due to logic in DUT
  constant c_dut_latency       : natural := 3;

  -- The state name tells what kind of test is being done
  type t_state_enum is (
    s_disable,
    s_dp_on,
    s_dp_on_pps
  );

  -- Define the PPS (SSN) and BSN grid that both start at 0 according to Figure 3.1 in [1]:
  type t_time_grid is record
    pps  : std_logic;  -- pulse per second, g_pps_interval clk per pps interval
    ssn  : natural;  -- seconds sequence number
    bsn  : natural;  -- block sequence number, g_block_size clk per block
    sync : std_logic;  -- active at sop when pps is active or was active
    sop  : std_logic;  -- start of block
    eop  : std_logic;  -- end of block
  end record;

  constant c_time_grid_rst : t_time_grid := ('0', 0, 0, '0', '0', '0');

  -- Reference grid
  signal ref_grid        : t_time_grid := c_time_grid_rst;
  signal ssn_eop         : std_logic := '0';
  signal hold_pps        : std_logic := '0';
  signal nxt_hold_pps    : std_logic := '0';

  -- Tb
  signal tb_end       : std_logic := '0';
  signal rst          : std_logic := '1';
  signal clk          : std_logic := '1';
  signal tb_state        : t_state_enum := s_disable;

  -- DUT
  signal dp_on           : std_logic := '0';
  signal dp_on_pps       : std_logic := '0';
  signal dp_on_status    : std_logic;
  signal bs_restart      : std_logic;
  signal bs_new_interval : std_logic;
  signal tb_new_interval : std_logic := '0';
  signal bsn_init        : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '0');
  signal bsn_time_offset : std_logic_vector(c_bsn_time_offset_w - 1 downto 0) := (others => '0');
  signal bs_sosi         : t_dp_sosi;

  -- Verify
  signal exp_grid           : t_time_grid;
  signal unexpected_bs_sync : std_logic;
  signal sl0                : std_logic := '0';
  signal verify_en          : std_logic := '0';
  signal verify_sync        : std_logic := '0';
  signal hold_bs_sop        : std_logic;
  signal prev_bs_valid      : std_logic;
  signal bs_starts_cnt      : natural := 0;

  signal dbg_c_nof_pps      : natural := c_nof_pps;
  signal dbg_c_nof_repeat   : natural := c_nof_repeat;

  signal dbg_nof_blk        : natural;
  signal dbg_accumulate     : natural;
  signal dbg_expected_bsn   : natural;
begin
  rst <= '1', '0' after c_clk_period * 7;
  clk <= (not clk) or tb_end after c_clk_period / 2;

  -----------------------------------------------------------------------------
  -- Generate reference time grid
  -----------------------------------------------------------------------------
  proc_common_gen_pulse(1, g_pps_interval, '1', sl0, clk, ref_grid.pps);
  proc_common_gen_pulse(1, g_block_size, '1', sl0, clk, ref_grid.sop);
  ref_grid.eop <= ref_grid.sop'delayed((g_block_size - 1) * c_clk_period);
  ssn_eop <= ref_grid.pps'delayed((g_pps_interval - 1) * c_clk_period);
  ref_grid.ssn <= ref_grid.ssn + 1 when rising_edge(clk) and ssn_eop = '1';
  ref_grid.bsn <= ref_grid.bsn + 1 when rising_edge(clk) and ref_grid.eop = '1';

  -- Issue sync at start of block
  p_ref_grid_sync : process(ref_grid, hold_pps)
  begin
    ref_grid.sync <= '0';
    nxt_hold_pps <= hold_pps;

    if ref_grid.pps = '1' then
      if ref_grid.sop = '1' then
        ref_grid.sync <= '1';  -- immediately issue sync
      else
        nxt_hold_pps <= '1';  -- wait until next block
      end if;
    end if;

    if hold_pps = '1' then
      if ref_grid.sop = '1' then
        ref_grid.sync <= '1';  -- issue pending sync
        nxt_hold_pps <= '0';
      end if;
    end if;
  end process;

  hold_pps <= nxt_hold_pps when rising_edge(clk);

  exp_grid <= ref_grid'delayed(c_dut_latency * c_clk_period);

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------
  p_mm : process
    variable v_ssn             : natural;
    variable v_bsn_init        : natural;
    variable v_bsn_time_offset : natural;
  begin
    -- Get synchronous to clk
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    -- Start asynchronously by making dp_on high
    verify_en <= '0';  -- only verify visualy in wave window
    tb_state <= s_dp_on;
    dp_on_pps <= '0';
    dp_on <= '1';
    proc_common_wait_some_cycles(clk, c_nof_pps * g_pps_interval);
    tb_state <= s_disable;
    dp_on <= '0';
    dp_on_pps <= '0';
    proc_common_wait_some_cycles(clk, 10);

    -- Start synchronously by making dp_on and dp_on_pps high
    verify_en <= '1';  -- verify automatically in test bench

    for I in 0 to c_nof_repeat - 1 loop
      -- Wait some variable time between tests, to enforce testing different
      -- bsn_time_offset values
      proc_common_wait_some_cycles(clk, 20);
      proc_common_wait_some_cycles(clk, I * g_pps_interval);

      -- Wait until in the beginning of PPS interval
      proc_common_wait_until_hi_lo(clk, ref_grid.pps);
      proc_common_wait_some_cycles(clk, c_dut_latency);

      -- Determine bsn_init and bsn_time_offset for BSN source start
      -- . bsn_init = BSN at sync
      -- . bsn_time_offset = number of clk that sync occurs after PPS
      v_ssn := ref_grid.ssn + 1;  -- +1 to prepare start in next PPS interval
      v_bsn_init := ceil_div(v_SSN * g_pps_interval, g_block_size);  -- Equation 3.6 in [1]
      v_bsn_time_offset := v_bsn_init * g_block_size - v_SSN * g_pps_interval;  -- Equation 3.7 in [1]
      bsn_init <= TO_UVEC(v_bsn_init, c_bsn_w);  --
      bsn_time_offset <= TO_UVEC(v_bsn_time_offset, c_bsn_time_offset_w);
      -- Start synchronously by making dp_on and dp_on_pps high
      tb_state  <= s_dp_on_pps;
      dp_on_pps <= '1';
      dp_on <= '1';
      proc_common_wait_some_cycles(clk, c_nof_pps * g_pps_interval);
      tb_state <= s_disable;
      dp_on <= '0';
      dp_on_pps <= '0';
    end loop;

    proc_common_wait_some_cycles(clk, 10);
    assert bs_starts_cnt = 1 + c_nof_repeat
      report "Wrong number of BSN source starts."
      severity ERROR;

    tb_end <= '1';
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- Verification
  -- . Some aspects of bs_sosi are verified multiple times in different ways,
  --   this overlap is fine, because the tb and DUT are rather complicated, so
  --   using different approaches also helpt to verify the tb itself.
  -----------------------------------------------------------------------------
  verify_sync <= verify_en and bs_sosi.valid;

  proc_dp_verify_sop_and_eop(clk, bs_sosi.valid, bs_sosi.sop, bs_sosi.eop, hold_bs_sop);  -- Verify that sop and eop come in pairs
  --proc_dp_verify_sync(clk, verify_sync, bs_sosi.sync, exp_grid.sop, exp_grid.sync);  -- Verify sync at sop and at expected_sync

  -- Verify sync at sop and at expected_sync
  proc_dp_verify_sync(0,  -- start bsn of PPS grid and BSN grid is 0, see [1]
                      g_pps_interval,
                      g_block_size,
                      clk,
                      verify_en,
                      bs_sosi.sync,
                      bs_sosi.sop,
                      bs_sosi.bsn,
                      dbg_nof_blk,
                      dbg_accumulate,
                      dbg_expected_bsn);

  -- Verify bs_sosi by comparing with exp_grid, this again verifies bs_sosi.sync, sop and bsn
  p_verify_bs_sosi_grid : process(clk)
  begin
    if rising_edge(clk) then
      unexpected_bs_sync <= '0';
      if verify_en = '1' and bs_sosi.valid = '1' then
        assert TO_UINT(bs_sosi.bsn) = exp_grid.bsn
          report "Wrong bs_sosi.bsn /= exp_grid.bsn"
          severity ERROR;
        assert bs_sosi.sync = exp_grid.sync
          report "Wrong bs_sosi.sync /= exp_grid.sync"
          severity ERROR;
        assert bs_sosi.sop = exp_grid.sop
          report "Wrong bs_sosi.sop /= exp_grid.sop"
          severity ERROR;
        assert bs_sosi.eop = exp_grid.eop
          report "Wrong bs_sosi.eop /= exp_grid.eop"
          severity ERROR;
        -- Mark error in Wave window
        if bs_sosi.sync = '1' and bs_sosi.sync /= exp_grid.sync then
          unexpected_bs_sync <= '1';
        end if;
      end if;
    end if;
  end process;

  -- Verify that bs_sosi.valid = '1' did happen after dp_on by verifying bs_start
  prev_bs_valid <= bs_sosi.valid when rising_edge(clk);
  bs_starts_cnt <= bs_starts_cnt + 1 when rising_edge(clk) and bs_sosi.valid = '1' and prev_bs_valid = '0';

  p_verify_bs_restart : process(clk)
  begin
    if rising_edge(clk) then
      if bs_restart = '1' then
        assert bs_sosi.sync = '1'
          report "Unexpected bs_start while bs_sosi.sync /= 1"
          severity ERROR;
        assert prev_bs_valid = '0'
          report "Unexpected bs_start while prev_bs_valid /= 0"
          severity ERROR;
      end if;
    end if;
  end process;

  p_verify_bs_new_interval : process(clk)
  begin
    if rising_edge(clk) then
      if bs_restart = '1' then
        assert bs_new_interval = '1'
          report "Wrong begin of bs_new_interval"
          severity ERROR;
        tb_new_interval <= '1';
      elsif bs_sosi.sync = '1' then
        assert bs_new_interval = '0'
          report "Wrong end of bs_new_interval"
          severity ERROR;
        tb_new_interval <= '0';
      elsif tb_new_interval = '1' then
        assert bs_new_interval = '1'
          report "Wrong level during bs_new_interval"
          severity ERROR;
      else
        assert bs_new_interval = '0'
          report "Unexpected bs_new_interval"
          severity ERROR;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- DUT: dp_bsn_source_v2
  -----------------------------------------------------------------------------

  dut : entity work.dp_bsn_source_v2
  generic map (
    g_block_size         => g_block_size,
    g_nof_clk_per_sync   => g_pps_interval,
    g_bsn_w              => c_bsn_w,
    g_bsn_time_offset_w  => c_bsn_time_offset_w
  )
  port map (
    rst              => rst,
    clk              => clk,
    pps              => ref_grid.pps,
    -- MM control
    dp_on            => dp_on,
    dp_on_pps        => dp_on_pps,

    dp_on_status     => dp_on_status,  -- = src_out.valid
    bs_restart       => bs_restart,  -- = src_out.sync for first sync after dp_on went high
    bs_new_interval  => bs_new_interval,  -- active during first src_out.sync interval

    bsn_init         => bsn_init,
    bsn_time_offset  => bsn_time_offset,

    -- Streaming
    src_out          => bs_sosi
  );
end tb;
