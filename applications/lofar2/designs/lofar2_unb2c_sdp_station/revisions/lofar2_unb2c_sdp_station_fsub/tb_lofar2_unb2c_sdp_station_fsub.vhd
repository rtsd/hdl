-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle (original), E. Kooistra (updates)
-- Purpose: Self-checking testbench for simulating lofar2_unb2c_sdp_station_fsub using WG data.
--
-- Description:
--   MM control actions:
--
--   1) Enable calc mode for WG via reg_diag_wg with:
--        freq = 19.921875MHz
--        ampl = 0.5 * 2**13
--
--   2) Read current BSN from reg_bsn_scheduler_wg and write reg_bsn_scheduler_wg
--      to trigger start of WG at BSN.
--
--   3) Read subband statistics (SST) via MM and verify with exp_sp_subband_sst at g_subband.
--      . use weighted subbands (default selected by MM)
--      . g_use_cross_weight = TRUE, then use g_sp_cross_subband_weight_gain/phase and c_cross_sp
--      . g_use_cross_weight = FALSE, then do not use c_cross_sp, so only use g_sp and
--        g_co_subband_weight_gain/phase
--
--   4) View in wave window
--      . in_sosi.sop and in_data in u_si_arr(g_sp) to check that:
--        - WG starts with zero phase sine when c_subband_phase = 0.0 degrees
--        - WG amplitude = 8191 (is full scale - 1) when wg_ampl = 1.0
--      . pfb_sosi_arr(c_pfb_index).im/re and fsub_sosi_arr(c_pfb_index).im/re
--        in u_fsub in decimal radix and analog format to check that subband
--        phase is g_co_subband_weight_phase phase as set by the subband weight.
--        - Raw: pfb_sosi_arr = atan2(-65195 / 0) = -90 degrees
--        - Weighted: fsub_sosi_arr = atan2(-56457 / 32598) = -60 degrees
--          --> rotated expected g_co_subband_weight_phase = -60 - -90 = +30 degrees.
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   # Manually add missing signal
--   > add wave -position insertpoint  \
--     sim:/tb_lofar2_unb2c_sdp_station_fsub/sp_subband_ssts_arr2
--   > run -a
--   # Takes about 30 m when g_read_all_SST = FALSE
--   # Takes about 40 m when g_read_all_SST = TRUE
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2c_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, lofar2_unb2c_sdp_station_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use lofar2_sdp_lib.tb_sdp_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;

entity tb_lofar2_unb2c_sdp_station_fsub is
  generic (
    g_sp                            : natural := 3;  -- signal path index in range(S_pn = 12) of co-polarization
    g_co_wg_ampl                    : real := 0.5;  -- WG normalized amplitude, use same WG settings for both polarizations (g_sp and c_cross_sp)
    g_cross_wg_ampl                 : real := 0.4;  -- WG normalized amplitude, use same WG settings for both polarizations (g_sp and c_cross_sp)
    g_cross_wg_phase                : real := 90.0;  -- WG phase in degrees for cross-sp, relative to co-sp
    g_subband                       : natural := 102;  -- select subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
    g_co_subband_weight_gain        : real := 1.0;  -- subband weight normalized gain, for co-polarization in g_sp
    g_co_subband_weight_phase       : real := 30.0;  -- subband weight phase rotation in degrees, for co-polarization in g_sp
    g_use_cross_weight              : boolean := true;
    g_sp_cross_subband_weight_gain  : real := 0.5;  -- subband weight normalized gain, for cross polarization of g_sp
    g_sp_cross_subband_weight_phase : real := -10.0;  -- subband weight phase rotation in degrees, for cross polarization of g_sp
    g_read_all_SST                  : boolean := true  -- when FALSE only read SST for g_subband, to save sim time
  );
end tb_lofar2_unb2c_sdp_station_fsub;

architecture tb of tb_lofar2_unb2c_sdp_station_fsub is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_init_bsn        : natural := 17;  -- some recognizable value >= 0

  -- signal path index of cross-polarization
  constant c_cross_sp        : natural := sel_a_b(g_sp mod c_sdp_N_pol = 0, g_sp + 1, g_sp - 1);

  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C

  constant c_nof_block_per_sync  : natural := 16;
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_pps_period          : natural := c_nof_clk_per_sync;
  constant c_wpfb_sim            : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands, c_nof_block_per_sync);
  constant c_stat_data_sz        : natural := c_wpfb_sim.stat_data_sz;  -- = 2

  constant c_percentage          : real := 0.05;  -- percentage that actual value may differ from expected value
  constant c_lo_factor           : real := 1.0 - c_percentage;  -- lower boundary
  constant c_hi_factor           : real := 1.0 + c_percentage;  -- higher boundary

  -- WG
  constant c_bsn_start_wg         : natural := c_init_bsn + 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  -- .ampl
  constant c_co_wg_ampl           : natural := natural(g_co_wg_ampl * real(c_sdp_FS_adc));  -- in number of lsb
  constant c_cross_wg_ampl        : natural := natural(g_cross_wg_ampl * real(c_sdp_FS_adc));  -- in number of lsb
  constant c_exp_co_sp_power      : real := real(c_co_wg_ampl**2) / 2.0;
  constant c_exp_cross_sp_power   : real := real(c_cross_wg_ampl**2) / 2.0;
  constant c_exp_co_sp_ast        : real := c_exp_co_sp_power * real(c_nof_clk_per_sync);
  constant c_exp_cross_sp_ast     : real := c_exp_cross_sp_power * real(c_nof_clk_per_sync);
  -- . freq
  constant c_subband_freq         : real := real(g_subband) / real(c_sdp_N_fft);  -- normalized by fs = f_adc = 200 MHz is dp_clk
  -- . phase
  constant c_subband_phase        : real := 0.0;  -- wanted subband phase in degrees = WG phase at sop
  constant c_wg_latency           : integer := c_diag_wg_latency + c_sdp_shiftram_latency - 0;  -- -0 to account for BSN scheduler start trigger latency
  constant c_wg_phase_offset      : real := 360.0 * real(c_wg_latency) * c_subband_freq;  -- c_diag_wg_latency is in dp_clk cycles
  constant c_wg_phase             : real := c_subband_phase + c_wg_phase_offset;  -- WG phase in degrees
  constant c_co_wg_phase          : real := c_wg_phase;
  constant c_cross_wg_phase       : real := c_wg_phase + g_cross_wg_phase;
  constant c_subband_phase_offset : real := -90.0;  -- WG with zero phase sinus yields subband with -90 degrees phase (negative Im, zero Re)
  constant c_co_subband_phase     : real := c_subband_phase_offset + c_co_wg_phase;
  constant c_cross_subband_phase  : real := c_subband_phase_offset + c_cross_wg_phase;

  -- FSUB
  constant c_pol_index            : natural := g_sp mod c_sdp_Q_fft;
  constant c_pfb_index            : natural := g_sp / c_sdp_Q_fft;  -- only read used WPFB unit out of range(c_sdp_P_pfb = 6)

  constant c_exp_co_subband_ampl_raw          : real := real(c_co_wg_ampl) * c_sdp_wpfb_subband_sp_ampl_ratio;
  constant c_exp_cross_subband_ampl_raw       : real := real(c_cross_wg_ampl) * c_sdp_wpfb_subband_sp_ampl_ratio;
  constant c_exp_co_subband_ampl_weighted     : real := c_exp_co_subband_ampl_raw * g_co_subband_weight_gain;
  constant c_exp_cross_subband_ampl_weighted  : real := c_exp_cross_subband_ampl_raw * 1.0;  -- unit gain, this is co gain for cross sp
  constant c_exp_jones_subband_tuple          : t_real_arr(0 to 3) := func_sdp_subband_equalizer(
                                                c_exp_co_subband_ampl_raw, c_co_subband_phase, g_co_subband_weight_gain, g_co_subband_weight_phase,
                                                c_exp_cross_subband_ampl_raw, c_cross_subband_phase, g_sp_cross_subband_weight_gain, g_sp_cross_subband_weight_phase);
  constant c_exp_sp_subband_ampl_weighted     : real := sel_a_b(g_use_cross_weight, c_exp_jones_subband_tuple(0), c_exp_co_subband_ampl_weighted);

  constant c_exp_co_subband_power_raw         : real := c_exp_co_subband_ampl_raw**2.0;  -- complex signal ampl, so power is A**2 (not A**2 / 2 as for real)
  constant c_exp_cross_subband_power_raw      : real := c_exp_cross_subband_ampl_raw**2.0;
  constant c_exp_sp_subband_power_weighted    : real := c_exp_sp_subband_ampl_weighted**2.0;
  constant c_exp_cross_subband_power_weighted : real := c_exp_cross_subband_ampl_weighted**2.0;
  constant c_exp_co_subband_sst_raw           : real := c_exp_co_subband_power_raw * real(c_nof_block_per_sync);
  constant c_exp_cross_subband_sst_raw        : real := c_exp_cross_subband_power_raw * real(c_nof_block_per_sync);
  constant c_exp_sp_subband_sst_weighted      : real := c_exp_sp_subband_power_weighted * real(c_nof_block_per_sync);
  constant c_exp_cross_subband_sst_weighted   : real := c_exp_cross_subband_power_weighted * real(c_nof_block_per_sync);

  -- . expected limit values, obtained with print_str() for g_subband = 102,
  --   g_co_wg_ampl = 1.0, g_co_subband_weight_gain = 1.0, g_co_subband_weight_phase = 30.0
  constant c_exp_sp_subband_sst_leakage_snr_dB   : real := 70.0;  -- < 74.913
  constant c_exp_sp_subband_sst_crosstalk_snr_dB : real := 90.0;  -- < 96.284

  type t_real_arr is array (integer range <>) of real;
  type t_slv_64_subbands_arr is array (integer range <>) of t_slv_64_arr(0 to c_sdp_N_sub - 1);

  -- . Subband weights for selected g_sp
  constant c_co_subband_weight_re : integer := integer(g_co_subband_weight_gain * real(c_sdp_unit_sub_weight) * COS(g_co_subband_weight_phase * MATH_2_PI / 360.0));
  constant c_co_subband_weight_im : integer := integer(g_co_subband_weight_gain * real(c_sdp_unit_sub_weight) * SIN(g_co_subband_weight_phase * MATH_2_PI / 360.0));

  -- . Subband weights cross for selected g_sp
  constant c_sp_cross_subband_weight_re : integer := integer(g_sp_cross_subband_weight_gain * real(c_sdp_unit_sub_weight) * COS(g_sp_cross_subband_weight_phase * MATH_2_PI / 360.0));
  constant c_sp_cross_subband_weight_im : integer := integer(g_sp_cross_subband_weight_gain * real(c_sdp_unit_sub_weight) * SIN(g_sp_cross_subband_weight_phase * MATH_2_PI / 360.0));

  -- MM
  -- . Address widths of a single MM instance
  constant c_addr_w_reg_diag_wg           : natural := 2;
  -- . Address spans of a single MM instance
  constant c_mm_span_reg_diag_wg          : natural := 2**c_addr_w_reg_diag_wg;

  constant c_mm_file_reg_bsn_source_v2         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_bsn_scheduler_wg      : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg               : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_ram_equalizer_gains       : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_EQUALIZER_GAINS";
  constant c_mm_file_ram_equalizer_gains_cross : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_EQUALIZER_GAINS_CROSS";
  constant c_mm_file_reg_dp_selector           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DP_SELECTOR";
  constant c_mm_file_ram_st_sst                : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_ST_SST";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0) := (others => '0');

  signal pps_rst             : std_logic := '1';
  signal gen_pps             : std_logic := '0';

  -- WG
  signal current_bsn_wg      : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- FSUB
  -- . WPFB
  signal sp_subband_ssts_arr2            : t_slv_64_subbands_arr(c_sdp_N_pol - 1 downto 0);  -- [pol][sub]
  signal sp_subband_sst_sum_arr          : t_real_arr(c_sdp_N_pol - 1 downto 0) := (others => 0.0);
  signal sp_subband_sst                  : real := 0.0;
  signal sp_cross_subband_sst            : real := 0.0;
  signal sp_subband_sst_leakage          : real := 0.0;
  signal sp_subband_sst_leakage_snr_dB   : real := 0.0;  -- signal to noise (leakage) ratio
  signal sp_subband_sst_crosstalk        : real := 0.0;
  signal sp_subband_sst_crosstalk_snr_dB : real := 0.0;  -- signal to noise (crosstalk) ration

  signal exp_sp_subband_ampl       : real := 0.0;
  signal exp_sp_subband_power      : real := 0.0;
  signal exp_sp_subband_sst        : real := 0.0;
  signal exp_cross_subband_sst  : real := 0.0;
  signal stat_data              : std_logic_vector(c_longword_w - 1 downto 0);

  -- . Selector
  signal sst_offload_weighted_subbands     : std_logic;

  -- . Subband equalizer
  signal sp_co_subband_weight_re    : integer := 0;
  signal sp_co_subband_weight_im    : integer := 0;
  signal sp_co_subband_weight_gain  : real := 0.0;
  signal sp_co_subband_weight_phase : real := 0.0;
  signal sp_co_subband_weight_val   : std_logic := '0';

  signal sp_cross_subband_weight_re    : integer := 0;
  signal sp_cross_subband_weight_im    : integer := 0;
  signal sp_cross_subband_weight_gain  : real := 0.0;
  signal sp_cross_subband_weight_phase : real := 0.0;
  signal sp_cross_subband_weight_val   : std_logic := '0';

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal ext_pps             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_txp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_rxp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref     : std_logic;
  signal jesd204b_sync_n     : std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(5, c_pps_period, '1', pps_rst, ext_clk, gen_pps);
  jesd204b_sysref <= gen_pps;
  ext_pps <= gen_pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2c_sdp_station_fsub : entity lofar2_unb2c_sdp_station_lib.lofar2_unb2c_sdp_station
  generic map (
    g_design_name            => "lofar2_unb2c_sdp_station_fsub",
    g_design_note            => "",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr,
    g_wpfb                   => c_wpfb_sim,
    g_bsn_nof_clk_per_sync   => c_nof_clk_per_sync,
    g_scope_selected_subband => g_subband
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => ext_pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK      => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => jesd204b_sysref,
    JESD204B_SYNC_N => jesd204b_sync_n
  );

  -- Raw or weighted subbands
  exp_sp_subband_ampl      <= sel_a_b(sst_offload_weighted_subbands = '0', c_exp_co_subband_ampl_raw, c_exp_sp_subband_ampl_weighted);
  exp_sp_subband_power     <= sel_a_b(sst_offload_weighted_subbands = '0', c_exp_co_subband_power_raw, c_exp_sp_subband_power_weighted);
  exp_sp_subband_sst       <= sel_a_b(sst_offload_weighted_subbands = '0', c_exp_co_subband_sst_raw, c_exp_sp_subband_sst_weighted);
  exp_cross_subband_sst <= sel_a_b(sst_offload_weighted_subbands = '0', c_exp_cross_subband_sst_raw, c_exp_cross_subband_sst_weighted);

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn                           : natural;
    variable v_data_lo, v_data_hi            : std_logic_vector(c_word_w - 1 downto 0);
    variable v_stat_data                     : std_logic_vector(c_longword_w - 1 downto 0);
    variable v_len, v_span, v_offset, v_addr : natural;  -- address ranges, indices
    variable v_W, v_P, v_U, v_S, v_B         : natural;  -- array indicies
    variable v_re, v_im, v_weight            : integer;
    variable v_power                         : real;
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    ----------------------------------------------------------------------------
    -- Enable BSN
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 2,         c_init_bsn, tb_clk);  -- Init BSN
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 3,                  0, tb_clk);  -- Write high part activates the init BSN
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 1, c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0,       16#00000003#, tb_clk);  -- Enable BSN at PPS

    -- Release PPS pulser, to get first PPS now and to start BSN source
    wait for 1 us;
    pps_rst <= '0';

    ----------------------------------------------------------------------------
    -- Read weighted subband selector
    ----------------------------------------------------------------------------
    mmf_mm_bus_rd(c_mm_file_reg_dp_selector, 0, rd_data, tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);
    sst_offload_weighted_subbands <= not rd_data(0);

    ----------------------------------------------------------------------------
    -- Enable and start WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    -- g_sp is co-polarization
    v_offset := g_sp * c_mm_span_reg_diag_wg;
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 1, integer(c_co_wg_phase * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 2, integer(real(g_subband) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 3, integer(real(c_co_wg_ampl) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl

    if g_use_cross_weight then
      -- c_cross_sp is cross-polarization for g_sp, use same WG settings for c_cross_sp as for g_sp
      v_offset := c_cross_sp * c_mm_span_reg_diag_wg;
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 1, integer(c_cross_wg_phase * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 2, integer(real(g_subband) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 3, integer(real(c_cross_wg_ampl) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl
    end if;

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, c_bsn_start_wg, tb_clk);  -- first write low then high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,              0, tb_clk);  -- assume v_bsn < 2**31-1

    ----------------------------------------------------------------------------
    -- Write subband weight for selected g_sp and g_subband
    ----------------------------------------------------------------------------

    -- Co-polarization subband weight for g_sp
    -- . MM format: (cint16)RAM_EQUALIZER_GAINS[S_pn/Q_fft]_[Q_fft][N_sub] = [S_pn][N_sub]
    v_addr := g_sp * c_sdp_N_sub + g_subband;
    -- . read
    mmf_mm_bus_rd(c_mm_file_ram_equalizer_gains, v_addr, rd_data, tb_clk);
    v_re := unpack_complex_re(rd_data, c_sdp_W_sub_weight);
    v_im := unpack_complex_im(rd_data, c_sdp_W_sub_weight);
    sp_co_subband_weight_re <= v_re;
    sp_co_subband_weight_im <= v_im;
    sp_co_subband_weight_gain <= COMPLEX_RADIUS(real(v_re), real(v_im)) / real(c_sdp_unit_sub_weight);
    sp_co_subband_weight_phase <= COMPLEX_PHASE(real(v_re), real(v_im));
    sp_co_subband_weight_val <= '1';
    proc_common_wait_some_cycles(tb_clk, 1);
    assert sp_co_subband_weight_re = c_sdp_unit_sub_weight
      report "Default sp_co_subband_weight_re /= c_sdp_unit_sub_weight"
      severity ERROR;
    assert sp_co_subband_weight_im = 0
      report "Default sp_co_subband_weight_im /= 0"
      severity ERROR;
    -- . write
    v_weight := pack_complex(re => c_co_subband_weight_re, im => c_co_subband_weight_im, w => c_sdp_W_sub_weight);  -- c_sdp_W_sub_weight = 16 bit
    mmf_mm_bus_wr(c_mm_file_ram_equalizer_gains, v_addr, v_weight, tb_clk);
    proc_common_wait_cross_clock_domain_latency(c_tb_clk_period, c_ext_clk_period, c_common_cross_clock_domain_latency * 2);
    -- . read back
    mmf_mm_bus_rd(c_mm_file_ram_equalizer_gains, v_addr, rd_data, tb_clk);
    v_re := unpack_complex_re(rd_data, c_sdp_W_sub_weight);
    v_im := unpack_complex_im(rd_data, c_sdp_W_sub_weight);
    sp_co_subband_weight_re <= v_re;
    sp_co_subband_weight_im <= v_im;
    sp_co_subband_weight_gain <= COMPLEX_RADIUS(real(v_re), real(v_im)) / real(c_sdp_unit_sub_weight);
    sp_co_subband_weight_phase <= COMPLEX_PHASE(real(v_re), real(v_im));
      proc_common_wait_some_cycles(tb_clk, 1);
    assert sp_co_subband_weight_re = c_co_subband_weight_re
      report "Readback sp_co_subband_weight_re /= c_co_subband_weight_re"
      severity ERROR;
    assert sp_co_subband_weight_im = c_co_subband_weight_im
      report "Readback sp_co_subband_weight_im /= c_co_subband_weight_im"
      severity ERROR;

    if g_use_cross_weight then
      -- Cross-polarization subband weight for g_sp
      -- . MM format: (cint16)RAM_EQUALIZER_GAINS_CROSS[S_pn/Q_fft]_[Q_fft][N_sub] = [S_pn][N_sub]
      v_addr := g_sp * c_sdp_N_sub + g_subband;
      -- . read
      mmf_mm_bus_rd(c_mm_file_ram_equalizer_gains_cross, v_addr, rd_data, tb_clk);
      v_re := unpack_complex_re(rd_data, c_sdp_W_sub_weight);
      v_im := unpack_complex_im(rd_data, c_sdp_W_sub_weight);
      sp_cross_subband_weight_re <= v_re;
      sp_cross_subband_weight_im <= v_im;
      sp_cross_subband_weight_gain <= COMPLEX_RADIUS(real(v_re), real(v_im)) / real(c_sdp_unit_sub_weight);
      sp_cross_subband_weight_phase <= COMPLEX_PHASE(real(v_re), real(v_im));
      sp_cross_subband_weight_val <= '1';
      proc_common_wait_some_cycles(tb_clk, 1);
      assert sp_cross_subband_weight_re = 0
        report "Default sp_cross_subband_weight_re /= 0"
        severity ERROR;
      assert sp_cross_subband_weight_im = 0
        report "Default sp_cross_subband_weight_im /= 0"
        severity ERROR;
      -- . write
      v_weight := pack_complex(re => c_sp_cross_subband_weight_re, im => c_sp_cross_subband_weight_im, w => c_sdp_W_sub_weight);  -- c_sdp_W_sub_weight = 16 bit
      mmf_mm_bus_wr(c_mm_file_ram_equalizer_gains_cross, v_addr, v_weight, tb_clk);
      proc_common_wait_cross_clock_domain_latency(c_tb_clk_period, c_ext_clk_period, c_common_cross_clock_domain_latency * 2);
      -- . read back
      mmf_mm_bus_rd(c_mm_file_ram_equalizer_gains_cross, v_addr, rd_data, tb_clk);
      v_re := unpack_complex_re(rd_data, c_sdp_W_sub_weight);
      v_im := unpack_complex_im(rd_data, c_sdp_W_sub_weight);
      sp_cross_subband_weight_re <= v_re;
      sp_cross_subband_weight_im <= v_im;
      sp_cross_subband_weight_gain <= COMPLEX_RADIUS(real(v_re), real(v_im)) / real(c_sdp_unit_sub_weight);
      sp_cross_subband_weight_phase <= COMPLEX_PHASE(real(v_re), real(v_im));
      proc_common_wait_some_cycles(tb_clk, 1);
      assert sp_cross_subband_weight_re = c_sp_cross_subband_weight_re
        report "Readback sp_cross_subband_weight_re /= c_sp_cross_subband_weight_re"
        severity ERROR;
      assert sp_cross_subband_weight_im = c_sp_cross_subband_weight_im
        report "Readback sp_cross_subband_weight_im /= c_sp_cross_subband_weight_im"
        severity ERROR;
    end if;

    ----------------------------------------------------------------------------
    -- Wait for enough WG data and start of sync interval
    ----------------------------------------------------------------------------
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                            "UNSIGNED", rd_data, ">=", c_init_bsn + c_nof_block_per_sync * 3,  -- this is the wait until condition
                            c_sdp_T_sub, tb_clk);

    ---------------------------------------------------------------------------
    -- Read subband statistics
    ---------------------------------------------------------------------------
    -- . the subband statistics are c_stat_data_sz = 2 word power values.
    -- . there are c_sdp_S_pn = 12 signal inputs A, B, C, D, E, F, G, H, I, J, K, L
    -- . there are c_sdp_N_sub = 512 subbands per signal input (SI, = signal path, SP)
    -- . one complex WPFB can process two real inputs A, B, so there are c_sdp_P_pfb = 6 WPFB units,
    --   but only read for the 1 WPFB unit of the selected g_sp, to save sim time
    -- . the outputs for A, B are time multiplexed, c_sdp_Q_fft = 2, assume that they
    --   correspond to the c_sdp_N_pol = 2 signal polarizations
    -- . the subbands are output alternately so A0 B0 A1 B1 ... A511 B511 for input A, B
    -- . the subband statistics multiple WPFB units appear in order in the ram_st_sst address map
    -- . the subband statistics are stored first lo word 0 then hi word 1
    v_len := c_sdp_N_sub * c_sdp_N_pol * c_stat_data_sz;  -- 2048 = 512 * 2 * 64/32
    v_span := true_log_pow2(v_len);  -- = 2048
    for I in 0 to v_len - 1 loop
      v_W := I mod c_stat_data_sz;  -- 0, 1 per statistics word, word index
      v_P := (I / c_stat_data_sz) mod c_sdp_N_pol;  -- 0, 1 per SP pol, polarization index
      v_B := I / (c_sdp_N_pol * c_stat_data_sz);  -- subband index, range(N_sub = 512) per dual pol
      v_addr := I + c_pfb_index * v_span;  -- MM address for WPFB unit of selected g_sp
      -- Only read SST for g_subband for dual pol SP, to save sim time
      if g_read_all_SST = true or v_B = g_subband then
        if v_W = 0 then
          -- low part
          mmf_mm_bus_rd(c_mm_file_ram_st_sst, v_addr, rd_data, tb_clk);
          v_data_lo := rd_data;
        else
          -- high part
          mmf_mm_bus_rd(c_mm_file_ram_st_sst, v_addr, rd_data, tb_clk);
          v_data_hi := rd_data;
          v_stat_data := v_data_hi & v_data_lo;

          sp_subband_ssts_arr2(v_P)(v_B) <= v_stat_data;
          stat_data <= v_stat_data;  -- for time series view in Wave window

          -- sum of all subband powers per pol
          sp_subband_sst_sum_arr(v_P) <= sp_subband_sst_sum_arr(v_P) + TO_UREAL(v_stat_data);
        end if;
      end if;
    end loop;
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Subband power of g_subband in g_sp
    -- . For the selected g_subband in g_sp the sp_subband_sst will be close
    --   to sp_subband_sst_sum_arr(c_pol_index), because the input is a
    --   sinus, so most power will be in 1 subband.
    sp_subband_sst <= TO_UREAL(sp_subband_ssts_arr2(c_pol_index)(g_subband));
    -- Subband power of g_subband in c_cross_sp
    sp_cross_subband_sst <= TO_UREAL(sp_subband_ssts_arr2(not_int(c_pol_index))(g_subband));
    proc_common_wait_some_cycles(tb_clk, 1);

    -- The sp_subband_sst_leakage shows how much power from the input sinus at a specific
    -- subband has leaked into the N_sub-1 = 511 other subbands. The power ratio yields an
    -- indication of the SNR, although that also depends on the SNR of the WG sinus.
    v_power := sp_subband_sst_sum_arr(c_pol_index) - sp_subband_sst;
    sp_subband_sst_leakage <= v_power;
    if sp_subband_sst > c_eps and v_power > c_eps then
      sp_subband_sst_leakage_snr_dB <= 10.0 * LOG10(sp_subband_sst / v_power);
    elsif g_read_all_SST then
      report "Wrong, zero leakage is unexpected for SP-" & natural'image(g_sp)
        severity ERROR;
    end if;

    if not g_use_cross_weight then
      -- The other WPFB input WG at c_cross_sp is not used, so it should have ~ zero power.
      -- The sp_subband_sst_crosstalk shows how much power from one WPFB input cross talks
      -- into the other output, due to quantization cross talk in the complex FFT. The power
      -- ration indicates the suppression, provided that the other input was zero.
      v_power := sp_subband_sst_sum_arr(not_int(c_pol_index));  -- not_int(0) = 1, not_int(/= 0) = 0
      sp_subband_sst_crosstalk <= v_power;
      if sp_subband_sst > c_eps and v_power > c_eps then
        sp_subband_sst_crosstalk_snr_dB <= 10.0 * LOG10(sp_subband_sst / v_power);
      elsif g_read_all_SST then
        report "Zero crosstalk for SP-" & natural'image(g_sp)
          severity NOTE;
      end if;
    end if;

    proc_common_wait_some_cycles(tb_clk, 10);

    ---------------------------------------------------------------------------
    -- Log WG, subband statistics
    ---------------------------------------------------------------------------
    print_str("");
    print_str("WG:");
    print_str(". c_co_wg_ampl for g_sp                        = " & int_to_str(c_co_wg_ampl));
    print_str(". c_cross_wg_ampl for c_cross_sp               = " & int_to_str(c_cross_wg_ampl));

    print_str("");
    print_str("Subband selector:");
    print_str(". sst_offload_weighted_subbands                = " & sl_to_str(sst_offload_weighted_subbands));

    print_str("");
    print_str("Subband weights for g_sp:");
    print_str(". sp_co_subband_weight_gain                    = " & real_to_str(sp_co_subband_weight_gain, 20, 6));
    print_str(". sp_co_subband_weight_phase                   = " & real_to_str(sp_co_subband_weight_phase, 20, 6));
    if g_use_cross_weight then
      print_str(". sp_cross_subband_weight_gain                 = " & real_to_str(sp_cross_subband_weight_gain, 20, 6));
      print_str(". sp_cross_subband_weight_phase                = " & real_to_str(sp_cross_subband_weight_phase, 20, 6));
    end if;

    print_str("");
    print_str("SST results:");
    print_str(". exp_sp_subband_ampl                          = " & int_to_str(natural(exp_sp_subband_ampl)));
    print_str(". exp_sp_subband_power                         = " & real_to_str(exp_sp_subband_power, 20, 1));
    print_str(". exp_sp_subband_sst                           = " & real_to_str(exp_sp_subband_sst, 20, 1));
    print_str(". exp_cross_subband_sst                        = " & real_to_str(exp_cross_subband_sst, 20, 1));
    print_str("");
    print_str(". sp_subband_sst                               = " & real_to_str(sp_subband_sst, 20, 1));
    print_str(". sp_subband_sst / exp_sp_subband_sst          = " & real_to_str(sp_subband_sst / exp_sp_subband_sst, 20, 6));
    print_str(". sp_cross_subband_sst                         = " & real_to_str(sp_cross_subband_sst, 20, 1));
    print_str(". sp_cross_subband_sst / exp_cross_subband_sst = " & real_to_str(sp_cross_subband_sst / exp_cross_subband_sst, 20, 6));

    if g_read_all_SST then
      -- Log WPFB details, these are allready verified in tb of wpfb_unit_dev.vhd, so here
      -- quality indicators like leakage and crosstalk are also reported out of interest.
      print_str("");
      print_str("SST quality indicators");
      print_str(". sp_subband_sst_leakage                       = " & real_to_str(sp_subband_sst_leakage, 20, 0));
      print_str(". sp_subband_sst_leakage_snr_dB                = " & real_to_str(sp_subband_sst_leakage_snr_dB, 20, 3));
      if not g_use_cross_weight then
        print_str(". sp_subband_sst_crosstalk                     = " & real_to_str(sp_subband_sst_crosstalk, 20, 0));
        print_str(". sp_subband_sst_crosstalk_snr_db              = " & real_to_str(sp_subband_sst_crosstalk_snr_db, 20, 3));
      end if;
    end if;

    ---------------------------------------------------------------------------
    -- Verify SST
    ---------------------------------------------------------------------------
    -- Verify expected subband power based on WG power for g_sp
    assert sp_subband_sst > c_lo_factor * exp_sp_subband_sst
      report "Wrong subband power for SP " & natural'image(g_sp)
      severity ERROR;
    assert sp_subband_sst < c_hi_factor * exp_sp_subband_sst
      report "Wrong subband power for SP " & natural'image(g_sp)
      severity ERROR;
    if g_use_cross_weight then
      -- Verify expected subband power based on WG power for c_cross_sp
      -- The other WPFB input WG at c_cross_sp is used as cross polarization input, with default
      -- unit co-polarization subband weight and zero cross-polarization subband weight.
      assert sp_cross_subband_sst > c_lo_factor * exp_cross_subband_sst
        report "Wrong subband power for cross SP " & natural'image(c_cross_sp)
        severity ERROR;
      assert sp_cross_subband_sst < c_hi_factor * exp_cross_subband_sst
        report "Wrong subband power for cross SP " & natural'image(c_cross_sp)
        severity ERROR;
    end if;

    if g_read_all_SST then
      -- Verify expected SNR quality measures
      assert sp_subband_sst_leakage = 0.0 or sp_subband_sst_leakage_snr_dB > c_exp_sp_subband_sst_leakage_snr_dB
        report "Wrong too much leakage for SP " & natural'image(g_sp)
        severity ERROR;
      if not g_use_cross_weight then
        assert sp_subband_sst_crosstalk = 0.0 or sp_subband_sst_crosstalk_snr_dB > c_exp_sp_subband_sst_crosstalk_snr_dB
          report "Wrong too much crosstalk for SP " & natural'image(g_sp)
          severity ERROR;
      end if;
    end if;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;
end tb;
