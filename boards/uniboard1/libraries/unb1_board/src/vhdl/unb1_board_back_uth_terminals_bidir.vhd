-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Instantiate uthernet TX and/or RX terminals for UniBoard back
-- Description:
-- Remark: This file is identical to unb1_board_mesh_uth_terminals_bidir.vhd
--         except for the SOSI entity I/O types and the monitor outputs.

library IEEE, common_lib, dp_lib, uth_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_packet_pkg.all;
use work.unb1_board_pkg.all;
use uth_lib.uth_pkg.all;

entity unb1_board_back_uth_terminals_bidir is
  generic (
    -- User
    g_usr_nof_streams     : natural := 4;  -- number of user streams per bus
    g_usr_use_complex     : boolean := false;  -- when TRUE transport sosi im & re fields via DP data, else transport sosi data via DP data
    g_usr_data_w          : natural := 32;
    g_usr_frame_len       : natural := 20;
    -- DP/UTH packet
    g_packet_data_w       : natural := 32;  -- packet data width for DP packet and for UTH packet, must be >= g_usr_data_w
    -- Phy
    g_phy_nof_serial      : natural := 4;  -- up to 4 serial lanes per bus
    -- Tx
    g_use_tx              : boolean := true;
    g_tx_input_use_fifo   : boolean := true;  -- Tx input uses FIFO to create slack for inserting DP Packet and Uthernet frame headers
    g_tx_input_fifo_size  : natural := c_bram_m9k_fifo_depth;  -- g_tx_input_use_fifo=TRUE, choose to use full BRAM size = 256 for FIFO depth at input to uth_terminal_tx
    g_tx_input_fifo_fill  : natural := 0;
    -- Rx
    g_use_rx              : boolean := true;
    g_rx_output_use_fifo  : boolean := true;  -- Rx output provides FIFOs to ensure that dp_distribute does not get blocked due to substantial backpressure on another output
    g_rx_output_fifo_size : natural := c_bram_m9k_fifo_depth;  -- g_rx_output_use_fifo, choose to use full BRAM size = 256 for FIFO depth at output of uth_terminal_rx
    g_rx_output_fifo_fill : natural := 0;
    g_rx_timeout_w        : natural := 0  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive uth_rx snk_in.valid
  );
  port (
    dp_rst                : in  std_logic;
    dp_clk                : in  std_logic;

    -- User
    tx_dp_sosi_2arr       : in  t_unb1_board_back_sosi_2arr;
    tx_dp_siso_2arr       : out t_unb1_board_back_siso_2arr;

    rx_dp_sosi_2arr       : out t_unb1_board_back_sosi_2arr;
    rx_dp_siso_2arr       : in  t_unb1_board_back_siso_2arr;

    -- Phy
    tx_uth_sosi_2arr      : out t_unb1_board_back_sosi_2arr;
    tx_uth_siso_2arr      : in  t_unb1_board_back_siso_2arr;

    rx_uth_sosi_2arr      : in  t_unb1_board_back_sosi_2arr;
    rx_uth_siso_2arr      : out t_unb1_board_back_siso_2arr
  );
end unb1_board_back_uth_terminals_bidir;

architecture str of unb1_board_back_uth_terminals_bidir is
  constant c_tx_mux_mode  : natural := 1;  -- can use 0 for non-blocking tx mux in dp_distribute or use 1 to preserve input order for tx
  constant c_rx_mux_mode  : natural := 0;  -- must use 0 for non-blocking rx mux in dp_distribute (can not use 1 to preserve input order for rx, because some rx frames may go lost)
begin
  gen_bus : for I in 0 to c_unb1_board_tr_back.nof_bus - 1 generate
    u_uth_terminal_bidir : entity uth_lib.uth_terminal_bidir
    generic map (
      -- User
      g_usr_nof_streams     => g_usr_nof_streams,
      g_usr_use_complex     => g_usr_use_complex,
      g_usr_data_w          => g_usr_data_w,
      g_usr_frame_len       => g_usr_frame_len,
      -- DP/UTH packet
      g_packet_data_w       => g_packet_data_w,
      -- Phy
      g_phy_nof_serial      => g_phy_nof_serial,
      -- Tx
      g_use_tx              => g_use_tx,
      g_tx_mux_mode         => c_tx_mux_mode,
      g_tx_input_use_fifo   => g_tx_input_use_fifo,
      g_tx_input_fifo_size  => g_tx_input_fifo_size,
      g_tx_input_fifo_fill  => g_tx_input_fifo_fill,
      -- Rx
      g_use_rx              => g_use_rx,
      g_rx_output_use_fifo  => g_rx_output_use_fifo,
      g_rx_output_fifo_size => g_rx_output_fifo_size,
      g_rx_output_fifo_fill => g_rx_output_fifo_fill,
      g_rx_timeout_w        => g_rx_timeout_w
    )
    port map (
      dp_rst                => dp_rst,
      dp_clk                => dp_clk,

      -- usr side interface
      tx_dp_sosi_arr        => tx_dp_sosi_2arr(I),
      tx_dp_siso_arr        => tx_dp_siso_2arr(I),

      rx_dp_sosi_arr        => rx_dp_sosi_2arr(I),
      rx_dp_siso_arr        => rx_dp_siso_2arr(I),

      -- phy side interface
      tx_uth_sosi_arr       => tx_uth_sosi_2arr(I)(g_phy_nof_serial - 1 downto 0),
      tx_uth_siso_arr       => tx_uth_siso_2arr(I)(g_phy_nof_serial - 1 downto 0),

      rx_uth_sosi_arr       => rx_uth_sosi_2arr(I)(g_phy_nof_serial - 1 downto 0),
      rx_uth_siso_arr       => rx_uth_siso_2arr(I)(g_phy_nof_serial - 1 downto 0),

      -- monitoring interface
      rx_mon_pkt_sosi_arr   => OPEN,
      rx_mon_dist_sosi_arr  => open
    );
  end generate;
end str;
