-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . initial, Daniel van der Schuur
--   Pieter Donker, Mar-2018, filled in verify process.
--   Pieter Donker, Apr-2018, add stop test.
-- Purpose:
-- . Test bench for dp_counter_func
-- Description:
-- . dp_counter_func contains *only* the function
--   . so no flow control (taken care of in dp_counter.vhd wrapper)
--   . so no pipelining   (taken care of in dp_counter.vhd wrapper)
-- . The above means that this TB does not have to cover flow control or
--   pipelining either, which greatly simplifies things.
--Usage:
-- . as 10
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_counter_func is
  generic (
    g_nof_counters : natural := 2;
    -- min range = [0,2,1] => (0,1) 'the python way'
    g_range_start  : t_nat_natural_arr(9 downto 0) := (0, 0, 0, 0, 0, 0, 0, 1, 0, 0);
    g_range_stop   : t_nat_natural_arr(9 downto 0) := (2, 2, 2, 2, 2, 2, 7, 16, 16, 16);
    g_range_step   : t_nat_natural_arr(9 downto 0) := (1, 1, 1, 1, 1, 1, 2, 2, 2, 1)
  );
end tb_dp_counter_func;

architecture tb of tb_dp_counter_func is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_clk_period : time := 5 ns;
  constant c_max_count_w : natural := 32;

  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';

  ------------------------------------------------------------------------------
  -- dp_counter_func
  ------------------------------------------------------------------------------
  signal dp_counter_func_count_en          : std_logic := '0';
  signal dp_counter_func_count_src_out_arr : t_dp_sosi_arr(g_nof_counters - 1 downto 0);

  -- It is difficult / not possible to avoid the glitches in the dp_counter_func outputs, because
  -- dp_counter_func intentionally works combinatorially, so its outputs can vary during a few
  -- delta-cycles, before the finalize. Therefore use a pipeline debug signal to view the outputs
  -- without the combinatorial glitches in the wave window.
  signal dp_counter_func_count_src_out_arr_p : t_dp_sosi_arr(g_nof_counters - 1 downto 0);

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------
  signal tb_eop_sop_en   : std_logic := '1';
  signal tb_step_en      : std_logic := '0';
  signal tb_start_en     : std_logic := '0';
  signal tb_stopped_en   : std_logic := '0';
  signal tb_carryover_en : std_logic := '0';
  signal tb_end          : std_logic := '0';

  type t_count_arr is array(g_nof_counters - 1 downto 0) of natural;
  signal tb_count_arr      : t_count_arr := (others => 0);
  signal tb_last_count_arr : t_count_arr := (others => 0);
  begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  p_stimuli : process
  variable run_clk_cnt: natural := 1;
  begin
    -- wait for reset
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 1);

    report "Start tb dp_counter_func";

    dp_counter_func_count_en <= '1';  -- start counting
    proc_common_gen_pulse(clk, tb_start_en);  -- check start value

    proc_common_wait_some_cycles(clk, 10);  -- wait some time
    dp_counter_func_count_en <= '0';  -- stop counting
    proc_common_wait_some_cycles(clk, 10);  -- wait some time
    proc_common_gen_pulse(clk, tb_stopped_en);  -- check if counting is stopped

    dp_counter_func_count_en <= '1';  -- start counting again
    tb_step_en <= '1';  -- enable step-size check

    if g_nof_counters > 1 then
      -- issue strobe to check counter carry over
      proc_common_wait_until_lo_hi(clk, dp_counter_func_count_src_out_arr(g_nof_counters - 2).eop);  -- wait for carryover
      proc_common_gen_pulse(clk, tb_carryover_en);  -- check if dimension carryover is going right
    end if;

    -- keep running for full range counting of the slowest counter
    proc_common_wait_until_hi_lo(clk, dp_counter_func_count_src_out_arr(g_nof_counters - 1).eop);  -- wait for carryover

    -- run some more intervals for slowest counter, to more clearly view how the slowest counter behaves
    proc_common_wait_until_hi_lo(clk, dp_counter_func_count_src_out_arr(g_nof_counters - 1).eop);  -- wait for carryover
    proc_common_wait_until_hi_lo(clk, dp_counter_func_count_src_out_arr(g_nof_counters - 1).eop);  -- wait for carryover

    report "Stop tb dp_counter_func";
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- dp_counter_func
  ------------------------------------------------------------------------------
  u_dp_counter_func : entity work.dp_counter_func
  generic map (
    g_nof_counters => g_nof_counters,
    g_range_start  => g_range_start,
    g_range_stop   => g_range_stop,
    g_range_step   => g_range_step
  )
  port map (
    rst       => rst,
    clk       => clk,

    count_en  => dp_counter_func_count_en,

    count_src_out_arr => dp_counter_func_count_src_out_arr
  );

  -- Add pipeline to removed combinatorial glitches for viewing in the wave window
  dp_counter_func_count_src_out_arr_p <= dp_counter_func_count_src_out_arr when rising_edge(clk);

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------
  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      -- check if direct after start the counters hold the start value
      if tb_start_en = '1' then
        for I in 0 to g_nof_counters - 1 loop
          assert tb_count_arr(I) = g_range_start(I)
            report "DP : Wrong start of counter " & int_to_str(I)
            severity ERROR;
        end loop;
      end if;

      -- check if dp_counter_func is stopped after dp_counter_func_count_en <= '0'
      if tb_stopped_en = '1' then
        for I in 0 to g_nof_counters - 1 loop
          assert tb_count_arr(I) = TO_UINT(dp_counter_func_count_src_out_arr(I).data(c_max_count_w - 1 downto 0))
            report "DP : Counter " & int_to_str(I) & " not stopped after dp_counter_func_count_en <= 0"
            severity ERROR;
        end loop;
      end if;

      -- check dimension carryover
      if tb_carryover_en = '1' then
        -- all counters except the last one should hold the start value
        for I in 0 to g_nof_counters - 2 loop
          assert tb_count_arr(I) = g_range_start(I)
            report "DP : Wrong carryover, counter:" & int_to_str(I)
            severity ERROR;
        end loop;
        -- the last counter should hold the start value + step
        assert tb_count_arr(g_nof_counters - 1) = g_range_start(g_nof_counters - 1) + g_range_step(g_nof_counters - 1)
          report "DP : Wrong carryover, counter:" & int_to_str(g_nof_counters - 1)
          severity ERROR;
      end if;

      -- check counter values on sop and eop
      if tb_eop_sop_en = '1' then
        for I in 0 to g_nof_counters - 1 loop
          -- on eop counter should hold the stop_value-1
          if dp_counter_func_count_src_out_arr(I).eop = '1' then
            assert tb_count_arr(I) = ((g_range_stop(I) - 1 - g_range_start(I)) / g_range_step(I)) * g_range_step(I) + g_range_start(I)
              report "DP : Wrong stop count on eop, counter:" & int_to_str(I) &
                     " is:" & int_to_str(tb_count_arr(I)) &
                     " expected:" & int_to_str(((g_range_stop(I) - 1 - g_range_start(I)) / g_range_step(I)) * g_range_step(I) + g_range_start(I))
              severity ERROR;
          end if;
          -- on sop counter should hold the start_value
          if dp_counter_func_count_src_out_arr(I).sop = '1' then
            assert tb_count_arr(I) = g_range_start(I)
              report "DP : Wrong start count on sop, counter:" & int_to_str(I) &
                     " is:" & int_to_str(tb_count_arr(I)) &
                     " expected:" & int_to_str(g_range_start(I))
              severity ERROR;
          end if;
        end loop;
      end if;

      -- check step values
      if tb_step_en = '1' then
        for I in 0 to g_nof_counters - 1 loop
          if tb_count_arr(I) > tb_last_count_arr(I) then
            assert (tb_last_count_arr(I) + g_range_step(I)) = tb_count_arr(I)
              report "DP : Wrong step count, counter:" & int_to_str(I) &
                     " is:" & int_to_str(tb_count_arr(I)) &
                     " expected:" & int_to_str(tb_last_count_arr(I) + g_range_step(I))
              severity ERROR;
          elsif tb_count_arr(I) < tb_last_count_arr(I) then
            assert g_range_start(I) = tb_count_arr(I)
              report "DP : Wrong step count, counter:" & int_to_str(I) &
                     " is:" & int_to_str(tb_count_arr(I)) &
                     " expected:" & int_to_str(g_range_start(I)) & ")"
              severity ERROR;
          end if;
        end loop;
      end if;
    end if;
  end process;

  tb_last_count_arr <= tb_count_arr when rising_edge(clk);

  gen_tb_count_arr : for i in 0 to g_nof_counters - 1 generate
    tb_count_arr(I) <= TO_UINT(dp_counter_func_count_src_out_arr(I).data(c_max_count_w - 1 downto 0)) when dp_counter_func_count_en = '1';
  end generate;
end tb;
