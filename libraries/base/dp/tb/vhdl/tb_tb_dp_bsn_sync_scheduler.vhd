-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Author: Eric Kooistra
-- Purpose: Multi test bench for tb_dp_bsn_sync_scheduler.vhd

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

-- > as 4
-- > run -all --> OK

entity tb_tb_dp_bsn_sync_scheduler is
end tb_tb_dp_bsn_sync_scheduler;

architecture tb of tb_tb_dp_bsn_sync_scheduler is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  -- Use g_block_size < c_nof_input_sync, because the fractional sync pattern
  -- will repeat within g_block_size number of output sync intervals. The
  -- repeat period of the sync pattern is visible by nxt_r.accumulate in
  -- dp_bsn_scheduler.vhd.
  constant c_nof_input_sync : natural := 25;
  constant c_pipeline       : natural := 1;
begin
  -- from tb_dp_bsn_scheduler.vhd
  --
  -- -- Input sync period and sosi ctrl
  -- g_nof_input_sync               : NATURAL := 10;
  -- g_nof_block_per_input_sync     : NATURAL := 17;
  -- g_block_size                   : NATURAL := 10;
  -- g_input_gap_size               : NATURAL := 3;
  --
  -- -- Output sync period
  -- g_nof_samples_per_output_sync  : NATURAL := 45;  -- = g_block_size * 9 / 2
  -- g_pipeline                     : NATURAL := 0

  gen_tb : for P in 0 to c_pipeline generate
    u_output_is_input             : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync, 17, 10, 3, 170, P);  -- 170/10 = 17    block/out_sync, = in_sosi
    u_output_is_input_no_gaps     : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync, 17,  5, 0,  85, P);  -- 85/5  = 17    block/out_sync, = in_sosi

    u_sync_interval_0_5x          : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync, 17, 10, 3,  85, P);  -- 85/10 =  8.5  block/out_sync, factor  85/170 = 0.5,
    u_sync_interval_1_5x          : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync, 17, 10, 3, 255, P);  -- 255/10 = 25.5  block/out_sync, factor 255/170 = 1.5,
    u_sync_interval_prime_251     : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync, 17, 10, 3, 251, P);  -- 251/10 = 25.1  block/out_sync, 251 is a prime

    u_short_block_4_3_15          : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync,  4,  3, 3,  15, P);  -- 15/3  =  5    block/out_sync,
    u_short_block_5_3_16          : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync,  5,  3, 3,  16, P);  -- 16/3  =  5.33 block/out_sync,
    u_short_block_6_3_17          : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync,  6,  3, 3,  17, P);  -- 17/3  =  5.66 block/out_sync,

    u_short_block_no_gaps_4_3_15  : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync,  4,  3, 0,  15, P);  -- 15/3  =  5    block/out_sync,
    u_short_block_no_gaps_5_3_16  : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync,  5,  3, 0,  16, P);  -- 16/3  =  5.33 block/out_sync,
    u_short_block_no_gaps_6_3_17  : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync,  6,  3, 0,  17, P);  -- 17/3  =  5.66 block/out_sync,

    u_short_block_size_2          : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync,  7,  2, 3,  16, P);  -- 16/2  =  8    block/out_sync,
    u_short_block_size_2_no_gaps  : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync,  7,  2, 0,  16, P);  -- 16/2  =  8    block/out_sync,

    u_fraction_half               : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync, 17, 10, 3,  45, P);  -- 45/10 =  4.5  block/out_sync
    u_fraction_0                  : entity work.tb_dp_bsn_sync_scheduler generic map (c_nof_input_sync, 17, 10, 3,  50, P);  -- 50/10 =  5    block/out_sync
  end generate;
end tb;
