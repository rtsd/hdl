README.txt for $HDL_WORK/libraries/technology/ip_agi027_1e1v/ddio

Contents:

1) DDIO components
2) Agilex7 IP
3) Synthesis trials
4) Issues


1) DDIO components:

  ip_agi027_1e1v_ddio_in.vhd   = Double Date Rate input
  ip_agi027_1e1v_ddio_out.vhd  = Double Date Rate output
  

2) Agilex7 IP

  In the past, the components altddio_in and altddio_out were used by the StratixIV IP. For more information about this see the
  README.txt from ip_arria10_e2sg/ddio. Quartus convert them using the altera_gpio component for Arria10.
  
  The altera_gpio component is not part of the default Quartus tool libraries, but instead it is created by Qsys together
  with the IP. This makes that the altera_gpio can not easily be used in simulation and synthesis like was possible with altera_mf
  in for Stratix IV (Quartus 11.1). 
  
  The ddio_in component is used by the PPSH and the ddio_out component is used by the ADUH. In both cases the g_width=1.
  The Agilex7 IP can be generated using a fixed width of 1. Therefore the same parameter settings are used (the width was set to 1)
  in the conversion from Quartus v19.4 for arria10_e2sg to Quartus 23.2 for agi027_1e1vs and these IP source files are stored as:
  
    ip_agi027_1e1v_ddio_in_1.ip
    ip_agi027_1e1v_ddio_out_1.ip
  
  If the application would need a wider port then it can achieve this by instantiating the IP multiple times. This approach
  avoids having to generate DDIO IP for every possible width. An alternative would be:
  - to generate IP for e.g. width=16 and assuming that that is sufficient for all applications. Any application that uses less
    width then leaves these IO unconnected so that the unused IO will get optimized away by synthesis.
  - create the IP when it is needed, this scheme is more difficult to manage but is something to consider for the future.

  The IP needs to be generated using:
  
   generate_ip_libs iwave
  
  to create the simulation and synthesis files, because these are initially not kept in the git repository, only the IP source files.
 
 
3) Synthesis trials

  No synthesis trials have taken place for the Agilex 7. When it is preferable to verify that the DDIO IP actually synthesise to the
  appropriate FPGA resources, use the Quartus project quartus/ddio.qsf from the ip_arria10/ram/ folder and follow the steps in the the
  README.txt from the ip_agi027_1e1v/fifo or */ram folder. Use the Quartus GUI to manually select a top level component for synthesis
  e.g. by right clicking the entity vhd file in the file tab of the Quartus project navigator window.
  Then check the resource usage in the synthesis and fitter reports.


4) Issues

a) Simulation model does not work (for Quartus 14.1, not tried for Quartus 15.0 and Quartus 23.2)

  The simulation model for the DDIO does not compile ok because a din port is missing in the ddio_out and a dout port is
  missing in the ddio_in. Adding this ports manualy does work for compile, but when the component is loaded as a simulation
  then Modelsim reports some internal error on the IP. The compile also does not work when using 'do msim_setup.tcl', so
  there is something wrong with the DDIO simulation model. The synthesis of the DDIO IP using ddio.qpf does work.
  
  The work around is not not use the simulation model, but instead use a behavioral simulation model for the IP:
     sim/ip_agi027_1e1v_ddio_in_1.vhd
     sim/ip_agi027_1e1v_ddio_out_1.vhd
     sim/tb_ip_agi027_1e1v_ddio_1.vhd   = self checking tb for ip_agi027_1e1v_ddio_in_1 -> ip_agi027_1e1v_ddio_out_1
  
  The selection between the IP model or the behavioral model is made in the compile_ip.tcl script.
