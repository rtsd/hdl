
Instructions of how to set the fields in the IP catalog for ddr4.qsys

(see the datasheets in the subdir datasheet/micron/* )

All settings are based on the Micron DDR4 module: MTA18ASF1G72HZ-2G1A1ZJ
This is a 8GB SODIMM dual rank 
-2G1 stands for: PC4-2133 with CAS latency CL=15

pagesize is 1KB (page 2)

The IC's on the module: MT40A512M8 (4Gb)
The IC marking is (photo): D9RGQ but should be -093E 


Quartus IP catalog "Arria10 External Memory Interface" fill in:
(the page numbers are of the MT40A512M8 datasheet)


1. Preset selection from library: DDR4-2133P CL15 1CS 4Gb (512Mb x 8)
   This means a single rank DDR4
   The ES of Arria10 does not support dual rank (see Errata document)
   After setting the ddr4.qsys IP the CS1, CKE1 and ODT1 lines to the module should be manually disabled,
   then the module acts as a single rank

2. Tab "General"
   For the Memory clock frequency select 800.0 MHz or 700 MHz instead of 1066.667 MHz because the ES of Arria does not 
   support higher Memory frequencies.
   Set the PLL reference clock frequency to 200 MHz
   PLL reference clock jitter=10.0 ps


3. Tab "I/O"
   Address/Command:
     I/O standard=SSTL-12
     Output mode: 40 ohm without calibration
     Slew rate=fast

   Memory clock:
     I/O standard=SSTL-12
     Output mode: 40 ohm without calibration
     Slew rate=fast

   Data bus
     I/O standard=1.2V POD
     Output mode: 34 ohm with calibration
     Input mode: 60 ohm with calibration
     Starting Vrefin=60.0

   RZQ=240 ohm

4. Tab "Memory Topology"
   Memory format=SODIMM
   DQ width=72

   unselect: Data mask
   unselect: Enable ALERT

   In the section "Mode Register Settings"
   CAS latency: See page 312 table 138 (column 15-15-15)
   for 700MHz set it to 12 (mode register 0)
   Memory write CAS latency also to 9 (mode register 2)

   Output drive strength setting=RZQ/7 (mode register 1)
   ODT Rtt nominal value=RZQ/5 (mode register 1)

   Dynamic ODT (Rtt_WR)= OFF (mode register 2)

   ODT Activation Settings to Default


5. Tab "Memory Timing"
   Speed bin=-2133 

   (timings are recalculated for 800MHz Memory clock)

   tIS (base) = 80 ps          (table 143, page 321)
   tIS (base) AC level = 100mV (table 80, page 254)
   tIH (base) = 105 ps         (table 143, page 321)
   tIH (base) DC level = 75mV  (table 800, page 254)
   tdIVW_dj = 0.1 UI   (table 82, page 257) Max=0.2UI but 0.1UI is default
   VdiVW_total = 136mV (table 82, page 257)
 * tDQSQ = 114ps (page 319) (0.16*UId. UI=tCK/2.  tCK=1/700e6)
 * tQH = 0.38    (page 319) (0.76*UId)/tCK 
   tDQSCK = 170  (page 320) (within -180 and 180)
   tDQSS = 0.27  (page 320)
   tQSH = 0.38   (page 320)
   tDSH = 0.18   (page 320)
   tDSS = 0.18   (page 320)
 * tWLS = 185.7  (page 328) 0.13*CK (CK=1/700e6)
 * tWLH = 185.7  (page 328) 0.13*CK (CK=1/700e6)
   tINIT = 500us (page 29)
   tMRD = 8      (page 323)
   tRAS = 33     (page 312)
   tRCD = 14.06  (page 312)
   tRP  = 14.06  (page 312)
   tWR  = 15     (page 322)

 * tRRD_S = 3     (page 176,321) should be 3.7ns. Using 3*CK=3.75ns (CK=1/700e6)
 * tRRD_L = 4     (page 176,321) should be 5.3ns. Using 4*CK=5ns
 * tFAW   = 21 ns (page 322)
   tCCD_S = 4     (page 176,321)
 * tCCD_L = 4     (page 176,321) should be 5.355ns. Using 4*CK=5ns
 * tWTR_S = 2     (page 177,321) should be 2.5ns. Using 2*CK=2.5ns
 * tWTR_L = 6     (page 177,321) should be 7.5ns. Using 6*CK=7.5ns

   tRFC = 260 ns  (see page 325) 
   tREFI = 7.8 us (see page 325)

   (* marked lines have to be recalculated when using different Memory clock frequency)

6. Tab "Board Timing"
   Slew Rates (numbers from simulation):
     CK              2.43
     A4              1.16
     DQS0 FPGA->DIMM 3.7
     DQ0  FPGA->DIMM 2.16
     DQS0 DIMM->FPGA 3.7
     DQ0  DIMM->FPGA 2.2

   Crosstalk: use default values

   Board/Package skews
     2x radiobuttons "Package deskewed..."        = OFF
     Maximum board skew within DQS group          = 0.03 ns
     Maximum board skew within addr/command group = 0.146 ns
   
     Avg delay difference between DQS and CK      = -0.21 ns
     Max delay difference between DIMMs           = 0.0 ns
     Max skew between DQS groups                  = 0.133 ns
     Avg delay difference between addr/cmd and CK = 0.013 ns

     Max CK delay to DIMM  = 0.252 ns
     Max DQS delay to DIMM = 0.323 ns

10. compile steps

  click save ddr4.qsys
  click "generate HDL"
  click "generate Example Design"
  close IP catalog GUI

  run:
  cd emif_0_example_design
  . ${RADIOHDL_GEAR}/quartus/set_quartus unb2 && quartus_sh -t make_qii_design.tcl

  add to qii/ed_synth.qsf:
  source ../../unb2_pins_ed_synth.tcl

  edit qii/synth/ed_synth.v:
  add to module ed_synth the wires:

    output  wire [8:0]  emif_0_example_design_mem_mem_dbi_n,
    output wire cs1_export,
    output wire cke1_export,
    output wire odt1_export,
    input  wire testio1

  add below local wires:

    assign cs1_export = testio1;
    assign cke1_export = ~testio1;
    assign odt1_export = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[0] = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[1] = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[2] = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[3] = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[4] = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[5] = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[6] = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[7] = ~testio1;
    assign emif_0_example_design_mem_mem_dbi_n[8] = ~testio1;

  (assuming testio1 is pulled up to VCC)





