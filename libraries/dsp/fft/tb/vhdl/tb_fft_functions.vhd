-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for functions in fft_pkg.vhd
--
-- Description:
--   Use signals to observe the result of functions in the Wave Window.
--   Manually verify that the functions yield the expected result.
-- Usage:
--   > as 3
--   > run 1 us
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.fft_pkg.all;
use work.tb_fft_pkg.all;

entity tb_fft_functions is
end tb_fft_functions;

architecture tb of tb_fft_functions is
  constant c_wb_factor     : natural := 4;
  constant c_nof_points    : natural := 32;
  constant c_w             : natural := ceil_log2(c_nof_points);

  -- index_arr                          =  0  1  2  3   4  5  6  7   8  9 10 11  12 13 14 15  16 17 18 19  20 21 22 23  24 25 26 27  28 29 30 31
  -- index_transpose_P_N_arr            =  0  8 16 24   1  9 17 25   2 10 18 26   3 11 19 27   4 12 20 28   5 13 21 29   6 14 22 30   7 15 23 31
  -- index_transpose_N_P_arr            =  0  4  8 12  16 20 24 28   1  5  9 13  17 21 25 29   2  6 10 14  18 22 26 30   3  7 11 15  19 23 27 31
  -- bin_complex_arr                    = 16  0 24  8  20  4 28 12  18  2 26 10  22  6 30 14  17  1 25  9  21  5 29 13  19  3 27 11  23  7 31 15
  -- bin_complex_flip_arr               = 16 17 18 19  20 21 22 23  24 25 26 27  28 29 30 31   0  1  2  3   4  5  6  7   8  9 10 11  12 13 14 15
  -- bin_complex_flip_transpose_arr     =  4 12 20 28   5 13 21 29   6 14 22 30   7 15 23 31   0  8 16 24   1  9 17 25   2 10 18 26   3 11 19 27
  -- bin_complex_reorder_arr            =  0  1  2  3   4  5  6  7   8  9 10 11  12 13 14 15  16 17 18 19  20 21 22 23  24 25 26 27  28 29 30 31
  -- bin_complex_reorder_transpose_arr  =  0  8 16 24   1  9 17 25   2 10 18 26   3 11 19 27   4 12 20 28   5 13 21 29   6 14 22 30   7 15 23 31
  -- bin_two_real_reorder_arr           =  0  1  2  3   0  1  2  3   4  5  6  7   4  5  6  7   8  9 10 11   8  9 10 11  12 13 14 15  12 13 14 15
  -- bin_two_real_reorder_transpose_arr =  0  4  8 12   0  4  8 12   1  5  9 13   1  5  9 13   2  6 10 14   2  6 10 14   3  7 11 15   3  7 11 15
  signal index_arr                          : t_natural_arr(0 to c_nof_points - 1) := array_init(0, c_nof_points, 1);
  signal index_transpose_P_N_arr            : t_natural_arr(0 to c_nof_points - 1);
  signal index_transpose_N_P_arr            : t_natural_arr(0 to c_nof_points - 1);
  signal index_transpose_N_P_flip_arr       : t_natural_arr(0 to c_nof_points - 1);
  signal index_flip_transpose_N_P_arr       : t_natural_arr(0 to c_nof_points - 1);
  signal index_flip_arr                     : t_natural_arr(0 to c_nof_points - 1);
  signal index_flip_shift_arr               : t_natural_arr(0 to c_nof_points - 1);
  signal index_shift_flip_arr               : t_natural_arr(0 to c_nof_points - 1);
  signal bin_complex_arr                    : t_natural_arr(0 to c_nof_points - 1);
  signal bin_complex_flip_arr               : t_natural_arr(0 to c_nof_points - 1);  -- flip()
  signal bin_complex_flip_transpose_arr     : t_natural_arr(0 to c_nof_points - 1);
  signal bin_complex_reorder_arr            : t_natural_arr(0 to c_nof_points - 1);  -- fft_shift(flip())
  signal bin_complex_reorder_transpose_arr  : t_natural_arr(0 to c_nof_points - 1);
  signal bin_two_real_reorder_arr           : t_natural_arr(0 to c_nof_points - 1);  -- separate(flip())
  signal bin_two_real_reorder_transpose_arr : t_natural_arr(0 to c_nof_points - 1);
begin
  p_bin : process
  begin
    for I in 0 to c_nof_points - 1 loop
      index_flip_arr(I)                    <= flip(I, c_w);
      index_transpose_P_N_arr(I)           <= transpose(I, c_wb_factor, c_nof_points / c_wb_factor);
      index_transpose_N_P_arr(I)           <= transpose(I, c_nof_points / c_wb_factor, c_wb_factor);
      index_transpose_N_P_flip_arr(I)      <= transpose(flip(I, c_w), c_nof_points / c_wb_factor, c_wb_factor);
      index_flip_transpose_N_P_arr(I)      <= flip(transpose(I, c_nof_points / c_wb_factor, c_wb_factor), c_w);
      index_flip_shift_arr(I)              <= flip(fft_shift(I, c_w), c_w);
      index_shift_flip_arr(I)              <= fft_shift(flip(I, c_w), c_w);
      --                                                                                                use_       use_      use_
      --                                                                                                index_flip fft_shift separate
      bin_complex_arr(I)                    <= fft_index_to_bin_frequency(c_wb_factor, c_nof_points, I, false,     false,    false);
      bin_complex_flip_arr(I)               <= fft_index_to_bin_frequency(c_wb_factor, c_nof_points, I, true,      false,    false);
      bin_complex_flip_transpose_arr(I)     <= fft_index_to_bin_frequency(c_wb_factor, c_nof_points, I, true,      false,    false);
      bin_complex_reorder_arr(I)            <= fft_index_to_bin_frequency(c_wb_factor, c_nof_points, I, true,      true,     false);
      bin_complex_reorder_transpose_arr(I)  <= fft_index_to_bin_frequency(c_wb_factor, c_nof_points, I, true,      true,     false);
      bin_two_real_reorder_arr(I)           <= fft_index_to_bin_frequency(c_wb_factor, c_nof_points, I, true,      false,    true);
      bin_two_real_reorder_transpose_arr(I) <= fft_index_to_bin_frequency(c_wb_factor, c_nof_points, I, true,      false,    true);
    end loop;
    wait;
  end process;
end tb;
