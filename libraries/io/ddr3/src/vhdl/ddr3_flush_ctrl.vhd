--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------
-- Purpose: Provide control over the flusher that is connected to the wr_fifo in the DDR3 controller
--
-- Description: Activating the flushing can be established in two ways:
--
--                   1. When the DDR3 driver is not ready to receive commands: dvr_done = '0'
--                   2. Using the external flush_ena input.
--
--              When g_ext_ena is FALSE, option 1 is selected.
--              When g_ext_ena is TRUE, option 2 is selected.
--              Applying a pulse on the flush_ena input is enough to activate the flushmode.
--              Once in flush mode the specified deactivation trigger has to occur before
--              flushing is disabled again.
--
--              There are several ways to deactivate the flusing (deactivation-trigger)
--
--              1. Receiving valid data                                (g_sop = FALSE, g_sop_sync = FALSE, g_sop_channel = FALSE)
--              2. Receiving a SOP                                     (g_sop = TRUE,  g_sop_sync = FALSE, g_sop_channel = FALSE)
--              3. Receiving a SOP and a SYNC                          (g_sop = TRUE,  g_sop_sync = TRUE , g_sop_channel = FALSE)
--              4. Receiving a SOP and a specified channel or channels (g_sop = TRUE,  g_sop_sync = FALSE, g_sop_channel = TRUE)
--
--
-- Remarks:
--

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.ddr3_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity ddr3_flush_ctrl is
  generic (
    g_ext_ena           : boolean := false;  -- TRUE: Enables the external flesh_ena signaland discards the dvr_done signal
    g_sop               : boolean := false;  -- Stop flushing on SOP, otherwise stop flushing on valid data
    g_sop_sync          : boolean := false;  -- Stop flushing when SOP and SYNC are received, otherwise flushing is stopped on valid data
    g_sop_channel       : boolean := false;  -- When g_sop=TRUE, also check if the channel matches g_sop_start_channel
    g_sop_start_channel : natural := 0;
    g_nof_channels      : natural := 0
   );
  port (
    clk                : in  std_logic;
    rst                : in  std_logic;

    dvr_en             : in  std_logic := '1';
    dvr_wr_not_rd      : in  std_logic;
    dvr_done           : in  std_logic;

    flush_ena          : in  std_logic;

    wr_sosi            : in  t_dp_sosi;

    dvr_flush          : out std_logic

   );
end ddr3_flush_ctrl;

architecture str of ddr3_flush_ctrl is
  constant c_nof_channels  : natural := sel_a_b(g_nof_channels = 0, 1, g_nof_channels);
  constant c_channel_w     : natural := ceil_log2(c_nof_channels);

  type t_state is (s_idle, s_flush, s_stop);

  signal state     : t_state;
  signal nxt_state : t_state;

  signal flush_dis : std_logic;
begin
  -- Flush ddr3 module's FIFO (keep sinking the stream but simply discard the
  -- data) after reset to prevent ddr3 write fifo from filling up - which would
  -- cause problems downstream (dp_mux uses two fifos that cannot be
  -- allowed to fill up too much).
  -- Also flush the ddr3 module's FIFO when it is reading.

  gen_sop : if g_sop = true generate  -- Disable flushing on arrival of SOP
--    gen_sop_only: IF g_sop_channel = FALSE GENERATE
--      flush_dis <= '1' WHEN wr_sosi.sop='1' ELSE '0';
--    END GENERATE;
--    gen_channel : IF g_sop_channel = TRUE GENERATE -- Only disable flushing on arrival of specific channel SOP
--      flush_dis <= '1' WHEN wr_sosi.sop='1' AND UNSIGNED(wr_sosi.channel(c_channel_w-1 DOWNTO 0))=g_sop_start_channel ELSE '0';
--    END GENERATE;
    gen_sync    : if g_sop_sync = true generate  -- Only disable flushing on arrival of SOP that is accompanied with a SYNC
      flush_dis <= '1' when wr_sosi.sop = '1' and wr_sosi.sync = '1' else '0';
    end generate;
  end generate;

  gen_val : if g_sop = false generate  -- Disable flushing on arrival of 1st valid data
    flush_dis <= wr_sosi.valid;
  end generate;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      state <= s_flush;
    elsif rising_edge(clk) then
      state <= nxt_state;
    end if;
  end process;

  p_state : process(state, dvr_done, flush_ena, dvr_en, dvr_wr_not_rd, flush_dis)
  begin
    nxt_state <= state;
    dvr_flush <= '0';
    case state is
      when s_idle =>
        if dvr_done = '1' and g_ext_ena = false then
          dvr_flush <= '1';
          nxt_state <= s_flush;
        end if;
        if flush_ena = '1' then
          dvr_flush <= '1';
          nxt_state <= s_stop;
        end if;
      when s_flush =>
        dvr_flush <= '1';
        if dvr_en = '1' and dvr_wr_not_rd = '1' then
          nxt_state <= s_stop;
        elsif g_ext_ena = true then
          nxt_state <= s_idle;
        end if;
      when others =>  -- s_stop
        dvr_flush <= '1';
        if flush_dis = '1' then
          nxt_state <= s_idle;
        end if;
    end case;
  end process;
end str;
