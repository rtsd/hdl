--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Pieter Donker
-- Purpose:
-- .
-- Description:
-- .

library IEEE, common_lib, dp_lib, technology_lib, diag_lib, mm_lib;
use IEEE.std_logic_1164.all;

entity tb_tb_beamformer is
end tb_tb_beamformer;

architecture tb of tb_tb_beamformer is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
-- Usage
--   > as 8
--   > run -all
--   > Testbenches are self-checking

--
--  g_tb_index       : NATURAL := 0;      -- use different index to avoid MM file conflict in multi tb
--  --g_technology   : NATURAL := c_tech_select_default;
--  g_nof_inputs     : NATURAL := 2;
--  g_nof_weights    : NATURAL := 32;
--  g_data_w         : NATURAL := 8;   --8b complex input data
--  g_use_weight_ram : BOOLEAN := FALSE;
--  g_use_weight_reg : BOOLEAN := TRUE
--

-- do test for different number of inputs
-- . (weights in RAM)
sim_i01_ram_beamformer : entity work.tb_beamformer generic map (1,  1, 32, 8, true, false);
sim_i02_ram_beamformer : entity work.tb_beamformer generic map (2,  2, 32, 8, true, false);
sim_i32_ram_beamformer : entity work.tb_beamformer generic map (3, 32, 32, 8, true, false);
-- . (weights in Registers)
sim_i01_reg_beamformer : entity work.tb_beamformer generic map (4,  1, 32, 8, false, true);
sim_i02_reg_beamformer : entity work.tb_beamformer generic map (5,  2, 32, 8, false, true);
sim_i32_reg_beamformer : entity work.tb_beamformer generic map (6, 32, 32, 8, false, true);
end tb;
