--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Generate a stream of frames with test sequence data.
-- Description:
--   Each frame has g_frame_len words of out_dat. The test data can be PRSG or
--   COUNTER dependent on diag_sel, or constant data when diag_dc='1'.
--   The frame generator is enabled by diag_en. The actual frame rate depends
--   on the g_sop_period and on out_ready.
--   The out_ready acts as a data request. During a frame the out_dat is valid
--   for every active out_ready, to support streaming flow control.
-- Remark:
-- . This component can be easily mapped to standard MM and ST interfaces.

entity diag_frm_generator is
  generic (
    g_sel         : std_logic := '1';  -- '0' = PRSG, '1' = COUNTER
    g_frame_len   : natural := 2;  -- >= 2, test frame length in nof dat words
    g_sof_period  : natural := 1;  -- >= 1, nof cycles between sop that start a frame, typically >> g_frame_len
                                       --       to generate frames with idle in between
    g_frame_cnt_w : natural := 32;
    g_dat_w       : natural := 16;  -- >= 1, test data width
    g_symbol_w    : natural := 16;  -- >= 1, and nof_symbols_per_dat = g_dat_w/g_symbol_w, must be an integer
    g_empty       : natural := 0  -- >= 0 and < nof symbols per dat,
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;
    clken            : in  std_logic := '1';

    -- Static control input (connect via MM or leave open to use default)
    diag_en          : in  std_logic;
    diag_sel         : in  std_logic := g_sel;  -- '0' = PRSG sequence data, '1' = COUNTER sequence data
    diag_dc          : in  std_logic := '0';  -- '0' = output diag_sel sequence data, '1' = output constant diag_init data
    diag_frame_len   : in  std_logic_vector(ceil_log2(g_frame_len) - 1 downto 0) := TO_UVEC(g_frame_len, ceil_log2(g_frame_len));
    diag_frame_empty : in  std_logic_vector(ceil_log2(g_dat_w / g_symbol_w) - 1 downto 0) := TO_UVEC(g_empty, ceil_log2(g_dat_w / g_symbol_w));
    diag_sof_period  : in  std_logic_vector(ceil_log2(g_sof_period) - 1 downto 0) := TO_UVEC(g_sof_period, ceil_log2(g_sof_period));
    diag_frame_cnt   : out std_logic_vector(g_frame_cnt_w - 1 downto 0);

    -- ST output
    out_ready       : in  std_logic := '1';  -- '1' = request ST test data output, '0' = halt ST test data output
    out_dat         : out std_logic_vector(g_dat_w - 1 downto 0);  -- test sequence data
    out_val         : out std_logic;  -- '1' when out_data is valid
    out_sop         : out std_logic;  -- '1' for first valid frame data
    out_eop         : out std_logic;  -- '1' for last  valid frame data
    out_empty       : out std_logic_vector(ceil_log2(g_dat_w / g_symbol_w) - 1 downto 0)  -- nof empty symbols in last out_dat word marked by out_eop
  );
end diag_frm_generator;

architecture str of diag_frm_generator is
  constant c_init           : natural := 0;  -- first data word of first frame that is generated after diag_en = '1'
  constant c_frame_len_w    : natural := ceil_log2(g_frame_len);

  signal diag_en_revt       : std_logic;
  signal diag_init          : std_logic_vector(g_dat_w - 1 downto 0) := TO_UVEC(c_init, g_dat_w);  -- init data for out_dat when diag_sop = '1'
  signal nxt_diag_init      : std_logic_vector(diag_init'range);
  signal diag_sop           : std_logic;
  signal diag_ready         : std_logic;

  signal frame_cnt          : std_logic_vector(diag_frame_cnt'range);
  signal nxt_diag_frame_cnt : std_logic_vector(diag_frame_cnt'range);

  signal i_out_sop          : std_logic;
  signal i_out_eop          : std_logic;
begin
  out_sop   <= i_out_sop;
  out_eop   <= i_out_eop;
  out_empty <= diag_frame_empty;

  -- Signal begin of diag_en
  u_diag_en_revt : entity common_lib.common_evt
  generic map (
    g_evt_type => "RISING",
    g_out_reg  => false
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    in_sig  => diag_en,
    out_evt => diag_en_revt
  );

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      diag_init      <= TO_UVEC(c_init, g_dat_w);
      diag_frame_cnt <= (others => '0');
    elsif rising_edge(clk) then
      if clken = '1' then
        diag_init      <= nxt_diag_init;
        diag_frame_cnt <= nxt_diag_frame_cnt;
      end if;
    end if;
  end process;

  nxt_diag_init <= TO_UVEC(c_init, g_dat_w) when diag_en = '0' else INCR_UVEC(diag_init, 1) when i_out_sop = '1';

  u_pulse_sop : entity common_lib.common_pulser
  generic map (
    g_pulse_period => g_sof_period
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => clken,
    pulse_period   => diag_sof_period,
    pulse_en       => diag_en,
    pulse_clr      => diag_en_revt,
    pulse_out      => diag_sop
  );

  -- Hold last frame count also when the generator is disabled, clear when it is restarted
  nxt_diag_frame_cnt <= (others => '0') when diag_en_revt = '1' else frame_cnt;

  u_frm_cnt : entity common_lib.common_counter
  generic map (
    g_width     => g_frame_cnt_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    cnt_clr => diag_en_revt,
    cnt_en  => i_out_eop,
    count   => frame_cnt
  );

  u_tx_frm : entity work.diag_tx_frm
  generic map (
    g_sel       => g_sel,
    g_init      => c_init,
    g_frame_len => g_frame_len,
    g_dat_w     => g_dat_w
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => clken,

    -- Static control input (connect via MM or leave open to use default)
    diag_sel       => diag_sel,
    diag_dc        => diag_dc,
    diag_frame_len => diag_frame_len,

    -- Dynamic control input (connect via MM or via ST input or leave open to use defaults)
    diag_ready     => diag_ready,
    diag_init      => diag_init,
    diag_sop       => diag_sop,

    -- ST output
    out_ready      => out_ready,
    out_dat        => out_dat,
    out_val        => out_val,
    out_sop        => i_out_sop,
    out_eop        => i_out_eop
  );
end str;
