#!/usr/bin/env bash -e
###############################################################################
#
# Copyright (C) 2018 
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

#
# Initialisation script to setup the environment variables for this branch
#

# 
# Make sure it is sourced and no one accidentally gave the script execution rights and just executes it.
if [[ "$_" == "${0}" ]]; then
    echo "ERROR: Use this command with '. ' or 'source '"
    sleep 1
    return
fi

# 
if [ -z "${ALTERA_DIR}" ]; then
    echo "== environ variable 'ALTERA_DIR' not set. =="
    echo "should be in your .bashrc file."
    echo "if it is your .bashrc file but not active run bash in your terminal"
    return
fi

# Figure out where this script is located and set environment variables accordingly
export HDL_WORK="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
echo "HDL environment will be setup for" $HDL_WORK

# setup paths to build and config dir if not already defined by the user.
export ARGS_WORK=${HDL_WORK}
export HDL_BUILD_DIR=${HDL_WORK}/build
if [[ ! -d "${HDL_BUILD_DIR}" ]]; then
    echo "make buil dir"
    echo "${HDL_BUILD_DIR}"
    mkdir "${HDL_BUILD_DIR}"
fi

# set hdl_build_quartus dir
export HDL_BUILD_QUARTUS_DIR=${HDL_BUILD_DIR}/quartus
if [[ ! -d "${HDL_BUILD_QUARTUS_DIR}" ]]; then
    echo "make hdl/build/quartus dir"
    echo "${HDL_BUILD_QUARTUS_DIR}"
    mkdir "${HDL_BUILD_QUARTUS_DIR}"
    echo "Do not remove this dir, it is used by modelsim_altera_libs symlink" > ${HDL_BUILD_QUARTUS_DIR}/readme.txt
fi

export MODELSIM_ALTERA_LIBS_DIR=${HDL_BUILD_DIR}/modelsim_altera_libs
if [[ -L "${MODELSIM_ALTERA_LIBS_DIR}" ]] && [[ -e "${MODELSIM_ALTERA_LIBS_DIR}" ]]; then
    echo "${MODELSIM_ALTERA_LIBS_DIR} is a valid symlink"
else
    echo "make a symlink for modelsim_altera_libs dir"
    echo "${MODELSIM_ALTERA_LIBS_DIR}"
    ln -s "${HDL_BUILD_QUARTUS_DIR}" "${MODELSIM_ALTERA_LIBS_DIR}"
fi

# modelsim uses this sim dir for testing
export HDL_IOFILE_SIM_DIR=${HDL_BUILD_DIR}/sim
if [[ ! -d "${HDL_IOFILE_SIM_DIR}" ]]; then
    echo "make sim dir"
    echo "${HDL_IOFILE_SIM_DIR}"
    mkdir "${HDL_IOFILE_SIM_DIR}"
fi
# if sim dir not empty, remove all files and dirs
if [ ! -z "$(ls -A ${HDL_IOFILE_SIM_DIR})" ]; then
    echo "clear sim dir"
    rm -r ${HDL_IOFILE_SIM_DIR}/*
fi

# copy git user_componets.ipx into Altera dir's
for altera_dir in ${ALTERA_DIR}/*; do
    if [[ -d "${altera_dir}" ]] &&  [[ ! -h "${altera_dir}" ]]; then
        echo "copy git hdl_user_components.ipx to ${altera_dir}/ip/altera/user_components.ipx"
        cp ${HDL_WORK}/hdl_user_components.ipx $altera_dir/ip/altera/user_components.ipx
    fi
done

# source also radiohdl, args and vhdl_style tools
. ${GIT}/radiohdl/init_radiohdl.sh
if [[ -d "${GIT}/args" ]]; then
    . ${GIT}/args/init_args.sh
fi
if [[ -d "${GIT}/vhdlstyle2_0" ]]; then
    . ${GIT}/vhdlstyle2_0/init_vhdl_style.sh
fi
