###############################################################################
#
# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

set_location_assignment PIN_AC31 -to sens_sc
set_location_assignment PIN_AC32 -to sens_sd

set_instance_assignment -name IO_STANDARD "2.5 V" -to sens_sc
set_instance_assignment -name IO_STANDARD "2.5 V" -to sens_sd


# Default: drive = 12 mA and fast SR = 3 --> I2C sometimes starts to fail for
# BN3. The pull up is 4.7 kOhm @ 3V so > 1.5 mA should be enough. BN3 has
# multiple slaves so ringing due to too fast SCL edges could cause false clock
# events.
# Try different settings via ECO in ChipPlanner:
# - SCL  4 mA, 0 and SDA  4 mA, 0 --> X (255)
# - SCL 12 mA, 0 and SDA  4 mA, 0 --> X (255)
# - SCL 12 mA, 0 and SDA 12 mA, 0 --> OK
# - SCL  4 mA, 0 and SDA 12 mA, 0 --> OK
# - SCL  4 mA, 3 and SDA  4 mA, 3 --> X (255)

# set_instance_assignment -name CURRENT_STRENGTH_NEW 12MA -to sens_sc
# set_instance_assignment -name CURRENT_STRENGTH_NEW 12MA -to sens_sd
# set_instance_assignment -name SLEW_RATE 0 -to sens_sc
# set_instance_assignment -name SLEW_RATE 0 -to sens_sd
