-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

-- Purpose: Parallel operation tree.
-- Description:
-- . Perform the operation on g_nof_inputs from an input vector in_data_vec.
-- Remarks:
-- . The implementation follows the same scheme as common_adder_tree_a_str
-- . Instead of using common_pipeline in case g_nof_inputs is not a power of 2
--   it would have been easier to internally extend the input in_data_vec to a
--   power of 2 using default input values for the unused inputs or by
--   setting in_en_vec fixed to '0' for them. Synthesis will then optimise them
--   away.
-- . More operations can be added e.g.:
--   - Operation "ADD" to provide same as common_adder_tree.vhd, but with more
--     control over the stage pipelining thanks to g_pipeline_mod
--   - Operation "SEL" with in_sel input to provide same as
--     common_select_symbol.vhd but with pipelining per stage instead of only
--     at the output

entity common_operation_tree is
  generic (
    g_operation      : string   := "MAX";  -- supported operations "MAX", "MIN"
    g_representation : string   := "SIGNED";
    g_pipeline       : natural  := 0;  -- amount of output pipelining per stage
    g_pipeline_mod   : positive := 1;  -- only pipeline the stage output by g_pipeline when the stage number MOD g_pipeline_mod = 0
    g_nof_inputs     : natural  := 4;  -- >= 1, nof stages = ceil_log2(g_nof_inputs)
    g_dat_w          : natural  := 8
  );
  port (
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    in_data_vec : in  std_logic_vector(g_nof_inputs * g_dat_w - 1 downto 0);
    in_en_vec   : in  std_logic_vector(g_nof_inputs        - 1 downto 0);
    result      : out std_logic_vector(             g_dat_w - 1 downto 0)
  );
end common_operation_tree;

architecture str of common_operation_tree is
  -- operation pipelining
  constant c_pipeline_in  : natural := 0;
  constant c_pipeline_out : natural := g_pipeline;

  constant c_w            : natural := g_dat_w;  -- input data width

  constant c_N            : natural := g_nof_inputs;  -- nof inputs to the adder tree
  constant c_nof_stages   : natural := ceil_log2(c_N);  -- nof stages in the adder tree

  type t_stage_arr    is array (integer range <>) of std_logic_vector(c_N * g_dat_w - 1 downto 0);
  type t_stage_en_arr is array (integer range <>) of std_logic_vector(c_N        - 1 downto 0);

  signal stage_arr    : t_stage_arr(-1 to c_nof_stages - 1) := (others => (others => '0'));
  signal stage_en_arr : t_stage_en_arr(-1 to c_nof_stages - 1) := (others => (others => '1'));
begin
  gen_tree : if g_nof_inputs > 1 generate
    -- Input wires
    stage_arr(-1)    <= in_data_vec;
    stage_en_arr(-1) <= in_en_vec;  -- the first stage enables depend on in_en_vec, the other stages are determined in the stages

    -- Adder tree
    gen_stage : for j in 0 to c_nof_stages - 1 generate
      gen_oper : for i in 0 to (c_N + (2**j) - 1) / (2**(j + 1)) - 1 generate
        u_operj : entity work.common_operation
        generic map (
          g_operation       => g_operation,
          g_representation  => g_representation,
          g_pipeline_input  => c_pipeline_in,
          g_pipeline_output => sel_a_b((j + 1) mod g_pipeline_mod = 0, c_pipeline_out, 0),
          g_dat_w           => c_w
        )
        port map (
          clk     => clk,
          clken   => clken,
          in_a    => stage_arr(j - 1)((2 * i + 1) * c_w - 1 downto (2 * i + 0) * c_w),
          in_b    => stage_arr(j - 1)((2 * i + 2) * c_w - 1 downto (2 * i + 1) * c_w),
          in_en_a => sl(stage_en_arr(j - 1)(2 * i   downto 2 * i  )),
          in_en_b => sl(stage_en_arr(j - 1)(2 * i + 1 downto 2 * i + 1)),
          result  => stage_arr(j)((i + 1) * c_w - 1 downto i * c_w)
        );

        -- In case two adjacent inputs are disbaled, the result of their operation should be disabled in the next stage as well.
        -- Therfor a logic OR creates the stage_en vector for the next stage.
        stage_en_arr(j)(i) <= stage_en_arr(j - 1)(2 * i) or stage_en_arr(j - 1)(2 * i + 1);
      end generate;

      gen_pipe : if ((c_N + (2**j) - 1) / (2**j)) mod 2 /= 0 generate
        -- In case of an odd number of inputs the enable of the last input should ripple through
        -- to the place where the data is connected to a common_operation block.
        stage_en_arr(j)(c_N / (2**(j + 1))) <= in_en_vec(g_nof_inputs - 1);

        u_pipej : entity work.common_pipeline
        generic map (
          g_representation => g_representation,
          g_pipeline       => sel_a_b((j + 1) mod g_pipeline_mod = 0, c_pipeline_out, 0),
          g_in_dat_w       => c_w,
          g_out_dat_w      => c_w
        )
        port map (
          clk     => clk,
          clken   => clken,
          in_dat  => stage_arr(j - 1)((2 * ((c_N + (2**j) - 1) / (2**(j + 1))) + 1) * c_w - 1 downto
                                    (2 * ((c_N + (2**j) - 1) / (2**(j + 1))) + 0) * c_w),
          out_dat => stage_arr(j)(((c_N + (2**j) - 1) / (2**(j + 1)) + 1) * c_w - 1 downto
                                  ((c_N + (2**j) - 1) / (2**(j + 1))  ) * c_w)
        );
      end generate;
    end generate;

    result <= stage_arr(c_nof_stages - 1)(c_w - 1 downto 0);
  end generate;  -- gen_tree

  no_tree : if g_nof_inputs = 1 generate
    u_reg : entity work.common_pipeline
    generic map (
      g_representation => g_representation,
      g_pipeline       => g_pipeline,
      g_in_dat_w       => g_dat_w,
      g_out_dat_w      => g_dat_w
    )
    port map (
      clk     => clk,
      clken   => clken,
      in_dat  => in_data_vec,
      out_dat => result
    );
  end generate;  -- no_tree
end str;
