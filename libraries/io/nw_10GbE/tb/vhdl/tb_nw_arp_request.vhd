-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Verify nw_arp_request.vhd
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_nw_arp_request is
generic (
  g_test_backpressure : boolean := true
        );
end tb_nw_arp_request;

architecture tb of tb_nw_arp_request is
  -- Use c_tb_timeout to raise ERROR if there is no result
  constant c_tb_timeout : natural := 100;

  signal tb_end             : std_logic := '0';
  signal clk                : std_logic := '1';
  signal dp_pps             : std_logic := '1';
  signal rst                : std_logic := '1';

  signal src_in  : t_dp_siso := c_dp_siso_rst;
  signal src_out : t_dp_sosi := c_dp_sosi_rst;

  signal arp_sha : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0) := x"AABBCCDDEEFF";
  signal arp_spa : std_logic_vector(c_network_ip_addr_w - 1 downto 0) := x"A1A2A3A4";
  signal arp_tpa : std_logic_vector(c_network_ip_addr_w - 1 downto 0) := x"B1B2B3B4";
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;
  src_in.xon <= '1';

  p_stimuli : process
  begin
    if g_test_backpressure then
      src_in.ready <= '0';
    else
      src_in.ready <= '1';
    end if;
    dp_pps <= '0';
    proc_common_wait_some_cycles(clk, 10);
    dp_pps <= '1';
    proc_common_wait_some_cycles(clk, 1);
    dp_pps <= '0';
    proc_common_wait_some_cycles(clk, 10);
    src_in.ready <= '1';
    proc_common_wait_until_evt(c_tb_timeout, clk, src_out.eop);
    tb_end <= '1';
    -- End with WAIT to avoid Warning: (vcom-1090) Possible infinite loop:
    -- Process contains no WAIT statement.
    wait;
  end process;

  dut: entity work.nw_arp_request
  generic map(
    g_period_s  =>  1
  )
  port map(
    dp_pps  => dp_pps,
    dp_clk  => clk,
    dp_rst  => rst,

    src_in  => src_in,
    src_out => src_out,

    arp_sha => arp_sha,
    arp_spa => arp_spa,
    arp_tpa => arp_tpa
  );
end tb;
