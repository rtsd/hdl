--IP Functional Simulation Model
--VERSION_BEGIN 11.1SP2 cbx_mgl 2012:01:25:21:26:09:SJ cbx_simgen 2012:01:25:21:25:27:SJ  VERSION_END


-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

 LIBRARY sgate;
 USE sgate.sgate_pack.all;

--synthesis_resources = lut 13 mux21 13 oper_add 2 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  altera_merlin_traffic_limiter_0003 IS 
	 PORT 
	 ( 
		 clk	:	IN  STD_LOGIC;
		 cmd_sink_channel	:	IN  STD_LOGIC_VECTOR (6 DOWNTO 0);
		 cmd_sink_data	:	IN  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 cmd_sink_endofpacket	:	IN  STD_LOGIC;
		 cmd_sink_ready	:	OUT  STD_LOGIC;
		 cmd_sink_startofpacket	:	IN  STD_LOGIC;
		 cmd_sink_valid	:	IN  STD_LOGIC;
		 cmd_src_channel	:	OUT  STD_LOGIC_VECTOR (6 DOWNTO 0);
		 cmd_src_data	:	OUT  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 cmd_src_endofpacket	:	OUT  STD_LOGIC;
		 cmd_src_ready	:	IN  STD_LOGIC;
		 cmd_src_startofpacket	:	OUT  STD_LOGIC;
		 cmd_src_valid	:	OUT  STD_LOGIC_VECTOR (6 DOWNTO 0);
		 reset	:	IN  STD_LOGIC;
		 rsp_sink_channel	:	IN  STD_LOGIC_VECTOR (6 DOWNTO 0);
		 rsp_sink_data	:	IN  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 rsp_sink_endofpacket	:	IN  STD_LOGIC;
		 rsp_sink_ready	:	OUT  STD_LOGIC;
		 rsp_sink_startofpacket	:	IN  STD_LOGIC;
		 rsp_sink_valid	:	IN  STD_LOGIC;
		 rsp_src_channel	:	OUT  STD_LOGIC_VECTOR (6 DOWNTO 0);
		 rsp_src_data	:	OUT  STD_LOGIC_VECTOR (68 DOWNTO 0);
		 rsp_src_endofpacket	:	OUT  STD_LOGIC;
		 rsp_src_ready	:	IN  STD_LOGIC;
		 rsp_src_startofpacket	:	OUT  STD_LOGIC;
		 rsp_src_valid	:	OUT  STD_LOGIC
	 ); 
 END altera_merlin_traffic_limiter_0003;

 ARCHITECTURE RTL OF altera_merlin_traffic_limiter_0003 IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_0_397q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_1_396q	:	STD_LOGIC := '0';
	 SIGNAL  wire_ni_w_lg_w471w479w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w_lg_w471w472w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w478w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w471w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_has_pending_responses_362q	:	STD_LOGIC := '0';
	 SIGNAL  wire_nl_w476w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_0_393q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_1_361q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_2_360q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_3_359q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_4_358q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_5_357q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_6_356q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_0_355q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_1_354q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_2_353q	:	STD_LOGIC := '0';
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_sink_ready_400m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_0_422m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_1_421m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_2_420m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_3_419m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_4_418m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_5_417m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_6_416m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_internal_valid_401m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_373m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_374m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_376m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_377m_dataout	:	STD_LOGIC;
	 SIGNAL  wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_a	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_b	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
	 SIGNAL  wire_gnd	:	STD_LOGIC;
	 SIGNAL  wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_o	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_a	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_b	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_o	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_w_lg_w470w474w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_lg_w_cmd_sink_data_range154w465w483w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_cmd_sink_valid467w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w480w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w473w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_lg_w470w474w475w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_reset469w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w513w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w470w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w477w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w466w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_cmd_sink_data_range154w465w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_always1_378_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_402_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_403_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_404_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_405_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_406_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_407_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_408_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_nonposted_cmd_accepted_369_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_response_accepted_371_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_save_dest_id_333_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_399_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_vcc :	STD_LOGIC;
	 SIGNAL  wire_w_cmd_sink_data_range154w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
 BEGIN

	wire_gnd <= '0';
	wire_w_lg_w470w474w(0) <= wire_w470w(0) AND wire_w473w(0);
	wire_w_lg_w_lg_w_cmd_sink_data_range154w465w483w(0) <= wire_w_lg_w_cmd_sink_data_range154w465w(0) AND wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_internal_valid_401m_dataout;
	wire_w_lg_cmd_sink_valid467w(0) <= cmd_sink_valid AND wire_w466w(0);
	wire_w480w(0) <= s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_nonposted_cmd_accepted_369_dataout AND wire_ni_w_lg_w471w479w(0);
	wire_w473w(0) <= s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_response_accepted_371_dataout AND wire_ni_w_lg_w471w472w(0);
	wire_w_lg_w_lg_w470w474w475w(0) <= NOT wire_w_lg_w470w474w(0);
	wire_w_lg_reset469w(0) <= NOT reset;
	wire_w513w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_always1_378_dataout;
	wire_w470w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_nonposted_cmd_accepted_369_dataout;
	wire_w477w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_response_accepted_371_dataout;
	wire_w466w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_399_dataout;
	wire_w_lg_w_cmd_sink_data_range154w465w(0) <= NOT wire_w_cmd_sink_data_range154w(0);
	cmd_sink_ready <= wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_sink_ready_400m_dataout;
	cmd_src_channel <= ( cmd_sink_channel(6 DOWNTO 0));
	cmd_src_data <= ( cmd_sink_data(68 DOWNTO 0));
	cmd_src_endofpacket <= cmd_sink_endofpacket;
	cmd_src_startofpacket <= cmd_sink_startofpacket;
	cmd_src_valid <= ( wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_6_416m_dataout & wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_5_417m_dataout & wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_4_418m_dataout & wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_3_419m_dataout & wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_2_420m_dataout & wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_1_421m_dataout & wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_0_422m_dataout);
	rsp_sink_ready <= rsp_src_ready;
	rsp_src_channel <= ( rsp_sink_channel(6 DOWNTO 0));
	rsp_src_data <= ( rsp_sink_data(68 DOWNTO 0));
	rsp_src_endofpacket <= rsp_sink_endofpacket;
	rsp_src_startofpacket <= rsp_sink_startofpacket;
	rsp_src_valid <= rsp_sink_valid;
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_always1_378_dataout <= (s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_nonposted_cmd_accepted_369_dataout AND s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_response_accepted_371_dataout);
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_402_dataout <= (cmd_sink_valid AND cmd_sink_channel(0));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_403_dataout <= (cmd_sink_valid AND cmd_sink_channel(1));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_404_dataout <= (cmd_sink_valid AND cmd_sink_channel(2));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_405_dataout <= (cmd_sink_valid AND cmd_sink_channel(3));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_406_dataout <= (cmd_sink_valid AND cmd_sink_channel(4));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_407_dataout <= (cmd_sink_valid AND cmd_sink_channel(5));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_408_dataout <= (cmd_sink_valid AND cmd_sink_channel(6));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_nonposted_cmd_accepted_369_dataout <= (wire_w_lg_w_lg_w_cmd_sink_data_range154w465w483w(0) AND (cmd_sink_endofpacket AND cmd_src_ready));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_response_accepted_371_dataout <= (rsp_sink_endofpacket AND (rsp_sink_valid AND rsp_src_ready));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_save_dest_id_333_dataout <= (wire_w_lg_w_cmd_sink_data_range154w465w(0) AND wire_w_lg_cmd_sink_valid467w(0));
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout <= (wire_w_lg_w_cmd_sink_data_range154w465w(0) AND altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_has_pending_responses_362q);
	s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_399_dataout <= (s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout AND (NOT (((NOT (cmd_sink_data(65) XOR altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_0_355q)) AND (NOT (cmd_sink_data(66) XOR altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_1_354q))) AND (NOT (cmd_sink_data(67) XOR altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_2_353q)))));
	s_wire_vcc <= '1';
	wire_w_cmd_sink_data_range154w(0) <= cmd_sink_data(51);
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_0_397q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_1_396q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
			IF (s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_always1_378_dataout = '0') THEN
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_0_397q <= wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_377m_dataout;
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_1_396q <= wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_376m_dataout;
			END IF;
		END IF;
	END PROCESS;
	wire_ni_w_lg_w471w479w(0) <= wire_ni_w471w(0) AND wire_ni_w478w(0);
	wire_ni_w_lg_w471w472w(0) <= wire_ni_w471w(0) AND altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_0_397q;
	wire_ni_w478w(0) <= NOT altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_0_397q;
	wire_ni_w471w(0) <= NOT altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_1_396q;
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_has_pending_responses_362q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_has_pending_responses_362q <= (wire_nl_w476w(0) OR (wire_w477w(0) AND wire_w480w(0)));
		END IF;
	END PROCESS;
	wire_nl_w476w(0) <= altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_has_pending_responses_362q AND wire_w_lg_w_lg_w470w474w475w(0);
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_0_393q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_1_361q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_2_360q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_3_359q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_4_358q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_5_357q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_6_356q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_0_355q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_1_354q <= '0';
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_2_353q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
			IF (s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_save_dest_id_333_dataout = '1') THEN
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_0_393q <= cmd_sink_channel(0);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_1_361q <= cmd_sink_channel(1);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_2_360q <= cmd_sink_channel(2);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_3_359q <= cmd_sink_channel(3);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_4_358q <= cmd_sink_channel(4);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_5_357q <= cmd_sink_channel(5);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_6_356q <= cmd_sink_channel(6);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_0_355q <= cmd_sink_data(65);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_1_354q <= cmd_sink_data(66);
				altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_dest_id_2_353q <= cmd_sink_data(67);
			END IF;
		END IF;
	END PROCESS;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_sink_ready_400m_dataout <= cmd_src_ready AND NOT(s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_399_dataout);
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_0_422m_dataout <= (altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_0_393q AND s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_402_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_402_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_1_421m_dataout <= (altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_1_361q AND s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_403_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_403_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_2_420m_dataout <= (altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_2_360q AND s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_404_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_404_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_3_419m_dataout <= (altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_3_359q AND s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_405_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_405_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_4_418m_dataout <= (altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_4_358q AND s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_406_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_406_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_5_417m_dataout <= (altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_5_357q AND s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_407_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_407_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_6_416m_dataout <= (altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_last_channel_6_356q AND s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_408_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_398_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_cmd_src_valid_408_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_internal_valid_401m_dataout <= cmd_sink_valid AND NOT(s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_suppress_399_dataout);
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_373m_dataout <= wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_o(1) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_nonposted_cmd_accepted_369_dataout = '1'  ELSE altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_1_396q;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_374m_dataout <= wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_o(0) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_nonposted_cmd_accepted_369_dataout = '1'  ELSE altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_0_397q;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_376m_dataout <= wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_o(2) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_response_accepted_371_dataout = '1'  ELSE wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_373m_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_377m_dataout <= wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_o(1) WHEN s_wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_response_accepted_371_dataout = '1'  ELSE wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_next_pending_response_count_374m_dataout;
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_a <= ( altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_1_396q & altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_0_397q);
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_b <= ( "0" & "1");
	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372 :  oper_add
	  GENERIC MAP (
		sgate_representation => 0,
		width_a => 2,
		width_b => 2,
		width_o => 2
	  )
	  PORT MAP ( 
		a => wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_a,
		b => wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_b,
		cin => wire_gnd,
		o => wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add0_372_o
	  );
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_a <= ( altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_1_396q & altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_pending_response_count_0_397q & "1");
	wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_b <= ( "1" & "0" & "1");
	altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375 :  oper_add
	  GENERIC MAP (
		sgate_representation => 0,
		width_a => 3,
		width_b => 3,
		width_o => 3
	  )
	  PORT MAP ( 
		a => wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_a,
		b => wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_b,
		cin => wire_gnd,
		o => wire_altera_merlin_traffic_limiter_0003_altera_merlin_traffic_limiter_limiter_002_add1_375_o
	  );

 END RTL; --altera_merlin_traffic_limiter_0003
--synopsys translate_on
--VALID FILE
