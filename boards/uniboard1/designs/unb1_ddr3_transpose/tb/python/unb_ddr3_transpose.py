#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
"""
On execution, this script will start ModelSim, compile and run DESIGN_NAME and
run COMMANDS against the running simulation. If --hold is not passed, the running
simulation will be killed after completion.
"""
import sys, os
sys.path.append(os.environ['UNB']+'/Firmware/sim/python')
from auto_sim import *

LIBRARY_NAME = 'unb_ddr3_transpose'
TB_NAME      = 'tb_unb_ddr3_transpose'   
TARGET_NODES = ' --unb 0 --bn 3 '
COMMANDS     = ['$UPE_GEAR/peripherals/util_system_info.py' +TARGET_NODES+ '-n 2',
                '$UPE_GEAR/peripherals/util_system_info.py' +TARGET_NODES+ '-n 4',
                '$UPE_GEAR/peripherals/util_unb_sens.py'    +TARGET_NODES+ '-n 0'
               ]

# Give sim some time until the sensors have been read
INIT_DELAY_NS = 5000

GENERICS = {}
OTHER = ''
    
# Run the sim and return its result using sys.exit([return_value])
sys.exit(auto_sim(os.environ['UNB'], LIBRARY_NAME, TB_NAME, COMMANDS, INIT_DELAY_NS, GENERICS, OTHER))