-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- Verify multiple variations of tb_dp_block_validate_bsn_at_sync
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_block_validate_bsn_at_sync is
end tb_tb_dp_block_validate_bsn_at_sync;

architecture tb of tb_tb_dp_block_validate_bsn_at_sync is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  constant c_blk_per_sync   : natural := 5;
  constant c_data_per_blk   : natural := 9;
begin
--    g_nof_blocks_per_sync  : NATURAL := 5;
--    g_nof_data_per_blk     : NATURAL := 6;
--    g_bsn_init             : NATURAL := 7 -- >= g_nof_blocks_per_sync for discarded sync, < g_nof_blocks_per_sync for no discarded sync.

  u_smaller    : entity work.tb_dp_block_validate_bsn_at_sync generic map(c_blk_per_sync, c_data_per_blk, c_blk_per_sync - 4);  -- g_bsn_init < g_nof_blocks_per_sync
  u_equal      : entity work.tb_dp_block_validate_bsn_at_sync generic map(c_blk_per_sync, c_data_per_blk, c_blk_per_sync);  -- g_bsn_init = g_nof_blocks_per_sync
  u_larger     : entity work.tb_dp_block_validate_bsn_at_sync generic map(c_blk_per_sync, c_data_per_blk, c_blk_per_sync + 4);  -- g_bsn_init > g_nof_blocks_per_sync
end tb;
