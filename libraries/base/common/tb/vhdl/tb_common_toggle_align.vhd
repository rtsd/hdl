-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra, 20 jul 2017, Created
-- Purpose: Test bench for common_toggle_align.vhd
-- Usage:
-- > as 10
-- > run -all
-- Observe out_toggle in Wave Window in relation to in_toggle and align

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_toggle_align is
end tb_common_toggle_align;

architecture tb of tb_common_toggle_align is
  constant clk_period           : time := 10 ns;

  constant c_interval           : natural := 10;
  constant c_pipeline           : natural := 0;
  constant c_toggle_period      : natural := 4;
  constant c_half_period        : natural := c_toggle_period / 2;

  signal tb_end     : std_logic := '0';
  signal rst        : std_logic;
  signal clk        : std_logic := '0';
  signal in_align   : std_logic;
  signal in_toggle  : std_logic;
  signal out_toggle : std_logic;
begin
  clk  <= not clk or tb_end after clk_period / 2;
  rst  <= '1', '0' after 7 * clk_period;

  p_in_stimuli : process
  begin
    in_align <= '1';
    in_toggle <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    ---------------------------------------------------------------------------
    -- align
    ---------------------------------------------------------------------------

    in_align <= '1';

    -- start toggling
    for I in 0 to c_interval - 1 loop
      in_toggle <= not in_toggle;
      proc_common_wait_some_cycles(clk, c_half_period);
    end loop;
    -- introduce a glitch in the toggling and check that out_toggle remains aligned
    proc_common_wait_some_cycles(clk, 1);
    for I in 0 to c_interval - 1 loop
      in_toggle <= not in_toggle;
      proc_common_wait_some_cycles(clk, c_half_period);
    end loop;

    ---------------------------------------------------------------------------
    -- maintain
    ---------------------------------------------------------------------------
    in_align <= '0';

    -- continue toggling
    for I in 0 to c_interval - 1 loop
      in_toggle <= not in_toggle;
      proc_common_wait_some_cycles(clk, c_half_period);
    end loop;
    -- introduce a glitch in the toggling and check that out_toggle ignores the glitch
    proc_common_wait_some_cycles(clk, 1);
    for I in 0 to c_interval - 1 loop
      in_toggle <= not in_toggle;
      proc_common_wait_some_cycles(clk, c_half_period);
    end loop;

    ---------------------------------------------------------------------------
    -- align again
    ---------------------------------------------------------------------------

    in_align <= '1';

    -- continue toggling and check that out_toggle aligns again
    for I in 0 to c_interval - 1 loop
      in_toggle <= not in_toggle;
      proc_common_wait_some_cycles(clk, c_half_period);
    end loop;

    ---------------------------------------------------------------------------
    -- stop in_toggle
    ---------------------------------------------------------------------------
    proc_common_wait_some_cycles(clk, c_half_period * c_interval);

    ---------------------------------------------------------------------------
    -- maintain after in_toggle has stopped
    ---------------------------------------------------------------------------
    in_align <= '0';
    proc_common_wait_some_cycles(clk, c_half_period * c_interval);

    ---------------------------------------------------------------------------
    -- end
    ---------------------------------------------------------------------------
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  u_toggle : entity work.common_toggle_align
  generic map (
    g_pipeline           => c_pipeline,
    g_nof_clk_per_period => c_toggle_period
  )
  port map (
    rst         => rst,
    clk         => clk,
    in_align    => in_align,
    in_toggle   => in_toggle,
    out_toggle  => out_toggle
  );
end tb;
