function outData = polyphase_synthesis_b(inBins, filt, Nstep)

% polyphase synthesis on blocks of filterbank data
%       John Bunton original version Oct 2003

% inBins = input bin data, 2D array of multiple blocks of FFT data.
%          Nfft = number of frequency bins
% filt = prototype lowpass filter
% Nstep = increment along data at output of IFFT, so upsample inBins by
%         Nstep, use Nstep = Nftt for critically sampled inBins
% outData = reconstructed output time data
%
s = size(inBins);
nof_blocks = s(1);  % number of FFT blocks
Nfft = s(2);  % length of FFT

% Support padding Ncoefs with zeros, if filt is too short
Ntaps = ceil(length(filt) / Nfft);
Ncoefs = Ntaps * Nfft;
bfir = (1:Ncoefs) * 0;
bfir(1:length(filt)) = filt;

outData = (1:nof_blocks*Nstep + Ncoefs) * 0;
for n = 1:nof_blocks
    % IFFT
    inData = real(ifft(inBins(n,:)));

    % Copy the FFT data at each FIR tap
    tapData = (1:Ncoefs) * 0;
    for m = 0:Ntaps-1
        tapData((1:Nfft) + m*Nfft) = inData;
    end
    % Apply the FIR coefficients by using bfir as window
    tapData = tapData .* bfir;

    % Sum the FIR taps by overlap add the weighted input to the output.
    % Progress with Nstep per block of Nfft to upsample by Nstep, to match
    % the downsample by Nstep of the analysis filterbank.
    range = (1:Ncoefs) + (n-1)*Nstep;
    outData(range) = outData(range) + tapData;  % continuous time output
    if n <= 3
        %inBins(n,:)
        %inData
        %tapData
        %outData(range)
    end
end
