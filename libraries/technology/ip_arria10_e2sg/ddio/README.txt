README.txt for $HDL_WORK/libraries/technology/ip_arria10/ddio

Contents:

1) DDIO components
2) Arria10 IP
3) Synthesis trials
4) Issues


1) DDIO components:
  ip_arria10_ddio_in.vhd   = Double Date Rate input
  ip_arria10_ddio_out.vhd  = Double Date Rate output
  

2) Arria10 IP

  The StratixIV IP uses altddio_in and altddio_out. First a Megawizard file for this StratixIV IP was made using the settings that
  were used in common_ddio_in.vhd and common_ddio_out.vhd. This Megawizard IP file was then opened in Quartus to be able to let
  Quartus 14 convert them using the altera_gpio component for Arria10.
  
  The altera_gpio component is not part of the default Quartus 14.0a10 tool libraries, but instead it is created by Qsys together
  with the IP. This makes that the altera_gpio can not easily be used in simulation and synthesis like was possible with altera_mf
  in for Stratix IV (Quartus 11.1).
  
  The ddio_in component is used by the PPSH and the ddio_out component is used by the ADUH. In both cases the g_width=1.
  The Arria10 IP can be generated using a fixed width of 1. Therefore the width was set to 1 in the conversion from MegaWizard
  to Qsys and the qsys files are stored as:
  
    ip_arria10_ddio_in_1.qsys
    ip_arria10_ddio_out_1.qsys
  
  If the application would need a wider port then it can achieve this by instantiating the IP multiple times. This approach
  avoids having to generate DDIO IP for every possible width. An alternative would be:
  - to generate IP for e.g. width=16 and assuming that that is sufficient for all applications. Any application that uses less
    width then leaves these IO unconnected so that the unused IO will get optimized away by synthesis.
  - create the IP when it is needed, this scheme is more difficult to manage but is something to consider for the future.
  
  The IP needs to be generated with:
  
  ./generate_ip.sh
  
  to create the simulation and synthesis files, because these are initially not kept in SVN.
  

3) Synthesis trials

  The Quartus project:
  
    quartus/ddio.qpf
  
  was used to verify that the DDIO IP actually synthesise to the appropriate FPGA resources.
  Use the Quartus GUI to manually select a top level component for synthesis e.g. by right clicking the entity vhd file
  in the file tab of the Quartus project navigator window.
  Then check the resource usage in the synthesis and fitter reports.


4) Issues

a) Simulation model does not work (for Quartus 14.1, not tried for Quartus 15.0)

  The simulation model for the DDIO does not compile ok because a din port is missing in the ddio_out en a dout port is
  missing in the ddio_in. Adding this ports manualy does work for compile, but when the component is loaded as a simulation
  then Modelsim reports some internal error on the IP. The compile also does not work when using 'do msim_setup.tcl', so
  there is something wrong with the DDIO simulation model. The synthesis of the DDIO IP using ddio.qpf does work.
  
  The work around is not not use the simulation model, but instead use a behavioral simulation model for the IP:
     sim/ip_arria10_ddio_in_1.vhd
     sim/ip_arria10_ddio_out_1.vhd
     sim/tb_ip_arria10_ddio_1.vhd   = self checking tb for ip_arria10_ddio_in_1 -> ip_arria10_ddio_out_1
  
  The selection between the IP model or the behavioral model is made in the compile_ip.tcl script.
  
