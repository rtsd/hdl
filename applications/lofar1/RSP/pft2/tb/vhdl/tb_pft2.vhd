-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Usage:
-- > as 3
-- > run -a
-- - View PFT in and out signals in Wave Window with decimal radix and in analog format:
--   . Input: in_dat_x, in_dat_y
--   . Output:
--     - PFT_MODE_BITREV, PFT_MODE_COMPLEX: out_fft_re, out_fft_im
--     - PFT_MODE_REAL2: out_x_re, out_x_im, out_y_re, out_y_im
--   . Copy these signals in Wave Window to view them in both literal format and analog format
--   View diff_max_* in Wave Window, if diff_max_* < c_diff_max then tb yields OK.
-- - The tb works OK for all three PFT modes.

library IEEE, tst_lib, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.pft_pkg.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_pft2 is
  generic (
    -- >>> PFT settings
    g_switch_en    : std_logic := '0';
    g_stage_dat_w  : natural := c_pft_stage_dat_w;  -- c_pft_stage_dat_w = 20 in pft_pkg.vhd

    -- The PFT has 3 modes:
    -- . PFT_MODE_BITREV   --> DIT output
    -- . PFT_MODE_COMPLEX  --> DIF output
    -- . PFT_MODE_REAL2    --> DIT output of two real inputs (default for LOFAR)
    --g_pft_mode : pft_mode_type := PFT_MODE_BITREV;
    --g_pft_mode : pft_mode_type := PFT_MODE_COMPLEX;
    g_pft_mode : pft_mode_type := PFT_MODE_REAL2;

    -- . For PFT_MODE_REAL2 select any signal pair for X and Y input.
    -- . For PFT_MODE_BITREV and PFT_MODE_COMPLEX select zeros for Y input.

    -- >>> Testbench settings
    -- Select one input signal for X (used as real input to the PFT)
    --g_name_x   : STRING  := "cosin_N2";
    --g_name_x   : STRING  := "cosin_1";
    --g_name_x   : STRING  := "cosin_39";
    --g_name_x   : STRING  := "sinus_1";
    --g_name_x   : STRING  := "sinus_13";
    --g_name_x   : STRING  := "sinus_13s";
    --g_name_x   : STRING  := "impulse_0";
    --g_name_x   : STRING  := "impulse_1";
    --g_name_x   : STRING  := "zeros";
    --g_name_x   : STRING  := "dc";
    g_name_x   : string  := "block_1";
    --g_name_x   : STRING  := "block_117";
    --g_name_x   : STRING  := "u_noise";

    -- Select one input signal for Y (used as imag input to the PFT)
    --g_name_y   : STRING  := "cosin_N2";
    --g_name_y   : STRING  := "cosin_1";
    --g_name_y   : STRING  := "cosin_39";
    --g_name_y   : STRING  := "sinus_1";
    --g_name_y   : STRING  := "sinus_13";
    --g_name_y   : STRING  := "sinus_13s";
    --g_name_y   : STRING  := "impulse_0";
    --g_name_y   : STRING  := "impulse_1";
    g_name_y   : string  := "zeros";  -- For PFT_MODE_BITREV and PFT_MODE_COMPLEX select zeros for Y input.
    --g_name_y   : STRING  := "dc";
    --g_name_y   : STRING  := "block_1";
    --g_name_y   : STRING  := "block_117";
    --g_name_y   : STRING  := "u_noise";

    g_repeat   : natural := 2  -- minimal 2 due to PFT latency
    --g_repeat   : NATURAL := 10  -- > c_nof_block_per_sync to view multiple in_sync and out_sync intervals
  );
end tb_pft2;

architecture tb of tb_pft2 is
  constant c_clk_period         : time    := 10 ns;
  constant c_rst_period         : natural := 20;
  constant c_fft_size_w         : natural := 10;
  constant c_in_dat_w           : natural := 18;
  constant c_out_dat_w          : natural := 18;

  constant c_nof_block_per_sync : natural := 2;

  -- The input stimuli signals are located in c_tst_data_dir and have been
  -- generated with TC 5.2 and are used in the PFT continuous test TC 5.19.
  constant c_tst_data_dir       : string  := "data/";

  -- Maximum quantization error in PFT output
  constant c_diff_max           : natural := 20;  -- Maximum quantization error in PFT output, as used in tc.tcl
  --CONSTANT c_diff_max           : NATURAL := 10;  -- value per subband
  --CONSTANT c_pdiff_max          : NATURAL := 5;  -- average power diff over all subbands from TC 5.19

  -- Input signal for X and Y (used as real and imag input to the PFT)
  constant c_file_pft_in_x      : string  := c_tst_data_dir & g_name_x & ".sig";
  constant c_file_pft_in_y      : string  := c_tst_data_dir & g_name_y & ".sig";

  -- The reference FFT ouput results have been generated with MATLAB.
  constant c_file_pft_ref_x_re  : string  := c_tst_data_dir & g_name_x & ".re";
  constant c_file_pft_ref_x_im  : string  := c_tst_data_dir & g_name_x & ".im";
  constant c_file_pft_ref_y_re  : string  := c_tst_data_dir & g_name_y & ".re";
  constant c_file_pft_ref_y_im  : string  := c_tst_data_dir & g_name_y & ".im";
  constant c_file_pft_dat_w     : natural := 32;

  constant c_fft_size           : natural := 2**c_fft_size_w;

  type t_ref_dat       is array (0 to c_fft_size    ) of integer;  -- one extra dummy
  type t_ref_fft_dat   is array (0 to c_fft_size-1)   of integer;  -- PFT_MODE_BITREV, PFT_MODE_COMPLEX scaled and rounded
  type t_ref_real2_dat is array (0 to c_fft_size / 2 - 1) of integer;  -- PFT_MODE_REAL2 scaled and rounded

  -- Signals
  signal tb_end             : std_logic := '0';
  signal clk                : std_logic := '1';
  signal rst                : std_logic := '1';
  signal rst_sync           : std_logic := '1';

  -- PFT input stimuli from file
  signal in_en              : std_logic;
  signal in_sync            : std_logic;
  signal in_dat_x           : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal in_dat_y           : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal in_val_x           : std_logic;
  signal in_val_y           : std_logic;
  signal in_val             : std_logic;

  -- PFT output
  signal out_re             : std_logic_vector(c_out_dat_w - 1 downto 0);
  signal out_im             : std_logic_vector(c_out_dat_w - 1 downto 0);
  signal out_val            : std_logic;
  signal toggle             : std_logic;
  signal nxt_toggle         : std_logic;
  signal toggle_dly         : std_logic;
  signal out_re_dly         : std_logic_vector(c_out_dat_w - 1 downto 0);
  signal out_im_dly         : std_logic_vector(c_out_dat_w - 1 downto 0);
  signal out_val_dly        : std_logic;
  -- For PFT_MODE_BITREV, PFT_MODE_COMPLEX
  signal nxt_out_fft_re     : integer;
  signal nxt_out_fft_im     : integer;
  signal out_fft_re         : integer;
  signal out_fft_im         : integer;
  -- For PFT_MODE_REAL2
  signal nxt_out_x_re       : integer;
  signal nxt_out_x_im       : integer;
  signal nxt_out_y_re       : integer;
  signal nxt_out_y_im       : integer;
  signal out_x_re           : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal out_x_im           : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal out_y_re           : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal out_y_im           : integer := 0;  -- init at 0 to fit automatic analog scaling in Wave Window
  signal out_sync           : std_logic;

  -- Reference FFT output data from MATLAB generated files
  signal ref_en             : std_logic := '1';
  signal ref_dat_x_re       : std_logic_vector(c_file_pft_dat_w - 1 downto 0);
  signal ref_dat_x_im       : std_logic_vector(c_file_pft_dat_w - 1 downto 0);
  signal ref_dat_y_re       : std_logic_vector(c_file_pft_dat_w - 1 downto 0);
  signal ref_dat_y_im       : std_logic_vector(c_file_pft_dat_w - 1 downto 0);
  signal ref_val_x_re       : std_logic;
  signal ref_val_x_im       : std_logic;
  signal ref_val_y_re       : std_logic;
  signal ref_val_y_im       : std_logic;
  signal ref_val            : std_logic;
  signal ref_val_prev       : std_logic := '0';
  signal ref_rdy            : std_logic;
  signal nxt_ref_rdy        : std_logic;
  signal rd_cnt             : natural := 0;
  signal nxt_rd_cnt         : natural;
  signal rd_dat_x_re        : t_ref_dat;  -- Keep the ref data in an array
  signal rd_dat_x_im        : t_ref_dat;
  signal rd_dat_y_re        : t_ref_dat;
  signal rd_dat_y_im        : t_ref_dat;
  -- For PFT_MODE_BITREV, PFT_MODE_COMPLEX
  signal ref_fft_dat_re     : t_ref_fft_dat;  -- Scaled and rounded ref data array
  signal ref_fft_dat_im     : t_ref_fft_dat;
  -- For PFT_MODE_REAL2
  signal ref_real2_dat_x_re : t_ref_real2_dat;  -- Scaled and rounded ref data array
  signal ref_real2_dat_x_im : t_ref_real2_dat;
  signal ref_real2_dat_y_re : t_ref_real2_dat;
  signal ref_real2_dat_y_im : t_ref_real2_dat;

  -- Difference between PFT output and the reference FFT output data
  -- For PFT_MODE_BITREV, PFT_MODE_COMPLEX
  signal diff_fft_re        : integer := 0;
  signal diff_fft_im        : integer := 0;
  signal diff_max_fft_re    : integer := 0;
  signal diff_max_fft_im    : integer := 0;
  signal pdiff_fft_re       : integer := 0;
  signal pdiff_fft_im       : integer := 0;
  -- For PFT_MODE_REAL2
  signal diff_x_re          : integer := 0;
  signal diff_x_im          : integer := 0;
  signal diff_y_re          : integer := 0;
  signal diff_y_im          : integer := 0;
  signal diff_max_x_re      : integer := 0;
  signal diff_max_x_im      : integer := 0;
  signal diff_max_y_re      : integer := 0;
  signal diff_max_y_im      : integer := 0;
  signal diff_val           : std_logic;
  signal diff_cnt           : natural := 0;
  signal nxt_diff_cnt       : natural;
  signal diff_rdy           : std_logic;
  signal nxt_diff_rdy       : std_logic;

  -----------------------------------------------------------------------------
  -- Procedures to scale and round the reference FFT results
  -----------------------------------------------------------------------------

  -- PFT_MODE_BITREV
  function func_bitrev(n, w : in natural) return natural is
    variable un : unsigned(w - 1 downto 0);
    variable ur : unsigned(w - 1 downto 0);
  begin
    un := to_unsigned(n, w);
    for i in 0 to w - 1 loop
      ur(i) := un(w - 1 - i);
    end loop;
    return to_integer(ur);
  end func_bitrev;

  procedure proc_fft_bitrev(en        : in  std_logic;
                            ref       : in  t_ref_dat;
                            signal sr : out t_ref_fft_dat) is
    constant N : natural := c_fft_size;
    constant w : natural := c_fft_size_w;
    variable r : natural;
  begin
    if en = '1' then
      for i in 0 to N - 1 loop
        r := func_bitrev(i, w);
        sr(r) <= ref(i) / N;
      end loop;
    end if;
  end proc_fft_bitrev;

  -- PFT_MODE_COMPLEX
  procedure proc_fft_complex(en        : in  std_logic;
                             ref       : in  t_ref_dat;
                             signal sr : out t_ref_fft_dat) is
    constant N  : natural := c_fft_size;
  begin
    if en = '1' then
      for i in 0 to N - 1 loop
        sr(i) <= ref(i) / N;
      end loop;
    end if;
  end proc_fft_complex;

  -- PFT_MODE_REAL2: The procedures are based on the TCL code from TC 5.19.
  --
  -- . The PFT stages scale the output by the FFT size N so by 1/N = 1/$c_slice_size.
  -- . PFT seperate does not divide by 2 in Xa(m) = [X*(N-m) + X(m)]/2, Xb(m)=j[X*(N-m) - X(m)]/2
  -- . PFT seperate result for m=N is same as for m=0
  -- . PFT seperate puts real result for m=N/2 in imag of m=0
  procedure proc_fft_real2_im(en        : in  std_logic;
                              re        : in  t_ref_dat;
                              im        : in  t_ref_dat;
                              signal sr : out t_ref_real2_dat) is
    constant N  : natural := c_fft_size;
    variable lo : integer;
    variable hi : integer;
  begin
    if en = '1' then
      sr(0) <= 2 * re(N / 2) / N;
      for i in 1 to N / 2 - 1 loop
        lo := im(i);
        hi := im(N - i);
        sr(i) <= (-hi + lo) / N;
      end loop;
    end if;
  end proc_fft_real2_im;

  procedure proc_fft_real2_re(en        : in  std_logic;
                              re        : in  t_ref_dat;
                              signal sr : out t_ref_real2_dat) is
    constant N  : natural := c_fft_size;
    variable lo : integer;
    variable hi : integer;
  begin
    if en = '1' then
      sr(0) <= 2 * re(0) / N;
      for i in 1 to N / 2 - 1 loop
        lo := re(i);
        hi := re(N - i);
        sr(i) <= (hi + lo) / N;
      end loop;
    end if;
  end proc_fft_real2_re;
begin
  p_tb_end : process
  begin
    proc_common_wait_until_hi_lo(clk, out_val);  -- end of test
    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  rst <= '0' after c_clk_period * c_rst_period;

  clk <= (not clk) or tb_end after c_clk_period / 2;

  in_en  <= '0', '1' after c_clk_period * c_rst_period * 2;
  in_val <= in_val_x;

  rst_sync <= '1', '0' after c_clk_period * (c_rst_period * 2 - 1);  -- start in_sync pulse interval 1 clk cycle before first in_val = '1'
  proc_common_gen_pulse(1, c_nof_block_per_sync * c_fft_size, '1', rst_sync, clk, in_sync);

  -----------------------------------------------------------------------------
  -- Input X, Y data
  -----------------------------------------------------------------------------

  u_in_x: entity tst_lib.tst_input
  generic map (
    g_file_name   => c_file_pft_in_x,
    g_file_repeat => g_repeat,
    g_nof_data    => 1,
    g_data_width  => c_in_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk       => clk,
    rst       => rst,
    en        => in_en,
    out_dat   => in_dat_x,
    out_val   => in_val_x
  );

  u_in_y: entity tst_lib.tst_input
  generic map (
    g_file_name   => c_file_pft_in_y,
    g_file_repeat => g_repeat,
    g_nof_data    => 1,
    g_data_width  => c_in_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk       => clk,
    rst       => rst,
    en        => in_en,
    out_dat   => in_dat_y,
    out_val   => in_val_y
  );

  -----------------------------------------------------------------------------
  -- Read expected Xre, Xim, Yre, Yim data
  -----------------------------------------------------------------------------

  u_ref_x_re: entity tst_lib.tst_input
  generic map (
    g_file_name   => c_file_pft_ref_x_re,
    g_file_repeat => 1,
    g_nof_data    => 1,
    g_data_width  => c_file_pft_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk       => clk,
    rst       => rst,
    en        => ref_en,
    out_dat   => ref_dat_x_re,
    out_val   => ref_val_x_re
  );

  u_ref_x_im: entity tst_lib.tst_input
  generic map (
    g_file_name   => c_file_pft_ref_x_im,
    g_file_repeat => 1,
    g_nof_data    => 1,
    g_data_width  => c_file_pft_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk       => clk,
    rst       => rst,
    en        => ref_en,
    out_dat   => ref_dat_x_im,
    out_val   => ref_val_x_im
  );

  u_ref_y_re: entity tst_lib.tst_input
  generic map (
    g_file_name   => c_file_pft_ref_y_re,
    g_file_repeat => 1,
    g_nof_data    => 1,
    g_data_width  => c_file_pft_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk       => clk,
    rst       => rst,
    en        => ref_en,
    out_dat   => ref_dat_y_re,
    out_val   => ref_val_y_re
  );

  u_ref_y_im: entity tst_lib.tst_input
  generic map (
    g_file_name   => c_file_pft_ref_y_im,
    g_file_repeat => 1,
    g_nof_data    => 1,
    g_data_width  => c_file_pft_dat_w,
    g_data_type   => "SIGNED"
  )
  port map (
    clk       => clk,
    rst       => rst,
    en        => ref_en,
    out_dat   => ref_dat_y_im,
    out_val   => ref_val_y_im
  );

  p_ref_reg : process(clk)
  begin
    if rising_edge(clk) then
      rd_cnt       <= nxt_rd_cnt;
      ref_val_prev <= ref_val;
      ref_rdy      <= nxt_ref_rdy;
    end if;
  end process;

  nxt_rd_cnt <= rd_cnt + 1 when ref_val = '1' else rd_cnt;

  rd_dat_x_re(rd_cnt) <= to_integer(signed(ref_dat_x_re));
  rd_dat_x_im(rd_cnt) <= to_integer(signed(ref_dat_x_im));
  rd_dat_y_re(rd_cnt) <= to_integer(signed(ref_dat_y_re));
  rd_dat_y_im(rd_cnt) <= to_integer(signed(ref_dat_y_im));

  ref_val <= ref_val_x_re;
  nxt_ref_rdy <= not ref_val and ref_val_prev;

  -- Adapt the reference results to the PFT mode

  gen_fft_bitrev : if g_pft_mode = PFT_MODE_BITREV generate
    -- Support only real input test signal to the PFT, so imag input is zeros
    proc_fft_bitrev(ref_rdy, rd_dat_x_re, ref_fft_dat_re);
    proc_fft_bitrev(ref_rdy, rd_dat_x_im, ref_fft_dat_im);
  end generate;

  gen_fft_complex : if g_pft_mode = PFT_MODE_COMPLEX generate
    -- Support only real input test signal to the PFT, so imag input is zeros
    proc_fft_complex(ref_rdy, rd_dat_x_re, ref_fft_dat_re);
    proc_fft_complex(ref_rdy, rd_dat_x_im, ref_fft_dat_im);
  end generate;

  gen_fft_real2 : if g_pft_mode = PFT_MODE_REAL2 generate
    -- Scale and round the reference FFT outputs for the two real inputs X and Y
    proc_fft_real2_re(ref_rdy, rd_dat_x_re,              ref_real2_dat_x_re);
    proc_fft_real2_re(ref_rdy, rd_dat_y_re,              ref_real2_dat_y_re);
    proc_fft_real2_im(ref_rdy, rd_dat_x_re, rd_dat_x_im, ref_real2_dat_x_im);
    proc_fft_real2_im(ref_rdy, rd_dat_y_re, rd_dat_y_im, ref_real2_dat_y_im);
  end generate;

  -----------------------------------------------------------------------------
  -- Verify
  -----------------------------------------------------------------------------

  p_diff_reg : process(clk)
  begin
    if rising_edge(clk) then
      diff_cnt  <= nxt_diff_cnt;
      diff_rdy  <= nxt_diff_rdy;
    end if;
  end process;

  gen_diff_fft : if g_pft_mode = PFT_MODE_BITREV or g_pft_mode = PFT_MODE_COMPLEX generate
    nxt_diff_cnt <= diff_cnt + 1 when out_val_dly = '1' and diff_cnt < c_fft_size-1 else
                    0            when out_val_dly = '1' and diff_cnt = c_fft_size-1 else
                    diff_cnt;
    nxt_diff_rdy <= '1' when diff_cnt = c_fft_size-1 else '0';

    diff_fft_re <= 0 when out_val_dly = '0' else ref_fft_dat_re(diff_cnt) - out_fft_re;
    diff_fft_im <= 0 when out_val_dly = '0' else ref_fft_dat_im(diff_cnt) - out_fft_im;

    diff_max_fft_re <= largest(abs(diff_fft_re), diff_max_fft_re);
    diff_max_fft_im <= largest(abs(diff_fft_im), diff_max_fft_im);

    assert diff_max_fft_re <= c_diff_max
      report "FFT re output differs to much from reference data"
      severity ERROR;
    assert diff_max_fft_im <= c_diff_max
      report "FFT im output differs to much from reference data"
      severity ERROR;

    p_report : process(diff_rdy)
    begin
      if diff_rdy = '1' then
        if diff_max_fft_re <= c_diff_max then report "FFT real output for re " & g_name_x & " and im " & g_name_y & " is OK"    severity NOTE;
                                         else report "FFT real output for re " & g_name_x & " and im " & g_name_y & " is wrong" severity ERROR; end if;
        if diff_max_fft_im <= c_diff_max then report "FFT imag output for im " & g_name_x & " and im " & g_name_y & " is OK"    severity NOTE;
                                         else report "FFT imag output for im " & g_name_x & " and im " & g_name_y & " is wrong" severity ERROR; end if;
      end if;
    end process;
  end generate;

  gen_diff_real2 : if g_pft_mode = PFT_MODE_REAL2 generate
    nxt_diff_cnt <= diff_cnt + 1 when out_val_dly = '1' and toggle_dly = '1' and diff_cnt < c_fft_size / 2 - 1 else
                    0            when out_val_dly = '1' and toggle_dly = '1' and diff_cnt = c_fft_size / 2 - 1 else
                    diff_cnt;
    nxt_diff_rdy <= '1' when diff_cnt = c_fft_size / 2 - 1 else '0';

    diff_x_re <= 0 when out_val_dly = '0' else ref_real2_dat_x_re(diff_cnt) - out_x_re when toggle_dly = '0' else diff_x_re;
    diff_x_im <= 0 when out_val_dly = '0' else ref_real2_dat_x_im(diff_cnt) - out_x_im when toggle_dly = '0' else diff_x_im;
    diff_y_re <= 0 when out_val_dly = '0' else ref_real2_dat_y_re(diff_cnt) - out_y_re when toggle_dly = '1' else diff_y_re;
    diff_y_im <= 0 when out_val_dly = '0' else ref_real2_dat_y_im(diff_cnt) - out_y_im when toggle_dly = '1' else diff_y_im;

    diff_max_x_re <= largest(abs(diff_x_re), diff_max_x_re);
    diff_max_x_im <= largest(abs(diff_x_im), diff_max_x_im);
    diff_max_y_re <= largest(abs(diff_y_re), diff_max_y_re);
    diff_max_y_im <= largest(abs(diff_y_im), diff_max_y_im);

    assert diff_max_x_re <= c_diff_max
      report "FFT X re output differs too much from reference data"
      severity ERROR;
    assert diff_max_x_im <= c_diff_max
      report "FFT X im output differs too much from reference data"
      severity ERROR;
    assert diff_max_y_re <= c_diff_max
      report "FFT Y re output differs too much from reference data"
      severity ERROR;
    assert diff_max_y_im <= c_diff_max
      report "FFT Y im output differs too much from reference data"
      severity ERROR;

    p_report : process(diff_rdy)
    begin
      if diff_rdy = '1' then
        if diff_max_x_re <= c_diff_max then report "FFT X real output for " & g_name_x & " is OK"    severity NOTE;
                                       else report "FFT X real output for " & g_name_x & " is wrong" severity ERROR; end if;
        if diff_max_x_im <= c_diff_max then report "FFT X imag output for " & g_name_x & " is OK"    severity NOTE;
                                       else report "FFT X imag output for " & g_name_x & " is wrong" severity ERROR; end if;
        if diff_max_y_re <= c_diff_max then report "FFT Y real output for " & g_name_y & " is OK"    severity NOTE;
                                       else report "FFT Y real output for " & g_name_y & " is wrong" severity ERROR; end if;
        if diff_max_y_im <= c_diff_max then report "FFT Y imag output for " & g_name_y & " is OK"    severity NOTE;
                                       else report "FFT Y imag output for " & g_name_y & " is wrong" severity ERROR; end if;
      end if;
    end process;
  end generate;

  diff_val <= out_val_dly;

  -----------------------------------------------------------------------------
  -- PFT
  -----------------------------------------------------------------------------

  u_pft : entity work.pft
  generic map (
    g_fft_size_w   => c_fft_size_w,
    g_in_dat_w     => c_in_dat_w,
    g_out_dat_w    => c_out_dat_w,
    g_stage_dat_w  => g_stage_dat_w,
    g_mode         => g_pft_mode
  )
  port map (
    in_re          => in_dat_x,
    in_im          => in_dat_y,
    in_val         => in_val,
    in_sync        => in_sync,
    switch_en      => g_switch_en,
    out_re         => out_re,
    out_im         => out_im,
    out_val        => out_val,
    out_sync       => out_sync,
    clk            => clk,
    rst            => rst
  );

  p_pft_reg : process(clk)
  begin
    if rising_edge(clk) then
      toggle      <= nxt_toggle;
      toggle_dly  <= toggle;
      out_re_dly  <= out_re;
      out_im_dly  <= out_im;
      out_val_dly <= out_val;
      out_fft_re  <= nxt_out_fft_re;
      out_fft_im  <= nxt_out_fft_im;
      out_x_re    <= nxt_out_x_re;
      out_y_re    <= nxt_out_y_re;
      out_x_im    <= nxt_out_x_im;
      out_y_im    <= nxt_out_y_im;
    end if;
  end process;

  nxt_toggle <= '0' when out_val = '0' else not toggle;

  gen_out_fft : if g_pft_mode = PFT_MODE_BITREV or g_pft_mode = PFT_MODE_COMPLEX generate
    nxt_out_fft_re <= 0 when out_val = '0' else to_integer(signed(out_re));
    nxt_out_fft_im <= 0 when out_val = '0' else to_integer(signed(out_im));
  end generate;

  gen_out_real2 : if g_pft_mode = PFT_MODE_REAL2 generate
    nxt_out_x_re <= 0 when out_val = '0' else to_integer(signed(out_re)) when toggle = '0' else out_x_re;
    nxt_out_y_re <= 0 when out_val = '0' else to_integer(signed(out_re)) when toggle = '1' else out_y_re;
    nxt_out_x_im <= 0 when out_val = '0' else to_integer(signed(out_im)) when toggle = '0' else out_x_im;
    nxt_out_y_im <= 0 when out_val = '0' else to_integer(signed(out_im)) when toggle = '1' else out_y_im;
  end generate;

end tb;
