###############################################################################
#
# Copyright (C) 2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Constrain the input I/O path
#set_input_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2c_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e2sg:u0|xcvr_fpll_a10_0|outclk0}] -max 3 [all_inputs]
#set_input_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2c_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e2sg:u0|xcvr_fpll_a10_0|outclk0}] -min 2 [all_inputs]
# Constrain the output I/O path
#set_output_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2c_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e2sg:u0|xcvr_fpll_a10_0|outclk0}] -max 3 [all_inputs]
#set_output_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2c_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e2sg:u0|xcvr_fpll_a10_0|outclk0}] -min 2 [all_inputs]


# False path the PPS to DDIO:
#set_input_delay  -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2c_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e2sg:u0|xcvr_fpll_a10_0|outclk0}] 3 [get_ports {PPS}]
#set_false_path -from {PPS} -to {ctrl_unb2c_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e2sg_ddio_in:\gen_ip_arria10_e2sg:u0|ip_arria10_e2sg_ddio_in_1:\gen_w:0:u_ip_arria10_e2sg_ddio_in_1|ip_arria10_e2sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio~ddio_in_fr}; set_false_path -from {PPS} -to {ctrl_unb2c_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e2sg_ddio_in:\gen_ip_arria10_e2sg:u0|ip_arria10_e2sg_ddio_in_1:\gen_w:0:u_ip_arria10_e2sg_ddio_in_1|ip_arria10_e2sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio~ddio_in_fr}


#set_false_path -from [get_ports {PPS}] -to [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2c_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e2sg:u0|xcvr_fpll_a10_0|outclk0}]

#set_input_delay -min -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2c_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e2sg:u0|xcvr_fpll_a10_0|outclk0}] 2 [get_ports {ctrl_unb2c_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|pps_ext_cap}]
#set_input_delay -max -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2c_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e2sg:u0|xcvr_fpll_a10_0|outclk0}] 4 [get_ports {ctrl_unb2c_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|pps_ext_cap}]

#set_false_path -from {PPS} -to {ctrl_unb2c_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e2sg_ddio_in:\gen_ip_arria10_e2sg:u0|ip_arria10_e2sg_ddio_in_1:\gen_w:0:u_ip_arria10_e2sg_ddio_in_1|ip_arria10_e2sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio*}



set_time_format -unit ns -decimal_places 3

create_clock -period 125Mhz [get_ports {ETH_CLK}]
create_clock -period 200Mhz [get_ports {CLK}]
create_clock -period 100Mhz [get_ports {CLKUSR}]
create_clock -period 644.53125Mhz [get_ports {SA_CLK}]
create_clock -period 644.53125Mhz [get_ports {SB_CLK}]
create_clock -period 200MHz -name {BCK_REF_CLK} { BCK_REF_CLK }

# Create altera reserved tck to solve unconstrained clock warning.
create_clock -period "100.000 ns" -name {altera_reserved_tck} {altera_reserved_tck}

derive_pll_clocks
derive_clock_uncertainty

set_clock_groups -asynchronous -group {CLK}
set_clock_groups -asynchronous -group {BCK_REF_CLK}
set_clock_groups -asynchronous -group {CLK_USR}
set_clock_groups -asynchronous -group {CLKUSR}
set_clock_groups -asynchronous -group {SA_CLK}
set_clock_groups -asynchronous -group {SB_CLK}
# Do not put ETH_CLK in this list, otherwise the Triple Speed Ethernet does not work

# Altera temp sense clock
set_clock_groups -asynchronous -group [get_clocks altera_ts_clk]

# ALtera JTAG clock
set_clock_groups -asynchronous -group [get_clocks altera_reserved_tck]

# IOPLL outputs (which have global names defined in the IP qsys settings)
set_clock_groups -asynchronous -group [get_clocks pll_clk20]
set_clock_groups -asynchronous -group [get_clocks pll_clk50]
set_clock_groups -asynchronous -group [get_clocks pll_clk100]
set_clock_groups -asynchronous -group [get_clocks pll_clk125]
set_clock_groups -asynchronous -group [get_clocks pll_clk200]
set_clock_groups -asynchronous -group [get_clocks pll_clk200p]
set_clock_groups -asynchronous -group [get_clocks pll_clk400]


# FPLL outputs
#set_clock_groups -asynchronous -group [get_clocks {*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*mac_clock*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*dp_clk*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*xcvr_fpll_a10_0|outclk1}]
set_clock_groups -asynchronous -group [get_clocks {*_board_clk125_pll|*xcvr_fpll_a10_0|outclk2}]
set_clock_groups -asynchronous -group [get_clocks {*_board_clk125_pll|*xcvr_fpll_a10_0|outclk3}]

set_clock_groups -asynchronous -group [get_clocks {*xcvr_native_a10_0|g_xcvr_native_insts[*]|rx_pma_clk}]

#set_false_path -from {*u_rst200|u_async|din_meta[2]} -to {*FIFOram*}

#set_clock_groups -asynchronous \
#-group [get_clocks {inst2|xcvr_4ch_native_phy_inst|xcvr_native_a10_0|g_xcvr_native_insts[?]|rx_pma_clk}] \
#-group [get_clocks {inst2|xcvr_pll_inst|xcvr_fpll_a10_0|tx_bonding_clocks[0]}]

# JESD

# The link_clk has clk contstraint of 100 MHz, but this seems not sufficient to
# guarantee proper cross clock domain data transfer between the link_clk and
# the 200 MHz frame_clk. Therefore use set_clock_uncertainty to enforce
# constraints on link_clk and frame_clk that are somewhat more than Fmax = 200
# MHz. To get a reasonable Fmax use Fmax reported for designs where the JESD
# did not show bit errors or sample shifts.
#
# For unb2c e.g. Fmax = 218 MHz for link_clk and Fmax = 244 MHz for frame_clk
# were achieved, so similar as for unb2b. Thererfore choose to use same Fmax
# values as for unb2b,
#
# In Timing Anayser -> Tasks Report Setup Summary -> Start -> Report timing for
# link_clk yields timing diagram. Paste set_clock_uncertainty constraint in
# cli -> Report: Regenerate, to update timing results.
#
# link_clk 100 MHz:
#      _________           ____
#     |         |         |
#     ^         v         ^
#  ___|         |_________|
#     0         5        10 ns
#             <-----------*  = 5.727 ns for both rise-rise and rise-fall
#     <------> = 10 - 5.727 = 4.263 ns --> 234 MHz
#
# frame_clk 200 MHz:
#      ____      ____
#     |    |    |
#     ^    v    ^
#  ___|    |____|
#     0   2.5   5 ns
#             <-*  = 0.496 ns for both rise-rise and rise-fall
#     <------> = 5 - 0.496 = 4.504 ns --> 222 MHz
#
# Idem use same set_clock_uncertainty for fall-fall and fall_rise. Although
# maybe only the rise-rise constraint for link_clk is already sufficient.

# Increase clock uncertainty to force link_clk to have Fmax > 234MHz
set_clock_uncertainty -rise_from [get_clocks {*|iopll_0|link_clk}] -rise_to [get_clocks {*|iopll_0|link_clk}]  5.727
set_clock_uncertainty -rise_from [get_clocks {*|iopll_0|link_clk}] -fall_to [get_clocks {*|iopll_0|link_clk}]  5.727
set_clock_uncertainty -fall_from [get_clocks {*|iopll_0|link_clk}] -rise_to [get_clocks {*|iopll_0|link_clk}]  5.727
set_clock_uncertainty -fall_from [get_clocks {*|iopll_0|link_clk}] -fall_to [get_clocks {*|iopll_0|link_clk}]  5.727

# Increase clock uncertainty to force frame_clk to have Fmax > 222MHz
set_clock_uncertainty -rise_from [get_clocks {*|iopll_0|frame_clk}] -rise_to [get_clocks {*|iopll_0|frame_clk}]  0.496
set_clock_uncertainty -rise_from [get_clocks {*|iopll_0|frame_clk}] -fall_to [get_clocks {*|iopll_0|frame_clk}]  0.496
set_clock_uncertainty -fall_from [get_clocks {*|iopll_0|frame_clk}] -rise_to [get_clocks {*|iopll_0|frame_clk}]  0.496
set_clock_uncertainty -fall_from [get_clocks {*|iopll_0|frame_clk}] -fall_to [get_clocks {*|iopll_0|frame_clk}]  0.496

# false paths added for the jesd interface as these clocks are independent.
#set_false_path -from [get_clocks {*xcvr_fpll_a10_0|outclk2}] -to [get_clocks {*iopll_0|link_clk}]
#set_false_path -from [get_clocks {*iopll_0|link_clk}] -to [get_clocks {*xcvr_fpll_a10_0|outclk2}]
#set_false_path -from [get_clocks {*xcvr_fpll_a10_0|outclk2}] -to [get_clocks {*iopll_0|frame_clk}]
#set_false_path -from [get_clocks {*iopll_0|frame_clk}] -to [get_clocks {*xcvr_fpll_a10_0|outclk2}]
