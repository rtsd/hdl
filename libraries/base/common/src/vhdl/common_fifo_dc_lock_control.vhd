-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Control the FIFO fill level and report the dual clock lock status
-- Description:
--   When the FIFO has been filled in state s_detect_wr_clk then wr_clk_rst goes
--   high, because then the input write clock to the FIFO is active. The
--   wr_clk_rst pulse is then issued to reset the phase of the FIFO wr_clk.
--   This assumes that the FIFO wr_clk comes from an external clock divider
--   that divides a higher rate sample clock.
--   The dual clock FIFO is held in reset by means of the g_hold_dc_fifo_rst
--   timeout in state s_reset_fifo until it is sure that the wr_clk has
--   restarted again. After that the c_fifo_latency timeout in state
--   s_init_fifo is used to ensure that the FIFO filling must have started
--   after that dc_fifo_rst was released.
--   When the dual clock FIFO has been filled again in state s_fill_fifo then
--   the wr_clk and the rd_clk are assumed to be in lock and the state becomes
--   s_fill_level. In state s_fill_level the c_fifo_latency timeout is used
--   to wait until the rd_usedw is stable. The rd_usedw still increments a few
--   because the FIFO has some latency between applying rd_req and rd_usedw
--   reacting on that in case rd_usedw is derived from wr_usedw directly. The
--   actual FIFO fill level is then registered in fill_level and used in the
--   state s_dc_locked.
--   If the dual clocks of the dual clock (dc) fifo are in lock in then the
--   rd_usedw will remain constant +-g_rd_fill_margin, because then the
--   FIFO wr_clk and FIFO rd_clk have the same rate but can have a different
--   phase. Default g_rd_fill_margin = 0.
--   If rd_usedw goes below fill_level-0 then the FIFO wr_clk has stopped or
--   is slower than the FIFO rd_clk. If the rd_usedw goes above fill_level+0
--   then the FIFO wr_clk is faster than the FIFO rd_clk. Both these cases
--   indicate that the wr_clk and rd_clk lost lock.
--   The +- g_rd_fill_margin can be used to cope with phase jitter on the
--   clocks, but typically it should be 0 for systems that have a stable phase
--   relation between the wr_clk and the rd_clk, also after each powerup.
--   The control tries to recover from the loss of lock between the wr_clk and
--   the rd_clk by resetting the FIFO and letting it fill again to the
--   g_rd_fill_level in the state s_detect_wr_clk.
--   The dual clock lock status is indicated by dc_locked, when '1' then the
---  wr_clk and rd_clk are currently in lock. Whether one or  multiple losses
--   of dc lock occured is reported via dc_stable. The dc_stable is '1' when
---  the dc_locked is '1' and has not gone low (i.e. no loss of lock occured)
--   since the last time that dc_stable was acknowledged via dc_stable_ack.
--   Hence by pulsing dc_stable_ack the user can start a new fresh period for
--   dc_stable.
-- Remarks:
-- . See tb_lvdsh_dd for a test bench that uses common_fifo_dc_lock_control
-- . Use the FIFO rd_clk clock domain to clock this common_fifo_dc_lock_control
-- . Best use a FIFO of at least size 16 and a fill level of 4, >= 3 to have
--   margin from empty at the read side and <= 8 (= 16 - 8 = 8), to have margin
--   from full at the write side.
-- . Increase g_rd_fill_level to have more time between wr_clk_rst going high
--   and dc_locked going high.
-- . The rd_fill_level can also be set via MM control to support different a
--   fill level per signal path, e.g. to compensate for cable length
--   differences between these signal paths in units of the rd_clk period.

entity common_fifo_dc_lock_control is
  generic (
    g_hold_wr_clk_rst  : natural := 2;  -- >= 1, nof cycles to hold the wr_clk_rst
    g_hold_dc_fifo_rst : natural := 7;  -- >= 1, nof cycles to hold the dc_fifo_rst, sufficiently long for wr_clk to have restarted after wr_clk_rst release
    g_rd_fill_level    : natural := 8;
    g_rd_fill_margin   : natural := 1
  );
  port (
    -- FIFO rd_clk domain
    rd_rst        : in  std_logic;  -- connect to FIFO rd_rst
    rd_clk        : in  std_logic;  -- connect to FIFO rd_clk
    rd_usedw      : in  std_logic_vector;
    rd_req        : out std_logic;
    wr_clk_rst    : out std_logic;
    dc_fifo_rst   : out std_logic;

    -- MM in rd_clk domain
    rd_fill_level : in  natural := g_rd_fill_level;
    dc_locked     : out std_logic;
    dc_stable     : out std_logic;
    dc_stable_ack : in  std_logic
  );
end common_fifo_dc_lock_control;

architecture rtl of common_fifo_dc_lock_control is
  constant c_fifo_latency      : natural := 10;  -- large enough to ensure that the FIFO filling has started, but small enough such that the FIFO is not filled yet
  constant c_fill_level_max    : natural := 2**rd_usedw'length - 1;

  constant c_cnt_arr           : t_natural_arr := (g_hold_wr_clk_rst, g_hold_dc_fifo_rst, c_fifo_latency);  -- array to hold all timeouts
  constant c_cnt_max           : natural := largest(c_cnt_arr);  -- largest of all timeouts
  constant c_cnt_w             : natural := ceil_log2(c_cnt_max + 1);

  type t_state is (s_detect_wr_clk, s_restart_wr_clk, s_reset_fifo, s_init_fifo, s_fill_fifo, s_fill_level, s_dc_locked, s_dc_lost);

  signal state                : t_state;
  signal nxt_state            : t_state;

  signal prev_rd_usedw        : std_logic_vector(rd_usedw'range);

  signal nxt_wr_clk_rst       : std_logic;
  signal nxt_dc_fifo_rst      : std_logic;

  signal cnt_clr              : std_logic;
  signal cnt_en               : std_logic;
  signal cnt                  : std_logic_vector(c_cnt_w - 1 downto 0);

  signal i_dc_locked          : std_logic;
  signal nxt_dc_locked        : std_logic;

  signal fill_level           : natural range 0 to c_fill_level_max;
  signal nxt_fill_level       : natural;
begin
  dc_locked <= i_dc_locked;

  p_rd_clk: process(rd_clk, rd_rst)
  begin
    if rd_rst = '1' then
      state         <= s_detect_wr_clk;
      prev_rd_usedw <= (others => '0');
      wr_clk_rst    <= '0';
      dc_fifo_rst   <= '1';
      i_dc_locked   <= '0';
      fill_level    <= 0;
    elsif rising_edge(rd_clk) then
      state         <= nxt_state;
      prev_rd_usedw <= rd_usedw;
      wr_clk_rst    <= nxt_wr_clk_rst;
      dc_fifo_rst   <= nxt_dc_fifo_rst;
      i_dc_locked   <= nxt_dc_locked;
      fill_level    <= nxt_fill_level;
    end if;
  end process;

  p_state : process(state, prev_rd_usedw, rd_usedw, rd_fill_level, fill_level, cnt)
  begin
    rd_req          <= '0';
    cnt_clr         <= '0';
    cnt_en          <= '0';
    nxt_wr_clk_rst  <= '0';
    nxt_dc_fifo_rst <= '0';
    nxt_dc_locked   <= '0';
    nxt_fill_level  <= fill_level;
    nxt_state       <= state;

    case state is
      when s_detect_wr_clk =>
        if unsigned(rd_usedw) >= rd_fill_level - 1 then
          -- the FIFO has filled, so there is activity on the wr_clk
          -- assume the wr_clk is reliable, if it is not then that will be detected and recovered from in state s_fill_fifo
          cnt_clr <= '1';
          nxt_state <= s_restart_wr_clk;
        end if;
      when s_restart_wr_clk =>
        cnt_en <= '1';
        if unsigned(cnt) < g_hold_wr_clk_rst then
          nxt_wr_clk_rst <= '1';  -- reset and restart the external write clock divider by asserting wr_clk_rst for g_hold_wr_clk_rst cycles of the rd_clk
        else
          cnt_clr <= '1';
          nxt_state <= s_reset_fifo;
        end if;
      when s_reset_fifo =>
        cnt_en <= '1';
        if unsigned(cnt) < g_hold_dc_fifo_rst then
          nxt_dc_fifo_rst <= '1';  -- reset the input FIFO until the wr_clk has been able to restart for sure
        else
          cnt_clr <= '1';
          nxt_state <= s_init_fifo;
        end if;
      when s_init_fifo =>
        cnt_en <= '1';
        if unsigned(cnt) > c_fifo_latency then
          -- reset release latency : the FIFO should have started filling by now
          nxt_state <= s_fill_fifo;
        end if;
      when s_fill_fifo =>
        if unsigned(rd_usedw) = unsigned(prev_rd_usedw) + 1 then
          -- the FIFO filling properly at every rd_clk cycle
          if unsigned(rd_usedw) >= rd_fill_level - 1 then
            rd_req <= '1';
            cnt_clr <= '1';
            nxt_state <= s_fill_level;
          end if;
        else
          -- the FIFO filling went too slow or too fast so the potential lock is lost
          nxt_state <= s_dc_lost;
        end if;
      when s_fill_level =>
        rd_req <= '1';
        cnt_en <= '1';
        if unsigned(cnt) > c_fifo_latency then
          -- synchronizer chain latency : the FIFO fill level on the write side should be stable by now
          nxt_fill_level <= TO_UINT(rd_usedw);
          nxt_state <= s_dc_locked;
        end if;
      when s_dc_locked =>
        rd_req <= '1';
        nxt_dc_locked <= '1';
        if unsigned(rd_usedw) < fill_level - g_rd_fill_margin or unsigned(rd_usedw) > fill_level + g_rd_fill_margin then
          -- the FIFO fill level changed (too much) so the lock is lost
          nxt_state <= s_dc_lost;
        end if;
      when others =>  -- s_dc_lost
        nxt_dc_fifo_rst <= '1';  -- reset the input FIFO to reset rd_usedw
        nxt_state <= s_detect_wr_clk;
    end case;
  end process;

  u_cnt : entity common_lib.common_counter
  generic map (
    g_width  => c_cnt_w
  )
  port map (
    rst     => rd_rst,
    clk     => rd_clk,
    cnt_clr => cnt_clr,
    cnt_en  => cnt_en,
    count   => cnt
  );

  u_dc_locked_monitor : entity work.common_stable_monitor
  port map (
    rst          => rd_rst,
    clk          => rd_clk,
    -- MM
    r_in         => i_dc_locked,
    r_stable     => dc_stable,
    r_stable_ack => dc_stable_ack
  );
end rtl;
