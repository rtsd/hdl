-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author : E. Kooistra, 13 Apr 2017
-- Purpose : Throttle snk_in.xon to control the input block rate
-- Description:
--   The XON='1' time is determined by g_nof_block_on*g_block_size, so the
--   number of active snk_in.valid input. The XON='0' cannot be derived from
--   the snk_in because it is off. Therefore the off time is counted in clock
--   cycles. Typically choose g_nof_clk_off = g_block_size * 'nof block off'
--   * (1/'data valid duty_cycle').

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_throttle_xon is
  generic (
    g_restart_at_sync : boolean := false;  -- if true restart xon control at each snk_in_sync, else and start at first valid and never restart
    g_block_size      : natural := 100;  -- number of valid data per block marked by sop and eop
    g_nof_block_on    : natural := 10;  -- number of blocks that snk_out.xon is active
    g_nof_clk_off     : natural := 10  -- number of clock cycles that snk_out.xon is kept inactive
  );
  port (
    rst      : in  std_logic;
    clk      : in  std_logic;
    -- ST sinks
    snk_out  : out t_dp_siso;
    snk_in   : in  t_dp_sosi := c_dp_sosi_rst;
    -- ST source
    src_in   : in  t_dp_siso := c_dp_siso_rdy;
    src_out  : out t_dp_sosi
  );
end dp_throttle_xon;

architecture rtl of dp_throttle_xon is
  constant c_nof_block_on : natural := g_nof_block_on - 1;  -- adjust by -1 to account for that first block already appears at cnt = 0
  constant c_nof_valid_on : natural := c_nof_block_on * g_block_size;

  signal cnt     : natural range 0 to largest(g_block_size * c_nof_block_on, g_nof_clk_off);
  signal nxt_cnt : natural;
  signal xon     : std_logic;
  signal nxt_xon : std_logic;
begin
  snk_out.ready <= src_in.ready;  -- pass on ready
  snk_out.xon   <= src_in.xon and xon;  -- control xon

  src_out <= snk_in;  -- pass on sosi

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      cnt <= 0;
      xon <= '1';
    elsif rising_edge(clk) then
      cnt <= nxt_cnt;
      xon <= nxt_xon;
    end if;
  end process;

  p_comb : process(snk_in, cnt, xon)
  begin
    -- Default
    nxt_cnt <= cnt;
    nxt_xon <= xon;

    -- XON/XOFF control
    if xon = '1' then
      -- XON time
      if snk_in.valid = '1' then
        if cnt < c_nof_valid_on - 1 then
          nxt_cnt <= cnt + 1;
        else
          nxt_cnt <= 0;
          nxt_xon <= '0';
        end if;
      end if;
    else
      -- XOFF time
      if cnt < g_nof_clk_off - 1 then
        nxt_cnt <= cnt + 1;
      else
        nxt_cnt <= 0;
        nxt_xon <= '1';
      end if;
    end if;

    -- Overrule if restart at sync
    if g_restart_at_sync = true and snk_in.sync = '1' then
      nxt_cnt <= 1;
      nxt_xon <= '1';
    end if;

    -- Overrule if g_nof_clk_off=0
    if g_nof_clk_off = 0 then
      nxt_cnt <= 1;
      nxt_xon <= '1';
    end if;
  end process;
end rtl;
