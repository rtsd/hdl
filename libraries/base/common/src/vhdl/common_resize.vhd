-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common_pkg.all;

entity common_resize is
  generic (
    g_representation  : string  := "SIGNED";  -- SIGNED or UNSIGNED resizing
    g_clip            : boolean := false;  -- when TRUE clip input if it is outside the output range, else wrap
    g_clip_symmetric  : boolean := false;  -- when TRUE clip signed symmetric to +c_smax and -c_smax, else to +c_smax and c_smin_symm
                                              -- for wrapping when g_clip=FALSE the g_clip_symmetric is ignored, so signed wrapping is done asymmetric
    g_pipeline_input  : natural := 0;  -- >= 0
    g_pipeline_output : natural := 1;  -- >= 0
    g_in_dat_w        : integer := 36;
    g_out_dat_w       : integer := 18
  );
  port (
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_dat     : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    out_dat    : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_ovr    : out std_logic
  );
end;

architecture rtl of common_resize is
  -- Clipping is only necessary when g_out_dat_w<g_in_dat_w.
  constant c_clip      : boolean := g_clip and (g_out_dat_w < g_in_dat_w);

  -- Use SIGNED, UNSIGNED to avoid NATURAL (32 bit range) overflow error
  constant c_umax      : unsigned(out_dat'range) := unsigned(      c_slv1(g_out_dat_w - 1 downto 0));  -- =  2** g_out_dat_w   -1
  constant c_smax      :   signed(out_dat'range) :=   signed('0' & c_slv1(g_out_dat_w - 2 downto 0));  -- =  2**(g_out_dat_w-1)-1
  constant c_smin_most :   signed(out_dat'range) :=   signed('1' & c_slv0(g_out_dat_w - 2 downto 0));  -- = -2**(c_in_dat_w-1)
  constant c_smin_symm :   signed(out_dat'range) := -c_smax;  -- = -2**(c_in_dat_w-1)+1
  constant c_smin      :   signed(out_dat'range) := sel_a_b(g_clip_symmetric, c_smin_symm, c_smin_most);

  signal reg_dat     : std_logic_vector(in_dat'range);
  signal wrap        : std_logic;
  signal clip        : std_logic;
  signal sign        : std_logic;
  signal res_ovr     : std_logic;
  signal res_dat     : std_logic_vector(out_dat'range);
  signal res_vec     : std_logic_vector(g_out_dat_w downto 0);
  signal out_vec     : std_logic_vector(g_out_dat_w downto 0);
begin
  u_input_pipe : entity work.common_pipeline  -- pipeline input
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => g_pipeline_input,
    g_in_dat_w       => g_in_dat_w,
    g_out_dat_w      => g_in_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => in_dat,
    out_dat => reg_dat
  );

  no_clip : if c_clip = false generate
    -- Note that g_pipeline_input=0 AND g_clip=FALSE is equivalent to using RESIZE_SVEC or RESIZE_UVEC directly.
    gen_s : if g_representation = "SIGNED" generate
      -- If g_out_dat_w>g_in_dat_w then IEEE resize extends the sign bit,
      -- else IEEE resize preserves the sign bit and keeps the low part.
      wrap <= '1' when signed(reg_dat) > c_smax or signed(reg_dat) < c_smin_most else '0';
      res_dat <= RESIZE_SVEC(reg_dat, g_out_dat_w);
      res_ovr <= wrap;
    end generate;

    gen_u : if g_representation = "UNSIGNED" generate
      -- If g_out_dat_w>g_in_dat_w then IEEE resize sign extends with '0',
      -- else IEEE resize keeps the low part.
      wrap <= '1' when unsigned(reg_dat) > c_umax else '0';
      res_dat <= RESIZE_UVEC(reg_dat, g_out_dat_w);
      res_ovr <= wrap;
    end generate;
  end generate;

  gen_clip : if c_clip = true generate
    gen_s_clip : if g_representation = "SIGNED" generate
      clip <= '1' when signed(reg_dat) > c_smax or signed(reg_dat) < c_smin else '0';
      sign <= reg_dat(reg_dat'high);
      res_dat <= reg_dat(out_dat'range) when clip = '0' else std_logic_vector( c_smax) when sign = '0' else std_logic_vector(c_smin);
      res_ovr <= clip;
    end generate;

    gen_u_clip : if g_representation = "UNSIGNED" generate
      clip <= '1' when unsigned(reg_dat) > c_umax else '0';
      res_dat <= reg_dat(out_dat'range) when clip = '0' else std_logic_vector(c_umax);
      res_ovr <= clip;
    end generate;
  end generate;

  res_vec <= res_ovr & res_dat;

  u_output_pipe : entity work.common_pipeline  -- pipeline output
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => g_pipeline_output,
    g_in_dat_w       => g_out_dat_w + 1,
    g_out_dat_w      => g_out_dat_w + 1
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => res_vec,
    out_dat => out_vec
  );

  out_ovr <= out_vec(g_out_dat_w);
  out_dat <= out_vec(g_out_dat_w - 1 downto 0);
end rtl;
