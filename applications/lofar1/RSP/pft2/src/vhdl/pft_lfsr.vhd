-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: pipelined FFT
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE;
use IEEE.std_logic_1164.all;

entity pft_lfsr is
  port (
    in_en          : in  std_logic;
    out_bit1       : out std_logic;
    out_bit2       : out std_logic;
    clk            : in  std_logic;
    rst            : in  std_logic
  );
end pft_lfsr;

architecture rtl of pft_lfsr is
  -- uses preferred pair of pritive trinomials
  -- x^41 + x^20 + 1  and x^41 + x^3 + 1
  -- see XAPP217

  constant c_max : natural := 41;
  constant c1    : natural := 20;
  constant c2    : natural := 3;

  signal s1      : std_logic_vector(c_max - 1 downto 0);
  signal nxt_s1  : std_logic_vector(c_max - 1 downto 0);

  signal s2      : std_logic_vector(c_max - 1 downto 0);
  signal nxt_s2  : std_logic_vector(c_max - 1 downto 0);
begin
  regs: process(rst, clk)
  begin
    if rst = '1' then
      s1 <= "01000101011101110101001011111000101100001";
      s2 <= "11011001000101001011011001110101100101100";
    elsif rising_edge(clk) then
      s1 <= nxt_s1;
      s2 <= nxt_s2;
    end if;
  end process;

  out_bit1 <= s1(s1'high);
  out_bit2 <= s2(s2'high);

  seed_proc: process(in_en, s1, s2)
  begin
    nxt_s1 <= s1;
    nxt_s2 <= s2;
    if in_en = '1' then
      -- shift
      nxt_s1(c_max - 1 downto 1) <= s1(c_max - 2 downto 0);
      nxt_s2(c_max - 1 downto 1) <= s2(c_max - 2 downto 0);

      -- feedback 1
      nxt_s1(0) <= s1(c_max - 1);
      nxt_s2(0) <= s2(c_max - 1);

      -- feedback 2
      nxt_s1(c1) <= s1(c_max - 1) xor s1(c1 - 1);
      nxt_s2(c2) <= s2(c_max - 1) xor s2(c2 - 1);
    end if;
  end process;
end rtl;
