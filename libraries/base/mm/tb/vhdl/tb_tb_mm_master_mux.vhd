-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Multi test bench for mm_master_mux.vhd
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity tb_tb_mm_master_mux is
end tb_tb_mm_master_mux;

architecture tb of tb_tb_mm_master_mux is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Usage:
  -- > as 4
  -- > run -all

  -- g_nof_masters             : POSITIVE := 2;   -- Number of master memory interfaces on the MM bus array.
  -- g_base_arr                : t_nat_natural_arr := (0, 256);  -- Address base per slave port of mm_bus
  -- g_width_arr               : t_nat_natural_arr := (4,   8);  -- Address width per slave port of mm_bus
  -- g_waitrequest             : BOOLEAN := FALSE;    -- When TRUE model waitrequest by the MM RAM slave, else fixed '0'
  -- g_pipeline_bus_mosi       : BOOLEAN := FALSE;
  -- g_pipeline_bus_miso_rdval : BOOLEAN := FALSE;
  -- g_pipeline_bus_miso_wait  : BOOLEAN := FALSE

  u_no_pipe                          : entity work.tb_mm_master_mux generic map (2, (0, 256), (4,   8), false, false, false, false);
  u_pipe_mosi                        : entity work.tb_mm_master_mux generic map (2, (0, 256), (4,   8), false,  true, false, false);
  u_pipe_miso_rdval                  : entity work.tb_mm_master_mux generic map (2, (0, 256), (4,   8), false, false,  true, false);
  u_waitrequest_no_pipe              : entity work.tb_mm_master_mux generic map (2, (0, 256), (4,   8),  true, false, false, false);
  u_waitrequest_pipe_miso_rdval      : entity work.tb_mm_master_mux generic map (2, (0, 256), (4,   8),  true, false,  true, false);
  u_waitrequest_pipe_mosi            : entity work.tb_mm_master_mux generic map (2, (0, 256), (4,   8),  true,  true, false, false);
  u_waitrequest_pipe_mosi_miso_rdval : entity work.tb_mm_master_mux generic map (2, (0, 256), (4,   8),  true,  true,  true, false);

  -- Do not support simultaneous g_pipeline_mosi = TRUE and g_pipeline_miso_wait = TRUE, see mm_bus_pipe.vhd.
  --u_waitrequest_pipe_all             : ENTITY work.tb_mm_master_mux GENERIC MAP (2, (0, 256), (4,   8),  TRUE,  TRUE,  TRUE, TRUE);
end tb;
