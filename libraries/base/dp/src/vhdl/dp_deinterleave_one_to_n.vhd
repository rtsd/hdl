--------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Author:
-- . Eric Kooistra
-- Purpose:
-- . Deinterleave the input into g_nof_outputs parallel outputs, with one input
--   sample per output and all outputs valid at the same cycle.
-- Description:
-- . The snk_in blocks size should typically be an integer multiple of
--   g_nof_outputs. If the snk_in block size is an integer multiple of
--   g_nof_outputs, then the snk_in.eop occurs when cnt = g_nof_outputs-1, else
--   the snk_in.eop will break the deinterleaving and output immediately to
--   maintain output blocks with sop and eop.
-- . The snk_in.sop info (bsn, channel) and snk_in.eop info (empty, err) are
--   preserved and passed on to the src_out_arr outputs.
-- . The implementation supports siso flow control, thanks to that the
--   function is implemented combinatorially (so RL=0) using internal state
--   registers. The function only outputs when thereis valid input, therefore
--   it does not need to use src_in_arr().ready. The combinatorial output is
--   pipelined to ease timing closure.
-- Remark:
-- . This function is similar to dp_deinterleave by only supports m=1 to
--   g_nof_outputs and therefore the implementation is simpler.

entity dp_deinterleave_one_to_n is
  generic (
    g_pipeline      : natural := 1;  -- 0 for combinatorial, > 0 for registers
    g_nof_outputs   : natural
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_out     : out t_dp_siso;
    snk_in      : in  t_dp_sosi;
    src_in_arr  : in  t_dp_siso_arr(g_nof_outputs - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr : out t_dp_sosi_arr(g_nof_outputs - 1 downto 0)
  );
end dp_deinterleave_one_to_n;

architecture rtl of dp_deinterleave_one_to_n is
  -- local state registers
  type t_reg is record
    cnt         : natural range 0 to g_nof_outputs - 1;
    input_arr   : t_dp_sosi_arr(g_nof_outputs - 1 downto 0);
  end record;

  signal s            : t_reg;  -- combinatoral state
  signal r            : t_reg;  -- register state

  signal out_siso_arr : t_dp_siso_arr(g_nof_outputs - 1 downto 0);
  signal out_sosi_arr : t_dp_sosi_arr(g_nof_outputs - 1 downto 0);
begin
  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      r.cnt       <= 0;
      r.input_arr <= (others => c_dp_sosi_rst);
    elsif rising_edge(clk) then
      r <= s;
    end if;
  end process;

  -- Count every g_nof_outputs valid input samples in snk_in, or restart at sop
  p_cnt : process(snk_in, r)
  begin
    s.cnt <= r.cnt;
    if snk_in.sop = '1' then
      s.cnt <= 0;  -- wrap or force restart at sop
    elsif snk_in.valid = '1' then
      if r.cnt < g_nof_outputs - 1 then
        s.cnt <= r.cnt + 1;
      else
        s.cnt <= 0;  -- wrap after every g_nof_outputs valid input samples
      end if;
    end if;
  end process;

  -- Load in series of g_nof_outputs valid snk_in
  -- Alternative solution is to shift in snk_in to avoid selection logic based
  -- on cnt. However then possibly selection logic based on cnt is needed to
  -- know where the sop info is in case the deinterleaving needs to break on
  -- eop.
  p_input_arr : process(snk_in, s, r)
  begin
    s.input_arr <= r.input_arr;
    if snk_in.valid = '1'THEN
      s.input_arr(s.cnt) <= snk_in;
    end if;
  end process;

  -- Output s.input_arr to parallel out_sosi_arr including all sosi data fields
  p_out_sosi_arr : process(snk_in, s)
  begin
    -- Default pass on data
    out_sosi_arr <= s.input_arr;
    for I in 0 to g_nof_outputs - 1 loop
      out_sosi_arr(I).valid <= '0';
      out_sosi_arr(I).sync  <= '0';
      out_sosi_arr(I).sop   <= '0';
      out_sosi_arr(I).eop   <= '0';
    end loop;

    -- Output at valid and end of cnt or break at eop
    if snk_in.valid = '1' then
      if s.cnt = g_nof_outputs - 1 or snk_in.eop = '1'THEN
        for I in 0 to g_nof_outputs - 1 loop
          out_sosi_arr(I).valid <= '1';
          -- Use sop info from cnt=0
          out_sosi_arr(I).sync    <= s.input_arr(0).sync;
          out_sosi_arr(I).sop     <= s.input_arr(0).sop;
          out_sosi_arr(I).bsn     <= s.input_arr(0).bsn;
          out_sosi_arr(I).channel <= s.input_arr(0).channel;
          -- Use eop info directly from snk_in
          out_sosi_arr(I).eop     <= snk_in.eop;
          out_sosi_arr(I).empty   <= snk_in.empty;
          out_sosi_arr(I).err     <= snk_in.err;
        end loop;
      end if;
    end if;
  end process;

  -- Register and pass on flow control
  snk_out <= out_siso_arr(0);  -- all out_siso_arr have the same siso, so wire output 0

  u_pipeline_outputs : entity work.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_outputs,
    g_pipeline    => g_pipeline  -- 0 for wires, > 0 for registers
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out_arr  => out_siso_arr,
    snk_in_arr   => out_sosi_arr,
    -- ST source
    src_in_arr   => src_in_arr,
    src_out_arr  => src_out_arr
  );
end rtl;
