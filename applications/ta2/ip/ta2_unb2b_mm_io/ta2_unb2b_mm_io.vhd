-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Provide Monitor and control I/O interface for OpenCL kernel on Arria10
-- Description:
-- . This core consists of two dual clock fifos and glue logic to be used between
--   the OpenCL kernel IO channel and dp MM interface to board qsys.
-- . After a MM write request the waitrequest is immediatly pulled low due to the
--   fifo being ready.
-- . After a MM read request the waitrequest is kept high until valid data has
--   been received back from the OpenCL kernel through the dual clock fifo. Due
--   to the latency of both dual clock fifos, a read request takes at least 14
--   mm clock cycles (excluding OpenCL kernel process time).
-- . Details:
--   . This core was developed for use on the Uniboard2b.
--   . The implementation of the MM data mapped onto the IO channel is shown below.
--
--   MOSI -> IO channel (72 bits)
--   +-----------+---------+----------------------------------+
--   | Bit range | Name    | Description                      |
--   +-----------+---------+----------------------------------+
--   | [0:31]    | wrdata  | Write data                       |
--   +-----------+---------+----------------------------------+
--   | [32:63]   | address | Address                          |
--   +-----------+---------+----------------------------------+
--   | [64]      | rd/wr   | '1' = wr, '0' = rd               |
--   +-----------+---------+----------------------------------+
--   | [65:71]   | -       | reserved bits                    |
--   +-----------+---------+----------------------------------+
--
--   IO channel (32) bits -> MISO
--   +-----------+---------+----------------------------------+
--   | Bit range | Name    | Description                      |
--   +-----------+---------+----------------------------------+
--   | [0:31]    | rddata  | Read data                        |
--   +-----------+---------+----------------------------------+
--
--   . The generic: g_use_opencl can be used for testing perposes. When g_use_opencl
--     is FALSE, the core wil use an internal rtl model that emulates an OpenCL kernel.
--     This model has 4 register on addresses 0:3. On addr 1, the rddata = wrdata +1 and
--     on addr 2, the rddata = wrdata +2, to make them distinguishable. The others
--     registers are rddata = wrdata.
-- --------------------------------------------------------------------------
library IEEE, common_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity ta2_unb2b_mm_io is
  generic (
    g_use_opencl : boolean := true
  );
  port (
    mm_clk        : in  std_logic;
    mm_rst        : in  std_logic;

    kernel_clk    : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset  : in  std_logic;

    -- MM registers
    mm_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    mm_miso       : out t_mem_miso;

    -- DP
    snk_in        : in  t_dp_sosi;
    snk_out       : out t_dp_siso;

    src_out       : out t_dp_sosi;
    src_in        : in  t_dp_siso
  );
end ta2_unb2b_mm_io;

architecture str of ta2_unb2b_mm_io is
  constant c_fifo_size : natural := 8;
  constant c_wr_data_w : natural := 72;
  constant c_rd_data_w : natural := 32;

  signal wr_usedw : std_logic_vector(ceil_log2(c_fifo_size) - 1 downto 0);
  signal rd_usedw : std_logic_vector(ceil_log2(c_fifo_size) - 1 downto 0);
  signal wr_sosi  : t_dp_sosi := c_dp_sosi_rst;
  signal wr_siso  : t_dp_siso := c_dp_siso_rdy;
  signal rd_sosi  : t_dp_sosi := c_dp_sosi_rst;
  signal rd_siso  : t_dp_siso := c_dp_siso_rdy;

  signal busy : std_logic := '0';
  signal done : std_logic := '0';

  signal is_reading : boolean := false;
  signal cnt : natural := 0;
  signal c_cnt_max : natural := 3;

  signal in_sosi  : t_dp_sosi := c_dp_sosi_rst;
  signal in_siso  : t_dp_siso := c_dp_siso_rst;
  signal out_sosi : t_dp_sosi := c_dp_sosi_rst;
  signal out_siso : t_dp_siso := c_dp_siso_rst;

  signal reg_a : std_logic_vector(31 downto 0) := (others => '0');
  signal reg_b : std_logic_vector(31 downto 0) := (others => '0');
  signal reg_c : std_logic_vector(31 downto 0) := (others => '0');
  signal reg_d : std_logic_vector(31 downto 0) := (others => '0');
begin
  -- Connect MM <-> DP
  wr_sosi.data(31 downto 0) <= mm_mosi.wrdata(31 downto 0);
  mm_miso.rddata(31 downto 0) <= rd_sosi.data(31 downto 0);

  wr_sosi.data(63 downto 32) <= mm_mosi.address(31 downto 0);
  wr_sosi.data(64) <= mm_mosi.wr;
  done <= wr_siso.ready when mm_mosi.wr = '1' else rd_sosi.valid;
  mm_miso.waitrequest <= not done;
  wr_sosi.valid <= (mm_mosi.wr or mm_mosi.rd) when busy = '0' else '0';

  p_valid : process(mm_clk)
  begin
    if (rising_edge(mm_clk)) then
      if (mm_mosi.wr or mm_mosi.rd) = '1' then
        busy <= '1';
      end if;
      if done = '1' then
        busy <= '0';
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- dual clock FIFOs
  -----------------------------------------------------------------------------
    u_dp_fifo_dc_wr : entity dp_lib.dp_fifo_dc
    generic map (
      g_technology  => c_tech_arria10_e1sg,
      g_data_w      => c_wr_data_w,
      g_use_ctrl    => false,  -- No sop & eop
      g_fifo_size   => c_fifo_size,
      g_fifo_rl     => 0
    )
    port map (
      wr_rst      => mm_rst,
      wr_clk      => mm_clk,
      rd_rst      => kernel_reset,
      rd_clk      => kernel_clk,

      wr_usedw    => wr_usedw,

      snk_in      => wr_sosi,
      snk_out     => wr_siso,

      src_in      => out_siso,
      src_out     => out_sosi
    );

    u_dp_fifo_dc_rd : entity dp_lib.dp_fifo_dc
    generic map (
      g_technology  => c_tech_arria10_e1sg,
      g_data_w      => c_rd_data_w,
      g_use_ctrl    => false,  -- No sop & eop
      g_fifo_size   => c_fifo_size,
      g_fifo_rl     => 0
    )
    port map (
      wr_rst      => kernel_reset,
      wr_clk      => kernel_clk,
      rd_rst      => mm_rst,
      rd_clk      => mm_clk,

      wr_usedw    => rd_usedw,

      snk_in      => in_sosi,
      snk_out     => in_siso,

      src_in      => rd_siso,
      src_out     => rd_sosi
    );

gen_no_opencl : if not g_use_opencl generate
  -- simulate an OpenCL kernel response (rl=0)
  p_is_reading : process(kernel_clk)
  begin
    if rising_edge(kernel_clk) then
      if cnt >= c_cnt_max then
        cnt <= 0;
        is_reading <= false;
      else
        cnt <= cnt + 1;
      end if;
      if in_sosi.valid = '1' then
        is_reading <= true;
        cnt <= 0;
      end if;
    end if;
  end process;

  p_stim_st : process(out_sosi, in_siso, is_reading)
  begin
    in_sosi.valid <= '0';
    if out_sosi.valid = '1' then
      if out_sosi.data(64) = '1' then  -- Write request
        if TO_UINT(out_sosi.data(63 downto 56)) = 0 then
          reg_a <= out_sosi.data(31 downto 0);
        elsif TO_UINT(out_sosi.data(63 downto 56)) = 1 then
          reg_b <= TO_UVEC(TO_UINT(out_sosi.data(31 downto 24)) + 1, 8) & out_sosi.data(23 downto 0);  -- wrdata +1 to make distinguishable
        elsif TO_UINT(out_sosi.data(63 downto 56)) = 2 then
          reg_c <= out_sosi.data(31 downto 0);
        elsif TO_UINT(out_sosi.data(63 downto 56)) = 3 then
          reg_d <= TO_UVEC(TO_UINT(out_sosi.data(31 downto 24)) + 2, 8) & out_sosi.data(23 downto 0);  -- wrdata +2 to make distinguishable
        end if;
        out_siso.ready <= '1';
      else  -- read request
        if not is_reading then
          out_siso.ready <= '1';
          in_sosi.valid <= '1';
          if TO_UINT(out_sosi.data(63 downto 56)) = 0 then
            in_sosi.data(31 downto 0) <= reg_a;
          elsif TO_UINT(out_sosi.data(63 downto 56)) = 1 then
            in_sosi.data(31 downto 0) <= reg_b;
          elsif TO_UINT(out_sosi.data(63 downto 56)) = 2 then
            in_sosi.data(31 downto 0) <= reg_c;
          elsif TO_UINT(out_sosi.data(63 downto 56)) = 3 then
            in_sosi.data(31 downto 0) <= reg_d;
          else
            in_sosi.data(31 downto 0) <= (others => '0');
          end if;
        end if;
      end if;
    end if;
  end process;

  src_out <= c_dp_sosi_rst;
  snk_out <= c_dp_siso_rdy;
end generate;

gen_opencl : if g_use_opencl generate
  src_out <= out_sosi;
  out_siso <= src_in;
  snk_out <= in_siso;
  in_sosi <= snk_in;
end generate;
end str;
