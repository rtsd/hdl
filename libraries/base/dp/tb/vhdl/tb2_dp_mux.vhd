-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify multiple levels of DP mux using random stimuli
-- Description:
-- . The difference with tb_dp_mux is that this tb2 uses two levels of mux and
--   of demux and that it uses the random stimuli approach for verifing DP DUTs
-- . The two levels of mux and demux from a subset of the tb_uth_dp_packet.vhd
--   test bench that shows that the multiplexed stream can be passed on via
--   a (gigabit) serial link.
-- . Data flow:
--     in_sosi_2arr(I)(J)--> MUX--> mux_sosi_arr(I)
--                           MUX--> mux_sosi
--                           DEMUX--> demux_sosi_arr(I)
--                           DEMUX--> out_sosi_2arr(I)(J)
--   Backpressure:                <-- out_sosi_2arr(I)(J) <-- out_siso
--
-- . Verify options (applied in tb_tb2_dp_mux):
--     dp_mux  dp_demux
--     g_mode  g_mode    g_combined_demux
--     0       0         T or F
--     1       0         T or F
--   and fixed because each input has independent stimuli that get verified at
--   the corresponding output:
--     c_mode_demux     = 0
--     c_use_channel_lo = TRUE
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-- . The verify procedures check the correct output
-- . Observe out_* signals in Wave Window

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb2_dp_mux is
  generic (
    g_in_en          : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_out_ready      : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_mode_mux       : natural := 0;  -- Use 0 or 1
    g_mux_use_fifo   : boolean := true;
    g_mux_fifo_fill  : natural := 10;
    g_combined_demux : boolean := false;  -- FALSE, can also use TRUE because the same tb out_siso is used for all outputs
    g_nof_repeat     : natural := 10  -- for constant active stream control using 1 is sufficient, use > 1 to verify longer with random stimuli
  );
end tb2_dp_mux;

architecture tb of tb2_dp_mux is
  constant c_mode_demux                : natural := 0;
  constant c_use_channel_lo            : boolean := true;
  constant c_use_bsn                   : boolean := true;
  constant c_use_sync                  : boolean := true;

  constant c_data_w                    : natural := 16;
  constant c_rl                        : natural := 1;
  constant c_data_init                 : integer := 0;
  constant c_pulse_active              : natural := 1;
  constant c_pulse_period              : natural := 7;
  constant c_sync_period               : natural := 7;
  constant c_sync_offset               : natural := 2;

  constant c_nof_type_max              : natural := 5;
  constant c_nof_type                  : natural := 5;  -- Second multiplexing level
  constant c_nof_input                 : natural := 3;  -- Input multiplexing level
  constant c_nof_streams               : natural := c_nof_type * c_nof_input;
  constant c_len_arr                   : t_natural_arr(0 to c_nof_type_max - 1) := (3, 1, 1, 50, 7);

  -- c_in_nof_repeat = g_nof_repeat when g_mux_fifo_fill=0 else push in some more frames to account for small frames that remain in the FIFO
  constant c_len_min                   : natural := 1;
  constant c_nof_repeat_extra          : natural := sel_a_b(g_mux_use_fifo, (c_nof_streams + g_mux_fifo_fill) / c_len_min, 0);
  constant c_in_nof_repeat             : natural := g_nof_repeat + c_nof_repeat_extra;
  constant c_in_repeat_arr_mode_mux_0  : t_natural_arr(0 to c_nof_streams - 1) := array_init(c_in_nof_repeat, c_nof_streams, 1);  -- use g_nof_repeat+cK to have different expected_out_data(I,J) per input, to verify that the output demultiplexing works correct
  constant c_in_repeat_arr_mode_mux_1  : t_natural_arr(0 to c_nof_streams - 1) := array_init(c_in_nof_repeat, c_nof_streams);  -- use g_nof_repeat for all inputs
  constant c_in_repeat_arr             : t_natural_arr := sel_a_b(g_mode_mux = 0, c_in_repeat_arr_mode_mux_0, c_in_repeat_arr_mode_mux_1);
  constant c_out_repeat_arr_mode_mux_0 : t_natural_arr(0 to c_nof_streams - 1) := array_init(g_nof_repeat, c_nof_streams, 1);
  constant c_out_repeat_arr_mode_mux_1 : t_natural_arr(0 to c_nof_streams - 1) := array_init(g_nof_repeat, c_nof_streams);
  constant c_out_repeat_arr            : t_natural_arr := sel_a_b(g_mode_mux = 0, c_out_repeat_arr_mode_mux_0, c_out_repeat_arr_mode_mux_1);

  constant c_channel_input_w           : natural := ceil_log2(c_nof_input);
  constant c_channel_w                 : natural := ceil_log2(c_nof_type) + c_channel_input_w;

  type t_siso_2arr is array (integer range <>) of t_dp_siso_arr(0 to c_nof_input - 1);
  type t_sosi_2arr is array (integer range <>) of t_dp_sosi_arr(0 to c_nof_input - 1);

  type t_ctrl_2arr   is array (0 to c_nof_type-1, 0 to c_nof_input - 1) of std_logic;
  type t_data_2arr   is array (0 to c_nof_type-1, 0 to c_nof_input - 1) of std_logic_vector(c_data_w - 1 downto 0);
  type t_rl_vec_2arr is array (0 to c_nof_type-1, 0 to c_nof_input - 1) of std_logic_vector(0 to c_rl);

  signal tb_end_vec        : std_logic_vector(c_nof_streams - 1 downto 0) := (others => '0');
  signal tb_end            : std_logic;
  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';

  signal random_0          : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1          : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0           : std_logic;
  signal pulse_1           : std_logic;
  signal pulse_en          : std_logic := '1';

  signal in_en             : std_logic := '1';
  signal in_siso_2arr      : t_siso_2arr(0 to c_nof_type-1);
  signal in_sosi_2arr      : t_sosi_2arr(0 to c_nof_type-1);

  signal mux_siso_arr      : t_dp_siso_arr(0 to c_nof_type-1);
  signal mux_sosi_arr      : t_dp_sosi_arr(0 to c_nof_type-1);

  signal mux_siso          : t_dp_siso;
  signal mux_sosi          : t_dp_sosi;
  signal mux_data          : std_logic_vector(c_data_w - 1 downto 0);
  signal mux_bsn           : std_logic_vector(c_data_w - 1 downto 0);
  signal mux_sync          : std_logic;
  signal mux_val           : std_logic;
  signal mux_sop           : std_logic;
  signal mux_eop           : std_logic;
  signal mux_channel       : std_logic_vector(c_channel_w - 1 downto 0);

  signal demux_siso_arr    : t_dp_siso_arr(0 to c_nof_type-1);
  signal demux_sosi_arr    : t_dp_sosi_arr(0 to c_nof_type-1);

  signal out_siso_2arr     : t_siso_2arr(0 to c_nof_type-1);
  signal out_sosi_2arr     : t_sosi_2arr(0 to c_nof_type-1);

  signal out_siso          : t_dp_siso := c_dp_siso_rdy;

  signal verify_en         : t_ctrl_2arr := (others => (others => '0'));
  signal verify_done       : t_ctrl_2arr := (others => (others => '0'));
  signal count_eop         : t_integer_matrix(0 to c_nof_type-1, 0 to c_nof_input - 1) := (others => (others => 0));
  signal prev_count_eop    : t_integer_matrix(0 to c_nof_type-1, 0 to c_nof_input - 1) := (others => (others => 0));

  signal prev_out_ready    : t_rl_vec_2arr;
  signal prev_out_data     : t_data_2arr := (others => (others => TO_SVEC(c_data_init - 1, c_data_w)));
  signal out_data          : t_data_2arr;
  signal out_bsn           : t_data_2arr;
  signal out_sync          : t_ctrl_2arr;
  signal out_val           : t_ctrl_2arr;
  signal out_sop           : t_ctrl_2arr;
  signal out_eop           : t_ctrl_2arr;
  signal hold_out_sop      : t_ctrl_2arr;
  signal expected_out_data : t_data_2arr;
begin
  tb_end <= vector_and(tb_end_vec);

  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en         <= '1'                     when g_in_en = e_active               else
                   random_0(random_0'high) when g_in_en = e_random               else
                   pulse_0                 when g_in_en = e_pulse;

  out_siso.ready <= '1'                     when g_out_ready = e_active           else
                    random_1(random_1'high) when g_out_ready = e_random           else
                    pulse_1                 when g_out_ready = e_pulse;

  out_siso_2arr <= (others => (others => out_siso));

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  gen_type : for I in 0 to c_nof_type-1 generate
    gen_input : for J in 0 to c_nof_input - 1 generate
      p_stimuli : process
        constant cK             : natural := I * c_nof_input + J;
        variable v_data_init    : natural;
        variable v_sync         : std_logic;
      begin
        v_data_init := c_data_init + cK;
        in_sosi_2arr(I)(J) <= c_dp_sosi_rst;
        proc_common_wait_until_low(clk, rst);
        proc_common_wait_some_cycles(clk, 5);

        -- Begin of stimuli
        for R in 0 to c_in_repeat_arr(cK) - 1 loop
          v_sync := sel_a_b(R mod c_sync_period = c_sync_offset, '1', '0');
          proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_init, 0, 0, c_len_arr(I), 0, 0, v_sync, TO_DP_BSN(R), clk, in_en, in_siso_2arr(I)(J), in_sosi_2arr(I)(J));
          --proc_common_wait_some_cycles(clk, 10);
          v_data_init := v_data_init + c_len_arr(I);
        end loop;

        -- End of stimuli
        expected_out_data(I, J) <= TO_UVEC(v_data_init - 1, c_data_w);
        wait;
      end process;
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  prev_count_eop <= count_eop when rising_edge(clk);

  gen_verify : for I in 0 to c_nof_type-1 generate
    gen_output : for J in 0 to c_nof_input - 1 generate
      -- Verification logistics
      verify_en(I, J) <= '1'              when rising_edge(clk) and out_sosi_2arr(I)(J).sop = '1';  -- verify enable after first output sop
      count_eop(I, J) <= count_eop(I, J) + 1 when rising_edge(clk) and out_sosi_2arr(I)(J).eop = '1';  -- count number of output eop
      verify_done(I, J) <= '1'            when rising_edge(clk) and count_eop(I, J) = c_out_repeat_arr(I * c_nof_input + J) and count_eop(I, J) /= prev_count_eop(I, J) else '0';
      tb_end_vec(I * c_nof_input + J) <= '1' when rising_edge(clk) and count_eop(I, J) = c_out_repeat_arr(I * c_nof_input + J);

      -- Actual verification of the output streams
      proc_dp_verify_data("out_sosi_2arr.data", c_rl, clk, verify_en(I, J), out_siso_2arr(I)(J).ready, out_sosi_2arr(I)(J).valid, out_data(I, J), prev_out_data(I, J));  -- Verify that the output is incrementing data, like the input stimuli
      proc_dp_verify_valid(c_rl, clk, verify_en(I, J), out_siso_2arr(I)(J).ready, prev_out_ready(I, J), out_sosi_2arr(I)(J).valid);  -- Verify that the output valid fits with the output ready latency
      proc_dp_verify_sop_and_eop(clk, out_sosi_2arr(I)(J).valid, out_sosi_2arr(I)(J).sop, out_sosi_2arr(I)(J).eop, hold_out_sop(I, J));  -- Verify that sop and eop come in pairs
      proc_dp_verify_value(e_equal, clk, verify_done(I, J), expected_out_data(I, J), prev_out_data(I, J));  -- Verify that the stimuli have been applied at all
      proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_en(I, J), out_sosi_2arr(I)(J).sync, out_sosi_2arr(I)(J).sop, out_sosi_2arr(I)(J).bsn);
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  -- Input level multiplexing
  gen_mux : for I in 0 to c_nof_type-1 generate
    u_input_mux : entity dp_lib.dp_mux
    generic map (
      -- MUX
      g_mode              => g_mode_mux,
      g_nof_input         => c_nof_input,
      g_append_channel_lo => c_use_channel_lo,
      -- Input FIFO
      g_use_fifo          => g_mux_use_fifo,
      g_bsn_w             => c_data_w,
      g_data_w            => c_data_w,
      g_use_bsn           => c_use_bsn,
      g_use_sync          => c_use_sync,
      g_fifo_size         => array_init(           1024, c_nof_input),  -- must match g_nof_input, even when g_use_fifo=FALSE
      g_fifo_fill         => array_init(g_mux_fifo_fill, c_nof_input)  -- must match g_nof_input, even when g_use_fifo=FALSE
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- ST sinks
      snk_out_arr => in_siso_2arr(I),
      snk_in_arr  => in_sosi_2arr(I),
      -- ST source
      src_in      => mux_siso_arr(I),
      src_out     => mux_sosi_arr(I)
    );
  end generate;

  -- Second level multiplexing
  u_type_mux : entity dp_lib.dp_mux
  generic map (
    -- MUX
    g_mode              => g_mode_mux,
    g_nof_input         => c_nof_type,
    g_append_channel_lo => c_use_channel_lo,
    -- Input FIFO
    g_use_fifo          => g_mux_use_fifo,
    g_bsn_w             => c_data_w,
    g_data_w            => c_data_w,
    g_in_channel_w      => c_channel_input_w,  -- pass channel due to u_input_mux
    g_use_in_channel    => g_mux_use_fifo,
    g_use_bsn           => c_use_bsn,
    g_use_sync          => c_use_sync,
    g_fifo_size         => array_init(           1024, c_nof_type),  -- must match g_nof_input, even when g_use_fifo=FALSE
    g_fifo_fill         => array_init(g_mux_fifo_fill, c_nof_type)  -- must match g_nof_input, even when g_use_fifo=FALSE
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => mux_siso_arr,
    snk_in_arr  => mux_sosi_arr,
    -- ST source
    src_in      => mux_siso,
    src_out     => mux_sosi
  );

  -- Map to slv to ease monitoring in wave window
  mux_data    <= mux_sosi.data(c_data_w - 1 downto 0);
  mux_bsn     <= mux_sosi.bsn(c_data_w - 1 downto 0);
  mux_sync    <= mux_sosi.sync;
  mux_val     <= mux_sosi.valid;
  mux_sop     <= mux_sosi.sop;
  mux_eop     <= mux_sosi.eop;
  mux_channel <= mux_sosi.channel(c_channel_w - 1 downto 0);

  ------------------------------------------------------------------------------
  -- DATA DEMULTIPLEXING
  ------------------------------------------------------------------------------

  u_type_demux: entity dp_lib.dp_demux
  generic map (
    g_mode              => c_mode_demux,
    g_nof_output        => c_nof_type,
    g_remove_channel_lo => c_use_channel_lo,
    g_combined          => g_combined_demux
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out     => mux_siso,
    snk_in      => mux_sosi,
    -- ST source
    src_in_arr  => demux_siso_arr,
    src_out_arr => demux_sosi_arr
  );

  gen_demux : for I in 0 to c_nof_type-1 generate
    u_output_demux : entity dp_lib.dp_demux
    generic map (
      g_mode              => c_mode_demux,
      g_nof_output        => c_nof_input,
      g_remove_channel_lo => c_use_channel_lo,
      g_combined          => g_combined_demux
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- ST sinks
      snk_out     => demux_siso_arr(I),
      snk_in      => demux_sosi_arr(I),
      -- ST source
      src_in_arr  => out_siso_2arr(I),
      src_out_arr => out_sosi_2arr(I)
    );

    gen_output : for J in 0 to c_nof_input - 1 generate
      out_data(I, J) <= out_sosi_2arr(I)(J).data(c_data_w - 1 downto 0);
      out_bsn( I, J) <= out_sosi_2arr(I)(J).bsn(c_data_w - 1 downto 0);
      out_sync(I, J) <= out_sosi_2arr(I)(J).sync;
      out_val( I, J) <= out_sosi_2arr(I)(J).valid;
      out_sop( I, J) <= out_sosi_2arr(I)(J).sop;
      out_eop( I, J) <= out_sosi_2arr(I)(J).eop;
    end generate;
  end generate;
end tb;
