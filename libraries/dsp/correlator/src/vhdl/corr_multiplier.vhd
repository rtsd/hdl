--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, common_mult_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Provide an array of multipliers with streaming I/O
-- Description:
-- . Multiplies snk_in_2arr_2[i][0] with snk_in_2arr_2[i][1] yielding
--   src_out_2arr[i] for i in 0..g_nof_inputs-1.

entity corr_multiplier is
  generic (
    g_technology  : natural := c_tech_select_default;
    g_nof_inputs  : natural;
    g_data_w      : natural
   );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_2arr_2  : in  t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);

    src_out_arr   : out t_dp_sosi_arr(g_nof_inputs - 1 downto 0)
  );
end corr_multiplier;

architecture str of corr_multiplier is
  constant c_pipeline_input   : natural := 1;
  constant c_pipeline_product : natural := 0;
  constant c_pipeline_adder   : natural := 1;
  constant c_pipeline_output  : natural := 1;

  constant c_pipeline : natural := c_pipeline_input + c_pipeline_product + c_pipeline_adder + c_pipeline_output;

  signal common_complex_mult_src_out_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  signal dp_pipeline_src_out_arr : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Complex Multipliers
  -----------------------------------------------------------------------------
  gen_common_complex_mult : for i in 0 to g_nof_inputs - 1 generate
    u_common_complex_mult : entity common_mult_lib.common_complex_mult
    generic map (
      g_technology       => g_technology,
      g_variant          => "IP",
      g_in_a_w           => g_data_w,
      g_in_b_w           => g_data_w,
      g_out_p_w          => 2 * g_data_w,  -- default use g_out_p_w = g_in_a_w+g_in_b_w
      g_conjugate_b      => true,
      g_pipeline_input   => c_pipeline_input,
      g_pipeline_product => c_pipeline_product,
      g_pipeline_adder   => c_pipeline_adder,
      g_pipeline_output  => c_pipeline_output
    )
    port map (
      clk        => clk,
      clken      => '1',
      rst        => '0',
      in_ar      => snk_in_2arr_2(i)(0).re(g_data_w - 1 downto 0),
      in_ai      => snk_in_2arr_2(i)(0).im(g_data_w - 1 downto 0),
      in_br      => snk_in_2arr_2(i)(1).re(g_data_w - 1 downto 0),
      in_bi      => snk_in_2arr_2(i)(1).im(g_data_w - 1 downto 0),
      in_val     => snk_in_2arr_2(i)(0).valid,
      out_pr     => common_complex_mult_src_out_arr(i).re(2 * g_data_w - 1 downto 0),
      out_pi     => common_complex_mult_src_out_arr(i).im(2 * g_data_w - 1 downto 0),
      out_val    => common_complex_mult_src_out_arr(i).valid
    );

    src_out_arr(i).re    <= RESIZE_DP_DSP_DATA(common_complex_mult_src_out_arr(i).re(2 * g_data_w - 1 downto 0));
    src_out_arr(i).im    <= RESIZE_DP_DSP_DATA(common_complex_mult_src_out_arr(i).im(2 * g_data_w - 1 downto 0));
    src_out_arr(i).valid <= common_complex_mult_src_out_arr(i).valid;
  end generate;

  -----------------------------------------------------------------------------
  -- Forward the input sync with the correct latency
  -----------------------------------------------------------------------------
 u_common_pipeline_sl : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline  => c_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,

    in_dat  => snk_in_2arr_2(0)(0).sync,
    out_dat => src_out_arr(0).sync
  );
end str;
