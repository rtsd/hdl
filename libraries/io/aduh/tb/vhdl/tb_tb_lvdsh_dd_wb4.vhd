-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_lvdsh_dd_wb4 is
end tb_tb_lvdsh_dd_wb4;

architecture tb of tb_tb_lvdsh_dd_wb4 is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 3
  -- > run -all

  -- g_dclk_drift  : TIME := 0 ps;     -- 0 ps, use -2 ps or 2 ps to model the range of sample phase uncertainty by letting the sclk and dclk drift with respect to the dp_clk,
  -- g_dclk_offon  : BOOLEAN := TRUE;  -- when TRUE switch the dclk off-on periodically, to model ADU not present or ADU restart or ADU replaced
  -- g_in_phase    : NATURAL := 1      -- 0:3

  u_phase_0    : entity work.tb_lvdsh_dd_wb4 generic map (0 ps, false, 0);
  u_phase_1    : entity work.tb_lvdsh_dd_wb4 generic map (0 ps, false, 1);
  u_phase_2    : entity work.tb_lvdsh_dd_wb4 generic map (0 ps, false, 2);
  u_phase_3    : entity work.tb_lvdsh_dd_wb4 generic map (0 ps, false, 3);
end tb;
