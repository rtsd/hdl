#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist                                                                                        


set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_transceiver_pll_10g/sim"

vmap  altera_common_sv_packages           ./work/
vmap  altera_xcvr_atx_pll_a10_180         ./work/

  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/altera_xcvr_native_a10_functions_h.sv"                                                                   -work altera_common_sv_packages          
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/twentynm_xcvr_avmm.sv"                                                      -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/mentor/twentynm_xcvr_avmm.sv"                                               -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/alt_xcvr_resync.sv"                                                         -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/alt_xcvr_arbiter.sv"                                                        -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/mentor/alt_xcvr_resync.sv"                                                  -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/mentor/alt_xcvr_arbiter.sv"                                                 -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/a10_avmm_h.sv"                                                              -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/alt_xcvr_atx_pll_rcfg_arb.sv"                                               -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/a10_xcvr_atx_pll.sv"                                                        -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/alt_xcvr_pll_embedded_debug.sv"                                             -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/alt_xcvr_pll_avmm_csr.sv"                                                   -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/mentor/alt_xcvr_atx_pll_rcfg_arb.sv"                                        -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/mentor/a10_xcvr_atx_pll.sv"                                                 -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/mentor/alt_xcvr_pll_embedded_debug.sv"                                      -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/mentor/alt_xcvr_pll_avmm_csr.sv"                                            -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/ip_arria10_e1sg_transceiver_pll_10g_altera_xcvr_atx_pll_a10_180_fdgop6i.sv" -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
  vlog -sv   "$IP_DIR/../altera_xcvr_atx_pll_a10_180/sim/alt_xcvr_atx_pll_rcfg_opt_logic_fdgop6i.sv"                                 -L altera_common_sv_packages -work altera_xcvr_atx_pll_a10_180        
                                                                                               
