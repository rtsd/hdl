-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--   Map usr bus to phy bus and compensate for the backplane PCB reordering.
-- Description:
--   The backplane has 4 UniBoards and each board can connect to 3 other
--   boards. Therefore at the phy side busses 2:0 are used to connect to the
--   other 3 UniBoards and bus 3 is not used. At the usr side the usr bus
--   index indicates the tx destination Uniboard and the rx source UniBoard.
--   The usr bus index can be 3:0, so including this UniBoard itself that is
--   indicated by usr bus index bck_id. The other 3 usr busses each connect to
--   the corresponding Uniboard.
--   The unb1_board_back_reorder has to map the usr index 3:0 (excluding its
--   own index bck_id) on to the phy index 2:0. The logical mapping is:
--
--   UniBoard bck_id=3 connects to UniBoards 2,1,0 via phy busses 2,1,0
--                   2                       3,1,0                2,1,0
--                   1                       3,2,0                2,1,0
--                   0                       3,2,1                2,1,0
--
--   However the Apertif beamformer backplane uses a different phy bus mapping
--   to ease the PCB routing. The Apertif beamformer backplane mapping is:
--
--   UniBoard bck_id=3 connects to UniBoards 2,1,0 via phy busses 2,1,0
--                   2                       3,1,0                1,0,2
--                   1                       3,2,0                0,1,2
--                   0                       3,2,1                0,2,1
--
--   The mapping is the same for all BN on the UniBoard. The
--   unb1_board_back_reorder  maps for the Apertif beamformer backplane with
--   4 UniBoards.
-- Remark:
-- . See unb1_board_back_model_sl.vhd for the Apertif backplane model

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;

entity unb1_board_back_reorder is
  port (
    bck_id           : in  std_logic_vector(c_unb1_board_nof_uniboard_w - 1 downto 0);
    clk              : in  std_logic;

    -- usr side interface
    tx_usr_sosi_2arr : in  t_unb1_board_back_sosi_2arr;
    tx_usr_siso_2arr : out t_unb1_board_back_siso_2arr;

    rx_usr_sosi_2arr : out t_unb1_board_back_sosi_2arr;
    rx_usr_siso_2arr : in  t_unb1_board_back_siso_2arr;

    -- phy side interface
    tx_phy_sosi_2arr : out t_unb1_board_back_sosi_2arr;
    tx_phy_siso_2arr : in  t_unb1_board_back_siso_2arr;

    rx_phy_sosi_2arr : in  t_unb1_board_back_sosi_2arr;
    rx_phy_siso_2arr : out t_unb1_board_back_siso_2arr
  );
end unb1_board_back_reorder;

architecture rtl of unb1_board_back_reorder is
begin
  p_comb : process(bck_id, tx_usr_sosi_2arr, rx_phy_sosi_2arr, rx_usr_siso_2arr, tx_phy_siso_2arr)
  begin
    -- Default set all IO to not used:
    -- . usr bus with index bck_id of this UniBoard remains not used
    -- . phy bus 3 remains not used
    tx_phy_sosi_2arr <= (others => (others => c_dp_sosi_x));
    tx_usr_siso_2arr <= (others => (others => c_dp_siso_x));

    rx_usr_sosi_2arr <= (others => (others => c_dp_sosi_x));
    rx_phy_siso_2arr <= (others => (others => c_dp_siso_x));

    -- Map the usr busses for the other UniBoards to the phy busses 2:0
    case TO_UINT(bck_id) is
      when 0 =>
           -- UniBoard 0 connects to UniBoards 3,2,1 via phy busses 0,2,1
           tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(1);  -- to   unb 1
           tx_usr_siso_2arr(1) <= tx_phy_siso_2arr(1);
           rx_usr_sosi_2arr(1) <= rx_phy_sosi_2arr(1);  -- from unb 1
           rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(1);

           tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(2);  -- to   unb 2
           tx_usr_siso_2arr(2) <= tx_phy_siso_2arr(2);
           rx_usr_sosi_2arr(2) <= rx_phy_sosi_2arr(2);  -- from unb 2
           rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(2);

           tx_phy_sosi_2arr(0) <= tx_usr_sosi_2arr(3);  -- to   unb 3
           tx_usr_siso_2arr(3) <= tx_phy_siso_2arr(0);
           rx_usr_sosi_2arr(3) <= rx_phy_sosi_2arr(0);  -- from unb 3
           rx_phy_siso_2arr(0) <= rx_usr_siso_2arr(3);
      when 1 =>
           -- UniBoard 1 connects to UniBoards 3,2,0 via phy busses 0,1,2
           tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(0);  -- to   unb 0
           tx_usr_siso_2arr(0) <= tx_phy_siso_2arr(2);
           rx_usr_sosi_2arr(0) <= rx_phy_sosi_2arr(2);  -- from unb 0
           rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(0);

           tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(2);  -- to   unb 2
           tx_usr_siso_2arr(2) <= tx_phy_siso_2arr(1);
           rx_usr_sosi_2arr(2) <= rx_phy_sosi_2arr(1);  -- from unb 2
           rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(2);

           tx_phy_sosi_2arr(0) <= tx_usr_sosi_2arr(3);  -- to   unb 3
           tx_usr_siso_2arr(3) <= tx_phy_siso_2arr(0);
           rx_usr_sosi_2arr(3) <= rx_phy_sosi_2arr(0);  -- from unb 3
           rx_phy_siso_2arr(0) <= rx_usr_siso_2arr(3);
      when 2 =>
           -- UniBoard 2 connects to UniBoards 3,1,0 via phy busses 1,0,2
           tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(0);  -- to   unb 0
           tx_usr_siso_2arr(0) <= tx_phy_siso_2arr(2);
           rx_usr_sosi_2arr(0) <= rx_phy_sosi_2arr(2);  -- from unb 0
           rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(0);

           tx_phy_sosi_2arr(0) <= tx_usr_sosi_2arr(1);  -- to   unb 1
           tx_usr_siso_2arr(1) <= tx_phy_siso_2arr(0);
           rx_usr_sosi_2arr(1) <= rx_phy_sosi_2arr(0);  -- from unb 1
           rx_phy_siso_2arr(0) <= rx_usr_siso_2arr(1);

           tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(3);  -- to   unb 3
           tx_usr_siso_2arr(3) <= tx_phy_siso_2arr(1);
           rx_usr_sosi_2arr(3) <= rx_phy_sosi_2arr(1);  -- from unb 3
           rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(3);
      when 3 =>
           -- UniBoard 3 connects to UniBoards 2,1,0 via phy busses 2,1,0
           tx_phy_sosi_2arr(0) <= tx_usr_sosi_2arr(0);  -- to   unb 0
           tx_usr_siso_2arr(0) <= tx_phy_siso_2arr(0);
           rx_usr_sosi_2arr(0) <= rx_phy_sosi_2arr(0);  -- from unb 0
           rx_phy_siso_2arr(0) <= rx_usr_siso_2arr(0);

           tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(1);  -- to   unb 1
           tx_usr_siso_2arr(1) <= tx_phy_siso_2arr(1);
           rx_usr_sosi_2arr(1) <= rx_phy_sosi_2arr(1);  -- from unb 1
           rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(1);

           tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(2);  -- to   unb 2
           tx_usr_siso_2arr(2) <= tx_phy_siso_2arr(2);
           rx_usr_sosi_2arr(2) <= rx_phy_sosi_2arr(2);  -- from unb 2
           rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(2);
      when others => null;
    end case;
  end process;
end rtl;
