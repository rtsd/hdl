-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. vd Walle
-- Purpose: Multi test bench for eth_tester for high bandwidths.
-- Description: Similar to the 1GbE TB as described in [1] but for 10 / 100 GbE.
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+ETH+tester+unit+for+1GbE
--
-- Usage:
--   > as 8
--   > run -all
--   Takes about 10 m

library IEEE, diag_lib;
use IEEE.std_logic_1164.all;
use diag_lib.diag_pkg.all;
use work.tb_eth_tester_pkg.all;

entity tb_tb_eth_tester_high_bw is
end tb_tb_eth_tester_high_bw;

architecture tb of tb_tb_eth_tester_high_bw is
  -- Multi tb
  constant c_tb_w       : natural := 100;  -- sufficiently long to fit all tb instances
  constant c_tb_end_vec : std_logic_vector(c_tb_w - 1 downto 0) := (others => '1');

  signal tb_end_vec   : std_logic_vector(c_tb_w - 1 downto 0) := c_tb_end_vec;  -- best view as hex in Wave Window
  signal tb_end       : std_logic := '0';

  -- Tb
  constant c_eth_clk_MHz   : natural := 125;
  constant c_st_clk_MHz    : natural := 200;
  constant c_nof_sync      : natural := 2;
  constant c_nof_sync_many : natural := 50;  -- sufficient to achieve Tx FIFO fill level
  constant c_nof_streams   : natural := 3;
  constant c_nof_blk       : natural := 3;  -- nof_blk per sync

  -- Tx packet size and gap size in octets
  constant c_block_len       : natural := 500;  -- BG block length of first stream [0]
  constant c_block_len_jumbo : natural := 9000;
  constant c_link_len        : natural := func_eth_tester_eth_packet_on_link_length(c_block_len);

  -- For near maximum 1Gbps link rate the c_block_len + c_gap_len_min time
  -- in the st_clk domain equals c_link_len time in eth_clk domain.
  constant c_gap_len_min   : natural := c_link_len * c_st_clk_MHz / c_eth_clk_MHz - c_block_len;
  constant c_slot_len_min  : natural := c_block_len + c_gap_len_min;

  -- Choose c_gap_len somewhat larger to have packet link rate < 1 Gbps
  constant c_gap_len       : natural := c_gap_len_min * 2;  -- for g_nof_streams = 1
  constant c_long_gap      : natural := c_gap_len_min * 10;
  constant c_short_gap     : natural := 10;  -- to cause BG xon/xoff flow control
  constant c_zero_gap      : natural := 0;  -- to verify BG ready flow control

  -- Choose c_others_len > c_block_len, so same c_gap_len is suitable to
  -- keep Ethernet link rate < 1 Gbps
  constant c_others_len    : natural := 65;  -- BG block length of other streams [c_nof_streams-1 : 1]

  -- BG ctrl
  constant c_high             : natural := c_diag_bg_mem_max_adr;  -- = 2**24

  constant c_bg_ctrl_rst      : t_diag_block_gen_integer := ('0', '0', 1, c_nof_blk, c_gap_len, 0, c_high, 0);  -- place holder for unused stream

  constant c_bg_ctrl_one      : t_diag_block_gen_integer := ('1', '1', c_block_len,  c_nof_blk, c_gap_len, 0, c_high, 0);  -- for first stream
  constant c_bg_ctrl_others   : t_diag_block_gen_integer := ('1', '1', c_others_len, c_nof_blk, c_gap_len, 0, c_high, 0);  -- for other streams

  -- . BG with different block lengths and other payload values
  --   The payload values are only verified manually using the Wave Window
  constant c_bg_ctrl_len_0    : t_diag_block_gen_integer := ('1', '1', c_block_len + 0, c_nof_blk, c_gap_len,      0,      0, 0);  -- nof octets
  constant c_bg_ctrl_len_1    : t_diag_block_gen_integer := ('1', '1', c_block_len + 1, c_nof_blk, c_gap_len,      1,      1, 0);  -- nof octets
  constant c_bg_ctrl_len_2    : t_diag_block_gen_integer := ('1', '1', c_block_len + 2, c_nof_blk, c_gap_len,      1,      7, 0);  -- nof octets
  constant c_bg_ctrl_len_3    : t_diag_block_gen_integer := ('1', '1', c_block_len + 3, c_nof_blk, c_gap_len, c_high - 1, c_high - 1, 0);  -- nof octets

  constant c_bg_ctrl_multiple_first    : t_diag_block_gen_integer := ('1', '1', c_block_len,  c_nof_blk, c_nof_streams * c_gap_len, 0, c_high, 0);  -- for first stream
  constant c_bg_ctrl_multiple_others   : t_diag_block_gen_integer := ('1', '1', c_others_len, c_nof_blk, c_nof_streams * c_gap_len, 0, c_high, 0);  -- for other streams
begin
--  g_tb_index         : NATURAL := 0;  -- use to incremental delay logging from tb instances in tb_tb
--  g_nof_sync         : NATURAL := 3;  -- number of BG sync intervals to set c_run_time
--  g_nof_streams      : NATURAL := 2;
--  g_nof_octet_output   : NATURAL := 96; -- maximum = 96 bytes as max dp.data field = 768 bits.
--  g_nof_octet_generate : NATURAL := 96;
--
--  -- t_diag_block_gen_integer =
--  --   sl:  enable
--  --   sl:  enable_sync
--  --   nat: samples_per_packet
--  --   nat: blocks_per_sync
--  --   nat: gapsize
--  --   nat: mem_low_adrs
--  --   nat: mem_high_adrs
--  --   nat: bsn_init
--  g_bg_ctrl_first    : t_diag_block_gen_integer := ('1', '1', 50, c_nof_blk, 100, 0, 30, 0);  -- for first stream
--  g_bg_ctrl_others   : t_diag_block_gen_integer := ('1', '1', 30, c_nof_blk, 10, 0, 30, 0)   -- for other streams

  -- Tb instance prefix:
  -- . u_st   : uses streaming Tx-Rx interface

  -----------------------------------------------------------------------------
  -- Single stream
  -----------------------------------------------------------------------------
  -- Try different loopback interfaces
  u_st_10g      : entity work.tb_eth_tester_high_bw generic map (0, c_nof_sync, 1, 8, 8, c_bg_ctrl_one, c_bg_ctrl_rst) port map (tb_end_vec(0));  -- 8 byte wide
  u_st_100g     : entity work.tb_eth_tester_high_bw generic map (1, c_nof_sync, 1, 64, 64, c_bg_ctrl_one, c_bg_ctrl_rst) port map (tb_end_vec(1));  -- 64 byte wide
  u_st_max      : entity work.tb_eth_tester_high_bw generic map (2, c_nof_sync, 1, 96, 96, c_bg_ctrl_one, c_bg_ctrl_rst) port map (tb_end_vec(2));  -- 96 byte wide as max dp.data width = 96*8=768 bits.

  -- Try large block size and nof blocks_per_sync = 1
  u_st_jumbo1   : entity work.tb_eth_tester_high_bw generic map (10, c_nof_sync, 1, 96, 96,
                                                         ('1', '1', c_block_len_jumbo, 1, c_zero_gap, 0, c_high, 0),
                                                         c_bg_ctrl_rst)
                                            port map (tb_end_vec(10));

  -- Try large block sizes
  u_st_jumbo2   : entity work.tb_eth_tester_high_bw generic map (11, c_nof_sync, 1, 64, 64,
                                                         ('1', '1', c_block_len_jumbo, 2, c_zero_gap, 0, c_high, 0),
                                                         c_bg_ctrl_rst)
                                            port map (tb_end_vec(11));

  -- Try small block sizes at 64*8 = 512b bus size
  -- . BG supports samples_per_packet >= 2, BG treats samples_per_packet = 1 as 2
  -- . ETH MAC pads samples_per_packet <= 6 to 6, to have minimum packet length of 64 octets,
  --   because hdr = 14 + 20 + 8 + 12 and crc = 4 have 58 octets.
  u_st_len2 : entity work.tb_eth_tester_high_bw generic map (20, c_nof_sync, 1, 64, 64,
                                                     ('1', '1', 2, c_nof_blk, c_gap_len, 0, c_high, 0),
                                                     c_bg_ctrl_rst)
                                        port map (tb_end_vec(20));

  -- Try different BG block lengths and data widths to verify sosi.empty nof octets in last word and repack.
  u_st_bg_len_0 : entity work.tb_eth_tester_high_bw generic map (30, c_nof_sync, 1, 64,  8, c_bg_ctrl_len_0, c_bg_ctrl_rst) port map (tb_end_vec(30));
  u_st_bg_len_1 : entity work.tb_eth_tester_high_bw generic map (31, c_nof_sync, 1, 8,   1, c_bg_ctrl_len_1, c_bg_ctrl_rst) port map (tb_end_vec(31));
  u_st_bg_len_2 : entity work.tb_eth_tester_high_bw generic map (32, c_nof_sync, 1, 8,   4, c_bg_ctrl_len_2, c_bg_ctrl_rst) port map (tb_end_vec(32));
  u_st_bg_len_3 : entity work.tb_eth_tester_high_bw generic map (33, c_nof_sync, 1, 64, 64, c_bg_ctrl_len_3, c_bg_ctrl_rst) port map (tb_end_vec(33));

  -----------------------------------------------------------------------------
  -- Multiple streams
  -----------------------------------------------------------------------------
  u_st_multiple_streams : entity work.tb_eth_tester_high_bw
                          generic map (80, c_nof_sync, c_nof_streams, 8, 8,
                                       c_bg_ctrl_multiple_first,
                                       c_bg_ctrl_multiple_others)
                          port map (tb_end_vec(80));

  tb_end <= '1' when tb_end_vec = c_tb_end_vec else '0';

  p_tb_end : process
  begin
    wait until tb_end = '1';
    wait for 1 ns;
    report "Multi tb simulation finished."
      severity FAILURE;
    wait;
  end process;
end tb;
