-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Scope component to show the concateneated DP in_data at the SCLK
--          sample rate in the Wave Window
-- Description:
-- . See dp_wideband_sp_arr_scope (for g_nof_streams=1)
-- . The wideband in_data has g_wideband_factor nof samples per word. For
--   g_wideband_big_endian=TRUE sthe first sample is in the MS symbol.
-- Remark:
-- . Only for simulation.
-- . When g_use_sclk=TRUE then the input SCLK is used. Else the SCLK is derived
--   from the DCLK so that it does not have to be applied via an input. This
--   eases the use of this scope within a design.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_wideband_data_scope is
  generic (
    g_sim                 : boolean := false;
    g_use_sclk            : boolean := true;
    g_wideband_factor     : natural := 4;  -- Wideband rate factor = 4 for dp_clk processing frequency is 200 MHz frequency and SCLK sample frequency Fs is 800 MHz
    g_wideband_big_endian : boolean := true;  -- When true in_data[3:0] = sample[t0,t1,t2,t3], else when false : in_data[3:0] = sample[t3,t2,t1,t0]
    g_dat_w               : natural := 8  -- Actual width of the data samples
  );
  port (
    -- Digital processing clk
    DCLK      : in std_logic := '0';

    -- Sampling clk, for simulation only
    SCLK      : in std_logic := '0';  -- SCLK rate = g_wideband_factor * DCLK rate

    -- Streaming input data
    in_data   : in std_logic_vector(g_wideband_factor * g_dat_w - 1 downto 0);
    in_val    : in std_logic;

    -- Scope output samples
    out_dat   : out std_logic_vector(g_dat_w - 1 downto 0);
    out_int   : out integer;
    out_val   : out std_logic
  );
end common_wideband_data_scope;

architecture beh of common_wideband_data_scope is
  signal SCLKi       : std_logic;  -- sampling clk, for simulation only
  signal scope_cnt   : natural;
  signal scope_dat   : std_logic_vector(g_dat_w - 1 downto 0);
begin
  sim_only : if g_sim = true generate
    use_sclk : if g_use_sclk = true generate
      SCLKi <= SCLK;  -- no worry about the delta cycle delay from SCLK to SCLKi
    end generate;

    gen_sclk : if g_use_sclk = false generate
      proc_common_dclk_generate_sclk(g_wideband_factor, DCLK, SCLKi);
    end generate;

    -- View in_data at the sample rate using out_dat
    p_scope_dat : process(SCLKi)
      variable vI : natural;
    begin
      if rising_edge(SCLKi) then
        if g_wideband_big_endian = true then
          vI := g_wideband_factor - 1 - scope_cnt;
        else
          vI := scope_cnt;
        end if;
        scope_cnt <= 0;
        if in_val = '1' and scope_cnt < g_wideband_factor - 1 then
          scope_cnt <= scope_cnt + 1;
        end if;
        scope_dat <= in_data((vI + 1) * g_dat_w - 1 downto vI * g_dat_w);
        out_val <= in_val;
      end if;
    end process;

    out_dat <= scope_dat;
    out_int <= TO_SINT(scope_dat);
  end generate;
end beh;
