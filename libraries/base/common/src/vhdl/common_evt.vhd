-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_evt is
  generic (
    g_evt_type   : string := "RISING";  -- type can be: "RISING", "FALLING", or "BOTH"
    g_out_invert : boolean := false;  -- if TRUE then invert the output to have active low output, else default use active high output
    g_out_reg    : boolean := false  -- if TRUE then the output is registered, else it is not
  );
  port (
    rst      : in  std_logic := '0';
    clk      : in  std_logic;
    clken    : in  std_logic := '1';
    in_sig   : in  std_logic;
    out_evt  : out std_logic
  );
end common_evt;

architecture rtl of common_evt is
  signal in_sig_prev  : std_logic := '0';
  signal sig_evt      : std_logic;
  signal sig_evt_n    : std_logic;
begin
  -- Create previous in_sig
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      in_sig_prev <= '0';
    elsif rising_edge(clk) then
      if clken = '1' then
        in_sig_prev <= in_sig;
      end if;
    end if;
  end process;

  -- Detect input event
  gen_rising  : if g_evt_type = "RISING"  generate sig_evt <=     in_sig and not in_sig_prev;  end generate;
  gen_falling : if g_evt_type = "FALLING" generate sig_evt <= not in_sig and     in_sig_prev;  end generate;
  gen_both    : if g_evt_type = "BOTH"    generate sig_evt <=     in_sig xor     in_sig_prev;  end generate;

  sig_evt_n <= not sig_evt;

  -- Output combinatorial event pulse
  no_out_reg : if g_out_reg = false generate
    out_evt <= sel_a_b(g_out_invert, sig_evt_n, sig_evt);
  end generate;

  -- Output registered event pulse
  gen_out_reg : if g_out_reg = true generate
    p_clk : process(rst, clk)
    begin
      if rst = '1' then
        out_evt <= sel_a_b(g_out_invert, '1', '0');
      elsif rising_edge(clk) then
        if clken = '1' then
          out_evt <= sel_a_b(g_out_invert, sig_evt_n, sig_evt);
        end if;
      end if;
    end process;
  end generate;
end rtl;
