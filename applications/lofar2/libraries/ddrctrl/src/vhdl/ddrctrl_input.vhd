-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: Folding a stream of data into a mm data configuration so it can be
-- stored in a DDR RAM-stick.
--
-- Description:
--  First the data from the sosi array gets collected into one data vector.
--  After that this data vector gets resized to the right size data vector in
--  order to make it storable in a DDR RAM-stick.
--  After that a address gets assigned to the data so the data can be found back.
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
--  The maximum value of the address is determend by g_tech_ddr.

library IEEE, technology_lib, tech_ddr_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use technology_lib.technology_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity ddrctrl_input is
  generic (
    g_tech_ddr          : t_c_tech_ddr;  -- type of memory
    g_nof_streams       : natural       := 12;  -- number of input streams
    g_data_w            : natural       := 14;  -- data with of input data vectors
    g_max_adr           : natural;
    g_bim               : natural;
    g_of_pb             : natural;
    g_block_size        : natural
  );
  port (
    clk                   : in  std_logic := '0';
    rst                   : in  std_logic;
    rst_ddrctrl_input_ac  : in  std_logic;
    in_sosi_arr           : in  t_dp_sosi_arr;  -- input data
    in_stop               : in  std_logic;
    out_sosi              : out t_dp_sosi;  -- output data
    out_adr               : out natural;
    out_bsn_adr           : out natural;
    out_data_stopped      : out std_logic
  );
end ddrctrl_input;

architecture str of ddrctrl_input is
  -- constant for readability
  constant  c_out_data_w : natural    := g_nof_streams * g_data_w;  -- the input data with for ddrctrl_repack

  -- signals for connecting the components
  signal    sosi_p_rp           : t_dp_sosi   := c_dp_sosi_init;
  signal    sosi_rp_ac          : t_dp_sosi   := c_dp_sosi_init;
  signal    adr                 : natural     := 0;
  signal    valid               : std_logic   := '0';
  signal    data_stopped_rp_ac  : std_logic   := '0';
begin
  out_adr <= adr;

  -- makes one data vector out of all the data from the t_dp_sosi_arr
  u_ddrctrl_input_pack : entity work.ddrctrl_input_pack
  generic map(
    g_nof_streams     => g_nof_streams,  -- number of input streams
    g_data_w          => g_data_w  -- data with of input data vectors
  )
  port map(
    in_sosi_arr       => in_sosi_arr,  -- input data
    out_sosi          => sosi_p_rp  -- output data
  );

  -- resizes the input data vector so that the output data vector can be stored into the ddr memory
  u_ddrctrl_input_repack : entity work.ddrctrl_input_repack
  generic map(
    g_tech_ddr          => g_tech_ddr,  -- type of memory
    g_in_data_w         => c_out_data_w,  -- the input data with
    g_bim               => g_bim,
    g_of_pb             => g_of_pb,
    g_block_size        => g_block_size
  )
  port map(
    clk               => clk,
    rst               => rst,
    in_sosi           => sosi_p_rp,  -- input data
    in_stop           => in_stop,
    out_sosi          => sosi_rp_ac,  -- output data
    out_data_stopped  => data_stopped_rp_ac
  );

  -- creates address by counting input valids
  u_ddrctrl_input_address_counter : entity work.ddrctrl_input_address_counter
  generic map(
    g_tech_ddr        => g_tech_ddr,  -- type of memory
    g_max_adr         => g_max_adr
  )
  port map(
    clk               => clk,
    rst               => rst_ddrctrl_input_ac,
    in_sosi           => sosi_rp_ac,  -- input data
    in_data_stopped   => data_stopped_rp_ac,
    out_sosi          => out_sosi,  -- output data
    out_adr           => adr,
    out_bsn_adr       => out_bsn_adr,
    out_data_stopped  => out_data_stopped
  );
end str;
