-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, tst_lib, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Test bench for common_transpose_symbol.vhd
-- Usage:
-- > as 3
-- > run -all
--   p_verify self-checks the output of two time the transpose

entity tb_common_transpose_symbol is
  generic (
    g_pipeline   : natural := 1;
    g_nof_data   : natural := 4;
    g_data_w     : natural := 12
  );
end tb_common_transpose_symbol;

architecture tb of tb_common_transpose_symbol is
  constant clk_period   : time := 10 ns;

  constant c_symbol_w   : natural := g_data_w / g_nof_data;

  constant c_frame_len  : natural := 17;
  constant c_frame_sop  : natural := 1;
  constant c_frame_eop  : natural := (c_frame_sop + c_frame_len - 1) mod c_frame_len;

  -- Stimuli
  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '1';

  signal in_val         : std_logic;
  signal in_dat         : std_logic_vector(           g_data_w - 1 downto 0) := (others => '0');
  signal in_sop         : std_logic;
  signal in_eop         : std_logic;

  signal in_data_vec    : std_logic_vector(g_nof_data * g_data_w - 1 downto 0);

  -- DUT output
  signal trans_val      : std_logic;
  signal trans_sop      : std_logic;
  signal trans_eop      : std_logic;
  signal trans_data_vec : std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
  signal out_data_vec   : std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_eop        : std_logic;

  -- Verify
  signal exp_val        : std_logic;
  signal exp_sop        : std_logic;
  signal exp_eop        : std_logic;
  signal exp_data_vec   : std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  tb_end <= '1' when signed(in_dat) = -1 else '0';

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      in_val <= '0';
      in_dat <= (others => '0');
    elsif rising_edge(clk) then
      in_val <= '1';
      in_dat <= std_logic_vector(signed(in_dat) + 1);
    end if;
  end process;

  gen_vec: for I in g_nof_data - 1 downto 0 generate
    in_data_vec((I + 1) * g_data_w - 1 downto I * g_data_w) <= INCR_UVEC(in_dat, I * 2**c_symbol_w);
  end generate;

  in_sop <= in_val when TO_UINT(in_dat) mod c_frame_len = c_frame_sop else '0';
  in_eop <= in_val when TO_UINT(in_dat) mod c_frame_len = c_frame_eop else '0';

  -- DUT
  u_transpose_in : entity work.common_transpose_symbol
  generic map (
    g_pipeline  => g_pipeline,
    g_nof_data  => g_nof_data,
    g_data_w    => g_data_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => in_data_vec,
    in_val     => in_val,
    in_sop     => in_sop,
    in_eop     => in_eop,

    out_data   => trans_data_vec,
    out_val    => trans_val,
    out_sop    => trans_sop,
    out_eop    => trans_eop
  );

  u_transpose_out : entity work.common_transpose_symbol
  generic map (
    g_pipeline  => g_pipeline,
    g_nof_data  => g_nof_data,
    g_data_w    => g_data_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => trans_data_vec,
    in_val     => trans_val,
    in_sop     => trans_sop,
    in_eop     => trans_eop,

    out_data   => out_data_vec,
    out_val    => out_val,
    out_sop    => out_sop,
    out_eop    => out_eop
  );

  -- Verification
  p_verify : process(rst, clk)
  begin
    if rst = '1' then
    elsif rising_edge(clk) then
      if exp_data_vec /= out_data_vec then report "Unexpected out_data_vec" severity ERROR; end if;
      if exp_val      /= out_val      then report "Unexpected out_val"      severity ERROR; end if;
      if exp_sop      /= out_sop      then report "Unexpected out_sop"      severity ERROR; end if;
      if exp_eop      /= out_eop      then report "Unexpected out_eop"      severity ERROR; end if;
    end if;
  end process;

  -- pipeline data input
  u_out_dat : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline * 2,
    g_in_dat_w  => g_nof_data * g_data_w,
    g_out_dat_w => g_nof_data * g_data_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_data_vec,
    out_dat => exp_data_vec
  );

  -- pipeline control input
  u_out_val : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline * 2
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_val,
    out_dat => exp_val
  );

  u_out_sop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline * 2
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sop,
    out_dat => exp_sop
  );

  u_out_eop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline * 2
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_eop,
    out_dat => exp_eop
  );
end tb;
