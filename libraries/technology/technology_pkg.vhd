-------------------------------------------------------------------------------
--
-- Copyright (C) 2014-20243
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Purpose: Define the list of FPGA technology identifiers
-- Description:
--   The technology dependent IP is organised per FPGA device type. Each FPGA
--   type that is supported has a c_tech_<device_name> identifier constant.
-- Remark:
-- . The package also contains some low level functions that often are copied
--   from common_pkg.vhd. They need to be redefined in this technology_pkg.vhd
--   because the common_lib also use technology dependent IP like RAM, FIFO,
--   DDIO. Therefore common_lib can not be used in the IP wrappers for those
--   IP blocks, because common_lib is compiled later.
-- . For technology wrappers that are not used by components in common_lib the
--   common_pkg.vhd can be used. Similar technology wrappers that are not used
--   by components in dp_lib can use the dp_stream_pkg.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.math_real.all;

package technology_pkg is
  -- Technology identifiers
  constant c_tech_inferred           : integer := 0;
  constant c_tech_virtex4            : integer := 1;  -- e.g. used on RSP3 for Lofar
  constant c_tech_stratixiv          : integer := 2;  -- e.g. used on UniBoard1
  constant c_tech_virtex6            : integer := 3;  -- e.g. used on Roach2 for Casper
  constant c_tech_virtex7            : integer := 4;  -- e.g. used on Roach3 for Casper
  constant c_tech_arria10_proto      : integer := 5;  -- e.g. used on UniBoard2 first proto (1 board version "00" may 2015)
  constant c_tech_arria10_e3sge3     : integer := 6;  -- e.g. used on UniBoard2 second run (7 boards version "01" dec 2015)
  constant c_tech_arria10_e1sg       : integer := 7;  -- e.g. used on UniBoard2b third run (5 ARTS boards version "01" feb 2017)
  constant c_tech_arria10_e2sg       : integer := 8;  -- e.g. used on UniBoard2c (2 LOFAR2.0 SDP boards version "11" f 2021)
  constant c_tech_ultrascale         : integer := 9;  -- e.g. used on Alveo FPGA platforms
  constant c_tech_agi027_1e1v        : integer := 10; -- e.g. used on iWave Intel Agilex 7 for ALMA (porting pfb from arrial10_e2sg version "01" jan 2024)
  constant c_tech_nof_technologies   : integer := 11;

  -- Functions
  function tech_sel_a_b(sel : boolean; a, b : string)  return string;
  function tech_sel_a_b(sel : boolean; a, b : integer) return integer;

  function tech_true_log2(n : natural) return natural;  -- tech_true_log2(n) = log2(n)
  function tech_ceil_log2(n : natural) return natural;  -- tech_ceil_log2(n) = log2(n), but force tech_ceil_log2(1) = 1

  function tech_ceil_div(n, d : natural) return natural;  -- tech_ceil_div    = n/d + (n MOD d)/=0

  function tech_nat_to_mbps_str( n : in natural ) return string;
end technology_pkg;

package body technology_pkg is
  function tech_sel_a_b(sel : boolean; a, b : string) return string is
  begin
    if sel = true then return a; else return b; end if;
  end;

  function tech_sel_a_b(sel : boolean; a, b : integer) return integer is
  begin
    if sel = true then return a; else return b; end if;
  end;

  function tech_true_log2(n : natural) return natural is
  -- Purpose: For calculating extra vector width of existing vector
  -- Description: Return mathematical ceil(log2(n))
  --   n    log2()
  --   0 -> -oo  --> FAILURE
  --   1 ->  0
  --   2 ->  1
  --   3 ->  2
  --   4 ->  2
  --   5 ->  3
  --   6 ->  3
  --   7 ->  3
  --   8 ->  3
  --   9 ->  4
  --   etc, up to n = NATURAL'HIGH = 2**31-1
  begin
    return natural(integer(ceil(log2(real(n)))));
  end;

  function tech_ceil_log2(n : natural) return natural is
  -- Purpose: For calculating vector width of new vector
  -- Description:
  --   Same as tech_true_log2() except tech_ceil_log2(1) = 1, which is needed to support
  --   the vector width width for 1 address, to avoid NULL array for single
  --   word register address.
  begin
    if n = 1 then
      return 1;  -- avoid NULL array
    else
      return tech_true_log2(n);
    end if;
  end;

  function tech_ceil_div(n, d : natural) return natural is
  begin
    return n / d + tech_sel_a_b(n mod d = 0, 0, 1);
  end;

  function tech_nat_to_mbps_str( n : in natural ) return string is  -- Converts a selection of naturals to Mbps strings, used for edited MegaWizard file in ip_stratixiv_hssi_*_generic.vhd
    variable r : string(1 to 9);
  begin
    case n is
    when 2500 => r := "2500 Mbps";
    when 3125 => r := "3125 Mbps";
    when 5000 => r := "5000 Mbps";
    when 6250 => r := "6250 Mbps";
    when others =>
      r := "ERROR: tech_nat_to_mbps_str UNSUPPORTED DATA RATE";  -- This too long string will cause an error in Quartus synthesis
      report r
        severity FAILURE;  -- Severity Failure will stop the Modelsim simulation
    end case;
    return r;
  end;

end technology_pkg;
