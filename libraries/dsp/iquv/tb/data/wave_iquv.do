onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -height 15 /tb_iquv/valid_enable
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_i/u_output_pipe/rst
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_i/u_output_pipe/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_i/u_output_pipe/clken
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_i/u_output_pipe/in_clr
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_i/u_output_pipe/in_en
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_i/u_output_pipe/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_i/u_output_pipe/out_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_i/u_output_pipe/out_dat_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/clken
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/sel_add
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/in_a
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/in_b
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/result
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/in_a_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/in_b_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/in_add
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/sel_add_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_i -radix unsigned /tb_iquv/dut/u_adder_i/result_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_q/u_output_pipe/rst
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_q/u_output_pipe/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_q/u_output_pipe/clken
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_q/u_output_pipe/in_clr
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_q/u_output_pipe/in_en
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_q/u_output_pipe/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_q/u_output_pipe/out_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -group u_output_pipe -radix unsigned /tb_iquv/dut/u_adder_q/u_output_pipe/out_dat_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/clken
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/sel_add
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/in_a
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/in_b
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/result
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/in_a_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/in_b_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/in_add
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/sel_add_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_adder_q -radix unsigned /tb_iquv/dut/u_adder_q/result_p
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/rst
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/clken
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/cnt_clr
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/cnt_ld
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/cnt_en
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/cnt_max
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/load
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/count
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/reg_count
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/nxt_count
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_ctrl_count -radix unsigned /tb_iquv/dut/u_ctrl_count/comb_count
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_imag -radix unsigned /tb_iquv/dut/u_delay_input_imag/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_imag -radix unsigned /tb_iquv/dut/u_delay_input_imag/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_imag -radix unsigned /tb_iquv/dut/u_delay_input_imag/out_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_real -radix unsigned /tb_iquv/dut/u_delay_input_real/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_real -radix unsigned /tb_iquv/dut/u_delay_input_real/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_real -radix unsigned /tb_iquv/dut/u_delay_input_real/out_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_valid -radix unsigned /tb_iquv/dut/u_delay_input_valid/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_valid -radix unsigned /tb_iquv/dut/u_delay_input_valid/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_input_valid -radix unsigned /tb_iquv/dut/u_delay_input_valid/out_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_xy_imag -radix unsigned /tb_iquv/dut/u_delay_xy_imag/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_xy_imag -radix unsigned /tb_iquv/dut/u_delay_xy_imag/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_xy_imag -radix unsigned /tb_iquv/dut/u_delay_xy_imag/out_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_xy_real -radix unsigned /tb_iquv/dut/u_delay_xy_real/clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_xy_real -radix unsigned /tb_iquv/dut/u_delay_xy_real/in_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -group u_delay_xy_real -radix unsigned /tb_iquv/dut/u_delay_xy_real/out_dat
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned -expand -subitemconfig {/tb_iquv/dut/in_complex.sync {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.bsn {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.data {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.re {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.im {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.valid {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.sop {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.eop {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.empty {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.channel {-height 15 -radix unsigned} /tb_iquv/dut/in_complex.err {-height 15 -radix unsigned}} /tb_iquv/dut/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/in_complex_valid
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/in_complex_valid_d1
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/in_complex_re_d1
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/in_complex_im_d1
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/ctrl_count
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 /tb_iquv/dut/enable_strobe
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/products_valid
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 /tb_iquv/dut/products_valid_vec
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/product_xx
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/product_xy_re
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/product_xy_im
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/product_yy
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/i_result
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/q_result
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/u_result
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -group dut -height 15 -radix unsigned /tb_iquv/dut/v_result
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -expand -subitemconfig {/tb_iquv/dut/i_out_raw_sosi.sync {-height 15} /tb_iquv/dut/i_out_raw_sosi.bsn {-height 15} /tb_iquv/dut/i_out_raw_sosi.data {-height 15} /tb_iquv/dut/i_out_raw_sosi.re {-height 15} /tb_iquv/dut/i_out_raw_sosi.im {-height 15} /tb_iquv/dut/i_out_raw_sosi.valid {-height 15} /tb_iquv/dut/i_out_raw_sosi.sop {-height 15} /tb_iquv/dut/i_out_raw_sosi.eop {-height 15} /tb_iquv/dut/i_out_raw_sosi.empty {-height 15} /tb_iquv/dut/i_out_raw_sosi.channel {-height 15} /tb_iquv/dut/i_out_raw_sosi.err {-height 15}} /tb_iquv/dut/i_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 /tb_iquv/dut/q_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 /tb_iquv/dut/u_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 /tb_iquv/dut/v_out_raw_sosi
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned -expand -subitemconfig {/tb_iquv/dut/i_out.sync {-height 15 -radix unsigned} /tb_iquv/dut/i_out.bsn {-height 15 -radix unsigned} /tb_iquv/dut/i_out.data {-height 15 -radix unsigned} /tb_iquv/dut/i_out.re {-height 15 -radix unsigned} /tb_iquv/dut/i_out.im {-height 15 -radix unsigned} /tb_iquv/dut/i_out.valid {-height 15 -radix unsigned} /tb_iquv/dut/i_out.sop {-height 15 -radix unsigned} /tb_iquv/dut/i_out.eop {-height 15 -radix unsigned} /tb_iquv/dut/i_out.empty {-height 15 -radix unsigned} /tb_iquv/dut/i_out.channel {-height 15 -radix unsigned} /tb_iquv/dut/i_out.err {-height 15 -radix unsigned}} /tb_iquv/dut/i_out
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/dut/q_out
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/dut/u_out
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/dut/v_out
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix decimal /tb_iquv/diag_i_out_data
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix decimal /tb_iquv/diag_q_out_data
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix decimal /tb_iquv/diag_u_out_data
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix decimal /tb_iquv/diag_v_out_data
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 /tb_iquv/diag_out_valid
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 /tb_iquv/random0
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/tb_end
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/dp_rst
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/dp_clk
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/in_complex
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/i_out
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/q_out
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/u_out
add wave -noupdate -height 15 -expand -group tb_iquv -height 15 -radix unsigned /tb_iquv/v_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {246952314 fs} 0}
configure wave -namecolwidth 219
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {170391699 fs} {341180763 fs}
