-------------------------------------------------------------------------------
-- Title      : Common Type Declarations
-- Project    :
-------------------------------------------------------------------------------
-- File       : common_types_pkg.vhd
-- Author     : William Kamp  <william.kamp@aut.ac.nz>
-- Company    : High Performance Computing Research Lab, Auckland University of Technology
-- Created    : 2015-12-07
-- Last update: 2018-01-01
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2015 High Performance Computing Research Lab, Auckland University of Technology
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2015-12-07  1.0      wkamp   Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_pkg.all;

package common_types_pkg is

    type t_natural_a is array (integer range<>) of natural;
    type t_integer_a is array (integer range<>) of integer;
    type t_boolean_a is array (integer range<>) of boolean;

    type slv_a is array (natural range <>) of std_logic_vector;
    -- Standard powers of two
    subtype slv_1_a is slv_a (open)(0 downto 0);
    subtype slv_2_a is slv_a (open)(1 downto 0);
    subtype slv_4_a is slv_a (open)(3 downto 0);
    subtype slv_8_a is slv_a (open)(7 downto 0);
    subtype slv_16_a is slv_a (open)(15 downto 0);
    subtype slv_32_a is slv_a (open)(31 downto 0);
    subtype slv_64_a is slv_a (open)(63 downto 0);
    subtype slv_128_a is slv_a (open)(127 downto 0);
    subtype slv_256_a is slv_a (open)(255 downto 0);

    type unsigned_a is array (natural range <>) of unsigned;
    -- Unsigned powers of two vectors
    subtype unsigned_1_a is unsigned_a (open)(0 downto 0);
    subtype unsigned_2_a is unsigned_a (open)(1 downto 0);
    subtype unsigned_4_a is unsigned_a (open)(3 downto 0);
    subtype unsigned_8_a is unsigned_a (open)(7 downto 0);
    subtype unsigned_16_a is unsigned_a (open)(15 downto 0);
    subtype unsigned_32_a is unsigned_a (open)(31 downto 0);
    subtype unsigned_64_a is unsigned_a (open)(63 downto 0);
    subtype unsigned_128_a is unsigned_a (open)(127 downto 0);
    subtype unsigned_256_a is unsigned_a (open)(255 downto 0);

    type signed_a is array (natural range <>) of signed;
    -- signed powers of two vectors
    subtype signed_1_a   is signed_a (open)(0 downto 0);
    subtype signed_2_a   is signed_a (open)(1 downto 0);
    subtype signed_4_a   is signed_a (open)(3 downto 0);
    subtype signed_8_a   is signed_a (open)(7 downto 0);
    subtype signed_16_a  is signed_a (open)(15 downto 0);
    subtype signed_32_a  is signed_a (open)(31 downto 0);
    subtype signed_64_a  is signed_a (open)(63 downto 0);
    subtype signed_128_a is signed_a (open)(127 downto 0);
    subtype signed_256_a is signed_a (open)(255 downto 0);

    type sfixed_a is array (natural range <>) of sfixed;
    type ufixed_a is array (natural range <>) of ufixed;
    
    -- Stratix5 transceiver interfaces.
    subtype slv_80_a is slv_a (open)(79 downto 0);
    subtype slv_92_a is slv_a (open)(91 downto 0);
    subtype slv_140_a is slv_a (open)(139 downto 0);

    function to_slv(slv_array : slv_a; big_endian : boolean := true) return std_logic_vector;
    function from_slv(slv     : std_logic_vector; width : natural := 64; big_endian : boolean := true) return slv_a;

end package common_types_pkg;

package body common_types_pkg is

    function to_slv(
        slv_array : slv_a; 
        big_endian : boolean := true) 
    return std_logic_vector is
        variable slv    : std_logic_vector(slv_array'length * slv_array(slv_array'left)'length - 1 downto 0);
        variable hi     : integer;
        constant stride : natural := slv_array(slv_array'left)'length;
    begin
        hi := slv'high;
        for idx in slv_array'range loop
            if big_endian then
                slv(hi downto hi + 1 - stride) := slv_array(idx);
                hi                             := hi - stride;
            else
                slv(idx * stride + stride - 1 downto idx * stride) := slv_array(idx);
            end if;
        end loop;
        return slv;
    end function to_slv;

    function from_slv(
        slv   : std_logic_vector;
        width : natural := 64;
        big_endian : boolean := true)
    return slv_a is
        variable slvz      : std_logic_vector(slv'length - 1 downto 0);
        variable slv_array : slv_a(0 to slv'length / width - 1)(width - 1 downto 0);
        variable hi        : integer;
    begin
        slvz := slv;
        hi   := slvz'high;
        for idx in slv_array'range loop
            if big_endian then
                slv_array(idx) := slvz(hi downto hi + 1 - width);
                hi             := hi - width;
            else
                slv_array(idx) := slvz(idx*width + width - 1 downto idx*width);
            end if;
        end loop;
        return slv_array;
    end function from_slv;

end package body common_types_pkg;