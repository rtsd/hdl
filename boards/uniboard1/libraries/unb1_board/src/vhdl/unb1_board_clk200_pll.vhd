-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, tech_pll_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_pkg.all;

-- Purpose: PLL for UniBoard node CLK input @ 200 MHz
-- Description:
-- . The PLL runs in normal mode using c0 to compensate for the internal clock
--   network delay, so that c0 = st_clk200 is aligned to the input clk200.
-- . The assumption is that default the streaming DSP will run on the 200 MHz
--   clock from the CLK input via c1 = st_clk200p.
-- . The PLL normal mode operation compensates for internal clock network
--   delays of c0. This compensations aligns c0 to inclk0. With
--   tb_unb1_board_clk200_pll.vhd it appears that the phase setting for c0 does
--   not influence the compensation. Therefore it is llso possible to use
--   g_clk200_phase_shift /= 0 and touse c0 as processing clock instead of c1.
-- . The phase offset of c0 and c1 in the clk200_pll MegaWizard component
--   can be set in steps of 11.25 degrees (and even finer):
--                       g_clk200_phase_shift  (for c0)
--     phase [degrees]   g_clk200p_phase_shift (for c1)
--       0                 "0"
--       11.25             "156"
--       22.5              "313"
--       33.75             "469"
--       45                "625"
--       56.25             "781"
--       67.5              "938"
--       78.75             "1094"
--       90                "1250"
--      101.25             "1406"  = 1250+ 156
--      112.5              "1563"  = 1250+ 313
--      123.75             "1719"  = 1250+ 469
--      135                "1875"  = 1250+ 625
--      146.25             "2031"  = 1250+ 781
--      157.5              "2188"  = 1250+ 938
--      168.75             "2344"  = 1250+1094
--      180                "2500"  = 1250+1250
--      191.25             "2656"  = 2500+ 156
--      202.5              "2813"  = 2500+ 313
--      213.75             "2969"  = 2500+ 469
--      225                "3125"  = 2500+ 625
--      236.25             "3281"  = 2500+ 781
--      247.5              "3438"  = 2500+ 938
--      258.75             "3594"  = 2500+1094
--      270                "3750"  = 2500+1250
--      281.25             "3906"  = 3750+ 156
--      292.5              "4063"  = 3750+ 313
--      303.75             "4219"  = 3750+ 469
--      315                "4375"  = 3750+ 625
--      326.25             "4531"  = 3750+ 781
--      337.5              "4688"  = 3750+ 938
--      348.75             "4844"  = 3750+1094
--      360                "5000"  = 3750+1250
-- . With a phase offset of 22.5 degrees the c1 = clk200p is offset by a 1/16
--   period of 200 MHz, so 1/8 period of the 400 MHz DCLK from ADU and 1/4
--   period of the 800 MHz sample SCLK of ADU. This phase offset can be used
--   to achieve stable timing between the DCLK and the clk200p domain.
-- . Some DSP may also be possible at 400 MHz via st_clk400. Note that this
--   400 MHz can also be used at places where only a little more than 200 MHz
--   would be needed, e.g. to create packets at full data rate.
--   Therefore it is not necessary to create yet another st clock frequency.
--   This to also avoid the EMI or RFI that a non integer factor of 200 MHz
--   like e.g. 250 MHz would cause.
-- . At 400 MHz ADC samples can be clocked in at 800 MSps using DDIO. At 800
--   MSps the sample period is 1250 ns. Input timing can be tuned via fixed
--   pad input delays and/or by using another phase of the PLL output clock.
-- Remarks:
-- . If necessary more 400 M clock phase could be made available, via g_sel.
--

entity unb1_board_clk200_pll is
  generic (
    g_technology          : natural := c_tech_stratixiv;
    g_sel                 : natural := 0;
    g_operation_mode      : string  := "NORMAL";  -- "NORMAL", "NO_COMPENSATION", or "SOURCE_SYNCHRONOUS" --> requires PLL_COMPENSATE assignment to an input pin to compensate for (stratixiv)

    -- g_sel=0 for clk200_pll.vhd
    g_clk200_phase_shift  : string := "0";  -- default use 0 degrees, see clk200_pll.vhd for other phase values
    g_clk200p_phase_shift : string := "0";  -- default use 0 degrees, see clk200_pll.vhd for other phase values

    -- g_sel=1 for clk200_pll_p6.vhd
    g_clk0_phase_shift    : string :=    "0";  -- = 000    = st_clk_vec[0] = st_clk200
    g_clk_vec_w           : natural := 6;  -- clk200_pll_p6.vhd and supports up to 6 extra output clocks, e.g. with phase 0 to 90 in steps of 11.25 degrees, and with same or lower frequency
    g_clk1_phase_shift    : string :=    "0";  -- = 000    = st_clk_vec[1] = st_clk200p
    g_clk2_phase_shift    : string :=  "156";  -- = 011.25 = st_clk_vec[2]
    g_clk3_phase_shift    : string :=  "313";  -- = 022.5  = st_clk_vec[3]
    g_clk4_phase_shift    : string :=  "469";  -- = 033.75 = st_clk_vec[4]
    g_clk5_phase_shift    : string :=  "625";  -- = 045    = st_clk_vec[5]
                                   --  "781";  -- = 056.25
    g_clk6_phase_shift    : string :=  "938";  -- = 067.5  = st_clk_vec[6]
                                   -- "1094";  -- = 078.75
    g_clk1_divide_by      : natural := 32;  -- = clk 200/32 MHz
    g_clk2_divide_by      : natural := 32;  -- = clk 200/32 MHz
    g_clk3_divide_by      : natural := 32;  -- = clk 200/32 MHz
    g_clk4_divide_by      : natural := 32;  -- = clk 200/32 MHz
    g_clk5_divide_by      : natural := 32;  -- = clk 200/32 MHz
    g_clk6_divide_by      : natural := 32  -- = clk 200/32 MHz
  );
  port (
    -- It depends on g_sel which outputs are actually available
    -- . common
    arst        : in  std_logic := '0';
    clk200      : in  std_logic := '0';  -- connect to UniBoard CLK pin
    st_clk200   : out std_logic;  -- PLL c0 = g_clk200_phase_shift  degrees phase offset to input clk200
    st_rst200   : out std_logic;
    -- . g_sel=0
    st_clk200p  : out std_logic;  -- PLL c1 = g_clk200p_phase_shift degrees phase offset to input clk200 (see clk200_pll.vhd from MegaWizard)
    st_rst200p  : out std_logic;
    st_clk400   : out std_logic;  -- PLL c2 = 0                     degrees phase offset to input clk200
    st_rst400   : out std_logic;
    -- . g_sel=1
    st_clk_vec  : out std_logic_vector(g_clk_vec_w - 1 downto 0)  -- PLL c6-c1
  );
end unb1_board_clk200_pll;

architecture stratix4 of unb1_board_clk200_pll is
  constant c_reset_len : natural := c_meta_delay_len;

  constant c_clk1_used : string := sel_a_b(g_clk_vec_w > 0, "PORT_USED", "PORT_UNUSED");
  constant c_clk2_used : string := sel_a_b(g_clk_vec_w > 1, "PORT_USED", "PORT_UNUSED");
  constant c_clk3_used : string := sel_a_b(g_clk_vec_w > 2, "PORT_USED", "PORT_UNUSED");
  constant c_clk4_used : string := sel_a_b(g_clk_vec_w > 3, "PORT_USED", "PORT_UNUSED");
  constant c_clk5_used : string := sel_a_b(g_clk_vec_w > 4, "PORT_USED", "PORT_UNUSED");
  constant c_clk6_used : string := sel_a_b(g_clk_vec_w > 5, "PORT_USED", "PORT_UNUSED");

  signal i_st_rst200  : std_logic;
  signal i_st_clk200  : std_logic;
  signal i_st_clk200p : std_logic;
  signal i_st_clk400  : std_logic;
  signal i_st_clk_vec : std_logic_vector(5 downto 0) := (others => '0');

  signal st_locked    : std_logic;
  signal st_locked_n  : std_logic;
begin
  st_rst200  <= i_st_rst200;

  st_clk200  <= i_st_clk200;
  st_clk200p <= i_st_clk200p;
  st_clk400  <= i_st_clk400;
  st_clk_vec <= i_st_clk_vec(g_clk_vec_w - 1 downto 0);

  gen_0 : if g_sel = 0 generate
    u_st_pll : entity tech_pll_lib.tech_pll_clk200
    generic map (
      g_technology       => g_technology,
      g_operation_mode   => g_operation_mode,
      g_clk0_phase_shift => g_clk200_phase_shift,
      g_clk1_phase_shift => g_clk200p_phase_shift
    )
    port map (
      areset  => arst,
      inclk0  => clk200,
      c0      => i_st_clk200,
      c1      => i_st_clk200p,
      c2      => i_st_clk400,
      locked  => st_locked
    );
  end generate;

  gen_1 : if g_sel = 1 generate
    i_st_clk200p <= i_st_clk_vec(0);

    u_st_pll_p6 : entity tech_pll_lib.tech_pll_clk200_p6
    generic map (
      g_technology       => g_technology,
      g_operation_mode   => g_operation_mode,
      g_clk0_phase_shift => g_clk0_phase_shift,
      g_clk1_used        => c_clk1_used,
      g_clk2_used        => c_clk2_used,
      g_clk3_used        => c_clk3_used,
      g_clk4_used        => c_clk4_used,
      g_clk5_used        => c_clk5_used,
      g_clk6_used        => c_clk6_used,
      g_clk1_divide_by   => g_clk1_divide_by,
      g_clk2_divide_by   => g_clk2_divide_by,
      g_clk3_divide_by   => g_clk3_divide_by,
      g_clk4_divide_by   => g_clk4_divide_by,
      g_clk5_divide_by   => g_clk5_divide_by,
      g_clk6_divide_by   => g_clk6_divide_by,
      g_clk1_phase_shift => g_clk1_phase_shift,
      g_clk2_phase_shift => g_clk2_phase_shift,
      g_clk3_phase_shift => g_clk3_phase_shift,
      g_clk4_phase_shift => g_clk4_phase_shift,
      g_clk5_phase_shift => g_clk5_phase_shift,
      g_clk6_phase_shift => g_clk6_phase_shift
    )
    port map (
      areset  => arst,
      inclk0  => clk200,
      c0      => i_st_clk200,
      c1      => i_st_clk_vec(0),
      c2      => i_st_clk_vec(1),
      c3      => i_st_clk_vec(2),
      c4      => i_st_clk_vec(3),
      c5      => i_st_clk_vec(4),
      c6      => i_st_clk_vec(5),
      locked  => st_locked
    );
  end generate;

  -- Release clock domain resets after some clock cycles when the PLL has locked
  st_locked_n <= not st_locked;

  u_rst200 : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => st_locked_n,
    clk       => i_st_clk200,
    out_rst   => i_st_rst200
  );

  u_rst200p : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => st_locked_n,
    clk       => i_st_clk200p,
    out_rst   => st_rst200p
  );

  u_rst400 : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => st_locked_n,
    clk       => i_st_clk400,
    out_rst   => st_rst400
  );
end stratix4;
