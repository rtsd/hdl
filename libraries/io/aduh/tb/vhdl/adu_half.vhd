-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use i2c_lib.i2c_dev_adu_pkg.all;

-- Purpose: Behavioral model of the half ADU including the rewiring on the
--          backplane (AUB)
-- Description:
-- . The half ADU consists of one adc08d1020 which has two ADCs that output
--   via DI[7:0] and DQ[7:0]. The DI port is wired default, but the DQ port
--   is cross-wired on the ADU-UniBoard backplane PCB to ease layout.
-- . Models the I2C peripherals on ADU

entity adu_half is
  generic (
    g_dclk_init_phase : natural := 0  -- When 0 then use default phase for DCLK, else use the other DDR phase
  );
  port (
    AI              : in  integer := 0;
    AQ              : in  integer := 0;
    AOVR            : in  std_logic := '0';
    CLK             : in  std_logic := '0';
    DCLK            : out std_logic;
    DCLK_RST        : in  std_logic := '1';  -- inverted
    DI              : out std_logic_vector(7 downto 0);
    DQ              : out std_logic_vector(7 downto 0);
    OVR             : out std_logic;
    SCL             : inout std_logic;
    SDA             : inout std_logic;

    test_pattern_en : in std_logic := '0';
    lvds_skew_di    : in t_natural_arr(7 downto 0) := (others => 0);  -- ps unit
    lvds_skew_dq    : in t_natural_arr(7 downto 0) := (others => 0);  -- ps unit
    lvds_skew_ovr   : in natural := 0;  -- ps unit
    lvds_skew_dclk  : in natural := 0  -- ps unit
  );
end adu_half;

architecture beh of adu_half is
  constant c_dclk_rst_invert : boolean := false;  -- Default FALSE because DCLK_RST on ADC is active high, use TRUE to model for a P/N cross

  -- Model I2C slaves on the bus
  constant c_max1618_address      : std_logic_vector := TO_UVEC(I2C_ADU_MAX1617_ADR, 7);  -- MAX1618 address MID MID
  constant c_max1618_temp         : integer := 60;
  constant c_io_expander_address  : std_logic_vector := TO_UVEC(I2C_ADU_PCA9555_ADR, 7);  -- ADR_PCA9555 address

  signal DI_rewire : std_logic_vector(7 downto 0);
  signal DQ_rewire : std_logic_vector(7 downto 0);

  signal DCLK_RST_rewire : std_logic;

  -- IO expander
  signal iobank0           : std_logic_vector(7 downto 0);
  signal iobank1           : std_logic_vector(7 downto 0);

  -- IO expander ADU
  signal adu_sclk              : std_logic;
  signal adu_sdata             : std_logic;
  signal adu_led_scs_n         : std_logic;
  signal adu_cal_adc           : std_logic;
  signal adu_pwr_ctrl          : std_logic_vector(1 downto 0);
  signal adu_pwr_ctrl_1_dig    : std_logic;
  signal adu_pwr_ctrl_0_ana    : std_logic;
  signal adu_ctrl              : std_logic_vector(3 downto 0);
  signal adu_ctrl_0_atten_le   : std_logic;
  signal adu_ctrl_1_modem_on   : std_logic;
  signal adu_ctrl_2_atten_le   : std_logic;
  signal adu_ctrl_3_modem_on   : std_logic;

  signal adu_atten_ctrl        : std_logic_vector(5 downto 0);
begin
  -- I port A and C wire default
  DI <= DI_rewire;

  -- Q port B and D are rewired on AUB and on the single BN-ADU connector board:
  DQ(0) <= DQ_rewire(6);
  DQ(1) <= DQ_rewire(7);
  DQ(2) <= DQ_rewire(4);
  DQ(3) <= DQ_rewire(5);
  DQ(4) <= DQ_rewire(2);
  DQ(5) <= DQ_rewire(3);
  DQ(6) <= DQ_rewire(0);
  DQ(7) <= DQ_rewire(1);

  -- Model swapped P/N pins at the UniBoard back node by a NOT, see also in BACK_NODE_adc_pins.tcl and aduh_dd.vhd
  DCLK_RST_rewire <= DCLK_RST when c_dclk_rst_invert = false else not DCLK_RST;

  u_adc : entity work.adc08d1020
  generic map (
    g_dclk_init_phase => g_dclk_init_phase
  )
  port map (
    AI              => AI,
    AQ              => AQ,
    AOVR            => AOVR,
    CLK             => CLK,
    DCLK            => DCLK,
    DCLK_RST        => DCLK_RST_rewire,
    DI              => DI_rewire,
    DQ              => DQ_rewire,
    OVR             => OVR,

    test_pattern_en => test_pattern_en,
    lvds_skew_di    => lvds_skew_di,
    lvds_skew_dq    => lvds_skew_dq,
    lvds_skew_ovr   => lvds_skew_ovr,
    lvds_skew_dclk  => lvds_skew_dclk
  );

  -- I2C bus
  SCL <= 'H';  -- model I2C pull up
  SDA <= 'H';  -- model I2C pull up

  u_sens_temp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_max1618_address
  )
  port map (
    scl  => SCL,
    sda  => SDA,
    temp => c_max1618_temp
  );

  u_io_expander : entity i2c_lib.dev_pca9555
  generic map (
    g_address => c_io_expander_address
  )
  port map (
    scl       => SCL,
    sda       => SDA,
    iobank0   => iobank0,
    iobank1   => iobank1
  );

  -- ADU interpretation of the IO expander outputs
  adu_sclk            <= iobank0(4);
  adu_sdata           <= iobank0(5);
  adu_led_scs_n       <= iobank0(7);
  adu_cal_adc         <= iobank1(7);
  adu_pwr_ctrl        <= iobank0(6) & iobank1(6);
  adu_pwr_ctrl_1_dig  <= adu_pwr_ctrl(1);
  adu_pwr_ctrl_0_ana  <= adu_pwr_ctrl(0);

  adu_ctrl            <= iobank0(3 downto 0);
  adu_ctrl_0_atten_le <= adu_ctrl(0);
  adu_ctrl_1_modem_on <= adu_ctrl(1);
  adu_ctrl_2_atten_le <= adu_ctrl(2);
  adu_ctrl_3_modem_on <= adu_ctrl(3);

  adu_atten_ctrl      <= iobank1(5 downto 0);
end beh;
