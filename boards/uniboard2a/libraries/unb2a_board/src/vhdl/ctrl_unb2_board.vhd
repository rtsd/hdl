-------------------------------------------------------------------------------
--
-- Copyright (C) 2012-2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide general control infrastructure
-- Usage: In a design <design_name>.vhd that consists of:
--   . mmm_<design_name>.vhd with a Nios2 and the MM bus and the peripherals
--   . ctrl_unb2_board.vhd with e.g. 1GbE, PPS, I2C, Remu, EPCS

library IEEE, common_lib, dp_lib, ppsh_lib, i2c_lib, technology_lib, tech_tse_lib, eth_lib, remu_lib, epcs_lib, tech_pll_lib, tech_clkbuf_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb2_board_pkg.all;
use i2c_lib.i2c_pkg.all;
use technology_lib.technology_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use eth_lib.eth_pkg.all;

entity ctrl_unb2_board is
  generic (
    ----------------------------------------------------------------------------
    -- General
    ----------------------------------------------------------------------------
    g_technology     : natural := c_tech_arria10;
    g_sim            : boolean := false;
    g_design_name    : string := "UNUSED";
    g_fw_version     : t_unb2_board_fw_version := (0, 0);  -- firmware version x.y
    g_stamp_date     : natural := 0;
    g_stamp_time     : natural := 0;
    g_stamp_svn      : natural := 0;
    g_design_note    : string  := "UNUSED";
    g_base_ip        : std_logic_vector(16 - 1 downto 0) := X"0A63";  -- Base IP address used by unb_osy: 10.99.xx.yy
    g_mm_clk_freq    : natural := c_unb2_board_mm_clk_freq_125M;
    g_eth_clk_freq   : natural := c_unb2_board_eth_clk_freq_125M;
    g_tse_clk_buf    : boolean := false;

    ----------------------------------------------------------------------------
    -- External CLK
    ----------------------------------------------------------------------------
    g_dp_clk_freq    : natural := c_unb2_board_ext_clk_freq_200M;
    g_dp_clk_use_pll : boolean := true;
    -- PLL phase clk shift with respect to CLK
    --     STRING :=    "0"             = 0
    --     STRING :=  "156"             = 011.25
    --     STRING :=  "313"             = 022.5
    --     STRING :=  "469"             = 033.75
    --     STRING :=  "625"             = 045
    --     STRING :=  "781"             = 056.25
    --     STRING :=  "938"             = 067.5
    --     STRING := "1094"             = 078.75
    --     STRING := "1250"             = 090
    --     STRING := "1406" = 1250+ 156 = 101.25
    --     STRING := "1563" = 1250+ 313 = 112.5
    --     STRING := "1719" = 1250+ 469 = 123.75
    --     STRING := "1875" = 1250+ 625 = 135
    --     STRING := "2031" = 1250+ 781 = 146.25
    --     STRING := "2188" = 1250+ 938 = 157.5
    --     STRING := "2344" = 1250+1094 = 168.75
    --     STRING := "2500" = 1250+1250 = 180
    --     STRING := "2656" = 2500+ 156 = 191.25
    --     STRING := "2813" = 2500+ 313 = 202.5
    --     STRING := "2969" = 2500+ 469 = 213.75
    --     STRING := "3125" = 2500+ 625 = 225
    --     STRING := "3281" = 2500+ 781 = 236.25
    --     STRING := "3438" = 2500+ 938 = 247.5
    --     STRING := "3594" = 2500+1094 = 258.75
    --     STRING := "3750" = 2500+1250 = 270
    --     STRING := "3906" = 3750+ 156 = 281.25
    --     STRING := "4063" = 3750+ 313 = 292.5
    --     STRING := "4219" = 3750+ 469 = 303.75
    --     STRING := "4375" = 3750+ 625 = 315
    --     STRING := "4531" = 3750+ 781 = 326.25
    --     STRING := "4688" = 3750+ 938 = 337.5
    --     STRING := "4844" = 3750+1094 = 348.75
    --     STRING := "5000" = 3750+1250 = 360
    g_dp_clk_phase         : string := "0";  -- phase offset for PLL c0, typically any phase is fine, do not use 225 +-30 degrees because there the PPS edge occurs

    ----------------------------------------------------------------------------
    -- 1GbE UDP offload
    ----------------------------------------------------------------------------
    g_udp_offload             : boolean := false;
    g_udp_offload_nof_streams : natural := c_eth_nof_udp_ports;

    ----------------------------------------------------------------------------
    -- Auxiliary Interface
    ----------------------------------------------------------------------------
    g_fpga_temp_high    : natural := 85;
    g_app_led_red       : boolean := false;  -- when TRUE use external LED control via app_led_red
    g_app_led_green     : boolean := false;  -- when TRUE use external LED control via app_led_green

    g_aux               : t_c_unb2_board_aux := c_unb2_board_aux;
    g_factory_image     : boolean := false;
    g_protect_addr_range: boolean := false;
    g_protected_addr_lo : natural := 0;  -- Byte address
    g_protected_addr_hi : natural := 41943039  -- Byte address, for UniBoard1 this is 640 sectors*256 pages*256 bytes -1 = 41943039
  );
  port (
    --
    -- >>> SOPC system with conduit peripheral MM bus
    --
    -- System
    cs_sim                 : out std_logic;

    xo_ethclk              : out std_logic;  -- 125 MHz ETH_CLK
    xo_rst                 : out std_logic;  -- reset in ETH_CLK domain released after few cycles
    xo_rst_n               : out std_logic;

    ext_clk200             : out std_logic;  -- 200 MHz CLK
    ext_rst200             : out std_logic;  -- reset in CLK clock domain released after mm_rst

    mm_clk                 : out std_logic;  -- MM clock from xo_ethclk PLL
    mm_rst                 : out std_logic;  -- reset in MM clock domain released after xo_ethclk PLL locked

    dp_rst                 : out std_logic;  -- reset in DP clock domain released after mm_rst and after CLK PLL locked in case g_dp_clk_use_pll=TRUE
    dp_clk                 : out std_logic;  -- 200 MHz DP clock from CLK system clock direct or via CLK PLL dependent on g_dp_clk_use_pll
    dp_pps                 : out std_logic;  -- PPS in dp_clk domain
    dp_rst_in              : in  std_logic;  -- externally wire OUT dp_rst to dp_rst_in to avoid delta cycle difference on dp_clk
    dp_clk_in              : in  std_logic;  -- externally wire OUT dp_clk to dp_clk_in to avoid delta cycle difference on dp_clk

    mb_I_ref_rst           : out std_logic;  -- reset in MB_I_REF_CLK domain released after mm_rst
    mb_II_ref_rst          : out std_logic;  -- reset in MB_II_REF_CLK domain released after mm_rst

    this_chip_id           : out std_logic_vector(c_unb2_board_nof_chip_w - 1 downto 0);  -- [1:0], so range 0-3 for PN
    this_bck_id            : out std_logic_vector(c_unb2_board_nof_uniboard_w - 1 downto 0);  -- [1:0] used out of ID[7:2] to index boards 3..0 in subrack

    app_led_red            : in  std_logic := '0';
    app_led_green          : in  std_logic := '1';

    -- PIOs
    pout_wdi               : in  std_logic;  -- Toggled by unb_osy; can be overriden by reg_wdi.

    -- Manual WDI override
    reg_wdi_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    reg_wdi_miso           : out t_mem_miso;

    -- REMU
    reg_remu_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    reg_remu_miso          : out t_mem_miso;

    -- EPCS read
    reg_dpmm_data_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dpmm_data_miso     : out t_mem_miso;
    reg_dpmm_ctrl_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dpmm_ctrl_miso     : out t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mmdp_data_miso     : out t_mem_miso;
    reg_mmdp_ctrl_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mmdp_ctrl_miso     : out t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    reg_epcs_miso          : out t_mem_miso;

    -- MM buses to/from mms_unb2_board_system_info
    reg_unb_system_info_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_unb_system_info_miso : out t_mem_miso;

    rom_unb_system_info_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    rom_unb_system_info_miso : out t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_unb_sens_miso      : out t_mem_miso;

    reg_unb_pmbus_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_unb_pmbus_miso     : out t_mem_miso;

    -- FPGA sensors
    reg_fpga_temp_sens_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_fpga_temp_sens_miso     : out t_mem_miso;
    reg_fpga_voltage_sens_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_fpga_voltage_sens_miso  : out t_mem_miso;

    -- PPSH
    reg_ppsh_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ppsh_miso          : out t_mem_miso;

    -- eth1g control&monitoring
    eth1g_mm_rst           : in  std_logic;
    eth1g_tse_mosi         : in  t_mem_mosi;  -- ETH TSE MAC registers
    eth1g_tse_miso         : out t_mem_miso;
    eth1g_reg_mosi         : in  t_mem_mosi;  -- ETH control and status registers
    eth1g_reg_miso         : out t_mem_miso;
    eth1g_reg_interrupt    : out std_logic;  -- Interrupt
    eth1g_ram_mosi         : in  t_mem_mosi;  -- ETH rx frame and tx frame memory
    eth1g_ram_miso         : out t_mem_miso;

    -- eth1g UDP streaming ports
    udp_tx_sosi_arr        : in  t_dp_sosi_arr(g_udp_offload_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
    udp_tx_siso_arr        : out t_dp_siso_arr(g_udp_offload_nof_streams - 1 downto 0);
    udp_rx_sosi_arr        : out t_dp_sosi_arr(g_udp_offload_nof_streams - 1 downto 0);
    udp_rx_siso_arr        : in  t_dp_siso_arr(g_udp_offload_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    --
    -- >>> Ctrl FPGA pins
    --
    -- GENERAL
    CLK                    : in    std_logic;  -- System Clock
    PPS                    : in    std_logic;  -- System Sync
    WDI                    : out   std_logic;  -- Watchdog Clear
    INTA                   : inout std_logic;  -- FPGA interconnect line
    INTB                   : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION                : in    std_logic_vector(g_aux.version_w - 1 downto 0);
    ID                     : in    std_logic_vector(g_aux.id_w - 1 downto 0);
    TESTIO                 : inout std_logic_vector(g_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC                : inout std_logic := 'Z';
    SENS_SD                : inout std_logic := 'Z';

    -- pmbus
    PMBUS_SC               : inout std_logic := 'Z';
    PMBUS_SD               : inout std_logic := 'Z';
    PMBUS_ALERT            : in    std_logic := '0';

    -- DDR reference clock domains reset creation
    MB_I_REF_CLK           : in    std_logic := '0';  -- 25 MHz
    MB_II_REF_CLK          : in    std_logic := '0';  -- 25 MHz

    -- 1GbE Control Interface
    ETH_CLK                : in    std_logic;  -- 125 MHz
    ETH_SGIN               : in    std_logic_vector(c_unb2_board_nof_eth - 1 downto 0) := (others => '0');
    ETH_SGOUT              : out   std_logic_vector(c_unb2_board_nof_eth - 1 downto 0)
  );
end ctrl_unb2_board;

architecture str of ctrl_unb2_board is
  constant c_rom_version : natural := 1;  -- Only increment when something changes to the register map of rom_system_info.

  constant c_reset_len   : natural := 4;  -- >= c_meta_delay_len from common_pkg
  constant c_mm_clk_freq : natural := sel_a_b(g_sim = false, g_mm_clk_freq, c_unb2_board_mm_clk_freq_10M);

  -- Clock and reset
  signal i_ext_clk200           : std_logic;
  signal ext_pps                : std_logic;

  signal common_areset_in_rst   : std_logic;

  signal i_xo_ethclk            : std_logic;
  signal i_xo_rst               : std_logic;
  signal i_mm_rst               : std_logic;
  signal i_mm_clk               : std_logic;
  signal mm_locked              : std_logic;
  signal mm_sim_clk             : std_logic := '1';
  signal epcs_clk               : std_logic := '1';
  signal clk125                 : std_logic := '1';
  signal clk100                 : std_logic := '1';
  signal clk50                  : std_logic := '1';

  signal mm_wdi                 : std_logic;
  signal eth1g_st_clk           : std_logic;
  signal eth1g_st_rst           : std_logic;

  signal mm_pulse_ms            : std_logic;
  signal mm_pulse_s             : std_logic;
  signal mm_board_sens_start    : std_logic;

  signal led_toggle             : std_logic;
  signal led_toggle_red         : std_logic;
  signal led_toggle_green       : std_logic;

  -- eth1g
  signal i_tse_clk              : std_logic;
  signal eth1g_led              : t_tech_tse_led;

  -- Manual WDI override
  signal wdi_override           : std_logic;

  -- Temperature alarm  (temp > g_fpga_temp_high)
  signal temp_alarm             : std_logic;

  -- UDP offload I/O
  signal eth1g_udp_tx_sosi_arr  : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_sosi_rst);
  signal eth1g_udp_tx_siso_arr  : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal eth1g_udp_rx_sosi_arr  : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal eth1g_udp_rx_siso_arr  : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_siso_rdy);

  attribute keep: boolean;
  attribute keep of led_toggle_red:   signal is true;
  attribute keep of led_toggle_green: signal is true;

  attribute maxfan : integer;
  attribute maxfan of dp_rst : signal is 1024;
begin
  ext_clk200 <= i_ext_clk200;
  xo_ethclk  <= i_xo_ethclk;
  xo_rst     <=     i_xo_rst;
  xo_rst_n   <= not i_xo_rst;
  mm_clk     <= i_mm_clk;
  mm_rst     <= i_mm_rst;

  -- Default leave unused INOUT tri-state
  INTA <= 'Z';
  INTB <= 'Z';

  TESTIO <= (others => 'Z');  -- Leave unused INOUT tri-state

  ext_pps <= PPS;  -- use more special name for PPS pin signal to ease searching for it in editor

  -----------------------------------------------------------------------------
  -- ext_clk200 = CLK
  -----------------------------------------------------------------------------
  i_ext_clk200 <= CLK;  -- use more special name for CLK pin signal to ease searching for it in editor, the external 200 MHz CLK as ext_clk200

  u_common_areset_ext : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => '0',  -- release reset after some clock cycles
    clk       => i_ext_clk200,
    out_rst   => ext_rst200
  );

  -----------------------------------------------------------------------------
  -- xo_ethclk = ETH_CLK
  -----------------------------------------------------------------------------

  i_xo_ethclk <= ETH_CLK;  -- use the ETH_CLK pin as xo_clk

  u_common_areset_xo : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => '0',  -- release reset after some clock cycles
    clk       => i_xo_ethclk,
    out_rst   => i_xo_rst
  );

  -----------------------------------------------------------------------------
  -- MB_I_REF_CLK  --> mb_I_ref_rst
  -- MB_II_REF_CLK --> mb_II_ref_rst
  -----------------------------------------------------------------------------

  u_common_areset_mb_I : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => i_mm_rst,  -- release reset some clock cycles after i_mm_rst went low
    clk       => MB_I_REF_CLK,
    out_rst   => mb_I_ref_rst
  );

  u_common_areset_mb_II : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => i_mm_rst,  -- release reset some clock cycles after i_mm_rst went low
    clk       => MB_II_REF_CLK,
    out_rst   => mb_II_ref_rst
  );

  -----------------------------------------------------------------------------
  -- dp_clk + dp_rst generation
  -- . dp_clk = i_ext_clk200 in sim or on HW when PLL is not desired
  -- . dp_rst always comes from common_areset
  -----------------------------------------------------------------------------
  no_pll: if g_sim = true or (g_sim = false and g_dp_clk_use_pll = false) generate
    dp_clk <= i_ext_clk200;
    common_areset_in_rst <= i_mm_rst;
  end generate;

  gen_pll: if g_sim = false and g_dp_clk_use_pll = true generate
    u_unb2_board_clk200_pll : entity work.unb2_board_clk200_pll
    generic map (
      g_technology          => g_technology,
      g_use_fpll            => true,
      g_clk200_phase_shift  => g_dp_clk_phase
    )
    port map (
      arst       => i_mm_rst,
      clk200     => i_ext_clk200,
      st_clk200  => dp_clk,  -- = c0
      st_rst200  => common_areset_in_rst
    );
  end generate;

  u_common_areset_dp_rst : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => common_areset_in_rst,  -- release reset some clock cycles after i_mm_rst went low
    clk       => dp_clk_in,
    out_rst   => dp_rst
  );

  -----------------------------------------------------------------------------
  -- mm_clk
  -- . use mm_sim_clk in sim
  -- . derived from ETH_CLK via PLL on hardware
  -----------------------------------------------------------------------------

  i_mm_clk <= mm_sim_clk when g_sim = true else
              clk125     when g_mm_clk_freq = c_unb2_board_mm_clk_freq_125M else
              clk100     when g_mm_clk_freq = c_unb2_board_mm_clk_freq_100M else
              clk50      when g_mm_clk_freq = c_unb2_board_mm_clk_freq_50M  else
              clk50;  -- default

  gen_mm_clk_sim: if g_sim = true generate
      epcs_clk    <= not epcs_clk after 25 ns;  -- 20 MHz, 50ns/2
      clk50       <= not clk50 after 10 ns;  -- 50 MHz, 20ns/2
      clk100      <= not clk100 after 5 ns;  -- 100 MHz, 10ns/2
      clk125      <= not clk125 after 4 ns;  -- 125 MHz, 8ns/2
      mm_sim_clk  <= not mm_sim_clk after 50 ns;  -- 10 MHz, 100ns/2  --> FIXME: this mm_sim_clk should come from the MMM so that its speed can be adapted
      mm_locked   <= '0', '1' after 70 ns;
  end generate;

  gen_mm_clk_hardware: if g_sim = false generate
    u_unb2_board_clk125_pll : entity work.unb2_board_clk125_pll
    generic map (
      g_use_fpll   => true,
      g_technology => g_technology
    )
    port map (
      arst       => i_xo_rst,
      clk125     => i_xo_ethclk,
      c0_clk20   => epcs_clk,
      c1_clk50   => clk50,
      c2_clk100  => clk100,
      c3_clk125  => clk125,
      pll_locked => mm_locked
    );
  end generate;

  u_unb2_board_node_ctrl : entity work.unb2_board_node_ctrl
  generic map (
    g_pulse_us => c_mm_clk_freq / (10**6)  -- nof system clock cycles to get us period, equal to system clock frequency / 10**6
  )
  port map (
    -- MM clock domain reset
    mm_clk      => i_mm_clk,
    mm_locked   => mm_locked,
    mm_rst      => i_mm_rst,
    -- WDI extend
    mm_wdi_in   => pout_wdi,
    mm_wdi_out  => mm_wdi,  -- actively toggle the WDI via pout_wdi from software with toggle extend to allow software reload
    -- Pulses
    mm_pulse_us => OPEN,
    mm_pulse_ms => mm_pulse_ms,
    mm_pulse_s  => mm_pulse_s  -- could be used to toggle a LED
  );

  -----------------------------------------------------------------------------
  -- System info
  -----------------------------------------------------------------------------
  cs_sim <= is_true(g_sim);

  u_mms_unb2_board_system_info : entity work.mms_unb2_board_system_info
  generic map (
    g_sim         => g_sim,
    g_technology  => g_technology,
    g_design_name => g_design_name,
    g_fw_version  => g_fw_version,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_stamp_svn   => g_stamp_svn,
    g_design_note => g_design_note,
    g_rom_version => c_rom_version
  )
  port map (
    mm_clk      => i_mm_clk,
    mm_rst      => i_mm_rst,

    hw_version  => VERSION,
    id          => ID,

    reg_mosi    => reg_unb_system_info_mosi,
    reg_miso    => reg_unb_system_info_miso,

    rom_mosi    => rom_unb_system_info_mosi,
    rom_miso    => rom_unb_system_info_miso,

    chip_id     => this_chip_id,
    bck_id      => this_bck_id
  );

  -----------------------------------------------------------------------------
  -- Red LED control
  -----------------------------------------------------------------------------

  gen_app_led_red: if g_app_led_red = true generate
    -- Let external app control the LED via the app_led_red input
    TESTIO(c_unb2_board_testio_led_red)   <= app_led_red;
  end generate;

  no_app_led_red: if g_app_led_red = false generate
    TESTIO(c_unb2_board_testio_led_red)   <= led_toggle_red;
  end generate;

  -----------------------------------------------------------------------------
  -- Green LED control
  -----------------------------------------------------------------------------

  gen_app_led_green: if g_app_led_green = true generate
    -- Let external app control the LED via the app_led_green input
    TESTIO(c_unb2_board_testio_led_green) <= app_led_green;
  end generate;

  no_app_led_green: if g_app_led_green = false generate
    TESTIO(c_unb2_board_testio_led_green) <= led_toggle_green;
  end generate;

  ------------------------------------------------------------------------------
  -- Toggle red LED when unb2_minimal is running, green LED for other designs.
  ------------------------------------------------------------------------------
  led_toggle_red   <= sel_a_b(g_factory_image = true,  led_toggle, '0');
  led_toggle_green <= sel_a_b(g_factory_image = false, led_toggle, '0');

  u_toggle : entity common_lib.common_toggle
  port map (
    rst         => i_mm_rst,
    clk         => i_mm_clk,
    in_dat      => mm_pulse_s,
    out_dat     => led_toggle
  );

  ------------------------------------------------------------------------------
  -- WDI override
  ------------------------------------------------------------------------------
  -- Actively reset watchdog from software when used, else disable watchdog by leaving the WDI at tri-state level.
  -- A high temp_alarm will keep WDI asserted, causing the watch dog to reset the FPGA.
  -- A third option is to override the WDI manually using the output of a dedicated reg_wdi.
  WDI <= mm_wdi or temp_alarm or wdi_override;

  u_unb2_board_wdi_reg : entity work.unb2_board_wdi_reg
  port map (
    mm_rst              => i_mm_rst,
    mm_clk              => i_mm_clk,

    sla_in              => reg_wdi_mosi,
    sla_out             => reg_wdi_miso,

    wdi_override        => wdi_override
  );

  ------------------------------------------------------------------------------
  -- Remote upgrade
  ------------------------------------------------------------------------------
  -- Every design instantiates an mms_remu instance + MM status & control ports.
  -- So there is full control over the memory mapped registers to set start address of the flash
  -- and reconfigure from that address.
  u_mms_remu: entity remu_lib.mms_remu
  generic map (
    g_technology       => g_technology
  )
  port map (
    mm_rst             => i_mm_rst,
    mm_clk             => i_mm_clk,

    epcs_clk           => epcs_clk,

    remu_mosi          => reg_remu_mosi,
    remu_miso          => reg_remu_miso
  );

  -------------------------------------------------------------------------------
  ---- EPCS
  -------------------------------------------------------------------------------
  u_mms_epcs: entity epcs_lib.mms_epcs
  generic map (
    g_technology         => g_technology,
    g_protect_addr_range => g_protect_addr_range,
    g_protected_addr_lo  => g_protected_addr_lo,
    g_protected_addr_hi  => g_protected_addr_hi
  )
  port map (
    mm_rst             => i_mm_rst,
    mm_clk             => i_mm_clk,

    epcs_clk           => epcs_clk,

    epcs_mosi          => reg_epcs_mosi,
    epcs_miso          => reg_epcs_miso,

    dpmm_ctrl_mosi     => reg_dpmm_ctrl_mosi,
    dpmm_ctrl_miso     => reg_dpmm_ctrl_miso,

    dpmm_data_mosi     => reg_dpmm_data_mosi,
    dpmm_data_miso     => reg_dpmm_data_miso,

    mmdp_ctrl_mosi     => reg_mmdp_ctrl_mosi,
    mmdp_ctrl_miso     => reg_mmdp_ctrl_miso,

    mmdp_data_mosi     => reg_mmdp_data_mosi,
    mmdp_data_miso     => reg_mmdp_data_miso
  );

  ------------------------------------------------------------------------------
  -- PPS input
  ------------------------------------------------------------------------------

  u_mms_ppsh : entity ppsh_lib.mms_ppsh
  generic map (
    g_technology      => g_technology,
    g_st_clk_freq     => g_dp_clk_freq
  )
  port map (
    -- Clocks and reset
    mm_rst           => i_mm_rst,
    mm_clk           => i_mm_clk,
    st_rst           => dp_rst_in,
    st_clk           => dp_clk_in,
    pps_ext          => ext_pps,  -- with unknown but constant phase to st_clk

    -- Memory-mapped clock domain
    reg_mosi         => reg_ppsh_mosi,
    reg_miso         => reg_ppsh_miso,

    -- Streaming clock domain
    pps_sys          => dp_pps
  );

  ------------------------------------------------------------------------------
  -- I2C control for UniBoard sensors
  ------------------------------------------------------------------------------

  mm_board_sens_start <= mm_pulse_s when g_sim = false else mm_pulse_s;  -- mm_pulse_ms; ms pulse comes before the end of the I2C frame, this results in an overflow in simulation  -- speed up in simulation

  u_mms_unb2_board_sens : entity work.mms_unb2_board_sens
  generic map (
    g_sim             => g_sim,
    g_i2c_peripheral  => c_i2c_peripheral_sens,
    g_sens_nof_result => 40,
    g_clk_freq        => g_mm_clk_freq,
    g_comma_w         => 13
  )
  port map (
    -- Clocks and reset
    mm_rst    => i_mm_rst,
    mm_clk    => i_mm_clk,
    mm_start  => mm_board_sens_start,

    -- Memory-mapped clock domain
    reg_mosi  => reg_unb_sens_mosi,
    reg_miso  => reg_unb_sens_miso,

    -- i2c bus
    scl       => SENS_SC,
    sda       => SENS_SD
  );

  u_mms_unb2_board_pmbus : entity work.mms_unb2_board_sens
  generic map (
    g_sim             => g_sim,
    g_i2c_peripheral  => c_i2c_peripheral_pmbus,
    g_sens_nof_result => 42,
    g_clk_freq        => g_mm_clk_freq,
    g_comma_w         => 13
  )
  port map (
    -- Clocks and reset
    mm_rst    => i_mm_rst,
    mm_clk    => i_mm_clk,
    mm_start  => mm_board_sens_start,

    -- Memory-mapped clock domain
    reg_mosi  => reg_unb_pmbus_mosi,
    reg_miso  => reg_unb_pmbus_miso,

    -- i2c bus
    scl       => PMBUS_SC,
    sda       => PMBUS_SD
  );

  u_mms_unb2_fpga_sens : entity work.mms_unb2_fpga_sens
  generic map (
    g_sim        => g_sim,
    g_technology => g_technology,
    g_temp_high  => g_fpga_temp_high
  )
  port map (
    -- Clocks and reset
    mm_rst    => i_mm_rst,
    mm_clk    => i_mm_clk,

    --mm_start  => mm_board_sens_start, -- this does not work, perhaps pulsewidth is too small
    mm_start  => '1',  -- this works

    -- Memory-mapped clock domain
    reg_temp_mosi  => reg_fpga_temp_sens_mosi,
    reg_temp_miso  => reg_fpga_temp_sens_miso,
    reg_voltage_mosi  => reg_fpga_voltage_sens_mosi,
    reg_voltage_miso  => reg_fpga_voltage_sens_miso,

    -- Temperature alarm
    temp_alarm => temp_alarm
  );

  ------------------------------------------------------------------------------
  -- Ethernet 1GbE
  ------------------------------------------------------------------------------

  gen_tse_clk_buf: if g_tse_clk_buf = true generate
    -- Separate clkbuf for the 1GbE tse_clk:
    u_tse_clk_buf : entity tech_clkbuf_lib.tech_clkbuf
    generic map (
      g_technology   => g_technology,
      g_clock_net    => "GLOBAL"
    )
    port map (
      inclk  => i_xo_ethclk,
      outclk => i_tse_clk
    );
  end generate;

  gen_tse_no_clk_buf: if g_tse_clk_buf = false generate
      i_tse_clk <= i_xo_ethclk;
  end generate;

  wire_udp_offload: for i in 0 to g_udp_offload_nof_streams - 1 generate
    eth1g_udp_tx_sosi_arr(i) <= udp_tx_sosi_arr(i);
    udp_tx_siso_arr(i)       <= eth1g_udp_tx_siso_arr(i);

    udp_rx_sosi_arr(i)       <= eth1g_udp_rx_sosi_arr(i);
    eth1g_udp_rx_siso_arr(i) <= udp_rx_siso_arr(i);
  end generate;

  -- In simulation use file IO for MM control. In simulation only use 1GbE for streaming DP data offload (or on load) via 1GbE.
  no_eth1g : if g_sim = true and g_udp_offload = false generate
    eth1g_reg_interrupt <= '0';
    eth1g_tse_miso <= c_mem_miso_rst;
    eth1g_reg_miso <= c_mem_miso_rst;
    eth1g_ram_miso <= c_mem_miso_rst;
  end generate;

  --On hardware always generate 1GbE for MM control. In simulation only use 1GbE for streaming DP data offload (or on load) via 1GbE.
  gen_eth: if g_sim = false or g_udp_offload = true generate
    eth1g_st_clk <= dp_clk_in when g_udp_offload = true else i_mm_clk;
    eth1g_st_rst <= dp_rst_in when g_udp_offload = true else eth1g_mm_rst;

    u_eth : entity eth_lib.eth
    generic map (
      g_technology         => g_technology,
      g_init_ip_address    => g_base_ip & X"0000",  -- Last two bytes set by board/FPGA ID.
      g_cross_clock_domain => g_udp_offload,
      g_frm_discard_en     => true
    )
    port map (
      -- Clocks and reset
      mm_rst            => eth1g_mm_rst,  -- use reset from QSYS
      mm_clk            => i_mm_clk,  -- use mm_clk direct
      eth_clk           => i_tse_clk,  -- 125 MHz clock
      st_rst            => eth1g_st_rst,
      st_clk            => eth1g_st_clk,

      -- UDP transmit interface
      udp_tx_snk_in_arr  => eth1g_udp_tx_sosi_arr,
      udp_tx_snk_out_arr => eth1g_udp_tx_siso_arr,
      -- UDP receive interface
      udp_rx_src_in_arr  => eth1g_udp_rx_siso_arr,
      udp_rx_src_out_arr => eth1g_udp_rx_sosi_arr,

      -- Memory Mapped Slaves
      tse_sla_in        => eth1g_tse_mosi,
      tse_sla_out       => eth1g_tse_miso,
      reg_sla_in        => eth1g_reg_mosi,
      reg_sla_out       => eth1g_reg_miso,
      reg_sla_interrupt => eth1g_reg_interrupt,
      ram_sla_in        => eth1g_ram_mosi,
      ram_sla_out       => eth1g_ram_miso,

      -- PHY interface
      eth_txp           => ETH_SGOUT(0),
      eth_rxp           => ETH_SGIN(0),

      -- LED interface
      tse_led           => eth1g_led
    );
  end generate;
end str;
