-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, mm_lib, eth_lib, technology_lib, tech_tse_lib, tech_mac_10g_lib, io_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.common_network_layers_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use unb2b_board_lib.unb2b_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use eth_lib.eth_pkg.all;
use technology_lib.technology_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use work.qsys_unb2b_test_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;
use work.unb2b_test_pkg.all;

entity mmm_unb2b_test is
  generic (
    g_sim               : boolean := false;  -- FALSE: use SOPC; TRUE: use mm_file I/O
    g_sim_unb_nr        : natural := 0;
    g_sim_node_nr       : natural := 0;
    g_technology        : natural := c_tech_arria10_e1sg;
    g_bg_block_size     : natural;
    g_hdr_field_arr     : t_common_field_arr;
    g_nof_streams_1GbE  : natural;
    g_nof_streams_qsfp  : natural;
    g_nof_streams_ring  : natural;
    g_nof_streams_back0 : natural;
    g_nof_streams_back1 : natural
  );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi             : out t_mem_mosi;
    reg_wdi_miso             : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi : out t_mem_mosi;
    reg_unb_system_info_miso : in  t_mem_miso;
    rom_unb_system_info_mosi : out t_mem_mosi;
    rom_unb_system_info_miso : in  t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        : out t_mem_mosi;
    reg_unb_sens_miso        : in  t_mem_miso;

    reg_fpga_temp_sens_mosi   : out t_mem_mosi;
    reg_fpga_temp_sens_miso   : in  t_mem_miso;
    reg_fpga_voltage_sens_mosi: out t_mem_mosi;
    reg_fpga_voltage_sens_miso: in  t_mem_miso;

    reg_unb_pmbus_mosi       : out t_mem_mosi;
    reg_unb_pmbus_miso       : in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi            : out t_mem_mosi;
    reg_ppsh_miso            : in  t_mem_miso;

    -- eth1g ch0
    eth1g_eth0_mm_rst        : out std_logic;
    eth1g_eth0_tse_mosi      : out t_mem_mosi;
    eth1g_eth0_tse_miso      : in  t_mem_miso;
    eth1g_eth0_reg_mosi      : out t_mem_mosi;
    eth1g_eth0_reg_miso      : in  t_mem_miso;
    eth1g_eth0_reg_interrupt : in  std_logic;
    eth1g_eth0_ram_mosi      : out t_mem_mosi;
    eth1g_eth0_ram_miso      : in  t_mem_miso;

    -- eth1g ch1
    eth1g_eth1_mm_rst        : out std_logic;
    eth1g_eth1_tse_mosi      : out t_mem_mosi;
    eth1g_eth1_tse_miso      : in  t_mem_miso;
    eth1g_eth1_reg_mosi      : out t_mem_mosi;
    eth1g_eth1_reg_miso      : in  t_mem_miso;
    eth1g_eth1_reg_interrupt : in  std_logic;
    eth1g_eth1_ram_mosi      : out t_mem_mosi;
    eth1g_eth1_ram_miso      : in  t_mem_miso;

    -- EPCS read
    reg_dpmm_data_mosi       : out t_mem_mosi;
    reg_dpmm_data_miso       : in  t_mem_miso;
    reg_dpmm_ctrl_mosi       : out t_mem_mosi;
    reg_dpmm_ctrl_miso       : in  t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi       : out t_mem_mosi;
    reg_mmdp_data_miso       : in  t_mem_miso;
    reg_mmdp_ctrl_mosi       : out t_mem_mosi;
    reg_mmdp_ctrl_miso       : in  t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi            : out t_mem_mosi;
    reg_epcs_miso            : in  t_mem_miso;

    -- Remote Update
    reg_remu_mosi            : out t_mem_mosi;
    reg_remu_miso            : in  t_mem_miso;

    -- block gen
    ram_diag_bg_1GbE_mosi          : out t_mem_mosi;
    ram_diag_bg_1GbE_miso          : in  t_mem_miso;
    reg_diag_bg_1GbE_mosi          : out t_mem_mosi;
    reg_diag_bg_1GbE_miso          : in  t_mem_miso;
    reg_diag_tx_seq_1GbE_mosi      : out t_mem_mosi;
    reg_diag_tx_seq_1GbE_miso      : in  t_mem_miso;

    ram_diag_bg_10GbE_mosi         : out t_mem_mosi;
    ram_diag_bg_10GbE_miso         : in  t_mem_miso;
    reg_diag_bg_10GbE_mosi         : out t_mem_mosi;
    reg_diag_bg_10GbE_miso         : in  t_mem_miso;
    reg_diag_tx_seq_10GbE_mosi     : out t_mem_mosi;
    reg_diag_tx_seq_10GbE_miso     : in  t_mem_miso;

    -- dp_offload_tx
    --reg_dp_offload_tx_1GbE_mosi          : OUT t_mem_mosi;
    --reg_dp_offload_tx_1GbE_miso          : IN  t_mem_miso;
    --reg_dp_offload_tx_1GbE_hdr_dat_mosi  : OUT t_mem_mosi;
    --reg_dp_offload_tx_1GbE_hdr_dat_miso  : IN  t_mem_miso;

    -- dp_offload_rx
    --reg_dp_offload_rx_1GbE_hdr_dat_mosi  : OUT t_mem_mosi;
    --reg_dp_offload_rx_1GbE_hdr_dat_miso  : IN  t_mem_miso;

    -- bsn
    reg_bsn_monitor_1GbE_mosi      : out t_mem_mosi;
    reg_bsn_monitor_1GbE_miso      : in  t_mem_miso;
    reg_bsn_monitor_10GbE_mosi     : out t_mem_mosi;
    reg_bsn_monitor_10GbE_miso     : in  t_mem_miso;

    -- databuffer
    ram_diag_data_buf_1GbE_mosi    : out t_mem_mosi;
    ram_diag_data_buf_1GbE_miso    : in  t_mem_miso;
    reg_diag_data_buf_1GbE_mosi    : out t_mem_mosi;
    reg_diag_data_buf_1GbE_miso    : in  t_mem_miso;
    reg_diag_rx_seq_1GbE_mosi      : out t_mem_mosi;
    reg_diag_rx_seq_1GbE_miso      : in  t_mem_miso;

    ram_diag_data_buf_10GbE_mosi   : out t_mem_mosi;
    ram_diag_data_buf_10GbE_miso   : in  t_mem_miso;
    reg_diag_data_buf_10GbE_mosi   : out t_mem_mosi;
    reg_diag_data_buf_10GbE_miso   : in  t_mem_miso;
    reg_diag_rx_seq_10GbE_mosi     : out t_mem_mosi;
    reg_diag_rx_seq_10GbE_miso     : in  t_mem_miso;

    -- 10GbE
    reg_tr_10GbE_qsfp_ring_mosi    : out t_mem_mosi;
    reg_tr_10GbE_qsfp_ring_miso    : in  t_mem_miso;
    reg_tr_10GbE_back0_mosi        : out t_mem_mosi;
    reg_tr_10GbE_back0_miso        : in  t_mem_miso;
    reg_tr_10GbE_back1_mosi        : out t_mem_mosi;
    reg_tr_10GbE_back1_miso        : in  t_mem_miso;

    reg_eth10g_qsfp_ring_mosi      : out t_mem_mosi;
    reg_eth10g_qsfp_ring_miso      : in  t_mem_miso;
    reg_eth10g_back0_mosi          : out t_mem_mosi;
    reg_eth10g_back0_miso          : in  t_mem_miso;
    reg_eth10g_back1_mosi          : out t_mem_mosi;
    reg_eth10g_back1_miso          : in  t_mem_miso;

    -- DDR4 : MB I
    reg_io_ddr_MB_I_mosi                : out t_mem_mosi;
    reg_io_ddr_MB_I_miso                : in  t_mem_miso;

    reg_diag_tx_seq_ddr_MB_I_mosi       : out t_mem_mosi;
    reg_diag_tx_seq_ddr_MB_I_miso       : in  t_mem_miso;

    reg_diag_rx_seq_ddr_MB_I_mosi       : out t_mem_mosi;
    reg_diag_rx_seq_ddr_MB_I_miso       : in  t_mem_miso;

    reg_diag_data_buf_ddr_MB_I_mosi     : out t_mem_mosi;
    reg_diag_data_buf_ddr_MB_I_miso     : in  t_mem_miso;
    ram_diag_data_buf_ddr_MB_I_mosi     : out t_mem_mosi;
    ram_diag_data_buf_ddr_MB_I_miso     : in  t_mem_miso;

    -- DDR4 : MB II
    reg_io_ddr_MB_II_mosi                : out t_mem_mosi;
    reg_io_ddr_MB_II_miso                : in  t_mem_miso;

    reg_diag_tx_seq_ddr_MB_II_mosi       : out t_mem_mosi;
    reg_diag_tx_seq_ddr_MB_II_miso       : in  t_mem_miso;

    reg_diag_rx_seq_ddr_MB_II_mosi       : out t_mem_mosi;
    reg_diag_rx_seq_ddr_MB_II_miso       : in  t_mem_miso;

    reg_diag_data_buf_ddr_MB_II_mosi     : out t_mem_mosi;
    reg_diag_data_buf_ddr_MB_II_miso     : in  t_mem_miso;
    ram_diag_data_buf_ddr_MB_II_mosi     : out t_mem_mosi;
    ram_diag_data_buf_ddr_MB_II_miso     : in  t_mem_miso
  );
end mmm_unb2b_test;

architecture str of mmm_unb2b_test is
  constant c_sim_node_nr   : natural := g_sim_node_nr;
  constant c_sim_node_type : string(1 to 2) := "FN";

  constant g_nof_streams_10GbE                     : natural := g_nof_streams_qsfp + g_nof_streams_ring + g_nof_streams_back0 + g_nof_streams_back1;

  -- Block generator
  -- check with python: from common import *
  --                    ceil_log2(48 * 2**ceil_log2(900))
  constant c_ram_diag_bg_1GbE_addr_w               : natural := ceil_log2(g_nof_streams_1GbE  * pow2(ceil_log2(g_bg_block_size)));
  constant c_ram_diag_bg_10GbE_addr_w              : natural := ceil_log2(g_nof_streams_10GbE * pow2(ceil_log2(g_bg_block_size)));
  constant c_ram_diag_databuffer_10GbE_addr_w      : natural := ceil_log2(g_nof_streams_10GbE * pow2(ceil_log2(g_bg_block_size)));
  constant c_ram_diag_databuffer_1GbE_addr_w       : natural := ceil_log2(g_nof_streams_1GbE  * pow2(ceil_log2(g_bg_block_size)));
  constant c_ram_diag_databuffer_ddr_addr_w        : natural := ceil_log2(2                   * pow2(ceil_log2(g_bg_block_size)));

  -- dp_offload
--  CONSTANT c_reg_dp_offload_tx_adr_w                     : NATURAL := 1; -- Dev note: add to c_unb2b_board_peripherals_mm_reg_default
--  CONSTANT c_reg_dp_offload_tx_1GbE_multi_adr_w          : NATURAL := ceil_log2(g_nof_streams_1GbE * pow2(c_reg_dp_offload_tx_adr_w));
--
--  CONSTANT c_reg_dp_offload_tx_1GbE_hdr_dat_nof_words    : NATURAL := field_nof_words(c_hdr_field_arr, c_word_w);
--  CONSTANT c_reg_dp_offload_tx_1GbE_hdr_dat_adr_w        : NATURAL := ceil_log2(c_reg_dp_offload_tx_1GbE_hdr_dat_nof_words);
--  CONSTANT c_reg_dp_offload_tx_1GbE_hdr_dat_multi_adr_w  : NATURAL := ceil_log2(g_nof_streams_1GbE * pow2(c_reg_dp_offload_tx_1GbE_hdr_dat_adr_w));
--
--  CONSTANT c_reg_dp_offload_rx_1GbE_hdr_dat_nof_words    : NATURAL := field_nof_words(c_hdr_field_arr, c_word_w);
--  CONSTANT c_reg_dp_offload_rx_1GbE_hdr_dat_adr_w        : NATURAL := ceil_log2(c_reg_dp_offload_rx_1GbE_hdr_dat_nof_words);
--  CONSTANT c_reg_dp_offload_rx_1GbE_hdr_dat_multi_adr_w  : NATURAL := ceil_log2(g_nof_streams_1GbE * pow2(c_reg_dp_offload_rx_1GbE_hdr_dat_adr_w));

  -- tr_10GbE
  constant c_reg_tr_10GbE_adr_w                    : natural := func_tech_mac_10g_csr_addr_w(g_technology);
  constant c_reg_tr_10GbE_qsfp_ring_multi_adr_w    : natural := ceil_log2((g_nof_streams_qsfp + g_nof_streams_ring) * pow2(c_reg_tr_10GbE_adr_w));
  constant c_reg_tr_10GbE_back0_multi_adr_w        : natural := ceil_log2(g_nof_streams_back0 * pow2(c_reg_tr_10GbE_adr_w));
  constant c_reg_tr_10GbE_back1_multi_adr_w        : natural := ceil_log2(g_nof_streams_back1 * pow2(c_reg_tr_10GbE_adr_w));

  -- reg_eth10g
  constant c_reg_eth10g_adr_w                      : natural := 1;
  constant c_reg_eth10g_qsfp_ring_multi_adr_w      : natural := ceil_log2((g_nof_streams_qsfp + g_nof_streams_ring) * pow2(c_reg_eth10g_adr_w));
  constant c_reg_eth10g_back0_multi_adr_w          : natural := ceil_log2(g_nof_streams_back0 * pow2(c_reg_eth10g_adr_w));
  constant c_reg_eth10g_back1_multi_adr_w          : natural := ceil_log2(g_nof_streams_back1 * pow2(c_reg_eth10g_adr_w));

  -- BSN monitors
  constant c_reg_rsp_bsn_monitor_1GbE_adr_w        : natural := ceil_log2(g_nof_streams_1GbE  * pow2(c_unb2b_board_peripherals_mm_reg_default.reg_bsn_monitor_adr_w));
  constant c_reg_rsp_bsn_monitor_10GbE_adr_w       : natural := ceil_log2(g_nof_streams_10GbE * pow2(c_unb2b_board_peripherals_mm_reg_default.reg_bsn_monitor_adr_w));

  -- Simulation
  constant c_sim_eth_src_mac                       : std_logic_vector(c_network_eth_mac_slv'range) := X"00228608" & TO_UVEC(g_sim_unb_nr, c_byte_w) & TO_UVEC(g_sim_node_nr, c_byte_w);
  constant c_sim_eth_control_rx_en                 : natural := 2**c_eth_mm_reg_control_bi.rx_en;

  signal sim_eth_mm_bus_switch                     : std_logic;
  signal sim_eth_psc_access                        : std_logic;

  signal i_eth1g_eth0_reg_mosi                     : t_mem_mosi;
  signal i_eth1g_eth0_reg_miso                     : t_mem_miso;
  signal i_eth1g_eth1_reg_mosi                     : t_mem_mosi;
  signal i_eth1g_eth1_reg_miso                     : t_mem_miso;

  signal sim_eth1g_eth0_reg_mosi                   : t_mem_mosi;
  signal sim_eth1g_eth1_reg_mosi                   : t_mem_mosi;
  signal i_reset_n                                 : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $HDL_IOFILE_SIM_DIR.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    eth1g_eth0_mm_rst <= mm_rst;
    eth1g_eth1_mm_rst <= mm_rst;

    u_mm_file_reg_unb_system_info   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                                 port map(mm_rst, mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                                 port map(mm_rst, mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi               : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                                 port map(mm_rst, mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_unb_sens          : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
                                                 port map(mm_rst, mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso );

    u_mm_file_reg_unb_pmbus         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_PMBUS")
                                                 port map(mm_rst, mm_clk, reg_unb_pmbus_mosi, reg_unb_pmbus_miso );

    u_mm_file_reg_fpga_temp_sens  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_TEMP_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_temp_sens_mosi, reg_fpga_temp_sens_miso );

    u_mm_file_reg_fpga_voltage_sens :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_VOLTAGE_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_voltage_sens_mosi, reg_fpga_voltage_sens_miso );

    u_mm_file_reg_ppsh              : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                                 port map(mm_rst, mm_clk, reg_ppsh_mosi, reg_ppsh_miso );

    u_mm_file_reg_diag_bg_1GbE      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG_1GBE")
                                                 port map(mm_rst, mm_clk, reg_diag_bg_1GbE_mosi, reg_diag_bg_1GbE_miso);
    u_mm_file_ram_diag_bg_1GbE      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_BG_1GBE")
                                                 port map(mm_rst, mm_clk, ram_diag_bg_1GbE_mosi, ram_diag_bg_1GbE_miso);
    u_mm_file_reg_diag_tx_seq_1GbE  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_1GBE")
                                                 port map(mm_rst, mm_clk, reg_diag_tx_seq_1GbE_mosi, reg_diag_tx_seq_1GbE_miso);

    u_mm_file_reg_diag_bg_10GbE     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG_10GBE")
                                                 port map(mm_rst, mm_clk, reg_diag_bg_10GbE_mosi, reg_diag_bg_10GbE_miso);
    u_mm_file_ram_diag_bg_10GbE     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_BG_10GBE")
                                                 port map(mm_rst, mm_clk, ram_diag_bg_10GbE_mosi, ram_diag_bg_10GbE_miso);
    u_mm_file_reg_diag_tx_seq_10GbE : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_10GBE")
                                                 port map(mm_rst, mm_clk, reg_diag_tx_seq_10GbE_mosi, reg_diag_tx_seq_10GbE_miso);

--    u_mm_file_reg_dp_offload_tx_1GbE  : mm_file GENERIC MAP(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_TX_1GBE")
--                                                   PORT MAP(mm_rst, mm_clk, reg_dp_offload_tx_1GbE_mosi, reg_dp_offload_tx_1GbE_miso);
--
--    u_mm_file_reg_dp_offload_tx_1GbE_hdr_dat  : mm_file GENERIC MAP(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_TX_1GBE_HDR_DAT")
--                                                           PORT MAP(mm_rst, mm_clk, reg_dp_offload_tx_1GbE_hdr_dat_mosi, reg_dp_offload_tx_1GbE_hdr_dat_miso);
--
--    u_mm_file_reg_dp_offload_rx_1GbE_hdr_dat  : mm_file GENERIC MAP(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_RX_1GBE_HDR_DAT")
--                                                           PORT MAP(mm_rst, mm_clk, reg_dp_offload_rx_1GbE_hdr_dat_mosi, reg_dp_offload_rx_1GbE_hdr_dat_miso);

    u_mm_file_reg_bsn_monitor_1GbE       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_1GBE")
                                                      port map(mm_rst, mm_clk, reg_bsn_monitor_1GbE_mosi, reg_bsn_monitor_1GbE_miso);
    u_mm_file_reg_bsn_monitor_10GbE      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_10GBE")
                                                      port map(mm_rst, mm_clk, reg_bsn_monitor_10GbE_mosi, reg_bsn_monitor_10GbE_miso);

    u_mm_file_reg_diag_data_buffer_1GbE  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_1GBE")
                                                      port map(mm_rst, mm_clk, reg_diag_data_buf_1GbE_mosi, reg_diag_data_buf_1GbE_miso);
    u_mm_file_ram_diag_data_buffer_1GbE  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_1GBE")
                                                      port map(mm_rst, mm_clk, ram_diag_data_buf_1GbE_mosi, ram_diag_data_buf_1GbE_miso);
    u_mm_file_reg_diag_rx_seq_1GbE       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_1GBE")
                                                      port map(mm_rst, mm_clk, reg_diag_rx_seq_1GbE_mosi, reg_diag_rx_seq_1GbE_miso);

    u_mm_file_reg_diag_data_buffer_10GbE : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_10GBE")
                                                      port map(mm_rst, mm_clk, reg_diag_data_buf_10GbE_mosi, reg_diag_data_buf_10GbE_miso);
    u_mm_file_ram_diag_data_buffer_10GbE : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_10GBE")
                                                      port map(mm_rst, mm_clk, ram_diag_data_buf_10GbE_mosi, ram_diag_data_buf_10GbE_miso);
    u_mm_file_reg_diag_rx_seq_10GbE      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_10GBE")
                                                      port map(mm_rst, mm_clk, reg_diag_rx_seq_10GbE_mosi, reg_diag_rx_seq_10GbE_miso);

    u_mm_file_reg_io_ddr_MB_I                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_IO_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_io_ddr_MB_I_mosi, reg_io_ddr_MB_I_miso);
    u_mm_file_reg_diag_tx_seq_ddr_MB_I        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_tx_seq_ddr_MB_I_mosi, reg_diag_tx_seq_ddr_MB_I_miso);
    u_mm_file_reg_diag_rx_seq_ddr_MB_I        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_rx_seq_ddr_MB_I_mosi, reg_diag_rx_seq_ddr_MB_I_miso);
    u_mm_file_reg_diag_data_buffer_ddr_MB_I   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_data_buf_ddr_MB_I_mosi, reg_diag_data_buf_ddr_MB_I_miso);
    u_mm_file_ram_diag_data_buffer_ddr_MB_I   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, ram_diag_data_buf_ddr_MB_I_mosi, ram_diag_data_buf_ddr_MB_I_miso);

    u_mm_file_reg_io_ddr_MB_II                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_IO_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_io_ddr_MB_II_mosi, reg_io_ddr_MB_II_miso);
    u_mm_file_reg_diag_tx_seq_ddr_MB_II       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_tx_seq_ddr_MB_II_mosi, reg_diag_tx_seq_ddr_MB_II_miso);
    u_mm_file_reg_diag_rx_seq_ddr_MB_II       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_rx_seq_ddr_MB_II_mosi, reg_diag_rx_seq_ddr_MB_II_miso);
    u_mm_file_reg_diag_data_buffer_ddr_MB_II  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_data_buf_ddr_MB_II_mosi, reg_diag_data_buf_ddr_MB_II_miso);
    u_mm_file_ram_diag_data_buffer_ddr_MB_II  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, ram_diag_data_buf_ddr_MB_II_mosi, ram_diag_data_buf_ddr_MB_II_miso);

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    u_mm_file_reg_eth0            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                               port map(mm_rst, mm_clk, i_eth1g_eth0_reg_mosi, eth1g_eth0_reg_miso);
    u_mm_file_reg_eth1            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_1_MMS_REG")
                                               port map(mm_rst, mm_clk, i_eth1g_eth1_reg_mosi, eth1g_eth1_reg_miso);

    u_mm_file_reg_tr_10GbE_qsfp_ring : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_QSFP_RING")
                                                  port map(mm_rst, mm_clk, reg_tr_10GbE_qsfp_ring_mosi, reg_tr_10GbE_qsfp_ring_miso);
    u_mm_file_reg_tr_10GbE_back0     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_BACK0")
                                                  port map(mm_rst, mm_clk, reg_tr_10GbE_back0_mosi, reg_tr_10GbE_back0_miso);
    u_mm_file_reg_tr_10GbE_back1     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_BACK1")
                                                  port map(mm_rst, mm_clk, reg_tr_10GbE_back1_mosi, reg_tr_10GbE_back1_miso);

    u_mm_file_reg_eth10g_qsfp_ring   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_ETH10G_QSFP_RING")
                                                  port map(mm_rst, mm_clk, reg_eth10g_qsfp_ring_mosi, reg_eth10g_qsfp_ring_miso);
    u_mm_file_reg_eth10g_back0       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_ETH10G_BACK0")
                                                  port map(mm_rst, mm_clk, reg_eth10g_back0_mosi, reg_eth10g_back0_miso);
    u_mm_file_reg_eth10g_back1       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_ETH10G_BACK1")
                                                  port map(mm_rst, mm_clk, reg_eth10g_back1_mosi, reg_eth10g_back1_miso);

    ----------------------------------------------------------------------------
    -- 1GbE setup sequence normally performed by unb_os@NIOS
    ----------------------------------------------------------------------------
    p_eth_setup : process
    begin
      sim_eth_mm_bus_switch <= '1';

      eth1g_eth0_tse_mosi.wr <= '0';
      eth1g_eth0_tse_mosi.rd <= '0';
      wait for 400 ns;
      wait until rising_edge(mm_clk);
      proc_tech_tse_setup(g_technology, false, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tech_tse_tx_ready_latency, c_sim_eth_src_mac, sim_eth_psc_access, mm_clk, eth1g_eth0_tse_miso, eth1g_eth0_tse_mosi);

      -- Enable RX
      proc_mem_mm_bus_wr(c_eth_reg_control_wi + 0, c_sim_eth_control_rx_en, mm_clk, eth1g_eth0_reg_miso, sim_eth1g_eth0_reg_mosi);  -- control rx en
      sim_eth_mm_bus_switch <= '0';

      wait;
    end process;

    p_switch : process(sim_eth_mm_bus_switch, sim_eth1g_eth0_reg_mosi, i_eth1g_eth0_reg_mosi)
    begin
      if sim_eth_mm_bus_switch = '1' then
          eth1g_eth0_reg_mosi <= sim_eth1g_eth0_reg_mosi;
        else
          eth1g_eth0_reg_mosi <= i_eth1g_eth0_reg_mosi;
        end if;
    end process;

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(mm_clk, c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  i_reset_n <= not mm_rst;

  ----------------------------------------------------------------------------
  -- QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys : if g_sim = false generate
    u_qsys : qsys_unb2b_test
    port map (

      clk_clk                                   => mm_clk,
      reset_reset_n                             => i_reset_n,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2b_board.
      pio_wdi_external_connection_export        => pout_wdi,

      avs_eth_0_reset_export                    => eth1g_eth0_mm_rst,
      avs_eth_0_clk_export                      => OPEN,
      avs_eth_0_tse_address_export              => eth1g_eth0_tse_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                => eth1g_eth0_tse_mosi.wr,
      avs_eth_0_tse_read_export                 => eth1g_eth0_tse_mosi.rd,
      avs_eth_0_tse_writedata_export            => eth1g_eth0_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export             => eth1g_eth0_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export          => eth1g_eth0_tse_miso.waitrequest,
      avs_eth_0_reg_address_export              => eth1g_eth0_reg_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                => eth1g_eth0_reg_mosi.wr,
      avs_eth_0_reg_read_export                 => eth1g_eth0_reg_mosi.rd,
      avs_eth_0_reg_writedata_export            => eth1g_eth0_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export             => eth1g_eth0_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export              => eth1g_eth0_ram_mosi.address(c_unb2b_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                => eth1g_eth0_ram_mosi.wr,
      avs_eth_0_ram_read_export                 => eth1g_eth0_ram_mosi.rd,
      avs_eth_0_ram_writedata_export            => eth1g_eth0_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export             => eth1g_eth0_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                      => eth1g_eth0_reg_interrupt,

      avs_eth_1_reset_export                    => eth1g_eth1_mm_rst,
      avs_eth_1_clk_export                      => OPEN,
      avs_eth_1_tse_address_export              => eth1g_eth1_tse_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_1_tse_write_export                => eth1g_eth1_tse_mosi.wr,
      avs_eth_1_tse_read_export                 => eth1g_eth1_tse_mosi.rd,
      avs_eth_1_tse_writedata_export            => eth1g_eth1_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_1_tse_readdata_export             => eth1g_eth1_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_1_tse_waitrequest_export          => eth1g_eth1_tse_miso.waitrequest,
      avs_eth_1_reg_address_export              => eth1g_eth1_reg_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_1_reg_write_export                => eth1g_eth1_reg_mosi.wr,
      avs_eth_1_reg_read_export                 => eth1g_eth1_reg_mosi.rd,
      avs_eth_1_reg_writedata_export            => eth1g_eth1_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_1_reg_readdata_export             => eth1g_eth1_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_1_ram_address_export              => eth1g_eth1_ram_mosi.address(c_unb2b_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_1_ram_write_export                => eth1g_eth1_ram_mosi.wr,
      avs_eth_1_ram_read_export                 => eth1g_eth1_ram_mosi.rd,
      avs_eth_1_ram_writedata_export            => eth1g_eth1_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_1_ram_readdata_export             => eth1g_eth1_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_1_irq_export                      => eth1g_eth1_reg_interrupt,

      reg_unb_sens_reset_export                 => OPEN,
      reg_unb_sens_clk_export                   => OPEN,
      reg_unb_sens_address_export               => reg_unb_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      reg_unb_sens_write_export                 => reg_unb_sens_mosi.wr,
      reg_unb_sens_writedata_export             => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_read_export                  => reg_unb_sens_mosi.rd,
      reg_unb_sens_readdata_export              => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_unb_pmbus_reset_export                => OPEN,
      reg_unb_pmbus_clk_export                  => OPEN,
      reg_unb_pmbus_address_export              => reg_unb_pmbus_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_pmbus_adr_w - 1 downto 0),
      reg_unb_pmbus_write_export                => reg_unb_pmbus_mosi.wr,
      reg_unb_pmbus_writedata_export            => reg_unb_pmbus_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_pmbus_read_export                 => reg_unb_pmbus_mosi.rd,
      reg_unb_pmbus_readdata_export             => reg_unb_pmbus_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_temp_sens_reset_export           => OPEN,
      reg_fpga_temp_sens_clk_export             => OPEN,
      reg_fpga_temp_sens_address_export         => reg_fpga_temp_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export           => reg_fpga_temp_sens_mosi.wr,
      reg_fpga_temp_sens_writedata_export       => reg_fpga_temp_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export            => reg_fpga_temp_sens_mosi.rd,
      reg_fpga_temp_sens_readdata_export        => reg_fpga_temp_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_reset_export        => OPEN,
      reg_fpga_voltage_sens_clk_export          => OPEN,
      reg_fpga_voltage_sens_address_export      => reg_fpga_voltage_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export        => reg_fpga_voltage_sens_mosi.wr,
      reg_fpga_voltage_sens_writedata_export    => reg_fpga_voltage_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export         => reg_fpga_voltage_sens_mosi.rd,
      reg_fpga_voltage_sens_readdata_export     => reg_fpga_voltage_sens_miso.rddata(c_word_w - 1 downto 0),

      rom_system_info_reset_export              => OPEN,
      rom_system_info_clk_export                => OPEN,
      rom_system_info_address_export            => rom_unb_system_info_mosi.address(c_unb2b_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      rom_system_info_write_export              => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export          => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export               => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export           => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_system_info_reset_export              => OPEN,
      pio_system_info_clk_export                => OPEN,
      pio_system_info_address_export            => reg_unb_system_info_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export              => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export          => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export               => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export           => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_pps_reset_export                      => OPEN,
      pio_pps_clk_export                        => OPEN,
      pio_pps_address_export                    => reg_ppsh_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1 downto 0),
      pio_pps_write_export                      => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                  => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                       => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                   => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),

      reg_wdi_reset_export                      => OPEN,
      reg_wdi_clk_export                        => OPEN,
      reg_wdi_address_export                    => reg_wdi_mosi.address(0 downto 0),
      reg_wdi_write_export                      => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                  => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                       => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                   => reg_wdi_miso.rddata(c_word_w - 1 downto 0),

      reg_remu_reset_export                     => OPEN,
      reg_remu_clk_export                       => OPEN,
      reg_remu_address_export                   => reg_remu_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                     => reg_remu_mosi.wr,
      reg_remu_writedata_export                 => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                      => reg_remu_mosi.rd,
      reg_remu_readdata_export                  => reg_remu_miso.rddata(c_word_w - 1 downto 0),

      reg_epcs_reset_export                     => OPEN,
      reg_epcs_clk_export                       => OPEN,
      reg_epcs_address_export                   => reg_epcs_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                     => reg_epcs_mosi.wr,
      reg_epcs_writedata_export                 => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                      => reg_epcs_mosi.rd,
      reg_epcs_readdata_export                  => reg_epcs_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_reset_export                => OPEN,
      reg_dpmm_ctrl_clk_export                  => OPEN,
      reg_dpmm_ctrl_address_export              => reg_dpmm_ctrl_mosi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                => reg_dpmm_ctrl_mosi.wr,
      reg_dpmm_ctrl_writedata_export            => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                 => reg_dpmm_ctrl_mosi.rd,
      reg_dpmm_ctrl_readdata_export             => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_reset_export                => OPEN,
      reg_mmdp_data_clk_export                  => OPEN,
      reg_mmdp_data_address_export              => reg_mmdp_data_mosi.address(0 downto 0),
      reg_mmdp_data_write_export                => reg_mmdp_data_mosi.wr,
      reg_mmdp_data_writedata_export            => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                 => reg_mmdp_data_mosi.rd,
      reg_mmdp_data_readdata_export             => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_reset_export                => OPEN,
      reg_dpmm_data_clk_export                  => OPEN,
      reg_dpmm_data_address_export              => reg_dpmm_data_mosi.address(0 downto 0),
      reg_dpmm_data_read_export                 => reg_dpmm_data_mosi.rd,
      reg_dpmm_data_readdata_export             => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                => reg_dpmm_data_mosi.wr,
      reg_dpmm_data_writedata_export            => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_reset_export                => OPEN,
      reg_mmdp_ctrl_clk_export                  => OPEN,
      reg_mmdp_ctrl_address_export              => reg_mmdp_ctrl_mosi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                 => reg_mmdp_ctrl_mosi.rd,
      reg_mmdp_ctrl_readdata_export             => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                => reg_mmdp_ctrl_mosi.wr,
      reg_mmdp_ctrl_writedata_export            => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      reg_tr_10gbe_qsfp_ring_reset_export       => OPEN,
      reg_tr_10gbe_qsfp_ring_clk_export         => OPEN,
      reg_tr_10gbe_qsfp_ring_address_export     => reg_tr_10GbE_qsfp_ring_mosi.address(c_reg_tr_10GbE_qsfp_ring_multi_adr_w - 1 downto 0),
      reg_tr_10gbe_qsfp_ring_write_export       => reg_tr_10GbE_qsfp_ring_mosi.wr,
      reg_tr_10gbe_qsfp_ring_writedata_export   => reg_tr_10GbE_qsfp_ring_mosi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10gbe_qsfp_ring_read_export        => reg_tr_10GbE_qsfp_ring_mosi.rd,
      reg_tr_10gbe_qsfp_ring_readdata_export    => reg_tr_10GbE_qsfp_ring_miso.rddata(c_word_w - 1 downto 0),
      reg_tr_10gbe_qsfp_ring_waitrequest_export => reg_tr_10GbE_qsfp_ring_miso.waitrequest,

      reg_tr_10gbe_back0_reset_export           => OPEN,
      reg_tr_10gbe_back0_clk_export             => OPEN,
      reg_tr_10gbe_back0_address_export         => reg_tr_10GbE_back0_mosi.address(c_reg_tr_10GbE_back0_multi_adr_w - 1 downto 0),
      reg_tr_10gbe_back0_write_export           => reg_tr_10GbE_back0_mosi.wr,
      reg_tr_10gbe_back0_writedata_export       => reg_tr_10GbE_back0_mosi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10gbe_back0_read_export            => reg_tr_10GbE_back0_mosi.rd,
      reg_tr_10gbe_back0_readdata_export        => reg_tr_10GbE_back0_miso.rddata(c_word_w - 1 downto 0),
      reg_tr_10gbe_back0_waitrequest_export     => reg_tr_10GbE_back0_miso.waitrequest,

      reg_tr_10gbe_back1_reset_export           => OPEN,
      reg_tr_10gbe_back1_clk_export             => OPEN,
      reg_tr_10gbe_back1_address_export         => reg_tr_10GbE_back1_mosi.address(c_reg_tr_10GbE_back1_multi_adr_w - 1 downto 0),
      reg_tr_10gbe_back1_write_export           => reg_tr_10GbE_back1_mosi.wr,
      reg_tr_10gbe_back1_writedata_export       => reg_tr_10GbE_back1_mosi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10gbe_back1_read_export            => reg_tr_10GbE_back1_mosi.rd,
      reg_tr_10gbe_back1_readdata_export        => reg_tr_10GbE_back1_miso.rddata(c_word_w - 1 downto 0),
      reg_tr_10gbe_back1_waitrequest_export     => reg_tr_10GbE_back1_miso.waitrequest,

      reg_eth10g_qsfp_ring_reset_export         => OPEN,
      reg_eth10g_qsfp_ring_clk_export           => OPEN,
      reg_eth10g_qsfp_ring_address_export       => reg_eth10g_qsfp_ring_mosi.address(c_reg_eth10g_qsfp_ring_multi_adr_w - 1 downto 0),
      reg_eth10g_qsfp_ring_write_export         => reg_eth10g_qsfp_ring_mosi.wr,
      reg_eth10g_qsfp_ring_writedata_export     => reg_eth10g_qsfp_ring_mosi.wrdata(c_word_w - 1 downto 0),
      reg_eth10g_qsfp_ring_read_export          => reg_eth10g_qsfp_ring_mosi.rd,
      reg_eth10g_qsfp_ring_readdata_export      => reg_eth10g_qsfp_ring_miso.rddata(c_word_w - 1 downto 0),

      reg_eth10g_back0_reset_export             => OPEN,
      reg_eth10g_back0_clk_export               => OPEN,
      reg_eth10g_back0_address_export           => reg_eth10g_back0_mosi.address(c_reg_eth10g_back0_multi_adr_w - 1 downto 0),
      reg_eth10g_back0_write_export             => reg_eth10g_back0_mosi.wr,
      reg_eth10g_back0_writedata_export         => reg_eth10g_back0_mosi.wrdata(c_word_w - 1 downto 0),
      reg_eth10g_back0_read_export              => reg_eth10g_back0_mosi.rd,
      reg_eth10g_back0_readdata_export          => reg_eth10g_back0_miso.rddata(c_word_w - 1 downto 0),

      reg_eth10g_back1_reset_export             => OPEN,
      reg_eth10g_back1_clk_export               => OPEN,
      reg_eth10g_back1_address_export           => reg_eth10g_back1_mosi.address(c_reg_eth10g_back1_multi_adr_w - 1 downto 0),
      reg_eth10g_back1_write_export             => reg_eth10g_back1_mosi.wr,
      reg_eth10g_back1_writedata_export         => reg_eth10g_back1_mosi.wrdata(c_word_w - 1 downto 0),
      reg_eth10g_back1_read_export              => reg_eth10g_back1_mosi.rd,
      reg_eth10g_back1_readdata_export          => reg_eth10g_back1_miso.rddata(c_word_w - 1 downto 0),

--      -- the_reg_dp_offload_tx_1GbE
--      reg_dp_offload_tx_1GbE_address_export         => reg_dp_offload_tx_1GbE_mosi.address(c_reg_dp_offload_tx_1GbE_multi_adr_w-1 DOWNTO 0),
--      reg_dp_offload_tx_1GbE_clk_export             => OPEN,
--      reg_dp_offload_tx_1GbE_read_export            => reg_dp_offload_tx_1GbE_mosi.rd,
--      reg_dp_offload_tx_1GbE_readdata_export        => reg_dp_offload_tx_1GbE_miso.rddata(c_word_w-1 DOWNTO 0),
--      reg_dp_offload_tx_1GbE_reset_export           => OPEN,
--      reg_dp_offload_tx_1GbE_write_export           => reg_dp_offload_tx_1GbE_mosi.wr,
--      reg_dp_offload_tx_1GbE_writedata_export       => reg_dp_offload_tx_1GbE_mosi.wrdata(c_word_w-1 DOWNTO 0),
--
--      -- the_reg_dp_offload_tx_1GbE_hdr_dat
--      reg_dp_offload_tx_1GbE_hdr_dat_address_export    => reg_dp_offload_tx_1GbE_hdr_dat_mosi.address(c_reg_dp_offload_tx_1GbE_hdr_dat_multi_adr_w-1 DOWNTO 0),
--      reg_dp_offload_tx_1GbE_hdr_dat_clk_export        => OPEN,
--      reg_dp_offload_tx_1GbE_hdr_dat_read_export       => reg_dp_offload_tx_1GbE_hdr_dat_mosi.rd,
--      reg_dp_offload_tx_1GbE_hdr_dat_readdata_export   => reg_dp_offload_tx_1GbE_hdr_dat_miso.rddata(c_word_w-1 DOWNTO 0),
--      reg_dp_offload_tx_1GbE_hdr_dat_reset_export      => OPEN,
--      reg_dp_offload_tx_1GbE_hdr_dat_write_export      => reg_dp_offload_tx_1GbE_hdr_dat_mosi.wr,
--      reg_dp_offload_tx_1GbE_hdr_dat_writedata_export  => reg_dp_offload_tx_1GbE_hdr_dat_mosi.wrdata(c_word_w-1 DOWNTO 0),
--
--      -- the_reg_dp_offload_rx_1GbE_hdr_dat
--      reg_dp_offload_rx_1GbE_hdr_dat_address_export    => reg_dp_offload_rx_1GbE_hdr_dat_mosi.address(c_reg_dp_offload_rx_1GbE_hdr_dat_multi_adr_w-1 DOWNTO 0),
--      reg_dp_offload_rx_1GbE_hdr_dat_clk_export        => OPEN,
--      reg_dp_offload_rx_1GbE_hdr_dat_read_export       => reg_dp_offload_rx_1GbE_hdr_dat_mosi.rd,
--      reg_dp_offload_rx_1GbE_hdr_dat_readdata_export   => reg_dp_offload_rx_1GbE_hdr_dat_miso.rddata(c_word_w-1 DOWNTO 0),
--      reg_dp_offload_rx_1GbE_hdr_dat_reset_export      => OPEN,
--      reg_dp_offload_rx_1GbE_hdr_dat_write_export      => reg_dp_offload_rx_1GbE_hdr_dat_mosi.wr,
--      reg_dp_offload_rx_1GbE_hdr_dat_writedata_export  => reg_dp_offload_rx_1GbE_hdr_dat_mosi.wrdata(c_word_w-1 DOWNTO 0),

      reg_bsn_monitor_1gbe_reset_export         => OPEN,
      reg_bsn_monitor_1gbe_clk_export           => OPEN,
      reg_bsn_monitor_1gbe_address_export       => reg_bsn_monitor_1GbE_mosi.address(c_reg_rsp_bsn_monitor_1GbE_adr_w - 1 downto 0),
      reg_bsn_monitor_1gbe_write_export         => reg_bsn_monitor_1GbE_mosi.wr,
      reg_bsn_monitor_1gbe_writedata_export     => reg_bsn_monitor_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_1gbe_read_export          => reg_bsn_monitor_1GbE_mosi.rd,
      reg_bsn_monitor_1gbe_readdata_export      => reg_bsn_monitor_1GbE_miso.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_10gbe_reset_export        => OPEN,
      reg_bsn_monitor_10gbe_clk_export          => OPEN,
      reg_bsn_monitor_10gbe_address_export      => reg_bsn_monitor_10GbE_mosi.address(c_reg_rsp_bsn_monitor_10GbE_adr_w - 1 downto 0),
      reg_bsn_monitor_10gbe_write_export        => reg_bsn_monitor_10GbE_mosi.wr,
      reg_bsn_monitor_10gbe_writedata_export    => reg_bsn_monitor_10GbE_mosi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_10gbe_read_export         => reg_bsn_monitor_10GbE_mosi.rd,
      reg_bsn_monitor_10gbe_readdata_export     => reg_bsn_monitor_10GbE_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_1gbe_reset_export     => OPEN,
      reg_diag_data_buffer_1gbe_clk_export       => OPEN,
      reg_diag_data_buffer_1gbe_address_export   => reg_diag_data_buf_1gbe_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_1gbe_write_export     => reg_diag_data_buf_1gbe_mosi.wr,
      reg_diag_data_buffer_1gbe_writedata_export => reg_diag_data_buf_1gbe_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_1gbe_read_export      => reg_diag_data_buf_1gbe_mosi.rd,
      reg_diag_data_buffer_1gbe_readdata_export  => reg_diag_data_buf_1gbe_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_10gbe_reset_export     => OPEN,
      reg_diag_data_buffer_10gbe_clk_export       => OPEN,
      reg_diag_data_buffer_10gbe_address_export   => reg_diag_data_buf_10gbe_mosi.address(5 downto 0),
      reg_diag_data_buffer_10gbe_write_export     => reg_diag_data_buf_10gbe_mosi.wr,
      reg_diag_data_buffer_10gbe_writedata_export => reg_diag_data_buf_10gbe_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_10gbe_read_export      => reg_diag_data_buf_10gbe_mosi.rd,
      reg_diag_data_buffer_10gbe_readdata_export  => reg_diag_data_buf_10gbe_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_1gbe_clk_export       => OPEN,
      ram_diag_data_buffer_1gbe_reset_export     => OPEN,
      ram_diag_data_buffer_1gbe_address_export   => ram_diag_data_buf_1gbe_mosi.address(c_ram_diag_databuffer_1GbE_addr_w - 1 downto 0),
      ram_diag_data_buffer_1gbe_write_export     => ram_diag_data_buf_1gbe_mosi.wr,
      ram_diag_data_buffer_1gbe_writedata_export => ram_diag_data_buf_1gbe_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_1gbe_read_export      => ram_diag_data_buf_1gbe_mosi.rd,
      ram_diag_data_buffer_1gbe_readdata_export  => ram_diag_data_buf_1gbe_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_10gbe_clk_export       => OPEN,
      ram_diag_data_buffer_10gbe_reset_export     => OPEN,
      ram_diag_data_buffer_10gbe_address_export   => ram_diag_data_buf_10gbe_mosi.address(c_ram_diag_databuffer_10GbE_addr_w - 1 downto 0),
      ram_diag_data_buffer_10gbe_write_export     => ram_diag_data_buf_10gbe_mosi.wr,
      ram_diag_data_buffer_10gbe_writedata_export => ram_diag_data_buf_10gbe_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_10gbe_read_export      => ram_diag_data_buf_10gbe_mosi.rd,
      ram_diag_data_buffer_10gbe_readdata_export  => ram_diag_data_buf_10gbe_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_bg_1GbE_reset_export               => OPEN,
      reg_diag_bg_1GbE_clk_export                 => OPEN,
      reg_diag_bg_1GbE_address_export             => reg_diag_bg_1GbE_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_bg_adr_w - 1 downto 0),
      reg_diag_bg_1GbE_write_export               => reg_diag_bg_1GbE_mosi.wr,
      reg_diag_bg_1GbE_writedata_export           => reg_diag_bg_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_bg_1GbE_read_export                => reg_diag_bg_1GbE_mosi.rd,
      reg_diag_bg_1GbE_readdata_export            => reg_diag_bg_1GbE_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_bg_10GbE_reset_export              => OPEN,
      reg_diag_bg_10GbE_clk_export                => OPEN,
      reg_diag_bg_10GbE_address_export            => reg_diag_bg_10GbE_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_bg_adr_w - 1 downto 0),
      reg_diag_bg_10GbE_write_export              => reg_diag_bg_10GbE_mosi.wr,
      reg_diag_bg_10GbE_writedata_export          => reg_diag_bg_10GbE_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_bg_10GbE_read_export               => reg_diag_bg_10GbE_mosi.rd,
      reg_diag_bg_10GbE_readdata_export           => reg_diag_bg_10GbE_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_bg_1GbE_reset_export               => OPEN,
      ram_diag_bg_1GbE_clk_export                 => OPEN,
      ram_diag_bg_1GbE_address_export             => ram_diag_bg_1GbE_mosi.address(c_ram_diag_bg_1GbE_addr_w - 1 downto 0),
      ram_diag_bg_1GbE_write_export               => ram_diag_bg_1GbE_mosi.wr,
      ram_diag_bg_1GbE_writedata_export           => ram_diag_bg_1GbE_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_bg_1GbE_read_export                => ram_diag_bg_1GbE_mosi.rd,
      ram_diag_bg_1GbE_readdata_export            => ram_diag_bg_1GbE_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_bg_10GbE_reset_export              => OPEN,
      ram_diag_bg_10GbE_clk_export                => OPEN,
      ram_diag_bg_10GbE_address_export            => ram_diag_bg_10GbE_mosi.address(c_ram_diag_bg_10GbE_addr_w - 1 downto 0),
      ram_diag_bg_10GbE_write_export              => ram_diag_bg_10GbE_mosi.wr,
      ram_diag_bg_10GbE_writedata_export          => ram_diag_bg_10GbE_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_bg_10GbE_read_export               => ram_diag_bg_10GbE_mosi.rd,
      ram_diag_bg_10GbE_readdata_export           => ram_diag_bg_10GbE_miso.rddata(c_word_w - 1 downto 0),

      reg_io_ddr_MB_I_address_export                  => reg_io_ddr_MB_I_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_io_ddr_adr_w - 1 downto 0),
      reg_io_ddr_MB_I_clk_export                      => OPEN,
      reg_io_ddr_MB_I_read_export                     => reg_io_ddr_MB_I_mosi.rd,
      reg_io_ddr_MB_I_readdata_export                 => reg_io_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),
      reg_io_ddr_MB_I_reset_export                    => OPEN,
      reg_io_ddr_MB_I_write_export                    => reg_io_ddr_MB_I_mosi.wr,
      reg_io_ddr_MB_I_writedata_export                => reg_io_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),

      reg_io_ddr_MB_II_address_export                 => reg_io_ddr_MB_II_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_io_ddr_adr_w - 1 downto 0),
      reg_io_ddr_MB_II_clk_export                     => OPEN,
      reg_io_ddr_MB_II_read_export                    => reg_io_ddr_MB_II_mosi.rd,
      reg_io_ddr_MB_II_readdata_export                => reg_io_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),
      reg_io_ddr_MB_II_reset_export                   => OPEN,
      reg_io_ddr_MB_II_write_export                   => reg_io_ddr_MB_II_mosi.wr,
      reg_io_ddr_MB_II_writedata_export               => reg_io_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),

      reg_diag_tx_seq_ddr_MB_I_reset_export           => OPEN,
      reg_diag_tx_seq_ddr_MB_I_clk_export             => OPEN,
      reg_diag_tx_seq_ddr_MB_I_address_export         => reg_diag_tx_seq_ddr_MB_I_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_tx_seq_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_I_write_export           => reg_diag_tx_seq_ddr_MB_I_mosi.wr,
      reg_diag_tx_seq_ddr_MB_I_writedata_export       => reg_diag_tx_seq_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_I_read_export            => reg_diag_tx_seq_ddr_MB_I_mosi.rd,
      reg_diag_tx_seq_ddr_MB_I_readdata_export        => reg_diag_tx_seq_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_tx_seq_ddr_MB_II_reset_export          => OPEN,
      reg_diag_tx_seq_ddr_MB_II_clk_export            => OPEN,
      reg_diag_tx_seq_ddr_MB_II_address_export        => reg_diag_tx_seq_ddr_MB_II_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_tx_seq_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_II_write_export          => reg_diag_tx_seq_ddr_MB_II_mosi.wr,
      reg_diag_tx_seq_ddr_MB_II_writedata_export      => reg_diag_tx_seq_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_II_read_export           => reg_diag_tx_seq_ddr_MB_II_mosi.rd,
      reg_diag_tx_seq_ddr_MB_II_readdata_export       => reg_diag_tx_seq_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_rx_seq_ddr_MB_I_reset_export           => OPEN,
      reg_diag_rx_seq_ddr_MB_I_clk_export             => OPEN,
      reg_diag_rx_seq_ddr_MB_I_address_export         => reg_diag_rx_seq_ddr_MB_I_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_rx_seq_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_I_write_export           => reg_diag_rx_seq_ddr_MB_I_mosi.wr,
      reg_diag_rx_seq_ddr_MB_I_writedata_export       => reg_diag_rx_seq_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_I_read_export            => reg_diag_rx_seq_ddr_MB_I_mosi.rd,
      reg_diag_rx_seq_ddr_MB_I_readdata_export        => reg_diag_rx_seq_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_rx_seq_ddr_MB_II_reset_export          => OPEN,
      reg_diag_rx_seq_ddr_MB_II_clk_export            => OPEN,
      reg_diag_rx_seq_ddr_MB_II_address_export        => reg_diag_rx_seq_ddr_MB_II_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_rx_seq_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_II_write_export          => reg_diag_rx_seq_ddr_MB_II_mosi.wr,
      reg_diag_rx_seq_ddr_MB_II_writedata_export      => reg_diag_rx_seq_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_II_read_export           => reg_diag_rx_seq_ddr_MB_II_mosi.rd,
      reg_diag_rx_seq_ddr_MB_II_readdata_export       => reg_diag_rx_seq_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_ddr_MB_I_reset_export      => OPEN,
      reg_diag_data_buffer_ddr_MB_I_clk_export        => OPEN,
      reg_diag_data_buffer_ddr_MB_I_address_export    => reg_diag_data_buf_ddr_MB_I_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_I_write_export      => reg_diag_data_buf_ddr_MB_I_mosi.wr,
      reg_diag_data_buffer_ddr_MB_I_writedata_export  => reg_diag_data_buf_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_I_read_export       => reg_diag_data_buf_ddr_MB_I_mosi.rd,
      reg_diag_data_buffer_ddr_MB_I_readdata_export   => reg_diag_data_buf_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_ddr_MB_II_reset_export     => OPEN,
      reg_diag_data_buffer_ddr_MB_II_clk_export       => OPEN,
      reg_diag_data_buffer_ddr_MB_II_address_export   => reg_diag_data_buf_ddr_MB_II_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_II_write_export     => reg_diag_data_buf_ddr_MB_II_mosi.wr,
      reg_diag_data_buffer_ddr_MB_II_writedata_export => reg_diag_data_buf_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_II_read_export      => reg_diag_data_buf_ddr_MB_II_mosi.rd,
      reg_diag_data_buffer_ddr_MB_II_readdata_export  => reg_diag_data_buf_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_ddr_MB_I_clk_export        => OPEN,
      ram_diag_data_buffer_ddr_MB_I_reset_export      => OPEN,
      ram_diag_data_buffer_ddr_MB_I_address_export    => ram_diag_data_buf_ddr_MB_I_mosi.address(c_ram_diag_databuffer_ddr_addr_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_I_write_export      => ram_diag_data_buf_ddr_MB_I_mosi.wr,
      ram_diag_data_buffer_ddr_MB_I_writedata_export  => ram_diag_data_buf_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_I_read_export       => ram_diag_data_buf_ddr_MB_I_mosi.rd,
      ram_diag_data_buffer_ddr_MB_I_readdata_export   => ram_diag_data_buf_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_ddr_MB_II_clk_export       => OPEN,
      ram_diag_data_buffer_ddr_MB_II_reset_export     => OPEN,
      ram_diag_data_buffer_ddr_MB_II_address_export   => ram_diag_data_buf_ddr_MB_II_mosi.address(c_ram_diag_databuffer_ddr_addr_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_II_write_export     => ram_diag_data_buf_ddr_MB_II_mosi.wr,
      ram_diag_data_buffer_ddr_MB_II_writedata_export => ram_diag_data_buf_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_II_read_export      => ram_diag_data_buf_ddr_MB_II_mosi.rd,
      ram_diag_data_buffer_ddr_MB_II_readdata_export  => ram_diag_data_buf_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0)
    );
  end generate;
end str;
