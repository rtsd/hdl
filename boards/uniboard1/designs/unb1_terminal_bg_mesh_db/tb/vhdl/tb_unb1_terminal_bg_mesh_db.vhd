-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify that unb1_terminal_bg_mesh_db can simulate
-- Description:
--
-- Usage:
--   > as 8
--   > run 10 us

library IEEE, common_lib, unb1_board_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.tb_unb1_board_pkg.all;

entity tb_unb1_terminal_bg_mesh_db is
end tb_unb1_terminal_bg_mesh_db;

architecture tb of tb_unb1_terminal_bg_mesh_db is
  constant c_sim             : boolean := true;
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_sim_level       : natural := 1;

  constant c_nof_bn          : natural := 4;
  constant c_nof_fn          : natural := 4;
  constant c_nof_nodes       : natural := c_nof_bn + c_nof_fn;
  constant c_ena_mesh_reorder: boolean := true;

  constant c_sim_unb_nr      : natural := 0;

  constant c_cable_delay     : time := 12 ns;

  constant c_ext_clk_period  : time := 5 ns;  -- 200 MHz
  constant c_eth_clk_period  : time := 40 ns;  -- 25 MHz XO on UniBoard
  constant c_tr_clk_period   : time := 6400 ps;  -- 156.25 MHz XO on UniBoard

  type t_mesh_arr is array  (c_nof_nodes - 1 downto 0) of std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);

  -- DUT
  signal WDI                 : std_logic;
  signal ext_pps             : std_logic;
  signal ext_clk             : std_logic := '0';
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal VERSION             : std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
  signal ID                  : std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
  signal TESTIO              : std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic_vector(c_nof_nodes - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_nof_nodes - 1 downto 0);

  signal tr_clk              : std_logic := '0';

  signal FN_BN_0_TX_arr      : t_mesh_arr;
  signal FN_BN_0_RX_arr      : t_mesh_arr;
  signal FN_BN_1_TX_arr      : t_mesh_arr;
  signal FN_BN_1_RX_arr      : t_mesh_arr;
  signal FN_BN_2_TX_arr      : t_mesh_arr;
  signal FN_BN_2_RX_arr      : t_mesh_arr;
  signal FN_BN_3_TX_arr      : t_mesh_arr;
  signal FN_BN_3_RX_arr      : t_mesh_arr;

  ------------------------------------------------------------------------------
  -- BN & FN mesh side serial I/O
  ------------------------------------------------------------------------------
  signal bn_out_mesh_serial_3arr : t_unb1_board_mesh_sl_3arr;
  signal bn_in_mesh_serial_3arr  : t_unb1_board_mesh_sl_3arr;
  signal fn_in_mesh_serial_3arr  : t_unb1_board_mesh_sl_3arr;
  signal fn_out_mesh_serial_3arr : t_unb1_board_mesh_sl_3arr;
begin
  -- Run 1 ms
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- 1GbE XO clock (25 MHz)
  tr_clk  <= not tr_clk  after c_tr_clk_period / 2;  -- Transceiver clock (156.25 MHz)

  ext_pps <= '0';  -- not used

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up

  VERSION <= c_version;

  ------------------------------------------------------------------------------
  -- UniBoard
  ------------------------------------------------------------------------------
  gen_bn: for BN in 0 to c_nof_bn - 1 generate
    u_bn : entity work.unb1_terminal_bg_mesh_db
    generic map (
      -- General
      g_sim         => c_sim,
      g_sim_level   => c_sim_level,
      g_sim_unb_nr  => c_sim_unb_nr,
      g_sim_node_nr => (BN + 4)
    )
    port map (
      -- GENERAL
      WDI           => WDI,
      CLK           => ext_clk,
      PPS           => ext_pps,
      INTA          => INTA,
      INTB          => INTB,

      -- Others
      VERSION       => VERSION,
      ID            => TO_UVEC(BN + 4, c_unb1_board_aux.id_w),  -- BN chip ID 4,5,6,7
      TESTIO        => TESTIO,

      -- I2C Interface to Sensors
      sens_sc       => sens_scl,
      sens_sd       => sens_sda,

      -- 1GbE Control Interface
      ETH_clk       => eth_clk,  -- ETH reference clock also used for system reference clock
      ETH_SGIN      => eth_rxp(BN + c_nof_fn),
      ETH_SGOUT     => eth_txp(BN + c_nof_fn),

      -- Transceiver clocks
      SB_CLK        => tr_clk,  -- TR clock FN-BN(mesh)

      -- Mesh serial I/O
      FN_BN_0_TX    => FN_BN_0_TX_arr(BN + c_nof_fn),
      FN_BN_0_RX    => FN_BN_0_RX_arr(BN + c_nof_fn),
      FN_BN_1_TX    => FN_BN_1_TX_arr(BN + c_nof_fn),
      FN_BN_1_RX    => FN_BN_1_RX_arr(BN + c_nof_fn),
      FN_BN_2_TX    => FN_BN_2_TX_arr(BN + c_nof_fn),
      FN_BN_2_RX    => FN_BN_2_RX_arr(BN + c_nof_fn),
      FN_BN_3_TX    => FN_BN_3_TX_arr(BN + c_nof_fn),
      FN_BN_3_RX    => FN_BN_3_RX_arr(BN + c_nof_fn)
    );

    -- Use mesh_io block to create 3arr format for the mesh model.
    u_mesh_io : entity unb1_board_lib.unb1_board_mesh_io
    generic map (
      g_bus_w => c_unb1_board_tr_mesh.bus_w
    )
    port map (
      tx_serial_2arr => bn_in_mesh_serial_3arr(BN),
      rx_serial_2arr => bn_out_mesh_serial_3arr(BN),

      -- Serial I/O
      FN_BN_0_TX     => FN_BN_0_RX_arr(BN + c_nof_fn),
      FN_BN_0_RX     => FN_BN_0_TX_arr(BN + c_nof_fn),
      FN_BN_1_TX     => FN_BN_1_RX_arr(BN + c_nof_fn),
      FN_BN_1_RX     => FN_BN_1_TX_arr(BN + c_nof_fn),
      FN_BN_2_TX     => FN_BN_2_RX_arr(BN + c_nof_fn),
      FN_BN_2_RX     => FN_BN_2_TX_arr(BN + c_nof_fn),
      FN_BN_3_TX     => FN_BN_3_RX_arr(BN + c_nof_fn),
      FN_BN_3_RX     => FN_BN_3_TX_arr(BN + c_nof_fn)
    );
  end generate;

  gen_fn: for FN in 0 to c_nof_fn - 1 generate
    u_fn : entity work.unb1_terminal_bg_mesh_db
    generic map (
      -- General
      g_sim         => c_sim,
      g_sim_level   => c_sim_level,
      g_sim_unb_nr  => c_sim_unb_nr,
      g_sim_node_nr => FN
    )
    port map (
      -- GENERAL
      WDI           => WDI,
      CLK           => ext_clk,
      PPS           => ext_pps,
      INTA          => INTA,
      INTB          => INTB,

      -- Others
      VERSION       => VERSION,
      ID            => TO_UVEC(FN, c_unb1_board_aux.id_w),  -- FN chip ID 0,1,2,3,
      TESTIO        => TESTIO,

      -- I2C Interface to Sensors
      sens_sc       => sens_scl,
      sens_sd       => sens_sda,

      -- 1GbE Control Interface
      ETH_clk       => eth_clk,  -- ETH reference clock also used for system reference clock
      ETH_SGIN      => eth_rxp(FN),
      ETH_SGOUT     => eth_txp(FN),

      -- Transceiver clocks
      SB_CLK        => tr_clk,  -- TR clock FN-BN(mesh)

      -- Mesh serial I/O
      FN_BN_0_TX    => FN_BN_0_TX_arr(FN),
      FN_BN_0_RX    => FN_BN_0_RX_arr(FN),
      FN_BN_1_TX    => FN_BN_1_TX_arr(FN),
      FN_BN_1_RX    => FN_BN_1_RX_arr(FN),
      FN_BN_2_TX    => FN_BN_2_TX_arr(FN),
      FN_BN_2_RX    => FN_BN_2_RX_arr(FN),
      FN_BN_3_TX    => FN_BN_3_TX_arr(FN),
      FN_BN_3_RX    => FN_BN_3_RX_arr(FN)
    );

    -- Use mesh_io block to create 3arr format for the mesh model.
    u_mesh_io : entity unb1_board_lib.unb1_board_mesh_io
    generic map (
      g_bus_w => c_unb1_board_tr_mesh.bus_w
    )
    port map (
      tx_serial_2arr => fn_in_mesh_serial_3arr(FN),
      rx_serial_2arr => fn_out_mesh_serial_3arr(FN),

      -- Serial I/O
      FN_BN_0_TX     => FN_BN_0_RX_arr(FN),
      FN_BN_0_RX     => FN_BN_0_TX_arr(FN),
      FN_BN_1_TX     => FN_BN_1_RX_arr(FN),
      FN_BN_1_RX     => FN_BN_1_TX_arr(FN),
      FN_BN_2_TX     => FN_BN_2_RX_arr(FN),
      FN_BN_2_RX     => FN_BN_2_TX_arr(FN),
      FN_BN_3_TX     => FN_BN_3_RX_arr(FN),
      FN_BN_3_RX     => FN_BN_3_TX_arr(FN)
    );
  end generate;

  -- Direct interconnect BN0<->FN0.
  no_mesh : if c_nof_bn = 1 and c_nof_fn = 1 generate
    fn_in_mesh_serial_3arr(0) <= bn_out_mesh_serial_3arr(0);
    bn_in_mesh_serial_3arr(0) <= fn_out_mesh_serial_3arr(0);
  end generate;

  -- Mesh model
  gen_mesh : if c_nof_bn > 1 or c_nof_fn > 1 generate
    u_mesh_model_serial : entity unb1_board_lib.unb1_board_mesh_model_sl
    generic map(
      g_reorder      => c_ena_mesh_reorder
    )
    port map (
      -- FN to BN
      fn_tx_sl_3arr  => fn_out_mesh_serial_3arr,
      bn_rx_sl_3arr  => bn_in_mesh_serial_3arr,

      -- BN to FN
      bn_tx_sl_3arr  => bn_out_mesh_serial_3arr,
      fn_rx_sl_3arr  => fn_in_mesh_serial_3arr
    );
  end generate;
end tb;
