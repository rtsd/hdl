-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provides a wideband WG by using multiple diag_wg
-- Description:
-- Remarks:
-- . For g_wideband_factor=1 this diag_wg_wideband defaults to diag_wg.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.diag_pkg.all;
use technology_lib.technology_select_pkg.all;

entity diag_wg_wideband is
  generic (
    g_technology        : natural := c_tech_select_default;
    -- Use g_buf_dir to be able to have different path to waveform file for sim and for synth
    g_buf_dir           : string := "data/";

    -- Wideband parameters
    g_wideband_factor   : natural := 4;  -- Wideband rate factor >= 1 for unit frequency of g_wideband_factor * Fs

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w         : natural := 18;
    g_buf_addr_w        : natural := 11;
    g_calc_support      : boolean := true;
    g_calc_gain_w       : natural := 1;
    g_calc_dat_w        : natural := 12
  );
  port (
    -- Memory-mapped clock domain
    mm_rst               : in  std_logic;
    mm_clk               : in  std_logic;

    mm_wrdata            : in  std_logic_vector(g_buf_dat_w - 1 downto 0);
    mm_address           : in  std_logic_vector(g_buf_addr_w - 1 downto 0);
    mm_wr                : in  std_logic;
    mm_rd                : in  std_logic;
    mm_rdval             : out std_logic;
    mm_rddata            : out std_logic_vector(g_buf_dat_w - 1 downto 0);

    -- Streaming clock domain
    st_rst               : in  std_logic;
    st_clk               : in  std_logic;
    st_restart           : in  std_logic;

    st_ctrl              : in  t_diag_wg;
    st_mon_ctrl          : out t_diag_wg;

    out_ovr              : out std_logic_vector(g_wideband_factor            - 1 downto 0);  -- big endian, so first output sample in MSBit, MSData
    out_dat              : out std_logic_vector(g_wideband_factor * g_buf_dat_w - 1 downto 0);
    out_val              : out std_logic_vector(g_wideband_factor            - 1 downto 0);
    out_sync             : out std_logic_vector(g_wideband_factor            - 1 downto 0)
  );
end diag_wg_wideband;

architecture str of diag_wg_wideband is
  constant c_buf            : t_c_mem := (latency  => 1,
                                          adr_w    => g_buf_addr_w,
                                          dat_w    => g_buf_dat_w,  -- fit DSP multiply 18x18 element
                                          nof_dat  => 2**g_buf_addr_w,  -- = 2**adr_w
                                          init_sl  => '0');
  constant c_buf_file       : string := sel_a_b(c_buf.adr_w = 11 and c_buf.dat_w = 18, g_buf_dir & "diag_sin_2048x18.hex",
                                        sel_a_b(c_buf.adr_w = 10 and c_buf.dat_w = 18, g_buf_dir & "diag_sin_1024x18.hex",
                                        sel_a_b(c_buf.adr_w = 10 and c_buf.dat_w = 8, g_buf_dir & "diag_sin_1024x8.hex", "UNUSED")));

  type t_buf_dat_arr is array (natural range <>) of std_logic_vector(g_buf_dat_w - 1 downto 0);
  type t_buf_adr_arr is array (natural range <>) of std_logic_vector(g_buf_addr_w - 1 downto 0);

  signal st_mon_ctrl_arr : t_diag_wg_arr(0 to g_wideband_factor - 1);

  -- Use same address and data widths for both MM side and ST side memory ports
  signal buf_rdval     : std_logic_vector(0 to g_wideband_factor - 1);
  signal buf_rddata    : t_buf_dat_arr(0 to g_wideband_factor - 1);

  signal st_address    : t_buf_adr_arr(0 to g_wideband_factor - 1);
  signal st_rd         : std_logic_vector(0 to g_wideband_factor - 1);
  signal st_rdval      : std_logic_vector(0 to g_wideband_factor - 1);
  signal st_rddata     : t_buf_dat_arr(0 to g_wideband_factor - 1);
begin
  assert c_buf_file /= "UNUSED"
    report "diag_wg_wideband : no buffer waveform file available"
    severity FAILURE;

  -- MM write same to all g_wideband_factor waveform buffers
  -- MM read only from waveform buffer 0
  mm_rdval  <= buf_rdval(0);
  mm_rddata <= buf_rddata(0);

  st_mon_ctrl <= st_mon_ctrl_arr(0);  -- same for all g_wideband_factor waveform generators

  gen_wg : for I in 0 to g_wideband_factor - 1 generate
    -- Waveform buffer
    u_buf : entity common_lib.common_ram_crw_crw
    generic map (
      g_technology => g_technology,
      g_ram       => c_buf,
      g_init_file => c_buf_file
    )
    port map (
      rst_a     => mm_rst,
      clk_a     => mm_clk,
      wr_dat_a  => mm_wrdata,
      adr_a     => mm_address,
      wr_en_a   => mm_wr,
      rd_en_a   => mm_rd,
      rd_val_a  => buf_rdval(I),
      rd_dat_a  => buf_rddata(I),
      rst_b     => st_rst,
      clk_b     => st_clk,
      wr_dat_b  => (others => '0'),
      adr_b     => st_address(I),
      wr_en_b   => '0',
      rd_en_b   => st_rd(I),
      rd_val_b  => st_rdval(I),
      rd_dat_b  => st_rddata(I)
    );

    -- Waveform generator
    u_wg : entity work.diag_wg
    generic map (
      g_technology   => g_technology,
      g_buf_dat_w    => g_buf_dat_w,
      g_buf_addr_w   => g_buf_addr_w,
      g_rate_factor  => g_wideband_factor,
      g_rate_offset  => I,
      g_calc_support => g_calc_support,
      g_calc_gain_w  => g_calc_gain_w,
      g_calc_dat_w   => g_calc_dat_w
    )
    port map (
      rst            => st_rst,
      clk            => st_clk,
      restart        => st_restart,

      buf_rddat      => st_rddata(I),
      buf_rdval      => st_rdval(I),
      buf_addr       => st_address(I),
      buf_rden       => st_rd(I),

      ctrl           => st_ctrl,
      mon_ctrl       => st_mon_ctrl_arr(I),

      out_ovr        => out_ovr(                                            g_wideband_factor - I - 1),
      out_dat        => out_dat((g_wideband_factor - I) * g_buf_dat_w - 1 downto (g_wideband_factor - I - 1) * g_buf_dat_w),
      out_val        => out_val(                                            g_wideband_factor - I - 1),
      out_sync       => out_sync(                                           g_wideband_factor - I - 1)
    );
  end generate;
end str;
