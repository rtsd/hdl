#! /usr/bin/env python
###############################################################################
#
# Copyright (dC) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


from common import *
import test_case
import node_io
import pi_dp_offload_tx_hdr_dat_compaan_unb1_10g_bg_db
import pi_diag_block_gen
import pi_diag_data_buffer
import pi_eth 
from eth import *
                 
# Some definitions                 
c_10g_data_w          = 64     # 64 bit internal data
c_blocksize           = 365    # 365 samples * 8 bytes(= 64bit) = 2920 bytes
c_bg_nof_streams      = 1 
c_bg_ram_size         = 512
c_gap_size            = 3000 
c_nof_blocks_per_sync = 10
c_write_block_gen     = True

# Instantiate testcase and IO
tc = test_case.Testcase('TB - ', '')
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)                                                  


# Instantiate 10G offload objects: FN0=[0], FN1=[1]
dpotx_hdr_dat = pi_dp_offload_tx_hdr_dat_compaan_unb1_10g_bg_db.PiDpOffloadTxHdrDatCompaanUnb110GBgDb(tc, io, nof_inst=1)

# Create block generator instance (only FN1)
bg = pi_diag_block_gen.PiDiagBlockGen(tc, io, c_bg_nof_streams, c_bg_ram_size, tc.nodeFn3Nrs )


# MAC Addresses
eth_src_mac = 0x2286080008  # 10G MAC base address for UniBoard
eth_dst_mac = 0x074306C700 #+ 1  # 10G MAC address dop36

# Fixed header constants 
IP_HEADER_LENGTH   = 20
UDP_HEADER_LENGTH  =  8
USR_HEADER_LENGTH  = 20
USR_HDR_WORD_ALIGN = 2
NOF_PAYLOAD_BYTES  = (c_blocksize * 8)

###############################################################################
# The IP header field values. All fixed except ip_src_addr 
# (and concequently the ip_header_checksum).
###############################################################################
ip_version         = 4 
ip_header_length   = 5 # 5 32b words
ip_services        = 0 
ip_total_length    = IP_HEADER_LENGTH+UDP_HEADER_LENGTH+USR_HEADER_LENGTH+USR_HDR_WORD_ALIGN+NOF_PAYLOAD_BYTES # 6196B
ip_identification  = 0 
ip_flags           = 2 
ip_fragment_offset = 0 
ip_time_to_live    = 127 
ip_protocol        = 17 
ip_header_checksum = 0            # to be calculated
ip_src_addr_fn0    = 0xc0a80164   # 0xc0a80164 = 192.168.1.100
ip_src_addr_fn1    = 0xc0a80165   # 0xc0a80165 = 192.168.1.101
ip_src_addr_fn2    = 0xc0a80166   # 0xc0a80164 = 192.168.1.102
ip_src_addr_fn3    = 0xc0a80167   # 0xc0a80165 = 192.168.1.103
ip_dst_addr        = 0xc0a80102 #+ 1  # 0xc0a80102 = 192.168.1.2 = IP-address 10G in dop36

###############################################################################
# Calculate and print the IP header checksum for FN0
###############################################################################
hdr_bits_common = CommonBits(ip_version         ,4)  & \
                  CommonBits(ip_header_length   ,4)  & \
                  CommonBits(ip_services        ,8)  & \
                  CommonBits(ip_total_length    ,16) & \
                  CommonBits(ip_identification  ,16) & \
                  CommonBits(ip_flags           ,3)  & \
                  CommonBits(ip_fragment_offset ,13) & \
                  CommonBits(ip_time_to_live    ,8)  & \
                  CommonBits(ip_protocol        ,8)  & \
                  CommonBits(ip_header_checksum ,16)

hdr_bits_fn0    = hdr_bits_common & \
                  CommonBits(ip_src_addr_fn0    ,32) & \
                  CommonBits(ip_dst_addr        ,32)

hdr_bits_fn1    = hdr_bits_common & \
                  CommonBits(ip_src_addr_fn1    ,32) & \
                  CommonBits(ip_dst_addr        ,32)

hdr_bits_fn2    = hdr_bits_common & \
                  CommonBits(ip_src_addr_fn2    ,32) & \
                  CommonBits(ip_dst_addr        ,32)

hdr_bits_fn3    = hdr_bits_common & \
                  CommonBits(ip_src_addr_fn3    ,32) & \
                  CommonBits(ip_dst_addr        ,32)

hdr_bytes_fn0   = CommonBytes(hdr_bits_fn0.data, 20)
hdr_bytes_fn1   = CommonBytes(hdr_bits_fn1.data, 20)
hdr_bytes_fn2   = CommonBytes(hdr_bits_fn2.data, 20)
hdr_bytes_fn3   = CommonBytes(hdr_bits_fn3.data, 20)

#tc.append_log(3, 'IP header checksum FN0: %d' % ip_hdr_checksum(hdr_bytes_fn0))
#tc.append_log(3, 'IP header checksum FN1: %d' % ip_hdr_checksum(hdr_bytes_fn1))
#tc.append_log(3, 'IP header checksum FN2: %d' % ip_hdr_checksum(hdr_bytes_fn2))
tc.append_log(3, 'IP header checksum FN3: %s' % hex(ip_hdr_checksum(hdr_bytes_fn3)))


# Write setting for the block generator:
bg.write_block_gen_settings(samplesPerPacket=c_blocksize, blocksPerSync=c_nof_blocks_per_sync, gapSize=c_gap_size, memLowAddr=0, memHighAddr=c_bg_ram_size-1, BSNInit=10)


# Configure 10G of FN2
dpotx_hdr_dat.write(node_nrs=tc.nodeNrs[0], inst_nrs=tc.gpNumbers, registers=[('eth_src_mac', eth_src_mac + 3)], regmap=dpotx_hdr_dat.regmap)
dpotx_hdr_dat.write(node_nrs=tc.nodeNrs[0], inst_nrs=tc.gpNumbers, registers=[('eth_dst_mac', eth_dst_mac)],     regmap=dpotx_hdr_dat.regmap)
dpotx_hdr_dat.write(node_nrs=tc.nodeNrs[0], inst_nrs=tc.gpNumbers, registers=[('ip_src_addr', ip_src_addr_fn3)], regmap=dpotx_hdr_dat.regmap)
dpotx_hdr_dat.write(node_nrs=tc.nodeNrs[0], inst_nrs=tc.gpNumbers, registers=[('ip_dst_addr', ip_dst_addr)],     regmap=dpotx_hdr_dat.regmap)
#checksum is calculated in vhdl, no need to write it
#dpotx_hdr_dat.write(node_nrs=tc.nodeNrs[0], inst_nrs=tc.gpNumbers, registers=[('ip_header_checksum', ip_hdr_checksum(hdr_bytes_fn2))], regmap=dpotx_hdr_dat.regmap)



################################################################################
##
## Write data and settings to block generator
##
################################################################################
# Write setting for the block generator:
bg.write_block_gen_settings(samplesPerPacket=c_blocksize, blocksPerSync=c_nof_blocks_per_sync, gapSize=c_gap_size, memLowAddr=0, memHighAddr=c_bg_ram_size-1, BSNInit=10)

bg_data = []
for i in range(c_bg_ram_size):
  bg_data.append(i+0)

# Write the stimuli to the block generator and enable the block generator
if c_write_block_gen == True:
    for i in range(c_bg_nof_streams):
        bg.write_waveform_ram(data=bg_data, channelNr= i)

# BG: Enable the blockgenerator
bg.write_enable()