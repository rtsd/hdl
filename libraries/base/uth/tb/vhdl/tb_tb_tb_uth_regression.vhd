-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Regression multi TB for UTH library
-- Description:
--   All TB are self checking. If some component TB fails then simulate that TB
--   seperately to find the cause.
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_tb_uth_regression is
end tb_tb_tb_uth_regression;

architecture tb of tb_tb_tb_uth_regression is
begin
  u_tb_tb_uth           : entity work.tb_tb_uth;
  u_tb_tb_uth_dp_packet : entity work.tb_tb_uth_dp_packet;
  u_tb_tb_uth_terminals : entity work.tb_tb_uth_terminals;
end tb;
