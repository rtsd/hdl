-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.mdio_pkg.all;

entity tb_mdio_phy_reg is
end tb_mdio_phy_reg;

architecture tb of tb_mdio_phy_reg is
  constant c_sim              : boolean := true;

  constant clk_period         : time := 10 ns;
  constant c_delay            : natural := 50;

  constant c_st               : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_st;
  constant c_op_addr          : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_addr;
  constant c_op_wr            : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_wr;
  constant c_op_rd            : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_rd;
  constant c_op_rd_incr       : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_rd_incr;
  constant c_prtad            : std_logic_vector(4 downto 0) := "00000";  -- port address (example)
  constant c_devad            : std_logic_vector(4 downto 0) := "00001";  -- device address (example)
  constant c_ta               : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_ta;

  constant c_hdr_addr         : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_addr & c_prtad & c_devad & c_ta;
  constant c_hdr_wr           : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_wr   & c_prtad & c_devad & c_ta;
  constant c_hdr_rd           : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_rd   & c_prtad & c_devad & c_ta;

  constant c_phy_addr         : std_logic_vector(c_halfword_w - 1 downto 0) := std_logic_vector(to_unsigned(16#A001#, c_halfword_w));
  constant c_phy_data         : std_logic_vector(c_halfword_w - 1 downto 0) := std_logic_vector(to_unsigned(16#D001#, c_halfword_w));

  -- Register offsets:
  constant c_ofs_hdr          : natural := 0;
  constant c_ofs_tx_dat       : natural := 1;
  constant c_ofs_rx_dat       : natural := 2;
  constant c_ofs_done         : natural := 3;
  constant c_ofs_done_ack_evt : natural := 4;

  signal tb_end               : std_logic := '0';
  signal clk                  : std_logic := '1';
  signal rst                  : std_logic;

  signal phy_readdata         : std_logic_vector(c_halfword_w - 1 downto 0);

  signal mdc                  : std_logic;
  signal mdat_in              : std_logic;
  signal mdat_oen             : std_logic;
  signal mdio                 : std_logic;

  -- MDIO slave model
  constant c_slave_reg_addr   : natural := 16#A001#;
  constant c_slave_reg_data   : natural := 16#1234#;

  signal slave_addr           : std_logic_vector(c_halfword_w - 1 downto 0);  -- MDIO access address
  signal slave_reg_addr       : std_logic_vector(c_halfword_w - 1 downto 0);  -- the one data register address
  signal slave_reg_dat        : std_logic_vector(c_halfword_w - 1 downto 0);  -- the one data register data value

  signal mdio_mosi            : t_mem_mosi;
  signal mdio_miso            : t_mem_miso;

  signal hdr                  : std_logic_vector(c_halfword_w - 1 downto 0);
  signal tx_dat               : std_logic_vector(c_halfword_w - 1 downto 0);
  signal rx_dat               : std_logic_vector(c_halfword_w - 1 downto 0);

  signal en_evt               : std_logic;
  signal done                 : std_logic;
  signal done_ack_evt         : std_logic;
begin
  -- Run 25us

  clk  <= not clk or tb_end after clk_period / 2;
  rst  <= '1', '0' after clk_period * 3;

  mdio <= 'H';  -- pull up

  p_in_stimuli : process
  begin
    mdio_mosi <= c_mem_mosi_rst;

    wait until rst = '0';
    proc_common_wait_some_cycles(clk, c_delay);

    ----------------------------------------------------------------------------
    -- write_MMD
    ----------------------------------------------------------------------------
    -- write PHY register address
    proc_mem_mm_bus_wr(c_ofs_tx_dat, c_phy_addr, clk, mdio_mosi);  -- Write data (PHY address)
    proc_common_wait_some_cycles(clk, 10);

    proc_mem_mm_bus_wr(c_ofs_hdr, c_hdr_addr, clk, mdio_mosi);  -- then write header (op=addr), enables the MDIO access
    proc_common_wait_some_cycles(clk, 10);

    while done = '0' loop wait until rising_edge(clk); end loop;
    proc_mem_mm_bus_wr(c_ofs_done_ack_evt, 1, clk, mdio_mosi);  -- Acknowledge done
    proc_common_wait_some_cycles(clk, 10);

    -- write PHY register data
    proc_mem_mm_bus_wr(c_ofs_tx_dat, c_phy_data, clk, mdio_mosi);  -- Write data (PHY data)
    proc_common_wait_some_cycles(clk, 10);

    proc_mem_mm_bus_wr(c_ofs_hdr, c_hdr_wr, clk, mdio_mosi);  -- then write header (op=wr), enables the MDIO access
    proc_common_wait_some_cycles(clk, 10);

    while done = '0' loop wait until rising_edge(clk); end loop;
    proc_mem_mm_bus_wr(c_ofs_done_ack_evt, 1, clk, mdio_mosi);  -- Acknowledge done
    proc_common_wait_some_cycles(clk, 10);

    ----------------------------------------------------------------------------
    -- read_MMD
    ----------------------------------------------------------------------------
    -- write PHY register address
    proc_mem_mm_bus_wr(c_ofs_tx_dat, c_phy_addr, clk, mdio_mosi);  -- Write data (PHY address)
    proc_common_wait_some_cycles(clk, 10);

    proc_mem_mm_bus_wr(c_ofs_hdr, c_hdr_addr, clk, mdio_mosi);  -- then write header (op=addr), enables the MDIO access
    proc_common_wait_some_cycles(clk, 10);

    while done = '0' loop wait until rising_edge(clk); end loop;
    proc_mem_mm_bus_wr(c_ofs_done_ack_evt, 1, clk, mdio_mosi);  -- Acknowledge done
    proc_common_wait_some_cycles(clk, 10);

    -- read PHY register data
    proc_mem_mm_bus_wr(c_ofs_tx_dat, c_phy_data, clk, mdio_mosi);  -- Write data (PHY data)
    proc_common_wait_some_cycles(clk, 10);

    proc_mem_mm_bus_wr(c_ofs_hdr, c_hdr_rd, clk, mdio_mosi);  -- then write header (op=rd), enables the MDIO access
    proc_common_wait_some_cycles(clk, 10);

    while done = '0' loop wait until rising_edge(clk); end loop;
    proc_mem_mm_bus_wr(c_ofs_done_ack_evt, 1, clk, mdio_mosi);  -- Acknowledge done
    proc_common_wait_some_cycles(clk, 10);

    proc_common_wait_some_cycles(clk, c_delay);

    ----------------------------------------------------------------------------
    -- Read back and check value
    ----------------------------------------------------------------------------
    proc_mem_mm_bus_rd(c_ofs_rx_dat, clk, mdio_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, clk);
    phy_readdata <= mdio_miso.rddata(c_halfword_w - 1 downto 0);

    proc_common_wait_some_cycles(clk, c_delay);

    if phy_readdata = c_phy_data then
      report "Write MDIO slave reg and readback went OK"
        severity NOTE;
    else
      report "Write MDIO slave reg and readback went wrong"
        severity ERROR;
    end if;

    tb_end <= '1';
    wait;
  end process;

  -- MDIO phy register access
  u_mdio_phy_reg : entity work.mdio_phy_reg
  port map (
    mm_rst            => rst,
    mm_clk            => clk,

    mdio_rst          => rst,
    mdio_clk          => clk,

    sla_in            => mdio_mosi,
    sla_out           => mdio_miso,

    mdio_en_evt       => en_evt,
    mdio_done_ack_evt => done_ack_evt,
    mdio_done         => done,

    mdio_hdr          => hdr,
    mdio_tx_dat       => tx_dat,
    mdio_rx_dat       => rx_dat
  );

  -- MDIO master
  u_mdio_phy : entity work.mdio_phy
  port map (
    gs_sim            => c_sim,

    rst               => rst,
    clk               => clk,

    mdio_en_evt       => en_evt,
    mdio_done         => done,
    mdio_done_ack_evt => done_ack_evt,

    hdr               => hdr,
    tx_dat            => tx_dat,
    rx_dat            => rx_dat,

    -- External clock and serial data.
    mdc               => mdc,
    mdat_in           => mdat_in,
    mdat_oen          => mdat_oen
  );

  u_iobuf : entity common_lib.common_inout
  port map (
    dat_inout        => mdio,
    dat_in_from_line => mdat_in,
    dat_out_to_line  => '0',  -- for output '1' rely on external pull up, only
    dat_out_en       => mdat_oen  -- pull low when mdat_oen (so implicitely mdat_out='0')
  );

  -- MDIO slave
  u_slave : entity work.mmd_slave
  generic map (
    g_st        => c_st,
    g_prtad     => c_prtad,
    g_devad     => c_devad,
    g_reg_addr  => c_slave_reg_addr,
    g_reg_dat   => c_slave_reg_data,
    g_reg_w     => c_halfword_w
  )
  port map (
    mdc         => mdc,
    mdio        => mdio,
    addr        => slave_addr,
    reg_dat     => slave_reg_dat
  );

  slave_reg_addr <= std_logic_vector(to_unsigned(c_slave_reg_addr, c_halfword_w));
end tb;
