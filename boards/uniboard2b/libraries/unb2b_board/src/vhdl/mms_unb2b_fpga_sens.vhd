-------------------------------------------------------------------------------
--
-- Copyright (C) 2012-2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : MMS for unb2b_fpga_sens
-- Description: See unb2b_fpga_sens.vhd

library IEEE, technology_lib, common_lib, fpga_sense_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;

entity mms_unb2b_fpga_sens is
  generic (
    g_sim             : boolean := false;
    g_technology      : natural := c_tech_arria10_e1sg;
    g_temp_high       : natural := 85
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk            : in  std_logic;  -- memory-mapped bus clock
    mm_start          : in  std_logic;

    -- Memory-mapped clock domain
    reg_temp_mosi     : in  t_mem_mosi := c_mem_mosi_rst;  -- actual ranges defined by c_mm_reg
    reg_temp_miso     : out t_mem_miso;  -- actual ranges defined by c_mm_reg
    reg_voltage_mosi  : in  t_mem_mosi := c_mem_mosi_rst;  -- actual ranges defined by c_mm_reg
    reg_voltage_miso  : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- Temperature alarm output
    temp_alarm        : out std_logic
  );
end mms_unb2b_fpga_sens;

architecture str of mms_unb2b_fpga_sens is
begin
  u_fpga_sense: entity fpga_sense_lib.fpga_sense
  generic map (
    g_technology => g_technology,
    g_sim        => g_sim,
    g_temp_high  => g_temp_high
  )
  port map (
    mm_clk      => mm_clk,
    mm_rst      => mm_rst,

    start_sense => mm_start,
    temp_alarm  => temp_alarm,

    reg_temp_mosi    => reg_temp_mosi,
    reg_temp_miso    => reg_temp_miso,

    reg_voltage_store_mosi    => reg_voltage_mosi,
    reg_voltage_store_miso    => reg_voltage_miso
  );
end str;
