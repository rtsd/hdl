-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for unb1_board_sens
-- Description:
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |                xxx                     fpga_temp   = sens_data[0][7:0]|  0
--  |-----------------------------------------------------------------------|
--  |                xxx                     eth_temp    = sens_data[1][7:0]|  1
--  |-----------------------------------------------------------------------|
--  |                xxx               hot_swap_v_sense  = sens_data[2][7:0]|  2
--  |-----------------------------------------------------------------------|
--  |                xxx               hot_swap_v_source = sens_data[3][7:0]|  3
--  |-----------------------------------------------------------------------|
--  |                xxx                                         sens_err[0]|  4
--  |-----------------------------------------------------------------------|
--  |                xxx                                      temp_high[6:0]|  5
--  |-----------------------------------------------------------------------|
--
-- * The fpga_temp and eth_temp are in degrees (two's complement)
-- * The hot swap voltages depend on:
--   . From i2c_dev_ltc4260_pkg:
--     LTC4260_V_UNIT_SENSE        = 0.0003  --   0.3 mV over Rs for current sense
--     LTC4260_V_UNIT_SOURCE       = 0.4     -- 400   mV supply voltage (e.g +48 V)
--     LTC4260_V_UNIT_ADIN         = 0.01    --  10   mV ADC
--
--   . From UniBoard unb_sensors.h:
--     SENS_HOT_SWAP_R_SENSE       = 0.005   -- R sense on UniBoard is 5 mOhm (~= 10 mOhm // 10 mOhm)
--     SENS_HOT_SWAP_I_UNIT_SENSE  = LTC4260_V_UNIT_SENSE / SENS_HOT_SWAP_R_SENSE
--     SENS_HOT_SWAP_V_UNIT_SOURCE = LTC4260_V_UNIT_SOURCE
--
-- ==>
--   Via all FN and BN:
--   0 = FPGA temperature                 = TInt8(fpga_temp)
--   Only via BN3:
--   1 = UniBoard ETH PHY temperature     = TInt8(eth_temp)
--   2 = UniBoard hot swap supply current = hot_swap_v_sense * SENS_HOT_SWAP_I_UNIT_SENSE
--   3 = UniBoard hot swap supply voltage = hot_swap_v_source * SENS_HOT_SWAP_V_UNIT_SOURCE
--   4 = I2C error status for BN3 sensors access only, 0 = ok
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity unb1_board_sens_reg is
  generic (
    g_sens_nof_result : natural := 4;
    g_temp_high       : natural := 85
  );
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in     : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out    : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers
    sens_err   : in  std_logic := '0';
    sens_data  : in  t_slv_8_arr(0 to g_sens_nof_result - 1);

    -- Max temp output
    temp_high  : out std_logic_vector(6 downto 0)

  );
end unb1_board_sens_reg;

architecture rtl of unb1_board_sens_reg is
  -- Define the actual size of the MM slave register
  constant c_mm_nof_dat : natural := g_sens_nof_result + 1 + 1;  -- +1 to fit user set temp_high one additional address
                                                             -- +1 to fit sens_err in the last address

  constant c_mm_reg     : t_c_mem := (latency  => 1,
                                      adr_w    => ceil_log2(c_mm_nof_dat),
                                      dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                      nof_dat  => c_mm_nof_dat,
                                      init_sl  => '0');

  signal i_temp_high    : std_logic_vector(6 downto 0);
begin
  temp_high <= i_temp_high;

  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
    variable vA : natural := 0;
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out <= c_mem_miso_rst;
      -- Write access, register values
      i_temp_high <= TO_UVEC(g_temp_high, 7);

    elsif rising_edge(mm_clk) then
      vA := TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0));

      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        if vA = g_sens_nof_result + 1 then
            -- Only change temp_high if user writes a max. 7-bit value. This prevents accidentally
            -- setting a negative temp as temp_high, e.g. 128 which becomes -128.
            if unsigned(sla_in.wrdata(c_word_w - 1 downto 7)) = 0 then
              i_temp_high <= sla_in.wrdata(6 downto 0);
            end if;
        end if;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out        <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval  <= '1';  -- c_mm_reg.latency = 1

        -- no need to capture sens_data, it is not critical if the sens_data happens to be read just before and after an I2C access occurred
        if vA < g_sens_nof_result then
          sla_out.rddata <= RESIZE_MEM_DATA(sens_data(vA)(c_byte_w - 1 downto 0));
        elsif vA = g_sens_nof_result then
          sla_out.rddata(0) <= sens_err;  -- only valid for BN3
        else
          sla_out.rddata(6 downto 0) <= i_temp_high;
        end if;
        -- else unused addresses read zero
      end if;
    end if;
  end process;
end rtl;
