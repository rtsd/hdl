--------------------------------------------------------------------------------
-- Title       : RDMA bit width configuration package
-- Project     : Square Kilometer Array
--------------------------------------------------------------------------------
-- File        : rdma_bit_width_config_pkg.vhd
-- Author      : William Kamp <william.kamp@aut.ac.nz
-- Company     : High Performance Computing Research Lab, AUT
-- Created     : Mon Aug 19 14:12:52 2019
-- Last update : Fri Jan 31 17:34:16 2020
-- Platform    :
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2019 High Performance Computing Research Lab, AUT
-------------------------------------------------------------------------------
-- Description: Emulates an FSP's spead visibility interface.
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.AXI4_pkg.all;

package bit_width_config_pkg is
    
    subtype AXI4_streaming_RDMA_t is AXI4_Streaming_512_t;
    subtype AXI4_streaming_RDMA_t_a is AXI4_Streaming_512_t_a;
    subtype AXI4_streaming_RDMA_t_slv is AXI4_Streaming_512_t_slv;

    constant AXI4_Streaming_RDMA_t_slv_width : natural := AXI4_Streaming_512_t_slv_width;
    constant AXI4_Streaming_RDMA_t_ZERO : AXI4_streaming_RDMA_t := AXI4_Streaming_512_t_ZERO;
    constant AXI4_Streaming_RDMA_t_DONT_CARE : AXI4_streaming_RDMA_t := AXI4_Streaming_512_t_DONT_CARE;

    function to_slv (rec : AXI4_Streaming_RDMA_t) return AXI4_Streaming_RDMA_t_slv;
    function from_slv (slv : AXI4_Streaming_RDMA_t_slv) return AXI4_Streaming_RDMA_t;
    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_RDMA_t;
    
end package;

package body bit_width_config_pkg is

    function to_slv (rec : AXI4_Streaming_RDMA_t) return AXI4_Streaming_RDMA_t_slv is
    begin
        return work.AXI4_pkg.to_slv(rec);
    end function;

    function from_slv (slv : AXI4_Streaming_RDMA_t_slv) return AXI4_Streaming_RDMA_t is
    begin
        return work.AXI4_pkg.from_slv(slv);
    end function;

    function from_slv (slv : std_logic_vector; vld : std_logic) return AXI4_Streaming_RDMA_t is
    begin
        return work.AXI4_pkg.from_slv(slv, vld);
    end function;
    
end package body;
