-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;
use common_lib.all;
use common_lib.common_pkg.all;

-- Purpose: Requantize the data in the re, im or data field of the sosi record.
-- Description:
--   See common_requantize.vhd
-- Remarks:
-- . It does not take into account the ready signal from the siso record.

entity dp_requantize is
  generic (
    g_complex             : boolean := true;  -- when TRUE, the re and im field are processed, when false, the data field is processed
    g_representation      : string  := "SIGNED";  -- SIGNED (round +-0.5 away from zero to +- infinity) or UNSIGNED rounding (round 0.5 up to + inifinity)
    g_lsb_w               : integer := 4;  -- when > 0, number of LSbits to remove from in_dat
                                                  -- when < 0, number of LSBits to insert as a gain before resize to out_dat'LENGTH
                                                  -- when 0 then no effect
    g_lsb_round           : boolean := true;  -- when TRUE round else truncate the input LSbits
    g_lsb_round_clip      : boolean := false;  -- when TRUE round clip to +max to avoid wrapping to output -min (signed) or 0 (unsigned) due to rounding
    g_lsb_round_even      : boolean := true;  -- when TRUE round half to even, else round half away from zero
    g_msb_clip            : boolean := true;  -- when TRUE CLIP else WRAP the input MSbits
    g_msb_clip_symmetric  : boolean := false;  -- when TRUE clip signed symmetric to +c_smax and -c_smax, else to +c_smax and c_smin_symm
                                                  -- for wrapping when g_msb_clip=FALSE the g_msb_clip_symmetric is ignored, so signed wrapping is done asymmetric
    g_gain_w              : natural := 0;  -- do not use, must be 0, use negative g_lsb_w instead
    g_pipeline_remove_lsb : natural := 0;  -- >= 0
    g_pipeline_remove_msb : natural := 0;  -- >= 0, use g_pipeline_remove_lsb=0 and g_pipeline_remove_msb=0 for combinatorial output
    g_in_dat_w            : natural := 36;  -- input data width
    g_out_dat_w           : natural := 18  -- output data width
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_out      : out t_dp_sosi;
    --
    out_ovr      : out std_logic  -- out_ovr is '1' when the removal of MSbits causes clipping or wrapping
  );
end dp_requantize;

architecture str of dp_requantize is
  constant c_pipeline : natural := g_pipeline_remove_lsb + g_pipeline_remove_msb;

  signal snk_in_piped   : t_dp_sosi;

  signal quantized_data : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal quantized_re   : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal quantized_im   : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal out_ovr_re     : std_logic;
  signal out_ovr_im     : std_logic;
begin
  assert g_gain_w = 0
    report "dp_requantize: must use g_gain_w = 0, because gain is now supported via negative g_lsb_w."
    severity FAILURE;

  ---------------------------------------------------------------
  -- Requantize the sosi data field
  ---------------------------------------------------------------
  gen_requantize_data : if g_complex = false generate
    u_requantize_data : entity common_lib.common_requantize
    generic map (
      g_representation      => g_representation,
      g_lsb_w               => g_lsb_w,
      g_lsb_round           => g_lsb_round,
      g_lsb_round_clip      => g_lsb_round_clip,
      g_lsb_round_even      => g_lsb_round_even,
      g_msb_clip            => g_msb_clip,
      g_msb_clip_symmetric  => g_msb_clip_symmetric,
      g_pipeline_remove_lsb => g_pipeline_remove_lsb,
      g_pipeline_remove_msb => g_pipeline_remove_msb,
      g_in_dat_w            => g_in_dat_w,
      g_out_dat_w           => g_out_dat_w
    )
    port map (
      clk        => clk,
      in_dat     => snk_in.data,
      out_dat    => quantized_data,
      out_ovr    => out_ovr
    );
  end generate;

  ---------------------------------------------------------------
  -- Requantize the sosi complex fields
  ---------------------------------------------------------------
  gen_requantize_complex : if g_complex = true generate
    u_requantize_re: entity common_lib.common_requantize
    generic map (
      g_representation      => g_representation,
      g_lsb_w               => g_lsb_w,
      g_lsb_round           => g_lsb_round,
      g_lsb_round_clip      => g_lsb_round_clip,
      g_lsb_round_even      => g_lsb_round_even,
      g_msb_clip            => g_msb_clip,
      g_msb_clip_symmetric  => g_msb_clip_symmetric,
      g_pipeline_remove_lsb => g_pipeline_remove_lsb,
      g_pipeline_remove_msb => g_pipeline_remove_msb,
      g_in_dat_w            => g_in_dat_w,
      g_out_dat_w           => g_out_dat_w
    )
    port map (
      clk        => clk,
      in_dat     => snk_in.re,
      out_dat    => quantized_re,
      out_ovr    => out_ovr_re
    );

    u_requantize_im: entity common_lib.common_requantize
    generic map (
      g_representation      => g_representation,
      g_lsb_w               => g_lsb_w,
      g_lsb_round           => g_lsb_round,
      g_lsb_round_clip      => g_lsb_round_clip,
      g_lsb_round_even      => g_lsb_round_even,
      g_msb_clip            => g_msb_clip,
      g_msb_clip_symmetric  => g_msb_clip_symmetric,
      g_pipeline_remove_lsb => g_pipeline_remove_lsb,
      g_pipeline_remove_msb => g_pipeline_remove_msb,
      g_in_dat_w            => g_in_dat_w,
      g_out_dat_w           => g_out_dat_w
    )
    port map (
      clk        => clk,
      in_dat     => snk_in.im,
      out_dat    => quantized_im,
      out_ovr    => out_ovr_im
    );

    out_ovr <= out_ovr_re or out_ovr_im;
  end generate;

  --------------------------------------------------------------
  -- Pipeline to align the other sosi fields
  --------------------------------------------------------------
  u_dp_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_pipeline  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_in       => snk_in,
    -- ST source
    src_out      => snk_in_piped
  );

  process(snk_in_piped, quantized_data, quantized_re, quantized_im)
  begin
    src_out <= snk_in_piped;
    if g_complex = false then
      if g_representation = "UNSIGNED" then
        src_out.data <= RESIZE_DP_DATA( quantized_data);
      else
        src_out.data <= RESIZE_DP_SDATA(quantized_data);
      end if;
    else
      src_out.re <= RESIZE_DP_DSP_DATA(quantized_re);
      src_out.im <= RESIZE_DP_DSP_DATA(quantized_im);
    end if;
  end process;
end str;
