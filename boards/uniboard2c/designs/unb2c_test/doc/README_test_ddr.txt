Test env: hiemstra@dop421:~/git/upe_gear/peripherals

util_system_info.py --unb2 0 --pn2 0:3 -n4
export RADIOHDL=/home/hiemstra/git/hdl

# single node test:
python3 tc_unb2_test_ddr.py --unb2 0 --pn2 0 -s I,II -n 4294967295 --rep 1

# all nodes test (short duration):
python3 tc_unb2_test_ddr.py --unb2 0 --pn2 0:3 -s I,II -n 4294967295 --rep 10 > ddr4_24h_stress_test.txt

# 24 hours
python3 tc_unb2_test_ddr.py --unb2 0 --pn2 0:3 -s I,II -n 4294967295 --rep 13500 > ddr4_24h_stress_test.txt


