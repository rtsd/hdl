#------------------------------------------------------------------------------
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on Megawizard-generated file msim_setup.tcl.

# Get the memory model for the uphy_4g_* from the ip_stratixiv_ddr3_uphy_4g_800_master example design
set IP_DIR "$env(HDL_BUILD_DIR)/unb1/qmegawiz/ip_stratixiv_ddr3_uphy_4g_800_master_example_design"

# Assume library work already exists

# Compile the design files in correct order and map them all to library work
vlog -sv "$IP_DIR/simulation/vhdl/submodules/alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en.sv"            -work work
vlog -sv "$IP_DIR/simulation/vhdl/submodules/mentor/alt_mem_if_common_ddr_mem_model_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en.sv"   -work work

# or use the binary files from the mentor/ subdirectory as in msim_setup.tcl:
#vlog -sv "$IP_DIR/simulation/vhdl/submodules/alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en.sv"            -work work
#vlog -sv "$IP_DIR/simulation/vhdl/submodules/mentor/alt_mem_if_common_ddr_mem_model_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en.sv"   -work work
