--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- u_inst_mmm_unb1_terminal_bg_mesh_db : ENTITY work.mmm_unb1_terminal_bg_mesh_db
--   GENERIC MAP(
--     g_sim         => g_sim,
--     g_sim_unb_nr  => g_sim_unb_nr,
--     g_sim_node_nr => g_sim_node_nr
--   )
--   PORT MAP(
--     mm_clk                      =>  mm_clk,
--     mm_rst                      =>  mm_rst,
--     pout_wdi                    =>  pout_wdi,
--     reg_wdi_mosi                =>  reg_wdi_mosi,
--     reg_wdi_miso                =>  reg_wdi_miso,
--     reg_unb_system_info_mosi    =>  reg_unb_system_info_mosi,
--     reg_unb_system_info_miso    =>  reg_unb_system_info_miso,
--     rom_unb_system_info_mosi    =>  rom_unb_system_info_mosi,
--     rom_unb_system_info_miso    =>  rom_unb_system_info_miso,
--     reg_unb_sens_mosi           =>  reg_unb_sens_mosi,
--     reg_unb_sens_miso           =>  reg_unb_sens_miso,
--     reg_ppsh_mosi               =>  reg_ppsh_mosi,
--     reg_ppsh_miso               =>  reg_ppsh_miso,
--     eth1g_mm_rst                =>  eth1g_mm_rst,
--     eth1g_reg_interrupt         =>  eth1g_reg_interrupt,
--     eth1g_ram_mosi              =>  eth1g_ram_mosi,
--     eth1g_ram_miso              =>  eth1g_ram_miso,
--     eth1g_reg_mosi              =>  eth1g_reg_mosi,
--     eth1g_reg_miso              =>  eth1g_reg_miso,
--     eth1g_tse_mosi              =>  eth1g_tse_mosi,
--     eth1g_tse_miso              =>  eth1g_tse_miso,
--     reg_diag_data_buf_mosi      =>  reg_diag_data_buf_mosi,
--     reg_diag_data_buf_miso      =>  reg_diag_data_buf_miso,
--     ram_diag_data_buf_mosi      =>  ram_diag_data_buf_mosi,
--     ram_diag_data_buf_miso      =>  ram_diag_data_buf_miso,
--     reg_diag_bg_mosi            =>  reg_diag_bg_mosi,
--     reg_diag_bg_miso            =>  reg_diag_bg_miso,
--     ram_diag_bg_mosi            =>  ram_diag_bg_mosi,
--     ram_diag_bg_miso            =>  ram_diag_bg_miso,
--     reg_diagnostics_mosi        =>  reg_diagnostics_mosi,
--     reg_diagnostics_miso        =>  reg_diagnostics_miso,
--     reg_tr_nonbonded_mosi       =>  reg_tr_nonbonded_mosi,
--     reg_tr_nonbonded_miso       =>  reg_tr_nonbonded_miso,
--     ram_mesh_diag_data_buf_mosi =>  ram_mesh_diag_data_buf_mosi,
--     ram_mesh_diag_data_buf_miso =>  ram_mesh_diag_data_buf_miso,
--     reg_bsn_monitor_mosi        =>  reg_bsn_monitor_mosi,
--     reg_bsn_monitor_miso        =>  reg_bsn_monitor_miso
--   );
--
library IEEE, common_lib, unb1_board_lib, mm_lib, eth_lib, technology_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use eth_lib.eth_pkg.all;
use technology_lib.technology_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;

entity mmm_unb1_terminal_bg_mesh_db is
  generic (
    g_sim         : boolean := false;
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0
  );
  port (
    mm_clk                      : in  std_logic := '1';
    mm_rst                      : in  std_logic := '1';
    pout_wdi                    : out std_logic := '1';
    reg_wdi_mosi                : out t_mem_mosi;
    reg_wdi_miso                : in  t_mem_miso := c_mem_miso_rst;
    reg_unb_system_info_mosi    : out t_mem_mosi;
    reg_unb_system_info_miso    : in  t_mem_miso := c_mem_miso_rst;
    rom_unb_system_info_mosi    : out t_mem_mosi;
    rom_unb_system_info_miso    : in  t_mem_miso := c_mem_miso_rst;
    reg_unb_sens_mosi           : out t_mem_mosi;
    reg_unb_sens_miso           : in  t_mem_miso := c_mem_miso_rst;
    reg_ppsh_mosi               : out t_mem_mosi;
    reg_ppsh_miso               : in  t_mem_miso := c_mem_miso_rst;
    eth1g_mm_rst                : out std_logic;
    eth1g_reg_interrupt         : in  std_logic;
    eth1g_ram_mosi              : out t_mem_mosi;
    eth1g_ram_miso              : in  t_mem_miso := c_mem_miso_rst;
    eth1g_reg_mosi              : out t_mem_mosi;
    eth1g_reg_miso              : in  t_mem_miso := c_mem_miso_rst;
    eth1g_tse_mosi              : out t_mem_mosi;
    eth1g_tse_miso              : in  t_mem_miso := c_mem_miso_rst;
    reg_diag_data_buf_mosi      : out t_mem_mosi;
    reg_diag_data_buf_miso      : in  t_mem_miso := c_mem_miso_rst;
    ram_diag_data_buf_mosi      : out t_mem_mosi;
    ram_diag_data_buf_miso      : in  t_mem_miso := c_mem_miso_rst;
    reg_diag_bg_mosi            : out t_mem_mosi;
    reg_diag_bg_miso            : in  t_mem_miso := c_mem_miso_rst;
    ram_diag_bg_mosi            : out t_mem_mosi;
    ram_diag_bg_miso            : in  t_mem_miso := c_mem_miso_rst;
    reg_diagnostics_mosi        : out t_mem_mosi;
    reg_diagnostics_miso        : in  t_mem_miso := c_mem_miso_rst;
    reg_tr_nonbonded_mosi       : out t_mem_mosi;
    reg_tr_nonbonded_miso       : in  t_mem_miso := c_mem_miso_rst;
    ram_mesh_diag_data_buf_mosi : out t_mem_mosi;
    ram_mesh_diag_data_buf_miso : in  t_mem_miso := c_mem_miso_rst;
    reg_bsn_monitor_mosi        : out t_mem_mosi;
    reg_bsn_monitor_miso        : in  t_mem_miso := c_mem_miso_rst
  );
end entity mmm_unb1_terminal_bg_mesh_db;

architecture str of mmm_unb1_terminal_bg_mesh_db is
  constant c_sim_node_type         : string(1 to 2)                                := sel_a_b(g_sim_node_nr < 4, "FN", "BN");
  constant c_sim_node_nr           : natural                                       := sel_a_b(c_sim_node_type = "BN", g_sim_node_nr - 4, g_sim_node_nr);
  constant c_sim_eth_src_mac       : std_logic_vector(c_network_eth_mac_slv'range) := X"00228608" & TO_UVEC(g_sim_unb_nr, c_byte_w) & TO_UVEC(g_sim_node_nr, c_byte_w);
  constant c_sim_eth_control_rx_en : natural                                       := 2**c_eth_mm_reg_control_bi.rx_en;

  signal sim_eth_mm_bus_switch : std_logic;
  signal sim_eth_psc_access    : std_logic;
  signal i_eth1g_reg_mosi      : t_mem_mosi;
  signal i_eth1g_reg_miso      : t_mem_miso;
  signal mm_rst_n              : std_logic;
  signal sim_eth1g_reg_mosi    : t_mem_mosi;

  component qsys_unb1_terminal_bg_mesh_db is
    port (
      reg_diag_bg_reset_export                : out std_logic;
      ram_diag_bg_read_export                 : out std_logic;
      ram_diag_data_buf_readdata_export       : in  std_logic_vector(31 downto 0) := (others => '0');
      eth1g_reg_readdata_export               : in  std_logic_vector(31 downto 0) := (others => '0');
      reset_in_reset_n                        : in  std_logic := '0';
      reg_diagnostics_write_export            : out std_logic;
      pio_pps_address_export                  : out std_logic;
      pio_system_info_address_export          : out std_logic_vector(4 downto 0);
      reg_diag_data_buf_write_export          : out std_logic;
      pio_pps_reset_export                    : out std_logic;
      ram_mesh_diag_data_buf_readdata_export  : in  std_logic_vector(31 downto 0) := (others => '0');
      eth1g_tse_writedata_export              : out std_logic_vector(31 downto 0);
      ram_mesh_diag_data_buf_reset_export     : out std_logic;
      eth1g_ram_readdata_export               : in  std_logic_vector(31 downto 0) := (others => '0');
      ram_diag_data_buf_address_export        : out std_logic_vector(13 downto 0);
      eth1g_ram_address_export                : out std_logic_vector(9 downto 0);
      pio_pps_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_diag_data_buf_address_export        : out std_logic;
      reg_diag_bg_writedata_export            : out std_logic_vector(31 downto 0);
      pio_system_info_writedata_export        : out std_logic_vector(31 downto 0);
      eth1g_reg_writedata_export              : out std_logic_vector(31 downto 0);
      reg_unb_sens_reset_export               : out std_logic;
      eth1g_tse_address_export                : out std_logic_vector(9 downto 0);
      reg_wdi_reset_export                    : out std_logic;
      clk_in_clk                              : in  std_logic := '0';
      rom_system_info_clk_export              : out std_logic;
      reg_unb_sens_read_export                : out std_logic;
      reg_unb_sens_write_export               : out std_logic;
      reg_diag_data_buf_readdata_export       : in  std_logic_vector(31 downto 0) := (others => '0');
      ram_diag_bg_reset_export                : out std_logic;
      reg_diag_data_buf_writedata_export      : out std_logic_vector(31 downto 0);
      reg_diag_data_buf_read_export           : out std_logic;
      eth1g_tse_readdata_export               : in  std_logic_vector(31 downto 0) := (others => '0');
      eth1g_ram_write_export                  : out std_logic;
      reg_diagnostics_writedata_export        : out std_logic_vector(31 downto 0);
      reg_unb_sens_clk_export                 : out std_logic;
      reg_tr_nonbonded_clk_export             : out std_logic;
      ram_mesh_diag_data_buf_clk_export       : out std_logic;
      ram_diag_bg_write_export                : out std_logic;
      ram_diag_bg_address_export              : out std_logic_vector(10 downto 0);
      ram_mesh_diag_data_buf_writedata_export : out std_logic_vector(31 downto 0);
      reg_wdi_read_export                     : out std_logic;
      eth1g_ram_read_export                   : out std_logic;
      eth1g_reg_read_export                   : out std_logic;
      ram_diag_bg_writedata_export            : out std_logic_vector(31 downto 0);
      reg_bsn_monitor_reset_export            : out std_logic;
      eth1g_tse_write_export                  : out std_logic;
      reg_unb_sens_readdata_export            : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_bsn_monitor_write_export            : out std_logic;
      ram_mesh_diag_data_buf_address_export   : out std_logic_vector(13 downto 0);
      pio_pps_clk_export                      : out std_logic;
      eth1g_reg_address_export                : out std_logic_vector(3 downto 0);
      reg_diag_bg_address_export              : out std_logic_vector(2 downto 0);
      pio_system_info_readdata_export         : in  std_logic_vector(31 downto 0) := (others => '0');
      rom_system_info_writedata_export        : out std_logic_vector(31 downto 0);
      reg_tr_nonbonded_reset_export           : out std_logic;
      ram_mesh_diag_data_buf_read_export      : out std_logic;
      reg_bsn_monitor_read_export             : out std_logic;
      reg_tr_nonbonded_address_export         : out std_logic_vector(3 downto 0);
      reg_wdi_address_export                  : out std_logic;
      reg_diag_bg_clk_export                  : out std_logic;
      pio_system_info_write_export            : out std_logic;
      reg_tr_nonbonded_writedata_export       : out std_logic_vector(31 downto 0);
      reg_tr_nonbonded_write_export           : out std_logic;
      pio_pps_write_export                    : out std_logic;
      rom_system_info_write_export            : out std_logic;
      ram_diag_data_buf_writedata_export      : out std_logic_vector(31 downto 0);
      rom_system_info_read_export             : out std_logic;
      reg_diagnostics_address_export          : out std_logic_vector(5 downto 0);
      ram_diag_data_buf_write_export          : out std_logic;
      reg_tr_nonbonded_readdata_export        : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_diag_data_buf_clk_export            : out std_logic;
      reg_diag_bg_read_export                 : out std_logic;
      ram_diag_data_buf_reset_export          : out std_logic;
      eth1g_tse_read_export                   : out std_logic;
      ram_diag_bg_clk_export                  : out std_logic;
      reg_unb_sens_writedata_export           : out std_logic_vector(31 downto 0);
      eth1g_ram_writedata_export              : out std_logic_vector(31 downto 0);
      reg_tr_nonbonded_read_export            : out std_logic;
      out_port_from_the_pio_debug_wave        : out std_logic_vector(31 downto 0);
      pio_system_info_reset_export            : out std_logic;
      reg_wdi_writedata_export                : out std_logic_vector(31 downto 0);
      reg_diag_data_buf_reset_export          : out std_logic;
      reg_bsn_monitor_writedata_export        : out std_logic_vector(31 downto 0);
      pio_system_info_read_export             : out std_logic;
      reg_bsn_monitor_address_export          : out std_logic_vector(6 downto 0);
      ram_diag_data_buf_clk_export            : out std_logic;
      ram_diag_data_buf_read_export           : out std_logic;
      reg_wdi_clk_export                      : out std_logic;
      reg_diag_bg_write_export                : out std_logic;
      reg_diagnostics_read_export             : out std_logic;
      eth1g_mm_rst_export                     : out std_logic;
      reg_diagnostics_reset_export            : out std_logic;
      out_port_from_the_pio_wdi               : out std_logic;
      reg_bsn_monitor_clk_export              : out std_logic;
      eth1g_reg_write_export                  : out std_logic;
      reg_diag_bg_readdata_export             : in  std_logic_vector(31 downto 0) := (others => '0');
      rom_system_info_readdata_export         : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_wdi_write_export                    : out std_logic;
      reg_wdi_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => '0');
      pio_pps_read_export                     : out std_logic;
      pio_system_info_clk_export              : out std_logic;
      ram_mesh_diag_data_buf_write_export     : out std_logic;
      pio_pps_writedata_export                : out std_logic_vector(31 downto 0);
      eth1g_tse_waitrequest_export            : in  std_logic := '0';
      reg_bsn_monitor_readdata_export         : in  std_logic_vector(31 downto 0) := (others => '0');
      eth1g_mm_clk_export                     : out std_logic;
      rom_system_info_reset_export            : out std_logic;
      reg_unb_sens_address_export             : out std_logic_vector(2 downto 0);
      rom_system_info_address_export          : out std_logic_vector(9 downto 0);
      eth1g_irq_export                        : in  std_logic := '0';
      reg_diagnostics_readdata_export         : in  std_logic_vector(31 downto 0) := (others => '0');
      ram_diag_bg_readdata_export             : in  std_logic_vector(31 downto 0) := (others => '0');
      reg_diagnostics_clk_export              : out std_logic
    );
  end component qsys_unb1_terminal_bg_mesh_db;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $HDL_IOFILE_SIM_DIR.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    u_mm_file_reg_wdi  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
          port map(mm_rst, mm_clk, reg_wdi_mosi, reg_wdi_miso );
    u_mm_file_reg_unb_system_info  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
          port map(mm_rst, mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );
    u_mm_file_rom_unb_system_info  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
          port map(mm_rst, mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );
    u_mm_file_reg_unb_sens  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
          port map(mm_rst, mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso );
    u_mm_file_reg_ppsh  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
          port map(mm_rst, mm_clk, reg_ppsh_mosi, reg_ppsh_miso );
    u_mm_file_eth1g_ram  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ETH1G_RAM")
          port map(mm_rst, mm_clk, eth1g_ram_mosi, eth1g_ram_miso );
    u_mm_file_eth1g_reg  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
          port map(mm_rst, mm_clk, eth1g_reg_mosi, eth1g_reg_miso );
    u_mm_file_eth1g_tse  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ETH1G_TSE")
          port map(mm_rst, mm_clk, eth1g_tse_mosi, eth1g_tse_miso );
    u_mm_file_reg_diag_data_buf  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER")
          port map(mm_rst, mm_clk, reg_diag_data_buf_mosi, reg_diag_data_buf_miso );
    u_mm_file_ram_diag_data_buf  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER")
          port map(mm_rst, mm_clk, ram_diag_data_buf_mosi, ram_diag_data_buf_miso );
    u_mm_file_reg_diag_bg  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG")
          port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso );
    u_mm_file_ram_diag_bg  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_BG")
          port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso );
    u_mm_file_reg_diagnostics  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAGNOSTICS")
          port map(mm_rst, mm_clk, reg_diagnostics_mosi, reg_diagnostics_miso );
    u_mm_file_reg_tr_nonbonded  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_NONBONDED")
          port map(mm_rst, mm_clk, reg_tr_nonbonded_mosi, reg_tr_nonbonded_miso );
    u_mm_file_ram_mesh_diag_data_buf  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_MESH_DIAG_DATA_BUF")
          port map(mm_rst, mm_clk, ram_mesh_diag_data_buf_mosi, ram_mesh_diag_data_buf_miso );
    u_mm_file_reg_bsn_monitor  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR")
          port map(mm_rst, mm_clk, reg_bsn_monitor_mosi, reg_bsn_monitor_miso );
    ----------------------------------------------------------------------------
    -- 1GbE setup sequence normally performed by unb_os@NIOS
    ----------------------------------------------------------------------------
    p_eth_setup : process
    begin
      sim_eth_mm_bus_switch <= '1';
      eth1g_tse_mosi.wr <= '0';
      eth1g_tse_mosi.rd <= '0';
      wait for 400 ns;
      wait until rising_edge(mm_clk);
      proc_tech_tse_setup(c_tech_stratixiv, false, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tech_tse_tx_ready_latency, c_sim_eth_src_mac, sim_eth_psc_access, mm_clk, eth1g_tse_miso, eth1g_tse_mosi);
      -- Enable RX
      proc_mem_mm_bus_wr(c_eth_reg_control_wi + 0, c_sim_eth_control_rx_en, mm_clk, eth1g_reg_miso, sim_eth1g_reg_mosi);  -- control rx en
      sim_eth_mm_bus_switch <= '0';
      wait;
    end process;

    p_switch : process(sim_eth_mm_bus_switch, sim_eth1g_reg_mosi, i_eth1g_reg_mosi)
    begin
      if sim_eth_mm_bus_switch = '1' then
        eth1g_reg_mosi <= sim_eth1g_reg_mosi;
      else
        eth1g_reg_mosi <= i_eth1g_reg_mosi;
      end if;
    end process;

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  ----------------------------------------------------------------------------
  -- SOPC or QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys_unb1_terminal_bg_mesh_db : if g_sim = false generate
    mm_rst_n <= not(mm_rst);

    u_qsys_unb1_terminal_bg_mesh_db : qsys_unb1_terminal_bg_mesh_db
      port map(
      clk_in_clk                              => mm_clk,
      eth1g_irq_export                        => eth1g_reg_interrupt,
      eth1g_mm_clk_export                     => OPEN,
      eth1g_mm_rst_export                     => eth1g_mm_rst,
      eth1g_ram_address_export                => eth1g_ram_mosi.address(9 downto 0),
      eth1g_ram_read_export                   => eth1g_ram_mosi.rd,
      eth1g_ram_readdata_export               => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),
      eth1g_ram_write_export                  => eth1g_ram_mosi.wr,
      eth1g_ram_writedata_export              => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      eth1g_reg_address_export                => eth1g_reg_mosi.address(3 downto 0),
      eth1g_reg_read_export                   => eth1g_reg_mosi.rd,
      eth1g_reg_readdata_export               => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      eth1g_reg_write_export                  => eth1g_reg_mosi.wr,
      eth1g_reg_writedata_export              => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      eth1g_tse_address_export                => eth1g_tse_mosi.address(9 downto 0),
      eth1g_tse_read_export                   => eth1g_tse_mosi.rd,
      eth1g_tse_readdata_export               => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      eth1g_tse_waitrequest_export            => eth1g_tse_miso.waitrequest,
      eth1g_tse_write_export                  => eth1g_tse_mosi.wr,
      eth1g_tse_writedata_export              => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      out_port_from_the_pio_debug_wave        => OPEN,
      out_port_from_the_pio_wdi               => pout_wdi,
      pio_pps_address_export                  => reg_ppsh_mosi.address(0),
      pio_pps_clk_export                      => OPEN,
      pio_pps_read_export                     => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                 => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),
      pio_pps_reset_export                    => OPEN,
      pio_pps_write_export                    => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_address_export          => reg_unb_system_info_mosi.address(4 downto 0),
      pio_system_info_clk_export              => OPEN,
      pio_system_info_read_export             => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export         => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      pio_system_info_reset_export            => OPEN,
      pio_system_info_write_export            => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export        => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_bg_address_export              => ram_diag_bg_mosi.address(10 downto 0),
      ram_diag_bg_clk_export                  => OPEN,
      ram_diag_bg_read_export                 => ram_diag_bg_mosi.rd,
      ram_diag_bg_readdata_export             => ram_diag_bg_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_bg_reset_export                => OPEN,
      ram_diag_bg_write_export                => ram_diag_bg_mosi.wr,
      ram_diag_bg_writedata_export            => ram_diag_bg_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buf_address_export        => ram_diag_data_buf_mosi.address(13 downto 0),
      ram_diag_data_buf_clk_export            => OPEN,
      ram_diag_data_buf_read_export           => ram_diag_data_buf_mosi.rd,
      ram_diag_data_buf_readdata_export       => ram_diag_data_buf_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_data_buf_reset_export          => OPEN,
      ram_diag_data_buf_write_export          => ram_diag_data_buf_mosi.wr,
      ram_diag_data_buf_writedata_export      => ram_diag_data_buf_mosi.wrdata(c_word_w - 1 downto 0),
      ram_mesh_diag_data_buf_address_export   => ram_mesh_diag_data_buf_mosi.address(13 downto 0),
      ram_mesh_diag_data_buf_clk_export       => OPEN,
      ram_mesh_diag_data_buf_read_export      => ram_mesh_diag_data_buf_mosi.rd,
      ram_mesh_diag_data_buf_readdata_export  => ram_mesh_diag_data_buf_miso.rddata(c_word_w - 1 downto 0),
      ram_mesh_diag_data_buf_reset_export     => OPEN,
      ram_mesh_diag_data_buf_write_export     => ram_mesh_diag_data_buf_mosi.wr,
      ram_mesh_diag_data_buf_writedata_export => ram_mesh_diag_data_buf_mosi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_address_export          => reg_bsn_monitor_mosi.address(6 downto 0),
      reg_bsn_monitor_clk_export              => OPEN,
      reg_bsn_monitor_read_export             => reg_bsn_monitor_mosi.rd,
      reg_bsn_monitor_readdata_export         => reg_bsn_monitor_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_reset_export            => OPEN,
      reg_bsn_monitor_write_export            => reg_bsn_monitor_mosi.wr,
      reg_bsn_monitor_writedata_export        => reg_bsn_monitor_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_bg_address_export              => reg_diag_bg_mosi.address(2 downto 0),
      reg_diag_bg_clk_export                  => OPEN,
      reg_diag_bg_read_export                 => reg_diag_bg_mosi.rd,
      reg_diag_bg_readdata_export             => reg_diag_bg_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_bg_reset_export                => OPEN,
      reg_diag_bg_write_export                => reg_diag_bg_mosi.wr,
      reg_diag_bg_writedata_export            => reg_diag_bg_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buf_address_export        => reg_diag_data_buf_mosi.address(0),
      reg_diag_data_buf_clk_export            => OPEN,
      reg_diag_data_buf_read_export           => reg_diag_data_buf_mosi.rd,
      reg_diag_data_buf_readdata_export       => reg_diag_data_buf_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_data_buf_reset_export          => OPEN,
      reg_diag_data_buf_write_export          => reg_diag_data_buf_mosi.wr,
      reg_diag_data_buf_writedata_export      => reg_diag_data_buf_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diagnostics_address_export          => reg_diagnostics_mosi.address(5 downto 0),
      reg_diagnostics_clk_export              => OPEN,
      reg_diagnostics_read_export             => reg_diagnostics_mosi.rd,
      reg_diagnostics_readdata_export         => reg_diagnostics_miso.rddata(c_word_w - 1 downto 0),
      reg_diagnostics_reset_export            => OPEN,
      reg_diagnostics_write_export            => reg_diagnostics_mosi.wr,
      reg_diagnostics_writedata_export        => reg_diagnostics_mosi.wrdata(c_word_w - 1 downto 0),
      reg_tr_nonbonded_address_export         => reg_tr_nonbonded_mosi.address(3 downto 0),
      reg_tr_nonbonded_clk_export             => OPEN,
      reg_tr_nonbonded_read_export            => reg_tr_nonbonded_mosi.rd,
      reg_tr_nonbonded_readdata_export        => reg_tr_nonbonded_miso.rddata(c_word_w - 1 downto 0),
      reg_tr_nonbonded_reset_export           => OPEN,
      reg_tr_nonbonded_write_export           => reg_tr_nonbonded_mosi.wr,
      reg_tr_nonbonded_writedata_export       => reg_tr_nonbonded_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_address_export             => reg_unb_sens_mosi.address(2 downto 0),
      reg_unb_sens_clk_export                 => OPEN,
      reg_unb_sens_read_export                => reg_unb_sens_mosi.rd,
      reg_unb_sens_readdata_export            => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
      reg_unb_sens_reset_export               => OPEN,
      reg_unb_sens_write_export               => reg_unb_sens_mosi.wr,
      reg_unb_sens_writedata_export           => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_address_export                  => reg_wdi_mosi.address(0),
      reg_wdi_clk_export                      => OPEN,
      reg_wdi_read_export                     => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                 => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
      reg_wdi_reset_export                    => OPEN,
      reg_wdi_write_export                    => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reset_in_reset_n                        => mm_rst_n,
      rom_system_info_address_export          => rom_unb_system_info_mosi.address(9 downto 0),
      rom_system_info_clk_export              => OPEN,
      rom_system_info_read_export             => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export         => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      rom_system_info_reset_export            => OPEN,
      rom_system_info_write_export            => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export        => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0)
    );
  end generate;
end str;
