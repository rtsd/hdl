library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

architecture rtl of pfs_combine is
  signal i_out_dat_x        : std_logic_vector(out_dat_x'range);
  signal nxt_out_dat_x      : std_logic_vector(out_dat_x'range);
  signal i_out_dat_y        : std_logic_vector(out_dat_y'range);
  signal nxt_out_dat_y      : std_logic_vector(out_dat_y'range);
  signal nxt_out_val        : std_logic;
  signal nxt_out_sync       : std_logic;
begin
  out_dat_x <= i_out_dat_x;
  out_dat_y <= i_out_dat_y;

  registers : process (clk, rst)
  begin
    if rst = '1' then
      -- Outputs.
      i_out_dat_x       <= (others => '0');
      i_out_dat_y       <= (others => '0');
      out_val           <= '0';
      out_sync          <= '0';
    elsif rising_edge(clk) then
      -- Inputs.
      -- Outputs.
      i_out_dat_x       <= nxt_out_dat_x;
      i_out_dat_y       <= nxt_out_dat_y;
      out_val           <= nxt_out_val;
      out_sync          <= nxt_out_sync;
    end if;
  end process;

  select_value : process (i_out_dat_x, i_out_dat_y, in_val, in_dat_x, in_dat_y,
      in_sync)
  begin
    nxt_out_dat_x <= i_out_dat_x;
    nxt_out_dat_y <= i_out_dat_y;
    nxt_out_val <= '0';
    nxt_out_sync <= '0';

    for i in in_val'range loop
      if in_val(i) = '1' then
        nxt_out_dat_x <= in_dat_x((i + 1) * g_data_w - 1 downto i * g_data_w);
        nxt_out_dat_y <= in_dat_y((i + 1) * g_data_w - 1 downto i * g_data_w);
        nxt_out_val <= '1';
        nxt_out_sync <= in_sync(i);
      end if;
    end loop;
  end process;
end rtl;
