-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Define selections for revisions of the unb2c_test design

library IEEE, common_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_field_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

package unb2c_test_pkg is
  -- dp_offload_tx (carried over from unb2a_test_pkg
  --CONSTANT c_nof_hdr_fields : NATURAL := 1+3+12+4+2; -- Total header bits = 384 = 6 64b words
  constant c_nof_hdr_fields : natural := 3 + 12 + 4 + 2;  -- Total header bits = 384 = 6 64b words
  constant c_hdr_field_arr  : t_common_field_arr(c_nof_hdr_fields - 1 downto 0) := (  -- ( field_name_pad("align"              ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("eth_dst_mac"        ), "  ", 48, field_default(0) ),
                                                                                   ( field_name_pad("eth_src_mac"        ), "  ", 48, field_default(0) ),
                                                                                   ( field_name_pad("eth_type"           ), "  ", 16, field_default(x"0800") ),
                                                                                   ( field_name_pad("ip_version"         ), "  ",  4, field_default(4) ),
                                                                                   ( field_name_pad("ip_header_length"   ), "  ",  4, field_default(5) ),
                                                                                   ( field_name_pad("ip_services"        ), "  ",  8, field_default(0) ),
                                                                                   ( field_name_pad("ip_total_length"    ), "  ", 16, field_default(0) ),  -- FIXME fill this in for non point-to-point use
                                                                                   ( field_name_pad("ip_identification"  ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_flags"           ), "  ",  3, field_default(2) ),
                                                                                   ( field_name_pad("ip_fragment_offset" ), "  ", 13, field_default(0) ),
                                                                                   ( field_name_pad("ip_time_to_live"    ), "  ",  8, field_default(127) ),
                                                                                   ( field_name_pad("ip_protocol"        ), "  ",  8, field_default(17) ),
                                                                                   ( field_name_pad("ip_header_checksum" ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_src_addr"        ), "  ", 32, field_default(0) ),
                                                                                   ( field_name_pad("ip_dst_addr"        ), "  ", 32, field_default(0) ),
                                                                                   ( field_name_pad("udp_src_port"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_dst_port"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_total_length"   ), "  ", 16, field_default(0) ),  -- FIXME fill this in for non point-to-point use
                                                                                   ( field_name_pad("udp_checksum"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("usr_sync"           ), "  ",  1, field_default(0) ),
                                                                                   ( field_name_pad("usr_bsn"            ), "  ", 47, field_default(0) ));

  --CONSTANT c_hdr_field_ovr_init         : STD_LOGIC_VECTOR(c_nof_hdr_fields-1 DOWNTO 0) := "1001"&"111111111100"&"0011"&"00";
  constant c_hdr_field_ovr_init         : std_logic_vector(c_nof_hdr_fields - 1 downto 0) := "001" & "111111111100" & "0011" & "00";
  -----------------------------------------------------------------------------
  -- Revision control
  -----------------------------------------------------------------------------

  type t_unb2c_test_config is record
    use_loopback        : boolean;  -- for pinning designs
    use_1GbE_I_UDP      : boolean;  -- Use the UDP offload on eth0. Eth0 is always enabled for control
    use_1GbE_II         : boolean;  -- Intantiate eth1 for pinning designs
    use_10GbE_qsfp      : boolean;
    use_10GbE_ring      : boolean;
    use_10GbE_back0     : boolean;
    use_jesd204b        : boolean;
    use_MB_I            : boolean;
    use_MB_II           : boolean;
    use_heater          : boolean;
    type_MB_I           : t_c_tech_ddr;
    type_MB_II          : t_c_tech_ddr;
  end record;

  --                                                     loop  1GbE  1GbE  qsfp  ring  bk0   jesd  DDR4  DDR4 heatr
  constant c_test_minimal     : t_unb2c_test_config := (false, false, false, false, false, false, false, false, false, false, c_tech_ddr4_8g_1600m, c_tech_ddr4_8g_1600m);
  constant c_test_1GbE_I_UDP  : t_unb2c_test_config := (false, true, false, false, false, false, false, false, false, false, c_tech_ddr4_8g_1600m, c_tech_ddr4_8g_1600m);
  constant c_test_1GbE_II_UDP : t_unb2c_test_config := (false, true, true, false, false, false, false, false, false, false, c_tech_ddr4_8g_1600m, c_tech_ddr4_8g_1600m);
  constant c_test_10GbE       : t_unb2c_test_config := (false, false, false, true, true, false, false, false, false, false, c_tech_ddr4_8g_1600m, c_tech_ddr4_8g_1600m);
  constant c_test_10GbE_qb    : t_unb2c_test_config := (false, false, false, true, false, true, false, false, false, false, c_tech_ddr4_8g_1600m, c_tech_ddr4_8g_1600m);
  constant c_test_ddr         : t_unb2c_test_config := (false, false, false, false, false, false, false, true, true, false, c_tech_ddr4_8g_1600m, c_tech_ddr4_8g_1600m);
  constant c_test_ddr_16G     : t_unb2c_test_config := (false, false, false, false, false, false, false, true, true, false, c_tech_ddr4_16g_1600m_64, c_tech_ddr4_16g_1600m_72_64);
  constant c_test_ddr_16G_I   : t_unb2c_test_config := (false, false, false, false, false, false, false, true, false, false, c_tech_ddr4_16g_1600m_64, c_tech_ddr4_16g_1600m_72_64);
  constant c_test_heater      : t_unb2c_test_config := (false, false, false, false, false, false, false, false, false, true, c_tech_ddr4_8g_1600m, c_tech_ddr4_8g_1600m);
  constant c_test_jesd204b    : t_unb2c_test_config := (false, false, false, false, false, false, true, false, false, false, c_tech_ddr4_8g_1600m, c_tech_ddr4_8g_1600m);

  -- Function to select the revision configuration.
  function func_sel_revision_rec(g_design_name : string) return t_unb2c_test_config;
end unb2c_test_pkg;

package body unb2c_test_pkg is
  function func_sel_revision_rec(g_design_name : string) return t_unb2c_test_config is
  begin
    if    g_design_name = "unb2c_test_10GbE"            then return c_test_10GbE;
    elsif g_design_name = "unb2c_test_ddr"              then return c_test_ddr;
    elsif g_design_name = "unb2c_test_heater"           then return c_test_heater;
    elsif g_design_name = "unb2c_test_jesd204b"         then return c_test_jesd204b;
    elsif g_design_name = "unb2c_test_1GbE_I"           then return c_test_1GbE_I_UDP;
    elsif g_design_name = "unb2c_test_1GbE_II"          then return c_test_1GbE_II_UDP;
    elsif g_design_name = "unb2c_test_ddr_16G"          then return c_test_ddr_16G;
    elsif g_design_name = "unb2c_test_ddr_16G_I"        then return c_test_ddr_16G_I;
    else  return c_test_minimal;
    end if;

  end;
end unb2c_test_pkg;
