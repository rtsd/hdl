--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_field_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

package unb1_test_pkg is
  type t_unb1_test_config is record
    use_front             : natural;
    use_back              : natural;
    use_streaming_1GbE    : boolean;
    use_nof_streams_1GbE  : natural;
    use_10GbE             : boolean;
    use_nof_streams_10GbE : natural;
    use_ddr_MB_I          : natural;
    use_ddr_MB_II         : natural;
    use_nof_streams_ddr   : natural;
    use_tech_ddr          : t_c_tech_ddr;
  end record;

  -- dp_offload_tx
  constant c_nof_hdr_fields : natural := 4 + 12 + 4 + 9;  -- Total header bits = 512
  constant c_hdr_field_arr  : t_common_field_arr(c_nof_hdr_fields - 1 downto 0) := ( ( field_name_pad("eth_word_align"     ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("eth_dst_mac"        ), "  ", 48, field_default(0) ),
                                                                                   ( field_name_pad("eth_src_mac"        ), "  ", 48, field_default(0) ),
                                                                                   ( field_name_pad("eth_type"           ), "  ", 16, field_default(x"0800") ),
                                                                                   ( field_name_pad("ip_version"         ), "  ",  4, field_default(4) ),
                                                                                   ( field_name_pad("ip_header_length"   ), "  ",  4, field_default(5) ),
                                                                                   ( field_name_pad("ip_services"        ), "  ",  8, field_default(0) ),
                                                                                   ( field_name_pad("ip_total_length"    ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_identification"  ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_flags"           ), "  ",  3, field_default(2) ),
                                                                                   ( field_name_pad("ip_fragment_offset" ), "  ", 13, field_default(0) ),
                                                                                   ( field_name_pad("ip_time_to_live"    ), "  ",  8, field_default(127) ),
                                                                                   ( field_name_pad("ip_protocol"        ), "  ",  8, field_default(17) ),
                                                                                   ( field_name_pad("ip_header_checksum" ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_src_addr"        ), "  ", 32, field_default(0) ),
                                                                                   ( field_name_pad("ip_dst_addr"        ), "  ", 32, field_default(0) ),
                                                                                   ( field_name_pad("udp_src_port"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_dst_port"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_total_length"   ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_checksum"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("usr_sync"           ), "  ",  1, field_default(1) ),
                                                                                   ( field_name_pad("usr_bsn"            ), "  ", 60, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_0"    ), "  ",  7, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_1"    ), "  ",  9, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_2"    ), "  ", 10, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_3"    ), "  ", 33, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_4"    ), "  ",  5, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_5"    ), "  ",  8, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_6"    ), "  ", 27, field_default(0) ) );

  -- Function to select the revision configuration.
  function func_unb1_test_sel_revision_rec(g_design_name : string) return t_unb1_test_config;
end unb1_test_pkg;

package body unb1_test_pkg is
  constant c_all             : t_unb1_test_config := ( 1, 0,  true, 1,  true, 3, 1, 1, 2, c_tech_ddr3_4g_single_rank_800m_master);
  constant c_1GbE            : t_unb1_test_config := ( 0, 0,  true, 1, false, 0, 0, 0, 0, c_tech_ddr3_4g_single_rank_800m_master);
  constant c_10GbE           : t_unb1_test_config := ( 1, 0, false, 1,  true, 3, 0, 0, 0, c_tech_ddr3_4g_single_rank_800m_master);
  constant c_ddr_MB_I        : t_unb1_test_config := ( 0, 0, false, 1, false, 0, 1, 0, 1, c_tech_ddr3_4g_single_rank_800m_master);
  constant c_ddr_MB_II       : t_unb1_test_config := ( 0, 0, false, 1, false, 0, 0, 1, 1, c_tech_ddr3_4g_single_rank_800m_master);
  constant c_ddr_MB_I_II     : t_unb1_test_config := ( 0, 0, false, 1, false, 0, 1, 1, 2, c_tech_ddr3_4g_single_rank_800m_master);
  constant c_ddr_16g_MB_I    : t_unb1_test_config := ( 0, 0, false, 1, false, 0, 1, 0, 1, c_tech_ddr3_16g_dual_rank_800m);
  constant c_ddr_16g_MB_II   : t_unb1_test_config := ( 0, 0, false, 1, false, 0, 0, 1, 1, c_tech_ddr3_16g_dual_rank_800m);
  constant c_ddr_16g_MB_I_II : t_unb1_test_config := ( 0, 0, false, 1, false, 0, 1, 1, 2, c_tech_ddr3_16g_dual_rank_800m);

  function func_unb1_test_sel_revision_rec(g_design_name : string) return t_unb1_test_config is
  begin
    if    g_design_name = "unb1_test_all"             then return c_all;
    elsif g_design_name = "unb1_test_1GbE"            then return c_1GbE;
    elsif g_design_name = "unb1_test_10GbE"           then return c_10GbE;
    elsif g_design_name = "unb1_test_ddr_MB_I"        then return c_ddr_MB_I;
    elsif g_design_name = "unb1_test_ddr_MB_II"       then return c_ddr_MB_II;
    elsif g_design_name = "unb1_test_ddr_MB_I_II"     then return c_ddr_MB_I_II;
    elsif g_design_name = "unb1_test_ddr_16g_MB_I"    then return c_ddr_16g_MB_I;
    elsif g_design_name = "unb1_test_ddr_16g_MB_II"   then return c_ddr_16g_MB_II;
    elsif g_design_name = "unb1_test_ddr_16g_MB_I_II" then return c_ddr_16g_MB_I_II;
    else  return c_all;
    end if;

  end;
end unb1_test_pkg;
