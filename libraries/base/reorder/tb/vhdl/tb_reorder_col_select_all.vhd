-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : E. Kooistra
-- Purpose:
--   Verify reorder_col_select using two instances and select all input data,
--   so that second instance can restore the original input data order.
-- Description:
--   The second reorder_col_select instances undo's the reordering of the
--   first reorder_col_select instance. All data in the input block is used,
--   so that the output of the second reorder_col_select instance is equal
--   to the tb input data, except for a delay and data valid gaps.
--   The data and info fields are incrementing, so that the out_sosi can be
--   verified with dp_stream_verify.
--
--               p_comb_transpose     p_comb_undo_transpose
--                        |                   |
--                    ____v_____          ____v_____
--                   |reorder   |        |reorder   |
--   p_st_stimuli -->|col_select|------->|col_select|--> p_verify
--                   |__________|        |__________|
--      in_sosi             transposed_sosi         out_sosi
--      nof_ch_input        nof_ch_transposed       nof_ch_output
--
-- Remark:
-- * Development order from basic functionality to all verifying features:
--   . verify reorder 1 packet
--   . create func_reorder_transpose() in for both p_comb_transpose and
--     p_comb_undo_transpose.
--   . put t_reorder_transpose and func_reorder_transpose() in
--     reorder_pkg.vhd to anticipate for using the transpose reordering in
--     designs.
--   . verify reorder multiple packets with and without interpacket gaps
--   . verify g_use_complex
--   . verify all sosi fields using dp_stream_verify. Cannot use delayed
--     in_sosi and proc_dp_verify_sosi_equal(), because the out_sosi does
--     not have data valid gaps within a packet.
--   . add tb_tb_reorder_col_select_all
--   . verify dynamic change of nof_blocks_per_packet, nof_data_per_block.
--   . only support changing the nof_ch_in and nof_ch_sel when the no
--     reordering is busy.
--
-- Usage:
-- > as 8
-- > run -all
-- * The tb is self stopping and self checking, tb_end will stop the simulation
--   by stopping the clk and thus all toggling.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.reorder_pkg.all;

entity tb_reorder_col_select_all is
  generic(
    g_dsp_data_w            : natural := 16;  -- complex data width, = c_data_w / 2
    g_nof_sync              : natural := 5;
    g_nof_packets_per_sync  : natural := 3;
    g_nof_blocks_per_packet : natural := 5;
    g_nof_data_per_block    : natural := 3;
    g_inter_valid_gap       : natural := 0;  -- nof clk gap in in_sosi.valid
    g_inter_packet_gap      : natural := 3;  -- nof clk gap betweek in_sosi.eop and next in_sosi.sop
    g_use_complex           : boolean := true;
    g_use_identity          : boolean := true;  -- reorder identity or transpose
    g_use_dynamic_selection : boolean := false
  );
end tb_reorder_col_select_all;

architecture tb of tb_reorder_col_select_all is
  constant c_clk_period   : time := 10 ns;
  constant c_rl           : natural := 1;

  constant c_use_data     : boolean := not g_use_complex;
  constant c_data_w       : natural := c_nof_complex * g_dsp_data_w;

  -- Match c_nof_sync with p_nof_ch_stimuli
  constant c_nof_sync     : natural := sel_a_b(g_use_dynamic_selection, 2, g_nof_sync);
  constant c_factor_blk   : natural := sel_a_b(g_use_dynamic_selection, 2, 1);  -- factor for more blocks per packet
  constant c_factor_dat   : natural := sel_a_b(g_use_dynamic_selection, 1, 1);  -- factor for more data per block
  constant c_factor_ch    : natural := c_factor_blk * c_factor_dat;  -- factor for more data per packet
  constant c_nof_ch       : natural := g_nof_blocks_per_packet * g_nof_data_per_block;
  constant c_nof_ch_long  : natural := c_nof_ch * c_factor_ch;
  constant c_sel_gap      : natural := c_nof_ch_long * 2;

  -- Total output latency for transpose and undo transpose
  constant c_retrieve_lat    : natural := 2;  -- rd latency of reorder_col_select
  constant c_output_lat      : natural := (c_nof_ch + c_retrieve_lat) * 2;
  constant c_output_lat_long : natural := (c_nof_ch_long + c_retrieve_lat) * 2;

  signal rst              : std_logic;
  signal clk              : std_logic := '1';
  signal tb_end           : std_logic := '0';

  signal verify_en_sosi_sl          : t_dp_sosi_sl;
  signal verify_en_out_sosi_sl      : t_dp_sosi_sl;
  signal verify_en_out_sosi_sl_long : t_dp_sosi_sl;

  signal expected_last_sosi      : t_dp_sosi := c_dp_sosi_rst;
  signal expected_last_sosi_evt  : t_dp_sosi_sl := c_dp_sosi_sl_rst;

  -- Data
  signal in_en            : std_logic := '1';
  signal in_sosi          : t_dp_sosi := c_dp_sosi_rst;
  signal in_siso          : t_dp_siso := c_dp_siso_rdy;  -- used for proc_dp_gen_block_data
  signal transposed_sosi  : t_dp_sosi;
  signal out_sosi         : t_dp_sosi;

  -- Reorder control for first and second reorder_col_select
  signal sel_long                         : boolean := false;
  signal reorder_busy_transposed          : std_logic;
  signal reorder_busy_output              : std_logic;
  signal reorder_busy                     : std_logic;
  signal nof_ch_input                     : natural := c_nof_ch;
  signal nof_ch_transposed                : natural;
  signal nof_ch_output                    : natural;
  signal nof_blocks_per_packet_input      : natural := g_nof_blocks_per_packet;
  signal nof_blocks_per_packet_transposed : natural;
  signal nof_blocks_per_packet_output     : natural;
  signal nof_data_per_block_input         : natural := g_nof_data_per_block;
  signal nof_data_per_block_transposed    : natural;
  signal nof_data_per_block_output        : natural;

  -- Connect reorder identity or reorder transpose, dependent on g_use_identity
  signal select_copi      : t_mem_copi;
  signal undo_select_copi : t_mem_copi;
  signal select_cipo      : t_mem_cipo;
  signal undo_select_cipo : t_mem_cipo;
  signal r_identity       : t_reorder_identity := c_reorder_identity_rst;
  signal d_identity       : t_reorder_identity;
  signal r_redo_identity  : t_reorder_identity := c_reorder_identity_rst;
  signal d_redo_identity  : t_reorder_identity;
  signal r_transpose      : t_reorder_transpose := c_reorder_transpose_rst;
  signal d_transpose      : t_reorder_transpose;
  signal r_undo_transpose : t_reorder_transpose := c_reorder_transpose_rst;
  signal d_undo_transpose : t_reorder_transpose;
begin
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  -- Pulse in_en to control in_sosi.valid
  proc_common_gen_pulse(1, 1 + g_inter_valid_gap, '1', rst, clk, in_en);

  -- Dynamic reorder control
  nof_ch_input <= sel_a_b(sel_long, c_nof_ch_long, c_nof_ch);

  -- Input data packets stimuli
  p_st_stimuli : process
    variable v_nof_ch : natural := c_nof_ch;
    variable v_bsn    : std_logic_vector(31 downto 0) := (others => '0');
    variable v_info   : natural := 0;
    variable v_data   : natural := 0;
  begin
    sel_long <= false;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Run some sync intervals with counter data in the packets
    -- proc_dp_gen_block_data(
    --   constant c_ready_latency  : in  natural;  -- 0, 1 are supported by proc_dp_stream_ready_latency()
    --   constant c_use_data       : in  boolean;  -- when TRUE use data field, else use re, im fields,
    --                                                and keep unused fields at 'X'
    --   constant c_data_w         : in  natural;  -- data width for the data, re and im fields
    --   constant c_symbol_w       : in  natural;  -- c_data_w/c_symbol_w must be an integer
    --   constant c_symbol_init    : in  natural;  -- init counter for symbols in data field
    --   constant c_symbol_re_init : in  natural;  -- init counter for symbols in re field
    --   constant c_symbol_im_init : in  natural;  -- init counter for symbols in im field
    --   constant c_nof_symbols    : in  natural;  -- nof symbols per frame for the data, re and im fields
    --   constant c_channel        : in  natural;  -- channel field
    --   constant c_error          : in  natural;  -- error field
    --   constant c_sync           : in  std_logic;  -- when '1' issue sync pulse during this block
    --   constant c_bsn            : in  std_logic_vector;  -- bsn field
    --   signal   clk              : in  std_logic;
    --   signal   in_en            : in  std_logic;  -- when '0' then no valid output even when src_in is ready
    --   signal   src_in           : in  t_dp_siso;
    --   signal   src_out          : out t_dp_sosi);
    for I in 0 to c_nof_sync - 1 loop
      -- First packet in sync interval
      v_nof_ch := nof_ch_input;
      proc_dp_gen_block_data(c_rl,
                             c_use_data,
                             g_dsp_data_w,
                             g_dsp_data_w,
                             v_data,      -- data
                             v_data,      -- re
                             v_data + 1,  -- im
                             v_nof_ch,
                             v_info + 2,  -- channel
                             v_info + 3,  -- error
                             '1',  -- with sync
                             v_bsn,  -- bsn
                             clk,
                             in_en,
                             in_siso,
                             in_sosi);
      proc_common_wait_some_cycles(clk, g_inter_packet_gap);
      for J in 0 to g_nof_packets_per_sync - 2 loop
        -- Next packets in sync interval
        v_nof_ch := nof_ch_input;
        v_bsn := INCR_UVEC(v_bsn, 1);
        v_info := v_info + 1;
        v_data := v_data + v_nof_ch;
        proc_dp_gen_block_data(c_rl,
                               c_use_data,
                               g_dsp_data_w,
                               g_dsp_data_w,
                               v_data,      -- data
                               v_data,      -- re
                               v_data + 1,  -- im
                               v_nof_ch,
                               v_info + 2,  -- channel
                               v_info + 3,  -- error
                               '0',  -- no sync
                               v_bsn,  -- bsn
                               clk,
                               in_en,
                               in_siso,
                               in_sosi);
        proc_common_wait_some_cycles(clk, g_inter_packet_gap);
      end loop;
      v_bsn := INCR_UVEC(v_bsn, 1);
      v_info := v_info + 1;
      v_data := v_data + v_nof_ch;

      if g_use_dynamic_selection then
        -- Toggle dynamic reorder control
        proc_common_wait_until_low(clk, reorder_busy);
        sel_long <= not sel_long;
        proc_common_wait_some_cycles(clk, 1);
      end if;
    end loop;
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_some_cycles(clk, c_nof_ch_long * 2);
    proc_common_wait_some_cycles(clk, 10);

    -- Pulse event for used sosi fields, to verify that stimuli have been applied
    expected_last_sosi_evt <= c_dp_sosi_sl_ones;
    if g_use_complex then
      expected_last_sosi_evt.data <= '0';
    else
      expected_last_sosi_evt.re <= '0';
      expected_last_sosi_evt.im <= '0';
    end if;
    proc_common_wait_some_cycles(clk, 1);
    expected_last_sosi_evt <= c_dp_sosi_sl_rst;

    -- End of test
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  -- During stimuli verify that values are incrementing
  p_verify_en_sosi_sl : process
  begin
    verify_en_sosi_sl <= c_dp_sosi_sl_rst;
    proc_common_wait_until_low(clk, rst);

    -- Verify all sosi fields, except for some
    verify_en_sosi_sl <= c_dp_sosi_sl_ones;
    if g_use_complex then
      verify_en_sosi_sl.data <= '0';
    else
      verify_en_sosi_sl.re <= '0';
      verify_en_sosi_sl.im <= '0';
    end if;
    verify_en_sosi_sl.empty <= '0';
    wait;
  end process;

  verify_en_out_sosi_sl      <= verify_en_sosi_sl when sel_long = false else c_dp_sosi_sl_rst;
  verify_en_out_sosi_sl_long <= verify_en_sosi_sl when sel_long = true  else c_dp_sosi_sl_rst;

  -- After stimuli verify last valid values, to check that stimuli have been applied
  p_expected_last_sosi : process(clk)
  begin
    if rising_edge(clk) then
      if in_sosi.valid = '1' then
        -- hold last valid sosi.info fields
        expected_last_sosi <= in_sosi;
      end if;
      -- sosi.ctrl fields must have occured at least once
      expected_last_sosi.valid <= '1';
      expected_last_sosi.sync <= '1';
      expected_last_sosi.sop <= '1';
      expected_last_sosi.eop <= '1';
    end if;
  end process;

  u_verify_out_sosi : entity dp_lib.dp_stream_verify
  generic map (
    -- initializations
    g_sync_period     => g_nof_packets_per_sync,  -- BSN increment per sync interval
    g_sync_offset     => 0,  -- first BSN
    -- specific
    g_in_dat_w        => c_data_w,
    g_pkt_len         => c_nof_ch
  )
  port map (
    rst                        => rst,
    clk                        => clk,
    -- Verify data
    snk_in                     => out_sosi,
    -- During stimuli
    verify_snk_in_enable       => verify_en_out_sosi_sl,

    -- End of stimuli
    expected_snk_in            => expected_last_sosi,
    verify_expected_snk_in_evt => expected_last_sosi_evt
  );

  -- When g_use_dynamic_selection = true then c_nof_sync = 2 and second sync interval
  -- will contain the long packets. All sync intervals have g_nof_packets_per_sync.
  -- The first sync interval will start with BSN = g_sync_offset = 0, so the second
  -- sync interval with the long packets, will start with BSN = g_sync_offset =
  -- g_nof_packets_per_sync.
  u_verify_out_sosi_long : entity dp_lib.dp_stream_verify
  generic map (
    -- initializations
    g_sync_period     => g_nof_packets_per_sync,
    g_sync_offset     => g_nof_packets_per_sync,
    -- specific
    g_in_dat_w        => c_data_w,
    g_pkt_len         => c_nof_ch_long
  )
  port map (
    rst                        => rst,
    clk                        => clk,
    -- Verify data
    snk_in                     => out_sosi,
    -- During stimuli
    verify_snk_in_enable       => verify_en_out_sosi_sl_long,

    -- End of stimuli
    expected_snk_in            => c_dp_sosi_rst,  -- u_verify_out_sosi already does this
    verify_expected_snk_in_evt => c_dp_sosi_sl_rst
  );

  ------------------------------------------------------------------------------
  -- DUT reorder control for select_copi/select_cipo
  ------------------------------------------------------------------------------

  select_copi      <= r_identity.select_copi      when g_use_identity else r_transpose.select_copi;
  undo_select_copi <= r_redo_identity.select_copi when g_use_identity else r_undo_transpose.select_copi;

  -- Use synchronous reset in d signals
  p_clk : process(clk)
  begin
    if rising_edge(clk) then
      r_identity       <= d_identity;
      r_redo_identity  <= d_redo_identity;
      r_transpose      <= d_transpose;
      r_undo_transpose <= d_undo_transpose;
    end if;
  end process;

  p_comb_identity : process(rst, select_cipo, nof_ch_input, r_identity)
    variable v : t_reorder_identity;
  begin
    if select_cipo.waitrequest = '0' then
      -- Read from reorder_col_select page
      v := func_reorder_identity(nof_ch_input, r_identity);
    else
      -- No read, new reorder_col_select page not available yet
      v := c_reorder_identity_rst;
    end if;
    -- Synchronous reset
    if rst = '1' then
      v := c_reorder_identity_rst;
    end if;
    d_identity <= v;
  end process;

  p_comb_redo_identity : process(rst, undo_select_cipo, nof_ch_input, r_redo_identity)
    variable v : t_reorder_identity;
  begin
    if undo_select_cipo.waitrequest = '0' then
      -- Read from reorder_col_select page
      v := func_reorder_identity(nof_ch_input, r_redo_identity);
    else
      -- No read, new reorder_col_select page not available yet
      v := c_reorder_identity_rst;
    end if;
    -- Synchronous reset
    if rst = '1' then
      v := c_reorder_identity_rst;
    end if;
    d_redo_identity <= v;
  end process;

  -- The p_comb_transpose and p_comb_undo_transpose can both use
  -- func_reorder_transpose(), by swapping the transpose dimensions.

  nof_blocks_per_packet_input <= g_nof_blocks_per_packet when nof_ch_input = c_nof_ch else
                                 g_nof_blocks_per_packet * c_factor_blk;
  nof_data_per_block_input <= g_nof_data_per_block when nof_ch_input = c_nof_ch else
                              g_nof_data_per_block * c_factor_dat;

  p_comb_transpose : process(rst, select_cipo,
                             nof_data_per_block_input, nof_blocks_per_packet_input, r_transpose)
    variable v : t_reorder_transpose;
  begin
    if select_cipo.waitrequest = '0' then
      -- Read from reorder_col_select page
      v := func_reorder_transpose(nof_data_per_block_input, nof_blocks_per_packet_input, r_transpose);
    else
      -- No read, new reorder_col_select page not available yet
      v := c_reorder_transpose_rst;
    end if;
    -- Synchronous reset
    if rst = '1' then
      v := c_reorder_transpose_rst;
    end if;
    d_transpose <= v;
  end process;

  nof_blocks_per_packet_transposed <= g_nof_blocks_per_packet when nof_ch_transposed = c_nof_ch else
                                      g_nof_blocks_per_packet * c_factor_blk;
  nof_data_per_block_transposed <= g_nof_data_per_block when nof_ch_transposed = c_nof_ch else
                                   g_nof_data_per_block * c_factor_dat;

  p_comb_undo_transpose : process(rst, undo_select_cipo,
                                  nof_blocks_per_packet_transposed, nof_data_per_block_transposed, r_undo_transpose)
    variable v : t_reorder_transpose;
  begin
    if undo_select_cipo.waitrequest = '0' then
      -- Read from reorder_col_select page
      v := func_reorder_transpose(nof_blocks_per_packet_transposed, nof_data_per_block_transposed, r_undo_transpose);
    else
      -- No read, new reorder_col_select page not available yet
      v := c_reorder_transpose_rst;
    end if;
    -- Synchronous reset
    if rst = '1' then
      v := c_reorder_transpose_rst;
    end if;
    d_undo_transpose <= v;
  end process;

  nof_blocks_per_packet_output <= g_nof_blocks_per_packet when nof_ch_output = c_nof_ch else
                                  g_nof_blocks_per_packet * c_factor_blk;
  nof_data_per_block_output <= g_nof_data_per_block when nof_ch_output = c_nof_ch else
                               g_nof_data_per_block * c_factor_dat;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_transpose : entity work.reorder_col_select
  generic map (
    g_dsp_data_w  => g_dsp_data_w,
    g_nof_ch_in   => c_nof_ch_long,
    g_nof_ch_sel  => c_nof_ch_long,
    g_use_complex => g_use_complex
  )
  port map (
    dp_rst          => rst,
    dp_clk          => clk,

    reorder_busy    => reorder_busy_transposed,

    -- Dynamic reorder block size control
    nof_ch_in       => nof_ch_input,
    nof_ch_sel      => nof_ch_input,

    -- Captured reorder block size control used for output_sosi
    output_nof_ch_in  => nof_ch_transposed,
    output_nof_ch_sel => open,

    -- Memory Mapped
    col_select_mosi => select_copi,
    col_select_miso => select_cipo,  -- only used for waitrequest

    -- Streaming
    input_sosi      => in_sosi,
    output_sosi     => transposed_sosi
  );

  u_undo_transpose : entity work.reorder_col_select
  generic map (
    g_dsp_data_w  => g_dsp_data_w,
    g_nof_ch_in   => c_nof_ch_long,
    g_nof_ch_sel  => c_nof_ch_long,
    g_use_complex => g_use_complex
  )
  port map (
    dp_rst          => rst,
    dp_clk          => clk,

    reorder_busy    => reorder_busy_output,

    -- Dynamic reorder block size control
    nof_ch_in       => nof_ch_transposed,
    nof_ch_sel      => nof_ch_transposed,

    -- Captured reorder block size control used for output_sosi
    output_nof_ch_in  => nof_ch_output,
    output_nof_ch_sel => open,

    -- Memory Mapped
    col_select_mosi => undo_select_copi,
    col_select_miso => undo_select_cipo,  -- only used for waitrequest

    -- Streaming
    input_sosi      => transposed_sosi,
    output_sosi     => out_sosi
  );

  reorder_busy <= reorder_busy_transposed or reorder_busy_output;
end tb;
