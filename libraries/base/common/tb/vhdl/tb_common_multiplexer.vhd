-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_lfsr_sequences_pkg.all;
use work.tb_common_pkg.all;

-- Purpose: Test bench for common_multiplexer.vhd and common_demultiplexer.vhd
-- Usage:
-- > as 6
-- > run -all
--   The tb p_verify self-checks the output by using first a 1->g_nof_streams
--   demultiplexer and then a g_nof_streams->1 multiplexer. Both the use the
--   same output and input selection so that the expected output data is again
--   the same as the input stimuli data.
-- Remark:

entity tb_common_multiplexer is
  generic (
    g_pipeline_demux_in  : natural := 1;
    g_pipeline_demux_out : natural := 1;
    g_nof_streams        : natural := 3;
    g_pipeline_mux_in    : natural := 1;
    g_pipeline_mux_out   : natural := 1;
    g_dat_w              : natural := 8;
    g_random_in_val      : boolean := false;
    g_test_nof_cycles    : natural := 500
  );
end tb_common_multiplexer;

architecture tb of tb_common_multiplexer is
  constant clk_period        : time := 10 ns;

  constant c_rl              : natural := 1;
  constant c_init            : natural := 0;

  -- DUT constants
  constant c_pipeline_demux  : natural := g_pipeline_demux_in + g_pipeline_demux_out;
  constant c_pipeline_mux    : natural := g_pipeline_mux_in   + g_pipeline_mux_out;
  constant c_pipeline_total  : natural := c_pipeline_demux + c_pipeline_mux;

  constant c_sel_w           : natural := ceil_log2(g_nof_streams);

  -- Stimuli
  signal tb_end             : std_logic := '0';
  signal rst                : std_logic;
  signal clk                : std_logic := '1';
  signal ready              : std_logic := '1';
  signal verify_en          : std_logic := '0';
  signal random_0           : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal cnt_en             : std_logic := '1';

  -- DUT input
  signal in_dat             : std_logic_vector(g_dat_w - 1 downto 0) := (others => '0');
  signal in_val             : std_logic;
  signal in_sel             : std_logic_vector(c_sel_w - 1 downto 0) := (others => '0');

  -- Demux-Mux interface
  signal demux_dat_vec      : std_logic_vector(g_nof_streams * g_dat_w - 1 downto 0);
  signal demux_val_vec      : std_logic_vector(g_nof_streams        - 1 downto 0);
  signal demux_val          : std_logic;
  signal demux_sel          : std_logic_vector(c_sel_w - 1 downto 0);

  -- DUT output
  signal out_dat            : std_logic_vector(g_dat_w - 1 downto 0);
  signal out_val            : std_logic;

  -- Verify
  signal prev_out_dat       : std_logic_vector(g_dat_w - 1 downto 0);
  signal pipe_dat_vec       : std_logic_vector(0 to (c_pipeline_total + 1) * g_dat_w - 1);
  signal pipe_val_vec       : std_logic_vector(0 to (c_pipeline_total + 1) * 1      - 1);
begin
  ------------------------------------------------------------------------------
  -- Stimuli
  ------------------------------------------------------------------------------

  -- . tb
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;
  tb_end <= '0', '1' after g_test_nof_cycles * clk_period;

  -- . data
  random_0 <= func_common_random(random_0) when rising_edge(clk);

  cnt_en <= '1' when g_random_in_val = false else random_0(random_0'high);

  proc_common_gen_data(c_rl, c_init, rst, clk, cnt_en, ready, in_dat, in_val);

  -- . selection
  in_sel <= INCR_UVEC(in_sel, 1) when rising_edge(clk) and TO_UINT(in_sel) < g_nof_streams - 1 else
            TO_UVEC(0, c_sel_w)  when rising_edge(clk);  -- periodic selection over all demultiplexer output and multiplexer input streams

  -- . verification
  p_verify_en : process
  begin
    proc_common_wait_until_high(clk, in_val);
    proc_common_wait_some_cycles(clk, c_pipeline_total);

    verify_en <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUT : 1 --> g_nof_streams --> 1
  ------------------------------------------------------------------------------

  -- . Demultiplex single input to output[in_sel]
  u_demux : entity work.common_demultiplexer
  generic map (
    g_pipeline_in   => g_pipeline_demux_in,
    g_pipeline_out  => g_pipeline_demux_out,
    g_nof_out       => g_nof_streams,
    g_dat_w         => g_dat_w
  )
  port map(
    rst         => rst,
    clk         => clk,

    in_dat      => in_dat,
    in_val      => in_val,

    out_sel     => in_sel,
    out_dat     => demux_dat_vec,
    out_val     => demux_val_vec
  );

  -- . pipeline in_sel to align demux_sel to demux_*_vec
  u_pipe_sel : entity common_lib.common_pipeline
  generic map (
    g_pipeline  => c_pipeline_demux,
    g_in_dat_w  => c_sel_w,
    g_out_dat_w => c_sel_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sel,
    out_dat => demux_sel
  );

  demux_val <= demux_val_vec(TO_UINT(demux_sel));

  -- . Multiplex input[demux_sel] back to a single output
  u_mux : entity work.common_multiplexer
  generic map (
    g_pipeline_in   => g_pipeline_mux_in,
    g_pipeline_out  => g_pipeline_mux_out,
    g_nof_in        => g_nof_streams,
    g_dat_w         => g_dat_w
  )
  port map (
    rst         => rst,
    clk         => clk,

    in_sel      => demux_sel,
    in_dat      => demux_dat_vec,
    in_val      => demux_val,

    out_dat     => out_dat,
    out_val     => out_val
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  proc_common_verify_data(c_rl, clk, verify_en, ready, out_val, out_dat, prev_out_dat);  -- verify out_dat assuming incrementing data
  proc_common_verify_latency("data",  c_pipeline_total, clk, verify_en, in_dat, pipe_dat_vec, out_dat);  -- verify out_dat using delayed input
  proc_common_verify_latency("valid", c_pipeline_total, clk, verify_en, in_val, pipe_val_vec, out_val);  -- verify out_val using delayed input
end tb;
