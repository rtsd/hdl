-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Purpose: Instantiate RAM IP with generics
-- Description:
--   Copied component instantiation from Vivado XPM template

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

entity ip_ultrascale_ram_crw_crw is
  generic (
    g_inferred   : boolean := false;
    g_adr_w      : natural := 5;
    g_dat_w      : natural := 8;
    g_nof_words  : natural := 2**5;
    g_rd_latency : natural := 1;  -- choose 1 or 2
    g_init_file  : string  := "none"
  );
  port
  (
    address_a : in std_logic_vector(g_adr_w - 1 downto 0);
    address_b : in std_logic_vector(g_adr_w - 1 downto 0);
    clk_a     : in std_logic  := '1';
    clk_b     : in std_logic;
    data_a    : in std_logic_vector(g_dat_w - 1 downto 0);
    data_b    : in std_logic_vector(g_dat_w - 1 downto 0);
    wren_a    : in std_logic  := '0';
    wren_b    : in std_logic  := '0';
    q_a       : out std_logic_vector(g_dat_w - 1 downto 0);
    q_b       : out std_logic_vector(g_dat_w - 1 downto 0)
  );
end ip_ultrascale_ram_crw_crw;

architecture SYN of ip_ultrascale_ram_crw_crw is
begin
  assert g_rd_latency = 1 or g_rd_latency = 2
    report "ip_ultrascale_ram_crw_crw : read latency must be 1 (default) or 2"
    severity FAILURE;
  assert g_inferred = false
    report "ip_ultrascale_ram_crw_crw : cannot infer RAM"
    severity FAILURE;

   -- xpm_memory_tdpram: True Dual Port RAM
   -- Xilinx Parameterized Macro, version 2022.1

   xpm_memory_tdpram_inst : xpm_memory_tdpram
   generic map (
      ADDR_WIDTH_A => g_adr_w,  -- DECIMAL
      ADDR_WIDTH_B => g_adr_w,  -- DECIMAL
      AUTO_SLEEP_TIME => 0,  -- DECIMAL
      BYTE_WRITE_WIDTH_A => g_dat_w,  -- DECIMAL
      BYTE_WRITE_WIDTH_B => g_dat_w,  -- DECIMAL
      CASCADE_HEIGHT => 0,  -- DECIMAL
      CLOCKING_MODE => "independent_clock",  -- String
      ECC_MODE => "no_ecc",  -- String
      MEMORY_INIT_FILE => g_init_file,  -- String
      MEMORY_INIT_PARAM => "0",  -- String
      MEMORY_OPTIMIZATION => "true",  -- String
      MEMORY_PRIMITIVE => "block",  -- String
      MEMORY_SIZE => g_nof_words,  -- DECIMAL
      MESSAGE_CONTROL => 0,  -- DECIMAL
      READ_DATA_WIDTH_A => g_dat_w,  -- DECIMAL
      READ_DATA_WIDTH_B => g_dat_w,  -- DECIMAL
      READ_LATENCY_A => g_rd_latency,  -- DECIMAL
      READ_LATENCY_B => g_rd_latency,  -- DECIMAL
      READ_RESET_VALUE_A => "0",  -- String
      READ_RESET_VALUE_B => "0",  -- String
      RST_MODE_A => "SYNC",  -- String
      RST_MODE_B => "SYNC",  -- String
      SIM_ASSERT_CHK => 0,  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      USE_EMBEDDED_CONSTRAINT => 0,  -- DECIMAL
      USE_MEM_INIT => 1,  -- DECIMAL
      USE_MEM_INIT_MMI => 0,  -- DECIMAL
      WAKEUP_TIME => "disable_sleep",  -- String
      WRITE_DATA_WIDTH_A => g_dat_w,  -- DECIMAL
      WRITE_DATA_WIDTH_B => g_dat_w,  -- DECIMAL
      WRITE_MODE_A => "no_change",  -- String
      WRITE_MODE_B => "no_change",  -- String
      WRITE_PROTECT => 1  -- DECIMAL
   )
   port map (
      dbiterra => OPEN,  -- 1-bit output: Status signal to indicate double bit error occurrence
                                        -- on the data output of port A.

      dbiterrb => OPEN,  -- 1-bit output: Status signal to indicate double bit error occurrence
                                        -- on the data output of port A.

      douta => q_a,  -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
      doutb => q_b,  -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
      sbiterra => OPEN,  -- 1-bit output: Status signal to indicate single bit error occurrence
                                        -- on the data output of port A.

      sbiterrb => OPEN,  -- 1-bit output: Status signal to indicate single bit error occurrence
                                        -- on the data output of port B.

      addra => address_a,  -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
      addrb => address_b,  -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
      clka => clk_a,  -- 1-bit input: Clock signal for port A. Also clocks port B when
                                        -- parameter CLOCKING_MODE is "common_clock".

      clkb => clk_b,  -- 1-bit input: Clock signal for port B when parameter CLOCKING_MODE is
                                        -- "independent_clock". Unused when parameter CLOCKING_MODE is
                                        -- "common_clock".

      dina => data_a,  -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
      dinb => data_b,  -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
      ena => '1',  -- 1-bit input: Memory enable signal for port A. Must be high on clock
                                        -- cycles when read or write operations are initiated. Pipelined
                                        -- internally.

      enb => '1',  -- 1-bit input: Memory enable signal for port B. Must be high on clock
                                        -- cycles when read or write operations are initiated. Pipelined
                                        -- internally.

      injectdbiterra => '0',  -- 1-bit input: Controls double bit error injection on input data when
                                        -- ECC enabled (Error injection capability is not available in
                                        -- "decode_only" mode).

      injectdbiterrb => '0',  -- 1-bit input: Controls double bit error injection on input data when
                                        -- ECC enabled (Error injection capability is not available in
                                        -- "decode_only" mode).

      injectsbiterra => '0',  -- 1-bit input: Controls single bit error injection on input data when
                                        -- ECC enabled (Error injection capability is not available in
                                        -- "decode_only" mode).

      injectsbiterrb => '0',  -- 1-bit input: Controls single bit error injection on input data when
                                        -- ECC enabled (Error injection capability is not available in
                                        -- "decode_only" mode).

      regcea => '1',  -- 1-bit input: Clock Enable for the last register stage on the output
                                        -- data path.

      regceb => '1',  -- 1-bit input: Clock Enable for the last register stage on the output
                                        -- data path.

      rsta => '0',  -- 1-bit input: Reset signal for the final port A output register
                                        -- stage. Synchronously resets output port douta to the value specified
                                        -- by parameter READ_RESET_VALUE_A.

      rstb => '0',  -- 1-bit input: Reset signal for the final port B output register
                                        -- stage. Synchronously resets output port doutb to the value specified
                                        -- by parameter READ_RESET_VALUE_B.

      sleep => '0',  -- 1-bit input: sleep signal to enable the dynamic power saving feature.
      wea(0) => wren_a,  -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector
                                        -- for port A input data port dina. 1 bit wide when word-wide writes
                                        -- are used. In byte-wide write configurations, each bit controls the
                                        -- writing one byte of dina to address addra. For example, to
                                        -- synchronously write only bits [15-8] of dina when WRITE_DATA_WIDTH_A
                                        -- is 32, wea would be 4'b0010.

      web(0) => wren_a  -- WRITE_DATA_WIDTH_B/BYTE_WRITE_WIDTH_B-bit input: Write enable vector
                                        -- for port B input data port dinb. 1 bit wide when word-wide writes
                                        -- are used. In byte-wide write configurations, each bit controls the
                                        -- writing one byte of dinb to address addrb. For example, to
                                        -- synchronously write only bits [15-8] of dinb when WRITE_DATA_WIDTH_B
                                        -- is 32, web would be 4'b0010.

   );

   -- End of xpm_memory_tdpram_inst instantiation
end SYN;
