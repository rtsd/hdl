###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

source $::env(HDL_WORK)/boards/uniboard2b/libraries/unb2b_board/quartus/pinning/unb2b_minimal_pins.tcl
# unb2b_jesd204_pins contains undesired QSFP pinning
#source $::env(HDL_WORK)/boards/uniboard2b/libraries/unb2b_board/quartus/pinning/unb2b_jesd204b_pins.tcl
#
#=====================
# QSFP pins
# ====================

set_location_assignment PIN_AL32 -to CLKUSR

set_location_assignment PIN_Y36 -to SA_CLK
set_instance_assignment -name IO_STANDARD LVDS -to SA_CLK
# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SA_CLK

set_location_assignment PIN_AH9 -to SB_CLK
set_instance_assignment -name IO_STANDARD LVDS -to SB_CLK
# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SB_CLK


set_global_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON


set_location_assignment PIN_AT31 -to QSFP_RST

set_location_assignment PIN_AY33 -to QSFP_SCL[0]
set_location_assignment PIN_AY32 -to QSFP_SCL[1]
set_location_assignment PIN_AY30 -to QSFP_SCL[2]
set_location_assignment PIN_AN33 -to QSFP_SCL[3]
set_location_assignment PIN_AN31 -to QSFP_SCL[4]
set_location_assignment PIN_AJ33 -to QSFP_SCL[5]
set_location_assignment PIN_BA32 -to QSFP_SDA[0]
set_location_assignment PIN_BA31 -to QSFP_SDA[1]
set_location_assignment PIN_AP33 -to QSFP_SDA[2]
set_location_assignment PIN_AM33 -to QSFP_SDA[3]
set_location_assignment PIN_AK33 -to QSFP_SDA[4]
set_location_assignment PIN_AH32 -to QSFP_SDA[5]

set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_RST

### QSFP_1_0
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to  QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to                QSFP_1_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_1_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_1_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_1_TX[0]

### QSFP_1_RX
set_location_assignment PIN_AC38 -to QSFP_1_RX[0]
set_location_assignment PIN_AD40 -to QSFP_1_RX[1]
set_location_assignment PIN_AF40 -to QSFP_1_RX[2]
set_location_assignment PIN_AG38 -to QSFP_1_RX[3]

### QSFP_1_TX
set_location_assignment PIN_AC42 -to QSFP_1_TX[0]
set_location_assignment PIN_AD44 -to QSFP_1_TX[1]
set_location_assignment PIN_AF44 -to QSFP_1_TX[2]
set_location_assignment PIN_AG42 -to QSFP_1_TX[3]


#=====================
# JESD pins
# ====================
# Pins needed for the 12 channel JESD204B interface to the ADCs
set_instance_assignment -name IO_STANDARD LVDS -to JESD204B_SYSREF
set_instance_assignment -name IO_STANDARD LVDS -to "JESD204B_SYSREF(n)"
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[6]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[7]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[8]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[9]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[10]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[11]

#set_location_assignment PIN_B9 -to BCK_RX[0]
set_location_assignment PIN_D9 -to BCK_RX[1]
set_location_assignment PIN_C11 -to BCK_RX[2]
set_location_assignment PIN_F9 -to BCK_RX[3]
set_location_assignment PIN_C7 -to BCK_RX[4]
set_location_assignment PIN_E11 -to BCK_RX[5]
set_location_assignment PIN_E7 -to BCK_RX[6]
set_location_assignment PIN_D5 -to BCK_RX[7]
set_location_assignment PIN_G7 -to BCK_RX[8]
set_location_assignment PIN_F5 -to BCK_RX[9]
set_location_assignment PIN_J7 -to BCK_RX[10]
set_location_assignment PIN_H5 -to BCK_RX[11]
set_location_assignment PIN_L7 -to BCK_RX[12]
set_location_assignment PIN_K5 -to BCK_RX[13]
set_location_assignment PIN_N7 -to BCK_RX[14]
set_location_assignment PIN_M5 -to BCK_RX[15]
set_location_assignment PIN_R7 -to BCK_RX[16]
set_location_assignment PIN_P5 -to BCK_RX[17]
set_location_assignment PIN_U7 -to BCK_RX[18]
set_location_assignment PIN_T5 -to BCK_RX[19]
set_location_assignment PIN_W7 -to BCK_RX[20]
set_location_assignment PIN_V5 -to BCK_RX[21]
set_location_assignment PIN_AA7 -to BCK_RX[22]
set_location_assignment PIN_Y5 -to BCK_RX[23]
set_location_assignment PIN_AC7 -to BCK_RX[24]
set_location_assignment PIN_AB5 -to BCK_RX[25]
set_location_assignment PIN_AE7 -to BCK_RX[26]
set_location_assignment PIN_AD5 -to BCK_RX[27]
set_location_assignment PIN_AG7 -to BCK_RX[28]
set_location_assignment PIN_AF5 -to BCK_RX[29]
set_location_assignment PIN_AJ7 -to BCK_RX[30]
set_location_assignment PIN_AH5 -to BCK_RX[31]
set_location_assignment PIN_AL7 -to BCK_RX[32]
set_location_assignment PIN_AK5 -to BCK_RX[33]
set_location_assignment PIN_AN7 -to BCK_RX[34]
set_location_assignment PIN_AM5 -to BCK_RX[35]
set_location_assignment PIN_AR7 -to BCK_RX[36]
set_location_assignment PIN_AP5 -to BCK_RX[37]
set_location_assignment PIN_AU7 -to BCK_RX[38]
set_location_assignment PIN_AT5 -to BCK_RX[39]
set_location_assignment PIN_AW7 -to BCK_RX[40]
set_location_assignment PIN_AV5 -to BCK_RX[41]
set_location_assignment PIN_BA7 -to BCK_RX[42]
set_location_assignment PIN_AY5 -to BCK_RX[43]
set_location_assignment PIN_BC7 -to BCK_RX[44]
set_location_assignment PIN_BB5 -to BCK_RX[45]
set_location_assignment PIN_AY9 -to BCK_RX[46]
set_location_assignment PIN_BB9 -to BCK_RX[47]


#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[0]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[0]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[0]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[0]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[0]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[0]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[1]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[1]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[1]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[1]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[1]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[1]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[2]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[2]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[2]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[2]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[2]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[2]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[3]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[3]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[3]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[3]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[3]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[3]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[4]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[4]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[4]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[4]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[4]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[4]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[5]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[5]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[5]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[5]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[5]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[5]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[6]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[6]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[6]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[6]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[6]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[6]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[7]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[7]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[7]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[7]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[7]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[7]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[8]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[8]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[8]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[8]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[8]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[8]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[9]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[9]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[9]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[9]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[9]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[9]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[10]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[10]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[10]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[10]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[10]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[10]
#
#set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[11]
#set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[11]
#set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[11]
#set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[11]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[11]
#set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[11]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[12]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[12]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[13]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[13]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[14]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[14]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[15]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[15]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[16]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[16]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[17]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[17]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[18]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[18]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[19]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[19]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[20]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[20]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[21]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[21]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[22]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[22]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[23]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[23]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[24]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[24]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[24]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[24]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[24]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[24]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[25]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[25]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[25]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[25]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[25]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[25]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[26]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[26]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[26]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[26]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[26]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[26]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[27]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[27]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[27]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[27]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[27]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[27]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[28]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[28]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[28]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[28]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[28]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[28]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[29]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[29]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[29]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[29]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[29]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[29]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[30]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[30]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[30]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[30]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[30]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[30]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[31]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[31]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[31]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[31]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[31]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[31]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[32]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[32]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[32]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[32]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[32]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[32]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[33]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[33]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[33]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[33]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[33]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[33]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[34]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[34]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[34]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[34]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[34]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[34]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[35]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[35]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[35]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[35]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[35]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[35]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[36]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[36]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[36]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[36]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[36]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[36]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[37]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[37]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[37]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[37]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[37]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[37]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[38]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[38]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[38]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[38]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[38]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[38]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[39]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[39]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[39]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[39]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[39]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[39]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[40]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[40]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[40]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[40]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[40]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[40]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[41]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[41]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[41]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[41]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[41]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[41]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[42]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[42]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[42]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[42]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[42]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[42]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[43]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[43]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[43]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[43]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[43]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[43]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[44]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[44]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[44]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[44]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[44]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[44]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[45]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[45]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[45]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[45]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[45]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[45]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[46]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[46]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[46]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[46]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[46]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[46]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[47]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[47]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[47]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[47]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[47]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[47]


# Substitute new signal names from the jesd_simple design
#set_location_assignment PIN_BA7 -to BCK_RX[0]

set_instance_assignment -name IO_STANDARD LVDS -to BCK_REF_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "BCK_REF_CLK(n)"
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to BCK_REF_CLK
set_location_assignment PIN_V9 -to BCK_REF_CLK
set_location_assignment PIN_V10 -to "BCK_REF_CLK(n)"

set_location_assignment PIN_V12 -to JESD204B_SYSREF
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYSREF

set_location_assignment PIN_U12 -to JESD204B_SYNC_N[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[0]
set_location_assignment PIN_U14 -to JESD204B_SYNC_N[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC_N[1]


