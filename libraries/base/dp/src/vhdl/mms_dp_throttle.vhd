-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_throttle is
  generic (
    g_dc_period      : natural := 100;
    g_throttle_valid : boolean := false
  );
  port (
    mm_rst   : in  std_logic;
    mm_clk   : in  std_logic;

    dp_rst   : in  std_logic;
    dp_clk   : in  std_logic;

    reg_mosi : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    reg_miso : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    snk_out  : out t_dp_siso;
    snk_in   : in  t_dp_sosi;

    src_in   : in  t_dp_siso;
    src_out  : out t_dp_sosi
  );
end mms_dp_throttle;

architecture str of mms_dp_throttle is
  signal throttle : std_logic_vector(ceil_log2(g_dc_period + 1) - 1 downto 0);
begin
  u_dp_throttle_reg : entity work.dp_throttle_reg
  generic map (
    g_dc_period => g_dc_period
  )
  port map (

    mm_rst   => mm_rst,
    mm_clk   => mm_clk,

    st_rst   => dp_rst,
    st_clk   => dp_clk,

    sla_in   => reg_mosi,
    sla_out  => reg_miso,

    throttle => throttle
  );

  u_dp_throttle : entity work.dp_throttle
  generic map (
    g_dc_period      => g_dc_period,
    g_throttle_valid => g_throttle_valid
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,

    snk_out   => snk_out,
    snk_in    => snk_in,

    src_in    => src_in,
    src_out   => src_out,

    throttle  => throttle
  );
end str;
