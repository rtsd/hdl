--------------------------------------------------------------------------------
--   Copyright (C) 2017
--   ASTRON (Netherlands Institute for Radio Astronomy)
--   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
--   This file is part of the UniBoard software suite.
--   The file is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Produce pulse_out pulse_delay clk cycles after pulse_in
-- Description:
-- . Note: pulse_out must have occurured before the next pulse_in can be delayed.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_pulse_delay is
  generic (
    g_pulse_delay_max : natural;  -- Maximum delay value desired by user; determines internal counter width.
    g_register_out    : boolean  -- TRUE adds output register, adding 1 cycle of delay to the pulse_delay setting.
  );
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;
    pulse_in    : in  std_logic;
    pulse_delay : in  std_logic_vector(ceil_log2(g_pulse_delay_max + 1) - 1 downto 0);
    pulse_out   : out std_logic
  );
end entity common_pulse_delay;

architecture str of common_pulse_delay is
  constant c_pulse_delay_max_width : natural := ceil_log2(g_pulse_delay_max + 1);

  signal pulse_delay_reg       : std_logic_vector(c_pulse_delay_max_width - 1 downto 0);
  signal nxt_pulse_delay_reg   : std_logic_vector(c_pulse_delay_max_width - 1 downto 0);

  signal common_counter_cnt_en : std_logic;
  signal common_counter_count  : std_logic_vector(c_pulse_delay_max_width - 1 downto 0);

  signal nxt_pulse_out         : std_logic;
begin
  -------------------------------------------------------------------------------
  -- Switch to start counter @ pulse_in, and stop counter @ pulse_out.
  -------------------------------------------------------------------------------
  u_common_switch : entity work.common_switch
  generic map (
    g_or_high      => true,
    g_priority_lo  => false
  )
  port map (
    clk         => clk,
    rst         => rst,
    switch_high => pulse_in,
    switch_low  => nxt_pulse_out,
    out_level   => common_counter_cnt_en
  );

  -------------------------------------------------------------------------------
  -- Count delay cycles relative to pulse_in
  -- . We're using the load input to start counting at 1 instead of 0.
  --   . Starting with zero makes it difficult to tell the difference between
  --     common_counter's count output value (0) at reset and the first valid
  --     output count value after cnt_en (also 0).
  -------------------------------------------------------------------------------
  u_common_counter : entity work.common_counter
  generic map (
    g_width => c_pulse_delay_max_width,
    g_init  => 1
  )
  port map (
    clk      => clk,
    rst      => rst,
    cnt_ld   => pulse_in,  -- Clear (load "1") the counter on every pulse_in
    cnt_en   => common_counter_cnt_en,
    load     => TO_UVEC(1, c_pulse_delay_max_width),
    count    => common_counter_count
  );

  -------------------------------------------------------------------------------
  -- Assign nxt_pulse_out
  -------------------------------------------------------------------------------
  -- Store the control setting
  nxt_pulse_delay_reg <= pulse_delay when pulse_in = '1' else pulse_delay_reg;

  nxt_pulse_out <= pulse_in when pulse_delay = TO_UVEC(0, c_pulse_delay_max_width) else  -- 0 cycles delay (pulse_delay_reg not valid yet; using pulse_delay)
                        '1' when common_counter_count = pulse_delay_reg else '0';  -- >=1 cycles delay (so pulse_delay_reg will contain registered pulse_delay)

  -------------------------------------------------------------------------------
  -- Optional output register
  -------------------------------------------------------------------------------
  gen_register : if g_register_out = true generate
    p_clk : process (rst, clk)
    begin
      if rst = '1' then
        pulse_out       <= '0';
      elsif rising_edge(clk) then
        pulse_out       <= nxt_pulse_out;
      end if;
    end process;
  end generate;

  no_register : if g_register_out = false generate
    pulse_out <= nxt_pulse_out;
  end generate;

  -------------------------------------------------------------------------------
  -- Registers
  -------------------------------------------------------------------------------
  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      pulse_delay_reg <= (others => '0');
    elsif rising_edge(clk) then
      pulse_delay_reg <= nxt_pulse_delay_reg;
    end if;
  end process;
end str;
