-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Multi testbench for nw_10GbE the 10G Ethernet IO module
-- Description:
--   The multi-tb finishes when all instances have signaled tb_end.
-- Usage:
--   > as 5
--   > run -all
-------------------------------------------------------------------------------

library IEEE, technology_lib, tech_pll_lib, tech_mac_10g_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_mac_10g_lib.tb_tech_mac_10g_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tb_tb_nw_10GbE is
end tb_tb_nw_10GbE;

architecture tb of tb_tb_nw_10GbE is
  constant c_644       : time := tech_pll_clk_644_period;
  constant c_156       : time := 6.4 ns;
  constant c_data_type : natural := c_tb_tech_mac_10g_data_type_symbols;

  constant c_tb_end_vec : std_logic_vector(7 downto 0) := (others => '1');
  signal   tb_end_vec   : std_logic_vector(7 downto 0) := c_tb_end_vec;  -- sufficiently long to fit all tb instances
  signal   tb_end       : std_logic := '0';
begin
-- g_technology              : NATURAL := c_tech_select_default;
-- g_tb_end                  : BOOLEAN := TRUE;   -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
-- g_no_dut                  : BOOLEAN := FALSE;  -- default FALSE to verify the DUT, else use TRUE to verify the tb itself without the DUT
-- g_dp_clk_period           : TIME :=  5 ns;     -- 200 MHz
-- g_sim_level               : NATURAL := 1;      -- 0 = use IP; 1 = use fast serdes model
-- g_nof_channels            : NATURAL := 1;
-- g_direction               : STRING := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
-- g_ref_clk_644_period      : TIME := tech_pll_clk_644_period;  -- for 10GBASE-R
-- g_ref_clk_156_period      : TIME := 6.4 ns;                   -- for XAUI
-- g_data_type               : NATURAL := c_tb_tech_mac_10g_data_type_symbols;
-- g_verify_link_recovery    : BOOLEAN := TRUE;

  u_no_dut                     : entity work.tb_nw_10GbE generic map (c_tech_select_default, false,  true, 5   ns, 0, 1, "TX_RX",   c_644, c_156, c_data_type, true) port map (tb_end_vec(0));
  u_nw_10GbE                   : entity work.tb_nw_10GbE generic map (c_tech_select_default, false, false, 5   ns, 0, 1, "TX_RX",   c_644, c_156, c_data_type, true) port map (tb_end_vec(1));
  u_nw_10GbE_tx_only           : entity work.tb_nw_10GbE generic map (c_tech_select_default, false, false, 5   ns, 0, 1, "TX_ONLY", c_644, c_156, c_data_type, true) port map (tb_end_vec(2));
  u_nw_10GbE_rx_only           : entity work.tb_nw_10GbE generic map (c_tech_select_default, false, false, 5   ns, 0, 1, "RX_ONLY", c_644, c_156, c_data_type, true) port map (tb_end_vec(3));
  u_nw_10GbE_dp_clk_6_5ns      : entity work.tb_nw_10GbE generic map (c_tech_select_default, false, false, 6.5 ns, 0, 1, "TX_RX",   c_644, c_156, c_data_type, true) port map (tb_end_vec(4));

  gen_2_channels : if c_tech_select_default = c_tech_stratixiv generate
    u_nw_10GbE_nof_channels_is_2 : entity work.tb_nw_10GbE generic map (c_tech_select_default, false, false, 5   ns, 0, 2, "TX_RX",   c_644, c_156, c_data_type, true) port map (tb_end_vec(5));
  end generate;

  -- For arria10, nof_channels need to be 1, 4, 12, 24, 48.
  gen_24_channels : if c_tech_select_default /= c_tech_stratixiv generate
  u_nw_10GbE_nof_channels_is_24 : entity work.tb_nw_10GbE generic map (c_tech_select_default, false, false, 5   ns, 0, 24, "TX_RX",   c_644, c_156, c_data_type, true) port map (tb_end_vec(5));
  end generate;

  u_nw_10GbE_sim_level_is_1    : entity work.tb_nw_10GbE generic map (c_tech_select_default, false, false, 5   ns, 1, 1, "TX_RX",   c_644, c_156, c_data_type, true) port map (tb_end_vec(6));

  tb_end <= '1' when tb_end_vec = c_tb_end_vec else '0';

  p_tb_end : process
  begin
    wait until tb_end = '1';
    wait for 1 ns;
    report "Multi tb simulation finished."
      severity FAILURE;
    wait;
  end process;
end tb;
