-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose : MMS for dp_bsn_scheduler
-- Description: See dp_bsn_scheduler.vhd

entity mms_dp_bsn_scheduler is
  generic (
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and dp_clk are the same, else use TRUE to cross the clock domain
    g_bsn_w              : natural := c_dp_stream_bsn_w
  );
  port (
    -- Memory-mapped clock domain
    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;

    reg_mosi    : in  t_mem_mosi;
    reg_miso    : out t_mem_miso;

    -- Streaming clock domain
    dp_rst      : in  std_logic;
    dp_clk      : in  std_logic;

    snk_in      : in  t_dp_sosi;  -- only uses eop (= block sync), bsn[]
    trigger_out : out std_logic
  );
end mms_dp_bsn_scheduler;

architecture str of mms_dp_bsn_scheduler is
  signal scheduled_bsn : std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
begin
  u_mm_reg : entity work.dp_bsn_scheduler_reg
  generic map (
    g_cross_clock_domain => g_cross_clock_domain
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    st_rst            => dp_rst,
    st_clk            => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in            => reg_mosi,
    sla_out           => reg_miso,

    -- MM registers in dp_clk domain
    st_current_bsn    => snk_in.bsn,
    st_scheduled_bsn  => scheduled_bsn
  );

  u_bsn_scheduler : entity work.dp_bsn_scheduler
  generic map (
    g_bsn_w       => g_bsn_w
  )
  port map (
    rst           => dp_rst,
    clk           => dp_clk,
    -- MM control
    scheduled_bsn => scheduled_bsn,
    -- Streaming
    snk_in        => snk_in,
    trigger_out   => trigger_out
  );
end str;
