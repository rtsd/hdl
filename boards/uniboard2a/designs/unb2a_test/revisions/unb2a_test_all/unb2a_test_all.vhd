-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2a_board_lib, unb2a_test_lib, technology_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2a_board_lib.unb2_board_pkg.all;
use technology_lib.technology_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity unb2a_test_all is
  generic (
    g_design_name      : string  := "unb2a_test_all";
    g_design_note      : string  := "Test design with: 6xQSFP,DDR4";
    g_sim              : boolean := false;  -- Overridden by TB
    g_sim_unb_nr       : natural := 0;
    g_sim_node_nr      : natural := 0;
    g_stamp_date       : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time       : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn        : natural := 0  -- SVN revision    -- set by QSF
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK       : in    std_logic;  -- Clock 10GbE front (qsfp) and ring lines
    SB_CLK       : in    std_logic;  -- Clock 10GbE back upper 24 lines
    BCK_REF_CLK  : in    std_logic;  -- Clock 10GbE back lower 24 lines

    -- DDR reference clocks
    MB_I_REF_CLK  : in   std_logic;  -- Reference clock for MB_I
    MB_II_REF_CLK : in   std_logic;  -- Reference clock for MB_II

    -- back transceivers
--    BCK_RX       : IN    STD_LOGIC_VECTOR((c_unb2_board_tr_back.bus_w * c_unb2_board_tr_back.nof_bus)-1 downto 0);
--    BCK_TX       : OUT   STD_LOGIC_VECTOR((c_unb2_board_tr_back.bus_w * c_unb2_board_tr_back.nof_bus)-1 downto 0);
    BCK_SDA      : inout std_logic_vector(c_unb2_board_tr_back.i2c_w - 1 downto 0);
    BCK_SCL      : inout std_logic_vector(c_unb2_board_tr_back.i2c_w - 1 downto 0);
    BCK_ERR      : inout std_logic_vector(c_unb2_board_tr_back.i2c_w - 1 downto 0);

    -- ring transceivers
--    RING_0_RX    : IN    STD_LOGIC_VECTOR(c_unb2_board_tr_ring.bus_w-1 downto 0);
--    RING_0_TX    : OUT   STD_LOGIC_VECTOR(c_unb2_board_tr_ring.bus_w-1 downto 0);
--    RING_1_RX    : IN    STD_LOGIC_VECTOR(c_unb2_board_tr_ring.bus_w-1 downto 0);
--    RING_1_TX    : OUT   STD_LOGIC_VECTOR(c_unb2_board_tr_ring.bus_w-1 downto 0);
    -- pmbus
    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic;
    -- front transceivers
    QSFP_0_RX    : in    std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_0_TX    : out   std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_1_RX    : in    std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_1_TX    : out   std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_2_RX    : in    std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_2_TX    : out   std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_3_RX    : in    std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_3_TX    : out   std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_4_RX    : in    std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_4_TX    : out   std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_5_RX    : in    std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);
    QSFP_5_TX    : out   std_logic_vector(c_unb2_board_tr_qsfp.bus_w - 1 downto 0);

    QSFP_SDA     : inout std_logic_vector(c_unb2_board_tr_qsfp.i2c_w - 1 downto 0);
    QSFP_SCL     : inout std_logic_vector(c_unb2_board_tr_qsfp.i2c_w - 1 downto 0);

    -- SO-DIMM Memory Bank I
    MB_I_IN      : in    t_tech_ddr4_phy_in;
    MB_I_IO      : inout t_tech_ddr4_phy_io;
    MB_I_OU      : out   t_tech_ddr4_phy_ou;

    -- SO-DIMM Memory Bank II
    MB_II_IN     : in    t_tech_ddr4_phy_in;
    MB_II_IO     : inout t_tech_ddr4_phy_io;
    MB_II_OU     : out   t_tech_ddr4_phy_ou;

    QSFP_LED     : out   std_logic_vector(c_unb2_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end unb2a_test_all;

architecture str of unb2a_test_all is
begin
  u_revision : entity unb2a_test_lib.unb2a_test
  generic map (
    g_design_name => g_design_name,
    g_design_note => g_design_note,
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_stamp_svn   => g_stamp_svn
  )
  port map (
    -- GENERAL
    CLK          => CLK,
    PPS          => PPS,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => VERSION,
    ID           => ID,
    TESTIO       => TESTIO,

    -- I2C Interface to Sensors
    SENS_SC      => SENS_SC,
    SENS_SD      => SENS_SD,

    -- 1GbE Control Interface
    ETH_clk      => ETH_clk,
    ETH_SGIN     => ETH_SGIN,
    ETH_SGOUT    => ETH_SGOUT,

    -- Transceiver clocks
    SA_CLK       => SA_CLK,
    SB_CLK       => SB_CLK,
    BCK_REF_CLK  => BCK_REF_CLK,

    -- DDR reference clocks
    MB_I_REF_CLK  => MB_I_REF_CLK,
    MB_II_REF_CLK => MB_II_REF_CLK,

    -- back transceivers
 --   BCK_RX       => BCK_RX,
 --   BCK_TX       => BCK_TX,
    BCK_SDA      => BCK_SDA,
    BCK_SCL      => BCK_SCL,
    BCK_ERR      => BCK_ERR,

    -- ring transceivers
 --   RING_0_RX    => RING_0_RX,
 --   RING_0_TX    => RING_0_TX,
 --   RING_1_RX    => RING_1_RX,
 --   RING_1_TX    => RING_1_TX,
    -- pmbus
    PMBUS_SC     => PMBUS_SC,
    PMBUS_SD     => PMBUS_SD,
    PMBUS_ALERT  => PMBUS_ALERT,

    -- front transceivers
    QSFP_0_RX    => QSFP_0_RX,
    QSFP_0_TX    => QSFP_0_TX,
    QSFP_1_RX    => QSFP_1_RX,
    QSFP_1_TX    => QSFP_1_TX,
    QSFP_2_RX    => QSFP_2_RX,
    QSFP_2_TX    => QSFP_2_TX,
    QSFP_3_RX    => QSFP_3_RX,
    QSFP_3_TX    => QSFP_3_TX,
    QSFP_4_RX    => QSFP_4_RX,
    QSFP_4_TX    => QSFP_4_TX,
    QSFP_5_RX    => QSFP_5_RX,
    QSFP_5_TX    => QSFP_5_TX,

    QSFP_SDA     => QSFP_SDA,
    QSFP_SCL     => QSFP_SCL,

    -- SO-DIMM Memory Bank I
    MB_I_IN      => MB_I_IN,
    MB_I_IO      => MB_I_IO,
    MB_I_OU      => MB_I_OU,

    -- SO-DIMM Memory Bank II
    MB_II_IN     => MB_II_IN,
    MB_II_IO     => MB_II_IO,
    MB_II_OU     => MB_II_OU,

    QSFP_LED     => QSFP_LED
  );
end str;
