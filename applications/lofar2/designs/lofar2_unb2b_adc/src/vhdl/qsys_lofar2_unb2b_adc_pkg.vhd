-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_lofar2_unb2b_adc_pkg is
  -----------------------------------------------------------------------------
  -- this component declaration is copy-pasted from Quartus QSYS builder generated file:
  -- $HDL_WORK/build/unb2b/quartus/unb2b_test_ddr/qsys_unb2b_test/sim/qsys_unb2b_test.vhd
  -----------------------------------------------------------------------------

    component qsys_lofar2_unb2b_adc is
        port (
            avs_eth_0_clk_export                      : out std_logic;  -- export
            avs_eth_0_irq_export                      : in  std_logic                     := 'X';  -- export
            avs_eth_0_ram_address_export              : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_ram_read_export                 : out std_logic;  -- export
            avs_eth_0_ram_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_ram_write_export                : out std_logic;  -- export
            avs_eth_0_ram_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reg_address_export              : out std_logic_vector(3 downto 0);  -- export
            avs_eth_0_reg_read_export                 : out std_logic;  -- export
            avs_eth_0_reg_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_reg_write_export                : out std_logic;  -- export
            avs_eth_0_reg_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reset_export                    : out std_logic;  -- export
            avs_eth_0_tse_address_export              : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_tse_read_export                 : out std_logic;  -- export
            avs_eth_0_tse_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_tse_waitrequest_export          : in  std_logic                     := 'X';  -- export
            avs_eth_0_tse_write_export                : out std_logic;  -- export
            avs_eth_0_tse_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            clk_clk                                   : in  std_logic                     := 'X';  -- clk
            jesd204b_address_export                   : out std_logic_vector(11 downto 0);  -- export
            jesd204b_clk_export                       : out std_logic;  -- export
            jesd204b_read_export                      : out std_logic;  -- export
            jesd204b_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            jesd204b_reset_export                     : out std_logic;  -- export
            jesd204b_write_export                     : out std_logic;  -- export
            jesd204b_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            pio_jesd_ctrl_address_export              : out std_logic_vector(0 downto 0);  -- export
            pio_jesd_ctrl_clk_export                  : out std_logic;  -- export
            pio_jesd_ctrl_read_export                 : out std_logic;  -- export
            pio_jesd_ctrl_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_jesd_ctrl_reset_export                : out std_logic;  -- export
            pio_jesd_ctrl_write_export                : out std_logic;  -- export
            pio_jesd_ctrl_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            pio_pps_address_export                    : out std_logic_vector(0 downto 0);  -- export
            pio_pps_clk_export                        : out std_logic;  -- export
            pio_pps_read_export                       : out std_logic;  -- export
            pio_pps_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_pps_reset_export                      : out std_logic;  -- export
            pio_pps_write_export                      : out std_logic;  -- export
            pio_pps_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            pio_system_info_address_export            : out std_logic_vector(4 downto 0);  -- export
            pio_system_info_clk_export                : out std_logic;  -- export
            pio_system_info_read_export               : out std_logic;  -- export
            pio_system_info_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_system_info_reset_export              : out std_logic;  -- export
            pio_system_info_write_export              : out std_logic;  -- export
            pio_system_info_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            pio_wdi_external_connection_export        : out std_logic;  -- export
            ram_aduh_monitor_address_export           : out std_logic_vector(11 downto 0);  -- export
            ram_aduh_monitor_clk_export               : out std_logic;  -- export
            ram_aduh_monitor_read_export              : out std_logic;  -- export
            ram_aduh_monitor_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_aduh_monitor_reset_export             : out std_logic;  -- export
            ram_aduh_monitor_write_export             : out std_logic;  -- export
            ram_aduh_monitor_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_bsn_address_export   : out std_logic_vector(20 downto 0);  -- export
            ram_diag_data_buffer_bsn_clk_export       : out std_logic;  -- export
            ram_diag_data_buffer_bsn_read_export      : out std_logic;  -- export
            ram_diag_data_buffer_bsn_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_bsn_reset_export     : out std_logic;  -- export
            ram_diag_data_buffer_bsn_write_export     : out std_logic;  -- export
            ram_diag_data_buffer_bsn_writedata_export : out std_logic_vector(31 downto 0);  -- export
            ram_wg_address_export                     : out std_logic_vector(13 downto 0);  -- export
            ram_wg_clk_export                         : out std_logic;  -- export
            ram_wg_read_export                        : out std_logic;  -- export
            ram_wg_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_wg_reset_export                       : out std_logic;  -- export
            ram_wg_write_export                       : out std_logic;  -- export
            ram_wg_writedata_export                   : out std_logic_vector(31 downto 0);  -- export
            reg_aduh_monitor_address_export           : out std_logic_vector(5 downto 0);  -- export
            reg_aduh_monitor_clk_export               : out std_logic;  -- export
            reg_aduh_monitor_read_export              : out std_logic;  -- export
            reg_aduh_monitor_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_aduh_monitor_reset_export             : out std_logic;  -- export
            reg_aduh_monitor_write_export             : out std_logic;  -- export
            reg_aduh_monitor_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_input_address_export      : out std_logic_vector(7 downto 0);  -- export
            reg_bsn_monitor_input_clk_export          : out std_logic;  -- export
            reg_bsn_monitor_input_read_export         : out std_logic;  -- export
            reg_bsn_monitor_input_readdata_export     : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_input_reset_export        : out std_logic;  -- export
            reg_bsn_monitor_input_write_export        : out std_logic;  -- export
            reg_bsn_monitor_input_writedata_export    : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_scheduler_address_export          : out std_logic_vector(0 downto 0);  -- export
            reg_bsn_scheduler_clk_export              : out std_logic;  -- export
            reg_bsn_scheduler_read_export             : out std_logic;  -- export
            reg_bsn_scheduler_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_scheduler_reset_export            : out std_logic;  -- export
            reg_bsn_scheduler_write_export            : out std_logic;  -- export
            reg_bsn_scheduler_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_source_address_export             : out std_logic_vector(1 downto 0);  -- export
            reg_bsn_source_clk_export                 : out std_logic;  -- export
            reg_bsn_source_read_export                : out std_logic;  -- export
            reg_bsn_source_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_source_reset_export               : out std_logic;  -- export
            reg_bsn_source_write_export               : out std_logic;  -- export
            reg_bsn_source_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_bsn_address_export   : out std_logic_vector(11 downto 0);  -- export
            reg_diag_data_buffer_bsn_clk_export       : out std_logic;  -- export
            reg_diag_data_buffer_bsn_read_export      : out std_logic;  -- export
            reg_diag_data_buffer_bsn_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_bsn_reset_export     : out std_logic;  -- export
            reg_diag_data_buffer_bsn_write_export     : out std_logic;  -- export
            reg_diag_data_buffer_bsn_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_dp_shiftram_address_export            : out std_logic_vector(2 downto 0);  -- export
            reg_dp_shiftram_clk_export                : out std_logic;  -- export
            reg_dp_shiftram_read_export               : out std_logic;  -- export
            reg_dp_shiftram_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_shiftram_reset_export              : out std_logic;  -- export
            reg_dp_shiftram_write_export              : out std_logic;  -- export
            reg_dp_shiftram_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_ctrl_address_export              : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_ctrl_clk_export                  : out std_logic;  -- export
            reg_dpmm_ctrl_read_export                 : out std_logic;  -- export
            reg_dpmm_ctrl_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_ctrl_reset_export                : out std_logic;  -- export
            reg_dpmm_ctrl_write_export                : out std_logic;  -- export
            reg_dpmm_ctrl_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_data_address_export              : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_data_clk_export                  : out std_logic;  -- export
            reg_dpmm_data_read_export                 : out std_logic;  -- export
            reg_dpmm_data_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_data_reset_export                : out std_logic;  -- export
            reg_dpmm_data_write_export                : out std_logic;  -- export
            reg_dpmm_data_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_epcs_address_export                   : out std_logic_vector(2 downto 0);  -- export
            reg_epcs_clk_export                       : out std_logic;  -- export
            reg_epcs_read_export                      : out std_logic;  -- export
            reg_epcs_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_epcs_reset_export                     : out std_logic;  -- export
            reg_epcs_write_export                     : out std_logic;  -- export
            reg_epcs_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_temp_sens_address_export         : out std_logic_vector(2 downto 0);  -- export
            reg_fpga_temp_sens_clk_export             : out std_logic;  -- export
            reg_fpga_temp_sens_read_export            : out std_logic;  -- export
            reg_fpga_temp_sens_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_temp_sens_reset_export           : out std_logic;  -- export
            reg_fpga_temp_sens_write_export           : out std_logic;  -- export
            reg_fpga_temp_sens_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_voltage_sens_address_export      : out std_logic_vector(3 downto 0);  -- export
            reg_fpga_voltage_sens_clk_export          : out std_logic;  -- export
            reg_fpga_voltage_sens_read_export         : out std_logic;  -- export
            reg_fpga_voltage_sens_readdata_export     : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_voltage_sens_reset_export        : out std_logic;  -- export
            reg_fpga_voltage_sens_write_export        : out std_logic;  -- export
            reg_fpga_voltage_sens_writedata_export    : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_ctrl_address_export              : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_ctrl_clk_export                  : out std_logic;  -- export
            reg_mmdp_ctrl_read_export                 : out std_logic;  -- export
            reg_mmdp_ctrl_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_ctrl_reset_export                : out std_logic;  -- export
            reg_mmdp_ctrl_write_export                : out std_logic;  -- export
            reg_mmdp_ctrl_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_data_address_export              : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_data_clk_export                  : out std_logic;  -- export
            reg_mmdp_data_read_export                 : out std_logic;  -- export
            reg_mmdp_data_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_data_reset_export                : out std_logic;  -- export
            reg_mmdp_data_write_export                : out std_logic;  -- export
            reg_mmdp_data_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_remu_address_export                   : out std_logic_vector(2 downto 0);  -- export
            reg_remu_clk_export                       : out std_logic;  -- export
            reg_remu_read_export                      : out std_logic;  -- export
            reg_remu_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_remu_reset_export                     : out std_logic;  -- export
            reg_remu_write_export                     : out std_logic;  -- export
            reg_remu_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            reg_unb_pmbus_address_export              : out std_logic_vector(5 downto 0);  -- export
            reg_unb_pmbus_clk_export                  : out std_logic;  -- export
            reg_unb_pmbus_read_export                 : out std_logic;  -- export
            reg_unb_pmbus_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_pmbus_reset_export                : out std_logic;  -- export
            reg_unb_pmbus_write_export                : out std_logic;  -- export
            reg_unb_pmbus_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_unb_sens_address_export               : out std_logic_vector(5 downto 0);  -- export
            reg_unb_sens_clk_export                   : out std_logic;  -- export
            reg_unb_sens_read_export                  : out std_logic;  -- export
            reg_unb_sens_readdata_export              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_sens_reset_export                 : out std_logic;  -- export
            reg_unb_sens_write_export                 : out std_logic;  -- export
            reg_unb_sens_writedata_export             : out std_logic_vector(31 downto 0);  -- export
            reg_wdi_address_export                    : out std_logic_vector(0 downto 0);  -- export
            reg_wdi_clk_export                        : out std_logic;  -- export
            reg_wdi_read_export                       : out std_logic;  -- export
            reg_wdi_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wdi_reset_export                      : out std_logic;  -- export
            reg_wdi_write_export                      : out std_logic;  -- export
            reg_wdi_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            reg_wg_address_export                     : out std_logic_vector(5 downto 0);  -- export
            reg_wg_clk_export                         : out std_logic;  -- export
            reg_wg_read_export                        : out std_logic;  -- export
            reg_wg_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wg_reset_export                       : out std_logic;  -- export
            reg_wg_write_export                       : out std_logic;  -- export
            reg_wg_writedata_export                   : out std_logic_vector(31 downto 0);  -- export
            reset_reset_n                             : in  std_logic                     := 'X';  -- reset_n
            rom_system_info_address_export            : out std_logic_vector(9 downto 0);  -- export
            rom_system_info_clk_export                : out std_logic;  -- export
            rom_system_info_read_export               : out std_logic;  -- export
            rom_system_info_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            rom_system_info_reset_export              : out std_logic;  -- export
            rom_system_info_write_export              : out std_logic;  -- export
            rom_system_info_writedata_export          : out std_logic_vector(31 downto 0)  -- export
        );
    end component qsys_lofar2_unb2b_adc;
end qsys_lofar2_unb2b_adc_pkg;
