-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose: Test bench for the wideband poly phase filterbank.
--
--          The testbech uses blockgenerators to generate data for
--          every input of the wideband poly phase filterbank.
--          The output of the WPFB is stored in databuffers.
--          Both the block generators and databuffers are controlled
--          via a mm interface.
--          Use this testbench in conjunction with:
--
--          ../python/tc_mmf_wpfb_unit.py
--          For verifying the complete wideband polyphase filter bank: g_use_bg = FALSE
--
--          ../python/tc_mmf_wpfb_unit_functional.py
--          For verifying the different wide- and narrowband configurationss
--          of the wpfb_unit.
--
-- (Automated) Usage:
--   > Be sure that the c_start_modelsim variable is set to 1 in the script.
--   > Run python script in separate terminal: "python tc_mmf_wpfb_unit.py --unb 0 --bn 0 --sim"
--
-- (Manual) Usage:
--   > run -all
--   > Be sure that the c_start_modelsim variable is set to 0 in the script.
--   > Run python script in separate terminal: "python tc_mmf_wpfb_unit.py --unb 0 --bn 0 --sim"
--   > Check the results of the python script.
--   > Stop the simulation manually in Modelsim by pressing the stop-button.
--   > For fractional frequencies set g_nof_blocks=32 to be able to simulate a sufficent number of periods without transition.

library IEEE, common_lib, mm_lib, diag_lib, dp_lib, rTwoSDF_lib, fft_lib, filter_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use rTwoSDF_lib.twiddlesPkg.all;
use rTwoSDF_lib.rTwoSDFPkg.all;
use fft_lib.tb_fft_pkg.all;
use fft_lib.fft_pkg.all;
use filter_lib.fil_pkg.all;
use work.wpfb_pkg.all;

entity tb_mmf_wpfb_unit is
  generic(
    g_wb_factor         : natural := 1;  -- = default 1, wideband factor
    g_nof_wb_streams    : natural := 1;  -- = 1, the number of parallel wideband streams. The filter coefficients are shared on every wb-stream.
    g_nof_chan          : natural := 0;  -- = default 0, defines the number of channels (=time-multiplexed input signals): nof channels = 2**nof_chan
    g_nof_points        : natural := 64;  -- = 1024, N point FFT
    g_nof_taps          : natural := 16;  -- = 8 nof taps n the filter
    g_nof_blocks        : natural := 4;  -- = 4, the number of blocks of g_nof_points each in the BG waveform (must be power of 2 due to that BG c_bg_block_len must be power of 2)
    g_in_dat_w          : natural := 8;  -- = 8, number of input bits
    g_out_dat_w         : natural := 16;  -- = 14, number of output bits: in_dat_w + natural((ceil_log2(nof_points))/2)
    g_use_separate      : boolean := false;  -- = false for complex input, true for two real inputs
    g_use_bg            : boolean := false;
    g_coefs_file_prefix : string  := "data/coefs_wide"
  );
end tb_mmf_wpfb_unit;

architecture tb of tb_mmf_wpfb_unit is
    constant c_in_backoff_w     : natural := 0;  -- = 0, number of bits for input backoff to avoid FIR output overflow
    constant c_nof_blk_per_sync : natural := 20;

    constant c_wpfb : t_wpfb  := (g_wb_factor, g_nof_points, g_nof_chan, g_nof_wb_streams,
                                  g_nof_taps, c_in_backoff_w, g_in_dat_w, 16, 16,
                                  true, false, g_use_separate, 16, g_out_dat_w, 0, 18, 2, true, 56, 2, c_nof_blk_per_sync,
                                  c_fft_pipeline, c_fft_pipeline, c_fil_ppf_pipeline);

    --  type t_wpfb is record
    --  -- General parameters for the wideband poly phase filter
    --  wb_factor         : natural;        -- = default 4, wideband factor
    --  nof_points        : natural;        -- = 1024, N point FFT (Also the number of subbands for the filetr part)
    --  nof_chan          : natural;        -- = default 0, defines the number of channels (=time-multiplexed input signals): nof channels = 2**nof_chan
    --  nof_wb_streams    : natural;        -- = 1, the number of parallel wideband streams. The filter coefficients are shared on every stream.
    --
    --  -- Parameters for the poly phase filter
    --  nof_taps          : natural;        -- = 16, the number of FIR taps per subband
    --  fil_backoff_w     : natural;        -- = 0, number of bits for input backoff to avoid output overflow
    --  fil_in_dat_w      : natural;        -- = 8, number of input bits
    --  fil_out_dat_w     : natural;        -- = 16, number of output bits
    --  coef_dat_w        : natural;        -- = 16, data width of the FIR coefficients
    --
    --  -- Parameters for the FFT
    --  use_reorder       : boolean;        -- = false for bit-reversed output, true for normal output
    --  use_fft_shift     : boolean;        -- = false for [0, pos, neg] bin frequencies order, true for [neg, 0, pos] bin frequencies order in case of complex input
    --  use_separate      : boolean;        -- = false for complex input, true for two real inputs
    --  fft_in_dat_w      : natural;        -- = 16, number of input bits
    --  fft_out_dat_w     : natural;        -- = 13, number of output bits
    --  fft_out_gain_w    : natural;        -- = 0, output gain factor applied after the last stage output, before requantization to out_dat_w
    --  stage_dat_w       : natural;        -- = 18, number of bits that are used inter-stage
    --  guard_w           : natural;        -- = 2
    --  guard_enable      : boolean;        -- = true
    --
    --  -- Parameters for the statistics
    --  stat_data_w       : positive;       -- = 56
    --  stat_data_sz      : positive;       -- = 2
    --  nof_blk_per_sync  : natural;        -- = 800000, number of FFT output blocks per sync interval
    --
    --  -- Pipeline parameters for both poly phase filter and FFT. These are heritaged from the filter and fft libraries.
    --  pft_pipeline      : t_fft_pipeline;     -- Pipeline settings for the pipelined FFT
    --  fft_pipeline      : t_fft_pipeline;     -- Pipeline settings for the parallel FFT
    --  fil_pipeline      : t_fil_ppf_pipeline; -- Pipeline settings for the filter units
    --
    --  end record;

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period         : time := 100 ps;
  constant c_dp_clk_period         : time := 5 ns;
  constant c_sclk_period           : time := c_dp_clk_period / c_wpfb.wb_factor;
  constant c_dp_pps_period         : natural := 64;

  signal dp_pps                    : std_logic;

  signal mm_rst                    : std_logic;
  signal mm_clk                    : std_logic := '0';

  signal dp_rst                    : std_logic;
  signal dp_clk                    : std_logic := '0';

  signal SCLK                      : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  signal reg_diag_bg_mosi          : t_mem_mosi;
  signal reg_diag_bg_miso          : t_mem_miso;

  signal ram_diag_bg_mosi          : t_mem_mosi;
  signal ram_diag_bg_miso          : t_mem_miso;

  signal ram_diag_data_buf_re_mosi : t_mem_mosi;
  signal ram_diag_data_buf_re_miso : t_mem_miso;

  signal reg_diag_data_buf_re_mosi : t_mem_mosi;
  signal reg_diag_data_buf_re_miso : t_mem_miso;

  signal ram_diag_data_buf_im_mosi : t_mem_mosi;
  signal ram_diag_data_buf_im_miso : t_mem_miso;

  signal reg_diag_data_buf_im_mosi : t_mem_mosi;
  signal reg_diag_data_buf_im_miso : t_mem_miso;

  signal ram_st_sst_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_sst_miso           : t_mem_miso := c_mem_miso_rst;

  signal ram_fil_coefs_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal ram_fil_coefs_miso        : t_mem_miso := c_mem_miso_rst;

  signal reg_diag_bg_pfb_mosi      : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diag_bg_pfb_miso      : t_mem_miso := c_mem_miso_rst;

  signal ram_diag_bg_pfb_mosi      : t_mem_mosi := c_mem_mosi_rst;
  signal ram_diag_bg_pfb_miso      : t_mem_miso := c_mem_miso_rst;

  constant c_coefs_file_prefix      : string  := g_coefs_file_prefix & natural'image(c_wpfb.wb_factor) & "_p" & natural'image(c_wpfb.nof_points) & "_t" & natural'image(c_wpfb.nof_taps);

  constant c_nof_streams            : positive := c_wpfb.nof_wb_streams * c_wpfb.wb_factor;
  constant c_nof_channels           : natural  := 2**c_wpfb.nof_chan;
  constant c_bg_block_len           : natural  := c_wpfb.nof_points * g_nof_blocks * c_nof_channels / c_wpfb.wb_factor;

  constant c_bg_buf_adr_w           : natural := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, c_nof_streams, 1);
  constant c_bg_data_file_prefix    : string := "UNUSED";

  signal bg_siso_arr                : t_dp_siso_arr(c_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(c_nof_streams - 1 downto 0);
  signal out_sosi_arr               : t_dp_sosi_arr(c_nof_streams - 1 downto 0);

  signal scope_in_sosi              : t_dp_sosi_integer_arr(c_wpfb.nof_wb_streams - 1 downto 0);
  signal scope_out_sosi             : t_dp_sosi_integer_arr(c_wpfb.nof_wb_streams - 1 downto 0);
  signal scope_out_power            : real := 0.0;
  signal scope_out_ampl             : real := 0.0;
  signal scope_out_index            : natural;
  signal scope_out_bin              : natural;
  signal scope_out_band             : natural;
  signal scope_out_ampl_x           : real := 0.0;
  signal scope_out_ampl_y           : real := 0.0;
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  SCLK   <= not SCLK after c_sclk_period / 2;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  u_mm_file_reg_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_BG")
                                           port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

  u_mm_file_ram_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_BG")
                                           port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

  u_mm_file_ram_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_re_mosi, ram_diag_data_buf_re_miso);

  u_mm_file_reg_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_re_mosi, reg_diag_data_buf_re_miso);

  u_mm_file_ram_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_im_mosi, ram_diag_data_buf_im_miso);

  u_mm_file_reg_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_im_mosi, reg_diag_data_buf_im_miso);

  u_mm_file_ram_fil_coefs        : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_FIL_COEFS")
                                           port map(mm_rst, mm_clk, ram_fil_coefs_mosi, ram_fil_coefs_miso);

  u_mm_file_ram_st_sst           : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_ST_SST")
                                           port map(mm_rst, mm_clk, ram_st_sst_mosi, ram_st_sst_miso);

  u_mm_file_reg_diag_pfb_bg      : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_BG_PFB")
                                           port map(mm_rst, mm_clk, reg_diag_bg_pfb_mosi, reg_diag_bg_pfb_miso);

  u_mm_file_ram_diag_pfb_bg      : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_BG_PFB")
                                           port map(mm_rst, mm_clk, ram_diag_bg_pfb_mosi, ram_diag_bg_pfb_miso);

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_nof_streams,
    g_buf_dat_w          => c_nof_complex * c_wpfb.fil_in_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,  -- Waveform buffer size 2**g_buf_addr_w nof samples
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,
    en_sync          => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi => reg_diag_bg_mosi,
    reg_bg_ctrl_miso => reg_diag_bg_miso,
    ram_bg_data_mosi => ram_diag_bg_mosi,
    ram_bg_data_miso => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr     => bg_siso_arr,
    out_sosi_arr     => bg_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- Source: DUT input scope
  ----------------------------------------------------------------------------
  gen_input_scopes : for I in 0 to c_wpfb.nof_wb_streams - 1 generate
    u_in_scope : entity dp_lib.dp_wideband_wb_arr_scope
    generic map (
      g_sim                 => true,
      g_wideband_factor     => c_wpfb.wb_factor,
      g_wideband_big_endian => false,
      g_dat_w               => c_wpfb.fil_in_dat_w
    )
    port map (
      SCLK         => SCLK,
      wb_sosi_arr  => bg_sosi_arr((I + 1) * c_wpfb.wb_factor - 1 downto I * c_wpfb.wb_factor),
      scope_sosi   => scope_in_sosi(I)
    );
  end generate;

  ----------------------------------------------------------------------------
  -- DUT = Device Under Test
  ----------------------------------------------------------------------------
  u_dut : entity work.wpfb_unit
  generic map(
    g_wpfb              => c_wpfb,
    g_use_bg            => g_use_bg,
    g_coefs_file_prefix => c_coefs_file_prefix
  )
  port map(
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,
    ram_fil_coefs_mosi => ram_fil_coefs_mosi,
    ram_fil_coefs_miso => ram_fil_coefs_miso,
    ram_st_sst_mosi    => ram_st_sst_mosi,
    ram_st_sst_miso    => ram_st_sst_miso,
    reg_bg_ctrl_mosi   => reg_diag_bg_pfb_mosi,
    reg_bg_ctrl_miso   => reg_diag_bg_pfb_miso,
    ram_bg_data_mosi   => ram_diag_bg_pfb_mosi,
    ram_bg_data_miso   => ram_diag_bg_pfb_miso,
    in_sosi_arr        => bg_sosi_arr,
    out_sosi_arr       => out_sosi_arr
  );

  time_map : process is
    variable sim_time_str_v : string(1 to 30);  -- 30 chars should be enough
    variable sim_time_len_v : natural;
  begin
    wait for 1000 ns;
    sim_time_len_v := time'image(now)'length;
    sim_time_str_v := (others => ' ');
    sim_time_str_v(1 to sim_time_len_v) := time'image(now);
    report "Sim time string length: " & integer'image(sim_time_len_v);
    report "Sim time string.......:'" & sim_time_str_v & "'";
  end process;

  ----------------------------------------------------------------------------
  -- Sink: DUT output scope
  ----------------------------------------------------------------------------
  gen_output_scopes : for I in 0 to c_wpfb.nof_wb_streams - 1 generate
    u_out_scope : entity dp_lib.dp_wideband_wb_arr_scope
    generic map (
      g_sim                 => true,
      g_wideband_factor     => c_wpfb.wb_factor,
      g_wideband_big_endian => false,
      g_dat_w               => c_wpfb.fft_out_dat_w
    )
    port map (
      SCLK         => SCLK,
      wb_sosi_arr  => out_sosi_arr((I + 1) * c_wpfb.wb_factor - 1 downto I * c_wpfb.wb_factor),
      scope_sosi   => scope_out_sosi(I)
    );
  end generate;

  p_scope_out_index : process(SCLK)
  begin
    if rising_edge(SCLK) then
      if scope_out_sosi(0).valid = '1' then
        scope_out_index <= scope_out_index + 1;
        if scope_out_index >= g_nof_points - 1 then
          scope_out_index <= 0;
        end if;
      end if;
    end if;
  end process;
  scope_out_bin    <= fft_index_to_bin_frequency(c_wpfb.wb_factor, c_wpfb.nof_points, scope_out_index, true, false, true);  -- complex bin
  scope_out_band   <= fft_index_to_bin_frequency(c_wpfb.wb_factor, c_wpfb.nof_points, scope_out_index, true, true, true);  -- two real bin

  scope_out_power  <= real(scope_out_sosi(0).re)**2 + real(scope_out_sosi(0).im)**2;
  scope_out_ampl   <= SQRT(scope_out_power);
  scope_out_ampl_x <= scope_out_ampl when (scope_out_bin mod 2) = 0 else 0.0;
  scope_out_ampl_y <= scope_out_ampl when (scope_out_bin mod 2) = 1 else 0.0;

  ----------------------------------------------------------------------------
  -- Sink: data buffer real
  ----------------------------------------------------------------------------
  u_data_buf_re : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => c_nof_streams,
    g_data_type    => e_real,
    g_data_w       => c_wpfb.fft_out_dat_w,
    g_buf_nof_data => c_bg_block_len,
    g_buf_use_sync => true
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_re_mosi,
    ram_data_buf_miso => ram_diag_data_buf_re_miso,

    reg_data_buf_mosi => reg_diag_data_buf_re_mosi,
    reg_data_buf_miso => reg_diag_data_buf_re_miso,

    -- ST interface
    in_sync           => out_sosi_arr(0).sync,
    in_sosi_arr       => out_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer imag
  ----------------------------------------------------------------------------
  u_data_buf_im : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => c_nof_streams,
    g_data_type    => e_imag,
    g_data_w       => c_wpfb.fft_out_dat_w,
    g_buf_nof_data => c_bg_block_len,
    g_buf_use_sync => true
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_im_mosi,
    ram_data_buf_miso => ram_diag_data_buf_im_miso,

    reg_data_buf_mosi => reg_diag_data_buf_im_mosi,
    reg_data_buf_miso => reg_diag_data_buf_im_miso,

    -- ST interface
    in_sync           => out_sosi_arr(0).sync,
    in_sosi_arr       => out_sosi_arr
  );
end tb;
