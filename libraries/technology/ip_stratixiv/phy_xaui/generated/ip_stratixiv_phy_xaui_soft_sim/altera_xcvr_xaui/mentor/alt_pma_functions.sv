// Copyright (C) Altera Corporation. All rights reserved. 
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 11.1sp2 linux32 Build 259 01/26/2012
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6c"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
bGpAvLh1/ZUreODVSRw8PY9zQzDCVgZvO/IgTMwrxXQ++UAPoMctgHg7XX4iJEXO
mjnPma55xG8zi1IU6rbZ25wPG1itawPrbMGvPYe5dmZUcsjVKeZI2e6G1j8sIyPi
lcunJKwVcoPfndodUu0V12DYatCoNCtcME9SQ1rOvik=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2496)
jng+bRgaKuhzSzh80Ndw7hRJPQuNnj6qns2tGESxGO7fEK4jrAzxFdhnVMHyEXut
jmurQOopfavKRzPyyG/vSpmJtECaULZkFuynIYqxr4rCyMTbWx88T0SAE8WFDNEz
/p7KYF3DJLG0pda4+iZob894crggmyPELWUK2bJYtF7zKCKuZ4pgCKH3nHi5Odq5
tYWu2T6BNVp035XgHPnJ9ZWq7VZjJBKI1oKU5pS1D3zsotYYM2UQuwmYnCp1jT25
rWoxTHhmbKtBBz3ulHG+pdFs6AUS2MONMXKj6fBFkAE444L+OskU+Yk/Ag7SY+YI
F+g5Tm0WmaqqwN1b/R33PgOvNiAakyX1sH3JQSkU5+0Bc8jJo6jmwqB3yDykUt2r
95mMo0z93t5d8r3uKLqWO9gRothJ/HD4chG8sHWX+hf7STSU1seMzNaczGDbAC6/
e2ZqO7+eFgk/f6+rYxUo0KUTxdWFnQeM8itjNYiji9/R3u6ezfIk8wxE/CyHU829
pIuKnHgwL4sM9M5i7n4LvjXZArpFIPW3uSl8bxlrPCQk79fjqg/A3+HO/T2nU3vL
9nKsnG1u6k31ISco0kDcDIR1CA2WF0HcfahpFhPL/dpxNZheBCtb5LYE8LESLc97
A7ITZuyujYitKCQs8Sdqhb/HVTypPnlwOqXrcTSihMSrtOguzYFvY90z66JchZe+
nfvlSK7DBMJGjimuqGpLHE0ozi6mlnNqNoJmE9fDY7i94knctn0cwvv3hOgFjnVH
+A4wzbqafj6UL+UiOCC4z265Xy9zR4dP4mOAeL6rNFoM1m7/eUZfbE38tz/OPu0L
Vbde82u3lW0NnYOZXGqLdGG2W31L2ZMv6WOS5UNYbjuD+PRZoR+tXcqXe+CTOGjm
OIx/7VXztvcaga5iTXIxT7jMI+mRfryzRJmrFDfQb49HRVp71Kq+RExm55Vc+tLO
7BCXvs7bVeKPQOyUNIAYOs2obtCCgNoUxyWrRlxeqEEdF9nQ54B4S42JsW4JW3MJ
zvjk0aPZMVJbCphSCbkFhnGDAdrdZSH7vuC0Elz1jOGMV0k56oRS+Q2sWP+aglCG
ig0UB9KyqnMybsnfRPzt5MzUYmDUPxL72FJzthBuKEqI6lmDQOMxPgloLNBh/97k
sQJhzfGCCe02Fowc8uEe7HPduTS9fULUfj5npcSnFt0gNCkVk394/rKcWtQ4Hdq3
BP/Tbmec4BMoSvyc8n2s1m0w682l75zXTRjuEL0AOyObsANt0LH5kF6BJp+Flg4Q
u2a0TQLE/SMwg2/v3elb3WwhG9sJobFJBCPI3tX2dGlud7nzNxt3VlhXXU1Qa3Da
59LD7z2jri0ofwcxxFypxHrHLGSE9DlqWJRsCh+e9W4WmQM3fcHuqgHTc0goNiAU
o/5LD8f65rj4YxAMNDpg2o498t57c8htjJfn8l524OMX1aAS7q2pYLn4d3xOb2Oq
NF5sXfS3cdt+lTXUyad0MuYyVxAUvJeLefr9EZFlrwmO7b+u3EVwmudSs/ICwwSo
iHlzp7cv5QHXtKuSRHYY/ifSgqudyfHD9pCrGheH7REW8BDKfyTqN1WX4JJthEIs
qQN4m9Ny3ntR6itjUphI5HmHcg+boE+/w2mI7jBibydaTn+TvDGP7/MqBdPSfeK5
Yay+4ZCraDbUneFxIzUj6hw7xAT4Oll7rAj4DiBBTtgMuvtefa8Jf9KonC/OPmj/
eSvEgCZkxPf0FRjxq5yioARMjPEsqvKnTS4p0J3zJENfwbGAd21h1h2nq5/OQzG9
qCW9SH05UZGZAyN0b6ZuLnYNyfwNdOo6R8PKScR3Bb40Dm7HksGzeQO4RhhEMJyN
W05oRX1si5lntnedTORfDy2XBsVs6zQI9hpwUMC+i/TMgC+ikzBVEOyCYKec5bip
sVStWRkbt1LMnr75hBFpGC5npU1qmGF0fjMpyW9V6AN6dc7bLmnzmtWCWIHqnDFS
tLTnV+MWTOtlCgG0pz2fLrqYTjuEKGON1iJ2D+DSvX+MlYAvd9HCKLeH7HmrXqte
NKvc7q1nkxndv1SXmtFRovJfCd8xq8+swwNxIC+F/8CdgsUKEEwCKnViGUKx4EuP
Kq2jA06n/9QXbylk62UxTaKmT93l0xw6lAo3nq3ML267sUlkfdUqBQZrqtoPSyMC
tjSlk9iMPHbGqa57HA+oIyD5MfBzFoIzD7MXMA5sZra/EcoqF85tROgdr/tn7dRX
I7m2K8xsY+p7JgSb+yzOYZzsgweDIlU53Fje0NAlUn14+nIyO3vmspmkmToyBOyQ
oJiPdIwriirps7RPvwdUaU5MbUh/5o/xxBGud48FpawHr6FoOfKN0FnYKO1lzVNO
OWjmhs9/pQyiO84qrJoOeA713bVFhKGt2ko+68NlK1lizvHeCT6mmQ9RdlbWimmp
baFQzvma++UkMTx6+5vn5c8I0Cy8kvHKTYJy1Y1xZgk+yLfVQqJ+ldl7zeopyzoO
fSNACwyP/XahXa76vwdnscYGDSCi/dy5E78diGLjEENCKrbGv3pGgJCvRthes5E4
D9o/WcObA4xY1ncuNbdLZ/3FFcqsyGjexPlCYLjJJLr7dyCskaOko4yoHGgt/eNQ
AWO2N4TKCQkOtdyXFDcxa8urQ6Eon5zEfqaiGypqsPcod4p5NnCbqjBqO89Ihj+N
+V0aWu8tnc7Ij92rfhVPUUJqJdDg8dXe4g8i3aij3r0+FNp288WcaZ+kTlnTDktX
cdhhPotERnbapW3Zl6dEoHxfQKSYavbNL+ctEoXnLngCE3UrpoMVRmPRzqikTati
WlHOiTYkbBLnzgzMwTi3THOI07P/YDfnRpR5is+t4r5wp1qhyOyHMT3xaIg7PpdC
l1wck6HHWGKttFu33Qdq80wKCoFbyiUOdyuV860K8t5ISVLSRTrDKx/z0v9Fd7gQ
VrNb5QLyFKRMCwYP+qxLSIA9IohQkqnQs3nckUpgoXGfzKIs6C2LNWPU4iVMvrIP
8FkQXa+129oBnw4L/p1i8Qcpq3g7ZxkGTwrxU7r18fzge/3fhFU9VP/oxf1n+e7O
MG/Nx4gq04XtwlqWpfDgKOxbqRaceLvopRzGGNJH1MRqSF9Bww9o6Gd0AX6Yvxh/
Y9So9V3yD/WhEcQ3/Dgudsg9eEHP5FA+Cl4qC9ZOuS1jghEd6bnqmw5oJ3bNRHLp
d9OSESrcJl/44czejrqPCJGTjbwDefck8+ZK+MtVLBBM+ZPNGo4oS8bYBcABjU+q
c77VscCzfY8CXlovpVHIA+fP1UHXXhgWGqP3stQfE9WXz80R7KH5k20vPf5/qRR+
`pragma protect end_protected
