-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use work.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_rom is
  generic (
    g_technology : natural := c_tech_select_default;
    g_ram        : t_c_mem := c_mem_ram;
    g_init_file  : string  := "UNUSED"
  );
  port (
    rst           : in  std_logic := '0';
    clk           : in  std_logic;
    clken         : in  std_logic := '1';
    rd_en         : in  std_logic := '1';
    rd_adr        : in  std_logic_vector(g_ram.adr_w - 1 downto 0) := (others => '0');
    rd_dat        : out std_logic_vector(g_ram.dat_w - 1 downto 0) := (others => '0');
    rd_val        : out std_logic
  );
end common_rom;

architecture str of common_rom is
begin
  -- Only use the read port

  u_r_w : entity work.common_ram_r_w
  generic map (
    g_technology => g_technology,
    g_ram        => g_ram,
    g_init_file  => g_init_file
  )
  port map (
    rst       => rst,
    clk       => clk,
    clken     => clken,
    wr_en     => '0',
    --wr_adr    => (OTHERS=>'0'),
    --wr_dat    => (OTHERS=>'0'),
    rd_en     => rd_en,
    rd_adr    => rd_adr,
    rd_dat    => rd_dat,
    rd_val    => rd_val
  );
end str;
