-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra, 30 mar 2017
-- Purpose: Set gains per block of valid samples in a stream via MM
-- Description:
--   Similar as mms_dp_gain_par.vhd but instead this mms_dp_gain_serial_arr can
--   apply g_nof_gains different gains in time (series) using a gains RAM per
--   input data stream.
--   Complex gains are stored as data = im & re
--
-- Remark:
-- . This component is a generalization of the weights multiplication function
--   in the bf_unit.vhd. In addition to complex input and gains this
--   mms_dp_gain_serial_arr also supports using real input data or using real
--   gains.
-- . The gains memories can be pre-initialized for simulation using .hex files.
--   RAM init files (.hex files) only work when g_gains_write_only is set to
--   FALSE.
-- . The gains memories are single page.
-- . The MM interface (ram_gains_mosi/miso) has a read latency of 2. Therefore,
--   choose the correct avs_common_mm_readlatency2 when creating the qsys system.
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library common_lib, common_mult_lib, technology_lib;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_gain_serial_arr is
  generic (
    g_technology                    : natural := c_tech_select_default;
    g_nof_streams                   : natural := 1;
    g_nof_gains                     : natural := 256;  -- number of gains in series per stream
    g_complex_data                  : boolean := true;
    g_complex_gain                  : boolean := false;
    g_gain_w                        : natural := 16;  -- to fit 32b MM data use <= 32 for real, <= 16 for complex
    g_in_dat_w                      : natural := 9;
    g_out_dat_w                     : natural := 24;  -- 16 + 9 - 1 = 24 (-1 to skip double sign bit in product)
    g_gains_file_name               : string  := "UNUSED";  -- "UNUSED" or relative path to some "gains_#.hex" file, where # is the stream index
    g_gains_write_only              : boolean := false;  -- When set to TRUE the M9K blocks are forced to Simple Dual Port mode to save memory. When FALSE it is True Dual Port.

    -- pipelining (typically use defaults)
    -- . real multiplier
    g_pipeline_real_mult_input      : natural := 1;  -- 0 or 1
    g_pipeline_real_mult_product    : natural := 1;  -- 0 or 1
    g_pipeline_real_mult_output     : natural := 1;  -- >= 0
    -- . complex multiplier
    g_pipeline_complex_mult_input   : natural := 1;  -- 0 or 1
    g_pipeline_complex_mult_product : natural := 0;  -- 0 or 1
    g_pipeline_complex_mult_adder   : natural := 1;  -- 0 or 1
    g_pipeline_complex_mult_output  : natural := 1  -- >= 0
  );
  port (
    -- System
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;
    dp_rst                  : in  std_logic := '0';
    dp_clk                  : in  std_logic;

    -- MM interface
    ram_gains_mosi          : in  t_mem_mosi := c_mem_mosi_rst;  -- write side
    ram_gains_miso          : out t_mem_miso;

    -- ST interface
    gains_rd_address        : in  std_logic_vector(ceil_log2(g_nof_gains) - 1 downto 0);  -- read side, same read address for all streams

    in_sosi_arr             : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    out_sosi_arr            : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_gain_serial_arr;

architecture str of mms_dp_gain_serial_arr is
  constant c_real_multiply  : boolean := g_complex_data = false and g_complex_gain = false;
  constant c_conjugate      : boolean := false;

  -- TYPE t_c_mem IS RECORD
  --   latency   : NATURAL;    -- read latency
  --   adr_w     : NATURAL;
  --   dat_w     : NATURAL;
  --   nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
  --   init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
  constant c_mm_ram : t_c_mem := (latency  => 2,  -- set latency to 2 to ease timing
                                  adr_w    => ceil_log2(g_nof_gains),
                                  dat_w    => sel_a_b(g_complex_gain, c_nof_complex, 1) * g_gain_w,
                                  nof_dat  => g_nof_gains,
                                  init_sl  => '0');

  constant c_pipeline_real_latency : natural := g_pipeline_real_mult_input
                                               + g_pipeline_real_mult_product
                                               + g_pipeline_real_mult_output
                                               + c_mm_ram.latency;

  constant c_pipeline_complex_latency : natural := g_pipeline_complex_mult_input
                                                  + g_pipeline_complex_mult_product
                                                  + g_pipeline_complex_mult_adder
                                                  + g_pipeline_complex_mult_output
                                                  + c_mm_ram.latency;

  constant c_pipeline_latency : natural := sel_a_b(c_real_multiply, c_pipeline_real_latency, c_pipeline_complex_latency);

  type t_slv_rddata_arr      is array (integer range <>) of std_logic_vector(c_mm_ram.dat_w - 1 downto 0);
  type t_slv_gains_arr       is array (integer range <>) of std_logic_vector(g_gain_w - 1 downto 0);
  type t_slv_in_dat_arr      is array (integer range <>) of std_logic_vector(g_in_dat_w - 1 downto 0);
  type t_slv_out_dat_arr     is array (integer range <>) of std_logic_vector(g_out_dat_w - 1 downto 0);

  signal mm_gains_mosi_arr     : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal mm_gains_miso_arr     : t_mem_miso_arr(g_nof_streams - 1 downto 0) := (others => c_mem_miso_rst);

  signal gains_rd_data_arr     : t_slv_rddata_arr(g_nof_streams - 1 downto 0);
  signal gains_re_arr          : t_slv_gains_arr(g_nof_streams - 1 downto 0);
  signal gains_im_arr          : t_slv_gains_arr(g_nof_streams - 1 downto 0);

  signal in_sosi_arr_pipe      : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal in_sosi_arr_pipe_ctrl : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal in_dat_re_arr         : t_slv_in_dat_arr(g_nof_streams - 1 downto 0);
  signal in_dat_im_arr         : t_slv_in_dat_arr(g_nof_streams - 1 downto 0);
  signal in_val_arr            : std_logic_vector(g_nof_streams - 1 downto 0);

  signal out_dat_re_arr        : t_slv_out_dat_arr(g_nof_streams - 1 downto 0);
  signal out_dat_im_arr        : t_slv_out_dat_arr(g_nof_streams - 1 downto 0);
  signal out_val_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
begin
  -- pipeline in_sosi_arr to align it with gains_rd_data_arr
  u_pipeline_arr : entity work.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_streams,
    g_pipeline    => c_mm_ram.latency  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    snk_in_arr   => in_sosi_arr,
    src_out_arr  => in_sosi_arr_pipe
  );

  -- pipeline in_sosi_arr to add sop, eop and sync back in  out_sosi_arr
  u_pipeline_arr_ctrl : entity work.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_streams,
    g_pipeline    => c_pipeline_latency
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    snk_in_arr   => in_sosi_arr,
    src_out_arr  => in_sosi_arr_pipe_ctrl
  );

  u_mem_mux_gains : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => ceil_log2(g_nof_gains)
  )
  port map (
    mosi     => ram_gains_mosi,
    miso     => ram_gains_miso,
    mosi_arr => mm_gains_mosi_arr,
    miso_arr => mm_gains_miso_arr
  );

  gen_nof_streams : for I in 0 to g_nof_streams - 1 generate
    -- Instantiate a gains memory for each input stream:
    u_common_ram_crw_crw : entity common_lib.common_ram_crw_crw
    generic map (
      g_technology     => g_technology,
      g_ram            => c_mm_ram,
      g_init_file      => sel_a_b(g_gains_file_name = "UNUSED", g_gains_file_name, g_gains_file_name & "_" & natural'image(I) & ".hex"),
      g_true_dual_port => not(g_gains_write_only)
    )
    port map (
      -- MM side
      rst_a     => mm_rst,
      clk_a     => mm_clk,
      wr_en_a   => mm_gains_mosi_arr(I).wr,
      wr_dat_a  => mm_gains_mosi_arr(I).wrdata(c_mm_ram.dat_w - 1 downto 0),
      adr_a     => mm_gains_mosi_arr(I).address(c_mm_ram.adr_w - 1 downto 0),
      rd_en_a   => mm_gains_mosi_arr(I).rd,
      rd_dat_a  => mm_gains_miso_arr(I).rddata(c_mm_ram.dat_w - 1 downto 0),
      rd_val_a  => mm_gains_miso_arr(I).rdval,
      -- ST side
      rst_b     => dp_rst,
      clk_b     => dp_clk,
      wr_en_b   => '0',
      wr_dat_b  => (others => '0'),
      adr_b     => gains_rd_address,
      rd_en_b   => '1',
      rd_dat_b  => gains_rd_data_arr(I)(c_mm_ram.dat_w - 1 downto 0),
      rd_val_b  => open
    );

    gen_real_multiply : if c_real_multiply = true generate
      gains_re_arr(I)  <= gains_rd_data_arr(I)(g_gain_w - 1 downto 0);

      in_dat_re_arr(I) <= in_sosi_arr_pipe(I).re(g_in_dat_w - 1 downto 0);
      in_val_arr(I)    <= in_sosi_arr_pipe(I).valid;

      u_common_mult : entity common_mult_lib.common_mult
      generic map (
        g_technology       => g_technology,
        g_variant          => "IP",
        g_in_a_w           => g_gain_w,
        g_in_b_w           => g_in_dat_w,
        g_out_p_w          => g_out_dat_w,  -- g_out_dat_w = g_gain_w+g_in_dat_w, use smaller g_out_dat_w to truncate MSbits, or larger g_out_dat_w to extend MSbits
        g_nof_mult         => 1,
        g_pipeline_input   => g_pipeline_real_mult_input,
        g_pipeline_product => g_pipeline_real_mult_product,
        g_pipeline_output  => g_pipeline_real_mult_output,
        g_representation   => "SIGNED"  -- or "UNSIGNED"
      )
      port map (
        rst        => dp_rst,
        clk        => dp_clk,
        in_a       => gains_re_arr(I),
        in_b       => in_dat_re_arr(I),
        in_val     => in_val_arr(I),
        out_p      => out_dat_re_arr(I),
        out_val    => out_val_arr(I)
      );

      p_out_sosi_arr : process(out_val_arr, out_dat_re_arr, in_sosi_arr_pipe_ctrl)
      begin
        out_sosi_arr(I)       <= c_dp_sosi_rst;  -- to avoid (vsim-8684) No drivers exist on out port out_sosi_arr
        out_sosi_arr(I).valid <= out_val_arr(I);
        out_sosi_arr(I).data  <= RESIZE_DP_SDATA(out_dat_re_arr(I));  -- sign extend
        out_sosi_arr(I).sop   <= in_sosi_arr_pipe_ctrl(I).sop;
        out_sosi_arr(I).eop   <= in_sosi_arr_pipe_ctrl(I).eop;
        out_sosi_arr(I).sync  <= in_sosi_arr_pipe_ctrl(I).sync;
        out_sosi_arr(I).bsn   <= in_sosi_arr_pipe_ctrl(I).bsn;
      end process;
    end generate gen_real_multiply;

    gen_complex_multiply : if c_real_multiply = false generate
      gen_real_gain : if g_complex_gain = false generate
        gains_re_arr(I) <= gains_rd_data_arr(I)(g_gain_w - 1 downto 0);
        gains_im_arr(I) <= gains_rd_data_arr(I)(g_gain_w - 1 downto 0);
      end generate gen_real_gain;

      gen_complex_gain : if g_complex_gain = true generate
        gains_re_arr(I) <= gains_rd_data_arr(I)(              g_gain_w - 1 downto 0);
        gains_im_arr(I) <= gains_rd_data_arr(I)(c_nof_complex * g_gain_w - 1 downto g_gain_w);
      end generate gen_complex_gain;

      in_dat_re_arr(I) <= in_sosi_arr_pipe(I).re(g_in_dat_w - 1 downto 0);
      in_dat_im_arr(I) <= in_sosi_arr_pipe(I).im(g_in_dat_w - 1 downto 0);
      in_val_arr(I)    <= in_sosi_arr_pipe(I).valid;

      u_common_complex_mult : entity common_mult_lib.common_complex_mult
      generic map (
        g_technology       => g_technology,
        g_variant          => "IP",
        g_in_a_w           => g_gain_w,
        g_in_b_w           => g_in_dat_w,
        g_out_p_w          => g_out_dat_w,
        g_conjugate_b      => c_conjugate,
        g_pipeline_input   => g_pipeline_complex_mult_input,
        g_pipeline_product => g_pipeline_complex_mult_product,
        g_pipeline_adder   => g_pipeline_complex_mult_adder,
        g_pipeline_output  => g_pipeline_complex_mult_output
      )
      port map (
        rst        => dp_rst,
        clk        => dp_clk,
        in_ar      => gains_re_arr(I),
        in_ai      => gains_im_arr(I),
        in_br      => in_dat_re_arr(I),
        in_bi      => in_dat_im_arr(I),
        in_val     => in_val_arr(I),
        out_pr     => out_dat_re_arr(I),
        out_pi     => out_dat_im_arr(I),
        out_val    => out_val_arr(I)
      );

      p_out_sosi_arr : process(out_val_arr, out_dat_re_arr, out_dat_im_arr, in_sosi_arr_pipe_ctrl)
      begin
        out_sosi_arr(I)       <= c_dp_sosi_rst;  -- to avoid (vsim-8684) No drivers exist on out port out_sosi_arr
        out_sosi_arr(I).valid <= out_val_arr(I);
        out_sosi_arr(I).re    <= RESIZE_DP_DSP_DATA(out_dat_re_arr(I));  -- sign extend
        out_sosi_arr(I).im    <= RESIZE_DP_DSP_DATA(out_dat_im_arr(I));  -- sign extend
        out_sosi_arr(I).sop   <= in_sosi_arr_pipe_ctrl(I).sop;
        out_sosi_arr(I).eop   <= in_sosi_arr_pipe_ctrl(I).eop;
        out_sosi_arr(I).sync  <= in_sosi_arr_pipe_ctrl(I).sync;
        out_sosi_arr(I).bsn   <= in_sosi_arr_pipe_ctrl(I).bsn;
      end process;
    end generate gen_complex_multiply;
  end generate gen_nof_streams;
end str;
