--------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Author:
-- . Eric Kooistra, 14 Feb 2023
-- Purpose:
-- . Reverse the order of multiplexed data per every g_reverse_len values in
--   time.
-- Description:
-- . The snk_in blocks size must be an integer multiple of g_reverse_len.
-- . For example with g_reverse_len = 3, then snk_in input data 012_345_678
--   becomes 210_543_876 at src_out output.
-- Remark:
-- . The dp_reverse_n_data.vhd and dp_reverse_n_data_fc.vhd are functionally
--   equivalent. Use dp_reverse_n_data_fc.vhd flow control (fc) is needed.
--   A difference is that dp_reverse_n_data_fc causes extra gaps in the data
--   valid after each g_reverse_len. Therefore use dp_reverse_n_data when no
--   flow control is needed.
-- . Typically g_reverse_len should not be too large (~< 4), because then the
--   implementation takes relatively too much logic.

entity dp_reverse_n_data_fc is
  generic (
    -- Pipeline: 0 for combinatorial, > 0 for registers
    g_pipeline_in   : natural := 1;  -- serial to parallel section
    g_pipeline_out  : natural := 0;  -- parallel to serial section
    g_reverse_len   : natural
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_out     : out t_dp_siso;
    snk_in      : in  t_dp_sosi;
    src_in      : in  t_dp_siso := c_dp_siso_rdy;
    src_out     : out t_dp_sosi
  );
end dp_reverse_n_data_fc;

architecture str of dp_reverse_n_data_fc is
  signal demux_siso_arr   : t_dp_siso_arr(g_reverse_len - 1 downto 0);
  signal demux_sosi_arr   : t_dp_sosi_arr(g_reverse_len - 1 downto 0);
  signal reverse_siso_arr : t_dp_siso_arr(g_reverse_len - 1 downto 0);
  signal reverse_sosi_arr : t_dp_sosi_arr(g_reverse_len - 1 downto 0);
begin
  u_demux_one_to_n : entity work.dp_deinterleave_one_to_n
  generic map (
    g_pipeline     => g_pipeline_in,
    g_nof_outputs  => g_reverse_len
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out     => snk_out,
    snk_in      => snk_in,
    src_in_arr  => demux_siso_arr,
    src_out_arr => demux_sosi_arr
  );

  gen_reverse : for I in 0 to g_reverse_len - 1 generate
    demux_siso_arr(g_reverse_len - 1 - I) <= reverse_siso_arr(I);
    reverse_sosi_arr(I) <= demux_sosi_arr(g_reverse_len - 1 - I);
  end generate;

  u_mux_n_to_one : entity work.dp_interleave_n_to_one
  generic map (
    g_pipeline   => g_pipeline_out,
    g_nof_inputs => g_reverse_len
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out_arr => reverse_siso_arr,
    snk_in_arr  => reverse_sosi_arr,
    src_in      => src_in,
    src_out     => src_out
  );
end str;
