-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for lvdsh_dd_wb4
-- Description:
-- Usage:
-- > as 10
-- > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_lvdsh_dd_wb4 is
  generic (
     g_dclk_drift  : time := 0 ps;  -- 0 ps, use -2 ps or 2 ps to model the range of sample phase uncertainty by letting the sclk and dclk drift with respect to the dp_clk,
                                       -- use factor 2 value because of integer divide by 2 for rising and falling edge per sclk clock period.
     g_dclk_offon  : boolean := true;  -- when TRUE switch the dclk off-on periodically, to model ADU not present or ADU restart or ADU replaced
     g_in_phase    : natural := 1  -- 0:3
  );
end tb_lvdsh_dd_wb4;

architecture tb of tb_lvdsh_dd_wb4 is
  constant c_sim               : boolean := true;
  constant c_tb_duration       : natural := sel_a_b(g_dclk_drift /= 0 ps or g_dclk_offon = true, 1000, 10);  -- nof tb intervals for tb duration
  constant c_tb_init           : natural := sel_a_b(g_dclk_drift /= 0 ps or g_dclk_offon = true,   50,  5);  -- nof tb intervals for tb init before verify_en
  constant c_interval          : natural := 180;  -- some unit tb interval
  constant c_offon_interval    : natural := c_interval * c_tb_duration / 13;

  constant c_dd_factor         : natural := 2;
  constant c_rx_factor         : natural := 2;
  constant c_wb_factor         : natural := c_dd_factor * c_rx_factor;
  constant c_clk_factor        : natural := 100;  -- slow down dclk to improve modelling dclk phase uncertainy with g_dclk_drift, which is minimal 2 ps

  constant c_sclk_period       : time := c_clk_factor * 1.25 ns + g_dclk_drift;  -- 800 MHz single data rate ADC sample clock with optional drift
  constant c_dp_sclk_period    : time := c_clk_factor * 1.25 ns;  -- 800 MHz sample clock for dp_sample_dat monitor
  constant c_dp_clk_period     : time := c_clk_factor * 5 ns;  -- 200 MHz data clock for Data Path processing
  constant c_dp_clk_delay      : time := (0.0   + 0.05) * c_dp_clk_period;  -- =   0 degrees
  --CONSTANT c_dp_clk_delay      : TIME := (0.125 + 0.05)*c_dp_clk_period;  -- =  45 degrees
  --CONSTANT c_dp_clk_delay      : TIME := (0.25  + 0.05)*c_dp_clk_period;  -- =  90 degrees
  --CONSTANT c_dp_clk_delay      : TIME := (0.375 + 0.05)*c_dp_clk_period;  -- = 135 degrees
  --CONSTANT c_dp_clk_delay      : TIME := (0.5   + 0.05)*c_dp_clk_period;  -- = 180 degrees
  --CONSTANT c_dp_clk_delay      : TIME := (0.625 + 0.05)*c_dp_clk_period;  -- = 225 degrees
  --CONSTANT c_dp_clk_delay      : TIME := (0.75  + 0.05)*c_dp_clk_period;  -- = 270 degrees
  --CONSTANT c_dp_clk_delay      : TIME := (0.875 + 0.05)*c_dp_clk_period;  -- = 315 degrees

  constant c_rl                : natural := 1;
  constant c_init              : natural := 0;
  constant c_in_dat_w          : natural := 8;
  constant c_dp_dat_w          : natural := c_wb_factor * c_in_dat_w;

  signal tb_g_dclk_drift    : time := g_dclk_drift;
  signal tb_g_dclk_offon    : boolean := g_dclk_offon;
  signal tb_g_in_phase      : natural := g_in_phase;

  signal tb_end             : std_logic := '0';
  signal rst                : std_logic := '1';
  signal s_clk              : std_logic := '1';
  signal d_clk              : std_logic := '1';
  signal d_clk_off          : std_logic := '0';
  signal d_clk_rst          : std_logic;
  signal dp_clk             : std_logic := '1';
  signal dp_clkq            : std_logic;
  signal dp_clk_dly         : std_logic;
  signal dp_clkq_dly        : std_logic;
  signal dp_sclk            : std_logic := '1';
  signal ready              : std_logic := '1';
  signal verify_en          : std_logic := '0';
  signal verify_phase_en    : std_logic := '0';
  signal verify_data_en     : std_logic := '0';
  signal cnt_en             : std_logic := '1';

  signal in_dat             : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal in_val             : std_logic;
  signal dp_dat             : std_logic_vector(c_dp_dat_w - 1 downto 0);
  signal dp_val             : std_logic := '1';
  signal dp_sample_dat      : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal prev_dp_sample_dat : std_logic_vector(c_in_dat_w - 1 downto 0);

  signal dp_sync_phase      : natural;
  signal dp_status          : std_logic_vector(c_word_w - 1 downto 0);  -- extra status information for debug
  signal dp_locked          : std_logic;
  signal dp_stable          : std_logic;
  signal dp_stable_ack      : std_logic;
begin
  d_clk_off <= not d_clk_off or tb_end after c_offon_interval * c_dp_clk_period when g_dclk_offon = true;

  s_clk   <= not s_clk   or tb_end after c_sclk_period / 2;
  dp_sclk <= not dp_sclk or tb_end after c_dp_sclk_period / 2;

  p_dclk_divider : process
  begin
    if rst = '1' or d_clk_off = '1' then
      d_clk <= '1';
      wait until rising_edge(s_clk);
    else
      case g_in_phase is
        when 0|2 => d_clk <= '1'; wait until rising_edge(s_clk);
                    d_clk <= '0'; wait until rising_edge(s_clk);
        when 1|3 => d_clk <= '0'; wait until rising_edge(s_clk);
                    d_clk <= '1'; wait until rising_edge(s_clk);
        when others => null;
      end case;
    end if;
  end process;

  p_dp_clk_divider : process
  begin
    if rst = '1' then
      dp_clk <= '1';
      wait until rising_edge(s_clk);
    else
      dp_clk <= '1'; wait until rising_edge(dp_sclk);
      dp_clk <= '1'; wait until rising_edge(dp_sclk);
      dp_clk <= '0'; wait until rising_edge(dp_sclk);
      dp_clk <= '0'; wait until rising_edge(dp_sclk);
    end if;
  end process;

  rst <= '1', '0' after 3 * c_dp_clk_period;

  proc_common_gen_pulse(1, c_interval, '1',  rst, dp_clk, dp_stable_ack);

  -- Stimuli data
  p_stimuli : process
  begin
    proc_common_wait_some_cycles(dp_clk, c_interval * c_tb_init);
    if g_dclk_drift = 0 ps then
      verify_en <= '1';
    end if;

    proc_common_wait_some_cycles(dp_clk, c_interval * c_tb_duration);

    proc_common_wait_some_cycles(dp_clk, c_interval);
    tb_end <= '1';
    wait;
  end process;

  proc_common_gen_data(c_rl, c_init, rst, s_clk, cnt_en, ready, in_dat, in_val);

  -- Verify data
  verify_data_en  <= verify_en and dp_locked;
  verify_phase_en <= verify_en;

  proc_common_verify_data(c_rl, dp_sclk, verify_data_en, ready, dp_val, dp_sample_dat, prev_dp_sample_dat);

  p_verify_phase : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if verify_phase_en = '1' then
        assert dp_sync_phase = g_in_phase or dp_sync_phase = 4 + g_in_phase
          report "Unexpected dp_sync_phase"
          severity ERROR;
      end if;
    end if;
  end process;

  -- DUT (device under test)
  dp_clkq     <= transport dp_clk     after c_dp_clk_period / 4;
  dp_clk_dly  <= transport dp_clk     after c_dp_clk_delay;
  dp_clkq_dly <= transport dp_clk_dly after c_dp_clk_period / 4;

  u_lvdsh_dd_wb4 : entity work.lvdsh_dd_wb4
  generic map (
    g_sim       => c_sim,
    g_sim_phase => g_in_phase,
    g_wb_factor => c_wb_factor,
    g_in_dat_w  => c_in_dat_w
  )
  port map (
    -- PHY input interface
    in_clk_rst    => d_clk_rst,
    in_clk        => d_clk,
    in_dat        => in_dat,

    -- DD --> Rx domain interface at d_clk rate or g_dp_factor lower rate (via FIFO)
    dp_rst        => rst,
    dp_clk        => dp_clk_dly,
    dp_clkq       => dp_clkq_dly,
    dp_dat        => dp_dat,
    dp_val        => dp_val,

    -- Rx status monitor
    dp_sync_phase => dp_sync_phase,
    dp_status     => dp_status,
    dp_locked     => dp_locked,
    dp_stable     => dp_stable,
    dp_stable_ack => dp_stable_ack
  );

  u_wb_data_scope : entity common_lib.common_wideband_data_scope
  generic map (
    g_sim                 => true,
    g_wideband_factor     => c_wb_factor,
    g_wideband_big_endian => true,
    g_dat_w               => c_in_dat_w
  )
  port map (
    -- Sample clock
    SCLK      => dp_sclk,

    -- Streaming input data
    in_data   => dp_dat,
    in_val    => dp_val,

    -- Scope output samples
    out_dat   => dp_sample_dat
  );
end tb;
