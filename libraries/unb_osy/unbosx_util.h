/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

#ifndef UNBOSX_UTIL_H
#define UNBOSX_UTIL_H


/* includes */
#include <system.h>
#include <unbosx_types.h>
#include <altera_avalon_pio_regs.h>


/* The NiosII CPU is little endian so we 
 * must swap going from host -> network and from network -> host byteorder
 * since networkbyteorder is big endian
 */

/* 16 bit swap */
#define UNBOSX_UTIL_ntohs(x)  ((((x) >> 8) & 0xff) | \
		               (((x) << 8) & 0xff00))
#define UNBOSX_UTIL_htons(x)  UNBOSX_UTIL_ntohs(x)

/* 32 bit swap */
#define UNBOSX_UTIL_ntohl(x)  ((((x >> 24) & 0x000000ff)) | \
		               (((x >>  8) & 0x0000ff00)) | \
		               (((x) & 0x0000ff00) <<  8) | \
			       (((x) & 0x000000ff) << 24))
#define UNBOSX_UTIL_htonl(a)  UNBOSX_UTIL_ntohl(a)




/* UNB1: */
/* [7:0] = id : UniBoard FPGA node ID in entire multi subrack system:
 * [2:0] = nodenumber (0 -> 7)
 * [2]   => if '1' this implies it's a backnode (ie: 4, 5, 6, 7)
 * [7:3] = backplaneid
 */
#define UNBOSX_GetNodeNumber()  (IORD_ALTERA_AVALON_PIO_DATA(PIO_SYSTEM_INFO_BASE)&0x7)
#define UNBOSX_GetBackplaneId() ((IORD_ALTERA_AVALON_PIO_DATA(PIO_SYSTEM_INFO_BASE)&0xff)>>3)
#define UNBOSX_IsBacknode()     ((IORD_ALTERA_AVALON_PIO_DATA(PIO_SYSTEM_INFO_BASE)&0x4)==0x4)



/* UNB2: */
/* [7:0] = id : UniBoard FPGA node ID in entire multi subrack system:
 * [1:0] = nodenumber (0 -> 3)
 * [7:2] = backplaneid
 */
#define UNBOSX_unb2_GetNodeNumber()  (IORD_ALTERA_AVALON_PIO_DATA(PIO_SYSTEM_INFO_BASE)&0x3)
#define UNBOSX_unb2_GetBackplaneId() ((IORD_ALTERA_AVALON_PIO_DATA(PIO_SYSTEM_INFO_BASE)&0xff)>>2)
#define UNBOSX_unb2_IsBacknode()     (0)



#define UNBOSX_G_SIM()          ((IORD_ALTERA_AVALON_PIO_DATA(PIO_SYSTEM_INFO_BASE)&0x400)!=0)

#define UNBOSX_ResetWatchdog()  (IOWR_ALTERA_AVALON_PIO_DATA(PIO_WDI_BASE, !((IORD_ALTERA_AVALON_PIO_DATA(PIO_WDI_BASE))&0x1)))
/* public types */

/* public variables */

/* public function protos */

#endif

