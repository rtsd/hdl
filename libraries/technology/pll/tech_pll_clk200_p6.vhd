-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_pll_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_pll_lib;

entity tech_pll_clk200_p6 is
  generic (
    g_technology       : natural := c_tech_select_default;
    g_pll_type         : string := "Left_Right";  -- "AUTO", "Left_Right", or "Top_Bottom". Set "Left_Right" to direct using PLL_L3 close to CLK pin on UniBoard, because with "AUTO" still a top/bottom PLL may get inferred.
    g_operation_mode   : string := "NORMAL";  -- or "SOURCE_SYNCHRONOUS" --> requires PLL_COMPENSATE assignment to an input pin to compensate for (stratixiv)
    g_clk0_phase_shift : string := "0";  -- = 0 degrees for clk 200 MHz
    g_clk1_used        : string := "PORT_USED";  -- or "PORT_UNUSED"
    g_clk2_used        : string := "PORT_USED";  -- or "PORT_UNUSED"
    g_clk3_used        : string := "PORT_USED";  -- or "PORT_UNUSED"
    g_clk4_used        : string := "PORT_USED";  -- or "PORT_UNUSED"
    g_clk5_used        : string := "PORT_USED";  -- or "PORT_UNUSED"
    g_clk6_used        : string := "PORT_USED";  -- or "PORT_UNUSED"
    g_clk1_divide_by   : natural := 32;  -- = clk 200/32 MHz
    g_clk2_divide_by   : natural := 32;  -- = clk 200/32 MHz
    g_clk3_divide_by   : natural := 32;  -- = clk 200/32 MHz
    g_clk4_divide_by   : natural := 32;  -- = clk 200/32 MHz
    g_clk5_divide_by   : natural := 32;  -- = clk 200/32 MHz
    g_clk6_divide_by   : natural := 32;  -- = clk 200/32 MHz
    g_clk1_phase_shift : string := "0";  -- = 0
    g_clk2_phase_shift : string := "156";  -- = 011.25
    g_clk3_phase_shift : string := "313";  -- = 022.5
    g_clk4_phase_shift : string := "469";  -- = 033.75
    g_clk5_phase_shift : string := "625";  -- = 045
                                -- "781"         -- = 056.25
    g_clk6_phase_shift : string := "938"  -- = 067.5
                                -- "1094"        -- = 078.75
  );
  port
  (
    areset    : in std_logic  := '0';
    inclk0    : in std_logic  := '0';
    c0        : out std_logic;
    c1        : out std_logic;
    c2        : out std_logic;
    c3        : out std_logic;
    c4        : out std_logic;
    c5        : out std_logic;
    c6        : out std_logic;
    locked    : out std_logic
  );
end tech_pll_clk200_p6;

architecture str of tech_pll_clk200_p6 is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_pll_clk200_p6
    generic map (g_pll_type, g_operation_mode,
                 g_clk0_phase_shift,
                 g_clk1_used,        g_clk2_used,        g_clk3_used,        g_clk4_used,        g_clk5_used,        g_clk6_used,
                 g_clk1_divide_by,   g_clk2_divide_by,   g_clk3_divide_by,   g_clk4_divide_by,   g_clk5_divide_by,   g_clk6_divide_by,
                 g_clk1_phase_shift, g_clk2_phase_shift, g_clk3_phase_shift, g_clk4_phase_shift, g_clk5_phase_shift, g_clk6_phase_shift)
    port map (areset, inclk0, c0, c1, c2, c3, c4, c5, c6, locked);
  end generate;
end architecture;
