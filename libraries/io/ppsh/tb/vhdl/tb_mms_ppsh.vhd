-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;

entity tb_mms_ppsh is
end tb_mms_ppsh;

architecture tb of tb_mms_ppsh is
  constant c_st_clk_freq     : natural := 1000;  -- clock frequency in Hz
  constant c_st_clk_period   : time    := 1000000 us / c_st_clk_freq;
  constant c_mm_clk_period   : time    := c_st_clk_period * 3;  -- somewhat slower mm_clk
  constant c_pps_period      : natural := c_st_clk_freq;  -- 1 s takes c_clk_freq clk cycles

  constant c_cnt_w           : natural := ceil_log2(c_st_clk_freq);

  signal tb_end           : std_logic := '0';
  signal mm_rst           : std_logic := '1';
  signal mm_clk           : std_logic := '1';
  signal st_rst           : std_logic := '1';
  signal st_clk           : std_logic := '1';

  -- DUT
  signal pps_ext          : std_logic;
  signal pps_sys          : std_logic;

  signal reg_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso         : t_mem_miso;

  -- Verify
  signal bsn              : natural;  -- block sequence number counts seconds
  signal pps_toggle       : std_logic;
  signal pps_stable       : std_logic;
  signal capture_cnt      : natural;
  signal offset_cnt       : natural;
  signal last_offset_cnt  : natural;
begin
  -- Usage:
  -- > as 10
  -- > run -all
  -- p_verify assert when unexpected capture_cnt and pps_stable are read via MM

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------
  st_rst <= '1', '0' after 7 * c_st_clk_period;
  st_clk <= not st_clk or tb_end after c_st_clk_period / 2;
  mm_rst <= '1', '0' after 7 * c_mm_clk_period;
  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;

  p_pps_ext : process
    variable v_pps_period : natural := c_pps_period;
  begin
    bsn <= 0;
    pps_ext <= '0';
    while true loop
      -- Generate default and some disturbed PPS periods
      v_pps_period := c_pps_period;
      if bsn = 29 then
        v_pps_period := c_pps_period - 1;
      elsif bsn = 69 then
        v_pps_period := c_pps_period + 1;
      end if;
      proc_common_wait_some_cycles(st_clk, v_pps_period - 1);
      pps_ext <= '1';
      bsn <= bsn + 1;
      proc_common_wait_some_cycles(st_clk, 1);
      pps_ext <= '0';
    end loop;

    wait;
  end process;

  p_mm_stimuli : process
    variable v_word : std_logic_vector(c_word_w - 1 downto 0);
  begin
    proc_common_wait_until_low(st_clk, st_rst);  -- Wait until reset has finished
    proc_common_wait_until_low(mm_clk, mm_rst);  -- Wait until reset has finished
    proc_common_wait_some_cycles(mm_clk, 10);  -- Wait an additional amount of cycles

    v_word := '0' & TO_UVEC(c_pps_period, 31);  -- capture_edge = '0' = at rising edge
                                                 -- expected_cnt = c_pps_period = 1000
    proc_mem_mm_bus_wr(1, v_word, mm_clk, reg_mosi);

    -- Simulate reading PPS status every 10 PPS periods
    proc_common_wait_some_cycles(st_clk, 10);
    for I in 0 to 9 loop
      proc_common_wait_some_cycles(st_clk, c_pps_period * 10);

      proc_mem_mm_bus_rd(0, mm_clk, reg_mosi);
      proc_common_wait_some_cycles(mm_clk, 1);
      pps_toggle  <= reg_miso.rddata(31);
      pps_stable  <= reg_miso.rddata(30);
      capture_cnt <= TO_UINT(reg_miso.rddata(c_cnt_w - 1 downto 0));
    end loop;

    -- Simulate reading PPS offset counter every 0.25 PPS periods
    proc_common_wait_some_cycles(st_clk, 10);
    for I in 0 to 40 loop
      proc_common_wait_some_cycles(st_clk, c_pps_period / 4);
      last_offset_cnt <= offset_cnt;
      proc_mem_mm_bus_rd(2, mm_clk, reg_mosi);
      proc_common_wait_some_cycles(mm_clk, 1);
      offset_cnt <= TO_UINT(reg_miso.rddata(c_cnt_w - 1 downto 0));
    end loop;

    proc_common_wait_some_cycles(st_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  p_verify : process
  begin
    proc_common_wait_until_low(st_clk, st_rst);  -- Wait until reset has finished
    proc_common_wait_some_cycles(st_clk, 10);  -- Wait an additional amount of cycles

    proc_common_wait_some_cycles(st_clk, c_pps_period / 2);  -- Verification offset
    -- 1
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '0'
      report "1) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 1000
      report "1) Wrong capture_cnt"
      severity ERROR;
    -- 2
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '1'
      report "2) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 1000
      report "2) Wrong capture_cnt"
      severity ERROR;
    -- 3
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '0'
      report "3) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 999
      report "3) Wrong capture_cnt"
      severity ERROR;
    -- 4
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '0'
      report "4) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 1000
      report "4) Wrong capture_cnt"
      severity ERROR;
    -- 5
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '1'
      report "5) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 1000
      report "5) Wrong capture_cnt"
      severity ERROR;
    -- 6
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '1'
      report "6) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 1000
      report "6) Wrong capture_cnt"
      severity ERROR;
    -- 7
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '0'
      report "7) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 1001
      report "7) Wrong capture_cnt"
      severity ERROR;
    -- 8
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '0'
      report "8) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 1000
      report "8) Wrong capture_cnt"
      severity ERROR;
    -- 9
    proc_common_wait_some_cycles(st_clk, c_pps_period * 10);
    assert pps_stable = '1'
      report "9) Wrong pps_stable"
      severity ERROR;
    assert capture_cnt = 1000
      report "9) Wrong capture_cnt"
      severity ERROR;
    -- 10
    proc_common_wait_some_cycles(st_clk, c_pps_period / 10);
    assert offset_cnt = last_offset_cnt
      report "10) Wrong offset_cnt"
      severity ERROR;
    -- 11
    proc_common_wait_some_cycles(st_clk, c_pps_period / 10);
    assert offset_cnt = last_offset_cnt
      report "11) Wrong offset_cnt"
      severity ERROR;
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- DUT: PPSH
  -----------------------------------------------------------------------------

  dut : entity work.mms_ppsh
  generic map (
    g_st_clk_freq    => c_st_clk_freq
  )
  port map (
    -- Clocks and reset
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    st_rst           => st_rst,
    st_clk           => st_clk,
    pps_ext          => pps_ext,

    -- Memory-mapped clock domain
    reg_mosi         => reg_mosi,
    reg_miso         => reg_miso,

    -- Streaming clock domain
    pps_sys          => pps_sys
  );
end tb;
