-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

-- Purpose: Provide visual activity information via a LED.
-- Description:
--   ctrl_on = '0' : then led = ctrl_input, so completely driven by external control
--   ctrl_on = '1' : then led = '1' but pulses '0' for g_nof_ms each time that a ctrl_evt clk pulse occurs
-- Remark:
--   The p_state machine ensures that after g_nof_ms off the led also stays on
--   for at least g_nof_ms, to avoid that a too fast ctrl_evt rate would cause
--   the led too stay off. Therefore the maximum event rate that can be
--   signalled is 1/(2*g_nof_ms). If events occur faster then these can not be
--   visualized exactly anymore and will get lost.

entity common_led_controller is
  generic (
    g_nof_ms      : natural := 100  -- force LED off for g_nof_ms and then on for at least g_nof_ms
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    pulse_ms      : in  std_logic := '0';  -- pulses every ms, used to time the ctrl_evt effect on the led
    -- led control
    ctrl_on       : in  std_logic := '0';
    ctrl_evt      : in  std_logic := '0';  -- when ctrl_on='1' then the led output is on and pulses off for g_nof_ms when a ctrl_evt='1' event pulse occurs
    ctrl_input    : in  std_logic := '0';  -- when ctrl_on='0' then use ctrl_input to control the led output
    -- led output
    led           : out std_logic
  );
end common_led_controller;

architecture rtl of common_led_controller is
  type t_state is (s_idle, s_off, s_on);

  signal state          : t_state;
  signal nxt_state      : t_state;

  -- Register inputs locally at this input to ease timing closure in case there are multiple common_led_controller all using the same central source
  signal pulse_ms_reg   : std_logic;
  signal ctrl_input_reg : std_logic;

  signal cnt            : natural range 0 to g_nof_ms;
  signal nxt_cnt        : natural;

  signal nxt_led        : std_logic;
begin
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      pulse_ms_reg   <= '0';
      ctrl_input_reg <= '0';
      cnt            <= 0;
      state          <= s_idle;
      led            <= '0';
    elsif rising_edge(clk) then
      pulse_ms_reg   <= pulse_ms;
      ctrl_input_reg <= ctrl_input;
      cnt            <= nxt_cnt;
      state          <= nxt_state;
      led            <= nxt_led;
    end if;
  end process;

  p_state : process(state, ctrl_on, ctrl_evt, ctrl_input_reg, pulse_ms_reg, cnt)
  begin
    if ctrl_on = '0' then
      -- Default behaviour when ctrl_on = '0'
      nxt_cnt   <= 0;
      nxt_state <= s_idle;
      nxt_led   <= ctrl_input_reg;
    else
      -- Pulse led off briefly on event when ctrl_on = '1'
      nxt_cnt   <= cnt;
      nxt_state <= state;
      nxt_led   <= '1';
      case state is
        when s_idle =>
          nxt_cnt <= 0;
          if ctrl_evt = '1' then
            nxt_state <= s_off;
          end if;
        when s_off =>
          nxt_led <= '0';
          if pulse_ms_reg = '1' then
            nxt_cnt <= cnt + 1;
            if cnt = g_nof_ms then
              nxt_cnt <= 0;
              nxt_state <= s_on;
            end if;
          end if;
        when others =>  -- s_on
          if pulse_ms_reg = '1' then
            nxt_cnt <= cnt + 1;
            if cnt = g_nof_ms then
              nxt_cnt <= 0;
              nxt_state <= s_idle;
            end if;
          end if;
      end case;
    end if;
  end process;
end rtl;
