-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: VHDL wrapper for wbs_arbiter.v

library IEEE, technology_lib, tech_fifo_lib, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mm_arbiter is
  generic (
    g_nof_slaves     : natural;
    g_slave_base_arr : t_natural_arr;
    g_slave_high_arr : t_natural_arr
  );
  port (
    mm_clk         : in  std_logic;
    mm_rst         : in  std_logic;

    master_mosi    : in  t_mem_mosi;
    master_miso    : out t_mem_miso;

    slave_mosi_arr : out t_mem_mosi_arr(g_nof_slaves - 1 downto 0);
    slave_miso_arr : in  t_mem_miso_arr(g_nof_slaves - 1 downto 0)
  );
end mm_arbiter;

architecture str of mm_arbiter is
  constant c_slave_base : std_logic_vector(g_nof_slaves * 32 - 1 downto 0) := nat_arr_to_concat_slv(g_slave_base_arr, g_nof_slaves);  -- Concatenated addresses; one for each slave
  constant c_slave_high : std_logic_vector(g_nof_slaves * 32 - 1 downto 0) := nat_arr_to_concat_slv(g_slave_high_arr, g_nof_slaves);  -- Concatenated addresess; one for each slave

  ----------------------------------------------------------------------------
  -- Wishbone Arbiter
  ----------------------------------------------------------------------------
  component wbs_arbiter
  generic(
    NUM_SLAVES : natural;
    SLAVE_ADDR : std_logic_vector;  -- NUM_SLAVES concatenated 32b integers
    SLAVE_HIGH : std_logic_vector;  -- NUM_SLAVES concatenated 32b integers
    TIMEOUT    : natural
  );
  port (
    wb_clk_i     : in  std_logic;
    wb_rst_i     : in  std_logic;

    wbm_cyc_i     : in  std_logic;
    wbm_stb_i     : in  std_logic;
    wbm_we_i      : in  std_logic;
    wbm_sel_i     : in  std_logic_vector(       4 - 1 downto 0);
    wbm_adr_i     : in  std_logic_vector(c_word_w - 1 downto 0);
    wbm_dat_i     : in  std_logic_vector(c_word_w - 1 downto 0);
    wbm_dat_o     : out std_logic_vector(c_word_w - 1 downto 0);
    wbm_ack_o     : out std_logic;
    wbm_err_o     : out std_logic;

    wbs_cyc_o     : out std_logic_vector(g_nof_slaves - 1 downto 0);
    wbs_stb_o     : out std_logic_vector(g_nof_slaves - 1 downto 0);
    wbs_we_o      : out std_logic;
    wbs_sel_o     : out std_logic_vector(       4 - 1 downto 0);
    wbs_adr_o     : out std_logic_vector(c_word_w - 1 downto 0);
    wbs_dat_o     : out std_logic_vector(c_word_w - 1 downto 0);
    wbs_dat_i     : in  std_logic_vector(g_nof_slaves * c_word_w - 1 downto 0);
    wbs_ack_i     : in  std_logic_vector(g_nof_slaves - 1 downto 0)
  );
  end component;
--  input  wb_clk_i, wb_rst_i;
--
--  input  wbm_cyc_i;
--  input  wbm_stb_i;
--  input  wbm_we_i;
--  input   [3:0] wbm_sel_i;
--  input  [31:0] wbm_adr_i;
--  input  [31:0] wbm_dat_i;
--  output [31:0] wbm_dat_o;
--  output wbm_ack_o;
--  output wbm_err_o;
--
--  output [NUM_SLAVES - 1:0] wbs_cyc_o;
--  output [NUM_SLAVES - 1:0] wbs_stb_o;
--  output wbs_we_o;
--  output  [3:0] wbs_sel_o;
--  output [31:0] wbs_adr_o;
--  output [31:0] wbs_dat_o;
--  input  [NUM_SLAVES*32 - 1:0] wbs_dat_i;
--  input  [NUM_SLAVES - 1:0] wbs_ack_i;

  ----------------------------------------------------------------------------
  -- wbs_arbiter i/o signals
  ----------------------------------------------------------------------------
  signal wbm_cyc_i : std_logic;
  signal wbm_stb_i : std_logic;
  signal wbm_we_i  : std_logic;
  signal wbm_sel_i : std_logic_vector(       4 - 1 downto 0);
  signal wbm_adr_i : std_logic_vector(c_word_w - 1 downto 0);
  signal wbm_dat_i : std_logic_vector(c_word_w - 1 downto 0);
  signal wbm_dat_o : std_logic_vector(c_word_w - 1 downto 0);
  signal wbm_ack_o : std_logic;
  signal wbm_err_o : std_logic;

  signal wbs_cyc_o : std_logic_vector(g_nof_slaves - 1 downto 0);
  signal wbs_stb_o : std_logic_vector(g_nof_slaves - 1 downto 0);
  signal wbs_we_o  : std_logic;
  signal wbs_sel_o : std_logic_vector(       4 - 1 downto 0);
  signal wbs_adr_o : std_logic_vector(c_word_w - 1 downto 0);
  signal wbs_dat_o : std_logic_vector(c_word_w - 1 downto 0);
  signal wbs_dat_i : std_logic_vector(g_nof_slaves * c_word_w - 1 downto 0);
  signal wbs_ack_i : std_logic_vector(g_nof_slaves - 1 downto 0);

  ----------------------------------------------------------------------------
  -- Arrayed SLV versions of the wbs_arbiter slave side i/o signals
  ----------------------------------------------------------------------------
  type t_wb_sl_arr    is array(g_nof_slaves - 1 downto 0) of std_logic;
  type t_wb_slv4_arr  is array(g_nof_slaves - 1 downto 0) of std_logic_vector(4 - 1 downto 0);
  type t_wb_slv32_arr is array(g_nof_slaves - 1 downto 0) of std_logic_vector(32 - 1 downto 0);

  signal wbs_cyc_o_arr : t_wb_sl_arr;
  signal wbs_stb_o_arr : t_wb_sl_arr;
  signal wbs_we_o_arr  : t_wb_sl_arr;
  signal wbs_sel_o_arr : t_wb_slv4_arr;
  signal wbs_adr_o_arr : t_wb_slv32_arr;
  signal wbs_dat_o_arr : t_wb_slv32_arr;
  signal wbs_dat_i_arr : t_wb_slv32_arr;
  signal wbs_ack_i_arr : t_wb_sl_arr;

  ----------------------------------------------------------------------------
  -- Arrayed bus versions of the wbs_arbiter slave side i/o signals
  ----------------------------------------------------------------------------
  type t_wb_mosi is record  -- Master Out Slave In
    cyc : std_logic;
    stb : std_logic;
    we  : std_logic;
    sel : std_logic_vector(       4 - 1 downto 0);
    adr : std_logic_vector(c_word_w - 1 downto 0);
    dat : std_logic_vector(c_word_w - 1 downto 0);
  end record;

  type t_wb_miso is record  -- Master In Slave Out
    dat : std_logic_vector(c_word_w - 1 downto 0);
    ack : std_logic;
--    err : STD_LOGIC;
  end record;

  type t_wb_miso_arr is array (integer range <>) of t_wb_miso;
  type t_wb_mosi_arr is array (integer range <>) of t_wb_mosi;

  signal wbs_mosi_arr : t_wb_mosi_arr(g_nof_slaves - 1 downto 0);
  signal wbs_miso_arr : t_wb_miso_arr(g_nof_slaves - 1 downto 0);
begin
--  wbs_arbiter #(
--    .NUM_SLAVES (N_WB_SLAVES),
--    .SLAVE_ADDR (SLAVE_BASE),
--    .SLAVE_HIGH (SLAVE_HIGH),
--    .TIMEOUT    (1024)
--  ) wbs_arbiter_inst (
--    .wb_clk_i  (wb_clk_i), //mm_clk
--    .wb_rst_i  (wb_rst_i), //mm_rst
--
--    .wbm_cyc_i (wbm_cyc_o),//
--    .wbm_stb_i (wbm_stb_o),//
--    .wbm_we_i  (wbm_we_o), //master_mosi.wr
--    .wbm_sel_i (wbm_sel_o),//byte select, one hot.
--    .wbm_adr_i (wbm_adr_o),//master_mosi.address
--    .wbm_dat_i (wbm_dat_o),//master_miso.data
--    .wbm_dat_o (wbm_dat_i),//master_mosi.data
--    .wbm_ack_o (wbm_ack_i),//master_miso.rdval
--    .wbm_err_o (wbm_err_i),//
--
--    .wbs_cyc_o (wbs_cyc_o),//
--    .wbs_stb_o (wbs_stb_o),//
--    .wbs_we_o  (wbs_we_o), //Write not read.
--    .wbs_sel_o (wbs_sel_o),//Byte select, one hot.
--    .wbs_adr_o (wbs_adr_o),//slave_mosi.address
--    .wbs_dat_o (wbs_dat_o),//slave_mosi.data
--    .wbs_dat_i (wbs_dat_i),//slave_miso.data
--    .wbs_ack_i (wbs_ack_i) //slave_miso.rdval
--  );

  ----------------------------------------------------------------------------
  -- Wire wbs_arbiter master side i/o to WB SLV array types
  ----------------------------------------------------------------------------
  wbm_cyc_i <= master_mosi.wr or master_mosi.rd;
  wbm_stb_i <= master_mosi.wr or master_mosi.rd;
  wbm_we_i  <= master_mosi.wr;  -- Low = implicit read
  wbm_sel_i <= (others => '1');
  wbm_adr_i <= master_mosi.address(c_word_w - 1 downto 0);
  wbm_dat_i <= master_mosi.wrdata(c_word_w - 1 downto 0);

  master_miso.rddata(c_word_w - 1 downto 0) <= wbm_dat_o;
  master_miso.rdval <= wbm_ack_o;
  -- NC <= wbm_err_o;

  ----------------------------------------------------------------------------
  -- Wire wbs_arbiter slave side i/o to WB SLV array types
  ----------------------------------------------------------------------------
  gen_slv_arrays : for i in 0 to g_nof_slaves - 1 generate
    -- wbs_arbiter slave side outputs
    wbs_cyc_o_arr(i) <= wbs_cyc_o(i);
    wbs_stb_o_arr(i) <= wbs_stb_o(i);
    wbs_we_o_arr(i)  <= wbs_we_o;
    wbs_sel_o_arr(i) <= wbs_sel_o;
    wbs_adr_o_arr(i) <= wbs_adr_o;
    wbs_dat_o_arr(i) <= wbs_dat_o;
    -- wbs_arbiter slave side inputs
    wbs_dat_i_arr(i) <= wbs_dat_i(i * c_word_w + c_word_w - 1 downto i * c_word_w);
    wbs_ack_i_arr(i) <= wbs_ack_i(i);
  end generate;

  ----------------------------------------------------------------------------
  -- Wire slave side WB SLV array types to WB bus array types
  ----------------------------------------------------------------------------
  gen_wb_arrays : for i in 0 to g_nof_slaves - 1 generate
    -- wbs_arbiter slave side outputs
    wbs_mosi_arr(i).cyc <= wbs_cyc_o_arr(i);
    wbs_mosi_arr(i).stb <= wbs_stb_o_arr(i);
    wbs_mosi_arr(i).we  <= wbs_we_o_arr(i);
    wbs_mosi_arr(i).sel <= wbs_sel_o_arr(i);
    wbs_mosi_arr(i).adr <= wbs_adr_o_arr(i);
    wbs_mosi_arr(i).dat <= wbs_dat_o_arr(i);
    -- wbs_arbiter slave side inputs
    wbs_dat_i_arr(i) <= wbs_miso_arr(i).dat;
    wbs_ack_i_arr(i) <= wbs_miso_arr(i).ack;
  end generate;

  ----------------------------------------------------------------------------
  -- Wire slave side WB bus array types to MM bus array types
  ----------------------------------------------------------------------------
  gen_mm_arrays : for i in 0 to g_nof_slaves - 1 generate
    -- wbs_arbiter slave side outputs
    slave_mosi_arr(i).wrdata( c_word_w - 1 downto 0) <= wbs_mosi_arr(i).dat;
    slave_mosi_arr(i).address(c_word_w - 1 downto 0) <= wbs_mosi_arr(i).adr;
    slave_mosi_arr(i).wr                           <=     wbs_mosi_arr(i).we  and wbs_mosi_arr(i).cyc and wbs_mosi_arr(i).stb;
    slave_mosi_arr(i).rd                           <= not(wbs_mosi_arr(i).we) and wbs_mosi_arr(i).cyc and wbs_mosi_arr(i).stb;
    -- wbs_arbiter slave side inputs
    wbs_miso_arr(i).dat <= slave_miso_arr(i).rddata(c_word_w - 1 downto 0);
    wbs_miso_arr(i).ack <= slave_miso_arr(i).rdval;
  end generate;

  ----------------------------------------------------------------------------
  -- Wishbone Arbiter
  -- . Example parameters:
  --
  ----------------------------------------------------------------------------
  u_wbs_arbiter :  wbs_arbiter
  generic map (
    NUM_SLAVES => g_nof_slaves,
    SLAVE_ADDR => c_slave_base,
    SLAVE_HIGH => c_slave_high,
    TIMEOUT    => 1024
  )
  port map (
    wb_clk_i  => mm_clk,
    wb_rst_i  => mm_rst,

    wbm_cyc_i => wbm_cyc_i,
    wbm_stb_i => wbm_stb_i,
    wbm_we_i  => wbm_we_i,
    wbm_sel_i => wbm_sel_i,
    wbm_adr_i => wbm_adr_i,
    wbm_dat_i => wbm_dat_i,
    wbm_dat_o => wbm_dat_o,
    wbm_ack_o => wbm_ack_o,
    wbm_err_o => wbm_err_o,

    wbs_cyc_o => wbs_cyc_o,
    wbs_stb_o => wbs_stb_o,
    wbs_we_o  => wbs_we_o,
    wbs_sel_o => wbs_sel_o,
    wbs_adr_o => wbs_adr_o,
    wbs_dat_o => wbs_dat_o,
    wbs_dat_i => wbs_dat_i,
    wbs_ack_i => wbs_ack_i
  );
end str;
