-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose: Requantize the input data to the output data width by removing
--          LSbits and/or MSbits
-- Description:
--
-- . in_dat --> remove LSbits --> rem_dat --> remove MSbits --> shift left by c_gain_w --> out_dat
--
-- . Remove LSBits by means of ROUND or TRUNCATE
-- . Remove LSBits when c_lsb_w>0
--
-- . Remove MSBits by means of CLIP or WRAP
-- . Remove MSbits when g_in_dat_w-c_lsb_w > g_out_dat_w:
--     in_dat   <---------------------g_in_dat_w--------------------->
--     rem_dat  <---------------------c_rem_dat_w-------><--c_lsb_w-->
--     res_dat          <-------------g_out_dat_w-------><--c_lsb_w-->
--
-- . Extend MSbits when g_in_dat_w-c_lsb_w <= g_out_dat_w::
--     in_dat           <-------------g_in_dat_w--------------------->
--     rem_dat          <-------------c_rem_dat_w-------><--c_lsb_w-->
--     res_dat  <---------------------g_out_dat_w-------><--c_lsb_w-->
--
-- . Shift left res_dat before resizing to out_dat'LENGTH, which is useful to keep the res_dat in the MSbits when out_dat'LENGTH > g_out_dat_w
--     gain_dat <-------g_out_dat_w-------><--c_gain_w-->
--
-- Remarks:
-- . It is not necessary to define g_msb_w, because the number of MSbits that
--   need to be removed (or extended) follows from the other widths.

entity common_requantize is
  generic (
    g_representation      : string  := "SIGNED";  -- SIGNED (round +-0.5 away from zero to +- infinity) or UNSIGNED rounding (round 0.5 up to + inifinity)
    g_lsb_w               : integer := 4;  -- when > 0, number of LSbits to remove from in_dat
                                                  -- when < 0, number of LSBits to insert as a gain before resize to out_dat'LENGTH
                                                  -- when 0 then no effect
    g_lsb_round           : boolean := true;  -- when TRUE round else truncate the input LSbits
    g_lsb_round_clip      : boolean := false;  -- when TRUE round clip to +max to avoid wrapping to output -min (signed) or 0 (unsigned) due to rounding
    g_lsb_round_even      : boolean := true;  -- when TRUE round half to even, else round half away from zero
    g_msb_clip            : boolean := true;  -- when TRUE CLIP else WRAP the input MSbits
    g_msb_clip_symmetric  : boolean := false;  -- when TRUE clip signed symmetric to +c_smax and -c_smax, else to +c_smax and c_smin_symm
                                                  -- for wrapping when g_msb_clip=FALSE the g_msb_clip_symmetric is ignored, so signed wrapping is done asymmetric
    g_gain_w              : natural := 0;  -- do not use, must be 0, use negative g_lsb_w instead
    g_pipeline_remove_lsb : natural := 0;  -- >= 0
    g_pipeline_remove_msb : natural := 0;  -- >= 0, use g_pipeline_remove_lsb=0 and g_pipeline_remove_msb=0 for combinatorial output
    g_in_dat_w            : natural := 36;  -- input data width
    g_out_dat_w           : natural := 18  -- output data width
  );
  port (
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_dat     : in  std_logic_vector;  -- unconstrained slv to also support widths other than g_in_dat_w by only using [g_in_dat_w-1:0] from the in_dat slv
    out_dat    : out std_logic_vector;  -- unconstrained slv to also support widths other then g_out_dat_w by resizing the result [g_out_dat_w-1:0] to the out_dat slv
    out_ovr    : out std_logic  -- out_ovr is '1' when the removal of MSbits causes clipping or wrapping
  );
end;

architecture str of common_requantize is
  -- Use c_lsb_w > 0 to remove LSBits and support c_lsb < 0 to shift in zero value LSbits as a gain
  constant c_lsb_w        : natural := sel_a_b(g_lsb_w > 0,  g_lsb_w, 0);
  constant c_gain_w       : natural := sel_a_b(g_lsb_w < 0, -g_lsb_w, 0);

  constant c_rem_dat_w    : natural := g_in_dat_w - c_lsb_w;

  signal rem_dat       : std_logic_vector(c_rem_dat_w - 1 downto 0);  -- remaining in_dat after removing the c_lsb_w number of LSBits
  signal res_dat       : std_logic_vector(g_out_dat_w - 1 downto 0);  -- resulting out_dat after removing the g_msb_w number of MSBits

  signal gain_dat      : std_logic_vector(g_out_dat_w + c_gain_w - 1 downto 0) := (others => '0');  -- fill extra LSBits with '0' instead of extending MSbits
begin
  assert g_gain_w = 0
    report "common_requantize: must use g_gain_w = 0, because gain is now supported via negative g_lsb_w."
    severity FAILURE;

  -- Remove LSBits using ROUND or TRUNCATE
  u_remove_lsb : entity common_lib.common_round
  generic map (
    g_representation  => g_representation,
    g_round           => g_lsb_round,
    g_round_clip      => g_lsb_round_clip,
    g_round_even      => g_lsb_round_even,
    g_pipeline_input  => 0,
    g_pipeline_output => g_pipeline_remove_lsb,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => c_rem_dat_w
  )
  port map (
    clk        => clk,
    clken      => clken,
    in_dat     => in_dat(g_in_dat_w - 1 downto 0),
    out_dat    => rem_dat
  );

  -- Remove MSBits using CLIP or WRAP
  u_remove_msb : entity common_lib.common_resize
  generic map (
    g_representation  => g_representation,
    g_pipeline_input  => 0,
    g_pipeline_output => g_pipeline_remove_msb,
    g_clip            => g_msb_clip,
    g_clip_symmetric  => g_msb_clip_symmetric,
    g_in_dat_w        => c_rem_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk        => clk,
    clken      => clken,
    in_dat     => rem_dat,
    out_dat    => res_dat,
    out_ovr    => out_ovr
  );

  -- Output gain
  gain_dat(g_out_dat_w + c_gain_w - 1 downto c_gain_w) <= res_dat;

  out_dat <= RESIZE_SVEC(gain_dat, out_dat'length) when g_representation = "SIGNED" else RESIZE_UVEC(gain_dat, out_dat'length);
end str;
