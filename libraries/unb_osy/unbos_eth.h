/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */
#ifndef UNBOS_ETH_H
#define UNBOS_ETH_H

/* includes */
#include <system.h>
#include <unbosx_types.h>
#include <unbosx_eth_regs.h>
#include <unbosx_util.h>

/* We ABSOLUTELY need the AVS_ETH thingamabob so we're not going to
 * conditionally compile this file */

// Start address of the rx and tx frame in the frame buffer
#define ETH_RX_RAM_BASE                0
#define ETH_TX_RAM_BASE                (AVS_ETH_0_MMS_RAM_SPAN/2)
#define ETH_FRAME_LENGTH               9018
#define UNB_ETH_SRC_MAC_BASE           ((TUInt32)0x00228608)
#define UNB_ETH_SRC_MAC_BASE_HI        ((UNB_ETH_SRC_MAC_BASE >> 16) & 0xFFFF)
#define UNB_ETH_SRC_MAC_BASE_LO        ( UNB_ETH_SRC_MAC_BASE & 0xFFFF)

#if 0
/* translate an ip number to ASCII 'dotted quad' format */
char const* NET_inet_ntoa(const TUInt32 ipv4addr);
#endif

/* Initialize the ETH hardware and this module */
void UNBOSX_NET_Setup(void);

/* send nbyte from TX buffer to network.
 * it is YOUR responsibility to make sure there is 
 * a valid packet there
 *
 * NOTE: you HAVE to take into account that the 
 *       first two bytes in the TX buffer are padding bytes
 *       so the amount of bytes you wish to send should
 *       be 2 larger than you actually think
 */
void         UNBOSX_NET_Send(unsigned int nbyte);

/* this lets the main loop decide to not send a reply but
 * discard whatever's in the TX buffer. Reenables RX'ing 
 * packets. TX buffer contents will be lost.
 */
void         UNBOSX_NET_Discard();
/* Uses the WORDS and EMPTY fields in the status to work out how many
 * bytes were received */
//unsigned int NET_BytesReceived(TUInt32 status);

// The AVS ETH has two regs: a control and a status one
// Upon receipt of a packet eth_frmInfo != 0
extern volatile TUInt32   eth_status;
extern volatile TUInt32   eth_frmInfo;

/*
 * Two buffers: one for RX and one for TX
 *    * Remember the actual start of the ethernetheader
 *      (the first header you must write in the
 *       packetbuffer) starts at byte #2. Byte 0 and 1
 *       in the buffer are padding bytes in order to
 *       make, eventually, the IP and/or UDP payloads
 *       start at a 4-byte-aligned address.
 *    * When done, set the member to the full
 *      packetssize + 2 bytes (for the padding, remember).
 */

extern TUInt32 volatile* const rx;
extern TUInt32 volatile* const tx;

// Shorthands for inspecting/extracting bits from the status register
#define TXAVAIL(status) ((status&AVS_ETH_REG_STATUS_TX_AVAIL_BIT_MASK)==AVS_ETH_REG_STATUS_TX_AVAIL_BIT_MASK)
#define RXAVAIL(status) ((status&AVS_ETH_REG_STATUS_RX_AVAIL_BIT_MASK)==AVS_ETH_REG_STATUS_RX_AVAIL_BIT_MASK)
#define NWORD(status)   ((status&AVS_ETH_REG_STATUS_RX_NOF_WORDS_BIT_MASK)>>AVS_ETH_REG_STATUS_RX_NOF_WORDS_BIT_OFST)
#define NEMPTY(status)  ((status&AVS_ETH_REG_STATUS_RX_EMPTY_BIT_MASK)>>AVS_ETH_REG_STATUS_RX_EMPTY_BIT_OFST)

// Shorthands for reading/writing regiters and RAM buffers
#define RDRX(idx)       ((TUInt32)AVS_ETH_RAM_RD(AVS_ETH_0_MMS_RAM_BASE+ETH_RX_RAM_BASE, idx))
#define RDTX(idx)       ((TUInt32)AVS_ETH_RAM_RD(AVS_ETH_0_MMS_RAM_BASE+ETH_TX_RAM_BASE, idx))
#define WRTX(idx, w)    (AVS_ETH_RAM_WR(AVS_ETH_0_MMS_RAM_BASE+ETH_TX_RAM_BASE, idx, w))
#define RDREG(idx)      ((TUInt32)AVS_ETH_REG_RD(AVS_ETH_0_MMS_REG_BASE, idx))
#define WRREG(idx, w)   (AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, idx, w))

// The AVS_ETH module sets flags in the frameInfo register depending on what kind of
// packet is received. Use these shorthands for easy access to them.
#define NET_FLAGCHECK(frameInfo,flag) \
    ((frameInfo&AVS_ETH_REG_FRAME_##flag)==AVS_ETH_REG_FRAME_##flag)
#define NET_CHECK(frameInfo,flag) \
    NET_FLAGCHECK(frameInfo,flag)
#define NET_IS_ARP(frameInfo)        NET_CHECK(frameInfo, IS_ARP_BIT_MASK)
#define NET_IS_IP(frameInfo)         NET_CHECK(frameInfo, IS_IP_BIT_MASK)
#define NET_IPCHECKSUM_OK(frameInfo) NET_CHECK(frameInfo, IP_CHECKSUM_OK_BIT_MASK)
#define NET_IS_ICMP(frameInfo)       NET_CHECK(frameInfo, IS_ICMP_BIT_MASK)
#define NET_IS_UDP(frameInfo)        NET_CHECK(frameInfo, IS_UDP_BIT_MASK)
#define NET_IS_CONTROLPKT(frameInfo) NET_CHECK(frameInfo, IS_UDP_CTRL_PORT_BIT_MASK)

/* depending on what kind of interrupt API we set the type of ISR function pointer we expect */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
typedef void (*isr_func_type)(void*);
#else
typedef void (*isr_func_type)(void*, alt_u32);
#endif

/* Init + install the ISR of your choice */
void UNBOSX_ETH_Init_ISR(isr_func_type isr_fn, void *p_isr_context);

/* Setup the AVS_ETH module with a MAC address and other IP properties */
void UNBOSX_ETH_Setup(TBool   g_sim,
               TUInt32 src_mac_hi, TUInt32 src_mac_lo, TUInt32 frm_length, TUInt32 src_ip, TUInt32 udp_port_ctrl,
               TUInt32 udp_port_data0, TUInt32 udp_port_data1, TUInt32 udp_port_data2);

#endif // UNBOS_ETH_H
