-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Author:
--   E. Kooistra        10 Mar 2017  Created
--
-- Purpose: Testbench for the fringe_stop_unit using MM file IO
-- Description:
--   This testbench verifies:
--   . the MM control and page swaps of fringe_stop_unit.
--   . gaps in the input data valid via g_in_valid_gapsize = 0 or > 0
--
--   The testbench uses a constant phasor input data and the same offset and step
--   setting for all fringe stop channels, to ease the observation in the Wave
--   window.
-- Usage:
-- * VHDL stimuli: edit g_mm_stimuli_enable = TRUE
--   > as 5
--   > run -all
--   The tb is self stopping and self checking.
--   For example observe mm_mosi, mm_miso, out_sosi.re,im in wave window. The
--   out_re (= out_sosi.re), out_im (= out_sosi.im) are integer sinusiods (Wave
--   view: radix= decimal, format=analogue).
--
-- * Python stimuli: edit g_mm_stimuli_enable = FALSE
--   In  Modelsim:
--     > as 5
--     > run 1000 us
--   On command line:
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 99
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 2 -r 4   write 4 in entire offset RAM
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 3 -r 5   write 5 in entire step RAM
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 0        read entire offset RAM
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 1        read entire step RAM
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 4 -r 6,0,1   write 6 in instance 0 at address 1 in offset RAM
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 5 -r 7,0,2   write 7 in instance 0 at address 2 in offset RAM
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 0        read entire offset RAM
--     > python $UPE_GEAR/peripherals/util_fringe_stop.py --sim --unb 0 --fn 0 -n 1        read entire step RAM

library IEEE, common_lib, technology_lib, mm_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tb_mmf_fringe_stop_unit is
  generic(
    g_tb_index                  : natural := 0;  -- use different index to avoid MM file conflict in multi tb
    g_mm_stimuli_enable         : boolean := true;
    --g_mm_stimuli_enable         : BOOLEAN := FALSE;  -- use TRUE to activate p_mm_stimuli, else FALSE to apply mm_file IO via Python
    g_mm_clk_faster_than_dp_clk : boolean := true;  -- when TRUE than mm_clk > dp_clk.
    g_in_valid_gapsize          : natural := 0
  );
end tb_mmf_fringe_stop_unit;

architecture tb of tb_mmf_fringe_stop_unit is
  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_dp_clk_period      : time := 5 ns;
  constant c_mm_clk_period      : time := sel_a_b(g_mm_clk_faster_than_dp_clk, 1 ns, 10 ns);  -- 100 ps makes simulation slow

  -- Fringe stop settings
  constant c_in_dat_w                  : positive := 8;  -- Width of the incoming data.
  constant c_fs_offset_w               : positive := 10;  -- Width of the offset of the linear coefficient
  constant c_fs_step_w                 : positive := 17;  -- Width of the step of the linear coefficient
  constant c_accu_w                    : positive := 31;  -- Width of the accumulation register
  constant c_phi_minus_sign            : boolean := true;  -- Apply exp(j*phi) or exp(-j*phi) based on phasor lookup table address phi
  --CONSTANT c_phi_minus_sign            : BOOLEAN := FALSE;
  constant c_phasor_w                  : positive := 9;  -- Width of the phasor values in the lookup table
  constant c_nof_channels              : positive := 4;  -- Number of serial channels for which the fringe stopping must be applied uniquely
  constant c_nof_pages                 : natural := 2;

  constant c_in_offset                 : natural := 0;  -- range      0:1023   = 0:2**c_fs_offset_w-1
  --CONSTANT c_in_step                   : INTEGER := 65431;  -- range -65536:65535  = -2**(c_fs_step-1):+2**(c_fs_step-1)-1
  constant c_in_step                   : integer := 2**15;
  constant c_in_step2                  : integer := 17;
  constant c_in_step_w                 : integer := ceil_log2(c_in_step);
  constant c_exp_nof_steps_per_period  : integer := 2**(c_accu_w - c_in_step_w) * c_nof_channels;

  -- Input data control
  constant c_in_amplitude              : natural := 2**(c_in_dat_w - 1) - 1;
  constant c_in_block_size             : natural := c_nof_channels;
  constant c_in_block_interval         : natural := c_in_block_size + g_in_valid_gapsize;
  constant c_in_nof_blocks_per_sync    : natural := 1000;
  constant c_in_sync_interval          : natural := c_in_block_interval * c_in_nof_blocks_per_sync;

  constant c_in_nof_sync               : natural := 40;

  -- MM
  constant c_unb_nr                         : natural := 0;
  constant c_node_nr                        : natural := 0;  -- choose node 0 is FN 0
  constant c_cross_clock_domain_delay       : natural := 50;  -- apply in both dp_clk and mm_clk domain, to be independent of which one is fastest
  constant c_mm_file_ram_fringe_stop_offset : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_FRINGE_STOP_OFFSET_" & int_to_str(g_tb_index);
  constant c_mm_file_ram_fringe_stop_step   : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_FRINGE_STOP_STEP_" & int_to_str(g_tb_index);

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  signal tb_state               : string(1 to 14) := "Init          ";
  signal tb_end                 : std_logic := '0';
  signal tb_almost_end          : std_logic := '0';

  -- DUT
  signal ram_fringe_stop_offset_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal ram_fringe_stop_offset_miso : t_mem_miso := c_mem_miso_rst;

  signal ram_fringe_stop_step_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal ram_fringe_stop_step_miso   : t_mem_miso := c_mem_miso_rst;

  signal rd_data                     : std_logic_vector(c_word_w - 1 downto 0);

  signal in_enable                   : std_logic := '0';
  signal in_sosi                     : t_dp_sosi := c_dp_sosi_rst;
  signal out_sosi                    : t_dp_sosi;

  signal out_re                      : integer := 0;
  signal out_re_prev                 : integer := 0;
  signal out_re_max                  : integer := 0;
  signal out_re_min                  : integer := 0;
  signal out_re_sum                  : integer := 0;
  signal out_im                      : integer := 0;
  signal out_im_prev                 : integer := 0;
  signal out_im_max                  : integer := 0;
  signal out_im_min                  : integer := 0;
  signal out_im_sum                  : integer := 0;
  signal verify_out_en               : std_logic := '0';
  signal verify_sum_en               : std_logic := '0';
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ----------------------------------------------------------------------------
  -- Stimuli
  ----------------------------------------------------------------------------

  -- sync, valid
  proc_common_gen_duty_pulse(0,                 1, c_in_sync_interval,  '1', dp_rst, dp_clk, in_enable, in_sosi.sync);
  proc_common_gen_duty_pulse(0,   c_in_block_size, c_in_block_interval, '1', dp_rst, dp_clk, in_enable, in_sosi.valid);
  proc_common_gen_duty_pulse(0,                 1, c_in_block_interval, '1', dp_rst, dp_clk, in_enable, in_sosi.sop);
  proc_common_gen_duty_pulse(c_in_block_size-1, 1, c_in_block_interval, '1', dp_rst, dp_clk, in_enable, in_sosi.eop);  -- DUT uses eop
  -- bsn
  in_sosi.bsn <= INCR_UVEC(in_sosi.bsn, 1) when rising_edge(dp_clk) and in_sosi.eop = '1';

  -- Keep input phasor constant
  in_sosi.re <= TO_DP_DSP_DATA(c_in_amplitude);
  in_sosi.im <= TO_DP_DSP_DATA(0);

  ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  gen_sim_ctrl : if not g_mm_stimuli_enable generate
    mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  p_mm_stimuli : process
    variable v_addr : natural;
    variable v_data : integer;
  begin
    if g_mm_stimuli_enable then
      proc_common_wait_until_low(mm_clk, mm_rst);
      proc_common_wait_until_low(dp_clk, dp_rst);
      proc_common_wait_some_cycles(dp_clk, 10);

      -------------------------------------------------------------------------
      -- Test page swap
      -------------------------------------------------------------------------
      -- enable in_sosi
      in_enable <= '1';

      -- wait until begin of sync interval
      proc_common_wait_until_hi_lo(dp_clk, in_sosi.sync);
      for I in 0 to c_nof_channels - 1 loop
        -- write first page
        mmf_mm_bus_wr(c_mm_file_ram_fringe_stop_offset, I, c_in_offset, mm_clk);
        mmf_mm_bus_wr(c_mm_file_ram_fringe_stop_step, I, c_in_step2, mm_clk);
        -- read back MM page
        mmf_mm_bus_rd(c_mm_file_ram_fringe_stop_offset, I, rd_data, mm_clk);
        assert TO_UINT(rd_data) = c_in_offset
          report "Wrong fringe stop offset readback value"
          severity ERROR;
        mmf_mm_bus_rd(c_mm_file_ram_fringe_stop_step, I, rd_data, mm_clk);
        assert TO_UINT(rd_data) = c_in_step2
          report "Wrong fringe stop step readback value"
          severity ERROR;
      end loop;
       -- write to last address c_nof_channels-1 will cause page swap at in_sosi.sync

      -- wait until begin of sync interval
      proc_common_wait_until_hi_lo(dp_clk, in_sosi.sync);
      proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_delay);
      tb_state <= "1-st page swap";
      -- read back power up default step value to verify page swap
      mmf_mm_bus_rd(c_mm_file_ram_fringe_stop_step, 0, rd_data, mm_clk);
      assert TO_UINT(rd_data) = 0
        report "Wrong power up default step readback value"
        severity ERROR;

      for I in 0 to c_nof_channels - 1 loop
        -- write next page
        mmf_mm_bus_wr(c_mm_file_ram_fringe_stop_offset, I, c_in_offset, mm_clk);
        mmf_mm_bus_wr(c_mm_file_ram_fringe_stop_step, I, c_in_step, mm_clk);
      end loop;
      -- write to last address c_nof_channels-1 will cause page swap at in_sosi.sync

      -- wait until begin of sync interval
      proc_common_wait_until_hi_lo(dp_clk, in_sosi.sync);
      proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_delay);
      tb_state <= "2-nd page swap";
      verify_out_en <= '1';
      -- read back first written step value to verify page swap
      mmf_mm_bus_rd(c_mm_file_ram_fringe_stop_step, 0, rd_data, mm_clk);
      assert TO_UINT(rd_data) = c_in_step2
        report "Wrong first written step readback value"
        severity ERROR;
      -- no write to last address c_nof_channels-1, so no page swap at in_sosi.sync

      -- wait until begin of sync interval
      proc_common_wait_until_hi_lo(dp_clk, in_sosi.sync);
      proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_delay);
      tb_state <= "no page swap  ";
      -- read back second written step value to verify page swap
      mmf_mm_bus_rd(c_mm_file_ram_fringe_stop_step, 0, rd_data, mm_clk);
      assert TO_UINT(rd_data) = c_in_step2
        report "Wrong first written step readback value, unexpected page swap"
        severity ERROR;

      report "MM done " & integer'image(g_tb_index)
        severity NOTE;

      -------------------------------------------------------------------------
      -- Test FS step for one phasor period
      -------------------------------------------------------------------------
      -- Wait for at least one fringe stop phase period
      v_data := out_re;
      proc_common_wait_until_value(v_data + 1, dp_clk, out_re);  -- wait until out_re+1, initial (out_re, out_im) = (-c_in_amplitude, 0)
      report "Phasor start " & integer'image(g_tb_index)
        severity NOTE;
      tb_state <= "Phasor start  ";
      proc_common_wait_until_evt(dp_clk, out_re);  -- wait until out_re+1 changes
      proc_common_wait_until_value(v_data, dp_clk, out_re);  -- wait until out_re again
      proc_common_wait_until_value(v_data + 1, dp_clk, out_re);  -- wait until out_re+1 again
      report "Phasor end " & integer'image(g_tb_index)
        severity NOTE;
      tb_state <= "Phasor end    ";

      -- Wait some more sync intervals
      proc_common_wait_some_cycles(dp_clk, c_in_sync_interval * 3);

      report "tb_almost_end " & integer'image(g_tb_index)
        severity NOTE;
      tb_almost_end <= '1';
      proc_common_wait_some_cycles(dp_clk, 10);
      report "tb_end " & integer'image(g_tb_index)
        severity NOTE;
      tb_end <= '1';
    end if;
    wait;
  end process;

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------

  u_mm_file_ram_fringe_stop_offset : entity mm_lib.mm_file generic map(c_mm_file_ram_fringe_stop_offset) port map(mm_rst, mm_clk, ram_fringe_stop_offset_mosi, ram_fringe_stop_offset_miso);
  u_mm_file_ram_fringe_stop_step   : entity mm_lib.mm_file generic map(c_mm_file_ram_fringe_stop_step)   port map(mm_rst, mm_clk, ram_fringe_stop_step_mosi,   ram_fringe_stop_step_miso);

  u_dut : entity work.fringe_stop_unit
  generic map (
    g_technology     => c_tech_select_default,
    g_in_dat_w       => c_in_dat_w,
    g_fs_offset_w    => c_fs_offset_w,
    g_fs_step_w      => c_fs_step_w,
    g_accu_w         => c_accu_w,
    g_phi_minus_sign => c_phi_minus_sign,
    g_phasor_w       => c_phasor_w,
    g_nof_channels   => c_nof_channels
  )
  port map (
    -- System
    dp_rst                 =>  dp_rst,
    dp_clk                 =>  dp_clk,
    mm_rst                 =>  mm_rst,
    mm_clk                 =>  mm_clk,

    -- MM interface
    ram_fringe_stop_offset_mosi => ram_fringe_stop_offset_mosi,
    ram_fringe_stop_offset_miso => ram_fringe_stop_offset_miso,
    ram_fringe_stop_step_mosi   => ram_fringe_stop_step_mosi,
    ram_fringe_stop_step_miso   => ram_fringe_stop_step_miso,

    -- ST interface
    in_sosi            => in_sosi,
    out_sosi           => out_sosi
  );

  ----------------------------------------------------------------------------
  -- Verify FS output
  ----------------------------------------------------------------------------
  out_re <= TO_SINT(s_round(out_sosi.re, c_phasor_w - c_sign_w));
  out_im <= TO_SINT(s_round(out_sosi.im, c_phasor_w - c_sign_w));

  out_re_max  <= out_re when out_re > out_re_max;
  out_re_min  <= out_re when out_re < out_re_min;
  out_re_prev <= out_re when rising_edge(dp_clk);
  out_re_sum  <= out_re_sum + out_re when rising_edge(dp_clk) and out_sosi.valid = '1' and verify_sum_en = '1';

  out_im_max  <= out_im when out_im > out_im_max;
  out_im_min  <= out_im when out_im < out_im_min;
  out_im_prev <= out_im when rising_edge(dp_clk);
  out_im_sum  <= out_im_sum + out_im when rising_edge(dp_clk) and out_sosi.valid = '1' and verify_sum_en = '1';

  p_verify : process
  begin
    if g_mm_stimuli_enable then
      proc_common_wait_until_high(dp_clk, verify_out_en);
      -- Verify expected initial FS output (c_phi_minus_sign has no influence on initial FS output, because out_im=-out_im=0)
      assert out_re = c_in_amplitude
        report "Wrong first out_re"
        severity ERROR;
      assert out_im = 0
        report "Wrong first out_im"
        severity ERROR;

      -- Assume that output is sinusoid if the output changes smoothly and has reached both -c_in_amplitude and +c_in_amplitude
      -- . Verify that FS output changes smoothly
      while tb_almost_end = '0' loop
        proc_common_wait_some_cycles(dp_clk, 1);
        assert out_re = out_re_prev or out_re = out_re_prev - 1 or out_re = out_re_prev + 1
          report "Too large change in out_re"
          severity ERROR;
        assert out_im = out_im_prev or out_im = out_im_prev - 1 or out_im = out_im_prev + 1
          report "Too large change in out_im"
          severity ERROR;
      end loop;
      -- . Verify that output has reached +-c_in_amplitude
      assert out_re_max = c_in_amplitude
        report "Wrong out_re_max"
        severity ERROR;
      assert out_im_max = c_in_amplitude
        report "Wrong out_im_max"
        severity ERROR;
      assert out_re_min = -c_in_amplitude
        report "Wrong out_re_min"
        severity ERROR;
      assert out_im_min = -c_in_amplitude
        report "Wrong out_im_min"
        severity ERROR;
    end if;
    wait;
 end process;

  p_verify_dc : process
    variable vBegin  : natural;
    variable vEnd    : natural;
    variable vPeriod : natural;
  begin
    -- Only verify DC when it should be exactly 0 and for the easy case of no valid gap size
    if c_in_step = 2**ceil_log2(c_in_step) and g_in_valid_gapsize = 0 then
      proc_common_wait_until_high(dp_clk, verify_out_en);
      -- Measure
      verify_sum_en <= '0';
      -- Initial (out_re, out_im) = (-c_in_amplitude, 0)
      proc_common_wait_until_value(-c_in_amplitude, dp_clk, out_re);  -- wait until out_re=-c_in_amplitude
      proc_common_wait_until_value(-c_in_amplitude+1, dp_clk, out_re);  -- wait until out_re=-c_in_amplitude+1
      proc_common_wait_until_evt(dp_clk, out_re);  -- wait until out_re changes
      vBegin := NOW / c_dp_clk_period;
      verify_sum_en <= '1';
      proc_common_wait_until_value(-c_in_amplitude, dp_clk, out_re);  -- wait until out_re=-c_in_amplitude again
      proc_common_wait_until_value(-c_in_amplitude+1, dp_clk, out_re);  -- wait until out_re=-c_in_amplitude+1 again
      proc_common_wait_until_evt(dp_clk, out_re);  -- wait until out_re changes
      vEnd := NOW / c_dp_clk_period;
      vPeriod := vEnd - vBegin;
      verify_sum_en <= '0';

      -- Report
      proc_common_wait_some_cycles(dp_clk, 10);
      assert out_re_sum = 0
        report "Fringe stop real sum must have DC = 0 over one period."
        severity ERROR;
      assert out_im_sum = 0
        report "Fringe stop imag sum must have DC = 0 over one period."
        severity ERROR;
      assert vPeriod = c_exp_nof_steps_per_period
        report "Unexpected fringe stop period " & int_to_str(vPeriod) & " /= " & int_to_str(c_exp_nof_steps_per_period)
        severity ERROR;
    end if;
    wait;
  end process;
end tb;
