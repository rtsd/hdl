-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for unb1_test_ddr_MB_I.
-- Description: see tb_unb1_test

library IEEE, unb1_test_lib;
use IEEE.std_logic_1164.all;

entity tb_unb1_test_ddr_MB_I is
end tb_unb1_test_ddr_MB_I;

architecture tb of tb_unb1_test_ddr_MB_I is
begin
  u_tb_unb1_test : entity unb1_test_lib.tb_unb1_test
  generic map (
    g_design_name => "unb1_test_ddr_MB_I",
    --g_sim_node_nr => 7 -- BN3
    g_sim_node_nr => 0  -- FN0
  );
end tb;
