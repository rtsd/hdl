-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--   Provide the user with an array of streaming interfaces that can
--   be indexed using a global UniBoard number 3..0.
-- Description:
--   As one of the global UniBoard numbers will be itself, the streams with that
--   particaular array index will be looped back (user TX to user RX).
-- Remark:
--   Main differences with unb1_board_terminals_mesh:
--   - UniBoard-indexing instead of node indexing;
--   - One array index (matching board ID) is sourced by hosting node.
--   - Always use the GX in both directions between the BN, so no need for
--     g_use_tx and g_use_rx because they are both TRUE.

library IEEE, common_lib, dp_lib, uth_lib, tr_nonbonded_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.unb1_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_packet_pkg.all;
use uth_lib.uth_pkg.all;

entity unb1_board_terminals_back is
  generic (
    g_sim                     : boolean := false;
    g_sim_level               : natural := 0;
    -- System
    g_nof_bus                 : natural := c_unb1_board_nof_uniboard;  -- = 4 Uniboard in subrack, this UniBoard and 3 other UniBoards, so each UniBoard can be indexed by logical index 3:0
    -- User
    g_usr_use_complex         : boolean := false;  -- when TRUE transport sosi im & re fields via DP data, else transport sosi data via DP data
    g_usr_data_w              : natural := 32;
    g_usr_frame_len           : natural := 20;
    g_usr_nof_streams         : natural := 4;  -- number of user streams per bus
    -- Phy
    g_phy_nof_serial          : natural := 4;  -- up to 4 serial lanes per bus
    g_phy_gx_mbps             : natural := 5000;
    g_phy_rx_fifo_size        : natural := c_bram_m9k_fifo_depth;  -- g_fifos=TRUE in mms_tr_nonbonded, choose to use full BRAM size = 256 for FIFO depth at output from PHY
    -- Tx
    g_tx_input_use_fifo       : boolean := true;  -- Tx input uses FIFO to create slack for inserting DP Packet and Uthernet frame headers
    g_tx_input_fifo_size      : natural := c_bram_m9k_fifo_depth;  -- g_tx_input_use_fifo=TRUE, choose to use full BRAM size = 256 for FIFO depth at input to uth_terminal_tx
    g_tx_input_fifo_fill      : natural := 0;
    -- Rx
    g_rx_output_use_fifo      : boolean := false;  -- Rx output provides FIFOs to ensure that dp_distribute does not get blocked due to substantial backpressure on another output
    g_rx_output_fifo_size     : natural := c_bram_m9k_fifo_depth;  -- g_rx_output_use_fifo, choose to use full BRAM size = 256 for FIFO depth at output of uth_terminal_rx
    g_rx_output_fifo_fill     : natural := 0;
    g_rx_timeout_w            : natural := 0  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive uth_rx snk_in.valid
  );
  port (
    bck_id                 : in  std_logic_vector(c_unb1_board_nof_uniboard_w - 1 downto 0);  -- [7:3]; only [1:0] required to index boards 3:0 in a subrack

    mm_rst                 : in  std_logic;
    mm_clk                 : in  std_logic;
    dp_rst                 : in  std_logic;
    dp_clk                 : in  std_logic;
    tr_clk                 : in  std_logic;
    cal_clk                : in  std_logic;

    -- User interface
    tx_usr_siso_2arr       : out t_unb1_board_back_siso_2arr;
    tx_usr_sosi_2arr       : in  t_unb1_board_back_sosi_2arr := (others => (others => c_dp_sosi_rst));
    rx_usr_siso_2arr       : in  t_unb1_board_back_siso_2arr := (others => (others => c_dp_siso_rdy));
    rx_usr_sosi_2arr       : out t_unb1_board_back_sosi_2arr;

    -- Serial (tr_nonbonded)
    tx_serial_2arr         : out t_unb1_board_back_sl_2arr;
    rx_serial_2arr         : in  t_unb1_board_back_sl_2arr := (others => (others => '0'));

    -- MM Control
    -- . tr_nonbonded
    reg_tr_nonbonded_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_tr_nonbonded_miso  : out t_mem_miso;
    reg_diagnostics_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diagnostics_miso   : out t_mem_miso

  );
end unb1_board_terminals_back;

architecture str of unb1_board_terminals_back is
  -- DP/UTH packet
  constant c_packet_data_w    : natural := g_usr_data_w;  -- = 32, packet data width for DP packet and for UTH packet, must be >= g_usr_data_w

  constant c_nof_bus_logical  : natural := g_nof_bus;  -- Indexing 3:0 so each UniBoard can be indexed by logical index
  constant c_nof_bus_serial   : natural := g_nof_bus - 1;  -- Indexing 2:0, only the 'other' UniBoards can be indexed
  constant c_nof_gx           : natural := c_nof_bus_serial * g_phy_nof_serial;

  -- unb1_board_back_select
  signal tx_sel_siso_2arr  : t_unb1_board_back_siso_2arr;
  signal tx_sel_sosi_2arr  : t_unb1_board_back_sosi_2arr;

  signal rx_sel_siso_2arr  : t_unb1_board_back_siso_2arr;
  signal rx_sel_sosi_2arr  : t_unb1_board_back_sosi_2arr;

  -- unb1_board_back_reorder
  signal tx_term_siso_2arr : t_unb1_board_back_siso_2arr;
  signal tx_term_sosi_2arr : t_unb1_board_back_sosi_2arr;

  signal rx_term_siso_2arr : t_unb1_board_back_siso_2arr;
  signal rx_term_sosi_2arr : t_unb1_board_back_sosi_2arr;

  -- unb1_board_back_terminals
  signal tx_phy_siso_2arr  : t_unb1_board_back_siso_2arr;
  signal tx_phy_sosi_2arr  : t_unb1_board_back_sosi_2arr;
  signal rx_phy_siso_2arr  : t_unb1_board_back_siso_2arr;
  signal rx_phy_sosi_2arr  : t_unb1_board_back_sosi_2arr;

  -- mms_tr_nonbonded
  signal tx_phy_siso_arr   : t_dp_siso_arr(c_nof_gx - 1 downto 0);
  signal tx_phy_sosi_arr   : t_dp_sosi_arr(c_nof_gx - 1 downto 0);
  signal rx_phy_siso_arr   : t_dp_siso_arr(c_nof_gx - 1 downto 0);
  signal rx_phy_sosi_arr   : t_dp_sosi_arr(c_nof_gx - 1 downto 0);

  signal tx_serial_arr     : std_logic_vector(c_nof_gx - 1 downto 0);
  signal rx_serial_arr     : std_logic_vector(c_nof_gx - 1 downto 0);
begin
  u_unb1_board_back_select: entity work.unb1_board_back_select
  port map (
    bck_id           => bck_id,
    clk              => dp_clk,

    -- User side
    tx_usr_sosi_2arr => tx_usr_sosi_2arr,
    tx_usr_siso_2arr => tx_usr_siso_2arr,

    rx_usr_sosi_2arr => rx_usr_sosi_2arr,
    rx_usr_siso_2arr => rx_usr_siso_2arr,

    -- Phy side
    tx_phy_sosi_2arr => tx_sel_sosi_2arr,
    tx_phy_siso_2arr => tx_sel_siso_2arr,

    rx_phy_sosi_2arr => rx_sel_sosi_2arr,
    rx_phy_siso_2arr => rx_sel_siso_2arr
  );

  u_unb1_board_back_reorder : entity work.unb1_board_back_reorder
  port map (
    bck_id           => bck_id,
    clk              => dp_clk,

    -- User side
    tx_usr_sosi_2arr => tx_sel_sosi_2arr,
    tx_usr_siso_2arr => tx_sel_siso_2arr,

    rx_usr_sosi_2arr => rx_sel_sosi_2arr,
    rx_usr_siso_2arr => rx_sel_siso_2arr,

    -- Phy side
    tx_phy_sosi_2arr => tx_term_sosi_2arr,
    tx_phy_siso_2arr => tx_term_siso_2arr,

    rx_phy_sosi_2arr => rx_term_sosi_2arr,
    rx_phy_siso_2arr => rx_term_siso_2arr
  );

  u_unb1_board_back_uth_terminals_bidir : entity work.unb1_board_back_uth_terminals_bidir
  generic map (
    -- User
    g_usr_nof_streams     => g_usr_nof_streams,
    g_usr_use_complex     => g_usr_use_complex,
    g_usr_data_w          => g_usr_data_w,
    g_usr_frame_len       => g_usr_frame_len,
    -- DP/UTH packet
    g_packet_data_w       => c_packet_data_w,
    -- Phy
    g_phy_nof_serial      => g_phy_nof_serial,
    -- Tx
    g_tx_input_use_fifo   => g_tx_input_use_fifo,
    -- Rx
    g_rx_output_use_fifo  => g_rx_output_use_fifo,
    g_rx_timeout_w        => g_rx_timeout_w
  )
  port map (
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,

    -- User
    tx_dp_sosi_2arr  => tx_term_sosi_2arr,
    tx_dp_siso_2arr  => tx_term_siso_2arr,

    rx_dp_sosi_2arr  => rx_term_sosi_2arr,
    rx_dp_siso_2arr  => rx_term_siso_2arr,

    -- Phy
    tx_uth_sosi_2arr => tx_phy_sosi_2arr,
    tx_uth_siso_2arr => tx_phy_siso_2arr,

    rx_uth_sosi_2arr => rx_phy_sosi_2arr,
    rx_uth_siso_2arr => rx_phy_siso_2arr
  );

  ------------------------------------------------------------------------------
  -- GX serial interface level (g_sim_level)
  ------------------------------------------------------------------------------

  -- Map 1-dim array on 2-dim array
  gen_bus : for i in c_nof_bus_serial - 1 downto 0 generate
    gen_lane : for j in g_phy_nof_serial - 1 downto 0 generate
       -- SOSI
       tx_phy_sosi_arr(i * g_phy_nof_serial + j) <= tx_phy_sosi_2arr(i)(j);
       tx_phy_siso_2arr(i)(j)                  <= tx_phy_siso_arr(i * g_phy_nof_serial + j);

       rx_phy_sosi_2arr(i)(j)                  <= rx_phy_sosi_arr(i * g_phy_nof_serial + j);
       rx_phy_siso_arr(i * g_phy_nof_serial + j) <= rx_phy_siso_2arr(i)(j);

       -- Serial
       tx_serial_2arr(i)(j)                  <= tx_serial_arr(i * g_phy_nof_serial + j);
       rx_serial_arr(i * g_phy_nof_serial + j) <= rx_serial_2arr(i)(j);
    end generate;
  end generate;

  u_tr_nonbonded : entity tr_nonbonded_lib.mms_tr_nonbonded
  generic map (
    g_sim           => g_sim,
    g_sim_level     => g_sim_level,
    g_nof_gx        => c_nof_gx,
    g_mbps          => g_phy_gx_mbps,
    g_tx            => true,
    g_rx            => true,
    g_rx_fifo_depth => g_phy_rx_fifo_size
  )
  port map (
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,

    st_rst               => dp_rst,
    st_clk               => dp_clk,

    tr_clk               => tr_clk,
    cal_rec_clk          => cal_clk,

    --Serial data I/O
    tx_dataout           => tx_serial_arr,
    rx_datain            => rx_serial_arr,

    --Streaming I/O
    snk_out_arr          => tx_phy_siso_arr,
    snk_in_arr           => tx_phy_sosi_arr,

    src_in_arr           => rx_phy_siso_arr,
    src_out_arr          => rx_phy_sosi_arr,

    tr_nonbonded_mm_mosi => reg_tr_nonbonded_mosi,
    tr_nonbonded_mm_miso => reg_tr_nonbonded_miso,

    diagnostics_mm_mosi  => reg_diagnostics_mosi,
    diagnostics_mm_miso  => reg_diagnostics_miso
  );
end str;
