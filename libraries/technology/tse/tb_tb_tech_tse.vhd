-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Multi-testbench for tech_tse
-- Description:
--   Verify tech_tse for different data types
-- Usage:
--   > as 3
--   > run -all

library IEEE, technology_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;

entity tb_tb_tech_tse is
end tb_tb_tech_tse;

architecture tb of tb_tb_tech_tse is
  constant c_tech : natural := c_tech_select_default;

  constant c_tb_end_vec : std_logic_vector(15 downto 0) := (others => '1');
  signal   tb_end_vec   : std_logic_vector(15 downto 0) := c_tb_end_vec;  -- sufficiently long to fit all tb instances
  signal   tb_end       : std_logic := '0';
begin
-- g_technology : NATURAL := c_tech_select_default;
-- --   g_data_type = c_tb_tech_tse_data_type_symbols  = 0
-- --   g_data_type = c_tb_tech_tse_data_type_counter  = 1
-- g_data_type  : NATURAL := c_tb_tech_tse_data_type_symbols;
-- g_sim        : BOOLEAN := TRUE;
-- g_sim_level  : NATURAL := 1;    -- 0 = use IP; 1 = use fast serdes model;
-- g_tb_end     : BOOLEAN := TRUE  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation

  u_ip           : entity work.tb_tech_tse generic map (c_tech, c_tb_tech_tse_data_type_symbols, false, 0, false) port map (tb_end_vec(0));
  u_sim_level_0  : entity work.tb_tech_tse generic map (c_tech, c_tb_tech_tse_data_type_symbols,  true, 0, false) port map (tb_end_vec(1));
  u_sim_level_1  : entity work.tb_tech_tse generic map (c_tech, c_tb_tech_tse_data_type_symbols,  true, 1, false) port map (tb_end_vec(2));

  tb_end <= '1' when tb_end_vec = c_tb_end_vec else '0';

  p_tb_end : process
  begin
    wait until tb_end = '1';
    wait for 1 ns;
    report "Multi tb simulation finished."
      severity FAILURE;
    wait;
  end process;
end tb;
