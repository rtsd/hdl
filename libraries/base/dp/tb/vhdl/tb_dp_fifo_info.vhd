-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Test bench for dp_fifo_info
-- Description:
-- . The p_stimuli_st uses proc_dp_gen_block_data to generate g_nof_data_block
--   packets. The stimuli data is delayed by g_data_delay to mis align the ST
--   data from the ST info. The DUT must then realign them.
-- Usage:
-- > as 10
-- > run -all
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_fifo_info is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_pulse;  -- always active, random or pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_pulse;  -- always active, random or pulse flow control
    -- specific
    g_info_use_sync          : boolean := true;
    g_info_use_bsn           : boolean := true;
    g_info_use_channel       : boolean := true;
    g_info_use_empty         : boolean := true;
    g_info_use_error         : boolean := true;
    g_info_fifo_size         : natural := 8;  -- >= ceil_div(g_data_delay, g_data_block_len)
    g_nof_data_block         : natural := 25;
    g_data_block_len         : natural := 10;
    g_data_delay             : natural := 35
  );
end tb_dp_fifo_info;

architecture tb of tb_dp_fifo_info is
  constant c_rl                       : natural := 1;  -- fixed

  constant c_data_w                   : natural := 16;
  constant c_data_max                 : unsigned(c_data_w - 1 downto 0) := (others => '1');
  constant c_data_init                : integer := -1;
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_err_init                 : natural := 247;
  constant c_channel_init             : integer := 5;

  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;
  constant c_sync_period              : natural := 8;
  constant c_sync_offset              : natural := 7;

  signal tb_end                     : std_logic := '0';
  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';
  signal sl1                        : std_logic := '1';

  signal random_0                   : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1                   : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0                    : std_logic;
  signal pulse_1                    : std_logic;
  signal pulse_en                   : std_logic := '1';

  signal stimuli_en                 : std_logic := '1';
  signal stimuli_src_in             : t_dp_siso;
  signal stimuli_src_out            : t_dp_sosi;

  signal dp_pipeline_src_in         : t_dp_siso;
  signal dp_pipeline_src_out        : t_dp_sosi;

  signal fifo_wr_ful                : std_logic;  -- corresponds to the carry bit of usedw when FIFO is full
  signal fifo_usedw                 : std_logic_vector(ceil_log2(g_info_fifo_size) - 1 downto 0);
  signal fifo_rd_emp                : std_logic;

  signal prev_verify_snk_out_ready  : std_logic_vector(0 to c_rl);
  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal prev_verify_snk_in         : t_dp_sosi;

  signal verify_hold_sop            : std_logic := '0';
  signal verify_en_valid            : std_logic := '0';
  signal verify_en_sop              : std_logic := '0';
  signal verify_en_eop              : std_logic := '0';
  signal verify_done                : std_logic := '0';

  signal verify_en_sop_sync         : std_logic := '0';
  signal verify_en_sop_bsn          : std_logic := '0';
  signal verify_en_sop_channel      : std_logic := '0';
  signal verify_en_eop_error        : std_logic := '0';
  signal verify_done_bsn            : std_logic := '0';
  signal verify_done_channel        : std_logic := '0';
  signal verify_done_error          : std_logic := '0';

  signal expected_verify_snk_in     : t_dp_sosi;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  stimuli_en           <= '1'                     when g_flow_control_stimuli = e_active else
                          random_0(random_0'high) when g_flow_control_stimuli = e_random else
                          pulse_0                 when g_flow_control_stimuli = e_pulse;

  verify_snk_out.ready <= '1'                     when g_flow_control_verify = e_active  else
                          random_1(random_1'high) when g_flow_control_verify = e_random  else
                          pulse_1                 when g_flow_control_verify = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli_st : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    -- Adjust initial sosi field values by -1 to compensate for auto increment
    v_sosi.bsn     := INCR_UVEC(c_bsn_init,                    -1);
    v_sosi.channel := INCR_UVEC(TO_DP_CHANNEL(c_channel_init), -1);
    v_sosi.data    := INCR_UVEC(TO_DP_DATA(c_data_init),       -1);
    v_sosi.err     := INCR_UVEC(TO_DP_ERROR(c_err_init),       -1);

    stimuli_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Generate g_nof_data_block
    for I in 0 to g_nof_data_block - 1 loop
      -- Auto increment v_sosi field values for this packet
      v_sosi.bsn     := INCR_UVEC(v_sosi.bsn, 1);
      v_sosi.sync    := sel_a_b((unsigned(v_sosi.bsn) mod c_sync_period) = c_sync_offset, '1', '0');  -- insert sync starting at BSN=c_sync_offset and with period c_sync_period
      v_sosi.channel := INCR_UVEC(v_sosi.channel, 1);
      v_sosi.data    := INCR_UVEC(v_sosi.data, g_data_block_len);
      v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(c_data_w - 1 downto 0));  -- wrap when >= 2**c_data_w
      v_sosi.err     := INCR_UVEC(v_sosi.err, 1);

      -- Send packet
      proc_dp_gen_block_data(c_data_w, TO_UINT(v_sosi.data), g_data_block_len, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn, clk, stimuli_en, stimuli_src_in, stimuli_src_out);
    end loop;

    -- Determine expected sosi field values after end of stimuli
    v_sosi.bsn     := std_logic_vector(unsigned(c_bsn_init) + g_nof_data_block - 1);
    v_sosi.channel := TO_DP_CHANNEL(c_channel_init          + g_nof_data_block - 1);
    v_sosi.err     := TO_DP_ERROR(c_err_init                + g_nof_data_block - 1);
    -- . account for g_data_block_len
    v_sosi.data    := INCR_UVEC(v_sosi.data, g_data_block_len - 1);
    v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(c_data_w - 1 downto 0));  -- wrap when >= 2**c_data_w
    expected_verify_snk_in <= v_sosi;

    -- Signal end of stimuli
    proc_common_wait_some_cycles(clk, 500);  -- latency from stimuli to verify depends on the flow control, so wait sufficiently long for last packet to have passed through
    proc_common_gen_pulse(clk, verify_done);
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  -- Stimuli data delay
  u_dp_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => g_data_delay
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => stimuli_src_in,
    snk_in       => stimuli_src_out,
    -- ST source
    src_in       => dp_pipeline_src_in,
    src_out      => dp_pipeline_src_out
  );

  ------------------------------------------------------------------------------
  -- DUT dp_fifo_info
  ------------------------------------------------------------------------------
  dut : entity work.dp_fifo_info
  generic map (
    g_use_sync       => g_info_use_sync,
    g_use_bsn        => g_info_use_bsn,
    g_use_channel    => g_info_use_channel,
    g_use_empty      => g_info_use_empty,
    g_use_error      => g_info_use_error,
    g_bsn_w          => c_dp_stream_bsn_w,
    g_empty_w        => c_dp_stream_empty_w,
    g_channel_w      => c_dp_stream_channel_w,
    g_error_w        => c_dp_stream_error_w,
    g_fifo_size      => g_info_fifo_size
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- Monitor info FIFO filling
    fifo_wr_ful  => fifo_wr_ful,
    fifo_usedw   => fifo_usedw,
    fifo_rd_emp  => fifo_rd_emp,
    -- ST sink
    data_snk_out => dp_pipeline_src_in,
    data_snk_in  => dp_pipeline_src_out,  -- delayed snk_in data
    info_snk_in  => stimuli_src_out,  -- original snk_in info
    -- ST source
    src_in       => verify_snk_out,
    src_out      => verify_snk_in
  );

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Start verify after first valid, sop or eop
  verify_en_valid <= '1' when verify_snk_in.valid = '1' and rising_edge(clk);
  verify_en_sop   <= '1' when verify_snk_in.sop = '1'   and rising_edge(clk);
  verify_en_eop   <= '1' when verify_snk_in.eop = '1'   and rising_edge(clk);

  -- Verify that the stimuli have been applied at all
  verify_done_bsn     <= '1' when verify_done = '1' and g_info_use_bsn    = true else '0';
  verify_done_channel <= '1' when verify_done = '1' and g_info_use_channel = true else '0';
  verify_done_error   <= '1' when verify_done = '1' and g_info_use_error  = true else '0';

  proc_dp_verify_value("verify_snk_in.valid",            clk, verify_done, sl1, verify_en_valid);
  proc_dp_verify_value("verify_snk_in.sop",              clk, verify_done, sl1, verify_en_sop);
  proc_dp_verify_value("verify_snk_in.eop",              clk, verify_done, sl1, verify_en_eop);
  proc_dp_verify_value("verify_snk_in.data",    e_equal, clk, verify_done,         expected_verify_snk_in.data,    verify_snk_in.data);
  proc_dp_verify_value("verify_snk_in.bsn",     e_equal, clk, verify_done_bsn,     expected_verify_snk_in.bsn,     verify_snk_in.bsn);
  proc_dp_verify_value("verify_snk_in.channel", e_equal, clk, verify_done_channel, expected_verify_snk_in.channel, verify_snk_in.channel);
  proc_dp_verify_value("verify_snk_in.err",     e_equal, clk, verify_done_error,   expected_verify_snk_in.err,     verify_snk_in.err);

  -- Verify that the output is incrementing data, like the input stimuli
  verify_en_sop_sync    <= '1' when verify_en_sop = '1' and g_info_use_sync   = true and g_info_use_bsn = true else '0';
  verify_en_sop_bsn     <= '1' when verify_en_sop = '1' and g_info_use_bsn    = true else '0';
  verify_en_sop_channel <= '1' when verify_en_sop = '1' and g_info_use_channel = true else '0';
  verify_en_eop_error   <= '1' when verify_en_eop = '1' and g_info_use_error  = true else '0';

  proc_dp_verify_data("verify_snk_in.data",    c_rl, clk, verify_en_valid,       verify_snk_out.ready, verify_snk_in.valid, verify_snk_in.data,    prev_verify_snk_in.data);
  proc_dp_verify_data("verify_snk_in.bsn",     c_rl, clk, verify_en_sop_bsn,     verify_snk_out.ready, verify_snk_in.sop,   verify_snk_in.bsn,     prev_verify_snk_in.bsn);
  proc_dp_verify_data("verify_snk_in.channel", c_rl, clk, verify_en_sop_channel, verify_snk_out.ready, verify_snk_in.sop,   verify_snk_in.channel, prev_verify_snk_in.channel);
  proc_dp_verify_data("verify_snk_in.err",     c_rl, clk, verify_en_eop_error,   verify_snk_out.ready, verify_snk_in.eop,   verify_snk_in.err,     prev_verify_snk_in.err);

  -- Verify that the output sync occurs when expected
  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_en_sop_sync, verify_snk_in.sync, verify_snk_in.sop, verify_snk_in.bsn);

  -- Verify output packet ctrl
  proc_dp_verify_sop_and_eop(c_rl, clk, verify_snk_out.ready, verify_snk_in.valid, verify_snk_in.sop, verify_snk_in.eop, verify_hold_sop);

  -- Verify output ready latency
  proc_dp_verify_valid(c_rl, clk, verify_en_valid, verify_snk_out.ready, prev_verify_snk_out_ready, verify_snk_in.valid);
end tb;
