-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: generate PING response packet if a PING request is received.
-- Description: A response header is derived from the incoming request header.
-- . The payload of the response is created by pipelining the request packet.
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, technology_lib, tech_mac_10g_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;

entity nw_ping_response is
  generic (
    g_technology : natural := c_tech_select_default
  );
  port (
    clk     : in std_logic;
    rst     : in std_logic;

    snk_in  : in  t_dp_sosi := c_dp_sosi_rst;

    src_in  : in  t_dp_siso := c_dp_siso_rdy;
    src_out : out t_dp_sosi := c_dp_sosi_rst;

    eth_src_mac : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)
  );
end nw_ping_response;

architecture rtl of nw_ping_response is
  constant c_data_w          : natural := c_xgmii_data_w;
  constant c_dp_fifo_empty_w : natural := c_tech_mac_10g_empty_w;
  constant c_dp_fifo_size    : natural := 16;

  constant c_cin_w : natural := 4;
  constant c_pipeline : natural := c_network_total_header_64b_nof_words + 5;  -- Header length and 5 more pipeline cycles to allow for the other states

  type t_state is (s_idle, s_capture, s_check, s_sum, s_output, s_wait);

  type t_reg is record
    state         : t_state;
    word_cnt      : natural range 0 to c_network_total_header_64b_nof_words;
    hdr_words_arr : t_network_total_header_64b_arr;
    hdr_fields    : t_network_total_header;
    hdr_response  : t_network_total_header_64b_arr;
    ip_checksum   : std_logic_vector(c_halfword_w - 1 downto 0);
    icmp_checksum : std_logic_vector(c_halfword_w - 1 downto 0);
    ip_sum        : unsigned(c_halfword_w + c_cin_w - 1 downto 0);
    src_out       : t_dp_sosi;
  end record;

  signal r, rin : t_reg;

  constant c_r_rst : t_reg := ( state => s_idle,
                                word_cnt => 0,
                                hdr_words_arr => (others => (others => '0')),
                                hdr_fields => c_network_total_header_ones,
                                hdr_response =>  (others => (others => '0')),
                                ip_checksum => (others => '0'),
                                icmp_checksum => (others => '0'),
                                ip_sum => (others => '0'),
                                src_out => c_dp_sosi_rst );

  signal dp_pipeline_src_out : t_dp_sosi;
  signal dp_fifo_sc_src_out : t_dp_sosi;
  signal dp_fifo_sc_rd_emp : std_logic;
begin
-- Combinational Process
  p_comb : process(r, rst, snk_in, dp_pipeline_src_out, dp_fifo_sc_rd_emp, eth_src_mac)
    variable v : t_reg;
  begin
    v := r;

    case r.state is

      when s_idle =>  -- Wait for valid SOP
        if snk_in.sop = '1' and snk_in.valid = '1' then
          v.state := s_capture;
          v.hdr_words_arr(r.word_cnt) := snk_in.data(c_data_w - 1 downto 0);
          v.word_cnt := r.word_cnt + 1;
        end if;

      when s_capture =>  -- Capture header
        if snk_in.valid = '1' and r.word_cnt < c_network_total_header_64b_nof_words then
          v.word_cnt := r.word_cnt + 1;
          v.hdr_words_arr(r.word_cnt) := snk_in.data(c_data_w - 1 downto 0);
        elsif snk_in.eop = '1' and snk_in.valid = '1' then
          v := c_r_rst;
        elsif r.word_cnt >= c_network_total_header_64b_nof_words then
          v.word_cnt := 0;
          v.state := s_check;
          v.hdr_fields.eth  := func_network_total_header_no_align_extract_eth(  r.hdr_words_arr);
          v.hdr_fields.ip   := func_network_total_header_no_align_extract_ip(   r.hdr_words_arr);
          v.hdr_fields.icmp := func_network_total_header_no_align_extract_icmp( r.hdr_words_arr);
        end if;

      when s_check =>  -- Check if packet is ICMP echo request, create response header if it is the case.
        if TO_UINT(r.hdr_fields.eth.eth_type) = c_network_eth_type_ip and
           TO_UINT(r.hdr_fields.ip.protocol) = c_network_ip_protocol_icmp and
           TO_UINT(r.hdr_fields.icmp.msg_type) = c_network_icmp_msg_type_request then
          v.hdr_response := func_network_total_header_no_align_response_icmp(r.hdr_words_arr, eth_src_mac);
          v.state := s_sum;
        else
          v := c_r_rst;
        end if;

      when s_sum =>  -- Sum halfwords of headers for checksum
        v.state := s_output;
        v.ip_sum := r.ip_sum
                  + unsigned(r.hdr_response(1)(c_halfword_w - 1 downto 0))  -- ip_version, ip_header_length, ip_services
                  + unsigned(r.hdr_response(2)(c_halfword_w * 4 - 1 downto c_halfword_w * 3))  -- ip_total_length
                  + unsigned(r.hdr_response(2)(c_halfword_w * 3 - 1 downto c_halfword_w * 2))  -- ip_identification
                  + unsigned(r.hdr_response(2)(c_halfword_w * 2 - 1 downto c_halfword_w))  -- ip_flags, ip_fragment_offset
                  + unsigned(r.hdr_response(2)(c_halfword_w   - 1 downto 0))  -- ip_time_to_live, ip_protocol
                  + unsigned(r.hdr_response(3)(c_halfword_w * 3 - 1 downto c_halfword_w * 2))  -- ip_src_addr(1/2)
                  + unsigned(r.hdr_response(3)(c_halfword_w * 2 - 1 downto c_halfword_w))  -- ip_src_addr(2/2)
                  + unsigned(r.hdr_response(3)(c_halfword_w   - 1 downto 0))  -- ip_dst_addr(1/2)
                  + unsigned(r.hdr_response(4)(c_halfword_w * 4 - 1 downto c_halfword_w * 3));  -- ip_dst_addr(2/2)

      when s_output =>  -- Send out ICMP response
        v.src_out := dp_pipeline_src_out;
        if dp_pipeline_src_out.valid = '1' then
          case r.word_cnt is
            when 0 =>
              -- Also perform final checksum calculation
              if dp_pipeline_src_out.sop = '1' then  -- Wait for SOP
                v.src_out.data(c_data_w - 1 downto 0) := r.hdr_response(r.word_cnt);
                v.ip_checksum := not(std_logic_vector(r.ip_sum(c_halfword_w - 1 downto 0) + r.ip_sum(r.ip_sum'high downto c_halfword_w)));  -- checksum = inverted (sum + carry)
                v.icmp_checksum := TO_UVEC((2048 + TO_UINT(r.hdr_fields.icmp.checksum)), c_halfword_w);  -- checksum = original checksum + 0x0800 (incremental update)
                v.word_cnt := r.word_cnt + 1;
              else
                v.src_out := c_dp_sosi_rst;
              end if;
            when 1 to 2 =>
              v.src_out.data(c_data_w - 1 downto 0) := r.hdr_response(r.word_cnt);
              v.word_cnt := r.word_cnt + 1;
            when 3 =>
              v.src_out.data(c_data_w - 1 downto 0) := r.hdr_response(r.word_cnt);
              v.src_out.data(c_halfword_w * 4 - 1 downto c_halfword_w * 3) := r.ip_checksum;
              v.word_cnt := r.word_cnt + 1;
            when 4 =>
              v.src_out.data(c_data_w - 1 downto 0) := r.hdr_response(r.word_cnt);
              v.src_out.data(c_halfword_w * 2 - 1 downto c_halfword_w) := r.icmp_checksum;
              v.word_cnt := r.word_cnt + 1;
            when others =>
              if dp_pipeline_src_out.eop = '1' then
                v.state := s_wait;
              end if;
          end case;
        end if;

      when s_wait =>
            v.src_out := c_dp_sosi_rst;
        if dp_fifo_sc_rd_emp = '1' then  -- Wait until ping response has left the fifo
          v := c_r_rst;
        end if;
    end case;

    if rst = '1' then
      v := c_r_rst;
    end if;

    rin <= v;
  end process;

  -- Sequential process
  p_regs : process(clk)
  begin
    if rising_edge(clk) then
      r <= rin;
    end if;
  end process;

  -- Pipeline packet payload
  u_dp_pipeline : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline => c_pipeline
  )
  port map (
    clk => clk,
    rst => rst,
    snk_in => snk_in,
    src_out => dp_pipeline_src_out
  );

  -- Store response packet until ready
  u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
  generic map (
    g_technology  => g_technology,
    g_data_w      => c_data_w,
    g_empty_w     => c_dp_fifo_empty_w,
    g_use_empty   => true,
    g_fifo_size   => c_dp_fifo_size
  )
  port map (
    rst      => rst,
    clk      => clk,

    rd_emp   => dp_fifo_sc_rd_emp,

    snk_out  => OPEN,
    snk_in   => r.src_out,

    src_in   => src_in,
    src_out  => dp_fifo_sc_src_out
  );
  src_out <= dp_fifo_sc_src_out;
end rtl;
