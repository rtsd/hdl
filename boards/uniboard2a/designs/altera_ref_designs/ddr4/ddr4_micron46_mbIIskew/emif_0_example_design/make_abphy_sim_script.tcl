# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and other 
# software and tools, and its AMPP partner logic functions, and any output 
# files any of the foregoing (including device programming or simulation 
# files), and any associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License Subscription 
# Agreement, Altera MegaCore Function License Agreement, or other applicable 
# license agreement, including, without limitation, that your use is for the 
# sole purpose of programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the applicable 
# agreement for further details.





proc error_and_exit {msg} {
   post_message -type error "SCRIPT_ABORTED!!!"
   foreach line [split $msg "\n"] {
      post_message -type error $line
   }
   qexit -error
}

proc show_usage_and_exit {argv0} {
   post_message -type error  "USAGE: $argv0 \[VERILOG|VHDL\]"
   qexit -error
}

set argv0 "quartus_sh -t [info script]"
set args $quartus(args)

if {[llength $args] == 1 } {
   set lang [string toupper [string trim [lindex $args 0]]]
   if {$lang != "VERILOG" && $lang != "VHDL"} {
      show_usage_and_exit $argv0
   }
} else {
   set lang "VERILOG"
}

if {[llength $args] > 1} {
   show_usage_and_exit $argv0
}

if {[is_project_open]} {
   post_message "Closing currently opened project..."
   project_close
}

set script_path [file dirname [file normalize [info script]]]

source "$script_path/params.tcl"

set ex_design_path         "$script_path/sim"
set system_name            $ed_params(SIM_QSYS_NAME)
set qsys_file              "${system_name}.qsys"
set family                 $ip_params(SYS_INFO_DEVICE_FAMILY)
set device                 $ed_params(DEFAULT_DEVICE)

post_message " "
post_message "*************************************************************************"
post_message "Altera External Memory Interface Abstract Phy Sim Script builder"
post_message " "
post_message "Type    : Simulation Design"
post_message "Family  : $family"
post_message "Language: $lang"
post_message " "
post_message "This script takes ~1 minute to execute..."
post_message "*************************************************************************"
post_message " "

if {!$ip_params(MEM_HAS_SIM_SUPPORT)} {
   error_and_exit "Simulation for this memory interface configuration is not supported by the current IP version."
}

if {[file isdirectory $ex_design_path] == 0} {
   error_and_exit "Directory $ex_design_path does NOT exists.\nThis script should only be run after creation of the simulation design.\n"
}

post_message "Generating abstract phy version of simulation script..."

cd $ex_design_path

set sim_scripts [list]
lappend sim_scripts "${ex_design_path}/synopsys/vcs/vcs_setup.sh"
lappend sim_scripts "${ex_design_path}/mentor/msim_setup.tcl"
lappend sim_scripts "${ex_design_path}/aldec/rivierapro_setup.tcl"
lappend sim_scripts "${ex_design_path}/synopsys/vcsmx/vcsmx_setup.sh"
lappend sim_scripts "${ex_design_path}/cadence/ncsim_setup.sh"

foreach sim_script $sim_scripts {
   if {[file exists $sim_script]} {
      if {[regexp -- {synopsys\/vcs\/} $sim_script]} {
        file copy "${script_path}/altera_emif_check_abphy_nios_forces.tcl" "${ex_design_path}/synopsys/vcs/."
      } elseif {[regexp -- {synopsys\/vcsmx\/} $sim_script]} {
        file copy "${script_path}/altera_emif_check_abphy_nios_forces.tcl" "${ex_design_path}/synopsys/vcsmx/."
      } elseif {[regexp -- {mentor} $sim_script]} {
        file copy "${script_path}/altera_emif_check_abphy_nios_forces.tcl" "${ex_design_path}/mentor/."
      } elseif {[regexp -- {aldec} $sim_script]} {
        file copy "${script_path}/altera_emif_check_abphy_nios_forces.tcl" "${ex_design_path}/aldec/."
      } elseif {[regexp -- {cadence} $sim_script]} {
        file copy "${script_path}/altera_emif_check_abphy_nios_forces.tcl" "${ex_design_path}/cadence/."
      }
      set fh [open $sim_script r]
      set file_data [read $fh]
      close $fh
      
      set firstline 1
      set fh [open $sim_script w]
      foreach line [split $file_data "\n"] {
         if { $firstline == 1 } {
            set firstline 0
            puts $fh "tclsh altera_emif_check_abphy_nios_forces.tcl"
         }
         puts $fh $line
      }
      close $fh
   }
}

post_message " "
post_message "*************************************************************************"
post_message "Successfully generated abstract phy simulation script"
post_message "*************************************************************************"
post_message " "
