-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Author : Hajee Pepping
-- Purpose: Verify dp_shiftram
-- Description:
-- Usage:
-- > as 8
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
--
-- Use visual inspection to see if the src_out record has the desired jums in the data
-- at the pulses of the sync_in signal. The latency of dp_shift_ram is 4 clock cylces,
-- so the effect of the new setting is visible 4 clock cycles after the sync_in pulse.
--

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_shiftram is
  generic (
    -- DUT
    g_nof_streams         : natural := 2;
    g_nof_words           : natural := 64;
    g_data_w              : natural := 16;
    g_use_sync_in         : boolean := true;
    -- TB
    g_nof_blocks_per_sync : natural := 4;
    g_nof_repeat          : natural := 20
  );
end tb_dp_shiftram;

architecture tb of tb_dp_shiftram is
  constant dp_clk_period    : time :=  5 ns;
  constant mm_clk_period    : time := 20 ns;

  constant c_block_size     : natural := 16;
  constant c_gap_size       : natural := 0;
  constant c_half_nof_words : natural := g_nof_words / 2;

  signal tb_end             : std_logic := '0';
  signal mm_clk             : std_logic := '1';
  signal mm_rst             : std_logic := '1';

  signal dp_clk             : std_logic := '1';
  signal dp_rst             : std_logic := '1';

  signal sync_in            : std_logic := '0';

  signal reg_mosi           : t_mem_mosi;
  signal reg_miso           : t_mem_miso;

  signal ref_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal ref_sosi_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_sosi_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal state              : natural := 0;
begin
  mm_clk <= (not mm_clk) or tb_end after mm_clk_period / 2;
  mm_rst <= '1', '0' after mm_clk_period * 7;

  dp_clk <= (not dp_clk) or tb_end after dp_clk_period / 2;
  dp_rst <= '1', '0' after dp_clk_period * 7;

  ------------------------------------------------------------------------------
  -- STIMULI DATAPATH
  ------------------------------------------------------------------------------
  p_stimuli : process
  begin
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(dp_clk, 5);

    wait until state = 1;

    for I in 0 to g_nof_repeat - 1 loop
      ref_sosi.sync  <= '1';
      ref_sosi.sop   <= '1';
      ref_sosi.valid <= '1';
      ref_sosi.bsn   <= TO_DP_BSN(I);
      proc_common_wait_some_cycles(dp_clk, 1);
      ref_sosi.sync  <= '0';
      ref_sosi.sop   <= '0';
      proc_common_wait_some_cycles(dp_clk, c_block_size-2);
      ref_sosi.eop   <= '1';
      proc_common_wait_some_cycles(dp_clk, 1);
      ref_sosi.eop   <= '0';
      ref_sosi.valid <= '0';
      proc_common_wait_some_cycles(dp_clk, c_gap_size);
      for I in 0 to g_nof_blocks_per_sync - 2 loop
        ref_sosi.sop   <= '1';
        ref_sosi.valid <= '1';
        proc_common_wait_some_cycles(dp_clk, 1);
        ref_sosi.sync  <= '0';
        ref_sosi.sop   <= '0';
        proc_common_wait_some_cycles(dp_clk, c_block_size-2);
        ref_sosi.eop   <= '1';
        proc_common_wait_some_cycles(dp_clk, 1);
        ref_sosi.eop   <= '0';
        ref_sosi.valid <= '0';
        proc_common_wait_some_cycles(dp_clk, c_gap_size);
      end loop;
    end loop;

    -- End of stimuli
    proc_common_wait_some_cycles(dp_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  ref_sosi.data  <= INCR_UVEC(ref_sosi.data, 1) when ref_sosi.valid = '1' and rising_edge(dp_clk);
  ref_sosi.re    <= INCR_UVEC(ref_sosi.re,   1) when ref_sosi.valid = '1' and rising_edge(dp_clk);
  ref_sosi.im    <= INCR_UVEC(ref_sosi.im,   1) when ref_sosi.valid = '1' and rising_edge(dp_clk);

  gen_copy_inputs : for i in 0 to g_nof_streams - 1 generate
    ref_sosi_arr(i) <= ref_sosi;
  end generate;

  sync_in <= ref_sosi_arr(0).sync;

  ------------------------------------------------------------------------------
  -- STIMULI MM
  -- Apply different delay settings on two streams.
  ------------------------------------------------------------------------------
  p_mm_stimuli : process
  begin
    state <= 0;
    reg_mosi <= c_mem_mosi_rst;
     proc_common_wait_some_cycles(mm_clk, 10);
    proc_mem_mm_bus_wr( 0, c_half_nof_words, mm_clk, reg_mosi);
    proc_common_wait_some_cycles(mm_clk, 12);
    state <= 1;
    proc_common_wait_some_cycles(mm_clk, 80);

    -- Modify first stream
    proc_mem_mm_bus_wr( 0, c_half_nof_words - 6, mm_clk, reg_mosi);
    wait until sync_in = '1';
    proc_mem_mm_bus_wr( 0, c_half_nof_words - 4, mm_clk, reg_mosi);
    wait until sync_in = '1';
    proc_mem_mm_bus_wr( 0, c_half_nof_words - 3, mm_clk, reg_mosi);
    wait until sync_in = '1';
    proc_mem_mm_bus_wr( 0, c_half_nof_words - 2, mm_clk, reg_mosi);
    wait until sync_in = '1';
    proc_mem_mm_bus_wr( 0, c_half_nof_words + 3, mm_clk, reg_mosi);
    wait until sync_in = '1';
    proc_mem_mm_bus_wr( 0, c_half_nof_words, mm_clk, reg_mosi);

    -- Modify second stream
    wait until sync_in = '1';
    proc_mem_mm_bus_wr( 2, c_half_nof_words - 4, mm_clk, reg_mosi);
    wait until sync_in = '1';
    proc_mem_mm_bus_wr( 2, c_half_nof_words - 3, mm_clk, reg_mosi);
    wait until sync_in = '1';
    proc_mem_mm_bus_wr( 2, c_half_nof_words - 2, mm_clk, reg_mosi);

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dut: entity work.dp_shiftram
  generic map (
    g_nof_streams => g_nof_streams,
    g_nof_words   => g_nof_words,
    g_data_w      => g_data_w,
    g_use_sync_in => g_use_sync_in

  )
  port map (
    dp_rst       => dp_rst,
    dp_clk       => dp_clk,

    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    sync_in      => sync_in,

    reg_mosi     => reg_mosi,
    reg_miso     => reg_miso,

    -- Streaming sink
    snk_in_arr   => ref_sosi_arr,
    -- Streaming source
    src_out_arr  => out_sosi_arr
  );
end tb;
