#ifndef IO2_COPY_H
#define IO2_COPY_H
#include <alt_types.h>
// mind you: to + from are taken to be pointers to 32-bit words;
//  sinc and dinc are increments in *units of words* (ie 4bytes)
alt_u32 io2_copy(void* to, void* from, unsigned int n, unsigned int sinc, unsigned int dinc);
#endif
