-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use work.qsys_unb1_minimal_pkg.all;

entity mmm_unb1_minimal is
  generic (
    g_sim         : boolean := false;  -- FALSE: use SOPC; TRUE: use mm_file I/O
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0;
    g_use_qsys    : boolean := false;
    g_use_sopc    : boolean := false
  );
  port (
    xo_clk                   : in  std_logic;
    xo_rst_n                 : in  std_logic;
    xo_rst                   : in  std_logic;

    mm_rst                   : in  std_logic;
    mm_clk                   : out std_logic;
    mm_locked                : out std_logic;

    epcs_clk                 : out std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi             : out t_mem_mosi;
    reg_wdi_miso             : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi : out t_mem_mosi;
    reg_unb_system_info_miso : in  t_mem_miso;
    rom_unb_system_info_mosi : out t_mem_mosi;
    rom_unb_system_info_miso : in  t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        : out t_mem_mosi;
    reg_unb_sens_miso        : in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi            : out t_mem_mosi;
    reg_ppsh_miso            : in  t_mem_miso;

    -- eth1g
    eth1g_tse_clk            : out std_logic;
    eth1g_mm_rst             : out std_logic;
    eth1g_tse_mosi           : out t_mem_mosi;
    eth1g_tse_miso           : in  t_mem_miso;
    eth1g_reg_mosi           : out t_mem_mosi;
    eth1g_reg_miso           : in  t_mem_miso;
    eth1g_reg_interrupt      : in  std_logic;
    eth1g_ram_mosi           : out t_mem_mosi;
    eth1g_ram_miso           : in  t_mem_miso;
    -- EPCS read
    reg_dpmm_data_mosi       : out t_mem_mosi;
    reg_dpmm_data_miso       : in  t_mem_miso;
    reg_dpmm_ctrl_mosi       : out t_mem_mosi;
    reg_dpmm_ctrl_miso       : in  t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi       : out t_mem_mosi;
    reg_mmdp_data_miso       : in  t_mem_miso;
    reg_mmdp_ctrl_mosi       : out t_mem_mosi;
    reg_mmdp_ctrl_miso       : in  t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi            : out t_mem_mosi;
    reg_epcs_miso            : in  t_mem_miso;

    -- Remote Update
    reg_remu_mosi            : out t_mem_mosi;
    reg_remu_miso            : in  t_mem_miso
  );
end mmm_unb1_minimal;

architecture str of mmm_unb1_minimal is
  constant c_mm_clk_period   : time := 8 ns;  -- 125 MHz
  constant c_epcs_clk_period : time := 50 ns;  -- 20 MHz

  constant c_sim_node_type : string(1 to 2) := sel_a_b(g_sim_node_nr < 4, "FN", "BN");
  constant c_sim_node_nr   : natural := sel_a_b(c_sim_node_type = "BN", g_sim_node_nr - 4, g_sim_node_nr);

  signal i_mm_clk   : std_logic := '1';
  signal i_epcs_clk : std_logic := '1';

  ----------------------------------------------------------------------------
  -- MM arbiter
  -- . In the NIOS-only (plus 1GbE actually) QSYS, all devices are below 0x4000.
  --   . NOTE! Except onchip mem @ 0x20000.
  -- . A single avs_common_mm exports all adresses 0x4000 and above to MM arbiter
  -- . 0x4000 within the QSYS translates to 0x0 on the avs_common_mm exported bus
  -- . Slaves connected to MM arbiter:
  --     [name]        [Base address]  [word address width]  [nof words]  [nof bytes]  [Byte high addr]  [slave index]
  --   . reg_unb_sens               0                     3            8           32                31              0
  --   . rom_system_info           32                    10         1024         4096              4127              1
  --   . pio_system_info         4128                     5           32          128              4255              2
  --   . pio_pps                 4256                     1            2            8              4263              3
  --   . reg_wdi                 4264                     1            2            8              4271              4
  --   . reg_remu                4272                     3            8           32              4303              5
  --   . reg_epcs                4304                     3            8           32              4335              6
  --   . reg_dpmm_ctrl           4336                     1            2            8              4343              7
  --   . reg_dpmm_stat           4344                     1            2            8              4351              8
  --   . reg_mmdp_ctrl           4352                     1            2            8              4359              9
  --   . reg_mmdp_stat           4360                     1            2            8              4367             10

  ----------------------------------------------------------------------------
  constant c_nof_slaves     : natural := 11;
  constant c_slave_base_arr : t_natural_arr(c_nof_slaves - 1 downto 0) := ( 0,  32, 4128, 4256, 4264, 4272, 4304, 4336, 4344, 4352, 4360);
  constant c_slave_high_arr : t_natural_arr(c_nof_slaves - 1 downto 0) := (31, 4127, 4255, 4263, 4271, 4303, 4335, 4343, 4351, 4359, 4367);

  signal master_mosi    : t_mem_mosi;
  signal master_miso    : t_mem_miso;

  signal slave_mosi_arr : t_mem_mosi_arr(c_nof_slaves - 1 downto 0);
  signal slave_miso_arr : t_mem_miso_arr(c_nof_slaves - 1 downto 0);
begin
  mm_clk   <= i_mm_clk;
  epcs_clk <= i_epcs_clk;

  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $HDL_IOFILE_SIM_DIR.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    i_mm_clk   <= not i_mm_clk after c_mm_clk_period / 2;
    mm_locked  <= '0', '1' after c_mm_clk_period * 5;
    i_epcs_clk <= not i_epcs_clk after c_epcs_clk_period / 2;

    eth1g_mm_rst <= mm_rst;

    u_mm_file_reg_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                               port map(mm_rst, i_mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                               port map(mm_rst, i_mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                               port map(mm_rst, i_mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_unb_sens        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
                                               port map(mm_rst, i_mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso );

    u_mm_file_reg_ppsh            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                               port map(mm_rst, i_mm_clk, reg_ppsh_mosi, reg_ppsh_miso );

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    u_mm_file_eth1g_reg           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                               port map(mm_rst, i_mm_clk, eth1g_reg_mosi, eth1g_reg_miso );
    u_mm_file_eth1g_ram           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_RAM")
                                               port map(mm_rst, i_mm_clk, eth1g_ram_mosi, eth1g_ram_miso );
    u_mm_file_eth1g_tse           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_TSE")
                                               port map(mm_rst, i_mm_clk, eth1g_tse_mosi, eth1g_tse_miso );

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  ----------------------------------------------------------------------------
  -- SOPC or QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_sopc : if g_sim = false and g_use_sopc = true generate
    u_sopc : entity work.sopc_unb1_minimal
    port map (
      clk_0                                         => xo_clk,
      reset_n                                       => xo_rst_n,
      mm_clk                                        => i_mm_clk,
      tse_clk                                       => eth1g_tse_clk,
      epcs_clk                                      => i_epcs_clk,

       -- the_altpll_0
      locked_from_the_altpll_0                      => mm_locked,
      phasedone_from_the_altpll_0                   => OPEN,
      areset_to_the_altpll_0                        => xo_rst,

      -- the_avs_eth_0
      coe_clk_export_from_the_avs_eth_0             => OPEN,
      coe_reset_export_from_the_avs_eth_0           => eth1g_mm_rst,
      coe_tse_address_export_from_the_avs_eth_0     => eth1g_tse_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      coe_tse_write_export_from_the_avs_eth_0       => eth1g_tse_mosi.wr,
      coe_tse_writedata_export_from_the_avs_eth_0   => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      coe_tse_read_export_from_the_avs_eth_0        => eth1g_tse_mosi.rd,
      coe_tse_readdata_export_to_the_avs_eth_0      => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      coe_tse_waitrequest_export_to_the_avs_eth_0   => eth1g_tse_miso.waitrequest,
      coe_reg_address_export_from_the_avs_eth_0     => eth1g_reg_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      coe_reg_write_export_from_the_avs_eth_0       => eth1g_reg_mosi.wr,
      coe_reg_writedata_export_from_the_avs_eth_0   => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      coe_reg_read_export_from_the_avs_eth_0        => eth1g_reg_mosi.rd,
      coe_reg_readdata_export_to_the_avs_eth_0      => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      coe_irq_export_to_the_avs_eth_0               => eth1g_reg_interrupt,
      coe_ram_address_export_from_the_avs_eth_0     => eth1g_ram_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      coe_ram_write_export_from_the_avs_eth_0       => eth1g_ram_mosi.wr,
      coe_ram_writedata_export_from_the_avs_eth_0   => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      coe_ram_read_export_from_the_avs_eth_0        => eth1g_ram_mosi.rd,
      coe_ram_readdata_export_to_the_avs_eth_0      => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),

      -- the_reg_unb_sens
      coe_clk_export_from_the_reg_unb_sens          => OPEN,
      coe_reset_export_from_the_reg_unb_sens        => OPEN,
      coe_address_export_from_the_reg_unb_sens      => reg_unb_sens_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_unb_sens         => reg_unb_sens_mosi.rd,
      coe_readdata_export_to_the_reg_unb_sens       => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_unb_sens        => reg_unb_sens_mosi.wr,
      coe_writedata_export_from_the_reg_unb_sens    => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_pps
      coe_clk_export_from_the_pio_pps               => OPEN,
      coe_reset_export_from_the_pio_pps             => OPEN,
      coe_address_export_from_the_pio_pps           => reg_ppsh_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1),  -- 1 bit address width so must use (0) instead of (0 DOWNTO 0)
      coe_read_export_from_the_pio_pps              => reg_ppsh_mosi.rd,
      coe_readdata_export_to_the_pio_pps            => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_pps             => reg_ppsh_mosi.wr,
      coe_writedata_export_from_the_pio_pps         => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_system_info: actually a avs_common_mm instance
      coe_clk_export_from_the_pio_system_info       => OPEN,
      coe_reset_export_from_the_pio_system_info     => OPEN,
      coe_address_export_from_the_pio_system_info   => reg_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_pio_system_info      => reg_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_pio_system_info    => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_system_info     => reg_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_pio_system_info => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_rom_system_info
      coe_clk_export_from_the_rom_system_info       => OPEN,
      coe_reset_export_from_the_rom_system_info     => OPEN,
      coe_address_export_from_the_rom_system_info   => rom_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_rom_system_info      => rom_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_rom_system_info    => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_rom_system_info     => rom_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_rom_system_info => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb1_board.
      out_port_from_the_pio_wdi                     => pout_wdi,

      -- the_reg_dpmm_data
      coe_clk_export_from_the_reg_dpmm_data         => OPEN,
      coe_reset_export_from_the_reg_dpmm_data       => OPEN,
      coe_address_export_from_the_reg_dpmm_data     => reg_dpmm_data_mosi.address(0),
      coe_read_export_from_the_reg_dpmm_data        => reg_dpmm_data_mosi.rd,
      coe_readdata_export_to_the_reg_dpmm_data      => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_dpmm_data       => reg_dpmm_data_mosi.wr,
      coe_writedata_export_from_the_reg_dpmm_data   => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dpmm_ctrl
      coe_clk_export_from_the_reg_dpmm_ctrl         => OPEN,
      coe_reset_export_from_the_reg_dpmm_ctrl       => OPEN,
      coe_address_export_from_the_reg_dpmm_ctrl     => reg_dpmm_ctrl_mosi.address(0),
      coe_read_export_from_the_reg_dpmm_ctrl        => reg_dpmm_ctrl_mosi.rd,
      coe_readdata_export_to_the_reg_dpmm_ctrl      => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_dpmm_ctrl       => reg_dpmm_ctrl_mosi.wr,
      coe_writedata_export_from_the_reg_dpmm_ctrl   => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_mmdp_data
      coe_clk_export_from_the_reg_mmdp_data         => OPEN,
      coe_reset_export_from_the_reg_mmdp_data       => OPEN,
      coe_address_export_from_the_reg_mmdp_data     => reg_mmdp_data_mosi.address(0),
      coe_read_export_from_the_reg_mmdp_data        => reg_mmdp_data_mosi.rd,
      coe_readdata_export_to_the_reg_mmdp_data      => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_mmdp_data       => reg_mmdp_data_mosi.wr,
      coe_writedata_export_from_the_reg_mmdp_data   => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_mmdp_ctrl
      coe_clk_export_from_the_reg_mmdp_ctrl         => OPEN,
      coe_reset_export_from_the_reg_mmdp_ctrl       => OPEN,
      coe_address_export_from_the_reg_mmdp_ctrl     => reg_mmdp_ctrl_mosi.address(0),
      coe_read_export_from_the_reg_mmdp_ctrl        => reg_mmdp_ctrl_mosi.rd,
      coe_readdata_export_to_the_reg_mmdp_ctrl      => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_mmdp_ctrl       => reg_mmdp_ctrl_mosi.wr,
      coe_writedata_export_from_the_reg_mmdp_ctrl   => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_epcs
      coe_clk_export_from_the_reg_epcs              => OPEN,
      coe_reset_export_from_the_reg_epcs            => OPEN,
      coe_address_export_from_the_reg_epcs          => reg_epcs_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_epcs             => reg_epcs_mosi.rd,
      coe_readdata_export_to_the_reg_epcs           => reg_epcs_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_epcs            => reg_epcs_mosi.wr,
      coe_writedata_export_from_the_reg_epcs        => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_remu
      coe_clk_export_from_the_reg_remu              => OPEN,
      coe_reset_export_from_the_reg_remu            => OPEN,
      coe_address_export_from_the_reg_remu          => reg_remu_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_remu             => reg_remu_mosi.rd,
      coe_readdata_export_to_the_reg_remu           => reg_remu_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_remu            => reg_remu_mosi.wr,
      coe_writedata_export_from_the_reg_remu        => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_wdi: Manual WDI override; causes FPGA reconfiguration if WDI is enabled (g_use_phy).
      coe_clk_export_from_the_reg_wdi               => OPEN,
      coe_reset_export_from_the_reg_wdi             => OPEN,
      coe_address_export_from_the_reg_wdi           => reg_wdi_mosi.address(0),
      coe_read_export_from_the_reg_wdi              => reg_wdi_mosi.rd,
      coe_readdata_export_to_the_reg_wdi            => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_wdi             => reg_wdi_mosi.wr,
      coe_writedata_export_from_the_reg_wdi         => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0)
      );
  end generate;

  gen_qsys : if g_sim = false and g_use_qsys = true generate
    u_qsys : qsys_unb1_minimal
    port map (
      clk_0                                         => xo_clk,
      reset_n                                       => xo_rst_n,
      mm_clk                                        => i_mm_clk,
      tse_clk                                       => eth1g_tse_clk,
      epcs_clk                                      => i_epcs_clk,

       -- the_altpll_0
      locked_from_the_altpll_0                      => mm_locked,
      phasedone_from_the_altpll_0                   => OPEN,
      areset_to_the_altpll_0                        => xo_rst,

      -- the_avs_eth_0
      coe_clk_export_from_the_avs_eth_0             => OPEN,
      coe_reset_export_from_the_avs_eth_0           => eth1g_mm_rst,
      coe_tse_address_export_from_the_avs_eth_0     => eth1g_tse_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      coe_tse_write_export_from_the_avs_eth_0       => eth1g_tse_mosi.wr,
      coe_tse_writedata_export_from_the_avs_eth_0   => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      coe_tse_read_export_from_the_avs_eth_0        => eth1g_tse_mosi.rd,
      coe_tse_readdata_export_to_the_avs_eth_0      => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      coe_tse_waitrequest_export_to_the_avs_eth_0   => eth1g_tse_miso.waitrequest,
      coe_reg_address_export_from_the_avs_eth_0     => eth1g_reg_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      coe_reg_write_export_from_the_avs_eth_0       => eth1g_reg_mosi.wr,
      coe_reg_writedata_export_from_the_avs_eth_0   => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      coe_reg_read_export_from_the_avs_eth_0        => eth1g_reg_mosi.rd,
      coe_reg_readdata_export_to_the_avs_eth_0      => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      coe_irq_export_to_the_avs_eth_0               => eth1g_reg_interrupt,
      coe_ram_address_export_from_the_avs_eth_0     => eth1g_ram_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      coe_ram_write_export_from_the_avs_eth_0       => eth1g_ram_mosi.wr,
      coe_ram_writedata_export_from_the_avs_eth_0   => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      coe_ram_read_export_from_the_avs_eth_0        => eth1g_ram_mosi.rd,
      coe_ram_readdata_export_to_the_avs_eth_0      => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),

      -- the_reg_unb_sens
      coe_clk_export_from_the_reg_unb_sens          => OPEN,
      coe_reset_export_from_the_reg_unb_sens        => OPEN,
      coe_address_export_from_the_reg_unb_sens      => reg_unb_sens_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_unb_sens         => reg_unb_sens_mosi.rd,
      coe_readdata_export_to_the_reg_unb_sens       => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_unb_sens        => reg_unb_sens_mosi.wr,
      coe_writedata_export_from_the_reg_unb_sens    => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_pps
      coe_clk_export_from_the_pio_pps               => OPEN,
      coe_reset_export_from_the_pio_pps             => OPEN,
      coe_address_export_from_the_pio_pps           => reg_ppsh_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1),  -- 1 bit address width so must use (0) instead of (0 DOWNTO 0)
      coe_read_export_from_the_pio_pps              => reg_ppsh_mosi.rd,
      coe_readdata_export_to_the_pio_pps            => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_pps             => reg_ppsh_mosi.wr,
      coe_writedata_export_from_the_pio_pps         => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_system_info: actually a avs_common_mm instance
      coe_clk_export_from_the_pio_system_info       => OPEN,
      coe_reset_export_from_the_pio_system_info     => OPEN,
      coe_address_export_from_the_pio_system_info   => reg_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_pio_system_info      => reg_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_pio_system_info    => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_system_info     => reg_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_pio_system_info => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_rom_system_info
      coe_clk_export_from_the_rom_system_info       => OPEN,
      coe_reset_export_from_the_rom_system_info     => OPEN,
      coe_address_export_from_the_rom_system_info   => rom_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_rom_system_info      => rom_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_rom_system_info    => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_rom_system_info     => rom_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_rom_system_info => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb1_board.
      out_port_from_the_pio_wdi                     => pout_wdi,

      -- the_reg_dpmm_data
      coe_clk_export_from_the_reg_dpmm_data         => OPEN,
      coe_reset_export_from_the_reg_dpmm_data       => OPEN,
      coe_address_export_from_the_reg_dpmm_data     => reg_dpmm_data_mosi.address(0),
      coe_read_export_from_the_reg_dpmm_data        => reg_dpmm_data_mosi.rd,
      coe_readdata_export_to_the_reg_dpmm_data      => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_dpmm_data       => reg_dpmm_data_mosi.wr,
      coe_writedata_export_from_the_reg_dpmm_data   => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dpmm_ctrl
      coe_clk_export_from_the_reg_dpmm_ctrl         => OPEN,
      coe_reset_export_from_the_reg_dpmm_ctrl       => OPEN,
      coe_address_export_from_the_reg_dpmm_ctrl     => reg_dpmm_ctrl_mosi.address(0),
      coe_read_export_from_the_reg_dpmm_ctrl        => reg_dpmm_ctrl_mosi.rd,
      coe_readdata_export_to_the_reg_dpmm_ctrl      => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_dpmm_ctrl       => reg_dpmm_ctrl_mosi.wr,
      coe_writedata_export_from_the_reg_dpmm_ctrl   => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_mmdp_data
      coe_clk_export_from_the_reg_mmdp_data         => OPEN,
      coe_reset_export_from_the_reg_mmdp_data       => OPEN,
      coe_address_export_from_the_reg_mmdp_data     => reg_mmdp_data_mosi.address(0),
      coe_read_export_from_the_reg_mmdp_data        => reg_mmdp_data_mosi.rd,
      coe_readdata_export_to_the_reg_mmdp_data      => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_mmdp_data       => reg_mmdp_data_mosi.wr,
      coe_writedata_export_from_the_reg_mmdp_data   => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_mmdp_ctrl
      coe_clk_export_from_the_reg_mmdp_ctrl         => OPEN,
      coe_reset_export_from_the_reg_mmdp_ctrl       => OPEN,
      coe_address_export_from_the_reg_mmdp_ctrl     => reg_mmdp_ctrl_mosi.address(0),
      coe_read_export_from_the_reg_mmdp_ctrl        => reg_mmdp_ctrl_mosi.rd,
      coe_readdata_export_to_the_reg_mmdp_ctrl      => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_mmdp_ctrl       => reg_mmdp_ctrl_mosi.wr,
      coe_writedata_export_from_the_reg_mmdp_ctrl   => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_epcs
      coe_clk_export_from_the_reg_epcs              => OPEN,
      coe_reset_export_from_the_reg_epcs            => OPEN,
      coe_address_export_from_the_reg_epcs          => reg_epcs_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_epcs             => reg_epcs_mosi.rd,
      coe_readdata_export_to_the_reg_epcs           => reg_epcs_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_epcs            => reg_epcs_mosi.wr,
      coe_writedata_export_from_the_reg_epcs        => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_remu
      coe_clk_export_from_the_reg_remu              => OPEN,
      coe_reset_export_from_the_reg_remu            => OPEN,
      coe_address_export_from_the_reg_remu          => reg_remu_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_remu             => reg_remu_mosi.rd,
      coe_readdata_export_to_the_reg_remu           => reg_remu_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_remu            => reg_remu_mosi.wr,
      coe_writedata_export_from_the_reg_remu        => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_wdi: Manual WDI override; causes FPGA reconfiguration if WDI is enabled (g_use_phy).
      coe_clk_export_from_the_reg_wdi               => OPEN,
      coe_reset_export_from_the_reg_wdi             => OPEN,
      coe_address_export_from_the_reg_wdi           => reg_wdi_mosi.address(0),
      coe_read_export_from_the_reg_wdi              => reg_wdi_mosi.rd,
      coe_readdata_export_to_the_reg_wdi            => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_wdi             => reg_wdi_mosi.wr,
      coe_writedata_export_from_the_reg_wdi         => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0)
      );
  end generate;

  -----------------------------------------------------------------------------
  -- MM arbiter experiment
  -----------------------------------------------------------------------------
  gen_mm_arbiter : if g_use_qsys = false and g_use_sopc = false generate  -- Still a QSYS actually....bus a minimal one.

    -----------------------------------------------------------------------------
    -- MM master: a minimal QSYS (for now)
    -----------------------------------------------------------------------------
    u_qsys_unb1_minimal_mm_arbiter : qsys_unb1_minimal_mm_arbiter
    port map (
      clk_0                                         => xo_clk,
      reset_n                                       => xo_rst_n,
      mm_clk                                        => i_mm_clk,
      tse_clk                                       => eth1g_tse_clk,
      epcs_clk                                      => i_epcs_clk,

       -- the_altpll_0
      locked_from_the_altpll_0                      => mm_locked,
      phasedone_from_the_altpll_0                   => OPEN,
      areset_to_the_altpll_0                        => xo_rst,

      -- the_avs_eth_0
      coe_clk_export_from_the_avs_eth_0             => OPEN,
      coe_reset_export_from_the_avs_eth_0           => eth1g_mm_rst,
      coe_tse_address_export_from_the_avs_eth_0     => eth1g_tse_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      coe_tse_write_export_from_the_avs_eth_0       => eth1g_tse_mosi.wr,
      coe_tse_writedata_export_from_the_avs_eth_0   => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      coe_tse_read_export_from_the_avs_eth_0        => eth1g_tse_mosi.rd,
      coe_tse_readdata_export_to_the_avs_eth_0      => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      coe_tse_waitrequest_export_to_the_avs_eth_0   => eth1g_tse_miso.waitrequest,
      coe_reg_address_export_from_the_avs_eth_0     => eth1g_reg_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      coe_reg_write_export_from_the_avs_eth_0       => eth1g_reg_mosi.wr,
      coe_reg_writedata_export_from_the_avs_eth_0   => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      coe_reg_read_export_from_the_avs_eth_0        => eth1g_reg_mosi.rd,
      coe_reg_readdata_export_to_the_avs_eth_0      => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      coe_irq_export_to_the_avs_eth_0               => eth1g_reg_interrupt,
      coe_ram_address_export_from_the_avs_eth_0     => eth1g_ram_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      coe_ram_write_export_from_the_avs_eth_0       => eth1g_ram_mosi.wr,
      coe_ram_writedata_export_from_the_avs_eth_0   => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      coe_ram_read_export_from_the_avs_eth_0        => eth1g_ram_mosi.rd,
      coe_ram_readdata_export_to_the_avs_eth_0      => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),

      -- the_pio_system_info: Entangled within unb_osy code, so still required for now.
      pio_system_info_clk_export       => OPEN,
      pio_system_info_reset_export     => OPEN,
      pio_system_info_address_export   => reg_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_read_export      => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export  => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      pio_system_info_write_export     => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- avs_common_mm export to MM arbiter
      to_mm_arbiter_clk_export         => OPEN,
      to_mm_arbiter_reset_export       => OPEN,
      to_mm_arbiter_address_export     => master_mosi.address(11 - 1 downto 0),
      to_mm_arbiter_read_export        => master_mosi.rd,
      to_mm_arbiter_readdata_export    => master_miso.rddata(c_word_w - 1 downto 0),
      to_mm_arbiter_write_export       => master_mosi.wr,
      to_mm_arbiter_writedata_export   => master_mosi.wrdata(c_word_w - 1 downto 0)
    );

    -----------------------------------------------------------------------------
    -- MM arbiter
    -----------------------------------------------------------------------------
    u_mm_arbiter : entity mm_lib.mm_arbiter
    generic map (
      g_nof_slaves     => c_nof_slaves,
      g_slave_base_arr => c_slave_base_arr,
      g_slave_high_arr => c_slave_high_arr
    )
    port map (
      mm_clk         => i_mm_clk,
      mm_rst         => mm_rst,

      master_mosi    => master_mosi,
      master_miso    => master_miso,

      slave_mosi_arr => slave_mosi_arr,
      slave_miso_arr => slave_miso_arr
    );

    -----------------------------------------------------------------------------
    -- Connect slave array to individually names MM buses
    -----------------------------------------------------------------------------
    reg_unb_sens_mosi <= slave_mosi_arr(0);
    slave_miso_arr(0) <= reg_unb_sens_miso;

    rom_unb_system_info_mosi <= slave_mosi_arr(1);
    slave_miso_arr(1) <= rom_unb_system_info_miso;

    -- pio_system_info; still needed within QSYS, so not connected here.
--    reg_unb_system_info_mosi <= slave_mosi_arr(2);
    slave_miso_arr(2) <= c_mem_miso_rst;

    reg_ppsh_mosi <= slave_mosi_arr(3);
    slave_miso_arr(3) <= reg_ppsh_miso;

    reg_wdi_mosi <= slave_mosi_arr(4);
    slave_miso_arr(4) <= reg_wdi_miso;

    reg_remu_mosi <= slave_mosi_arr(5);
    slave_miso_arr(5) <= reg_remu_miso;

    reg_epcs_mosi <= slave_mosi_arr(6);
    slave_miso_arr(6) <= reg_epcs_miso;

    reg_dpmm_ctrl_mosi <= slave_mosi_arr(7);
    slave_miso_arr(7) <= reg_dpmm_ctrl_miso;

    reg_dpmm_data_mosi <= slave_mosi_arr(8);
    slave_miso_arr(8) <= reg_dpmm_data_miso;

    reg_mmdp_ctrl_mosi <= slave_mosi_arr(9);
    slave_miso_arr(9) <= reg_mmdp_ctrl_miso;

    reg_mmdp_data_mosi <= slave_mosi_arr(10);
    slave_miso_arr(10) <= reg_mmdp_data_miso;
  end generate;
end str;
