-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 4 nov 2021
-- Purpose: Multi tb for common_round.vhd and s_round(), u_round() in common_pkg.vhd

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_round is
end tb_tb_round;

architecture tb of tb_tb_round is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- Supported for RESIZE_NUM() and common_round.vhd
  -- g_in_dat_w        : NATURAL := 5;
  -- g_out_dat_w       : NATURAL := 3

  u_extend      : entity work.tb_round generic map (5, 6);
  u_wires       : entity work.tb_round generic map (5, 5);
  u_round_w4_r1 : entity work.tb_round generic map (4, 3);  -- -r = 4 - 3 = 1
  u_round_w4_r2 : entity work.tb_round generic map (4, 2);  -- -r = 4 - 2 = 2
  u_round_w4_r3 : entity work.tb_round generic map (4, 1);  -- -r = 4 - 1 = 3
  u_round_w5_r2 : entity work.tb_round generic map (5, 3);  -- -r = 5 - 3 = 2
  u_round_w5_r3 : entity work.tb_round generic map (5, 2);  -- -r = 5 - 2 = 3
end tb;
