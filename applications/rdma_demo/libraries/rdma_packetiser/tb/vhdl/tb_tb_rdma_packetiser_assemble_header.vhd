-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Verify multiple variations of tb_rdma_packetiser_assemble_header
-- Description:
-- Usage:
-- > as 3
-- > run -all
-- Remark: testbench takes roughly 3 minutes without wave window.
-------------------------------------------------------------------------------

library IEEE;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;

entity tb_tb_rdma_packetiser_assemble_header is
end tb_tb_rdma_packetiser_assemble_header;

architecture tb of tb_tb_rdma_packetiser_assemble_header is
  constant c_low_start_addr  : unsigned(63 downto 0) := X"000000000000BCDF"; -- arbitrary low start address
  constant c_high_start_addr : unsigned(63 downto 0) := X"CBA9876543210000"; -- arbitrary high start address
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- All generics of TB
  --    g_data_w                   : natural := c_word_w;
  --    g_use_immediate            : boolean := true;
  --    g_use_msg_cnt_as_immediate : boolean := true;
  --    g_nof_rep                  : natural := 15;
  --    g_frame_len                : natural := 15;
  --    g_start_address            : unsigned(c_longword_w - 1 downto 0) := (others => '0');
  --    g_nof_packets_in_msg       : natural := 4;
  --    g_nof_msg                  : natural := 3

  u_lo_addr    : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   true,  true,  50,   15,   c_low_start_addr,  4,   5);
  u_hi_addr    : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   true,  true,  50,   15,   c_high_start_addr, 4,   5);
  u_no_mid     : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   true,  true,  50,   15,   c_high_start_addr, 2,   5);
  u_wr_only    : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   true,  true,  50,   15,   c_high_start_addr, 1,   5);
  u_large      : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   true,  true,  10,   2000, c_low_start_addr,  3,   1);
  u_no_imm_cnt : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   false, true,  50,   15,   c_low_start_addr,  4,   5);
  u_no_cnt     : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   true,  false, 50,   15,   c_low_start_addr,  4,   5);
  u_no_imm     : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   false, false, 50,   7,    c_high_start_addr, 3,   5);
  u_wide       : entity work.tb_rdma_packetiser_assemble_header generic map( 1024, true,  true,  50,   6,    c_low_start_addr,  1,   5);
  u_many       : entity work.tb_rdma_packetiser_assemble_header generic map( 32,   true,  true,  600,  7,    c_low_start_addr,  100, 5);
end tb;
