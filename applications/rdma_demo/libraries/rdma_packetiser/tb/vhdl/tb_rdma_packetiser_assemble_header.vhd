-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. vd Walle
-- Purpose:
-- . test bench for rdma_packetiser_assemble_header
-- Description:
-- . Generates DP data using proc_dp_gen_block_data that streams into DUT.
-- . Verifies the resulting header field array comming from the DUT by
--   comparing to expected values.
-- . The generics can be used to test specific conditions.
--   . g_use_immediate: When true, the DUT will use the optional immediate data
--     field.
--   . g_use_msg_cnt_as_immediate: When true, the DUT will use the message count
--     value as immediate data. When false, the DUT sets the immediate data to
--     the value of the immediate_data input port.
--   . g_nof_rep: number of packets the TB should generate.
--   . g_frame_len: length of the data frames that the TB should generate.
--   . g_start_address: 64 bit value to use as a start address for the DUT
--   . g_nof_packets_in_msg: The number of packets the DUT should put in one
--     RDMA message.
--   . g_nof_msg: Number of RDMA messages the DUT should create.
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use common_lib.common_field_pkg.all;
  use common_lib.tb_common_pkg.all;
  use common_lib.tb_common_mem_pkg.all;
  use dp_lib.tb_dp_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use work.rdma_packetiser_pkg.all;

entity tb_rdma_packetiser_assemble_header is
  generic (
    g_data_w                   : natural := c_word_w;
    g_use_immediate            : boolean := true;
    g_use_msg_cnt_as_immediate : boolean := true;
    g_nof_rep                  : natural := 60;
    g_frame_len                : natural := 15;
    g_start_address            : unsigned(c_longword_w - 1 downto 0) := (others => '0');
    g_nof_packets_in_msg       : natural := 4;
    g_nof_msg                  : natural := 3
  );
end tb_rdma_packetiser_assemble_header;

architecture tb of tb_rdma_packetiser_assemble_header is
  constant c_dp_clk_period      : time := 5 ns;  -- 200 MHz
  constant c_mm_clk_period      : time := 1 ns;  -- 1 GHz
  constant c_data_init          : natural := 13;
  constant c_hdr_fields_slv_rst : std_logic_vector(1023 downto 0) := (others => '0');
  constant c_block_len          : natural := g_frame_len * (g_data_w / c_octet_w);
  constant c_dma_len            : natural := c_block_len * g_nof_packets_in_msg;

  constant c_mm_addr_config_start_address            : natural := 0;
  constant c_mm_addr_config_nof_msg                  : natural := 2;
  constant c_mm_addr_config_nof_packets_in_msg       : natural := 3;
  constant c_mm_addr_config_use_msg_cnt_as_immediate : natural := 4;
  constant c_mm_addr_config_use_immediate            : natural := 5;
  constant c_mm_addr_immediate_data                  : natural := 7;
  constant c_mm_addr_reth_dma_length                 : natural := 8;

  signal tb_end               : std_logic := '0';

  signal dp_clk               : std_logic := '1';
  signal dp_rst               : std_logic;
  signal mm_clk               : std_logic := '1';
  signal mm_rst               : std_logic;

  signal block_len            : std_logic_vector(c_halfword_w - 1 downto 0) := TO_UVEC(c_block_len, c_halfword_w);
  signal nof_msg              : std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(g_nof_msg, c_word_w);
  signal dma_len              : std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(c_dma_len, c_word_w);
  signal start_address        : std_logic_vector(c_longword_w - 1 downto 0) := std_logic_vector(g_start_address);
  signal immediate_data       : std_logic_vector(c_word_w - 1 downto 0) := X"89ABCDEF";

  signal first_hdr_fields_arr : std_logic_vector(1023 downto 0) := (others => '0');
  signal mid_hdr_fields_arr   : std_logic_vector(1023 downto 0) := (others => '0');
  signal last_hdr_fields_arr  : std_logic_vector(1023 downto 0) := (others => '0');
  signal wo_hdr_fields_arr    : std_logic_vector(1023 downto 0) := (others => '0');
  signal rx_rdma_header       : t_rdma_packetiser_roce_header;
  signal exp_rdma_header      : t_rdma_packetiser_roce_header := func_rdma_packetiser_map_header(c_hdr_fields_slv_rst, c_rdma_packetiser_wo_hdr_field_arr);

  signal in_en   : std_logic := '0';
  signal mm_done : std_logic := '0';

  signal snk_in   : t_dp_sosi := c_dp_sosi_rst;
  signal snk_out  : t_dp_siso := c_dp_siso_rdy;
  signal src_out  : t_dp_sosi := c_dp_sosi_rst;
  signal src_in   : t_dp_siso := c_dp_siso_rdy;
  signal sop_sosi : t_dp_sosi := c_dp_sosi_rst;

  signal reg_hdr_dat_copi   : t_mem_copi := c_mem_copi_rst;
  signal reg_hdr_dat_cipo   : t_mem_cipo;

begin
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;

  p_dp_stimuli : process
  begin
    -- dp stimuli
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_until_high(dp_clk, mm_done);  -- wait mm setup
    proc_common_wait_some_cycles(dp_clk, 100);
    in_en <= '1';
    for rep in 0 to g_nof_rep - 1 loop
      proc_dp_gen_block_data(1, true, g_data_w, g_data_w, c_data_init, 0, 0, g_frame_len, 0, 0, '0', TO_DP_BSN(rep), dp_clk, in_en, snk_out, snk_in);
    end loop;
    proc_common_wait_some_cycles(dp_clk, 100);
    in_en <= '0';
    wait;
  end process;

  p_mm_setup : process
  begin
    proc_common_wait_until_low(dp_clk, mm_rst);
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(mm_clk, 50);

    proc_mem_mm_bus_wr(c_mm_addr_config_start_address,                    start_address(31 downto 0),        mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    proc_mem_mm_bus_wr(c_mm_addr_config_start_address + 1,                start_address(63 downto 32),       mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    proc_mem_mm_bus_wr(c_mm_addr_config_nof_msg,                          g_nof_msg,                         mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    proc_mem_mm_bus_wr(c_mm_addr_config_nof_packets_in_msg,               g_nof_packets_in_msg,              mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    proc_mem_mm_bus_wr(c_mm_addr_config_use_msg_cnt_as_immediate, sel_a_b(g_use_msg_cnt_as_immediate, 1, 0), mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    proc_mem_mm_bus_wr(c_mm_addr_config_use_immediate,            sel_a_b(g_use_immediate, 1, 0),            mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    proc_mem_mm_bus_wr(c_mm_addr_immediate_data,                          immediate_data,                    mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    proc_mem_mm_bus_wr(c_mm_addr_reth_dma_length,                         c_dma_len,                         mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    mm_done <= '1';
    wait;
  end process;

  -- check if values in rdma_packetiser_assemble_header match with expected values
  p_verify_rdma_header : process
    variable v_exp_ip_total_length      : natural;
    variable v_exp_udp_total_length     : natural;
    variable v_exp_bth_opcode           : std_logic_vector(c_byte_w - 1 downto 0);
    variable v_exp_bth_psn              : natural;
    variable v_exp_reth_virtual_address : unsigned(c_longword_w - 1 downto 0);
    variable v_exp_reth_dma_length      : natural;
    variable v_exp_immediate_data       : std_logic_vector(c_word_w - 1 downto 0);
    variable v_p, v_m                   : natural := 0;
    variable v_exp_hdr_raw              : std_logic_vector(field_slv_len(c_rdma_packetiser_wo_hdr_field_arr) - 1 downto 0);
    variable v_exp_rdma_header          : t_rdma_packetiser_roce_header := func_rdma_packetiser_map_header(c_hdr_fields_slv_rst, c_rdma_packetiser_wo_hdr_field_arr);
  begin

    proc_common_wait_until_high(dp_clk, mm_done);  -- wait mm setup

    for rep in 0 to g_nof_rep - 1 loop
      proc_common_wait_until_high(dp_clk, sop_sosi.sop);  -- wait for sop of dp_offload

      v_exp_bth_psn              := v_p;
      v_exp_immediate_data       := sel_a_b(g_use_immediate,
      sel_a_b(g_use_msg_cnt_as_immediate, to_uvec((v_m mod g_nof_msg), c_word_w), immediate_data), to_uvec(0, c_word_w));

      -- determine expected opcode
      if v_p mod g_nof_packets_in_msg = 0 then
        v_exp_bth_opcode := c_rdma_packetiser_opcode_uc_write_first;
        if g_nof_packets_in_msg = 1 and g_use_immediate then
          v_exp_bth_opcode := c_rdma_packetiser_opcode_uc_write_only_imm;
        elsif g_nof_packets_in_msg = 1 then
          v_exp_bth_opcode := c_rdma_packetiser_opcode_uc_write_only;
        end if;
      elsif v_p mod g_nof_packets_in_msg = g_nof_packets_in_msg - 1 then
        v_exp_bth_opcode := c_rdma_packetiser_opcode_uc_write_last;
        if g_use_immediate then
          v_exp_bth_opcode := c_rdma_packetiser_opcode_uc_write_last_imm;
        end if;
      else
        v_exp_bth_opcode := c_rdma_packetiser_opcode_uc_write_middle;
      end if;

      -- calculate expected lengths
      v_exp_udp_total_length     := c_network_udp_header_len + c_rdma_packetiser_bth_len + to_uint(block_len) + c_rdma_packetiser_icrc_len;
      v_exp_reth_virtual_address := (others => '0');
      v_exp_reth_dma_length      := 0;

      if v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_first    or
         v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_only     or
         v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_only_imm then
        v_exp_udp_total_length := v_exp_udp_total_length + c_rdma_packetiser_reth_len;
        v_exp_reth_virtual_address := g_start_address + to_unsigned((v_m mod g_nof_msg) * c_dma_len, c_longword_w);
        v_exp_reth_dma_length      := c_dma_len;
      end if;

      if v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_only_imm or
         v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_last_imm then
        v_exp_udp_total_length := v_exp_udp_total_length + c_rdma_packetiser_imm_len;
      end if;

      v_exp_ip_total_length := c_network_ip_header_len + v_exp_udp_total_length;

      -- select header based on expected opcode
      if v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_first or
         v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_only then
        rx_rdma_header <= func_rdma_packetiser_map_header(first_hdr_fields_arr, c_rdma_packetiser_first_hdr_field_arr);
        v_exp_immediate_data := (others => '0'); -- does not exist in this header
      elsif v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_middle or
            v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_last then
        rx_rdma_header <= func_rdma_packetiser_map_header(mid_hdr_fields_arr,   c_rdma_packetiser_mid_hdr_field_arr);
        v_exp_immediate_data := (others => '0'); -- does not exist in this header
      elsif v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_last_imm then
        rx_rdma_header <= func_rdma_packetiser_map_header(last_hdr_fields_arr,    c_rdma_packetiser_last_hdr_field_arr);
      elsif v_exp_bth_opcode = c_rdma_packetiser_opcode_uc_write_only_imm then
        rx_rdma_header <= func_rdma_packetiser_map_header(wo_hdr_fields_arr,    c_rdma_packetiser_wo_hdr_field_arr);
      end if;

      -- assign expected values to signal to view in wave window.
      -- defaults
      v_exp_rdma_header.ip.version           := to_uvec(4, 4);
      v_exp_rdma_header.ip.header_length     := to_uvec(5, 4);
      v_exp_rdma_header.ip.flags             := to_uvec(2, 3);
      v_exp_rdma_header.ip.time_to_live      := to_uvec(127, 8);
      v_exp_rdma_header.ip.protocol          := to_uvec(17, 8);
      v_exp_rdma_header.bth.partition_key    := to_uvec(65535, 16);
      -- changed by DUT
      v_exp_rdma_header.ip.total_length      := to_uvec(v_exp_ip_total_length, c_halfword_w);
      v_exp_rdma_header.udp.total_length     := to_uvec(v_exp_udp_total_length, c_halfword_w );
      v_exp_rdma_header.bth.opcode           := v_exp_bth_opcode;
      v_exp_rdma_header.bth.psn              := to_uvec(v_exp_bth_psn, c_word_w);
      v_exp_rdma_header.reth.virtual_address := std_logic_vector(v_exp_reth_virtual_address);
      v_exp_rdma_header.reth.dma_length      := to_uvec(v_exp_reth_dma_length, c_word_w);
      v_exp_rdma_header.immediate_data       := v_exp_immediate_data;
       -- determine expected ip header checksum
      v_exp_hdr_raw := func_rdma_packetiser_unmap_header(v_exp_rdma_header, c_rdma_packetiser_wo_hdr_field_arr);
      v_exp_rdma_header.ip.header_checksum := func_network_ip_header_checksum(c_rdma_packetiser_wo_hdr_field_arr, v_exp_hdr_raw);

      exp_rdma_header <= v_exp_rdma_header; -- for view in wave window

      -- increase counters
      v_p := v_p + 1;
      v_m := v_p / g_nof_packets_in_msg;

      proc_common_wait_some_cycles(dp_clk, 1);

      -- assert when header is not as expected.
      assert rx_rdma_header = exp_rdma_header
        report "Wrong rx_rdma_header"
        severity error;
      assert rx_rdma_header.ip.total_length = exp_rdma_header.ip.total_length
        report "Wrong rx_rdma_header.ip.total_length value"
        severity error;
      assert rx_rdma_header.udp.total_length = exp_rdma_header.udp.total_length
        report "Wrong rx_rdma_header.udp.total_length value"
        severity error;
      assert rx_rdma_header.bth.opcode = exp_rdma_header.bth.opcode
        report "Wrong rx_rdma_header.bth.opcode value"
        severity error;
      assert rx_rdma_header.bth.psn = exp_rdma_header.bth.psn
        report "Wrong rx_rdma_header.bth.psn value"
        severity error;
      assert rx_rdma_header.reth.virtual_address = exp_rdma_header.reth.virtual_address
        report "Wrong rx_rdma_header.reth.virtual_address value"
        severity error;
      assert rx_rdma_header.reth.dma_length = exp_rdma_header.reth.dma_length
        report "Wrong rx_rdma_header.reth.dma_length value"
        severity error;
      assert rx_rdma_header.immediate_data = exp_rdma_header.immediate_data
        report "Wrong rx_rdma_header.immediate_data value"
        severity error;

    end loop;

    proc_common_wait_some_cycles(dp_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  u_dut: entity work.rdma_packetiser_assemble_header
    generic map (
      g_data_w => g_data_w
    )
    port map (
      dp_clk             => dp_clk,
      dp_rst             => dp_rst,
      mm_clk             => mm_clk,
      mm_rst             => mm_rst,

      reg_hdr_dat_copi   => reg_hdr_dat_copi,
      reg_hdr_dat_cipo   => reg_hdr_dat_cipo,

      snk_in             => snk_in,
      snk_out            => snk_out,

      src_out            => src_out,
      src_in             => src_in,

      block_len          => block_len
    );

  -------------------------------------------------------------------------------
  -- Header for first packets or write only without immediate data
  -------------------------------------------------------------------------------
  u_dp_offload_first: entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams => 1,
    g_data_w => g_data_w,
    g_symbol_w => c_byte_w,
    g_hdr_field_arr => c_rdma_packetiser_first_hdr_field_arr
  )
  port map (
    dp_clk                => dp_clk,
    dp_rst                => dp_rst,
    mm_clk                => mm_clk,
    mm_rst                => mm_rst,
    snk_in_arr(0)         => src_out,
    hdr_fields_raw_arr(0) => first_hdr_fields_arr
  );

  -------------------------------------------------------------------------------
  -- Header for middle or last without immediate data (no RETH, no immediate data)
  -------------------------------------------------------------------------------
  u_dp_offload_mid: entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams => 1,
    g_data_w => g_data_w,
    g_symbol_w => c_byte_w,
    g_hdr_field_arr => c_rdma_packetiser_mid_hdr_field_arr
  )
  port map (
    dp_clk                => dp_clk,
    dp_rst                => dp_rst,
    mm_clk                => mm_clk,
    mm_rst                => mm_rst,
    snk_in_arr(0)         => src_out,
    hdr_fields_raw_arr(0) => mid_hdr_fields_arr
  );

  -------------------------------------------------------------------------------
  -- Header for last packets with immediate data
  -------------------------------------------------------------------------------
  u_dp_offload_last: entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams => 1,
    g_data_w => g_data_w,
    g_symbol_w => c_byte_w,
    g_hdr_field_arr => c_rdma_packetiser_last_hdr_field_arr
  )
  port map (
    dp_clk                => dp_clk,
    dp_rst                => dp_rst,
    mm_clk                => mm_clk,
    mm_rst                => mm_rst,
    snk_in_arr(0)         => src_out,
    hdr_fields_raw_arr(0) => last_hdr_fields_arr
  );

  -------------------------------------------------------------------------------
  -- Header for write only packets with immediate data
  -------------------------------------------------------------------------------
  u_dp_offload_wo: entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams => 1,
    g_data_w => g_data_w,
    g_symbol_w => c_byte_w,
    g_hdr_field_arr => c_rdma_packetiser_wo_hdr_field_arr
  )
  port map (
    dp_clk                => dp_clk,
    dp_rst                => dp_rst,
    mm_clk                => mm_clk,
    mm_rst                => mm_rst,
    snk_in_arr(0)         => src_out,
    src_out_arr(0)        => sop_sosi,
    hdr_fields_raw_arr(0) => wo_hdr_fields_arr
  );
end tb;
