-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Test bench parameters for the JESD204B IP.
-- Description:
-- Remark:
-- . From:
--   - registermap-jesd-rx.xlsx
--   - JESD204B Intel® FPGA IP User Guide, Updated for Intel® Quartus® Prime
--     Design Suite: 19.4, IP Version: 19.2.0
-------------------------------------------------------------------------------

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;

package tech_jesd204b_pkg is
  -- Intel ARRIA10 IP MM interface
  -- . JESD204B MM has 256 bytes = 64 words, so span_w = 6 would be enough,
  --   but using span_w = 8 and span = 256 words is fine too.
  constant tech_jesd204b_port_span_w             : natural := 8;
  constant tech_jesd204b_port_span               : natural := 2**tech_jesd204b_port_span_w;

  constant tech_jesd204b_field_rx_syncn_sysref_ctrl_adr             : natural := 16#54# / 4;
  constant tech_jesd204b_field_rx_syncn_sysref_ctrl_link_reinit     : natural := 1;
  constant tech_jesd204b_field_rx_syncn_sysref_ctrl_sysref_singled  : natural := 2;
  constant tech_jesd204b_field_rx_syncn_sysref_ctrl_sysref_alwayson : natural := 4;

  constant tech_jesd204b_field_rx_err0_adr       : natural := 16#60# / 4;
  constant tech_jesd204b_field_rx_err0_w         : natural := 9;
  constant tech_jesd204b_field_rx_err0_lo        : natural := 0;
  constant tech_jesd204b_field_rx_err0_hi        : natural := tech_jesd204b_field_rx_err0_w - 1 + tech_jesd204b_field_rx_err0_lo;
  constant tech_jesd204b_field_rx_err1_adr       : natural := 16#64# / 4;
  constant tech_jesd204b_field_rx_err1_w         : natural := 10;
  constant tech_jesd204b_field_rx_err1_lo        : natural := 0;
  constant tech_jesd204b_field_rx_err1_hi        : natural := tech_jesd204b_field_rx_err1_w - 1 + tech_jesd204b_field_rx_err1_lo;

  constant tech_jesd204b_field_rx_err_enable_adr        : natural := 16#74# / 4;
  constant tech_jesd204b_field_rx_err_enable_reset      : integer := 16#17F9FE#;
  constant tech_jesd204b_field_rx_err_link_reinit_adr   : natural := 16#78# / 4;
  constant tech_jesd204b_field_rx_err_link_reinit_reset : integer := 16#3902#;

  constant tech_jesd204b_field_csr_rbd_count_adr : natural := 16#80# / 4;
  constant tech_jesd204b_field_csr_rbd_count_w   : natural := 8;
  constant tech_jesd204b_field_csr_rbd_count_lo  : natural := 3;
  constant tech_jesd204b_field_csr_rbd_count_hi  : natural := tech_jesd204b_field_csr_rbd_count_w - 1 + tech_jesd204b_field_csr_rbd_count_lo;
  constant tech_jesd204b_field_csr_dev_syncn_adr : natural := 16#80# / 4;
  constant tech_jesd204b_field_csr_dev_syncn_w   : natural := 1;
  constant tech_jesd204b_field_csr_dev_syncn_lo  : natural := 0;
  constant tech_jesd204b_field_csr_dev_syncn_hi  : natural := tech_jesd204b_field_csr_dev_syncn_w - 1 + tech_jesd204b_field_csr_dev_syncn_lo;

  constant tech_jesd204b_field_rx_ilas_csr_l_adr         : natural := 16#25#;  -- = 16#94# / 4;
  constant tech_jesd204b_field_rx_ilas_csr_f_adr         : natural := 16#25#;
  constant tech_jesd204b_field_rx_ilas_csr_k_adr         : natural := 16#25#;
  constant tech_jesd204b_field_rx_ilas_csr_m_adr         : natural := 16#25#;

  constant tech_jesd204b_field_rx_ilas_csr_n_adr         : natural := 16#26#;  -- = 16#98# / 4;
  constant tech_jesd204b_field_rx_ilas_csr_cs_adr        : natural := 16#26#;
  constant tech_jesd204b_field_rx_ilas_csr_np_adr        : natural := 16#26#;
  constant tech_jesd204b_field_rx_ilas_csr_subclassv_adr : natural := 16#26#;
  constant tech_jesd204b_field_rx_ilas_csr_s_adr         : natural := 16#26#;
  constant tech_jesd204b_field_rx_ilas_csr_jesdv_adr     : natural := 16#26#;
  constant tech_jesd204b_field_rx_ilas_csr_cf_adr        : natural := 16#26#;
  constant tech_jesd204b_field_rx_ilas_csr_hd_adr        : natural := 16#26#;

  constant tech_jesd204b_field_rx_status1_adr    : natural := 16#21#;
  constant tech_jesd204b_field_rx_status2_adr    : natural := 16#22#;
  constant tech_jesd204b_field_rx_status3_adr    : natural := 16#23#;
  constant tech_jesd204b_field_rx_status4_adr    : natural := 16#3c#;
  constant tech_jesd204b_field_rx_status5_adr    : natural := 16#3d#;
  constant tech_jesd204b_field_rx_status6_adr    : natural := 16#3e#;
  constant tech_jesd204b_field_rx_status7_adr    : natural := 16#3f#;
end tech_jesd204b_pkg;

package body tech_jesd204b_pkg is
end tech_jesd204b_pkg;
