-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose: Adapt from ready latency 1 to 0 to make a look ahead FIFO
-- Description: -
-- Remark:
-- . Derived from dp_latency_adapter.vhd.
-- . There is no need for a rd_emp output signal, because a show ahead FIFO
--   will have rd_val='0' when it is empty.

entity common_fifo_rd is
  generic (
    g_dat_w : natural := 18
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    -- ST sink: RL = 1
    fifo_req   : out std_logic;
    fifo_dat   : in  std_logic_vector(g_dat_w - 1 downto 0);
    fifo_val   : in  std_logic := '0';
    -- ST source: RL = 0
    rd_req     : in  std_logic;
    rd_dat     : out std_logic_vector(g_dat_w - 1 downto 0);
    rd_val     : out std_logic
  );
end common_fifo_rd;

architecture wrap of common_fifo_rd is
begin
  u_rl0 : entity work.common_rl_decrease
  generic map (
    g_adapt       => true,
    g_dat_w       => g_dat_w
  )
  port map (
    rst           => rst,
    clk           => clk,
    -- ST sink: RL = 1
    snk_out_ready => fifo_req,
    snk_in_dat    => fifo_dat,
    snk_in_val    => fifo_val,
    -- ST source: RL = 0
    src_in_ready  => rd_req,
    src_out_dat   => rd_dat,
    src_out_val   => rd_val
  );
end wrap;
