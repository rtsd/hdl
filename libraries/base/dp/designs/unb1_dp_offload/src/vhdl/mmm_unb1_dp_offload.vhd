-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . MM master for all MM peripherals in the unb1_dp_offload design
-- Description:
-- . Synthesis: a NIOS II (running app unb_osy) -based SOPC is instantiated:
--   . Cloks are generated by the PLL in the SOPC
--   . Access to MM buses via 1GbE and unb_osy application
--   . Application unb_osy initializes the 1GbE module
--   . Python controls/monitors MM peripherals by sending/receiving packets
-- . Simulation: an mm_file instance in instantiated for each MM bus:
--   . Clocks are generated by non-synthesizable VHDL
--   . Access to MM buses via file I/O
--   . Python controls/monitors MM peripherals by writing/reading files
--   . Additional sim.ctrl and sim.stat files are used for non-functional
--     Python<->ModelSim interaction such as getting the simulation time.
--   . g_sim_unb_nr and g_sim_node_nr are passed to the MM file instances so
--     Python can target the files using the same board + node arguments as
--     used to target hardware (--unb # --fn # --bn #)

library IEEE, common_lib, unb1_board_lib, mm_lib, eth_lib, tech_tse_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use eth_lib.eth_pkg.all;
use common_lib.common_network_layers_pkg.all;

entity mmm_unb1_dp_offload is
  generic (
    g_sim           : boolean := false;  -- FALSE: use SOPC; TRUE: use mm_file I/O
    g_sim_unb_nr    : natural := 0;
    g_sim_node_nr   : natural := 0;
    g_nof_streams   : natural;
    g_bg_block_size : natural;
    g_hdr_field_arr : t_common_field_arr
  );
  port (
    xo_clk                         : in  std_logic;
    xo_rst_n                       : in  std_logic;
    xo_rst                         : in  std_logic;

    mm_rst                         : in  std_logic;
    mm_clk                         : out std_logic;
    mm_locked                      : out std_logic;

    dp_clk                         : out std_logic;

    pout_wdi                       : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi                   : out t_mem_mosi;
    reg_wdi_miso                   : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi       : out t_mem_mosi;
    reg_unb_system_info_miso       : in  t_mem_miso;
    rom_unb_system_info_mosi       : out t_mem_mosi;
    rom_unb_system_info_miso       : in  t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi              : out t_mem_mosi;
    reg_unb_sens_miso              : in  t_mem_miso;

    -- eth1g
    eth1g_tse_clk                  : out std_logic;
    eth1g_mm_rst                   : out std_logic;
    eth1g_tse_mosi                 : out t_mem_mosi;
    eth1g_tse_miso                 : in  t_mem_miso;
    eth1g_reg_mosi                 : out t_mem_mosi;
    eth1g_reg_miso                 : in  t_mem_miso;
    eth1g_reg_interrupt            : in  std_logic;
    eth1g_ram_mosi                 : out t_mem_mosi;
    eth1g_ram_miso                 : in  t_mem_miso;

    reg_dp_offload_tx_hdr_dat_mosi : out t_mem_mosi;
    reg_dp_offload_tx_hdr_dat_miso : in  t_mem_miso;

    reg_dp_offload_rx_hdr_dat_mosi : out t_mem_mosi;
    reg_dp_offload_rx_hdr_dat_miso : in  t_mem_miso;

    reg_bsn_monitor_mosi           : out t_mem_mosi;
    reg_bsn_monitor_miso           : in  t_mem_miso;

    ram_diag_data_buf_mosi         : out t_mem_mosi;
    ram_diag_data_buf_miso         : in  t_mem_miso;

    reg_diag_data_buf_mosi         : out t_mem_mosi;
    reg_diag_data_buf_miso         : in  t_mem_miso;

    ram_diag_bg_mosi               : out t_mem_mosi;
    ram_diag_bg_miso               : in  t_mem_miso;
    reg_diag_bg_mosi               : out t_mem_mosi;
    reg_diag_bg_miso               : in  t_mem_miso
  );
end mmm_unb1_dp_offload;

architecture str of mmm_unb1_dp_offload is
  -- Clocks
  signal i_mm_clk                                  : std_logic := '1';
  signal i_tse_clk                                 : std_logic := '1';
  signal i_dp_clk                                  : std_logic := '1';
  signal i_cal_rec_clk                             : std_logic := '1';

  -- Block generator
  constant c_ram_diag_bg_addr_w                    : natural := ceil_log2(g_nof_streams * pow2(ceil_log2(g_bg_block_size)));

  -- dp_offload
  constant c_reg_dp_offload_tx_adr_w               : natural := 1;  -- Dev note: add to c_unb1_board_peripherals_mm_reg_default
  constant c_reg_dp_offload_tx_multi_adr_w         : natural := ceil_log2(g_nof_streams * pow2(c_reg_dp_offload_tx_adr_w));

  constant c_reg_dp_offload_tx_hdr_dat_nof_words   : natural := field_nof_words(g_hdr_field_arr, c_word_w);
  constant c_reg_dp_offload_tx_hdr_dat_adr_w       : natural := ceil_log2(c_reg_dp_offload_tx_hdr_dat_nof_words);
  constant c_reg_dp_offload_tx_hdr_dat_multi_adr_w : natural := ceil_log2(g_nof_streams * pow2(c_reg_dp_offload_tx_hdr_dat_adr_w));

  constant c_reg_dp_offload_rx_hdr_dat_nof_words   : natural := field_nof_words(g_hdr_field_arr, c_word_w);
  constant c_reg_dp_offload_rx_hdr_dat_adr_w       : natural := ceil_log2(c_reg_dp_offload_rx_hdr_dat_nof_words);
  constant c_reg_dp_offload_rx_hdr_dat_multi_adr_w : natural := ceil_log2(g_nof_streams * pow2(c_reg_dp_offload_rx_hdr_dat_adr_w));

  -- BSN monitors
  constant c_reg_rsp_bsn_monitor_adr_w             : natural := ceil_log2(g_nof_streams * pow2(c_unb1_board_peripherals_mm_reg_default.reg_bsn_monitor_adr_w));

  -- Simulation
  constant c_mm_clk_period                         : time := 8 ns;
  constant c_tech_tse_clk_period                        : time := 8 ns;
  constant c_dp_clk_period                         : time := 5 ns;

  constant c_sim_node_type                         : string(1 to 2) := sel_a_b(g_sim_node_nr < 4, "FN", "BN");
  constant c_sim_node_nr                           : natural := sel_a_b(c_sim_node_type = "BN", g_sim_node_nr - 4, g_sim_node_nr);

  constant c_sim_eth_src_mac                       : std_logic_vector(c_network_eth_mac_slv'range) := X"00228608" & TO_UVEC(g_sim_unb_nr, c_byte_w) & TO_UVEC(g_sim_node_nr, c_byte_w);
  constant c_sim_eth_control_rx_en                 : natural := 2**c_eth_mm_reg_control_bi.rx_en;

  signal sim_eth_mm_bus_switch                     : std_logic;
  signal sim_eth_psc_access                        : std_logic;

  signal i_eth1g_reg_mosi                          : t_mem_mosi;
  signal i_eth1g_reg_miso                          : t_mem_miso;

  signal sim_eth1g_reg_mosi                        : t_mem_mosi;
begin
  mm_clk        <= i_mm_clk;
  eth1g_tse_clk <= i_tse_clk;
  dp_clk        <= i_dp_clk;

  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $HDL_IOFILE_SIM_DIR.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    i_dp_clk      <= not i_dp_clk      after c_dp_clk_period / 2;
    i_mm_clk      <= not i_mm_clk      after c_mm_clk_period / 2;
    i_tse_clk     <= not i_tse_clk     after c_tech_tse_clk_period / 2;

    mm_locked     <= '0', '1' after c_mm_clk_period * 5;
    eth1g_mm_rst  <= '1', '0' after c_tech_tse_clk_period * 5;

    eth1g_ram_mosi <= c_mem_mosi_rst;

    u_mm_file_reg_unb_system_info       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                                     port map(mm_rst, i_mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                                     port map(mm_rst, i_mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi                   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                                     port map(mm_rst, i_mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_unb_sens              : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
                                                     port map(mm_rst, i_mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso );

    u_mm_file_reg_eth                   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                                     port map(mm_rst, i_mm_clk, i_eth1g_reg_mosi, eth1g_reg_miso );

    u_mm_file_reg_diag_bg               : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG")
                                                     port map(mm_rst, i_mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

    u_mm_file_ram_diag_bg               : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_BG")
                                                     port map(mm_rst, i_mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

    u_mm_file_reg_dp_offload_tx_hdr_dat : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_TX_HDR_DAT")
                                                     port map(mm_rst, i_mm_clk, reg_dp_offload_tx_hdr_dat_mosi, reg_dp_offload_tx_hdr_dat_miso );

    u_mm_file_reg_dp_offload_rx_hdr_dat : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_OFFLOAD_RX_HDR_DAT")
                                                     port map(mm_rst, i_mm_clk, reg_dp_offload_rx_hdr_dat_mosi, reg_dp_offload_rx_hdr_dat_miso );

    u_mm_file_reg_bsn_monitor           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR")
                                                     port map(mm_rst, i_mm_clk, reg_bsn_monitor_mosi, reg_bsn_monitor_miso );

    u_mm_file_ram_diag_data_buffer      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUF")
                                                     port map(mm_rst, i_mm_clk, ram_diag_data_buf_mosi, ram_diag_data_buf_miso);

    u_mm_file_reg_diag_data_buffer      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUF")
                                                     port map(mm_rst, i_mm_clk, reg_diag_data_buf_mosi, reg_diag_data_buf_miso);

    ----------------------------------------------------------------------------
    -- 1GbE setup sequence normally performed by unb_os@NIOS
    ----------------------------------------------------------------------------
    p_eth_setup : process
    begin
      sim_eth_mm_bus_switch <= '1';

      eth1g_tse_mosi.wr <= '0';
      eth1g_tse_mosi.rd <= '0';
      wait for 400 ns;
      wait until rising_edge(i_mm_clk);
      proc_tech_tse_setup(c_tech_stratixiv, false, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tech_tse_tx_ready_latency, c_sim_eth_src_mac, sim_eth_psc_access, i_mm_clk, eth1g_tse_miso, eth1g_tse_mosi);
      -- Enable RX
      proc_mem_mm_bus_wr(c_eth_reg_control_wi + 0, c_sim_eth_control_rx_en, i_mm_clk, eth1g_reg_miso, sim_eth1g_reg_mosi);  -- control rx en
      sim_eth_mm_bus_switch <= '0';

      wait;
    end process;

    p_switch : process(sim_eth_mm_bus_switch, sim_eth1g_reg_mosi, i_eth1g_reg_mosi)
    begin
      if sim_eth_mm_bus_switch = '1' then
          eth1g_reg_mosi <= sim_eth1g_reg_mosi;
        else
          eth1g_reg_mosi <= i_eth1g_reg_mosi;
        end if;
    end process;

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  ----------------------------------------------------------------------------
  -- SOPC for synthesis
  ----------------------------------------------------------------------------
  gen_sopc : if g_sim = false generate
    u_sopc : entity work.sopc_unb1_dp_offload
    port map (
      clk_0                                                   => xo_clk,  -- 25 MHz from ETH_clk pin
      reset_n                                                 => xo_rst_n,
      mm_clk                                                  => i_mm_clk,  -- 125 MHz system clock
      tse_clk                                                 => i_tse_clk,  -- PLL clk[2] = 125 MHz calibration clock for the TSE
      dp_clk                                                  => i_dp_clk,
      cal_reconf_clk                                          => OPEN,

       -- the_altpll_0
      locked_from_the_altpll_0                                => mm_locked,
      phasedone_from_the_altpll_0                             => OPEN,
      areset_to_the_altpll_0                                  => xo_rst,

      -- the_avs_eth_0
      coe_clk_export_from_the_avs_eth_0                       => OPEN,
      coe_reset_export_from_the_avs_eth_0                     => eth1g_mm_rst,
      coe_tse_address_export_from_the_avs_eth_0               => eth1g_tse_mosi.address(c_tech_tse_byte_addr_w - 1 downto 0),
      coe_tse_write_export_from_the_avs_eth_0                 => eth1g_tse_mosi.wr,
      coe_tse_writedata_export_from_the_avs_eth_0             => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      coe_tse_read_export_from_the_avs_eth_0                  => eth1g_tse_mosi.rd,
      coe_tse_readdata_export_to_the_avs_eth_0                => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      coe_tse_waitrequest_export_to_the_avs_eth_0             => eth1g_tse_miso.waitrequest,
      coe_reg_address_export_from_the_avs_eth_0               => eth1g_reg_mosi.address(c_eth_reg_addr_w - 1 downto 0),
      coe_reg_write_export_from_the_avs_eth_0                 => eth1g_reg_mosi.wr,
      coe_reg_writedata_export_from_the_avs_eth_0             => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      coe_reg_read_export_from_the_avs_eth_0                  => eth1g_reg_mosi.rd,
      coe_reg_readdata_export_to_the_avs_eth_0                => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      coe_irq_export_to_the_avs_eth_0                         => eth1g_reg_interrupt,
      coe_ram_address_export_from_the_avs_eth_0               => eth1g_ram_mosi.address(c_eth_ram_addr_w - 1 downto 0),
      coe_ram_write_export_from_the_avs_eth_0                 => eth1g_ram_mosi.wr,
      coe_ram_writedata_export_from_the_avs_eth_0             => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      coe_ram_read_export_from_the_avs_eth_0                  => eth1g_ram_mosi.rd,
      coe_ram_readdata_export_to_the_avs_eth_0                => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),

      -- the_reg_unb_sens
      coe_clk_export_from_the_reg_unb_sens                    => OPEN,
      coe_reset_export_from_the_reg_unb_sens                  => OPEN,
      coe_address_export_from_the_reg_unb_sens                => reg_unb_sens_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_unb_sens                   => reg_unb_sens_mosi.rd,
      coe_readdata_export_to_the_reg_unb_sens                 => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_unb_sens                  => reg_unb_sens_mosi.wr,
      coe_writedata_export_from_the_reg_unb_sens              => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_debug_wave
      out_port_from_the_pio_debug_wave                        => OPEN,

      -- the_pio_system_info: actually a avs_common_mm instance
      coe_clk_export_from_the_pio_system_info                 => OPEN,
      coe_reset_export_from_the_pio_system_info               => OPEN,
      coe_address_export_from_the_pio_system_info             => reg_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_pio_system_info                => reg_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_pio_system_info              => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_system_info               => reg_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_pio_system_info           => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_rom_system_info
      coe_clk_export_from_the_rom_system_info                 => OPEN,
      coe_reset_export_from_the_rom_system_info               => OPEN,
      coe_address_export_from_the_rom_system_info             => rom_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_rom_system_info                => rom_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_rom_system_info              => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_rom_system_info               => rom_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_rom_system_info           => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_wdi
      out_port_from_the_pio_wdi                               => pout_wdi,

      -- the_reg_wdi
      coe_clk_export_from_the_reg_wdi                         => OPEN,
      coe_reset_export_from_the_reg_wdi                       => OPEN,
      coe_address_export_from_the_reg_wdi                     => reg_wdi_mosi.address(0),
      coe_read_export_from_the_reg_wdi                        => reg_wdi_mosi.rd,
      coe_readdata_export_to_the_reg_wdi                      => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_wdi                       => reg_wdi_mosi.wr,
      coe_writedata_export_from_the_reg_wdi                   => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_diag_bg
      coe_address_export_from_the_reg_diag_bg                 => reg_diag_bg_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_bg_adr_w - 1 downto 0),
      coe_clk_export_from_the_reg_diag_bg                     => OPEN,
      coe_read_export_from_the_reg_diag_bg                    => reg_diag_bg_mosi.rd,
      coe_readdata_export_to_the_reg_diag_bg                  => reg_diag_bg_miso.rddata(c_word_w - 1 downto 0),
      coe_reset_export_from_the_reg_diag_bg                   => OPEN,
      coe_write_export_from_the_reg_diag_bg                   => reg_diag_bg_mosi.wr,
      coe_writedata_export_from_the_reg_diag_bg               => reg_diag_bg_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_ram_diag_bg
      coe_address_export_from_the_ram_diag_bg                 => ram_diag_bg_mosi.address(c_ram_diag_bg_addr_w - 1 downto 0),
      coe_clk_export_from_the_ram_diag_bg                     => OPEN,
      coe_read_export_from_the_ram_diag_bg                    => ram_diag_bg_mosi.rd,
      coe_readdata_export_to_the_ram_diag_bg                  => ram_diag_bg_miso.rddata(c_word_w - 1 downto 0),
      coe_reset_export_from_the_ram_diag_bg                   => OPEN,
      coe_write_export_from_the_ram_diag_bg                   => ram_diag_bg_mosi.wr,
      coe_writedata_export_from_the_ram_diag_bg               => ram_diag_bg_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dp_offload_tx_hdr_dat
      coe_address_export_from_the_reg_dp_offload_tx_hdr_dat   => reg_dp_offload_tx_hdr_dat_mosi.address(c_reg_dp_offload_tx_hdr_dat_multi_adr_w - 1 downto 0),
      coe_clk_export_from_the_reg_dp_offload_tx_hdr_dat       => OPEN,
      coe_read_export_from_the_reg_dp_offload_tx_hdr_dat      => reg_dp_offload_tx_hdr_dat_mosi.rd,
      coe_readdata_export_to_the_reg_dp_offload_tx_hdr_dat    => reg_dp_offload_tx_hdr_dat_miso.rddata(c_word_w - 1 downto 0),
      coe_reset_export_from_the_reg_dp_offload_tx_hdr_dat     => OPEN,
      coe_write_export_from_the_reg_dp_offload_tx_hdr_dat     => reg_dp_offload_tx_hdr_dat_mosi.wr,
      coe_writedata_export_from_the_reg_dp_offload_tx_hdr_dat => reg_dp_offload_tx_hdr_dat_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_dp_offload_rx_hdr_dat
      coe_address_export_from_the_reg_dp_offload_rx_hdr_dat   => reg_dp_offload_rx_hdr_dat_mosi.address(c_reg_dp_offload_rx_hdr_dat_multi_adr_w - 1 downto 0),
      coe_clk_export_from_the_reg_dp_offload_rx_hdr_dat       => OPEN,
      coe_read_export_from_the_reg_dp_offload_rx_hdr_dat      => reg_dp_offload_rx_hdr_dat_mosi.rd,
      coe_readdata_export_to_the_reg_dp_offload_rx_hdr_dat    => reg_dp_offload_rx_hdr_dat_miso.rddata(c_word_w - 1 downto 0),
      coe_reset_export_from_the_reg_dp_offload_rx_hdr_dat     => OPEN,
      coe_write_export_from_the_reg_dp_offload_rx_hdr_dat     => reg_dp_offload_rx_hdr_dat_mosi.wr,
      coe_writedata_export_from_the_reg_dp_offload_rx_hdr_dat => reg_dp_offload_rx_hdr_dat_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_bsn_monitor
      coe_address_export_from_the_reg_bsn_monitor             => reg_bsn_monitor_mosi.address(c_reg_rsp_bsn_monitor_adr_w - 1 downto 0),
      coe_clk_export_from_the_reg_bsn_monitor                 => OPEN,
      coe_read_export_from_the_reg_bsn_monitor                => reg_bsn_monitor_mosi.rd,
      coe_readdata_export_to_the_reg_bsn_monitor              => reg_bsn_monitor_miso.rddata(c_word_w - 1 downto 0),
      coe_reset_export_from_the_reg_bsn_monitor               => OPEN,
      coe_write_export_from_the_reg_bsn_monitor               => reg_bsn_monitor_mosi.wr,
      coe_writedata_export_from_the_reg_bsn_monitor           => reg_bsn_monitor_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_ram_diag_data_buffer
      coe_address_export_from_the_ram_diag_data_buffer        => ram_diag_data_buf_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_diag_db_adr_w - 1 downto 0),
      coe_clk_export_from_the_ram_diag_data_buffer            => OPEN,
      coe_read_export_from_the_ram_diag_data_buffer           => ram_diag_data_buf_mosi.rd,
      coe_readdata_export_to_the_ram_diag_data_buffer         => ram_diag_data_buf_miso.rddata(c_word_w - 1 downto 0),
      coe_reset_export_from_the_ram_diag_data_buffer          => OPEN,
      coe_write_export_from_the_ram_diag_data_buffer          => ram_diag_data_buf_mosi.wr,
      coe_writedata_export_from_the_ram_diag_data_buffer      => ram_diag_data_buf_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_reg_diag_data_buffer
      coe_address_export_from_the_reg_diag_data_buffer        => reg_diag_data_buf_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      coe_clk_export_from_the_reg_diag_data_buffer            => OPEN,
      coe_read_export_from_the_reg_diag_data_buffer           => reg_diag_data_buf_mosi.rd,
      coe_readdata_export_to_the_reg_diag_data_buffer         => reg_diag_data_buf_miso.rddata(c_word_w - 1 downto 0),
      coe_reset_export_from_the_reg_diag_data_buffer          => OPEN,
      coe_write_export_from_the_reg_diag_data_buffer          => reg_diag_data_buf_mosi.wr,
      coe_writedata_export_from_the_reg_diag_data_buffer      => reg_diag_data_buf_mosi.wrdata(c_word_w - 1 downto 0)
    );
  end generate;
end str;
