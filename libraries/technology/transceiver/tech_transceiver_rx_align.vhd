--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tech_transceiver_rx_align is
  generic(
    g_sim    : boolean;
    g_data_w : natural;
    g_mbps   : natural
  );
  port(
    rx_clk             : in  std_logic;
    rx_rst             : in  std_logic;
    rx_align_en_in     : in  std_logic;
    rx_align_en_out    : out std_logic;
    rxc_rx_aligned     : out std_logic;
    rx_state           : out std_logic_vector(1 downto 0)
   );
end tech_transceiver_rx_align;

architecture rtl of tech_transceiver_rx_align is
  constant c_rx_clk_freq : natural := (g_mbps / g_data_w) / 10 * 8 * 1000000;

  constant c_timeout_1ms : natural := c_rx_clk_freq / 1000;
  constant c_timeout_sim : natural := 100;
  constant c_timeout     : natural := sel_a_b(g_sim, c_timeout_sim, 200 * c_timeout_1ms);
  constant c_timeout_w   : natural := ceil_log2(c_timeout);

  type t_state_enum is (s_init, s_wait, s_word_align, s_deassert_word_align, s_byte_order, s_deassert_byte_order, s_rx_aligned);

  signal state                    : t_state_enum;
  signal nxt_state                : t_state_enum;

  signal i_rx_align_en_out        : std_logic;
  signal nxt_rx_align_en_out      : std_logic;

  signal i_rxc_rx_aligned         : std_logic;
  signal nxt_rxc_rx_aligned       : std_logic;

  signal cycle_cnt                : std_logic_vector(c_timeout_w - 1 downto 0);
  signal nxt_cycle_cnt            : std_logic_vector(c_timeout_w - 1 downto 0);

  signal nxt_rx_state             : std_logic_vector(1 downto 0);
  signal i_rx_state               : std_logic_vector(1 downto 0);
begin
  rx_state <= i_rx_state;

  rx_align_en_out <= i_rx_align_en_out;
  rxc_rx_aligned  <= i_rxc_rx_aligned;

  p_clk : process(rx_rst, rx_clk)
  begin
    if rx_rst = '1' then
      state                <= s_init;
      i_rx_align_en_out    <= '0';
      i_rxc_rx_aligned     <= '0';
      cycle_cnt            <= (others => '0');
      i_rx_state           <= (others => '0');
    elsif rising_edge(rx_clk) then
      state                <= nxt_state;
      i_rx_align_en_out    <= nxt_rx_align_en_out;
      i_rxc_rx_aligned     <= nxt_rxc_rx_aligned;
      cycle_cnt            <= nxt_cycle_cnt;
      i_rx_state           <= nxt_rx_state;
    end if;
  end process;

  p_state : process(state, i_rx_align_en_out, i_rxc_rx_aligned, cycle_cnt, i_rx_state, rx_align_en_in)
  begin
    nxt_state           <= state;
    nxt_rx_align_en_out <= i_rx_align_en_out;
    nxt_rxc_rx_aligned  <= i_rxc_rx_aligned;
    nxt_cycle_cnt       <= cycle_cnt;
    nxt_rx_state        <= i_rx_state;

    case state is
      when s_init =>
        nxt_state <= s_wait;

      when s_wait =>
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if cycle_cnt = TO_UVEC(c_timeout, c_timeout_w) then
           nxt_state     <= s_word_align;
           nxt_cycle_cnt <= (others => '0');
        end if;

      when s_word_align =>  -- First assertion of rx_align_en to perform word alignment
        nxt_rx_state        <= "01";
        nxt_rx_align_en_out <= '1';
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if cycle_cnt = TO_UVEC(c_timeout, c_timeout_w) then
           nxt_state     <= s_deassert_word_align;
           nxt_cycle_cnt <= (others => '0');
        end if;

      when s_deassert_word_align =>  -- Deasssert rx_align_en, re-assert after delay
        nxt_rx_align_en_out <= '0';
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if cycle_cnt = TO_UVEC(c_timeout, c_timeout_w) then
           nxt_state     <= s_byte_order;
           nxt_cycle_cnt <= (others => '0');
        end if;

      when s_byte_order =>  -- Assert rx_align_en a second time to order the bytes
        nxt_rx_state        <= "10";
        nxt_rx_align_en_out <= '1';
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if cycle_cnt = TO_UVEC(c_timeout, c_timeout_w) then
           nxt_state     <= s_deassert_byte_order;
           nxt_cycle_cnt <= (others => '0');
        end if;

     when s_deassert_byte_order =>  -- Deasssert rx_align_en
        nxt_rx_align_en_out <= '0';
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if cycle_cnt = TO_UVEC(c_timeout, c_timeout_w) then
           nxt_state  <= s_rx_aligned;
           nxt_cycle_cnt <= (others => '0');
        end if;

      when s_rx_aligned =>  -- Asserting rx_aligned on next cycle.
        nxt_rx_state        <= "11";
        nxt_rx_align_en_out <= '0';
        nxt_rxc_rx_aligned  <= '1';
        if rx_align_en_in = '1' then
          nxt_state           <= s_wait;
          nxt_rxc_rx_aligned  <= '0';
        end if;

      when others =>
        nxt_state <= s_init;
    end case;
  end process;
end rtl;
