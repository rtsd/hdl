---------------------------------------------------------------------------------
--
--   Vhdl file created by I/O Designer
--   Fri Feb 28 17:51:25 2014
--
---------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity jtag_top is
    port (
        CTRL : in std_logic_vector(1 downto 0);
        ENABLE_MSP : in std_logic;
        IDN : in std_logic_vector(3 downto 0);
        LPSEL : in std_logic_vector(4 downto 0);
        MSPTCK : out std_logic_vector(4 downto 0);
        MSPTDI : in std_logic_vector(4 downto 0);
        MSPTDO : out std_logic_vector(4 downto 0);
        MSPTMS : out std_logic_vector(4 downto 0);
        MSPTRST : out std_logic_vector(4 downto 0);
        TCK : in std_logic;
        TDI : in std_logic;
        TDO : out std_logic;
        TMS : in std_logic;
        TRST : in std_logic
    );
end jtag_top;
