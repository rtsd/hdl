-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, ddr3_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use ddr3_lib.ddr3_pkg.all;

entity bn_capture_storage_reg is
  generic (
    g_ddr             : t_c_ddr3_phy
 );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk            : in  std_logic;  -- memory-mapped bus clock
    st_rst            : in  std_logic;  -- reset synchronous with st_clk
    st_clk            : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in            : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out           : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    st_en_evt         : out std_logic;
    st_wr_not_rd      : out std_logic;

    st_start_addr     : out t_ddr3_addr;
    st_end_addr       : out t_ddr3_addr;

    st_done           : in  std_logic;
    st_init_done      : in  std_logic;
    st_ctlr_rdy       : in  std_logic;

    -- MM registers
    mm_rd_usedw       : in  std_logic_vector(31 downto 0)
   );
end bn_capture_storage_reg;

architecture rtl of bn_capture_storage_reg is
  constant c_mm_reg        : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(8),
                                         dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                         nof_dat  => 8,
                                         init_sl  => '0');
  -- Registers in mm_clk domain
  signal mm_en_evt         : std_logic;
  signal mm_wr_not_rd      : std_logic;

  signal mm_start_address  : std_logic_vector(31 downto 0);
  signal mm_end_address    : std_logic_vector(31 downto 0);

  signal st_start_address  : std_logic_vector(31 downto 0);
  signal st_end_address    : std_logic_vector(31 downto 0);

  signal mm_done           : std_logic;
  signal mm_init_done      : std_logic;
  signal mm_ctlr_rdy       : std_logic;
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out   <= c_mem_miso_rst;
      -- Write access, register values
      mm_wr_not_rd  <= '0';
      mm_start_address <= (others => '0');
      mm_end_address   <= (others => '0');

    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write event defaults
      mm_en_evt     <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Write Block Sync
          when 0 =>
            mm_en_evt        <= sla_in.wrdata(0);
          when 1 =>
            mm_wr_not_rd     <= sla_in.wrdata(0);
          when 5 =>
            mm_start_address <= sla_in.wrdata(31 downto 0);
          when 6 =>
            mm_end_address   <= sla_in.wrdata(31 downto 0);
          when others => null;  -- unused MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Read Block Sync
          when 2 =>
            sla_out.rddata(0)           <= mm_done;
          when 3 =>
            sla_out.rddata(0)           <= mm_init_done;
          when 4 =>
            sla_out.rddata(0)           <= mm_ctlr_rdy;
          when 7 =>
            sla_out.rddata(31 downto 0) <= mm_rd_usedw;
          when others => null;  -- unused MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and st_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the safe side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  u_spulse_en_evt : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_en_evt,
    in_busy   => OPEN,
    out_rst   => st_rst,
    out_clk   => st_clk,
    out_pulse => st_en_evt
  );

  u_async_wr_not_rd : entity common_lib.common_async
  generic map (
    g_rst_level => '0'
  )
  port map (
    rst  => st_rst,
    clk  => st_clk,
    din  => mm_wr_not_rd,
    dout => st_wr_not_rd
  );

  u_async_done : entity common_lib.common_async
  generic map (
    g_rst_level => '0'
  )
  port map (
    rst  => mm_rst,
    clk  => mm_clk,
    din  => st_done,
    dout => mm_done
  );

  u_async_init_done : entity common_lib.common_async
  generic map (
    g_rst_level => '0'
  )
  port map (
    rst  => mm_rst,
    clk  => mm_clk,
    din  => st_init_done,
    dout => mm_init_done
  );

  u_async_rdy : entity common_lib.common_async
  generic map (
    g_rst_level => '0'
  )
  port map (
    rst  => mm_rst,
    clk  => mm_clk,
    din  => st_ctlr_rdy,
    dout => mm_ctlr_rdy
  );

  -- Address range should be set before asserting the DDR3 enable bit.

  -- t_ddr3_addr record contents:
  --   chip      : STD_LOGIC_VECTOR(0 DOWNTO 0);
  --   bank      : STD_LOGIC_VECTOR(2 DOWNTO 0);
  --   row       : STD_LOGIC_VECTOR(14 DOWNTO 0);
  --   column    : STD_LOGIC_VECTOR(9 DOWNTO 0);

  st_start_addr.chip(ceil_log2(g_ddr.cs_w)  - 1 downto 0) <= st_start_address(28 downto 28);
  st_start_addr.bank(          g_ddr.ba_w   - 1 downto 0) <= st_start_address(27 downto 25);
  st_start_addr.row(           g_ddr.a_row_w - 1 downto 0) <= st_start_address(24 downto 10);
  st_start_addr.column(        g_ddr.a_col_w - 1 downto 0) <= st_start_address(9  downto 0);

  st_end_addr.chip(ceil_log2(g_ddr.cs_w)  - 1 downto 0) <= st_end_address(28 downto 28);
  st_end_addr.bank(          g_ddr.ba_w   - 1 downto 0) <= st_end_address(27 downto 25);
  st_end_addr.row(           g_ddr.a_row_w - 1 downto 0) <= st_end_address(24 downto 10);
  st_end_addr.column(        g_ddr.a_col_w - 1 downto 0) <= st_end_address(9  downto 0);

  u_cross_domain_end_addr : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_new      => '1',
      in_dat      => mm_end_address,
      in_done     => OPEN,
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => st_end_address,
      out_new     => open
    );

  u_cross_domain_start_addr : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_new      => '1',
      in_dat      => mm_start_address,
      in_done     => OPEN,
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => st_start_address,
      out_new     => open
    );
end rtl;
