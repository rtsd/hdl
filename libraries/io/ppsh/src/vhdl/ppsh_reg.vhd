-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for ppsh
-- Description:
-- . Report PPS toggle, stable and period capture count
-- . Set dp_clk capture edge for PPS
--   Set expected period capture count for PPS stable
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |toggle[31], stable[30]   xxx                        capture_cnt = [n:0]|  0
--  |-----------------------------------------------------------------------|
--  |edge[31],                xxx                       expected_cnt = [n:0]|  1
--  |-----------------------------------------------------------------------|
--  |                         xxx                         offset_cnt = [n:0]|  2
--  |-----------------------------------------------------------------------|

-- Info from L2SDP-78 ticket.
-- Add a new offset_cnt field to the PPSH register that reports the current capture_cnt value at the moment that this MM read access occurs.
-- The offset_cnt reports the time since last PPSH in units of the dp_clk, so 5 ns (at 200MHz). The host can use this offset_cnt value to
-- determine the alignment between its local Time of Day and the PPS in the FPGA.
--
-- The offset_cnt needs to be supported in the pi_ppsh.py and util_ppsh.py. Please also use the option --rep to quickly repeat the reading
-- of the PPSH registers. This is useful to show that the offset_cnt increases and then restarts after every new PPS. The PPS in the FPGA is
-- also represented by the pps_toggle field in the PPSH registers.
--
-- The PPSH is part of unb2_minimal. To fit the PPSH register the span in QSYS and in the mmm file needs to be increased from 2 words to 4 words.
--
-- Also prepare unb2c_minimal by updating the PPSH register span there.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity ppsh_reg is
  generic (
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_st_clk_freq        : natural := 200 * 10**6  -- clock frequency of st_clk in Hz
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk            : in  std_logic;  -- memory-mapped bus clock
    st_rst            : in  std_logic;  -- reset synchronous with st_clk
    st_clk            : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in            : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out           : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    st_pps_toggle     : in  std_logic;
    st_pps_stable     : in  std_logic;
    st_capture_cnt    : in  std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);  -- counts the number of clk clock cycles between subsequent pps_ext pulses
    st_offset_cnt     : in  std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);  -- counts the number of clk clock cycles between now and last pps_ext pulse

    st_pps_stable_ack : out std_logic;

    st_capture_edge   : out std_logic;
    st_expected_cnt   : out std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0)  -- expected number of clk clock cycles between subsequent pps_ext pulses
 );
end ppsh_reg;

architecture rtl of ppsh_reg is
  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => ceil_log2(4),
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => 4,
                                  init_sl  => '0');

  -- Register access control signal in mm_clk domain
  signal mm_pps_stable_ack   : std_logic;

  -- Registers in mm_clk domain
  signal mm_pps_toggle     : std_logic;
  signal mm_pps_stable     : std_logic;
  signal mm_capture_edge   : std_logic;

  signal mm_capture_cnt    : std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);
  signal mm_expected_cnt   : std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);
  signal mm_offset_cnt     : std_logic_vector(ceil_log2(g_st_clk_freq) - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out                  <= c_mem_miso_rst;

      -- Access event, register values
      mm_pps_stable_ack    <= '0';
      mm_capture_edge      <= '0';  -- default rising edge
      mm_expected_cnt      <= TO_UVEC(g_st_clk_freq, mm_expected_cnt'length);

    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Access event defaults
      mm_pps_stable_ack <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 1 =>
            -- Write PPSH control
            mm_capture_edge         <= sla_in.wrdata(31);
            mm_expected_cnt         <= sla_in.wrdata(mm_expected_cnt'range);
          when others => null;  -- not used MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            -- Read PPSH status
            mm_pps_stable_ack           <= '1';
            sla_out.rddata(31)          <= mm_pps_toggle;
            sla_out.rddata(30)          <= mm_pps_stable;
            sla_out.rddata(29 downto 0) <= RESIZE_UVEC(mm_capture_cnt, 30);
          when 1 =>
            -- Read back PPSH control
            sla_out.rddata(31)          <= mm_capture_edge;
            sla_out.rddata(29 downto 0) <= RESIZE_UVEC(mm_expected_cnt, 30);
          when 2 =>
            -- Read PPSH offset count
            sla_out.rddata(29 downto 0) <= RESIZE_UVEC(mm_offset_cnt, 30);
          when others => null;  -- not used MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and st_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the save side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  no_cross : if g_cross_clock_domain = false generate  -- so mm_clk = st_clk
    mm_pps_toggle      <= st_pps_toggle;
    mm_pps_stable      <= st_pps_stable;
    mm_capture_cnt     <= st_capture_cnt;
    mm_offset_cnt      <= st_offset_cnt;

    st_pps_stable_ack  <= mm_pps_stable_ack;

    st_capture_edge    <= mm_capture_edge;
    st_expected_cnt    <= mm_expected_cnt;
  end generate;  -- no_cross

  gen_cross : if g_cross_clock_domain = true generate
    -- ST --> MM
    u_pps_toggle : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_pps_toggle,
      dout => mm_pps_toggle
    );

    u_pps_stable : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_pps_stable,
      dout => mm_pps_stable
    );

    u_capture_cnt : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_capture_cnt,
      in_done     => OPEN,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_capture_cnt,
      out_new     => open
    );

    u_offset_cnt : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_offset_cnt,
      in_done     => OPEN,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_offset_cnt,
      out_new     => open
    );

    -- MM --> ST
    u_pps_stable_ack : entity common_lib.common_spulse
    port map (
      in_rst    => mm_rst,
      in_clk    => mm_clk,
      in_pulse  => mm_pps_stable_ack,
      in_busy   => OPEN,
      out_rst   => st_rst,
      out_clk   => st_clk,
      out_pulse => st_pps_stable_ack
    );

    u_capture_edge : entity common_lib.common_async
    generic map (
      g_rst_level => '0'
    )
    port map (
      rst  => st_rst,
      clk  => st_clk,
      din  => mm_capture_edge,
      dout => st_capture_edge
    );

    u_expected_cnt : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_dat      => mm_expected_cnt,
      in_done     => OPEN,
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => st_expected_cnt,
      out_new     => open
    );
  end generate;  -- gen_cross
end rtl;
