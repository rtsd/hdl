// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HS2^H.6HA[*UPQ9=_0*5"3]8G2,^&,!*EA>6BJ=(94P$*GI??*]S%MP  
H#(="W_8^X<HI$Y#JY+0HWRAIQ_U"9BW;(5\.+ D\O$F!,&%]<-"$*P  
HTNBD%@ZL[&E2Q,C;)>N"/.O-.UGA)!K@(?]BA$B716$?J>9S:CNEO   
HB/[I%*]WW0/&2H]#AO]-_2"V)@HR#);/;A&A*0HS[ KLJ/QHUFQ3T0  
HB$SYA2+5$?D"#S9^XRIZIC48PE@3H6$GG3+VS2FBLBFN%,A>T29<T@  
`pragma protect encoding=(enctype="uuencode",bytes=1584        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@8"4? HW!U^U*#(64#N)%C+#=9_FM@AHUK4) \ZM185@ 
@]0"D:M_G/TWC>'+P)1^0"8&P%_T_0QP;+\1(&>6=!UT 
@.=!M?9G^5S<;+-<IY==^[NJ%J[/E6LC<K8Z-U1:T,C< 
@5?ED<P2B;QCT=;AY$)W)6S\\WQJX541_[?Z0<#=2YM< 
@Q9*W=,WF:*XX.G3"U.#D>Z1H[/$2VJZ%+\=BUV32PU$ 
@CI?VDJ4#J/8!7_IOQXL-=( ;3DE()Y)1&Y'X<%=RW 0 
@!Y%=-SSTC_K -5_&8T9XPMQ5.6I1)IV'3!I+#^Y1GH( 
@:&LIVYGV1UC_,^WTY1;T=6TPD$O9%TRVD2O6037$7X0 
@B&^T+M,UL?UJ3O5MZERI:1?ZA;&]H*-Z7-XKM^.9[58 
@;9YJ(Y-[>40(-O?9H1"9K15XE#7V6_0#Z*.'\QK+\7@ 
@DEA]$X'FM8%D4N'_0']'UVLAK[#7;]50T.UB2*Q$))( 
@FII["#,F-B&#A_DFZUM<!6"&[_8ZI>L8II/&J[U?O!P 
@)TP']5*N4-RB_RR1'V5D_X#G:5P&WR)4^+D@@T83MXP 
@\'$N]:1W-58I.BM>C1Q56*Z<8'\>V;HBD9F]&JW^&0@ 
@&94!1UPEC.*]P^Q<5,?R?7)XXSKR*,TF=(*S!!<CM_D 
@7F#JV@*9=R5W&Z?)C6I1,]#W#PPL]O<RA8#X4Q^_WWH 
@527JOZ6#=W)BH%ZN>X\"4)V-%2HX:M!:":H^LL/:45( 
@:M&O*EZK?^+G'\#SJ<6%W$,M[P'^N0V04(='6AO;I9< 
@I#*^T$IZ!9G;6IE'@HJC@\%;E(=Z\EB(L FZNWRLEP8 
@%6/0WFC3O& 3G##6QH9$:N4#0FZGJ'>$0<^:R#PC W8 
@]?X&!Q<9D:[HL*.GV6#3O/AIJ[Q=613<:F@.B&\69%X 
@H.WS5QP U:ED783RQ$^G ?KA2,]Z/!-18JA>A%,C=[0 
@2$ U@=!^Y[M''.M',- Y\$];;%9,IN%'JS742&<_ZT8 
@)>5=K<->5/LV===5G!2-I-I'34S@N@K&.Z*CMOY[%4@ 
@#;A6^&H:->F999?J!<6#/FW8NWERJD[\*=Y&AY8955$ 
@*]LTOCD% ?A%&IR0;B4%/ROKV0+O 8602[J#D!6$R$@ 
@6Z./&8'('_./79NA6EJ#N,#H^0S0;Y*_NKD0(A<(4VL 
@:XMKD2_QW[/HI2V( @TZJ_==&Y@U7W@ICA_!4"-GADL 
@Z 9CM8H66XR[-8H!:^T[ Q"(616JQ <6BD.$W8JX<X< 
@-&, \N>WLL9"UC%;\]:AE6H*X9OVM/-LP[DH9.+5L%D 
@4.GW>.7X7A2XT)++5S@6-&4F;40U\^'#_]JC+ ::)2, 
@L%@#GF9"19M@H2MEA5\W+O?#2$R! ^*D+X3LX6/*\P0 
@:O-I?\TKV?FR@:&6&!9,43)GT"(L\-7'4BF,_]LC@@4 
@(FT4[V??$&'KAM!,X'FTU3*+G]C-B$@(>POY4';M^'0 
@0<(;+G7@8@OA'QZPTZXGW""ZR6]SW&11"'X+2>F[SV, 
@5I.Z(ZG&6P,"6+%LO0EY6_@CNG<R.]]T&/J6IS(].)P 
@YV(9WB$X1JBZ=%&[N%#!:.9E[TW^#;MDSRBJ2/Q?X'  
@&(/&= >XX)1<*2<](''P--$;SDH.TK+A;2WW";GJ#*H 
@(P?K T#WA@27W56)\+U:)3P&#)3X,11;6>E$$G67GIT 
@4,$BNP^I^0V]<FU$VVU31WT4/ <RS%2T-)J1% SK-ST 
@ZOH)9F)<96Y9LRU&3?5,TG+KZC$I<YF:>R4'FX77'%T 
@Z!0]Q_/Z$6KS-^% C:32S .LRB$,ML(;<LZR!Y5Q<2( 
@A+:)B.C$$R',"(@2*TGXQYP\*O8(@Q]I9+]S&?7N/8L 
@@^JPS7;Z2Q#HS^#%=RX'10Y<?==RE%SX:B#@QO9%K)4 
@'#,&V'_R5<BR60Z+=274<_J=<R^XL+F795_F]'B!V]@ 
@!0IC8B26<H09> EU.H"-;3V@1=@DKTQ"!Z#1C%)Q%<\ 
@^"MBJ4XR%O04 /)?2H1ERI&>@L/Z\#6@6OU#T?Q5-#T 
@GGO\A&'R@7X6AQ]GD521"9D>N#F7IWQ:I>-],:.YHVT 
@H7087U"1L@ WD7G:QB.\I=[;OS5.,*S68EN7/+)@;H4 
0TAHIY__#]RHFG&&Z \2=]0  
`pragma protect end_protected
