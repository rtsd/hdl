-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

architecture recursive of common_adder_tree is
  -- common_add_sub pipelining
  constant c_pipeline_in  : natural := 0;
  constant c_pipeline_out : natural := g_pipeline;

  constant c_nof_h1       : natural :=                g_nof_inputs / 2;  -- lower half
  constant c_nof_h2       : natural := g_nof_inputs - g_nof_inputs / 2;  -- upper half

  -- The h1 branch needs an extra dummy stage when c_nof_h1 is a power of 2 AND c_nof_h2=c_nof_h1+1
  function func_stage_h1(h1, h2 : natural) return boolean is
    variable v_ret : boolean := false;
  begin
    if h1 > 1 then
      if h1 = 2**ceil_log2(h1) and h2 = h1 + 1 then
        v_ret := true;
      end if;
    end if;
    return v_ret;
  end;

  constant c_stage_h1     : boolean := func_stage_h1(c_nof_h1, c_nof_h2);

  constant c_sum_w        : natural := g_dat_w + ceil_log2(g_nof_inputs);  -- internally work with worst case bit growth
  constant c_sum_h_w      : natural := c_sum_w - 1;
  constant c_sum_h1_w     : natural := sel_a_b(c_stage_h1, c_sum_h_w - 1, c_sum_h_w);
  constant c_sum_h2_w     : natural :=                                  c_sum_h_w;

  component common_adder_tree is
    generic (
      g_representation : string;
      g_pipeline       : natural;
      g_nof_inputs     : natural;
      g_dat_w          : natural;
      g_sum_w          : natural
    );
    port (
      clk    : in  std_logic;
      clken  : in  std_logic := '1';
      in_dat : in  std_logic_vector(g_nof_inputs * g_dat_w - 1 downto 0);
      sum    : out std_logic_vector(             g_sum_w - 1 downto 0)
    );
  end component;

  -- The FOR ALL the instance label must be at level 1, it can not point within a generate statement because then it gets at level 2.
  --FOR ALL : common_adder_tree USE ENTITY work.common_adder_tree(recursive);

  signal sum_h1     : std_logic_vector(c_sum_h1_w - 1 downto 0);
  signal sum_h1_reg : std_logic_vector(c_sum_h2_w - 1 downto 0);
  signal sum_h2     : std_logic_vector(c_sum_h2_w - 1 downto 0);

  signal result     : std_logic_vector(c_sum_w - 1 downto 0);
begin
  leaf_pipe : if g_nof_inputs = 1 generate
    u_reg : entity work.common_pipeline
    generic map (
      g_representation => g_representation,
      g_pipeline       => g_pipeline,
      g_in_dat_w       => g_dat_w,
      g_out_dat_w      => g_dat_w + 1
    )
    port map (
      clk     => clk,
      clken   => clken,
      in_dat  => in_dat,
      out_dat => result
    );
  end generate;

  leaf_add : if g_nof_inputs = 2 generate
    u_add : entity work.common_add_sub
    generic map (
      g_direction       => "ADD",
      g_representation  => g_representation,
      g_pipeline_input  => c_pipeline_in,
      g_pipeline_output => c_pipeline_out,
      g_in_dat_w        => g_dat_w,
      g_out_dat_w       => c_sum_w
    )
    port map (
      clk     => clk,
      clken   => clken,
      in_a    => in_dat(  g_dat_w - 1 downto 0      ),
      in_b    => in_dat(2 * g_dat_w - 1 downto g_dat_w),
      result  => result
    );
  end generate;

  gen_tree : if g_nof_inputs > 2 generate
    u_h1 : common_adder_tree
    generic map (
      g_representation => g_representation,
      g_pipeline       => g_pipeline,
      g_nof_inputs     => c_nof_h1,
      g_dat_w          => g_dat_w,
      g_sum_w          => c_sum_h1_w
    )
    port map (
      clk    => clk,
      clken  => clken,
      in_dat => in_dat(c_nof_h1 * g_dat_w - 1 downto 0),
      sum    => sum_h1
    );

    u_h2 : common_adder_tree
    generic map (
      g_representation => g_representation,
      g_pipeline       => g_pipeline,
      g_nof_inputs     => c_nof_h2,
      g_dat_w          => g_dat_w,
      g_sum_w          => c_sum_h2_w
    )
    port map (
      clk    => clk,
      clken  => clken,
      in_dat => in_dat(g_nof_inputs * g_dat_w - 1 downto c_nof_h1 * g_dat_w),
      sum    => sum_h2
    );

    no_reg_h1 : if c_stage_h1 = false generate
      sum_h1_reg <= sum_h1;
    end generate;

    gen_reg_h1 : if c_stage_h1 = true generate
      u_reg_h1 : entity work.common_pipeline
      generic map (
        g_representation => g_representation,
        g_pipeline       => g_pipeline,
        g_in_dat_w       => c_sum_h1_w,
        g_out_dat_w      => c_sum_h2_w
      )
      port map (
        clk     => clk,
        clken   => clken,
        in_dat  => sum_h1,
        out_dat => sum_h1_reg
      );
    end generate;

    trunk_add : entity work.common_add_sub
    generic map (
      g_direction       => "ADD",
      g_representation  => g_representation,
      g_pipeline_input  => c_pipeline_in,
      g_pipeline_output => c_pipeline_out,
      g_in_dat_w        => c_sum_h_w,
      g_out_dat_w       => c_sum_w
    )
    port map (
      clk     => clk,
      clken   => clken,
      in_a    => sum_h1_reg,
      in_b    => sum_h2,
      result  => result
    );
  end generate;

  sum <= RESIZE_SVEC(result, g_sum_w) when g_representation = "SIGNED" else RESIZE_UVEC(result, g_sum_w);
end recursive;
