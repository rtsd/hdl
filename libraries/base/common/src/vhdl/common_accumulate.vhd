-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Description:
--   Signed accumulator of valid input data. The accumlator gets reloaded with
--   valid in_dat or set to 0 when there is no valid in_dat when sload is
--   active.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common_pkg.all;

entity common_accumulate is
  generic (
    g_representation  : string  := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    rst     : in  std_logic := '0';
    clk     : in  std_logic;
    clken   : in  std_logic := '1';
    sload   : in  std_logic;
    in_val  : in  std_logic := '1';
    in_dat  : in  std_logic_vector;
    out_dat : out std_logic_vector
  );
end common_accumulate;

architecture rtl of common_accumulate is
 constant c_acc_w : natural := out_dat'length;

 signal result : std_logic_vector(c_acc_w - 1 downto 0);
begin
  process(rst, clk)
  begin
    if rst = '1' then
      result <= (others => '0');
    elsif rising_edge(clk) then
      if clken = '1' then
        if sload = '1' then
          result <= (others => '0');
          if in_val = '1' then
            if g_representation = "SIGNED" then
              result <= RESIZE_SVEC(in_dat, c_acc_w);
            else
              result <= RESIZE_UVEC(in_dat, c_acc_w);
            end if;
          end if;
        elsif in_val = '1' then
          if g_representation = "SIGNED" then
            result <= std_logic_vector(  signed(result) +   signed(RESIZE_SVEC(in_dat, c_acc_w)));
          else
            result <= std_logic_vector(unsigned(result) + unsigned(RESIZE_UVEC(in_dat, c_acc_w)));
          end if;
        end if;
      end if;
    end if;
  end process;

  out_dat <= result;
end rtl;
