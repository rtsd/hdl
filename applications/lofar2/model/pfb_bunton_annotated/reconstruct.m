%reconstruction of signal after polyphase filterbank
% John Bunton created Dec 2005
%
% modified to add variables ImpulseResponse and Correct
% Impulse Response = 1 calculates the impulse response of the system
% the result is in variable outData.  It also causes the transfer function
% correction filter to be calculated, Result in variable correct
% Correct = 1 applies the correction filter to the reconstructed signal
% outData
% John Bunton 2015
%
% Usage from README.txt:
% . first run Gen_filter12 to create prototype lowpass filter cl
% . then run reconstruct with ImpulseResponse = 0 and Correct = 0 to filter
%   the inData
% . to correct the overall response, rerun reconstruct with ImpulseResponse =
%   1 and Correct = 1 to calculate an impulse response and a correction filter
%   for the analysis PFB and synthesis PFB transfer function error
% . then rerun reconstruct with ImpulseResponse = 0 and Correct = 1 to also
%   apply the correction filtering
% . use rng('default') to check SNR results, comment rng() to experiment with
%   new noise input every rerun

close all
format longE

% Uncomment rng() line or run rng() line first in shell, to produce same random
% numbers as with MATLAB restart, to verify the SNR results for Correct = 0, 1.
rng('default')

ImpulseResponse = 0;  % set to 1 to calculate correction filter
Correct = 0;          % set to 1 to allow correction filter to be applied

% cl is the prototype lowpass filter
Ncoefs = length(cl);  % = 2304
Ntaps = 12;
Nfft = Ncoefs / Ntaps;  % = 192

% Input data
% 1) random block wave signal
% Create random block wave signal with block size Nhold = 16 samples, first
% insert 16-1 zeros between every sample in z. This yield length
% (len - 1) * 16 + 1. Then convolve with ones(16, 1) to replicate each value
% of z instead of the zeros. The length of conv(A, B) is length(A) +
% length(B) - 1, so length of inData becomes ((len - 1) * 16 + 1) + (16 - 1) =
% len * 16 = 80000.
len = 5000;
z = rand(len,1);
z = z > 0.5;
z = 2 * (z - 0.5);
clear inData
Nhold = 16;
inData(1:Nhold:len*Nhold) = z;
inData = conv(inData, ones(Nhold, 1));
k = length(inData);  % = len * Nhold

% 2) random normal noise signal
%inData = randn(1, k);  % yields similar SNR as random block wave signal

% 3) sine wave signal
%inData = cos(((1:k).^2 ) * len / (k)^2);
%inData = cos(2*pi*(1:k) * 0.031 / 16);
%inData = cos(2*pi*(0:k-1) * 1.5 / Nfft);  % use integer for CW at bin
%inData = (0:k-1);

% 4) impulse signal
if ImpulseResponse == 1
    inData = inData .* 0;  % for testing impulse response of system
    inData(k / 2) = 1;     % and allows calculation of correction filter
end

% Set up parameters for the analysis and synthesis filterbank
Ros = 32 / 24;
% fix() rounds to nearest integer towards zero.
Nstep = fix(Nfft / Ros);  % = 144

% these is the actual analysis and synthesis operations
outBins = polyphase_analysis(inData, cl, Nfft, Nstep);
outData = polyphase_synthesis_b(outBins, cl, Nstep);

% adjust gain
% . assume unit gain in prototype LPF Gen_filter12.m
% . adjust for 1/Nfft in synthesis IFFT and adjust for 1/Nup in synthesis
%   interpolation, with analysis Ndown = synthesis Nup = Nstep.
adjust = Nfft * Nstep;
outData = adjust * outData;

if ImpulseResponse == 1
    % correction filter to fix frequency response error, also
    % corrects gain because input inData has unit level
    correct = real(ifft(1./fft(outData)));
end

% Correct for error in transfer function
if Correct == 1
    % Choose parameter k to suit oversampling ratio
    % oversampling ratio 32/23 then k=0
    % oversampling ratio 32/24 then k=39
    % oversampling ratio 32/25 then k=72
    % oversampling ratio 32/26 then k=3
    % oversampling ratio 32/27 then k=48
    k=39;
    corr = correct;
    % Correct for time shift for transfer function correction filter
    corr(1+k:length(corr)) = corr(1:length(corr)-k);
    outData = ifft(fft(outData) .* fft(fftshift(corr)));
end

% Take the difference to see the error
diff = outData - inData(1:length(outData));

% SNR
% First do rng('default') to reset the random number generator, then run
% reconstruct. The original code by John Bunton for the random block wave wave
% input signal then yields:
% . rng('default'); reconstruct, with Correct = 0: SNR = 30.56 dB
% . rng('default'); reconstruct, with Correct = 1: SNR = 62.24 dB
% Note:
% . With the impulse data input the SNR = 304.46 dB, so perfect.
% . For other input data there is still some error when Correct = 1, due to
%   the limited stopband attenuation of the prototype LPF (can be seen using
%   DEVS = 0.003 instead of 0.0003 in Gen_filter12).
% . When Correct = 0 then the error is caused largely by the passband ripple
%   DEVP = 0.02 and W0 = 1 / 15.3 of the prototype LPF, and db(0.02) = -34 dB.
%   With DEVP = 0.005 and W0 = 1 / 15.05 then db(0.005) = -46.0 dB and SNR =
%   38.8 dB, so increased.
SNR = 10 * log10(sum(abs(inData(10000:70000) .^ 2)) / sum(abs(diff(10000:70000) .^ 2)));
SNR = 20 * log10(std(inData(10000:70000)) / std(diff(10000:70000)));
sprintf('SNR = %.5f [dB]', SNR)

%plot(diff)
%plot(db(fft(outData))) %to see frequency error
%plot(db(fft(diff(length(inData)/8:7*length(inData)/8))))

figure(1)
hold off
plot(db(fft(inData(10000:70000)/30000)),'r')
hold on
plot(db(fft(diff(10000:70000)/30000)),'b')
grid
title('Spectrum of input and error')

figure(2)
plot(diff)
title('Difference between input and reconstructed')

figure(3)
hold off
plot(inData(1:10000))
hold on
title('Input and Reconstructed signal')
if (ImpulseResponse == 1)
    plot(outData)
else
    plot(outData(1:10000),'r')
end
