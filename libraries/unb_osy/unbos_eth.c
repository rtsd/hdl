/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

/* Compile switches. */

/* includes */
#include <unbos_eth.h>
#include <unbos_i2c.h>
#include <osx_stdio.h>
#include <sys/alt_irq.h>
#include <altera_avalon_pio_regs.h>
#include <unbosx_eth_regs.h>
#include <io2_copy.h>

TUInt32                 netIP       = 0;
TUInt32                 netIP_nwbo  = 0;
volatile TUInt32        eth_status  = 0;
volatile TUInt32        eth_frmInfo = 0;
TUInt32 volatile* const rx          = (TUInt32 volatile* const)(AVS_ETH_0_MMS_RAM_BASE+ETH_RX_RAM_BASE);
TUInt32 volatile* const tx          = (TUInt32 volatile* const)(AVS_ETH_0_MMS_RAM_BASE+ETH_TX_RAM_BASE);


void UNBOSX_NET_Send(unsigned int nbyte) {
	TUInt32      words, empty, ctrl = AVS_ETH_REG_CONTROL_RX_EN_BIT_MASK;

	// assume outsize is already including 2byte padding
	words = nbyte/4;
	empty = (4 - nbyte%4);

	// If the packet extends into a word (not filling it)
	// we must inc the number of words and set the number
	// of empty bytes accordingly
	words += ((empty==4)?0:1);
	empty  = ((empty==4)?0:empty);

	// prepare the new control-register-variable
	ctrl |= (words << AVS_ETH_REG_CONTROL_TX_NOF_WORDS_BIT_OFST);
	ctrl |= (empty << AVS_ETH_REG_CONTROL_TX_EMPTY_BIT_OFST);
	ctrl |= AVS_ETH_REG_CONTROL_TX_EN_BIT_MASK;

    // Now write new value of control register and the continue reg
    WRREG(AVS_ETH_REG_CONTROL_WI, ctrl);
	  // clear global status
  	eth_status  = 0;
  	eth_frmInfo = 0;
    WRREG(AVS_ETH_REG_CONTINUE_WI, 0);
}

void UNBOSX_NET_Discard() {
    // Now write new value of control register and the continue reg
    WRREG(AVS_ETH_REG_CONTROL_WI, AVS_ETH_REG_CONTROL_RX_EN_BIT_MASK);
	  // clear global status
  	eth_status  = 0;
  	eth_frmInfo = 0;
    WRREG(AVS_ETH_REG_CONTINUE_WI, 0);
}

#if 0
// IPv4 address is at maximum 4*3 digits + 3 dots + 1 NUL character = 16 characters
char const* NET_inet_ntoa(const TUInt32 ipv4addr) {
	static char                buffer[16];
	unsigned const char* const ip = (unsigned const char* const)(&ipv4addr);
	osx_sprintf(buffer, "%d.%d.%d.%d", ip[3], ip[2], ip[1], ip[0]);
	return &buffer[0];
}
#endif
#if 0
// Return the timedifference in clock_t ticks 
// between new and old.
clock_t delta_t(clock_t old_t, clock_t new_t) {
    const clock_t cClockMax = ~((~(clock_t)0)+1);
    // if new_t < old_t assume the counter wrapped
    if( new_t<old_t )
        return cClockMax - (old_t-new_t);
    else
        return new_t - old_t;
}
#endif

#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void net_isr(void* context);
#else
static void net_isr(void* context, alt_u32 id);
#endif

/* Set up the network card */
void UNBOSX_NET_Setup() {
	TUInt32            netMAChi   = UNB_ETH_SRC_MAC_BASE_HI;
	TUInt32            netMAClo   = (UNB_ETH_SRC_MAC_BASE_LO<<16);
  TUInt32            base_ip    = (TUInt32)AVS_ETH_REG_RD(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_CONFIG_WI+2); 

#if defined(COMPILE_FOR_GEN2_UNB2)
#warning "Compiling for UNB2"
	const TUInt32      node      = UNBOSX_unb2_GetNodeNumber();
	const TUInt32      backplane = UNBOSX_unb2_GetBackplaneId();
    osx_printf("backplane %d node %cN %d\n",
               backplane, 
               (UNBOSX_unb2_IsBacknode()?'B':'F'),
	       node);
#else
#warning "Compiling for UNB1"
	const TUInt32      node      = UNBOSX_GetNodeNumber();
	const TUInt32      backplane = UNBOSX_GetBackplaneId();
    osx_printf("backplane %d node %cN %d\n",
               backplane, 
               (UNBOSX_IsBacknode()?'B':'F'),
	       node);
#endif

     /* Use the ip range 10.99.xx.yy where 
     *               xx = UNB_System_Info.bck_id [backplane ID]
     *               yy = (UNB_System_Info.id + 1)
     *                    [0-3] = frontnode
     *                    [4-7] = backnode
     */
    if (base_ip==0) {
      netIP      = (TUInt32)0x0a630000 + (backplane<<8) + node + 1;
    } else {
      netIP      = base_ip + (backplane<<8) + node + 1;
    }

	  netIP_nwbo = UNBOSX_UTIL_htonl(netIP);
    netMAClo   = (UNB_ETH_SRC_MAC_BASE_LO<<16) + (backplane<<8) + node;

    /* Now it's time to actually set things up */
    UNBOSX_ETH_Init_ISR(net_isr, 0);
    UNBOSX_ETH_Setup(UNBOSX_G_SIM(),
              netMAChi, netMAClo, ETH_FRAME_LENGTH,
              netIP, 5000 /*UNB_UDP_PORT_CTRL*/, 0, 0, 0);

    osx_printf("setup_tse: ip=%d.%d.%d.%d\n",
                netIP>>24, (netIP>>16)&0xff, (netIP>>8)&0xff, netIP&0xff);

    // and switch on rx_enable
	WRREG(AVS_ETH_REG_CONTROL_WI, AVS_ETH_REG_CONTROL_RX_EN_BIT_MASK);
	WRREG(AVS_ETH_REG_CONTINUE_WI, 0);
    return;
}

/* If it is an IPv4-ARP packet then word 4 in the rx buffer
 * should look like this in NetworkByteOrder.
 * This can be precomputed such that the ISR doesn't have
 * to swap bytes */
static const TUInt32 IPv4Arp_nwbo   = (UNBOSX_UTIL_htonl(((1<<16)+0x0800)));
static const TUInt32 ArpReqMsk_nwbo = (UNBOSX_UTIL_htonl(0x0000ffff));
static const TUInt32 ArpReqOne_nwbo = (UNBOSX_UTIL_htonl(1));

#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void net_isr(void* context) {
#else
static void net_isr(void* context, alt_u32 id) {
#endif
	/*
	 * If the ctrl reg at some point becomes !0 this indicates
	 * that WE (the ISR) have handled the packet - so we write
	 * it back to the hardware
	 * If the frmInfo != 0 at the end of the ISR, this means
	 * that that the user has to deal with the packet and 
	 * we don't do nothing
	 */
	TUInt32          ctrl = 0;
	volatile TUInt32 tmp;

    eth_status  = RDREG(AVS_ETH_REG_STATUS_WI);
	eth_frmInfo = (TUInt32)RDREG(AVS_ETH_REG_FRAME_WI); 
	// If there was in incoming packet, check if we deal with it *now*
    if( RXAVAIL(eth_status) ) {
        // If someone was looking for US (ARP request with us as target)
        // then handle it here rather than passing it up the chain
		TUInt32  isARP   = NET_FLAGCHECK(eth_frmInfo, IS_ARP_BIT_MASK);
		TUInt32  isICMP  = NET_FLAGCHECK(eth_frmInfo, IS_ICMP_BIT_MASK);


        // Only look at IP ARP requests
        // Note: do not check the MAC_ADDRESS_MATCH_BIT_MASK in the
        //       status reg - ARP requests are typically eth broadcasts.
        //       If the ARP-IP-REQUEST-FOR-OUR-IP did not come from
        //       an ethernet broadcast ... who cares!
		if( isARP ) {
            // Check if:
			// - It *is* an ARP request AND
			// - Target Protocol == IPv4 AND
			// - Target Protocol Address == Our IP Address
          
		   	// We recognized this packet as ARP - it may or may
			// not be for us that's not important right now	
			// We *always* handle these packets - so we *always* must
			// write the control register since either:
			//  * we want to send the cooked reply, or,
			//  * we want to discard it and immediately go back to
			//    receiving the next packet
			// This is done by setting the RX_ENABLE bit.
			// The TX enable bit will be set conditionally
			ctrl = AVS_ETH_REG_CONTROL_RX_EN_BIT_MASK;

			if( (RDRX(4)==IPv4Arp_nwbo) &&
				((RDRX(5)&ArpReqMsk_nwbo)==ArpReqOne_nwbo) &&
				(RDRX(10)==netIP_nwbo) ) {
                // Great. Someone was loox0ring just for US.
                // The hardware (thanks Eric!) has prepared the answer
                // already in the TX buffer. Set it off and let the rest
                // of the system forget about this pakkit
                // send whatever is in the TX buffer
                // The ip arp packet is exactly 11 32bit words long 
                //   (so make sure the "empty" bit field contains 0)
				ctrl |= (11 << AVS_ETH_REG_CONTROL_TX_NOF_WORDS_BIT_OFST);
				ctrl &= ~AVS_ETH_REG_CONTROL_TX_EMPTY_BIT_MASK;
				ctrl |= AVS_ETH_REG_CONTROL_TX_EN_BIT_MASK;
            }
			eth_frmInfo = 0;
			eth_status  = 0;
        }
		
        // Or ICMP ECHO? directed at us
		if( isICMP ) {
			// Packet is ICMP - update stats. It may or may not be for us
			// but that doesn't matter right now
			// Indicate we already dealt with this'un
			
			// Again, as with the ARP thingy, we *always* tell the h/w
			// what to do next. Sending the precooked reply is optional.
			//
			// Sending the reply depends, critically, on wether it was an ICMP echo request
			// directed at us
			ctrl = AVS_ETH_REG_CONTROL_RX_EN_BIT_MASK;

			if( NET_FLAGCHECK(eth_frmInfo, MAC_ADDRESS_MATCH_BIT_MASK) && /* our MAC, so far so good */
    	        NET_FLAGCHECK(eth_frmInfo, IP_ADDRESS_MATCH_BIT_MASK) && /* our IP! */
        	    ((UNBOSX_UTIL_ntohl(RDRX(9))>>24)==8) /* ICMP code 8 == "ECHO?" */ ) {
				// Cool. The request was and ECHO REQUEST directed directly at US
				// So 'code' becomes 0 (== ECHO REPLY)
				// But the hardware has already done that.
				// The hardware has NOT copied the payload across yet so we must do that
				// (NOTE: it _has_ already copied over the first 11 words)
				io2_copy(tx+11, rx+11, NWORD(eth_status)-11, 1, 1);

				// All that's left is to make sure that the number of 
				// bytes in the control reg are set correctly
				ctrl |= ((NWORD(eth_status)-1) << AVS_ETH_REG_CONTROL_TX_NOF_WORDS_BIT_OFST);
				ctrl |= (NEMPTY(eth_status) << AVS_ETH_REG_CONTROL_TX_EMPTY_BIT_OFST);
				ctrl |= AVS_ETH_REG_CONTROL_TX_EN_BIT_MASK;
			}
			eth_frmInfo = 0;
			eth_status  = 0;
        }
    }

    // Write a '0' in order to clear the in-tar-upt source
    WRREG(AVS_ETH_REG_STATUS_WI, 0);
    // wait until interrupt source has cleared
    do {
        tmp = RDREG(AVS_ETH_REG_STATUS_WI);
    } while( tmp!=0 );
    // Now write new value of control register and the continue reg
	// (ctrl != 0 => the h/w should continue)
	if( eth_frmInfo==0 ) {	
	    WRREG(AVS_ETH_REG_CONTROL_WI, ctrl|AVS_ETH_REG_CONTROL_RX_EN_BIT_MASK);
	    WRREG(AVS_ETH_REG_CONTINUE_WI, 0);
	}
}


/* ... */
void UNBOSX_ETH_Init_ISR(isr_func_type isr_fn, void *p_isr_context) {
	// Hook the ISR
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
	alt_ic_isr_register(AVS_ETH_0_MMS_REG_IRQ_INTERRUPT_CONTROLLER_ID,
				AVS_ETH_0_MMS_REG_IRQ,
				isr_fn,
				p_isr_context,
				(void*)0);
#else
	alt_irq_register(AVS_ETH_0_MMS_REG_IRQ, p_isr_context, isr_fn);
#endif
}

/* announce prototype */
static void UNBOSX_TSE_Setup(TBool g_sim, TUInt32 src_mac_hi, TUInt32 src_mac_lo, TUInt32 frm_length);

/*------------------------------------------------------------------------------
 * Function name:  ETH_Setup
 * Parameters:     g_sim          = When true then running in simulation, else on hardware
 *                 src_mac_hi     = 0x1234,
 *                 src_mac_lo     = 0x56789ABC for MAC address 12-34-56-78-9A-BC
 *                 frm_length     = 1518 octets for ethernet, up to 9000 for jumbo
 *                 src_ip         = 0x07060504;  -- = 07:06:05:04
 *                 udp_port_ctrl  = Control UDP port number
 *                 udp_port_data0 = Streaming data UDP port 0 number + enable bit mask
 *                 udp_port_data1 = Streaming data UDP port 1 number + enable bit mask
 *                 udp_port_data2 = Streaming data UDP port 2 number + enable bit mask
 * Return:         -
 * Description:    Set up the ETH registers, similar to p_eth_control in
 *                 modules\tse\src\vhdl\tb_eth.vhd and call TSE_Setup to set up
 *                 the TSE MAC.
 *------------------------------------------------------------------------------
 */
void UNBOSX_ETH_Setup(TBool   g_sim,
               TUInt32 src_mac_hi, TUInt32 src_mac_lo, TUInt32 frm_length, TUInt32 src_ip, TUInt32 udp_port_ctrl,
               TUInt32 udp_port_data0, TUInt32 udp_port_data1, TUInt32 udp_port_data2)
{
  // Setup the DEMUX UDP
  AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_DEMUX_WI+0, udp_port_data0);
  AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_DEMUX_WI+1, udp_port_data1);
  AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_DEMUX_WI+2, udp_port_data2);
  
  // Setup the RX CONFIG
  AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_CONFIG_WI+0, src_mac_lo);
  AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_CONFIG_WI+1, src_mac_hi);
  AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_CONFIG_WI+2, src_ip);
  AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_CONFIG_WI+3, udp_port_ctrl);
  
  // Enable RX
  AVS_ETH_REG_WR(AVS_ETH_0_MMS_REG_BASE, AVS_ETH_REG_CONTROL_WI, AVS_ETH_REG_CONTROL_RX_EN_BIT_MASK);
  
  // Setup the TSE MAC
  UNBOSX_TSE_Setup(g_sim, src_mac_hi, src_mac_lo, frm_length);
}

/*------------------------------------------------------------------------------
 * Function name:  TSE_Setup
 * Parameters:     g_sim      = When true then running in simulation, else on hardware
 *                 src_mac_hi = 0x1234,
 *                 src_mac_lo = 0x56789ABC for MAC address 12-34-56-78-9A-BC
 *                 frm_length = 1518 octets for ethernet, up to 9000 for jumbo
 * Return:         -
 * Description:    Set up the TSE MAC, similar to proc_tse_setup in
 *                 modules\tse\src\vhdl\tb_tse(pkg).vhd. The read accesses
 *                 are void, because the result is not used further.
 *------------------------------------------------------------------------------
 */
static void UNBOSX_TSE_Setup(TBool g_sim, TUInt32 src_mac_hi, TUInt32 src_mac_lo, TUInt32 frm_length) {
  TUInt16 pcs_data;
  TUInt32 mac_data;
  TUInt32 mac_0;
  TUInt32 mac_1;
  
  // PSC control
  pcs_data = AVS_ETH_TSE_PCS_RD(AVS_ETH_0_MMS_TSE_BASE, 0x22);  // REV --> 0x0901
  AVS_ETH_TSE_PCS_WR(AVS_ETH_0_MMS_TSE_BASE, 0x28, 0x0008);     // IF_MODE <-- 0x0008 = Enable 1000BASE-X gigabit mode and gigabit speed
  pcs_data = AVS_ETH_TSE_PCS_RD(AVS_ETH_0_MMS_TSE_BASE, 0x00);  // CONTROL --> 0x1140 = Gigabit speed and full duplex (RO), auto-negotiation enabled (RW)
  pcs_data = AVS_ETH_TSE_PCS_RD(AVS_ETH_0_MMS_TSE_BASE, 0x02);  // STATUS --> 0x002D
  #if ETH_LOOPBACK_PCS == FALSE
  #define LOOP_PCS  0
  #else
  #define LOOP_PCS (0x4000)
  #endif
  if (g_sim==FALSE) {
    AVS_ETH_TSE_PCS_WR(AVS_ETH_0_MMS_TSE_BASE, 0x00, 0x1140 + LOOP_PCS);  // CONTROL <-- Keep auto negotiate enabled (is reset default)
  } else {
    AVS_ETH_TSE_PCS_WR(AVS_ETH_0_MMS_TSE_BASE, 0x00, 0x0140 + LOOP_PCS);  // CONTROL <-- In simulation disable auto negotiate
  }
  
  // MAC control
  mac_data = AVS_ETH_TSE_MAC_RD(AVS_ETH_0_MMS_TSE_BASE, 0x000);   // REV --> CUST_VERSION & 0x0901
  
  // COMMAND_CONFIG bits
  // Only the bits relevant to UniBoard are explained here, others are 0
  // [    0] = TX_ENA             = 1, enable tx datapath
  // [    1] = RX_ENA             = 1, enable rx datapath
  // [    2] = XON_GEN            = 0
  // [    3] = ETH_SPEED          = 1, enable 1GbE operation
  // [    4] = PROMIS_EN          = 0, when 1 then receive all frames
  // [    5] = PAD_EN             = 0, when 1 enable receive padding removal (requires ethertype=payload length)
  // [    6] = CRC_FWD            = 1, enable receive CRC forward
  // [    7] = PAUSE_FWD          = 0
  // [    8] = PAUSE_IGNORE       = 0
  // [    9] = TX_ADDR_INS        = 0, when 1 then MAX overwrites tx SRC MAC with mac_0,1 or one of the supplemental mac
  // [   10] = HD_ENA             = 0
  // [   11] = EXCESS_COL         = 0
  // [   12] = LATE_COL           = 0
  // [   13] = SW_RESET           = 0, when 1 MAC disables tx and rx, clear statistics and flushes receive FIFO
  // [   14] = MHAS_SEL           = 0, select multicast address resolutions hash-code mode
  // [   15] = LOOP_ENA           = 0
  // [18-16] = TX_ADDR_SEL[2:0]   = 000, TX_ADDR_INS insert mac_0,1 or one of the supplemental mac
  // [   19] = MAGIC_EN           = 0
  // [   20] = SLEEP              = 0
  // [   21] = WAKEUP             = 0
  // [   22] = XOFF_GEN           = 0
  // [   23] = CNT_FRM_ENA        = 0
  // [   24] = NO_LGTH_CHECK      = 1, when 0 then check payload length of received frames (requires ethertype=payload length)
  // [   25] = ENA_10             = 0
  // [   26] = RX_ERR_DISC        = 0, when 1 then discard erroneous frames (requires store and forward mode, so rx_section_full=0)
  //                                   when 0 then pass on with rx_err[0]=1
  // [   27] = DISABLE_RD_TIMEOUT = 0
  // [30-28] = RSVD               = 000
  // [   31] = CNT_RESET          = 0, when 1 clear statistics
  #if ETH_LOOPBACK_MAC==FALSE
  #define LOOP_ENA  0
  #else
  #define LOOP_ENA (0x00008000)
  #endif
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x008, 0x0100004B + LOOP_ENA);  // COMMAND_CONFIG <--

  // The SRC_MAC address for the TSE MAC needs to be in big endian
  // Example:
  // - MAC address 12-34-56-78-9A-BC
  // - Input little endian:
  //   . src_mac_lo    = 0x56789ABC
  //   . src_mac_hi    = 0x1234
  // - Output big endian:
  //   . mac_0    = 0x78563412
  //   . mac_1    = 0xBC9A
  mac_0 = UNBOSX_UTIL_htonl(((src_mac_hi << 16) + (src_mac_lo >> 16)));
  mac_1 = UNBOSX_UTIL_htonl(src_mac_lo << 16);
  
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x00C, mac_0);  // MAC_0
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x010, mac_1);  // MAC_1
  
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x05C, 0x0000000C);  // X_IPG_LENGTH <-- interpacket gap = 12
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x014, frm_length);  // FRM_LENGTH <-- receive max frame length = 1518

  // FIFO legenda:
  // . Tx section full  = There is enough data in the FIFO to start reading it, when 0 then store and forward.
  // . Rx section full  = There is enough data in the FIFO to start reading it, when 0 then store and forward.
  // . Tx section empty = There is not much empty space anymore in the FIFO, warn user via ff_tx_septy
  // . Rx section empty = There is not much empty space anymore in the FIFO, inform remote device via XOFF flow control
  // . Tx almost full   = Assert ff_tx_a_full and deassert ff_tx_rdy. Furthermore TX_ALMOST_FULL = c_tx_ready_latency+3,
  //                      so choose 3 for zero tx ready latency
  // . Rx almost full   = Assert ff_rx_a_full and if the user is not ready ff_rx_rdy then:
  //                      --> break off the reception with an error to avoid FIFO overflow
  // . Tx almost empty  = Assert ff_tx_a_empty and if the FIFO does not contain a eop yet then:
  //                      --> break off the transmission with an error to avoid FIFO underflow
  // . Rx almost empty  = Assert ff_rx_a_empty
  // Typical FIFO values:
  // . TX_SECTION_FULL  = 16   > 8   = TX_ALMOST_EMPTY
  // . RX_SECTION_FULL  = 16   > 8   = RX_ALMOST_EMPTY
  // . TX_SECTION_EMPTY = D-16 < D-3 = Tx FIFO depth - TX_ALMOST_FULL
  // . RX_SECTION_EMPTY = D-16 < D-8 = Rx FIFO depth - RX_ALMOST_FULL
  // . c_tse_tx_fifo_depth = 1 M9K = 256*32b = 1k * 8b is sufficient when the Tx user respects ff_tx_rdy, to store a complete
  //                         ETH packet would require 1518 byte, so 2 M9K = 2k * 8b
  // . c_tse_rx_fifo_depth = 1 M9K = 256*32b = 1k * 8b is sufficient when the Rx user ff_rx_rdy is sufficiently active
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x01C,   AVS_ETH_TSE_RX_FIFO_DEPTH-16);  // RX_SECTION_EMPTY <-- default FIFO depth - 16, >3
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x020,                             16);  // RX_SECTION_FULL  <-- default 16
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x024,   AVS_ETH_TSE_TX_FIFO_DEPTH-16);  // TX_SECTION_EMPTY <-- default FIFO depth - 16, >3
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x028,                             16);  // TX_SECTION_FULL  <-- default 16, >~ 8 otherwise no tx
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x02C,                              8);  // RX_ALMOST_EMPTY  <-- default 8
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x030,                              8);  // RX_ALMOST_FULL   <-- default 8
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x034,                              8);  // TX_ALMOST_EMPTY  <-- default 8
  AVS_ETH_TSE_MAC_WR(AVS_ETH_0_MMS_TSE_BASE, 0x038, AVS_ETH_TSE_TX_READY_LATENCY+3);  // TX_ALMOST_FULL   <-- default 3

  mac_data = AVS_ETH_TSE_MAC_RD(AVS_ETH_0_MMS_TSE_BASE, 0x0E8);   // TX_CMD_STAT --> 0x00040000 : [18]=1 TX_SHIFT16, [17]=0 OMIT_CRC
  mac_data = AVS_ETH_TSE_MAC_RD(AVS_ETH_0_MMS_TSE_BASE, 0x0EC);   // RX_CMD_STAT --> 0x02000000 : [25]=1 RX_SHIFT16
}
