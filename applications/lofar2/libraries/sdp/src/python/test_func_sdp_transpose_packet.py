###############################################################################
#
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

###############################################################################

# Author: Eric Kooistra
# Date: Aug 2023
# Purpose:
#   Use Python to verify equivalent VHDL functions
#   func_sdp_transpose_beamlet_packet() and
#   func_sdp_undo_transpose_beamlet_packet() and
#   in tb_sdp_pkg.vhd
# Usage:
# > python test_func_sdp_transpose_packet.py > x
# > more x
# > tail -n 50 x

c_sdp_N_pol_bf = 2

def func_sdp_transpose_packet(nof_blocks_per_packet, nof_beamlets_per_block, packet_list):
    v_list = [0] * len(packet_list)
    for blk in range(nof_blocks_per_packet):
        for blet in range(nof_beamlets_per_block):
            for pol_bf in range(c_sdp_N_pol_bf):
                v_in = (blk * nof_beamlets_per_block + blet) * c_sdp_N_pol_bf + pol_bf
                v_out = (blet * nof_blocks_per_packet + blk) * c_sdp_N_pol_bf + pol_bf
                v_list[v_out] = packet_list[v_in]
    return v_list

nof_blocks_per_packet = 4
nof_beamlets_per_block = 488
packet_list = list(range(0, nof_beamlets_per_block * nof_blocks_per_packet * c_sdp_N_pol_bf))
out_list = func_sdp_transpose_packet(nof_blocks_per_packet, nof_beamlets_per_block, packet_list)
for d in out_list:
    print('%d' % d)
