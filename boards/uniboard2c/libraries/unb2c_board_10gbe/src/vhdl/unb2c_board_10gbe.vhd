-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, technology_lib, tech_pll_lib, tr_10GbE_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;

entity unb2c_board_10gbe is
  generic (
    g_sim                    : boolean := false;
    g_sim_level              : natural := 1;  -- 0 = use IP; 1 = use fast serdes model
    g_technology             : natural := c_tech_arria10_e2sg;
    g_nof_macs               : natural;
    g_use_loopback           : boolean := false;
    g_direction              : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_tx_fifo_fill           : natural := 10;  -- Release tx packet only when sufficiently data is available,
    g_tx_fifo_size           : natural := 256;  -- 2 * 32b * 256 = 2 M9K (DP interface has 64b data, so at least 2 M9K needed)
    g_rx_fifo_size           : natural := 256;  -- 2 * 32b * 256 = 2 M9K (DP interface has 64b data, so at least 2 M9K needed)
    g_word_alignment_padding : boolean := false
  );
  port (
    tr_ref_clk          : in  std_logic := '0';

    -- MM interface
    mm_rst              : in  std_logic;
    mm_clk              : in  std_logic;

    reg_mac_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mac_miso        : out t_mem_miso;

    reg_eth10g_mosi     : in  t_mem_mosi := c_mem_mosi_rst;  -- ETH10G (link status register)
    reg_eth10g_miso     : out t_mem_miso;

    reg_10gbase_r_24_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_10gbase_r_24_miso   : out t_mem_miso;

    -- DP interface
    dp_rst              : in  std_logic := '0';
    dp_clk              : in  std_logic := '0';

    snk_out_arr         : out t_dp_siso_arr(g_nof_macs - 1 downto 0);
    snk_in_arr          : in  t_dp_sosi_arr(g_nof_macs - 1 downto 0) := (others => c_dp_sosi_rst);

    src_in_arr          : in  t_dp_siso_arr(g_nof_macs - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr         : out t_dp_sosi_arr(g_nof_macs - 1 downto 0);

    -- Serial IO
    serial_tx_arr       : out std_logic_vector(g_nof_macs - 1 downto 0);
    serial_rx_arr       : in  std_logic_vector(g_nof_macs - 1 downto 0) := (others => '0')
  );
end unb2c_board_10gbe;

architecture str of unb2c_board_10gbe is
  signal tr_ref_clk_312 : std_logic;
  signal tr_ref_clk_156 : std_logic;
  signal tr_ref_rst_156 : std_logic;
begin
  u_unb2c_board_clk644_pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
  generic map (
    g_technology => g_technology
  )
  port map (
    refclk_644 => tr_ref_clk,
    rst_in     => mm_rst,
    clk_156    => tr_ref_clk_156,
    clk_312    => tr_ref_clk_312,
    rst_156    => tr_ref_rst_156,
    rst_312    => open
  );

  u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
    generic map (
      g_technology    => g_technology,
      g_sim           => g_sim,
      g_sim_level     => 1,
      g_nof_macs      => g_nof_macs,
      g_direction     => g_direction,
      g_use_loopback  => g_use_loopback,
      g_tx_fifo_fill  => g_tx_fifo_fill,
      g_tx_fifo_size  => g_tx_fifo_size
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644      => tr_ref_clk,
      tr_ref_clk_312      => tr_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
      tr_ref_clk_156      => tr_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
      tr_ref_rst_156      => tr_ref_rst_156,  -- for 10GBASE-R or for XAUI

      -- MM interface
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,

      reg_mac_mosi        => reg_mac_mosi,
      reg_mac_miso        => reg_mac_miso,

      reg_eth10g_mosi     => reg_eth10g_mosi,
      reg_eth10g_miso     => reg_eth10g_miso,

      reg_10gbase_r_24_mosi => reg_10gbase_r_24_mosi,
      reg_10gbase_r_24_miso => reg_10gbase_r_24_miso,

      -- DP interface
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,

      src_out_arr         => src_out_arr,
      src_in_arr          => src_in_arr,

      snk_out_arr         => snk_out_arr,
      snk_in_arr          => snk_in_arr,

      -- Serial IO
      serial_tx_arr       => serial_tx_arr,
      serial_rx_arr       => serial_rx_arr
    );
end str;
