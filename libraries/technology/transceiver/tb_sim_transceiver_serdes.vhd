--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
--   Model a basic serializer->deserializer link and verify received data
-- Description:
--   Data generator -> Serializer -> Deserializer -> Data verification
-- Usage:
--   as 10
--   run -all
--   Observe:
--   . user tx_in_data on serializer == user rx_out_data on deserializer
--   . serial_line carries 4 bytes per serialized word. Each
--     byte is followed by its 2 valid bits and is sent LSb first.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_sim_transceiver_serdes is
end entity tb_sim_transceiver_serdes;

architecture tb of tb_sim_transceiver_serdes is
  constant c_data_w         : natural := 32;
  constant c_tr_clk_period  : time := 6.4 ns;  -- 156.25 MHz

  signal tb_end           : std_logic := '0';

  signal tr_clk           : std_logic := '0';
  signal tr_rst           : std_logic := '1';

  signal tx_enable        : std_logic := '1';
  signal tx_ready         : std_logic;

  signal tx_in_data       : std_logic_vector(c_data_w - 1 downto 0);
  signal tx_in_val        : std_logic;
  signal tx_in_ctrl       : std_logic_vector(c_data_w / c_byte_w - 1 downto 0);

  signal serial_line      : std_logic;

  signal rx_out_data      : std_logic_vector(c_data_w - 1 downto 0);
  signal rx_out_val       : std_logic;
  signal rx_out_ctrl      : std_logic_vector(c_data_w / c_byte_w - 1 downto 0);

  signal prev_rx_out_data : std_logic_vector(c_data_w - 1 downto 0);
  signal verify_en        : std_logic := '0';
  signal rd_ready         : std_logic := '1';
begin
  p_tb_end : process
  begin
    wait for c_tr_clk_period * 300;

    -- Stop the simulation
    tb_end <= '1';
    assert false
      report "Simulation finished."
      severity NOTE;
    wait;
  end process;

  tr_clk  <= not tr_clk or tb_end after c_tr_clk_period / 2;
  tr_rst  <= '0' after c_tr_clk_period * 10;

  p_tx_ready: process
  begin
    tx_ready <= '0';
    wait until tr_rst = '0';
    while tb_end = '0' loop
      tx_ready <= '1';
      wait for c_tr_clk_period * 50;
      tx_ready <= '0';
      wait for c_tr_clk_period * 50;
    end loop;
  end process;

  -- Generate Tx data output with c_rl = 1 and counter data starting at 0
  proc_common_gen_data(1, 0, tr_rst, tr_clk, tx_enable, tx_ready, tx_in_data, tx_in_val);

  tx_in_ctrl <= (others => tx_in_val);

  u_ser: entity work.sim_transceiver_serializer
  generic map (
    g_data_w        => c_data_w,
    g_tr_clk_period => c_tr_clk_period
  )
  port map (
    tb_end             => tb_end,
    tr_clk             => tr_clk,
    tr_rst             => tr_rst,

    tx_in_data         => tx_in_data,
    tx_in_ctrl         => tx_in_ctrl,

    tx_serial_out      => serial_line
  );

  u_des: entity work.sim_transceiver_deserializer
  generic map (
    g_data_w        => c_data_w,
    g_tr_clk_period => c_tr_clk_period
  )
  port map (
    tb_end             => tb_end,
    tr_clk             => tr_clk,
    tr_rst             => tr_rst,

    rx_out_data        => rx_out_data,
    rx_out_ctrl        => rx_out_ctrl,

    rx_serial_in       => serial_line
  );

  p_verify_en: process
  begin
    verify_en <= '0';
    wait until tr_rst = '0';
    wait for c_tr_clk_period * 5;
    verify_en <= '1';
    wait;
  end process;

  rx_out_val <= andv(rx_out_ctrl);

  -- Verify dut output incrementing data
  proc_common_verify_data(1, tr_clk, verify_en, rd_ready, rx_out_val, rx_out_data, prev_rx_out_data);
end tb;
