-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle

-- Purpose:
-- . Ring info register
-- Description:
-- See ring_pkg.vhd
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.ring_pkg.all;

entity ring_info is
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock

    dp_clk     : in  std_logic;
    dp_rst     : in  std_logic;

    reg_copi   : in  t_mem_copi;
    reg_cipo   : out t_mem_cipo;

    ring_info  : out t_ring_info
  );
end ring_info;

architecture str of ring_info is
  signal mm_fields_out : std_logic_vector(field_slv_out_len(c_ring_info_field_arr) - 1 downto 0);
begin
  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    g_use_slv_in_val  => false,  -- use FALSE to save logic when always slv_in_val='1'
    g_field_arr       => c_ring_info_field_arr
  )
  port map (
    mm_clk     => mm_clk,
    mm_rst     => mm_rst,

    mm_mosi    => reg_copi,
    mm_miso    => reg_cipo,

    slv_clk    => dp_clk,
    slv_rst    => dp_rst,

    slv_in_val => '1',

    slv_out    => mm_fields_out
  );

  -- get "RW" fields from mm_fields
  ring_info.O_rn <= mm_fields_out(field_hi(c_ring_info_field_arr, "O_rn") downto field_lo(c_ring_info_field_arr, "O_rn"));
  ring_info.N_rn <= mm_fields_out(field_hi(c_ring_info_field_arr, "N_rn") downto field_lo(c_ring_info_field_arr, "N_rn"));

  ring_info.use_cable_to_previous_rn <= sl(mm_fields_out(field_hi(c_ring_info_field_arr, "use_cable_to_previous_rn") downto field_lo(c_ring_info_field_arr, "use_cable_to_previous_rn")));
  ring_info.use_cable_to_next_rn     <= sl(mm_fields_out(field_hi(c_ring_info_field_arr, "use_cable_to_next_rn") downto field_lo(c_ring_info_field_arr, "use_cable_to_next_rn")));
end str;
