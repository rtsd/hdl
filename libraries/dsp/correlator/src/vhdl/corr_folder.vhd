--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- Description:
-- . nof_outputs = ceil_div(g_nof_inputs, 2^(g_nof_folds)) for g_nof_folds>=0
--   . Examples:
--     . g_nof_inputs=10, g_nof_folds=0 -> nof_outputs=10
--     . g_nof_inputs=10, g_nof_folds=1 -> nof_outputs= 5
--     . g_nof_inputs=10, g_nof_folds=2 -> nof_outputs= 3
--     . g_nof_inputs=10, g_nof_folds=3 -> nof_outputs= 2
--     . g_nof_inputs=10, g_nof_folds=4 -> nof_outputs= 1
--     . g_nof_inputs=10, g_nof_folds<0 -> nof_outputs= 1

entity corr_folder is
  generic (
    g_nof_inputs : natural;  -- Number of inputs
    g_nof_folds  : integer := -1  -- >0: Number of folds;
  );  -- 0: Wire out to in;
  port (  -- <0: Fold until one output remains
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

    src_out_arr    : out t_dp_sosi_arr(sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1) - 1 downto 0)
  );
end corr_folder;

architecture str of corr_folder is
  component corr_folder is
    generic (
      g_nof_inputs : natural;
      g_nof_folds  : integer := -1
    );
    port (
      rst            : in  std_logic;
      clk            : in  std_logic;

      snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

      src_out_arr    : out t_dp_sosi_arr(sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1) - 1 downto 0)
    );
  end component;

  constant c_nof_muxes : natural := ceil_div(g_nof_inputs, 2);

  signal mux_snk_in_arr      : t_dp_sosi_arr(2 * c_nof_muxes - 1 downto 0);
  signal mux_snk_in_2arr_2   : t_dp_sosi_2arr_2(c_nof_muxes - 1 downto 0);

  signal mux_src_out_arr     : t_dp_sosi_arr(c_nof_muxes - 1 downto 0);
  signal nxt_mux_src_out_arr : t_dp_sosi_arr(c_nof_muxes - 1 downto 0);
begin
  gen_arch: if g_nof_folds /= 0 generate
    -----------------------------------------------------------------------------
    -- Wire input array to mux_snk_in_arr to make sure we have an even number
    -- of buses to work with in case of an odd number of inputs
    -- . We want an even number of buses because we will wire up 2-input muxes
    -----------------------------------------------------------------------------
    gen_even_nof_buses: for i in 0 to g_nof_inputs - 1 generate
      mux_snk_in_arr(i) <= snk_in_arr(i);
    end generate;

    -----------------------------------------------------------------------------
    -- Wire inputs to the 2-input muxes
    -----------------------------------------------------------------------------
    gen_mux_inputs_0: for i in 0 to c_nof_muxes - 1 generate
      mux_snk_in_2arr_2(i)(0) <= mux_snk_in_arr(2 * i);
      mux_snk_in_2arr_2(i)(1) <= mux_snk_in_arr(2 * i + 1);
    end generate;

    -----------------------------------------------------------------------------
    -- Simple 2-input mux logic
    -----------------------------------------------------------------------------
    gen_mux_comb: for i in 0 to c_nof_muxes - 1 generate
      nxt_mux_src_out_arr(i) <= mux_snk_in_2arr_2(i)(0) when mux_snk_in_2arr_2(i)(0).valid = '1' else
                                mux_snk_in_2arr_2(i)(1) when mux_snk_in_2arr_2(i)(1).valid = '1' else
                                c_dp_sosi_rst;
    end generate;

    -----------------------------------------------------------------------------
    -- If c_nof_muxes=1 or g_nof_folds=1, this is the last stage. Otherwise,
    -- add a stage.
    -- . g_nof_folds <0 : user wants to fold all the way to one output
    -- . g_nof_folds >0 : user wants to fold n times
    -----------------------------------------------------------------------------
    gen_corr_folder: if (g_nof_folds < 0 and c_nof_muxes > 1) or g_nof_folds > 1 generate
      u_corr_folder : corr_folder
      generic map (
        g_nof_inputs => c_nof_muxes,
        g_nof_folds  => g_nof_folds - 1
      )
      port map (
        rst         => rst,
        clk         => clk,

        snk_in_arr  => mux_src_out_arr,
        src_out_arr => src_out_arr
      );
    end generate;

    gen_src_out_arr: if (g_nof_folds < 0 and c_nof_muxes = 1) or g_nof_folds = 1 generate
      src_out_arr <= mux_src_out_arr;
    end generate;

    -----------------------------------------------------------------------------
    -- Registers
    -----------------------------------------------------------------------------
    p_clk: process(clk, rst)
    begin
      if rst = '1' then
        mux_src_out_arr <= (others => c_dp_sosi_rst);
      elsif rising_edge(clk) then
        mux_src_out_arr <= nxt_mux_src_out_arr;
      end if;
    end process;
  end generate;

  -----------------------------------------------------------------------------
  -- Wire output to input if g_nof_folds=0
  -----------------------------------------------------------------------------
  gen_wire_out_to_in: if g_nof_folds = 0 generate
    src_out_arr <= snk_in_arr;
  end generate;
end str;
