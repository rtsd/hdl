library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pfs_fir_tapsbuf is
  generic (
    g_data_w      : natural;
    g_nof_words   : natural;
    g_addr_w      : natural
  );
  port (
    data_a        : in  std_logic_vector(g_data_w - 1 downto 0);
    wren_a        : in  std_logic;
    addr_a        : in  std_logic_vector(g_addr_w - 1 downto 0);
    addr_b        : in  std_logic_vector(g_addr_w - 1 downto 0);
    rden_b        : in  std_logic;
    data_b        : out std_logic_vector(g_data_w - 1 downto 0);
    clk           : in  std_logic;
    rst           : in  std_logic
  );
end pfs_fir_tapsbuf;
