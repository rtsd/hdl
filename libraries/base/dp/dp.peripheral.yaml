schema_name: args
schema_version: 1.0
schema_type: peripheral

hdl_library_name: dp
hdl_library_description: "Data path (DP) peripherals for streaming data."

peripherals:
  - peripheral_name: dpmm    # pi_dpmm.py
    peripheral_description: "DP to MM FIFO to provide memory mapped MM read access from Data Path (DP) streaming interface."
    mm_ports:
      # MM port for mms_dp_fifo_to_mm.vhd / dp_fifo_to_mm_reg.vhd
      - mm_port_name: REG_DPMM_CTRL
        mm_port_type: REG
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: "DPMM = Monitor the DP to MM read FIFO."
        fields:
          - - field_name: rd_usedw
              field_description: "Number of words that can be read from the FIFO."
              address_offset: 0x0
              access_mode: RO
      # MM port for mms_dp_fifo_to_mm.vhd / dp_fifo_to_mm.vhd
      - mm_port_name: REG_DPMM_DATA   # Use REG_, instead of preferred FIFO_, to match mm_port_name in pi_dpmm.py
        mm_port_type: FIFO
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: "DPMM = read word from the DP to MM read FIFO"
        fields:
          - - field_name: rd_data
              field_description: "Read data from the FIFO."
              address_offset: 0x0
              access_mode: RO
        

  - peripheral_name: mmdp    # pi_mmdp.py
    peripheral_description: "MM to DP FIFO to provide memory mapped MM write access to Data Path (DP) streaming interface."
    mm_ports:
      # MM port for mms_dp_fifo_from_mm.vhd / dp_fifo_from_mm_reg.vhd
      - mm_port_name: REG_MMDP_CTRL
        mm_port_type: REG
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: "MMDP = Monitor the MM to DP write FIFO."
        fields:
          - - field_name: wr_usedw
              field_description: "Number of words that are in the write FIFO."
              address_offset: 0x0
              access_mode: RO
              
          - - field_name: wr_availw
              field_description: "Number of words that can be written to the write FIFO."
              address_offset: 0x4
              access_mode: RO
      # MM port for mms_dp_fifo_from_mm.vhd / dp_fifo_from_mm.vhd
      - mm_port_name: REG_MMDP_DATA   # Use REG_, instead of preferred FIFO_, to match mm_port_name in pi_mmdp.py
        mm_port_type: FIFO
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: "MMDP = write word to the MM to DP write FIFO."
        fields:
          - - field_name: data
              field_description: "Write data to the FIFO."
              address_offset: 0x0
              access_mode: WO


  - peripheral_name: dp_xonoff    # pi_dp_xonoff.py
    peripheral_description: "Enable or disable a data path (DP) stream."
    parameters:
      # Parameters of mms_dp_xonoff.vhd
      - { name: g_nof_streams, value: 1 }
      - { name: g_combine_streams, value: False }
    mm_ports:
      # MM port for mms_dp_xonoff.vhd
      - mm_port_name: REG_DP_XONOFF
        mm_port_type: REG
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: "When g_combine_streams = False then there is one enable bit per stream, else there is one enable bit for all streams."
        fields:
          - - field_name: enable_stream
              field_description: |
                "When enable_stream = 0 the data stream is stopped, else when 1 then the data stream is passed on.
                 Toggling the data stream on or off happens at block or packet boundaries."
              number_of_fields: g_nof_streams  # sel_a_b(g_combine_streams, 1, g_nof_streams)
              address_offset: 0x0
              mm_width: 1
              access_mode: RW


  - peripheral_name: dp_shiftram    # pi_dp_shiftram.py
    peripheral_description: "Sample delay buffer with programmable delay for streaming data."
    parameters:
      # Parameters of dp_shiftram.vhd
      - { name: g_nof_streams, value: 1 }
      - { name: g_nof_words, value: 1024 }
      - { name: g_data_w, value: 16 }
    mm_ports:
      # MM port for dp_shiftram.vhd
      - mm_port_name: REG_DP_SHIFTRAM
        mm_port_type: REG
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: ""
        number_of_mm_ports: g_nof_streams
        fields:
          - - field_name: shift
              field_description: "Fill level of the sample delay buffer in number of data samples."
              address_offset: 0x0
              mm_width: ceil_log2(g_nof_words)
              access_mode: RW


  - peripheral_name: dp_bsn_align_v2    # pi_dp_bsn_align_v2.py
    peripheral_description: "Align packets from multiple input streams."
    parameters:
      # Parameters of dp_bsn_align_v2.vhd
      - { name: g_nof_streams, value: 2 }
    mm_ports:
      # MM port for dp_bsn_align_v2.vhd
      - mm_port_name: REG_DP_BSN_ALIGN_V2
        mm_port_type: REG
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: ""
        number_of_mm_ports: g_nof_streams
        fields:
          - - field_name: enable
              field_description: "Stream enable per stream. Value 0 disables the stream, 1 enables the stream. Disabled streams are not aligned."
              address_offset: 0 * MM_BUS_SIZE
              mm_width: 1
              access_mode: RW
          - - field_name: replaced_pkt_cnt
              field_description: "Count of packets per stream that contain replacement data per sync interval."
              address_offset: 1 * MM_BUS_SIZE
              access_mode: RO

  - peripheral_name: dp_bsn_source    # pi_dp_bsn_source.py
    peripheral_description: "Block Sequence Number (BSN) source for timestamping blocks of data samples."
    parameters:
      # Parameters of dp_bsn_source_reg.vhd
      - { name: g_nof_block_per_sync, value: 20 }
    mm_ports:
      # MM port for dp_bsn_source_reg.vhd
      - mm_port_name: REG_DP_BSN_SOURCE
        mm_port_type: REG
        mm_port_span: 4 * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: dp_on
              field_description: |
                "When 1 then enable BSN source, else when 0 disable BSN source. If dp_on_pps is 0,
                 then dp_on = 1 enables the BSN source immediately. To enable the BSN source at
                 the next PPS, then first set dp_on_pps = 1. Clearing dp_on stops the BSN source."
              address_offset: 0x0
              mm_width: 1
              access_mode: RW
          - - field_name: dp_on_pps
              field_description: "When 1 and dp_on = 1 then enable BSN source at next PPS."
              address_offset: 0x0
              bit_offset: 1
              mm_width: 1
              access_mode: RW
          - - field_name: nof_block_per_sync
              field_description: "Number of blocks per sync interval."
              address_offset: 0x4
              access_mode: RW
          - - field_name: bsn
              field_description: "Write: initial BSN. Read: BSN at last sync. First access lo, then hi."
              address_offset: 0x8
              user_width: 64
              radix: uint64
              access_mode: RW


  - peripheral_name: dp_bsn_source_v2    # pi_dp_bsn_source_v2.py
    peripheral_description: "Block Sequence Number (BSN) source with block time offset, for timestamping blocks of data samples."
    parameters:
      # Parameters of dp_bsn_source_reg_v2.vhd
      - { name: g_nof_clk_per_sync, value: 200000000 }
      - { name: g_block_size, value: 256 }
      - { name: g_bsn_time_offset_w, value: 8 }  # note: g_bsn_time_offset_w = ceil_log2(g_block_size)
    mm_ports:
      # MM port for dp_bsn_source_reg_v2.vhd
      - mm_port_name: REG_DP_BSN_SOURCE_V2
        mm_port_type: REG
        mm_port_span: 8 * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: dp_on
              field_description: |
                "When 1 then enable BSN source, else when 0 disable BSN source. If dp_on_pps is 0,
                 then dp_on = 1 enables the BSN source immediately. To enable the BSN source at
                 the next PPS, then first set dp_on_pps = 1. Clearing dp_on stops the BSN source."
              address_offset: 0x0
              mm_width: 1
              access_mode: RW
          - - field_name: dp_on_pps
              field_description: "When 1 and dp_on = 1, then enable BSN source at next PPS."
              address_offset: 0x0
              bit_offset: 1
              mm_width: 1
              access_mode: RW
          - - field_name: nof_clk_per_sync
              field_description: "Number of clock cycles per sync interval."
              address_offset: 0x4
              access_mode: RW
          - - field_name: bsn_init
              field_description: "Write: initial BSN. Read: BSN at last sync. First access lo, then hi."
              address_offset: 0x8
              user_width: 64
              radix: uint64
              access_mode: RW
          - - field_name: bsn_time_offset
              field_description: "The BSN block time offset in number of clock cycles, with respect to the PPS."
              address_offset: 0x10
              mm_width: g_bsn_time_offset_w
              access_mode: RW

              
  - peripheral_name: dp_bsn_scheduler    # pi_dp_bsn_scheduler.py
    peripheral_description: "Schedule a trigger at a certain Block Sequence Number (BSN) instant."
    mm_ports:
      # MM port for dp_bsn_scheduler_reg.vhd
      - mm_port_name: REG_DP_BSN_SCHEDULER
        mm_port_type: REG
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: scheduled_bsn
              field_description: "Write: scheduled BSN. Read: last BSN. First access lo, then hi."
              address_offset: 0x0
              user_width: 64
              radix: uint64
              access_mode: RW

              
  - peripheral_name: dp_bsn_sync_scheduler    # pi_dp_bsn_sync_scheduler.py
    peripheral_description: "Disable the output, or enable the output with a programmable sync interval."
    mm_ports:
      # MM port for mmp_dp_bsn_sync_scheduler.vhd
      - mm_port_name: REG_DP_BSN_SYNC_SCHEDULER
        mm_port_type: REG
        mm_port_span: 16 * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: ctrl_enable
              field_description:
                "Enable the output when 1, disable the output when 0. First disable output to re-enable output."
              address_offset: 0 * MM_BUS_SIZE
              mm_width: 1
              access_mode: RW
          - - field_name: ctrl_interval_size
              field_description:
                "Number of samples in output sync interval. For example, for sample rate 200 MHz
                 this implies a maximum sync interval period of 2**31 * 5 ns = 10.7 s (because
                 VHDL NATURAL range is 0 - 2**31-1)"
              address_offset: 1 * MM_BUS_SIZE
              mm_width: 31
              access_mode: RW
          - - field_name: ctrl_start_bsn
              field_description:
                "Schedule start BSN for the output. The start BSN needs to be in the future,
                if the start BSN is in the past or if the input stream is not active, then
                ctrl_enable = '1' will have no effect."
              address_offset: 2 * MM_BUS_SIZE
              user_width: 64
              radix: uint64
              access_mode: RW
          - - field_name: mon_current_input_bsn
              field_description: "Current input BSN. This can be read to determine a start BSN in the future."
              address_offset: 4 * MM_BUS_SIZE
              user_width: 64
              radix: uint64
              access_mode: RO
          - - field_name: mon_input_bsn_at_sync
              field_description: "Input BSN at input sync. This can be read to compare with mon_output_sync_bsn."
              address_offset: 6 * MM_BUS_SIZE
              user_width: 64
              radix: uint64
              access_mode: RO
          - - field_name: mon_output_enable
              field_description:
                "Is 1 when output is enabled, 0 when output is disabled. The output stream gets
                 enabled when ctrl_enable is set '1' and when the BSN of the input stream has
                 reached the ctrl_start_bsn."
              address_offset: 8 * MM_BUS_SIZE
              mm_width: 1
              access_mode: RO
          - - field_name: mon_output_sync_bsn
              field_description: "Output BSN at sync. This yields the number of blocks per output sync interval."
              address_offset: 9 * MM_BUS_SIZE
              user_width: 64
              radix: uint64
              access_mode: RO
          - - field_name: block_size
              field_description: "Number of samples per BSN block, same for input stream and output stream."
              address_offset: 11 * MM_BUS_SIZE
              access_mode: RO

  - peripheral_name: dp_bsn_monitor    # pi_dp_bsn_monitor.py
    peripheral_description: "Monitor the Block Sequence Number (BSN) status of streaming data."
    parameters:
      # Parameters of mms_dp_bsn_monitor.vhd
      - { name: g_nof_streams, value: 1 }
    mm_ports:
      # MM port for dp_bsn_monitor_reg.vhd
      - mm_port_name: REG_DP_BSN_MONITOR
        mm_port_type: REG
        mm_port_span: 16 * MM_BUS_SIZE
        mm_port_description: ""
        number_of_mm_ports: g_nof_streams
        fields:
          - - field_name: xon_stable
              field_description: "Data block flow control xon signal was active and stable during last sync interval."
              address_offset: 0x0
              bit_offset: 0
              mm_width: 1
              access_mode: RO
          - - field_name: ready_stable
              field_description: "Clock cycle flow control ready signal was active and stable during last sync interval."
              address_offset: 0x0
              bit_offset: 1
              mm_width: 1
              access_mode: RO
          - - field_name: sync_timeout
              field_description: "Data stream sync did not occur during last sync interval."
              address_offset: 0x0
              bit_offset: 2      # EK TODO: 2 is correct, but using 1 cause gen_doc.py to fail without clear error, because fields then overlap
              mm_width: 1
              access_mode: RO
          - - field_name: bsn_at_sync
              field_description: "Data stream BSN at sync."
              address_offset: 0x4
              user_width: 64
              radix: uint64
              access_mode: RO
          - - field_name: nof_sop
              field_description: "Number data blocks (sop = start of packet) during last sync interval."
              address_offset: 0xC
              access_mode: RO
          - - field_name: nof_valid
              field_description: "Number valid samples of the data blocks during last sync interval (= nof_sop * block size)."
              address_offset: 0x10
              access_mode: RO
          - - field_name: nof_err
              field_description: "Number data blocks with error indication during last sync interval."
              address_offset: 0x14
              access_mode: RO
          - - field_name: bsn_first
              field_description: "First data stream BSN ever."
              address_offset: 0x18
              user_width: 64
              radix: uint64
              access_mode: RO
          - - field_name: bsn_first_cycle_cnt
              field_description: "Arrival latency of first data stream BSN ever, relative to local sync."
              address_offset: 0x20
              access_mode: RO


  - peripheral_name: dp_bsn_monitor_v2    # pi_dp_bsn_monitor_v2.py
    peripheral_description: "Monitor the Block Sequence Number (BSN) status and latency of streaming data."
    parameters:
      # Parameters of mms_dp_bsn_monitor_v2.vhd
      - { name: g_nof_streams, value: 1 }
    mm_ports:
      # MM port for dp_bsn_monitor_reg_v2.vhd
      - mm_port_name: REG_DP_BSN_MONITOR_V2
        mm_port_type: REG
        mm_port_span: 8 * MM_BUS_SIZE
        mm_port_description: ""
        number_of_mm_ports: g_nof_streams
        fields:
          - - field_name: xon_stable
              field_description: "Data block flow control xon signal was active and stable during last sync interval."
              address_offset: 0x0
              bit_offset: 0
              mm_width: 1
              access_mode: RO
          - - field_name: ready_stable
              field_description: "Clock cycle flow control ready signal was active and stable during last sync interval."
              address_offset: 0x0
              bit_offset: 1
              mm_width: 1
              access_mode: RO
          - - field_name: sync_timeout
              field_description: "Data stream sync did not occur during last sync interval."
              address_offset: 0x0
              bit_offset: 2
              mm_width: 1
              access_mode: RO
          - - field_name: bsn_at_sync
              field_description: "Data stream BSN at sync."
              address_offset: 1 * MM_BUS_SIZE
              user_width: 64
              radix: uint64
              access_mode: RO
          - - field_name: nof_sop
              field_description: "Number data blocks (sop = start of packet) during last sync interval."
              address_offset: 3 * MM_BUS_SIZE
              access_mode: RO
          - - field_name: nof_valid
              field_description: "Number valid samples of the data blocks during last sync interval (= nof_sop * block size)."
              address_offset: 4 * MM_BUS_SIZE
              access_mode: RO
          - - field_name: nof_err
              field_description: "Number data blocks with error indication during last sync interval."
              address_offset: 5 * MM_BUS_SIZE
              access_mode: RO
          - - field_name: latency
              field_description: "Arrival latency of data stream BSN at sync, relative to local sync."
              address_offset: 6 * MM_BUS_SIZE
              access_mode: RO


  - peripheral_name: dp_selector    # pi_dp_selector.py
    peripheral_description: "Select between two data streams or between two arrays of data streams."
    mm_ports:
      # MM port for dp_selector_arr.vhd
      - mm_port_name: REG_DP_SELECTOR
        mm_port_type: REG
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: input_select
              field_description: |
                "When input_select = 0 select the reference data stream(s), else when 1 select the other data stream(s).
                 The input_select is synchronsized to the start of a sync interval."
              address_offset: 0x0
              mm_width: 1
              access_mode: RW


  - peripheral_name: dp_sync_insert_v2     # pi_dp_sync_insert_v2.py
    peripheral_description: "Every nof_blk_per_sync block a sync pulse is created at the output."
    mm_ports:
      # MM port for dp_sync_insert_v2.vhd
      - mm_port_name: REG_DP_SYNC_INSERT_V2
        mm_port_type: REG
        mm_port_span: 2 * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: nof_blk_per_sync
              field_description: |
                "The block counter resets if a sync arrives at the input or when nof_blk_per_sync is reached."
              address_offset: 0x0
              access_mode: RW


  - peripheral_name: dp_strobe_total_count
    peripheral_description: "Count strobes"
    parameters:
      - { name: g_nof_counts_max, value: 15 }  # fixed by REGMAP
      - { name: g_nof_counts, value: 1 }  # actual nof counts, <= g_nof_counts_max
    mm_ports:
      # MM port for dp_strobe_total_count.vhd
      - mm_port_name: REG_DP_STROBE_TOTAL_COUNT
        mm_port_type: REG
        mm_port_span: ceil_pow2(g_nof_counts_max*2 + 2) * MM_BUS_SIZE  # = 32 * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: counts
              field_description: "Total number of strobes counters, updated at internal sync."
              number_of_fields: g_nof_counts
              address_offset: 0
              user_width: 64
              radix: uint64
              access_mode: RO
          - - field_name: clear
              field_description: "Read or write this register to clear all counts."
              address_offset: (g_nof_counts_max*2 + 0) * MM_BUS_SIZE  # 30 * MM_BUS_SIZE
              access_mode: RW
          - - field_name: flush
              field_description: "Flush current strobe counter values into the counts."
              address_offset: (g_nof_counts_max*2 + 1) * MM_BUS_SIZE  # 31 * MM_BUS_SIZE
              access_mode: RW


  - peripheral_name: dp_block_validate_err    # pi_dp_block_validate_err.py
    peripheral_description: "Validate the error field of a DP block"
    parameters:
      - { name: g_nof_err_counts, value: 8 }
    mm_ports:
      - mm_port_name: REG_DP_BLOCK_VALIDATE_ERR
        mm_port_type: REG
        mm_port_span: ceil_pow2(g_nof_err_counts + 1 + 2 + 1) * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: err_count_index
              field_description: "Number of errors count per bit in the input sosi.err field (g_nof_err_counts-1 DOWNTO 0)."
              number_of_fields: g_nof_err_counts
              address_offset: 0
              access_mode: RO
          - - field_name: total_discarded_blocks
              field_description: "The total amount of discarded DP blocks. All input DP blocks with sosi.err /= 0 are discarded."
              address_offset: g_nof_err_counts * MM_BUS_SIZE  # 8 * MM_BUS_SIZE
              access_mode: RO
          - - field_name: total_block_count
              field_description: "The total amount of DP blocks that streamed in this dp_block_validate_err."
              address_offset: (g_nof_err_counts + 1) * MM_BUS_SIZE  # 9 * MM_BUS_SIZE
              user_width: 64
              radix: uint64
              access_mode: RO
          - - field_name: clear 
              field_description: "Read or write this register to clear all counters."
              address_offset: (g_nof_err_counts + 3) * MM_BUS_SIZE  # 11 * MM_BUS_SIZE
              access_mode: RW


  - peripheral_name: dp_block_validate_bsn_at_sync    # dp_block_validate_bsn_at_sync
    peripheral_description: "Validate whether the remote block sequence number (BSN) at the start of an in_sosi.sync interval is equal to the local BSN at start of the local bs_sosi.sync interval."
    mm_ports:
      - mm_port_name: REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC
        mm_port_type: REG
        mm_port_span: 4 * MM_BUS_SIZE
        mm_port_description: ""
        fields:
          - - field_name: nof_sync_discarded
              field_description: "The total amount of discarded sync intervals."
              address_offset: 0
              access_mode: RO
          - - field_name: nof_sync
              field_description: "The total amount of sync intervals."
              address_offset: 1 * MM_BUS_SIZE
              access_mode: RO
          - - field_name: clear 
              field_description: "Read or write this register to clear all counters."
              address_offset: 2 * MM_BUS_SIZE
              access_mode: RW


