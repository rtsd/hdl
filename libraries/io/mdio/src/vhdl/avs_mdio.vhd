-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.mdio_pkg.all;

-- Need to wrap mdio with this avs_mdio, because the SOPC Component
-- Editor does not support the user defined types from mdio_pkg.

-- Name signals for automatic type and interface recognition:
--
--  <interface type>_<interface name>_<signal type>[_n]
--
-- * Avalon <interface type>:
--   . csi_ = Clock input
--   . avs_ = Avalon-MM slave
--   . coe_ = Conduit
-- * User defined <interface name>:
--   . _system_
--   . _mdio_
--   . etc.
-- * Avalon <signal type>:
--   . _reset
--   . _clk
--   . _address
--   . etc.

entity avs_mdio is
  generic (
    -- g_mdio_phy
    g_mdc_period : natural := c_mdio_phy_mdc_period;  -- must be a power of 2
    g_hold_time  : natural := c_mdio_phy_hold_time;
    g_setup_time : natural := c_mdio_phy_setup_time
  );
  port (
    ---------------------------------------------------------------------------
    -- Avalon Conduit interfaces: coe_*_export
    ---------------------------------------------------------------------------
    -- GENERIC Signal
    coe_gs_sim_export             : in  std_logic := '0';

    -- MDIO interface
    coe_mdio_phy_mdc_export       : out std_logic;  -- MDIO PHY serial clock line
    coe_mdio_phy_mdat_in_export   : in  std_logic;  -- MDIO PHY serial data in from line
    coe_mdio_phy_mdat_oen_export  : out std_logic;  -- MDIO PHY serial data out pull low to line

    ---------------------------------------------------------------------------
    -- Avalon Clock Input interface: csi_*
    ---------------------------------------------------------------------------
    csi_system_reset              : in  std_logic;
    csi_system_clk                : in  std_logic;

    ---------------------------------------------------------------------------
    -- Avalon Memory Mapped Slave interfaces: avs_*
    ---------------------------------------------------------------------------
    -- MM slave MDIO header register
    avs_header_address            : in  std_logic;
    avs_header_write              : in  std_logic;
    avs_header_read               : in  std_logic;
    avs_header_writedata          : in  std_logic_vector(c_halfword_w - 1 downto 0);  -- use half MM bus width for mdio register
    avs_header_readdata           : out std_logic_vector(c_halfword_w - 1 downto 0);
    -- MM slave MDIO data register
    avs_data_address              : in  std_logic;
    avs_data_write                : in  std_logic;
    avs_data_read                 : in  std_logic;
    avs_data_writedata            : in  std_logic_vector(c_halfword_w - 1 downto 0);  -- use half MM bus width for mdio register
    avs_data_readdata             : out std_logic_vector(c_halfword_w - 1 downto 0);
    ---------------------------------------------------------------------------
    -- Avalon Interrupt Sender interface: ins_*
    ---------------------------------------------------------------------------
    ins_interrupt_irq             : out std_logic
  );
end avs_mdio;

architecture wrap of avs_mdio is
  constant c_avs_mdio_phy : t_c_mdio_phy := (g_mdc_period, g_hold_time, g_setup_time);

  signal cs_sim               : boolean;
  signal i_avs_header_address : std_logic_vector(0 downto 0);
  signal i_avs_data_address   : std_logic_vector(0 downto 0);
begin
  -- SOPC Builder does not use BOOLEAN, so use STD_LOGIC and is_true()
  cs_sim <= is_true(coe_gs_sim_export);

  -- SOPC Builder writes STD_LOGIC_VECTOR(0 DOWNTO 0) as STD_LOGIC so take care of mapping
  i_avs_header_address(0) <= avs_header_address;
  i_avs_data_address(0) <= avs_data_address;

  u_mdio : entity work.mdio
  generic map (
    g_mdio_phy          => c_avs_mdio_phy
  )
  port map (
    -- GENERIC Signal
    gs_sim              => cs_sim,

    -- Clock & reset
    rst                 => csi_system_reset,
    clk                 => csi_system_clk,

    -- Memory Mapped Slave interface with Interrupt
    mms_header_address  => i_avs_header_address,
    mms_header_write    => avs_header_write,
    mms_header_read     => avs_header_read,
    mms_header_writedata => avs_header_writedata,
    mms_header_readdata => avs_header_readdata,

    mms_data_address    => i_avs_data_address,
    mms_data_write      => avs_data_write,
    mms_data_read       => avs_data_read,
    mms_data_writedata  => avs_data_writedata,
    mms_data_readdata   => avs_data_readdata,

    ins_mdio_rdy        => ins_interrupt_irq,

    -- MDIO external interface
    mdc                 => coe_mdio_phy_mdc_export,
    mdat_in             => coe_mdio_phy_mdat_in_export,
    mdat_oen            => coe_mdio_phy_mdat_oen_export
  );
end wrap;
