-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use work.ddr3_pkg.all;

entity seq_ddr3 is
  generic (
    g_ddr      : t_c_ddr3_phy := c_ddr3_phy_4g;
    g_phy      : natural      := 1;
    g_mts      : natural      := 800;
    g_data_w   : natural      := c_ddr3_ctlr_data_w;
    g_ddr3_seq : t_ddr3_seq   := c_ddr3_seq
  );
  port (
    ctlr_ref_clk      : in    std_logic;
    ctlr_rst          : in    std_logic;  -- asynchronous reset input to controller

    ctlr_gen_clk      : out   std_logic;  -- Controller generated clock
    ctlr_gen_rst      : out   std_logic;

    wr_clk            : in    std_logic;
    wr_rst            : in    std_logic;

    wr_sosi           : in    t_dp_sosi;
    wr_siso           : out   t_dp_siso;

    flush_ena         : in    std_logic;

    rd_sosi           : out   t_dp_sosi;
    rd_siso           : in    t_dp_siso;

    rd_clk            : in    std_logic;
    rd_rst            : in    std_logic;

    rd_data_mosi      : in    t_mem_mosi := c_mem_mosi_rst;
    rd_data_miso      : out   t_mem_miso := c_mem_miso_rst;

    ser_term_ctrl_out : out   std_logic_vector(13 downto 0);
    par_term_ctrl_out : out   std_logic_vector(13 downto 0);

    ser_term_ctrl_in  : in    std_logic_vector(13 downto 0) := (others => '0');
    par_term_ctrl_in  : in    std_logic_vector(13 downto 0) := (others => '0');

    -- SO-DIMM Memory Bank
    ddr3_in          : in    t_tech_ddr3_phy_in;
    ddr3_io          : inout t_tech_ddr3_phy_io;
    ddr3_ou          : out   t_tech_ddr3_phy_ou
  );
end seq_ddr3;

architecture str of seq_ddr3 is
  constant c_min_fifo_size    : positive := 256;
  constant c_blocksize        : positive := g_ddr3_seq.wr_nof_chunks * g_ddr3_seq.wr_chunksize;
  constant c_wr_fifo_depth    : natural  := sel_a_b(c_blocksize > c_min_fifo_size, c_blocksize, c_min_fifo_size);  -- c_blocksize * 2;
  constant c_rd_fifo_depth    : natural  := sel_a_b(c_blocksize > c_min_fifo_size, c_blocksize, c_min_fifo_size);  -- c_blocksize * 2;

  signal i_ctlr_gen_rst       : std_logic;
  signal i_ctlr_gen_clk       : std_logic;

  -- ctrl & status DDR3 driver
  signal dvr_start_addr       : t_ddr3_addr;
  signal dvr_end_addr         : t_ddr3_addr;

  signal dvr_en               : std_logic;
  signal dvr_wr_not_rd        : std_logic;
  signal dvr_done             : std_logic;

  -- DDR3 controller status
  signal ctlr_init_done       : std_logic;
  signal ctlr_rdy             : std_logic;
  signal init_done_data_start : std_logic;
begin
  ctlr_gen_clk    <= i_ctlr_gen_clk;
  ctlr_gen_rst    <= i_ctlr_gen_rst;

  u_ddr3: entity work.ddr3
  generic map(
    g_ddr                     => g_ddr,
    g_phy                     => g_phy,
    g_mts                     => g_mts,
    g_wr_data_w               => g_data_w,
    g_wr_use_ctrl             => true,
    g_wr_fifo_depth           => c_wr_fifo_depth,
    g_rd_fifo_depth           => c_rd_fifo_depth,
    g_rd_data_w               => g_data_w,
    g_flush_wr_fifo           => true,
    g_flush_sop               => true,
    g_flush_sop_sync          => true,
    g_flush_sop_channel       => false,
    g_flush_sop_start_channel => 0,
    g_flush_nof_channels      => 0
  )
  port map (
    ctlr_ref_clk       => ctlr_ref_clk,
    ctlr_rst           => ctlr_rst,

    phy_in             => ddr3_in,
    phy_io             => ddr3_io,
    phy_ou             => ddr3_ou,

    ctlr_gen_clk       => i_ctlr_gen_clk,
    ctlr_gen_rst       => i_ctlr_gen_rst,

    ctlr_init_done     => ctlr_init_done,

    ctlr_rdy           => ctlr_rdy,
    dvr_start_addr     => dvr_start_addr,
    dvr_end_addr       => dvr_end_addr,

    dvr_done           => dvr_done,
    dvr_wr_not_rd      => dvr_wr_not_rd,
    dvr_en             => dvr_en,

    wr_clk             => wr_clk,
    wr_rst             => wr_rst,

    wr_sosi            => wr_sosi,
    wr_siso            => wr_siso,

    flush_ena          => flush_ena,

    rd_sosi            => rd_sosi,
    rd_siso            => rd_siso,

    rd_clk             => rd_clk,
    rd_rst             => rd_rst,

    ser_term_ctrl_out  => ser_term_ctrl_out,
    par_term_ctrl_out  => par_term_ctrl_out,

    ser_term_ctrl_in   => ser_term_ctrl_in,
    par_term_ctrl_in   => par_term_ctrl_in,

    rd_fifo_usedw      => open
  );

  init_done_data_start <= ctlr_init_done and wr_sosi.sync;

  u_ddr3_sequencer: entity work.ddr3_seq
  generic map(
    g_ddr      => g_ddr,
    g_ddr3_seq => g_ddr3_seq
  )
  port map (
    dp_rst     => wr_rst,
    dp_clk     => wr_clk,

    en_evt     => dvr_en,
    wr_not_rd  => dvr_wr_not_rd,

    start_addr => dvr_start_addr,
    end_addr   => dvr_end_addr,

    done       => dvr_done,
    init_done  => init_done_data_start,
    ctlr_rdy   => ctlr_rdy
  );
end str;
