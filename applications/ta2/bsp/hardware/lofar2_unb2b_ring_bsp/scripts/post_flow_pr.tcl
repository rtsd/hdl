# (C) 1992-2018 Intel Corporation.                            
# Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
# and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
# and/or other countries. Other marks and brands may be claimed as the property  
# of others. See Trademarks on intel.com for full list of Intel trademarks or    
# the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
# Your use of Intel Corporation's design tools, logic functions and other        
# software and tools, and its AMPP partner logic functions, and any output       
# files any of the foregoing (including device programming or simulation         
# files), and any associated documentation or information are expressly subject  
# to the terms and conditions of the Altera Program License Subscription         
# Agreement, Intel MegaCore Function License Agreement, or other applicable      
# license agreement, including, without limitation, that your use is for the     
# sole purpose of programming logic devices manufactured by Intel and sold by    
# Intel or its authorized distributors.  Please refer to the applicable          
# agreement for further details.                                                 
# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Reinier vd Walle 
# Purpose:
# . post-compile script for OpenCL applications
# Description:
# . " "
# ##########################################################################
post_message "Running post_flow_pr.tcl script"

post_message "Checking for OpenCL SDK installation, environment should have INTELFPGAOCLSDKROOT defined"
if {[catch {set sdk_root $::env(INTELFPGAOCLSDKROOT)} result]} {
  post_message -type error "OpenCL SDK installation not found.  Make sure INTELFPGAOCLSDKROOT is correctly set"
  exit 2
} else {
  post_message "INTELFPGAOCLSDKROOT=$::env(INTELFPGAOCLSDKROOT)"
}

# Load OpenCL BSP utility functions
source "$sdk_root/ip/board/bsp/opencl_bsp_util.tcl"

set project_name  [::opencl_bsp::get_project_name $quartus(args)]
set revision_name [::opencl_bsp::get_revision_name $quartus(args) $project_name]
set fast_compile  [::aocl_fast_compile::is_fast_compile]
set logic_limit 75.0
set update_mif 1


##############################################################################
##############################       MAIN        #############################
##############################################################################

post_message "Project name: $project_name"
post_message "Revision name: $revision_name"


# Run adjust PLL script 
source "$sdk_root/ip/board/bsp/adjust_plls_a10.tcl"

# Copy flat.sof and flat.rbf to parent directory
if {[file exists "flat_time_limited.sof"] == 1} {
   post_message "Warning: flat.sof and flat.rbf are Time Limited!"
   qexec "quartus_cpf -c --option=scripts/rbf_options_file flat_time_limited.sof ../flat.rbf"
   file copy -force flat_time_limited.sof ../flat.sof
}

if {[file exists "flat.sof"] == 1} {
   qexec "quartus_cpf -c --option=scripts/rbf_options_file flat.sof ../flat.rbf"
   file copy -force flat.sof ../flat.sof
}

if {$fast_compile} {
  ::aocl_fast_compile::check_logic_utilization $logic_limit
}

