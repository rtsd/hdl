// Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus II License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 14.1
// ALTERA_TIMESTAMP:Thu Dec  4 07:39:37 PST 2014
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "DEV-6.6"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
R8me71UCEoraht5Yl1N2sx9BPWWocuG+BUD/nUNYtPljvqCrLxHE+Do5SRs+Hf66
dQsES5xKsUdOxOYpzminwyxANzEmkN9gnhkghnFdJqZ8PPMn4NPBXcri7HDr7wng
sYo+cni7oneVT4Re01egTZBiQMKLvvor/9Tws7LZJao=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1824)
6Fs8X+Tar82PxaZZe0lmEYOHvRvgsjw/0DKMqfSEFVyzo3NXoRssHpCBGGWac9Tj
on3q4kDmZH4Fofb63XKao2MToLURiGe18kUBc4lQYQSPL6CoFCixofzYa+psElR6
bvfogmXH+JRCknBDYfSMYfi2IzQu7bIJv+VZNCD2T0OfveWH30O+KwCp9TTSSBbO
NQEYYuccPgS+isDBErKjwcybaMWNr/wvx3xi+n1TK56Qkb0vXbBqvMmCcCkV9Odc
lXmtmr8jwC6lDgNzXMDf5is/vXQNxf733UCYeEeNzye3DqmlFcYUPwAdNSaWfFkp
TTyeZJKYqSOvxRaqx4wZggbqf5L5PYfeYl8CVeVHUDQFXwhchYllGmWMn4bwrU90
5Wg4FaQ6wJpb3ah2vIpg9xpt2EGR3LDR3QZua+g87t7Mt6HaR3bXjYoPZ8cqzcLM
9Tl+31zFg7WxxWo/4/Mxlw9S+cxpN20zwx/9IfvobR6naWfEA4ZN7xGbqd4yLptH
r9NN+tVSLj7tnrNgK8btx7CnVxlMN9JAWqGrHnAt0NK5FInMW6HYUHEroMvCQWQa
z96supKW2ULqdZQL7wVNFKBCHM7N9bzI6/KmiBO26f0VllzU7pnWV/P9i+kqGDdK
d873lXUrhat5UvgPmMFk2hH5Nsg/3NhOTygd5JfEnnf+BX2+Vu19f5E4zrVSnkFi
67H+3xRAPB2c2RMJ8LY/0VeotOmBZtt7/Xt1u2Fg/59fMAvM+lNvlWviSbk7VGnm
hii+G2WS4AjRL896RDLqdqa4r/zmFI5q1bsfgzJXBSOhaVUjU1ZVVmiHwPK+Kqs4
wZv5ctDxfQ4AkEsuE6d12sNO0Mns9T6R6vQ1TPuAsnTPRNPGMKVfB6bph5MQTlO9
GJZ6Z8URyZObtpl45R054ZG2L9m8+QtxXqq/CAlMx2/+gXZEyKbUOllcFu0bv0ZZ
bri1sjK1WEOVL5rj4kTHk+iRjcs0fyptING0392G8u35Vlms9DKRClfqbBC8vqIU
GrtYAbeFtyANtuZcwyWXVSSHARQDevVyv1VQz0Jbbf6mnx3nJDQOzf4mKBY7pWGq
Kh1AfZOOF0sXilF6216zqDpjbH4GUuZq7GfUmlACw3EO1ZCFw5c6A7bjnsJWHqYk
l8MKf2zgF2rwjauj326P5iKtKyxQLo+CpeoFopVnS3sHR/gfDxRsfP6bxvRDj/uJ
pKcGaKHbYyejIhFUQCIxKkNLTnaXLnNVagxeDhy4vySAygBiJl+v2H7oQrvEUnJ1
knEwPtG/GzY1ytv/9oEXttdykitfERZYIvByT+rjkvukP6UoFJkOl0BhyFEh+w31
l61K9l0/I/hMdspdiSnR5uk9zzHt0wfxjjToOlIr/4OKE1ChGigEy77EeiDT8G9N
i+0IN7K08w/SzQ20FwymKNtcay91dHRzoHYmEUhLOjgO9N8MD9/zGPnV2q+Afnpb
ctgbAwlvJixWLaWBsV3sIT3Ah6x65EnfwxnLfB4sOKvf18kD1HNqN08hYTCA8DxV
rfEjzLTQ+4iUy8W2ZBBInvclVn9ouMj/vEWIuXUIyU4kVS5cdtpRlCliHiqBHhPX
4EFXcWHOeOXAjYWEF/wpovL7KlHwM0c7bX4aF2eQpHbpZ64HPO+oFMr7oNvVuuhZ
sTtDUciwKixss1JXwW29RpaN+2CR/qD/ujAAcZYCTR9gBIGvtqGe00DXyKt//Sa1
hcx7ku3Qhpvi5WhftKERC24b5uGzQe9gQs1OuslX8ud/wL2YQ2oKTYdKNEUnRqq/
yCUK7I6F+aBb+Z2/RwVv0zH8pFC0VS3tRpLxJ4gVuoAv12d4QWRnOI0PyTQ58MhK
Ftj8b8mSWBCla7UVcmuVcjaFzGuXLFf75pzzhmBpzZ45qnQT0rI8x/BZAmOckihG
rTOy/0JRdMut07kkJzl+5hgu5XYS8U9UCQTxw9Nku/fXcdc2kzBdvJj/KBHG/9SZ
F6MC6v64qL06SmEtjgYVuSV6GcWWgQQdF9xgZmLO3/HFpHtyjHZvcpll4vmP3Qk5
wAqAssAiYAZ9FlqKqkxc1Jc0I3YFsM8GRI8V5J/H448nQwRc0gUqvorrFaLxTqpU
MI5xDAnYQu/LTTM1FAtightx2seZdKW4tCFp7IXsTyBBcHW56NLgnO+FVgUZvk6w
pnSWrw0Av1QAgz4JIsOhdftTVfYNHFJucBWa4N6L0/mgv71C7E8inN6v2YzIHQTb
Z/1SSsfUZZCjnDWZEsmUItuyjvLtLObUiWcxtGWjG+AjFtth6w8kWjKja2LD5MCh
ghdQn69E4IDPmPOuv8b28mUkVAc8Ydu3OeIdPxGAo2k3GPk6MouKqH8atBi3uult
qGD7W02amvItD8yvwdpRTK2FzU2/8JadZnz631qxYM6KxqqJFeNG+qEP4N9zIHAf
`pragma protect end_protected
