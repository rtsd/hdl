-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;
use common_lib.common_str_pkg.all;

entity tb_dp_hdr_insert_remove is
  generic (
    g_data_w       : natural := 96;  -- Testing with uncommon data width. 96 is OK as it a a multiple of both c_word_w and g_symbol_w.
    g_symbol_w     : natural := 8;
    g_nof_symbols  : natural := 21;
    g_in_en        : t_dp_flow_control_enum := e_random;
    g_out_ready    : t_dp_flow_control_enum := e_random  -- e_active
  );
end tb_dp_hdr_insert_remove;

architecture tb of tb_dp_hdr_insert_remove is
  -- tb default
  constant c_rl                       : natural := 1;
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  -- tb specific
  constant c_nof_repeat               : natural := 100;
  constant c_bsn_w                    : natural := 16;
  constant c_symbol_init              : natural := 0;
  constant c_symbol_mod               : integer := 2**g_symbol_w;  -- used to avoid TO_UVEC warning for smaller g_symbol_w : "NUMERIC_STD.TO_UNSIGNED: vector truncated"
  constant c_channel_init             : integer := 0;
  constant c_err_init                 : natural := 0;
  constant c_sync_period              : natural := 7;
  constant c_sync_offset              : natural := 2;

  -- tb default
  constant c_mm_clk_period    : time := 20 ns;
  constant c_st_clk_period    : time := 5 ns;

  signal tb_end               : std_logic := '0';
  signal mm_clk               : std_logic := '0';
  signal mm_rst               : std_logic;
  signal st_rst               : std_logic;
  signal st_clk               : std_logic := '0';

  signal random_0             : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1             : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0              : std_logic;
  signal pulse_1              : std_logic;
  signal pulse_en             : std_logic := '1';

  signal in_en                : std_logic := '1';

  -- tb verify
  signal verify_en            : std_logic := '0';
  signal verify_done          : std_logic := '0';

  signal prev_out_ready       : std_logic_vector(0 to c_rl);
  signal prev_out_data        : std_logic_vector(g_data_w - 1 downto 0);
  signal prev_out_bsn         : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '1');  -- = -1
  signal prev_out_channel     : std_logic_vector(c_dp_stream_channel_w - 1 downto 0) := TO_SVEC(c_channel_init - 1, c_dp_stream_channel_w);
  signal prev_out_err         : std_logic_vector(c_dp_stream_error_w - 1 downto 0) := TO_SVEC(c_err_init - 1, c_dp_stream_error_w);
--  SIGNAL expected_out_bsn     : STD_LOGIC_VECTOR(c_bsn_w-1 DOWNTO 0);
--  SIGNAL expected_out_channel : STD_LOGIC_VECTOR(c_dp_stream_channel_w-1 DOWNTO 0);
--  SIGNAL expected_out_err     : STD_LOGIC_VECTOR(c_dp_stream_error_w-1 DOWNTO 0);

  -- tb dut
  signal in_siso              : t_dp_siso;
  signal in_sosi              : t_dp_sosi;
  signal in_data              : std_logic_vector(g_data_w - 1 downto 0);
  signal in_bsn               : std_logic_vector(c_bsn_w - 1 downto 0);
  signal in_sync              : std_logic;
  signal in_val               : std_logic;
  signal in_sop               : std_logic;
  signal in_eop               : std_logic;
  signal in_empty             : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  signal in_channel           : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal in_err               : std_logic_vector(c_dp_stream_error_w - 1 downto 0);

  signal hdr_siso             : t_dp_siso;
  signal hdr_sosi             : t_dp_sosi;
  signal hdr_data             : std_logic_vector(g_data_w - 1 downto 0);
  signal hdr_bsn              : std_logic_vector(c_bsn_w - 1 downto 0);
  signal hdr_sync             : std_logic;
  signal hdr_val              : std_logic;
  signal hdr_sop              : std_logic;
  signal hdr_eop              : std_logic;
  signal hdr_empty            : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  signal hdr_channel          : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal hdr_err              : std_logic_vector(c_dp_stream_error_w - 1 downto 0);

  signal out_siso             : t_dp_siso := c_dp_siso_hold;  -- default xon = '1' is needed for proc_dp_gen_block_data()
  signal out_sosi             : t_dp_sosi;
  signal out_data             : std_logic_vector(g_data_w - 1 downto 0);
  signal out_bsn              : std_logic_vector(c_bsn_w - 1 downto 0);
  signal out_sync             : std_logic;
  signal out_val              : std_logic;
  signal out_sop              : std_logic;
  signal out_eop              : std_logic;
  signal out_empty            : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  signal out_channel          : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal out_err              : std_logic_vector(c_dp_stream_error_w - 1 downto 0);

  signal out_gap              : std_logic := '1';

  signal ram_hdr_mosi        : t_mem_mosi;
  signal reg_hdr_mosi        : t_mem_mosi;

  signal hdr_data_miso        : t_mem_miso;

  constant c_hdr_nof_words    : natural := 3;  -- we're using a header of 3 96-bit words.
  constant c_hdr_nof_mm_words : natural := c_hdr_nof_words * (g_data_w / c_word_w);

  constant c_ram_header_start : std_logic_vector(c_word_w - 1 downto 0) := x"DEADBEEF";
  constant c_ram_header_end   : std_logic_vector(c_word_w - 1 downto 0) := x"CAFEBABE";
begin
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 20;

  st_clk <= (not st_clk) or tb_end after c_st_clk_period / 2;
  st_rst <= '1', '0' after c_st_clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(st_clk);
  random_1 <= func_common_random(random_1) when rising_edge(st_clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', st_rst, st_clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', st_rst, st_clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en          <= '1'                     when g_in_en = e_active      else
                    random_0(random_0'high) when g_in_en = e_random      else
                    pulse_0                 when g_in_en = e_pulse;

  out_siso.ready <= '1'                     when g_out_ready = e_active  else
                    random_1(random_1'high) when g_out_ready = e_random  else
                    pulse_1                 when g_out_ready = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli : process
    variable v_sync      : std_logic := '0';
    variable v_bsn       : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '0');
    variable v_symbol    : natural := c_symbol_init;
    variable v_channel   : natural := c_channel_init;
    variable v_err       : natural := c_err_init;
  begin
    in_sosi  <= c_dp_sosi_rst;
    ram_hdr_mosi <= c_mem_mosi_rst;
    reg_hdr_mosi <= c_mem_mosi_rst;

    -- MM Stimuli: write HEADER to RAM: 0xDEADBEEF, [counter data words 0..c_hdr_nof_mm_words-2-1], 0xCAFEBABE
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 5);
    proc_mem_mm_bus_wr(0, c_ram_header_start, mm_clk, ram_hdr_mosi);

    for i in 0 to c_hdr_nof_mm_words - 2 - 1 loop
      proc_mem_mm_bus_wr(1 + i, i, mm_clk, ram_hdr_mosi);
    end loop;

    proc_mem_mm_bus_wr(c_hdr_nof_mm_words - 1, c_ram_header_end, mm_clk, ram_hdr_mosi);
    proc_common_wait_some_cycles(mm_clk, 5);

    -- Release the header onto the DP
    proc_mem_mm_bus_wr(0, 1, mm_clk, reg_hdr_mosi);

    -- Begin of ST stimuli
    proc_common_wait_until_low(st_clk, st_rst);
    proc_common_wait_some_cycles(st_clk, 50);

    for R in 0 to c_nof_repeat - 1 loop
      v_sync := sel_a_b(R mod c_sync_period = c_sync_offset, '1', '0');  -- v_bsn = R
      proc_dp_gen_block_data(c_rl, true, g_data_w, g_symbol_w, v_symbol, 0, 0, g_nof_symbols, v_channel, v_err, v_sync, TO_DP_BSN(R), st_clk, in_en, in_siso, in_sosi);
      v_bsn     := INCR_UVEC(v_bsn, 1);
      v_symbol  := (v_symbol + g_nof_symbols) mod c_symbol_mod;
      v_channel := v_channel + 1;
      v_err     := v_err + 1;
      --proc_common_wait_some_cycles(st_clk, 10);               -- create gap between frames
    end loop;

    -- End of stimuli
--    expected_out_bsn     <= INCR_UVEC(v_bsn, -1);
--    expected_out_channel <= TO_UVEC(v_channel-1, c_dp_stream_channel_w);
--    expected_out_err     <= TO_UVEC(v_err-1, c_dp_stream_error_w);

    proc_common_wait_some_cycles(st_clk, 50);  -- depends on stream control
    verify_done <= '1';
    proc_common_wait_some_cycles(st_clk, 1);
    verify_done <= '0';

    -- Read the stripped header via MM bus and print it in the transcript window
    print_str("Reading stripped header from RAM:");
    for i in 0 to c_hdr_nof_mm_words - 1 loop
      proc_mem_mm_bus_rd(i, mm_clk, ram_hdr_mosi);
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
      print_str("[" & time_to_str(now) & "] 0x" & slv_to_hex(hdr_data_miso.rddata(c_word_w - 1 downto 0)));
    end loop;

    proc_common_wait_some_cycles(st_clk, 10);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  verify_en <= '1' when rising_edge(st_clk) and out_sosi.sop = '1';  -- verify enable after first output sop

  -- SOSI control
  proc_dp_verify_valid(c_rl, st_clk, verify_en, out_siso.ready, prev_out_ready, out_val);  -- Verify that the output valid fits with the output ready latency
  proc_dp_verify_gap_invalid(st_clk, out_val, out_sop, out_eop, out_gap);  -- Verify that the output valid is low between blocks from eop to sop

  -- SOSI data
  -- . verify that the output is incrementing symbols, like the input stimuli
  proc_dp_verify_symbols(c_rl, g_data_w, g_symbol_w, st_clk, verify_en, out_siso.ready, out_sosi.valid, out_sosi.eop, out_data, out_sosi.empty, prev_out_data);

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_hdr_insert : entity work.dp_hdr_insert
  generic map (
    g_data_w        => g_data_w,
    g_symbol_w      => g_symbol_w,
    g_hdr_nof_words => c_hdr_nof_words
  )
  port map (
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    st_rst      => st_rst,
    st_clk      => st_clk,

    ram_mosi    => ram_hdr_mosi,
    reg_mosi    => reg_hdr_mosi,

    snk_out     => in_siso,
    snk_in      => in_sosi,

    src_in      => hdr_siso,
    src_out     => hdr_sosi
  );

  u_hdr_remove : entity work.dp_hdr_remove
  generic map (
    g_data_w        => g_data_w,
    g_symbol_w      => g_symbol_w,
    g_hdr_nof_words => c_hdr_nof_words
  )
  port map (
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    st_rst      => st_rst,
    st_clk      => st_clk,

    snk_out     => hdr_siso,
    snk_in      => hdr_sosi,

    sla_in      => ram_hdr_mosi,
    sla_out     => hdr_data_miso,

    src_in      => out_siso,
    src_out     => out_sosi
  );

  -- Map to slv to ease monitoring in wave window
  in_data     <= in_sosi.data(g_data_w - 1 downto 0);
  in_val      <= in_sosi.valid;
  in_sop      <= in_sosi.sop;
  in_eop      <= in_sosi.eop;
  in_empty    <= in_sosi.empty(in_empty'range);
  in_channel  <= in_sosi.channel(in_channel'range);
  in_err      <= in_sosi.err(in_err'range);
  in_sync     <= in_sosi.sync;
  in_bsn      <= in_sosi.bsn(c_bsn_w - 1 downto 0);

  hdr_data    <= hdr_sosi.data(g_data_w - 1 downto 0);
  hdr_val     <= hdr_sosi.valid;
  hdr_sop     <= hdr_sosi.sop;
  hdr_eop     <= hdr_sosi.eop;
  hdr_empty   <= hdr_sosi.empty(in_empty'range);
  hdr_channel <= hdr_sosi.channel(in_channel'range);
  hdr_err     <= hdr_sosi.err(in_err'range);
  hdr_sync    <= hdr_sosi.sync;
  hdr_bsn     <= hdr_sosi.bsn(c_bsn_w - 1 downto 0);

  out_data    <= out_sosi.data(g_data_w - 1 downto 0);
  out_val     <= out_sosi.valid;
  out_sop     <= out_sosi.sop;
  out_eop     <= out_sosi.eop;
  out_empty   <= out_sosi.empty(in_empty'range);
  out_channel <= out_sosi.channel(in_channel'range);
  out_err     <= out_sosi.err(in_err'range);
  out_sync    <= out_sosi.sync;
  out_bsn     <= out_sosi.bsn(c_bsn_w - 1 downto 0);
end tb;
