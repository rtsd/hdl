-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 10 march 2022
-- Purpose: Regression multi tb for tb_lofar2_unb2c_sdp_station_bf
-- Description:
--   The multi tb only has one instance, so the tb_tb is more a wrapper to
--   ensure that always the same tb generics are used in the regression test.
--   This allows modifying the generics in the tb.
-- Usage:
-- > as 4
-- > run -all

library IEEE, lofar2_sdp_lib;
use IEEE.std_logic_1164.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity tb_tb_lofar2_unb2c_sdp_station_bf is
end tb_tb_lofar2_unb2c_sdp_station_bf;

architecture tb of tb_tb_lofar2_unb2c_sdp_station_bf is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  u_bf : entity work.tb_lofar2_unb2c_sdp_station_bf
  generic map (
    g_sp                 => 3,  -- WG signal path (SP) index in range(S_pn = 12)
    g_sp_ampl            => 0.5,  -- WG normalized amplitude
    g_sp_phase           => -110.0,  -- WG phase in degrees = subband phase
    g_sp_remnant_ampl    => 0.1,  -- WG normalized amplitude for remnant sp
    g_sp_remnant_phase   => 15.0,  -- WG phase in degrees for remnant sp
    g_subband            => 102,  -- select g_subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
    g_beamlet            => c_sdp_S_sub_bf - 1,  -- map g_subband to g_beamlet index in beamset in range(c_sdp_S_sub_bf = 488)
    g_beamlet_scale      => 1.0 / 2.0**9,  -- g_beamlet output scale factor
    g_bf_x_gain          => 0.7,  -- g_beamlet X BF weight normalized gain for g_sp
    g_bf_y_gain          => 0.6,  -- g_beamlet Y BF weight normalized gain for g_sp
    g_bf_x_phase         => 30.0,  -- g_beamlet X BF weight phase rotation in degrees for g_sp
    g_bf_y_phase         => 40.0,  -- g_beamlet Y BF weight phase rotation in degrees for g_sp
    g_bf_remnant_x_gain  => 0.05,  -- g_beamlet X BF weight normalized gain for remnant sp
    g_bf_remnant_y_gain  => 0.04,  -- g_beamlet Y BF weight normalized gain for remnant sp
    g_bf_remnant_x_phase => 170.0,  -- g_beamlet X BF weight phase rotation in degrees for g_sp
    g_bf_remnant_y_phase => -135.0,  -- g_beamlet Y BF weight phase rotation in degrees for g_sp
    g_read_all_SST       => false,  -- when FALSE only read SST for g_subband, to save sim time
    g_read_all_BST       => false  -- when FALSE only read BST for g_beamlet, to save sim time
  );
end tb;
