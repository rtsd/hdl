-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE;
use IEEE.std_logic_1164.all;

package tech_10gbase_r_component_pkg is
  ------------------------------------------------------------------------------
  -- ip_arria10
  ------------------------------------------------------------------------------

  component ip_arria10_phy_10gbase_r is
  port (
    tx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    rx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    tx_cal_busy             : out std_logic_vector(0 downto 0);  -- tx_cal_busy.tx_cal_busy
    rx_cal_busy             : out std_logic_vector(0 downto 0);  -- rx_cal_busy.rx_cal_busy
    tx_serial_clk0          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_serial_clk0.clk
    rx_cdr_refclk0          : in  std_logic                     := '0';  -- rx_cdr_refclk0.clk
    tx_serial_data          : out std_logic_vector(0 downto 0);  -- tx_serial_data.tx_serial_data
    rx_serial_data          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_is_lockedtoref       : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_is_lockedtodata      : out std_logic_vector(0 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    tx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_coreclkin.clk
    rx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_coreclkin.clk
    tx_clkout               : out std_logic_vector(0 downto 0);  -- tx_clkout.clk
    rx_clkout               : out std_logic_vector(0 downto 0);  -- rx_clkout.clk
    tx_parallel_data        : in  std_logic_vector(63 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    rx_parallel_data        : out std_logic_vector(63 downto 0);  -- rx_parallel_data.rx_parallel_data
    tx_pma_div_clkout       : out std_logic_vector(0 downto 0);  -- tx_pma_div_clkout.clk
    tx_control              : in  std_logic_vector(7 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_err_ins              : in  std_logic                     := '0';  -- tx_err_ins.tx_err_ins
    unused_tx_parallel_data : in  std_logic_vector(63 downto 0) := (others => '0');  -- unused_tx_parallel_data.unused_tx_parallel_data
    unused_tx_control       : in  std_logic_vector(8 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    rx_control              : out std_logic_vector(7 downto 0);  -- rx_control.rx_control
    unused_rx_parallel_data : out std_logic_vector(63 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_rx_control       : out std_logic_vector(11 downto 0);  -- unused_rx_control.unused_rx_control
    tx_enh_data_valid       : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_full        : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pfull       : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_enh_fifo_empty       : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_pempty      : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    rx_enh_data_valid       : out std_logic_vector(0 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_full        : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_empty       : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_del         : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_insert      : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(0 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_enh_blk_lock         : out std_logic_vector(0 downto 0)  -- rx_enh_blk_lock.rx_enh_blk_lock
  );
  end component;

  component ip_arria10_phy_10gbase_r_4
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(11 downto 0)  := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(3 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(3 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(31 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(3 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(3 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(3 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(3 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(3 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(255 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(3 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(3 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(3 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(3 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(31 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(255 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(3 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(47 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(255 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(35 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(255 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_phy_10gbase_r_12
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(13 downto 0)  := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(11 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(11 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(95 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(11 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(11 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(11 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(11 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(11 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(767 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(11 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(11 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(11 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(11 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(95 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(767 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(11 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(143 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(767 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(107 downto 0) := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(767 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_phy_10gbase_r_24
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)    := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(14 downto 0)   := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)   := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(23 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                       := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(23 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(191 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(23 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(23 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(23 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(23 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(23 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(1535 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(23 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(23 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(23 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(23 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(191 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(1535 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(23 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(287 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(1535 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_phy_10gbase_r_48
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)    := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(15 downto 0)   := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)   := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(47 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                       := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(47 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(383 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(47 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(47 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(47 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(47 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(47 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(3071 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(47 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(47 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(47 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(47 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(383 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(3071 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(47 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(575 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(3071 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(431 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(3071 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_transceiver_pll_10g is
  port (
    mcgb_rst              : in  std_logic                     := '0';  -- mcgb_rst.mcgb_rst
    mcgb_serial_clk       : out std_logic;  -- mcgb_serial_clk.clk
    pll_cal_busy          : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked            : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown         : in  std_logic                     := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0           : in  std_logic                     := '0';  -- pll_refclk0.clk
    reconfig_write0       : in  std_logic                     := '0';  -- reconfig_avmm0.write
    reconfig_read0        : in  std_logic                     := '0';  -- .read
    reconfig_address0     : in  std_logic_vector(9 downto 0)  := (others => '0');  -- .address
    reconfig_writedata0   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reconfig_readdata0    : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest0 : out std_logic;  -- .waitrequest
    reconfig_clk0         : in  std_logic                     := '0';  -- reconfig_clk0.clk
    reconfig_reset0       : in  std_logic                     := '0';  -- reconfig_reset0.reset
    tx_serial_clk         : out std_logic  -- tx_serial_clk.clk
--    pll_powerdown   : in  std_logic := '0'; -- pll_powerdown.pll_powerdown
--    pll_refclk0     : in  std_logic := '0'; --   pll_refclk0.clk
--    pll_locked      : out std_logic;        --    pll_locked.pll_locked
--    pll_cal_busy    : out std_logic;        --  pll_cal_busy.pll_cal_busy
--    mcgb_rst        : in  std_logic := '0';
--    mcgb_serial_clk : out std_logic         -- tx_serial_clk.clk
  );
  end component;

  component ip_arria10_transceiver_reset_controller_1 is
  port (
    clock              : in  std_logic                    := '0';  -- clock.clk
    reset              : in  std_logic                    := '0';  -- reset.reset
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    tx_analogreset     : out std_logic_vector(0 downto 0);  -- tx_analogreset.tx_analogreset
    tx_digitalreset    : out std_logic_vector(0 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(0 downto 0);  -- tx_ready.tx_ready
    pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked.pll_locked
    pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select.pll_select
    tx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    rx_analogreset     : out std_logic_vector(0 downto 0);  -- rx_analogreset.rx_analogreset
    rx_digitalreset    : out std_logic_vector(0 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_ready           : out std_logic_vector(0 downto 0);  -- rx_ready.rx_ready
    rx_is_lockedtodata : in  std_logic_vector(0 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0')  -- rx_cal_busy.rx_cal_busy
  );
  end component;

  component ip_arria10_transceiver_reset_controller_4
  port (
    clock              : in  std_logic                    := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                    := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(3 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(3 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(3 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(3 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(3 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(3 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(3 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(3 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(3 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_transceiver_reset_controller_12
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(11 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(11 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(11 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(11 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(11 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(11 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(11 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(11 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(11 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_transceiver_reset_controller_24
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(23 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(23 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(23 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(23 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(23 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(23 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(23 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_transceiver_reset_controller_48
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(47 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(47 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(47 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(47 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(47 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(47 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(47 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  ------------------------------------------------------------------------------

  component ip_arria10_e3sge3_phy_10gbase_r is
  port (
    tx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    rx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    tx_cal_busy             : out std_logic_vector(0 downto 0);  -- tx_cal_busy.tx_cal_busy
    rx_cal_busy             : out std_logic_vector(0 downto 0);  -- rx_cal_busy.rx_cal_busy
    tx_serial_clk0          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_serial_clk0.clk
    rx_cdr_refclk0          : in  std_logic                     := '0';  -- rx_cdr_refclk0.clk
    tx_serial_data          : out std_logic_vector(0 downto 0);  -- tx_serial_data.tx_serial_data
    rx_serial_data          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_is_lockedtoref       : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_is_lockedtodata      : out std_logic_vector(0 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    tx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_coreclkin.clk
    rx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_coreclkin.clk
    tx_clkout               : out std_logic_vector(0 downto 0);  -- tx_clkout.clk
    rx_clkout               : out std_logic_vector(0 downto 0);  -- rx_clkout.clk
    tx_parallel_data        : in  std_logic_vector(63 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    rx_parallel_data        : out std_logic_vector(63 downto 0);  -- rx_parallel_data.rx_parallel_data
    tx_pma_div_clkout       : out std_logic_vector(0 downto 0);  -- tx_pma_div_clkout.clk
    tx_control              : in  std_logic_vector(7 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_err_ins              : in  std_logic                     := '0';  -- tx_err_ins.tx_err_ins
    unused_tx_parallel_data : in  std_logic_vector(63 downto 0) := (others => '0');  -- unused_tx_parallel_data.unused_tx_parallel_data
    unused_tx_control       : in  std_logic_vector(8 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    rx_control              : out std_logic_vector(7 downto 0);  -- rx_control.rx_control
    unused_rx_parallel_data : out std_logic_vector(63 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_rx_control       : out std_logic_vector(11 downto 0);  -- unused_rx_control.unused_rx_control
    tx_enh_data_valid       : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_full        : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pfull       : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_enh_fifo_empty       : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_pempty      : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    rx_enh_data_valid       : out std_logic_vector(0 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_full        : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_empty       : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_del         : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_insert      : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(0 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_enh_blk_lock         : out std_logic_vector(0 downto 0)  -- rx_enh_blk_lock.rx_enh_blk_lock
  );
  end component;

  component ip_arria10_e3sge3_phy_10gbase_r_4
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(11 downto 0)  := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(3 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(3 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(31 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(3 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(3 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(3 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(3 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(3 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(255 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(3 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(3 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(3 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(3 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(31 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(255 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(3 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(47 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(255 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(35 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(255 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e3sge3_phy_10gbase_r_12
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(13 downto 0)  := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(11 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(11 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(95 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(11 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(11 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(11 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(11 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(11 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(767 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(11 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(11 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(11 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(11 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(95 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(767 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(11 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(143 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(767 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(107 downto 0) := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(767 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e3sge3_phy_10gbase_r_24
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)    := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(14 downto 0)   := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)   := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(23 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                       := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(23 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(191 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(23 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(23 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(23 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(23 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(23 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(1535 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(23 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(23 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(23 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(23 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(191 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(1535 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(23 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(287 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(1535 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e3sge3_phy_10gbase_r_48
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)    := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(15 downto 0)   := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)   := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(47 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                       := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(47 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(383 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(47 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(47 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(47 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(47 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(47 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(3071 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(47 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(47 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(47 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(47 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(383 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(3071 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(47 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(575 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(3071 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(431 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(3071 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e3sge3_transceiver_pll_10g is
  port (
    mcgb_rst              : in  std_logic                     := '0';  -- mcgb_rst.mcgb_rst
    mcgb_serial_clk       : out std_logic;  -- mcgb_serial_clk.clk
    pll_cal_busy          : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked            : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown         : in  std_logic                     := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0           : in  std_logic                     := '0';  -- pll_refclk0.clk
    reconfig_write0       : in  std_logic                     := '0';  -- reconfig_avmm0.write
    reconfig_read0        : in  std_logic                     := '0';  -- .read
    reconfig_address0     : in  std_logic_vector(9 downto 0)  := (others => '0');  -- .address
    reconfig_writedata0   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reconfig_readdata0    : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest0 : out std_logic;  -- .waitrequest
    reconfig_clk0         : in  std_logic                     := '0';  -- reconfig_clk0.clk
    reconfig_reset0       : in  std_logic                     := '0';  -- reconfig_reset0.reset
    tx_serial_clk         : out std_logic  -- tx_serial_clk.clk
--    pll_powerdown   : in  std_logic := '0'; -- pll_powerdown.pll_powerdown
--    pll_refclk0     : in  std_logic := '0'; --   pll_refclk0.clk
--    pll_locked      : out std_logic;        --    pll_locked.pll_locked
--    pll_cal_busy    : out std_logic;        --  pll_cal_busy.pll_cal_busy
--    mcgb_rst        : in  std_logic := '0';
--    mcgb_serial_clk : out std_logic         -- tx_serial_clk.clk
  );
  end component;

  component ip_arria10_e3sge3_transceiver_reset_controller_1 is
  port (
    clock              : in  std_logic                    := '0';  -- clock.clk
    reset              : in  std_logic                    := '0';  -- reset.reset
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    tx_analogreset     : out std_logic_vector(0 downto 0);  -- tx_analogreset.tx_analogreset
    tx_digitalreset    : out std_logic_vector(0 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(0 downto 0);  -- tx_ready.tx_ready
    pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked.pll_locked
    pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select.pll_select
    tx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    rx_analogreset     : out std_logic_vector(0 downto 0);  -- rx_analogreset.rx_analogreset
    rx_digitalreset    : out std_logic_vector(0 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_ready           : out std_logic_vector(0 downto 0);  -- rx_ready.rx_ready
    rx_is_lockedtodata : in  std_logic_vector(0 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0')  -- rx_cal_busy.rx_cal_busy
  );
  end component;

  component ip_arria10_e3sge3_transceiver_reset_controller_4
  port (
    clock              : in  std_logic                    := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                    := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(3 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(3 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(3 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(3 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(3 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(3 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(3 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(3 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(3 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e3sge3_transceiver_reset_controller_12
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(11 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(11 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(11 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(11 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(11 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(11 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(11 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(11 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(11 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e3sge3_transceiver_reset_controller_24
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(23 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(23 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(23 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(23 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(23 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(23 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(23 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e3sge3_transceiver_reset_controller_48
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(47 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(47 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(47 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(47 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(47 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(47 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(47 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e1sg
  ------------------------------------------------------------------------------

  component ip_arria10_e1sg_phy_10gbase_r is
  port (
    rx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(0 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                     := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(0 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(7 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(0 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(0 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(0 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(0 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(63 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(0 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(0 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(0 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(0 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(7 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic                     := '0';  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(63 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(0 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(11 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(63 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(8 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(63 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e1sg_phy_10gbase_r_3 is
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- read
    reconfig_address        : in  std_logic_vector(11 downto 0)  := (others => '0');  -- address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reset
    rx_analogreset          : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_analogreset
    rx_cal_busy             : out std_logic_vector(2 downto 0);  -- rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- clk
    rx_clkout               : out std_logic_vector(2 downto 0);  -- clk
    rx_control              : out std_logic_vector(23 downto 0);  -- rx_control
    rx_coreclkin            : in  std_logic_vector(2 downto 0)   := (others => '0');  -- clk
    rx_digitalreset         : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(2 downto 0);  -- rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(2 downto 0);  -- rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(2 downto 0);  -- rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(2 downto 0);  -- rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(2 downto 0);  -- rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(2 downto 0);  -- rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(2 downto 0);  -- rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(2 downto 0);  -- rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(2 downto 0);  -- rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(191 downto 0);  -- rx_parallel_data
    rx_prbs_done            : out std_logic_vector(2 downto 0);  -- rx_prbs_done
    rx_prbs_err             : out std_logic_vector(2 downto 0);  -- rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(2 downto 0)   := (others => '0');  -- tx_analogreset
    tx_cal_busy             : out std_logic_vector(2 downto 0);  -- tx_cal_busy
    tx_clkout               : out std_logic_vector(2 downto 0);  -- clk
    tx_control              : in  std_logic_vector(23 downto 0)  := (others => '0');  -- tx_control
    tx_coreclkin            : in  std_logic_vector(2 downto 0)   := (others => '0');  -- clk
    tx_digitalreset         : in  std_logic_vector(2 downto 0)   := (others => '0');  -- tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(2 downto 0)   := (others => '0');  -- tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(2 downto 0);  -- tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(2 downto 0);  -- tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(2 downto 0);  -- tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(2 downto 0);  -- tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(2 downto 0)   := (others => '0');  -- tx_err_ins
    tx_parallel_data        : in  std_logic_vector(191 downto 0) := (others => '0');  -- tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(2 downto 0)   := (others => '0');  -- clk
    tx_serial_data          : out std_logic_vector(2 downto 0);  -- tx_serial_data
    unused_rx_control       : out std_logic_vector(35 downto 0);  -- unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(191 downto 0);  -- unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(26 downto 0)  := (others => '0');  -- unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(191 downto 0) := (others => '0')  -- unused_tx_parallel_data
  );
 end component ip_arria10_e1sg_phy_10gbase_r_3;

  component ip_arria10_e1sg_phy_10gbase_r_4
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(11 downto 0)  := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(3 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(3 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(31 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(3 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(3 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(3 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(3 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(3 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(255 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(3 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(3 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(3 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(3 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(31 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(255 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(3 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(47 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(255 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(35 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(255 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e1sg_phy_10gbase_r_12
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(13 downto 0)  := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(11 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(11 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(95 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(11 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(11 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(11 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(11 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(11 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(767 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(11 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(11 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(11 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(11 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(95 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(767 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(11 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(143 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(767 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(107 downto 0) := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(767 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e1sg_phy_10gbase_r_24
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)    := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(14 downto 0)   := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)   := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(23 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                       := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(23 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(191 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(23 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(23 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(23 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(23 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(23 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(1535 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(23 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(23 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(23 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(23 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(191 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(1535 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(23 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(287 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(1535 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e1sg_phy_10gbase_r_48
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)    := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(15 downto 0)   := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)   := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(47 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                       := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(47 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(383 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(47 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(47 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(47 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(47 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(47 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(3071 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(47 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(47 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(47 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(47 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(383 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(3071 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(47 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(575 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(3071 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(431 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(3071 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e1sg_transceiver_pll_10g is
  port (
    mcgb_rst              : in  std_logic                     := '0';  -- mcgb_rst.mcgb_rst
    mcgb_serial_clk       : out std_logic;  -- mcgb_serial_clk.clk
    pll_cal_busy          : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked            : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown         : in  std_logic                     := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0           : in  std_logic                     := '0';  -- pll_refclk0.clk
    reconfig_write0       : in  std_logic                     := '0';  -- reconfig_avmm0.write
    reconfig_read0        : in  std_logic                     := '0';  -- .read
    reconfig_address0     : in  std_logic_vector(9 downto 0)  := (others => '0');  -- .address
    reconfig_writedata0   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reconfig_readdata0    : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest0 : out std_logic;  -- .waitrequest
    reconfig_clk0         : in  std_logic                     := '0';  -- reconfig_clk0.clk
    reconfig_reset0       : in  std_logic                     := '0';  -- reconfig_reset0.reset
    tx_serial_clk         : out std_logic  -- tx_serial_clk.clk
--    pll_powerdown   : in  std_logic := '0'; -- pll_powerdown.pll_powerdown
--    pll_refclk0     : in  std_logic := '0'; --   pll_refclk0.clk
--    pll_locked      : out std_logic;        --    pll_locked.pll_locked
--    pll_cal_busy    : out std_logic;        --  pll_cal_busy.pll_cal_busy
--    mcgb_rst        : in  std_logic := '0';
--    mcgb_serial_clk : out std_logic         -- tx_serial_clk.clk
  );
  end component;

  component ip_arria10_e1sg_transceiver_reset_controller_1 is
  port (
    clock              : in  std_logic                    := '0';  -- clock.clk
    reset              : in  std_logic                    := '0';  -- reset.reset
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    tx_analogreset     : out std_logic_vector(0 downto 0);  -- tx_analogreset.tx_analogreset
    tx_digitalreset    : out std_logic_vector(0 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(0 downto 0);  -- tx_ready.tx_ready
    pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked.pll_locked
    pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select.pll_select
    tx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    rx_analogreset     : out std_logic_vector(0 downto 0);  -- rx_analogreset.rx_analogreset
    rx_digitalreset    : out std_logic_vector(0 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_ready           : out std_logic_vector(0 downto 0);  -- rx_ready.rx_ready
    rx_is_lockedtodata : in  std_logic_vector(0 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0')  -- rx_cal_busy.rx_cal_busy
  );
  end component;

  component ip_arria10_e1sg_transceiver_reset_controller_3 is
    port (
      clock              : in  std_logic                    := '0';  -- clk
      pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked
      pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown
      pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select
      reset              : in  std_logic                    := '0';  -- reset
      rx_analogreset     : out std_logic_vector(2 downto 0);  -- rx_analogreset
      rx_cal_busy        : in  std_logic_vector(2 downto 0) := (others => '0');  -- rx_cal_busy
      rx_digitalreset    : out std_logic_vector(2 downto 0);  -- rx_digitalreset
      rx_is_lockedtodata : in  std_logic_vector(2 downto 0) := (others => '0');  -- rx_is_lockedtodata
      rx_ready           : out std_logic_vector(2 downto 0);  -- rx_ready
      tx_analogreset     : out std_logic_vector(2 downto 0);  -- tx_analogreset
      tx_cal_busy        : in  std_logic_vector(2 downto 0) := (others => '0');  -- tx_cal_busy
      tx_digitalreset    : out std_logic_vector(2 downto 0);  -- tx_digitalreset
      tx_ready           : out std_logic_vector(2 downto 0)  -- tx_ready
    );
  end component ip_arria10_e1sg_transceiver_reset_controller_3;

  component ip_arria10_e1sg_transceiver_reset_controller_4
  port (
    clock              : in  std_logic                    := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                    := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(3 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(3 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(3 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(3 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(3 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(3 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(3 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(3 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(3 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e1sg_transceiver_reset_controller_12
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(11 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(11 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(11 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(11 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(11 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(11 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(11 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(11 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(11 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e1sg_transceiver_reset_controller_24
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(23 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(23 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(23 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(23 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(23 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(23 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(23 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e1sg_transceiver_reset_controller_48
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(47 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(47 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(47 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(47 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(47 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(47 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(47 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e2sg
  ------------------------------------------------------------------------------

  component ip_arria10_e2sg_phy_10gbase_r is
  port (
        rx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
        rx_cal_busy             : out std_logic_vector(0 downto 0);  -- rx_cal_busy.rx_cal_busy
        rx_cdr_refclk0          : in  std_logic                     := '0';  -- rx_cdr_refclk0.clk
        rx_clkout               : out std_logic_vector(0 downto 0);  -- rx_clkout.clk
        rx_control              : out std_logic_vector(7 downto 0);  -- rx_control.rx_control
        rx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_coreclkin.clk
        rx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
        rx_enh_blk_lock         : out std_logic_vector(0 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
        rx_enh_data_valid       : out std_logic_vector(0 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
        rx_enh_fifo_del         : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
        rx_enh_fifo_empty       : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
        rx_enh_fifo_full        : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
        rx_enh_fifo_insert      : out std_logic_vector(0 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
        rx_enh_highber          : out std_logic_vector(0 downto 0);  -- rx_enh_highber.rx_enh_highber
        rx_is_lockedtodata      : out std_logic_vector(0 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
        rx_is_lockedtoref       : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
        rx_parallel_data        : out std_logic_vector(63 downto 0);  -- rx_parallel_data.rx_parallel_data
        rx_prbs_done            : out std_logic_vector(0 downto 0);  -- rx_prbs_done.rx_prbs_done
        rx_prbs_err             : out std_logic_vector(0 downto 0);  -- rx_prbs_err.rx_prbs_err
        rx_prbs_err_clr         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
        rx_serial_data          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_serial_data.rx_serial_data
        rx_seriallpbken         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
        tx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
        tx_cal_busy             : out std_logic_vector(0 downto 0);  -- tx_cal_busy.tx_cal_busy
        tx_clkout               : out std_logic_vector(0 downto 0);  -- tx_clkout.clk
        tx_control              : in  std_logic_vector(7 downto 0)  := (others => '0');  -- tx_control.tx_control
        tx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_coreclkin.clk
        tx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
        tx_enh_data_valid       : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
        tx_enh_fifo_empty       : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
        tx_enh_fifo_full        : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
        tx_enh_fifo_pempty      : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
        tx_enh_fifo_pfull       : out std_logic_vector(0 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
        tx_err_ins              : in  std_logic                     := '0';  -- tx_err_ins.tx_err_ins
        tx_parallel_data        : in  std_logic_vector(63 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
        tx_serial_clk0          : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_serial_clk0.clk
        tx_serial_data          : out std_logic_vector(0 downto 0);  -- tx_serial_data.tx_serial_data
        unused_rx_control       : out std_logic_vector(11 downto 0);  -- unused_rx_control.unused_rx_control
        unused_rx_parallel_data : out std_logic_vector(63 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
        unused_tx_control       : in  std_logic_vector(8 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
        unused_tx_parallel_data : in  std_logic_vector(63 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
    );
  end component;

  component ip_arria10_e2sg_phy_10gbase_r_3 is
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- read
    reconfig_address        : in  std_logic_vector(11 downto 0)  := (others => '0');  -- address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reset
    rx_analogreset          : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_analogreset
    rx_cal_busy             : out std_logic_vector(2 downto 0);  -- rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- clk
    rx_clkout               : out std_logic_vector(2 downto 0);  -- clk
    rx_control              : out std_logic_vector(23 downto 0);  -- rx_control
    rx_coreclkin            : in  std_logic_vector(2 downto 0)   := (others => '0');  -- clk
    rx_digitalreset         : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(2 downto 0);  -- rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(2 downto 0);  -- rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(2 downto 0);  -- rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(2 downto 0);  -- rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(2 downto 0);  -- rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(2 downto 0);  -- rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(2 downto 0);  -- rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(2 downto 0);  -- rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(2 downto 0);  -- rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(191 downto 0);  -- rx_parallel_data
    rx_prbs_done            : out std_logic_vector(2 downto 0);  -- rx_prbs_done
    rx_prbs_err             : out std_logic_vector(2 downto 0);  -- rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(2 downto 0)   := (others => '0');  -- rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(2 downto 0)   := (others => '0');  -- tx_analogreset
    tx_cal_busy             : out std_logic_vector(2 downto 0);  -- tx_cal_busy
    tx_clkout               : out std_logic_vector(2 downto 0);  -- clk
    tx_control              : in  std_logic_vector(23 downto 0)  := (others => '0');  -- tx_control
    tx_coreclkin            : in  std_logic_vector(2 downto 0)   := (others => '0');  -- clk
    tx_digitalreset         : in  std_logic_vector(2 downto 0)   := (others => '0');  -- tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(2 downto 0)   := (others => '0');  -- tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(2 downto 0);  -- tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(2 downto 0);  -- tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(2 downto 0);  -- tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(2 downto 0);  -- tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(2 downto 0)   := (others => '0');  -- tx_err_ins
    tx_parallel_data        : in  std_logic_vector(191 downto 0) := (others => '0');  -- tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(2 downto 0)   := (others => '0');  -- clk
    tx_serial_data          : out std_logic_vector(2 downto 0);  -- tx_serial_data
    unused_rx_control       : out std_logic_vector(35 downto 0);  -- unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(191 downto 0);  -- unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(26 downto 0)  := (others => '0');  -- unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(191 downto 0) := (others => '0')  -- unused_tx_parallel_data
  );
 end component;

  component ip_arria10_e2sg_phy_10gbase_r_4
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(11 downto 0)  := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(3 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(3 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(31 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(3 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(3 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(3 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(3 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(3 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(3 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(255 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(3 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(3 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(3 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(3 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(31 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(3 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(255 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(3 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(3 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(47 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(255 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(35 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(255 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e2sg_phy_10gbase_r_12
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(13 downto 0)  := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(11 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                      := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(11 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(95 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(11 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(11 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(11 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(11 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(11 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(11 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(767 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(11 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(11 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(11 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(11 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(95 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(11 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(767 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(11 downto 0)  := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(11 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(143 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(767 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(107 downto 0) := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(767 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e2sg_phy_10gbase_r_24
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)    := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(14 downto 0)   := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)   := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(23 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                       := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(23 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(191 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(23 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(23 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(23 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(23 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(23 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(23 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(1535 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(23 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(23 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(23 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(23 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(191 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(23 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(1535 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(23 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(23 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(287 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(1535 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e2sg_phy_10gbase_r_48
  port (
    reconfig_write          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_avmm.write
    reconfig_read           : in  std_logic_vector(0 downto 0)    := (others => '0');  -- .read
    reconfig_address        : in  std_logic_vector(15 downto 0)   := (others => '0');  -- .address
    reconfig_writedata      : in  std_logic_vector(31 downto 0)   := (others => '0');  -- .writedata
    reconfig_readdata       : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest    : out std_logic_vector(0 downto 0);  -- .waitrequest
    reconfig_clk            : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_clk.clk
    reconfig_reset          : in  std_logic_vector(0 downto 0)    := (others => '0');  -- reconfig_reset.reset
    rx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy             : out std_logic_vector(47 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk0          : in  std_logic                       := '0';  -- rx_cdr_refclk0.clk
    rx_clkout               : out std_logic_vector(47 downto 0);  -- rx_clkout.clk
    rx_control              : out std_logic_vector(383 downto 0);  -- rx_control.rx_control
    rx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_coreclkin.clk
    rx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_enh_blk_lock         : out std_logic_vector(47 downto 0);  -- rx_enh_blk_lock.rx_enh_blk_lock
    rx_enh_data_valid       : out std_logic_vector(47 downto 0);  -- rx_enh_data_valid.rx_enh_data_valid
    rx_enh_fifo_del         : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_del.rx_enh_fifo_del
    rx_enh_fifo_empty       : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_empty.rx_enh_fifo_empty
    rx_enh_fifo_full        : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_full.rx_enh_fifo_full
    rx_enh_fifo_insert      : out std_logic_vector(47 downto 0);  -- rx_enh_fifo_insert.rx_enh_fifo_insert
    rx_enh_highber          : out std_logic_vector(47 downto 0);  -- rx_enh_highber.rx_enh_highber
    rx_is_lockedtodata      : out std_logic_vector(47 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref       : out std_logic_vector(47 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_parallel_data        : out std_logic_vector(3071 downto 0);  -- rx_parallel_data.rx_parallel_data
    rx_prbs_done            : out std_logic_vector(47 downto 0);  -- rx_prbs_done.rx_prbs_done
    rx_prbs_err             : out std_logic_vector(47 downto 0);  -- rx_prbs_err.rx_prbs_err
    rx_prbs_err_clr         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_prbs_err_clr.rx_prbs_err_clr
    rx_serial_data          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_serial_data.rx_serial_data
    rx_seriallpbken         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- rx_seriallpbken.rx_seriallpbken
    tx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy             : out std_logic_vector(47 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_clkout               : out std_logic_vector(47 downto 0);  -- tx_clkout.clk
    tx_control              : in  std_logic_vector(383 downto 0)  := (others => '0');  -- tx_control.tx_control
    tx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_coreclkin.clk
    tx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_enh_data_valid       : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_enh_data_valid.tx_enh_data_valid
    tx_enh_fifo_empty       : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_empty.tx_enh_fifo_empty
    tx_enh_fifo_full        : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_full.tx_enh_fifo_full
    tx_enh_fifo_pempty      : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_pempty.tx_enh_fifo_pempty
    tx_enh_fifo_pfull       : out std_logic_vector(47 downto 0);  -- tx_enh_fifo_pfull.tx_enh_fifo_pfull
    tx_err_ins              : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_err_ins.tx_err_ins
    tx_parallel_data        : in  std_logic_vector(3071 downto 0) := (others => '0');  -- tx_parallel_data.tx_parallel_data
    tx_serial_clk0          : in  std_logic_vector(47 downto 0)   := (others => '0');  -- tx_serial_clk0.clk
    tx_serial_data          : out std_logic_vector(47 downto 0);  -- tx_serial_data.tx_serial_data
    unused_rx_control       : out std_logic_vector(575 downto 0);  -- unused_rx_control.unused_rx_control
    unused_rx_parallel_data : out std_logic_vector(3071 downto 0);  -- unused_rx_parallel_data.unused_rx_parallel_data
    unused_tx_control       : in  std_logic_vector(431 downto 0)  := (others => '0');  -- unused_tx_control.unused_tx_control
    unused_tx_parallel_data : in  std_logic_vector(3071 downto 0) := (others => '0')  -- unused_tx_parallel_data.unused_tx_parallel_data
  );
  end component;

  component ip_arria10_e2sg_transceiver_pll_10g is
  port (
    mcgb_rst              : in  std_logic                     := '0';  -- mcgb_rst.mcgb_rst
    mcgb_serial_clk       : out std_logic;  -- mcgb_serial_clk.clk
    pll_cal_busy          : out std_logic;  -- pll_cal_busy.pll_cal_busy
    pll_locked            : out std_logic;  -- pll_locked.pll_locked
    pll_powerdown         : in  std_logic                     := '0';  -- pll_powerdown.pll_powerdown
    pll_refclk0           : in  std_logic                     := '0';  -- pll_refclk0.clk
    reconfig_write0       : in  std_logic                     := '0';  -- reconfig_avmm0.write
    reconfig_read0        : in  std_logic                     := '0';  -- .read
    reconfig_address0     : in  std_logic_vector(9 downto 0)  := (others => '0');  -- .address
    reconfig_writedata0   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reconfig_readdata0    : out std_logic_vector(31 downto 0);  -- .readdata
    reconfig_waitrequest0 : out std_logic;  -- .waitrequest
    reconfig_clk0         : in  std_logic                     := '0';  -- reconfig_clk0.clk
    reconfig_reset0       : in  std_logic                     := '0';  -- reconfig_reset0.reset
    tx_serial_clk         : out std_logic  -- tx_serial_clk.clk
--    pll_powerdown   : in  std_logic := '0'; -- pll_powerdown.pll_powerdown
--    pll_refclk0     : in  std_logic := '0'; --   pll_refclk0.clk
--    pll_locked      : out std_logic;        --    pll_locked.pll_locked
--    pll_cal_busy    : out std_logic;        --  pll_cal_busy.pll_cal_busy
--    mcgb_rst        : in  std_logic := '0';
--    mcgb_serial_clk : out std_logic         -- tx_serial_clk.clk
  );
  end component;

  component ip_arria10_e2sg_transceiver_reset_controller_1 is
  port (
    clock              : in  std_logic                    := '0';  -- clock.clk
    reset              : in  std_logic                    := '0';  -- reset.reset
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    tx_analogreset     : out std_logic_vector(0 downto 0);  -- tx_analogreset.tx_analogreset
    tx_digitalreset    : out std_logic_vector(0 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(0 downto 0);  -- tx_ready.tx_ready
    pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked.pll_locked
    pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select.pll_select
    tx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    rx_analogreset     : out std_logic_vector(0 downto 0);  -- rx_analogreset.rx_analogreset
    rx_digitalreset    : out std_logic_vector(0 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_ready           : out std_logic_vector(0 downto 0);  -- rx_ready.rx_ready
    rx_is_lockedtodata : in  std_logic_vector(0 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0')  -- rx_cal_busy.rx_cal_busy
  );
  end component;

  component ip_arria10_e2sg_transceiver_reset_controller_3 is
    port (
      clock              : in  std_logic                    := '0';  -- clk
      pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked
      pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown
      pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select
      reset              : in  std_logic                    := '0';  -- reset
      rx_analogreset     : out std_logic_vector(2 downto 0);  -- rx_analogreset
      rx_cal_busy        : in  std_logic_vector(2 downto 0) := (others => '0');  -- rx_cal_busy
      rx_digitalreset    : out std_logic_vector(2 downto 0);  -- rx_digitalreset
      rx_is_lockedtodata : in  std_logic_vector(2 downto 0) := (others => '0');  -- rx_is_lockedtodata
      rx_ready           : out std_logic_vector(2 downto 0);  -- rx_ready
      tx_analogreset     : out std_logic_vector(2 downto 0);  -- tx_analogreset
      tx_cal_busy        : in  std_logic_vector(2 downto 0) := (others => '0');  -- tx_cal_busy
      tx_digitalreset    : out std_logic_vector(2 downto 0);  -- tx_digitalreset
      tx_ready           : out std_logic_vector(2 downto 0)  -- tx_ready
    );
  end component;

  component ip_arria10_e2sg_transceiver_reset_controller_4
  port (
    clock              : in  std_logic                    := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0) := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                    := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(3 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(3 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(3 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(3 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(3 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(3 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(3 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(3 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(3 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e2sg_transceiver_reset_controller_12
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(11 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(11 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(11 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(11 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(11 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(11 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(11 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(11 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(11 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e2sg_transceiver_reset_controller_24
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(23 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(23 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(23 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(23 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(23 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(23 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(23 downto 0)  -- tx_ready.tx_ready
  );
  end component;

  component ip_arria10_e2sg_transceiver_reset_controller_48
  port (
    clock              : in  std_logic                     := '0';  -- clock.clk
    pll_locked         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_locked.pll_locked
    pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown.pll_powerdown
    pll_select         : in  std_logic_vector(0 downto 0)  := (others => '0');  -- pll_select.pll_select
    reset              : in  std_logic                     := '0';  -- reset.reset
    rx_analogreset     : out std_logic_vector(47 downto 0);  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => '0');  -- rx_cal_busy.rx_cal_busy
    rx_digitalreset    : out std_logic_vector(47 downto 0);  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : in  std_logic_vector(47 downto 0) := (others => '0');  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_ready           : out std_logic_vector(47 downto 0);  -- rx_ready.rx_ready
    tx_analogreset     : out std_logic_vector(47 downto 0);  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => '0');  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : out std_logic_vector(47 downto 0);  -- tx_digitalreset.tx_digitalreset
    tx_ready           : out std_logic_vector(47 downto 0)  -- tx_ready.tx_ready
  );
  end component;
end tech_10gbase_r_component_pkg;
