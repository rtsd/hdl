-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--
-- Description:
--
-- Remarks:
--
--
-- Some more remarks:

library IEEE, common_lib, dp_lib, unb1_board_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;

entity node_unb1_terminal_bg_mesh_db is
  generic(
    g_sim                     : boolean := false;
    g_sim_level               : natural := 0;

    -- BLOCK GENERATOR
    g_use_bg                  : boolean := true;

    -- MESH TERMINAL
    -- System
    g_node_type               : t_e_unb1_board_node := e_any;  -- or e_fn, or e_bn
    g_nof_bus                 : natural := 4;  -- one bus to each of the 4 nodes on the other side of the mesh
    -- User
    g_usr_use_complex         : boolean := false;  -- when TRUE transport sosi im & re fields via DP data, else transport sosi data via DP data
    g_usr_data_w              : natural := 16;  -- <= 32, to avoid need for DP packet data packing and to fit on the tr_nonbonded PHY data width of 32 bit
    g_usr_frame_len           : natural := 20;
    g_usr_nof_streams         : natural := 3;  -- number of user streams per bus
    -- Phy
    g_phy_nof_serial          : natural := 3;  -- up to 4 serial lanes per bus
    g_phy_gx_mbps             : natural := 5000;
    g_phy_rx_fifo_size        : natural := c_bram_m9k_fifo_depth;  -- g_fifos=TRUE in mms_tr_nonbonded, choose to use full BRAM size = 256 for FIFO depth at output from PHY
    g_phy_ena_reorder         : boolean := true;
    -- Tx
    g_use_tx                  : boolean := true;
    g_tx_input_use_fifo       : boolean := true;  -- Tx input uses FIFO to create slack for inserting DP Packet and Uthernet frame headers
    g_tx_input_fifo_size      : natural := c_bram_m9k_fifo_depth;  -- g_tx_input_use_fifo=TRUE, choose to use full BRAM size = 256 for FIFO depth at input to uth_terminal_tx
    g_tx_input_fifo_fill      : natural := 0;
    -- Rx
    g_use_rx                  : boolean := true;
    g_rx_output_use_fifo      : boolean := true;  -- Rx output provides FIFOs to ensure that dp_distribute does not get blocked due to substantial backpressure on another output
    g_rx_output_fifo_size     : natural := c_bram_m9k_fifo_depth;  -- g_rx_output_use_fifo, choose to use full BRAM size = 256 for FIFO depth at output of uth_terminal_rx
    g_rx_output_fifo_fill     : natural := 0;
    g_rx_timeout_w            : natural := 0;  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive uth_rx snk_in.valid
    -- Monitoring
    g_mon_select              : natural := 0;  -- 0 = no SOSI data buffers monitor via MM
                                                   -- 1 = enable monitor the Rx UTH packets per serial lane after the tr_nonbonded
                                                   -- 2 = enable monitor the Rx UTH packets per serial lane after the mesh reorder
                                                   -- 3 = enable monitor the Rx DP  packets per serial lane after the uth_rx
                                                   -- 4 = enable monitor the Rx DP  packets per user stream after the dp_distribute
                                                   -- 5 = enable monitor the Tx UTH packets per serial lane to the tr_nonbonded
                                                   -- 6 = enable monitor the Tx UTH packets per serial lane to the mesh reorder
    g_mon_nof_words           : natural := 1024;
    g_mon_use_sync            : boolean := true;
    -- UTH
    g_uth_len_max             : natural := 255;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
    g_uth_typ_ofs             : natural := 256;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs

    -- Auxiliary Interface
    g_aux                     : t_c_unb1_board_aux := c_unb1_board_aux;
    -- BSN ALIGNER
    g_use_bsn_align           : boolean := true;  -- default TRUE, support FALSE for packet flow debugging purposes (faster synthesis)
    -- DATA BUFFER
    g_use_data_buf            : boolean := true
  );
  port(
    -- System
    chip_id                     : in  std_logic_vector(g_aux.chip_id_w - 1 downto 0);  -- [2:0]

    mm_rst                      : in  std_logic;
    mm_clk                      : in  std_logic;  -- 50 MHz from xo_clk PLL in SOPC system
    dp_rst                      : in  std_logic;
    dp_clk                      : in  std_logic;  -- 200 MHz from CLK system clock
    dp_pps                      : in  std_logic := '0';
    tr_mesh_clk                 : in  std_logic;  -- 156.25 MHz from SB_CLK transceiver clock
    cal_clk                     : in  std_logic;  -- 40 MHz from xo_clk PLL in SOPC system

    -- MM interface
    -- . block generator
    reg_diag_bg_mosi            : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diag_bg_miso            : out t_mem_miso;
    ram_diag_bg_mosi            : in  t_mem_mosi := c_mem_mosi_rst;
    ram_diag_bg_miso            : out t_mem_miso;
    -- . diag_data_buffer
    ram_diag_data_buf_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    ram_diag_data_buf_miso      : out t_mem_miso;
    reg_diag_data_buf_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diag_data_buf_miso      : out t_mem_miso;
    -- . tr_nonbonded
    reg_tr_nonbonded_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    reg_tr_nonbonded_miso       : out t_mem_miso;
    reg_diagnostics_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diagnostics_miso        : out t_mem_miso;
    -- . diag_data_buffer_mesh
    ram_mesh_diag_data_buf_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_mesh_diag_data_buf_miso : out t_mem_miso;
    -- . bsn_monitor
    reg_bsn_monitor_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_monitor_miso        : out t_mem_miso;

    -- Datapath User interface (4 nodes)(4 input streams)
    tx_usr_siso_2arr            : out t_unb1_board_mesh_siso_2arr;
    tx_usr_sosi_2arr            : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));  -- Tx
    rx_usr_siso_2arr            : in  t_unb1_board_mesh_siso_2arr := (others => (others => c_dp_siso_rdy));
    rx_usr_sosi_2arr            : out t_unb1_board_mesh_sosi_2arr;  -- Rx

    -- Mesh serial interface (tr_nonbonded)
    tx_serial_2arr              : out t_unb1_board_mesh_sl_2arr;  -- Tx
    rx_serial_2arr              : in  t_unb1_board_mesh_sl_2arr := (others => (others => '0'))  -- Rx support for diagnostics
  );
end node_unb1_terminal_bg_mesh_db;

architecture str of node_unb1_terminal_bg_mesh_db is
  -----------------------------------------------------------------------------
  -- Block Generator
  -----------------------------------------------------------------------------
  constant c_use_usr_input           : boolean := not(g_use_bg);
  constant c_bg_nof_streams          : natural := g_nof_bus * g_usr_nof_streams;  -- 4 x 3 = 12
  constant c_in_dat_w                : natural := g_usr_data_w;
  constant c_bg_addr_w               : natural := 7;
  constant c_file_name_prefix        : string  := "hex/bg_in_data";

  -----------------------------------------------------------------------------
  -- BSN Aligner
  -----------------------------------------------------------------------------
  constant c_bsn_align_nof_streams   : natural := c_bg_nof_streams;
  constant c_block_size              : natural := 128;  -- FIXME: current fn_beamformer output block size.
  constant c_block_period            : natural := 192;
  constant c_bsn_align_latency       : natural := 3;
  constant c_bsn_align_xoff_timeout  : natural :=  c_bsn_align_latency * 2  * c_block_period;  -- flush factor 2 longer than needed
  constant c_bsn_align_sop_timeout   : natural := (c_bsn_align_latency + 1) * c_block_period;  -- wait somewhat more than c_bsn_align_latency periods

  -----------------------------------------------------------------------------
  -- BSN Monitor
  -----------------------------------------------------------------------------
  constant c_bsn_mon_nof_streams     : natural := 1;
  constant c_bsn_sync_time_out       : natural := (800000 * 256 * 10) / 8;

  -----------------------------------------------------------------------------
  -- Data Buffer
  -----------------------------------------------------------------------------
  constant c_db_nof_streams          : natural := c_bg_nof_streams;
  constant c_db_nof_data             : natural := 128;

  --. Block generator
  signal bg_snk_out_arr           : t_dp_siso_arr(c_bg_nof_streams - 1 downto 0);
  signal bg_snk_in_arr            : t_dp_sosi_arr(c_bg_nof_streams - 1 downto 0);
  signal bg_src_in_arr            : t_dp_siso_arr(c_bg_nof_streams - 1 downto 0);
  signal bg_src_out_arr           : t_dp_sosi_arr(c_bg_nof_streams - 1 downto 0);

  signal bg_out_siso_2arr         : t_unb1_board_mesh_siso_2arr;
  signal bg_out_sosi_2arr         : t_unb1_board_mesh_sosi_2arr;

  --. Mesh terminal
  signal rx_usr_i_siso_2arr       : t_unb1_board_mesh_siso_2arr;
  signal rx_usr_i_sosi_2arr       : t_unb1_board_mesh_sosi_2arr;

  --. BSN Aligner
  signal bsn_align_snk_in_arr     : t_dp_sosi_arr(c_bg_nof_streams - 1 downto 0);
  signal bsn_align_snk_out_arr    : t_dp_siso_arr(c_bg_nof_streams - 1 downto 0);
  signal bsn_align_src_in_arr     : t_dp_siso_arr(c_bg_nof_streams - 1 downto 0);
  signal bsn_align_src_out_arr    : t_dp_sosi_arr(c_bg_nof_streams - 1 downto 0);

  --. BSN Monitor
  signal bsn_mon_snk_out_arr      : t_dp_siso_arr(c_bsn_mon_nof_streams - 1 downto 0);
  signal bsn_mon_snk_in_arr       : t_dp_sosi_arr(c_bsn_mon_nof_streams - 1 downto 0);
begin
  ---------------------------------------------------------------------------------------
  -- From 2d to 1d array. Input port to input BG.
  ---------------------------------------------------------------------------------------
  gen_i_a : for I in 0 to g_nof_bus - 1 generate
    gen_j_a : for J in 0 to g_usr_nof_streams - 1 generate
      bg_snk_in_arr(I * g_usr_nof_streams + J) <= tx_usr_sosi_2arr(I)(J);
      tx_usr_siso_2arr(I)(J)                 <= bg_snk_out_arr(I * g_usr_nof_streams + J);
    end generate;
  end generate;

  ---------------------------------------------------------------------------------------
  -- Block Generator
  ---------------------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    -- Generate configurations
    g_use_usr_input      => c_use_usr_input,
    g_use_bg             => g_use_bg,
    g_use_tx_seq         => false,
    -- General
    g_nof_streams        => c_bg_nof_streams,
    -- BG settings
    g_use_bg_buffer_ram  => true,
    g_buf_dat_w          => c_nof_complex * c_in_dat_w,
    g_buf_addr_w         => c_bg_addr_w,
    g_file_name_prefix   => c_file_name_prefix,
    -- User input multiplexer option
    g_usr_bypass_xonoff  => false
  )
  port map(
    -- System
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,
    en_sync          => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi => reg_diag_bg_mosi,
    reg_bg_ctrl_miso => reg_diag_bg_miso,
    ram_bg_data_mosi => ram_diag_bg_mosi,
    ram_bg_data_miso => ram_diag_bg_miso,

    -- ST interface
    usr_siso_arr     => bg_snk_out_arr,
    usr_sosi_arr     => bg_snk_in_arr,
    out_siso_arr     => bg_src_in_arr,
    out_sosi_arr     => bg_src_out_arr
  );

  ---------------------------------------------------------------------------------------
  -- From 1d to 2d array. Output BG to input Mesh
  ---------------------------------------------------------------------------------------
  gen_i_b : for I in 0 to g_nof_bus - 1 generate
    gen_j_b : for J in 0 to g_usr_nof_streams - 1 generate
      bg_src_in_arr(I * g_usr_nof_streams + J) <= bg_out_siso_2arr(I)(J);
      bg_out_sosi_2arr(I)(J)                 <= bg_src_out_arr(I * g_usr_nof_streams + J);
    end generate;
  end generate;

  u_mesh_terminal : entity unb1_board_lib.unb1_board_terminals_mesh
  generic map(
    g_sim                 => g_sim,
    g_sim_level           => g_sim_level,
    -- System
    g_node_type           => g_node_type,
    g_nof_bus             => g_nof_bus,
    -- User
    g_usr_use_complex     => g_usr_use_complex,
    g_usr_data_w          => g_usr_data_w,
    g_usr_frame_len       => g_usr_frame_len,
    g_usr_nof_streams     => g_usr_nof_streams,
    -- Phy
    g_phy_nof_serial      => g_phy_nof_serial,
    g_phy_gx_mbps         => g_phy_gx_mbps,
    g_phy_rx_fifo_size    => g_phy_rx_fifo_size,
    g_phy_ena_reorder     => g_phy_ena_reorder,
    -- Tx
    g_use_tx              => g_use_tx,
    g_tx_input_use_fifo   => g_tx_input_use_fifo,
    g_tx_input_fifo_size  => g_tx_input_fifo_size,
    g_tx_input_fifo_fill  => g_tx_input_fifo_fill,
    -- Rx
    g_use_rx              => g_use_rx,
    g_rx_output_use_fifo  => g_rx_output_use_fifo,
    g_rx_output_fifo_size => g_rx_output_fifo_size,
    g_rx_output_fifo_fill => g_rx_output_fifo_fill,
    g_rx_timeout_w        => g_rx_timeout_w,

    -- Monitoring
    g_mon_select          => g_mon_select,
    g_mon_nof_words       => g_mon_nof_words,
    g_mon_use_sync        => g_mon_use_sync,

    -- UTH
    g_uth_len_max         => g_uth_len_max,
    g_uth_typ_ofs         => g_uth_typ_ofs
  )
  port map (
    chip_id                => chip_id,

    mm_rst                 => mm_rst,
    mm_clk                 => mm_clk,
    dp_rst                 => dp_rst,
    dp_clk                 => dp_clk,
    dp_sync                => dp_pps,
    tr_clk                 => tr_mesh_clk,
    cal_clk                => cal_clk,

    -- User interface (4 nodes)(4 input streams)
    tx_usr_siso_2arr       => bg_out_siso_2arr,
    tx_usr_sosi_2arr       => bg_out_sosi_2arr,  -- <== Data to the Mesh
    rx_usr_siso_2arr       => rx_usr_i_siso_2arr,
    rx_usr_sosi_2arr       => rx_usr_i_sosi_2arr,  -- ==> Data from the Mesh

    -- Serial (tr_nonbonded)
    tx_serial_2arr         => tx_serial_2arr,
    rx_serial_2arr         => rx_serial_2arr,

    -- MM Control
    -- . tr_nonbonded
    reg_tr_nonbonded_mosi  => reg_tr_nonbonded_mosi,
    reg_tr_nonbonded_miso  => reg_tr_nonbonded_miso,
    reg_diagnostics_mosi   => reg_diagnostics_mosi,
    reg_diagnostics_miso   => reg_diagnostics_miso,

    -- . monitor data buffer
    ram_diag_data_buf_mosi => ram_mesh_diag_data_buf_mosi,
    ram_diag_data_buf_miso => ram_mesh_diag_data_buf_miso
  );

  ---------------------------------------------------------------------------------------
  -- From 2d to 1d array. Input port to input BG.
  ---------------------------------------------------------------------------------------
  gen_i_c : for I in 0 to g_nof_bus - 1 generate
    gen_j_c : for J in 0 to g_usr_nof_streams - 1 generate
      bsn_align_snk_in_arr(I * g_usr_nof_streams + J) <= rx_usr_i_sosi_2arr(I)(J);
      rx_usr_i_siso_2arr(I)(J)                      <= bsn_align_snk_out_arr(I * g_usr_nof_streams + J);
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- BSN ALIGNER
  -----------------------------------------------------------------------------
  gen_bsn_align : if g_use_bsn_align generate
    u_dp_bsn_align : entity dp_lib.dp_bsn_align
    generic map (
      g_block_size           => c_block_size,
      g_block_period         => c_block_period,
      g_nof_input            => c_bsn_align_nof_streams,
      g_xoff_timeout         => c_bsn_align_xoff_timeout,
      g_sop_timeout          => c_bsn_align_sop_timeout,
      g_bsn_latency          => c_bsn_align_latency,
      g_bsn_request_pipeline => 2
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,

      snk_out_arr => bsn_align_snk_out_arr,
      snk_in_arr  => bsn_align_snk_in_arr,

      src_in_arr  => bsn_align_src_in_arr,
      src_out_arr => bsn_align_src_out_arr
    );
  end generate;

  gen_no_bsn_align : if not(g_use_bsn_align) generate
    bsn_align_src_out_arr <= bsn_align_snk_in_arr;
    bsn_align_snk_out_arr <= bsn_align_src_in_arr;
  end generate;

  -----------------------------------------------------------------------------
  -- BSN monitors at the output of the BSN aligner
  -----------------------------------------------------------------------------
  u_dp_bsn_monitor : entity dp_lib.mms_dp_bsn_monitor
  generic map (
    g_nof_streams        => c_bsn_mon_nof_streams,
    g_sync_timeout       => c_bsn_sync_time_out,
    g_log_first_bsn      => true
  )
  port map (
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    reg_mosi    => reg_bsn_monitor_mosi,
    reg_miso    => reg_bsn_monitor_miso,

    dp_rst      => dp_rst,
    dp_clk      => dp_clk,
    in_siso_arr => bsn_mon_snk_out_arr,
    in_sosi_arr => bsn_mon_snk_in_arr
  );

  bsn_mon_snk_in_arr  <= bsn_align_src_out_arr(c_bsn_mon_nof_streams - 1 downto 0);
  bsn_mon_snk_out_arr <= bsn_align_src_in_arr(c_bsn_mon_nof_streams - 1 downto 0);

  gen_data_buf : if g_use_data_buf generate
    u_data_buf : entity diag_lib.mms_diag_data_buffer
    generic map (
      g_nof_streams  => c_db_nof_streams,
      g_data_w       => g_usr_data_w,
      g_buf_nof_data => c_db_nof_data,
      g_buf_use_sync => true
    )
    port map (
      -- System
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,
      -- MM interface
      ram_data_buf_mosi => ram_diag_data_buf_mosi,
      ram_data_buf_miso => ram_diag_data_buf_miso,
      reg_data_buf_mosi => reg_diag_data_buf_mosi,
      reg_data_buf_miso => reg_diag_data_buf_miso,
      -- ST interface
      in_sync           => bsn_align_src_out_arr(0).sync,
      in_sosi_arr       => bsn_align_src_out_arr
    );
  end generate;

  ---------------------------------------------------------------------------------------
  -- From 1d to 2d array. Output BSN Aligner to port output
  ---------------------------------------------------------------------------------------
  gen_i_d : for I in 0 to g_nof_bus - 1 generate
    gen_j_d : for J in 0 to g_usr_nof_streams - 1 generate
      bsn_align_src_in_arr(I * g_usr_nof_streams + J) <= rx_usr_siso_2arr(I)(J);
      rx_usr_sosi_2arr(I)(J)                        <= bsn_align_src_out_arr(I * g_usr_nof_streams + J);
    end generate;
  end generate;
end str;
