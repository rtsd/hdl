#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for the tb_mms_reorder_rewire entity.

   Description: This testcase sends a set of different selections to 
                the reorder_rewire entity and verifies the output. 

   Usage:

   > python tc_mms_reorder_rewire.py --unb 0 --bn 0 --sim 

"""

###############################################################################
# System imports
import test_case
import node_io
import pi_diag_block_gen
import pi_diag_data_buffer
import pi_reorder_rewire

import sys, os
from tools import *
from common import *  
import mem_init_file

###############################################################################

# Create a test case object
tc = test_case.Testcase('TB - ', '')

# Constants/Generics that are shared between VHDL and Python
# Name                   Value   Default   Description
# START_VHDL_GENERICS
g_nof_streams     = 8       # 16 
g_blocks_per_sync = 16 #32   # 781250

# Define settings for the block generator
c_bg_nof_streams          = g_nof_streams 
c_bg_ram_size             = 16
c_db_nof_streams          = g_nof_streams
c_db_ram_size             = 16
c_samples_per_packet      = 16
c_gapsize                 = 8
c_mem_low_addr            = 0
c_mem_high_addr           = c_bg_ram_size-1
c_bsn_init                = 42
c_in_dat_w                = 8
c_dat_db_w                = 8

c_nof_read_back_samples   = 1
c_write_bg_data           = True  # BG data must be written in HW, always! 
c_write_bg_data_to_file   = False  
                             
tc.append_log(3, '>>>')      
tc.append_log(1, '>>> Title : Test script for tb_mms_reorder_rewire' )
tc.append_log(3, '>>>')
tc.append_log(3, '')
tc.set_result('PASSED')

# Create access object for nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

# Create block generator instance
bg = pi_diag_block_gen.PiDiagBlockGen(tc, io, c_bg_nof_streams, c_bg_ram_size, instanceName='')

rewire = pi_reorder_rewire.PiReorderRewire(tc, io, g_nof_streams) 

# Create databuffer instances
db = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = '', nofStreams=c_db_nof_streams, ramSizePerStream=c_db_ram_size)

if __name__ == "__main__":  

    ################################################################################
    ##
    ## Initialize the blockgenerators
    ##
    ################################################################################
    # - Write settings to the block generator 
    tc.append_log(3, '>>>')
    tc.append_log(3, '>>> Write settings to the block generator')
    tc.append_log(3, '>>>')

    bg.write_block_gen_settings(c_samples_per_packet, g_blocks_per_sync, c_gapsize, c_mem_low_addr, c_mem_high_addr, c_bsn_init)
    
    # - Create a list with the input data and write it to the RAMs of the block generator
    tc.append_log(3, '>>>')
    tc.append_log(3, '>>> Write data to the waveform RAM of all channels')
    tc.append_log(3, '>>>')
    inputData = []
    
    dataList=[]
    nodeData = []
    for i in xrange(0, g_nof_streams):
        data = []
        for j in xrange(0, c_bg_ram_size):
            data.append(i)   
        nodeData.append(data)
    dataList.append(nodeData)

    inputData = []
    in_h = 0
    for h in tc.nodeNrs:   
        for i in xrange(g_nof_streams):
            if c_write_bg_data == True:      
                bg.write_waveform_ram(dataList[in_h][i], i, [h])
            if c_write_bg_data_to_file == True:                                                                   
                filename = "../../src/hex/node" + str(h) + "/bg_in_data_" + str(i) + ".hex"
                mem_init_file.list_to_hex(list_in=dataList[in_h][i], filename=filename, mem_width=c_nof_complex*c_in_dat_w, mem_depth=2**(ceil_log2(c_bg_ram_size)))
            dataListComplex = bg.convert_concatenated_to_complex(dataList[in_h][i], c_in_dat_w)
            inputData.append(dataListComplex)  
        in_h = in_h+1
    
    selection_table = [[0,1,2,3,4,5,6,7],
                       [3,3,3,3,3,3,3,3],
                       [7,6,5,4,3,2,1,0],
                       [4,4,2,2,1,1,0,0],
                       [3,1,2,2,3,0,0,3],
                       [4,5,3,6,3,3,7,3],
                       [3,4,4,5,6,3,2,3],
                       [1,3,3,4,5,6,3,5]]
                       
    for t in range(len(selection_table)):
        ################################################################################
        ##
        ## Make reorder_rewire settings
        ##
        ################################################################################
        selection = selection_table[t]
        rewire.write_settings(selection)
        #print rewire.read_settings()
     
        # - Enable the block generator
        tc.append_log(3, '>>>')
        tc.append_log(3, '>>> Start the block generator')
        tc.append_log(3, '>>>')
        tc.append_log(3, '')

        bg.write_enable()

        do_until_ge(db.read_nof_words, ms_retry=3000, val=c_db_ram_size, s_timeout=3600)
        
        bg.write_disable()
        ###############################################################################
        #
        # Read data from data buffer
        #
        ###############################################################################
        db_out = []
        for i in range(c_bg_nof_streams): 
            db_out.append(flatten(db.read_data_buffer(streamNr=i, n=c_nof_read_back_samples, radix='uns', width=c_nof_complex*c_dat_db_w, nofColumns=8)))
        print flatten(db_out)
        if flatten(db_out) == selection: 
            tc.append_log(3, 'Selection OK!')
        else:
           tc.set_result(False) 
    ###############################################################################
    # End
    tc.set_section_id('')
    tc.append_log(3, '')
    tc.append_log(3, '>>>')
    tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
    tc.append_log(3, '>>>')
    
    sys.exit(tc.get_result())




