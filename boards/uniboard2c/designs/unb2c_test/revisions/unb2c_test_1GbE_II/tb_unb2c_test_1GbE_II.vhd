-------------------------------------------------------------------------------
--
-- Copyright (C) 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Tb to try loading design in simulator
-- Description:
-- Usage:
-- > as 12
-- > run 1 us.
--
-- Or try some MM:
-- > run -a (or run 100 us)
-- On command line do:
-- > python $UPE_GEAR/peripherals/util_system_info.py --gn 0 -n 0 -v 5 --sim
--
-- To run BG eth_tester in simuation do:
-- > run -a (or run 1 ms)
-- . use -n 10000 packets/s to have 1 packet per BG sync interval of 100 us in sim
-- > tc_unb2_test_eth.py --gn2 0 --stream 4 --dest loopback -r 10000 --sizes 1000 --interval 100 --scheme tx_rx --sim
-- . stop simulation.
--
-- Remark:
-- . To run with sim_tse or tech_tse use g_sim_level = 1 or 0 for u_eth_stream
--   in unb2c_test. The tech_tse also models the MM setup. Default use sim_tse
--   for faster sim.

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_unb2c_test_1GbE_II is
end tb_unb2c_test_1GbE_II;

architecture tb of tb_unb2c_test_1GbE_II is
  signal clk  : std_logic := '0';
  signal pps  : std_logic := '0';
  signal wdi  : std_logic := '0';

  signal eth_clk   : std_logic_vector(1 downto 0) := "00";
  signal eth_sgin  : std_logic_vector(1 downto 0);
  signal eth_sgout : std_logic_vector(1 downto 0);
begin
  clk <= not clk after 5 ns;
  eth_clk(0) <= not eth_clk(0) after 8 ns;
  eth_clk(1) <= not eth_clk(1) after 8 ns;

  pps <= not pps after 80 ns;

  eth_sgin <= eth_sgout;  -- loopback eth0 and eth1

  u_unb2c_test_1GbE_II : entity work.unb2c_test_1GbE_II
  generic map (
    g_sim        => true
  )
  port map (
    -- GENERAL
    CLK          => clk,
    PPS          => pps,
    WDI          => wdi,
    INTA         => OPEN,
    INTB         => OPEN,

    -- Others
    VERSION      => "00",
    ID           => "00000000",
    TESTIO       => OPEN,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_sgin,
    ETH_SGOUT    => eth_sgout,

    QSFP_LED     => open
  );
end tb;
