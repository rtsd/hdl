-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Array of dp_fifo_monitor instances
-- Description:
-- . see dp_fifo_monitor.vhd

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.dp_stream_pkg.all;

entity dp_fifo_monitor_arr is
  generic (
    g_nof_streams : natural
  );
  port (
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    dp_clk           : in  std_logic;
    dp_rst           : in  std_logic;

    reg_mosi         : in  t_mem_mosi;
    reg_miso         : out t_mem_miso;
    -- Status inputs
    rd_usedw_32b_arr : in  t_slv_32_arr(g_nof_streams - 1 downto 0) := (others => (others => '0'));
    wr_usedw_32b_arr : in  t_slv_32_arr(g_nof_streams - 1 downto 0) := (others => (others => '0'));
    rd_emp_arr       : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
    wr_full_arr      : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
    -- Control output
    rd_fill_32b_arr  : out t_slv_32_arr(g_nof_streams - 1 downto 0)
  );
end dp_fifo_monitor_arr;

architecture str of dp_fifo_monitor_arr is
  constant c_nof_regs : natural := 5;

  signal reg_mosi_arr : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr : t_mem_miso_arr(g_nof_streams - 1 downto 0);
begin
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => ceil_log2(c_nof_regs)
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  gen_dp_fifo_monitor : for i in 0 to g_nof_streams - 1 generate
    u_dp_fifo_monitor: entity work.dp_fifo_monitor
    port map (
      mm_clk       => mm_clk,
      mm_rst       => mm_rst,

      dp_clk       => dp_clk,
      dp_rst       => dp_rst,

      reg_mosi     => reg_mosi_arr(i),
      reg_miso     => reg_miso_arr(i),

      rd_usedw_32b => rd_usedw_32b_arr(i),
      wr_usedw_32b => wr_usedw_32b_arr(i),
      rd_emp       => rd_emp_arr(i),
      wr_full      => wr_full_arr(i),
      rd_fill_32b  => rd_fill_32b_arr(i)
    );
  end generate;
end str;
