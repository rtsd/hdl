/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

// This also applies to MAX1618. The adavantage of the MAX1618 over the
// MAX1617A is that the MAX1618 has a thermostat mode, which changes the
// function of the ALERT output from a latched interrupt-type output to a
// selfclearing thermostat for fan control.

#ifndef I2C_MAX1617_H
#define I2C_MAX1617_H

/* includes */

/* public constants */
//                      ADD0_ADD1
#define I2C_MAX1617_ADR_LOW_LOW             0x18
#define I2C_MAX1617_ADR_LOW_MID             0x19
#define I2C_MAX1617_ADR_LOW_HIGH            0x1A
#define I2C_MAX1617_ADR_MID_LOW             0x29
#define I2C_MAX1617_ADR_MID_MID             0x2A
#define I2C_MAX1617_ADR_MID_HIGH            0x2B
#define I2C_MAX1617_ADR_HIGH_LOW            0x4C
#define I2C_MAX1617_ADR_HIGH_MID            0x4D
#define I2C_MAX1617_ADR_HIGH_HIGH           0x4E

#define I2C_MAX1617_CMD_READ_LOCAL_TEMP       0
#define I2C_MAX1617_CMD_READ_REMOTE_TEMP      1
#define I2C_MAX1617_CMD_READ_STATUS           2
#define I2C_MAX1617_CMD_READ_CONFIG           3
#define I2C_MAX1617_CMD_READ_RATE             4
#define I2C_MAX1617_CMD_READ_LOCAL_HIGH       5
#define I2C_MAX1617_CMD_READ_LOCAL_LOW        6
#define I2C_MAX1617_CMD_READ_REMOTE_HIGH      7
#define I2C_MAX1617_CMD_READ_REMOTE_LOW       8

#define I2C_MAX1617_CMD_WRITE_CONFIG          9
#define I2C_MAX1617_CMD_WRITE_RATE           10
#define I2C_MAX1617_CMD_WRITE_LOCAL_HIGH     11
#define I2C_MAX1617_CMD_WRITE_LOCAL_LOW      12
#define I2C_MAX1617_CMD_WRITE_REMOTE_HIGH    13
#define I2C_MAX1617_CMD_WRITE_REMOTE_LOW     14

#define I2C_MAX1617_CMD_ONE_SHOT             15

#define I2C_MAX1617_RATE_0_0625               0
#define I2C_MAX1617_RATE_0_125                1
#define I2C_MAX1617_RATE_0_25                 2
#define I2C_MAX1617_RATE_0_5                  3
#define I2C_MAX1617_RATE_1                    4
#define I2C_MAX1617_RATE_2                    5
#define I2C_MAX1617_RATE_4                    6
#define I2C_MAX1617_RATE_8                    7

#define I2C_MAX1617_CONFIG_ID               0x08
#define I2C_MAX1617_CONFIG_THERM            0x10
#define I2C_MAX1617_CONFIG_POL              0x20
#define I2C_MAX1617_CONFIG_RUN_STOP         0x40
#define I2C_MAX1617_CONFIG_MASK             0x80

#define I2C_MAX1617_STATUS_BUSY             0x80
#define I2C_MAX1617_STATUS_RHIGH            0x10
#define I2C_MAX1617_STATUS_RLOW             0x08
#define I2C_MAX1617_STATUS_DIODE            0x04

/* public macros */

/* public types */

/* public variables */

/* public function protos */

#endif

