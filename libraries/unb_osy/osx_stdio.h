#ifndef OSX_STDIO_H
#define OSX_STDIO_H
#include <stdarg.h>
int osx_vsprintf(char* str, const char* fmt, va_list args );
int osx_sprintf(char* str, const char* fmt, ... );
int osx_printf(const char* fmt, ...);
#endif
