-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- AUthor: E. Kooistra, R. vd Walle
-- Purpose: Test the 1GbE interface by sending and counting received packets.
-- Description: Tx part of eth_tester, see detailed design in [1]
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+ETH+tester+unit+for+1GbE
--
-- Remarks:
-- . g_nof_octet_generate can be set to increase the amount of bytes generated
--   per clock cycle by the BG. This is done by duplicating the one byte
--   g_nof_octet_generate times using a generate statement.
-- . g_nof_octet_output determines the output data width, e.g.
--   when set to 4, the output sosi containis 4 * 8 = 32 bits of generated data.
--   This should be the same or a multiple of g_nof_octet_generate such that
--   it can be repacked.

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use work.eth_tester_pkg.all;

entity eth_tester_tx is
  generic (
    g_bg_sync_timeout     : natural  := 220 * 10**6;  -- 10% margin for nominal 1 s with st_clk at 200MHz
    g_nof_octet_generate  : natural  := 1;
    g_nof_octet_output    : natural  := 4;  -- must be multiple of g_nof_octet_generate
    g_use_eth_header      : boolean  := true;
    g_use_ip_udp_header   : boolean  := true;
    g_use_dp_header       : boolean  := true;
    g_hdr_calc_ip_crc     : boolean  := false;
    g_hdr_field_arr       : t_common_field_arr := c_eth_tester_hdr_field_arr;
    g_hdr_field_sel       : std_logic_vector   := c_eth_tester_hdr_field_sel;
    g_hdr_app_len         : natural := c_eth_tester_app_hdr_len
  );
  port (
    -- Clocks and reset
    mm_rst             : in  std_logic;
    mm_clk             : in  std_logic;
    st_rst             : in  std_logic;
    st_clk             : in  std_logic;
    st_pps             : in  std_logic;
    ref_sync           : out std_logic;

    -- UDP transmit interface
    eth_src_mac        : in  std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    ip_src_addr        : in  std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    udp_src_port       : in  std_logic_vector(c_network_udp_port_w - 1 downto 0);

    tx_length          : out natural range 0 to 2**c_halfword_w - 1;  -- 16 bit
    tx_fifo_rd_emp     : out std_logic;

    tx_udp_sosi        : out t_dp_sosi;
    tx_udp_siso        : in  t_dp_siso := c_dp_siso_rdy;

    -- Memory Mapped Slaves (one per stream)
    reg_bg_ctrl_copi               : in  t_mem_copi := c_mem_copi_rst;
    reg_bg_ctrl_cipo               : out t_mem_cipo;
    reg_hdr_dat_copi               : in  t_mem_copi := c_mem_copi_rst;
    reg_hdr_dat_cipo               : out t_mem_cipo;
    reg_bsn_monitor_v2_tx_copi     : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_tx_cipo     : out t_mem_cipo;
    reg_strobe_total_count_tx_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_strobe_total_count_tx_cipo : out t_mem_cipo;
    reg_dp_split_copi              : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_split_cipo              : out t_mem_cipo
  );
end eth_tester_tx;

architecture str of eth_tester_tx is
  constant c_empty_w              : natural := ceil_log2(g_nof_octet_output);

  -- Choose 10% extra margin for FIFO fill level that will result in BG block
  -- level flow control via  bg_siso.xon. The input eop will release blocks
  -- for FIFO output already before the FIFO is fill level is reached.
  -- Choose FIFO size to fit one more packet on top of FIFO fill level.
  constant c_packet_sz_max        : natural := ceil_div(c_eth_tester_bg_block_len_max, g_nof_octet_output);
  constant c_fifo_fill            : natural := c_packet_sz_max * 11 / 10;
  constant c_fifo_size            : natural := true_log_pow2(c_fifo_fill + c_packet_sz_max);
  constant c_fifo_size_w          : natural := ceil_log2(c_fifo_size);

  constant c_nof_total_counts     : natural := 1;  -- one to count Tx packets

  constant c_nof_repack_words     : natural := g_nof_octet_output / g_nof_octet_generate;  -- yields integer as g_nof_octet_output is multiple of g_nof_octet_generate.
  constant c_generate_data_w      : natural := g_nof_octet_generate * c_octet_w;
  constant c_out_data_w           : natural := g_nof_octet_output * c_octet_w;
  constant c_nof_symbols_max      : natural := c_network_eth_payload_jumbo_max;
  constant c_use_split            : boolean := sel_a_b(g_nof_octet_generate > 1, true, false);
  constant c_hdr_calc_ip_crc      : boolean := g_use_ip_udp_header and g_hdr_calc_ip_crc;

  signal ip_total_length          : natural;
  signal udp_total_length         : natural;
  signal app_total_length         : natural;

  signal bg_siso                  : t_dp_siso := c_dp_siso_rdy;
  signal bg_sosi                  : t_dp_sosi;
  signal in_split_sosi            : t_dp_sosi;
  signal in_split_siso            : t_dp_siso := c_dp_siso_rdy;
  signal out_split_sosi_arr       : t_dp_sosi_arr(1 downto 0);
  signal out_split_siso_arr       : t_dp_siso_arr(1 downto 0) := (others => c_dp_siso_rdy);
  signal split_nof_symbols        : natural;
  signal bg_data                  : std_logic_vector(c_octet_w - 1 downto 0);
  signal bg_ctrl_hold             : t_diag_block_gen;
  signal bg_block_len             : natural;
  signal tx_packed_sosi           : t_dp_sosi;
  signal tx_packed_data           : std_logic_vector(c_word_w - 1 downto 0);
  signal tx_fifo_sosi             : t_dp_sosi;
  signal tx_fifo_data             : std_logic_vector(c_word_w - 1 downto 0);
  signal tx_fifo_siso             : t_dp_siso;
  signal tx_fifo_wr_ful           : std_logic;
  signal tx_fifo_wr_usedw         : std_logic_vector(c_fifo_size_w - 1 downto 0);
  signal i_tx_fifo_rd_emp         : std_logic;
  signal tx_offload_siso          : t_dp_siso;
  signal tx_offload_sosi          : t_dp_sosi;
  signal tx_offload_frame_siso    : t_dp_siso;
  signal tx_offload_frame_sosi    : t_dp_sosi;

  signal i_ref_sync               : std_logic := '0';
  signal in_strobe_arr            : std_logic_vector(c_nof_total_counts - 1 downto 0);
  signal i_tx_udp_sosi            : t_dp_sosi;
  signal tx_udp_data              : std_logic_vector(c_word_w - 1 downto 0);

  -- Use hdr_fields_slv_in default 0, to have DP driven fields ip_header_checksum = 0 and udp_checksum = 0
  signal hdr_fields_slv_in        : std_logic_vector(1023 downto 0) := (others => '0');
  signal hdr_fields_slv_tx        : std_logic_vector(1023 downto 0);
  signal hdr_fields_rec_in        : t_eth_tester_header;
  signal hdr_fields_rec_tx        : t_eth_tester_header;
begin
  ref_sync <= i_ref_sync;
  tx_length <= app_total_length;
  tx_fifo_rd_emp <= i_tx_fifo_rd_emp;
  tx_udp_sosi <= i_tx_udp_sosi;

  -- View sosi.data in Wave Window
  bg_data        <= bg_sosi.data(c_octet_w - 1 downto 0);
  tx_packed_data <= tx_packed_sosi.data(c_word_w - 1 downto 0);
  tx_fifo_data   <= tx_fifo_sosi.data(c_word_w - 1 downto 0);
  tx_udp_data    <= i_tx_udp_sosi.data(c_word_w - 1 downto 0);

  -------------------------------------------------------------------------------
  -- Generate packed data blocks
  -------------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map (
    g_nof_streams        => 1,
    g_use_bg_buffer_ram  => false,
    g_buf_addr_w         => c_diag_bg_mem_adrs_w,  -- = 24, use full range 2**24 for BG addr --> data values
    g_buf_dat_w          => c_octet_w
  )
  port map (
    -- System
    mm_rst  => mm_rst,
    mm_clk  => mm_clk,
    dp_rst  => st_rst,
    dp_clk  => st_clk,
    en_sync => st_pps,  -- block generator enable sync pulse in ST clock domain
    -- MM interface
    reg_bg_ctrl_mosi    => reg_bg_ctrl_copi,  -- BG control register (one for all streams)
    reg_bg_ctrl_miso    => reg_bg_ctrl_cipo,
    -- ST interface
    bg_ctrl_hold_arr(0) => bg_ctrl_hold,
    out_sosi_arr(0)     => bg_sosi,
    out_siso_arr(0)     => bg_siso
  );

  -- Duplicate bg_sosi.data g_nof_octets_generate times
  p_duplicate : process(bg_sosi)
  begin
    in_split_sosi <= bg_sosi;
    for I in 0 to g_nof_octet_generate-1 loop
      in_split_sosi.data((I + 1) * c_octet_w - 1 downto I * c_octet_w) <= bg_sosi.data(c_octet_w - 1 downto 0);
    end loop;
  end process;

  -- BG clock level flow control, needed when the dp_repack_data has to insert
  -- empty octets into the last packed word.
  bg_siso.ready <= in_split_siso.ready;

  -- BG block level flow control, needed in case BG settings result in eth bit
  -- rate larger than the rated ethernet output speed, to avoid u_tx_fifo overflow.
  p_bg_siso_xon : process(st_rst, st_clk)
  begin
    if st_rst = '1' then
      bg_siso.xon <= '1';
    elsif rising_edge(st_clk) then
      bg_siso.xon <= '1';
      if TO_UINT(tx_fifo_wr_usedw) > c_fifo_fill then
        bg_siso.xon <= '0';
      end if;
    end if;
  end process;

  -- Use dp_split to make fine packet length adjustment on octet level.
  -- dp_split sets the empty field to indicate the unused bytes in the last word.
  gen_split : if c_use_split generate
    u_mms_dp_split : entity dp_lib.mms_dp_split
    generic map (
      g_data_w          => c_generate_data_w,
      g_symbol_w        => c_octet_w,
      g_nof_symbols_max => c_nof_symbols_max
    )
    port map (
      mm_rst             => mm_rst,
      mm_clk             => mm_clk,
      dp_rst             => st_rst,
      dp_clk             => st_clk,

      reg_mosi           => reg_dp_split_copi,
      reg_miso           => reg_dp_split_cipo,

      snk_in_arr(0)      => in_split_sosi,
      snk_out_arr(0)     => in_split_siso,

      src_in_2arr(0)     => out_split_siso_arr,
      src_out_2arr(0)    => out_split_sosi_arr,

      out_nof_symbols(0) => split_nof_symbols
    );
  end generate;

  gen_no_split : if not c_use_split generate
    out_split_sosi_arr(1) <= in_split_sosi;
    in_split_siso <= out_split_siso_arr(1);
  end generate;

  u_pack : entity dp_lib.dp_repack_data  -- repack generated words to output width.
  generic map (
    g_in_dat_w       => c_generate_data_w,
    g_in_nof_words   => c_nof_repack_words,
    g_in_symbol_w    => c_octet_w,
    g_out_dat_w      => c_out_data_w,
    g_out_nof_words  => 1,
    g_out_symbol_w   => c_octet_w
  )
  port map (
    rst              => st_rst,
    clk              => st_clk,
    snk_out          => out_split_siso_arr(1),  -- connect dp_split head part, tail is discarded.
    snk_in           => out_split_sosi_arr(1),
    src_out          => tx_packed_sosi
  );

  u_tx_fifo : entity dp_lib.dp_fifo_fill_eop
  generic map (
    g_data_w         => c_out_data_w,
    g_bsn_w          => c_diag_bg_bsn_init_w,  -- = 64 bit
    g_empty_w        => c_empty_w,
    g_use_bsn        => true,
    g_use_empty      => true,
    g_use_sync       => true,
    g_fifo_fill      => c_fifo_fill,
    g_fifo_size      => c_fifo_size
  )
  port map (
    wr_rst       => st_rst,
    wr_clk       => st_clk,
    rd_rst       => st_rst,
    rd_clk       => st_clk,
    -- Monitor FIFO filling
    wr_ful       => tx_fifo_wr_ful,
    wr_usedw     => tx_fifo_wr_usedw,
    rd_emp       => i_tx_fifo_rd_emp,
    -- ST sink
    snk_in       => tx_packed_sosi,
    -- ST source
    src_in       => tx_fifo_siso,
    src_out      => tx_fifo_sosi
  );

  -------------------------------------------------------------------------------
  -- Assemble header info
  -------------------------------------------------------------------------------
  -- Whether the dp_offload_tx_hdr_fields value is actually used in the Tx
  -- header depends on:
  --   c_eth_tester_hdr_field_sel = "1"&"101"&"111011111001"&"0100"&"100"
  --                                     eth   ip             udp    app
  --   where 0 = data path, 1 = MM controlled. The '0' fields are assigned here
  --   in hdr_fields_slv_in in order:
  --     access   field
  --     MM       word_align
  --
  --     MM       eth_dst_mac
  --        DP    eth_src_mac
  --     MM       eth_type
  --
  --     MM       ip_version
  --     MM       ip_header_length
  --     MM       ip_services
  --        DP    ip_total_length
  --     MM       ip_identification
  --     MM       ip_flags
  --     MM       ip_fragment_offset
  --     MM       ip_time_to_live
  --     MM       ip_protocol
  --        DP    ip_header_checksum --> not here, will be filled in by 1GbE
  --                                     eth component
  --        DP    ip_src_addr
  --     MM       ip_dst_addr
  --
  --        DP    udp_src_port
  --     MM       udp_dst_port
  --        DP    udp_total_length
  --        DP    udp_checksum --> default fixed 0, so not used, not calculated
  --                               here or in 1GbE eth component because would
  --                               require store and forward
  --        DP    dp_length
  --     MM       dp_reserved
  --        DP    dp_sync
  --        DP    dp_bsn

  -- The bg_block_len is still valid because bg_ctrl_hold holds the BG settings
  -- until it restarts, so no need to pass bg_block_len on via e.g. the channel
  -- field in u_fifo.
  bg_block_len <= split_nof_symbols when c_use_split else TO_UINT(bg_ctrl_hold.samples_per_packet(15 downto 0));  -- packet lenghts fit in 16b
  app_total_length <= g_hdr_app_len + bg_block_len when rising_edge(st_clk);
  udp_total_length <= c_network_udp_header_len + app_total_length when rising_edge(st_clk);
  ip_total_length <= c_network_ip_header_len + udp_total_length when rising_edge(st_clk);

  gen_eth_header : if g_use_eth_header generate
    hdr_fields_slv_in(field_hi(g_hdr_field_arr, "eth_src_mac"     ) downto field_lo(g_hdr_field_arr,  "eth_src_mac"     )) <= eth_src_mac;
  end generate;

  gen_ip_udp_header : if g_use_ip_udp_header generate
    hdr_fields_slv_in(field_hi(g_hdr_field_arr, "ip_total_length" ) downto field_lo(g_hdr_field_arr,  "ip_total_length" )) <= TO_UVEC(ip_total_length, 16);
    hdr_fields_slv_in(field_hi(g_hdr_field_arr, "ip_src_addr"     ) downto field_lo(g_hdr_field_arr,  "ip_src_addr"     )) <= ip_src_addr;
    hdr_fields_slv_in(field_hi(g_hdr_field_arr, "udp_src_port"    ) downto field_lo(g_hdr_field_arr,  "udp_src_port"    )) <= udp_src_port;
    hdr_fields_slv_in(field_hi(g_hdr_field_arr, "udp_total_length") downto field_lo(g_hdr_field_arr,  "udp_total_length")) <= TO_UVEC(udp_total_length, 16);
  end generate;

  gen_dp_header : if g_use_dp_header generate
    hdr_fields_slv_in(field_hi(g_hdr_field_arr, "dp_length"       ) downto field_lo(g_hdr_field_arr,  "dp_length"       )) <= TO_UVEC(app_total_length, 16);
    hdr_fields_slv_in(field_hi(g_hdr_field_arr, "dp_sync"         ) downto field_lo(g_hdr_field_arr,  "dp_sync"         )) <= slv(tx_fifo_sosi.sync);
    hdr_fields_slv_in(field_hi(g_hdr_field_arr, "dp_bsn"          ) downto field_lo(g_hdr_field_arr,  "dp_bsn"          )) <= tx_fifo_sosi.bsn;
  end generate;

  -------------------------------------------------------------------------------
  -- Tx ETH/UDP/IP packets with packed BG data
  -------------------------------------------------------------------------------
  u_dp_offload_tx : entity dp_lib.dp_offload_tx_v3
  generic map (
    g_nof_streams    => 1,
    g_data_w         => c_out_data_w,
    g_symbol_w       => c_octet_w,
    g_hdr_field_arr  => g_hdr_field_arr,
    g_hdr_field_sel  => g_hdr_field_sel,
    g_pipeline_ready => true
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,
    dp_rst                => st_rst,
    dp_clk                => st_clk,

    reg_hdr_dat_mosi      => reg_hdr_dat_copi,
    reg_hdr_dat_miso      => reg_hdr_dat_cipo,

    snk_in_arr(0)         => tx_fifo_sosi,
    snk_out_arr(0)        => tx_fifo_siso,

    src_out_arr(0)        => tx_offload_sosi,
    src_in_arr(0)         => tx_offload_siso,

    hdr_fields_in_arr(0)  => hdr_fields_slv_in,  -- hdr_fields_slv_in_arr(i) is considered valid @ snk_in_arr(i).sop
    hdr_fields_out_arr(0) => hdr_fields_slv_tx
  );

  -- View record in Wave Window
  hdr_fields_rec_in <= func_eth_tester_map_header(hdr_fields_slv_in);
  hdr_fields_rec_tx <= func_eth_tester_map_header(hdr_fields_slv_tx);

  -------------------------------------------------------------------------------
  -- IP header checksum
  -------------------------------------------------------------------------------
  gen_ip_crc : if c_hdr_calc_ip_crc generate
    u_eth_ip_header_checksum : entity work.eth_ip_header_checksum
    generic map (
      g_data_w        => c_out_data_w,
      g_hdr_field_arr => g_hdr_field_arr
    )
    port map (
      rst               => st_rst,
      clk               => st_clk,

      snk_in            => tx_offload_sosi,
      snk_out           => tx_offload_siso,

      src_out           => tx_offload_frame_sosi,
      src_in            => tx_offload_frame_siso,

      hdr_fields_slv_in => hdr_fields_slv_tx
    );
  end generate;

  gen_no_ip_crc : if not c_hdr_calc_ip_crc generate
    tx_offload_frame_sosi <= tx_offload_sosi;
    tx_offload_siso       <= tx_offload_frame_siso;
  end generate;

  -------------------------------------------------------------------------------
  -- dp_pipeline_ready to ease timing closure
  -------------------------------------------------------------------------------
  u_dp_pipeline_ready : entity dp_lib.dp_pipeline_ready
  port map(
    rst     => st_rst,
    clk     => st_clk,

    snk_out => tx_offload_frame_siso,
    snk_in  => tx_offload_frame_sosi,
    src_in  => tx_udp_siso,
    src_out => i_tx_udp_sosi
  );

  -------------------------------------------------------------------------------
  -- Tx packet monitors
  -------------------------------------------------------------------------------
  i_ref_sync <= tx_fifo_sosi.sync when rising_edge(st_clk);

  u_mms_dp_bsn_monitor_v2 : entity dp_lib.mms_dp_bsn_monitor_v2
  generic map (
    g_nof_streams  => 1,
    g_sync_timeout => g_bg_sync_timeout
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    reg_mosi       => reg_bsn_monitor_v2_tx_copi,
    reg_miso       => reg_bsn_monitor_v2_tx_cipo,

    -- Streaming clock domain
    dp_rst         => st_rst,
    dp_clk         => st_clk,
    ref_sync       => i_ref_sync,

    in_siso_arr(0) => tx_fifo_siso,
    in_sosi_arr(0) => tx_fifo_sosi
  );

  in_strobe_arr(0) <= tx_fifo_sosi.sop when rising_edge(st_clk);  -- count total nof Tx packets

  u_dp_strobe_total_count : entity dp_lib.dp_strobe_total_count
  generic map (
    g_nof_counts  => c_nof_total_counts,
    g_count_w     => c_longword_w,
    g_clip        => true
  )
  port map (
    dp_rst        => st_rst,
    dp_clk        => st_clk,

    ref_sync      => i_ref_sync,
    in_strobe_arr => in_strobe_arr,

    mm_rst        => mm_rst,
    mm_clk        => mm_clk,

    reg_mosi      => reg_strobe_total_count_tx_copi,
    reg_miso      => reg_strobe_total_count_tx_cipo
  );
end str;
