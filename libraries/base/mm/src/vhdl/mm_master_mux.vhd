-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Multiplex an array of MM master interfaces to a single MM master
--          interface
-- Description:
--   This mm_master_mux is a simple multiplexer that allows multiple
--   masters to access the same MM port. The mm_master_mux does not
--   provide arbitration between the masters in the array. Therefore the
--   precondition is that the external application takes care that the MM
--   accesses of the multiple masters in the array do not overlap in time.
--
--   Write accesses from multiple masters occur may without gaps. After a read
--   access from one master the read latency must first be accounted for by
--   the application introducing a gap, before a read access by another master
--   can be multiplexed.
--
--   The mm_master_mux operates combinatorially, so it introduces no
--   extra latency. The mm_clk is needed to hold the index of the master that
--   is currently active, to ensure that the read data is passed on to the
--   master that did the rd access.
--
-- Remarks:
-- . This resembles common_mem_demux.vhd, but is not identical. The difference
--   is that common_mem_demux is the inverse of common_mem_demux and therefore
--   assumes that all the mux_mosi spans the entire array whereas for this
--   mm_master_mux the mux_mosi spans one element.
-- . There is no bus arbitrator. This is sufficient for use cases where e.g.
--   one master only does some initialization accesses after reset and the
--   other master is the main master that does all subsequent accesses.
--   Therefore this mm_master_mux is typically suited per MM slave
--   that needs dual master access, rather then to select between two main
--   central MM masters.
-- . There is no pipelining. The advantage is that the mux_miso.waitrequest is
--   supported without extra effort.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mm_master_mux is
  generic (
    g_nof_masters     : positive;  -- Number of MM masters
    g_rd_latency_min  : natural  -- Minimum read latency
  );
  port (
    mm_clk          : in  std_logic;
    master_mosi_arr : in  t_mem_mosi_arr(0 to g_nof_masters - 1) := (others => c_mem_mosi_rst);
    master_miso_arr : out t_mem_miso_arr(0 to g_nof_masters - 1) := (others => c_mem_miso_rst);
    mux_mosi        : out t_mem_mosi;
    mux_miso        : in  t_mem_miso
  );
end mm_master_mux;

architecture rtl of mm_master_mux is
  signal index                : natural := 0;
  signal index_hold           : natural := 0;
begin
  gen_single : if g_nof_masters = 1 generate
    mux_mosi           <= master_mosi_arr(0);
    master_miso_arr(0) <= mux_miso;
  end generate;

  gen_multiple : if g_nof_masters > 1 generate
    -- Detect which master in the array is active
    -- The pre condition is that the input masters will only start an access
    -- when the mux master is free. For a rd access this means that the
    -- read latency of the rdval has passed. Therefor it is not necessary
    -- that this mm_master_mux maintains an index pipeline
    -- from rd until expected rdval. Instead it is sufficient to hold the
    -- index of the active master, until the next master does an access. For
    -- rd access hold the last active index to ensure that rdval will be
    -- directed to the master that orginated the rd access. For wr access
    -- hold last active index instead of reset to '0' to ease observation of
    -- the index value in wave window.
    p_index : process(master_mosi_arr, index_hold)
    begin
      index <= index_hold;  -- default hold index of last active master
      for I in 0 to g_nof_masters - 1 loop
        if master_mosi_arr(I).wr = '1' or master_mosi_arr(I).rd = '1' then
          index <= I;  -- index of active master
          exit;  -- Found active master, no need to loop further. EXIT is not
                 -- realy needed, because there should be only one active
                 -- master, and if there are more active masters, then it
                 -- does not matter whether the first or the last is selected.
        end if;
      end loop;
    end process;

    index_hold <= index when rising_edge(mm_clk);  -- hold index of last active master

    -- Multiplex master access, can be write or read
    mux_mosi <= master_mosi_arr(index);

    -- Multiplex slave read response
    p_miso : process(mux_miso, index, index_hold)
    begin
      master_miso_arr <= (others => mux_miso);  -- default assign to all, to avoid latches
      for I in 0 to g_nof_masters - 1 loop
        master_miso_arr(I).rdval <= '0';
        -- If the minimal read latency is g_rd_latency_min = 0, then the mux
        -- has to use the combinatorial index, else it use the registered
        -- index, to ease achieving timing closure.
        if g_rd_latency_min = 0 then
          if I = index then
            master_miso_arr(I).rdval <= mux_miso.rdval;
          end if;
        else
          if I = index_hold then
            master_miso_arr(I).rdval <= mux_miso.rdval;
          end if;
        end if;
      end loop;
    end process;

  end generate;
end rtl;
