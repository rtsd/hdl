------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, dp_lib, eth_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use dp_lib.dp_stream_pkg.all;
use eth_lib.eth_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;

entity unb1_fn_terminal_db is
  generic (
    -- General
    g_rev_multi_unb : boolean := false;  -- Set to TRUE by Quartus revision fn_terminal_db_rev_multi.
    g_design_name   : string  := "unb1_fn_terminal_db";  -- Set to "fn_terminal_db_rev_multi_unb" by revision fn_terminal_db_rev_multi
    g_design_note   : string  := "UNUSED";
    g_sim           : boolean := false;  -- Overridden by TB
    g_sim_unb_nr    : natural := 0;
    g_sim_node_nr   : natural := 0;
    g_stamp_date    : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time    : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn     : natural := 0  -- SVN revision    -- set by QSF
  );
  port (
   -- GENERAL
    CLK                    : in    std_logic;  -- System Clock
    PPS                    : in    std_logic;  -- System Sync
    WDI                    : out   std_logic;  -- Watchdog Clear
    INTA                   : inout std_logic;  -- FPGA interconnect line
    INTB                   : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION                : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID                     : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO                 : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc                : inout std_logic;
    sens_sd                : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk                : in    std_logic;
    ETH_SGIN               : in    std_logic;
    ETH_SGOUT              : out   std_logic;

    -- Transceiver clocks
    --SA_CLK                 : IN  STD_LOGIC := '0';  -- TR clock    BN-BI (backplane)
    SB_CLK                 : in  std_logic := '0';  -- TR clock FN-BN    (mesh)

    -- Serial I/O
    FN_BN_0_TX             : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_0_RX             : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_1_TX             : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_1_RX             : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_2_TX             : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_2_RX             : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_3_TX             : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_3_RX             : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0')
  );
end unb1_fn_terminal_db;

architecture str of unb1_fn_terminal_db is
  constant c_use_phy                : t_c_unb1_board_use_phy := (1, 0, 1, 0, 0, 0, 0, 1);
  constant c_fw_version             : t_unb1_board_fw_version := (1, 0);  -- firmware version x.y

  constant c_use_mesh               : boolean := c_use_phy.tr_mesh = 1;
  constant c_mesh_mon_select        : natural := 1;  -- > 0 = enable SOSI data buffers monitor via MM
  constant c_mesh_mon_nof_words     : natural := c_unb1_board_peripherals_mm_reg_default.ram_diag_db_buf_size;  -- = 1024
  constant c_mesh_mon_use_sync      : boolean := true;  -- when TRUE use dp_pps to trigger the data buffer capture, else new data capture after read access of last data word

  constant c_reg_diag_db_adr_w      : natural := 5;

  -- System
  signal cs_sim                     : std_logic;
  signal xo_clk                     : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_locked                  : std_logic;
  signal mm_rst                     : std_logic;
  signal cal_clk                    : std_logic;

  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;
  signal dp_pps                     : std_logic;

  signal this_chip_id               : std_logic_vector(c_unb1_board_nof_chip_w - 1 downto 0);  -- [2:0], so range 0-3 for FN and range 4-7 BN

  -- PIOs
  signal pout_debug_wave            : std_logic_vector(c_word_w - 1 downto 0);
  signal pout_wdi                   : std_logic;
  signal pin_pps                    : std_logic_vector(c_word_w - 1 downto 0);
  signal pin_intab                  : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
  signal pout_intab                 : std_logic_vector(c_word_w - 1 downto 0);

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;  -- mms_unb_sens registers
  signal reg_unb_sens_miso          : t_mem_miso;

  -- eth1g
  signal eth1g_tse_clk              : std_logic;
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- tr_mesh
  signal tx_serial_2arr             : t_unb1_board_mesh_sl_2arr;  -- Tx
  signal rx_serial_2arr             : t_unb1_board_mesh_sl_2arr;  -- Rx support for diagnostics

  -- MM tr_nonbonded with diagnostics
  signal reg_tr_nonbonded_mosi      : t_mem_mosi := c_mem_mosi_rst;
  signal reg_tr_nonbonded_miso      : t_mem_miso;

  signal reg_diagnostics_mosi       : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diagnostics_miso       : t_mem_miso;

  -- MM diag_data_buffer_mesh
  signal ram_mesh_diag_data_buf_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal ram_mesh_diag_data_buf_miso : t_mem_miso;

  -- MM diag_data_buffer (main)
  signal ram_diag_data_buf_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal ram_diag_data_buf_miso     : t_mem_miso;
  signal reg_diag_data_buf_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diag_data_buf_miso     : t_mem_miso;

  -- MM bsn_monitor
  signal reg_bsn_monitor_mosi       : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_monitor_miso       : t_mem_miso;
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim           => g_sim,
    g_design_name   => g_design_name,
    g_design_note   => g_design_note,
    g_stamp_date    => g_stamp_date,
    g_stamp_time    => g_stamp_time,
    g_stamp_svn     => g_stamp_svn,
    g_fw_version    => c_fw_version,
    g_mm_clk_freq   => c_unb1_board_mm_clk_freq_50M,
    g_use_phy       => c_use_phy,
    g_aux           => c_unb1_board_aux
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,
    dp_pps                   => OPEN,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,
    pin_pps                  => pin_pps,

    -- MM buses
    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,  -- 125 MHz from xo_clk PLL in SOPC system
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    sens_sc                  => sens_sc,
    sens_sd                  => sens_sd,
    -- . 1GbE Control Interface
    ETH_clk                  => ETH_clk,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_unb1_fn_terminal_db
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr

   )
  port map(
    xo_clk                      => xo_clk,
    xo_rst_n                    => xo_rst_n,
    xo_rst                      => xo_rst,

    mm_rst                      => mm_rst,
    mm_clk                      => mm_clk,
    mm_locked                   => mm_locked,
    cal_clk                     => cal_clk,

    -- PIOs
    pout_wdi                    => pout_wdi,
    pin_pps                     => pin_pps,

    -- Manual WDI override
    reg_wdi_mosi                => reg_wdi_mosi,
    reg_wdi_miso                => reg_wdi_miso,

    -- system_info
    reg_unb_system_info_mosi    => reg_unb_system_info_mosi,
    reg_unb_system_info_miso    => reg_unb_system_info_miso,
    rom_unb_system_info_mosi    => rom_unb_system_info_mosi,
    rom_unb_system_info_miso    => rom_unb_system_info_miso,

    -- UniBoard I2C sensors
    reg_unb_sens_mosi           => reg_unb_sens_mosi,
    reg_unb_sens_miso           => reg_unb_sens_miso,

    -- eth1g
    eth1g_tse_clk               => eth1g_tse_clk,
    eth1g_mm_rst                => eth1g_mm_rst,
    eth1g_tse_mosi              => eth1g_tse_mosi,
    eth1g_tse_miso              => eth1g_tse_miso,
    eth1g_reg_mosi              => eth1g_reg_mosi,
    eth1g_reg_miso              => eth1g_reg_miso,
    eth1g_reg_interrupt         => eth1g_reg_interrupt,
    eth1g_ram_mosi              => eth1g_ram_mosi,
    eth1g_ram_miso              => eth1g_ram_miso,

    -- . tr_nonbonded
    reg_tr_nonbonded_mosi       => reg_tr_nonbonded_mosi,
    reg_tr_nonbonded_miso       => reg_tr_nonbonded_miso,
    reg_diagnostics_mosi        => reg_diagnostics_mosi,
    reg_diagnostics_miso        => reg_diagnostics_miso,

    -- . diag_data_buffer
    ram_diag_data_buf_mosi      => ram_diag_data_buf_mosi,
    ram_diag_data_buf_miso      => ram_diag_data_buf_miso,
    reg_diag_data_buf_mosi      => reg_diag_data_buf_mosi,
    reg_diag_data_buf_miso      => reg_diag_data_buf_miso,

    -- . diag_data_buffer_mesh
    ram_mesh_diag_data_buf_mosi => ram_mesh_diag_data_buf_mosi,
    ram_mesh_diag_data_buf_miso => ram_mesh_diag_data_buf_miso,

    -- . bsn_monitor
    reg_bsn_monitor_mosi        => reg_bsn_monitor_mosi,
    reg_bsn_monitor_miso        => reg_bsn_monitor_miso

  );

  -----------------------------------------------------------------------------
  -- Node functioon: Terminals and data buffer
  -----------------------------------------------------------------------------
  u_node_unb1_fn_terminal_db : entity unb1_board_lib.node_unb1_fn_terminal_db
  generic map(
    g_multi_unb                 => g_rev_multi_unb,
    -- Terminals interface
    g_use_mesh                  => c_use_mesh,
    g_mesh_mon_select           => c_mesh_mon_select,
    g_mesh_mon_nof_words        => c_mesh_mon_nof_words,
    g_mesh_mon_use_sync         => c_mesh_mon_use_sync,
    -- Auxiliary Interface
    g_aux                       => c_unb1_board_aux
  )
  port map(
    -- System
    mm_rst                      => mm_rst,
    mm_clk                      => mm_clk,
    dp_rst                      => dp_rst,
    dp_clk                      => dp_clk,
    dp_pps                      => dp_pps,
    tr_mesh_clk                 => SB_CLK,
    cal_clk                     => cal_clk,

    chip_id                     => this_chip_id,

    -- MM interface
    -- . tr_nonbonded
    reg_tr_nonbonded_mosi       => reg_tr_nonbonded_mosi,
    reg_tr_nonbonded_miso       => reg_tr_nonbonded_miso,
    reg_diagnostics_mosi        => reg_diagnostics_mosi,
    reg_diagnostics_miso        => reg_diagnostics_miso,
    -- . diag_data_buffer
    ram_diag_data_buf_mosi      => ram_diag_data_buf_mosi,
    ram_diag_data_buf_miso      => ram_diag_data_buf_miso,
    reg_diag_data_buf_mosi      => reg_diag_data_buf_mosi,
    reg_diag_data_buf_miso      => reg_diag_data_buf_miso,
    -- . diag_data_buffer_mesh
    ram_mesh_diag_data_buf_mosi => ram_mesh_diag_data_buf_mosi,
    ram_mesh_diag_data_buf_miso => ram_mesh_diag_data_buf_miso,
    -- . bsn_monitor
    reg_bsn_monitor_mosi        => reg_bsn_monitor_mosi,
    reg_bsn_monitor_miso        => reg_bsn_monitor_miso,

    -- Mesh interface
    tx_serial_2arr              => tx_serial_2arr,
    rx_serial_2arr              => rx_serial_2arr
  );

  -----------------------------------------------------------------------------
  -- Mesh I/O
  -----------------------------------------------------------------------------
  no_tr_mesh : if c_use_phy.tr_mesh = 0 generate
    rx_serial_2arr <= (others => (others => '0'));
  end generate;

  gen_tr_mesh : if c_use_phy.tr_mesh /= 0 generate
    u_mesh_io : entity unb1_board_lib.unb1_board_mesh_io
    generic map (
      g_bus_w => c_unb1_board_tr_mesh.bus_w
    )
    port map (
      tx_serial_2arr => tx_serial_2arr,
      rx_serial_2arr => rx_serial_2arr,

      -- Serial I/O
      FN_BN_0_TX     => FN_BN_0_TX,
      FN_BN_0_RX     => FN_BN_0_RX,
      FN_BN_1_TX     => FN_BN_1_TX,
      FN_BN_1_RX     => FN_BN_1_RX,
      FN_BN_2_TX     => FN_BN_2_TX,
      FN_BN_2_RX     => FN_BN_2_RX,
      FN_BN_3_TX     => FN_BN_3_TX,
      FN_BN_3_RX     => FN_BN_3_RX
    );
  end generate;
end;
