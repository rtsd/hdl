-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_pkg.all;

package tech_ddr_pkg is
  -- Gather all DDR parameters in one record
  type t_c_tech_ddr is record
    -- PHY variant within a technology
    name                              : string(1 to 4);  -- = "DDR3" or "DDR4"
    mts                               : natural;  -- = 800   access rate in mega transfers per second
    master                            : boolean;  -- = TRUE  TRUE = uniphy master, FALSE = uniphy slave regarding OCT and terminationcontrol for DDR3
    rank                              : string(1 to 6);  -- = "SINGLE" or "DUAL  "
    -- PHY external FPGA IO
    a_w                               : natural;  -- = 16
    a_row_w                           : natural;  -- = 16     = a_w, row address width, via a_w lines
    a_col_w                           : natural;  -- = 10    <= a_w, col address width, via a_w lines
    ba_w                              : natural;  -- = 3
    dq_w                              : natural;  -- = 64
    dqs_w                             : natural;  -- = 8      = dq_w / nof_dq_per_dqs
    dm_w                              : natural;  -- = 8
    dbi_w                             : natural;  -- = 8
    bg_w                              : natural;  -- = 2
    ck_w                              : natural;  -- = 2
    cke_w                             : natural;  -- = 2
    cs_w                              : natural;  -- = 2      = number of chip select lines
    cs_w_w                            : natural;  -- = 1      = true_log2(cs_w), use when the number of chip select lines is converted to a logical address
    odt_w                             : natural;  -- = 2
    -- PHY internal FPGA IO
    terminationcontrol_w              : natural;  -- = 14     internal bus in FPGA
    -- Controller
    rsl                               : natural;  -- = 4      = 2 (use both PHY clock edges) * 2 (PHY transfer at double rate), resolution
    rsl_w                             : natural;  -- = 2      = ceil_log2(rsl)
    command_queue_depth               : natural;  -- = 8
    maxburstsize                      : natural;  -- = 64
    maxburstsize_w                    : natural;  -- = 7      = ceil_log2(maxburstsize+1)
    mem_dq_w                          : natural;  -- = 64 dq connected to the memory module, can be = dq_w or 64 whenn dq_w = 72
  end record;

  function func_tech_sel_ddr(g_technology : natural; g_ddr3, g_ddr4 : t_c_tech_ddr) return t_c_tech_ddr;  -- Select DDR3 or DDR4 dependent on the technology
  function func_tech_sel_ddr(g_sel        : boolean;      a,      b : t_c_tech_ddr) return t_c_tech_ddr;  -- Select DDR dependent on the boolean

  function func_tech_ddr_dq_address_w(  c_ddr : t_c_tech_ddr) return natural;  -- return DDR address width for the DQ data at the PHY mts rate
  function func_tech_ddr_ctlr_address_w(c_ddr : t_c_tech_ddr) return natural;  -- return DDR address width for the controller data at the by rsl=4 reduced rate
  function func_tech_ddr_ctlr_data_w(   c_ddr : t_c_tech_ddr) return natural;  -- return DDR    data width for the controller data at the by rsl=4 reduced rate
  function func_tech_ddr_ctlr_ip_data_w(c_ddr : t_c_tech_ddr) return natural;  -- return DDR    data width for the controller data at the by rsl=4 reduced rate

  -- return DDR module size in log2(number of bytes), because 2**nofbytes_w may not fit in 31 bit NATURAL
  function func_tech_ddr_module_nofbytes_w(c_ddr : t_c_tech_ddr) return natural;
  -- return DDR module size in GiBytes when >= 1 GByte which is typical on HW, else
  -- return size as negative value to indicate 2**value fraction of 1GByte which is typical in simulation
  function func_tech_ddr_module_gigabytes(c_ddr : t_c_tech_ddr) return integer;

  function func_tech_ddr_sim_size(c_ddr : t_c_tech_ddr; sim_ctrl_addr_w : natural) return t_c_tech_ddr;  -- derive sim_ddr from c_ddr (or alternatively use predefined c_tech_ddr*_sim)
  function func_tech_ddr_rewire_64b_to_72b(c_ddr : t_c_tech_ddr; vec_64b : std_logic_vector) return std_logic_vector;
  function func_tech_ddr_rewire_72b_to_64b(c_ddr : t_c_tech_ddr; vec_72b : std_logic_vector) return std_logic_vector;

  --                                                                                                    a   a                               cs cs
  --                                                                 name    mts   master rank      a   row col ba dq  dqs dm dbi bg ck cke w  w_w  odt term rsl rsl_w cqd burst burst_w mem_dq_w
  constant c_tech_ddr3_max                        : t_c_tech_ddr := ("none",  800,  true, "DUAL  ", 16, 16, 11, 3, 64, 8,  8, 0,  0, 2, 2,  2, 1,   2,  14,  4,  2,    4,  64,   7,      64);  -- maximum ranges for record field definitions

  -- use predefined c_tech_ddr3_sim or derive it using func_tech_ddr_sim_size()
  constant c_tech_ddr3_sim_8k                     : t_c_tech_ddr := ("DDR3",  800,  true, "DUAL  ", 10,  1, 10, 3, 64, 8,  8, 0,  0, 2, 2,  2, 1,   2,  14,  4,  2,    4,  64,   7,      64);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col - rsl_w)
  constant c_tech_ddr3_sim_16k                    : t_c_tech_ddr := ("DDR3",  800,  true, "DUAL  ", 10,  2, 10, 3, 64, 8,  8, 0,  0, 2, 2,  2, 1,   2,  14,  4,  2,    4,  64,   7,      64);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col - rsl_w)
  constant c_tech_ddr3_sim_128k                   : t_c_tech_ddr := ("DDR3",  800,  true, "DUAL  ", 10,  5, 10, 3, 64, 8,  8, 0,  0, 2, 2,  2, 1,   2,  14,  4,  2,    4,  64,   7,      64);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col - rsl_w)
  constant c_tech_ddr3_sim_1m                     : t_c_tech_ddr := ("DDR3",  800,  true, "DUAL  ", 10,  8, 10, 3, 64, 8,  8, 0,  0, 2, 2,  2, 1,   2,  14,  4,  2,    4,  64,   7,      64);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col - rsl_w)

  constant c_tech_ddr3_16g_dual_rank_800m         : t_c_tech_ddr := ("DDR3",  800,  true, "DUAL  ", 16, 16, 11, 3, 64, 8,  8, 0,  0, 2, 2,  2, 1,   2,  14,  4,  2,    4,  64,   7,      64);
  constant c_tech_ddr3_4g_800m_master             : t_c_tech_ddr := ("DDR3",  800,  true, "DUAL  ", 15, 15, 10, 3, 64, 8,  8, 0,  0, 2, 2,  2, 1,   2,  14,  4,  2,    4,  64,   7,      64);
  constant c_tech_ddr3_4g_800m_slave              : t_c_tech_ddr := ("DDR3",  800, false, "DUAL  ", 15, 15, 10, 3, 64, 8,  8, 0,  0, 2, 2,  2, 1,   2,  14,  4,  2,    4,  64,   7,      64);
  constant c_tech_ddr3_4g_single_rank_800m_master : t_c_tech_ddr := ("DDR3",  800,  true, "SINGLE", 16, 16, 10, 3, 64, 8,  8, 0,  0, 2, 1,  1, 0,   1,  14,  4,  2,    4,  64,   7,      64);
  constant c_tech_ddr3_4g_single_rank_800m_slave  : t_c_tech_ddr := ("DDR3",  800, false, "SINGLE", 16, 16, 10, 3, 64, 8,  8, 0,  0, 2, 1,  1, 0,   1,  14,  4,  2,    4,  64,   7,      64);

--CONSTANT c_tech_ddr4_max                        : t_c_tech_ddr := ("none", 1600,  TRUE, "DUAL  ", 17, 15, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 0,   1,   0,  8,  3,    8,  64,   7,      72);  -- maximum ranges for record field definitions
  constant c_tech_ddr4_max                        : t_c_tech_ddr := ("none", 1600,  true, "DUAL  ", 17, 15, 10, 2, 72, 9,  0, 9,  2, 2, 2,  2, 1,   2,   0,  8,  3,    8,  64,   7,      72);  -- maximum ranges for record field definitions

  -- use predefined c_tech_ddr4_sim or derive it using func_tech_ddr_sim_size()
  constant c_tech_ddr4_sim_4k                     : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 10,  1, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 0,   1,   0,  8,  3,    8,  64,   7,      72);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col + bg_w - rsl_w)
  constant c_tech_ddr4_sim_8k                     : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 10,  2, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 0,   1,   0,  8,  3,    8,  64,   7,      72);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col + bg_w - rsl_w)
  constant c_tech_ddr4_sim_16k                    : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 10,  3, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 0,   1,   0,  8,  3,    8,  64,   7,      72);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col + bg_w - rsl_w)
  constant c_tech_ddr4_sim_128k                   : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 10,  6, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 0,   1,   0,  8,  3,    8,  64,   7,      72);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col + bg_w - rsl_w)
  constant c_tech_ddr4_sim_1m                     : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 10,  9, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 0,   1,   0,  8,  3,    8,  64,   7,      72);  -- use a_row to set nof ctrl addr = 2**(cs_w + ba + a_row + a_col + bg_w - rsl_w)

  constant c_tech_ddr4_4g_1600m                   : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 17, 15, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 0,   1,   0,  8,  3,    8,  64,   7,      72);
  constant c_tech_ddr4_8g_1600m                   : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 17, 15, 10, 2, 72, 9,  0, 9,  2, 2, 2,  2, 1,   2,   0,  8,  3,    8,  64,   7,      72);
  constant c_tech_ddr4_8g_1600m_64                : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 17, 15, 10, 2, 64, 8,  0, 8,  2, 2, 2,  2, 1,   2,   0,  8,  3,    8,  64,   7,      64);
  constant c_tech_ddr4_16g_1600m_72_64            : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 17, 16, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 1,   1,   0,  8,  3,    8,  64,   7,      64);  -- 72b connection from IP, 64b to memory module (used for unb2c slot II with 64 bit modules)
  constant c_tech_ddr4_16g_1600m_64               : t_c_tech_ddr := ("DDR4", 1600,  true, "DUAL  ", 17, 16, 10, 2, 64, 8,  0, 8,  2, 1, 1,  1, 1,   1,   0,  8,  3,    8,  64,   7,      64);
  constant c_tech_ddr4_4g_2000m                   : t_c_tech_ddr := ("DDR4", 2000,  true, "DUAL  ", 17, 15, 10, 2, 72, 9,  0, 9,  2, 1, 1,  1, 0,   1,   0,  8,  3,    8,  64,   7,      72);
  constant c_tech_ddr4_8g_2400m                   : t_c_tech_ddr := ("DDR4", 2400,  true, "DUAL  ", 17, 15, 10, 2, 72, 9,  0, 9,  2, 2, 2,  2, 1,   2,   0,  8,  3,    8,  64,   7,      72);

  -- PHY in, inout and out signal records
  type t_tech_ddr3_phy_in is record  -- DDR3 Description
    evt                        : std_logic;  -- event signal is Not Connected to DDR3 PHY
    oct_rup                    : std_logic;  -- only master DDR3 PHY has On Chip Termination OCT inputs
    oct_rdn                    : std_logic;  -- only master DDR3 PHY has On Chip Termination OCT inputs
  end record;

  type t_tech_ddr4_phy_in is record  -- DDR4 Description
    --evt                      : STD_LOGIC;                                                         -- event signal
    alert_n                    : std_logic;  -- alert signal
    oct_rzqin                  : std_logic;  -- PHY has On Chip Termination OCT inputs
  end record;

  type t_tech_ddr3_phy_io is record  -- DDR3 Description
    dq               : std_logic_vector(c_tech_ddr3_max.dq_w - 1 downto 0);  -- data bus
    dqs              : std_logic_vector(c_tech_ddr3_max.dqs_w - 1 downto 0);  -- data strobe bus
    dqs_n            : std_logic_vector(c_tech_ddr3_max.dqs_w - 1 downto 0);  -- data strobe bus negative
    scl              : std_logic;  -- I2C clock
    sda              : std_logic;  -- I2C data
  end record;

  type t_tech_ddr4_phy_io is record  -- DDR4 Description
    dq               : std_logic_vector(c_tech_ddr4_max.dq_w - 1 downto 0);  -- data bus
    dqs              : std_logic_vector(c_tech_ddr4_max.dqs_w - 1 downto 0);  -- data strobe bus
    dqs_n            : std_logic_vector(c_tech_ddr4_max.dqs_w - 1 downto 0);  -- data strobe bus negative
    dbi_n            : std_logic_vector(c_tech_ddr4_max.dbi_w - 1 downto 0);  -- data bus inversion
  end record;

  type t_tech_ddr3_phy_ou is record  -- DDR3 Description
    a                          : std_logic_vector(c_tech_ddr3_max.a_w - 1 downto 0);  -- row and column address
    ba                         : std_logic_vector(c_tech_ddr3_max.ba_w - 1 downto 0);  -- bank address
    dm                         : std_logic_vector(c_tech_ddr3_max.dm_w - 1 downto 0);  -- data mask bus
    ras_n                      : std_logic;  -- row address strobe
    cas_n                      : std_logic;  -- column address strobe
    we_n                       : std_logic;  -- write enable signal
    reset_n                    : std_logic;  -- reset signal
    ck                         : std_logic_vector(c_tech_ddr3_max.ck_w - 1 downto 0);  -- clock, positive edge clock
    ck_n                       : std_logic_vector(c_tech_ddr3_max.ck_w - 1 downto 0);  -- clock, negative edge clock
    cke                        : std_logic_vector(c_tech_ddr3_max.cke_w - 1 downto 0);  -- clock enable
    cs_n                       : std_logic_vector(c_tech_ddr3_max.cs_w - 1 downto 0);  -- chip select
    odt                        : std_logic_vector(c_tech_ddr3_max.odt_w - 1 downto 0);  -- on-die termination control signal
  end record;

  type t_tech_ddr4_phy_ou is record  -- DDR4  Description
    a                          : std_logic_vector(c_tech_ddr4_max.a_w - 1 downto 0);  -- row and column address (note eg. a[16]=ras_n, a[15]=cas_n, a[14]=we_n)
    ba                         : std_logic_vector(c_tech_ddr4_max.ba_w - 1 downto 0);  -- bank address
    bg                         : std_logic_vector(c_tech_ddr4_max.bg_w - 1 downto 0);  -- bank group
    act_n                      : std_logic;  -- activate signal
    par                        : std_logic;  -- parity signal
    reset_n                    : std_logic;  -- reset signal
    ck                         : std_logic_vector(c_tech_ddr4_max.ck_w - 1 downto 0);  -- clock, positive edge clock
    ck_n                       : std_logic_vector(c_tech_ddr4_max.ck_w - 1 downto 0);  -- clock, negative edge clock
    cke                        : std_logic_vector(c_tech_ddr4_max.cke_w - 1 downto 0);  -- clock enable
    cs_n                       : std_logic_vector(c_tech_ddr4_max.cs_w - 1 downto 0);  -- chip select
    odt                        : std_logic_vector(c_tech_ddr4_max.odt_w - 1 downto 0);  -- on-die termination control signal
  end record;

  type t_tech_ddr3_phy_in_arr is array(natural range <>) of t_tech_ddr3_phy_in;
  type t_tech_ddr3_phy_io_arr is array(natural range <>) of t_tech_ddr3_phy_io;
  type t_tech_ddr3_phy_ou_arr is array(natural range <>) of t_tech_ddr3_phy_ou;

  type t_tech_ddr4_phy_in_arr is array(natural range <>) of t_tech_ddr4_phy_in;
  type t_tech_ddr4_phy_io_arr is array(natural range <>) of t_tech_ddr4_phy_io;
  type t_tech_ddr4_phy_ou_arr is array(natural range <>) of t_tech_ddr4_phy_ou;

  type t_tech_ddr3_phy_terminationcontrol is record  -- DDR3 Termination control
    seriesterminationcontrol   : std_logic_vector(c_tech_ddr3_max.terminationcontrol_w - 1 downto 0);  -- termination control from master to slave DDR3 PHY (internal signal in FPGA)
    parallelterminationcontrol : std_logic_vector(c_tech_ddr3_max.terminationcontrol_w - 1 downto 0);  -- termination control from master to slave DDR3 PHY (internal signal in FPGA)
    pll_mem_clk                : std_logic;
    pll_write_clk              : std_logic;
    pll_write_clk_pre_phy_clk  : std_logic;
    pll_addr_cmd_clk           : std_logic;
    pll_locked                 : std_logic;
    pll_avl_clk                : std_logic;
    pll_config_clk             : std_logic;
    dll_delayctrl              : std_logic_vector(5 downto 0);
    afi_clk                    : std_logic;
    afi_half_clk               : std_logic;
    afi_reset_n                : std_logic;
  end record;

  constant c_tech_ddr3_phy_terminationcontrol_x   : t_tech_ddr3_phy_terminationcontrol := ((others => 'X'), (others => 'X'), 'X', 'X', 'X', 'X', 'X', 'X', 'X', (others => 'X'), 'X', 'X', 'X');
  constant c_tech_ddr3_phy_terminationcontrol_rst : t_tech_ddr3_phy_terminationcontrol := ((others => '0'), (others => '0'), '0', '0', '0', '0', '0', '0', '0', (others => '0'), '0', '0', '0');

  constant c_tech_ddr3_phy_in_x     : t_tech_ddr3_phy_in := ('X', 'X', 'X');
  constant c_tech_ddr4_phy_in_x     : t_tech_ddr4_phy_in := ('X', 'X');
  constant c_tech_ddr3_phy_ou_x     : t_tech_ddr3_phy_ou := ((others => 'X'), (others => 'X'), (others => 'X'), 'X', 'X', 'X', 'X', (others => 'X'), (others => 'X'), (others => 'X'), (others => 'X'), (others => 'X'));
  constant c_tech_ddr4_phy_ou_x     : t_tech_ddr4_phy_ou := ((others => 'X'), (others => 'X'), (others => 'X'), 'X', 'X', 'X', (others => 'X'), (others => 'X'), (others => 'X'), (others => 'X'), (others => 'X'));
end tech_ddr_pkg;

package body tech_ddr_pkg is
  function func_tech_sel_ddr(g_technology : natural; g_ddr3, g_ddr4 : t_c_tech_ddr) return t_c_tech_ddr is
  begin
    case g_technology is
      when c_tech_stratixiv        => return g_ddr3;  -- unb1
      when c_tech_arria10_proto    => return g_ddr4;  -- unb2
      when c_tech_arria10_e3sge3   => return g_ddr4;  -- unb2
      when c_tech_arria10_e1sg     => return g_ddr4;  -- unb2b
      when c_tech_arria10_e2sg     => return g_ddr4;  -- unb2c
      when others                  => return g_ddr3;
    end case;

  end;

  function func_tech_sel_ddr(g_sel : boolean; a, b : t_c_tech_ddr) return t_c_tech_ddr is
  begin
    if g_sel = true then return a; else return b; end if;
  end;

  function func_tech_ddr_dq_address_w(c_ddr : t_c_tech_ddr) return natural is
  begin
    if c_ddr.name = "DDR3" then return c_ddr.cs_w_w + c_ddr.ba_w + c_ddr.a_row_w + c_ddr.a_col_w;              end if;  -- PHY address
    if c_ddr.name = "DDR4" then return c_ddr.cs_w_w + c_ddr.ba_w + c_ddr.a_row_w + c_ddr.a_col_w + c_ddr.bg_w; end if;  -- PHY address
  end;

  function func_tech_ddr_ctlr_address_w(c_ddr : t_c_tech_ddr) return natural is
    constant c_dq_address_w : natural := func_tech_ddr_dq_address_w(c_ddr);
  begin
    return c_dq_address_w - c_ddr.rsl_w;  -- CTLR address
  end;

  function func_tech_ddr_ctlr_data_w(c_ddr : t_c_tech_ddr) return natural is
  begin
    return c_ddr.mem_dq_w * c_ddr.rsl;  -- CTLR data
  end;

  function func_tech_ddr_ctlr_ip_data_w(c_ddr : t_c_tech_ddr) return natural is
  begin
    return c_ddr.dq_w * c_ddr.rsl;  -- CTLR data
  end;

  function func_tech_ddr_sim_size(c_ddr : t_c_tech_ddr; sim_ctrl_addr_w : natural) return t_c_tech_ddr is
    variable v_ddr         : t_c_tech_ddr := c_ddr;
    variable v_ctrl_addr_w : natural;
  begin
    -- adapt a_row to set number of ctrl addr, use a_row_w = 1 to derive minimal size
    v_ddr.a_row_w := 1;
    v_ctrl_addr_w := func_tech_ddr_ctlr_address_w(v_ddr);
    if sim_ctrl_addr_w > v_ctrl_addr_w then
      v_ddr.a_row_w := 1 + sim_ctrl_addr_w - v_ctrl_addr_w;
    end if;
    return v_ddr;
  end;

  function func_tech_ddr_module_nofbytes_w(c_ddr : t_c_tech_ddr) return natural is
    constant c_dq_address_w       : natural := func_tech_ddr_dq_address_w(c_ddr);
    constant c_dq_nof_bytes       : natural := 8;  -- both dw_q = 64 and 72 are regarded as having 8 bytes (either with 8 or 9 bits per byte)
    constant c_dq_nof_bytes_w     : natural := ceil_log2(c_dq_nof_bytes);
    constant c_module_nof_bytes_w : natural := c_dq_address_w + c_dq_nof_bytes_w;
  begin
    return c_module_nof_bytes_w;
  end;

  function func_tech_ddr_module_gigabytes(c_ddr : t_c_tech_ddr) return integer is
    constant c_module_nof_bytes_w : natural := func_tech_ddr_module_nofbytes_w(c_ddr);
    constant c_1GB_w              : natural := 30;
  begin
    if c_module_nof_bytes_w < c_1GB_w then
      -- Return <= -1 to indicate fraction of 1GByte, so e.g. -1 implies 2**-1 = 0.5 GByte
      return c_module_nof_bytes_w - c_1GB_w;
    else
      -- Return >= 1 to indicate multiple of 1GByte = number of GByte
      return 2**(c_module_nof_bytes_w - c_1GB_w);
    end if;
  end;

  function func_tech_ddr_rewire_64b_to_72b(c_ddr : t_c_tech_ddr; vec_64b : std_logic_vector) return std_logic_vector is
    variable vec_72b : std_logic_vector(func_tech_ddr_ctlr_ip_data_w(c_ddr) - 1 downto 0) := (others => '0');
  begin
    for w in 0 to c_ddr.rsl - 1 loop
      vec_72b( w * c_ddr.dq_w + c_ddr.mem_dq_w - 1 downto w * c_ddr.dq_w) := vec_64b((w + 1) * c_ddr.mem_dq_w - 1 downto w * c_ddr.mem_dq_w);
    end loop;
    return vec_72b;
  end;

  function func_tech_ddr_rewire_72b_to_64b(c_ddr : t_c_tech_ddr; vec_72b : std_logic_vector) return std_logic_vector is
    variable vec_64b : std_logic_vector(func_tech_ddr_ctlr_data_w(c_ddr) - 1 downto 0) := (others => '0');
  begin
    for w in 0 to c_ddr.rsl - 1 loop
      vec_64b((w + 1) * c_ddr.mem_dq_w - 1 downto w * c_ddr.mem_dq_w) := vec_72b( w * c_ddr.dq_w + c_ddr.mem_dq_w - 1 downto w * c_ddr.dq_w);
    end loop;
    return vec_64b;
  end;

end tech_ddr_pkg;
