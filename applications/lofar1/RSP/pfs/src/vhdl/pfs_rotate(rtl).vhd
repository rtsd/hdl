library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

architecture rtl of pfs_rotate is
  signal i_out_dat_x     : std_logic_vector(out_dat_x'range);
  signal nxt_out_dat_x   : std_logic_vector(out_dat_x'range);
  signal i_out_dat_y     : std_logic_vector(out_dat_y'range);
  signal nxt_out_dat_y   : std_logic_vector(out_dat_y'range);
  signal nxt_out_val     : std_logic_vector(out_val'range);
  signal nxt_out_sync    : std_logic;
  signal cnt             : integer range 0 to out_val'length - 1;
  signal nxt_cnt         : integer;
begin
  out_dat_x <= i_out_dat_x;
  out_dat_y <= i_out_dat_y;

  registers_proc : process (clk, rst)
  begin
    if rst = '1' then
      i_out_dat_x    <= (others => '0');
      i_out_dat_y    <= (others => '0');
      out_val        <= (others => '0');
      out_sync       <= '0';
      cnt            <= 0;
    elsif rising_edge(clk) then
      i_out_dat_x    <= nxt_out_dat_x;
      i_out_dat_y    <= nxt_out_dat_y;
      out_val        <= nxt_out_val;
      out_sync       <= nxt_out_sync;
      cnt            <= nxt_cnt;
    end if;
  end process;

  counter_proc : process (cnt, in_val)
  begin
    nxt_cnt <= cnt;
    if cnt = g_nof_fir - 1 and in_val = '1' then
      nxt_cnt <= 0;
    elsif in_val = '1' then
      nxt_cnt <= cnt + 1;
    end if;
  end process;

  interleaver_proc : process (in_val, cnt, in_dat_x, in_dat_y, in_sync,
      i_out_dat_x, i_out_dat_y)
  begin
    nxt_out_dat_x <= i_out_dat_x;
    nxt_out_dat_y <= i_out_dat_y;
    nxt_out_val <= (others => '0');
    nxt_out_sync <= in_sync;

    if in_val = '1' then
      nxt_out_val(cnt) <= '1';
      nxt_out_dat_x <= in_dat_x;
      nxt_out_dat_y <= in_dat_y;
    end if;
  end process;
end rtl;
