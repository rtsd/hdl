-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--     Jonathan Hargreaves, 05 Dec 2016
--
-- Purpose
--     Accumulators to integrate the I, Q, U and V Stokes products
-- Description
--     I is always positive, hence unsigned
--     The number of succsessive values to integrate is coded in the width parameter
--     g_accumcount_w = ceil_log2(g_nof_int) where g_nof_int is 8 or 16
-- Remarks
--     The outputs including the pipelined valid are placed in the data field of a dp_sosi record

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity iquv_accum is
  generic (
    g_accumcount_w  : natural := 3;  -- log2 of the number of samples to integrate
    g_in_data_w     : natural := 12  -- width of the incoming data
  );
  port (
    dp_rst          : in  std_logic;
    dp_clk          : in  std_logic;
    i_in            : in  std_logic_vector(g_in_data_w - 1 downto 0);
    q_in            : in  std_logic_vector(g_in_data_w - 1 downto 0);
    u_in            : in  std_logic_vector(g_in_data_w - 1 downto 0);
    v_in            : in  std_logic_vector(g_in_data_w - 1 downto 0);
    in_valid        : in  std_logic;
    i_out_sosi      : out t_dp_sosi;
    q_out_sosi      : out t_dp_sosi;
    u_out_sosi      : out t_dp_sosi;
    v_out_sosi      : out t_dp_sosi

  );
end iquv_accum;

architecture str of iquv_accum is
  constant c_accum_count_zero     : std_logic_vector(g_accumcount_w - 1 downto 0) := (others => '0');

  signal accum_ctrl_count         : std_logic_vector(g_accumcount_w - 1 downto 0);
  signal accum_ctrl               : std_logic;
  signal enable_output_valid      : std_logic := '0';
  signal nxt_enable_output_valid  : std_logic := '0';
begin
  ------------------------------------------------------------------------------
  -- control counter to generate an end of integration period strobe
  ------------------------------------------------------------------------------
  u_accum_ctrl_count : entity common_lib.common_counter
  generic map (
    g_latency   => 1,
    g_init      => 0,
    g_width     => g_accumcount_w,
    g_step_size => 1
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,
    cnt_en      => in_valid,
    count       => accum_ctrl_count
  );

  accum_ctrl <= '1' when accum_ctrl_count = c_accum_count_zero and in_valid = '1' else '0';

  ------------------------------------------------------------------------------
  -- Accumulators to integrate the Stokes products
  ------------------------------------------------------------------------------
  u_accum_i : entity common_lib.common_accumulate
  generic map (
    g_representation   => "UNSIGNED"
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,
    clken     => '1',
    sload     => accum_ctrl,
    in_val    => in_valid,
    in_dat    => i_in,
    out_dat   => i_out_sosi.data
  );

  u_accum_q : entity common_lib.common_accumulate
  generic map (
    g_representation   => "SIGNED"
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,
    clken     => '1',
    sload     => accum_ctrl,
    in_val    => in_valid,
    in_dat    => q_in,
    out_dat   => q_out_sosi.data
  );

  u_accum_u : entity common_lib.common_accumulate
  generic map (
    g_representation   => "SIGNED"
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,
    clken     => '1',
    sload     => accum_ctrl,
    in_val    => in_valid,
    in_dat    => u_in,
    out_dat   => u_out_sosi.data
  );

  u_accum_v : entity common_lib.common_accumulate
  generic map (
    g_representation   => "SIGNED"
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,
    clken     => '1',
    sload     => accum_ctrl,
    in_val    => in_valid,
    in_dat    => v_in,
    out_dat   => v_out_sosi.data
  );

  p_clk : process (dp_clk, dp_rst)
  begin
    if dp_rst = '1' then
      enable_output_valid <= '0';
    else
      if rising_edge(dp_clk) then
        enable_output_valid <= nxt_enable_output_valid;
      end if;
    end if;
  end process;

  nxt_enable_output_valid <= '1' when (accum_ctrl = '1' and in_valid = '1') else enable_output_valid;

  i_out_sosi.valid <= accum_ctrl and in_valid and enable_output_valid;
  q_out_sosi.valid <= accum_ctrl and in_valid and enable_output_valid;
  u_out_sosi.valid <= accum_ctrl and in_valid and enable_output_valid;
  v_out_sosi.valid <= accum_ctrl and in_valid and enable_output_valid;
end str;
