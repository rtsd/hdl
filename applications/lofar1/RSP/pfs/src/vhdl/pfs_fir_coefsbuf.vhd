library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pfs_fir_coefsbuf is
  generic (
    g_data_w    : natural;
    g_nof_coefs : natural;
    g_coefs_w   : natural;
    g_init_file : string
  );
  port (
    addr        : in std_logic_vector(g_coefs_w - 1 downto 0);
    rden        : in std_logic;
    data        : out std_logic_vector(g_data_w - 1 downto 0);
    clk         : in std_logic;
    rst         : in std_logic
  );
end pfs_fir_coefsbuf;
