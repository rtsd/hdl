-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle

-- Purpose: Handle TX side of ring design.
-- Description: See https://support.astron.nl/confluence/x/jyu7Ag
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.ring_pkg.all;

entity ring_tx is
  generic (
    g_lane_direction   : natural := 1;
    g_use_dp_layer     : boolean := true;
    g_data_w           : natural := 64;
    g_symbol_w         : natural := 8;
    g_ring_pkt_type    : std_logic_vector(c_halfword_w - 1 downto 0) := c_ring_pkt_type_bf;
    g_validate_channel : boolean := true;
    g_mode             : string  := ">";
    g_nof_tx_monitors  : natural := 1;
    g_sync_timeout     : natural := 220 * 10**6  -- 10% margin
  );
  port (
    -- Clocks and reset
    mm_rst                  : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk                  : in  std_logic;  -- memory-mapped bus clock

    dp_clk                  : in  std_logic;
    dp_rst                  : in  std_logic;

    to_lane_sosi            : in  t_dp_sosi;
    lane_tx_cable_sosi      : out t_dp_sosi;
    lane_tx_board_sosi      : out t_dp_sosi;

    reg_bsn_monitor_v2_copi : in  t_mem_copi;
    reg_bsn_monitor_v2_cipo : out t_mem_cipo;

    ref_sync                : in  std_logic;
    tx_select               : in  std_logic;
    remove_channel          : in  std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
    this_rn                 : in  std_logic_vector(c_byte_w - 1 downto 0);
    N_rn                    : in  std_logic_vector(c_byte_w - 1 downto 0)
  );
end ring_tx;

architecture str of ring_tx is
  constant c_use_empty      : boolean := sel_a_b(g_symbol_w = g_data_w, false, true);
  constant c_empty_w        : natural := ceil_log2(g_data_w / g_symbol_w);
  constant c_nof_hdr_fields : natural := sel_a_b(g_use_dp_layer, c_ring_dp_nof_hdr_fields, c_ring_eth_nof_hdr_fields);
  constant c_hdr_field_sel  : std_logic_vector(c_nof_hdr_fields - 1 downto 0)   := sel_a_b(g_use_dp_layer, c_ring_dp_hdr_field_sel, c_ring_eth_hdr_field_sel);
  constant c_hdr_field_arr  : t_common_field_arr(c_nof_hdr_fields - 1 downto 0) := sel_a_b(g_use_dp_layer, c_ring_dp_hdr_field_arr, c_ring_eth_hdr_field_arr);
  constant c_fifo_size      : natural := 64;  -- Large enough to fit ETH/DP header + slack (choose power of 2).

  signal validated_sosi     : t_dp_sosi;
  signal tx_sosi            : t_dp_sosi;
  signal tx_fifo_sosi       : t_dp_sosi;
  signal tx_fifo_siso       : t_dp_siso;
  signal lane_tx_sosi       : t_dp_sosi;
  signal monitor_sosi       : t_dp_sosi;
  signal piped_monitor_sosi : t_dp_sosi;
  signal demux_sosi_arr     : t_dp_sosi_arr(0 to g_nof_tx_monitors - 1);  -- using 0 TO ... as that is the output of the demux
  signal monitor_sosi_arr   : t_dp_sosi_arr(g_nof_tx_monitors - 1 downto 0);

  signal hdr_fields_in     : std_logic_vector(1023 downto 0);
  signal hdr_fields_in_reg : std_logic_vector(1023 downto 0);
begin
  -- Validate transport_nof_hops
  gen_validate : if g_validate_channel generate
    u_dp_block_validate_channel: entity dp_lib.dp_block_validate_channel
    generic map (
      g_mode           => g_mode
    )
    port map (
      dp_rst => dp_rst,
      dp_clk => dp_clk,

      in_sosi       => to_lane_sosi,
      out_keep_sosi => validated_sosi,

      remove_channel => remove_channel
    );
  end generate;

  -- Don't validate transport_nof_hops
  gen_no_validate : if not g_validate_channel generate
    validated_sosi <= to_lane_sosi;
  end generate;

  -- increase channel by 1, e.g. add 1 hop.
  p_hop: process(validated_sosi, N_rn)
  begin
    tx_sosi <= validated_sosi;
    if TO_UINT(validated_sosi.channel) >= TO_UINT(N_rn) - 1 then
      tx_sosi.channel <= (others => '0');
    else
      tx_sosi.channel <= INCR_DP_CHANNEL(validated_sosi.channel, 1, c_dp_stream_channel_w);
    end if;
  end process;

  -- Encode packet header
  hdr_fields_in(field_hi(c_hdr_field_arr, "eth_dst_mac"  ) downto field_lo(c_hdr_field_arr, "eth_dst_mac" )) <= c_ring_eth_dst_mac;
  hdr_fields_in(field_hi(c_hdr_field_arr, "eth_src_mac"  ) downto field_lo(c_hdr_field_arr, "eth_src_mac" )) <= c_ring_eth_src_mac;
  hdr_fields_in(field_hi(c_hdr_field_arr, "eth_type"     ) downto field_lo(c_hdr_field_arr, "eth_type"    )) <= g_ring_pkt_type;

  gen_hdr_dp : if g_use_dp_layer generate
    hdr_fields_in(field_hi(c_hdr_field_arr, "dp_channel" ) downto field_lo(c_hdr_field_arr, "dp_channel"  )) <= tx_sosi.channel(c_halfword_w - 1 downto 0);
    hdr_fields_in(field_hi(c_hdr_field_arr, "dp_sync"    ) downto field_lo(c_hdr_field_arr, "dp_sync"     )) <= slv(tx_sosi.sync);
    hdr_fields_in(field_hi(c_hdr_field_arr, "dp_bsn"     ) downto field_lo(c_hdr_field_arr, "dp_bsn"      )) <= tx_sosi.bsn(62 downto 0);
  end generate;

  -- Lead DP info around the FIFO via hdr_fields_in_reg, to avoid having to pass on the DP info via the FIFO, to save on block RAM.
  hdr_fields_in_reg <= hdr_fields_in when rising_edge(dp_clk) and tx_sosi.sop = '1' else hdr_fields_in_reg;

  -- Fifo for inserting header
  u_dp_fifo_sc: entity dp_lib.dp_fifo_sc
  generic map (
    g_data_w    => g_data_w,
    g_empty_w   => c_empty_w,
    g_use_empty => c_use_empty,
    g_fifo_size => c_fifo_size
  )
  port map (
    rst => dp_rst,
    clk => dp_clk,

    snk_in => tx_sosi,

    src_out => tx_fifo_sosi,
    src_in  => tx_fifo_siso
  );

  -- Inserting ETH/DP header
  u_dp_offload_tx_v3: entity dp_lib.dp_offload_tx_v3
  generic map (
    g_nof_streams   => 1,
    g_data_w        => g_data_w,
    g_symbol_w      => g_symbol_w,
    g_hdr_field_arr => c_hdr_field_arr,
    g_hdr_field_sel => c_hdr_field_sel,
    g_pipeline_ready        => false,  -- unpack MM header
    g_pipeline_ready_inp    => true,  -- use true to ease timning closure for ring_tx
    g_capture_hdr_fields_in => false,  -- can use false when g_pipeline_ready_inp = true, because hdr_fields_in_reg captures hdr_fields_in
    g_pipeline_ready_outp   => false  -- use false, because there is no src_in_arr flow control
  )
  port map (
    mm_rst => mm_rst,
    mm_clk => mm_clk,

    dp_rst => dp_rst,
    dp_clk => dp_clk,

    snk_in_arr(0)  => tx_fifo_sosi,
    snk_out_arr(0) => tx_fifo_siso,
    src_out_arr(0) => lane_tx_sosi,

    hdr_fields_in_arr(0)  => hdr_fields_in_reg
  );

  -- Select output based on tx_select
  p_sel_out : process(lane_tx_sosi, tx_select)
  begin
    if tx_select = '1' then  -- transmit via cable
      lane_tx_cable_sosi <= lane_tx_sosi;
      lane_tx_board_sosi <= c_dp_sosi_rst;
    else  -- transmit via  board
      lane_tx_cable_sosi <= c_dp_sosi_rst;
      lane_tx_board_sosi <= lane_tx_sosi;
    end if;
  end process;

  -- BSN Monitors
  gen_bsn_monitors : if g_use_dp_layer generate
    p_hop_to_src_rn: process(dp_rst, dp_clk)
    begin
      if dp_rst = '1' then
        monitor_sosi       <= c_dp_sosi_rst;
        piped_monitor_sosi <= c_dp_sosi_rst;
      elsif rising_edge(dp_clk) then
        -- Pipe monitor sosi to ease timing.
        piped_monitor_sosi <= monitor_sosi;

        -- Convert nof_hops to source RN
        monitor_sosi <= validated_sosi;
        monitor_sosi.channel <= func_ring_nof_hops_to_source_rn(validated_sosi.channel, this_rn, N_rn, g_lane_direction);
      end if;
    end process;

    u_dp_demux : entity dp_lib.dp_demux
    generic map (
      g_nof_output => g_nof_tx_monitors
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,
      snk_in      => piped_monitor_sosi,
      src_out_arr => demux_sosi_arr
    );
    monitor_sosi_arr <= func_dp_stream_arr_reverse_range(demux_sosi_arr);  -- Fix reversed bus.

    u_mms_dp_bsn_monitor_v2 : entity dp_lib.mms_dp_bsn_monitor_v2
    generic map (
      g_nof_streams => g_nof_tx_monitors,
      g_sync_timeout => g_sync_timeout
    )
    port map (
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,
      reg_mosi    => reg_bsn_monitor_v2_copi,
      reg_miso    => reg_bsn_monitor_v2_cipo,

      dp_rst      => dp_rst,
      dp_clk      => dp_clk,
      in_sosi_arr => monitor_sosi_arr,
      ref_sync    => ref_sync
    );
  end generate;
end str;
