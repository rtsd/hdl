-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

-- Purpose: Transpose the g_nof_data symbols in g_nof_data in_data to
--                        g_nof_data symbols in g_nof_data out_data,
--          with support for interleaved address calculation via in_offset.
-- Description:
-- . Each in_data has g_nof_data symbols. After every g_nof_data in_data the
--   symbols in the g_nof_data by g_nof_data matrix transposed.
-- . In parallel for each out_data the in_addr is incremented by 0, 1, ...,
--   or (g_nof_data-1) * in_offset. The in_offset can be set fixed via
--   g_addr_offset or dynamically via in_offset.
-- Remarks:

entity common_transpose is
  generic (
    g_pipeline_shiftreg  : natural := 0;
    g_pipeline_transpose : natural := 0;
    g_pipeline_hold      : natural := 0;
    g_pipeline_select    : natural := 1;
    g_nof_data           : natural := 4;
    g_data_w             : natural := 16;  -- must be multiple of g_nof_data
    g_addr_w             : natural := 9;
    g_addr_offset        : natural := 0  -- default use fixed offset, in_offset * g_nof_data must fit in g_addr_w address range
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;

    in_offset  : in  std_logic_vector(g_addr_w - 1 downto 0) := TO_UVEC(g_addr_offset, g_addr_w);
    in_addr    : in  std_logic_vector(g_addr_w - 1 downto 0) := (others => '0');
    in_data    : in  std_logic_vector(g_data_w - 1 downto 0);
    in_val     : in  std_logic;
    in_eop     : in  std_logic;

    out_addr   : out std_logic_vector(g_addr_w - 1 downto 0);
    out_data   : out std_logic_vector(g_data_w - 1 downto 0);
    out_val    : out std_logic;
    out_eop    : out std_logic
  );
end common_transpose;

architecture str of common_transpose is
  constant c_sel_w          : natural := ceil_log2(g_nof_data);

  constant c_nof_data_max   : natural := 8;
  constant c_holdline_arr   : t_natural_arr(c_nof_data_max - 1 downto 0) := (2, 2, 2, 2, 2, 2, 2, 1);

  constant c_pipeline_sel   : natural := g_pipeline_transpose + g_pipeline_hold;

  signal offset_addr_vec  : std_logic_vector(g_nof_data * g_addr_w - 1 downto 0);

  signal sreg_addr_vec    : std_logic_vector(g_nof_data * g_addr_w - 1 downto 0);
  signal sreg_data_vec    : std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
  signal sreg_val         : std_logic;
  signal sreg_eop         : std_logic;
  signal sreg_sel         : std_logic_vector(c_sel_w - 1 downto 0);
  signal sreg_full        : std_logic;

  signal add_addr_vec     : std_logic_vector(g_nof_data * g_addr_w - 1 downto 0);
  signal trans_data_vec   : std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
  signal trans_full       : std_logic;

  signal hold_addr_vec    : std_logic_vector(g_nof_data * g_addr_w - 1 downto 0);
  signal hold_data_vec    : std_logic_vector(g_nof_data * g_data_w - 1 downto 0);
  signal hold_val         : std_logic;
  signal hold_eop         : std_logic;
  signal hold_sel         : std_logic_vector(c_sel_w - 1 downto 0);
begin
  u_sreg_data : entity common_lib.common_shiftreg
  generic map (
    g_pipeline  => g_pipeline_shiftreg,
    g_flush_en  => true,
    g_nof_dat   => g_nof_data,
    g_dat_w     => g_data_w
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_dat       => in_data,
    in_val       => in_val,
    in_eop       => in_eop,

    out_data_vec => sreg_data_vec,
    out_cnt      => sreg_sel,

    out_val      => sreg_val,
    out_eop      => sreg_eop
  );

  u_sreg_addr : entity common_lib.common_shiftreg
  generic map (
    g_pipeline  => g_pipeline_shiftreg,
    g_flush_en  => true,
    g_nof_dat   => g_nof_data,
    g_dat_w     => g_addr_w
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_dat       => in_addr,
    in_val       => in_val,
    in_eop       => in_eop,

    out_data_vec => sreg_addr_vec
  );

  sreg_full <= '1' when sreg_val = '1' and TO_UINT(sreg_sel) = 0 else '0';

  u_transpose_data : entity common_lib.common_transpose_symbol
  generic map (
    g_pipeline  => g_pipeline_transpose,
    g_nof_data  => g_nof_data,
    g_data_w    => g_data_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => sreg_data_vec,
    out_data   => trans_data_vec
  );

  gen_offsets : for I in g_nof_data - 1 downto 0 generate
    offset_addr_vec((I + 1) * g_addr_w - 1 downto I * g_addr_w) <= TO_UVEC(I * TO_UINT(in_offset), g_addr_w);
  end generate;

  u_add_addr : entity common_lib.common_add_symbol
  generic map (
    g_pipeline    => g_pipeline_transpose,
    g_nof_symbols => g_nof_data,
    g_symbol_w    => g_addr_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_a       => offset_addr_vec,
    in_b       => sreg_addr_vec,

    out_data   => add_addr_vec
  );

  u_trans_full : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline_transpose
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => sreg_full,
    out_dat => trans_full
  );

  u_hold_data : entity common_lib.common_shiftreg_symbol
  generic map (
    g_shiftline_arr => c_holdline_arr(g_nof_data - 1 downto 0),
    g_pipeline      => g_pipeline_hold,
    g_flush_en      => false,
    g_nof_symbols   => g_nof_data,
    g_symbol_w      => g_data_w
  )
  port map (
    rst         => rst,
    clk         => clk,

    in_data     => trans_data_vec,
    in_val      => trans_full,

    out_data    => hold_data_vec
  );

  u_hold_addr : entity common_lib.common_shiftreg_symbol
  generic map (
    g_shiftline_arr => c_holdline_arr(g_nof_data - 1 downto 0),
    g_pipeline      => g_pipeline_hold,
    g_flush_en      => false,
    g_nof_symbols   => g_nof_data,
    g_symbol_w      => g_addr_w
  )
  port map (
    rst         => rst,
    clk         => clk,

    in_data     => add_addr_vec,
    in_val      => trans_full,

    out_data    => hold_addr_vec
  );

  u_hold_sel : entity common_lib.common_pipeline
  generic map (
    g_pipeline  => c_pipeline_sel,
    g_in_dat_w  => c_sel_w,
    g_out_dat_w => c_sel_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => sreg_sel,
    out_dat => hold_sel
  );

  u_hold_val : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => c_pipeline_sel
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => sreg_val,
    out_dat => hold_val
  );

  u_hold_eop : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => c_pipeline_sel
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => sreg_eop,
    out_dat => hold_eop
  );

  u_output_data : entity common_lib.common_select_symbol
  generic map (
    g_pipeline_in  => 0,
    g_pipeline_out => g_pipeline_select,
    g_nof_symbols  => g_nof_data,
    g_symbol_w     => g_data_w,
    g_sel_w        => c_sel_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => hold_data_vec,
    in_val     => hold_val,
    in_eop     => hold_eop,

    in_sel     => hold_sel,

    out_symbol => out_data,
    out_val    => out_val,
    out_eop    => out_eop
  );

  u_output_addr : entity common_lib.common_select_symbol
  generic map (
    g_pipeline_in  => 0,
    g_pipeline_out => g_pipeline_select,
    g_nof_symbols  => g_nof_data,
    g_symbol_w     => g_addr_w,
    g_sel_w        => c_sel_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => hold_addr_vec,
    in_val     => hold_val,
    in_eop     => hold_eop,

    in_sel     => hold_sel,

    out_symbol => out_addr
  );
end str;
