-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use work.tech_tse_component_pkg.all;
use work.tech_tse_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_arria10_e3sge3_tse_sgmii_lvds_altera_eth_tse_151;
library ip_arria10_e3sge3_tse_sgmii_gx_altera_eth_tse_151;

entity tech_tse_arria10_e3sge3 is
  generic (
    g_ETH_PHY      : string  := "LVDS"  -- "LVDS" (default): uses LVDS IOs for ctrl_unb2_board, "XCVR": uses tranceiver PHY
  );
  port (
    -- Clocks and reset
    mm_rst         : in  std_logic;
    mm_clk         : in  std_logic;
    eth_clk        : in  std_logic;
    tx_snk_clk     : in  std_logic;
    rx_src_clk     : in  std_logic;

    -- Memory Mapped Slave
    mm_sla_in      : in  t_mem_mosi;
    mm_sla_out     : out t_mem_miso;

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      : in  t_dp_sosi;
    tx_snk_out     : out t_dp_siso;
    -- . MAC specific
    tx_mac_in      : in  t_tech_tse_tx_mac;
    tx_mac_out     : out t_tech_tse_tx_mac;

    -- MAC receive interface
    -- . ST Source
    rx_src_in      : in  t_dp_siso;
    rx_src_out     : out t_dp_sosi;
    -- . MAC specific
    rx_mac_out     : out t_tech_tse_rx_mac;

    -- PHY interface
    eth_txp        : out std_logic;
    eth_rxp        : in  std_logic;

    tse_led        : out t_tech_tse_led
  );
end tech_tse_arria10_e3sge3;

architecture str of tech_tse_arria10_e3sge3 is
  signal ff_tx_mod        : std_logic_vector(c_tech_tse_empty_w - 1 downto 0);

  signal ff_rx_out        : t_dp_sosi := c_dp_sosi_rst;
begin
  -- Default frame level flow control
  tx_snk_out.xon <= '1';

  -- Force empty = 0 when eop = '0' to avoid TSE MAC bug of missing two bytes when empty = 2 (observed with v9.1)
  ff_tx_mod <= tx_snk_in.empty(c_tech_tse_empty_w - 1 downto 0) when tx_snk_in.eop = '1' else (others => '0');

  -- Force unused bits and fields in rx_src_out to c_dp_sosi_rst to avoid confusing 'X' in wave window
  rx_src_out <= ff_rx_out;

  u_LVDS_tse: if g_ETH_PHY = "LVDS" generate
    u_tse : ip_arria10_e3sge3_tse_sgmii_lvds
      -- The tse_sgmii_lvds needs to be regenerated if its parameters are changed.
      -- . ENABLE_SHIFT16  = 1   : Align packet headers to 32 bit, useful for Nios data handling
      -- . ENABLE_SUP_ADDR = 0   : An extra MAC addresses can e.g. be used as service MAC for tests
      -- . ENA_HASH        = 0   : A multi cast hash table can be used to address all nodes at once
      -- . STAT_CNT_ENA    = 0   : PHY statistics counts are useful for monitoring, but not realy needed
      -- . EG_FIFO         = 256 : Tx FIFO depth in nof 32 bit words (256 --> 1 M9K)
      -- . ING_FIFO        = 256 : Rx FIFO depth in nof 32 bit words (256 --> 1 M9K)
      -- . ENABLE_SGMII    = 0   : PHY access 1000BASE-X
    port map (
      -- MAC transmit interface
      -- . Avalon ST
      ff_tx_clk      => tx_snk_clk,
      ff_tx_rdy      => tx_snk_out.ready,
      ff_tx_data     => tx_snk_in.data(c_tech_tse_data_w - 1 downto 0),
      ff_tx_wren     => tx_snk_in.valid,
      ff_tx_sop      => tx_snk_in.sop,
      ff_tx_eop      => tx_snk_in.eop,
      ff_tx_mod      => ff_tx_mod,
      ff_tx_err      => tx_snk_in.err(0),
      -- . MAC specific
      ff_tx_crc_fwd  => tx_mac_in.crc_fwd,  -- when '0' MAC inserts CRC32 after eop
      ff_tx_septy    => tx_mac_out.septy,  -- when '0' then tx FIFO goes above section-empty threshold
      ff_tx_a_full   => tx_mac_out.a_full,  -- when '1' then tx FIFO goes above almost-full threshold
      ff_tx_a_empty  => tx_mac_out.a_empty,  -- when '1' then tx FIFO goes below almost-empty threshold
      tx_ff_uflow    => tx_mac_out.uflow,  -- when '1' then tx FIFO underflow
      -- MAC receive interface
      -- . Avalon ST
      ff_rx_clk      => rx_src_clk,
      ff_rx_rdy      => rx_src_in.ready,
      ff_rx_data     => ff_rx_out.data(c_tech_tse_data_w - 1 downto 0),
      ff_rx_dval     => ff_rx_out.valid,
      ff_rx_sop      => ff_rx_out.sop,
      ff_rx_eop      => ff_rx_out.eop,
      ff_rx_mod      => ff_rx_out.empty(c_tech_tse_empty_w - 1 downto 0),
      rx_err         => ff_rx_out.err(c_tech_tse_error_w - 1 downto 0),  -- [5] collision error (can only occur in half duplex mode)
                                                                      -- [4] PHY error on GMII
                                                                      -- [3] receive frame truncated due to FIFO overflow
                                                                      -- [2] CRC-32 error
                                                                      -- [1] invalid length
                                                                      -- [0] = OR of [1:5]
      -- . MAC specific
      rx_err_stat    => rx_mac_out.ethertype,  -- [17,16] VLAN info, [15:0] Ethernet lentgh/type field
      rx_frm_type    => rx_mac_out.frm_type,  -- [3]=VLAN, [2]=Broadcast, [1]=Multicast, [0]=Unicast
      ff_rx_dsav     => rx_mac_out.dsav,  -- rx frame available, but not necessarily a complete frame
      ff_rx_a_full   => rx_mac_out.a_full,  -- when '1' then rx FIFO goes above almost-full threshold
      ff_rx_a_empty  => rx_mac_out.a_empty,  -- when '1' then rx FIFO goes below almost-empty threshold
      -- Reset
      reset          => mm_rst,  -- asynchronous reset (choose synchronous to mm_clk)
      -- MM control interface
      clk            => mm_clk,
      reg_addr       => mm_sla_in.address(c_tech_tse_byte_addr_w - 1 downto 2),
      reg_data_out   => mm_sla_out.rddata(c_tech_tse_data_w - 1 downto 0),
      reg_rd         => mm_sla_in.rd,
      reg_data_in    => mm_sla_in.wrdata(c_tech_tse_data_w - 1 downto 0),
      reg_wr         => mm_sla_in.wr,
      reg_busy       => mm_sla_out.waitrequest,
      -- Status LEDs
      led_an         => tse_led.an,  -- '1' = autonegation completed
      led_link       => tse_led.link,  -- '1' = successful link synchronisation
      led_disp_err   => tse_led.disp_err,  -- TBI character error
      led_char_err   => tse_led.char_err,  -- TBI disparity error
      -- crs and col are only available with the SGMII bridge
      led_crs        => tse_led.crs,  -- carrier sense '1' when there is tx/rx activity on the line
      led_col        => tse_led.col,  -- tx collision detected (always '0' for full duplex)
      -- Serial 1.25 Gbps
      rx_recovclkout => OPEN,
      ref_clk        => eth_clk,
      txp            => eth_txp,
      rxp            => eth_rxp
    );
  end generate;

  u_XCVR_tse: if g_ETH_PHY = "XCVR" generate
    u_tse : ip_arria10_e3sge3_tse_sgmii_gx
      -- The tse_sgmii_xcvr needs to be regenerated if its parameters are changed.
      -- . ENABLE_SHIFT16  = 1   : Align packet headers to 32 bit, useful for Nios data handling
      -- . ENABLE_SUP_ADDR = 0   : An extra MAC addresses can e.g. be used as service MAC for tests
      -- . ENA_HASH        = 0   : A multi cast hash table can be used to address all nodes at once
      -- . STAT_CNT_ENA    = 0   : PHY statistics counts are useful for monitoring, but not realy needed
      -- . EG_FIFO         = 2048 : Tx FIFO depth in nof 32 bit words (2048 --> 4 M9K)
      -- . ING_FIFO        = 2048 : Rx FIFO depth in nof 32 bit words (2048 --> 4 M9K)
      -- . ENABLE_SGMII    = 0   : PHY access 1000BASE-X
    port map (
      -- MAC transmit interface
      -- . Avalon ST
      ff_tx_clk      => tx_snk_clk,
      ff_tx_rdy      => tx_snk_out.ready,
      ff_tx_data     => tx_snk_in.data(c_tech_tse_data_w - 1 downto 0),
      ff_tx_wren     => tx_snk_in.valid,
      ff_tx_sop      => tx_snk_in.sop,
      ff_tx_eop      => tx_snk_in.eop,
      ff_tx_mod      => ff_tx_mod,
      ff_tx_err      => tx_snk_in.err(0),
      -- . MAC specific
      ff_tx_crc_fwd  => tx_mac_in.crc_fwd,  -- when '0' MAC inserts CRC32 after eop
      ff_tx_septy    => tx_mac_out.septy,  -- when '0' then tx FIFO goes above section-empty threshold
      ff_tx_a_full   => tx_mac_out.a_full,  -- when '1' then tx FIFO goes above almost-full threshold
      ff_tx_a_empty  => tx_mac_out.a_empty,  -- when '1' then tx FIFO goes below almost-empty threshold
      tx_ff_uflow    => tx_mac_out.uflow,  -- when '1' then tx FIFO underflow
      -- MAC receive interface
      -- . Avalon ST
      ff_rx_clk      => rx_src_clk,
      ff_rx_rdy      => rx_src_in.ready,
      ff_rx_data     => ff_rx_out.data(c_tech_tse_data_w - 1 downto 0),
      ff_rx_dval     => ff_rx_out.valid,
      ff_rx_sop      => ff_rx_out.sop,
      ff_rx_eop      => ff_rx_out.eop,
      ff_rx_mod      => ff_rx_out.empty(c_tech_tse_empty_w - 1 downto 0),
      rx_err         => ff_rx_out.err(c_tech_tse_error_w - 1 downto 0),  -- [5] collision error (can only occur in half duplex mode)
                                                                      -- [4] PHY error on GMII
                                                                      -- [3] receive frame truncated due to FIFO overflow
                                                                      -- [2] CRC-32 error
                                                                      -- [1] invalid length
                                                                      -- [0] = OR of [1:5]
      -- . MAC specific
      rx_err_stat    => rx_mac_out.ethertype,  -- [17,16] VLAN info, [15:0] Ethernet lentgh/type field
      rx_frm_type    => rx_mac_out.frm_type,  -- [3]=VLAN, [2]=Broadcast, [1]=Multicast, [0]=Unicast
      ff_rx_dsav     => rx_mac_out.dsav,  -- rx frame available, but not necessarily a complete frame
      ff_rx_a_full   => rx_mac_out.a_full,  -- when '1' then rx FIFO goes above almost-full threshold
      ff_rx_a_empty  => rx_mac_out.a_empty,  -- when '1' then rx FIFO goes below almost-empty threshold
      -- Reset
      reset          => mm_rst,  -- asynchronous reset (choose synchronous to mm_clk)
      -- MM control interface
      clk            => mm_clk,
      reg_addr       => mm_sla_in.address(c_tech_tse_byte_addr_w - 1 downto 2),
      reg_data_out   => mm_sla_out.rddata(c_tech_tse_data_w - 1 downto 0),
      reg_rd         => mm_sla_in.rd,
      reg_data_in    => mm_sla_in.wrdata(c_tech_tse_data_w - 1 downto 0),
      reg_wr         => mm_sla_in.wr,
      reg_busy       => mm_sla_out.waitrequest,
      -- Status LEDs
      led_an         => tse_led.an,  -- '1' = autonegation completed
      led_link       => tse_led.link,  -- '1' = successful link synchronisation
      led_disp_err   => tse_led.disp_err,  -- TBI character error
      led_char_err   => tse_led.char_err,  -- TBI disparity error
      -- crs and col are only available with the SGMII bridge
      led_crs        => tse_led.crs,  -- carrier sense '1' when there is tx/rx activity on the line
      led_col        => tse_led.col,  -- tx collision detected (always '0' for full duplex)
      -- Serial 1.25 Gbps
      rx_recovclkout => OPEN,
      ref_clk        => eth_clk,
      txp            => eth_txp,
      rxp            => eth_rxp,

      -- GX connections ????
      tx_serial_clk      => (others => '0'),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --                 tx_serial_clk.clk
      rx_cdr_refclk      => '0',  -- : in  std_logic                     := '0';             --                 rx_cdr_refclk.clk
      tx_analogreset     => (others => '0'),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --                tx_analogreset.tx_analogreset
      tx_digitalreset    => (others => '0'),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --               tx_digitalreset.tx_digitalreset
      rx_analogreset     => (others => '0'),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --                rx_analogreset.rx_analogreset
      rx_digitalreset    => (others => '0'),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --               rx_digitalreset.rx_digitalreset
      tx_cal_busy        => OPEN,  -- : out std_logic_vector(0 downto 0);                     --                   tx_cal_busy.tx_cal_busy
      rx_cal_busy        => OPEN,  -- : out std_logic_vector(0 downto 0);                     --                   rx_cal_busy.rx_cal_busy
      rx_set_locktodata  => (others => '0'),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --             rx_set_locktodata.rx_set_locktodata
      rx_set_locktoref   => (others => '0'),  -- : in  std_logic_vector(0 downto 0)  := (others => '0'); --              rx_set_locktoref.rx_set_locktoref
      rx_is_lockedtoref  => OPEN,  -- : out std_logic_vector(0 downto 0);                     --             rx_is_lockedtoref.rx_is_lockedtoref
      rx_is_lockedtodata => open  -- : out std_logic_vector(0 downto 0)                      --            rx_is_lockedtodata.rx_is_lockedtodata
    );
  end generate;
end str;
