-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dev_pca9555 is
  generic(
    g_address   : std_logic_vector(6 downto 0)  -- PCA9555 slave address is "0100" & A2 & A1 & A0
  );
  port(
    scl         : in    std_logic;
    sda         : inout std_logic;
    iobank0     : inout std_logic_vector(7 downto 0);
    iobank1     : inout std_logic_vector(7 downto 0)
  );
end dev_pca9555;

architecture beh of dev_pca9555 is
  constant c_cmd_input_0   : natural := 0;
  constant c_cmd_input_1   : natural := 1;
  constant c_cmd_output_0  : natural := 2;
  constant c_cmd_output_1  : natural := 3;
  constant c_cmd_invert_0  : natural := 4;
  constant c_cmd_invert_1  : natural := 5;
  constant c_cmd_config_0  : natural := 6;
  constant c_cmd_config_1  : natural := 7;

  signal enable       : std_logic;  -- new access, may be write command or write data
  signal stop         : std_logic;  -- end of access
  signal wr_dat       : std_logic_vector(7 downto 0);  -- I2C write data
  signal wr_val       : std_logic;
  signal rd_dat       : std_logic_vector(7 downto 0);  -- I2C read data
  signal rd_req       : std_logic;

  signal cmd_en       : std_logic := '0';
  signal wr_cmd       : natural;  -- device write command
  signal rd_cmd       : natural;  -- device read command

  signal input_reg0   : std_logic_vector(7 downto 0);  -- device registers with powerup default value
  signal input_reg1   : std_logic_vector(7 downto 0);
  signal output_reg0  : std_logic_vector(7 downto 0) := (others => '1');
  signal output_reg1  : std_logic_vector(7 downto 0) := (others => '1');
  signal invert_reg0  : std_logic_vector(7 downto 0) := (others => '0');
  signal invert_reg1  : std_logic_vector(7 downto 0) := (others => '0');
  signal config_reg0  : std_logic_vector(7 downto 0) := (others => '1');
  signal config_reg1  : std_logic_vector(7 downto 0) := (others => '1');
begin
  i2c_slv_device : entity work.i2c_slv_device
  generic map (
    g_address => g_address
  )
  port map (
    scl     => scl,
    sda     => sda,
    en      => enable,
    p       => stop,
    wr_dat  => wr_dat,
    wr_val  => wr_val,
    rd_req  => rd_req,
    rd_dat  => rd_dat
  );

  -- First write byte is treated as command
  -- After sending data to one register, the next data byte will be sent to the other register in the pair
  -- There is no limitation on the number of data bytes sent in one write transmission
  -- Similar for reading data from a register.
  p_write : process (enable, wr_val)
  begin
    if rising_edge(enable) then
      cmd_en <= '1';
    elsif falling_edge(enable) then
      cmd_en <= '0';
    end if;
    if rising_edge(wr_val) then
      cmd_en <= '0';
      if cmd_en = '1' then
        wr_cmd <= to_integer(unsigned(wr_dat));
      else
        case wr_cmd is
          when c_cmd_input_0  => null;                   wr_cmd <= c_cmd_input_1;
          when c_cmd_input_1  => null;                   wr_cmd <= c_cmd_input_0;
          when c_cmd_output_0 => output_reg0 <= wr_dat;  wr_cmd <= c_cmd_output_1;
          when c_cmd_output_1 => output_reg1 <= wr_dat;  wr_cmd <= c_cmd_output_0;
          when c_cmd_invert_0 => invert_reg0 <= wr_dat;  wr_cmd <= c_cmd_invert_1;
          when c_cmd_invert_1 => invert_reg1 <= wr_dat;  wr_cmd <= c_cmd_invert_0;
          when c_cmd_config_0 => config_reg0 <= wr_dat;  wr_cmd <= c_cmd_config_1;
          when c_cmd_config_1 => config_reg1 <= wr_dat;  wr_cmd <= c_cmd_config_0;
          when others         => null;
        end case;
      end if;
    end if;
  end process;

  p_read : process (wr_val, rd_req)
  begin
    if rising_edge(wr_val) then
      if cmd_en = '1' then
        rd_cmd <= to_integer(unsigned(wr_dat));
      end if;
    end if;
    if rising_edge(rd_req) then
      case rd_cmd is
        when c_cmd_input_0  => rd_dat <= input_reg0;   rd_cmd <= c_cmd_input_1;
        when c_cmd_input_1  => rd_dat <= input_reg1;   rd_cmd <= c_cmd_input_0;
        when c_cmd_output_0 => rd_dat <= output_reg0;  rd_cmd <= c_cmd_output_1;
        when c_cmd_output_1 => rd_dat <= output_reg1;  rd_cmd <= c_cmd_output_0;
        when c_cmd_invert_0 => rd_dat <= invert_reg0;  rd_cmd <= c_cmd_invert_1;
        when c_cmd_invert_1 => rd_dat <= invert_reg1;  rd_cmd <= c_cmd_invert_0;
        when c_cmd_config_0 => rd_dat <= config_reg0;  rd_cmd <= c_cmd_config_1;
        when c_cmd_config_1 => rd_dat <= config_reg1;  rd_cmd <= c_cmd_config_0;
        when others         => rd_dat <= (others => '1');
      end case;
    end if;
  end process;

  iobank0 <= (others => 'H');
  iobank1 <= (others => 'H');

  input_reg0 <= iobank0 xor invert_reg0;
  input_reg1 <= iobank1 xor invert_reg1;

  p_iobank : process(output_reg0, config_reg0, output_reg1, config_reg1)
  begin
    for I in 7 downto 0 loop
      if config_reg0(I) = '0' then iobank0(I) <= output_reg0(I); else iobank0(I) <= 'Z'; end if;
      if config_reg1(I) = '0' then iobank1(I) <= output_reg1(I); else iobank1(I) <= 'Z'; end if;
    end loop;
  end process;
end beh;
