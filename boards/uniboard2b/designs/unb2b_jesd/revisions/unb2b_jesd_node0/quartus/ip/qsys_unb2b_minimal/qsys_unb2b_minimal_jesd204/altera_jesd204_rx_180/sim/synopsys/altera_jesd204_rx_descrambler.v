// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
dgdaCXHeodx0glUijterPmnfEHSZeeXTho1xxcrGEQUts/ON/leW7pdnbWKlq5O5
VvfA21baJi10oHKGpX/tOa5K+1f99KV8R6Gsq63bwNOkKibn0U+PHqvGCKAU/VZc
Nxbqb0CBusa2OtY2MK11aCOl10k3UwbF9Ixkz8ia0/A=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 944 )
`pragma protect data_block
n+93bAhXOzwUwaTcYN4cTU6FxXfmXth2ccxJEjGR/vOQ1Q1VxtxzToKo6ltVVLRi
bvz2Gg69FoORZ1tvo0wCrWjSvmdnhfZoduHpETHyai2USgqGTpeQGVstn/qoMv3j
8XHRNFDSPPExsDc0cx7EoAHz3ONlxjUofRNaxXlgYTGS/TqiAzkF2dz9XGnvY7Jm
Bq/RuwzQ9Lmd0aarO0uj6HsbxGbUKc8jlrjxPH555y40vSiBsGfdiGZjLyyVFys0
RasgBNVySj70uoqx/36BaJkmEojm3Jx9cqQACdR4Jz+i6Rhvexp+T6iNLVk9ta59
EQU454OoRhkxAkEodALLdrnqqXgsklEJvpvSyMPgLkHYv0kO3lmk9NRfVOCiimlC
jx42tlDozUveP+Dld07wwKoQaU4rgocbklnrnQRAkTlOJPQJV2IERwS1e9XQpDE9
4POzaJGCwaBEMxuYhB8qND7gVJLU/Vo8ELroqy5M5LiizHCB0Fq/pYSfBJE9xFII
WkrlzyZoz0VkeZSlk30D8brh2F1SON4dPF1tzuTqHP6TjVrLo/yH3HUUMKnJ/VnW
y1vWGDtdjZU+g+MdSDAhrmljUFwZAy3+8VXcd6SrfjCqURK0qdKKgduQxTekY+yd
Zt4d5U1Qpr/Kj7kfQAwFa+3O9z5RZPQpD48hdtX44UC6xudwVYcU50Q6pqei9A83
gtJ2e3Uv4hn0McxZRMiCqevqrN0/z1eMv0POAyOLpX26U7RWkTG7NKxmjQ/OvgOO
YnVD1OnbAMu95zacYuZAF97fdgdA7p1br4zpQydN4xBVIuoMtxlJs+9ss65m2Z0k
M+0TN2nLVfE6HYqzIsTKKCAKBMC6iLVa3Efq0qvydgQ0l1XkdYDEXwEibf3MlCdQ
a2EkFOwgt7irBL6lhq5vLugrtgCsvfj8jLjUMpnRQhhmsDkHGkE6WUfzIzlQ39rZ
m2pfuPi/u1YOyfSoDJQL4lhHuVXV4IRMx3c0DasS3glWf8JwBkDkun0LA0eq9ypW
v0He+UIjBsCL/X31abwy0Y3v+tU3lJIyyTcrYopB/61giEarWzV5aQaKEf+iukpv
0g+m98Qz3lBbVQ4BtwQPR2HSUhVK8FhbqhDCl2OdvupMnOK0olhnsvSSiZYvvuci
1kPIn7MTUj/U/76XPo7urtPHatl3WNL2qdJYrrCiY3uGoZTBSyz6WWlFsLkTdurc
toCX0QX8b29yYjdotuCzlw10ZRNOW/fKR33sXsdBids=

`pragma protect end_protected
