-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Multi tb for variants of common_complex_mult
-- Description:
-- Usage:
--   > as 3
--   > run -all
-- --------------------------------------------------------------------------

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_tb_common_complex_mult is
end tb_tb_common_complex_mult;

architecture tb of tb_tb_common_complex_mult is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_variant          : STRING := "IP";
  -- g_in_dat_w         : NATURAL := 4;
  -- g_conjugate_b      : BOOLEAN := FALSE;   -- When FALSE p = a * b, else p = a * conj(b)
  -- g_pipeline_input   : NATURAL := 1;
  -- g_pipeline_product : NATURAL := 0;
  -- g_pipeline_adder   : NATURAL := 1;
  -- g_pipeline_output  : NATURAL := 1

  -- IP variants
  u_ip_18b      : entity work.tb_common_complex_mult generic map ("IP", 18, false, 1, 0, 1, 1);
  u_ip_18b_conj : entity work.tb_common_complex_mult generic map ("IP", 18,  true, 1, 0, 1, 1);

  gen_27b : if c_tech_select_default /= c_tech_stratixiv generate
    u_ip_27b      : entity work.tb_common_complex_mult generic map ("IP", 27, false, 1, 0, 1, 1);
    u_ip_27b_conj : entity work.tb_common_complex_mult generic map ("IP", 27,  true, 1, 0, 1, 1);
  end generate;

  -- RTL variants
  u_rtl_18b      : entity work.tb_common_complex_mult generic map ("RTL", 18, false, 1, 0, 1, 1);
  u_rtl_27b      : entity work.tb_common_complex_mult generic map ("RTL", 27, false, 1, 0, 1, 1);
  u_rtl_18b_conj : entity work.tb_common_complex_mult generic map ("RTL", 18,  true, 1, 0, 1, 1);
  u_rtl_27b_conj : entity work.tb_common_complex_mult generic map ("RTL", 27,  true, 1, 0, 1, 1);
end tb;
