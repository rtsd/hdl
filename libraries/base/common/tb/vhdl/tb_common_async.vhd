-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench to compare common_async with common_areset
-- Usage:
--   > as 1
--   > run -all
--
-- Description:

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_async is
end tb_common_async;

architecture tb of tb_common_async is
  constant clk_period   : time := 10 ns;

  constant c_delay_len  : natural := 3;

  signal dbg_state                : natural;

  signal tb_end                   : std_logic := '0';
  signal clk                      : std_logic := '0';

  signal in_rst                   : std_logic;
  signal in_dat                   : std_logic;

  signal out_async                : std_logic;
  signal out_areset               : std_logic;
begin
  clk  <= not clk or tb_end after clk_period / 2;

  p_in_stimuli : process
  begin
    dbg_state <= 0;
    in_dat <= '1';
    in_rst <= '1';
    proc_common_wait_some_cycles(clk, 5);
    in_dat <= '0';
    in_rst <= '0';
    proc_common_wait_some_cycles(clk, 50);

    dbg_state <= 1;
    in_dat <= '1';
    proc_common_wait_some_cycles(clk, 5);
    in_dat <= '0';
    proc_common_wait_some_cycles(clk, 50);

    dbg_state <= 2;
    in_rst <= '1';
    proc_common_wait_some_cycles(clk, 5);
    in_rst <= '0';
    proc_common_wait_some_cycles(clk, 50);

    dbg_state <= 9;
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  u_async : entity work.common_async
  generic map (
    g_rst_level => '1',
    g_delay_len => c_delay_len
  )
  port map (
    rst  => in_rst,
    clk  => clk,
    din  => in_dat,
    dout => out_async
  );

  u_areset : entity work.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_delay_len
  )
  port map (
    in_rst    => in_rst,
    clk       => clk,
    out_rst   => out_areset
  );
end tb;
