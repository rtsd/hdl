-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- Test bench for dp_block_validate_err.
-- Description:
-- Verifies the output sosi of the DUT with the expected sosi.
-- The TB also reads the register values via MM and verifies them against the
-- expected values.
-- Usage:
-- . as 5
-- . add wave -position insertpoint  \
--   sim:/tb_dp_block_validate_err/c_exp_blk_cnt \
--   sim:/tb_dp_block_validate_err/c_exp_discarded_cnt
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_block_validate_err is
  generic (
    g_nof_blocks_per_sync  : natural := 5;
    g_nof_data_per_blk     : natural := 9;
    g_max_block_size       : natural := 9;
    g_nof_err_counts       : natural := 8;
    g_gap_size             : natural := 4;
    g_cnt_w                : natural := 3
  );
end tb_dp_block_validate_err;

architecture tb of tb_dp_block_validate_err is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_dp_clk_period  : time := 5 ns;
  constant c_mm_clk_period  : time := 10 ns;

  constant c_dut_pipeline             : natural := g_nof_data_per_blk + 3;
  constant c_nof_sync                 : natural := 5;
  constant c_nof_blk                  : natural := g_nof_blocks_per_sync * (c_nof_sync - 1);
  constant c_nof_discarded            : natural := c_nof_blk - ceil_div(c_nof_blk, 2**g_nof_err_counts);
  constant c_max_cnt                  : natural := 2**g_cnt_w - 1;

  constant c_mm_addr_dp_clear         : natural := g_nof_err_counts + 3;
  constant c_mm_addr_dp_blk_cnt       : natural := g_nof_err_counts + 1;
  constant c_mm_addr_dp_discarded_cnt : natural := g_nof_err_counts;
  constant c_exp_blk_cnt              : natural := sel_a_b(c_nof_blk < c_max_cnt, c_nof_blk, c_max_cnt);
  constant c_exp_discarded_cnt        : natural := sel_a_b(c_nof_discarded < c_max_cnt, c_nof_discarded, c_max_cnt);

  signal dp_clk : std_logic := '1';
  signal mm_clk : std_logic := '1';
  signal rst    : std_logic := '1';
  signal tb_end : std_logic := '0';

  signal stimuli_rst       : std_logic := '1';
  signal stimuli_end       : std_logic;
  signal stimuli_sosi      : t_dp_sosi;
  signal stimuli_siso      : t_dp_siso;

  signal verify_sosi       : t_dp_sosi;
  signal verify_siso       : t_dp_siso := c_dp_siso_rdy;
  signal reference_cnt     : natural;
  signal reference_cnt_reg : natural;
  signal reference_sosi    : t_dp_sosi;
  signal reference_siso    : t_dp_siso := c_dp_siso_rdy;

  signal reg_mosi          : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso          : t_mem_miso := c_mem_miso_rst;

  signal rd_blk_cnt        : natural;
  signal rd_discarded_cnt  : natural;
  signal rd_err_cnt_arr    : t_natural_arr(g_nof_err_counts - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  rst <= '1', '0' after c_dp_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  stimuli_rst <= '1', '0' after c_dp_clk_period * 17;

  -- Generate snk_in with data frames
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => g_nof_blocks_per_sync,
    g_nof_repeat  => g_nof_blocks_per_sync * c_nof_sync,
    g_pkt_len     => g_nof_data_per_blk,
    g_pkt_gap     => g_gap_size,
    g_err_init    => 0,
    g_err_incr    => 1
  )
  port map (
    rst               => stimuli_rst,
    clk               => dp_clk,

    -- Generate stimuli
    src_in            => stimuli_siso,
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dut : entity work.dp_block_validate_err
  generic map (
    g_cnt_w           => g_cnt_w,
    g_blk_cnt_w       => g_cnt_w,
    g_max_block_size  => g_max_block_size,
    g_nof_err_counts  => g_nof_err_counts,
    g_data_w          => c_word_w,
    g_bsn_w           => c_dp_stream_bsn_w,
    g_empty_w         => c_dp_stream_empty_w,
    g_channel_w       => c_dp_stream_channel_w,
    g_use_bsn         => true,
    g_use_empty       => true,
    g_use_channel     => true,
    g_use_sync        => true
  )
  port map (
    dp_rst       => rst,
    dp_clk       => dp_clk,

    ref_sync     => stimuli_sosi.sync,

    -- ST sink
    snk_out      => stimuli_siso,
    snk_in       => stimuli_sosi,
    -- ST source
    src_in       => verify_siso,
    src_out      => verify_sosi,

    mm_rst       => rst,
    mm_clk       => mm_clk,

    reg_mosi     => reg_mosi,
    reg_miso     => reg_miso
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_dut_pipeline
  )
  port map (
    rst         => rst,
    clk         => dp_clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => stimuli_sosi,
    -- ST source
    src_in      => reference_siso,
    src_out     => reference_sosi
  );

  reference_cnt_reg <= reference_cnt when rising_edge(dp_clk);
  reference_cnt     <= 0           when reference_sosi.eop = '1' and ((reference_cnt_reg + 1) mod 2**g_nof_err_counts) = 0 else
             reference_cnt_reg + 1 when reference_sosi.eop = '1' else
             reference_cnt_reg;

  p_verify : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if reference_cnt_reg = 0 then  -- no errors so we expect a block
        assert verify_sosi.valid = reference_sosi.valid
          report "Unexpected difference between in / out sosi"
          severity ERROR;
        assert verify_sosi.sop = reference_sosi.sop
          report "Unexpected difference between in / out sosi"
          severity ERROR;
        assert verify_sosi.eop = reference_sosi.eop
          report "Unexpected difference between in / out sosi"
          severity ERROR;
        assert verify_sosi.data = reference_sosi.data
          report "Unexpected difference between in / out sosi"
          severity ERROR;
        assert verify_sosi.channel = reference_sosi.channel
          report "Unexpected difference between in / out sosi"
          severity ERROR;
        assert verify_sosi.bsn = reference_sosi.bsn
          report "Unexpected difference between in / out sosi"
          severity ERROR;
        assert verify_sosi.empty = reference_sosi.empty
          report "Unexpected difference between in / out sosi"
          severity ERROR;
        assert verify_sosi.sync = reference_sosi.sync
          report "Unexpected difference between in / out sosi"
          severity ERROR;
      else  -- we expect no block as there are errors
        assert verify_sosi.valid = '0'
          report "Wrong, valid is not '0' which is unexpected."
          severity ERROR;
        assert verify_sosi.sop = '0'
          report "Wrong, sop is not '0' which is unexpected."
          severity ERROR;
        assert verify_sosi.eop = '0'
          report "Wrong, eop is not '0' which is unexpected."
          severity ERROR;
      end if;
    end if;
  end process;

  p_verify_mm : process
    variable v_X : integer := 0;  -- variable to hold 2**I * ((c_nof_blk-1) / 2**(I+1))
    variable v_Y : integer := 0;  -- variable to hold (c_nof_blk-1) + 1 - (2 * v_X) - 2**I
    -- v_N is a variable to hold the expected cnt number for the error counter registers = v_X + v_Y for v_Y > 0, else = v_x.
    -- this can be calculated as the dp error field is a counter up to c_nof_blk - 1.
    variable v_N : integer := 0;
  begin
    proc_common_wait_until_low(mm_clk, rst);
    proc_mem_mm_bus_wr(c_mm_addr_dp_clear, 1, mm_clk, reg_mosi);

    proc_common_wait_until_lo_hi(dp_clk, stimuli_end);
    proc_common_wait_some_cycles(dp_clk, c_dut_pipeline + 1);
    proc_common_wait_some_cycles(mm_clk, 1);
    proc_mem_mm_bus_rd(c_mm_addr_dp_discarded_cnt, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_discarded_cnt <= TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0));
    assert c_exp_discarded_cnt = TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0))
      report "Wrong total discarded block count"
      severity ERROR;
    proc_mem_mm_bus_rd(c_mm_addr_dp_blk_cnt, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_blk_cnt <= TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0));
    assert c_exp_blk_cnt = TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0))
      report "Wrong total block count"
      severity ERROR;
    for I in 0 to g_nof_err_counts - 1 loop
      v_X := 2**I * ((c_nof_blk - 1) / 2**(I + 1));
      v_Y := c_nof_blk - 2 * v_X - 2**I;
      if v_Y < 0 then  -- v_N = v_X + v_Y only holds for v_Y > 0.
        v_N := v_X;
      else
        v_N := v_X + v_Y;
      end if;

      if v_N > c_max_cnt then
        v_N := c_max_cnt;  -- the DUT clips the counters
      end if;

      proc_mem_mm_bus_rd(I, mm_clk, reg_miso, reg_mosi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      rd_err_cnt_arr(I) <= TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0));
      assert v_N = TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0))
        report "Wrong error count"
        severity ERROR;
    end loop;
    proc_common_wait_some_cycles(dp_clk, 10);

    -- check cnt clear
    proc_mem_mm_bus_wr(c_mm_addr_dp_clear, 1, mm_clk, reg_mosi);
    proc_common_wait_cross_clock_domain_latency(mm_clk, dp_clk);

    proc_mem_mm_bus_rd(c_mm_addr_dp_discarded_cnt, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    assert 0 = TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0))
      report "Wrong cleared total discarded block count"
      severity ERROR;
    proc_mem_mm_bus_rd(c_mm_addr_dp_blk_cnt, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    assert 0 = TO_UINT(reg_miso.rddata(c_word_w - 1 downto 0))
      report "Wrong cleared total block count"
      severity ERROR;
    proc_common_wait_some_cycles(dp_clk, 10);

    tb_end <= '1';
    wait;
  end process;
end tb;
