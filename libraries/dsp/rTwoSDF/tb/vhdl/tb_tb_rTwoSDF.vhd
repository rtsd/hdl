--------------------------------------------------------------------------------
--   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
--   Copyright (C) 2009-2010
--   ASTRON (Netherlands Institute for Radio Astronomy)
--   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
--   This file is part of the UniBoard software suite.
--   The file is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

-- Purpose:
-- Description:
--   Generates FFT testbenches (tb_rTwoSDF) for various g_in_dat_w and
--   g_nof_points. Note that twiddlePkg.vhd must be generated for the largest
--   value of g_nof_points used in this structure.

library ieee, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;
use common_lib.common_pkg.all;
use work.rTwoSDFPkg.all;

entity tb_tb_rTwoSDF is
end entity tb_tb_rTwoSDF;

architecture tb of tb_tb_rTwoSDF is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--  -- generics for tb
--  g_use_uniNoise_file : boolean  := true;
--  g_in_en             : natural  := 0;     -- 1 = always active, others = random control
--  -- generics for rTwoSDF
--  g_use_reorder       : boolean  := true;
--  g_nof_points        : natural  := 1024;
--  g_in_dat_w          : natural  := 8;
--  g_out_dat_w         : natural  := 14;
--  g_guard_w           : natural  := 2      -- guard bits are used to avoid overflow in single FFT stage.

  --u_act_impulse_16p_16i_16o         : entity work.tb_rTwoSDF generic map (false, 1,  true,   16, 16, 16, 2);
  u_act_noise_1024p_8i_14o          : entity work.tb_rTwoSDF generic map (true,  1,  true, 1024,  8, 14, 2);
  u_rnd_noise_1024p_8i_14o          : entity work.tb_rTwoSDF generic map (true,  0,  true, 1024,  8, 14, 2);
  u_rnd_noise_1024p_8i_14o_flipped  : entity work.tb_rTwoSDF generic map (true,  0, false, 1024,  8, 14, 2);
end tb;
