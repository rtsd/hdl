###############################################################################
#
# Copyright (C) 2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Constrain the input I/O path
#set_input_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] -max 3 [all_inputs]
#set_input_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] -min 2 [all_inputs]
# Constrain the output I/O path
#set_output_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] -max 3 [all_inputs]
#set_output_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] -min 2 [all_inputs]


# False path the PPS to DDIO:
#set_input_delay  -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] 3 [get_ports {PPS}]
#set_false_path -from {PPS} -to {ctrl_unb2_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e1sg_ddio_in:\gen_ip_arria10_e1sg:u0|ip_arria10_e1sg_ddio_in_1:\gen_w:0:u_ip_arria10_e1sg_ddio_in_1|ip_arria10_e1sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio~ddio_in_fr}; set_false_path -from {PPS} -to {ctrl_unb2_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e1sg_ddio_in:\gen_ip_arria10_e1sg:u0|ip_arria10_e1sg_ddio_in_1:\gen_w:0:u_ip_arria10_e1sg_ddio_in_1|ip_arria10_e1sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio~ddio_in_fr}


#set_false_path -from [get_ports {PPS}] -to [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}]

#set_input_delay -min -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] 2 [get_ports {ctrl_unb2_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|pps_ext_cap}]
#set_input_delay -max -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] 4 [get_ports {ctrl_unb2_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|pps_ext_cap}]

#set_false_path -from {PPS} -to {ctrl_unb2_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e1sg_ddio_in:\gen_ip_arria10_e1sg:u0|ip_arria10_e1sg_ddio_in_1:\gen_w:0:u_ip_arria10_e1sg_ddio_in_1|ip_arria10_e1sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio*}



set_time_format -unit ns -decimal_places 3

create_clock -period 125Mhz [get_ports {ETH_CLK[0]}]
create_clock -period 125Mhz [get_ports {ETH_CLK[1]}]
create_clock -period 200Mhz [get_ports {CLK}]
create_clock -period 100Mhz [get_ports {CLKUSR}]
create_clock -period 644.53125Mhz [get_ports {SA_CLK}]
create_clock -period 644.53125Mhz [get_ports {SB_CLK}]
create_clock -period 1.552 -name {BCK_REF_CLK} { BCK_REF_CLK }

derive_pll_clocks
derive_clock_uncertainty

set_clock_groups -asynchronous -group {CLK}
set_clock_groups -asynchronous -group {BCK_REF_CLK}
set_clock_groups -asynchronous -group {CLK_USR}
set_clock_groups -asynchronous -group {CLKUSR}
set_clock_groups -asynchronous -group {SA_CLK}
set_clock_groups -asynchronous -group {SB_CLK}
# Do not put ETH_CLK in this list, otherwise the Triple Speed Ethernet does not work

# IOPLL outputs (which have global names defined in the IP qsys settings)
set_clock_groups -asynchronous -group [get_clocks pll_clk20]
set_clock_groups -asynchronous -group [get_clocks pll_clk50]
set_clock_groups -asynchronous -group [get_clocks pll_clk100]
set_clock_groups -asynchronous -group [get_clocks pll_clk125]
set_clock_groups -asynchronous -group [get_clocks pll_clk200]
set_clock_groups -asynchronous -group [get_clocks pll_clk200p]
set_clock_groups -asynchronous -group [get_clocks pll_clk400]

# FPLL outputs
# Three of these have been removed because they cut paths between the PHY and MAC, and between the 156.25MHz
# and 132.5MHz paths within the MAC. The other two asynchronous groups will be checked to see if they are 
# needed 

#set_clock_groups -asynchronous -group [get_clocks {*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*mac_clock*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*dp_clk*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*xcvr_fpll_a10_0|outclk1}]
set_clock_groups -asynchronous -group [get_clocks {*xcvr_fpll_a10_0|outclk3}]


#set_clock_groups -asynchronous \
#-group [get_clocks {inst2|xcvr_4ch_native_phy_inst|xcvr_native_a10_0|g_xcvr_native_insts[?]|rx_pma_clk}] \
#-group [get_clocks {inst2|xcvr_pll_inst|xcvr_fpll_a10_0|tx_bonding_clocks[0]}]


#JTAG Signal Constraints
#constrain the TDI TMS and TDO ports  -- (modified from timequest SDC cookbook)
#set_input_delay  -clock altera_reserved_tck 5 [get_ports altera_reserved_tdi]
#set_input_delay  -clock altera_reserved_tck 5 [get_ports altera_reserved_tms]
#set_output_delay -clock altera_reserved_tck -clock_fall -fall -max 5 [get_ports altera_reserved_tdo]

## alternative constraints to be tested (new)
##JTAG Signal Constraints
##constrain the TCK port
#create_clock -name tck -period "10MHz" [get_ports altera_reserved_tck]
##cut all paths to and from tck 
#set_clock_groups -exclusive -group [get_clocks tck]
##constrain the TDI port
#set_input_delay -clock tck 20 [get_ports altera_reserved_tdi]
##constrain the TMS port
#set_input_delay -clock tck 20 [get_ports altera_reserved_tms]
##constrain the TDO port
#set_output_delay -clock tck 20 [get_ports altera_reserved_tdo]

