onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group tb_mmf_wpfb_unit -radix decimal -expand -subitemconfig {/tb_mmf_wpfb_unit/scope_in_sosi.sync {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.bsn {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.data {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.re {-format Analog-Step -height 84 -max 127.0 -min -127.0 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.im {-format Analog-Step -height 84 -max 127.0 -min -127.0 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.valid {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.sop {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.eop {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.empty {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.channel {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_in_sosi.err {-height 15 -radix decimal}} /tb_mmf_wpfb_unit/scope_in_sosi
add wave -noupdate -expand -group tb_mmf_wpfb_unit -radix decimal -expand -subitemconfig {/tb_mmf_wpfb_unit/scope_out_sosi.sync {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.bsn {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.data {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.re {-format Analog-Step -height 84 -max 17000.0 -min -17000.0 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.im {-format Analog-Step -height 84 -max 17000.0 -min -17000.0 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.valid {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.sop {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.eop {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.empty {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.channel {-height 15 -radix decimal} /tb_mmf_wpfb_unit/scope_out_sosi.err {-height 15 -radix decimal}} /tb_mmf_wpfb_unit/scope_out_sosi
add wave -noupdate -expand -group tb_mmf_wpfb_unit -radix decimal /tb_mmf_wpfb_unit/scope_out_power
add wave -noupdate -expand -group tb_mmf_wpfb_unit -radix decimal /tb_mmf_wpfb_unit/scope_out_ampl
add wave -noupdate -expand -group tb_mmf_wpfb_unit -radix decimal /tb_mmf_wpfb_unit/scope_out_index
add wave -noupdate -expand -group tb_mmf_wpfb_unit -radix decimal /tb_mmf_wpfb_unit/scope_out_bin
add wave -noupdate -expand -group tb_mmf_wpfb_unit -radix decimal /tb_mmf_wpfb_unit/scope_out_band
add wave -noupdate -expand -group tb_mmf_wpfb_unit -format Analog-Step -height 84 -max 17000.0 -radix decimal /tb_mmf_wpfb_unit/scope_out_ampl_x
add wave -noupdate -expand -group tb_mmf_wpfb_unit -format Analog-Step -height 84 -max 17000.0 -radix decimal /tb_mmf_wpfb_unit/scope_out_ampl_y
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {23073125 ps} 0}
configure wave -namecolwidth 300
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {101664833 ps}
