-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use work.eth_pkg.all;

entity eth_mm_registers is
  generic (
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_init_ip_address    : std_logic_vector(c_network_ip_addr_w - 1 downto 0) := X"00000000"  -- 0.0.0.0
  );
  port (
    -- Clocks and reset
    mm_rst             : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk             : in  std_logic;  -- memory-mapped bus clock
    st_rst             : in  std_logic;  -- reset synchronous with st_clk
    st_clk             : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave
    sla_in             : in  t_mem_mosi;
    sla_out            : out t_mem_miso;
    sla_interrupt      : out std_logic;

    -- MM registers in st_clk domain
    -- . write/read back
    st_reg_demux       : out t_eth_mm_reg_demux;
    st_reg_config      : out t_eth_mm_reg_config;
    st_reg_control     : out t_eth_mm_reg_control;
    st_reg_continue_wr : out std_logic;  -- used to know that control reg has been written and ETH module can continue
    -- . read only
    st_reg_frame       : in  t_eth_mm_reg_frame;
    st_reg_status      : in  t_eth_mm_reg_status;
    st_reg_status_wr   : out std_logic  -- used to know that status reg has been read and can be cleared to remove the sla_interrupt
  );
end eth_mm_registers;

architecture str of eth_mm_registers is
  -- Use MM bus data width = c_word_w = 32
  constant c_mm_reg  : t_c_mem := (latency  => 1,
                                   adr_w    => c_eth_reg_addr_w,
                                   dat_w    => c_word_w,
                                   nof_dat  => c_eth_reg_nof_words,
                                   init_sl  => '0');

  -- Initial contents of the config register
  constant c_init_vec_udp_port    : std_logic_vector(    c_network_udp_port_w - 1 downto 0) := (others => '0');
  constant c_init_vec_ip_address  : std_logic_vector(     c_network_ip_addr_w - 1 downto 0) := g_init_ip_address;
  constant c_init_vec_mac_address : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0) := (others => '0');
  constant c_init_reg_udp_port    : std_logic_vector(ceil_div(    c_network_udp_port_w, c_mm_reg.dat_w) * c_mm_reg.dat_w - 1 downto 0) := RESIZE_UVEC(c_init_vec_udp_port,    ceil_div(c_network_udp_port_w,     c_mm_reg.dat_w) * c_mm_reg.dat_w);
  constant c_init_reg_ip_address  : std_logic_vector(ceil_div(     c_network_ip_addr_w, c_mm_reg.dat_w) * c_mm_reg.dat_w - 1 downto 0) := RESIZE_UVEC(c_init_vec_ip_address,  ceil_div(c_network_ip_addr_w,      c_mm_reg.dat_w) * c_mm_reg.dat_w);
  constant c_init_reg_mac_address : std_logic_vector(ceil_div(c_network_eth_mac_addr_w, c_mm_reg.dat_w) * c_mm_reg.dat_w - 1 downto 0) := RESIZE_UVEC(c_init_vec_mac_address, ceil_div(c_network_eth_mac_addr_w, c_mm_reg.dat_w) * c_mm_reg.dat_w);

  -- Initial contents of all registers (only the MS 256 bits)
  constant c_init_reg_demux       : std_logic_vector(c_eth_reg_demux_nof_words  * c_mm_reg.dat_w - 1 downto 0) := (others => '0');
  constant c_init_reg_config      : std_logic_vector(c_eth_reg_config_nof_words * c_mm_reg.dat_w - 1 downto 0) := c_init_reg_udp_port & c_init_reg_ip_address & c_init_reg_mac_address;
  constant c_init_reg             : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := RESIZE_UVEC(c_init_reg_config & c_init_reg_demux, c_mem_reg_init_w);

  constant c_in_latency : natural := 1;

  -- Input register
  signal sla_in_reg            : t_mem_mosi;  -- so c_in_latency = 1

  -- Register store
  signal mm_vec_wr             : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0);
  signal mm_vec_rd             : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0);

  -- Sub sections register vector in the mm_clk and in the st_clk domain
  -- . write/read back
  signal mm_vec_demux          : std_logic_vector(c_eth_reg_demux_nof_words  * c_mm_reg.dat_w - 1 downto 0);
  signal st_vec_demux          : std_logic_vector(c_eth_reg_demux_nof_words  * c_mm_reg.dat_w - 1 downto 0);
  signal mm_vec_config         : std_logic_vector(c_eth_reg_config_nof_words * c_mm_reg.dat_w - 1 downto 0);
  signal st_vec_config         : std_logic_vector(c_eth_reg_config_nof_words * c_mm_reg.dat_w - 1 downto 0);
  signal mm_vec_control        : std_logic_vector(c_eth_reg_control_nof_words * c_mm_reg.dat_w - 1 downto 0);
  signal st_vec_control        : std_logic_vector(c_eth_reg_control_nof_words * c_mm_reg.dat_w - 1 downto 0);
  -- . read only
  signal mm_vec_frame          : std_logic_vector(c_eth_reg_frame_nof_words  * c_mm_reg.dat_w - 1 downto 0);
  signal st_vec_frame          : std_logic_vector(c_eth_reg_frame_nof_words  * c_mm_reg.dat_w - 1 downto 0);
  signal mm_vec_status         : std_logic_vector(c_eth_reg_status_nof_words * c_mm_reg.dat_w - 1 downto 0);
  signal st_vec_status         : std_logic_vector(c_eth_reg_status_nof_words * c_mm_reg.dat_w - 1 downto 0);

  -- Handshake control with MM master via interrupt and MM register access detection
  signal nxt_sla_interrupt      : std_logic;
  signal mm_reg_continue_wr     : std_logic;
  signal nxt_mm_reg_continue_wr : std_logic;
  signal mm_reg_status_wr       : std_logic;
  signal nxt_mm_reg_status_wr   : std_logic;

  -- . Information in the status register in the mm_clk domain is used to generate the interrupt
  signal mm_reg_status         : t_eth_mm_reg_status;
begin
  ------------------------------------------------------------------------------
  -- Register fields mapping (all wires)
  -- . wire the entire register vector to the register field vectors
  -- . map the register field vectors to the register field record using the functions
  ------------------------------------------------------------------------------

  -- . write/read back
  mm_vec_demux   <= mm_vec_wr((c_eth_reg_demux_nof_words  + c_eth_reg_demux_wi)  * c_mm_reg.dat_w - 1 downto c_eth_reg_demux_wi  * c_mm_reg.dat_w);
  mm_vec_config  <= mm_vec_wr((c_eth_reg_config_nof_words + c_eth_reg_config_wi) * c_mm_reg.dat_w - 1 downto c_eth_reg_config_wi * c_mm_reg.dat_w);
  mm_vec_control <= mm_vec_wr((c_eth_reg_control_nof_words + c_eth_reg_control_wi) * c_mm_reg.dat_w - 1 downto c_eth_reg_control_wi * c_mm_reg.dat_w);

  st_reg_demux   <= func_eth_mm_reg_demux(st_vec_demux);  -- Demux UDP registers
  st_reg_config  <= func_eth_mm_reg_config(st_vec_config);  -- Packet config registers
  st_reg_control <= func_eth_mm_reg_control(st_vec_control);  -- Rx control registers

  -- . read only
  st_vec_frame   <= func_eth_mm_reg_frame(st_reg_frame);  -- Rx frame registers
  st_vec_status  <= func_eth_mm_reg_status(st_reg_status);  -- ETH status registers

  mm_vec_rd      <= mm_vec_status & mm_vec_frame & mm_vec_control & mm_vec_config & mm_vec_demux;

  ------------------------------------------------------------------------------
  -- Register store in MM clock domain
  ------------------------------------------------------------------------------
  u_mm_reg : entity common_lib.common_reg_r_w
  generic map (
    g_reg       => c_mm_reg,
    g_init_reg  => c_init_reg
  )
  port map (
    rst         => mm_rst,
    clk         => mm_clk,
    -- control side
    wr_en       => sla_in.wr,
    wr_adr      => sla_in.address(c_mm_reg.adr_w - 1 downto 0),
    wr_dat      => sla_in.wrdata(c_mm_reg.dat_w - 1 downto 0),
    rd_en       => sla_in.rd,
    rd_adr      => sla_in.address(c_mm_reg.adr_w - 1 downto 0),
    rd_dat      => sla_out.rddata(c_mm_reg.dat_w - 1 downto 0),
    rd_val      => OPEN,
    -- data side
    out_reg     => mm_vec_wr,
    in_reg      => mm_vec_rd
  );

  ------------------------------------------------------------------------------
  -- Detect specific register accesses for handshake control with MM master
  ------------------------------------------------------------------------------

  -- Register sla_in to ease timing closure on these MM bus signals, the extra latency does not matter
  p_detect : process(mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      sla_in_reg         <= c_mem_mosi_rst;
      sla_interrupt      <= '0';
      mm_reg_continue_wr <= '0';
      mm_reg_status_wr   <= '0';
    elsif rising_edge(mm_clk) then
      sla_in_reg         <= sla_in;
      sla_interrupt      <= nxt_sla_interrupt;
      mm_reg_continue_wr <= nxt_mm_reg_continue_wr;
      mm_reg_status_wr   <= nxt_mm_reg_status_wr;
    end if;
  end process;

  nxt_mm_reg_continue_wr <= '1' when sla_in_reg.wr = '1' and unsigned(sla_in_reg.address(c_mm_reg.adr_w - 1 downto 0)) = c_eth_reg_continue_wi else '0';
  nxt_mm_reg_status_wr   <= '1' when sla_in_reg.wr = '1' and unsigned(sla_in_reg.address(c_mm_reg.adr_w - 1 downto 0)) = c_eth_reg_status_wi   else '0';

  ------------------------------------------------------------------------------
  -- Interrupt MM master based on status register info
  ------------------------------------------------------------------------------

  -- Need to use the status register in the mm_clk domain to get the interrupt information
  mm_reg_status <= func_eth_mm_reg_status(mm_vec_status);

  -- Interrupt when there is a new Rx packet in the buffer or to acknowledge Tx instert request
  nxt_sla_interrupt <= mm_reg_status.rx_avail or mm_reg_status.tx_avail;

  no_cross : if g_cross_clock_domain = false generate
    -- MM -> ST
    st_vec_demux   <= mm_vec_demux;
    st_vec_config  <= mm_vec_config;
    st_vec_control <= mm_vec_control;

    st_reg_status_wr   <= mm_reg_status_wr;
    st_reg_continue_wr <= mm_reg_continue_wr;

    -- ST -> MM
    mm_vec_frame  <= st_vec_frame;
    mm_vec_status <= st_vec_status;
  end generate;

  gen_cross : if g_cross_clock_domain = true generate
    ------------------------------------------------------------------------------
    -- Cross the register access event between the MM and the ST clock domain
    ------------------------------------------------------------------------------

    -- MM -> ST
    u_reg_status_wr : entity common_lib.common_spulse
    port map (
      in_rst       => mm_rst,
      in_clk       => mm_clk,
      in_pulse     => mm_reg_status_wr,
      in_busy      => OPEN,
      out_rst      => st_rst,
      out_clk      => st_clk,
      out_pulse    => st_reg_status_wr
    );

    u_reg_continue_wr : entity common_lib.common_spulse
    port map (
      in_rst       => mm_rst,
      in_clk       => mm_clk,
      in_pulse     => mm_reg_continue_wr,
      in_busy      => OPEN,
      out_rst      => st_rst,
      out_clk      => st_clk,
      out_pulse    => st_reg_continue_wr
    );

    ------------------------------------------------------------------------------
    -- Cross the register data between the MM and the ST clock domain
    ------------------------------------------------------------------------------

    -- MM -> ST
    u_demux_wr : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_dat      => mm_vec_demux,
      in_new      => sla_in_reg.wr,  -- using the slave wr is OK to trigger a cross clock domain
      in_done     => OPEN,
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => st_vec_demux,
      out_new     => open
    );

    u_config_wr : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_dat      => mm_vec_config,
      in_new      => sla_in_reg.wr,  -- using the slave wr is OK to trigger a cross clock domain
      in_done     => OPEN,
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => st_vec_config,
      out_new     => open
    );

    u_control_wr : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_dat      => mm_vec_control,
      in_new      => sla_in_reg.wr,  -- using the slave wr is OK to trigger a cross clock domain
      in_done     => OPEN,
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => st_vec_control,
      out_new     => open
    );

    -- ST -> MM
    -- . no need to use in_new, continuously cross the clock domain
    u_frame_rd : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_vec_frame,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_vec_frame
    );

    u_status_rd : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_dat      => st_vec_status,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_vec_status
    );
  end generate;
end str;
