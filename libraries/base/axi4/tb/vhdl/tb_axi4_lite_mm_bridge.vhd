-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:
--   TB for testing axi4_lite_mm_bridge using common_ram_r_w
-- Description:
--   Stimuli/verification is done by a MM interface generated using proc_mem_mm_bus
--   procedures. The DUT translates the MM stimuli to AXI4-Lite which is looped
--   back into the DUT to translate it again to MM. The resulting MM is connected
--   to RAM to provide some address space for verification. Additionaly the
--   mm_waitrequest_model is used to provide backpressure to model a peripheral
--   with flow control.
--
--               +---------------------+    +------------+   +-------+
-- Stimuli/Verify|         DUT         |    | Waitrequest|   |       |
--       <------->mm_in          mm_out<---->   Model    <--->  RAM  |
--               | copi/cipo  copi/cipo|    |            |   |       |
--               |                     |    +------------+   +-------+
--            +-->axi_in        axi_out<--+
--            |  | copi/cipo  copi/cipo|  |
--            |  +---------------------+  |
--            |                           |
--            +---------------------------+
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use work.axi4_lite_pkg.all;

entity tb_axi4_lite_mm_bridge is
end tb_axi4_lite_mm_bridge;

architecture tb of tb_axi4_lite_mm_bridge is
  constant c_mm_clk_period   : time := 40 ns;
  constant c_reset_len       : natural := 4;

  constant c_mm_usr_ram      : t_c_mem :=   (latency    => 1,
                                             adr_w      => 5,
                                             dat_w      => 8,
                                             nof_dat    => 32,
                                             init_sl    => '0');
  constant c_offset          : natural := 57;  -- Some value to offset the counter data written to ram.

  signal mm_rst              : std_logic;
  signal mm_clk              : std_logic := '0';
  signal tb_end              : std_logic := '0';

  signal mm_in_copi          : t_mem_copi := c_mem_copi_rst;
  signal mm_in_cipo          : t_mem_cipo := c_mem_cipo_rst;
  signal mm_out_copi         : t_mem_copi := c_mem_copi_rst;
  signal mm_out_cipo         : t_mem_cipo := c_mem_cipo_rst;
  signal ram_copi            : t_mem_copi := c_mem_copi_rst;
  signal ram_cipo            : t_mem_cipo := c_mem_cipo_rst;

  signal axi_copi            : t_axi4_lite_copi;
  signal axi_cipo            : t_axi4_lite_cipo;
begin
  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * c_reset_len;

  -- DUT
  u_axi4_lite_mm_bridge : entity work.axi4_lite_mm_bridge
  port map (
     in_clk        => mm_clk,
     in_rst        => mm_rst,

     axi4_in_copi  => axi_copi,
     axi4_in_cipo  => axi_cipo,
     mm_out_copi   => mm_out_copi,
     mm_out_cipo   => mm_out_cipo,
     mm_in_copi    => mm_in_copi,
     mm_in_cipo    => mm_in_cipo,
     axi4_out_copi => axi_copi,
     axi4_out_cipo => axi_cipo
  );

  -- Provide waitrequest stimuli to model a peripheral with MM flow control.
  u_waitrequest_model : entity mm_lib.mm_waitrequest_model
  generic map (
    g_waitrequest => true,
    g_seed        => c_offset
  )
  port map (
    mm_clk     => mm_clk,
    bus_mosi   => mm_out_copi,
    bus_miso   => mm_out_cipo,
    slave_mosi => ram_copi,
    slave_miso => ram_cipo
  );
  -- Use common ram as a MM peripherpal.
  u_ram : entity common_lib.common_ram_r_w
  generic map (
    g_ram     => c_mm_usr_ram
  )
  port map (
    rst       => mm_rst,
    clk       => mm_clk,
    clken     => '1',
    wr_en     => ram_copi.wr,
    wr_dat    => ram_copi.wrdata(c_mm_usr_ram.dat_w - 1 downto 0),
    wr_adr    => ram_copi.address(c_mm_usr_ram.adr_w - 1 downto 0),
    rd_en     => ram_copi.rd,
    rd_adr    => ram_copi.address(c_mm_usr_ram.adr_w - 1 downto 0),
    rd_dat    => ram_cipo.rddata(c_mm_usr_ram.dat_w - 1 downto 0),
    rd_val    => ram_cipo.rdval
  );

  -- Testbench writes/reads a number of words to/from memory through the axi4_lite_mm_bridge.
  -- This tests the interface MM <-> AXI4-Lite <-> MM.
  tb : process
  begin
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 3);
    -- write a number of words to memory
    for I in 0 to 10 loop
      proc_mem_mm_bus_wr(I, (c_offset + I),  mm_clk, mm_in_cipo, mm_in_copi);
    end loop;

    proc_common_wait_some_cycles(mm_clk, 10);
    -- read the words from memory
    for i in 0 to 10 loop
      proc_mem_mm_bus_rd(I, mm_clk, mm_in_cipo, mm_in_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      assert TO_UINT(mm_in_cipo.rddata(c_mm_usr_ram.dat_w - 1 downto 0)) = (c_offset + I)
        report "Wrong value read from RAM at address " & int_to_str(I) & " expected " & int_to_str(c_offset + I)
               & " but received " & int_to_str(TO_UINT(mm_in_cipo.rddata(c_mm_usr_ram.dat_w - 1 downto 0)))
        severity ERROR;
    end loop;

    tb_end <= '1';
    wait;
  end process tb;
end tb;
