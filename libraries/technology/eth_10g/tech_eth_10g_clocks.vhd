-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Connect the clocks for the tech_eth_10g.
-- Description:
--   Apply the clocks from top level down, such that they have their
--   rising_edge() aligned without any delta-delay. The reference clocks
--   are also reassigned to preserve same delta-cycle phase as for the data
--   clocks.

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tech_eth_10g_clocks is
  generic (
    g_technology          : natural := c_tech_select_default;
    g_nof_channels        : natural := 1
  );
  port (
    -- Input clocks
    -- . Reference
    tr_ref_clk_644    : in  std_logic := 'X';  -- 10GBASE-R
    tr_ref_clk_312    : in  std_logic := 'X';  -- 10GBASE-R
    tr_ref_clk_156    : in  std_logic := 'X';  -- 10GBASE-R or XAUI
    tr_ref_rst_156    : in  std_logic := 'X';  -- 10GBASE-R or XAUI

    -- . XAUI
    tx_rst_arr        : in  std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');
    rx_clk_arr        : in  std_logic_vector(g_nof_channels - 1 downto 0) := (others => 'X');
    rx_rst_arr        : in  std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');

    -- Output clocks
    -- . Reference
    eth_ref_clk_644   : out std_logic;
    eth_ref_clk_312   : out std_logic;
    eth_ref_clk_156   : out std_logic;
    eth_ref_rst_156   : out std_logic;

    -- . Data
    eth_tx_clk_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    eth_tx_rst_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);

    eth_rx_clk_arr    : out std_logic_vector(g_nof_channels - 1 downto 0);
    eth_rx_rst_arr    : out std_logic_vector(g_nof_channels - 1 downto 0)
  );
end tech_eth_10g_clocks;

architecture str of tech_eth_10g_clocks is
begin
  -- Reference
  eth_ref_clk_644 <= tr_ref_clk_644;
  eth_ref_clk_312 <= tr_ref_clk_312;
  eth_ref_clk_156 <= tr_ref_clk_156;
  eth_ref_rst_156 <= tr_ref_rst_156;

  gen_clocks_xaui : if g_technology = c_tech_stratixiv generate
    -- Data
    eth_tx_clk_arr  <= (others => tr_ref_clk_156);
    eth_tx_rst_arr  <= tx_rst_arr;

    eth_rx_clk_arr  <= rx_clk_arr;
    eth_rx_rst_arr  <= rx_rst_arr;
  end generate;

  gen_clocks_10gbase_r : if g_technology = c_tech_arria10_proto or g_technology = c_tech_arria10_e3sge3 or g_technology = c_tech_arria10_e1sg or g_technology = c_tech_arria10_e2sg generate
    eth_tx_clk_arr  <= (others => tr_ref_clk_156);
    eth_tx_rst_arr  <= (others => tr_ref_rst_156);

    eth_rx_clk_arr  <= (others => tr_ref_clk_156);
    eth_rx_rst_arr  <= (others => tr_ref_rst_156);
  end generate;
end str;
