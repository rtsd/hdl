/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

#ifndef COMMON_UTIL_H
#define COMMON_UTIL_H


/* includes */
#include "common_types.h"


/* public constants */

// Define UTIL_HOST_IS_LITTLE_ENDIAN if the host is little endian, comment if
// the host is big endian.
// . Network communication is big endian (MSByte is 'big end first').
// . The Nios II architecture is little endian (LSByte is 'little end first').
#define UTIL_HOST_IS_LITTLE_ENDIAN

#ifndef UTIL_HOST_IS_LITTLE_ENDIAN
#define UTIL_HOST_IS_BIG_ENDIAN
#endif

/* public macros */

// Define host to network big endian 'b' and host 'h' to little endian 'l' for
// 32 bit 'l' and for 16 bit 's'
#ifdef UTIL_HOST_IS_LITTLE_ENDIAN
#define UTIL_htoll(a) (a)
#define UTIL_htols(a) (a)
#define UTIL_htobl(a) (UTIL_swapl(a))
#define UTIL_htobs(a) (UTIL_swaps(a))
#else
#define UTIL_htoll(a) (UTIL_swapl(a))
#define UTIL_htols(a) (UTIL_swaps(a))
#define UTIL_htobl(a) (a)
#define UTIL_htobs(a) (a)
#endif
// For completeness also define equivalent 'n' to 'h' and 'l' to 'h' for
// 32 bit and 16 bit
#define UTIL_ltohl(a) (UTIL_htoll(a))
#define UTIL_ltohs(a) (UTIL_htols(a))
#define UTIL_btohl(a) (UTIL_htobl(a))
#define UTIL_btohs(a) (UTIL_htobs(a))

#define UTIL_MAX(a, b) ((a) > (b) ? (a) : (b))
#define UTIL_SEL_A_B(sel, a, b) ((sel) ? (a) : (b))


/* public types */

/* public variables */

/* public function protos */
TUInt32 UTIL_swapl(TUInt32 in_dat);
TUInt16 UTIL_swaps(TUInt16 in_dat);

#endif

