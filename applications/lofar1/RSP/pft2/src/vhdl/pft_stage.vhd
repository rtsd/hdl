-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: pipelined FFT
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;

entity pft_stage is
  generic (
    g_index          : natural;
    g_in_dat_w       : natural;
    g_out_dat_w      : natural
  );
  port (
    in_re            : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_im            : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val           : in  std_logic;
    in_sync          : in  std_logic;
    out_re           : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_im           : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val          : out std_logic;
    out_sync         : out std_logic;
    clk              : in  std_logic;
    rst              : in  std_logic
 );
end pft_stage;

architecture str of pft_stage is
  constant c_round_pipeline_in  : natural := 1;
  constant c_round_pipeline_out : natural := 1;
  constant c_round_pipeline     : natural := c_round_pipeline_in + c_round_pipeline_out;

  constant c_bf1_out_w   : natural := g_in_dat_w + 1;
  constant c_bf2_out_w   : natural := g_in_dat_w + 2;

  signal bf1_re    : std_logic_vector(c_bf1_out_w - 1 downto 0);
  signal bf1_im    : std_logic_vector(c_bf1_out_w - 1 downto 0);
  signal bf1_val   : std_logic;
  signal bf1_sync  : std_logic;

  signal bf2_re    : std_logic_vector(c_bf2_out_w - 1 downto 0);
  signal bf2_im    : std_logic_vector(c_bf2_out_w - 1 downto 0);
  signal bf2_val   : std_logic;
  signal bf2_sync  : std_logic;
begin
  gen_middle: if g_index > 0 generate
    bf1 : entity work.pft_bf
    generic map (
      g_index          => 2 * g_index + 1,
      g_in_dat_w       => g_in_dat_w,
      g_out_dat_w      => c_bf1_out_w,
      g_bf_name        => "bf1"
    )
    port map (
      in_re            => in_re,
      in_im            => in_im,
      in_val           => in_val,
      in_sync          => in_sync,
      out_re           => bf1_re,
      out_im           => bf1_im,
      out_val          => bf1_val,
      out_sync         => bf1_sync,
      clk              => clk,
      rst              => rst
    );

    bf2 : entity work.pft_bf
    generic map (
      g_index          => 2 * g_index,
      g_in_dat_w       => c_bf1_out_w,
      g_out_dat_w      => c_bf2_out_w,
      g_bf_name        => "bf2"
    )
    port map (
      in_re            => bf1_re,
      in_im            => bf1_im,
      in_val           => bf1_val,
      in_sync          => bf1_sync,
      out_re           => bf2_re,
      out_im           => bf2_im,
      out_val          => bf2_val,
      out_sync         => bf2_sync,
      clk              => clk,
      rst              => rst
    );

    tmult : entity work.pft_tmult
    generic map (
      g_in_dat_w     => c_bf2_out_w,
      g_out_dat_w    => g_out_dat_w,
      g_index        => g_index
    )
    port map (
      in_re          => bf2_re,
      in_im          => bf2_im,
      in_val         => bf2_val,
      in_sync        => bf2_sync,
      out_re         => out_re,
      out_im         => out_im,
      out_val        => out_val,
      out_sync       => out_sync,
      clk            => clk,
      rst            => rst
    );
  end generate;

  gen_last: if g_index = 0 generate
    signal reg_val  : std_logic;
    signal reg_sync : std_logic;

  begin

    bf1_fw : entity work.pft_bf_fw
    generic map (
      g_index          => 2 * g_index + 1,
      g_in_dat_w       => g_in_dat_w,
      g_out_dat_w      => c_bf1_out_w,
      g_bf_name        => "bf1"
    )
    port map (
      in_re            => in_re,
      in_im            => in_im,
      in_val           => in_val,
      in_sync          => in_sync,
      out_re           => bf1_re,
      out_im           => bf1_im,
      out_val          => bf1_val,
      out_sync         => bf1_sync,
      clk              => clk,
      rst              => rst
    );

    bf2_fw : entity work.pft_bf_fw
    generic map (
      g_index          => 2 * g_index,
      g_in_dat_w       => c_bf1_out_w,
      g_out_dat_w      => c_bf2_out_w,
      g_bf_name        => "bf2"
    )
    port map (
      in_re            => bf1_re,
      in_im            => bf1_im,
      in_val           => bf1_val,
      in_sync          => bf1_sync,
      out_re           => bf2_re,
      out_im           => bf2_im,
      out_val          => bf2_val,
      out_sync         => bf2_sync,
      clk              => clk,
      rst              => rst
    );

    u_rnd : entity common_lib.common_complex_round
    generic map (
      g_representation  => "SIGNED",
      g_round           => true,
      g_round_clip      => false,
      g_pipeline_input  => c_round_pipeline_in,
      g_pipeline_output => c_round_pipeline_out,
      g_in_dat_w        => c_bf2_out_w,
      g_out_dat_w       => g_out_dat_w
    )
    port map (
      in_re          => bf2_re,
      in_im          => bf2_im,
      out_re         => out_re,
      out_im         => out_im,
      clk            => clk
    );

    p_regs: process(clk, rst)
    begin
      if rst = '1' then
        reg_val  <= '0';
        reg_sync <= '0';
        out_val  <= '0';
        out_sync <= '0';
      elsif rising_edge(clk) then
        out_val  <= reg_val;
        out_sync <= reg_sync;
        reg_val  <= bf2_val;
        reg_sync <= bf2_sync;
      end if;
    end process;

  end generate;
end str;
