-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Dual page memory with dual wr in one page and dual rd in other page
-- Description:
--   When next_page pulses then the next access will occur in the other page.
-- Remarks:
--   Each page uses one or more RAM blocks.

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;
use work.common_components_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_paged_ram_ww_rr is
  generic (
    g_technology      : natural := c_tech_select_default;
    g_pipeline_in     : natural := 0;  -- >= 0
    g_pipeline_out    : natural := 0;  -- >= 0
    g_data_w          : natural;
    g_page_sz         : natural;
    g_ram_rd_latency  : natural := 1  -- >= 1
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    -- next page control
    next_page   : in  std_logic;
    -- double write access to one page
    wr_adr_a    : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    wr_en_a     : in  std_logic := '0';
    wr_dat_a    : in  std_logic_vector(g_data_w - 1 downto 0) := (others => '0');
    wr_adr_b    : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    wr_en_b     : in  std_logic := '0';
    wr_dat_b    : in  std_logic_vector(g_data_w - 1 downto 0) := (others => '0');
    -- double read access from the other one page
    rd_adr_a    : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    rd_en_a     : in  std_logic := '1';
    rd_adr_b    : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    rd_en_b     : in  std_logic := '1';
    -- double read data from the other one page after c_rd_latency
    rd_dat_a    : out std_logic_vector(g_data_w - 1 downto 0);
    rd_val_a    : out std_logic;
    rd_dat_b    : out std_logic_vector(g_data_w - 1 downto 0);
    rd_val_b    : out std_logic
  );
end common_paged_ram_ww_rr;

architecture rtl of common_paged_ram_ww_rr is
  constant c_sel_latency      : natural := g_pipeline_in + g_ram_rd_latency;
  constant c_rd_latency       : natural := g_pipeline_in + g_ram_rd_latency + g_pipeline_out;

  constant c_nof_ports        : natural := 2;  -- Fixed dual port, port a and port b
  constant c_nof_pages        : natural := 2;  -- Fixed dual page, page 0 and page 1

  constant c_addr_w           : natural := ceil_log2(g_page_sz);

  constant c_page_ram         : t_c_mem := (latency  => g_ram_rd_latency,
                                            adr_w    => c_addr_w,
                                            dat_w    => g_data_w,
                                            nof_dat  => g_page_sz,
                                            init_sl  => '0');

  type t_data_arr is array (integer range <>) of std_logic_vector(g_data_w - 1 downto 0);
  type t_addr_arr is array (integer range <>) of std_logic_vector(c_addr_w - 1 downto 0);

  -- Page select control
  signal page_sel           : std_logic;
  signal nxt_page_sel       : std_logic;
  signal page_sel_dly       : std_logic_vector(0 to c_sel_latency - 1);
  signal nxt_page_sel_dly   : std_logic_vector(0 to c_sel_latency - 1);
  signal page_sel_in        : std_logic;
  signal page_sel_out       : std_logic;

  -- Double write in one page and double read in the other page
  -- . input
  signal nxt_page_wr_dat_a  : std_logic_vector(g_data_w - 1 downto 0);
  signal nxt_page_wr_dat_b  : std_logic_vector(g_data_w - 1 downto 0);
  signal page_wr_dat_a      : std_logic_vector(g_data_w - 1 downto 0);
  signal page_wr_dat_b      : std_logic_vector(g_data_w - 1 downto 0);
  signal nxt_page_wr_en_a   : std_logic_vector(0 to c_nof_pages - 1);
  signal nxt_page_wr_en_b   : std_logic_vector(0 to c_nof_pages - 1);
  signal page_wr_en_a       : std_logic_vector(0 to c_nof_pages - 1);
  signal page_wr_en_b       : std_logic_vector(0 to c_nof_pages - 1);
  signal nxt_page_rd_en_a   : std_logic_vector(0 to c_nof_pages - 1);
  signal nxt_page_rd_en_b   : std_logic_vector(0 to c_nof_pages - 1);
  signal page_rd_en_a       : std_logic_vector(0 to c_nof_pages - 1);
  signal page_rd_en_b       : std_logic_vector(0 to c_nof_pages - 1);
  signal nxt_page_adr_a     : t_addr_arr(0 to c_nof_pages - 1);
  signal nxt_page_adr_b     : t_addr_arr(0 to c_nof_pages - 1);
  signal page_adr_a         : t_addr_arr(0 to c_nof_pages - 1);
  signal page_adr_b         : t_addr_arr(0 to c_nof_pages - 1);

  -- . output
  signal page_rd_dat_a      : t_data_arr(0 to c_nof_pages - 1);
  signal page_rd_val_a      : std_logic_vector(0 to c_nof_pages - 1);
  signal page_rd_dat_b      : t_data_arr(0 to c_nof_pages - 1);
  signal page_rd_val_b      : std_logic_vector(0 to c_nof_pages - 1);

  signal nxt_rd_dat_a       : std_logic_vector(g_data_w - 1 downto 0);
  signal nxt_rd_val_a       : std_logic;
  signal nxt_rd_dat_b       : std_logic_vector(g_data_w - 1 downto 0);
  signal nxt_rd_val_b       : std_logic;
begin
  -- page select
  p_reg : process (rst, clk)
  begin
    if rst = '1' then
      page_sel     <= '0';
      page_sel_dly <= (others => '0');
    elsif rising_edge(clk) then
      page_sel     <= nxt_page_sel;
      page_sel_dly <= nxt_page_sel_dly;
    end if;
  end process;

  nxt_page_sel <= not page_sel when next_page = '1' else page_sel;

  nxt_page_sel_dly(0)                    <= page_sel;
  nxt_page_sel_dly(1 to c_sel_latency - 1) <= page_sel_dly(0 to c_sel_latency - 2);

  page_sel_in  <= page_sel;
  page_sel_out <= page_sel_dly(c_sel_latency - 1);

  -- apply the data to both pages, the page_wr_en* is only active for the selected page
  nxt_page_wr_dat_a   <= wr_dat_a;
  nxt_page_wr_dat_b   <= wr_dat_b;

  -- use page_sel_in for RAM access control
  nxt_page_wr_en_a(0) <= wr_en_a  when page_sel_in = '0' else '0';
  nxt_page_wr_en_b(0) <= wr_en_b  when page_sel_in = '0' else '0';
  nxt_page_wr_en_a(1) <= wr_en_a  when page_sel_in = '1' else '0';
  nxt_page_wr_en_b(1) <= wr_en_b  when page_sel_in = '1' else '0';

  nxt_page_rd_en_a(0) <= rd_en_a  when page_sel_in = '1' else '0';
  nxt_page_rd_en_b(0) <= rd_en_b  when page_sel_in = '1' else '0';
  nxt_page_rd_en_a(1) <= rd_en_a  when page_sel_in = '0' else '0';
  nxt_page_rd_en_b(1) <= rd_en_b  when page_sel_in = '0' else '0';

  nxt_page_adr_a(0)   <= wr_adr_a when page_sel_in = '0' else rd_adr_a;
  nxt_page_adr_b(0)   <= wr_adr_b when page_sel_in = '0' else rd_adr_b;
  nxt_page_adr_a(1)   <= wr_adr_a when page_sel_in = '1' else rd_adr_a;
  nxt_page_adr_b(1)   <= wr_adr_b when page_sel_in = '1' else rd_adr_b;

  -- input mux pipelining
  u_pipe_page_wr_dat_a : common_pipeline    generic map ("SIGNED", g_pipeline_in, 0, g_data_w, g_data_w) port map (rst, clk, clken, '0', '1', nxt_page_wr_dat_a, page_wr_dat_a);
  u_pipe_page_wr_dat_b : common_pipeline    generic map ("SIGNED", g_pipeline_in, 0, g_data_w, g_data_w) port map (rst, clk, clken, '0', '1', nxt_page_wr_dat_b, page_wr_dat_b);

  two_pages : for I in 0 to c_nof_pages - 1 generate
    -- input mux pipelining
    u_pipe_page_wr_en_a  : common_pipeline_sl generic map (          g_pipeline_in, 0, false)              port map (rst, clk, clken, '0', '1', nxt_page_wr_en_a(I),  page_wr_en_a(I));
    u_pipe_page_wr_en_b  : common_pipeline_sl generic map (          g_pipeline_in, 0, false)              port map (rst, clk, clken, '0', '1', nxt_page_wr_en_b(I),  page_wr_en_b(I));
    u_pipe_page_rd_en_a  : common_pipeline_sl generic map (          g_pipeline_in, 0, false)              port map (rst, clk, clken, '0', '1', nxt_page_rd_en_a(I),  page_rd_en_a(I));
    u_pipe_page_rd_en_b  : common_pipeline_sl generic map (          g_pipeline_in, 0, false)              port map (rst, clk, clken, '0', '1', nxt_page_rd_en_b(I),  page_rd_en_b(I));
    u_pipe_page_adr_a    : common_pipeline    generic map ("SIGNED", g_pipeline_in, 0, c_addr_w, c_addr_w) port map (rst, clk, clken, '0', '1', nxt_page_adr_a(I),    page_adr_a(I));
    u_pipe_page_adr_b    : common_pipeline    generic map ("SIGNED", g_pipeline_in, 0, c_addr_w, c_addr_w) port map (rst, clk, clken, '0', '1', nxt_page_adr_b(I),    page_adr_b(I));

    u_page : entity work.common_ram_rw_rw
    generic map (
      g_technology => g_technology,
      g_ram       => c_page_ram,
      g_init_file => "UNUSED"
    )
    port map (
      rst       => rst,
      clk       => clk,
      clken     => clken,
      adr_a     => page_adr_a(I),
      wr_en_a   => page_wr_en_a(I),
      wr_dat_a  => page_wr_dat_a,
      rd_en_a   => page_rd_en_a(I),
      rd_dat_a  => page_rd_dat_a(I),
      rd_val_a  => page_rd_val_a(I),
      adr_b     => page_adr_b(I),
      wr_en_b   => page_wr_en_b(I),
      wr_dat_b  => page_wr_dat_b,
      rd_en_b   => page_rd_en_b(I),
      rd_dat_b  => page_rd_dat_b(I),
      rd_val_b  => page_rd_val_b(I)
    );
  end generate;

  -- use page_sel_out to account for the RAM read latency
  nxt_rd_dat_a <= page_rd_dat_a(0) when page_sel_out = '1' else page_rd_dat_a(1);
  nxt_rd_dat_b <= page_rd_dat_b(0) when page_sel_out = '1' else page_rd_dat_b(1);
  nxt_rd_val_a <= page_rd_val_a(0) when page_sel_out = '1' else page_rd_val_a(1);
  nxt_rd_val_b <= page_rd_val_b(0) when page_sel_out = '1' else page_rd_val_b(1);

  -- output mux pipelining
  u_pipe_rd_dat_a : common_pipeline    generic map ("SIGNED", g_pipeline_out, 0, g_data_w, g_data_w) port map (rst, clk, clken, '0', '1', nxt_rd_dat_a, rd_dat_a);
  u_pipe_rd_dat_b : common_pipeline    generic map ("SIGNED", g_pipeline_out, 0, g_data_w, g_data_w) port map (rst, clk, clken, '0', '1', nxt_rd_dat_b, rd_dat_b);
  u_pipe_rd_val_a : common_pipeline_sl generic map (          g_pipeline_out, 0, false)              port map (rst, clk, clken, '0', '1', nxt_rd_val_a, rd_val_a);
  u_pipe_rd_val_b : common_pipeline_sl generic map (          g_pipeline_out, 0, false)              port map (rst, clk, clken, '0', '1', nxt_rd_val_b, rd_val_b);
end rtl;
