-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: Reinier vd Walle
-- Purpose: Tb to show that lofar2_unb2c_ring_one can simulate
-- Description:
--   This is a compile-only test bench
-- Usage:
--   Load sim    # check that design can load in vsim
--   > as 10     # check that the hierarchy for g_design_name is complete
--   > run -a    # check that design can simulate some us without error

library IEEE, common_lib, unb2c_board_lib, i2c_lib, tech_pll_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use common_lib.tb_common_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tb_lofar2_unb2c_ring_one is
end tb_lofar2_unb2c_ring_one;

architecture tb of tb_lofar2_unb2c_ring_one is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;
  constant c_node_nr         : natural := 0;
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);

  constant c_eth_clk_period  : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period  : time := 5 ns;
  constant c_sa_clk_period   : time := tech_pll_clk_644_period;  -- 644MHz
  constant c_pps_period      : natural := 1000;

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_txp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal pmbus_scl           : std_logic;
  signal pmbus_sda           : std_logic;

  signal SA_CLK              : std_logic := '1';

  signal i_QSFP_0_TX         : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
  signal i_QSFP_0_RX         : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
  signal i_RING_0_TX         : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
  signal i_RING_0_RX         : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
  signal i_RING_1_TX         : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
  signal i_RING_1_RX         : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk(0) <= not eth_clk(0) after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  SA_CLK <= not SA_CLK after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up
  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2c_ring_one : entity work.lofar2_unb2c_ring_one
  generic map (
    g_sim         => c_sim,
    g_sim_unb_nr  => c_unb_nr,
    g_sim_node_nr => c_node_nr
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- Transceiver clocks
    SA_CLK       => SA_CLK,

    -- front transceivers
    QSFP_0_RX    => i_QSFP_0_RX,
    QSFP_0_TX    => i_QSFP_0_TX,

    -- ring transceivers
    RING_0_RX    => i_RING_0_RX,
    RING_0_TX    => i_RING_0_TX,
    RING_1_RX    => i_RING_1_RX,
    RING_1_TX    => i_RING_1_TX,

    -- LEDs
    QSFP_LED     => open
  );

  ------------------------------------------------------------------------------
  -- Simulation end
  ------------------------------------------------------------------------------
  sim_done <= '0', '1' after 1 us;

  proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
end tb;
