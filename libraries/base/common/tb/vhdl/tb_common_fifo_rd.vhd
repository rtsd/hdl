-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_lfsr_sequences_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_fifo_rd is
  generic (
    g_random_control : boolean := true  -- use TRUE for random rd_req control
  );
end tb_common_fifo_rd;

-- Run -all, observe rd_dat in wave window

architecture tb of tb_common_fifo_rd is
  constant clk_period   : time := 10 ns;
  constant c_dat_w      : natural := 16;
  constant c_fifo_rl    : natural := 1;  -- FIFO has RL = 1
  constant c_read_rl    : natural := 0;  -- show ahead FIFO has RL = 0

  signal rst         : std_logic;
  signal clk         : std_logic := '0';
  signal tb_end      : std_logic := '0';

  signal fifo_req    : std_logic;
  signal fifo_dat    : std_logic_vector(c_dat_w - 1 downto 0);
  signal fifo_val    : std_logic;

  signal rd_req      : std_logic;
  signal rd_dat      : std_logic_vector(c_dat_w - 1 downto 0);
  signal rd_val      : std_logic;

  signal enable      : std_logic := '1';
  signal random      : std_logic_vector(31 downto 0) := (others => '0');
  signal verify_en   : std_logic := '1';
  signal prev_rd_req : std_logic;
  signal prev_rd_dat : std_logic_vector(c_dat_w - 1 downto 0);
begin
  rst <= '1', '0' after clk_period * 7;
  clk <= not clk or tb_end after clk_period / 2;
  tb_end <= '0', '1' after 20 us;

  verify_en <= '0', '1' after clk_period * 35;

  -- Model FIFO output with c_rl = 1 and counter data starting at 0
  proc_common_gen_data(c_fifo_rl, 0, rst, clk, enable, fifo_req, fifo_dat, fifo_val);

  -- Model rd_req
  random <= func_common_random(random) when rising_edge(clk);
  rd_req <= random(random'high) when g_random_control = true else '1';

  -- Verify dut output incrementing data
  proc_common_verify_data(c_read_rl, clk, verify_en, rd_req, rd_val, rd_dat, prev_rd_dat);

  -- Verify dut output stream ready - valid relation, prev_rd_req is an auxiliary signal needed by the proc
  proc_common_verify_valid(c_read_rl, clk, verify_en, rd_req, prev_rd_req, rd_val);

  u_dut : entity work.common_fifo_rd
  generic map (
    g_dat_w => c_dat_w
  )
  port map (
    rst        => rst,
    clk        => clk,
    -- ST sink: RL = 1
    fifo_req   => fifo_req,
    fifo_dat   => fifo_dat,
    fifo_val   => fifo_val,
    -- ST source: RL = 0
    rd_req     => rd_req,
    rd_dat     => rd_dat,
    rd_val     => rd_val
  );
end tb;
