-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.mdio_pkg.all;

entity tb_mdio is
end tb_mdio;

architecture tb of tb_mdio is
  constant c_sim        : boolean := true;

  constant clk_period   : time := 10 ns;
  constant c_delay      : natural := 50;

  constant c_st         : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_st;
  constant c_op_addr    : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_addr;
  constant c_op_wr      : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_wr;
  constant c_op_rd      : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_rd;
  constant c_op_rd_incr : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_rd_incr;
  constant c_prtad      : std_logic_vector(4 downto 0) := "00000";  -- port address (example)
  constant c_devad      : std_logic_vector(4 downto 0) := "00001";  -- device address (example)
  constant c_ta         : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_ta;

  constant c_hdr_addr   : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_addr & c_prtad & c_devad & c_ta;
  constant c_hdr_wr     : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_wr   & c_prtad & c_devad & c_ta;
  constant c_hdr_rd     : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_rd   & c_prtad & c_devad & c_ta;

  constant c_phy_addr   : std_logic_vector(c_halfword_w - 1 downto 0) := std_logic_vector(to_unsigned(16#A001#, c_halfword_w));
  constant c_phy_data   : std_logic_vector(c_halfword_w - 1 downto 0) := std_logic_vector(to_unsigned(16#D001#, c_halfword_w));

  signal tb_end             : std_logic := '0';
  signal clk                : std_logic := '1';
  signal rst                : std_logic;

  signal phy_readdata       : std_logic_vector(c_halfword_w - 1 downto 0);

  signal mms_header_address   : std_logic_vector(0 downto 0);
  signal mms_header_write     : std_logic;
  signal mms_header_read      : std_logic;
  signal mms_header_writedata : std_logic_vector(c_halfword_w - 1 downto 0);
  signal mms_header_readdata  : std_logic_vector(c_halfword_w - 1 downto 0);

  signal mms_data_address   : std_logic_vector(0 downto 0);
  signal mms_data_write     : std_logic;
  signal mms_data_read      : std_logic;
  signal mms_data_writedata : std_logic_vector(c_halfword_w - 1 downto 0);
  signal mms_data_readdata  : std_logic_vector(c_halfword_w - 1 downto 0);

  signal ins_mdio_rdy       : std_logic;

  signal mdc          : std_logic;
  signal mdat_in      : std_logic;
  signal mdat_oen     : std_logic;
  signal mdio         : std_logic;

  -- MDIO slave model
  constant c_slave_reg_addr : natural := 16#A001#;
  constant c_slave_reg_data : natural := 16#1234#;

  signal slave_addr     : std_logic_vector(c_halfword_w - 1 downto 0);  -- MDIO access address
  signal slave_reg_addr : std_logic_vector(c_halfword_w - 1 downto 0);  -- the one data register address
  signal slave_reg_dat  : std_logic_vector(c_halfword_w - 1 downto 0);  -- the one data register data value
begin
  -- run 50 us

  clk  <= not clk or tb_end after clk_period / 2;
  rst  <= '1', '0' after clk_period * 3;

  mdio <= 'H';  -- pull up

  -- run 1 us
  p_in_stimuli : process
  begin
    mms_header_address <= (others => '0');  -- = "0"
    mms_header_write <= '0';
    mms_header_read <= '0';
    mms_header_writedata <= (others => '0');

    mms_data_address <= (others => '0');  -- = "0"
    mms_data_write <= '0';
    mms_data_read <= '0';
    mms_data_writedata <= (others => '0');
    wait until rst = '0';
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- write_MMD
    ----------------------------------------------------------------------------
    -- write PHY register address
    mms_data_writedata <= c_phy_addr;  -- first write data (PHY address)
    mms_data_write <= '1';
    wait until rising_edge(clk);
    mms_data_write <= '0';
    mms_header_writedata <= c_hdr_addr;  -- then write header (op=addr), enables the MDIO access
    mms_header_write <= '1';
    wait until rising_edge(clk);
    mms_header_write <= '0';
    while ins_mdio_rdy = '0' loop wait until rising_edge(clk); end loop;
    mms_data_read <= '1';  -- read data to acknowledge MDIO done interrupt
    wait until rising_edge(clk);
    mms_data_read <= '0';
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    -- write PHY register data
    mms_data_writedata <= c_phy_data;  -- first write data (PHY data)
    mms_data_write <= '1';
    wait until rising_edge(clk);
    mms_data_write <= '0';
    mms_header_writedata <= c_hdr_wr;  -- then write header (op=wr), enables the MDIO access
    mms_header_write <= '1';
    wait until rising_edge(clk);
    mms_header_write <= '0';
    while ins_mdio_rdy = '0' loop wait until rising_edge(clk); end loop;
    mms_data_read <= '1';  -- read data to acknowledge the MDIO done interrupt
    wait until rising_edge(clk);
    mms_data_read <= '0';
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- read_MMD
    ----------------------------------------------------------------------------
    -- write PHY register address
    mms_data_writedata <= c_phy_addr;  -- first write data (PHY address)
    mms_data_write <= '1';
    wait until rising_edge(clk);
    mms_data_write <= '0';
    mms_header_writedata <= c_hdr_addr;  -- then write header (op=addr), enables the MDIO access
    mms_header_write <= '1';
    wait until rising_edge(clk);
    mms_header_write <= '0';
    while ins_mdio_rdy = '0' loop wait until rising_edge(clk); end loop;
    mms_data_read <= '1';  -- read data to acknowledge the MDIO done interrupt
    wait until rising_edge(clk);
    mms_data_read <= '0';
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    -- read PHY register data
    mms_header_writedata <= c_hdr_rd;  -- write header (op=rd) to enable the read PHY data access
    mms_header_write <= '1';
    wait until rising_edge(clk);
    mms_header_write <= '0';
    while ins_mdio_rdy = '0' loop wait until rising_edge(clk); end loop;
    mms_data_read <= '1';  -- read data to acknowledge the interrupt
    wait until rising_edge(clk);
    mms_data_read <= '0';
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Check read back value
    ----------------------------------------------------------------------------
    if phy_readdata = c_phy_data then
      report "Write MDIO slave reg and readback went OK"
        severity NOTE;
    else
      report "Write MDIO slave reg and readback went wrong"
        severity ERROR;
    end if;
    tb_end <= '1';
    wait;
  end process;

  phy_readdata <= mms_data_readdata;  -- capture read PHY data

  -- MDIO master
  u_mdio : entity work.mdio
  port map (
    gs_sim              => c_sim,

    rst                 => rst,
    clk                 => clk,

    -- Memory Mapped Slave interface with Interrupt
    mms_header_address  => mms_header_address,
    mms_header_write    => mms_header_write,
    mms_header_read     => mms_header_read,
    mms_header_writedata => mms_header_writedata,
    mms_header_readdata => mms_header_readdata,

    mms_data_address    => mms_data_address,
    mms_data_write      => mms_data_write,
    mms_data_read       => mms_data_read,
    mms_data_writedata  => mms_data_writedata,
    mms_data_readdata   => mms_data_readdata,

    ins_mdio_rdy        => ins_mdio_rdy,

    -- MDIO external interface
    mdc                 => mdc,
    mdat_in             => mdat_in,
    mdat_oen            => mdat_oen
  );

  u_iobuf : entity common_lib.common_inout
  port map (
    dat_inout        => mdio,
    dat_in_from_line => mdat_in,
    dat_out_to_line  => '0',  -- for output '1' rely on external pull up, only
    dat_out_en       => mdat_oen  -- pull low when mdat_oen (so implicitely mdat_out='0')
  );

  -- MDIO slave
  u_slave : entity work.mmd_slave
  generic map (
    g_st        => c_st,
    g_prtad     => c_prtad,
    g_devad     => c_devad,
    g_reg_addr  => c_slave_reg_addr,
    g_reg_dat   => c_slave_reg_data,
    g_reg_w     => c_halfword_w
  )
  port map (
    mdc         => mdc,
    mdio        => mdio,
    addr        => slave_addr,
    reg_dat     => slave_reg_dat
  );

  slave_reg_addr <= std_logic_vector(to_unsigned(c_slave_reg_addr, c_halfword_w));
end tb;
