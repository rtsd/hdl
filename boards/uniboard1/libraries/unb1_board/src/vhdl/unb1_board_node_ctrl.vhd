-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

-- Purpose:
--   Provide the basic node control along with an SOPC Builder system:
--   . sys_rst for sys_clk
--   . pulse every 1 us, 1 ms and 1 s
--   . extend WDI to avoid watchdog reset during SW reload

entity unb1_board_node_ctrl is
  generic (
    g_pulse_us     : natural := 125;  -- nof system clock cycles to get us period, equal to system clock frequency / 10**6
    g_pulse_ms     : natural := 1000;  -- nof pulse_us pulses to get ms period (fixed, use less to speed up simulation)
    g_pulse_s      : natural := 1000;  -- nof pulse_ms pulses to get  s period (fixed, use less to speed up simulation)
    g_wdi_extend_w : natural := 14  -- extend wdi by about 2**(14-1)= 8 s
  );
  port (
    xo_clk          : in  std_logic;  -- from pin, also used as reference for the PLL in the SOPC design
    xo_rst_n        : out std_logic;  -- to SOPC design
    sys_clk         : in  std_logic;  -- system clock from PLL in SOPC design (= mm_clk)
    sys_locked      : in  std_logic;  -- system clock PLL locked from SOPC design
    sys_rst         : out std_logic;  -- system reset released after system clock PLL has locked (= mm_rst)
    cal_clk         : in  std_logic := '0';  -- calibration or configuration interface clock
    cal_rst         : out std_logic;  -- calibration or configuration interface reset, released after sys_rst is released
    st_clk          : in  std_logic := '0';  -- streaming interface clock
    st_rst          : out std_logic;  -- streaming interface reset, released after sys_rst is released
    wdi_in          : in  std_logic;  -- from SW running on the NIOS2 in the SOPC design
    wdi_out         : out std_logic;  -- to FPGA pin
    pulse_us        : out std_logic;  -- pulses every us
    pulse_ms        : out std_logic;  -- pulses every ms
    pulse_s         : out std_logic  -- pulses every s
  );
end unb1_board_node_ctrl;

architecture str of unb1_board_node_ctrl is
  constant c_reset_len   : natural := 4;  -- >= c_meta_delay_len from common_pkg

  signal i_sys_rst   : std_logic;
  signal i_pulse_ms  : std_logic;
begin
  sys_rst <= i_sys_rst;

  pulse_ms <= i_pulse_ms;

  u_unb1_board_clk_rst : entity work.unb1_board_clk_rst
  port map (
    xo_clk     => xo_clk,
    xo_rst_n   => xo_rst_n,
    sys_clk    => sys_clk,
    sys_locked => sys_locked,
    sys_rst    => i_sys_rst  -- release reset some clock cycles after sys_locked went high
  );

  u_common_areset_cal : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => i_sys_rst,  -- release reset some clock cycles after i_sys_rst went low
    clk       => cal_clk,
    out_rst   => cal_rst
  );

  u_common_areset_st : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => i_sys_rst,  -- release reset some clock cycles after i_sys_rst went low
    clk       => st_clk,
    out_rst   => st_rst
  );

  u_common_pulser_us_ms_s : entity common_lib.common_pulser_us_ms_s
  generic map (
    g_pulse_us  => g_pulse_us,
    g_pulse_ms  => g_pulse_ms,
    g_pulse_s   => g_pulse_s
  )
  port map (
    rst         => i_sys_rst,
    clk         => sys_clk,
    pulse_us    => pulse_us,
    pulse_ms    => i_pulse_ms,
    pulse_s     => pulse_s
  );

  u_unb1_board_wdi_extend : entity work.unb1_board_wdi_extend
  generic map (
    g_extend_w => g_wdi_extend_w
  )
  port map (
    rst        => i_sys_rst,
    clk        => sys_clk,
    pulse_ms   => i_pulse_ms,
    wdi_in     => wdi_in,
    wdi_out    => wdi_out
  );
end str;
