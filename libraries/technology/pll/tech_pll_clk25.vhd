-------------------------------------------------------------------------------
--
-- Copyright (C) 2014-2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_pll_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_arria10_pll_clk25_altera_iopll_150;
library ip_stratixiv_pll_clk25_lib;
library ip_arria10_e3sge3_pll_clk25_altera_iopll_151;
library ip_arria10_e1sg_pll_clk25_altera_iopll_180;
library ip_arria10_e2sg_pll_clk25_altera_iopll_1930;

entity tech_pll_clk25 is
  generic (
    g_technology       : natural := c_tech_select_default
  );
  port (
    areset  : in std_logic  := '0';
    inclk0  : in std_logic  := '0';
    c0      : out std_logic;
    c1      : out std_logic;
    c2      : out std_logic;
    c3      : out std_logic;
    c4      : out std_logic;
    locked  : out std_logic
  );
end tech_pll_clk25;

architecture str of tech_pll_clk25 is
begin
  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_pll_clk25
    port map (
      rst      => areset,
      refclk   => inclk0,
      outclk_0 => c0,
      outclk_1 => c1,
      outclk_2 => c2,
      outclk_3 => c3,
      locked   => locked
    );
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_pll_clk25
    port map (
      rst      => areset,
      refclk   => inclk0,
      outclk_0 => c0,
      outclk_1 => c1,
      outclk_2 => c2,
      outclk_3 => c3,
      locked   => locked
    );
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_pll_clk25
    port map (
      rst      => areset,
      refclk   => inclk0,
      outclk_0 => c0,
      outclk_1 => c1,
      outclk_2 => c2,
      outclk_3 => c3,
      locked   => locked
    );
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_pll_clk25
    port map (
      rst      => areset,
      refclk   => inclk0,
      outclk_0 => c0,
      outclk_1 => c1,
      outclk_2 => c2,
      outclk_3 => c3,
      locked   => locked
    );
  end generate;

  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_pll_clk25
    port map (
      areset => areset,
      inclk0 => inclk0,
      c0     => c0,
      c1     => c1,
      c2     => c2,
      c3     => c3,
      c4     => c4,
      locked => locked
    );
  end generate;
end architecture;
