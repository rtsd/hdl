--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity common_flank_to_pulse is
  port (
    clk                : in std_logic;
    rst                : in std_logic;
    flank_in           : in std_logic;
    pulse_out          : out std_logic
  );
end common_flank_to_pulse;

architecture str of common_flank_to_pulse is
  signal flank_in_dly : std_logic;
begin
 p_in_dly : process(rst, clk)
  begin
    if rst = '1' then
      flank_in_dly  <= '0';
    elsif rising_edge(clk) then
      flank_in_dly <= flank_in;
    end if;
  end process;

  pulse_out <= flank_in and not(flank_in_dly);
end str;
