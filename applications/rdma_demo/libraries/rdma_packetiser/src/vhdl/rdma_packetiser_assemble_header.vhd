-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Assembles the RDMA header at snk_in.sop and inserts it in front of
-- the incoming data stream
-- Description:
-- Generates a RoCEv2 header (ETH + UDP + IP + RDMA). See [1]. Then it prepends
-- the generated header to the incoming sosi data stream.
-- MM config:
-- . config_use_immediate: When true, the immediate data field will be added to the
--   header.
-- . config_use_msg_cnt_as_immediate: When true, the message counter going from 0 to
--   "nof_msg" is used as immediate data. When false, the "immediate_data"
--   input port is used.
-- . immediate_data: Will be used as immediate data when
--   config_use_msg_cnt_as_immediate = False.
-- . config_nof_packets_in_msg: Should be set to the desired amount of packets in a message.
-- . config_nof_msg: Should be set to the desired amount of messages, this determines the
--   address space aswell, see remarks.
-- . reth_dma_len: The amount with which the address should increase every message,
--   this should be set to >= nof_packets_in_msg * block_len.
-- . config_start_address: The start address for the virtual_address field.
-- Signal inputs:
-- . block_len: Should be set to the length of the incoming data frame in octets.
-- Remarks
-- . The virtual_address is set automatically by increasing it with dma_len every
--   new message. The virtual address is reset to "start_address" when the number
--   of messages has reached "nof_msg". Then the cycle repeats.
-- . The PSN field (= Packet Sequence Number) is set to LSBs of the incoming BSN.
--   This can be used to check the order or detect missing packets at the receiver.
-- . The incoming datastream has to be at least 6 valid cycles long and be able to handle
--   backpressure. Using less than 6 cycles seem to result in corrupted packets. It seems
--   that it has to do with dp_offload_tx_v3 not capable of processing fast enough, further
--   investigation is necessary in order to get rid of this limitation.

-- References:
-- . [1] https://support.astron.nl/confluence/x/3pKrB

library IEEE, common_lib, dp_lib, eth_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use common_lib.common_field_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use dp_lib.dp_components_pkg.all;
  use eth_lib.eth_pkg.all;
  use eth_lib.eth_tester_pkg.all;
  use work.rdma_packetiser_pkg.all;

entity rdma_packetiser_assemble_header is
  generic (
    g_data_w : natural := 512
  );
  port (
    -- Clocks and reset
    dp_clk             : in  std_logic;
    dp_rst             : in  std_logic;

    mm_clk             : in  std_logic;
    mm_rst             : in  std_logic;

    reg_hdr_dat_copi   : in  t_mem_copi := c_mem_copi_rst;
    reg_hdr_dat_cipo   : out t_mem_cipo;

    snk_in             : in  t_dp_sosi := c_dp_sosi_rst;
    snk_out            : out t_dp_siso := c_dp_siso_rdy;

    src_out            : out t_dp_sosi := c_dp_sosi_rst;
    src_in             : in  t_dp_siso := c_dp_siso_rdy;

    block_len          : in  std_logic_vector(c_halfword_w - 1 downto 0); -- in octets

    eth_hdr            : out std_logic_vector(1023 downto 0) := (others => '0')

  );
end rdma_packetiser_assemble_header;

architecture str of rdma_packetiser_assemble_header is
  -- Created constants for each header length variant, also included the icrc length here.
  -- first = rdma_write_first and rdma_write_only without immediate.
  -- mid   = rdma_write_middle and rdma_write_last without immediate.
  -- last  = rdma_write_last with immediate.
  -- wo    = rdma_write_only with immediate.
  constant c_udp_app_first_hdr_len    : natural := c_network_udp_header_len + c_rdma_packetiser_bth_len + c_rdma_packetiser_reth_len + c_rdma_packetiser_icrc_len;
  constant c_udp_app_mid_hdr_len      : natural := c_network_udp_header_len + c_rdma_packetiser_bth_len + c_rdma_packetiser_icrc_len;
  constant c_udp_app_last_hdr_len     : natural := c_network_udp_header_len + c_rdma_packetiser_bth_len + c_rdma_packetiser_imm_len  + c_rdma_packetiser_icrc_len;
  constant c_udp_app_wo_hdr_len       : natural := c_network_udp_header_len + c_rdma_packetiser_bth_len + c_rdma_packetiser_reth_len + c_rdma_packetiser_imm_len + c_rdma_packetiser_icrc_len;
  constant c_ip_udp_app_first_hdr_len : natural := c_network_ip_header_len + c_udp_app_first_hdr_len;
  constant c_ip_udp_app_mid_hdr_len   : natural := c_network_ip_header_len + c_udp_app_mid_hdr_len;
  constant c_ip_udp_app_last_hdr_len  : natural := c_network_ip_header_len + c_udp_app_last_hdr_len;
  constant c_ip_udp_app_wo_hdr_len    : natural := c_network_ip_header_len + c_udp_app_wo_hdr_len;
  constant c_nof_offload           : natural := 4;

  type t_state is (s_first, s_middle, s_last);
  type t_reg is record -- record to keep the registers organized.
    state              : t_state;
    opcode             : std_logic_vector(c_byte_w - 1 downto 0);
    psn                : std_logic_vector(c_word_w - 1 downto 0);
    virtual_address    : unsigned(c_longword_w - 1 downto 0);
    dma_len            : unsigned(c_word_w - 1 downto 0);
    p_cnt              : natural; -- Packet count (0 to nof_packets_in_msg).
    msg_cnt            : natural; -- message count (0 to nof_msg).
    udp_total_length   : natural;
    ip_total_length    : natural;
    nof_packets_in_msg : natural;
    sel_ctrl           : natural range 0 to c_nof_offload - 1;
    sel_ctrl_delayed   : natural range 0 to c_nof_offload - 1;
  end record;

  constant c_reg_rst : t_reg := (s_first, (others => '1'), (others => '0'), (others => '0'), (others => '0'), 0, 0, 0, 0, 0, 0, 0);
  signal d, q : t_reg;

  signal use_immediate                : std_logic;
  signal use_msg_cnt_as_immediate     : std_logic;
  signal immediate_data               : std_logic_vector(c_rdma_packetiser_imm_len * c_octet_w - 1 downto 0) := (others => '0');
  signal nof_packets_in_msg           : std_logic_vector(c_word_w - 1 downto 0);
  signal nof_msg                      : std_logic_vector(c_word_w - 1 downto 0);
  signal dma_len                      : std_logic_vector(c_word_w - 1 downto 0); -- = block_len * nof_packets_in_msg
  signal start_address                : std_logic_vector(c_longword_w - 1 downto 0);

  signal hdr_fields_slv_out_mm        : std_logic_vector(1023 downto 0) := (others => '0');
  signal hdr_fields_slv_in            : std_logic_vector(1023 downto 0) := (others => '0');
  signal hdr_fields_slv_in_first      : std_logic_vector(1023 downto 0) := (others => '0');
  signal hdr_fields_slv_in_mid        : std_logic_vector(1023 downto 0) := (others => '0');
  signal hdr_fields_slv_in_last       : std_logic_vector(1023 downto 0) := (others => '0');
  signal hdr_fields_slv_in_wo         : std_logic_vector(1023 downto 0) := (others => '0');

  signal dp_demux_src_out_arr         : t_dp_sosi_arr(c_nof_offload - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_demux_src_in_arr          : t_dp_siso_arr(c_nof_offload - 1 downto 0) := (others => c_dp_siso_rdy);
  signal dp_mux_snk_in_arr            : t_dp_sosi_arr(c_nof_offload - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_mux_snk_out_arr           : t_dp_siso_arr(c_nof_offload - 1 downto 0) := (others => c_dp_siso_rdy);
  signal eth_ip_offload_first_snk_in  : t_dp_sosi := c_dp_sosi_rst;
  signal eth_ip_offload_first_snk_out : t_dp_siso := c_dp_siso_rdy;
  signal eth_ip_offload_first_src_out : t_dp_sosi := c_dp_sosi_rst;
  signal eth_ip_offload_first_src_in  : t_dp_siso := c_dp_siso_rdy;
  signal eth_ip_offload_mid_snk_in    : t_dp_sosi := c_dp_sosi_rst;
  signal eth_ip_offload_mid_snk_out   : t_dp_siso := c_dp_siso_rdy;
  signal eth_ip_offload_mid_src_out   : t_dp_sosi := c_dp_sosi_rst;
  signal eth_ip_offload_mid_src_in    : t_dp_siso := c_dp_siso_rdy;
  signal eth_ip_offload_last_snk_in   : t_dp_sosi := c_dp_sosi_rst;
  signal eth_ip_offload_last_snk_out  : t_dp_siso := c_dp_siso_rdy;
  signal eth_ip_offload_last_src_out  : t_dp_sosi := c_dp_sosi_rst;
  signal eth_ip_offload_last_src_in   : t_dp_siso := c_dp_siso_rdy;
  signal eth_ip_offload_wo_snk_in     : t_dp_sosi := c_dp_sosi_rst;
  signal eth_ip_offload_wo_snk_out    : t_dp_siso := c_dp_siso_rdy;
  signal eth_ip_offload_wo_src_out    : t_dp_sosi := c_dp_sosi_rst;
  signal eth_ip_offload_wo_src_in     : t_dp_siso := c_dp_siso_rdy;
  signal dp_pipeline_src_out          : t_dp_sosi := c_dp_sosi_rst;

begin
  immediate_data           <=    hdr_fields_slv_out_mm(field_hi(c_rdma_packetiser_mm_field_arr, "immediate_data"                 ) downto field_lo(c_rdma_packetiser_mm_field_arr, "immediate_data"                 ));
  use_immediate            <= sl(hdr_fields_slv_out_mm(field_hi(c_rdma_packetiser_mm_field_arr, "config_use_immediate"           ) downto field_lo(c_rdma_packetiser_mm_field_arr, "config_use_immediate"           )));
  use_msg_cnt_as_immediate <= sl(hdr_fields_slv_out_mm(field_hi(c_rdma_packetiser_mm_field_arr, "config_use_msg_cnt_as_immediate") downto field_lo(c_rdma_packetiser_mm_field_arr, "config_use_msg_cnt_as_immediate")));
  nof_packets_in_msg       <=    hdr_fields_slv_out_mm(field_hi(c_rdma_packetiser_mm_field_arr, "config_nof_packets_in_msg"      ) downto field_lo(c_rdma_packetiser_mm_field_arr, "config_nof_packets_in_msg"      ));
  nof_msg                  <=    hdr_fields_slv_out_mm(field_hi(c_rdma_packetiser_mm_field_arr, "config_nof_msg"                 ) downto field_lo(c_rdma_packetiser_mm_field_arr, "config_nof_msg"                 ));
  dma_len                  <=    hdr_fields_slv_out_mm(field_hi(c_rdma_packetiser_mm_field_arr, "reth_dma_length"                ) downto field_lo(c_rdma_packetiser_mm_field_arr, "reth_dma_length"                ));
  start_address            <=    hdr_fields_slv_out_mm(field_hi(c_rdma_packetiser_mm_field_arr, "config_start_address"           ) downto field_lo(c_rdma_packetiser_mm_field_arr, "config_start_address"           ));

  -- State machine to derive RDMA header fields.
  q <= d when rising_edge(dp_clk);
  p_comb : process(dp_rst, q, snk_in, nof_packets_in_msg, start_address, nof_msg, immediate_data, dma_len, block_len, use_immediate)
    variable v : t_reg;
  begin
    v := q;
    v.sel_ctrl_delayed := q.sel_ctrl;
    if snk_in.sop = '1' then
      v.psn := resize_uvec(snk_in.bsn, c_word_w);
      v.p_cnt := q.p_cnt + 1;
      case q.state is
        when s_first => -- wait to start a new message and set the first opcode.
          v.p_cnt := 1;
          if q.p_cnt >= v.nof_packets_in_msg then -- (re)set message counter and virtual address.
            if q.msg_cnt >= to_uint(nof_msg) - 1 then
              v.msg_cnt := 0;
            else
              v.msg_cnt := q.msg_cnt + 1;
              v.virtual_address := q.virtual_address + q.dma_len;
            end if;
          end if;

          if v.nof_packets_in_msg = 1 then -- set opcode to write_only.
            v.opcode := c_rdma_packetiser_opcode_uc_write_only;
            v.udp_total_length := c_udp_app_first_hdr_len + to_uint(block_len);
            v.ip_total_length  := c_ip_udp_app_first_hdr_len + to_uint(block_len);
            v.sel_ctrl := 0;
            if use_immediate = '1' then  -- set opcode to write_only with immediate data.
              v.opcode := c_rdma_packetiser_opcode_uc_write_only_imm;
              v.udp_total_length := c_udp_app_wo_hdr_len + to_uint(block_len);
              v.ip_total_length  := c_ip_udp_app_wo_hdr_len + to_uint(block_len);
              v.sel_ctrl := 3;
            end if;
          elsif v.nof_packets_in_msg = 2 then -- set opcode to write_first.
            v.state := s_last; -- next state is last as there are only 2 packets.
            v.opcode := c_rdma_packetiser_opcode_uc_write_first;
            v.udp_total_length := c_udp_app_first_hdr_len + to_uint(block_len);
            v.ip_total_length  := c_ip_udp_app_first_hdr_len + to_uint(block_len);
            v.sel_ctrl := 0;
          elsif v.nof_packets_in_msg > 2 then
            v.state := s_middle;
            v.opcode := c_rdma_packetiser_opcode_uc_write_first;
            v.udp_total_length := c_udp_app_first_hdr_len + to_uint(block_len);
            v.ip_total_length  := c_ip_udp_app_first_hdr_len + to_uint(block_len);
            v.sel_ctrl := 0;
          end if;

        when s_middle => -- wait unitl the first packet is done and set next opcode.
          v.opcode := c_rdma_packetiser_opcode_uc_write_middle;
          v.udp_total_length := c_udp_app_mid_hdr_len + to_uint(block_len);
          v.ip_total_length  := c_ip_udp_app_mid_hdr_len + to_uint(block_len);
          v.sel_ctrl := 1;
          if q.p_cnt >= v.nof_packets_in_msg - 2 then -- wait until last middle packet
            v.state := s_last;
          end if;

        when s_last => -- next packet must be last packet, set opcode.
          v.state := s_first;
          v.opcode := c_rdma_packetiser_opcode_uc_write_last;
          v.udp_total_length := c_udp_app_mid_hdr_len + to_uint(block_len);
          v.ip_total_length  := c_ip_udp_app_mid_hdr_len + to_uint(block_len);
          v.sel_ctrl := 1;
          if use_immediate = '1' then -- set opcode to write_last with immediate data
            v.opcode := c_rdma_packetiser_opcode_uc_write_last_imm;
            v.udp_total_length := c_udp_app_last_hdr_len + to_uint(block_len);
            v.ip_total_length  := c_ip_udp_app_last_hdr_len + to_uint(block_len);
            v.sel_ctrl := 2;
          end if;
      end case;
    end if;

    if v.msg_cnt = 0 then -- set on new message
      v.virtual_address    := unsigned(start_address);
      v.dma_len            := unsigned(dma_len);
      v.nof_packets_in_msg := to_uint(nof_packets_in_msg);
    end if;

    if dp_rst = '1' then
      v := c_reg_rst;
    end if;

    d <= v;
  end process;

  -------------------------------------------------------------------------------
  -- Wire the header fields
  -------------------------------------------------------------------------------
  hdr_fields_slv_in(field_hi(c_rdma_packetiser_mm_field_arr, "ip_total_length"     ) downto field_lo(c_rdma_packetiser_mm_field_arr, "ip_total_length"     )) <= TO_UVEC(q.ip_total_length, 16);
  hdr_fields_slv_in(field_hi(c_rdma_packetiser_mm_field_arr, "udp_total_length"    ) downto field_lo(c_rdma_packetiser_mm_field_arr, "udp_total_length"    )) <= TO_UVEC(q.udp_total_length, 16);
  hdr_fields_slv_in(field_hi(c_rdma_packetiser_mm_field_arr, "bth_opcode"          ) downto field_lo(c_rdma_packetiser_mm_field_arr, "bth_opcode"          )) <= q.opcode;
  hdr_fields_slv_in(field_hi(c_rdma_packetiser_mm_field_arr, "bth_psn"             ) downto field_lo(c_rdma_packetiser_mm_field_arr, "bth_psn"             )) <= q.psn;
  hdr_fields_slv_in(field_hi(c_rdma_packetiser_mm_field_arr, "reth_virtual_address") downto field_lo(c_rdma_packetiser_mm_field_arr, "reth_virtual_address")) <= std_logic_vector(q.virtual_address);
  hdr_fields_slv_in(field_hi(c_rdma_packetiser_mm_field_arr, "reth_dma_length"     ) downto field_lo(c_rdma_packetiser_mm_field_arr, "reth_dma_length"     )) <= std_logic_vector(q.dma_len);

  -------------------------------------------------------------------------------
  -- demux to guide the incoming stream to the correct eth_ip_offload_tx
  -------------------------------------------------------------------------------
  u_dp_demux : entity dp_lib.dp_demux
  generic map (
    g_mode            => 2,
    g_nof_output      => c_nof_offload,
    g_combined        => false,
    g_sel_ctrl_invert => true,
    g_sel_ctrl_pkt    => true
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,

    sel_ctrl    => q.sel_ctrl,

    snk_in      => snk_in,
    snk_out     => snk_out,

    src_out_arr => dp_demux_src_out_arr,
    src_in_arr  => dp_demux_src_in_arr
  );

  -- Wire demux outputs to dp_offload inputs.
  eth_ip_offload_first_snk_in <= dp_demux_src_out_arr(0);
  eth_ip_offload_mid_snk_in   <= dp_demux_src_out_arr(1);
  eth_ip_offload_last_snk_in  <= dp_demux_src_out_arr(2);
  eth_ip_offload_wo_snk_in    <= dp_demux_src_out_arr(3);
  dp_demux_src_in_arr(0)  <= eth_ip_offload_first_snk_out;
  dp_demux_src_in_arr(1)  <= eth_ip_offload_mid_snk_out;
  dp_demux_src_in_arr(2)  <= eth_ip_offload_last_snk_out;
  dp_demux_src_in_arr(3)  <= eth_ip_offload_wo_snk_out;

  -- Wire header fields for every eth_ip_offload_tx
  p_wire_headers : process(hdr_fields_slv_out_mm, use_msg_cnt_as_immediate, q)
  begin
    -- set headers.
    eth_hdr                 <= field_select_subset(c_rdma_packetiser_eth_hdr_field_arr,
                                                   c_rdma_packetiser_mm_field_arr,
                                                   hdr_fields_slv_out_mm);
    hdr_fields_slv_in_first <= field_select_subset(c_rdma_packetiser_first_hdr_field_arr,
                                                   c_rdma_packetiser_mm_field_arr,
                                                   hdr_fields_slv_out_mm);
    hdr_fields_slv_in_mid   <= field_select_subset(c_rdma_packetiser_mid_hdr_field_arr,
                                                   c_rdma_packetiser_mm_field_arr,
                                                   hdr_fields_slv_out_mm);
    hdr_fields_slv_in_last  <= field_select_subset(c_rdma_packetiser_last_hdr_field_arr,
                                                   c_rdma_packetiser_mm_field_arr,
                                                   hdr_fields_slv_out_mm);
    hdr_fields_slv_in_wo    <= field_select_subset(c_rdma_packetiser_wo_hdr_field_arr,
                                                   c_rdma_packetiser_mm_field_arr,
                                                   hdr_fields_slv_out_mm);

    if use_msg_cnt_as_immediate = '1' then -- set immediate data to msg_cnt when use_msg_cnt_as_immediate = '1'
      hdr_fields_slv_in_last(field_hi(c_rdma_packetiser_last_hdr_field_arr, "immediate_data") downto
                             field_lo(c_rdma_packetiser_last_hdr_field_arr, "immediate_data")) <= TO_UVEC(q.msg_cnt, 32);
      hdr_fields_slv_in_wo(  field_hi(c_rdma_packetiser_wo_hdr_field_arr,   "immediate_data") downto
                             field_lo(c_rdma_packetiser_wo_hdr_field_arr,   "immediate_data")) <= TO_UVEC(q.msg_cnt, 32);
    end if;
  end process;
  -------------------------------------------------------------------------------
  -- Header for first packets or write only without immediate data
  -------------------------------------------------------------------------------
  u_eth_ip_offload_first : entity eth_lib.eth_ip_offload_tx
  generic map (
    g_nof_streams    => 1,
    g_data_w         => g_data_w,
    g_symbol_w       => c_octet_w,
    g_hdr_field_arr  => c_rdma_packetiser_first_hdr_field_arr,
    g_hdr_field_sel  => c_rdma_packetiser_first_hdr_field_sel,
    g_pipeline_ready => true
  )
  port map (
    dp_rst                => dp_rst,
    dp_clk                => dp_clk,
    snk_in_arr(0)         => eth_ip_offload_first_snk_in,
    snk_out_arr(0)        => eth_ip_offload_first_snk_out,
    src_out_arr(0)        => eth_ip_offload_first_src_out,
    src_in_arr(0)         => eth_ip_offload_first_src_in,
    hdr_fields_in_arr(0)  => hdr_fields_slv_in_first
  );

  -------------------------------------------------------------------------------
  -- Header for middle or last without immediate data (no RETH, no immediate data)
  -------------------------------------------------------------------------------
  u_eth_ip_offload_mid : entity eth_lib.eth_ip_offload_tx
  generic map (
    g_nof_streams    => 1,
    g_data_w         => g_data_w,
    g_symbol_w       => c_octet_w,
    g_hdr_field_arr  => c_rdma_packetiser_mid_hdr_field_arr,
    g_hdr_field_sel  => c_rdma_packetiser_mid_hdr_field_sel,
    g_pipeline_ready => true
  )
  port map (
    dp_rst                => dp_rst,
    dp_clk                => dp_clk,
    snk_in_arr(0)         => eth_ip_offload_mid_snk_in,
    snk_out_arr(0)        => eth_ip_offload_mid_snk_out,
    src_out_arr(0)        => eth_ip_offload_mid_src_out,
    src_in_arr(0)         => eth_ip_offload_mid_src_in,
    hdr_fields_in_arr(0)  => hdr_fields_slv_in_mid
  );

  -------------------------------------------------------------------------------
  -- Header for last packets with immediate data
  -------------------------------------------------------------------------------
  u_eth_ip_offload_last : entity eth_lib.eth_ip_offload_tx
  generic map (
    g_nof_streams    => 1,
    g_data_w         => g_data_w,
    g_symbol_w       => c_octet_w,
    g_hdr_field_arr  => c_rdma_packetiser_last_hdr_field_arr,
    g_hdr_field_sel  => c_rdma_packetiser_last_hdr_field_sel,
    g_pipeline_ready => true
  )
  port map (
    dp_rst                => dp_rst,
    dp_clk                => dp_clk,
    snk_in_arr(0)         => eth_ip_offload_last_snk_in,
    snk_out_arr(0)        => eth_ip_offload_last_snk_out,
    src_out_arr(0)        => eth_ip_offload_last_src_out,
    src_in_arr(0)         => eth_ip_offload_last_src_in,
    hdr_fields_in_arr(0)  => hdr_fields_slv_in_last
  );

  -------------------------------------------------------------------------------
  -- Header for write only packets with immediate data
  -------------------------------------------------------------------------------
  u_eth_ip_offload_wo : entity eth_lib.eth_ip_offload_tx
  generic map (
    g_nof_streams    => 1,
    g_data_w         => g_data_w,
    g_symbol_w       => c_octet_w,
    g_hdr_field_arr  => c_rdma_packetiser_wo_hdr_field_arr,
    g_hdr_field_sel  => c_rdma_packetiser_wo_hdr_field_sel,
    g_pipeline_ready => true
  )
  port map (
    dp_rst                => dp_rst,
    dp_clk                => dp_clk,
    snk_in_arr(0)         => eth_ip_offload_wo_snk_in,
    snk_out_arr(0)        => eth_ip_offload_wo_snk_out,
    src_out_arr(0)        => eth_ip_offload_wo_src_out,
    src_in_arr(0)         => eth_ip_offload_wo_src_in,
    hdr_fields_in_arr(0)  => hdr_fields_slv_in_wo
  );

  -------------------------------------------------------------------------------
  -- Using extra dp_offload_tx_v3 only for MM that contains all headers + config register
  -------------------------------------------------------------------------------
  -- DP pipeline to correct for state machine latency
  u_dp_pipeline : entity dp_lib.dp_pipeline
  port map (
    rst => dp_rst,
    clk => dp_clk,
    snk_in  => snk_in,
    src_out => dp_pipeline_src_out
  );

  -- dp_offload_tx_v3
  u_dp_offload_tx : entity dp_lib.dp_offload_tx_v3
  generic map (
    g_nof_streams    => 1,
    g_data_w         => g_data_w,
    g_symbol_w       => c_octet_w,
    g_hdr_field_arr  => c_rdma_packetiser_mm_field_arr,
    g_hdr_field_sel  => c_rdma_packetiser_mm_field_sel,
    g_pipeline_ready => true
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,
    dp_rst                => dp_rst,
    dp_clk                => dp_clk,
    reg_hdr_dat_mosi      => reg_hdr_dat_copi,
    reg_hdr_dat_miso      => reg_hdr_dat_cipo,
    snk_in_arr(0)         => dp_pipeline_src_out,
    hdr_fields_in_arr(0)  => hdr_fields_slv_in,
    hdr_fields_out_arr(0) => hdr_fields_slv_out_mm
  );

  -------------------------------------------------------------------------------
  -- Mux to merge the packets from the different eth_ip_offload_tx
  -------------------------------------------------------------------------------
  -- Wire demux outputs to dp_offload inputs.
  dp_mux_snk_in_arr(0)    <= eth_ip_offload_first_src_out;
  dp_mux_snk_in_arr(1)    <= eth_ip_offload_mid_src_out;
  dp_mux_snk_in_arr(2)    <= eth_ip_offload_last_src_out;
  dp_mux_snk_in_arr(3)    <= eth_ip_offload_wo_src_out;
  eth_ip_offload_first_src_in <= dp_mux_snk_out_arr(0);
  eth_ip_offload_mid_src_in   <= dp_mux_snk_out_arr(1);
  eth_ip_offload_last_src_in  <= dp_mux_snk_out_arr(2);
  eth_ip_offload_wo_src_in    <= dp_mux_snk_out_arr(3);

  u_dp_mux : entity dp_lib.dp_mux
  generic map (
    g_mode              => 2,
    g_nof_input         => c_nof_offload,
    g_append_channel_lo => false,
    g_sel_ctrl_invert   => true,
    g_use_fifo          => false, -- fifo is not needed as the inputs cannot occur simultaneously.
    g_fifo_size         => array_init(0, c_nof_offload),
    g_fifo_fill         => array_init(0, c_nof_offload)
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,
    sel_ctrl    => q.sel_ctrl_delayed,
    snk_in_arr  => dp_mux_snk_in_arr,
    snk_out_arr => dp_mux_snk_out_arr,
    src_out     => src_out,
    src_in      => src_in
  );
end str;
