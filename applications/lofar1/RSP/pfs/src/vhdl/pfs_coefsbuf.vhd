-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: Polyphase FIR filter
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_mem_pkg.all;

entity pfs_coefsbuf is
  generic (
    g_data_w     : natural;
    g_coefs_file : string  := "data/pfs_coefsbuf_1024.hex";  -- Quartus .hex extension, replaced by .bin in common_rom works for XST
    --g_coefs_file : STRING  := "data/pfs_coefsbuf_1024.bin";  -- Synplify fails on file extension change to .bin in common_rom and requires extra ../
    g_nof_coefs  : natural;
    g_addr_w     : natural
  );
  port (
    addr        : in  std_logic_vector(g_addr_w - 1 downto 0);
    data        : out std_logic_vector(g_data_w - 1 downto 0);
    clk         : in  std_logic;
    rst         : in  std_logic
  );
end pfs_coefsbuf;

architecture str of pfs_coefsbuf is
  constant c_coefs_rom   : t_c_mem := (latency  => 2,
                                       adr_w    => g_addr_w,
                                       dat_w    => g_data_w,
                                       nof_dat  => g_nof_coefs,  -- <= 2**g_addr_w
                                       init_sl  => '0');
begin
  rom : entity common_lib.common_rom
  generic map (
    g_ram             => c_coefs_rom,
    g_init_file       => g_coefs_file
  )
  port map (
    rst               => rst,
    clk               => clk,
    rd_adr            => addr,
    rd_dat            => data
  );
end str;
