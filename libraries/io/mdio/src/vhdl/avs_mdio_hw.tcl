# TCL File Generated by Component Editor 9.1sp1
# Wed Feb 17 16:36:46 CET 2010
# DO NOT MODIFY


# +-----------------------------------
# | 
# | avs_mdio "avs_mdio" v1.0
# | null 2010.02.17.16:36:45
# | MDIO PHY access
# | 
# | D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/Lofar/mdio/src/vhdl/avs_mdio.vhd
# | 
# |    ./D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/common/src/vhdl/common_pkg.vhd syn, sim
# |    ./D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/common/src/vhdl/common_evt.vhd syn, sim
# |    ./D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/common/src/vhdl/common_inout.vhd syn, sim
# |    ./D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/common/src/vhdl/common_mem_pkg.vhd syn, sim
# |    ./D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/common/src/vhdl/common_reg_r_w.vhd syn, sim
# |    ./D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/common/src/vhdl/common_request.vhd syn, sim
# |    ./D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/common/src/vhdl/common_switch.vhd syn, sim
# |    ./D:/svnroot/UniBoard_FP7/UniBoard/trunk/Firmware/modules/common/src/vhdl/common_pipeline.vhd syn, sim
# |    ./avs_mdio.vhd syn, sim
# |    ./mdio_pkg.vhd syn, sim
# |    ./mdio.vhd syn, sim
# |    ./mdio_mm.vhd syn, sim
# |    ./mdio_phy.vhd syn, sim
# | 
# +-----------------------------------

# +-----------------------------------
# | request TCL package from ACDS 9.1
# | 
package require -exact sopc 9.1
# | 
# +-----------------------------------

# +-----------------------------------
# | module avs_mdio
# | 
set_module_property DESCRIPTION "MDIO PHY access"
set_module_property NAME avs_mdio
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property GROUP Uniboard
set_module_property DISPLAY_NAME avs_mdio
set_module_property TOP_LEVEL_HDL_FILE avs_mdio.vhd
set_module_property TOP_LEVEL_HDL_MODULE avs_mdio
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property ANALYZE_HDL TRUE
# | 
# +-----------------------------------

# +-----------------------------------
# | files
# | 
add_file ../../../../common/src/vhdl/common_pkg.vhd {SYNTHESIS SIMULATION}
add_file ../../../../common/src/vhdl/common_evt.vhd {SYNTHESIS SIMULATION}
add_file ../../../../common/src/vhdl/common_inout.vhd {SYNTHESIS SIMULATION}
add_file ../../../../common/src/vhdl/common_mem_pkg.vhd {SYNTHESIS SIMULATION}
add_file ../../../../common/src/vhdl/common_reg_r_w.vhd {SYNTHESIS SIMULATION}
add_file ../../../../common/src/vhdl/common_request.vhd {SYNTHESIS SIMULATION}
add_file ../../../../common/src/vhdl/common_switch.vhd {SYNTHESIS SIMULATION}
add_file ../../../../common/src/vhdl/common_pipeline.vhd {SYNTHESIS SIMULATION}
add_file avs_mdio.vhd {SYNTHESIS SIMULATION}
add_file mdio_pkg.vhd {SYNTHESIS SIMULATION}
add_file mdio.vhd {SYNTHESIS SIMULATION}
add_file mdio_mm.vhd {SYNTHESIS SIMULATION}
add_file mdio_phy.vhd {SYNTHESIS SIMULATION}
# | 
# +-----------------------------------

# +-----------------------------------
# | parameters
# | 
add_parameter g_mdc_period NATURAL 256
set_parameter_property g_mdc_period DEFAULT_VALUE 256
set_parameter_property g_mdc_period DISPLAY_NAME g_mdc_period
set_parameter_property g_mdc_period UNITS None
set_parameter_property g_mdc_period DISPLAY_HINT ""
set_parameter_property g_mdc_period AFFECTS_GENERATION false
set_parameter_property g_mdc_period HDL_PARAMETER true
add_parameter g_hold_time NATURAL 10
set_parameter_property g_hold_time DEFAULT_VALUE 10
set_parameter_property g_hold_time DISPLAY_NAME g_hold_time
set_parameter_property g_hold_time UNITS None
set_parameter_property g_hold_time DISPLAY_HINT ""
set_parameter_property g_hold_time AFFECTS_GENERATION false
set_parameter_property g_hold_time HDL_PARAMETER true
add_parameter g_setup_time NATURAL 2
set_parameter_property g_setup_time DEFAULT_VALUE 2
set_parameter_property g_setup_time DISPLAY_NAME g_setup_time
set_parameter_property g_setup_time UNITS None
set_parameter_property g_setup_time DISPLAY_HINT ""
set_parameter_property g_setup_time AFFECTS_GENERATION false
set_parameter_property g_setup_time HDL_PARAMETER true
# | 
# +-----------------------------------

# +-----------------------------------
# | display items
# | 
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point gs_sim
# | 
add_interface gs_sim conduit end

set_interface_property gs_sim ENABLED true

add_interface_port gs_sim coe_gs_sim_export export Input 1
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point mdio_phy_mdc
# | 
add_interface mdio_phy_mdc conduit end

set_interface_property mdio_phy_mdc ENABLED true

add_interface_port mdio_phy_mdc coe_mdio_phy_mdc_export export Output 1
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point mdio_phy_mdat_in
# | 
add_interface mdio_phy_mdat_in conduit end

set_interface_property mdio_phy_mdat_in ENABLED true

add_interface_port mdio_phy_mdat_in coe_mdio_phy_mdat_in_export export Input 1
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point mdio_phy_mdat_oen
# | 
add_interface mdio_phy_mdat_oen conduit end

set_interface_property mdio_phy_mdat_oen ENABLED true

add_interface_port mdio_phy_mdat_oen coe_mdio_phy_mdat_oen_export export Output 1
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point system
# | 
add_interface system clock end

set_interface_property system ENABLED true

add_interface_port system csi_system_reset reset Input 1
add_interface_port system csi_system_clk clk Input 1
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point header
# | 
add_interface header avalon end
set_interface_property header addressAlignment DYNAMIC
set_interface_property header associatedClock system
set_interface_property header burstOnBurstBoundariesOnly false
set_interface_property header explicitAddressSpan 0
set_interface_property header holdTime 0
set_interface_property header isMemoryDevice false
set_interface_property header isNonVolatileStorage false
set_interface_property header linewrapBursts false
set_interface_property header maximumPendingReadTransactions 0
set_interface_property header printableDevice false
set_interface_property header readLatency 0
set_interface_property header readWaitTime 1
set_interface_property header setupTime 0
set_interface_property header timingUnits Cycles
set_interface_property header writeWaitTime 0

set_interface_property header ASSOCIATED_CLOCK system
set_interface_property header ENABLED true

add_interface_port header avs_header_address address Input 1
add_interface_port header avs_header_write write Input 1
add_interface_port header avs_header_read read Input 1
add_interface_port header avs_header_writedata writedata Input 16
add_interface_port header avs_header_readdata readdata Output 16
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point data
# | 
add_interface data avalon end
set_interface_property data addressAlignment DYNAMIC
set_interface_property data associatedClock system
set_interface_property data burstOnBurstBoundariesOnly false
set_interface_property data explicitAddressSpan 0
set_interface_property data holdTime 0
set_interface_property data isMemoryDevice false
set_interface_property data isNonVolatileStorage false
set_interface_property data linewrapBursts false
set_interface_property data maximumPendingReadTransactions 0
set_interface_property data printableDevice false
set_interface_property data readLatency 0
set_interface_property data readWaitTime 1
set_interface_property data setupTime 0
set_interface_property data timingUnits Cycles
set_interface_property data writeWaitTime 0

set_interface_property data ASSOCIATED_CLOCK system
set_interface_property data ENABLED true

add_interface_port data avs_data_address address Input 1
add_interface_port data avs_data_write write Input 1
add_interface_port data avs_data_read read Input 1
add_interface_port data avs_data_writedata writedata Input 16
add_interface_port data avs_data_readdata readdata Output 16
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point interrupt
# | 
add_interface interrupt interrupt end
set_interface_property interrupt associatedAddressablePoint data

set_interface_property interrupt ASSOCIATED_CLOCK system
set_interface_property interrupt ENABLED true

add_interface_port interrupt ins_interrupt_irq irq Output 1
# | 
# +-----------------------------------
