-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.i2c_pkg.all;
use technology_lib.technology_select_pkg.all;

entity i2c_master is
  generic (
    g_technology             : natural := c_tech_select_default;
    g_i2c_mm                 : t_c_i2c_mm  := c_i2c_mm;
    g_i2c_phy                : t_c_i2c_phy
  );
  port (
    -- GENERIC Signal
    gs_sim                   : in  boolean := false;

    rst                      : in  std_logic;
    clk                      : in  std_logic;
    sync                     : in  std_logic := '1';

    ---------------------------------------------------------------------------
    -- Memory Mapped Slave interface with Interrupt
    ---------------------------------------------------------------------------
    -- MM slave I2C control register
    mms_control_address      : in  std_logic_vector(g_i2c_mm.control_adr_w - 1 downto 0);
    mms_control_write        : in  std_logic;
    mms_control_read         : in  std_logic;
    mms_control_writedata    : in  std_logic_vector(c_word_w - 1 downto 0);  -- use default MM bus width for control
    mms_control_readdata     : out std_logic_vector(c_word_w - 1 downto 0);
    -- MM slave I2C protocol register
    mms_protocol_address     : in  std_logic_vector(g_i2c_mm.protocol_adr_w - 1 downto 0);
    mms_protocol_write       : in  std_logic;
    mms_protocol_read        : in  std_logic;
    mms_protocol_writedata   : in  std_logic_vector(c_byte_w - 1 downto 0);  -- define MM bus data has same width as SMBus data
    mms_protocol_readdata    : out std_logic_vector(c_byte_w - 1 downto 0);
    -- MM slave I2C result register
    mms_result_address       : in  std_logic_vector(g_i2c_mm.result_adr_w - 1 downto 0);
    mms_result_write         : in  std_logic;
    mms_result_read          : in  std_logic;
    mms_result_writedata     : in  std_logic_vector(c_byte_w - 1 downto 0);  -- define MM bus data has same width as SMBus data
    mms_result_readdata      : out std_logic_vector(c_byte_w - 1 downto 0);
    -- Interrupt
    ins_result_rdy           : out std_logic;

    ---------------------------------------------------------------------------
    -- I2C interface
    ---------------------------------------------------------------------------
    scl                      : inout std_logic;  -- I2C Serial Clock Line
    sda                      : inout std_logic  -- I2C Serial Data Line
  );
end i2c_master;

architecture str of i2c_master is
  -- MM - I2C ctrl list
  signal protocol_rd_en         : std_logic;
  signal protocol_rd_adr        : std_logic_vector(g_i2c_mm.protocol_adr_w - 1 downto 0);
  signal protocol_rd_dat        : std_logic_vector(c_byte_w - 1 downto 0);
  signal protocol_rd_val        : std_logic;

  signal result_wr_en           : std_logic;
  signal result_wr_adr          : std_logic_vector(g_i2c_mm.result_adr_w - 1 downto 0);
  signal result_wr_dat          : std_logic_vector(c_byte_w - 1 downto 0);

  signal protocol_activate_evt  : std_logic;
  signal result_ready_evt       : std_logic;

  -- I2C ctrl list - SMBus
  signal smbus_in_dat           : std_logic_vector(c_byte_w - 1 downto 0);
  signal smbus_in_req           : std_logic;
  signal smbus_out_dat          : std_logic_vector(c_byte_w - 1 downto 0);
  signal smbus_out_val          : std_logic;
  signal smbus_out_err          : std_logic;
  signal smbus_out_ack          : std_logic;
  signal smbus_st_idle          : std_logic;
  signal smbus_st_end           : std_logic;
begin
  result_ready_evt <= smbus_st_end;

  u_mm : entity work.i2c_mm
  generic map (
    g_technology => g_technology,
    g_i2c_mm => g_i2c_mm
  )
  port map (
    rst                      => rst,
    clk                      => clk,
    sync                     => sync,
    ---------------------------------------------------------------------------
    -- Memory Mapped Slave interface with Interrupt
    ---------------------------------------------------------------------------
    -- MM slave I2C control register
    mms_control_address      => mms_control_address,
    mms_control_write        => mms_control_write,
    mms_control_read         => mms_control_read,
    mms_control_writedata    => mms_control_writedata,
    mms_control_readdata     => mms_control_readdata,
    -- MM slave I2C protocol register
    mms_protocol_address     => mms_protocol_address,
    mms_protocol_write       => mms_protocol_write,
    mms_protocol_read        => mms_protocol_read,
    mms_protocol_writedata   => mms_protocol_writedata,
    mms_protocol_readdata    => mms_protocol_readdata,
    -- MM slave I2C result register
    mms_result_address       => mms_result_address,
    mms_result_write         => mms_result_write,
    mms_result_read          => mms_result_read,
    mms_result_writedata     => mms_result_writedata,
    mms_result_readdata      => mms_result_readdata,
    -- Interrupt
    ins_result_rdy           => ins_result_rdy,
    ---------------------------------------------------------------------------
    -- SMBus control interface
    ---------------------------------------------------------------------------
    protocol_rd_en           => protocol_rd_en,
    protocol_rd_adr          => protocol_rd_adr,
    protocol_rd_dat          => protocol_rd_dat,
    protocol_rd_val          => protocol_rd_val,

    result_wr_en             => result_wr_en,
    result_wr_adr            => result_wr_adr,
    result_wr_dat            => result_wr_dat,

    protocol_activate_evt    => protocol_activate_evt,
    result_ready_evt         => result_ready_evt
  );

  u_ctrl : entity work.i2c_list_ctrl
  generic map (
    g_protocol_adr_w => g_i2c_mm.protocol_adr_w,
    g_result_adr_w   => g_i2c_mm.result_adr_w
  )
  port map (
    rst              => rst,
    clk              => clk,
    activate         => protocol_activate_evt,
    busy             => OPEN,
    protocol_rd_en   => protocol_rd_en,
    protocol_rd_adr  => protocol_rd_adr,
    protocol_rd_dat  => protocol_rd_dat,
    protocol_rd_val  => protocol_rd_val,
    result_wr_en     => result_wr_en,
    result_wr_adr    => result_wr_adr,
    result_wr_dat    => result_wr_dat,
    smbus_out_dat    => smbus_in_dat,
    smbus_out_req    => smbus_in_req,
    smbus_in_dat     => smbus_out_dat,
    smbus_in_val     => smbus_out_val,
    smbus_in_err     => smbus_out_err,
    smbus_in_ack     => smbus_out_ack,
    smbus_st_idle    => smbus_st_idle,
    smbus_st_end     => smbus_st_end
  );

  u_smbus : entity work.i2c_smbus
  generic map (
    g_i2c_phy        => g_i2c_phy
  )
  port map (
    gs_sim           => gs_sim,
    rst              => rst,
    clk              => clk,
    in_dat           => smbus_in_dat,
    in_req           => smbus_in_req,
    out_dat          => smbus_out_dat,
    out_val          => smbus_out_val,
    out_err          => smbus_out_err,
    out_ack          => smbus_out_ack,
    st_idle          => smbus_st_idle,
    st_end           => smbus_st_end,
    scl              => scl,
    sda              => sda
  );
end str;
