--------------------------------------------------------------------------------
-- Title       : Remote Direct Memory Access (RDMA) package
-- Project     : Square Kilometer Array
--------------------------------------------------------------------------------
-- File        : rdma_pkg.vhd
-- Author      : William Kamp <william.kamp@aut.ac.nz
-- Company     : High Performance Computing Research Lab, AUT
-- Created     : Fri Oct  4 12:16:55 2019
-- Last update : Fri Oct 11 12:24:27 2019
-- Platform    :
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2019 High Performance Computing Research Lab, AUT
-------------------------------------------------------------------------------
-- Description:
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.eth_ip_udp_header_prepend_pkg.all;
use work.crc_pkg.all; -- f_next_crc

package rdma_pkg is

    -- See Infiniband Specification Section 9.2: Base Transport Header
    subtype t_operation is std_logic_vector(4 downto 0);
    constant c_SEND_FIRST                : t_operation := "00000";
    constant c_SEND_MIDDLE               : t_operation := "00001";
    constant c_SEND_LAST                 : t_operation := "00010";
    constant c_SEND_LAST_WITH_IMMEDIATE  : t_operation := "00011";
    constant c_SEND_ONLY                 : t_operation := "00100";
    constant c_SEND_ONLY_WITH_IMMEDIATE  : t_operation := "00101";
    constant c_WRITE_FIRST               : t_operation := "00110";
    constant c_WRITE_MIDDLE              : t_operation := "00111";
    constant c_WRITE_LAST                : t_operation := "01000";
    constant c_WRITE_LAST_WITH_IMMEDIATE : t_operation := "01001";
    constant c_WRITE_ONLY                : t_operation := "01010";
    constant c_WRITE_ONLY_WITH_IMMEDIATE : t_operation := "01011";

    subtype t_connection is std_logic_vector(2 downto 0);
    constant c_RELIABLE_CONNECTION          : t_connection := "000";
    constant c_UNRELIABLE_CONNECTION        : t_connection := "001";
    constant c_RELIABLE_DATAGRAM            : t_connection := "010";
    constant c_UNRELIABLE_DATAGRAM          : t_connection := "011";
    constant c_CNP                          : t_connection := "100";
    constant c_EXTENDED_RELIABLE_CONNECTION : t_connection := "101";

    constant c_RC  : t_connection := c_RELIABLE_CONNECTION;
    constant c_UC  : t_connection := c_UNRELIABLE_CONNECTION;
    constant c_RD  : t_connection := c_RELIABLE_DATAGRAM;
    constant c_UD  : t_connection := c_UNRELIABLE_DATAGRAM;
    constant c_XRC : t_connection := c_EXTENDED_RELIABLE_CONNECTION;

    type t_opcode is record
        connection : t_connection;
        operation  : t_operation;
    end record;

    type t_rdma_base_transport_header is record
        OpCode : t_opcode;
        SE     : std_logic;
        MigReq : std_logic;
        PadCnt : unsigned(1 downto 0);
        TVer   : std_logic_vector(3 downto 0);
        P_KEY  : std_logic_vector(15 downto 0);
        FECN   : std_logic;
        BECN   : std_logic;
        resv6  : std_logic_vector(5 downto 0);
        DestQP : std_logic_vector(23 downto 0);
        AckReq : std_logic;
        resv7  : std_logic_vector(6 downto 0);
        PSN    : unsigned(23 downto 0);
    end record;

    type t_RoCEv2_header is record
        --pad : std_logic_vector(15 downto 0); -- align the eth hdr to end on 32b boundary.
        --eth : t_eth_header;
        ip  : t_ip_header;
        udp : t_udp_header;
        bth : t_rdma_base_transport_header;
    end record;

    -- autogen start decl
    --------------------------------------------------
    -- Auto-generated code below. You should not edit.
    -- Generated Fri Oct 11 12:24:39 2019
    --------------------------------------------------

    type t_opcode_a is array(natural range <>) of t_opcode;
    
    constant T_OPCODE_ZERO : t_opcode := (
            connection => (others => '0'),
            operation  => (others => '0')
        );
    
    constant T_OPCODE_DONT_CARE : t_opcode := (
            connection => (others => '-'),
            operation  => (others => '-')
        );
    
    constant T_OPCODE_SLV_WIDTH : natural := 8;
    
    subtype t_opcode_slv is std_logic_vector(7 downto 0);
    function to_slv (rec : t_opcode) return t_opcode_slv;
    function from_slv (slv : t_opcode_slv) return t_opcode;
    
    type t_rdma_base_transport_header_a is array(natural range <>) of t_rdma_base_transport_header;
    
    constant T_RDMA_BASE_TRANSPORT_HEADER_ZERO : t_rdma_base_transport_header := (
            OpCode => T_OPCODE_ZERO,
            SE     => '0',
            MigReq => '0',
            PadCnt => (others => '0'),
            TVer   => (others => '0'),
            P_KEY  => (others => '0'),
            FECN   => '0',
            BECN   => '0',
            resv6  => (others => '0'),
            DestQP => (others => '0'),
            AckReq => '0',
            resv7  => (others => '0'),
            PSN    => (others => '0')
        );
    
    constant T_RDMA_BASE_TRANSPORT_HEADER_DONT_CARE : t_rdma_base_transport_header := (
            OpCode => T_OPCODE_ZERO,
            SE     => '-',
            MigReq => '-',
            PadCnt => (others => '-'),
            TVer   => (others => '-'),
            P_KEY  => (others => '-'),
            FECN   => '-',
            BECN   => '-',
            resv6  => (others => '-'),
            DestQP => (others => '-'),
            AckReq => '-',
            resv7  => (others => '-'),
            PSN    => (others => '-')
        );
    
    constant T_RDMA_BASE_TRANSPORT_HEADER_SLV_WIDTH : natural := 96;
    
    subtype t_rdma_base_transport_header_slv is std_logic_vector(95 downto 0);
    function to_slv (rec : t_rdma_base_transport_header) return t_rdma_base_transport_header_slv;
    function from_slv (slv : t_rdma_base_transport_header_slv) return t_rdma_base_transport_header;
    
    type t_RoCEv2_header_a is array(natural range <>) of t_RoCEv2_header;
    
    constant T_ROCEV2_HEADER_ZERO : t_RoCEv2_header := (
            ip  => T_IP_HEADER_ZERO,
            udp => T_UDP_HEADER_ZERO,
            bth => T_RDMA_BASE_TRANSPORT_HEADER_ZERO
        );
    
    constant T_ROCEV2_HEADER_DONT_CARE : t_RoCEv2_header := (
            ip  => T_IP_HEADER_ZERO,
            udp => T_UDP_HEADER_ZERO,
            bth => T_RDMA_BASE_TRANSPORT_HEADER_ZERO
        );
    
    constant T_ROCEV2_HEADER_SLV_WIDTH : natural := 320;
    
    subtype t_RoCEv2_header_slv is std_logic_vector(319 downto 0);
    function to_slv (rec : t_RoCEv2_header) return t_RoCEv2_header_slv;
    function from_slv (slv : t_RoCEv2_header_slv) return t_RoCEv2_header;
    
    ------------------------------------------------------
    -- End of Autogenerated code. You may add yours below.
    ------------------------------------------------------
    -- autogen end decl

    function f_ICRC_INIT return std_logic_vector;

    -- ICRC fields replaced with 1 for the purpose of ICRC calc : Inifiniband Architecture Spec Annex17 RoCEv2, Section A17.3.3, CA17-22.c
    constant c_RDMA_IP_HEADER_INVARIANT_MASK : t_ip_header := (
            version         => (others => '0'),
            header_len      => (others => '0'),
            type_of_service => (others => '1'),
            total_length    => (others => '0'),
            identification  => (others => '0'),
            flags           => (others => '0'),
            frag_offset     => (others => '0'),
            ttl             => (others => '1'),
            protocol        => (others => '0'),
            header_checksum => (others => '1'),
            src_addr        => (others => '0'),
            dst_addr        => (others => '0')
        );

    constant c_RDMA_UDP_HEADER_INVARIANT_MASK : t_udp_header := (
            src_port => (others => '0'),
            dst_port => (others => '0'),
            length   => (others => '0'),
            checksum => (others => '1')
        );

    constant c_RDMA_BASE_TRANSPORT_HEADER_INVARIANT_MASK : t_rdma_base_transport_header := (
            OpCode => (
                connection => (others => '0'),
                operation  => (others => '0')
                ),
            SE     => '0',
            MigReq => '0',
            PadCnt => (others => '0'),
            TVer   => (others => '0'),
            P_KEY  => (others => '0'),
            FECN   => '1',
            BECN   => '1',
            resv6  => (others => '1'),
            DestQP => (others => '0'),
            AckReq => '0',
            resv7  => (others => '0'),
            PSN    => (others => '0')
        );

    constant c_RoCEv2_HEADER_INVARIANT_MASK : t_RoCEv2_header := (
            --pad => x"0000",
            --eth => T_ETH_HEADER_ZERO,
            ip  => c_RDMA_IP_HEADER_INVARIANT_MASK,
            udp => c_RDMA_UDP_HEADER_INVARIANT_MASK,
            bth => c_RDMA_BASE_TRANSPORT_HEADER_INVARIANT_MASK
        );

end package rdma_pkg;

package body rdma_pkg is

    -- autogen start body
    --------------------------------------------------
    -- Auto-generated code below. You should not edit.
    -- Generated Fri Oct 11 12:24:39 2019
    --------------------------------------------------

    function to_slv (rec : t_opcode) return t_opcode_slv is
        variable slv : std_logic_vector(7 downto 0);
    begin
        slv(4 downto 0) := rec.operation;
        slv(7 downto 5) := rec.connection;
        return slv;
    end function to_slv;
    
    function from_slv (slv : t_opcode_slv) return t_opcode is
        variable rec : t_opcode;
    begin
        rec.operation  := slv(4 downto 0);
        rec.connection := slv(7 downto 5);
    return rec;
    end function from_slv;
    
    function to_slv (rec : t_rdma_base_transport_header) return t_rdma_base_transport_header_slv is
        variable slv : std_logic_vector(95 downto 0);
    begin
        slv(23 downto 0)  := std_logic_vector(rec.PSN);
        slv(30 downto 24) := rec.resv7;
        slv(31)           := rec.AckReq;
        slv(55 downto 32) := rec.DestQP;
        slv(61 downto 56) := rec.resv6;
        slv(62)           := rec.BECN;
        slv(63)           := rec.FECN;
        slv(79 downto 64) := rec.P_KEY;
        slv(83 downto 80) := rec.TVer;
        slv(85 downto 84) := std_logic_vector(rec.PadCnt);
        slv(86)           := rec.MigReq;
        slv(87)           := rec.SE;
        slv(95 downto 88) := to_slv(rec.OpCode);
        return slv;
    end function to_slv;
    
    function from_slv (slv : t_rdma_base_transport_header_slv) return t_rdma_base_transport_header is
        variable rec : t_rdma_base_transport_header;
    begin
        rec.PSN    := unsigned(slv(23 downto 0));
        rec.resv7  := slv(30 downto 24);
        rec.AckReq := slv(31);
        rec.DestQP := slv(55 downto 32);
        rec.resv6  := slv(61 downto 56);
        rec.BECN   := slv(62);
        rec.FECN   := slv(63);
        rec.P_KEY  := slv(79 downto 64);
        rec.TVer   := slv(83 downto 80);
        rec.PadCnt := unsigned(slv(85 downto 84));
        rec.MigReq := slv(86);
        rec.SE     := slv(87);
        rec.OpCode := from_slv(slv(95 downto 88));
    return rec;
    end function from_slv;
    
    function to_slv (rec : t_RoCEv2_header) return t_RoCEv2_header_slv is
        variable slv : std_logic_vector(319 downto 0);
    begin
        slv(95 downto 0)    := to_slv(rec.bth);
        slv(159 downto 96)  := to_slv(rec.udp);
        slv(319 downto 160) := to_slv(rec.ip);
        return slv;
    end function to_slv;
    
    function from_slv (slv : t_RoCEv2_header_slv) return t_RoCEv2_header is
        variable rec : t_RoCEv2_header;
    begin
        rec.bth := from_slv(slv(95 downto 0));
        rec.udp := from_slv(slv(159 downto 96));
        rec.ip  := from_slv(slv(319 downto 160));
    return rec;
    end function from_slv;
    
    ------------------------------------------------------
    -- End of Autogenerated code. You may add yours below.
    ------------------------------------------------------
    -- autogen end body

    function f_ICRC_INIT return std_logic_vector is
        variable crc : std_logic_vector(31 downto 0);
    begin
        crc := x"FFFFFFFF"; -- Initial value of the CRC: Inifiniband Architecture Spec Section 7.8.1
        -- ICRC calc starts with 64b of '1': Inifiniband Architecture Spec Annex17 RoCEv2, Section A17.3.3, CA17-22.a
        crc := f_next_crc(x"FFFFFFFF", crc);
        crc := f_next_crc(x"FFFFFFFF", crc);
        return crc;
    end function f_ICRC_INIT;

end package body rdma_pkg;
