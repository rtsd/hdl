-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_pulse_extend is
  generic (
    g_p_in_level   : std_logic := '0';
    g_ep_out_level : std_logic := '0'
  );
end tb_common_pulse_extend;

architecture tb of tb_common_pulse_extend is
  constant clk_period     : time := 10 ns;

  constant c_extend_w     : natural := 3;
  constant c_extend_len   : natural := 2**c_extend_w;
  constant c_interval_len : natural := 2 * c_extend_len;

  signal tb_end    : std_logic := '0';
  signal rst       : std_logic;
  signal clk       : std_logic := '0';
  signal pulse_in  : std_logic;
  signal pulse_out : std_logic;
begin
  clk <= not clk or tb_end after clk_period / 2;

  p_in_stimuli : process
  begin
    rst <= '1';
    pulse_in <= '0';
    proc_common_wait_some_cycles(clk, 10);
    rst <= '0';
    proc_common_wait_some_cycles(clk, c_interval_len);
    pulse_in <= '1';
    proc_common_wait_some_cycles(clk, c_interval_len);
    pulse_in <= '0';
    proc_common_wait_some_cycles(clk, c_interval_len);
    proc_common_wait_some_cycles(clk, c_extend_len);
    pulse_in <= '1';
    proc_common_wait_some_cycles(clk, c_interval_len);
    pulse_in <= '0';
    proc_common_wait_some_cycles(clk, c_interval_len);
    pulse_in <= '1';
    proc_common_wait_some_cycles(clk, c_interval_len);
    proc_common_wait_some_cycles(clk, c_extend_len);
    tb_end <= '1';
    wait;
  end process;

  u_spulse : entity work.common_pulse_extend
  generic map (
    g_rst_level    => '0',
    g_p_in_level   => g_p_in_level,
    g_ep_out_level => g_ep_out_level,
    g_extend_w     => c_extend_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    p_in    => pulse_in,
    ep_out  => pulse_out
  );
end tb;
