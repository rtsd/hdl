-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify UTH frames over link
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_uth is
  generic (
    g_use_uth_tx_arch        : string := "HOLD";  -- "HOLD"  = use uth_tx(rtl_hold)
                                                  -- "DELAY" = use uth_tx(rtl_delay)
    g_use_uth_rx_arch        : string := "HOLD";  -- "HOLD"  = use uth_rx(rtl_hold)
                                                  -- "ADAPT" = use uth_rx(rtl_adapt)
    g_phy_link_valid_support : boolean := true;  -- when TRUE then the PHY link can pass on valid control, else the valid not transported and implicitely always active
    g_in_en                  : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_out_ready              : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_nof_repeat             : natural := 1  -- for constant active stream control using 1 is sufficient, use > 1 to verify longer with random stimuli
  );
end tb_uth;

architecture tb of tb_uth is
  constant c_rl                       : natural := 1;
  constant c_data_w                   : natural := 16;
  constant c_typ_ofs                  : natural := sel_a_b(c_data_w < 16, 2**7, 2**15);  -- avoid out of 32 bit INTEGER range for g_data_w > 30
  constant c_max_len                  : natural := c_typ_ofs - 1;
  constant c_data_init                : integer := 0;
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  constant c_tx_nof_ch                : natural := 10;  -- the channels are numbered from 0 TO c_tx_nof_ch-1, each channel represents a type of UTH frame
  constant c_tx_typ_arr               : t_natural_arr(0 to c_tx_nof_ch - 1) := (1, 2, 3, 5, c_typ_ofs, c_typ_ofs + 1, c_typ_ofs + 2, c_typ_ofs + 3, 100, 50);
  constant c_tx_len_arr               : t_natural_arr(0 to c_tx_nof_ch - 1) := (1, 2, 3, 5,         1,           8,           8,          10, 100, 50);
  constant c_rx_nof_ch                : natural := c_tx_nof_ch - 1;  -- do not support the last tx tlen channel
  constant c_rx_typ_arr               : t_natural_arr(0 to c_rx_nof_ch - 1) := c_tx_typ_arr(0 to c_rx_nof_ch - 1);
  constant c_rx_len_arr               : t_natural_arr(0 to c_rx_nof_ch - 1) := c_tx_len_arr(0 to c_rx_nof_ch - 1);
  constant c_rx_not_supported_ch      : natural := c_tx_nof_ch - 1;

  constant c_channel_w                : natural := ceil_log2(c_tx_nof_ch);
  constant c_err_w                    : natural := 1;

  signal tb_end            : std_logic := '0';
  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';

  signal random_0          : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1          : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0           : std_logic;
  signal pulse_1           : std_logic;
  signal pulse_en          : std_logic := '1';

  signal in_en             : std_logic := '1';
  signal in_siso           : t_dp_siso;
  signal in_sosi           : t_dp_sosi;
  signal in_data           : std_logic_vector(c_data_w - 1 downto 0);
  signal in_val            : std_logic;
  signal in_sop            : std_logic;
  signal in_eop            : std_logic;

  signal uth_tx_siso       : t_dp_siso := c_dp_siso_rdy;
  signal uth_tx_sosi       : t_dp_sosi;
  signal uth_tx_data       : std_logic_vector(c_data_w - 1 downto 0);
  signal uth_tx_val        : std_logic;
  signal uth_tx_sop        : std_logic;
  signal uth_tx_eop        : std_logic;

  signal phy_link_siso     : t_dp_siso;
  signal phy_link_sosi     : t_dp_sosi;
  signal phy_link_err      : std_logic;

  signal prev_uth_rx_ready : std_logic_vector(0 to c_rl);
  signal uth_rx_siso       : t_dp_siso := c_dp_siso_rdy;
  signal uth_rx_sosi       : t_dp_sosi;
  signal prev_uth_rx_data  : std_logic_vector(c_data_w - 1 downto 0) := TO_SVEC(c_data_init - 1, c_data_w);
  signal uth_rx_data       : std_logic_vector(c_data_w - 1 downto 0);
  signal uth_rx_val        : std_logic;
  signal uth_rx_sop        : std_logic;
  signal uth_rx_eop        : std_logic;
  signal uth_rx_channel    : std_logic_vector(c_channel_w - 1 downto 0);
  signal uth_rx_err        : std_logic_vector(c_err_w - 1 downto 0);
  signal hold_rx_sop       : std_logic;

  signal verify_en         : std_logic;
  signal verify_done       : std_logic := '0';

  signal expected_uth_rx_data : std_logic_vector(c_data_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  assert g_phy_link_valid_support = true
    report "TB_UTH : No valid support on the link, this requires using always active streaming control"
    severity NOTE;

  in_en             <= '1'                     when g_phy_link_valid_support = false else
                       '1'                     when g_in_en = e_active               else
                       random_0(random_0'high) when g_in_en = e_random               else
                       pulse_0                 when g_in_en = e_pulse;

  uth_rx_siso.ready <= '1'                     when g_phy_link_valid_support = false else
                       '1'                     when g_out_ready = e_active           else
                       random_1(random_1'high) when g_out_ready = e_random           else
                       pulse_1                 when g_out_ready = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli : process
    variable v_data_init : natural := c_data_init;
  begin
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- 1) Stress test by repeating all Rx channels
    for J in 0 to g_nof_repeat - 1 loop
      for I in 0 to c_rx_nof_ch - 1 loop
        proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_init, 0, 0, c_tx_len_arr(I), I, 0, c_sl0, "0", clk, in_en, in_siso, in_sosi);
        --proc_common_wait_some_cycles(clk, 10);
        v_data_init := v_data_init + c_tx_len_arr(I);
      end loop;

      -- Send a UTH frame that will be received uth_rx
      proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_init, 0, 0, c_tx_len_arr(0), 0, 0, c_sl0, "0", clk, in_en, in_siso, in_sosi);
      v_data_init := v_data_init + c_tx_len_arr(0);
      proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_init, 0, 0, c_tx_len_arr(0), 0, 0, c_sl0, "0", clk, in_en, in_siso, in_sosi);
      v_data_init := v_data_init + c_tx_len_arr(0);
      proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_init, 0, 0, c_tx_len_arr(0), 0, 0, c_sl0, "0", clk, in_en, in_siso, in_sosi);
      v_data_init := v_data_init + c_tx_len_arr(0);
    end loop;

    -- 2) Not supported Rx frame test
    proc_common_wait_some_cycles(clk, 50);
    -- Send a UTH frame that will get discarded by uth_rx, because it has an unsupported length
    proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, c_data_init, 0, 0, c_tx_len_arr(c_rx_not_supported_ch), c_rx_not_supported_ch, 0, c_sl0, "0", clk, in_en, in_siso, in_sosi);
    proc_common_wait_some_cycles(clk, 50);
    -- Send a UTH frame that will be received uth_rx
    proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_init, 0, 0, c_tx_len_arr(0), 0, 0, c_sl0, "0", clk, in_en, in_siso, in_sosi);
    v_data_init := v_data_init + c_tx_len_arr(0);

    -- Signal end of stimuli
    expected_uth_rx_data <= TO_UVEC(v_data_init - 1, c_data_w);
    proc_common_wait_some_cycles(clk, 100);  -- depends on stream control
    verify_done <= '1';
    proc_common_wait_some_cycles(clk, 1);
    verify_done <= '0';
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  -- Map to slv to ease monitoring in wave window
  in_data <= in_sosi.data(in_data'range);
  in_val  <= in_sosi.valid;
  in_sop  <= in_sosi.sop;
  in_eop  <= in_sosi.eop;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  verify_en <= '1';

  proc_dp_verify_data("uth_rx_data", c_rl, clk, verify_en, uth_rx_siso.ready, uth_rx_val, uth_rx_data, prev_uth_rx_data);  -- Verify that the output is incrementing data, like the input stimuli
  proc_dp_verify_valid(c_rl, clk, verify_en, uth_rx_siso.ready, prev_uth_rx_ready, uth_rx_val);  -- Verify that the output valid fits with the output ready latency
  proc_dp_verify_sop_and_eop(clk, uth_rx_val, uth_rx_sop, uth_rx_eop, hold_rx_sop);  -- Verify that sop and eop come in pairs
  proc_dp_verify_value(e_equal, clk, verify_done, expected_uth_rx_data, prev_uth_rx_data);  -- Verify that the stimuli have been applied at all

  ------------------------------------------------------------------------------
  -- TRANSMITTER
  ------------------------------------------------------------------------------

  -- Transmit the data block as a UTH frame
  use_tx_hold : if g_use_uth_tx_arch = "HOLD" generate
    u_uth_tx : entity work.uth_tx(rtl_hold)
    generic map (
      g_data_w      => c_data_w,
      g_nof_ch      => c_tx_nof_ch,
      g_typ_arr     => c_tx_typ_arr,
      g_out_rl      => 6  -- g_out_rl is ignored by architecture rtl_hold
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sinks
      snk_out   => in_siso,
      snk_in    => in_sosi,
      -- ST source
      src_in    => uth_tx_siso,
      src_out   => uth_tx_sosi
    );
  end generate;

  use_tx_delay : if g_use_uth_tx_arch = "DELAY" generate
    u_uth_tx : entity work.uth_tx(rtl_delay)
    generic map (
      g_data_w      => c_data_w,
      g_nof_ch      => c_tx_nof_ch,
      g_typ_arr     => c_tx_typ_arr,
      g_out_rl      => 1  -- adapt architecture rtl_delay RL = 6 to output RL = c_rl = 1
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sinks
      snk_out   => in_siso,
      snk_in    => in_sosi,
      -- ST source
      src_in    => uth_tx_siso,
      src_out   => uth_tx_sosi
    );
  end generate;

  -- Map to slv to ease monitoring in wave window
  uth_tx_data <= uth_tx_sosi.data(uth_tx_data'range);
  uth_tx_val  <= uth_tx_sosi.valid;
  uth_tx_sop  <= uth_tx_sosi.sop;
  uth_tx_eop  <= uth_tx_sosi.eop;

  ------------------------------------------------------------------------------
  -- PHY LINK INTERFACE
  ------------------------------------------------------------------------------

  -- Model the transceiver link
  -- . No sosi delay using dp_phy_link, to allow siso backpressure
  -- . Pass on backpressure from Rx to Tx for verification purposes, true backpressure at PHY link level needs to be done at packet level
  -- . Model link error somewhere during a UTH payload to check the Rx CRC

  p_phy_link : process(uth_tx_sosi, phy_link_err)
  begin
    phy_link_sosi       <= uth_tx_sosi;
    if g_phy_link_valid_support = false then
      phy_link_sosi.valid <= '1';
    end if;
    if phy_link_err = '1' then
      phy_link_sosi.data <= TO_DP_SDATA(-1);
    end if;
  end process;

  uth_tx_siso <= phy_link_siso;

  phy_link_err <= '0';
  --phy_link_err <= '0', '1' AFTER 100.1*clk_period, '0' AFTER 101.1*clk_period;  -- determine appropriate error pulse instant from the Wave window in a trial simualtion

  ------------------------------------------------------------------------------
  -- RECEIVER
  ------------------------------------------------------------------------------

  use_rx_adapt : if g_use_uth_rx_arch = "ADAPT" generate
    u_uth_rx : entity work.uth_rx(rtl_adapt)
    generic map (
      g_data_w      => c_data_w,
      g_len_max     => c_max_len,
      g_nof_ch      => c_rx_nof_ch,
      g_typ_arr     => c_rx_typ_arr,
      g_len_arr     => c_rx_len_arr,
      g_use_src_in  => true
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sinks
      snk_out   => phy_link_siso,
      snk_in    => phy_link_sosi,
      -- ST source
      src_in    => uth_rx_siso,
      src_out   => uth_rx_sosi
    );
  end generate;

  use_rx_hold : if g_use_uth_rx_arch = "HOLD" generate
    u_uth_rx : entity work.uth_rx(rtl_hold)
    generic map (
      g_data_w      => c_data_w,
      g_len_max     => c_max_len,
      g_nof_ch      => c_rx_nof_ch,
      g_typ_arr     => c_rx_typ_arr,
      g_len_arr     => c_rx_len_arr
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sinks
      snk_out   => phy_link_siso,
      snk_in    => phy_link_sosi,
      -- ST source
      src_in    => uth_rx_siso,
      src_out   => uth_rx_sosi
    );
  end generate;

  -- Map to slv to ease monitoring in wave window
  uth_rx_data    <= uth_rx_sosi.data(uth_tx_data'range);
  uth_rx_val     <= uth_rx_sosi.valid;
  uth_rx_sop     <= uth_rx_sosi.sop;
  uth_rx_eop     <= uth_rx_sosi.eop;
  uth_rx_channel <= uth_rx_sosi.channel(uth_rx_channel'range);
  uth_rx_err     <= uth_rx_sosi.err(uth_rx_err'range);
end tb;
