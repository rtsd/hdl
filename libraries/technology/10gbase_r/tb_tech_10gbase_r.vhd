-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for tech_10gbase_r.
-- Description:
-- . The tb is not self checking.
-- . The tb is used to investigate what clocks the 10gbase_r phy uses.
-- . The link fault is modelled on channel 0 only. Channel >= 1 is connected
--   from the start and remains connected.
-- Usage:
--   > as 10
--   > run -a
-- Observations:
-- . With the link_fault it appears that tx_ready and rx_ready do not depend
--   on whether the link is connected or not.
--   A link fault does cause XGMII control to become 0x11 and data to become
--   0x0100009C_0100009C. When the link is connected or reconnected then the
--   XGMII control depends on xgmii_tx_dc_arr.

library IEEE, technology_lib, tech_pll_lib, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.tb_common_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tb_tech_10gbase_r is
  -- Test bench control parameters
  generic (
    g_technology          : natural := c_tech_select_default;
    g_nof_channels        : natural := 24
  );
end tb_tech_10gbase_r;

architecture tb of tb_tech_10gbase_r is
  constant c_sim               : boolean := true;
  constant c_sim_level         : natural := 0;  -- 0 = use IP; 1 = use fast serdes model (not useful here, because no proper xgmii_tx_dc_arr stimuli)
  constant phy_loopback_delay  : time :=  sel_a_b(c_sim_level = 0, 1 ns, 0 ns);

  signal tb_end             : std_logic := '0';
  signal tr_ref_clk_644     : std_logic := '0';
  signal clk_156            : std_logic;
  signal rst_156            : std_logic;

  -- XGMII interface
  signal xgmii_tx_ready_arr : std_logic_vector(g_nof_channels - 1 downto 0);
  signal xgmii_rx_ready_arr : std_logic_vector(g_nof_channels - 1 downto 0);
  signal xgmii_tx_dc_arr    : t_xgmii_dc_arr(g_nof_channels - 1 downto 0) := (others => (others => '0'));  -- '0', '1'
  signal xgmii_rx_dc_arr    : t_xgmii_dc_arr(g_nof_channels - 1 downto 0);

  -- PHY serial interface
  --SIGNAL link_fault         : STD_LOGIC := '1';   -- model initial link fault
  signal link_fault         : std_logic := '0';
  signal tx_serial_arr      : std_logic_vector(g_nof_channels - 1 downto 0);
  signal tx_serial_arr_dly  : std_logic_vector(g_nof_channels - 1 downto 0);
  signal rx_serial_arr      : std_logic_vector(g_nof_channels - 1 downto 0);
begin
  tr_ref_clk_644 <= not tr_ref_clk_644 after tech_pll_clk_644_period / 2;

  p_stimuli : process
  begin
    wait for 50 us;
    -- Model a link fault to verify Rx recovery
    link_fault <= '1';
    wait for 10 us;
    link_fault <= '0';
    wait for 10 us;
    tb_end <= '1';
    wait;
  end process;

  -- This extra p_dummy process seems necessary to ensure that -label will stop
  -- the simulation before the FAILURE when p_tb_end is the only process that
  -- is still active, which is the case for g_technology that do not support
  -- the pll and dut. Only using 1 us >> tech_pll_clk_644_period in p_tb_end,
  -- instead of 1 ns does not help to avoid the FAILURE.
  p_dummy : process
  begin
    wait for 1 ns;
  end process;

  p_tb_end : process
  begin
    wait until tb_end = '1';
    wait for 1 ns;
    report "Tb simulation finished."
      severity FAILURE;
    wait;
  end process;

  pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
  generic map (
    g_technology => g_technology
  )
  port map (
    refclk_644 => tr_ref_clk_644,
    rst_in     => '0',
    clk_156    => clk_156,
    clk_312    => OPEN,
    rst_156    => rst_156,
    rst_312    => open
  );

  dut : entity work.tech_10gbase_r
  generic map (
    g_technology          => g_technology,
    g_sim                 => c_sim,
    g_sim_level           => c_sim_level,
    g_nof_channels        => g_nof_channels
  )
  port map (
    -- Transceiver ATX PLL reference clock
    tr_ref_clk_644     => tr_ref_clk_644,

    -- XGMII clocks
    clk_156            => clk_156,
    rst_156            => rst_156,

    -- XGMII interface
    xgmii_tx_ready_arr => xgmii_tx_ready_arr,
    xgmii_rx_ready_arr => xgmii_rx_ready_arr,
    xgmii_tx_dc_arr    => xgmii_tx_dc_arr,
    xgmii_rx_dc_arr    => xgmii_rx_dc_arr,

    -- PHY serial IO
    tx_serial_arr      => tx_serial_arr,
    rx_serial_arr      => rx_serial_arr
  );

  -- PHY loopback
  tx_serial_arr_dly <= transport tx_serial_arr after phy_loopback_delay;

  p_link : process(tx_serial_arr_dly, link_fault)
  begin
    rx_serial_arr <= tx_serial_arr_dly;
    if link_fault = '1' then
      rx_serial_arr(0) <= '0';  -- model link fault only for channel 0
    end if;
  end process;
end tb;
