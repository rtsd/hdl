###############################################################################
#
# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

set_location_assignment PIN_F9 -to SI_FN_0_CNTRL[0]
set_location_assignment PIN_G6 -to SI_FN_0_CNTRL[1]
set_location_assignment PIN_G9 -to SI_FN_0_CNTRL[2]
set_location_assignment PIN_W8 -to SI_FN_1_CNTRL[0]
set_location_assignment PIN_M7 -to SI_FN_1_CNTRL[1]
set_location_assignment PIN_M8 -to SI_FN_1_CNTRL[2]
set_location_assignment PIN_AP6 -to SI_FN_2_CNTRL[0]
set_location_assignment PIN_AG7 -to SI_FN_2_CNTRL[1]
set_location_assignment PIN_AG8 -to SI_FN_2_CNTRL[2]
set_location_assignment PIN_AV10 -to SI_FN_3_CNTRL[0]
set_location_assignment PIN_AU10 -to SI_FN_3_CNTRL[1]
set_location_assignment PIN_AT10 -to SI_FN_3_CNTRL[2]
set_location_assignment PIN_AW6 -to SI_FN_RSTN

set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_0_CNTRL[0]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_0_CNTRL[1]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_0_CNTRL[2]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_1_CNTRL[0]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_1_CNTRL[1]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_1_CNTRL[2]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_2_CNTRL[0]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_2_CNTRL[1]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_2_CNTRL[2]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_3_CNTRL[0]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_3_CNTRL[1]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_3_CNTRL[2]
set_instance_assignment -name IO_STANDARD "2.5 V" -to SI_FN_RSTN
