-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- The dp_block_validate_channel.vhd can remove a block of data from a
-- multiplexed in_sosi stream based on the in_sosi.channel field.
-- If in_sosi.channel = remove_channel, then the in_sosi is passed on to
-- remove_sosi and not passed on to keep_sosi, else vice versa.
-- Other modes can be selected by setting g_mode. g_mode options are:
-- . "=" which removes all blocks with channel = remove_channel
-- . "<" which removes all blocks with channel < remove_channel
-- . ">" which removes all blocks with channel > remove_channel
-- Remarks:

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.dp_stream_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity dp_block_validate_channel is
  generic (
    g_mode : string  := "="  -- can be "=", "<", ">"
  );
  port (
    dp_rst          : in  std_logic;
    dp_clk          : in  std_logic;
    -- ST sink
    in_sosi         : in  t_dp_sosi;
    -- ST source
    out_keep_sosi   : out t_dp_sosi;
    out_remove_sosi : out t_dp_sosi;

    remove_channel  : in  std_logic_vector(c_dp_stream_channel_w - 1 downto 0)

  );
end dp_block_validate_channel;

architecture rtl of dp_block_validate_channel is
  signal remove_blk      : std_logic;
  signal remove_blk_reg  : std_logic := '0';
  signal remove_sosi     : t_dp_sosi;
  signal keep_sosi       : t_dp_sosi;
begin
  assert g_mode = "=" or g_mode = "<" or g_mode = ">"
    report "g_mode must be one of three options: '=', '<' or '>'"
    severity ERROR;
  gen_equal   : if g_mode = "=" generate  -- remove all blocks with ch = remove_channel
   remove_blk  <= remove_blk_reg when in_sosi.sop = '0' else
                             '1' when unsigned(in_sosi.channel) = unsigned(remove_channel) else '0';
  end generate;
  gen_smaller : if g_mode = "<" generate  -- remove all blocks with ch < remove_channel
   remove_blk  <= remove_blk_reg when in_sosi.sop = '0' else
                             '1' when unsigned(in_sosi.channel) < unsigned(remove_channel) else '0';
  end generate;
  gen_larger  : if g_mode = ">" generate  -- remove all blocks with ch > remove_channel
   remove_blk  <= remove_blk_reg when in_sosi.sop = '0' else
                             '1' when unsigned(in_sosi.channel) > unsigned(remove_channel) else '0';
  end generate;

  p_dp_clk : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      remove_blk_reg      <= '0';
    elsif rising_edge(dp_clk) then
      remove_blk_reg      <= remove_blk;
    end if;
  end process;

  p_sosi : process(in_sosi, remove_blk)
  begin
    -- sosi that contains the removed blocks
    remove_sosi       <= in_sosi;
    remove_sosi.valid <= in_sosi.valid and remove_blk;
    remove_sosi.sop   <= in_sosi.sop   and remove_blk;
    remove_sosi.eop   <= in_sosi.eop   and remove_blk;
    remove_sosi.sync  <= in_sosi.sync  and remove_blk;
    -- sosi that contains the all but the removed blocks
    keep_sosi         <= in_sosi;
    keep_sosi.valid   <= in_sosi.valid and not remove_blk;
    keep_sosi.sop     <= in_sosi.sop   and not remove_blk;
    keep_sosi.eop     <= in_sosi.eop   and not remove_blk;
    keep_sosi.sync    <= in_sosi.sync  and not remove_blk;
  end process;

  u_pipe_remove : entity work.dp_pipeline
  generic map (
    g_pipeline   => 1  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => remove_sosi,
    -- ST source
    src_out      => out_remove_sosi
  );

  u_pipe_keep : entity work.dp_pipeline
  generic map (
    g_pipeline   => 1  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => keep_sosi,
    -- ST source
    src_out      => out_keep_sosi
  );
end rtl;
