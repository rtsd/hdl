// Copyright (C) Altera Corporation. All rights reserved. 
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 11.1sp2 linux32 Build 259 01/26/2012
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6c"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
NneEg4OpPb5zjP8l6OXBY/ieAUCtLHiV7xtuh6wGIsgYV+MeWtAgf/tEL8skhGjC
MhoUuByae9Yohh8ZEHnOdY2GjJn7UBbC+RO7QNaPMqkjf6GKVz34GtPIwsK2Qmup
5nuKnqCTGpjN+5PVieEUmPvS87LfDdwKoO5+cqWzTWQ=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 8528)
DgKzRryr4NLBJXFhikBpLdA7pgWe+YuUcRXMu9acB7IXCWj7tkwyA8QuETMNONJk
mOQDdMd1N0HDur6IAb8qWwXwU89WHoejS/9U7BwSybUJTpucU8/8UqDfCPE9NgMf
4exyrROuFZ6lDI8y6jFPGe5OyQ2O5qZbOmlHMo9b2sKyIxltcg1I0nslQK42G3c3
Z09wNKauZgK+DmdVenrhZxnKj3wWsmzlkHy0q2zXvbaQS6WyMJxPfJWPH+aKlu8F
HKLecpLJTFw2N5eqZxKwtOKGLkno1rgq/5gs6YEt2mxY7qfPI7oqyVRhYRuh4V2r
F4XzxmvnMYTy14TQ5O3HsDcSCR/uYsBkRti0P1/qrx6sa6wla60sLhrNH81P8/IF
dhCMvX3Zc3mg+j6T3PXaxCTlCM/FziVeTuL7811PWooRL5qKvnbRh/dJ8IcZgGiE
cKRlgORb+8Hpa9ZbbrbAroWlDXw8iuYa794kSGlAgbqUaegBYlGeoa+Jgix2Kazf
JQUuU4Mmk52VyxqMVVtfsyC0HqakI58/tW1TfK64KxcZSQU149OE/66rAsVhh3Zg
sBiaqLUwm8Px1jkrmaTIl/NJTBCEOUtO1lgExoQ98gHTRbih+rrGo6SzG1a3hO+o
nQOZSxOXkdmFUqpg5h3+9fplNz2DlUzCRWj8JsuvK9vyM2/uMblD3KIZZWio/ugD
buAs2ZB/Toqbte8QvET0lED9327xSEhcIQ2cgX4QVHZDXoxyNam1XCRA8zIXqES4
KiKqfqkLOFxwQT4S1aEjuHitNtnTjA/LmtYQO4TEiyZW+QZ+Xfd5aqIPYtzEf1PZ
/z36DCSU63G2EzT32Fr5WAvvL65XKOKRTt8bWGTxjUNhivjlTBhRAvA/80DF5jop
KWpCrlRlcil4MhtvxJzjRERwdJSSYE+O1LX2y72khdpaAJafiAK5oOF1g2yXgZ/r
V/qO2BuFCmr9zy3Aa4PiTxiv/EeziSRi7XFITwT55w+BPc/Mr6TwRJVf6fRIGqA3
HssoPeecMNHMKiEqjfDLeV9yxnlb/wWvHqBoFdMSRX9ByyH5tB5rUU2LxCaM5un2
0sJvUp+/ao7aszPjG/VwnnfaI14aHXF9iI53PHmi4D0j0QfSA4mFEUmPB+pTvxud
s4DqC4OF8Rf2k7JIH6c00gHLG8BXSpqmOZpZOXfXyS3ey73w/jNHqxhEMCIJJUjj
W/jqoAYzCjyKon0ehedxUzwruF/jgl8NT4dH8SF3GU2bJJBUUWuS11e+8qh0mehE
MjCI0abefdCBdTql4RsLCCsY4rxG6mlGm/xuiBUHMOTOIS3WeyaapUgP5otsQ2vR
cOFiqXbYCUwGEdxb/A7wqAOJvntpWjHRQHVlUZ2s16ipuPSpudBw07HtVN4qlpcF
5Dr0grK4ENcBzcIkSL1nB8tuOV7RaLMIpgQdVmlq7aSw5V7dgrA166rLp8KhvG+l
kV80WXuQRlVMIeaYUHhNApIVxOw3Lt+Fvbl9K1nIaOGTut3+Fxb5ZhCl0kkQTFyw
9hPfhC2XWFwpTFu+2s1X8m9y20xracIn3334BWLvAaKh5367ddJK99LQLAb4B5No
nmFSq2erKl5RJBxp/Z2y9i0Du6v2Z7XkIa0cN+Ou/3on/9iZJi92ee2qdCuK5AHu
+rviGg0JAuVLLCdsYUX9PR2l/IP4d5Mumw0OPBPwHkEOwZxFES8iLbAfE2nKFWvS
ru895GZkWQCPzA8yetx05YgHwtX+z8A0Bk9GarG74kAOt2y0qaIBqMLEpBi3/JOM
8nvCJyOqb3Fn5Z0AiJgCxsRdQkcq8SjyQufxAkZ+e09I5Aewmn8yApI2huUJj2JN
ndchNliVQtYRsGoxPqAZQpz4ui9mc76aQn066SUO/3Q2L1o4jDOg38QTv6jatSyU
jNaJT7F38Z//db8nVL7XfvXTwuBsS0i0gon4OT0R+0oOpln8If3cUuu0TsbwbLGM
KlCIQ63KrUsQCruyZ7m8GEhQIaaCHEmgkQ1BJS1zIAMqTErx337S3GgKMunOGi+K
JbCyJhpr/w8qNTeMTIJJq2jAKdicYKqx2Zfooufuyy53TnnVdahj5TmegqXLLg0L
vfSAX9sf0iU3KN94EkWH89c+0q/1KqrOZrbmdKRYsU5VwHKUeelNXhOCkKBzAJ4D
+tXIE3thxWoGiSTzJLdvYpfXI0gsMlEVXGD2/lXxhW9j7uDWFzyXzJ//3wUa8DV/
Cn0xPbMHQkDDOigyNGIMVvlEexnnkcOKRx8g46k8c/uQcphwrGeFTcHcPQUBRBCO
3q/2zFLVQZVxjd/pYbre1A3kGwBZLB23ib2OkIzwFHZ7QQ+k/GvscDfXzh4Q3bJr
ijZ1sK3cYJoOgzy3iK3Zhe2pgvnMVae5rnRfO9CdiDUg0HePEWniyxMNsolxpqPL
/kO2gnjK/kzu158khUfapqphXESBulQZr8CPvKtiXkPaSrW1C9pLzNnOTsfD46cW
d8EWFTwGo9pZRvybIkxr44XMNU2j9mjsV+t0/pnh79xW79an1sDFiLuw3sVFm2xz
/iwqDSONP21JjFNRwC4+xfcIIhvetpHM4TUM57oocUgRND/miyXe0XUvTqkIHvrU
l3B8Wh0NrxgcXySL7CoVB8EL8LoVlHxy5eIRIqOKK1hgOdtbG9W7AyA/iXfFAZdK
hPx2tEe7Ort4Rc+aN0hfkVlZ86CheckvMLFeRPzyDO9tZWYDfjeWBiKHbC4HOaiz
9/ntH9PbN9t9jX8p737d7+JgkxmN8Kn/SEayDzM/ZVHteWbNDOsZZ1nMb4L6Jg1z
BeOzoRxsLpDACouZyg+OZsAxlcfb4JmaSehRbgErhpJFKypWpIZXRyJnsxLuLeI7
M7eTnuNpDdVtgNRrgtkXoO/IlLWPTIYB0NGJFwBgH0V48o3sFfRjyauNnd8YrL0q
fnbcS+nxLE8JLV6htYPD1WFgmYyMd0NnRg58SuIwXgZohSIOfMLSXW4z6JnwknZ6
jc4IeTfiZwGwU9KVlmZqye38omFuO7WPnlA6MpCXrEVLH8na23PadbCZxFLYu2P+
vx58YZsDPtoV1Zsh8Dx6fB74jr6rpQvgCoVZTCSIleyJtU9rNDtOaIKTYZq7r37K
34sbaUCOMObEpssHxHVatwMKa1BiENnBBfsUkuhEsJTOgHSrg/PzIHn9KQQRZrDW
b8FrGWMLqCkWxogat+wM+R39/GGoLkCGjVnOJkDu9yNX5eQIFKTCKLLp3Z2A9fhV
119rAC8FxvWK/Rh4B/d3GazblGuEgz7oqtCXUb7kcFUoJ89WJ4Ig12hoXhE9as6d
UHouuCRYDWuqMDwwEJXVGB0ZaDJRV0YZ2bmWYKzCZ15DkUUB0ZybhFydZzftpIbq
FC1B64QOrN9pADsLgdX8SSGS9rU7UKeO7Dj2c1CdHdu1KfDZQ/CpPmnt/3nC1QrR
LwN9t6EMPe/vfBKIFZjhbFxngtuDJdi/UNqCta7Y19Qb/XrG8LoURNCVpq5z1t0k
8ry7MRpaPbHGz208xp22oDfRyXitXWw9gBideFZGm8wFqBxum4kIET4tjcFXjV7A
ccEnar2Z7vTy3VyUMcIBRldrSD+bPfsfdPSOKCWP47FqtkFG9BUCS7lrpxakCO1m
Xa/3YtJdKlBbn9JKwrGUYjcquaqbeyoTw+aBSEkail/dt4ZQIzvDOYEXnxd/FZui
Xo+U5u1LA9OhHrclTn4xEesRVWPXXRQMjCXRyRZ/DPMs45j4yIShr1Kl/ChSMYwN
X9vhSuMyhYsxVDQAmdq3MZraEC6LKfTHSzIHmnh86M9RUZk1FFwxWbN4ejejXunV
7mv8berWdifCpMvj6dNsY/wpTIpDJbnr2MT3Cq1v5qfp9TCAfHNWBTS7sC5l7cGw
2q0tSdw4FMk6LyOprpcGlE9uEXRkF21yJWAKG6eW4ylqoSXvl++XJm5PLsGe9T+B
Mv8Vr5Hz+qiehLVX9DRZif0EEdioOqcdL/hfIDEfCf98XE7ZHQNMhhMxMYHA0O2s
NX73qKdYnG11JXSfWG8zti9xYa6eczI9lXVZMkBYpyN8chjTXboi91QoYbivHwqz
qFnlbWWYWk4R9/93v6Sndhgm71deY1xtyFiM2G3rmxjuIgNM7HlMoxSjSEYb8Jqz
IQf+MT/JXV+0QenheBr1nNIe0cSSDIdAxWnhD/MIt/OX0KQ908FWanImc47Vgeq4
as5YsvzH7Q5v4KMwB0LRdVev1eYzpKzraUC4QMLaehr5V9J0Uze3lj3D75K+nYtN
2mA5/g1qO3bvCqXxkxLL+TgQB307MwDNGnYo9CTnNmHqXGzbmIdoAQKZIYOGSHXQ
qpsXaYzCwdHpjjmJT8bWe9EvcjppkojEs6Q3doZRGoM1CwXNmSdr59NxbyjFDwtb
x2APtrxBxJCeWLQPMcJobXMJtBLWe1MMsIsJimD2iHWeO1gZuZTcUtbEG++aeYz3
5BXIXFlNGbOUHDldu0hrLpAowkLfEfjmxsKYGSqREesJl6rDotgiKkX6AFRe0Tsp
BhPnXtPGqTAjxprRbPbca+7+Wmd6ULodg6BrHiyM7Nf/A0dPi2f3LthaS/drlwoX
0KKwWD8ax3rUFZCz0m5BWpWx3Mj43OvJ0t4pf5xnz2JRMVB1nq2rWN7KU9PuKzg4
g9RDpoqm9D/ag+WCF3W7FqRYcf8sX7IUGGvB6wiRDVRhF0Wv9wlGoGEGXBu7sUtl
RKsk0u4tgjq5qR6jIe29OkJ2F74Sge8MXv+Av6Ca5NIZtIy8HiAMvJiMQfiLz8/c
WXRVn/n8vhIbxPKqhbUFObWM/cpBE5SImHIqPjGmcZ9qfv+D1/Q7aGzffCI5x3Zn
Q+W8TVNVQL903mgTHgLGvR8ig2gPK1ubrrk9faLbCdavbvbAapg9bNQuxxfdRX10
swNsbi5a6vw9aTnJ3IKUlgXiwLIqA0h+U6qvmF0AHTQehnGtYYXl3PcWLLS/nlLd
MmfYCLkueGbBUR53ZzifaIMZHZLYbZxbV2mKUQrbvdxBAsKEiF9AGZO8iDkSU6La
zTb9JZSo7Rlug9PL3eZO8xSWum8S2RMWMRyZS1P17mdXE4Vi6W63nSEyrTv1LOrr
KR/Gj1eBQ3zgzHIjb8DGlmYOD0bCGUfepdrox9leqJMaNd3Je/T7v+3Is41SBHCM
InU+/NGJmo4UKDr7YG00GgKxuf5N+p4zYkf5XcSS7rkqADpLE5CsTRHS9MJsMRMk
//BLDH5/jBqacj/gz9bjHZiFGLZ7adT8zayZ+rpFQp97NfFof9TJxTS+v/T7LM1s
liis+rxjThCV716HrBzRkJCVASF2j6I04OHxNDOdf+qRUP7Bf/s1VnBNH50/NUGm
bTR0v3maiqGYcWnhwW7m8yFK9VPeMfHQ36UzUlkIh8ZXadKgDq4H5jINd/Jjd9EW
ZIOboClAONh9yXe51FTNhH6w6oU+A1I6WR4bu9Ar1O+nCkApVrsmo1NngzshRpj7
KXWahR/sz2yghjX5VXsxzg1KNypaSDCA+MFXGjRlAGsm4qj81tZ59EexsRbfy7vi
vtMqgBVCYvBhEMNBEonpldvapkwNMVz87CyMhyw0/yy7tdG11gD+pzDxx8TOeB56
oIV0vLhrpUJuC/LhzwkC/sSDHkpFKtSgzra092BG8om65+3p65qw2H0GMxs2s9NO
36EFzEirYgH51B/IlNkMXnuCHOm643M+j6+Mv7hdwgGMrQm0TFeduIPFJFHNIdVT
AZBntsl6+/oAhDPDAFaL8xnaP3y/XbsgMahtDVnCJ0Bg2Ei3mo655Cgp6SeOq4W8
lAtmmCdJdkr4LikAjotDTlKFS8UU+rgiGcREj+C88vT62qLXzKwOcYj+88tINI5b
GdcVtqKKBECoGSc1sIFSKFMqmJ1FUtLS6TB10vrKdGc8fN7ANKpFsc0zCs+jSXxo
0RlfmqkmYytteeVuboZBzmxMENzLTLVhhdo8ji3HwrL62DfKBYS9GQb0niMBxCDl
v2QfhQH++LV6Lzkd3zl97C8W5WiPIrQYmX1wPBfmWxMzkF6CTyKYnKUOn9u8sIA2
cAQNHaCLZbLQ7serd+Jr/8McqhMpqsQBwhU9MdCKM3e1KxoTB8BWhRKqEPya8Gkh
cXnLhhFeUL8J6dnPXjR+L0rthMw1nqEcKxKzxcGIfzNGgnT8mnOIFNzYGsqiwG0K
hC/I4NVe9Wz2MTzcqwWzjP0SbKLeSuEGeg4CDvbpccrOcrjZNK9ZhtP6OALj/ypR
gk+ItBhjxabLsXHTlNvX7OQ86QbOHHJdnHlo4iiWOae0ujoOT4TyYPxq+w0b5Hze
UOgYoXXuzHaQVDi9mfa0z/Yr2AMZvhaTndt2pM54drSvveqqloUTK0XHLIZS5QT2
gawLCLrM0E9b7/3F4shQv5rWyp5a7b8aGjmDAVoaYL+QkDIE4qejR2Lr96QTfXsm
1yKz9dpXPnXnb271owE2hgrBuAVJrpQK9JHqY1aM0cKrandGGQo/0xGdTDr7r65H
+4woSJ4qk9AfJAxPOJO9Hmm3L72Ub78mjaZ8KL7gs/mj1o8G8hetSkI9F6G8s6jG
oZwfevZMys5KmL4xeMo0CwCagrE27HpNU344ogstaQMfthQLlDm/7OITMEVhxKCe
qqUQWHRoZbEpTvjjnim5+08TEJLtqksSJTB1pzOlYv3+t2AncGTOpUQGbvhbbq6G
QAKOwsipt/NKA5A+Vnw9JZ3BzT+Mf9dgDCi58wKJZ5e4YgSzuzCmHScRbm7YEk4j
UVunPj9rsj8c5wXlD6QE6KkC5aphKxNcomYncjmr9OAs25Lyb+kpBZSRnKCejioB
923LWxDzVSRfMtODSUctxsVWGvdLFlOKpN9NQl6uTsnGrTYI1Aqp2kiqdTUQzpKA
ChHO5Y0xPAlpa3CZLr0HcpNniYsDVgBdcuVFXSlueWvDfDwXXfu8m38uIoyPY/zE
juPY0X9SAsese9BpFHGscUhWIii4lCqceFXvc/VMSJiCO8CnFtepVc/DCBJfKIsF
fhOxt6qR72g3olAwYX9gJgze4T3Gznpt5ymcGu+0+TKhfR4IhoLDZtEv5cKNfYzH
dTbjbKM0Zb//osuZm2G55+v8axCd76GZh1DUFUb/5HrKq1mgqkEQ7LBEub5p0fQ2
VC2RtH1HOzF6G7b3pYIfp5Vghdbz9gg257KKD90O9gfu8O70B1LlGjlXINATPLgK
+c9y0JfcXZ+Dco/hlZ/DGrbJHCGKVnEQVvsnakQCbAuSoZvPZIzE0u6LLy/j5w+f
BD2T4JocauJyLIkUfVmE6zf1t4tgXDpnaJodPlO26utRkWjFyiTGzNDuJ0DOp7ll
nXYciegqKjyvFZxSQZ/C4zvJZepGDrPiTUacGhUXxUjTyfQL3U7S8ZPcKggAjscL
M0VDkuEF4FYtOhBfPKFQ97h17oLhQ/YrsxVGPovv5c98HKkkHabDAdvfP/A1MYX2
mmXKOr31oxbFSSwQBPV0O8McNBJNlYzbbiV8BZv59TGznwop58VGm9P60c69/LFA
9mNvSdUHozzhW1HD+U3JE/wcAElkad4HfSWKlLPfGcGfec1lnPQYUOFXZJFc0FkN
kdz4KqP1PFS+gdfELXGfG3sTZWApOLi0LfQjHVG+78wkeiWdrTBHFkrfv9pUX1dD
P4tKbqJU2DfzE45EOgcDMeGvYduonZcI9QrCR/jaGZCunX3+/GsCmPBaUWIgPdyj
NT5+hLu+d6OUSBg13tsXsxYtPjjLM3og8hQFhH1DI+mAixpujORBP71v41JJnlbX
5JvmdN6aObBTP+7hfE50APAACCMbah6qr7IaZPRb+FDRiR/losX93x43RIz0u926
lZxKOD5evuZibgvEng9clf0PdjUJoz9hh/rrUZOd2gdbLULS8DGzYLiIyUZ+s6VQ
rK0ljDpbHaD5zHY3EyAg39hLgMZij3O274+70yaI3ldVt9FffTGfgephHVVhatDq
aetpi0xYISvfofF3+0rFiPo3gZtJjvCTjLMGNgZmTv5smGBKtWGWBkmdK1V0BQv0
9KEzKcbDiD3sjeiV2TddcuuUH4hKTEbhNLnGmCeh+fPoJp4brtp1lDyJ+WBNKAYl
b/TdQ4YB7y5o8aCb2zMACe23n8v8KVkLMGT1zTIDFeq5iFZkjwPYRWOZgEIeK5BU
UNijf219pEBtQbHOmNMC3ryGT2MVnX3VgDGIxZWklF1VBAaFLusLu3C7lKnzZvkc
OajPeWSWiqGzge7GbNAQtQYD73ADBR+qE2FoTTttGXCDsgJsuToo7gMRpP7fAhJo
rFiMMkxNEmO+TqsuDDDl02rVuebpPAiVX6x8ReDRSWy0Ei1naYjyNGHUXKLajFtN
IrCL+WZt7+Fs9A1XcDh1fTeRz6vTQ6Rp3A9X0FG12HYlnTieCv0mTaMUrtYGGhIX
EiS3vVTnagEYmRfqiyLWRoBTbtGBL7imDNAkJhYyRHMT9O7mrCDR79qdHHn+1dwN
aJ/1oRd2eO2NFr5Sg4QU4ITPKIWnJ8ZMwJFo3sSgufwgMhlpDSz7paVRIXlzuFpt
41XYq/pXRy84xJMa3unDgEjq8ljOtPTvOZJ3x4OY491jB0pK/MFr/raGqSZFJCLc
NgKtFIRS05h0918qc6fEBXsDwdxwdWDoxnnrprMWiDzCW0St4uuJBfB3ETekAN3y
hlfKAqAzySWkXG1Hjl2I7vL2oXcsjimn6/M8VH29JkzWhIQEpuxpLLR6oeTQQjnY
T5d4cetYfamZgsine5uiOn4X/peBSfLjl39K5lNpfdCGjrU68uIAqR0ajhrz5bJW
zGfKfdfsp6h9GJZVYXIqCqtGtdsebC5DGKOM8CqNZZRo5XD4S/xs/+Xg/dyiZK8O
EZdr2asA9gaOQJHOG1hfeTxQMPmO3An59e060SVYMIWhdkoMded8bEiWT5LTBL9S
EVhCRB96CgrASffqFIZMG8WwqWzFIwN69eRzf84qJS8476Ol5VbXNetqjhwnRe2g
LXZOF1XHSXgVaHa0zi1CDuL+dYctIJWY53yRyPGXETxb1tjZCE/TOzAve14i3xdj
6npD916Q5+reFpzrg//TAO3sgdQEMJHvuKfpWtoCtluq5zI6QzvzcsoKZhOMLWJv
BHXPgBzDRhW4U1V6rdVklU81AJALrRrnzfdhD1DHsTb+8+i0o8jZW2hBgA7SDnsD
LFIvsclxkHgA3U5waZl0daPnQdXJbIRYW/wNAivGKoGnjM2OoCf4rAd1xkYPfC0I
8DabFCiZqjG9JUDMKn0o0gZ74hXTeyY/nGhFAgbfBfhTf3LeeNEszLVvXx9/Xdfa
2rpmq7cvjcuI+RYi0HgF1v2mxd7RF7qIga3n2QcMoFRSqa12G83mAztI8uJn1qUr
+OkFNu9xJBqp4sUMLwZoxfufYpscLUujV8/Q7JedmbQM66T1xvzzZK+ypg325G9+
x3Pa50ZseNZrfDWZXQTOkTQ4iUVuQmesz7C1w7FZSHSDQKDZCzY+U6Pq1L/ys+/t
UgL75lR3FMnzKp14ooNgQmrKQZhT14OSuTGOk+OAhCeGij/1KsaGKTuEtspsPQ9c
gDpwDr0A6jDf04iz3VwG87yZXTeGr8Edl1tXQ7HW+VH4n1K2cYIQWVW0i5bmCLmm
lMVO9SSZYj74YA/guXbR+5uoBdKyQklOnxmIaxgZczT021nBJAEE2nq3NSl7B8Uv
o0YhsgIcwHz95ga0q51/niAkp1IFdTGz+Z0fi4tJIM/5s1ZegYBcK28ewdYjivzF
3aiOzIAn85wHDIRsXlZ5BXfhuXQqu5OCl8LOVfhLWpevmT+M8E16Y6uk6mQvtz6e
+RMRAX4En217S1v4LmdbpP5hk6JU9lqdHQQ3f9ESWu2tTUKwz7MiMEb/6nscCQjX
+wzJ5gbucKr+PI72SWuYyqNHN5+Ek6bGKa/1DMLFlpoHW4yOLY5NTeNfEZEYobg3
bk8oA8hQpELISDEBkAzmdPDzEda/A1pbI8k6LMq3iGi2Dv9nE81AmzPyk/r73So8
xMVVIdXJsrNZtjpCHzT839LeyuJG9BH1XRSVQGyN/h0TiXsYJ0btu7GZ9kOgczFe
x0V8aHxl2WJuv+zyWNF3USBQZkCF2RmghMSS6q2YbVi+oPFCUEZYTlF0Datd5Mac
+xDoVATAJTIEsOmM6wltJWe66B9k/sYayV8dOvq9Sl4muzrVmwX6Zsw21zxa0qh7
flKh7ohdZnf7LJgZLjkCNyUlHiK+kHeNZKscvsYQ4vnpKI/FDkHAPDT6WwRJgaWF
z/wQ3C0kOdstCINgxgQnZq1vhe3Q2C7N1LS/qKlwUMfwOQhGFEeej0cSegvmNSOP
CvTsJMpxtz6zye9ExxKhLNE/YpR0svx/lf0vKGY05DhTOY3zRIpK9QO5o/hHkdL4
CBrNw+edjfE2AcIwtmWpBWcue39q7AkuGIT0Z2Xn2pwnmvbemACOlHA+q7FCTksX
hj0d3Rj+7HmYp6HNvzJUdQYvraxw+apnXTDyiL6hl+EH239Oy/EPFDzxv+Rk3B1h
G5BfYzcIomgGfVUzTCFCHQTJW1AfZrjNWBQpkcn9tMVJDmlsurhCmvOrTYEqeAqG
uuN5gNPQjtx9CVgGiPBxzcKlhNRk9YRJjh9YqmYVCzDYPmhqjpOruSoXV1OqjCEG
C44oXAf5KFhvf5YSS5MQVGbvgz2tBQNgi8nygKq92fDcA8c3Qy6VOcBsxnuJnf5z
L2GbMahd6YthlY3GIB9qmo1lJ4DkYKIz6vfATDVdhwJmTWQx/D6rICARR780cmY5
RTSvkBEW1rj1OiAQ23sMMXKF3D5Bbsiv1NQJQNLJSChKnDRGzNnI3iH/eyK6r0t7
Dce2Ikn5/WFrhfd0CHj6PnorKXimALM2s2bThBLQXn7UGnBLEmSAhec90B7RtNbs
5xhLPh/7GKB8bH8ty9Deqo2JQ3JWVBJfFfu2IwKd2jrPd6vue/87mqqKlBGVcUK+
GetOuj/wnV07acCxpOeohbTHrAPy+PF9DwcZSSGb5WoXhPlX60460NgYIWCbQ+dQ
v9f6uP4/Ecj1aReItO6Xz0GTpthi14vR5MF1j4L8ZMVjZck4Jm/h6wlGZtiFE+e/
NlFk7Tup+zPylB31YKAB3GCpL2qy4vFo2khuhFyyDBPOmhOzGMxcn5BIaFRkXnGT
VU91yGYvlNUAA65KnFwoFWBT/EVX8HX5GHnuysAhViG7a6Qnpb6ODMAY1nURKpdp
nxD/bBLP23mPeY08Kopw/xNcHikxP9DCyDLsDD3OqFKiUEiS83arJbax2Uq5CiET
tClSSjNKRkyGJGLwb0cdEKwZBXsKopwaWGcZhUXuwCE=
`pragma protect end_protected
