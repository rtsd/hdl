-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_add_sub is
  generic (
    g_direction       : string  := "ADD";  -- or "SUB", or "BOTH" and use sel_add
    g_representation  : string  := "SIGNED";  -- or "UNSIGNED", important if g_out_dat_w > g_in_dat_w, not relevant if g_out_dat_w = g_in_dat_w
    g_pipeline_input  : natural := 0;  -- 0 or 1
    g_pipeline_output : natural := 1;  -- >= 0
    g_in_dat_w        : natural := 8;
    g_out_dat_w       : natural := 9  -- only support g_out_dat_w=g_in_dat_w and g_out_dat_w=g_in_dat_w+1
  );
  port (
    clk     : in  std_logic;
    clken   : in  std_logic := '1';
    sel_add : in  std_logic := '1';  -- only used for g_direction "BOTH"
    in_a    : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_b    : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    result  : out std_logic_vector(g_out_dat_w - 1 downto 0)
  );
end common_add_sub;

architecture str of common_add_sub is
  constant c_res_w     : natural := g_in_dat_w + 1;

  signal in_a_p        : std_logic_vector(in_a'range);
  signal in_b_p        : std_logic_vector(in_b'range);

  signal in_add        : std_logic;
  signal sel_add_p     : std_logic;

  signal result_p      : std_logic_vector(c_res_w - 1 downto 0);
begin
  in_add <= '1' when g_direction = "ADD" or (g_direction = "BOTH" and sel_add = '1') else '0';

  no_input_reg : if g_pipeline_input = 0 generate  -- wired input
    in_a_p    <= in_a;
    in_b_p    <= in_b;
    sel_add_p <= in_add;
  end generate;
  gen_input_reg : if g_pipeline_input > 0 generate  -- register input
    p_reg : process(clk)
    begin
      if rising_edge(clk) then
        if clken = '1' then
          in_a_p    <= in_a;
          in_b_p    <= in_b;
          sel_add_p <= in_add;
        end if;
      end if;
    end process;
  end generate;

  gen_signed : if g_representation = "SIGNED" generate
    result_p <= ADD_SVEC(in_a_p, in_b_p, c_res_w) when sel_add_p = '1' else SUB_SVEC(in_a_p, in_b_p, c_res_w);
  end generate;

  gen_unsigned : if g_representation = "UNSIGNED" generate
    result_p <= ADD_UVEC(in_a_p, in_b_p, c_res_w) when sel_add_p = '1' else SUB_UVEC(in_a_p, in_b_p, c_res_w);
  end generate;

  u_output_pipe : entity work.common_pipeline  -- pipeline output
  generic map (
    g_representation => g_representation,
    g_pipeline       => g_pipeline_output,  -- 0 for wires, >0 for register stages
    g_in_dat_w       => result'LENGTH,
    g_out_dat_w      => result'length
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => result_p(result'range),
    out_dat => result
  );
end str;
