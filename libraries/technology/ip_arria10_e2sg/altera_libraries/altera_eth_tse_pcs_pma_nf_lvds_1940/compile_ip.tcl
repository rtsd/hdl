#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist                                                                                        

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e2sg_tse_sgmii_lvds/sim"
    
vmap  altera_eth_tse_pcs_pma_nf_lvds_1940    ./work/

  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_eth_tse_pcs_pma_nf_lvds.v"                -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_align_sync.v"                         -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_dec10b8b.v"                           -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_dec_func.v"                           -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_enc8b10b.v"                           -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_autoneg.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_carrier_sense.v"                      -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  #vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_clk_gen.v"                            -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_sgmii_clk_div.v"                      -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_sgmii_clk_enable.v"                   -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_rx_encapsulation.v"                   -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_tx_encapsulation.v"                   -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_rx_encapsulation_strx_gx.v"           -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_pcs_control.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_pcs_host_control.v"                   -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_mdio_reg.v"                           -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_mii_rx_if_pcs.v"                      -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_mii_tx_if_pcs.v"                      -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_rx_sync.v"                            -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_sgmii_clk_cntl.v"                     -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_colision_detect.v"                    -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_rx_converter.v"                       -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_rx_fifo_rd.v"                         -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_rx_converter.v"                   -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_sgmii.v"                          -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_sgmii_strx_gx.v"                  -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_tx_converter.v"                   -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_tx_converter.v"                       -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_1000_base_x.v"                    -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_1000_base_x_strx_gx.v"            -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_pcs.v"                            -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_pcs_strx_gx.v"                    -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_rx.v"                             -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_top_tx.v"                             -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_lvds_reset_sequencer.v"               -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_lvds_reverse_loopback.v"              -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_pma_lvds_rx_av.v"                     -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_pma_lvds_rx.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_pma_lvds_tx.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_eth_tse_std_synchronizer.v"               -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_eth_tse_std_synchronizer_bundle.v"        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_eth_tse_ptp_std_synchronizer.v"           -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_false_path_marker.v"                  -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_reset_synchronizer.v"                 -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_clock_crosser.v"                      -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_a_fifo_13.v"                          -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_a_fifo_24.v"                          -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_a_fifo_34.v"                          -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_a_fifo_opt_1246.v"                    -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_a_fifo_opt_14_44.v"                   -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_a_fifo_opt_36_10.v"                   -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_gray_cnt.v"                           -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_sdpm_altsyncram.v"                    -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_altsyncram_dpm_fifo.v"                -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_bin_cnt.v"                            -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog -sv  "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ph_calculator.sv"                     -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_sdpm_gen.v"                           -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_dec_x10.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x10.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x10_wrapper.v"                -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_dec_x14.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x14.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x14_wrapper.v"                -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_dec_x2.v"                         -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x2.v"                         -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x2_wrapper.v"                 -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_dec_x23.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x23.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x23_wrapper.v"                -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_dec_x36.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x36.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x36_wrapper.v"                -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_dec_x40.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x40.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x40_wrapper.v"                -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_dec_x30.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x30.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_enc_x30_wrapper.v"                -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/mentor/altera_tse_ecc_status_crosser.v"                 -work altera_eth_tse_pcs_pma_nf_lvds_1940   
  vlog      "$IP_DIR/../altera_eth_tse_pcs_pma_nf_lvds_1940/sim/altera_std_synchronizer_nocut.v"                        -work altera_eth_tse_pcs_pma_nf_lvds_1940 
