-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- . Implements the functionality of the Subband Correlator (F_sub) in the
--   LOFAR2 SDPFW design.
-- Description:
-- Remark:
-- . Use new_interval to avoid offloading undefined XST data for the first
--   sync interval after an F_sub restart, because then there is no previous
--   sync interval with valid XST data yet.
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, reorder_lib, st_lib, mm_lib, ring_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use ring_lib.ring_pkg.all;
use work.sdp_pkg.all;

entity node_sdp_correlator is
  generic (
    g_sim                    : boolean := false;
    g_sim_sdp                : t_sdp_sim := c_sdp_sim;
    g_P_sq                   : natural := c_sdp_P_sq;
    -- Use no default raw width, to force instance to set it
    g_subband_raw_dat_w      : natural;  -- default: c_sdp_W_subband;
    g_subband_raw_fraction_w : natural  -- default: 0
  );
  port (
    dp_clk        : in  std_logic;
    dp_rst        : in  std_logic;

    in_sosi_arr   : in  t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
    xst_udp_sosi  : out t_dp_sosi;
    xst_udp_siso  : in  t_dp_siso;
    from_ri_sosi  : in  t_dp_sosi := c_dp_sosi_rst;
    to_ri_sosi    : out t_dp_sosi;
    xst_bs_sosi   : out t_dp_sosi;
    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;

    reg_bsn_sync_scheduler_xsub_copi         : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_sync_scheduler_xsub_cipo         : out t_mem_cipo;
    reg_crosslets_info_copi                  : in  t_mem_copi := c_mem_copi_rst;
    reg_crosslets_info_cipo                  : out t_mem_cipo;
    reg_nof_crosslets_copi                   : in  t_mem_copi := c_mem_copi_rst;
    reg_nof_crosslets_cipo                   : out t_mem_cipo;
    ram_st_xsq_copi                          : in  t_mem_copi := c_mem_copi_rst;
    ram_st_xsq_cipo                          : out t_mem_cipo;
    reg_stat_enable_copi                     : in  t_mem_copi := c_mem_copi_rst;
    reg_stat_enable_cipo                     : out t_mem_cipo;
    reg_stat_hdr_dat_copi                    : in  t_mem_copi := c_mem_copi_rst;
    reg_stat_hdr_dat_cipo                    : out t_mem_cipo;
    reg_bsn_align_copi                       : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_align_cipo                       : out t_mem_cipo;
    reg_bsn_monitor_v2_bsn_align_input_copi  : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_bsn_align_input_cipo  : out t_mem_cipo;
    reg_bsn_monitor_v2_bsn_align_output_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_bsn_align_output_cipo : out t_mem_cipo;
    reg_bsn_monitor_v2_xst_offload_copi      : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_xst_offload_cipo      : out t_mem_cipo;

    sdp_info          : in t_sdp_info;
    ring_info         : in t_ring_info;
    gn_id             : in std_logic_vector(c_sdp_W_gn_id - 1 downto 0);
    stat_eth_src_mac  : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    stat_ip_src_addr  : in std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    stat_udp_src_port : in std_logic_vector(c_network_udp_port_w - 1 downto 0)
  );
end node_sdp_correlator;

architecture str of node_sdp_correlator is
  constant c_nof_controllers      : positive := 2;

  -- crosslet statistics offload
  signal ram_st_offload_copi           : t_mem_copi := c_mem_copi_rst;
  signal ram_st_offload_cipo           : t_mem_cipo := c_mem_cipo_rst;

  signal controller_mem_mux_copi       : t_mem_copi := c_mem_copi_rst;
  signal controller_mem_mux_cipo       : t_mem_cipo := c_mem_cipo_rst;
  signal controller_copi_arr           : t_mem_copi_arr(0 to c_nof_controllers - 1) := (others => c_mem_copi_rst);
  signal controller_cipo_arr           : t_mem_cipo_arr(0 to c_nof_controllers - 1) := (others => c_mem_cipo_rst);

  signal quant_sosi_arr                : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal xsel_sosi                     : t_dp_sosi := c_dp_sosi_rst;
  signal new_interval                  : std_logic;
  signal ctrl_interval_size            : natural;
  signal offload_interval_size         : natural;

  signal crosslets_sosi                : t_dp_sosi  := c_dp_sosi_rst;
  signal crosslets_copi                : t_mem_copi := c_mem_copi_rst;
  signal crosslets_cipo_arr            : t_mem_cipo_arr(g_P_sq - 1 downto 0) := (others => c_mem_cipo_rst);
  signal mon_xst_udp_sosi_arr          : t_dp_sosi_arr(0 downto 0) := (others => c_dp_sosi_rst);

  signal prev_crosslets_info_rec       : t_sdp_crosslets_info;
  signal nof_crosslets_reg             : std_logic_vector(c_sdp_nof_crosslets_reg_w - 1 downto 0);
  signal nof_crosslets                 : std_logic_vector(c_sdp_nof_crosslets_reg_w - 1 downto 0);
begin
  ---------------------------------------------------------------
  -- Requantize 18b to 16b
  ---------------------------------------------------------------
  gen_requantize : for I in 0 to c_sdp_P_pfb - 1 generate
    u_dp_requantize : entity dp_lib.dp_requantize
    generic map (
      g_complex             => true,
      g_representation      => "SIGNED",
      g_lsb_w               => g_subband_raw_fraction_w,
      g_lsb_round           => true,  -- round subband fraction
      g_lsb_round_clip      => false,
      g_msb_clip            => true,  -- clip subband overflow
      g_msb_clip_symmetric  => false,
      g_pipeline_remove_lsb => 1,
      g_pipeline_remove_msb => 1,
      g_in_dat_w            => g_subband_raw_dat_w,
      g_out_dat_w           => c_sdp_W_crosslet
    )
    port map(
      rst => dp_rst,
      clk => dp_clk,

      snk_in  => in_sosi_arr(I),
      src_out => quant_sosi_arr(I)
    );
  end generate;

  ---------------------------------------------------------------
  -- Crosslet Subband Select
  ---------------------------------------------------------------
  u_crosslets_subband_select : entity work.sdp_crosslets_subband_select
  generic map (
    g_N_crosslets            => c_sdp_N_crosslets_max,
    g_ctrl_interval_size_min => sel_a_b(g_sim, g_sim_sdp.N_clk_per_sync_min, c_sdp_N_clk_per_sync_min)
  )
  port map(
    dp_clk         => dp_clk,
    dp_rst         => dp_rst,

    in_sosi_arr    => quant_sosi_arr,
    out_sosi       => xsel_sosi,

    new_interval       => new_interval,
    ctrl_interval_size => ctrl_interval_size,

    mm_rst         => mm_rst,
    mm_clk         => mm_clk,

    reg_crosslets_info_mosi => reg_crosslets_info_copi,
    reg_crosslets_info_miso => reg_crosslets_info_cipo,

    reg_bsn_sync_scheduler_xsub_mosi => reg_bsn_sync_scheduler_xsub_copi,
    reg_bsn_sync_scheduler_xsub_miso => reg_bsn_sync_scheduler_xsub_cipo,

    cur_crosslets_info_rec => OPEN,
    prev_crosslets_info_rec => prev_crosslets_info_rec
  );

  -- Use xsel_sosi as local bsn and sync reference since the sync
  -- is generated by the bsn_sync_scheduler in sdp_crosslets_subband_select.
  xst_bs_sosi <= xsel_sosi;

  ---------------------------------------------------------------
  -- Local and remote crosslets
  ---------------------------------------------------------------
  u_sdp_crosslets_remote : entity work.sdp_crosslets_remote_v2
  generic map (
    g_P_sq => g_P_sq
  )
  port map (
    dp_clk             => dp_clk,
    dp_rst             => dp_rst,

    xsel_sosi          => xsel_sosi,
    from_ri_sosi       => from_ri_sosi,
    to_ri_sosi         => to_ri_sosi,

    crosslets_sosi     => crosslets_sosi,
    crosslets_copi     => crosslets_copi,
    crosslets_cipo_arr => crosslets_cipo_arr,

    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    reg_bsn_align_copi                       => reg_bsn_align_copi,
    reg_bsn_align_cipo                       => reg_bsn_align_cipo,
    reg_bsn_monitor_v2_bsn_align_input_copi  => reg_bsn_monitor_v2_bsn_align_input_copi,
    reg_bsn_monitor_v2_bsn_align_input_cipo  => reg_bsn_monitor_v2_bsn_align_input_cipo,
    reg_bsn_monitor_v2_bsn_align_output_copi => reg_bsn_monitor_v2_bsn_align_output_copi,
    reg_bsn_monitor_v2_bsn_align_output_cipo => reg_bsn_monitor_v2_bsn_align_output_cipo
  );

  ---------------------------------------------------------------
  -- Crosslets Statistics (XST)
  ---------------------------------------------------------------
  u_crosslets_stats : entity st_lib.st_xst
  generic map(
    g_nof_streams       => g_P_sq,
    g_nof_crosslets     => c_sdp_N_crosslets_max,
    g_nof_signal_inputs => c_sdp_S_pn,
    g_in_data_w         => c_sdp_W_crosslet,
    g_stat_data_w       => c_longword_w,
    g_stat_data_sz      => c_longword_sz / c_word_sz
  )
  port map (
    mm_rst          => mm_rst,
    mm_clk          => mm_clk,
    dp_rst          => dp_rst,
    dp_clk          => dp_clk,
    in_sosi         => crosslets_sosi,
    mm_mosi         => crosslets_copi,
    mm_miso_arr     => crosslets_cipo_arr,

    ram_st_xsq_mosi => controller_mem_mux_copi,
    ram_st_xsq_miso => controller_mem_mux_cipo
  );

  ---------------------------------------------------------------
  -- MM controller multiplexer
  ---------------------------------------------------------------
  -- Connect 2 mm_controllers to the common_mem_mux output
  controller_copi_arr(0)  <= ram_st_xsq_copi;  -- MM access via QSYS MM bus
  controller_copi_arr(1)  <= ram_st_offload_copi;  -- MM access by UDP offload
  ram_st_xsq_cipo     <= controller_cipo_arr(0);
  ram_st_offload_cipo <= controller_cipo_arr(1);

  u_mem_controller_mux : entity mm_lib.mm_master_mux
  generic map (
    g_nof_masters    => c_nof_controllers,
    g_rd_latency_min => 1  -- read latency of statistics RAM is 1
  )
  port map (
    mm_clk => mm_clk,

    master_mosi_arr => controller_copi_arr,
    master_miso_arr => controller_cipo_arr,
    mux_mosi        => controller_mem_mux_copi,
    mux_miso        => controller_mem_mux_cipo
  );

  ---------------------------------------------------------------
  -- REG_NOF_CROSSLETS
  ---------------------------------------------------------------
  u_nof_crosslets : entity common_lib.mms_common_reg
  generic map(
    g_mm_reg => c_sdp_mm_reg_nof_crosslets
  )
  port map(
    -- Clocks and reset
    mm_rst => mm_rst,
    mm_clk => mm_clk,
    st_rst => dp_rst,
    st_clk => dp_clk,

    -- MM bus access in memory-mapped clock domain
    reg_mosi => reg_nof_crosslets_copi,
    reg_miso => reg_nof_crosslets_cipo,

    in_reg   => nof_crosslets,
    out_reg  => nof_crosslets_reg
  );

  -- Force nof crosslets to max nof crosslets if a higher value is written or to 1 if a lower value is written via MM.
  nof_crosslets <= TO_UVEC(1, c_sdp_nof_crosslets_reg_w) when TO_UINT(nof_crosslets_reg) < 1 else
                   nof_crosslets_reg when TO_UINT(nof_crosslets_reg) <= c_sdp_N_crosslets_max else
                   TO_UVEC(c_sdp_N_crosslets_max, c_sdp_nof_crosslets_reg_w);

  ---------------------------------------------------------------
  -- XST UDP offload
  ---------------------------------------------------------------
  xst_udp_sosi <= mon_xst_udp_sosi_arr(0);

  offload_interval_size <= sel_a_b(g_sim, g_sim_sdp.N_clk_per_sync, ctrl_interval_size);

  u_sdp_xst_udp_offload: entity work.sdp_statistics_offload
  generic map (
    g_statistics_type          => "XST",
    g_offload_node_time        => sel_a_b(g_sim, g_sim_sdp.offload_node_time, c_sdp_offload_node_time),
    g_offload_packet_time      => sel_a_b(g_sim, g_sim_sdp.offload_packet_time, c_sdp_offload_packet_time),
    g_offload_scale_w          => sel_a_b(g_sim, g_sim_sdp.offload_scale_w, c_sdp_offload_scale_w),
    g_ctrl_interval_size_min   => sel_a_b(g_sim, g_sim_sdp.N_clk_per_sync_min, c_sdp_N_clk_per_sync_min),
    g_P_sq                     => g_P_sq,
    g_crosslets_direction      => 1,  -- = lane direction
    g_bsn_monitor_sync_timeout => c_sdp_N_clk_sync_timeout_xsub
  )
  port map (
    mm_clk    => mm_clk,
    mm_rst    => mm_rst,

    dp_clk    => dp_clk,
    dp_rst    => dp_rst,

    master_mosi => ram_st_offload_copi,
    master_miso => ram_st_offload_cipo,

    reg_enable_mosi  => reg_stat_enable_copi,
    reg_enable_miso  => reg_stat_enable_cipo,

    reg_hdr_dat_mosi => reg_stat_hdr_dat_copi,
    reg_hdr_dat_miso => reg_stat_hdr_dat_cipo,

    reg_bsn_monitor_v2_offload_copi => reg_bsn_monitor_v2_xst_offload_copi,
    reg_bsn_monitor_v2_offload_cipo => reg_bsn_monitor_v2_xst_offload_cipo,

    in_sosi      => crosslets_sosi,
    new_interval => new_interval,
    ctrl_interval_size => offload_interval_size,

    out_sosi  => mon_xst_udp_sosi_arr(0),
    out_siso  => xst_udp_siso,

    eth_src_mac    => stat_eth_src_mac,
    udp_src_port   => stat_udp_src_port,
    ip_src_addr    => stat_ip_src_addr,

    gn_index       => TO_UINT(gn_id),
    ring_info      => ring_info,
    sdp_info       => sdp_info,
    weighted_subbands_flag => '1',  -- because XSub uses in_sosi_arr = fsub_sosi_arr, so weighted subbands

    nof_crosslets           => nof_crosslets,  -- from MM
    prev_crosslets_info_rec => prev_crosslets_info_rec
  );
end str;
