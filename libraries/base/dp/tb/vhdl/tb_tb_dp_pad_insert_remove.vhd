-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_pad_insert_remove is
end tb_tb_dp_pad_insert_remove;

architecture tb of tb_tb_dp_pad_insert_remove is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 2
  -- > run -all --> OK

  --                                                                    g_data_w, g_symbol_w, g_nof_symbols, g_nof_padding, g_in_en,  g_out_ready
  u_act_act          : entity work.tb_dp_pad_insert_remove generic map (64,       8,          293,            2,             e_active, e_active);
  u_rnd_act          : entity work.tb_dp_pad_insert_remove generic map (64,       8,          171,            2,             e_random, e_active);
  u_rnd_rnd          : entity work.tb_dp_pad_insert_remove generic map (64,       8,          172,            2,             e_random, e_random);
  u_act_rnd          : entity work.tb_dp_pad_insert_remove generic map (64,       8,          173,            2,             e_active, e_random);
  u_pls_pls          : entity work.tb_dp_pad_insert_remove generic map (64,       8,           51,            2,             e_pulse,  e_pulse);
  u_rnd_pls          : entity work.tb_dp_pad_insert_remove generic map (64,       8,           52,            2,             e_random, e_pulse);
  u_pls_rnd          : entity work.tb_dp_pad_insert_remove generic map (64,       8,           53,            2,             e_pulse,  e_random);
end tb;
