--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Deinterleave the input into g_nof_out outputs, outputting g_block_size_int words before switching to next output.
-- Description:
-- . DP wrapper for common_deinterleave
--
-- Status (erko):
-- . Obselete, do not use in new designs. Instead use:
--   - dp_deinterleave_one_to_n.vhd for 1 to N, preserves all sosi fields
--   - dp_unfolder.vhd for 1 to power of 2, but only preserves the valid
-- . The tb_tb_dp_deinterleave_interleave_to_one.vhd shows that dp_deinterleave.vhd fails if the
--   snk_in.valid is random.

entity dp_deinterleave is
  generic (
    g_dat_w             : natural;
    g_nof_out           : natural;
    g_block_size_int    : natural;  -- Deinterleaver block size; outputs g_block_size_int words before switching to next output
    g_block_size_output : natural;  -- Output block size: The number of samles in the blocks at the output
    g_use_ctrl          : boolean := true;  -- Requires: [input block size (sop:eop)] / [g_nof_out]/ [g_block_size_output] = integer number!
    g_use_sync_bsn      : boolean := true;  -- forwards (stored) input Sync+BSN to all output streams
    g_use_complex       : boolean;
    g_align_out         : boolean := false  -- Aligns the output streams
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_in      : in  t_dp_sosi;
    src_out_arr : out t_dp_sosi_arr(g_nof_out - 1 downto 0)
  );
end dp_deinterleave;

architecture wrap of dp_deinterleave is
  constant c_complex_w  : natural := g_dat_w / 2;

  signal common_deinterleave_in_dat  : std_logic_vector(g_dat_w - 1 downto 0);
  signal common_deinterleave_in_val  : std_logic;
  signal common_deinterleave_out_dat : std_logic_vector(g_nof_out * g_dat_w - 1 downto 0);
  signal common_deinterleave_out_val : std_logic_vector(g_nof_out - 1 downto 0);

  signal common_deinterleave_src_out_arr : t_dp_sosi_arr(g_nof_out - 1 downto 0);
  signal dp_block_gen_src_out_arr        : t_dp_sosi_arr(g_nof_out - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- DP wrapper around common_deinterleave
  -----------------------------------------------------------------------------
  gen_sosi_dat_in: if g_use_complex = false generate
    common_deinterleave_in_dat <= snk_in.data(g_dat_w - 1 downto 0);
  end generate;

  gen_sosi_compl_in: if g_use_complex = true generate
    common_deinterleave_in_dat <= snk_in.im(c_complex_w - 1 downto 0) & snk_in.re(c_complex_w - 1 downto 0);
  end generate;

  common_deinterleave_in_val <= snk_in.valid;

  u_deinterleave : entity common_lib.common_deinterleave
  generic map (
    g_nof_out    => g_nof_out,
    g_block_size => g_block_size_int,
    g_dat_w      => g_dat_w,
    g_align_out  => g_align_out
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_dat     => common_deinterleave_in_dat,
    in_val     => common_deinterleave_in_val,

    out_dat    => common_deinterleave_out_dat,
    out_val    => common_deinterleave_out_val
  );

  -----------------------------------------------------------------------------
  -- Use complex fields if required
  -----------------------------------------------------------------------------
  gen_wires_out : for i in 0 to g_nof_out - 1 generate
    gen_sosi_dat_out: if g_use_complex = false generate
      common_deinterleave_src_out_arr(i).data(g_dat_w - 1 downto 0) <= common_deinterleave_out_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w);
    end generate;

    gen_sosi_compl_out: if g_use_complex = true generate
      common_deinterleave_src_out_arr(i).im <= RESIZE_DP_DSP_DATA(common_deinterleave_out_dat( i * g_dat_w + g_dat_w     - 1 downto i * g_dat_w + c_complex_w));
      common_deinterleave_src_out_arr(i).re <= RESIZE_DP_DSP_DATA(common_deinterleave_out_dat( i * g_dat_w + c_complex_w - 1 downto i * g_dat_w));
    end generate;

    common_deinterleave_src_out_arr(i).valid <= common_deinterleave_out_val(i);
  end generate;

  -----------------------------------------------------------------------------
  -- Add SOP and EOP to the outputs
  -----------------------------------------------------------------------------
  gen_ctrl : if g_use_ctrl = true generate
    gen_dp_block_gen : for i in 0 to g_nof_out - 1 generate
      u_dp_block_gen : entity work.dp_block_gen
      generic map (
        g_use_src_in       => false,
        g_nof_data         => g_block_size_output,
        g_preserve_sync    => true,
        g_preserve_bsn     => true
      )
      port map(
        rst        => rst,
        clk        => clk,

        snk_in     => common_deinterleave_src_out_arr(i),
        src_out    => dp_block_gen_src_out_arr(i)
      );
    end generate;
  end generate;

  no_ctrl : if g_use_ctrl = false generate
    dp_block_gen_src_out_arr <= common_deinterleave_src_out_arr;
  end generate;

  -----------------------------------------------------------------------------
  -- Re-add input sync + BSN to all output streams
  -----------------------------------------------------------------------------
  align_out : if g_use_sync_bsn = true generate
    gen_dp_fifo_info: for i in 0 to g_nof_out - 1 generate
      u_dp_fifo_info : entity work.dp_fifo_info
      generic map (
        g_use_sync => true,
        g_use_bsn  => true
      )
      port map (
        rst          => rst,
        clk          => clk,

        data_snk_in  => dp_block_gen_src_out_arr(i),  -- delayed snk_in data
        info_snk_in  => snk_in,  -- original snk_in info

        src_in       => c_dp_siso_rdy,
        src_out      => src_out_arr(i)
      );
    end generate;
  end generate;

  no_align_out : if g_use_sync_bsn = false generate
    src_out_arr <= dp_block_gen_src_out_arr;
  end generate;
end wrap;
