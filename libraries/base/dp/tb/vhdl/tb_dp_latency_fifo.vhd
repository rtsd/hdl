-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

-- Purpose: Verify dp_latency_fifo
-- Description:
--   p_stimuli --> dp_latency_fifo --> verify
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

entity tb_dp_latency_fifo is
  generic (
    g_bypass                : boolean := false;  -- for clarity rather use g_bypass=TRUE than equivalent g_fifo_size=0
    g_input_rl              : natural := 1;  -- input ready latency
    g_output_rl             : natural := 1;  -- output ready latency
    g_fifo_size             : natural := 1;
    g_nof_symbols_per_block : natural := 51;
    g_nof_symbols_per_data  : natural := 4;
    g_in_en                 : t_dp_flow_control_enum := e_pulse;  -- always e_active, e_random or e_pulse flow control
    g_out_ready             : t_dp_flow_control_enum := e_random  -- always e_active, e_random or e_pulse flow control
  );
end tb_dp_latency_fifo;

architecture tb of tb_dp_latency_fifo is
  constant c_nof_data_per_block       : natural := ceil_div(g_nof_symbols_per_block, g_nof_symbols_per_data);

  -- tb default
  constant c_pulse_0_active           : natural := c_nof_data_per_block;
  constant c_pulse_0_gap              : natural := 10;
  constant c_pulse_0_period           : natural := c_nof_data_per_block + c_pulse_0_gap;

  constant c_pulse_1_active           : natural := 1;
  constant c_pulse_1_period           : natural := 7;

  -- tb specific
  constant c_nof_repeat               : natural := 20;
  constant c_symbol_w                 : natural := 8;
  constant c_data_w                   : natural := g_nof_symbols_per_data * c_symbol_w;
  constant c_symbol_init              : natural := 0;
  constant c_symbol_mod               : integer := 2**c_symbol_w;  -- used to avoid TO_UVEC warning for smaller c_symbol_w : "NUMERIC_STD.TO_UNSIGNED: vector truncated"

  -- tb default
  signal tb_end               : std_logic := '0';
  signal clk                  : std_logic := '1';
  signal rst                  : std_logic := '1';

  signal random_0             : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1             : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0              : std_logic;
  signal pulse_1              : std_logic;
  signal pulse_en             : std_logic := '1';

  signal in_en                : std_logic := '1';

  -- tb verify
  signal verify_en            : std_logic := '0';
  signal verify_done          : std_logic := '0';

  signal prev_out_ready       : std_logic_vector(0 to g_output_rl);
  signal prev_out_data        : std_logic_vector(c_data_w - 1 downto 0);

  signal expected_out_data    : std_logic_vector(c_data_w - 1 downto 0);

  -- tb dut
  signal in_siso              : t_dp_siso;
  signal in_sosi              : t_dp_sosi;
  signal in_data              : std_logic_vector(c_data_w - 1 downto 0);
  signal in_sync              : std_logic;
  signal in_val               : std_logic;
  signal in_sop               : std_logic;
  signal in_eop               : std_logic;

  signal fifo_usedw           : std_logic_vector(ceil_log2(2 + g_input_rl + g_fifo_size) - 1 downto 0);  -- +2+g_input_rl to match dp_latency_fifo usedw width
  signal fifo_ful             : std_logic;
  signal fifo_emp             : std_logic;

  signal out_siso             : t_dp_siso := c_dp_siso_hold;  -- default xon = '1' is needed for proc_dp_gen_block_data()
  signal out_sosi             : t_dp_sosi;
  signal out_data             : std_logic_vector(c_data_w - 1 downto 0);
  signal out_sync             : std_logic;
  signal out_val              : std_logic;
  signal out_sop              : std_logic;
  signal out_eop              : std_logic;

  signal out_gap              : std_logic := '1';
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_0_active, c_pulse_0_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_1_active, c_pulse_1_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en          <= '1'                     when g_in_en = e_active      else
                    random_0(random_0'high) when g_in_en = e_random      else
                    pulse_0                 when g_in_en = e_pulse;

  out_siso.ready <= '1'                     when g_out_ready = e_active  else
                    random_1(random_1'high) when g_out_ready = e_random  else
                    pulse_1                 when g_out_ready = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli : process
    variable v_sync      : std_logic := '0';
    variable v_bsn       : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := (others => '0');
    variable v_channel   : natural := 0;
    variable v_err       : natural := 0;
    variable v_symbol    : natural := c_symbol_init;
  begin
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Begin of stimuli
    for R in 0 to c_nof_repeat - 1 loop
      proc_dp_gen_block_data(g_input_rl, true, c_data_w, c_symbol_w, v_symbol, 0, 0, g_nof_symbols_per_block, v_channel, v_err, v_sync, v_bsn, clk, in_en, in_siso, in_sosi);
      v_bsn     := INCR_UVEC(v_bsn, 1);
      v_symbol  := (v_symbol + g_nof_symbols_per_block) mod c_symbol_mod;
      v_channel := v_channel + 1;
      v_err     := v_err + 1;
      --proc_common_wait_some_cycles(clk, 10);               -- create gap between frames
    end loop;

    -- End of stimuli
    expected_out_data    <= in_sosi.data(c_data_w - 1 downto 0);

    proc_common_wait_some_cycles(clk, 50);  -- depends on stream control
    verify_done <= '1';
    proc_common_wait_some_cycles(clk, 1);
    verify_done <= '0';
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  verify_en <= '1' when rising_edge(clk) and out_sosi.sop = '1';  -- verify enable after first output sop

  -- SOSI control
  proc_dp_verify_valid(g_output_rl, clk, verify_en, out_siso.ready, prev_out_ready, out_val);  -- Verify that the output valid fits with the output ready latency
  proc_dp_verify_gap_invalid(clk, out_val, out_sop, out_eop, out_gap);  -- Verify that the output valid is low between blocks from eop to sop

  -- SOSI data
  -- . verify that the output is incrementing symbols, like the input stimuli
  proc_dp_verify_symbols(g_output_rl, c_data_w, c_symbol_w, clk, verify_en, out_siso.ready, out_sosi.valid, out_sosi.eop, out_data, out_sosi.empty, prev_out_data);

  -- . verify that the stimuli have been applied at all
  proc_dp_verify_value(e_equal, clk, verify_done, expected_out_data, out_data);

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  dut : entity work.dp_latency_fifo
  generic map (
    g_bypass     => g_bypass,
    g_input_rl   => g_input_rl,  -- input ready latency
    g_output_rl  => g_output_rl,  -- output ready latency
    g_fifo_size  => g_fifo_size
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- Monitor FIFO filling
    usedw       => fifo_usedw,
    wr_ful      => fifo_ful,
    rd_emp      => fifo_emp,
    -- ST sink
    snk_in      => in_sosi,
    snk_out     => in_siso,
    -- ST source
    src_out     => out_sosi,
    src_in      => out_siso
  );

  -- Map to slv to ease monitoring in wave window
  in_data     <= in_sosi.data(c_data_w - 1 downto 0);
  in_val      <= in_sosi.valid;
  in_sop      <= in_sosi.sop;
  in_eop      <= in_sosi.eop;
  in_sync     <= in_sosi.sync;

  out_data    <= out_sosi.data(c_data_w - 1 downto 0);
  out_val     <= out_sosi.valid;
  out_sop     <= out_sosi.sop;
  out_eop     <= out_sosi.eop;
  out_sync    <= out_sosi.sync;
end tb;
