###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

set_location_assignment PIN_M4 -to SI_FN_0_TX[0]
set_location_assignment PIN_K4 -to SI_FN_0_TX[1]
set_location_assignment PIN_D4 -to SI_FN_0_TX[2]
set_location_assignment PIN_B4 -to SI_FN_0_TX[3]
set_location_assignment PIN_N2 -to SI_FN_0_RX[0]
set_location_assignment PIN_L2 -to SI_FN_0_RX[1]
set_location_assignment PIN_E2 -to SI_FN_0_RX[2]
set_location_assignment PIN_C2 -to SI_FN_0_RX[3]

set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_TX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_TX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_TX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_TX[3]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_RX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_RX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_RX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SI_FN_0_RX[3]

