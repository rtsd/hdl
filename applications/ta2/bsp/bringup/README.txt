(C) 1992-2018 Intel Corporation.                            
Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
and/or other countries. Other marks and brands may be claimed as the property  
of others. See Trademarks on intel.com for full list of Intel trademarks or    
the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
Your use of Intel Corporation's design tools, logic functions and other        
software and tools, and its AMPP partner logic functions, and any output       
files any of the foregoing (including device programming or simulation         
files), and any associated documentation or information are expressly subject  
to the terms and conditions of the Altera Program License Subscription         
Agreement, Intel MegaCore Function License Agreement, or other applicable      
license agreement, including, without limitation, that your use is for the     
sole purpose of programming logic devices manufactured by Intel and sold by    
Intel or its authorized distributors.  Please refer to the applicable          
agreement for further details.                                                 


Initializing the A10 GX Development Kit for OpenCL Use
======================================================

The A10 GX Development Kit is targetted for generic FPGA development and
therefore is not shipped ready for use with OpenCL.  An Intel FAE should
normally perform all bringup/initialization steps so that the A10 Dev Kit
works out of the box, meaning a user would need to only: plug the A10 dev kit
board into a machine's PCIe slot, run "aocl install", and then successfully
run "aocl diagnose".

In the event that an Intel FAE is unable to carry out the task, an end-user
(at their own risk) can follow the directions found in the document

"Configuring the Intel Arria 10 GX FPGA Development Kit for the Intel FPGA SDK for OpenCL" 

which can be downloaded from here:

https://www.altera.com/documentation/tgy1490191698959.html 

to prepare the board for OpenCL use.

********************************************************
* WARNING: This process requires hardware assembly and *
* familiarity with Quartus.  In addition there is real *
* risk that the card be damaged during an attempt to   *
* initialize it for OpenCL use.                        *
********************************************************

It is strongly advised that this initialization procedure is carried out by 
an Intel FAE only. Service requests should be placed to carry out or assist
with the A10 Dev Kit initialization for OpenCL.

