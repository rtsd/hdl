-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer
-- Description:
--   Dual clock domain with different aspect ratio data widhts
--   Use port a only for write in write clock domain
--   Use port b only for read in read clock domain
-- Remark:
--   Because the Agilex 7 (agi027_1e1v) does not support the crwk_crw IP,
--   and unfortunately, the rwk_rw IP isn't supported either, the crk_cw IP
--   has been created, resulting in modifications to this file.[1]
-- Reference:
--   [1] Based on the structure of common_crw_crw_ratio.vhd.

library IEEE, technology_lib, tech_memory_lib;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_ram_cr_cw_ratio is
  generic (
    g_technology : natural := c_tech_select_default;
    g_ram_wr     : t_c_mem := c_mem_ram;  -- settings for port a
    g_ram_rd     : t_c_mem := c_mem_ram;  -- data width and address range for port b
    g_init_file  : string := "UNUSED"
  );
  port (
    -- Write port clock domain
    wr_rst    : in  std_logic := '0';
    wr_clk    : in  std_logic;
    wr_clken  : in  std_logic := '1';
    wr_en     : in  std_logic := '0';
    wr_adr    : in  std_logic_vector(g_ram_wr.adr_w - 1 downto 0) := (others => '0');
    wr_dat    : in  std_logic_vector(g_ram_wr.dat_w - 1 downto 0) := (others => '0');
    -- Read port clock domain
    rd_rst    : in  std_logic := '0';
    rd_clk    : in  std_logic;
    rd_clken  : in  std_logic := '1';
    rd_en     : in  std_logic := '1';
    rd_adr    : in  std_logic_vector(g_ram_rd.adr_w - 1 downto 0) := (others => '0');
    rd_dat    : out std_logic_vector(g_ram_rd.dat_w - 1 downto 0);
    rd_val    : out std_logic
  );
end common_ram_cr_cw_ratio;

architecture str of common_ram_cr_cw_ratio is
  constant c_ram        : t_c_mem := g_ram_wr;  -- use shared parameters from port wr (a) parameter

  constant c_rd_latency : natural := sel_a_b(c_ram.latency < 2,            c_ram.latency,              2);  -- handle read latency 1 or 2 in RAM
  constant c_pipeline   : natural := sel_a_b(c_ram.latency > c_rd_latency, c_ram.latency - c_rd_latency, 0);  -- handle rest of read latency > 2 in pipeline

  -- Intermediate signal for extra pipelining
  signal ram_rd_dat     : std_logic_vector(rd_dat'range);

  -- Map sl to single bit slv for rd_val pipelining
  signal ram_rd_en      : std_logic_vector(0 downto 0);
  signal ram_rd_val     : std_logic_vector(0 downto 0);

begin
  assert c_ram.latency >= 1
    report "common_ram_cr_cw_ratio : only support read latency >= 1"
    severity FAILURE;

  assert g_ram_wr.latency = g_ram_rd.latency
    report "common_ram_cr_cw_ratio : only support same read latency for both ports"
    severity FAILURE;

  -- memory access
  u_ramk : entity tech_memory_lib.tech_memory_ram_crk_cw
  generic map (
    g_technology   => g_technology,
    g_wr_adr_w     => g_ram_wr.adr_w,
    g_rd_adr_w     => g_ram_rd.adr_w,
    g_wr_dat_w     => g_ram_wr.dat_w,
    g_rd_dat_w     => g_ram_rd.dat_w,
    g_wr_nof_words => g_ram_wr.nof_dat,
    g_rd_nof_words => g_ram_rd.nof_dat,
    g_rd_latency   => c_rd_latency,
    g_init_file    => g_init_file
  )
  port map (
    wrclock     => wr_clk,
    rdclock     => rd_clk,
    wrclocken   => wr_clken,
    rdclocken   => rd_clken,
    wren        => wr_en,
    data        => wr_dat,
    wraddress   => wr_adr,
    rdaddress   => rd_adr,
    q           => ram_rd_dat
  );

  -- read output
  u_pipe : entity work.common_pipeline
  generic map (
    g_pipeline   => c_pipeline,
    g_in_dat_w   => g_ram_rd.dat_w,
    g_out_dat_w  => g_ram_rd.dat_w
  )
  port map (
    clk     => rd_clk,
    clken   => rd_clken,
    in_dat  => ram_rd_dat,
    out_dat => rd_dat
  );

  -- rd_val control
  ram_rd_en(0) <= rd_en;

  rd_val <= ram_rd_val(0);

  u_rd_val : entity work.common_pipeline
  generic map (
    g_pipeline   => c_ram.latency,
    g_in_dat_w   => 1,
    g_out_dat_w  => 1
  )
  port map (
    clk     => rd_clk,
    clken   => rd_clken,
    in_dat  => ram_rd_en,
    out_dat => ram_rd_val
  );
end str;
