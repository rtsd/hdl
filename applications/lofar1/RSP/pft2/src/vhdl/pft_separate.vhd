-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: pipelined FFT
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pft_separate is
  generic (
    g_fft_sz          : natural;
    g_fft_sz_w        : natural;
    g_rd_dat_w        : natural;
    g_out_dat_w       : natural
  );
  port (
    rddata_re         : in  std_logic_vector(g_rd_dat_w - 1 downto 0);
    rddata_im         : in  std_logic_vector(g_rd_dat_w - 1 downto 0);
    rdaddr            : out std_logic_vector(g_fft_sz_w - 1 downto 0);
    rden              : out std_logic;
    rdval             : in  std_logic;
    rdsync            : in  std_logic;
    out_dat_re        : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_dat_im        : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val           : out std_logic;
    out_sync          : out std_logic;
    page_rdy          : in  std_logic;
    clk               : in  std_logic;
    rst               : in  std_logic
  );
end pft_separate;

architecture rtl of pft_separate is
  constant c_reg_delay     : natural := 2;
  constant c_add_delay     : natural := 2;
  constant c_tot_delay     : natural := c_reg_delay + c_add_delay;

  type data_dly_arr is array (natural range <>) of std_logic_vector(rddata_re'range);

  signal nxt_rden          : std_logic;

  signal cnt               : std_logic_vector(rdaddr'high downto 0);
  signal nxt_cnt           : std_logic_vector(cnt'range);

  signal rddata_re_dly     : data_dly_arr(0 to c_reg_delay);
  signal rddata_im_dly     : data_dly_arr(0 to c_reg_delay);

  signal rd_cnt            : std_logic_vector(g_fft_sz_w - 1 downto 0);
  signal nxt_rd_cnt        : std_logic_vector(rd_cnt'range);

  signal page_rdy_dly      : std_logic_vector( 0 to c_tot_delay - 1);
  signal rdval_dly         : std_logic_vector( 0 to c_tot_delay - 1);
  signal rdsync_dly        : std_logic_vector( 0 to c_tot_delay + 1);
  signal nxt_rdsync_dly    : std_logic_vector(rdsync_dly'range);
  signal rdsync_reg        : std_logic;
  signal nxt_rdsync_reg    : std_logic;

  signal nxt_out_dat_re    : std_logic_vector(out_dat_re'range);
  signal nxt_out_dat_im    : std_logic_vector(out_dat_im'range);
  signal nxt_out_val       : std_logic;
  signal nxt_out_sync      : std_logic;

  signal add_out           : std_logic_vector(out_dat_re'range);
  signal add0              : std_logic_vector(rddata_re'range);
  signal add1              : std_logic_vector(rddata_re'range);
  signal nxt_add0          : std_logic_vector(rddata_re'range);
  signal nxt_add1          : std_logic_vector(rddata_re'range);
  signal sub_out           : std_logic_vector(out_dat_im'range);
  signal sub0              : std_logic_vector(rddata_re'range);
  signal sub1              : std_logic_vector(rddata_re'range);
  signal nxt_sub0          : std_logic_vector(rddata_re'range);
  signal nxt_sub1          : std_logic_vector(rddata_re'range);
begin
  registers : process (rst, clk)
  begin
    if rst = '1' then
      -- Output signals.
      out_dat_re    <= (others => '0');
      out_dat_im    <= (others => '0');
      out_val       <= '0';
      out_sync      <= '0';
      rden          <= '0';
      -- Internal signals.
      cnt           <= std_logic_vector(to_signed(-2, g_fft_sz_w));
      rd_cnt        <= (others => '0');
      page_rdy_dly  <= (others => '0');
      rddata_re_dly <= (others => (others => '0'));
      rddata_im_dly <= (others => (others => '0'));
      rdval_dly     <= (others => '0');
      rdsync_dly    <= (others => '0');
      rdsync_reg    <= '0';
      add0          <= (others => '0');
      add1          <= (others => '0');
      sub0          <= (others => '0');
      sub1          <= (others => '0');
    elsif rising_edge(clk) then
      -- Output signals.
      out_dat_re    <= nxt_out_dat_re;
      out_dat_im    <= nxt_out_dat_im;
      out_val       <= nxt_out_val;
      out_sync      <= nxt_out_sync;
      rden          <= nxt_rden;
      -- Internal signals.
      cnt           <= nxt_cnt;
      rd_cnt        <= nxt_rd_cnt;
      page_rdy_dly  <= page_rdy & page_rdy_dly(0 to page_rdy_dly'high - 1);
      rddata_re_dly <= rddata_re & rddata_re_dly(0 to rddata_re_dly'high - 1);
      rddata_im_dly <= rddata_im & rddata_im_dly(0 to rddata_im_dly'high - 1);
      rdval_dly     <= rdval  & rdval_dly(0 to rdval_dly'high - 1);
      rdsync_dly    <= nxt_rdsync_dly;
      rdsync_reg    <= nxt_rdsync_reg;
      add0          <= nxt_add0;
      add1          <= nxt_add1;
      sub0          <= nxt_sub0;
      sub1          <= nxt_sub1;
    end if;
  end process;

  sync_proc : process(page_rdy, rdsync, rdsync_dly, rdsync_reg)
  begin
    nxt_rdsync_reg <= rdsync_reg;
    nxt_rdsync_dly <= '0' & rdsync_dly(0 to rdsync_dly'high - 1);
    if page_rdy = '1' then
      nxt_rdsync_reg <= rdsync;
      nxt_rdsync_dly <= ( 0 => rdsync_reg, others => '0');
    end if;
  end process;

  cnt_control : process (cnt, page_rdy)
  begin
    nxt_rden <= '0';
    nxt_cnt <= cnt;
    if page_rdy = '1' then
      nxt_cnt      <= (others => '1');
      nxt_rden <= '1';
    elsif signed(cnt) /= -2 then
      nxt_cnt <= std_logic_vector(unsigned(cnt) + 1);
      nxt_rden <= '1';
    end if;
  end process;

  addr_gen : process (cnt)
  begin
    rdaddr <= (others => '0');
    if cnt(0) = '0' then
      rdaddr <= '0' & cnt(cnt'high downto 1);
    else
      rdaddr <= '1' & std_logic_vector(not(unsigned(cnt(cnt'high downto 1))));
    end if;
  end process;

  rd_counter : process (rd_cnt, rdval_dly, page_rdy_dly)
  begin
    nxt_rd_cnt <= rd_cnt;
    if page_rdy_dly(3) = '1' then
      nxt_rd_cnt <= (others => '0');
    elsif rdval_dly(1) = '1' then
      nxt_rd_cnt <= std_logic_vector(unsigned(rd_cnt) + 1);
    end if;
  end process;

  adder_inputs : process (rddata_re_dly, rddata_im_dly, rd_cnt)
  begin
    if unsigned(rd_cnt) = 0 then
      nxt_add0 <= rddata_re_dly(0);
      nxt_add1 <= rddata_re_dly(0);
      nxt_sub0 <= rddata_re_dly(1);
      nxt_sub1 <= std_logic_vector(-signed(rddata_re_dly(1)));
    elsif unsigned(rd_cnt) = 1 then
      nxt_add0 <= rddata_im_dly(1);
      nxt_add1 <= rddata_im_dly(1);
      nxt_sub0 <= rddata_im_dly(2);
      nxt_sub1 <= std_logic_vector(-signed(rddata_im_dly(2)));
    elsif rd_cnt(0) = '0' then
      nxt_add0 <= rddata_re_dly(0);
      nxt_add1 <= rddata_re_dly(1);
      nxt_sub0 <= rddata_im_dly(0);
      nxt_sub1 <= rddata_im_dly(1);
    else
      nxt_add0 <= rddata_im_dly(2);
      nxt_add1 <= rddata_im_dly(1);
      nxt_sub0 <= rddata_re_dly(2);
      nxt_sub1 <= rddata_re_dly(1);
    end if;
  end process;

  nxt_out_dat_re <= add_out;
  nxt_out_dat_im <= sub_out;
  nxt_out_val    <= rdval_dly(rdval_dly'high);
  nxt_out_sync   <= rdsync_dly(rdsync_dly'high);

--  Intel Altera lmp_add_sub carry in:
--  ADD: out = a + b + cin      => cin = '0' to have out = a + b
--  SUB: out = a - b + cin - 1  => cin = '1' to have out = a - b

--  add : ENTITY common_lib.common_addsub
--  GENERIC MAP (
--    g_in_a_w   => add0'LENGTH,
--    g_in_b_w   => add1'LENGTH,
--    g_out_c_w  => add_out'LENGTH,
--    g_pipeline => c_add_delay-1,
--    g_add_sub  => "ADD"
--  )
--  PORT MAP (
--    in_a       => add0,
--    in_b       => add1,
--    in_cry     => '0',
--    out_c      => add_out,
--    clk        => clk
--  );

  add : entity common_lib.common_add_sub
  generic map (
    g_direction       => "ADD",
    g_representation  => "SIGNED",
    g_pipeline_input  => 0,  -- 0 or 1
    g_pipeline_output => c_add_delay - 1,  -- >= 0
    g_in_dat_w        => g_rd_dat_w,
    g_out_dat_w       => g_out_dat_w  -- only support g_out_dat_w=g_in_dat_w and g_out_dat_w=g_in_dat_w+1
  )
  port map (
    clk      => clk,
    in_a     => add0,
    in_b     => add1,
    result   => add_out
  );

--  sub : ENTITY common_lib.common_addsub
--  GENERIC MAP (
--    g_in_a_w   => sub0'LENGTH,
--    g_in_b_w   => sub1'LENGTH,
--    g_out_c_w  => sub_out'LENGTH,
--    g_pipeline => c_add_delay-1,
--    g_add_sub  => "SUB"
--  )
--  PORT MAP (
--    in_a       => sub0,
--    in_b       => sub1,
--    in_cry     => '1',
--    out_c      => sub_out,
--    clk        => clk
--  );

  sub : entity common_lib.common_add_sub
  generic map (
    g_direction       => "SUB",
    g_representation  => "SIGNED",
    g_pipeline_input  => 0,  -- 0 or 1
    g_pipeline_output => c_add_delay - 1,  -- >= 0
    g_in_dat_w        => g_rd_dat_w,
    g_out_dat_w       => g_out_dat_w  -- only support g_out_dat_w=g_in_dat_w and g_out_dat_w=g_in_dat_w+1
  )
  port map (
    clk      => clk,
    in_a     => sub0,
    in_b     => sub1,
    result   => sub_out
  );
end rtl;
