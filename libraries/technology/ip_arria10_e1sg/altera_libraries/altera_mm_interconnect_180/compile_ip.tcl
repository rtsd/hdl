#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist      
#
                                                      
vmap  altera_mm_interconnect_180          ./work/
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_1600/sim"
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_4g_1600_altera_mm_interconnect_180_7km4trq.vhd"             -work altera_mm_interconnect_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_4g_2000/sim"
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_4g_2000_altera_mm_interconnect_180_7km4trq.vhd"             -work altera_mm_interconnect_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_1600/sim"
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_mm_interconnect_180_ibrpcbq.vhd"             -work altera_mm_interconnect_180
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_mm_interconnect_180_7km4trq.vhd"             -work altera_mm_interconnect_180
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_1600_altera_mm_interconnect_180_mtvmp4i.vhd"             -work altera_mm_interconnect_180

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_ddr4_8g_2400/sim"
  vcom         "$IP_DIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_mm_interconnect_180_7km4trq.vhd"             -work altera_mm_interconnect_180