-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.mdio_pkg.all;

entity mdio is
  generic (
    g_mdio_phy   : t_c_mdio_phy := c_mdio_phy
  );
  port (
    -- GENERIC Signal
    gs_sim              : in  boolean := false;

    rst                 : in  std_logic;
    clk                 : in  std_logic;

    ---------------------------------------------------------------------------
    -- Memory Mapped Slave interface with Interrupt
    ---------------------------------------------------------------------------
    -- MM slave mdio header register
    mms_header_address  : in  std_logic_vector(0 downto 0);
    mms_header_write    : in  std_logic;
    mms_header_read     : in  std_logic;
    mms_header_writedata: in  std_logic_vector(c_halfword_w - 1 downto 0);  -- 16-bit
    mms_header_readdata : out std_logic_vector(c_halfword_w - 1 downto 0);
    -- MM slave mdio data register
    mms_data_address    : in  std_logic_vector(0 downto 0);
    mms_data_write      : in  std_logic;
    mms_data_read       : in  std_logic;
    mms_data_writedata  : in  std_logic_vector(c_halfword_w - 1 downto 0);  -- 16-bit
    mms_data_readdata   : out std_logic_vector(c_halfword_w - 1 downto 0);
    -- Interrupt
    ins_mdio_rdy        : out std_logic;

    ---------------------------------------------------------------------------
    -- MDIO interface
    ---------------------------------------------------------------------------
    mdc          : out std_logic;
    mdat_in      : in  std_logic;  -- tristate buffer input from line
    mdat_oen     : out std_logic  -- tristate buffer output to line enable
  );
end mdio;

architecture str of mdio is
  signal mdio_en_evt     : std_logic;
  signal mdio_done_evt   : std_logic;
  signal mdio_hdr        : std_logic_vector(c_halfword_w - 1 downto 0);
  signal mdio_tx_dat     : std_logic_vector(c_halfword_w - 1 downto 0);
  signal mdio_rx_dat     : std_logic_vector(c_halfword_w - 1 downto 0);
begin
  u_mm : entity work.mdio_mm
  port map (
    clk                 => clk,
    rst                 => rst,
    -- Memory Mapped Slave interface with Interrupt
    mms_header_address  => mms_header_address,
    mms_header_write    => mms_header_write,
    mms_header_read     => mms_header_read,
    mms_header_writedata => mms_header_writedata,
    mms_header_readdata => mms_header_readdata,

    mms_data_address    => mms_data_address,
    mms_data_write      => mms_data_write,
    mms_data_read       => mms_data_read,
    mms_data_writedata  => mms_data_writedata,
    mms_data_readdata   => mms_data_readdata,

    ins_mdio_rdy        => ins_mdio_rdy,
    -- MDIO PHY control interface
    mdio_en_evt         => mdio_en_evt,
    mdio_done_evt       => mdio_done_evt,
    mdio_hdr            => mdio_hdr,
    mdio_tx_dat         => mdio_tx_dat,
    mdio_rx_dat         => mdio_rx_dat
  );

  u_phy : entity work.mdio_phy
  generic map (
    g_mdio_phy    => g_mdio_phy
  )
  port map (
    gs_sim        => gs_sim,

    clk           => clk,
    rst           => rst,

    -- MDIO PHY control interface
    mdio_en_evt   => mdio_en_evt,
    mdio_done_evt => mdio_done_evt,
    hdr           => mdio_hdr,
    tx_dat        => mdio_tx_dat,
    rx_dat        => mdio_rx_dat,

    -- MDIO PHY external clock and serial data
    mdc           => mdc,
    mdat_in       => mdat_in,
    mdat_oen      => mdat_oen
  );
end str;
