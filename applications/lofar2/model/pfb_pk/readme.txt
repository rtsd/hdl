2021-01-20 Paulus Kruger script for offline analysis of LTS raw data and comparison with SST data.
Use Jupyter notebook: https://jupyter.org/
LTS results: https://support.astron.nl/confluence/display/RCU/2020-11-23+Stati+measurement+vs+raw+data
