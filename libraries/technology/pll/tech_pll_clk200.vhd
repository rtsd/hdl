-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_pll_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_pll_lib;
library ip_arria10_pll_clk200_altera_iopll_150;
library ip_arria10_e3sge3_pll_clk200_altera_iopll_151;
library ip_arria10_e1sg_pll_clk200_altera_iopll_180;
library ip_arria10_e2sg_pll_clk200_altera_iopll_1930;

entity tech_pll_clk200 is
  generic (
    g_technology       : natural := c_tech_select_default;
    g_operation_mode   : string := "NORMAL";  -- or "SOURCE_SYNCHRONOUS" --> requires PLL_COMPENSATE assignment to an input pin to compensate for (stratixiv)
    g_clk0_phase_shift : string := "0";
    g_clk1_phase_shift : string := "0"
  );
  port (
    areset  : in std_logic  := '0';
    inclk0  : in std_logic  := '0';
    c0      : out std_logic;
    c1      : out std_logic;
    c2      : out std_logic;
    locked  : out std_logic
  );
end tech_pll_clk200;

architecture str of tech_pll_clk200 is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_pll_clk200
    generic map (g_operation_mode, g_clk0_phase_shift, g_clk1_phase_shift)
    port map (areset, inclk0, c0, c1, c2, locked);
  end generate;

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_pll_clk200
    port map (
      rst      => areset,
      refclk   => inclk0,
      outclk_0 => c0,
      outclk_1 => c1,
      outclk_2 => c2,
      locked   => locked
    );
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_pll_clk200
    port map (
      rst      => areset,
      refclk   => inclk0,
      outclk_0 => c0,
      outclk_1 => c1,
      outclk_2 => c2,
      locked   => locked
    );
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_pll_clk200
    port map (
      rst      => areset,
      refclk   => inclk0,
      outclk_0 => c0,
      outclk_1 => c1,
      outclk_2 => c2,
      locked   => locked
    );
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_pll_clk200
    port map (
      rst      => areset,
      refclk   => inclk0,
      outclk_0 => c0,
      outclk_1 => c1,
      outclk_2 => c2,
      locked   => locked
    );
  end generate;
end architecture;
