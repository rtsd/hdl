-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

-- Author: Eric Kooistra, 16 Mar 2018
-- Purpose:
--   Resize a block by moving the sop and or the eop of the block
-- Description:
--   The output sop and eop can be put at any index of the input block in range(g_input_block_size).
--   Any valid input data before the new sop index and after the new eop index is lost. This
--   component is useful to e.g. remove an unused head or tail of a block.
--
--   * The info at sop (sync, bsn, channel) is preserved by sop_sosi when g_out_sop_index >= 0
--   * The info at eop (empty, err) gets lost if g_out_eop_index < g_input_block_size-1, this is
--     often no issue. If it is an issue then use dp_split instead.
-- Remarks:
-- - This component supports flow control and was designed by keeping the functional
--   state registers and the pipeline registers seperate. Therefore the function is
--   implemented using combinatorial logic and local state registers to keep its
--   state. The combinatorial function output preserves the snk_in ready latency and
--   is pipelined using dp_pipeline to ease timing closure on the output.
--   A similar approach is also used with dp_counter.vhd.

entity dp_block_resize is
  generic (
    g_input_block_size : natural := 256;
    g_out_sop_index    : natural := 0;  -- >= 0
    g_out_eop_index    : natural := 255  -- >= g_out_sop_index and < g_input_block_size
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso := c_dp_siso_rdy;
    src_out      : out t_dp_sosi
  );
end dp_block_resize;

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

architecture rtl of dp_block_resize is
  signal cnt_reg      : natural range 0 to g_input_block_size-1;
  signal cnt          : natural;
  signal sop_sosi_reg : t_dp_sosi;
  signal sop_sosi     : t_dp_sosi;
  signal block_sosi   : t_dp_sosi;
begin
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      cnt_reg      <= 0;
      sop_sosi_reg <= c_dp_sosi_rst;
    elsif rising_edge(clk) then
      cnt_reg      <= cnt;
      sop_sosi_reg <= sop_sosi;
    end if;
  end process;

  -- Preserve snk_in @ sop
  sop_sosi <= snk_in when snk_in.sop = '1' else sop_sosi_reg;

  -- Count valid per block
  p_cnt : process(snk_in, cnt_reg)
  begin
    cnt <= cnt_reg;
    if snk_in.sop = '1' then
      cnt <= 0;
    elsif snk_in.valid = '1' then
      cnt <= cnt_reg + 1;
    end if;
  end process;

  -- Resize snk_in combinatorially into block_sosi, so no impact on RL
  p_block_sosi : process(snk_in, cnt, sop_sosi)
  begin
    -- Default keep snk_in info and data fields
    block_sosi       <= snk_in;
    block_sosi.sync  <= '0';
    block_sosi.sop   <= '0';
    block_sosi.eop   <= '0';
    block_sosi.valid <= '0';
    if snk_in.valid = '1' then
      -- Set output sop and preserve info @ sop
      if cnt = g_out_sop_index then
        block_sosi.sop     <= '1';
        block_sosi.sync    <= sop_sosi.sync;
        block_sosi.bsn     <= sop_sosi.bsn;
        block_sosi.channel <= sop_sosi.channel;
      end if;
      -- Set output eop, info @ eop gets lost if g_out_eop_index < g_input_block_size-1
      if cnt = g_out_eop_index then
        block_sosi.eop   <= '1';
      end if;
      if cnt >= g_out_sop_index and cnt <= g_out_eop_index then
        block_sosi.valid <= '1';
      end if;
    end if;
  end process;

  -- Register block_sosi to easy timing closure
  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => 1  -- 0 for wires, > 0 for registers,
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => snk_out,
    snk_in       => block_sosi,
    -- ST source
    src_in       => src_in,
    src_out      => src_out
  );
end rtl;
