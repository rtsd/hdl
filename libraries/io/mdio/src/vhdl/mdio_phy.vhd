-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.mdio_pkg.all;

-- MDIO = Management Data Input Output
-- MMD  = MDIO Manageable Device
-- MDC  = Management Data Clock
entity mdio_phy is
  generic (
    g_mdio_phy       : t_c_mdio_phy := c_mdio_phy;
    g_add_mdc_cycle  : boolean := false
  );
  port (
    -- GENERIC Signal
    gs_sim            : in  boolean := false;

    rst               : in  std_logic;
    clk               : in  std_logic;

    mdio_en_evt       : in  std_logic;  -- going high starts the MDC/MDIO access, then low to prepare for next enable,
    mdio_done_evt     : out std_logic;  -- ingores next enable until MDC/MDIO access done pulse has happened
    mdio_done         : out std_logic;  -- Same as mdio_done_evt but remains high until acknowledged
    mdio_done_ack_evt : in  std_logic := '0';

    hdr               : in  std_logic_vector(c_halfword_w - 1 downto 0);
    tx_dat            : in  std_logic_vector(c_halfword_w - 1 downto 0);
    rx_dat            : out std_logic_vector(c_halfword_w - 1 downto 0);

    -- External clock and serial data.
    mdc               : out std_logic;
    mdat_in           : in  std_logic;  -- tristate buffer input from line
    mdat_oen          : out std_logic  -- tristate buffer output to line enable
                                        -- no need for mdat_out, only enable for output '0', else rely on external pull up for output '1'
  );
end mdio_phy;

architecture rtl of mdio_phy is
  -- Simulation constants for when gs_sim=TRUE
  constant c_sim_mdc_period     : natural := 8;  -- must be a power of 2 and <= g_mdio_phy.mdc_period
  constant c_sim_mdc_cnt_high   : natural := ceil_log2(c_sim_mdc_period) - 1;
  constant c_sim_hold_time      : natural := 3;
  constant c_sim_setup_time     : natural := 2;

  constant c_preamble_length    : natural := 32;
  constant c_write_msg_length   : natural := 32;
  constant c_read_msg_length    : natural := 14;
  constant c_receive_msg_length : natural := 19;

  constant c_hdr_rd_bit         : natural := 13;  -- hdr[15: 0] = st(2) & op(2) & prtad(5) & devad(5) & ta(2)
                                                  -- hdr[13:12] = op[1:0] and op[1]='1' indicates read access

  constant c_ta                 : std_logic_vector := "10";

  type t_mdio_state is (s_idle, s_preamble, s_write, s_read, s_receive, s_add_mdc, s_done);

  -- CONSTANT Signals that depend on GENERIC Signal gs_sim
  -- Timing constraints in system clock periods.
  signal cs_mdc_period          : natural;
  signal cs_mdc_period_min_1    : natural;
  signal cs_tx_en_cnt           : natural;
  signal cs_rx_en_cnt           : natural;

  signal state                  : t_mdio_state;
  signal nxt_state              : t_mdio_state;

  signal mdc_cnt                : std_logic_vector(ceil_log2(g_mdio_phy.mdc_period) - 1 downto 0);
  signal nxt_mdc_cnt            : std_logic_vector(mdc_cnt'range);
  signal bit_cnt                : natural range 0 to c_write_msg_length - 1;
  signal nxt_bit_cnt            : natural;
  signal bit_cnt_rst            : std_logic;

  signal msg_reg                : std_logic_vector(c_write_msg_length - 1 downto 0);
  signal nxt_msg_reg            : std_logic_vector(msg_reg'range);

  signal tx_en                  : std_logic;
  signal rx_en                  : std_logic;
  signal i_rx_dat               : std_logic_vector(rx_dat'range);
  signal nxt_rx_dat             : std_logic_vector(rx_dat'range);

  signal mdio_en_revt           : std_logic;
  signal nxt_mdio_done_evt      : std_logic;
  signal i_mdio_done_evt        : std_logic;

  signal nxt_mdc                : std_logic;
  signal mdc_oen                : std_logic;
  signal nxt_mdc_oen            : std_logic;
  signal i_mdat_oen             : std_logic;
  signal nxt_mdat_oen           : std_logic;
begin
  mdat_oen      <= i_mdat_oen;
  rx_dat        <= i_rx_dat;
  mdio_done_evt <= i_mdio_done_evt;

  -- CONSTANT Signals dependent on GENERIC Signal gs_sim
  cs_mdc_period       <= g_mdio_phy.mdc_period     when gs_sim = false else c_sim_mdc_period;
  cs_mdc_period_min_1 <= g_mdio_phy.mdc_period - 1 when gs_sim = false else c_sim_mdc_period - 1;

  nxt_mdc <= mdc_cnt(      mdc_cnt'high) when gs_sim = false and (mdc_oen = '1' or state = s_receive) else
             mdc_cnt(c_sim_mdc_cnt_high) when gs_sim = true  and (mdc_oen = '1' or state = s_receive) else '1';

  registers : process (clk, rst)
  begin
    if rst = '1' then
      -- Output registers.
      mdc             <= '1';
      i_mdio_done_evt <= '0';
      i_rx_dat        <= (others => '0');
      -- Internal registers.
      state           <= s_idle;
      mdc_cnt         <= (others => '0');
      bit_cnt         <= 0;
      msg_reg         <= (others => '0');
      mdc_oen         <= '0';
      i_mdat_oen      <= '0';
    elsif rising_edge(clk) then
      -- Output registers.
      mdc             <= nxt_mdc;
      i_mdio_done_evt <= nxt_mdio_done_evt;
      i_rx_dat        <= nxt_rx_dat;
      -- Internal registers.
      state           <= nxt_state;
      mdc_cnt         <= nxt_mdc_cnt;
      bit_cnt         <= nxt_bit_cnt;
      msg_reg         <= nxt_msg_reg;
      mdc_oen         <= nxt_mdc_oen;
      i_mdat_oen      <= nxt_mdat_oen;
    end if;
  end process;

  u_mdio_en_revt : entity common_lib.common_evt
  generic map (
    g_evt_type => "RISING",
    g_out_reg  => false
  )
  port map (
    rst      => rst,
    clk      => clk,
    in_sig   => mdio_en_evt,
    out_evt  => mdio_en_revt
  );

  u_mdio_done : entity common_lib.common_switch
  port map (
    clk         => clk,
    rst         => rst,
    switch_high => i_mdio_done_evt,
    switch_low  => mdio_done_ack_evt,
    out_level   => mdio_done
  );

  p_mdc_counter : process (mdc_cnt)
  begin
    nxt_mdc_cnt <= mdc_cnt;
    if unsigned(mdc_cnt) = cs_mdc_period_min_1 then
      nxt_mdc_cnt <= (others => '0');
    else
      nxt_mdc_cnt <= std_logic_vector(unsigned(mdc_cnt) + 1);
    end if;
  end process;

  p_state : process (state, mdio_en_revt, bit_cnt, hdr, tx_en, mdc_oen)
  begin
    nxt_state         <= state;
    nxt_mdio_done_evt <= '0';
    bit_cnt_rst       <= '0';

    case state is
      when s_idle =>
        if mdio_en_revt = '1' then
          nxt_state <= s_preamble;
        end if;

      when s_preamble =>
        if bit_cnt = c_preamble_length - 1 and tx_en = '1' then
          if hdr(c_hdr_rd_bit) = '0' then
            nxt_state <= s_write;
          else
            nxt_state <= s_read;
          end if;
          bit_cnt_rst <= '1';
        end if;

      when s_write =>
        if bit_cnt = c_write_msg_length - 1 and tx_en = '1' then
          bit_cnt_rst <= '1';
          if g_add_mdc_cycle = true then
            nxt_state <= s_add_mdc;
          else
            nxt_state <= s_done;
          end if;
        end if;

      when s_add_mdc =>
        bit_cnt_rst <= '1';  --
        if tx_en = '1' then
          nxt_state <= s_done;
        end if;

      when s_read =>
        if bit_cnt = c_read_msg_length - 1 and tx_en = '1' then
          nxt_state <= s_receive;
          bit_cnt_rst <= '1';
        end if;

      when s_receive =>
        if bit_cnt = c_receive_msg_length - 1 and tx_en = '1' then
          bit_cnt_rst <= '1';
          if g_add_mdc_cycle = true then
            nxt_state <= s_add_mdc;
          else
            nxt_state <= s_done;
          end if;
        end if;

      when s_done =>
        if mdc_oen = '0' and tx_en = '1' then
          nxt_state <= s_idle;
          nxt_mdio_done_evt <= '1';
        end if;

      when others =>
        assert false
          report "Uknown state."
          severity Failure;
        nxt_state <= s_idle;
    end case;
  end process;

  p_bit_counter : process (bit_cnt, bit_cnt_rst, tx_en, state)
  begin
    nxt_bit_cnt <= bit_cnt;
    if bit_cnt_rst = '1' or state = s_idle or state = s_done then
      nxt_bit_cnt <= 0;
    elsif tx_en = '1' then
      nxt_bit_cnt <= bit_cnt + 1;
    end if;
  end process;

  -- Derive signal constants for tx_en and rx_en strobe instants dependend on gs_sim
  cs_tx_en_cnt <= g_mdio_phy.mdc_period / 2 + g_mdio_phy.hold_time when gs_sim = false else
                       c_sim_mdc_period / 2 +      c_sim_hold_time;
  cs_rx_en_cnt <= g_mdio_phy.mdc_period / 2 - g_mdio_phy.setup_time when gs_sim = false else
                       c_sim_mdc_period / 2 -      c_sim_setup_time;

  tx_en <= '1' when unsigned(mdc_cnt) = cs_tx_en_cnt else '0';
  rx_en <= '1' when unsigned(mdc_cnt) = cs_rx_en_cnt and state = s_receive else '0';

  p_transmitter : process (msg_reg, state, mdio_en_revt, hdr, tx_dat, tx_en, mdc_oen, i_mdat_oen)
  begin
    nxt_mdc_oen <= mdc_oen;
    nxt_mdat_oen <= i_mdat_oen;
    nxt_msg_reg <= msg_reg;

    if mdio_en_revt = '1' and state = s_idle then
      nxt_msg_reg <= hdr & tx_dat;
    end if;

    if tx_en = '1' then
      if state = s_preamble then
        nxt_mdc_oen <= '1';
      elsif state = s_write or state = s_read then
        nxt_mdc_oen <= '1';
        nxt_mdat_oen <= not msg_reg(msg_reg'high);
        nxt_msg_reg <= msg_reg(msg_reg'high - 1 downto 0) & '0';
      elsif state = s_add_mdc then
        nxt_mdc_oen <= '1';
        nxt_mdat_oen <= '0';
      else
        nxt_mdc_oen <= '0';
        nxt_mdat_oen <= '0';
      end if;
    end if;
  end process;

  p_receiver : process (mdio_en_revt, i_rx_dat, rx_en, mdat_in, state, bit_cnt)
  begin
    nxt_rx_dat <= i_rx_dat;

    if mdio_en_revt = '1' and state = s_idle then
      nxt_rx_dat <= (others => '0');
    end if;

    if rx_en = '1' and state = s_receive then
      for i in 3 to c_receive_msg_length - 1 loop
        if bit_cnt = i then
          nxt_rx_dat(c_receive_msg_length - 1 - i) <= mdat_in;
        end if;
      end loop;
    end if;
  end process;
end rtl;
