-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . MM wrapper for common_pulse_delay.vhd

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_common_pulse_delay is
  generic (
    g_pulse_delay_max : natural := 0;  -- Maximum number of clk cycles that pulse can be delayed
    g_register_out    : boolean
   );
  port (
    pulse_clk        : in  std_logic;
    pulse_rst        : in  std_logic;
    pulse_in         : in  std_logic;
    pulse_out        : out std_logic;

    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;
    reg_mosi         : in  t_mem_mosi := c_mem_mosi_rst;
    reg_miso         : out t_mem_miso := c_mem_miso_rst
  );
end mms_common_pulse_delay;

architecture str of mms_common_pulse_delay is
  signal pulse_delay : std_logic_vector(ceil_log2(g_pulse_delay_max) - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- common_pulse_delay
  ------------------------------------------------------------------------------
  u_common_pulse_delay : entity common_lib.common_pulse_delay
  generic map (
    g_pulse_delay_max => g_pulse_delay_max,
    g_register_out    => true
  )
  port map (
    clk         => pulse_clk,
    rst         => pulse_rst,
    pulse_in    => pulse_in,
    pulse_delay => pulse_delay,
    pulse_out   => pulse_out
  );

  ------------------------------------------------------------------------------
  -- New MM interface via avs_common_mm
  ------------------------------------------------------------------------------
  u_common_pulse_delay_reg : entity work.common_pulse_delay_reg
  generic map (
    g_cross_clock_domain => true,
    g_pulse_delay_max    => g_pulse_delay_max
  )
  port map (
    pulse_clk   => pulse_clk,
    pulse_rst   => pulse_rst,
    pulse_delay => pulse_delay,

    mm_clk      => mm_clk,
    mm_rst      => mm_rst,
    sla_in      => reg_mosi,
    sla_out     => reg_miso
  );
end str;
