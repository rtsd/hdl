-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.i2c_pkg.all;
use technology_lib.technology_select_pkg.all;

entity i2c_mm is
  generic (
    g_technology             : natural := c_tech_select_default;
    g_i2c_mm                 : t_c_i2c_mm  := c_i2c_mm
  );
  port (
    rst                      : in  std_logic;
    clk                      : in  std_logic;
    sync                     : in  std_logic := '1';

    ---------------------------------------------------------------------------
    -- Avalon Memory Mapped Slave interface with Interrupt
    ---------------------------------------------------------------------------
    -- MM slave I2C control register
    mms_control_address      : in  std_logic_vector(g_i2c_mm.control_adr_w - 1 downto 0);
    mms_control_write        : in  std_logic;
    mms_control_read         : in  std_logic;
    mms_control_writedata    : in  std_logic_vector(c_word_w - 1 downto 0);  -- use default MM bus width for control
    mms_control_readdata     : out std_logic_vector(c_word_w - 1 downto 0);
    -- MM slave I2C protocol register
    mms_protocol_address     : in  std_logic_vector(g_i2c_mm.protocol_adr_w - 1 downto 0);
    mms_protocol_write       : in  std_logic;
    mms_protocol_read        : in  std_logic;
    mms_protocol_writedata   : in  std_logic_vector(c_byte_w - 1 downto 0);  -- define MM bus data has same width as SMBus data
    mms_protocol_readdata    : out std_logic_vector(c_byte_w - 1 downto 0);
    -- MM slave I2C result register
    mms_result_address       : in  std_logic_vector(g_i2c_mm.result_adr_w - 1 downto 0);
    mms_result_write         : in  std_logic;
    mms_result_read          : in  std_logic;
    mms_result_writedata     : in  std_logic_vector(c_byte_w - 1 downto 0);  -- define MM bus data has same width as SMBus data
    mms_result_readdata      : out std_logic_vector(c_byte_w - 1 downto 0);
    -- Interrupt
    ins_result_rdy           : out std_logic;

    ---------------------------------------------------------------------------
    -- SMBus control interface
    ---------------------------------------------------------------------------
    protocol_rd_en           : in  std_logic;
    protocol_rd_adr          : in  std_logic_vector(g_i2c_mm.protocol_adr_w - 1 downto 0);
    protocol_rd_dat          : out std_logic_vector(c_byte_w - 1 downto 0);
    protocol_rd_val          : out std_logic;

    result_wr_en             : in  std_logic;
    result_wr_adr            : in  std_logic_vector(g_i2c_mm.result_adr_w - 1 downto 0);
    result_wr_dat            : in  std_logic_vector(c_byte_w - 1 downto 0);

    protocol_activate_evt    : out std_logic;  -- pulse indicating new protocol list
    result_ready_evt         : in  std_logic  -- pulse indicating new result is available
  );
end i2c_mm;

architecture str of i2c_mm is
  -- Use default MM bus data width = c_word_w
  constant c_reg_control  : t_c_mem := (latency  => 1,
                                        adr_w    => 1,
                                        dat_w    => c_word_w,
                                        nof_dat  => 1,
                                        init_sl  => 'X');

  -- Use MM bus data width = SMBus data width = c_byte_w
  constant c_ram_protocol : t_c_mem := (latency  => 2,
                                        adr_w    => g_i2c_mm.protocol_adr_w,
                                        dat_w    => c_byte_w,
                                        nof_dat  => 2**g_i2c_mm.protocol_adr_w,
                                        init_sl  => 'X');
  constant c_ram_result   : t_c_mem := (latency  => 2,
                                        adr_w    => g_i2c_mm.result_adr_w,
                                        dat_w    => c_byte_w,
                                        nof_dat  => 2**g_i2c_mm.result_adr_w,
                                        init_sl  => 'X');

  -- Control register
  constant c_control_bit_activate  : natural := 0;
  constant c_control_bit_ready     : natural := 1;

  constant c_init_dummy            : std_logic_vector(c_mem_reg_init_w - 1 downto 2) := (others => '0');
  constant c_init_control_activate : std_logic := '0';
  constant c_init_control_ready    : std_logic := '0';
  constant c_init_control          : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := c_init_dummy
                                                                                    & c_init_control_ready
                                                                                    & c_init_control_activate;

  signal reg_control               : std_logic_vector(c_reg_control.nof_dat * c_reg_control.dat_w - 1 downto 0);
  signal reg_control_rd            : std_logic_vector(reg_control'range);

  signal result_ready              : std_logic;
begin
  -- Protocol activate control
  u_protocol_activate : entity common_lib.common_request
  port map (
    rst         => rst,
    clk         => clk,
    sync        => sync,
    in_req      => reg_control(c_control_bit_activate),
    out_req_evt => protocol_activate_evt
  );

  -- Result ready control
  u_result_ready : entity common_lib.common_switch
  port map (
    clk         => clk,
    rst         => rst,
    switch_high => result_ready_evt,
    switch_low  => mms_control_read,  -- read clear ready bit
    out_level   => result_ready
  );

  ins_result_rdy <= result_ready;

  -- I2C access control register
  p_reg_control_rd : process(reg_control, result_ready)
  begin
    reg_control_rd                      <= reg_control;
    reg_control_rd(c_control_bit_ready) <= result_ready;
  end process;

  u_reg_control : entity common_lib.common_reg_r_w
  generic map (
    g_reg       => c_reg_control,
    g_init_reg  => c_init_control
  )
  port map (
    rst         => rst,
    clk         => clk,
    clken       => '1',
    -- control side
    wr_en       => mms_control_write,
    wr_adr      => mms_control_address,
    wr_dat      => mms_control_writedata,
    rd_en       => mms_control_read,
    rd_adr      => mms_control_address,
    rd_dat      => mms_control_readdata,
    rd_val      => OPEN,
    -- data side
    out_reg     => reg_control,
    in_reg      => reg_control_rd
  );

  -- I2C protocol list register
  u_ram_protocol : entity common_lib.common_ram_rw_rw
  generic map (
    g_technology => g_technology,
    g_ram        => c_ram_protocol
  )
  port map (
    rst         => rst,
    clk         => clk,
    clken       => '1',
    wr_en_a     => mms_protocol_write,
    wr_dat_a    => mms_protocol_writedata,
    adr_a       => mms_protocol_address,
    rd_en_a     => mms_protocol_read,
    rd_dat_a    => mms_protocol_readdata,
    rd_val_a    => OPEN,
    wr_en_b     => '0',
    --wr_dat_b    => (OTHERS => '0'),
    adr_b       => protocol_rd_adr,
    rd_en_b     => protocol_rd_en,
    rd_dat_b    => protocol_rd_dat,
    rd_val_b    => protocol_rd_val
  );

  -- I2C result register
  u_ram_result : entity common_lib.common_ram_rw_rw
  generic map (
    g_technology => g_technology,
    g_ram        => c_ram_result
  )
  port map (
    rst         => rst,
    clk         => clk,
    clken       => '1',
    wr_en_a     => mms_result_write,
    wr_dat_a    => mms_result_writedata,
    adr_a       => mms_result_address,
    rd_en_a     => mms_result_read,
    rd_dat_a    => mms_result_readdata,
    rd_val_a    => OPEN,
    wr_en_b     => result_wr_en,
    wr_dat_b    => result_wr_dat,
    adr_b       => result_wr_adr,
    rd_en_b     => '0',
    rd_dat_b    => OPEN,
    rd_val_b    => open
  );
end str;
