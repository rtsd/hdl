-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Tb for tb_dp_rsn_source
--   Use dp_bsn_source_v2 to create bs_sosi and extending it with dut =
--   dp_rsn_source to create and verify rs_sosi.
-- Remark:
-- * This tb is made based on tb_dp_bsn_source_v2. Difference is that bs_sosi
--   has block grid that starts at t_epoch = 0, whereas rs_sosi has block
--   grid that can start at any bs_sosi.sync. Therefore p_exp_grid_rs is
--   needed

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_rsn_source is
  generic (
    g_pps_interval   : natural := 40;  -- 101;
    g_bs_block_size  : natural := 10;  -- 23, input BSN block size <= g_pps_interval
    g_rs_block_size  : natural := 10  -- 23, output RSN block size <= g_pps_interval
  );
end tb_dp_rsn_source;

architecture tb of tb_dp_rsn_source is
  -- The nof block per sync interval will be the same after every
  -- c_min_nof_pps_interval. The c_gcd is the greatest common divider of
  -- g_pps_interval and g_bs_block_size, so g_bs_block_size / c_gcd yields an
  -- integer. When g_pps_interval and g_bs_block_size are relative prime,
  -- then c_gcd = 1, and then it takes g_bs_block_size nof g_pps_interval
  -- for the pattern of c_nof_block_per_sync_lo and c_nof_block_per_sync_hi
  -- to repeat. If c_gcd = g_bs_block_size, then c_nof_block_per_sync_lo =
  -- c_nof_block_per_sync_hi, so then the nof block per sync interval is
  -- the same in every pps interval.
  constant c_gcd                   : natural := gcd(g_pps_interval, g_bs_block_size);
  constant c_min_nof_pps_interval  : natural := g_bs_block_size / c_gcd;

  constant c_nof_block_per_sync_lo : natural := g_pps_interval / g_bs_block_size;
  constant c_nof_block_per_sync_hi : natural := ceil_div(g_pps_interval, g_bs_block_size);

  -- choose c_nof_pps and c_nof_repeat > c_min_nof_pps_interval, because the
  -- fractional sync pattern will repeat every c_min_nof_pps_interval number
  -- of g_pps_intervals.
  constant c_factor            : natural := 5;
  constant c_nof_pps           : natural := c_min_nof_pps_interval * c_factor;
  constant c_nof_repeat        : natural := c_min_nof_pps_interval * c_factor;

  constant c_clk_period        : time    := 10 ns;
  constant c_bsn_w             : natural := 31;
  constant c_bsn_time_offset_w : natural := ceil_log2(g_bs_block_size);

  -- Minimum latency between sync and PPS, due to logic in DUT
  constant c_dp_bsn_latency    : natural := 3;
  constant c_dp_rsn_latency    : natural := 1;
  constant c_dut_latency       : natural := c_dp_bsn_latency + c_dp_rsn_latency;

  -- The state name tells what kind of test is being done
  type t_state_enum is (
    s_disable,
    s_dp_on,
    s_dp_on_pps
  );

  -- Define the PPS (SSN) and BSN grid that both start at 0 according to Figure 3.1 in [1]:
  type t_time_grid is record
    pps  : std_logic;  -- pulse per second, g_pps_interval clk per pps interval
    ssn  : natural;  -- seconds sequence number
    bsn  : natural;  -- block sequence number, g_bs_block_size clk per block
    sync : std_logic;  -- active at sop when pps is active or was active
    sop  : std_logic;  -- start of block
    eop  : std_logic;  -- end of block
  end record;

  constant c_time_grid_rst  : t_time_grid := ('0', 0, 0, '0', '0', '0');

  -- Reference grid
  signal ref_grid_bs        : t_time_grid := c_time_grid_rst;
  signal ssn_eop            : std_logic := '0';
  signal hold_pps           : std_logic := '0';
  signal nxt_hold_pps       : std_logic := '0';

  -- Tb
  signal tb_end             : std_logic := '0';
  signal rst                : std_logic := '1';
  signal clk                : std_logic := '1';
  signal tb_state           : t_state_enum := s_disable;

  -- BSN source
  signal dp_on              : std_logic := '0';
  signal dp_on_pps          : std_logic := '0';
  signal dp_on_status       : std_logic;
  signal bs_restart         : std_logic;
  signal bs_new_interval    : std_logic;
  signal tb_new_interval    : std_logic := '0';
  signal bsn_init           : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '0');
  signal bsn_time_offset    : std_logic_vector(c_bsn_time_offset_w - 1 downto 0) := (others => '0');
  signal bs_sosi            : t_dp_sosi;
  signal exp_grid_bs        : t_time_grid;  -- to check with bs_sosi.bsn, sync, sop, eop

  -- RSN source
  signal rs_restart         : std_logic;
  signal rs_new_interval    : std_logic;
  signal rs_sosi            : t_dp_sosi;
  signal exp_grid_rs        : t_time_grid;  -- to verify rs_sosi.bsn, sync, sop, eop

  -- Verify
  signal exp_rs_start_bsn   : natural;
  signal exp_rs_sync_cnt    : natural;
  signal exp_rs_block_cnt   : natural;
  signal hold_exp_rs_sync   : std_logic := '0';

  signal unexpected_rs_sync : std_logic;
  signal sl0                : std_logic := '0';
  signal verify_en          : std_logic := '0';
  signal verify_sync        : std_logic := '0';
  signal hold_rs_sop        : std_logic := '0';
  signal prev_rs_valid      : std_logic;
  signal rs_starts_cnt      : natural := 0;

  signal dbg_c_nof_pps      : natural := c_nof_pps;
  signal dbg_c_nof_repeat   : natural := c_nof_repeat;

  signal dbg_nof_blk        : natural;
  signal dbg_accumulate     : natural;
  signal dbg_expected_bsn   : natural;
begin
  rst <= '1', '0' after c_clk_period * 7;
  clk <= (not clk) or tb_end after c_clk_period / 2;

  -----------------------------------------------------------------------------
  -- Generate reference time grid for bs_sosi
  -----------------------------------------------------------------------------
  proc_common_gen_pulse(1, g_pps_interval, '1', sl0, clk, ref_grid_bs.pps);
  proc_common_gen_pulse(1, g_bs_block_size, '1', sl0, clk, ref_grid_bs.sop);
  ref_grid_bs.eop <= ref_grid_bs.sop'delayed((g_bs_block_size - 1) * c_clk_period);
  ssn_eop <= ref_grid_bs.pps'delayed((g_pps_interval - 1) * c_clk_period);
  ref_grid_bs.ssn <= ref_grid_bs.ssn + 1 when rising_edge(clk) and ssn_eop = '1';
  ref_grid_bs.bsn <= ref_grid_bs.bsn + 1 when rising_edge(clk) and ref_grid_bs.eop = '1';

  -- Issue sync at start of block
  p_ref_grid_bs_sync : process(ref_grid_bs, hold_pps)
  begin
    ref_grid_bs.sync <= '0';
    nxt_hold_pps <= hold_pps;

    if ref_grid_bs.pps = '1' then
      if ref_grid_bs.sop = '1' then
        ref_grid_bs.sync <= '1';  -- immediately issue sync
      else
        nxt_hold_pps <= '1';  -- wait until next block
      end if;
    end if;

    if hold_pps = '1' then
      if ref_grid_bs.sop = '1' then
        ref_grid_bs.sync <= '1';  -- issue pending sync
        nxt_hold_pps <= '0';
      end if;
    end if;
  end process;

  hold_pps <= nxt_hold_pps when rising_edge(clk);

  exp_grid_bs <= ref_grid_bs'delayed(c_dp_bsn_latency * c_clk_period);

  -----------------------------------------------------------------------------
  -- Generate reference time grid for rs_sosi
  -----------------------------------------------------------------------------

  -- using clk process accounts for c_dp_rsn_latency = 1
  p_exp_grid_rs : process(clk)
    variable v_sop : std_logic;
  begin
    if rising_edge(clk) then
      v_sop := '0';

      -- same pps, ssn
      exp_grid_rs.pps <= exp_grid_bs.pps;
      exp_grid_rs.ssn <= exp_grid_bs.ssn;
      exp_grid_rs.sync <= '0';
      exp_grid_rs.sop <= '0';
      exp_grid_rs.eop <= '0';

      -- bs blocks start at t_epoch = 0, but rs sync and blocks start at
      -- bs_restart, so not necessarily at t_epoch = 0
      if bs_restart = '1' then
        exp_rs_sync_cnt <= 0;
        exp_rs_block_cnt <= 0;
        exp_grid_rs.sync <= '1';
        exp_grid_rs.sop <= '1';
        exp_grid_rs.bsn <= exp_grid_bs.bsn * g_bs_block_size;
        exp_rs_start_bsn <= exp_grid_bs.bsn * g_bs_block_size;
        hold_exp_rs_sync <= '0';
      else
        -- expected rs_sosi sop, eop
        if exp_rs_block_cnt < g_rs_block_size-2 then
          exp_rs_block_cnt <= exp_rs_block_cnt + 1;
        elsif exp_rs_block_cnt = g_rs_block_size-2 then
          exp_rs_block_cnt <= exp_rs_block_cnt + 1;
          exp_grid_rs.eop <= '1';
        elsif exp_rs_block_cnt = g_rs_block_size-1 then
          v_sop := '1';
          exp_rs_block_cnt <= 0;
          exp_grid_rs.sop <= '1';
          exp_grid_rs.bsn <= exp_grid_rs.bsn + g_rs_block_size;
          -- check for pending rs_sosi sync
          if hold_exp_rs_sync = '1' then
            exp_grid_rs.sync <= '1';
            hold_exp_rs_sync <= '0';
          end if;
        end if;

        -- expected rs_sosi sync
        if exp_rs_sync_cnt < g_pps_interval - 1 then
          exp_rs_sync_cnt <= exp_rs_sync_cnt + 1;
        else
          exp_rs_sync_cnt <= 0;
          if v_sop = '1' then
            exp_grid_rs.sync <= '1';  -- issue sync immediately at this exp_grid_rs.sop
          else
            hold_exp_rs_sync <= '1';  -- pend sync until next exp_grid_rs.sop
          end if;
        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------
  p_mm : process
    variable v_ssn             : natural;
    variable v_bsn_init        : natural;
    variable v_bsn_time_offset : natural;
  begin
    -- Get synchronous to clk
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    -- Start asynchronously by making dp_on high
    verify_en <= '0';  -- only verify visualy in wave window
    tb_state <= s_dp_on;
    dp_on_pps <= '0';
    dp_on <= '1';
    proc_common_wait_some_cycles(clk, c_nof_pps * g_pps_interval);
    tb_state <= s_disable;
    dp_on <= '0';
    dp_on_pps <= '0';

    -- Wait sufficient until bs_sosi is idle again
    proc_common_wait_some_cycles(clk, 5 * g_pps_interval);

    -- Start synchronously by making dp_on and dp_on_pps high
    verify_en <= '1';  -- verify automatically in test bench

    for I in 0 to c_nof_repeat - 1 loop
      -- Wait some variable time between tests, to enforce testing different
      -- bsn_time_offset values
      proc_common_wait_some_cycles(clk, 5 * g_pps_interval);
      proc_common_wait_some_cycles(clk, I * g_pps_interval);

      -- Wait until in the beginning of PPS interval
      proc_common_wait_until_hi_lo(clk, ref_grid_bs.pps);
      proc_common_wait_some_cycles(clk, c_dp_bsn_latency);

      -- Determine bsn_init and bsn_time_offset for BSN source start
      -- . bsn_init = BSN at sync
      -- . bsn_time_offset = number of clk that sync occurs after PPS
      v_ssn := ref_grid_bs.ssn + 1;  -- +1 to prepare start in next PPS interval
      v_bsn_init := ceil_div(v_SSN * g_pps_interval, g_bs_block_size);  -- Equation 3.6 in [1]
      v_bsn_time_offset := v_bsn_init * g_bs_block_size - v_SSN * g_pps_interval;  -- Equation 3.7 in [1]
      bsn_init <= TO_UVEC(v_bsn_init, c_bsn_w);  --
      bsn_time_offset <= TO_UVEC(v_bsn_time_offset, c_bsn_time_offset_w);
      -- Start synchronously by making dp_on and dp_on_pps high
      tb_state  <= s_dp_on_pps;
      dp_on_pps <= '1';
      dp_on <= '1';
      proc_common_wait_some_cycles(clk, c_nof_pps * g_pps_interval);
      tb_state <= s_disable;
      dp_on <= '0';
      dp_on_pps <= '0';
    end loop;

    proc_common_wait_some_cycles(clk, 10);
    assert rs_starts_cnt = 1 + c_nof_repeat
      report "Wrong number of BSN source starts."
      severity ERROR;

    tb_end <= '1';
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- Verification
  -- . Some aspects of rs_sosi are verified multiple times in different ways,
  --   this overlap is fine, because the tb and DUT are rather complicated, so
  --   using different approaches also helpt to verify the tb itself.
  -----------------------------------------------------------------------------
  verify_sync <= verify_en and rs_sosi.valid;

  proc_dp_verify_sop_and_eop(clk, rs_sosi.valid, rs_sosi.sop, rs_sosi.eop, hold_rs_sop);  -- Verify that sop and eop come in pairs
  --proc_dp_verify_sync(clk, verify_sync, rs_sosi.sync, exp_grid_rs.sop, exp_grid_rs.sync);  -- Verify sync at sop and at expected_sync

  -- Verify sync at sop and at expected_sync
  proc_dp_verify_sync(exp_rs_start_bsn,
                      g_pps_interval,
                      g_rs_block_size,
                      true,  -- use BSN as RSN
                      clk,
                      verify_en,
                      rs_sosi.sync,
                      rs_sosi.sop,
                      rs_sosi.bsn,
                      dbg_nof_blk,
                      dbg_accumulate,
                      dbg_expected_bsn);

  -- Verify rs_sosi by comparing with exp_grid_rs, this again verifies rs_sosi.sync, sop and bsn
  p_verify_rs_sosi_grid : process(clk)
  begin
    if rising_edge(clk) then
      unexpected_rs_sync <= '0';
      if verify_en = '1' and rs_sosi.valid = '1' then
        assert TO_UINT(rs_sosi.bsn) = exp_grid_rs.bsn
          report "Wrong rs_sosi.bsn /= exp_grid_rs.bsn"
          severity ERROR;
        assert rs_sosi.sync = exp_grid_rs.sync
          report "Wrong rs_sosi.sync /= exp_grid_rs.sync"
          severity ERROR;
        assert rs_sosi.sop = exp_grid_rs.sop
          report "Wrong rs_sosi.sop /= exp_grid_rs.sop"
          severity ERROR;
        assert rs_sosi.eop = exp_grid_rs.eop
          report "Wrong rs_sosi.eop /= exp_grid_rs.eop"
          severity ERROR;
        -- Mark error in Wave window
        if rs_sosi.sync = '1' and rs_sosi.sync /= exp_grid_rs.sync then
          unexpected_rs_sync <= '1';
        end if;
      end if;
    end if;
  end process;

  -- Verify that rs_sosi.valid = '1' did happen after dp_on by verifying rs_start
  prev_rs_valid <= rs_sosi.valid when rising_edge(clk);
  rs_starts_cnt <= rs_starts_cnt + 1 when rising_edge(clk) and rs_sosi.valid = '1' and prev_rs_valid = '0';

  p_verify_rs_restart : process(clk)
  begin
    if rising_edge(clk) then
      if rs_restart = '1' then
        assert rs_sosi.sync = '1'
          report "Unexpected rs_start while rs_sosi.sync /= 1"
          severity ERROR;
        assert prev_rs_valid = '0'
          report "Unexpected rs_start while prev_rs_valid /= 0"
          severity ERROR;
      end if;
    end if;
  end process;

  p_verify_rs_new_interval : process(clk)
  begin
    if rising_edge(clk) then
      if rs_restart = '1' then
        assert rs_new_interval = '1'
          report "Wrong begin of rs_new_interval"
          severity ERROR;
        tb_new_interval <= '1';
      elsif rs_sosi.sync = '1' then
        assert rs_new_interval = '0'
          report "Wrong end of rs_new_interval"
          severity ERROR;
        tb_new_interval <= '0';
      elsif tb_new_interval = '1' then
        assert rs_new_interval = '1'
          report "Wrong level during rs_new_interval"
          severity ERROR;
      else
        assert rs_new_interval = '0'
          report "Unexpected rs_new_interval"
          severity ERROR;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- DUT: dp_bsn_source_v2 --> dp_rsn_source
  -----------------------------------------------------------------------------

  u_bsn : entity work.dp_bsn_source_v2
  generic map (
    g_block_size         => g_bs_block_size,
    g_nof_clk_per_sync   => g_pps_interval,
    g_bsn_w              => c_bsn_w,
    g_bsn_time_offset_w  => c_bsn_time_offset_w
  )
  port map (
    rst              => rst,
    clk              => clk,
    pps              => ref_grid_bs.pps,
    -- MM control
    dp_on            => dp_on,
    dp_on_pps        => dp_on_pps,

    dp_on_status     => dp_on_status,  -- = src_out.valid
    bs_restart       => bs_restart,  -- = src_out.sync for first sync after dp_on went high
    bs_new_interval  => bs_new_interval,  -- active during first src_out.sync interval

    bsn_init         => bsn_init,
    bsn_time_offset  => bsn_time_offset,

    -- Streaming
    src_out          => bs_sosi
  );

  u_rsn : entity work.dp_rsn_source
  generic map (
    g_bs_block_size     => g_bs_block_size,
    g_rs_block_size     => g_rs_block_size,
    g_nof_clk_per_sync  => g_pps_interval,
    g_bsn_w             => c_bsn_w
  )
  port map (
    rst                 => rst,
    clk                 => clk,

    -- Input stream sosi control using BSN
    bs_sosi             => bs_sosi,  -- input reference stream using BSN

    -- Output stream sosi control using RSN
    rs_sosi             => rs_sosi,  -- output stream using RSN and g_rs_block_size, g_nof_clk_per_sync
    rs_restart          => rs_restart,  -- = rs_sosi.sync for first sync after bs_sosi.valid went high
    rs_new_interval     => rs_new_interval  -- = active during first rs_sosi.sync interval
  );
end tb;
