--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_corr_multiplier is
  generic (
    g_nof_inputs  : natural := 4;
    g_multiplier_factor : natural := 2
  );
end tb_corr_multiplier;

architecture tb of tb_corr_multiplier is
  constant c_clk_period            : time := 10 ns;
  constant c_data_w                : natural := 32;

  signal tb_end                    : std_logic := '0';
  signal clk                       : std_logic := '1';
  signal rst                       : std_logic;
begin
  clk <= not clk or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;
end tb;
