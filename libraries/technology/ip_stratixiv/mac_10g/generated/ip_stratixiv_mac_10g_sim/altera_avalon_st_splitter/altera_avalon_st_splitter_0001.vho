--IP Functional Simulation Model
--VERSION_BEGIN 11.1SP2 cbx_mgl 2012:01:25:21:26:09:SJ cbx_simgen 2012:01:25:21:25:27:SJ  VERSION_END


-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

--synthesis_resources = 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  altera_avalon_st_splitter_0001 IS 
	 PORT 
	 ( 
		 clk	:	IN  STD_LOGIC;
		 in0_data	:	IN  STD_LOGIC_VECTOR (63 DOWNTO 0);
		 in0_empty	:	IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 in0_endofpacket	:	IN  STD_LOGIC;
		 in0_error	:	IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 in0_ready	:	OUT  STD_LOGIC;
		 in0_startofpacket	:	IN  STD_LOGIC;
		 in0_valid	:	IN  STD_LOGIC;
		 out0_data	:	OUT  STD_LOGIC_VECTOR (63 DOWNTO 0);
		 out0_empty	:	OUT  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 out0_endofpacket	:	OUT  STD_LOGIC;
		 out0_error	:	OUT  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 out0_ready	:	IN  STD_LOGIC;
		 out0_startofpacket	:	OUT  STD_LOGIC;
		 out0_valid	:	OUT  STD_LOGIC;
		 out1_data	:	OUT  STD_LOGIC_VECTOR (63 DOWNTO 0);
		 out1_empty	:	OUT  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 out1_endofpacket	:	OUT  STD_LOGIC;
		 out1_error	:	OUT  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 out1_ready	:	IN  STD_LOGIC;
		 out1_startofpacket	:	OUT  STD_LOGIC;
		 out1_valid	:	OUT  STD_LOGIC
	 ); 
 END altera_avalon_st_splitter_0001;

 ARCHITECTURE RTL OF altera_avalon_st_splitter_0001 IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
 BEGIN

	in0_ready <= (out0_ready AND out1_ready);
	out0_data <= ( in0_data(63 DOWNTO 0));
	out0_empty <= ( in0_empty(2 DOWNTO 0));
	out0_endofpacket <= in0_endofpacket;
	out0_error <= ( in0_error(2 DOWNTO 0));
	out0_startofpacket <= in0_startofpacket;
	out0_valid <= (in0_valid AND out1_ready);
	out1_data <= ( in0_data(63 DOWNTO 0));
	out1_empty <= ( in0_empty(2 DOWNTO 0));
	out1_endofpacket <= in0_endofpacket;
	out1_error <= ( in0_error(2 DOWNTO 0));
	out1_startofpacket <= in0_startofpacket;
	out1_valid <= (in0_valid AND out0_ready);

 END RTL; --altera_avalon_st_splitter_0001
--synopsys translate_on
--VALID FILE
