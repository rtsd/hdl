-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for unb1_test.
-- Description:
--   The DUT can be targeted at unb 0, bn3 with the same Python scripts
--   that are used on hardware.
-- Usage:
--   On command line do:
--     > run_modelsim & (to start Modeslim)
--
--   In Modelsim do:
--     > lp unb1_test
--     > mk clean all (only first time to clean all libraries)
--     > mk all (to compile all libraries that are needed for unb1_test)
--     . load tb_unb1_test simulation by double clicking the tb_unb1_test icon
--     > as 10 (to view signals in Wave Window)
--     > run 100 us (or run -all)
--
--   On command line do:
--     > python $UPE_GEAR/peripherals/util_system_info.py --unb 0 --bn 3 -n 0 -v 5 --sim
--     > python $UPE_GEAR/peripherals/util_unb_sens.py --unb 0 --bn 3 -n 0 -v 5 --sim
--     > python $UPE_GEAR/peripherals/util_ppsh.py --unb 0 --bn 3 -n 1 -v 5 --sim
--

library ip_stratixiv_ddr3_mem_model_lib;

library IEEE, common_lib, unb1_board_lib, i2c_lib, io_ddr_lib, technology_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use common_lib.tb_common_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use tech_ddr_lib.tech_ddr_mem_model_component_pkg.all;
use work.unb1_test_pkg.all;

entity tb_unb1_test is
    generic (
      g_design_name : string  := "unb1_test";
      g_sim_unb_nr  : natural := 0;  -- UniBoard 0
      g_sim_node_nr : natural := 7  -- Back node 3
    );
end tb_unb1_test;

architecture tb of tb_unb1_test is
  constant c_sim             : boolean := true;
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(g_sim_unb_nr, c_unb1_board_nof_uniboard_w) & TO_UVEC(g_sim_node_nr, c_unb1_board_nof_chip_w);

  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb1_board_fw_version := (1, 0);

  constant c_cable_delay     : time := 12 ns;
  constant c_eth_clk_period  : time := 40 ns;  -- 25 MHz XO on UniBoard
  constant c_clk_period      : time := 5 ns;
  constant c_sa_clk_period   : time := 6.4 ns;
  constant c_pps_period      : natural := 1000;

  constant c_revision_select                  : t_unb1_test_config := func_unb1_test_sel_revision_rec(g_design_name);

  -- DUT
  signal clk                 : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal pps_rst             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic;
  signal eth_rxp             : std_logic;

  signal VERSION             : std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0) := c_version;
  signal ID                  : std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0)      := c_id;
  signal TESTIO              : std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;

  -- 10GbE
  signal sa_clk              : std_logic := '1';

  -- Serial I/O
  signal si_fn_lpbk_0        : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_lpbk_1        : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_lpbk_2        : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_lpbk_3        : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);

  -- Signals to interface with the MB_I DDR3 memory model.
  signal phy_MB_I_in         : t_tech_ddr3_phy_in;
  signal phy_MB_I_io         : t_tech_ddr3_phy_io;
  signal phy_MB_I_ou         : t_tech_ddr3_phy_ou;

  -- Signals to interface with the MB_II DDR3 memory model.
  signal phy_MB_II_in        : t_tech_ddr3_phy_in;
  signal phy_MB_II_io        : t_tech_ddr3_phy_io;
  signal phy_MB_II_ou        : t_tech_ddr3_phy_ou;

  -- Model I2C sensor slaves as on the UniBoard
  constant c_fpga_temp_address   : std_logic_vector(6 downto 0) := "0011000";  -- MAX1618 address LOW LOW
  constant c_fpga_temp           : integer := 60;
  constant c_eth_temp_address    : std_logic_vector(6 downto 0) := "0101001";  -- MAX1618 address MID LOW
  constant c_eth_temp            : integer := 40;
  constant c_hot_swap_address    : std_logic_vector(6 downto 0) := "1000100";  -- LTC4260 address L L L
  constant c_hot_swap_R_sense    : real := 0.01;  -- = 10 mOhm on UniBoard

  constant c_uniboard_current    : real := 5.0;  -- = assume 5.0 A on UniBoard
  constant c_uniboard_supply     : real := 48.0;  -- = assume 48.0 V on UniBoard
  constant c_uniboard_adin       : real := -1.0;  -- = NC on UniBoard
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  clk     <= not clk     after c_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (25 MHz)
  sa_clk  <= not sa_clk  after c_sa_clk_period / 2;  -- sa clock (156.25 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, clk, pps);

  ------------------------------------------------------------------------------
  -- 1GbE Loopback model
  ------------------------------------------------------------------------------
  eth_rxp <= transport eth_txp after c_cable_delay;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_unb1_test : entity work.unb1_test
    generic map (
      g_sim         => c_sim,
      g_sim_unb_nr  => g_sim_unb_nr,
      g_sim_node_nr => g_sim_node_nr,
      g_design_name => g_design_name
    )
    port map (
      -- GENERAL
      CLK         => clk,
      PPS         => pps,
      WDI         => WDI,
      INTA        => INTA,
      INTB        => INTB,

      SENS_SC     => sens_scl,
      SENS_SD     => sens_sda,

      -- Others
      VERSION     => VERSION,
      ID          => ID,
      TESTIO      => TESTIO,

      -- 1GbE Control Interface
      ETH_CLK     => eth_clk,
      ETH_SGIN    => eth_rxp,
      ETH_SGOUT   => eth_txp,

      -- Transceiver clocks
      SA_CLK      => sa_clk,

      -- Serial I/O
      SI_FN_0_TX  => si_fn_lpbk_0,
      SI_FN_0_RX  => si_fn_lpbk_0,
      SI_FN_1_TX  => si_fn_lpbk_1,
      SI_FN_1_RX  => si_fn_lpbk_1,
      SI_FN_2_TX  => si_fn_lpbk_2,
      SI_FN_2_RX  => si_fn_lpbk_2,
      SI_FN_3_TX  => si_fn_lpbk_3,
      SI_FN_3_RX  => si_fn_lpbk_3,

      BN_BI_0_TX  => OPEN,
      BN_BI_0_RX  => (others => '0'),
      BN_BI_1_TX  => OPEN,
      BN_BI_1_RX  => (others => '0'),
      BN_BI_2_TX  => OPEN,
      BN_BI_2_RX  => (others => '0'),
      BN_BI_3_TX  => OPEN,
      BN_BI_3_RX  => (others => '0'),

      MB_I_IN     => phy_MB_I_in,
      MB_I_IO     => phy_MB_I_io,
      MB_I_OU     => phy_MB_I_ou,

      MB_II_IN    => phy_MB_II_in,
      MB_II_IO    => phy_MB_II_io,
      MB_II_OU    => phy_MB_II_ou
    );

  ------------------------------------------------------------------------------
  -- MB_I DDR3 memory model
  ------------------------------------------------------------------------------
  gen_tech_ddr_memory_model_MB_I : if c_revision_select.use_ddr_MB_I = 1 generate
    u_tech_ddr_memory_model_MB_I : entity tech_ddr_lib.tech_ddr_memory_model
    generic map (
      g_tech_ddr => c_revision_select.use_tech_ddr
    )
    port map (
      mem3_in => phy_MB_I_ou,
      mem3_io => phy_MB_I_io,
      mem3_ou => phy_MB_I_in
    );
  end generate;

  ------------------------------------------------------------------------------
  -- MB_II DDR3 memory model
  ------------------------------------------------------------------------------
  gen_tech_ddr_memory_model_MB_II : if c_revision_select.use_ddr_MB_II = 1 generate
    u_tech_ddr_memory_model_MB_II : entity tech_ddr_lib.tech_ddr_memory_model
    generic map (
      g_tech_ddr => c_revision_select.use_tech_ddr
    )
    port map (
      mem3_in => phy_MB_II_ou,
      mem3_io => phy_MB_II_io,
      mem3_ou => phy_MB_II_in
    );
  end generate;

  ------------------------------------------------------------------------------
  -- UniBoard sensors
  ------------------------------------------------------------------------------
  -- I2C slaves that are available for each FPGA
  u_fpga_temp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_fpga_temp_address
  )
  port map (
    scl  => sens_scl,
    sda  => sens_sda,
    temp => c_fpga_temp
  );

  -- I2C slaves that are available only via FPGA back node 3
  u_eth_temp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_eth_temp_address
  )
  port map (
    scl  => sens_scl,
    sda  => sens_sda,
    temp => c_eth_temp
  );

  u_power : entity i2c_lib.dev_ltc4260
  generic map (
    g_address => c_hot_swap_address,
    g_R_sense => c_hot_swap_R_sense
  )
  port map (
    scl               => sens_scl,
    sda               => sens_sda,
    ana_current_sense => c_uniboard_current,
    ana_volt_source   => c_uniboard_supply,
    ana_volt_adin     => c_uniboard_adin
  );
end tb;
