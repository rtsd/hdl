-- -----------------------------------------------------------------------------
--
-- Copyright 2009-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.tech_mult_component_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_mult_lib;
library ip_arria10_mult_lib;
library ip_agi027_1e1v_mult_lib;

entity tech_mult is
  generic (
    g_technology       : natural  := c_tech_select_default;
    g_variant          : string := "IP";
    g_in_a_w           : positive := 18;
    g_in_b_w           : positive := 18;
    g_out_p_w          : positive := 36;  -- c_prod_w = g_in_a_w+g_in_b_w, use smaller g_out_p_w to truncate MSbits, or larger g_out_p_w to extend MSbits
    g_nof_mult         : positive := 1;  -- using 2 for 18x18, 4 for 9x9 may yield better results when inferring * is used
    g_pipeline_input   : natural  := 1;  -- 0 or 1
    g_pipeline_product : natural  := 1;  -- 0 or 1
    g_pipeline_output  : natural  := 1;  -- >= 0
    g_representation   : string   := "SIGNED"  -- or "UNSIGNED"
  );
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    out_p      : out std_logic_vector(g_nof_mult * g_out_p_w - 1 downto 0)
  );
end tech_mult;

architecture str of tech_mult is
  -- When g_out_p_w < g_in_a_w+g_in_b_w then the LPM_MULT truncates the LSbits of the product. Therefore
  -- define c_prod_w to be able to let common_mult truncate the LSBits of the product.
  constant c_prod_w : natural := g_in_a_w + g_in_b_w;

  signal prod  : std_logic_vector(g_nof_mult * c_prod_w - 1 downto 0);
begin
  gen_ip_stratixiv_ip : if (g_technology = c_tech_stratixiv and g_variant = "IP") generate
    u0 : ip_stratixiv_mult
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_output  => g_pipeline_output,
      g_representation   => g_representation
    )
    port map(
      clk        => clk,
      clken      => clken,
      in_a       => in_a,
      in_b       => in_b,
      out_p      => prod
    );
  end generate;

  gen_ip_stratixiv_rtl : if (g_technology = c_tech_stratixiv and g_variant = "RTL") generate
    u0 : ip_stratixiv_mult_rtl
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_output  => g_pipeline_output,
      g_representation   => g_representation
    )
    port map(
      rst        => rst,
      clk        => clk,
      clken      => clken,
      in_a       => in_a,
      in_b       => in_b,
      out_p      => prod
    );
  end generate;

  gen_ip_arria10_ip : if ((g_technology = c_tech_arria10_proto or g_technology = c_tech_arria10_e3sge3 or g_technology = c_tech_arria10_e1sg or g_technology = c_tech_arria10_e2sg ) and g_variant = "IP") generate
    u0 : ip_arria10_mult
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_output  => g_pipeline_output,
      g_representation   => g_representation
    )
    port map(
      clk        => clk,
      clken      => clken,
      in_a       => in_a,
      in_b       => in_b,
      out_p      => prod
    );
  end generate;

  gen_ip_arria10_rtl : if ((g_technology = c_tech_arria10_proto or g_technology = c_tech_arria10_e3sge3 or g_technology = c_tech_arria10_e1sg or g_technology = c_tech_arria10_e2sg ) and g_variant = "RTL") generate
    u0 : ip_arria10_mult_rtl
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_output  => g_pipeline_output,
      g_representation   => g_representation
    )
    port map(
      rst        => rst,
      clk        => clk,
      clken      => clken,
      in_a       => in_a,
      in_b       => in_b,
      out_p      => prod
    );
  end generate;

  gen_ip_agi027_1e1v_ip : if (g_technology = c_tech_agi027_1e1v and g_variant = "IP") generate
    u0 : ip_agi027_1e1v_mult
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_output  => g_pipeline_output,
      g_representation   => g_representation
    )
    port map(
      clk        => clk,
      clken      => clken,
      in_a       => in_a,
      in_b       => in_b,
      out_p      => prod
    );
  end generate;

  gen_ip_agi027_1e1v_rtl : if (g_technology = c_tech_agi027_1e1v and g_variant = "RTL") generate
    u0 : ip_agi027_1e1v_mult_rtl
    generic map(
      g_in_a_w           => g_in_a_w,
      g_in_b_w           => g_in_b_w,
      g_out_p_w          => g_out_p_w,
      g_nof_mult         => g_nof_mult,
      g_pipeline_input   => g_pipeline_input,
      g_pipeline_product => g_pipeline_product,
      g_pipeline_output  => g_pipeline_output,
      g_representation   => g_representation
    )
    port map(
      rst        => rst,
      clk        => clk,
      clken      => clken,
      in_a       => in_a,
      in_b       => in_b,
      out_p      => prod
    );
  end generate;

  gen_trunk : for I in 0 to g_nof_mult - 1 generate
  -- Truncate MSbits, also for signed (common_pkg.vhd for explanation of RESIZE_SVEC)
    out_p((I + 1) * g_out_p_w - 1 downto I * g_out_p_w) <= RESIZE_SVEC(prod((I + 1) * c_prod_w - 1 downto I * c_prod_w), g_out_p_w) when g_representation = "SIGNED" else
                                                  RESIZE_UVEC(prod((I + 1) * c_prod_w - 1 downto I * c_prod_w), g_out_p_w);
  end generate;
end str;
