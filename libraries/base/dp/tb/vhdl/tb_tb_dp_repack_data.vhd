-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_repack_data
-- Description:
-- Usage:
-- > as 6
-- > run -all

entity tb_tb_dp_repack_data is
end tb_tb_dp_repack_data;

architecture tb of tb_tb_dp_repack_data is
  constant c_nof_repeat  : natural := 5;

  constant c_flow       : t_dp_flow_control_enum_arr := c_dp_flow_control_enum_arr;  -- for stimuli and verify flow
                                                                                     -- control options
  constant c_bypass     : t_nat_boolean_arr := c_nat_boolean_arr;  -- for g_in_bypass and g_pack_bypass variants
  constant c_in         : t_nat_boolean_arr := c_nat_boolean_arr;  -- for g_use_repack_in_load variants

  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- general
  -- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
  -- g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
  -- -- specific
  -- g_use_repack_in_load     : boolean := false;
  -- g_in_dat_w               : NATURAL := 5;
  -- g_in_nof_words           : NATURAL := 2;
  -- g_pack_dat_w             : NATURAL := 16;
  -- g_pack_nof_words         : NATURAL := 1;
  -- g_in_bypass              : BOOLEAN := FALSE;  -- can use TRUE when g_in_nof_words=1  or g_in_nof_words=g_out_nof_words
  -- g_pack_bypass            : BOOLEAN := FALSE;  -- can use TRUE when g_out_nof_words=1 or g_in_nof_words=g_out_nof_words
  -- g_in_symbol_w            : NATURAL := 1;      -- default 1 for snk_in.empty  in nof bits, else use power of 2
  -- g_pack_symbol_w          : NATURAL := 1;      -- default 1 for src_out.empty in nof bits, else use power of 2
  -- g_nof_repeat             : NATURAL := 10;
  -- g_pkt_len                : NATURAL := 11;     -- if not a multiple of g_in_nof_words then the input stage flush creates gap between blocks
  -- g_pkt_gap                : NATURAL := 0

  g_in : for V in 0 to 1 generate  -- 0 = false, 1 = true
    g_flow_control_stimuli : for I in 0 to 2 generate  -- 0 = e_active, 1 = e_random, 2 = e_pulse
      g_flow_control_verify : for J in 0 to 2 generate  -- 0 = e_active, 1 = e_random, 2 = e_pulse

        -------------------------------------------------------------------------
        -- Tests that can use bypass
        -- . g_in_nof_words = 1
        -- . g_out_nof_words = 1
        -- . g_in_nof_words = g_in_nof_words >= 1
        -------------------------------------------------------------------------

        gen_bool_bypass : for K in 0 to 1 generate  -- 0 = false, 1 = true
          gen_bool_bypass : for L in 0 to 1 generate  -- 0 = false, 1 = true
            -- no repack, g_in_nof_words = g_out_nof_words = 1
            u_16_1_16_1_len_10_gap_0   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 16, 1, 16, 1, c_bypass(K), c_bypass(L), 1, 1, c_nof_repeat, 10, 0);  -- g_pkt_len > g_in_nof_words
            u_16_1_16_1_len_3_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 16, 1, 16, 1, c_bypass(K), c_bypass(L), 1, 1, c_nof_repeat,  3, 0);  -- g_pkt_len > g_in_nof_words, odd
            u_16_1_16_1_len_2_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 16, 1, 16, 1, c_bypass(K), c_bypass(L), 1, 1, c_nof_repeat,  2, 0);  -- g_pkt_len > g_in_nof_words, even
            u_16_1_16_1_len_1_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 16, 1, 16, 1, c_bypass(K), c_bypass(L), 1, 1, c_nof_repeat,  1, 0);  -- g_pkt_len = g_in_nof_words

            u_16_1_16_1_len_1_gap_1    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 16, 1, 16, 1, c_bypass(K), c_bypass(L), 1, 1, c_nof_repeat,  1, 1);  -- g_pkt_gap > 0
          end generate;

          -- no repack, g_in_nof_words = g_out_nof_words > 1
          u_16_3_16_3_len_10_gap_0   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  16, 3, 16, 3, c_bypass(K), c_bypass(K), 1, 1, c_nof_repeat, 10,  0);

          -- g_in_nof_words > g_pack_nof_words can use always active stimuli except when g_pkt_len MOD g_in_nof_words /= 0, because then the input stage needs to flush
          u_8_4_32_1_len_1_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  8, 4, 32, 1, false, c_bypass(K), 1, 1, c_nof_repeat,  1,  0);  -- g_pkt_len < g_in_nof_words
          u_8_4_32_1_len_2_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  8, 4, 32, 1, false, c_bypass(K), 1, 1, c_nof_repeat,  2,  0);  -- g_pkt_len = g_in_nof_words
          u_8_4_32_1_len_3_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  8, 4, 32, 1, false, c_bypass(K), 1, 1, c_nof_repeat,  3,  0);  -- g_pkt_len > g_in_nof_words, MOD /= 0
          u_8_4_32_1_len_10_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  8, 4, 32, 1, false, c_bypass(K), 1, 1, c_nof_repeat, 10,  0);  -- g_pkt_len > g_in_nof_words, MOD /= 0
          u_8_4_32_1_len_11_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  8, 4, 32, 1, false, c_bypass(K), 1, 1, c_nof_repeat, 11,  0);  -- g_pkt_len > g_in_nof_words, MOD /= 0
          u_8_4_32_1_len_12_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  8, 4, 32, 1, false, c_bypass(K), 1, 1, c_nof_repeat, 12,  0);  -- g_pkt_len > g_in_nof_words, MOD = 0

          u_8_4_32_1_len_12_gap_2    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  8, 4, 32, 1, false, c_bypass(K), 1, 1, c_nof_repeat, 12,  2);  -- g_pkt_gap > 0

          -- g_in_nof_words < g_pack_nof_words will apply backpressure, because the output stage needs to output more
          u_32_1_8_4_len_1_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 32, 1, 8, 4, c_bypass(K), false, 1, 1, c_nof_repeat,  1,  0);  -- g_pkt_len = g_in_nof_words
          u_32_1_8_4_len_2_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 32, 1, 8, 4, c_bypass(K), false, 1, 1, c_nof_repeat,  2,  0);  -- g_pkt_len > g_in_nof_words
          u_32_1_8_4_len_3_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 32, 1, 8, 4, c_bypass(K), false, 1, 1, c_nof_repeat,  3,  0);  -- g_pkt_len > g_in_nof_words
          u_32_1_8_4_len_10_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 32, 1, 8, 4, c_bypass(K), false, 1, 1, c_nof_repeat, 10,  0);  -- g_pkt_len > g_in_nof_words

          u_32_1_8_4_len_11_gap_1    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 32, 1, 8, 4, c_bypass(K), false, 1, 1, c_nof_repeat, 11,  1);  -- g_pkt_gap > 0

          -- g_in_dat_w MOD 8 /= 0, g_in_nof_words=1
          u_14_1_8_2_len_10_gap_0   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 14, 1, 8, 2, c_bypass(K), false, 1, 1, c_nof_repeat, 10,  0);  -- repack with subsection padding, even multiple of g_in_nof_words
          u_14_1_8_2_len_11_gap_0   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 14, 1, 8, 2, c_bypass(K), false, 1, 1, c_nof_repeat, 11,  0);  -- repack with subsection padding, odd multiple of g_in_nof_words

          -- g_in_dat_w MOD 8 /= 0, g_out_nof_words=1
          u_5_2_16_1_len_10_gap_0  : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 5, 2, 16, 1, false, c_bypass(K), 1, 1, c_nof_repeat, 10,  0);  -- repack with subsection padding, integer multiple of g_in_nof_words
          u_5_2_16_1_len_11_gap_0  : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 5, 2, 16, 1, false, c_bypass(K), 1, 1, c_nof_repeat, 11,  0);  -- repack with subsection padding, fractional multiple of g_in_nof_words

          -- g_in_nof_words=1, g_pack_nof_words>1
          u_8_1_4_2_len_10_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   8, 1,  4,  2, c_bypass(K), false, 1, 1, c_nof_repeat, 10,  0);
          u_512_1_32_16_len_1_gap_20 : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 512, 1, 32, 16, c_bypass(K), false, 1, 1, c_nof_repeat,  1, 20);  -- pack a larger header slv into g_pack_dat_w words

          -- serialize to and deserialize from g_pack_dat_w=1 bit
          u_8_1_1_8_len_10_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  8, 1, 1,  8, c_bypass(K), false, 1, 1, c_nof_repeat, 10,  0);  -- g_pack_dat_w=1
          u_32_1_1_32_len_10_gap_7   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 32, 1, 1, 32, c_bypass(K), false, 1, 1, c_nof_repeat, 10,  7);  -- g_pack_dat_w=1

          -- g_in_symbol_w /= 1, g_out_symbol_w /= 1
          u_20_1_8_3_symbol_1_4_len_10_gap_0  : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 20, 1, 8, 3, c_bypass(K), false, 1, 4, c_nof_repeat, 10, 0);  -- no repack
          u_20_1_8_3_symbol_4_1_len_10_gap_0  : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 20, 1, 8, 3, c_bypass(K), false, 4, 1, c_nof_repeat, 10, 0);  -- no repack
          u_20_1_8_3_symbol_4_4_len_10_gap_0  : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 20, 1, 8, 3, c_bypass(K), false, 4, 4, c_nof_repeat, 10, 0);  -- no repack

          -- pack ETH/IP/UDP header, g_in_symbol_w = 8, g_out_symbol_w = 8
          u_336_1_32_11_symbol_8_8_len_1_gap_0  : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 336, 1, 32, 11, c_bypass(K), false, 8, 8, c_nof_repeat, 1, 0);  -- pack to 32 bit --> empty = 2
          u_336_1_64_6_symbol_8_8_len_1_gap_0   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V), 336, 1, 64,  6, c_bypass(K), false, 8, 8, c_nof_repeat, 1, 0);  -- pack to 64 bit --> empty = 6
        end generate;

        -------------------------------------------------------------------------
        -- Tests that cannot use bypass
        -------------------------------------------------------------------------

        -- g_in_nof_words > 1 and g_pack_nof_words > 1
        u_24_2_16_3_len_1_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  24, 2, 16, 3, false, false, 1, 1, c_nof_repeat,  1, 0);  -- g_pkt_len < g_in_nof_words
        u_24_2_16_3_len_2_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  24, 2, 16, 3, false, false, 1, 1, c_nof_repeat,  2, 0);  -- g_pkt_len = g_in_nof_words
        u_24_2_16_3_len_3_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  24, 2, 16, 3, false, false, 1, 1, c_nof_repeat,  3, 0);  -- g_pkt_len = fractional multiple of g_in_nof_words
        u_24_2_16_3_len_10_gap_0   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  24, 2, 16, 3, false, false, 1, 1, c_nof_repeat, 10, 0);  -- g_pkt_len = integer multiple of g_in_nof_words
        u_24_2_16_3_len_11_gap_0   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  24, 2, 16, 3, false, false, 1, 1, c_nof_repeat, 11, 0);  -- g_pkt_len = fractional multiple of g_in_nof_words

        u_24_2_16_3_len_11_gap_3   : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  24, 2, 16, 3, false, false, 1, 1, c_nof_repeat, 11, 3);  -- g_pkt_gap > 0

        -- g_in_dat_w MOD 8 /= 0
        u_6_5_10_3_len_1_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   6, 5, 10, 3, false, false, 1, 1, c_nof_repeat,  1, 0);  -- g_pkt_len < g_in_nof_words
        u_6_5_10_3_len_2_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   6, 5, 10, 3, false, false, 1, 1, c_nof_repeat,  2, 0);  -- g_pkt_len < g_in_nof_words
        u_6_5_10_3_len_3_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   6, 5, 10, 3, false, false, 1, 1, c_nof_repeat,  3, 0);  -- g_pkt_len < g_in_nof_words
        u_6_5_10_3_len_4_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   6, 5, 10, 3, false, false, 1, 1, c_nof_repeat,  4, 0);  -- g_pkt_len < g_in_nof_words
        u_6_5_10_3_len_5_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   6, 5, 10, 3, false, false, 1, 1, c_nof_repeat,  5, 0);  -- g_pkt_len = g_in_nof_words
        u_6_5_10_3_len_10_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   6, 5, 10, 3, false, false, 1, 1, c_nof_repeat, 10, 0);  -- g_pkt_len = integer multiple of g_in_nof_words
        u_6_5_10_3_len_11_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   6, 5, 10, 3, false, false, 1, 1, c_nof_repeat, 11, 0);  -- g_pkt_len = fractional multiple of g_in_nof_words

        u_6_5_10_3_len_21_gap_3    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),   6, 5, 10, 3, false, false, 1, 1, c_nof_repeat, 21, 3);  -- g_pkt_gap > 0

        -- subsection padding, g_in_dat_w * g_in_nof_words < g_pack_dat_w * g_pack_nof_words
        u_18_2_8_5_len_1_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  18, 2,  8, 5, false, false, 1, 1, c_nof_repeat,  1, 0);  -- g_pkt_len < g_in_nof_words
        u_18_2_8_5_len_2_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  18, 2,  8, 5, false, false, 1, 1, c_nof_repeat,  2, 0);  -- g_pkt_len = g_in_nof_words
        u_18_2_8_5_len_3_gap_0     : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  18, 2,  8, 5, false, false, 1, 1, c_nof_repeat,  3, 0);  -- g_pkt_len = fractional multiple of g_in_nof_words
        u_18_2_8_5_len_10_gap_0    : entity work.tb_dp_repack_data generic map (c_flow(I), c_flow(J), c_in(V),  18, 2,  8, 5, false, false, 1, 1, c_nof_repeat, 10, 0);  -- g_pkt_len = integer multiple of g_in_nof_words
      end generate;
    end generate;
  end generate;
end tb;
