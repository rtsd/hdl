-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dev_max6652 is
  generic(
    g_address   : std_logic_vector(6 downto 0)
  );
  port(
    scl         : in    std_logic;
    sda         : inout std_logic;
    volt_2v5    : in    integer;  -- unit 13 mV
    volt_3v3    : in    integer;  -- unit 17 mV
    volt_12v    : in    integer;  -- unit 62 mV
    volt_vcc    : in    integer;  -- unit 26 mV
    temp        : in    integer  -- unit degrees C
  );
end dev_max6652;

architecture beh of dev_max6652 is
  constant c_cmd_read_2v5  : std_logic_vector(7 downto 0) := "00100000";
  constant c_cmd_read_12v  : std_logic_vector(7 downto 0) := "00100001";
  constant c_cmd_read_3v3  : std_logic_vector(7 downto 0) := "00100010";
  constant c_cmd_read_vcc  : std_logic_vector(7 downto 0) := "00100011";
  constant c_cmd_read_temp : std_logic_vector(7 downto 0) := "00100111";
  constant c_cmd_config    : std_logic_vector(7 downto 0) := "01000000";

  signal enable    : std_logic;  -- new access, may be write command or write data
  signal stop      : std_logic;  -- end of access
  signal wr_dat    : std_logic_vector(7 downto 0);  -- I2C write data
  signal wr_val    : std_logic;
  signal rd_dat    : std_logic_vector(7 downto 0);  -- I2C read data
  signal rd_req    : std_logic;

  signal cmd_en     : std_logic := '0';
  signal cmd        : std_logic_vector(7 downto 0);  -- device command
  signal config_reg : std_logic_vector(7 downto 0) := "00001000";
begin
  i2c_slv_device : entity work.i2c_slv_device
  generic map (
    g_address => g_address
  )
  port map (
    scl     => scl,
    sda     => sda,
    en      => enable,
    p       => stop,
    wr_dat  => wr_dat,
    wr_val  => wr_val,
    rd_req  => rd_req,
    rd_dat  => rd_dat
  );

  p_write : process (enable, wr_val)  -- first write byte is treated as command
  begin
    if rising_edge(enable) then
      cmd_en <= '1';
    elsif falling_edge(enable) then
      cmd_en <= '0';
    end if;
    if rising_edge(wr_val) then
      cmd_en <= '0';
      if cmd_en = '1' then
        cmd <= wr_dat;
      else
        case cmd is
          when c_cmd_config => config_reg <= wr_dat;
          when others       => null;  -- no further model for write access
        end case;
      end if;
    end if;
  end process;

  p_read : process (rd_req)
  begin
    if rising_edge(rd_req) then
      case cmd is  -- only model read V and read temp
        when c_cmd_read_2v5  => rd_dat <= std_logic_vector(to_unsigned(volt_2v5, 8));
        when c_cmd_read_12v  => rd_dat <= std_logic_vector(to_unsigned(volt_12v, 8));
        when c_cmd_read_3v3  => rd_dat <= std_logic_vector(to_unsigned(volt_3v3, 8));
        when c_cmd_read_vcc  => rd_dat <= std_logic_vector(to_unsigned(volt_vcc, 8));
        when c_cmd_read_temp => rd_dat <= std_logic_vector(to_signed(temp, 8));
        when c_cmd_config    => rd_dat <= config_reg;
        when others          => rd_dat <= (others => '1');
      end case;
    end if;
  end process;
end beh;
