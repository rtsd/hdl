-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Store the 4 multiplexed signal streams into one DDR3 and readback

library IEEE, common_lib, unb_common_lib, dp_lib, ddr3_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use ddr3_lib.ddr3_pkg.all;

entity bn_capture_storage is
  generic (
    g_sim           : boolean := false;
    -- Multiplexed signal path interface
    g_nof_channel   : natural := 2;  -- nof channel is 4, i.e. port A, B, C and D, or 2 for only port A, B or port C, D
    g_start_channel : natural := 0;  -- start storage when sop for this g_start_channel arrives
    g_mux_data_w    : natural := 256;  -- multiplexed data width is 256 so equal to the internal DDR3 data width
    -- DDR3 Interface
    g_ddr           : t_c_ddr3_phy := c_ddr3_phy_4g  -- use c_ddr3_phy_4g or c_ddr3_phy_1g dependent on what was generated with the MegaWizard
  );
  port (
    mm_rst          : in  std_logic;
    mm_clk          : in  std_logic;

    dp_rst          : in  std_logic;
    dp_clk          : in  std_logic;

    phy_rst         : out std_logic;
    phy_clk         : out std_logic;

    phy_rst_2x      : out std_logic;
    phy_clk_2x      : out std_logic;

    ext_clk         : in  std_logic;  -- for DDR3 controller

    -- ST sink (multiplexed input signal paths)
    mux_wide_siso   : out t_dp_siso := c_dp_siso_rdy;
    mux_wide_sosi   : in  t_dp_sosi;  -- = Signal Paths A[255:0], B[255:0], C[255:0], D[255:0] multiplexed in time
                                       -- and e.g. A[255:0] = [A(t0), A(t1), ..., A(t31)], so 4*8=32 8b samples in time per one 256b word
    -- MM registers
    ctrl_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    ctrl_miso       : out t_mem_miso;

    data_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    data_miso       : out t_mem_miso := c_mem_miso_rst;

    -- SO-DIMM Memory Bank
    ddr3_in         : in    t_ddr3_phy_in;
    ddr3_io         : inout t_ddr3_phy_io;
    ddr3_ou         : out   t_ddr3_phy_ou
  );
end bn_capture_storage;

architecture str of bn_capture_storage is
  constant c_wr_fifo_depth : natural := 128;
  constant c_rd_fifo_depth : natural := 2048;

  constant c_channel_w     : natural := ceil_log2(g_nof_channel);

  type t_state is (s_idle, s_flush, s_stop);

  signal ctlr_init_done    : std_logic;
  signal ctlr_rdy          : std_logic;

  signal i_phy_clk         : std_logic;
  signal i_phy_rst         : std_logic;

  signal dvr_flush         : std_logic;

  signal dvr_done          : std_logic;
  signal dvr_en            : std_logic;
  signal dvr_wr_not_rd     : std_logic;
  signal dvr_start_addr    : t_ddr3_addr;
  signal dvr_end_addr      : t_ddr3_addr;

  signal rd_siso           : t_dp_siso;
  signal rd_sosi           : t_dp_sosi;

  signal rd_usedw          : std_logic_vector(ceil_log2(c_rd_fifo_depth) - 1 downto 0);
  signal mm_rd_usedw       : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_rd_data        : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_rd_val         : std_logic;
  signal mm_rd             : std_logic;

  signal state             : t_state;
  signal nxt_state         : t_state;
begin
  phy_clk <= i_phy_clk;
  phy_rst <= i_phy_rst;

  u_ddr3 : entity ddr3_lib.ddr3
  generic map(
    g_sim           => g_sim,
    g_phy           => 0,
    g_ddr           => g_ddr,
    g_mts           => 800,
    g_wr_data_w     => c_ddr3_ctlr_data_w,
    g_rd_data_w     => c_word_w,
    g_wr_fifo_depth => c_wr_fifo_depth,
    g_rd_fifo_depth => c_rd_fifo_depth / 8,
    g_wr_use_ctrl   => true
  )
  port map (
    ctlr_ref_clk       => ext_clk,
    ctlr_rst           => dp_rst,

    ctlr_gen_clk       => i_phy_clk,
    ctlr_gen_rst       => i_phy_rst,
    ctlr_gen_clk_2x    => phy_clk_2x,
    ctlr_gen_rst_2x    => phy_rst_2x,

    ctlr_init_done     => ctlr_init_done,
    ctlr_rdy           => ctlr_rdy,

    dvr_start_addr     => dvr_start_addr,
    dvr_end_addr       => dvr_end_addr,

    dvr_en             => dvr_en,
    dvr_wr_not_rd      => dvr_wr_not_rd,
    dvr_done           => dvr_done,
    --dvr_flush          => dvr_flush,

    wr_clk             => dp_clk,
    wr_rst             => dp_rst,

    wr_sosi            => mux_wide_sosi,
    wr_siso            => mux_wide_siso,

    -- ddr3 rd FIFO ST interface to the dp->mm adapter
    rd_sosi            => rd_sosi,
    rd_siso            => rd_siso,

    rd_clk             => mm_clk,
    rd_rst             => mm_rst,

    rd_fifo_usedw      => rd_usedw,

    phy_in             => ddr3_in,
    phy_io             => ddr3_io,
    phy_ou             => ddr3_ou
  );

  -- Flush ddr3 module's FIFO (keep sinking the stream but simply discard the
  -- data) after reset to prevent ddr3 write fifo from filling up - which would
  -- cause problems downstream (dp_mux uses two fifos that cannot be
  -- allowed to fill up too much).
  -- Also flush the ddr3 module's FIFO when it is reading.

  p_reg : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      state <= s_flush;
    elsif rising_edge(dp_clk) then
      state <= nxt_state;
    end if;
  end process;

  p_state : process(state, dvr_done, dvr_en, dvr_wr_not_rd, mux_wide_sosi)
  begin
    nxt_state <= state;
    dvr_flush <= '0';
    case state is
      when s_idle =>
        if dvr_done = '1' then
          dvr_flush <= '1';
          nxt_state <= s_flush;
        end if;
      when s_flush =>
        dvr_flush <= '1';
        if dvr_en = '1' and dvr_wr_not_rd = '1' then
          nxt_state <= s_stop;
        end if;
      when others =>  -- s_stop
        dvr_flush <= '1';
        if mux_wide_sosi.sop = '1' and unsigned(mux_wide_sosi.channel(c_channel_w - 1 downto 0)) = g_start_channel then
          nxt_state <= s_idle;
        end if;
    end case;
  end process;

  u_dp_fifo_to_mm : entity dp_lib.dp_fifo_to_mm
  generic map(
    g_fifo_size => c_rd_fifo_depth
  )
  port map (
     rst       => mm_rst,
     clk       => mm_clk,

     snk_out   => rd_siso,
     snk_in    => rd_sosi,
     usedw     => rd_usedw,  -- used words from rd FIFO

     mm_rd     => mm_rd,
     mm_rddata => mm_rd_data,
     mm_rdval  => mm_rd_val,
     mm_usedw  => mm_rd_usedw  -- resized to 32 bits
  );

  -- DDR3 streaming read output to mm bus
  data_miso.rddata(c_word_w - 1 downto 0) <= mm_rd_data;
  data_miso.rdval                       <= mm_rd_val;
  mm_rd                                 <= data_mosi.rd;

  u_storage_reg : entity work.bn_capture_storage_reg
  generic map(
    g_ddr => g_ddr
  )
  port map (
     mm_rst         => mm_rst,
     mm_clk         => mm_clk,
     st_rst         => i_phy_rst,
     st_clk         => i_phy_clk,

     sla_in         => ctrl_mosi,
     sla_out        => ctrl_miso,

     st_en_evt      => dvr_en,
     st_wr_not_rd   => dvr_wr_not_rd,

     st_start_addr  => dvr_start_addr,
     st_end_addr    => dvr_end_addr,

     st_done        => dvr_done,
     st_init_done   => ctlr_init_done,
     st_ctlr_rdy    => ctlr_rdy,

     mm_rd_usedw    => mm_rd_usedw
  );
end str;
