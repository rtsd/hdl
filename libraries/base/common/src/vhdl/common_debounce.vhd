-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose:
--
-- The output follows the input, but only if the input is stable for at least
-- g_latency clock cycles.

entity common_debounce is
  generic (
    g_type       : string := "BOTH";  -- "BOTH" = debounce g_latency clk cycles for both bgoing high when d_in='1' and for going low when d_in='0'
                                      -- "HIGH" = debounce g_latency clk cycles for going high when d_in='1', go low  immediately when d_in='0'
                                      -- "LOW"  = debounce g_latency clk cycles for going low  when d_in='0', go high immediately when d_in='1'
    g_delay_len  : natural := c_meta_delay_len;  -- = 3,  combat meta stability
    g_latency    : natural := 8;  -- >= 1, combat debounces over nof clk cycles
    g_init_level : std_logic := '1'
  );
  port (
    rst   : in  std_logic := '0';
    clk   : in  std_logic;
    clken : in  std_logic := '1';
    d_in  : in  std_logic;
    q_out : out std_logic
  );
end common_debounce;

architecture rtl of common_debounce is
  constant c_latency_w  : natural := ceil_log2(g_latency + 1);

  signal cnt         : std_logic_vector(c_latency_w - 1 downto 0);  -- use cnt = g_latency to stop the counter
  signal cnt_clr     : std_logic;
  signal cnt_en      : std_logic;
  signal stable_d    : std_logic;

  signal d_dly       : std_logic_vector(0 to g_delay_len - 1) := (others => g_init_level);
  signal d_reg       : std_logic := g_init_level;
  signal prev_d      : std_logic := g_init_level;
  signal i_q_out     : std_logic := g_init_level;
  signal nxt_q_out   : std_logic;
begin
  q_out <= i_q_out;

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      d_dly    <= (others => g_init_level);
      d_reg    <= g_init_level;
      prev_d   <= g_init_level;
      i_q_out  <= g_init_level;
    elsif rising_edge(clk) then
      if clken = '1' then
        d_dly    <= d_in & d_dly(0 to d_dly'high - 1);
        d_reg    <= d_dly(d_dly'high);
        prev_d   <= d_reg;
        i_q_out  <= nxt_q_out;
      end if;
    end if;
  end process;

  stable_d <= '1' when unsigned(cnt) >= g_latency else '0';
  cnt_en   <= not stable_d;

  gen_both : if g_type = "BOTH" generate
    cnt_clr <= d_reg xor prev_d;
    nxt_q_out <= prev_d when stable_d = '1' else i_q_out;
  end generate;

  gen_high : if g_type = "HIGH" generate
    cnt_clr <= not d_reg;
    nxt_q_out <= prev_d when stable_d = '1' else '0';
  end generate;

  gen_low : if g_type = "LOW" generate
    cnt_clr <= d_reg;
    nxt_q_out <= prev_d when stable_d = '1' else '1';
  end generate;

  u_counter : entity work.common_counter
  generic map (
    g_width     => c_latency_w
  )
  port map (
    rst     => '0',
    clk     => clk,
    clken   => clken,
    cnt_clr => cnt_clr,
    cnt_en  => cnt_en,
    count   => cnt
  );
end rtl;
