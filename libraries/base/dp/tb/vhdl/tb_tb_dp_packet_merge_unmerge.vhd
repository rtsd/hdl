-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- ----------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- Author: E. Kooistra
-- Purpose: Verify multiple variations of tb_dp_packet_merge_unmerge
-- Description:
-- Usage:
-- > as 8, to see that correct instances are generated
-- > run -all

entity tb_tb_dp_packet_merge_unmerge is
end tb_tb_dp_packet_merge_unmerge;

architecture tb of tb_tb_dp_packet_merge_unmerge is
  constant c_nof_repeat            : natural := 100;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --     -- general
  --     g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
  --     g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
  --     -- specific
  --     g_pipeline_ready         : boolean := true;
  --     g_data_w                 : natural := 4;
  --     g_nof_repeat             : natural := 20;
  --     g_nof_pkt_merge          : natural := 3;
  --     g_pkt_len_merge          : natural := 29;
  --     g_pkt_len_unmerge        : natural := 29;
  --     g_pkt_gap                : natural := 0;
  --     g_bsn_increment          : natural := 0;

  -- No flow control
  u_act_act_8_nof_0       : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 0, 29, 29, 0, 1);
  u_act_act_8_nof_1       : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 1, 29, 29, 0, 1);
  u_act_act_8_nof_2       : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 2, 29, 29, 0, 1);
  u_act_act_8_nof_3       : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 3, 29, 29, 0, 1);
  u_act_act_8_nof_4       : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 4, 29, 29, 0, 1);
  u_act_act_8_nof_4_bsn_0 : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 4, 29, 29, 0, 0);
  u_act_act_8_nof_4_bsn_2 : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 4, 29, 29, 0, 2);
  u_act_act_8_nof_5       : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 5, 29, 29, 0, 1);
  u_act_act_8_nof_6       : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 6, 29, 29, 0, 1);
  u_act_act_8_nof_7       : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 7, 29, 29, 0, 1);

  -- Fractional unmerge
  -- . u_act_act_8_nof_3_more_unmerge  : ceil((3 * 7) / 6) = 4 and (3 * 7) mod 6 = 3, so there are 4 unmerged packets and last has length 3
  -- . u_act_act_8_nof_3_last_len_1    : (3 * 7) mod 10 = 1, so last unmerged packet has length 1
  -- . u_act_act_8_nof_3_only_one      : (3 * 7) / 21 = 1, so there is only one unmerged packet with length 21
  -- . u_act_act_8_nof_3_only_one_frac : (3 * 7) / 22 = 0 and (3 * 7) mod 22 = 21, so there is only one unmerged packet with length 21
  u_act_act_8_nof_3_more_unmerge  : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 3, 7,  6, 0, 1);
  u_act_act_8_nof_3_last_len_1    : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 3, 7, 10, 0, 1);
  u_act_act_8_nof_3_only_one      : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 3, 7, 21, 0, 1);
  u_act_act_8_nof_3_only_one_frac : entity work.tb_dp_packet_merge_unmerge generic map ( e_active, e_active,  true, 8, c_nof_repeat, 3, 7, 30, 0, 1);

  -- Flow control
  u_rnd_act_8_nof_3       : entity work.tb_dp_packet_merge_unmerge generic map ( e_random, e_active,  true, 8, c_nof_repeat, 3, 29, 29, 0, 1);
  u_rnd_rnd_8_nof_3_comb  : entity work.tb_dp_packet_merge_unmerge generic map ( e_random, e_random, false, 8, c_nof_repeat, 3, 29, 29, 0, 1);
  u_rnd_rnd_8_nof_3_reg   : entity work.tb_dp_packet_merge_unmerge generic map ( e_random, e_random,  true, 8, c_nof_repeat, 3, 29, 29, 0, 1);
  u_pls_act_8_nof_3       : entity work.tb_dp_packet_merge_unmerge generic map ( e_pulse,  e_active,  true, 8, c_nof_repeat, 3, 29, 29, 0, 1);
  u_pls_rnd_8_nof_3       : entity work.tb_dp_packet_merge_unmerge generic map ( e_pulse,  e_random,  true, 8, c_nof_repeat, 3, 29, 29, 0, 1);
  u_pls_pls_8_nof_3       : entity work.tb_dp_packet_merge_unmerge generic map ( e_pulse,  e_pulse,   true, 8, c_nof_repeat, 3, 29, 29, 0, 1);

  u_rnd_act_8_nof_1       : entity work.tb_dp_packet_merge_unmerge generic map ( e_random, e_active,  true, 8, c_nof_repeat, 1, 29, 29,  0, 1);
  u_rnd_act_8_nof_3_gap   : entity work.tb_dp_packet_merge_unmerge generic map ( e_random, e_active,  true, 8, c_nof_repeat, 3, 29, 29, 17, 1);
end tb;
