README.txt for $HDL_WORK/libraries/technology/ip_arria10/tse_sgmii_lvds

Contents:
1) Porting
2) IP component
3) Compilation, simulation and verification
4) Synthesis
5) Remarks


1) Porting

The tse_sgmii_lvds IP was ported manually from Quartus v9.1 for Stratix IV  to Quartus 14.0a10 for Arria10 by creating it in Qsys using
the same parameter settings.


2) IP component

The generated IP is not kept in SVN, only the Qsys source file:

  ip/ip_arria10_tse_sgmii_lvds.qsys

Therefore first the IP needs to be generated using:

  ip/generate_ip.sh
  

3) Compilation, simulation and verification

The genrated IP also contains a msim_setup.tcl file that was used to manually create:

  ip/compile_ip.tcl
  
This ip/compile_ip.tcl is in the hdllib.cfg and gets compiled before the other code.

The IP can be verified with the self checking test bench:

  tb_ip_arria10_tse_sgmii_lvds_top.vhd
  
This test bench is functionally identical to tb_ip_stratixiv_tse_sgmii_lvds.vhd, but instead it instantiates the ip_arria10_tse_sgmii_lvds.vhd
component as DUT.


4) Synthesis

No synthesis trials were done, because this will implicitely be done when the IP is used in a design. The QIP file:

  ip/generated/ip_arria10_tse_sgmii_lvds.qip

is included in the ip/hdllib.cfg and contains what is needed to synthesize the IP.


5) Remarks

a) Use generated IP specific library clause name

  The generated ip_arria10_<lib_name>.vhd uses an IP specific library name. Therefore the hdllib.cfg uses the IP
  specific library as library claus name to make it known:
  
    hdl_lib_name = ip_arria10_<lib_name>
    hdl_library_clause_name = ip_arria10_<lib_name>_<ip_specific>

  
b) Generated IP uses several more libraries

  The generated IP uses several more libraries. Just as the ip_arria10_tse_sgmii_lvds_altera_eth_tse_140 library
  these other libraries are all mapped to ./work in ip/compile_ip.tcl.
  