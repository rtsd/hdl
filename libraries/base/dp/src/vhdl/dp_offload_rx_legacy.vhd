-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity dp_offload_rx_legacy is
  generic (
    g_nof_streams             : natural := 1;
    g_data_w                  : natural;
    g_hdr_nof_words           : natural;
    g_remove_crc              : boolean;
    g_crc_nof_words           : natural
  );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    st_rst                   : in  std_logic;
    st_clk                   : in  std_logic;

    -- MM registers: hdr_remove
    ram_hdr_remove_mosi      : in  t_mem_mosi;
    ram_hdr_remove_miso      : out t_mem_miso;

    dp_sosi_arr              : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    dp_siso_arr              : in  t_dp_siso_arr(g_nof_streams - 1 downto 0);

    rx_sosi_arr              : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    rx_siso_arr              : out t_dp_siso_arr(g_nof_streams - 1 downto 0)
  );
end dp_offload_rx_legacy;

architecture str of dp_offload_rx_legacy is
  constant c_hdr_remove_ram_addr_w : natural := ceil_log2( g_hdr_nof_words * (g_data_w / c_word_w) );

  signal rx_pkt_sosi_arr         : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal rx_pkt_siso_arr         : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal ram_hdr_remove_mosi_arr : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal ram_hdr_remove_miso_arr : t_mem_miso_arr(g_nof_streams - 1 downto 0);
begin
  u_common_mem_mux_hdr_ram : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_hdr_remove_ram_addr_w
  )
  port map (
    mosi     => ram_hdr_remove_mosi,
    miso     => ram_hdr_remove_miso,
    mosi_arr => ram_hdr_remove_mosi_arr,
    miso_arr => ram_hdr_remove_miso_arr
  );

  gen_nof_streams: for i in 0 to g_nof_streams - 1 generate
    ---------------------------------------------------------------------------------------
    -- RX: Unframe: remove header (and CRC if phy link is used) from DP packets
    ---------------------------------------------------------------------------------------
    u_frame_remove : entity dp_lib.dp_frame_remove
    generic map (
      g_data_w         => g_data_w,
      g_symbol_w       => c_byte_w,
      g_hdr_nof_words  => g_hdr_nof_words,
      g_tail_nof_words => sel_a_b(g_remove_crc, g_crc_nof_words, 0)
    )
    port map (
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,

      st_rst      => st_rst,
      st_clk      => st_clk,

      snk_out     => rx_siso_arr(i),
      snk_in      => rx_sosi_arr(i),

      -- dp_frame_remove uses hdr_remove internally
      sla_in      => ram_hdr_remove_mosi_arr(i),
      sla_out     => ram_hdr_remove_miso_arr(i),

      src_in      => rx_pkt_siso_arr(i),
      src_out     => rx_pkt_sosi_arr(i)
    );

    ---------------------------------------------------------------------------------------
    -- RX: Convert DP packets to DP stream
    ---------------------------------------------------------------------------------------
    u_dp_packet_dec : entity dp_lib.dp_packet_dec
    generic map (
      g_data_w => g_data_w
    )
    port map (
      rst       => st_rst,
      clk       => st_clk,

      snk_out   => rx_pkt_siso_arr(i),
      snk_in    => rx_pkt_sosi_arr(i),

      src_in    => dp_siso_arr(i),
      src_out   => dp_sosi_arr(i)
    );
  end generate;
end str;
