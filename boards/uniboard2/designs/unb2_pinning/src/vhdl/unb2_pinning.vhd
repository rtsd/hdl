-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, unb_common_lib;
use unb_common_lib.unb_common_pkg.all;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity unb2_pinning is
  port (
    -- GENERAL
    CLK                    : in    std_logic;  -- External system clock
    PPS                    : in    std_logic;  -- External system sync
    WDI                    : out   std_logic;  -- Watchdog Clear
    INTA                   : inout std_logic;  -- FPGA interconnect line
    INTB                   : inout std_logic;  -- FPGA interconnect line

    -- 1GbE Control Interfaces
    ETH_CLK                : in std_logic;
    ETH_SGIN               : in std_logic_vector(1 downto 0);
    ETH_SGOUT              : out std_logic_vector(1 downto 0);

    -- Transceiver clocks
    SA_CLK                 : in    std_logic;  -- SerDes reference clock front
    SB_CLK                 : in    std_logic;  -- SerDes reference clock back
    BCK_REF_CLK            : in    std_logic;  -- SerDes reference clock back

    -- SO-DIMM DDR4 Memory Bank i2c (common)
    MB_SCL                 : inout std_logic;
    MB_SDA                 : inout std_logic;
    MB_EVENT               : in std_logic;
    -- SO-DIMM DDR4 Memory Bank I
    MB_I_RZQ       : in   std_logic;
    MB_I_REF_CLK   : in    std_logic;  -- External reference clock
    MB_I_A : out std_logic_vector(13 downto 0);
    MB_I_ACT_N : out std_logic_vector(0 downto 0);
    MB_I_BA : out std_logic_vector(1 downto 0);
    MB_I_BG : out std_logic_vector(1 downto 0);
    MB_I_CAS_A15 : out std_logic;
    MB_I_CB : inout std_logic_vector(7 downto 0);
    MB_I_CK : out std_logic_vector(1 downto 0);
    MB_I_CK_n : out std_logic_vector(1 downto 0);
    MB_I_CKE : out std_logic_vector(1 downto 0);
    MB_I_CS : out std_logic_vector(1 downto 0);
    MB_I_DM : inout std_logic_vector(8 downto 0);  -- !! temporarily chg to inout
    MB_I_DQ : inout std_logic_vector(63 downto 0);
    MB_I_DQS : inout std_logic_vector(8 downto 0);
    MB_I_DQS_n : inout std_logic_vector(8 downto 0);
    MB_I_ODT : out std_logic_vector(1 downto 0);
    MB_I_PARITY : out std_logic_vector(0 downto 0);
    MB_I_RAS_A16 : out std_logic;
    MB_I_WE_A14 : out std_logic;
    MB_I_RESET_N           : out std_logic_vector(0 downto 0);
    MB_I_ALERT_N           : in std_logic_vector(0 downto 0);
    -- SO-DIMM DDR4 Memory Bank II
    MB_II_RZQ    : in   std_logic;
    MB_II_REF_CLK : in    std_logic;  -- External reference clock
    MB_II_A : out std_logic_vector(13 downto 0);
    MB_II_ACT_N : out std_logic_vector(0 downto 0);
    MB_II_BA : out std_logic_vector(1 downto 0);
    MB_II_BG : out std_logic_vector(1 downto 0);
    MB_II_CAS_A15 : out std_logic;
    MB_II_CB : inout std_logic_vector(7 downto 0);
    MB_II_CK : out std_logic_vector(1 downto 0);
    MB_II_CK_n : out std_logic_vector(1 downto 0);
    MB_II_CKE : out std_logic_vector(1 downto 0);
    MB_II_CS : out std_logic_vector(1 downto 0);
    MB_II_DM : inout std_logic_vector(8 downto 0);  -- !! temporarily chg to inout
    MB_II_DQ : inout std_logic_vector(63 downto 0);
    MB_II_DQS : inout std_logic_vector(8 downto 0);
    MB_II_DQS_n : inout std_logic_vector(8 downto 0);
    MB_II_ODT : out std_logic_vector(1 downto 0);
    MB_II_PARITY : out std_logic_vector(0 downto 0);
    MB_II_RAS_A16 : out std_logic;
    MB_II_WE_A14 : out std_logic;
    MB_II_RESET_N          : out std_logic_vector(0 downto 0);
    MB_II_ALERT_N           : in std_logic_vector(0 downto 0);

    -- back transceivers
    BCK_RX : in std_logic_vector(47 downto 0);
    BCK_TX : out std_logic_vector(47 downto 0);
    BCK_SDA : inout std_logic_vector(2 downto 0);
    BCK_SCL : inout std_logic_vector(2 downto 0);
    BCK_ERR : inout std_logic_vector(2 downto 0);
    -- ring transceivers
    RING_0_RX : in std_logic_vector(11 downto 0);
    RING_0_TX : out std_logic_vector(11 downto 0);
    RING_1_RX : in std_logic_vector(11 downto 0);
    RING_1_TX : out std_logic_vector(11 downto 0);
    -- pmbus
    PMBUS_SC : inout std_logic;
    PMBUS_SD : inout std_logic;
    PMBUS_ALERT : in std_logic;
    -- front transceivers
    QSFP_0_RX : in std_logic_vector(3 downto 0);
    QSFP_0_TX : out std_logic_vector(3 downto 0);
    QSFP_1_RX : in std_logic_vector(3 downto 0);
    QSFP_1_TX : out std_logic_vector(3 downto 0);
    QSFP_2_RX : in std_logic_vector(3 downto 0);
    QSFP_2_TX : out std_logic_vector(3 downto 0);
    QSFP_3_RX : in std_logic_vector(3 downto 0);
    QSFP_3_TX : out std_logic_vector(3 downto 0);
    QSFP_4_RX : in std_logic_vector(3 downto 0);
    QSFP_4_TX : out std_logic_vector(3 downto 0);
    QSFP_5_RX : in std_logic_vector(3 downto 0);
    QSFP_5_TX : out std_logic_vector(3 downto 0);
    QSFP_SDA : inout std_logic_vector(5 downto 0);
    QSFP_SCL : inout std_logic_vector(5 downto 0);
    QSFP_RST : inout std_logic;
    -- I2C Interface to Sensors
    SENS_SC                : inout   std_logic;
    SENS_SD                : inout std_logic;
     -- Others
--    CFG_DATA               : inout std_logic_vector (3 downto 0);
    VERSION                : in    std_logic_vector(1 downto 0);
    ID                     : in    std_logic_vector(7 downto 0);
    TESTIO                 : inout std_logic_vector(5 downto 0);
    QSFP_LED               : inout std_logic_vector(11 downto 0)
  );
end unb2_pinning;

architecture str of unb2_pinning is
    component ddr4 is
      port (
        global_reset_n      : in    std_logic                      := 'X';  -- reset_n
        pll_ref_clk         : in    std_logic                      := 'X';  -- clk
        oct_rzqin           : in    std_logic                      := 'X';  -- oct_rzqin
        mem_ck              : out   std_logic_vector(1 downto 0);  -- mem_ck
        mem_ck_n            : out   std_logic_vector(1 downto 0);  -- mem_ck_n
        mem_a               : out   std_logic_vector(16 downto 0);  -- mem_a
        mem_act_n           : out   std_logic_vector(0 downto 0);  -- mem_act_n
        mem_ba              : out   std_logic_vector(1 downto 0);  -- mem_ba
        mem_bg              : out   std_logic_vector(1 downto 0);  -- mem_bg
        mem_cke             : out   std_logic_vector(1 downto 0);  -- mem_cke
        mem_cs_n            : out   std_logic_vector(1 downto 0);  -- mem_cs_n
        mem_odt             : out   std_logic_vector(1 downto 0);  -- mem_odt
        mem_reset_n         : out   std_logic_vector(0 downto 0);  -- mem_reset_n
        mem_alert_n         : in   std_logic_vector(0 downto 0);  -- mem_alert_n
        mem_par             : out   std_logic_vector(0 downto 0);  -- mem_par  ** new in 14.0 **
        mem_dqs             : inout std_logic_vector(8 downto 0)   := (others => 'X');  -- mem_dqs
        mem_dqs_n           : inout std_logic_vector(8 downto 0)   := (others => 'X');  -- mem_dqs_n
        mem_dq              : inout std_logic_vector(71 downto 0)  := (others => 'X');  -- mem_dq
        mem_dbi_n           : inout std_logic_vector(8 downto 0)   := (others => 'X');  -- mem_dbi_n
        local_cal_success   : out   std_logic;  -- local_cal_success
        local_cal_fail      : out   std_logic;  -- local_cal_fail
        emif_usr_reset_n    : out   std_logic;  -- reset_n
        emif_usr_clk        : out   std_logic;  -- clk
        amm_ready_0         : out   std_logic;  -- waitrequest_n
        amm_read_0          : in    std_logic                      := 'X';  -- read
        amm_write_0         : in    std_logic                      := 'X';  -- write
        amm_address_0       : in    std_logic_vector(26 downto 0)  := (others => 'X');  -- address ** chg from 23 bits in 14.0 **
        amm_readdata_0      : out   std_logic_vector(575 downto 0);  -- readdata
        amm_writedata_0     : in    std_logic_vector(575 downto 0) := (others => 'X');  -- writedata
        amm_burstcount_0    : in    std_logic_vector(6 downto 0)   := (others => 'X');  -- burstcount ** chg from 8 bits in 14.0 **
        amm_byteenable_0    : in    std_logic_vector(71 downto 0)  := (others => 'X');  -- byteenable
        amm_readdatavalid_0 : out   std_logic  -- readdatavalid
      );
    end component ddr4;

    component transceiver_phy is
      port (
        tx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- tx_analogreset
        tx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- tx_digitalreset
        rx_analogreset          : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- rx_analogreset
        rx_digitalreset         : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- rx_digitalreset
        tx_cal_busy             : out std_logic_vector(47 downto 0);  -- tx_cal_busy
        rx_cal_busy             : out std_logic_vector(47 downto 0);  -- rx_cal_busy
        rx_is_lockedtodata      : out  std_logic_vector(47 downto 0) := (others => 'X');  -- rx_is_lockedtodata
        tx_serial_clk0          : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- clk
        rx_cdr_refclk0          : in  std_logic                       := 'X';  -- clk
        tx_serial_data          : out std_logic_vector(47 downto 0);  -- tx_serial_data
        rx_serial_data          : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- rx_serial_data
        tx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- clk
        rx_coreclkin            : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- clk
        tx_clkout               : out std_logic_vector(47 downto 0);  -- clk
        rx_clkout               : out std_logic_vector(47 downto 0);  -- clk
        tx_enh_data_valid       : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- tx_enh_data_valid
        rx_enh_data_valid       : out std_logic_vector(47 downto 0);  -- rx_enh_data_valid
        rx_enh_blk_lock         : out std_logic_vector(47 downto 0);  -- rx_enh_blk_lock
        tx_parallel_data        : in  std_logic_vector(3071 downto 0) := (others => 'X');  -- tx_parallel_data
        tx_control              : in  std_logic_vector(383 downto 0)  := (others => 'X');  -- tx_control
        tx_err_ins              : in  std_logic_vector(47 downto 0)   := (others => 'X');  -- tx_err_ins
        unused_tx_parallel_data : in  std_logic_vector(3071 downto 0) := (others => 'X');  -- unused_tx_parallel_data
        unused_tx_control       : in  std_logic_vector(431 downto 0)  := (others => 'X');  -- unused_tx_control
        rx_parallel_data        : out std_logic_vector(3071 downto 0);  -- rx_parallel_data
        rx_control              : out std_logic_vector(383 downto 0);  -- rx_control
        unused_rx_parallel_data : out std_logic_vector(3071 downto 0);  -- unused_rx_parallel_data
        unused_rx_control       : out std_logic_vector(575 downto 0)  -- unused_rx_control
      );
    end component transceiver_phy;

    component transceiver_phy_24channel is
      port (
        tx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- tx_analogreset
        tx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- tx_digitalreset
        rx_analogreset          : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- rx_analogreset
        rx_digitalreset         : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- rx_digitalreset
        tx_cal_busy             : out std_logic_vector(23 downto 0);  -- tx_cal_busy
        rx_cal_busy             : out std_logic_vector(23 downto 0);  -- rx_cal_busy
        rx_is_lockedtodata      : out  std_logic_vector(23 downto 0) := (others => 'X');  -- rx_is_lockedtodata
        tx_serial_clk0          : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- clk
        rx_cdr_refclk0          : in  std_logic                       := 'X';  -- clk
        tx_serial_data          : out std_logic_vector(23 downto 0);  -- tx_serial_data
        rx_serial_data          : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- rx_serial_data
        tx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- clk
        rx_coreclkin            : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- clk
        tx_clkout               : out std_logic_vector(23 downto 0);  -- clk
        rx_clkout               : out std_logic_vector(23 downto 0);  -- clk
        tx_enh_data_valid       : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- tx_enh_data_valid
        rx_enh_data_valid       : out std_logic_vector(23 downto 0);  -- rx_enh_data_valid
        rx_enh_blk_lock         : out std_logic_vector(23 downto 0);  -- rx_enh_blk_lock
        tx_parallel_data        : in  std_logic_vector(1535 downto 0) := (others => 'X');  -- tx_parallel_data
        tx_control              : in  std_logic_vector(191 downto 0)  := (others => 'X');  -- tx_control
        tx_err_ins              : in  std_logic_vector(23 downto 0)   := (others => 'X');  -- tx_err_ins
        unused_tx_parallel_data : in  std_logic_vector(1535 downto 0) := (others => 'X');  -- unused_tx_parallel_data
        unused_tx_control       : in  std_logic_vector(215 downto 0)  := (others => 'X');  -- unused_tx_control
        rx_parallel_data        : out std_logic_vector(1535 downto 0);  -- rx_parallel_data
        rx_control              : out std_logic_vector(191 downto 0);  -- rx_control
        unused_rx_parallel_data : out std_logic_vector(1535 downto 0);  -- unused_rx_parallel_data
        unused_rx_control       : out std_logic_vector(287 downto 0)  -- unused_rx_control
      );
    end component transceiver_phy_24channel;

    component transceiver_reset_controller is
      port (
        clock              : in  std_logic                     := 'X';  -- clk
        reset              : in  std_logic                     := 'X';  -- reset
        pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown
        tx_analogreset     : out std_logic_vector(47 downto 0);  -- tx_analogreset
        tx_digitalreset    : out std_logic_vector(47 downto 0);  -- tx_digitalreset
        tx_ready           : out std_logic_vector(47 downto 0);  -- tx_ready
        pll_locked         : in  std_logic_vector(0 downto 0) := (others => 'X');  -- pll_locked
        pll_select         : in  std_logic_vector(0 downto 0)  := (others => 'X');  -- pll_select
        tx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => 'X');  -- tx_cal_busy
        rx_analogreset     : out std_logic_vector(47 downto 0);  -- rx_analogreset
        rx_digitalreset    : out std_logic_vector(47 downto 0);  -- rx_digitalreset
        rx_ready           : out std_logic_vector(47 downto 0);  -- rx_ready
        rx_is_lockedtodata : in  std_logic_vector(47 downto 0) := (others => 'X');  -- rx_is_lockedtodata
        rx_cal_busy        : in  std_logic_vector(47 downto 0) := (others => 'X')  -- rx_cal_busy
      );
    end component transceiver_reset_controller;

    component transceiver_reset_controller_24 is
      port (
        clock              : in  std_logic                     := 'X';  -- clk
        reset              : in  std_logic                     := 'X';  -- reset
        pll_powerdown      : out std_logic_vector(0 downto 0);  -- pll_powerdown
        tx_analogreset     : out std_logic_vector(23 downto 0);  -- tx_analogreset
        tx_digitalreset    : out std_logic_vector(23 downto 0);  -- tx_digitalreset
        tx_ready           : out std_logic_vector(23 downto 0);  -- tx_ready
        pll_locked         : in  std_logic_vector(0 downto 0) := (others => 'X');  -- pll_locked
        pll_select         : in  std_logic_vector(0 downto 0)  := (others => 'X');  -- pll_select
        tx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => 'X');  -- tx_cal_busy
        rx_analogreset     : out std_logic_vector(23 downto 0);  -- rx_analogreset
        rx_digitalreset    : out std_logic_vector(23 downto 0);  -- rx_digitalreset
        rx_ready           : out std_logic_vector(23 downto 0);  -- rx_ready
        rx_is_lockedtodata : in  std_logic_vector(23 downto 0) := (others => 'X');  -- rx_is_lockedtodata
        rx_cal_busy        : in  std_logic_vector(23 downto 0) := (others => 'X')  -- rx_cal_busy
      );
    end component transceiver_reset_controller_24;

    component transceiver_pll is
      port (
        pll_powerdown   : in  std_logic := 'X';  -- pll_powerdown
        pll_refclk0     : in  std_logic := 'X';  -- clk
        pll_locked      : out std_logic;  -- pll_locked
        pll_cal_busy    : out std_logic;  -- pll_cal_busy
        mcgb_rst        : in  std_logic := 'X';  -- mcgb_rst
        mcgb_serial_clk : out std_logic  -- clk
     );
   end component transceiver_pll;

    component sys_clkctrl is
      port (
        inclk  : in  std_logic := 'X';  -- inclk
        outclk : out std_logic  -- outclk
      );
    end component sys_clkctrl;

   component system_pll is
     port (
       refclk    : in  std_logic := 'X';  -- clk
       rst       : in  std_logic := 'X';
       locked    : out std_logic;
       outclk_0  : out std_logic;  -- outclk0
       outclk_1  : out std_logic;  -- outclk1
       outclk_2  : out std_logic  -- outclk2
     );
  end component system_pll;

   component system_fpll is
     port (
       pll_refclk0    : in  std_logic := 'X';  -- clk
       pll_powerdown  : in  std_logic := 'X';
       pll_locked     : out std_logic;
       pll_cal_busy   : out std_logic;
       outclk0        : out std_logic;  -- outclk0
       outclk1        : out std_logic;  -- outclk1
       outclk2        : out std_logic  -- outclk2
     );
  end component system_fpll;

  component unb2_pinning_qsys is
    port (
      clk_clk                          : in    std_logic := 'X';  -- clk
      reset_reset_n                    : in    std_logic := 'X';  -- reset_n
      avs_i2c_master_0_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_0_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_0_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_0_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_1_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_1_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_1_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_1_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_11_i2c_sda_export : inout std_logic := 'X';  -- export
      avs_i2c_master_11_i2c_scl_export : inout std_logic := 'X';  -- export
      avs_i2c_master_11_sync_export    : in    std_logic := 'X';  -- export
      avs_i2c_master_11_gs_sim_export  : in    std_logic := 'X';  -- export
      avs_i2c_master_10_i2c_sda_export : inout std_logic := 'X';  -- export
      avs_i2c_master_10_i2c_scl_export : inout std_logic := 'X';  -- export
      avs_i2c_master_10_sync_export    : in    std_logic := 'X';  -- export
      avs_i2c_master_10_gs_sim_export  : in    std_logic := 'X';  -- export
      avs_i2c_master_9_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_9_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_9_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_9_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_8_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_8_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_8_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_8_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_7_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_7_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_7_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_7_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_6_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_6_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_6_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_6_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_5_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_5_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_5_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_5_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_4_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_4_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_4_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_4_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_3_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_3_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_3_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_3_i2c_sda_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_2_gs_sim_export   : in    std_logic := 'X';  -- export
      avs_i2c_master_2_sync_export     : in    std_logic := 'X';  -- export
      avs_i2c_master_2_i2c_scl_export  : inout std_logic := 'X';  -- export
      avs_i2c_master_2_i2c_sda_export  : inout std_logic := 'X';  -- export
      eth_tse_0_serial_connection_rxp_0          : in    std_logic := 'X';  -- rxp
      eth_tse_0_serial_connection_txp_0          : out   std_logic;  -- txp
      eth_tse_0_pcs_ref_clk_clock_connection_clk : in    std_logic := 'X';  -- clk
      eth_tse_1_pcs_ref_clk_clock_connection_clk : in    std_logic := 'X';  -- clk
      eth_tse_1_serial_connection_rxp_0          : in    std_logic := 'X';  -- rxp
      eth_tse_1_serial_connection_txp_0          : out   std_logic;  -- txp
      pio_0_external_connection_export           : in    std_logic_vector(11 downto 0) := (others => 'X')  -- export

   );
  end component unb2_pinning_qsys;

    -- constants
    constant cs_sim                : std_logic := '0';
    constant cs_sync               : std_logic := '1';

    -- general reset and clock signals
    signal reset_n                 : std_logic;
    signal reset_p                 : std_logic;
    signal pout_wdi                : std_logic := '0';
    signal sys_clk                 : std_logic := '0';
    signal sys_locked              : std_logic := '0';
    signal mm_clk                  : std_logic := '0';
    signal clk_125                 : std_logic := '0';
    signal CLK_buffered            : std_logic := '0';

    -- signals for the ddr4 controllers
    signal local_i_cal_success     : std_logic;
    signal local_i_cal_fail        : std_logic;
    signal local_i_reset_n         : std_logic;
    signal local_i_clk             : std_logic;
    signal local_i_ready           : std_logic;
    signal local_i_read            : std_logic;
    signal local_i_write           : std_logic;
    signal local_i_address         : std_logic_vector(26 downto 0);
    signal local_i_readdata        : std_logic_vector(575 downto 0);
    signal local_i_writedata       : std_logic_vector(575 downto 0);
    signal local_i_burstcount      : std_logic_vector(6 downto 0);
    signal local_i_be              : std_logic_vector(71 downto 0);
    signal local_i_read_data_valid : std_logic;
    signal mb_i_a_internal         : std_logic_vector(16 downto 0);
    signal local_ii_cal_success    : std_logic;
    signal local_ii_cal_fail       : std_logic;
    signal local_ii_reset_n        : std_logic;
    signal local_ii_clk            : std_logic;
    signal local_ii_ready          : std_logic;
    signal local_ii_read           : std_logic;
    signal local_ii_write          : std_logic;
    signal local_ii_address        : std_logic_vector(26 downto 0);
    signal local_ii_readdata       : std_logic_vector(575 downto 0);
    signal local_ii_writedata      : std_logic_vector(575 downto 0);
    signal local_ii_burstcount     : std_logic_vector(6 downto 0);
    signal local_ii_be             : std_logic_vector(71 downto 0);
    signal local_ii_read_data_valid: std_logic;
    signal mb_ii_a_internal        : std_logic_vector(16 downto 0);

    -- signals for the transceivers
    signal tx_serial_data_front    : std_logic_vector(47 downto 0);
    signal rx_serial_data_front    : std_logic_vector(47 downto 0);
    signal dataloopback_front      : std_logic_vector(3071 downto 0);
    signal controlloopback_front   : std_logic_vector(383 downto 0);
    signal tx_serdesclk_front      : std_logic_vector(47 downto 0);
    signal validloopback_front     : std_logic_vector(47 downto 0);
    signal tx_analogreset_front    : std_logic_vector(47 downto 0);
    signal tx_digitalreset_front   : std_logic_vector(47 downto 0);
    signal rx_analogreset_front    : std_logic_vector(47 downto 0);
    signal rx_digitalreset_front   : std_logic_vector(47 downto 0);
    signal tx_cal_busy_front       : std_logic_vector(47 downto 0);
    signal rx_cal_busy_front       : std_logic_vector(47 downto 0);
    signal txpll_cal_busy_front    : std_logic_vector(47 downto 0);
    signal pll_cal_busy_front      : std_logic;
    signal rx_is_lockedtodata_front: std_logic_vector(47 downto 0);
    signal pll_powerdown_front     : std_logic_vector(0 downto 0);
    signal pll_locked_front        : std_logic_vector(0 downto 0);
    signal tx_serial_clk_front     : std_logic_vector(47 downto 0);
    signal mcgb_serial_clk_front   : std_logic;

    signal tx_serial_data_back    : std_logic_vector(47 downto 0);
    signal rx_serial_data_back    : std_logic_vector(47 downto 0);
    signal dataloopback_back      : std_logic_vector(3071 downto 0);
    signal controlloopback_back   : std_logic_vector(383 downto 0);
    signal dataloopback_test      : std_logic_vector(1535 downto 0);
    signal controlloopback_test   : std_logic_vector(191 downto 0);
    signal tx_serdesclk_back      : std_logic_vector(47 downto 0);
    signal validloopback_back     : std_logic_vector(47 downto 0);
    signal tx_analogreset_back    : std_logic_vector(47 downto 0);
    signal tx_digitalreset_back   : std_logic_vector(47 downto 0);
    signal rx_analogreset_back    : std_logic_vector(47 downto 0);
    signal rx_digitalreset_back   : std_logic_vector(47 downto 0);
    signal tx_cal_busy_back       : std_logic_vector(47 downto 0);
    signal rx_cal_busy_back       : std_logic_vector(47 downto 0);
    signal txpll_cal_busy_back    : std_logic_vector(47 downto 0);
    signal pll_cal_busy_back_upper      : std_logic;
    signal pll_cal_busy_back_lower      : std_logic;
    signal rx_is_lockedtodata_back: std_logic_vector(47 downto 0);
    signal pll_powerdown_back_upper     : std_logic_vector(0 downto 0);
    signal pll_powerdown_back_lower     : std_logic_vector(0 downto 0);
    signal pll_locked_back_upper        : std_logic_vector(0 downto 0);
    signal pll_locked_back_lower        : std_logic_vector(0 downto 0);
    signal tx_serial_clk_back     : std_logic_vector(47 downto 0);
    signal mcgb_serial_clk_back_upper   : std_logic;
    signal mcgb_serial_clk_back_lower   : std_logic;

    -- signals for the bidirectional and misc ios
    signal inta_in    : std_logic;
    signal intb_in    : std_logic;
    signal testio_in  : std_logic_vector(5 downto 0);
    signal qsfp_led_in  : std_logic_vector(11 downto 0);
    signal bck_err_in : std_logic_vector(2 downto 0);
    signal inta_out   : std_logic;
    signal intb_out   : std_logic;
    signal testio_out : std_logic_vector(5 downto 0);
    signal qsfp_led_out  : std_logic_vector(11 downto 0);
    signal bck_err_out : std_logic_vector(2 downto 0);
    signal ver_id_pmbusalert     : std_logic_vector(11 downto 0);
begin
    WDI <= 'Z';

    --  ****** DDR4 memory controllers ******

    mb_i_a <= mb_i_a_internal(13 downto 0);
    mb_i_we_a14 <= mb_i_a_internal(14);
    mb_i_cas_a15 <= mb_i_a_internal(15);
    mb_i_ras_a16 <= mb_i_a_internal(16);

    local_i_proc : process(local_i_clk, local_i_reset_n)
    begin
      if local_i_reset_n = '0' then
        local_i_read <= '0';
        local_i_write <= '0';
        local_i_address <= (others => '0');
        local_i_writedata <= (others => '0');
        local_i_burstcount <= (others => '0');
        local_i_be <= (others => '0');
      else
        if local_i_clk'event and local_i_clk = '1' then
          local_i_be <= (others => '1');
          if local_i_ready = '1' then
      local_i_read <= not local_i_read;
            local_i_write <= local_i_read_data_valid;
            local_i_address <= local_i_address + 1;
      if local_i_read_data_valid = '1' then
              local_i_writedata <= not local_i_readdata;
      else
        local_i_writedata <= (others => '1');
            end if;
          end if;
  end if;
      end if;
    end process;

    u_ddr4_i : ddr4
      port map (
        global_reset_n      => reset_n,
        pll_ref_clk         => MB_I_REF_CLK,
        oct_rzqin           => MB_I_RZQ,
        mem_ck              => mb_i_ck,
        mem_ck_n            => mb_i_ck_n,
        mem_a               => mb_i_a_internal,
        mem_act_n           => mb_i_act_n,
        mem_ba              => mb_i_ba,
        mem_bg              => mb_i_bg,
        mem_cke             => mb_i_cke,
        mem_cs_n            => mb_i_cs,
        mem_odt             => mb_i_odt,
        mem_reset_n         => mb_i_reset_n,
        mem_alert_n         => mb_i_alert_n,
        mem_par             => mb_i_parity,
        mem_dqs             => mb_i_dqs,
        mem_dqs_n           => mb_i_dqs_n,
        mem_dq(63 downto 0) => mb_i_dq,
        mem_dq(71 downto 64) => mb_i_cb,
        mem_dbi_n           => mb_i_dm,
        local_cal_success   => local_i_cal_success,
        local_cal_fail      => local_i_cal_fail,
        emif_usr_reset_n    => local_i_reset_n,
        emif_usr_clk        => local_i_clk,
        amm_ready_0         => local_i_ready,
        amm_read_0          => local_i_read,
        amm_write_0         => local_i_write,
        amm_address_0       => local_i_address,
        amm_readdata_0      => local_i_readdata,
        amm_writedata_0     => local_i_writedata,
        amm_burstcount_0    => local_i_burstcount,
        amm_byteenable_0    => local_i_be,
        amm_readdatavalid_0 => local_i_read_data_valid
     );

    mb_ii_a <= mb_ii_a_internal(13 downto 0);
    mb_ii_we_a14 <= mb_ii_a_internal(14);
    mb_ii_cas_a15 <= mb_ii_a_internal(15);
    mb_ii_ras_a16 <= mb_ii_a_internal(16);

    local_ii_proc : process(local_ii_clk, local_ii_reset_n)
    begin
      if local_ii_reset_n = '0' then
        local_ii_read <= '0';
        local_ii_write <= '0';
        local_ii_address <= (others => '0');
        local_ii_writedata <= (others => '0');
        local_ii_burstcount <= (others => '0');
        local_ii_be <= (others => '0');
      else
        if local_ii_clk'event and local_ii_clk = '1' then
          local_ii_be <= (others => '1');
          if local_ii_ready = '1' then
      local_ii_read <= not local_ii_read;
            local_ii_write <= local_ii_read_data_valid;
            local_ii_address <= local_ii_address + 1;
      if local_ii_read_data_valid = '1' then
              local_ii_writedata <= not local_ii_readdata;
      else
        local_ii_writedata <= (others => '1');
            end if;
          end if;
  end if;
      end if;
    end process;

    u_ddr4_ii : ddr4
      port map (
        global_reset_n      => reset_n,
        pll_ref_clk         => MB_II_REF_CLK,
        oct_rzqin           => MB_II_RZQ,
        mem_ck              => mb_ii_ck,
        mem_ck_n            => mb_ii_ck_n,
        mem_a               => mb_ii_a_internal,
        mem_act_n           => mb_ii_act_n,
        mem_ba              => mb_ii_ba,
        mem_bg              => mb_ii_bg,
        mem_cke             => mb_ii_cke,
        mem_cs_n            => mb_ii_cs,
        mem_odt             => mb_ii_odt,
        mem_reset_n         => mb_ii_reset_n,
        mem_alert_n         => mb_ii_alert_n,
        mem_par             => mb_ii_parity,
        mem_dqs             => mb_ii_dqs,
        mem_dqs_n           => mb_ii_dqs_n,
        mem_dq(63 downto 0) => mb_ii_dq,
        mem_dq(71 downto 64) => mb_ii_cb,
        mem_dbi_n           => mb_ii_dm,
        local_cal_success   => local_ii_cal_success,
        local_cal_fail      => local_ii_cal_fail,
        emif_usr_reset_n    => local_ii_reset_n,
        emif_usr_clk        => local_ii_clk,
        amm_ready_0         => local_ii_ready,
        amm_read_0          => local_ii_read,
        amm_write_0         => local_ii_write,
        amm_address_0       => local_ii_address,
        amm_readdata_0      => local_ii_readdata,
        amm_writedata_0     => local_ii_writedata,
        amm_burstcount_0    => local_ii_burstcount,
        amm_byteenable_0    => local_ii_be,
        amm_readdatavalid_0 => local_ii_read_data_valid
     );

--    -- ****** Front side transceivers ******
--
    RING_0_TX <= tx_serial_data_front(47 downto 36);
    QSFP_0_TX  <= tx_serial_data_front(35 downto 32);
    QSFP_1_TX  <= tx_serial_data_front(31 downto 28);
    QSFP_2_TX  <= tx_serial_data_front(27 downto 24);
    QSFP_3_TX  <= tx_serial_data_front(23 downto 20);
    QSFP_4_TX  <= tx_serial_data_front(19 downto 16);
    QSFP_5_TX  <= tx_serial_data_front(15 downto 12);
    RING_1_TX <= tx_serial_data_front(11 downto 0);

    rx_serial_data_front <= RING_0_RX
                          & QSFP_0_RX & QSFP_1_RX & QSFP_2_RX & QSFP_3_RX & QSFP_4_RX & QSFP_5_RX
                          & RING_1_RX;

   transceiver_phy_front : transceiver_phy
      port map (
        tx_analogreset          => tx_analogreset_front,
        tx_digitalreset         => tx_digitalreset_front,
        rx_analogreset          => rx_analogreset_front,
        rx_digitalreset         => rx_digitalreset_front,
        tx_cal_busy             => tx_cal_busy_front,
        rx_cal_busy             => rx_cal_busy_front,
        rx_is_lockedtodata      => rx_is_lockedtodata_front,
        tx_serial_clk0          => tx_serial_clk_front,
        rx_cdr_refclk0          => sa_clk,
        tx_serial_data          => tx_serial_data_front,
        rx_serial_data          => rx_serial_data_front,
  tx_coreclkin            => tx_serdesclk_front,  -- write side clock for tx fifo
        rx_coreclkin            => tx_serdesclk_front,
        tx_clkout               => tx_serdesclk_front,
        rx_clkout               => open,
        tx_enh_data_valid       => validloopback_front,
        rx_enh_data_valid       => validloopback_front,
        rx_enh_blk_lock         => open,
        tx_parallel_data        => dataloopback_front,
        tx_control              => controlloopback_front,
        tx_err_ins              => (others => '0'),  -- use to insert sync errors
        unused_tx_parallel_data => (others => '0'),
        unused_tx_control       => (others => '0'),
        rx_parallel_data        => dataloopback_front,
        rx_control              => controlloopback_front,
        unused_rx_parallel_data => open,
        unused_rx_control       => open
      );

    transceiver_reset_front : transceiver_reset_controller
      port map (
        clock                   => clk,
        reset                   => reset_p,
        pll_powerdown           => pll_powerdown_front,
        tx_analogreset          => tx_analogreset_front,
        tx_digitalreset         => tx_digitalreset_front,
        tx_ready                => open,
        pll_locked              => pll_locked_front,
        pll_select              => "0",
        tx_cal_busy             => txpll_cal_busy_front,
        rx_analogreset          => rx_analogreset_front,
        rx_digitalreset         => rx_digitalreset_front,
        rx_ready                => open,
        rx_is_lockedtodata      => rx_is_lockedtodata_front,
        rx_cal_busy             => rx_cal_busy_front
      );

    transceiver_pll_front : transceiver_pll
      port map (
        pll_powerdown           => pll_powerdown_front(0),
        pll_refclk0             => sa_clk,
        pll_locked              => pll_locked_front(0),
        pll_cal_busy            => pll_cal_busy_front,
        mcgb_rst                => pll_powerdown_front(0),
        mcgb_serial_clk         => mcgb_serial_clk_front
      );

    tx_serial_clk_front <= (others => mcgb_serial_clk_front);
    txpll_cal_busy_front <= tx_cal_busy_front when pll_cal_busy_front = '0' else (others => '1');

    -- ****** Back side transceivers ******
    -- upper 24 transceivers use sb_clk
    -- Nov 4 - temporarily disconnect BCK_TX/RX(47) to see what gets synthesised away

    BCK_TX(47 downto 0) <= tx_serial_data_back(47 downto 0);
--    BCK_TX(47) <= '0';

    rx_serial_data_back(47 downto 0) <= BCK_RX(47 downto 0);
--    dataloopback_test <= X"0000000000000000" & dataloopback_back(3007 downto 1536);
--    controlloopback_test <= X"00" & controlloopback_back(375 downto 192);

   transceiver_phy_back_upper : transceiver_phy_24channel
      port map (
        tx_analogreset          => tx_analogreset_back(47 downto 24),
        tx_digitalreset         => tx_digitalreset_back(47 downto 24),
        rx_analogreset          => rx_analogreset_back(47 downto 24),
        rx_digitalreset         => rx_digitalreset_back(47 downto 24),
        tx_cal_busy             => tx_cal_busy_back(47 downto 24),
        rx_cal_busy             => rx_cal_busy_back(47 downto 24),
        rx_is_lockedtodata      => rx_is_lockedtodata_back(47 downto 24),
        tx_serial_clk0          => tx_serial_clk_back(47 downto 24),
        rx_cdr_refclk0          => sb_clk,
        tx_serial_data          => tx_serial_data_back(47 downto 24),
        rx_serial_data          => rx_serial_data_back(47 downto 24),
  tx_coreclkin            => tx_serdesclk_back(47 downto 24),  -- write side clock for tx fifo
        rx_coreclkin            => tx_serdesclk_back(47 downto 24),
        tx_clkout               => tx_serdesclk_back(47 downto 24),
        rx_clkout               => open,
        tx_enh_data_valid       => validloopback_back(47 downto 24),
        rx_enh_data_valid       => validloopback_back(47 downto 24),
        rx_enh_blk_lock         => open,
        tx_parallel_data        => dataloopback_back(3071 downto 1536),
        tx_control              => controlloopback_back(383 downto 192),
        tx_err_ins              => (others => '0'),  -- use to insert sync errors
        unused_tx_parallel_data => (others => '0'),
        unused_tx_control       => (others => '0'),
        rx_parallel_data        => dataloopback_back(3071 downto 1536),
        rx_control              => controlloopback_back(383 downto 192),
        unused_rx_parallel_data => open,
        unused_rx_control       => open
      );

    transceiver_reset_back_upper : transceiver_reset_controller_24
      port map (
        clock                   => clk,
        reset                   => reset_p,
        pll_powerdown           => pll_powerdown_back_upper,
        tx_analogreset          => tx_analogreset_back(47 downto 24),
        tx_digitalreset         => tx_digitalreset_back(47 downto 24),
        tx_ready                => open,
        pll_locked              => pll_locked_back_upper,
        pll_select              => "0",
        tx_cal_busy             => txpll_cal_busy_back(47 downto 24),
        rx_analogreset          => rx_analogreset_back(47 downto 24),
        rx_digitalreset         => rx_digitalreset_back(47 downto 24),
        rx_ready                => open,
        rx_is_lockedtodata      => rx_is_lockedtodata_back(47 downto 24),
        rx_cal_busy             => rx_cal_busy_back(47 downto 24)
      );

    transceiver_pll_back_upper : transceiver_pll
      port map (
        pll_powerdown           => pll_powerdown_back_upper(0),
        pll_refclk0             => sb_clk,
        pll_locked              => pll_locked_back_upper(0),
        pll_cal_busy            => pll_cal_busy_back_upper,
        mcgb_rst                => pll_powerdown_back_upper(0),
        mcgb_serial_clk         => mcgb_serial_clk_back_upper
      );

    tx_serial_clk_back(47 downto 24) <= (others => mcgb_serial_clk_back_upper);
    txpll_cal_busy_back(47 downto 24) <= tx_cal_busy_back(47 downto 24) when pll_cal_busy_back_upper = '0' else (others => '1');

    -- lower 24 transceivers use sb_clk

   transceiver_phy_back_lower : transceiver_phy_24channel
      port map (
        tx_analogreset          => tx_analogreset_back(23 downto 0),
        tx_digitalreset         => tx_digitalreset_back(23 downto 0),
        rx_analogreset          => rx_analogreset_back(23 downto 0),
        rx_digitalreset         => rx_digitalreset_back(23 downto 0),
        tx_cal_busy             => tx_cal_busy_back(23 downto 0),
        rx_cal_busy             => rx_cal_busy_back(23 downto 0),
        rx_is_lockedtodata      => rx_is_lockedtodata_back(23 downto 0),
        tx_serial_clk0          => tx_serial_clk_back(23 downto 0),
        rx_cdr_refclk0          => bck_ref_clk,
        tx_serial_data          => tx_serial_data_back(23 downto 0),
        rx_serial_data          => rx_serial_data_back(23 downto 0),
  tx_coreclkin            => tx_serdesclk_back(23 downto 0),  -- write side clock for tx fifo
        rx_coreclkin            => tx_serdesclk_back(23 downto 0),
        tx_clkout               => tx_serdesclk_back(23 downto 0),
        rx_clkout               => open,
        tx_enh_data_valid       => validloopback_back(23 downto 0),
        rx_enh_data_valid       => validloopback_back(23 downto 0),
        rx_enh_blk_lock         => open,
        tx_parallel_data        => dataloopback_back(1535 downto 0),
        tx_control              => controlloopback_back(191 downto 0),
        tx_err_ins              => (others => '0'),  -- use to insert sync errors
        unused_tx_parallel_data => (others => '0'),
        unused_tx_control       => (others => '0'),
        rx_parallel_data        => dataloopback_back(1535 downto 0),
        rx_control              => controlloopback_back(191 downto 0),
        unused_rx_parallel_data => open,
        unused_rx_control       => open
      );

    transceiver_reset_back_lower : transceiver_reset_controller_24
      port map (
        clock                   => clk,
        reset                   => reset_p,
        pll_powerdown           => pll_powerdown_back_lower,
        tx_analogreset          => tx_analogreset_back(23 downto 0),
        tx_digitalreset         => tx_digitalreset_back(23 downto 0),
        tx_ready                => open,
        pll_locked              => pll_locked_back_lower,
        pll_select              => "0",
        tx_cal_busy             => txpll_cal_busy_back(23 downto 0),
        rx_analogreset          => rx_analogreset_back(23 downto 0),
        rx_digitalreset         => rx_digitalreset_back(23 downto 0),
        rx_ready                => open,
        rx_is_lockedtodata      => rx_is_lockedtodata_back(23 downto 0),
        rx_cal_busy             => rx_cal_busy_back(23 downto 0)
      );

    transceiver_pll_back_lower : transceiver_pll
      port map (
        pll_powerdown           => pll_powerdown_back_lower(0),
        pll_refclk0             => bck_ref_clk,
        pll_locked              => pll_locked_back_lower(0),
        pll_cal_busy            => pll_cal_busy_back_lower,
        mcgb_rst                => pll_powerdown_back_lower(0),
        mcgb_serial_clk         => mcgb_serial_clk_back_lower
      );

    tx_serial_clk_back(23 downto 0) <= (others => mcgb_serial_clk_back_lower);
    txpll_cal_busy_back(23 downto 0) <= tx_cal_busy_back(23 downto 0) when pll_cal_busy_back_lower = '0' else (others => '1');

    -- ****** node control for resets and wdi

    u_node_ctrl : entity unb_common_lib.unb_node_ctrl
      generic map (
        g_pulse_us => c_unb_tse_clk_freq / (10**6)  -- nof system clock cycles to get us period, equal to system clock frequency / 10**6
      )
      port map (
        xo_clk      => ETH_clk,
        xo_rst_n    => reset_n,
        sys_clk     => sys_clk,
        sys_locked  => sys_locked,
        sys_rst     => open,
        st_clk      => clk,
        st_rst      => open,
        wdi_in      => pout_wdi,
        wdi_out     => WDI,  -- overrule default WDI = 'Z' and let SW toggle WDI via pout_wdi to enable the watchdog
        pulse_us    => open,
        pulse_ms    => open,
        pulse_s     => open  -- could be used to toggle a LED
      );

    reset_p <= not reset_n;

    u0 : component sys_clkctrl
      port map (
        inclk  => CLK,  -- altclkctrl_input.inclk
        outclk => CLK_buffered  -- altclkctrl_output.outclk
    );

    u_system_pll : system_pll
      port map(
--        refclk       => ETH_CLK,
        refclk       => CLK_buffered,
--        refclk       => INTB,
        rst          => reset_p,
        locked       => sys_locked,
        outclk_0     => mm_clk,  -- 100MHz
        outclk_1     => sys_clk,  -- 300MHz
        outclk_2     => clk_125  -- 125MHz for 1ge
     );

--    u_system_pll : system_fpll
--      port map(
--        pll_refclk0       => INTB,
--        pll_powerdown     => reset_p,
--        pll_locked        => sys_locked,
--        pll_cal_busy      => open,
--        outclk0           => mm_clk,  -- 100MHz
--        outclk1           => sys_clk, -- 300MHz
--        outclk2           => clk_125  -- 125MHz for 1ge
--     );

    -- ****** i2c interfaces ******

    u_qsys : unb2_pinning_qsys
      port map (
        clk_clk                          => mm_clk,
        reset_reset_n                    => reset_n,
        avs_i2c_master_0_gs_sim_export   => cs_sim,
        avs_i2c_master_0_sync_export     => cs_sync,
        avs_i2c_master_0_i2c_scl_export  => sens_sc,
        avs_i2c_master_0_i2c_sda_export  => sens_sd,
        avs_i2c_master_1_gs_sim_export   => cs_sim,
        avs_i2c_master_1_sync_export     => cs_sync,
        avs_i2c_master_1_i2c_scl_export  => pmbus_sc,
        avs_i2c_master_1_i2c_sda_export  => pmbus_sd,
        avs_i2c_master_2_gs_sim_export   => cs_sim,
        avs_i2c_master_2_sync_export     => cs_sync,
        avs_i2c_master_2_i2c_scl_export  => bck_scl(0),
        avs_i2c_master_2_i2c_sda_export  => bck_sda(0),
        avs_i2c_master_3_gs_sim_export   => cs_sim,
        avs_i2c_master_3_sync_export     => cs_sync,
        avs_i2c_master_3_i2c_scl_export  => bck_scl(1),
        avs_i2c_master_3_i2c_sda_export  => bck_sda(1),
        avs_i2c_master_4_sync_export     => cs_sync,
        avs_i2c_master_4_gs_sim_export   => cs_sim,
        avs_i2c_master_4_i2c_scl_export  => bck_scl(2),
        avs_i2c_master_4_i2c_sda_export  => bck_sda(2),
        avs_i2c_master_5_sync_export     => cs_sync,
        avs_i2c_master_5_gs_sim_export   => cs_sim,
        avs_i2c_master_5_i2c_sda_export  => qsfp_sda(0),
        avs_i2c_master_5_i2c_scl_export  => qsfp_scl(0),
        avs_i2c_master_6_sync_export     => cs_sync,
        avs_i2c_master_6_gs_sim_export   => cs_sim,
        avs_i2c_master_6_i2c_sda_export  => qsfp_sda(1),
        avs_i2c_master_6_i2c_scl_export  => qsfp_scl(1),
        avs_i2c_master_7_sync_export     => cs_sync,
        avs_i2c_master_7_gs_sim_export   => cs_sim,
        avs_i2c_master_7_i2c_sda_export  => qsfp_sda(2),
        avs_i2c_master_7_i2c_scl_export  => qsfp_scl(2),
        avs_i2c_master_8_sync_export     => cs_sync,
        avs_i2c_master_8_gs_sim_export   => cs_sim,
        avs_i2c_master_8_i2c_sda_export  => qsfp_sda(3),
        avs_i2c_master_8_i2c_scl_export  => qsfp_scl(3),
        avs_i2c_master_9_sync_export     => cs_sync,
        avs_i2c_master_9_gs_sim_export   => cs_sim,
        avs_i2c_master_9_i2c_sda_export  => qsfp_sda(4),
        avs_i2c_master_9_i2c_scl_export  => qsfp_scl(4),
        avs_i2c_master_10_sync_export    => cs_sync,
        avs_i2c_master_10_gs_sim_export  => cs_sim,
        avs_i2c_master_10_i2c_sda_export => qsfp_sda(5),
        avs_i2c_master_10_i2c_scl_export => qsfp_scl(5),
        avs_i2c_master_11_sync_export    => cs_sync,
        avs_i2c_master_11_gs_sim_export  => cs_sim,
        avs_i2c_master_11_i2c_sda_export => mb_sda,
        avs_i2c_master_11_i2c_scl_export => mb_scl,
        eth_tse_0_serial_connection_rxp_0          => ETH_SGIN(0),
        eth_tse_0_serial_connection_txp_0          => ETH_SGOUT(0),
        --eth_tse_0_pcs_ref_clk_clock_connection_clk => clk_125,
        eth_tse_0_pcs_ref_clk_clock_connection_clk => ETH_CLK,
        --eth_tse_1_pcs_ref_clk_clock_connection_clk => clk_125,
        eth_tse_1_pcs_ref_clk_clock_connection_clk => ETH_CLK,
        eth_tse_1_serial_connection_rxp_0          => ETH_SGIN(1),
        eth_tse_1_serial_connection_txp_0          => ETH_SGOUT(1),
        pio_0_external_connection_export           => ver_id_pmbusalert
     );

-- bidirectional and misc
-- use PPS as output enable

    INTA <= inta_out when PPS = '1' else 'Z';
    INTB <= intb_out when PPS = '1' else 'Z';
    TESTIO(5 downto 0) <= testio_out(5 downto 0) when PPS = '1' else "ZZZZZZ";
    QSFP_LED <= qsfp_led_out when PPS = '1' else "ZZZZZZZZZZZZ";
    BCK_ERR <= bck_err_out when PPS = '1' else "ZZZ";

    inta_in <= INTA;
    intb_in <= INTB;
    testio_in(5 downto 0) <= TESTIO(5 downto 0);
    qsfp_led_in <= QSFP_LED;
    bck_err_in <= BCK_ERR;

    inta_out <= intb_in;
    intb_out <= inta_in;
    testio_out(5 downto 3) <= testio_in(2 downto 0);
    testio_out(2 downto 0) <= testio_in(5 downto 3);
    qsfp_led_out(11 downto 6) <= qsfp_led_in(5 downto 0);
    qsfp_led_out(5 downto 0) <= qsfp_led_in(11 downto 6);
    bck_err_out(2) <= bck_err_in(1);
    bck_err_out(1) <= bck_err_in(0);
    bck_err_out(0) <= bck_err_in(2);

    ver_id_pmbusalert <= version & id & pmbus_alert & mb_event;
end str;
