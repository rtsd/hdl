-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, common_mult_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.util_heater_pkg.all;

-- Purpose:
--   Use multiplier DSP elements, RAM and logic in an FPGA to heat up the PFGA
--   to see how the FPGA behaves when it gets warm. This is useful to verify
--   whether the FPGA remains functional, especially its various external IO
--   interfaces (e.g. gigabit transceivers and DDR memory).
-- Desription:
--   The heater elements can be enabled or disabled via an MM register.
--   Each heater element consists of a MAC4. A MAC4 uses 4 18x18 multipliers.
--   The MAC4 inputs are random so: mac4 = r0r1 + r1r2 + r2r3+ r3r0. For
--   g_pipeline > 0 the output data gets pipelined to use more logic. For
--   g_nof_ram > the output data is put through a FIFO to use more RAM. The
--   final output data gets XOR-ed to get a single bit value that can be read
--   via the same MM register. The read value is not relevant, but the read
--   access connection ensures that the heater element logic will not be
--   optimized away by synthesis.
--   The number of heater elements can be set via g_nof_mac4. The MM register
--   allows enabling 0, 1, more or all MAC4 under SW control. In this way it
--   is possible to vary the power consumption during run time.
--   Using common_pipeline.vhd to invoke logic can get implemented in RAM
--   blocks for larger pipeline settings. Therefor g_nof_logic to instantiate
--   util_logic stages to enforce using logic (LUTs and FF).

entity util_heater is
  generic (
    g_technology: natural := c_tech_select_default;
    g_nof_mac4  : natural := 1;  -- >= 1, number of multiply 18x18 and accumulate 4 elements in the heater <= c_util_heater_nof_mac4_max
    g_pipeline  : natural := 1;  -- >= 0, number of pipelining register stages after the multiplier
    g_nof_ram   : natural := 1;  -- >= 0, number of 1k Byte RAM blocks in the FIFO per multiplier
    g_nof_logic : natural := 1  -- >= 0, number of XOR register stages after the FIFO
  );
  port (
    mm_rst  : in  std_logic;  -- MM is the microprocessor control clock domain
    mm_clk  : in  std_logic;
    st_rst  : in  std_logic;  -- ST is the DSP clock domain
    st_clk  : in  std_logic;
    -- Memory Mapped Slave
    sla_in  : in  t_mem_mosi;
    sla_out : out t_mem_miso
  );
end;

architecture rtl of util_heater is
  -- Use MM bus data width = c_word_w = 32
  constant c_mm_reg  : t_c_mem := (latency  => 1,
                                   adr_w    => c_util_heater_reg_addr_w,
                                   dat_w    => c_word_w,
                                   nof_dat  => c_util_heater_reg_nof_words,
                                   init_sl  => '0');

  constant c_remote_len     : natural := 5;  -- >> 0 to ease timing to reach logic throughout the whole chip
  constant c_sync_delay_len : natural := c_meta_delay_len + c_remote_len;  -- >= c_meta_delay_len=3

  -- Use a MAC with 4 multipliers as basic heater element to be able to use all 18x18 in a Stratix4 DSP block
  constant c_mac4                 : natural := 4;  -- 4 multipliers per mac4
  constant c_mac_pipeline_input   : natural := 1;
  constant c_mac_pipeline_product : natural := 0;
  constant c_mac_pipeline_adder   : natural := 1;
  constant c_mac_pipeline_output  : natural := 1;

  -- Random input generators
  constant c_in_dat_w       : natural := 18;  -- fixed multiplier input data width
  constant c_prsg_0_w       : natural := c_in_dat_w;  -- generate sufficiently large random range
  constant c_prsg_1_w       : natural := c_in_dat_w + 1;  -- generate different range
  constant c_prsg_2_w       : natural := c_in_dat_w + 2;  -- generate different range
  constant c_prsg_3_w       : natural := c_in_dat_w + 3;  -- generate different range

  constant c_mac_in_dat_w   : natural := c_mac4 * c_in_dat_w;  -- aggregate mac4 input data width
  constant c_mult_dat_w     : natural := c_in_dat_w + c_in_dat_w;  -- = 36
  constant c_mac_out_dat_w  : natural := c_mult_dat_w + 2;  -- + 2 = ceil_log2(c_mac4)
  constant c_fifo_dat_w     : natural := c_mult_dat_w;  -- = 36

  constant c_nof_fifo_dat_in_1kbyte_ram : natural := (1024 * 9) / c_fifo_dat_w;  -- = a byte has 8 + 1 = 9 bits

  type t_prsg_0_arr      is array (integer range <>) of std_logic_vector(c_prsg_0_w - 1 downto 0);
  type t_prsg_1_arr      is array (integer range <>) of std_logic_vector(c_prsg_1_w - 1 downto 0);
  type t_prsg_2_arr      is array (integer range <>) of std_logic_vector(c_prsg_2_w - 1 downto 0);
  type t_prsg_3_arr      is array (integer range <>) of std_logic_vector(c_prsg_3_w - 1 downto 0);

  type t_mac_in_dat_arr  is array (integer range <>) of std_logic_vector(c_mac_in_dat_w - 1 downto 0);
  type t_mac_out_dat_arr is array (integer range <>) of std_logic_vector(c_mac_out_dat_w - 1 downto 0);

  type t_fifo_dat_arr    is array (integer range <>) of std_logic_vector(c_fifo_dat_w - 1 downto 0);

  -- Available bits
  signal mm_reg_en          : std_logic_vector(c_util_heater_nof_mac4_max - 1 downto 0);
  signal mm_reg_xor         : std_logic_vector(c_util_heater_nof_mac4_max - 1 downto 0);

  -- Used bits
  signal mm_element_en      : std_logic_vector(g_nof_mac4 - 1 downto 0);
  signal mm_element_xor     : std_logic_vector(g_nof_mac4 - 1 downto 0);
  signal st_element_en      : std_logic_vector(g_nof_mac4 - 1 downto 0);
  signal st_element_xor     : std_logic_vector(g_nof_mac4 - 1 downto 0);
  signal nxt_st_element_xor : std_logic_vector(g_nof_mac4 - 1 downto 0);

  signal st_clken           : std_logic_vector(g_nof_mac4 - 1 downto 0);

  signal prsg_0_reg         : std_logic_vector(c_prsg_0_w - 1 downto 0);
  signal prsg_0             : std_logic_vector(c_prsg_0_w - 1 downto 0);
  signal nxt_prsg_0         : std_logic_vector(c_prsg_0_w - 1 downto 0);
  signal prsg_1_reg         : std_logic_vector(c_prsg_1_w - 1 downto 0);
  signal prsg_1             : std_logic_vector(c_prsg_1_w - 1 downto 0);
  signal nxt_prsg_1         : std_logic_vector(c_prsg_1_w - 1 downto 0);
  signal prsg_2_reg         : std_logic_vector(c_prsg_2_w - 1 downto 0);
  signal prsg_2             : std_logic_vector(c_prsg_2_w - 1 downto 0);
  signal nxt_prsg_2         : std_logic_vector(c_prsg_2_w - 1 downto 0);
  signal prsg_3_reg         : std_logic_vector(c_prsg_3_w - 1 downto 0);
  signal prsg_3             : std_logic_vector(c_prsg_3_w - 1 downto 0);
  signal nxt_prsg_3         : std_logic_vector(c_prsg_3_w - 1 downto 0);

  signal in_dat_a           : t_mac_in_dat_arr( 0 to g_nof_mac4 - 1);
  signal in_dat_b           : t_mac_in_dat_arr( 0 to g_nof_mac4 - 1);
  signal mac4               : t_mac_out_dat_arr(0 to g_nof_mac4 - 1);
  signal fifo_in_dat        : t_fifo_dat_arr(   0 to g_nof_mac4 - 1);
  signal fifo_out_dat       : t_fifo_dat_arr(   0 to g_nof_mac4 - 1);
  signal logic_dat          : t_fifo_dat_arr(   0 to g_nof_mac4 - 1);
begin
  ------------------------------------------------------------------------------
  -- MM clock domain
  ------------------------------------------------------------------------------
  u_mm_reg : entity common_lib.common_reg_r_w
  generic map (
    g_reg       => c_mm_reg,
    g_init_reg  => (others => '0')
  )
  port map (
    rst       => mm_rst,
    clk       => mm_clk,
    -- control side
    wr_en     => sla_in.wr,
    wr_adr    => sla_in.address(c_mm_reg.adr_w - 1 downto 0),
    wr_dat    => sla_in.wrdata(c_mm_reg.dat_w - 1 downto 0),
    rd_en     => sla_in.rd,
    rd_adr    => sla_in.address(c_mm_reg.adr_w - 1 downto 0),
    rd_dat    => sla_out.rddata(c_mm_reg.dat_w - 1 downto 0),
    rd_val    => OPEN,
    -- data side
    out_reg   => mm_reg_en,
    in_reg    => mm_reg_xor
  );

  mm_element_en                    <= mm_reg_en(mm_element_en'range);
  mm_reg_xor(mm_element_xor'range) <= mm_element_xor;

  ------------------------------------------------------------------------------
  -- Clock domain crossing
  ------------------------------------------------------------------------------
  gen_cross : for I in 0 to g_nof_mac4 - 1 generate
    -- The MM reg bits are use individually (i.e. not as numbers) therefore it is allowed
    -- to synchronize them using a series of flip flops per bit. If the bits would be
    -- interpreted as numbers then common_reg_cross_domain would have to be used, to ensure
    -- that all bits that make the number are transfered synchronously.

    -- MM --> ST
    u_cross_en : entity common_lib.common_async
    generic map (
      g_rst_level => '0',
      g_delay_len => c_sync_delay_len
    )
    port map (
      rst  => st_rst,
      clk  => st_clk,
      din  => mm_element_en(I),
      dout => st_element_en(I)
    );

    -- MM <-- ST
    u_cross_xor : entity common_lib.common_async
    generic map (
      g_rst_level => '0',
      g_delay_len => c_sync_delay_len
    )
    port map (
      rst  => mm_rst,
      clk  => mm_clk,
      din  => st_element_xor(I),
      dout => mm_element_xor(I)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- ST clock domain
  ------------------------------------------------------------------------------

  -- Heater element enable/disable
  st_clken <= st_element_en(g_nof_mac4 - 1 downto 0);

  -- Shared PSRG source
  p_st_clk : process (st_rst, st_clk)
  begin
    if st_rst = '1' then
      prsg_0         <= (others => '0');
      prsg_1         <= (others => '0');
      prsg_2         <= (others => '0');
      prsg_3         <= (others => '0');
      st_element_xor <= (others => '0');
    elsif rising_edge(st_clk) then
      prsg_0         <= nxt_prsg_0;
      prsg_1         <= nxt_prsg_1;
      prsg_2         <= nxt_prsg_2;
      prsg_3         <= nxt_prsg_3;
      st_element_xor <= nxt_st_element_xor;
    end if;
  end process;

  nxt_prsg_0 <= func_common_random(prsg_0);
  nxt_prsg_1 <= func_common_random(prsg_1);
  nxt_prsg_2 <= func_common_random(prsg_2);
  nxt_prsg_3 <= func_common_random(prsg_3);

  u_prsg_0_reg : entity common_lib.common_pipeline
  generic map (
    g_pipeline    => c_remote_len,
    g_in_dat_w    => prsg_0'LENGTH,
    g_out_dat_w   => prsg_0'length
  )
  port map (
    rst     => st_rst,
    clk     => st_clk,
    in_dat  => prsg_0,
    out_dat => prsg_0_reg
  );

  u_prsg_1_reg : entity common_lib.common_pipeline
  generic map (
    g_pipeline    => c_remote_len,
    g_in_dat_w    => prsg_1'LENGTH,
    g_out_dat_w   => prsg_1'length
  )
  port map (
    rst     => st_rst,
    clk     => st_clk,
    in_dat  => prsg_1,
    out_dat => prsg_1_reg
  );

  u_prsg_2_reg : entity common_lib.common_pipeline
  generic map (
    g_pipeline    => c_remote_len,
    g_in_dat_w    => prsg_2'LENGTH,
    g_out_dat_w   => prsg_2'length
  )
  port map (
    rst     => st_rst,
    clk     => st_clk,
    in_dat  => prsg_2,
    out_dat => prsg_2_reg
  );

  u_prsg_3_reg : entity common_lib.common_pipeline
  generic map (
    g_pipeline    => c_remote_len,
    g_in_dat_w    => prsg_3'LENGTH,
    g_out_dat_w   => prsg_3'length
  )
  port map (
    rst     => st_rst,
    clk     => st_clk,
    in_dat  => prsg_3,
    out_dat => prsg_3_reg
  );

  ------------------------------------------------------------------------------
  -- Heater elements
  ------------------------------------------------------------------------------
  gen_heat : for I in 0 to g_nof_mac4 - 1 generate
    ----------------------------------------------------------------------------
    -- Multipliers
    ----------------------------------------------------------------------------

    -- Random input stimuli: mac4 = r0*r1 + r1*r2 + r2*r3 + r3*r0
    in_dat_a(I) <= prsg_0_reg(c_in_dat_w - 1 downto 0) &
                   prsg_1_reg(c_in_dat_w - 1 downto 0) &
                   prsg_2_reg(c_in_dat_w - 1 downto 0) &
                   prsg_3_reg(c_in_dat_w - 1 downto 0);
    in_dat_b(I) <= prsg_1_reg(c_in_dat_w - 1 downto 0) &
                   prsg_2_reg(c_in_dat_w - 1 downto 0) &
                   prsg_3_reg(c_in_dat_w - 1 downto 0) &
                   prsg_0_reg(c_in_dat_w - 1 downto 0);

    -- Complex multipliers, these should get mapped on DSP elements in the FPGA
    u_dsp : entity common_mult_lib.common_mult_add4  -- (rtl)
    generic map (
      g_technology       => g_technology,
      g_in_a_w           => c_in_dat_w,
      g_in_b_w           => c_in_dat_w,
      g_res_w            => c_mac_out_dat_w,
      g_pipeline_input   => c_mac_pipeline_input,
      g_pipeline_product => c_mac_pipeline_product,
      g_pipeline_adder   => c_mac_pipeline_adder,
      g_pipeline_output  => c_mac_pipeline_output
    )
    port map (
      clk        => st_clk,
      clken      => st_clken(I),
      in_a       => in_dat_a(I),
      in_b       => in_dat_b(I),
      res        => mac4(I)
    );

    -- Pipeline, use g_pipeline > 0 to run some more logic resources or to ease achieving timing closure
    u_logic : entity common_lib.common_pipeline
    generic map (
      g_pipeline    => g_pipeline,  -- use 0 for no logic, so only wires
      g_in_dat_w    => c_mac_out_dat_w,
      g_out_dat_w   => c_fifo_dat_w
    )
    port map (
      rst     => st_rst,
      clk     => st_clk,
      clken   => st_clken(I),
      in_dat  => mac4(I),
      out_dat => fifo_in_dat(I)
    );

    ----------------------------------------------------------------------------
    -- RAM
    ----------------------------------------------------------------------------

    -- FIFO, use g_nof_ram > 0 to use RAM or g_nof_ram = 0 to bypass
    gen_ram : if g_nof_ram > 0 generate
      u_fifo : entity common_lib.common_fifo_sc
      generic map (
        g_technology => g_technology,
        g_dat_w     => c_fifo_dat_w,
        g_nof_words => g_nof_ram * c_nof_fifo_dat_in_1kbyte_ram
      )
      port map (
        rst      => st_rst,
        clk      => st_clk,
        wr_dat   => fifo_in_dat(I),
        wr_req   => st_clken(I),
        wr_ful   => OPEN,
        rd_dat   => fifo_out_dat(I),
        rd_req   => st_clken(I),
        rd_emp   => OPEN,
        rd_val   => OPEN,
        usedw    => open
      );
    end generate;

    no_ram : if g_nof_ram = 0 generate
      fifo_out_dat(I) <= fifo_in_dat(I);
    end generate;

    ----------------------------------------------------------------------------
    -- LUTs and FFs
    ----------------------------------------------------------------------------

    gen_logic : if g_nof_logic > 0 generate
      u_logic : entity work.util_logic
      generic map (
        g_nof_reg => g_nof_logic
      )
      port map (
        clk     => st_clk,
        clken   => st_clken(I),
        in_dat  => fifo_out_dat(I),
        out_dat => logic_dat(I)
      );
    end generate;

    no_logic : if g_nof_logic = 0 generate
      logic_dat(I) <= fifo_out_dat(I);
    end generate;

    -- Preserve result, to avoid that the synthesis will optimize all heater element away
    nxt_st_element_xor(I) <= vector_xor(logic_dat(I));  -- arbitrary function to group product bits into single output bit
  end generate;
end rtl;
