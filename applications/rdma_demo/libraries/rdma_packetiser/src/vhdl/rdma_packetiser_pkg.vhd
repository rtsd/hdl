-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. vd Walle
-- Purpose: This package contains rdma_packetiser specific constants and/or functions
-- Description: See [1] for RDMA explanation.
-- References:
-- . [1] https://support.astron.nl/confluence/x/3pKrB
-------------------------------------------------------------------------------
library IEEE, common_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_field_pkg.all;
  use common_lib.common_network_layers_pkg.all;

package rdma_packetiser_pkg is
  type t_rdma_packetiser_bth_header is record
    opcode        : std_logic_vector( 7 downto 0);
    se            : std_logic_vector( 0 downto 0);
    m             : std_logic_vector( 0 downto 0);
    pad           : std_logic_vector( 1 downto 0);
    tver          : std_logic_vector( 3 downto 0);
    partition_key : std_logic_vector(15 downto 0);
    fres          : std_logic_vector( 0 downto 0);
    bres          : std_logic_vector( 0 downto 0);
    reserved_a    : std_logic_vector( 5 downto 0);
    dest_qp       : std_logic_vector(15 downto 0);
    ack_req       : std_logic_vector( 0 downto 0);
    reserved_b    : std_logic_vector( 6 downto 0);
    psn           : std_logic_vector(31 downto 0);
  end record;

  type t_rdma_packetiser_reth_header is record
    virtual_address : std_logic_vector(63 downto 0);
    r_key           : std_logic_vector(31 downto 0);
    dma_length      : std_logic_vector(31 downto 0);
  end record;

  type t_rdma_packetiser_roce_header is record
    eth            : t_network_eth_header;
    ip             : t_network_ip_header;
    udp            : t_network_udp_header;
    bth            : t_rdma_packetiser_bth_header;
    reth           : t_rdma_packetiser_reth_header;
    immediate_data : std_logic_vector(31 downto 0);
  end record;

  constant c_rdma_packetiser_nof_octet_generate_100gbe : natural := 64;
  constant c_rdma_packetiser_nof_octet_output_100gbe   : natural := 64;

  -- hdr_field_sel bit selects where the hdr_field value is set:
  -- . 0 = data path controlled, value is set in data path, so field_default()
  --       is not used.
  -- . 1 = MM controlled, value is set via MM or by the field_default(), so any
  --       data path setting in eth_tester.vhd is not used.
  -- Remarks:
  -- . For constant values it is convenient to use MM controlled, because then
  --   the field_default() is used that can be set here in
  --   c_rdma_packetiser_hdr_field_arr.
  -- . For reserved values it is convenient to use MM controlled, because then
  --   in future they could still be changed via MM without having to recompile
  --   the FW.
  -- . Typically only use data path controlled if the value has to be set
  --   dynamically, so dependent on the state of the FW.
  -- . If a data path controlled field is not set in the FW, then it defaults
  --   to 0 by declaring hdr_fields_in_arr with all 0. Hence e.g. udp_checksum
  --   = 0 can be achieve via data path and default hdr_fields_in_arr = 0 or
  --   via MM controlled and field_default(0).

  -- RoCEv2 header for RDMA operation with all possible fields and user config fields
  -- ETH + IP + UDP + Base Transport Header (BTH) + RDMA Extended Transport Header (RETH) + Immediate Data + user config
  -- Primarly used as a MM register map to provide the data needed for all header variants.
  constant c_rdma_packetiser_mm_nof_fields : natural := 3 + 12 + 4 + 13 + 3 + 1 + 6;
  constant c_rdma_packetiser_mm_field_sel  : std_logic_vector(c_rdma_packetiser_mm_nof_fields - 1 downto 0) :=  "111" & "111011111011" & "1100" & "0111111111110" & "011" & "1" & "011111";

  constant c_rdma_packetiser_mm_field_arr : t_common_field_arr(
    c_rdma_packetiser_mm_nof_fields - 1 downto 0) := (
    ( field_name_pad("eth_dst_mac"                    ), "RW", 48, field_default(0) ),
    ( field_name_pad("eth_src_mac"                    ), "RW", 48, field_default(0) ),
    ( field_name_pad("eth_type"                       ), "RW", 16, field_default(x"0800") ),

    ( field_name_pad("ip_version"                     ), "RW",  4, field_default(4) ), -- fixed
    ( field_name_pad("ip_header_length"               ), "RW",  4, field_default(5) ), -- fixed
    ( field_name_pad("ip_services"                    ), "RW",  8, field_default(0) ), -- fixed
    ( field_name_pad("ip_total_length"                ), "RW", 16, field_default(0) ), -- set by data path
    ( field_name_pad("ip_identification"              ), "RW", 16, field_default(0) ), -- fixed
    ( field_name_pad("ip_flags"                       ), "RW",  3, field_default(2) ), -- fixed
    ( field_name_pad("ip_fragment_offset"             ), "RW", 13, field_default(0) ), -- fixed
    ( field_name_pad("ip_time_to_live"                ), "RW",  8, field_default(127) ), -- fixed
    ( field_name_pad("ip_protocol"                    ), "RW",  8, field_default(17) ), -- fixed
    ( field_name_pad("ip_header_checksum"             ), "RW", 16, field_default(0) ), -- set by data path
    ( field_name_pad("ip_src_addr"                    ), "RW", 32, field_default(0) ), -- set by M&C
    ( field_name_pad("ip_dst_addr"                    ), "RW", 32, field_default(0) ), -- set by M&C

    ( field_name_pad("udp_src_port"                   ), "RW", 16, field_default(0) ), -- set by M&C
    ( field_name_pad("udp_dst_port"                   ), "RW", 16, field_default(0) ), -- set by M&C
    ( field_name_pad("udp_total_length"               ), "RW", 16, field_default(0) ), -- set by data path
    ( field_name_pad("udp_checksum"                   ), "RW", 16, field_default(0) ), -- fixed

    ( field_name_pad("bth_opcode"                     ), "RW",  8, field_default(x"FF") ), -- set by data path
    ( field_name_pad("bth_se"                         ), "RW",  1, field_default(0) ), -- set by M&C
    ( field_name_pad("bth_m"                          ), "RW",  1, field_default(0) ), -- set by M&C
    ( field_name_pad("bth_pad"                        ), "RW",  2, field_default(0) ), -- set by M&C
    ( field_name_pad("bth_tver"                       ), "RW",  4, field_default(0) ), -- set by M&C
    ( field_name_pad("bth_partition_key"              ), "RW", 16, field_default(65535) ), -- set by M&C
    ( field_name_pad("bth_fres"                       ), "RW",  1, field_default(0) ), -- set by M&C
    ( field_name_pad("bth_bres"                       ), "RW",  1, field_default(0) ), -- set by M&C
    ( field_name_pad("bth_reserved_a"                 ), "RW",  6, field_default(0) ), -- fixed
    ( field_name_pad("bth_dest_qp"                    ), "RW", 16, field_default(0) ), -- set by M&C
    ( field_name_pad("bth_ack_req"                    ), "RW",  1, field_default(0) ), -- set by M&C
    ( field_name_pad("bth_reserved_b"                 ), "RW",  7, field_default(0) ), -- fixed
    ( field_name_pad("bth_psn"                        ), "RW", 32, field_default(0) ), -- set by data path

    ( field_name_pad("reth_virtual_address"           ), "RW", 64, field_default(0) ), -- set by data path
    ( field_name_pad("reth_r_key"                     ), "RW", 32, field_default(0) ), -- set by M&C
    ( field_name_pad("reth_dma_length"                ), "RW", 32, field_default(0) ), -- set by M&C

    ( field_name_pad("immediate_data"                 ), "RW", 32, field_default(0) ), -- set by M&C

    ( field_name_pad("config_reserved"                ), "RW",  6, field_default(0) ), -- fixed
    ( field_name_pad("config_use_immediate"           ), "RW",  1, field_default(0) ), -- set by M&C
    ( field_name_pad("config_use_msg_cnt_as_immediate"), "RW",  1, field_default(0) ), -- set by M&C
    ( field_name_pad("config_nof_packets_in_msg"      ), "RW", 32, field_default(0) ), -- set by M&C
    ( field_name_pad("config_nof_msg"                 ), "RW", 32, field_default(0) ), -- set by M&C
    ( field_name_pad("config_start_address"           ), "RW", 64, field_default(0) ) -- set by M&C
  );
  constant c_rdma_packetiser_reg_mm_dat_addr_w    : natural := ceil_log2(field_nof_words(c_rdma_packetiser_mm_field_arr, c_word_w));
  constant c_rdma_packetiser_reg_mm_dat_addr_span : natural := 2**c_rdma_packetiser_reg_mm_dat_addr_w;

  -- ETH header
  -- Handeled seperate from the other headers as the ethernet header must be excluded from the icrc checksum computation.
  constant c_rdma_packetiser_eth_hdr_nof_fields : natural := 3;
  constant c_rdma_packetiser_eth_hdr_field_sel  : std_logic_vector(c_rdma_packetiser_eth_hdr_nof_fields - 1 downto 0) :=  (others => '0');

  constant c_rdma_packetiser_eth_hdr_field_arr : t_common_field_arr(
    c_rdma_packetiser_eth_hdr_nof_fields - 1 downto 0) := (
    ( field_name_pad("eth_dst_mac"                    ), "RW", 48, field_default(0) ),
    ( field_name_pad("eth_src_mac"                    ), "RW", 48, field_default(0) ),
    ( field_name_pad("eth_type"                       ), "RW", 16, field_default(x"0800") )
  );

  -- RoCEv2 header for first packets and write only packets without immediate data
  -- IP + UDP + Base Transport Header (BTH) + RDMA Extended Transport Header (RETH)
  constant c_rdma_packetiser_first_nof_hdr_fields : natural := 12 + 4 + 13 + 3;
  constant c_rdma_packetiser_first_hdr_field_sel  : std_logic_vector(c_rdma_packetiser_first_nof_hdr_fields - 1 downto 0) :=  (others => '0');

  constant c_rdma_packetiser_first_hdr_field_arr : t_common_field_arr(
    c_rdma_packetiser_first_nof_hdr_fields - 1 downto 0) := (
    ( field_name_pad("ip_version"          ), "RW",  4, field_default(4) ),
    ( field_name_pad("ip_header_length"    ), "RW",  4, field_default(5) ),
    ( field_name_pad("ip_services"         ), "RW",  8, field_default(0) ),
    ( field_name_pad("ip_total_length"     ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_identification"   ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_flags"            ), "RW",  3, field_default(2) ),
    ( field_name_pad("ip_fragment_offset"  ), "RW", 13, field_default(0) ),
    ( field_name_pad("ip_time_to_live"     ), "RW",  8, field_default(127) ),
    ( field_name_pad("ip_protocol"         ), "RW",  8, field_default(17) ),
    ( field_name_pad("ip_header_checksum"  ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_src_addr"         ), "RW", 32, field_default(0) ),
    ( field_name_pad("ip_dst_addr"         ), "RW", 32, field_default(0) ),

    ( field_name_pad("udp_src_port"        ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_dst_port"        ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_total_length"    ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_checksum"        ), "RW", 16, field_default(0) ),

    ( field_name_pad("bth_opcode"          ), "RW",  8, field_default(x"FF") ),
    ( field_name_pad("bth_se"              ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_m"               ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_pad"             ), "RW",  2, field_default(0) ),
    ( field_name_pad("bth_tver"            ), "RW",  4, field_default(0) ),
    ( field_name_pad("bth_partition_key"   ), "RW", 16, field_default(65535) ),
    ( field_name_pad("bth_fres"            ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_bres"            ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_reserved_a"      ), "RW",  6, field_default(0) ),
    ( field_name_pad("bth_dest_qp"         ), "RW", 16, field_default(0) ),
    ( field_name_pad("bth_ack_req"         ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_reserved_b"      ), "RW",  7, field_default(0) ),
    ( field_name_pad("bth_psn"             ), "RW", 32, field_default(0) ),

    ( field_name_pad("reth_virtual_address"), "RW", 64, field_default(0) ),
    ( field_name_pad("reth_r_key"          ), "RW", 32, field_default(0) ),
    ( field_name_pad("reth_dma_length"     ), "RW", 32, field_default(0) )

  );

  -- RoCEv2 header for middle packets and last packets without immediate data.
  -- IP + UDP + Base Transport Header (BTH)
  constant c_rdma_packetiser_mid_nof_hdr_fields : natural := 12 + 4 + 13;
  constant c_rdma_packetiser_mid_hdr_field_sel  : std_logic_vector(c_rdma_packetiser_mid_nof_hdr_fields - 1 downto 0) :=  (others => '0');

  constant c_rdma_packetiser_mid_hdr_field_arr : t_common_field_arr(
    c_rdma_packetiser_mid_nof_hdr_fields - 1 downto 0) := (
    ( field_name_pad("ip_version"        ), "RW",  4, field_default(4) ),
    ( field_name_pad("ip_header_length"  ), "RW",  4, field_default(5) ),
    ( field_name_pad("ip_services"       ), "RW",  8, field_default(0) ),
    ( field_name_pad("ip_total_length"   ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_identification" ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_flags"          ), "RW",  3, field_default(2) ),
    ( field_name_pad("ip_fragment_offset"), "RW", 13, field_default(0) ),
    ( field_name_pad("ip_time_to_live"   ), "RW",  8, field_default(127) ),
    ( field_name_pad("ip_protocol"       ), "RW",  8, field_default(17) ),
    ( field_name_pad("ip_header_checksum"), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_src_addr"       ), "RW", 32, field_default(0) ),
    ( field_name_pad("ip_dst_addr"       ), "RW", 32, field_default(0) ),

    ( field_name_pad("udp_src_port"      ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_dst_port"      ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_total_length"  ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_checksum"      ), "RW", 16, field_default(0) ),

    ( field_name_pad("bth_opcode"        ), "RW",  8, field_default(x"FF") ),
    ( field_name_pad("bth_se"            ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_m"             ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_pad"           ), "RW",  2, field_default(0) ),
    ( field_name_pad("bth_tver"          ), "RW",  4, field_default(0) ),
    ( field_name_pad("bth_partition_key" ), "RW", 16, field_default(65535) ),
    ( field_name_pad("bth_fres"          ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_bres"          ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_reserved_a"    ), "RW",  6, field_default(0) ),
    ( field_name_pad("bth_dest_qp"       ), "RW", 16, field_default(0) ),
    ( field_name_pad("bth_ack_req"       ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_reserved_b"    ), "RW",  7, field_default(0) ),
    ( field_name_pad("bth_psn"           ), "RW", 32, field_default(0) )
  );

  -- RoCEv2 header for last packets with immediate data
  -- IP + UDP + Base Transport Header (BTH) + immediate data
  constant c_rdma_packetiser_last_nof_hdr_fields : natural := 12 + 4 + 13 + 1;
  constant c_rdma_packetiser_last_hdr_field_sel  : std_logic_vector(c_rdma_packetiser_last_nof_hdr_fields - 1 downto 0) :=  (others => '0');

  constant c_rdma_packetiser_last_hdr_field_arr : t_common_field_arr(
    c_rdma_packetiser_last_nof_hdr_fields - 1 downto 0) := (
    ( field_name_pad("ip_version"        ), "RW",  4, field_default(4) ),
    ( field_name_pad("ip_header_length"  ), "RW",  4, field_default(5) ),
    ( field_name_pad("ip_services"       ), "RW",  8, field_default(0) ),
    ( field_name_pad("ip_total_length"   ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_identification" ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_flags"          ), "RW",  3, field_default(2) ),
    ( field_name_pad("ip_fragment_offset"), "RW", 13, field_default(0) ),
    ( field_name_pad("ip_time_to_live"   ), "RW",  8, field_default(127) ),
    ( field_name_pad("ip_protocol"       ), "RW",  8, field_default(17) ),
    ( field_name_pad("ip_header_checksum"), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_src_addr"       ), "RW", 32, field_default(0) ),
    ( field_name_pad("ip_dst_addr"       ), "RW", 32, field_default(0) ),

    ( field_name_pad("udp_src_port"      ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_dst_port"      ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_total_length"  ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_checksum"      ), "RW", 16, field_default(0) ),

    ( field_name_pad("bth_opcode"        ), "RW",  8, field_default(x"FF") ),
    ( field_name_pad("bth_se"            ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_m"             ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_pad"           ), "RW",  2, field_default(0) ),
    ( field_name_pad("bth_tver"          ), "RW",  4, field_default(0) ),
    ( field_name_pad("bth_partition_key" ), "RW", 16, field_default(65535) ),
    ( field_name_pad("bth_fres"          ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_bres"          ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_reserved_a"    ), "RW",  6, field_default(0) ),
    ( field_name_pad("bth_dest_qp"       ), "RW", 16, field_default(0) ),
    ( field_name_pad("bth_ack_req"       ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_reserved_b"    ), "RW",  7, field_default(0) ),
    ( field_name_pad("bth_psn"           ), "RW", 32, field_default(0) ),

    ( field_name_pad("immediate_data"    ), "RW", 32, field_default(0) )
  );

  -- RoCEv2 header for write only packets with immediate data
  -- IP + UDP + Base Transport Header (BTH) + RDMA Extended Transport Header (RETH) + immediate data
  constant c_rdma_packetiser_wo_nof_hdr_fields : natural := 12 + 4 + 13 + 3 + 1;
  constant c_rdma_packetiser_wo_hdr_field_sel  : std_logic_vector(c_rdma_packetiser_wo_nof_hdr_fields - 1 downto 0) :=  (others => '0');

  constant c_rdma_packetiser_wo_hdr_field_arr : t_common_field_arr(
    c_rdma_packetiser_wo_nof_hdr_fields - 1 downto 0) := (
    ( field_name_pad("ip_version"          ), "RW",  4, field_default(4) ),
    ( field_name_pad("ip_header_length"    ), "RW",  4, field_default(5) ),
    ( field_name_pad("ip_services"         ), "RW",  8, field_default(0) ),
    ( field_name_pad("ip_total_length"     ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_identification"   ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_flags"            ), "RW",  3, field_default(2) ),
    ( field_name_pad("ip_fragment_offset"  ), "RW", 13, field_default(0) ),
    ( field_name_pad("ip_time_to_live"     ), "RW",  8, field_default(127) ),
    ( field_name_pad("ip_protocol"         ), "RW",  8, field_default(17) ),
    ( field_name_pad("ip_header_checksum"  ), "RW", 16, field_default(0) ),
    ( field_name_pad("ip_src_addr"         ), "RW", 32, field_default(0) ),
    ( field_name_pad("ip_dst_addr"         ), "RW", 32, field_default(0) ),

    ( field_name_pad("udp_src_port"        ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_dst_port"        ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_total_length"    ), "RW", 16, field_default(0) ),
    ( field_name_pad("udp_checksum"        ), "RW", 16, field_default(0) ),

    ( field_name_pad("bth_opcode"          ), "RW",  8, field_default(x"FF") ),
    ( field_name_pad("bth_se"              ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_m"               ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_pad"             ), "RW",  2, field_default(0) ),
    ( field_name_pad("bth_tver"            ), "RW",  4, field_default(0) ),
    ( field_name_pad("bth_partition_key"   ), "RW", 16, field_default(65535) ),
    ( field_name_pad("bth_fres"            ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_bres"            ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_reserved_a"      ), "RW",  6, field_default(0) ),
    ( field_name_pad("bth_dest_qp"         ), "RW", 16, field_default(0) ),
    ( field_name_pad("bth_ack_req"         ), "RW",  1, field_default(0) ),
    ( field_name_pad("bth_reserved_b"      ), "RW",  7, field_default(0) ),
    ( field_name_pad("bth_psn"             ), "RW", 32, field_default(0) ),

    ( field_name_pad("reth_virtual_address"), "RW", 64, field_default(0) ),
    ( field_name_pad("reth_r_key"          ), "RW", 32, field_default(0) ),
    ( field_name_pad("reth_dma_length"     ), "RW", 32, field_default(0) ),

    ( field_name_pad("immediate_data"      ), "RW", 32, field_default(0) )
  );

  constant c_rdma_packetiser_bth_len  : natural := 12;  -- octets
  constant c_rdma_packetiser_reth_len : natural := 16;  -- octets
  constant c_rdma_packetiser_imm_len  : natural := 4;   -- octets
  constant c_rdma_packetiser_icrc_len : natural := 4;   -- octets

  constant c_rdma_packetiser_opcode_uc_send_first      : std_logic_vector(7 downto 0) := "001" & "00000";
  constant c_rdma_packetiser_opcode_uc_send_middle     : std_logic_vector(7 downto 0) := "001" & "00001";
  constant c_rdma_packetiser_opcode_uc_send_last       : std_logic_vector(7 downto 0) := "001" & "00010"; -- without immediate
  constant c_rdma_packetiser_opcode_uc_send_last_imm   : std_logic_vector(7 downto 0) := "001" & "00011"; -- with immediate
  constant c_rdma_packetiser_opcode_uc_send_only       : std_logic_vector(7 downto 0) := "001" & "00100"; -- without immediate
  constant c_rdma_packetiser_opcode_uc_send_only_imm   : std_logic_vector(7 downto 0) := "001" & "00101"; -- with immediate
  constant c_rdma_packetiser_opcode_uc_write_first     : std_logic_vector(7 downto 0) := "001" & "00110";
  constant c_rdma_packetiser_opcode_uc_write_middle    : std_logic_vector(7 downto 0) := "001" & "00111";
  constant c_rdma_packetiser_opcode_uc_write_last      : std_logic_vector(7 downto 0) := "001" & "01000"; -- without immediate
  constant c_rdma_packetiser_opcode_uc_write_last_imm  : std_logic_vector(7 downto 0) := "001" & "01001"; -- with immediate
  constant c_rdma_packetiser_opcode_uc_write_only      : std_logic_vector(7 downto 0) := "001" & "01010"; -- without immediate
  constant c_rdma_packetiser_opcode_uc_write_only_imm  : std_logic_vector(7 downto 0) := "001" & "01011"; -- with immediate

  function func_rdma_packetiser_map_header(hdr_fields_raw : std_logic_vector; field_arr : t_common_field_arr) return t_rdma_packetiser_roce_header;
  function func_rdma_packetiser_unmap_header(hdr_fields : t_rdma_packetiser_roce_header; field_arr : t_common_field_arr) return std_logic_vector;
end rdma_packetiser_pkg;

package body rdma_packetiser_pkg is
  function func_rdma_packetiser_map_header(hdr_fields_raw : std_logic_vector; field_arr : t_common_field_arr) return t_rdma_packetiser_roce_header is
    variable v : t_rdma_packetiser_roce_header;
    constant c_hdr_field_arr : t_common_field_arr := field_arr;
  begin
    -- eth header (optional)
    if field_exists(c_hdr_field_arr, "eth_dst_mac") then -- eth header exists
      v.eth.dst_mac          := hdr_fields_raw(field_hi(c_hdr_field_arr, "eth_dst_mac") downto field_lo(c_hdr_field_arr, "eth_dst_mac"));
      v.eth.src_mac          := hdr_fields_raw(field_hi(c_hdr_field_arr, "eth_src_mac") downto field_lo(c_hdr_field_arr, "eth_src_mac"));
      v.eth.eth_type         := hdr_fields_raw(field_hi(c_hdr_field_arr, "eth_type")    downto field_lo(c_hdr_field_arr, "eth_type"));
    end if;

    -- ip header
    v.ip.version           := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_version")         downto field_lo(c_hdr_field_arr, "ip_version"));
    v.ip.header_length     := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_header_length")   downto field_lo(c_hdr_field_arr, "ip_header_length"));
    v.ip.services          := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_services")        downto field_lo(c_hdr_field_arr, "ip_services"));
    v.ip.total_length      := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_total_length")    downto field_lo(c_hdr_field_arr, "ip_total_length"));
    v.ip.identification    := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_identification")  downto field_lo(c_hdr_field_arr, "ip_identification"));
    v.ip.flags             := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_flags")           downto field_lo(c_hdr_field_arr, "ip_flags"));
    v.ip.fragment_offset   := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_fragment_offset") downto field_lo(c_hdr_field_arr, "ip_fragment_offset"));
    v.ip.time_to_live      := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_time_to_live")    downto field_lo(c_hdr_field_arr, "ip_time_to_live"));
    v.ip.protocol          := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_protocol")        downto field_lo(c_hdr_field_arr, "ip_protocol"));
    v.ip.header_checksum   := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_header_checksum") downto field_lo(c_hdr_field_arr, "ip_header_checksum"));
    v.ip.src_ip_addr       := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_src_addr")        downto field_lo(c_hdr_field_arr, "ip_src_addr"));
    v.ip.dst_ip_addr       := hdr_fields_raw(field_hi(c_hdr_field_arr, "ip_dst_addr")        downto field_lo(c_hdr_field_arr, "ip_dst_addr"));

    -- udp header
    v.udp.src_port         := hdr_fields_raw(field_hi(c_hdr_field_arr, "udp_src_port")     downto field_lo(c_hdr_field_arr, "udp_src_port"));
    v.udp.dst_port         := hdr_fields_raw(field_hi(c_hdr_field_arr, "udp_dst_port")     downto field_lo(c_hdr_field_arr, "udp_dst_port"));
    v.udp.total_length     := hdr_fields_raw(field_hi(c_hdr_field_arr, "udp_total_length") downto field_lo(c_hdr_field_arr, "udp_total_length"));
    v.udp.checksum         := hdr_fields_raw(field_hi(c_hdr_field_arr, "udp_checksum")     downto field_lo(c_hdr_field_arr, "udp_checksum"));

    -- bth header
    v.bth.opcode           := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_opcode")        downto field_lo(c_hdr_field_arr, "bth_opcode"));
    v.bth.se               := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_se")            downto field_lo(c_hdr_field_arr, "bth_se"));
    v.bth.m                := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_m")             downto field_lo(c_hdr_field_arr, "bth_m"));
    v.bth.pad              := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_pad")           downto field_lo(c_hdr_field_arr, "bth_pad"));
    v.bth.tver             := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_tver")          downto field_lo(c_hdr_field_arr, "bth_tver"));
    v.bth.partition_key    := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_partition_key") downto field_lo(c_hdr_field_arr, "bth_partition_key"));
    v.bth.fres             := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_fres")          downto field_lo(c_hdr_field_arr, "bth_fres"));
    v.bth.bres             := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_bres")          downto field_lo(c_hdr_field_arr, "bth_bres"));
    v.bth.reserved_a       := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_reserved_a")    downto field_lo(c_hdr_field_arr, "bth_reserved_a"));
    v.bth.dest_qp          := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_dest_qp")       downto field_lo(c_hdr_field_arr, "bth_dest_qp"));
    v.bth.ack_req          := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_ack_req")       downto field_lo(c_hdr_field_arr, "bth_ack_req"));
    v.bth.reserved_b       := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_reserved_b")    downto field_lo(c_hdr_field_arr, "bth_reserved_b"));
    v.bth.psn              := hdr_fields_raw(field_hi(c_hdr_field_arr, "bth_psn")           downto field_lo(c_hdr_field_arr, "bth_psn"));

    -- reth header (optional)
    v.reth := ((others => '0'), (others => '0'), (others => '0'));
    if field_exists(c_hdr_field_arr, "reth_virtual_address") then -- reth header exists
      v.reth.virtual_address := hdr_fields_raw(field_hi(c_hdr_field_arr, "reth_virtual_address") downto field_lo(c_hdr_field_arr, "reth_virtual_address"));
      v.reth.r_key           := hdr_fields_raw(field_hi(c_hdr_field_arr, "reth_r_key")           downto field_lo(c_hdr_field_arr, "reth_r_key"));
      v.reth.dma_length      := hdr_fields_raw(field_hi(c_hdr_field_arr, "reth_dma_length")      downto field_lo(c_hdr_field_arr, "reth_dma_length"));
    end if;

    --immediate data (optional)
    v.immediate_data := (others => '0');
    if field_exists(c_hdr_field_arr, "immediate_data") then -- immediate data exists
      v.immediate_data := hdr_fields_raw(field_hi(c_hdr_field_arr, "immediate_data") downto field_lo(c_hdr_field_arr, "immediate_data"));
    end if;

    return v;
  end func_rdma_packetiser_map_header;

  function func_rdma_packetiser_unmap_header(hdr_fields : t_rdma_packetiser_roce_header; field_arr : t_common_field_arr) return std_logic_vector is
    constant c_field_len : natural := field_slv_len(field_arr);
    variable v : std_logic_vector(c_field_len-1 downto 0) := (others => '0');
    constant c_hdr_field_arr : t_common_field_arr := field_arr;
  begin
    -- eth header (optional)
    if field_exists(c_hdr_field_arr, "eth_dst_mac") then -- eth header exists
      v(field_hi(c_hdr_field_arr, "eth_dst_mac") downto field_lo(c_hdr_field_arr, "eth_dst_mac"))               := hdr_fields.eth.dst_mac;
      v(field_hi(c_hdr_field_arr, "eth_src_mac") downto field_lo(c_hdr_field_arr, "eth_src_mac"))               := hdr_fields.eth.src_mac;
      v(field_hi(c_hdr_field_arr, "eth_type")    downto field_lo(c_hdr_field_arr, "eth_type"))                  := hdr_fields.eth.eth_type;
    end if;

    -- ip header
    v(field_hi(c_hdr_field_arr, "ip_version")         downto field_lo(c_hdr_field_arr, "ip_version"))         := hdr_fields.ip.version;
    v(field_hi(c_hdr_field_arr, "ip_header_length")   downto field_lo(c_hdr_field_arr, "ip_header_length"))   := hdr_fields.ip.header_length;
    v(field_hi(c_hdr_field_arr, "ip_services")        downto field_lo(c_hdr_field_arr, "ip_services"))        := hdr_fields.ip.services;
    v(field_hi(c_hdr_field_arr, "ip_total_length")    downto field_lo(c_hdr_field_arr, "ip_total_length"))    := hdr_fields.ip.total_length;
    v(field_hi(c_hdr_field_arr, "ip_identification")  downto field_lo(c_hdr_field_arr, "ip_identification"))  := hdr_fields.ip.identification;
    v(field_hi(c_hdr_field_arr, "ip_flags")           downto field_lo(c_hdr_field_arr, "ip_flags"))           := hdr_fields.ip.flags;
    v(field_hi(c_hdr_field_arr, "ip_fragment_offset") downto field_lo(c_hdr_field_arr, "ip_fragment_offset")) := hdr_fields.ip.fragment_offset;
    v(field_hi(c_hdr_field_arr, "ip_time_to_live")    downto field_lo(c_hdr_field_arr, "ip_time_to_live"))    := hdr_fields.ip.time_to_live;
    v(field_hi(c_hdr_field_arr, "ip_protocol")        downto field_lo(c_hdr_field_arr, "ip_protocol"))        := hdr_fields.ip.protocol;
    v(field_hi(c_hdr_field_arr, "ip_header_checksum") downto field_lo(c_hdr_field_arr, "ip_header_checksum")) := hdr_fields.ip.header_checksum;
    v(field_hi(c_hdr_field_arr, "ip_src_addr")        downto field_lo(c_hdr_field_arr, "ip_src_addr"))        := hdr_fields.ip.src_ip_addr;
    v(field_hi(c_hdr_field_arr, "ip_dst_addr")        downto field_lo(c_hdr_field_arr, "ip_dst_addr"))        := hdr_fields.ip.dst_ip_addr;

    -- udp header
    v(field_hi(c_hdr_field_arr, "udp_src_port")     downto field_lo(c_hdr_field_arr, "udp_src_port"))         := hdr_fields.udp.src_port;
    v(field_hi(c_hdr_field_arr, "udp_dst_port")     downto field_lo(c_hdr_field_arr, "udp_dst_port"))         := hdr_fields.udp.dst_port;
    v(field_hi(c_hdr_field_arr, "udp_total_length") downto field_lo(c_hdr_field_arr, "udp_total_length"))     := hdr_fields.udp.total_length;
    v(field_hi(c_hdr_field_arr, "udp_checksum")     downto field_lo(c_hdr_field_arr, "udp_checksum"))         := hdr_fields.udp.checksum;

    -- bth header
    v(field_hi(c_hdr_field_arr, "bth_opcode")        downto field_lo(c_hdr_field_arr, "bth_opcode"))          := hdr_fields.bth.opcode;
    v(field_hi(c_hdr_field_arr, "bth_se")            downto field_lo(c_hdr_field_arr, "bth_se"))              := hdr_fields.bth.se;
    v(field_hi(c_hdr_field_arr, "bth_m")             downto field_lo(c_hdr_field_arr, "bth_m"))               := hdr_fields.bth.m;
    v(field_hi(c_hdr_field_arr, "bth_pad")           downto field_lo(c_hdr_field_arr, "bth_pad"))             := hdr_fields.bth.pad;
    v(field_hi(c_hdr_field_arr, "bth_tver")          downto field_lo(c_hdr_field_arr, "bth_tver"))            := hdr_fields.bth.tver;
    v(field_hi(c_hdr_field_arr, "bth_partition_key") downto field_lo(c_hdr_field_arr, "bth_partition_key"))   := hdr_fields.bth.partition_key;
    v(field_hi(c_hdr_field_arr, "bth_fres")          downto field_lo(c_hdr_field_arr, "bth_fres"))            := hdr_fields.bth.fres;
    v(field_hi(c_hdr_field_arr, "bth_bres")          downto field_lo(c_hdr_field_arr, "bth_bres"))            := hdr_fields.bth.bres;
    v(field_hi(c_hdr_field_arr, "bth_reserved_a")    downto field_lo(c_hdr_field_arr, "bth_reserved_a"))      := hdr_fields.bth.reserved_a;
    v(field_hi(c_hdr_field_arr, "bth_dest_qp")       downto field_lo(c_hdr_field_arr, "bth_dest_qp"))         := hdr_fields.bth.dest_qp;
    v(field_hi(c_hdr_field_arr, "bth_ack_req")       downto field_lo(c_hdr_field_arr, "bth_ack_req"))         := hdr_fields.bth.ack_req;
    v(field_hi(c_hdr_field_arr, "bth_reserved_b")    downto field_lo(c_hdr_field_arr, "bth_reserved_b"))      := hdr_fields.bth.reserved_b;
    v(field_hi(c_hdr_field_arr, "bth_psn")           downto field_lo(c_hdr_field_arr, "bth_psn"))             := hdr_fields.bth.psn;

    -- reth header
    v(field_hi(c_hdr_field_arr, "reth_virtual_address") downto field_lo(c_hdr_field_arr, "reth_virtual_address")) := hdr_fields.reth.virtual_address;
    v(field_hi(c_hdr_field_arr, "reth_r_key")           downto field_lo(c_hdr_field_arr, "reth_r_key"))           := hdr_fields.reth.r_key;
    v(field_hi(c_hdr_field_arr, "reth_dma_length")      downto field_lo(c_hdr_field_arr, "reth_dma_length"))      := hdr_fields.reth.dma_length;

    --immediate data
    v(field_hi(c_hdr_field_arr, "immediate_data") downto field_lo(c_hdr_field_arr, "immediate_data")) := hdr_fields.immediate_data;

    return v;
  end func_rdma_packetiser_unmap_header;
end rdma_packetiser_pkg;
