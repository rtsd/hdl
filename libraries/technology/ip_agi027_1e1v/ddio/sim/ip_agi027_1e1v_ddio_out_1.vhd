-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   D.F. Brouwer
-- Purpose:
--   Simulation model for DDIO out
-- Description:
--   This function is the inverse of DDIO in as described in ip_agi027_1e1v_ddio_in_1.
--   The timing diagram:
--                 _   _   _   _   _
--     outclock   | |_| |_| |_| |_| |_
--     datain_h        1   3   5
--     datain_l        0   2   4
--     dataout @ r         1   3   5
--     dataout @ f       0   2   4
--     dataout           0 1 2 3 4 5 6 7
-- Reference:
--   Copied from ip_arria10_e2sg/ddio/sim/ip_arria10_e2sg_ddio_out_1.vhd

library IEEE;
use IEEE.std_logic_1164.all;

entity ip_agi027_1e1v_ddio_out_1 is
  port (
    dataout  : out std_logic_vector(0 downto 0);
    outclock : in  std_logic                    := '0';
    aclr     : in  std_logic                    := '0';
    datain_h : in  std_logic_vector(0 downto 0) := (others => '0');
    datain_l : in  std_logic_vector(0 downto 0) := (others => '0')
  );
end ip_agi027_1e1v_ddio_out_1;

architecture beh of ip_agi027_1e1v_ddio_out_1 is
  signal out_dat_r   : std_logic;
  signal out_dat_f   : std_logic;
begin
  dataout <= datain_l when falling_edge(outclock) else
             datain_h when rising_edge(outclock);
end beh;
