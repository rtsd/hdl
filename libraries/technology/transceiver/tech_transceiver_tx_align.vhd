--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tech_transceiver_tx_align is
  generic(
    g_sim    : boolean;
    g_data_w : natural;
    g_mbps   : natural
  );
  port(
    tx_clk             : in  std_logic;
    tx_rst             : in  std_logic;
    tx_align_en_in     : in  std_logic;
    tx_align_en_out    : out std_logic;
    tx_state           : out std_logic_vector(1 downto 0)
   );
end tech_transceiver_tx_align;

architecture rtl of tech_transceiver_tx_align is
  constant c_tx_clk_freq : natural := (g_mbps / g_data_w) / 10 * 8 * 1000000;

  constant c_timeout_1s  : natural := c_tx_clk_freq;
  constant c_timeout_sim : natural := 1000;
  constant c_timeout     : natural := sel_a_b(g_sim, c_timeout_sim, 2 * c_timeout_1s);
  constant c_timeout_w   : natural := ceil_log2(c_timeout);

  type t_state_enum is (s_init, s_init_send_pattern, s_send_user_data, s_force_pattern);

  signal state                    : t_state_enum;
  signal nxt_state                : t_state_enum;

  signal i_tx_align_en            : std_logic;
  signal nxt_tx_align_en          : std_logic;

  signal cycle_cnt                : std_logic_vector(c_timeout_w - 1 downto 0);
  signal nxt_cycle_cnt            : std_logic_vector(c_timeout_w - 1 downto 0);

  signal nxt_tx_state             : std_logic_vector(1 downto 0);
  signal i_tx_state               : std_logic_vector(1 downto 0);
begin
  tx_state        <= i_tx_state;
  tx_align_en_out <= i_tx_align_en;

  p_clk : process(tx_rst, tx_clk)
  begin
    if tx_rst = '1' then
      state                <= s_init;
      i_tx_align_en        <= '1';
      cycle_cnt            <= (others => '0');
      i_tx_state           <= (others => '0');
    elsif rising_edge(tx_clk) then
      state                <= nxt_state;
      i_tx_align_en        <= nxt_tx_align_en;
      cycle_cnt            <= nxt_cycle_cnt;
      i_tx_state           <= nxt_tx_state;
    end if;
  end process;

  p_state : process(state, i_tx_align_en, cycle_cnt, i_tx_state, tx_align_en_in)
  begin
    nxt_state           <= state;
    nxt_tx_align_en     <= i_tx_align_en;
    nxt_cycle_cnt       <= cycle_cnt;
    nxt_tx_state        <= i_tx_state;

    case state is
      when s_init =>
        nxt_state <= s_init_send_pattern;

      when s_init_send_pattern =>
        nxt_tx_align_en <= '1';
        nxt_cycle_cnt <= INCR_UVEC(cycle_cnt, 1);
        if cycle_cnt = TO_UVEC(c_timeout, c_timeout_w) then
           nxt_state <= s_send_user_data;
           nxt_cycle_cnt <= (others => '0');
        end if;
        nxt_tx_state <= "01";

      when s_send_user_data =>
        nxt_tx_align_en <= '0';
        if tx_align_en_in = '1' then
          nxt_tx_align_en <= '1';
          nxt_state <= s_force_pattern;
        end if;
        nxt_tx_state <= "11";

      when s_force_pattern =>
        nxt_tx_align_en <= '1';
        if tx_align_en_in = '0' then
          nxt_tx_align_en <= '0';
          nxt_state <= s_send_user_data;
        end if;
        nxt_tx_state <= "10";

      when others =>
        nxt_state <= s_init_send_pattern;
    end case;
  end process;
end rtl;
