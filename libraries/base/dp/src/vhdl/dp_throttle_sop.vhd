-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Throttle snk_out.ready to provide a parallel sink with data blocks at a
--   consistent, regular interval
-- Description:
-- . g_period is

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_throttle_sop is
  generic (
    g_period : natural
  );
  port (
    rst      : in  std_logic;
    clk      : in  std_logic;

    snk_out  : out t_dp_siso;
    snk_in   : in  t_dp_sosi
  );
end dp_throttle_sop;

architecture str of dp_throttle_sop is
  constant c_cnt_w : natural := ceil_log2(g_period);

  signal cnt_clr   : std_logic := '0';
  signal cnt_en    : std_logic := '0';
  signal cycle_cnt : std_logic_vector(c_cnt_w - 1 downto 0);

  signal switch_high : std_logic;
  signal switch_low  : std_logic;
  signal switch_out  : std_logic;
begin
  snk_out.xon <= '1';

  u_common_counter : entity common_lib.common_counter
  generic map (
    g_width     => c_cnt_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => cnt_clr,
    cnt_en  => cnt_en,
    count   => cycle_cnt
  );

  cnt_en  <= '1' when TO_UINT(cycle_cnt) < g_period - 2 else '0';
  cnt_clr <= snk_in.sop;

  u_common_switch : entity common_lib.common_switch
  generic map (
    g_rst_level    => '1',
    g_priority_lo  => false,
    g_or_high      => true,
    g_and_low      => true
  )
  port map (
    clk         => clk,
    rst         => rst,
    switch_high => switch_high,
    switch_low  => switch_low,
    out_level   => switch_out
  );

  switch_high   <= not cnt_en;
  switch_low    <= snk_in.eop;
  snk_out.ready <= switch_out;
end str;
