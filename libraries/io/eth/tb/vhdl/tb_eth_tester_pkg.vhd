-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: This package contains eth_tester specific constants and functions
--          for use in a test bench
-- Description: See [1]
-- References:
-- . [1] https://support.astron.nl/confluence/display/L2M/L3+SDP+Decision%3A+SDP+Parameter+definitions
--
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use work.eth_tester_pkg.all;

package tb_eth_tester_pkg is
  constant c_eth_tester_eth_dst_mac       : std_logic_vector(47 downto 0) := x"001B217176B9";  -- 001B217176B9 = DOP36-enp2s0
  constant c_eth_tester_ip_dst_addr       : std_logic_vector(31 downto 0) := x"0A6300FE";  -- 0A6300FE = '10.99.0.254' = DOP36-enp2s0
  constant c_eth_tester_udp_dst_port      : std_logic_vector(15 downto 0) := c_eth_tester_udp_port;

  -- Ethernet packet length in octets inclduing eth header and CRC
  function func_eth_tester_eth_packet_length(block_len : natural) return natural;

  -- Ethernet packet lenght on link including c_network_eth_preamble_len and one idle word
  function func_eth_tester_eth_packet_on_link_length(block_len : natural) return natural;
end tb_eth_tester_pkg;

package body tb_eth_tester_pkg is
  function func_eth_tester_eth_packet_length(block_len : natural) return natural is
    constant c_app_len : natural := c_eth_tester_app_hdr_len + block_len;
    constant c_udp_len : natural := c_network_udp_header_len + c_app_len;
    constant c_ip_len  : natural := c_network_ip_header_len + c_udp_len;
    constant c_eth_len : natural := c_network_eth_header_len + c_ip_len + c_network_eth_crc_len;
  begin
    return c_eth_len;
  end func_eth_tester_eth_packet_length;

  function func_eth_tester_eth_packet_on_link_length(block_len : natural) return natural is
  begin
    return c_network_eth_preamble_len + func_eth_tester_eth_packet_length(block_len) + c_word_sz;
  end func_eth_tester_eth_packet_on_link_length;
end tb_eth_tester_pkg;
