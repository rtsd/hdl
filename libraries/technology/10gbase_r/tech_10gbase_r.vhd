--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, technology_lib, common_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity tech_10gbase_r is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_sim            : boolean := false;
    g_sim_level      : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels   : natural := 1
  );
  port (
    -- MM
    mm_clk            : in  std_logic := '0';
    mm_rst            : in  std_logic := '0';
    reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso   : out t_mem_miso;
    reg_ip_arria10_e1sg_phy_10gbase_r_24_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ip_arria10_e1sg_phy_10gbase_r_24_miso     : out t_mem_miso;
    reg_ip_arria10_e2sg_phy_10gbase_r_24_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_ip_arria10_e2sg_phy_10gbase_r_24_miso     : out t_mem_miso;
    -- Transceiver ATX PLL reference clock
    tr_ref_clk_644          : in  std_logic;  -- 644.531250 MHz

    -- XGMII clocks
    clk_156                 : in std_logic;  -- 156.25 MHz
    rst_156                 : in std_logic;

    -- XGMII interface
    xgmii_tx_ready_arr      : out std_logic_vector(g_nof_channels - 1 downto 0);  -- can be used for xon flow control
    xgmii_rx_ready_arr      : out std_logic_vector(g_nof_channels - 1 downto 0);  -- typically leave not connected
    xgmii_tx_dc_arr         : in  t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit
    xgmii_rx_dc_arr         : out t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit

    -- PHY serial IO
    tx_serial_arr           : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_serial_arr           : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
end tech_10gbase_r;

architecture str of tech_10gbase_r is
  constant c_use_technology : boolean := g_sim = false or g_sim_level = 0;
  constant c_use_sim_model  : boolean := not c_use_technology;
begin
  gen_ip_arria10 : if c_use_technology = true and g_technology = c_tech_arria10_proto generate
    u0 : entity work.tech_10gbase_r_arria10
    generic map (g_sim, g_nof_channels)
    port map (tr_ref_clk_644,
              clk_156, rst_156,
              xgmii_tx_ready_arr, xgmii_rx_ready_arr, xgmii_tx_dc_arr, xgmii_rx_dc_arr,
              tx_serial_arr, rx_serial_arr);
  end generate;

  gen_ip_arria10_e3sge3 : if c_use_technology = true and g_technology = c_tech_arria10_e3sge3 generate
    u0 : entity work.tech_10gbase_r_arria10_e3sge3
    generic map (g_sim, g_nof_channels)
    port map (mm_clk, mm_rst,
              reg_ip_arria10_e3sge3_phy_10gbase_r_24_mosi,
              reg_ip_arria10_e3sge3_phy_10gbase_r_24_miso,
              tr_ref_clk_644,
              clk_156, rst_156,
              xgmii_tx_ready_arr, xgmii_rx_ready_arr, xgmii_tx_dc_arr, xgmii_rx_dc_arr,
              tx_serial_arr, rx_serial_arr);
  end generate;

  gen_ip_arria10_e1sg : if c_use_technology = true and g_technology = c_tech_arria10_e1sg generate
    u0 : entity work.tech_10gbase_r_arria10_e1sg
    generic map (g_sim, g_nof_channels)
    port map (mm_clk, mm_rst,
              reg_ip_arria10_e1sg_phy_10gbase_r_24_mosi,
              reg_ip_arria10_e1sg_phy_10gbase_r_24_miso,
              tr_ref_clk_644,
              clk_156, rst_156,
              xgmii_tx_ready_arr, xgmii_rx_ready_arr, xgmii_tx_dc_arr, xgmii_rx_dc_arr,
              tx_serial_arr, rx_serial_arr);
  end generate;

  gen_ip_arria10_e2sg : if c_use_technology = true and g_technology = c_tech_arria10_e2sg generate
    u0 : entity work.tech_10gbase_r_arria10_e2sg
    generic map (g_sim, g_nof_channels)
    port map (mm_clk, mm_rst,
              reg_ip_arria10_e2sg_phy_10gbase_r_24_mosi,
              reg_ip_arria10_e2sg_phy_10gbase_r_24_miso,
              tr_ref_clk_644,
              clk_156, rst_156,
              xgmii_tx_ready_arr, xgmii_rx_ready_arr, xgmii_tx_dc_arr, xgmii_rx_dc_arr,
              tx_serial_arr, rx_serial_arr);
  end generate;

  gem_sim_10gbase_r : if c_use_sim_model = true generate
    u0 : entity work.sim_10gbase_r
    generic map (g_sim, g_nof_channels)
    port map (tr_ref_clk_644,
              clk_156, rst_156,
              xgmii_tx_ready_arr, xgmii_rx_ready_arr, xgmii_tx_dc_arr, xgmii_rx_dc_arr,
              tx_serial_arr, rx_serial_arr);
  end generate;
end str;
