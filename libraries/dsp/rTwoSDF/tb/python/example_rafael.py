###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Python example Rafael

    
   
   Remark:
   
"""


###############################################################################
# System imports
from common import *

c_nof_points = 16
c_in_dat_w   = 8
c_out_dat_w  = 6
c_lsb_w      = 1

in_data_a = []
in_data_b = []
for i in range(c_nof_points):
    in_data_a.append(i)
    in_data_b.append(-i)

mult_data = []
for i in range(c_nof_points):  
    mult_data.append(in_data_a[i] * in_data_b[i])  

out_data = []
for i in range(c_nof_points): 
  out_data.append(int_requantize(inp=int(mult_data[i]), inp_w=c_in_dat_w, outp_w=c_out_dat_w, lsb_w=c_lsb_w, lsb_round=True, msb_clip=False, gain_w=0))   
  
for i in range(c_nof_points):
  print "in " + str(i) + "= " + str(in_data_a[i]) + " out " + str(i) + "= " + str(out_data[i])
            
