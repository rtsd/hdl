-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:
--   Core design for Lofar2 SDP station
-- Description:
--   Unb2c version for lab testing, using generic sdp_station.vhd for LOFAR2 SDP application.
-------------------------------------------------------------------------------

library IEEE, common_lib, diag_lib, dp_lib;
library unb2c_board_lib, tech_pll_lib, tech_jesd204b_lib;
library wpfb_lib, nw_10gbe_lib, eth_lib, lofar2_sdp_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use common_lib.common_field_pkg.all;
  use unb2c_board_lib.unb2c_board_pkg.all;
  use unb2c_board_lib.unb2c_board_peripherals_pkg.all;
  use diag_lib.diag_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use wpfb_lib.wpfb_pkg.all;
  use eth_lib.eth_pkg.all;
  use lofar2_sdp_lib.sdp_pkg.all;
  use work.lofar2_unb2c_sdp_station_pkg.all;

entity lofar2_unb2c_sdp_station is
  generic (
    g_design_name            : string  := "lofar2_unb2c_sdp_station";
    g_design_note            : string  := "UNUSED";
    g_sim                    : boolean := false;  -- Overridden by TB
    g_sim_unb_nr             : natural := 0;
    g_sim_node_nr            : natural := 0;
    g_stamp_date             : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time             : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id            : string  := "";  -- revision ID     -- set by QSF
    g_factory_image          : boolean := false;
    g_protect_addr_range     : boolean := false;
    g_wpfb                   : t_wpfb  := c_sdp_wpfb_subbands;
    g_bsn_nof_clk_per_sync   : natural := c_sdp_N_clk_per_sync;  -- Default 200M, overide for short simulation
    g_scope_selected_subband : natural := 0
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGIN     : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK        : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines

    -- front transceivers QSFP0 for Ring.
    QSFP_0_RX     : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_0_TX     : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);

    -- front transceivers QSFP1 for 10GbE output to CEP.
    QSFP_1_RX     : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_1_TX     : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);

    -- ring transceivers
    -- . Using qsfp bus width also for ring interfaces
    RING_0_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    RING_0_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
    RING_1_RX    : in    std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    RING_1_TX    : out   std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);

    -- LEDs
    QSFP_LED     : out   std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0);

     -- back transceivers (Note: numbered from 0)
    JESD204B_SERIAL_DATA       : in    std_logic_vector(c_sdp_S_pn - 1 downto 0) := (others => '0');
                                                  -- c_unb2c_board_nof_tr_jesd204b = c_sdp_S_pn = 12
                                                  -- Connect to the BCK_RX pins in the top wrapper
    JESD204B_REFCLK            : in    std_logic := '0';  -- Connect to BCK_REF_CLK pin in the top level wrapper

    -- jesd204b syncronization signals
    JESD204B_SYSREF            : in    std_logic := '0';
    JESD204B_SYNC_N            : out   std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0)
                                       -- c_unb2c_board_nof_sync_jesd204b = c_sdp_N_sync_jesd = 4
  );
end lofar2_unb2c_sdp_station;

architecture str of lofar2_unb2c_sdp_station is
  -- Revision parameters
  constant c_revision_select        : t_lofar2_unb2c_sdp_station_config := func_sel_revision_rec(g_design_name);

  -- Firmware version x.y
  constant c_fw_version             : t_unb2c_board_fw_version := (2, 0);
  constant c_mm_clk_freq            : natural := c_unb2c_board_mm_clk_freq_100M;

  -- 10 GbE Interface
  constant c_nof_streams_qsfp       : natural := c_unb2c_board_tr_qsfp.nof_bus * c_quad;

  signal gn_id    : std_logic_vector(c_sdp_W_gn_id - 1 downto 0);

  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic := '0';

  signal dp_pps                     : std_logic;
  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_copi               : t_mem_copi := c_mem_copi_rst;
  signal reg_wdi_cipo               : t_mem_cipo := c_mem_cipo_rst;

  -- PPSH
  signal reg_ppsh_copi              : t_mem_copi := c_mem_copi_rst;
  signal reg_ppsh_cipo              : t_mem_cipo := c_mem_cipo_rst;

  -- UniBoard system info
  signal reg_unb_system_info_copi   : t_mem_copi := c_mem_copi_rst;
  signal reg_unb_system_info_cipo   : t_mem_cipo := c_mem_cipo_rst;
  signal rom_unb_system_info_copi   : t_mem_copi := c_mem_copi_rst;
  signal rom_unb_system_info_cipo   : t_mem_cipo := c_mem_cipo_rst;

  -- FPGA sensors
  signal reg_fpga_temp_sens_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_fpga_temp_sens_cipo     : t_mem_cipo := c_mem_cipo_rst;
  signal reg_fpga_voltage_sens_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_fpga_voltage_sens_cipo  : t_mem_cipo := c_mem_cipo_rst;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_copi             : t_mem_copi := c_mem_copi_rst;  -- ETH TSE MAC registers
  signal eth1g_tse_cipo             : t_mem_cipo := c_mem_cipo_rst;
  signal eth1g_reg_copi             : t_mem_copi := c_mem_copi_rst;  -- ETH control and status registers
  signal eth1g_reg_cipo             : t_mem_cipo := c_mem_cipo_rst;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_copi             : t_mem_copi := c_mem_copi_rst;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_cipo             : t_mem_cipo := c_mem_cipo_rst;

  -- EPCS read
  signal reg_dpmm_data_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_dpmm_data_cipo         : t_mem_cipo := c_mem_cipo_rst;
  signal reg_dpmm_ctrl_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_dpmm_ctrl_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- EPCS write
  signal reg_mmdp_data_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_mmdp_data_cipo         : t_mem_cipo := c_mem_cipo_rst;
  signal reg_mmdp_ctrl_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_mmdp_ctrl_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- EPCS status/control
  signal reg_epcs_copi              : t_mem_copi := c_mem_copi_rst;
  signal reg_epcs_cipo              : t_mem_cipo := c_mem_cipo_rst;

  -- Remote Update
  signal reg_remu_copi              : t_mem_copi := c_mem_copi_rst;
  signal reg_remu_cipo              : t_mem_cipo := c_mem_cipo_rst;

  -- Scrap ram
  signal ram_scrap_copi             : t_mem_copi := c_mem_copi_rst;
  signal ram_scrap_cipo             : t_mem_cipo := c_mem_cipo_rst;

  ----------------------------------------------
  -- AIT
  ----------------------------------------------
  -- JESD
  signal jesd204b_copi              : t_mem_copi := c_mem_copi_rst;
  signal jesd204b_cipo              : t_mem_cipo := c_mem_cipo_rst;

  -- JESD control
  signal jesd_ctrl_copi             : t_mem_copi := c_mem_copi_rst;
  signal jesd_ctrl_cipo             : t_mem_cipo := c_mem_cipo_rst;

  -- Shiftram (applies per-antenna delay)
  signal reg_dp_shiftram_copi       : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_shiftram_cipo       : t_mem_cipo := c_mem_cipo_rst;

  -- bsn source
  signal reg_bsn_source_v2_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_source_v2_cipo     : t_mem_cipo := c_mem_cipo_rst;

  -- bsn scheduler
  signal reg_bsn_scheduler_wg_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_scheduler_wg_cipo  : t_mem_cipo := c_mem_cipo_rst;

  -- WG
  signal reg_wg_copi                : t_mem_copi := c_mem_copi_rst;
  signal reg_wg_cipo                : t_mem_cipo := c_mem_cipo_rst;
  signal ram_wg_copi                : t_mem_copi := c_mem_copi_rst;
  signal ram_wg_cipo                : t_mem_cipo := c_mem_cipo_rst;

  -- BSN MONITOR
  signal reg_bsn_monitor_input_copi : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_input_cipo : t_mem_cipo := c_mem_cipo_rst;

  -- Data buffer bsn
  signal ram_diag_data_buf_bsn_copi : t_mem_copi := c_mem_copi_rst;
  signal ram_diag_data_buf_bsn_cipo : t_mem_cipo := c_mem_cipo_rst;
  signal reg_diag_data_buf_bsn_copi : t_mem_copi := c_mem_copi_rst;
  signal reg_diag_data_buf_bsn_cipo : t_mem_cipo := c_mem_cipo_rst;

  -- ST Histogram
  signal ram_st_histogram_copi      : t_mem_copi := c_mem_copi_rst;
  signal ram_st_histogram_cipo      : t_mem_cipo := c_mem_cipo_rst;

  -- Aduh statistics monitor
  signal reg_aduh_monitor_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_aduh_monitor_cipo      : t_mem_cipo := c_mem_cipo_rst;

  ----------------------------------------------
  -- FSUB
  ----------------------------------------------
  -- Subband statistics
  signal ram_st_sst_copi            : t_mem_copi := c_mem_copi_rst;
  signal ram_st_sst_cipo            : t_mem_cipo := c_mem_cipo_rst;

  -- Spectral Inversion
  signal reg_si_copi                : t_mem_copi := c_mem_copi_rst;
  signal reg_si_cipo                : t_mem_cipo := c_mem_cipo_rst;

  -- Filter coefficients
  signal ram_fil_coefs_copi         : t_mem_copi := c_mem_copi_rst;
  signal ram_fil_coefs_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- Equalizer gains
  signal ram_equalizer_gains_copi   : t_mem_copi := c_mem_copi_rst;
  signal ram_equalizer_gains_cipo   : t_mem_cipo := c_mem_cipo_rst;
  signal ram_equalizer_gains_cross_copi : t_mem_copi := c_mem_copi_rst;
  signal ram_equalizer_gains_cross_cipo : t_mem_cipo := c_mem_cipo_rst;

  -- DP Selector
  signal reg_dp_selector_copi       : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_selector_cipo       : t_mem_cipo := c_mem_cipo_rst;

  ----------------------------------------------
  -- SDP Info
  ----------------------------------------------
  signal reg_sdp_info_copi          : t_mem_copi := c_mem_copi_rst;
  signal reg_sdp_info_cipo          : t_mem_cipo := c_mem_cipo_rst;

  ----------------------------------------------
  -- RING Info
  ----------------------------------------------
  signal reg_ring_info_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_ring_info_cipo         : t_mem_cipo := c_mem_cipo_rst;

  ----------------------------------------------
  -- XSUB
  ----------------------------------------------

  -- crosslets_info
  signal reg_crosslets_info_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_crosslets_info_cipo     : t_mem_cipo := c_mem_cipo_rst;

  -- crosslets_info
  signal reg_nof_crosslets_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_nof_crosslets_cipo      : t_mem_cipo := c_mem_cipo_rst;

  -- bsn_scheduler_xsub
  signal reg_bsn_sync_scheduler_xsub_copi : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_sync_scheduler_xsub_cipo : t_mem_cipo := c_mem_cipo_rst;

  -- st_xsq
  signal ram_st_xsq_copi             : t_mem_copi := c_mem_copi_rst;
  signal ram_st_xsq_cipo             : t_mem_cipo := c_mem_cipo_rst;

  ----------------------------------------------
  -- BF
  ----------------------------------------------
  -- Beamlet Subband Select
  signal ram_ss_ss_wide_copi        : t_mem_copi := c_mem_copi_rst;
  signal ram_ss_ss_wide_cipo        : t_mem_cipo := c_mem_cipo_rst;

  -- Local BF bf weights
  signal ram_bf_weights_copi        : t_mem_copi := c_mem_copi_rst;
  signal ram_bf_weights_cipo        : t_mem_cipo := c_mem_cipo_rst;

  -- BF bsn aligner_v2
  signal reg_bsn_align_v2_bf_copi   : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_align_v2_bf_cipo   : t_mem_cipo := c_mem_cipo_rst;

  -- BF bsn aligner_v2 bsn monitors
  signal reg_bsn_monitor_v2_rx_align_bf_copi : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_rx_align_bf_cipo : t_mem_cipo := c_mem_cipo_rst;
  signal reg_bsn_monitor_v2_aligned_bf_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_aligned_bf_cipo  : t_mem_cipo := c_mem_cipo_rst;

  -- mms_dp_scale Scale Beamlets
  signal reg_bf_scale_copi          : t_mem_copi := c_mem_copi_rst;
  signal reg_bf_scale_cipo          : t_mem_cipo := c_mem_cipo_rst;

  -- Beamlet Data Output header fields
  signal reg_hdr_dat_copi           : t_mem_copi := c_mem_copi_rst;
  signal reg_hdr_dat_cipo           : t_mem_cipo := c_mem_cipo_rst;
  signal reg_bdo_destinations_copi  : t_mem_copi := c_mem_mosi_rst;
  signal reg_bdo_destinations_cipo  : t_mem_cipo := c_mem_cipo_rst;

  -- Beamlet Data Output xonoff
  signal reg_dp_xonoff_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_xonoff_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- Beamlet Statistics (BST)
  signal ram_st_bst_copi            : t_mem_copi := c_mem_copi_rst;
  signal ram_st_bst_cipo            : t_mem_cipo := c_mem_cipo_rst;

  -- BF ring lane info
  signal  reg_ring_lane_info_bf_copi                 : t_mem_copi := c_mem_copi_rst;
  signal  reg_ring_lane_info_bf_cipo                 : t_mem_cipo := c_mem_cipo_rst;

  -- BF ring bsn monitor rx
  signal  reg_bsn_monitor_v2_ring_rx_bf_copi         : t_mem_copi := c_mem_copi_rst;
  signal  reg_bsn_monitor_v2_ring_rx_bf_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- BF ring bsn monitor tx
  signal  reg_bsn_monitor_v2_ring_tx_bf_copi         : t_mem_copi := c_mem_copi_rst;
  signal  reg_bsn_monitor_v2_ring_tx_bf_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- BF ring validate err
  signal  reg_dp_block_validate_err_bf_copi          : t_mem_copi := c_mem_copi_rst;
  signal  reg_dp_block_validate_err_bf_cipo          : t_mem_cipo := c_mem_cipo_rst;

  -- BF ring bsn at sync
  signal  reg_dp_block_validate_bsn_at_sync_bf_copi  : t_mem_copi := c_mem_copi_rst;
  signal  reg_dp_block_validate_bsn_at_sync_bf_cipo  : t_mem_cipo := c_mem_cipo_rst;
  ----------------------------------------------
  -- SST
  ----------------------------------------------
  -- Statistics Enable
  signal reg_stat_enable_sst_copi       : t_mem_copi;
  signal reg_stat_enable_sst_cipo       : t_mem_cipo;

  -- Statistics header info
  signal reg_stat_hdr_dat_sst_copi      : t_mem_copi;
  signal reg_stat_hdr_dat_sst_cipo      : t_mem_cipo;

  -- SST UDP offload bsn monitor
  signal  reg_bsn_monitor_v2_sst_offload_copi : t_mem_copi;
  signal  reg_bsn_monitor_v2_sst_offload_cipo : t_mem_cipo;
  ----------------------------------------------
  -- XST
  ----------------------------------------------
  -- Statistics Enable
  signal reg_stat_enable_xst_copi    : t_mem_copi;
  signal reg_stat_enable_xst_cipo    : t_mem_cipo;

  -- Statistics header info
  signal reg_stat_hdr_dat_xst_copi   : t_mem_copi;
  signal reg_stat_hdr_dat_xst_cipo   : t_mem_cipo;

  -- XST bsn aligner_v2
  signal  reg_bsn_align_v2_xsub_copi : t_mem_copi;
  signal  reg_bsn_align_v2_xsub_cipo : t_mem_cipo;

  -- XST bsn aligner_v2 bsn monitors
  signal reg_bsn_monitor_v2_rx_align_xsub_copi : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_rx_align_xsub_cipo : t_mem_cipo := c_mem_cipo_rst;
  signal reg_bsn_monitor_v2_aligned_xsub_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_aligned_xsub_cipo  : t_mem_cipo := c_mem_cipo_rst;

  -- XST UDP offload bsn monitor
  signal  reg_bsn_monitor_v2_xst_offload_copi         : t_mem_copi;
  signal  reg_bsn_monitor_v2_xst_offload_cipo         : t_mem_cipo;

  -- XST ring lane info
  signal  reg_ring_lane_info_xst_copi                 : t_mem_copi;
  signal  reg_ring_lane_info_xst_cipo                 : t_mem_cipo;

  -- XST ring bsn monitor rx
  signal  reg_bsn_monitor_v2_ring_rx_xst_copi         : t_mem_copi;
  signal  reg_bsn_monitor_v2_ring_rx_xst_cipo         : t_mem_cipo;

  -- XST ring bsn monitor tx
  signal  reg_bsn_monitor_v2_ring_tx_xst_copi         : t_mem_copi;
  signal  reg_bsn_monitor_v2_ring_tx_xst_cipo         : t_mem_cipo;

  -- XST ring validate err
  signal  reg_dp_block_validate_err_xst_copi          : t_mem_copi;
  signal  reg_dp_block_validate_err_xst_cipo          : t_mem_cipo;

  -- XST ring bsn at sync
  signal  reg_dp_block_validate_bsn_at_sync_xst_copi  : t_mem_copi;
  signal  reg_dp_block_validate_bsn_at_sync_xst_cipo  : t_mem_cipo;

  -- XST ring MAC10G
  signal  reg_tr_10GbE_mac_copi                       : t_mem_copi;
  signal  reg_tr_10GbE_mac_cipo                       : t_mem_cipo;

  -- XST ring ETH10G
  signal  reg_tr_10GbE_eth10g_copi                    : t_mem_copi;
  signal  reg_tr_10GbE_eth10g_cipo                    : t_mem_cipo;
  ----------------------------------------------
  -- BST
  ----------------------------------------------
  -- Statistics Enable
  signal reg_stat_enable_bst_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_stat_enable_bst_cipo      : t_mem_cipo := c_mem_cipo_rst;

  -- Statistics header info
  signal reg_stat_hdr_dat_bst_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_stat_hdr_dat_bst_cipo     : t_mem_cipo := c_mem_cipo_rst;

  -- BST UDP offload bsn monitor
  signal  reg_bsn_monitor_v2_bst_offload_copi : t_mem_copi;
  signal  reg_bsn_monitor_v2_bst_offload_cipo : t_mem_cipo;

  -- Beamlet output bsn monitor
  signal  reg_bsn_monitor_v2_beamlet_output_copi : t_mem_copi;
  signal  reg_bsn_monitor_v2_beamlet_output_cipo : t_mem_cipo;
  ----------------------------------------------
  -- UDP Offload
  ----------------------------------------------
  signal udp_tx_sosi_arr            : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_sosi_rst);
  signal udp_tx_siso_arr            : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_siso_rdy);

  ----------------------------------------------
  -- 10 GbE
  ----------------------------------------------
  signal reg_nw_10GbE_mac_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_nw_10GbE_mac_cipo      : t_mem_cipo := c_mem_cipo_rst;

  signal reg_nw_10GbE_eth10g_copi   : t_mem_copi := c_mem_copi_rst;
  signal reg_nw_10GbE_eth10g_cipo   : t_mem_cipo := c_mem_cipo_rst;

  -- 10GbE
  signal i_QSFP_TX : t_unb2c_board_qsfp_bus_2arr(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0) :=
                                                                                            (others => (others => '0'));
  signal i_QSFP_RX : t_unb2c_board_qsfp_bus_2arr(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0) :=
                                                                                            (others => (others => '0'));

  signal unb2_board_front_io_serial_tx_arr : std_logic_vector(c_nof_streams_qsfp - 1 downto 0) := (others => '0');
  signal unb2_board_front_io_serial_rx_arr : std_logic_vector(c_nof_streams_qsfp - 1 downto 0) := (others => '0');

  signal this_bck_id                       : std_logic_vector(c_unb2c_board_nof_uniboard_w - 1 downto 0);
  signal this_chip_id                      : std_logic_vector(c_unb2c_board_nof_chip_w - 1 downto 0);

  -- QSFP LEDS
  signal qsfp_green_led_arr                : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr                  : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);

  signal unb2_board_qsfp_leds_tx_sosi_arr : t_dp_sosi_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_sosi_rst);
  signal unb2_board_qsfp_leds_tx_siso_arr : t_dp_siso_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_siso_rst);
  signal unb2_board_qsfp_leds_rx_sosi_arr : t_dp_sosi_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_sosi_rst);
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb2c_board_lib.ctrl_unb2c_board
  generic map (
    g_sim                     => g_sim,
    g_design_name             => g_design_name,
    g_design_note             => g_design_note,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_revision_id             => g_revision_id,
    g_fw_version              => c_fw_version,
    g_mm_clk_freq             => c_mm_clk_freq,
    g_eth_clk_freq            => c_unb2c_board_eth_clk_freq_125M,
    g_aux                     => c_unb2c_board_aux,
    g_factory_image           => g_factory_image,
    g_protect_addr_range      => g_protect_addr_range,
    g_dp_clk_freq             => c_unb2c_board_ext_clk_freq_200M,
    g_dp_clk_use_pll          => false,
    g_udp_offload             => true,
    g_udp_offload_nof_streams => c_eth_nof_udp_ports
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,  -- Can be external 200MHz, or PLL generated
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    this_chip_id             => this_chip_id,
    this_bck_id              => this_bck_id,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_copi,
    reg_remu_miso            => reg_remu_cipo,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_copi,
    reg_dpmm_data_miso       => reg_dpmm_data_cipo,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_copi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_cipo,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_copi,
    reg_mmdp_data_miso       => reg_mmdp_data_cipo,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_copi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_cipo,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_copi,
    reg_epcs_miso            => reg_epcs_cipo,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_copi,
    reg_wdi_miso             => reg_wdi_cipo,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_copi,
    reg_unb_system_info_miso => reg_unb_system_info_cipo,
    rom_unb_system_info_mosi => rom_unb_system_info_copi,
    rom_unb_system_info_miso => rom_unb_system_info_cipo,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_copi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_cipo,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_copi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_cipo,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_copi,
    reg_ppsh_miso            => reg_ppsh_cipo,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_copi,
    eth1g_tse_miso           => eth1g_tse_cipo,
    eth1g_reg_mosi           => eth1g_reg_copi,
    eth1g_reg_miso           => eth1g_reg_cipo,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_copi,
    eth1g_ram_miso           => eth1g_ram_cipo,

    -- eth1g UDP streaming
    udp_tx_sosi_arr          => udp_tx_sosi_arr,
    udp_tx_siso_arr          => udp_tx_siso_arr,

    ram_scrap_mosi           => ram_scrap_copi,
    ram_scrap_miso           => ram_scrap_cipo,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,

    -- . 1GbE Control Interface
    ETH_CLK                  => ETH_CLK(0),
    ETH_SGIN                 => ETH_SGIN(0),
    ETH_SGOUT                => ETH_SGOUT(0)
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_lofar2_unb2c_sdp_station
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
   )
  port map(
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- mm interfaces for control
    reg_wdi_copi                => reg_wdi_copi,
    reg_wdi_cipo                => reg_wdi_cipo,
    reg_unb_system_info_copi    => reg_unb_system_info_copi,
    reg_unb_system_info_cipo    => reg_unb_system_info_cipo,
    rom_unb_system_info_copi    => rom_unb_system_info_copi,
    rom_unb_system_info_cipo    => rom_unb_system_info_cipo,
    reg_fpga_temp_sens_copi     => reg_fpga_temp_sens_copi,
    reg_fpga_temp_sens_cipo     => reg_fpga_temp_sens_cipo,
    reg_fpga_voltage_sens_copi  => reg_fpga_voltage_sens_copi,
    reg_fpga_voltage_sens_cipo  => reg_fpga_voltage_sens_cipo,
    reg_ppsh_copi               => reg_ppsh_copi,
    reg_ppsh_cipo               => reg_ppsh_cipo,
    eth1g_mm_rst                => eth1g_mm_rst,
    eth1g_tse_copi              => eth1g_tse_copi,
    eth1g_tse_cipo              => eth1g_tse_cipo,
    eth1g_reg_copi              => eth1g_reg_copi,
    eth1g_reg_cipo              => eth1g_reg_cipo,
    eth1g_reg_interrupt         => eth1g_reg_interrupt,
    eth1g_ram_copi              => eth1g_ram_copi,
    eth1g_ram_cipo              => eth1g_ram_cipo,
    reg_dpmm_data_copi          => reg_dpmm_data_copi,
    reg_dpmm_data_cipo          => reg_dpmm_data_cipo,
    reg_dpmm_ctrl_copi          => reg_dpmm_ctrl_copi,
    reg_dpmm_ctrl_cipo          => reg_dpmm_ctrl_cipo,
    reg_mmdp_data_copi          => reg_mmdp_data_copi,
    reg_mmdp_data_cipo          => reg_mmdp_data_cipo,
    reg_mmdp_ctrl_copi          => reg_mmdp_ctrl_copi,
    reg_mmdp_ctrl_cipo          => reg_mmdp_ctrl_cipo,
    reg_epcs_copi               => reg_epcs_copi,
    reg_epcs_cipo               => reg_epcs_cipo,
    reg_remu_copi               => reg_remu_copi,
    reg_remu_cipo               => reg_remu_cipo,

    -- mm buses for signal flow blocks
    -- Jesd ip status/control
    jesd204b_copi                                => jesd204b_copi,
    jesd204b_cipo                                => jesd204b_cipo,
    jesd_ctrl_copi                               => jesd_ctrl_copi,
    jesd_ctrl_cipo                               => jesd_ctrl_cipo,
    reg_dp_shiftram_copi                         => reg_dp_shiftram_copi,
    reg_dp_shiftram_cipo                         => reg_dp_shiftram_cipo,
    reg_bsn_source_v2_copi                       => reg_bsn_source_v2_copi,
    reg_bsn_source_v2_cipo                       => reg_bsn_source_v2_cipo,
    reg_bsn_scheduler_copi                       => reg_bsn_scheduler_wg_copi,
    reg_bsn_scheduler_cipo                       => reg_bsn_scheduler_wg_cipo,
    reg_wg_copi                                  => reg_wg_copi,
    reg_wg_cipo                                  => reg_wg_cipo,
    ram_wg_copi                                  => ram_wg_copi,
    ram_wg_cipo                                  => ram_wg_cipo,
    reg_bsn_monitor_input_copi                   => reg_bsn_monitor_input_copi,
    reg_bsn_monitor_input_cipo                   => reg_bsn_monitor_input_cipo,
    ram_diag_data_buf_bsn_copi                   => ram_diag_data_buf_bsn_copi,
    ram_diag_data_buf_bsn_cipo                   => ram_diag_data_buf_bsn_cipo,
    reg_diag_data_buf_bsn_copi                   => reg_diag_data_buf_bsn_copi,
    reg_diag_data_buf_bsn_cipo                   => reg_diag_data_buf_bsn_cipo,
    ram_st_histogram_copi                        => ram_st_histogram_copi,
    ram_st_histogram_cipo                        => ram_st_histogram_cipo,
    reg_aduh_monitor_copi                        => reg_aduh_monitor_copi,
    reg_aduh_monitor_cipo                        => reg_aduh_monitor_cipo,
    ram_st_sst_copi                              => ram_st_sst_copi,
    ram_st_sst_cipo                              => ram_st_sst_cipo,
    ram_fil_coefs_copi                           => ram_fil_coefs_copi,
    ram_fil_coefs_cipo                           => ram_fil_coefs_cipo,
    reg_si_copi                                  => reg_si_copi,
    reg_si_cipo                                  => reg_si_cipo,
    ram_equalizer_gains_copi                     => ram_equalizer_gains_copi,
    ram_equalizer_gains_cipo                     => ram_equalizer_gains_cipo,
    ram_equalizer_gains_cross_copi               => ram_equalizer_gains_cross_copi,
    ram_equalizer_gains_cross_cipo               => ram_equalizer_gains_cross_cipo,
    reg_dp_selector_copi                         => reg_dp_selector_copi,
    reg_dp_selector_cipo                         => reg_dp_selector_cipo,
    reg_sdp_info_copi                            => reg_sdp_info_copi,
    reg_sdp_info_cipo                            => reg_sdp_info_cipo,
    reg_ring_info_copi                           => reg_ring_info_copi,
    reg_ring_info_cipo                           => reg_ring_info_cipo,
    ram_ss_ss_wide_copi                          => ram_ss_ss_wide_copi,
    ram_ss_ss_wide_cipo                          => ram_ss_ss_wide_cipo,
    ram_bf_weights_copi                          => ram_bf_weights_copi,
    ram_bf_weights_cipo                          => ram_bf_weights_cipo,
    reg_bf_scale_copi                            => reg_bf_scale_copi,
    reg_bf_scale_cipo                            => reg_bf_scale_cipo,
    reg_hdr_dat_copi                             => reg_hdr_dat_copi,
    reg_hdr_dat_cipo                             => reg_hdr_dat_cipo,
    reg_bdo_destinations_copi                    => reg_bdo_destinations_copi,
    reg_bdo_destinations_cipo                    => reg_bdo_destinations_cipo,
    reg_dp_xonoff_copi                           => reg_dp_xonoff_copi,
    reg_dp_xonoff_cipo                           => reg_dp_xonoff_cipo,
    ram_st_bst_copi                              => ram_st_bst_copi,
    ram_st_bst_cipo                              => ram_st_bst_cipo,
    reg_bsn_align_v2_bf_copi                     => reg_bsn_align_v2_bf_copi,
    reg_bsn_align_v2_bf_cipo                     => reg_bsn_align_v2_bf_cipo,
    reg_bsn_monitor_v2_rx_align_bf_copi          => reg_bsn_monitor_v2_rx_align_bf_copi,
    reg_bsn_monitor_v2_rx_align_bf_cipo          => reg_bsn_monitor_v2_rx_align_bf_cipo,
    reg_bsn_monitor_v2_aligned_bf_copi           => reg_bsn_monitor_v2_aligned_bf_copi,
    reg_bsn_monitor_v2_aligned_bf_cipo           => reg_bsn_monitor_v2_aligned_bf_cipo,
    reg_ring_lane_info_bf_copi                   => reg_ring_lane_info_bf_copi,
    reg_ring_lane_info_bf_cipo                   => reg_ring_lane_info_bf_cipo,
    reg_bsn_monitor_v2_ring_rx_bf_copi           => reg_bsn_monitor_v2_ring_rx_bf_copi,
    reg_bsn_monitor_v2_ring_rx_bf_cipo           => reg_bsn_monitor_v2_ring_rx_bf_cipo,
    reg_bsn_monitor_v2_ring_tx_bf_copi           => reg_bsn_monitor_v2_ring_tx_bf_copi,
    reg_bsn_monitor_v2_ring_tx_bf_cipo           => reg_bsn_monitor_v2_ring_tx_bf_cipo,
    reg_dp_block_validate_err_bf_copi            => reg_dp_block_validate_err_bf_copi,
    reg_dp_block_validate_err_bf_cipo            => reg_dp_block_validate_err_bf_cipo,
    reg_dp_block_validate_bsn_at_sync_bf_copi    => reg_dp_block_validate_bsn_at_sync_bf_copi,
    reg_dp_block_validate_bsn_at_sync_bf_cipo    => reg_dp_block_validate_bsn_at_sync_bf_cipo,
    reg_nw_10GbE_mac_copi                        => reg_nw_10GbE_mac_copi,
    reg_nw_10GbE_mac_cipo                        => reg_nw_10GbE_mac_cipo,
    reg_nw_10GbE_eth10g_copi                     => reg_nw_10GbE_eth10g_copi,
    reg_nw_10GbE_eth10g_cipo                     => reg_nw_10GbE_eth10g_cipo,
    ram_scrap_copi                               => ram_scrap_copi,
    ram_scrap_cipo                               => ram_scrap_cipo,
    reg_stat_enable_sst_copi                     => reg_stat_enable_sst_copi,
    reg_stat_enable_sst_cipo                     => reg_stat_enable_sst_cipo,
    reg_stat_hdr_dat_sst_copi                    => reg_stat_hdr_dat_sst_copi,
    reg_stat_hdr_dat_sst_cipo                    => reg_stat_hdr_dat_sst_cipo,
    reg_stat_enable_xst_copi                     => reg_stat_enable_xst_copi,
    reg_stat_enable_xst_cipo                     => reg_stat_enable_xst_cipo,
    reg_stat_hdr_dat_xst_copi                    => reg_stat_hdr_dat_xst_copi,
    reg_stat_hdr_dat_xst_cipo                    => reg_stat_hdr_dat_xst_cipo,
    reg_stat_enable_bst_copi                     => reg_stat_enable_bst_copi,
    reg_stat_enable_bst_cipo                     => reg_stat_enable_bst_cipo,
    reg_stat_hdr_dat_bst_copi                    => reg_stat_hdr_dat_bst_copi,
    reg_stat_hdr_dat_bst_cipo                    => reg_stat_hdr_dat_bst_cipo,
    reg_crosslets_info_copi                      => reg_crosslets_info_copi,
    reg_crosslets_info_cipo                      => reg_crosslets_info_cipo,
    reg_nof_crosslets_copi                       => reg_nof_crosslets_copi,
    reg_nof_crosslets_cipo                       => reg_nof_crosslets_cipo,
    reg_bsn_sync_scheduler_xsub_copi             => reg_bsn_sync_scheduler_xsub_copi,
    reg_bsn_sync_scheduler_xsub_cipo             => reg_bsn_sync_scheduler_xsub_cipo,
    reg_bsn_align_v2_xsub_copi                   => reg_bsn_align_v2_xsub_copi,
    reg_bsn_align_v2_xsub_cipo                   => reg_bsn_align_v2_xsub_cipo,
    reg_bsn_monitor_v2_rx_align_xsub_copi        => reg_bsn_monitor_v2_rx_align_xsub_copi,
    reg_bsn_monitor_v2_rx_align_xsub_cipo        => reg_bsn_monitor_v2_rx_align_xsub_cipo,
    reg_bsn_monitor_v2_aligned_xsub_copi         => reg_bsn_monitor_v2_aligned_xsub_copi,
    reg_bsn_monitor_v2_aligned_xsub_cipo         => reg_bsn_monitor_v2_aligned_xsub_cipo,
    reg_bsn_monitor_v2_xst_offload_copi          => reg_bsn_monitor_v2_xst_offload_copi,
    reg_bsn_monitor_v2_xst_offload_cipo          => reg_bsn_monitor_v2_xst_offload_cipo,
    reg_bsn_monitor_v2_bst_offload_copi          => reg_bsn_monitor_v2_bst_offload_copi,
    reg_bsn_monitor_v2_bst_offload_cipo          => reg_bsn_monitor_v2_bst_offload_cipo,
    reg_bsn_monitor_v2_beamlet_output_copi       => reg_bsn_monitor_v2_beamlet_output_copi,
    reg_bsn_monitor_v2_beamlet_output_cipo       => reg_bsn_monitor_v2_beamlet_output_cipo,
    reg_bsn_monitor_v2_sst_offload_copi          => reg_bsn_monitor_v2_sst_offload_copi,
    reg_bsn_monitor_v2_sst_offload_cipo          => reg_bsn_monitor_v2_sst_offload_cipo,
    reg_ring_lane_info_xst_copi                  => reg_ring_lane_info_xst_copi,
    reg_ring_lane_info_xst_cipo                  => reg_ring_lane_info_xst_cipo,
    reg_bsn_monitor_v2_ring_rx_xst_copi          => reg_bsn_monitor_v2_ring_rx_xst_copi,
    reg_bsn_monitor_v2_ring_rx_xst_cipo          => reg_bsn_monitor_v2_ring_rx_xst_cipo,
    reg_bsn_monitor_v2_ring_tx_xst_copi          => reg_bsn_monitor_v2_ring_tx_xst_copi,
    reg_bsn_monitor_v2_ring_tx_xst_cipo          => reg_bsn_monitor_v2_ring_tx_xst_cipo,
    reg_dp_block_validate_err_xst_copi           => reg_dp_block_validate_err_xst_copi,
    reg_dp_block_validate_err_xst_cipo           => reg_dp_block_validate_err_xst_cipo,
    reg_dp_block_validate_bsn_at_sync_xst_copi   => reg_dp_block_validate_bsn_at_sync_xst_copi,
    reg_dp_block_validate_bsn_at_sync_xst_cipo   => reg_dp_block_validate_bsn_at_sync_xst_cipo,
    reg_tr_10GbE_mac_copi                        => reg_tr_10GbE_mac_copi,
    reg_tr_10GbE_mac_cipo                        => reg_tr_10GbE_mac_cipo,
    reg_tr_10GbE_eth10g_copi                     => reg_tr_10GbE_eth10g_copi,
    reg_tr_10GbE_eth10g_cipo                     => reg_tr_10GbE_eth10g_cipo,
    ram_st_xsq_copi                              => ram_st_xsq_copi,
    ram_st_xsq_cipo                              => ram_st_xsq_cipo
  );

  -- Use full 8 bit gn_id = ID
  gn_id <= ID;

  -----------------------------------------------------------------------------
  -- sdp nodes
  -----------------------------------------------------------------------------
  u_sdp_station : entity lofar2_sdp_lib.sdp_station
  generic map (
    g_sim                       => g_sim,
    g_wpfb                      => g_wpfb,
    g_bsn_nof_clk_per_sync      => g_bsn_nof_clk_per_sync,
    g_scope_selected_subband    => g_scope_selected_subband,
    g_no_jesd                   => c_revision_select.no_jesd,
    g_use_fsub                  => c_revision_select.use_fsub,
    g_use_oversample            => c_revision_select.use_oversample,
    g_use_xsub                  => c_revision_select.use_xsub,
    g_use_bf                    => c_revision_select.use_bf,
    g_use_bdo_transpose         => c_revision_select.use_bdo_transpose,
    g_nof_bdo_destinations_max  => c_revision_select.nof_bdo_destinations_max,
    g_use_ring                  => c_revision_select.use_ring,
    g_P_sq                      => c_revision_select.P_sq
  )
  port map (

    mm_clk => mm_clk,
    mm_rst => mm_rst,

    dp_pps => dp_pps,
    dp_rst => dp_rst,
    dp_clk => dp_clk,

    gn_id        => gn_id,
    this_bck_id  => this_bck_id,
    this_chip_id => this_chip_id,

    SA_CLK => SA_CLK,

    -- jesd204b
    JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK      => JESD204B_REFCLK,
    JESD204B_SYSREF      => JESD204B_SYSREF,
    JESD204B_SYNC_N      => JESD204B_SYNC_N,

    -- UDP Offload
    udp_tx_sosi_arr      =>  udp_tx_sosi_arr,
    udp_tx_siso_arr      =>  udp_tx_siso_arr,

    -- 10 GbE
    reg_nw_10GbE_mac_copi       => reg_nw_10GbE_mac_copi,
    reg_nw_10GbE_mac_cipo       => reg_nw_10GbE_mac_cipo,
    reg_nw_10GbE_eth10g_copi    => reg_nw_10GbE_eth10g_copi,
    reg_nw_10GbE_eth10g_cipo    => reg_nw_10GbE_eth10g_cipo,

    -- AIT
    jesd204b_copi               => jesd204b_copi,
    jesd204b_cipo               => jesd204b_cipo,
    jesd_ctrl_copi              => jesd_ctrl_copi,
    jesd_ctrl_cipo              => jesd_ctrl_cipo,
    reg_dp_shiftram_copi        => reg_dp_shiftram_copi,
    reg_dp_shiftram_cipo        => reg_dp_shiftram_cipo,
    reg_bsn_source_v2_copi      => reg_bsn_source_v2_copi,
    reg_bsn_source_v2_cipo      => reg_bsn_source_v2_cipo,
    reg_bsn_scheduler_wg_copi   => reg_bsn_scheduler_wg_copi,
    reg_bsn_scheduler_wg_cipo   => reg_bsn_scheduler_wg_cipo,
    reg_wg_copi                 => reg_wg_copi,
    reg_wg_cipo                 => reg_wg_cipo,
    ram_wg_copi                 => ram_wg_copi,
    ram_wg_cipo                 => ram_wg_cipo,
    reg_bsn_monitor_input_copi  => reg_bsn_monitor_input_copi,
    reg_bsn_monitor_input_cipo  => reg_bsn_monitor_input_cipo,
    ram_diag_data_buf_bsn_copi  => ram_diag_data_buf_bsn_copi,
    ram_diag_data_buf_bsn_cipo  => ram_diag_data_buf_bsn_cipo,
    reg_diag_data_buf_bsn_copi  => reg_diag_data_buf_bsn_copi,
    reg_diag_data_buf_bsn_cipo  => reg_diag_data_buf_bsn_cipo,
    ram_st_histogram_copi       => ram_st_histogram_copi,
    ram_st_histogram_cipo       => ram_st_histogram_cipo,
    reg_aduh_monitor_copi       => reg_aduh_monitor_copi,
    reg_aduh_monitor_cipo       => reg_aduh_monitor_cipo,

    -- FSUB
    ram_st_sst_copi             => ram_st_sst_copi,
    ram_st_sst_cipo             => ram_st_sst_cipo,
    reg_si_copi                 => reg_si_copi,
    reg_si_cipo                 => reg_si_cipo,
    ram_fil_coefs_copi          => ram_fil_coefs_copi,
    ram_fil_coefs_cipo          => ram_fil_coefs_cipo,
    ram_equalizer_gains_copi    => ram_equalizer_gains_copi,
    ram_equalizer_gains_cipo    => ram_equalizer_gains_cipo,
    ram_equalizer_gains_cross_copi => ram_equalizer_gains_cross_copi,
    ram_equalizer_gains_cross_cipo => ram_equalizer_gains_cross_cipo,
    reg_dp_selector_copi        => reg_dp_selector_copi,
    reg_dp_selector_cipo        => reg_dp_selector_cipo,

    -- SDP Info
    reg_sdp_info_copi           => reg_sdp_info_copi,
    reg_sdp_info_cipo           => reg_sdp_info_cipo,

    -- RING Info
    reg_ring_info_copi          => reg_ring_info_copi,
    reg_ring_info_cipo          => reg_ring_info_cipo,

    -- XSUB
    reg_crosslets_info_copi     => reg_crosslets_info_copi,
    reg_crosslets_info_cipo     => reg_crosslets_info_cipo,
    reg_nof_crosslets_copi      => reg_nof_crosslets_copi,
    reg_nof_crosslets_cipo      => reg_nof_crosslets_cipo,
    reg_bsn_sync_scheduler_xsub_copi => reg_bsn_sync_scheduler_xsub_copi,
    reg_bsn_sync_scheduler_xsub_cipo => reg_bsn_sync_scheduler_xsub_cipo,
    ram_st_xsq_copi             => ram_st_xsq_copi,
    ram_st_xsq_cipo             => ram_st_xsq_cipo,

    -- BF
    ram_ss_ss_wide_copi         => ram_ss_ss_wide_copi,
    ram_ss_ss_wide_cipo         => ram_ss_ss_wide_cipo,
    ram_bf_weights_copi         => ram_bf_weights_copi,
    ram_bf_weights_cipo         => ram_bf_weights_cipo,
    reg_bf_scale_copi           => reg_bf_scale_copi,
    reg_bf_scale_cipo           => reg_bf_scale_cipo,
    reg_hdr_dat_copi            => reg_hdr_dat_copi,
    reg_hdr_dat_cipo            => reg_hdr_dat_cipo,
    reg_bdo_destinations_copi   => reg_bdo_destinations_copi,
    reg_bdo_destinations_cipo   => reg_bdo_destinations_cipo,
    reg_dp_xonoff_copi          => reg_dp_xonoff_copi,
    reg_dp_xonoff_cipo          => reg_dp_xonoff_cipo,
    ram_st_bst_copi             => ram_st_bst_copi,
    ram_st_bst_cipo             => ram_st_bst_cipo,
    reg_bsn_align_v2_bf_copi    => reg_bsn_align_v2_bf_copi,
    reg_bsn_align_v2_bf_cipo    => reg_bsn_align_v2_bf_cipo,
    reg_bsn_monitor_v2_rx_align_bf_copi => reg_bsn_monitor_v2_rx_align_bf_copi,
    reg_bsn_monitor_v2_rx_align_bf_cipo => reg_bsn_monitor_v2_rx_align_bf_cipo,
    reg_bsn_monitor_v2_aligned_bf_copi  => reg_bsn_monitor_v2_aligned_bf_copi,
    reg_bsn_monitor_v2_aligned_bf_cipo  => reg_bsn_monitor_v2_aligned_bf_cipo,
    reg_ring_lane_info_bf_copi          => reg_ring_lane_info_bf_copi,
    reg_ring_lane_info_bf_cipo          => reg_ring_lane_info_bf_cipo,
    reg_bsn_monitor_v2_ring_rx_bf_copi  => reg_bsn_monitor_v2_ring_rx_bf_copi,
    reg_bsn_monitor_v2_ring_rx_bf_cipo  => reg_bsn_monitor_v2_ring_rx_bf_cipo,
    reg_bsn_monitor_v2_ring_tx_bf_copi  => reg_bsn_monitor_v2_ring_tx_bf_copi,
    reg_bsn_monitor_v2_ring_tx_bf_cipo  => reg_bsn_monitor_v2_ring_tx_bf_cipo,
    reg_dp_block_validate_err_bf_copi   => reg_dp_block_validate_err_bf_copi,
    reg_dp_block_validate_err_bf_cipo   => reg_dp_block_validate_err_bf_cipo,
    reg_dp_block_validate_bsn_at_sync_bf_copi => reg_dp_block_validate_bsn_at_sync_bf_copi,
    reg_dp_block_validate_bsn_at_sync_bf_cipo => reg_dp_block_validate_bsn_at_sync_bf_cipo,

    -- SST
    reg_stat_enable_sst_copi            => reg_stat_enable_sst_copi,
    reg_stat_enable_sst_cipo            => reg_stat_enable_sst_cipo,
    reg_stat_hdr_dat_sst_copi           => reg_stat_hdr_dat_sst_copi,
    reg_stat_hdr_dat_sst_cipo           => reg_stat_hdr_dat_sst_cipo,
    reg_bsn_monitor_v2_sst_offload_copi => reg_bsn_monitor_v2_sst_offload_copi,
    reg_bsn_monitor_v2_sst_offload_cipo => reg_bsn_monitor_v2_sst_offload_cipo,

    -- XST
    reg_stat_enable_xst_copi    => reg_stat_enable_xst_copi,
    reg_stat_enable_xst_cipo    => reg_stat_enable_xst_cipo,
    reg_stat_hdr_dat_xst_copi   => reg_stat_hdr_dat_xst_copi,
    reg_stat_hdr_dat_xst_cipo   => reg_stat_hdr_dat_xst_cipo,

    reg_bsn_align_v2_xsub_copi                 => reg_bsn_align_v2_xsub_copi,
    reg_bsn_align_v2_xsub_cipo                 => reg_bsn_align_v2_xsub_cipo,
    reg_bsn_monitor_v2_rx_align_xsub_copi      => reg_bsn_monitor_v2_rx_align_xsub_copi,
    reg_bsn_monitor_v2_rx_align_xsub_cipo      => reg_bsn_monitor_v2_rx_align_xsub_cipo,
    reg_bsn_monitor_v2_aligned_xsub_copi       => reg_bsn_monitor_v2_aligned_xsub_copi,
    reg_bsn_monitor_v2_aligned_xsub_cipo       => reg_bsn_monitor_v2_aligned_xsub_cipo,
    reg_bsn_monitor_v2_xst_offload_copi        => reg_bsn_monitor_v2_xst_offload_copi,
    reg_bsn_monitor_v2_xst_offload_cipo        => reg_bsn_monitor_v2_xst_offload_cipo,
    reg_ring_lane_info_xst_copi                => reg_ring_lane_info_xst_copi,
    reg_ring_lane_info_xst_cipo                => reg_ring_lane_info_xst_cipo,
    reg_bsn_monitor_v2_ring_rx_xst_copi        => reg_bsn_monitor_v2_ring_rx_xst_copi,
    reg_bsn_monitor_v2_ring_rx_xst_cipo        => reg_bsn_monitor_v2_ring_rx_xst_cipo,
    reg_bsn_monitor_v2_ring_tx_xst_copi        => reg_bsn_monitor_v2_ring_tx_xst_copi,
    reg_bsn_monitor_v2_ring_tx_xst_cipo        => reg_bsn_monitor_v2_ring_tx_xst_cipo,
    reg_dp_block_validate_err_xst_copi         => reg_dp_block_validate_err_xst_copi,
    reg_dp_block_validate_err_xst_cipo         => reg_dp_block_validate_err_xst_cipo,
    reg_dp_block_validate_bsn_at_sync_xst_copi => reg_dp_block_validate_bsn_at_sync_xst_copi,
    reg_dp_block_validate_bsn_at_sync_xst_cipo => reg_dp_block_validate_bsn_at_sync_xst_cipo,
    reg_tr_10GbE_mac_copi                      => reg_tr_10GbE_mac_copi,
    reg_tr_10GbE_mac_cipo                      => reg_tr_10GbE_mac_cipo,
    reg_tr_10GbE_eth10g_copi                   => reg_tr_10GbE_eth10g_copi,
    reg_tr_10GbE_eth10g_cipo                   => reg_tr_10GbE_eth10g_cipo,

    -- BST
    reg_stat_enable_bst_copi               => reg_stat_enable_bst_copi,
    reg_stat_enable_bst_cipo               => reg_stat_enable_bst_cipo,
    reg_stat_hdr_dat_bst_copi              => reg_stat_hdr_dat_bst_copi,
    reg_stat_hdr_dat_bst_cipo              => reg_stat_hdr_dat_bst_cipo,
    reg_bsn_monitor_v2_bst_offload_copi    => reg_bsn_monitor_v2_bst_offload_copi,
    reg_bsn_monitor_v2_bst_offload_cipo    => reg_bsn_monitor_v2_bst_offload_cipo,
    reg_bsn_monitor_v2_beamlet_output_copi => reg_bsn_monitor_v2_beamlet_output_copi,
    reg_bsn_monitor_v2_beamlet_output_cipo => reg_bsn_monitor_v2_beamlet_output_cipo,

    RING_0_TX => RING_0_TX,
    RING_0_RX => RING_0_RX,
    RING_1_TX => RING_1_TX,
    RING_1_RX => RING_1_RX,

    -- QSFP serial
    unb2_board_front_io_serial_tx_arr => unb2_board_front_io_serial_tx_arr,
    unb2_board_front_io_serial_rx_arr => unb2_board_front_io_serial_rx_arr,

    -- QSFP LEDS
    unb2_board_qsfp_leds_tx_sosi_arr  => unb2_board_qsfp_leds_tx_sosi_arr,
    unb2_board_qsfp_leds_tx_siso_arr  => unb2_board_qsfp_leds_tx_siso_arr,
    unb2_board_qsfp_leds_rx_sosi_arr  => unb2_board_qsfp_leds_rx_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- Interface : 10GbE
  -----------------------------------------------------------------------------
  -- put the QSFP_TX/RX ports into arrays
  i_QSFP_RX(0) <= QSFP_0_RX;
  i_QSFP_RX(1) <= QSFP_1_RX;
  QSFP_0_TX <= i_QSFP_TX(0);
  QSFP_1_TX <= i_QSFP_TX(1);
  ------------
  -- Front IO
  ------------
  u_front_io : entity unb2c_board_lib.unb2c_board_front_io
  generic map (
    g_nof_qsfp_bus => c_unb2c_board_tr_qsfp.nof_bus
  )
  port map (
    serial_tx_arr => unb2_board_front_io_serial_tx_arr,
    serial_rx_arr => unb2_board_front_io_serial_rx_arr,

    green_led_arr => qsfp_green_led_arr,
    red_led_arr   => qsfp_red_led_arr,

    QSFP_RX       => i_QSFP_RX,
    QSFP_TX       => i_QSFP_TX,

    QSFP_LED      => QSFP_LED
  );

  ------------
  -- LEDs
  ------------
  u_front_led : entity unb2c_board_lib.unb2c_board_qsfp_leds
  generic map (
    g_sim           => g_sim,
    g_factory_image => g_factory_image,
    g_nof_qsfp      => c_unb2c_board_tr_qsfp.nof_bus,
    g_pulse_us      => 1000 / (10**9 / c_mm_clk_freq)  -- nof clk cycles to get us period
  )
  port map (
    rst             => mm_rst,
    clk             => mm_clk,
    green_led_arr   => qsfp_green_led_arr,
    red_led_arr     => qsfp_red_led_arr,

    tx_siso_arr     => unb2_board_qsfp_leds_tx_siso_arr,
    tx_sosi_arr     => unb2_board_qsfp_leds_tx_sosi_arr,
    rx_sosi_arr     => unb2_board_qsfp_leds_rx_sosi_arr
  );
end str;
