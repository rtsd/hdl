README: Description of the ip_stratixiv library

Contents:
1) Single file IP
2) Multi file IP



1) Single file IP

The single file IP is grouped in ip_<device_name>/<ip_name> sub directories. Each with itsown hdllib.cfg.
   


2) Multi file IP

The multi file IP is kept in sub directories in the top level ip_<device_name>/<ip_name> directory to be able to keep the generated files together:

  ip_<device_name>/<ip_name>/              -- source files (all in SVN)
                   <ip_name>/generated/    -- generated files (may also be committed in SVN)

The source files are kept in the <ip_name> directory. The generated files are kept in a <ip_name>/generated/ directory. The MegaWizard variation file
is typically a <ip_name>.vhd VHDL file is the top level file and that also is used by the MegaWizard to generated the IP. The generate_ip.sh script
can be run to generate the IP, usage:

  ./generate_ip.sh

The generate_ip.sh script first copies the MegaWizard variation file in the 'generated' output dir, because:

  1) setting the output dir is not an option in the MegaWizard
  2) The MegaWizard overwrites the variation file sometimes - we don't want that as it could be hand-modified with e.g. added generics
  
The generated files for Quartus v11.1 that are needed for simulation and synthesis may also be committed in SVN, so it is not necessary to run this
generate_ip.sh script again. Files that are not needed should better not be committed in SVN. The advantage of only committing what is needed is that
it is more clear what is needed and that it may save substantial disk space in the repository and at local at each check out. The disadvantage of
committing only what is needed may be that it takes some effort to find out what is needed. If the size/amount of files that are not needed is
relatively small then committing all generated files is fine too.

For simulation the MegaWizard also generates a msim_setup.tcl script. A stripped down version of this msim_setup.tcl script is made manually and
kept in compile_ip.tcl. This compile_ip.tcl only compiles what is needed and is made available in the Modelsim project file via the
modelsim_compile_ip_files key in the hdllib.cfg. The 'mk' command automatically also executes the compile_ip.tcl before it compiles any other HDL
in the library.
