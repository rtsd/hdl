/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2009
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */
#ifndef UNBOS_PPS_H
#define UNBOS_PPS_H

/* Compile switches. */

/* includes */
#include <system.h>

/* If "system.h" defines PIO_PPS_BASE it's safe to 
 * assume the system has the PPS PIO register in it ... */
#ifdef PIO_PPS_BASE
    // System has PPS
    #include <alt_types.h>
	#include <altera_avalon_pio_regs.h>

    /* private constants (#define) */
    
    /* private macros */

    /* private types */

    /* private function protos */

    /* private constants (const) */
    
    /* private variables */
    
    /* public variables */

    /* public function implements */
    int UNBOS_pps_wait(void);

#else

    /* system does NOT have a 1PPS */
    #define UNBOS_pps_wait() 0

#endif

#endif // UNBOS_PPS_H
