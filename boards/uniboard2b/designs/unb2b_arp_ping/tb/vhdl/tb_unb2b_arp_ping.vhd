-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for unb2b_arp_ping.
-- Description:
--   The DUT can be targeted at unb 0, node 3 with the same Python scripts
--   that are used on hardware.
-- Usage:
--   On command line do:
--     > run_modelsim & (to start Modeslim)
--
--   In Modelsim do:
--     > lp unb2b_arp_ping
--     > mk clean all (only first time to clean all libraries)
--     > mk all (to compile all libraries that are needed for unb2b_arp_ping)
--     . load tb_unb1_arp_ping simulation by double clicking the tb_unb2b_arp_ping icon
--     > as 10 (to view signals in Wave Window)
--     > run 100 us (or run -all)
--
--   On command line do:
--     > python $UPE_GEAR/peripherals/util_system_info.py --gn 3 -n 0 -v 5 --sim
--     > python $UPE_GEAR/peripherals/util_unb_sens.py --gn 3 -n 0 -v 5 --sim
--     > python $UPE_GEAR/peripherals/util_ppsh.py --gn 3 -n 1 -v 5 --sim
--

library IEEE, common_lib, technology_lib, unb2b_board_lib, dp_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_unb2b_arp_ping is
    generic (
      g_frm_discard_en : boolean := false;  -- when TRUE discard frame types that would otherwise have to be discarded by the Nios MM master
      g_flush_test_en  : boolean := false;  -- when TRUE send many large frames to enforce flush in eth_buffer
      --   g_data_type = c_tb_tech_tse_data_type_symbols  = 0
      --   g_data_type = c_tb_tech_tse_data_type_counter  = 1
      --   g_data_type = c_tb_tech_tse_data_type_arp      = 2
      --   g_data_type = c_tb_tech_tse_data_type_ping     = 3
      --   g_data_type = c_tb_tech_tse_data_type_udp      = 4
      g_data_type      : natural := c_tb_tech_tse_data_type_ping;
      g_tb_end         : boolean := true  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
    );
  port (
    tb_end : out std_logic
  );
end tb_unb2b_arp_ping;

architecture tb of tb_unb2b_arp_ping is
  constant c_sim             : boolean := false;  -- TRUE;
  constant c_sim_level       : natural := 1;  -- 0 = use IP; 1 = use fast serdes model;

  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 3;  -- Node 3
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_unb_nr, c_unb2b_board_nof_uniboard_w) & TO_UVEC(c_node_nr, c_unb2b_board_nof_chip_w);

  constant c_version         : std_logic_vector(1 downto 0) := "00";

  constant c_cable_delay     : time := 12 ns;  -- 12 ns;
  constant c_clk_period      : time := 5 ns;  -- 200 MHz
  constant c_st_clk_period   : time := 5 ns;  -- 200 MHz
  constant c_eth_clk_period  : time := 8 ns;  -- 125 MHz
  constant c_tse_clk_period  : time := 8 ns;  -- 125 MHz
  constant c_mm_clk_period   : time := 10 ns;  -- 100 MHz
  constant c_pps_period      : natural := 1000;

  -----------------------------------------------------------------------------
  -- DUT
  -----------------------------------------------------------------------------

  -- Base address as used by unb_osy
  constant c_base_ip         : std_logic_vector(c_16 - 1 downto 0) := X"0A63";  -- Base IP address used by unb_osy: 10.99.xx.yy
  constant c_base_mac        : std_logic_vector(c_32 - 1 downto 0) := X"00228608";  -- Base MAC address used by unb_osy: 00228608_xx_yy

  -- Network addresses
  constant c_dut_src_ip      : std_logic_vector(c_network_ip_addr_slv'range) := c_base_ip  & TO_UVEC(c_unb_nr, c_8) & TO_UVEC(c_node_nr + 1, c_8);
  constant c_dut_src_mac     : std_logic_vector(c_network_eth_mac_slv'range) := c_base_mac & TO_UVEC(c_unb_nr, c_8) & TO_UVEC(c_node_nr, c_8);

  -- Clocks and reset
  signal sys_clk             : std_logic := '0';  -- system clock
  signal eth_clk             : std_logic := '0';  -- eth / tse reference clock

  -- DUT
  signal pps                 : std_logic := '0';
  signal pps_rst             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_txp_arr         : std_logic_vector(1 downto 0);
  signal eth_rxp_arr         : std_logic_vector(1 downto 0);

  signal VERSION             : std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0) := c_version;
  signal ID                  : std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0)      := c_id;
  signal TESTIO              : std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;

  signal PMBUS_SC            : std_logic;
  signal PMBUS_SD            : std_logic;
  signal PMBUS_ALERT         : std_logic := '0';

  signal qsfp_led            : std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0);

  -----------------------------------------------------------------------------
  -- DUT - LCU Ethernet interface
  -----------------------------------------------------------------------------
  signal lcu_txp             : std_logic;
  signal eth_rxp             : std_logic;
  signal eth_txp             : std_logic;
  signal lcu_rxp             : std_logic;

  -----------------------------------------------------------------------------
  -- LCU model
  -----------------------------------------------------------------------------
  constant c_lcu_src_ip      : std_logic_vector(c_network_ip_addr_slv'range) := X"05060708";
  constant c_lcu_src_mac     : std_logic_vector(c_network_eth_mac_slv'range) := X"10FA01020300";

  signal tse_clk             : std_logic := '0';  -- tse reference clock
  signal st_clk              : std_logic := '0';  -- stream clock
  signal st_rst              : std_logic := '1';
  signal mm_clk              : std_logic := '0';  -- MM bus clock
  signal mm_rst              : std_logic := '1';

  -- TSE interface
  signal lcu_init            : std_logic := '1';
  signal lcu_tse_miso        : t_mem_miso;
  signal lcu_tse_mosi        : t_mem_mosi;
  signal lcu_psc_access      : std_logic;
  signal lcu_tx_en           : std_logic := '1';
  signal lcu_tx_siso         : t_dp_siso;
  signal lcu_tx_sosi         : t_dp_sosi;
  signal lcu_tx_mac_in       : t_tech_tse_tx_mac;
  signal lcu_tx_mac_out      : t_tech_tse_tx_mac;
  signal lcu_rx_sosi         : t_dp_sosi;
  signal lcu_rx_siso         : t_dp_siso;
  signal lcu_rx_mac_out      : t_tech_tse_rx_mac;
  signal lcu_led             : t_tech_tse_led;

  -- TSE constants
  constant c_tx_ready_latency   : natural := c_tech_tse_tx_ready_latency;  -- 0, 1 are supported, must match TSE MAC c_tech_tse_tx_ready_latency
  constant c_promis_en          : boolean := false;

  -----------------------------------------------------------------------------
  -- Stimuli & Verification
  -----------------------------------------------------------------------------

  signal tx_end              : std_logic := '0';
  signal rx_end              : std_logic := '0';
  signal rx_timeout          : natural := 0;
  signal tx_pkt_cnt          : natural := 0;
  signal rx_pkt_cnt          : natural := 0;
  signal rx_pkt_discarded_cnt: natural := 0;
  signal rx_pkt_flushed_cnt  : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');  -- maintained in DUT, but not accessible in tb

  constant c_nof_tx_not_valid   : natural := 0;  -- when > 0 then pull tx valid low for c_nof_tx_not_valid beats during tx

  -- Payload user data
  constant c_tb_nof_data        : natural := 0;  -- nof UDP user data, nof ping padding data
  constant c_tb_ip_nof_data     : natural := c_network_udp_header_len + c_tb_nof_data;  -- nof IP data,
                                          -- also suits ICMP, because c_network_icmp_header_len = c_network_udp_header_len
  constant c_tb_reply_payload   : boolean := true;  -- TRUE copy rx payload into response payload, else header only (e.g. for ARP)

  -- Packet headers
  -- support only ARP and IPv4 over ETH
  --                                                             symbols   counter               ARP=0x806               IP=0x800               IP=0x800
  constant c_dut_ethertype      : natural := sel_n(g_data_type, 16#07F0#, 16#07F1#, c_network_eth_type_arp, c_network_eth_type_ip, c_network_eth_type_ip);

  constant c_tx_eth_header      : t_network_eth_header := (dst_mac    => c_dut_src_mac,
                                                           src_mac    => c_lcu_src_mac,
                                                           eth_type   => TO_UVEC(c_dut_ethertype, c_network_eth_type_w));
  constant c_discard_eth_header : t_network_eth_header := (dst_mac    => c_dut_src_mac,
                                                           src_mac    => c_lcu_src_mac,
                                                           eth_type   => TO_UVEC(16#07F0#, c_network_eth_type_w));
  constant c_exp_eth_header     : t_network_eth_header := (dst_mac    => c_tx_eth_header.src_mac,  -- \/
                                                           src_mac    => c_tx_eth_header.dst_mac,  -- /\
                                                           eth_type   => c_tx_eth_header.eth_type);  -- =

  -- . IP header
  constant c_tb_ip_total_length : natural := c_network_ip_total_length + c_tb_ip_nof_data;

  -- support only ping protocol or UDP protocol over IP
  --                                                          symbols counter  ARP                      ping=1                     UDP=17
  constant c_tb_ip_protocol     : natural := sel_n(g_data_type,    13,     14,  15, c_network_ip_protocol_icmp, c_network_ip_protocol_udp);

  constant c_tx_ip_header       : t_network_ip_header := (version         => TO_UVEC(c_network_ip_version,         c_network_ip_version_w),
                                                          header_length   => TO_UVEC(c_network_ip_header_length,   c_network_ip_header_length_w),
                                                          services        => TO_UVEC(c_network_ip_services,        c_network_ip_services_w),
                                                          total_length    => TO_UVEC(c_tb_ip_total_length,         c_network_ip_total_length_w),
                                                          identification  => TO_UVEC(c_network_ip_identification,  c_network_ip_identification_w),
                                                          flags           => TO_UVEC(c_network_ip_flags,           c_network_ip_flags_w),
                                                          fragment_offset => TO_UVEC(c_network_ip_fragment_offset, c_network_ip_fragment_offset_w),
                                                          time_to_live    => TO_UVEC(c_network_ip_time_to_live,    c_network_ip_time_to_live_w),
                                                          protocol        => TO_UVEC(c_tb_ip_protocol,             c_network_ip_protocol_w),
                                                          header_checksum => TO_UVEC(c_network_ip_header_checksum, c_network_ip_header_checksum_w),  -- init value (or try 0xEBBD = 60349)
                                                          src_ip_addr     =>         c_lcu_src_ip,
                                                          dst_ip_addr     =>         c_dut_src_ip);

  constant c_exp_ip_header      : t_network_ip_header := (version         => c_tx_ip_header.version,  -- =
                                                          header_length   => c_tx_ip_header.header_length,  -- =
                                                          services        => c_tx_ip_header.services,  -- =
                                                          total_length    => c_tx_ip_header.total_length,  -- =
                                                          identification  => c_tx_ip_header.identification,  -- =
                                                          flags           => c_tx_ip_header.flags,  -- =
                                                          fragment_offset => c_tx_ip_header.fragment_offset,  -- =
                                                          time_to_live    => c_tx_ip_header.time_to_live,  -- =
                                                          protocol        => c_tx_ip_header.protocol,  -- =
                                                          header_checksum => c_tx_ip_header.header_checksum,  -- init value
                                                          src_ip_addr     => c_tx_ip_header.dst_ip_addr,  -- \/
                                                          dst_ip_addr     => c_tx_ip_header.src_ip_addr);  -- /\

  -- . ARP packet
  constant c_tx_arp_packet      : t_network_arp_packet := (htype => TO_UVEC(c_network_arp_htype,        c_network_arp_htype_w),
                                                           ptype => TO_UVEC(c_network_arp_ptype,        c_network_arp_ptype_w),
                                                           hlen  => TO_UVEC(c_network_arp_hlen,         c_network_arp_hlen_w),
                                                           plen  => TO_UVEC(c_network_arp_plen,         c_network_arp_plen_w),
                                                           oper  => TO_UVEC(c_network_arp_oper_request, c_network_arp_oper_w),
                                                           sha   => c_lcu_src_mac,
                                                           spa   => c_lcu_src_ip,
                                                           tha   => c_dut_src_mac,
                                                           tpa   => c_dut_src_ip);

  constant c_exp_arp_packet     : t_network_arp_packet := (htype => c_tx_arp_packet.htype,
                                                           ptype => c_tx_arp_packet.ptype,
                                                           hlen  => c_tx_arp_packet.hlen,
                                                           plen  => c_tx_arp_packet.plen,
                                                           oper  => TO_UVEC(c_network_arp_oper_reply, c_network_arp_oper_w),  -- reply
                                                           sha   => c_tx_arp_packet.tha,  -- \/
                                                           spa   => c_tx_arp_packet.tpa,  -- /\  \/
                                                           tha   => c_tx_arp_packet.sha,  -- /  \ /\
                                                           tpa   => c_tx_arp_packet.spa);  -- /  \

  -- . ICMP header
  constant c_tx_icmp_header      : t_network_icmp_header := (msg_type => TO_UVEC(c_network_icmp_msg_type_request, c_network_icmp_msg_type_w),  -- ping request
                                                             code     => TO_UVEC(c_network_icmp_code,             c_network_icmp_code_w),
                                                             checksum => TO_UVEC(c_network_icmp_checksum,         c_network_icmp_checksum_w),  -- init value
                                                             id       => TO_UVEC(c_network_icmp_id,               c_network_icmp_id_w),
                                                             sequence => TO_UVEC(c_network_icmp_sequence,         c_network_icmp_sequence_w));
  constant c_exp_icmp_header     : t_network_icmp_header := (msg_type => TO_UVEC(c_network_icmp_msg_type_reply,   c_network_icmp_msg_type_w),  -- ping reply
                                                             code     => c_tx_icmp_header.code,
                                                             checksum => c_tx_icmp_header.checksum,  -- init value
                                                             id       => c_tx_icmp_header.id,
                                                             sequence => c_tx_icmp_header.sequence);

  -- . UDP header
  constant c_dut_udp_port_ctrl   : natural := 11;  -- ETH demux UDP for control
  constant c_lcu_udp_port        : natural := 10;  -- UDP port used for src_port

  constant c_tb_udp_total_length : natural := c_network_udp_total_length + c_tb_nof_data;

  constant c_tx_udp_header       : t_network_udp_header := (src_port     => TO_UVEC(c_lcu_udp_port,         c_network_udp_port_w),
                                                            dst_port     => TO_UVEC(c_dut_udp_port_ctrl,    c_network_udp_port_w),  -- or use c_dut_udp_port_st#
                                                            total_length => TO_UVEC(c_tb_udp_total_length,  c_network_udp_total_length_w),
                                                            checksum     => TO_UVEC(c_network_udp_checksum, c_network_udp_checksum_w));  -- init value

  constant c_exp_udp_header      : t_network_udp_header := (src_port     => c_tx_udp_header.dst_port,  -- \/
                                                            dst_port     => c_tx_udp_header.src_port,  -- /\
                                                            total_length => c_tx_udp_header.total_length,  -- =
                                                            checksum     => c_tx_udp_header.checksum);  -- init value

  signal tx_total_header     : t_network_total_header;  -- transmitted packet header
  signal discard_total_header: t_network_total_header;  -- transmitted packet header for to be discarded packet
  signal exp_total_header    : t_network_total_header;  -- expected received packet header
begin
  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  sys_clk <= not sys_clk after c_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)

  -- External PPS
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, sys_clk, pps);

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up

  PMBUS_SC <= 'H';  -- pull up
  PMBUS_SD <= 'H';  -- pull up

  u_dut : entity work.unb2b_arp_ping
  generic map (
    g_sim         => c_sim,
    g_sim_level   => c_sim_level
  )
  port map (
    -- GENERAL
    CLK         => sys_clk,
    PPS         => pps,
    WDI         => WDI,
    INTA        => INTA,
    INTB        => INTB,

    sens_sc     => sens_scl,
    sens_sd     => sens_sda,

    PMBUS_SC    => PMBUS_SC,
    PMBUS_SD    => PMBUS_SD,
    PMBUS_ALERT => PMBUS_ALERT,

    -- Others
    VERSION     => VERSION,
    ID          => ID,
    TESTIO      => TESTIO,

    -- 1GbE Control Interface
    ETH_clk     => eth_clk,
    ETH_SGIN    => eth_rxp_arr,
    ETH_SGOUT   => eth_txp_arr,

    QSFP_LED    => qsfp_led
  );

  ------------------------------------------------------------------------------
  -- Ethernet cable between LCU and DUT
  ------------------------------------------------------------------------------
  eth_rxp <= transport lcu_txp after c_cable_delay;
  lcu_rxp <= transport eth_txp after c_cable_delay;

  eth_rxp_arr(0) <= eth_rxp;
  eth_txp        <= eth_txp_arr(0);

  ------------------------------------------------------------------------------
  -- LCU Ethernet model
  ------------------------------------------------------------------------------

  tse_clk <= not tse_clk after c_tse_clk_period / 2;  -- TSE clock for LCU model
  st_clk <= not st_clk after c_st_clk_period / 2;  -- System clock for LCU model
  mm_clk <= not mm_clk after c_mm_clk_period / 2;  -- MM clock for LCU model

  st_rst <= '1', '0' after 10 * c_st_clk_period;
  mm_rst <= '1', '0' after 10 * c_mm_clk_period;

  p_lcu_setup : process
  begin
    lcu_tse_mosi <= c_mem_mosi_rst;
    -- Wait for reset release
    while mm_rst = '1' loop wait until rising_edge(mm_clk); end loop;

    -- Setup the LCU TSE MAC
    proc_tech_tse_setup(c_tech_select_default,
                        c_promis_en, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tx_ready_latency,
                        c_lcu_src_mac, lcu_psc_access,
                        mm_clk, lcu_tse_miso, lcu_tse_mosi);
    lcu_init <= '0';
    wait;
  end process;

  u_lcu : entity tech_tse_lib.tech_tse
  generic map (
    g_sim          => c_sim,
    g_sim_level    => c_sim_level
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    eth_clk        => tse_clk,
    tx_snk_clk     => st_clk,
    rx_src_clk     => st_clk,

    -- Memory Mapped Slave
    mm_sla_in      => lcu_tse_mosi,
    mm_sla_out     => lcu_tse_miso,

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      => lcu_tx_sosi,
    tx_snk_out     => lcu_tx_siso,
    -- . MAC specific
    tx_mac_in      => lcu_tx_mac_in,
    tx_mac_out     => lcu_tx_mac_out,

    -- MAC receive interface
    -- . ST Source
    rx_src_in      => lcu_rx_siso,
    rx_src_out     => lcu_rx_sosi,
    -- . MAC specific
    rx_mac_out     => lcu_rx_mac_out,

    -- PHY interface
    eth_txp        => lcu_txp,
    eth_rxp        => lcu_rxp,

    tse_led        => lcu_led
  );

  ------------------------------------------------------------------------------
  -- LCU transmit and receive packets
  ------------------------------------------------------------------------------

  -- Use signal to leave unused fields 'X'
  tx_total_header.eth  <= c_tx_eth_header;
  tx_total_header.arp  <= c_tx_arp_packet;
  tx_total_header.ip   <= c_tx_ip_header;
  tx_total_header.icmp <= c_tx_icmp_header;
  tx_total_header.udp  <= c_tx_udp_header;

  discard_total_header.eth  <= c_discard_eth_header;
  discard_total_header.arp  <= c_tx_arp_packet;
  discard_total_header.ip   <= c_tx_ip_header;
  discard_total_header.icmp <= c_tx_icmp_header;
  discard_total_header.udp  <= c_tx_udp_header;

  exp_total_header.eth  <= c_exp_eth_header;
  exp_total_header.arp  <= c_exp_arp_packet;
  exp_total_header.ip   <= c_exp_ip_header;
  exp_total_header.icmp <= c_exp_icmp_header;
  exp_total_header.udp  <= c_exp_udp_header;

  p_lcu_transmitter : process
  begin
    -- . Avalon ST
    lcu_tx_sosi <= c_dp_sosi_rst;
    -- . MAC specific
    lcu_tx_mac_in.crc_fwd <= '0';  -- when '0' then TSE MAC generates the TX CRC field

    while lcu_init /= '0' loop wait until rising_edge(st_clk); end loop;

    -- wait a while till init of dut is done
    for I in 0 to 1000 loop wait until rising_edge(st_clk); end loop;

    for I in 0 to 1 loop
      proc_tech_tse_tx_packet(tx_total_header, I, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      for J in 0 to 40 loop wait until rising_edge(st_clk); end loop;
    end loop;

    if g_frm_discard_en = true then
      -- Insert a counter data packet that should be discarded
      proc_tech_tse_tx_packet(discard_total_header, 13, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      rx_pkt_discarded_cnt <= rx_pkt_discarded_cnt + 1;
      -- Send another packet that should be received
      proc_tech_tse_tx_packet(tx_total_header,  14, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
    end if;

    if g_flush_test_en = true then
      proc_tech_tse_tx_packet(tx_total_header, 1496, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1497, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1498, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1499, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header,    0, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header,    1, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
      proc_tech_tse_tx_packet(tx_total_header,    2, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
    end if;

--     proc_tech_tse_tx_packet(tx_total_header,  104, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  105, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header, 1472, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header, 1500, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  101, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header, 1000, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  102, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header, 1000, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  103, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  104, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);
--     proc_tech_tse_tx_packet(tx_total_header,  105, g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, lcu_tx_en, lcu_tx_siso, lcu_tx_sosi);

    tx_end <= '1';
    wait;
  end process;

  p_lcu_receiver : process
  begin
    -- . Avalon ST
    lcu_rx_siso <= c_dp_siso_hold;

    wait until lcu_init = '0';

    -- Verification of multiple rx packets is only supported when all packets
    -- are of the same g_data_type, because the rx process can only support
    -- one expected result. The proc_tech_tse_rx_packet does not (yet) interpret the
    -- actually received packet, it relies on the preset expected total_header.

    -- Receive forever
    while true loop
      proc_tech_tse_rx_packet(exp_total_header, g_data_type, st_clk, lcu_rx_sosi, lcu_rx_siso);
    end loop;

    wait;
  end process;

  -----------------------------------------------------------------------------
  -- Verification
  -----------------------------------------------------------------------------

  tx_pkt_cnt <= tx_pkt_cnt + 1 when lcu_tx_sosi.sop = '1' and rising_edge(st_clk);
  rx_pkt_cnt <= rx_pkt_cnt + 1 when lcu_rx_sosi.eop = '1' and rising_edge(st_clk);

  p_rx_end : process
  begin
    rx_end <= '0';
    wait until tx_end = '1';

    -- use timeout since tx_end or last received packet to determine rx_end
    rx_timeout <= 0;
    while rx_end = '0' loop
      rx_timeout <= rx_timeout + 1;
      if lcu_rx_sosi.valid = '1' then
        rx_timeout <= 0;
      elsif rx_timeout > 5000 then  -- sufficiently large value determined by trial
        rx_end <= '1';
      end if;
      wait until rising_edge(st_clk);
    end loop;

    --WAIT FOR 10 us;
    --rx_end <= '1';
    wait;
  end process;

  p_tb_end : process
  begin
    tb_end <= '0';
    wait until rx_end = '1';

    -- Verify that all transmitted packets have been received
    if tx_pkt_cnt = 0 then
      report "No packets were transmitted."
        severity ERROR;
    elsif rx_pkt_cnt = 0 then
      report "No packets were received."
        severity ERROR;
    elsif tx_pkt_cnt /= rx_pkt_cnt + rx_pkt_discarded_cnt + TO_UINT(rx_pkt_flushed_cnt) then
      report "Not all transmitted packets were received."
        severity ERROR;
    end if;

    wait for 1 us;
    tb_end <= '1';
    if g_tb_end = false then
      report "Tb simulation finished."
        severity NOTE;
    else
      report "Tb simulation finished."
        severity FAILURE;
    end if;
    wait;
  end process;
end tb;
