--
--        _/_/           _/_/_/
--      _/_/_/_/         _/_/_/
--      _/_/_/_/_/       _/_/_/              ____________________________________________
--      _/_/_/_/_/       _/_/_/             /                                           /
--      _/_/_/_/_/       _/_/_/            /                                 M25P128   /
--      _/_/_/_/_/       _/_/_/           /                                           /
--      _/_/_/_/_/_/     _/_/_/          /                                  128Mbit  /
--      _/_/_/_/_/_/     _/_/_/         /                   Low Voltage, SPI, 50Mhz /
--      _/_/_/ _/_/_/    _/_/_/        /                                           /
--      _/_/_/  _/_/_/   _/_/_/       /                     VHDL Behavioral Model /
--      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.0 /
--      _/_/_/    _/_/_/ _/_/_/     /                                           /
--      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2008 Numonyx B.V. /
--      _/_/_/      _/_/_/_/_/    /___________________________________________/
--      _/_/_/       _/_/_/_/
--      _/_/          _/_/_/
--
--
--             NUMONYX
--
--
********************************************************************************* 


This README provides information on the following topics :

- VHDL Behavioral Model description
- Version History
- Install / uninstall information
- File list
- Get support
- Bug reports
- Send feedback / requests for features
- Known issues
- Ordering information


------------------------------------
VHDL BEHAVIORAL MODEL DESCRIPTION
------------------------------------

Behavioral modeling is a technic for the description of an hardware architecture at an 
algorithmic level where the designers do not necessarily think in terms of
logic gates or data flow, but in terms of the algorithm and its performance.
Only after the high-level architecture and algorithm are finalized, do designers 
start focusing on building the digital circuit to implement the algorithm.
To obtain this behavioral model we used VHDL Language.


------------------------------------
VERSION HISTORY
------------------------------------

 -   Version 1.0

        Date    :  18/06/08

        Note    :  First Release Numonyx

        Author                 Anna Faldarini
                                e-mail : anna.faldarini@numonyx.com

        Bug Fix:  Fix on Read STatus Register problem with Modelsim simulator
                  Fix on Array Data Read in case of consecutive reads with no change on DataOut

  - Version 1.0 - 

  Date    : 17/01/2007

  Note    : First Release

  Author  : 
            Giampaolo Giacopelli                             
            Tel    : +39916689948
            e-mail : giampaolo.giacopelli@st.com

         
  Based on Datasheet date: October 2006; rev. 2



---------------------------------
INSTALL / UNINSTALL INFORMATION
---------------------------------

For installing the model you have to process the NU_M25P128_V10.zip delivery package.  

Compatibility: the model has been tested by Cadence NCSim 5.4 simulator, on SUN OS environment.

******************************************************************************   

IMPORTANT:

  
    THIS PROGRAM IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,        
    EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE        
    IMPLIED WARRANTY OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      
    PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF         
    THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU      
    ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.     
  
******************************************************************************

---------------------------------
FILE LIST
---------------------------------

NU_M25P128_V10.zip file list:

Lib File:
        lib/def.vhd                            Definition of some utility constants
        lib/data.vhd                           Package for the definition of some useful data and types
        lib/CUIcommandData.vhd                 Package for the definition of the Command Table
        lib/TimingData.vhd                     Package for the definition of some timing constants
        lib/StringLib.vhd                      Package for the definition of some utility functions 
        lib/MemoryLib.vhd                      Package for the management of the memory array
        lib/BlockLib.vhd                       Package for the management of the Blocks


Code File:
        code/M58P128.vhd                       VHDL model of m58pr512j device

Top File:
        top/TestBench.vhd                      Test Bench file

Simulation File:        
        sim/memory_file                        Memory Array of device

Stimuli File:
        stim/Stimuli_SPI.vhd                   Package for the definition of stimuli utility functions and procedure
        stim/M25P128_driver.vhd                Example of Stimuli

-------------
GET SUPPORT
-------------

Please mail any questions, comments or problems you might have concerning
this VHDL Behavioral Model to :

        anna.faldarini@numonyxx.com
        lucia.di-martino@numonyxx.com


If you are having technical difficulties then please include some basic 
information in your email:

        Simulator Version and Operative System you have used; 
        Memory (RAM);
        Free Disk Space on installation drive;

-------------
BUG REPORTS
-------------

If you experience something you think might be a bug in the VHDL
Behavioral Model, please report it by sending a message to :

        anna.faldarini@numonyxx.com
        lucia.di-martino@numonyxx.com

Describe what you did (if anything), what happened
and what version of the Model you have. Please use the form
below for bug reports:

Error message :
Memory (RAM) :
Free Disk Space on installation drive :
Simulator Version and Operative System you have used :
Stimulus source file :
Waveform Image file (gif or jpeg format) :


---------------------------------------
SEND FEEDBACK / REQUESTS FOR FEATURES
---------------------------------------

Please let us know how to improve the behavioral model

        anna.faldarini@numonyx.com
        lucia.di-martino@numonyx.com

--------------
KNOWN ISSUES
--------------


---------------
HOW TO REQUEST?
---------------


