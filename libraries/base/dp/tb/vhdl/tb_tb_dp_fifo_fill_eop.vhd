-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle

-- Purpose:
--   Multi test bench for dp_fifo_fill_eop
-- Description:
--   Runs two tb_dp_fifo_fill_eop testbenches, one for single clock and one for
--   dual clock.
-- Remark:
-- Usage:
-- > as 10
-- > run -all
-- . signal tb_end will stop the simulation by stopping the clk
-- . the tb is self checking
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_fifo_fill_eop is
end tb_tb_dp_fifo_fill_eop;

architecture tb of tb_tb_dp_fifo_fill_eop is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Try FIFO settings
  -- g_dut_use_dual_clock  : boolean := true;
  -- g_dut_use_bsn         : boolean := false;
  -- g_dut_use_empty       : boolean := false;
  -- g_dut_use_channel     : boolean := false;
  -- g_dut_use_sync        : boolean := false;
  -- g_dut_fifo_rl         : natural := 1;  -- internal RL,  use 0 for look ahead FIFO, default 1 for normal FIFO
  -- g_dut_fifo_size       : natural := 128;
  -- g_dut_fifo_fill       : natural := 100;  -- selectable >= 0 for dp_fifo_fill
  -- g_block_size          : natural := 14;
  -- g_dut_use_rd_fill_32b : boolean := false;
  -- g_dut_use_gap         : boolean := true;
  -- g_dut_use_random_ctrl : boolean := true

  u_dut_sc_1             : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => false, g_dut_fifo_rl => 1, g_dut_use_random_ctrl => false);
  u_dut_sc_1_no_gap      : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => false, g_dut_fifo_rl => 1, g_dut_use_random_ctrl => false, g_dut_use_gap => false);
  u_dut_dc_1_no_gap      : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => true,  g_dut_fifo_rl => 1, g_dut_use_random_ctrl => false, g_dut_use_gap => false);
  u_dut_dc_0_no_gap      : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => true,  g_dut_fifo_rl => 0, g_dut_use_random_ctrl => false, g_dut_use_gap => false);
  u_dut_sc_1_rand        : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => false, g_dut_fifo_rl => 1, g_dut_use_random_ctrl => true);
  u_dut_dc_1_rand        : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => true,  g_dut_fifo_rl => 1, g_dut_use_random_ctrl => true);
  u_dut_sc_0_rand        : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => false, g_dut_fifo_rl => 0, g_dut_use_random_ctrl => true);
  u_dut_dc_0_rand        : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => true,  g_dut_fifo_rl => 0, g_dut_use_random_ctrl => true);
  u_dut_sc_1_rand_no_gap : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => false, g_dut_fifo_rl => 1, g_dut_use_random_ctrl => true, g_dut_use_gap => false);
  u_dut_dc_1_rand_no_gap : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => true,  g_dut_fifo_rl => 1, g_dut_use_random_ctrl => true, g_dut_use_gap => false);
  u_dut_sc_0_rand_no_gap : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => false, g_dut_fifo_rl => 0, g_dut_use_random_ctrl => true, g_dut_use_gap => false);
  u_dut_dc_0_rand_no_gap : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => true,  g_dut_fifo_rl => 0, g_dut_use_random_ctrl => true, g_dut_use_gap => false);

  u_dut_sc_1_blk_gt_fill : entity work.tb_dp_fifo_fill_eop generic map (g_dut_use_dual_clock => false, g_dut_fifo_rl => 1, g_dut_use_random_ctrl => false, g_dut_use_gap => false,
                                                                        g_dut_fifo_fill => 10, g_block_size => 20);
end tb;
