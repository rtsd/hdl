-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Reinier vd Walle
-- Purpose : MMS for dp_bsn_monitor_v2
-- Description: See dp_bsn_monitor_v2.vhd
-- --------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.dp_components_pkg.all;

entity mms_dp_bsn_monitor_v2 is
  generic (
    g_nof_streams        : positive := 1;
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and dp_clk are the same, else use TRUE to cross the clock domain
    g_sync_timeout       : natural := c_dp_sync_timeout;
    g_bsn_w              : natural := c_dp_stream_bsn_w;
    g_error_bi           : natural := 0;
    g_cnt_sop_w          : natural := c_word_w;
    g_cnt_valid_w        : natural := c_word_w;
    g_cnt_latency_w      : natural := c_word_w
  );
  port (
    -- Memory-mapped clock domain
    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;
    reg_mosi    : in  t_mem_mosi;
    reg_miso    : out t_mem_miso;

    -- Streaming clock domain
    dp_rst      : in  std_logic;
    dp_clk      : in  std_logic;
    ref_sync    : in  std_logic := '0';  -- ref Sync input

    in_siso_arr : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    in_sosi_arr : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_bsn_monitor_v2;

architecture str of mms_dp_bsn_monitor_v2 is
  constant c_reg_adr_w : natural := c_dp_bsn_monitor_v2_reg_adr_w;

  signal mon_evt_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mon_sync_timeout_arr  : std_logic_vector(g_nof_streams - 1 downto 0);

  signal mon_ready_stable_arr  : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mon_xon_stable_arr    : std_logic_vector(g_nof_streams - 1 downto 0);

  type t_mon_bsn_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(g_bsn_w - 1 downto 0);
  type t_mon_sop_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(g_cnt_sop_w - 1 downto 0);
  type t_mon_val_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(g_cnt_valid_w - 1 downto 0);
  type t_mon_lat_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(g_cnt_latency_w - 1 downto 0);

  signal mon_bsn_at_sync_arr         : t_mon_bsn_arr;
  signal mon_nof_sop_arr             : t_mon_sop_arr;
  signal mon_nof_err_arr             : t_mon_sop_arr;  -- use g_cnt_sop_w, because at maximium all frames have an error
  signal mon_nof_valid_arr           : t_mon_val_arr;
  signal mon_latency_arr             : t_mon_lat_arr;

  signal reg_mosi_arr                : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr                : t_mem_miso_arr(g_nof_streams - 1 downto 0);
begin
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_reg_adr_w
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  gen_stream : for i in 0 to g_nof_streams - 1 generate
    u_reg : entity work.dp_bsn_monitor_reg_v2
    generic map (
      g_cross_clock_domain => g_cross_clock_domain
    )
    port map (
      -- Clocks and reset
      mm_rst                  => mm_rst,
      mm_clk                  => mm_clk,
      st_rst                  => dp_rst,
      st_clk                  => dp_clk,

      -- Memory Mapped Slave in mm_clk domain
      sla_in                  => reg_mosi_arr(i),
      sla_out                 => reg_miso_arr(i),

      -- MM registers in dp_clk domain
      -- . control
      mon_evt                 => mon_evt_arr(i),
      mon_sync_timeout        => mon_sync_timeout_arr(i),
      -- . siso
      mon_ready_stable        => mon_ready_stable_arr(i),
      mon_xon_stable          => mon_xon_stable_arr(i),
      -- . sosi
      mon_bsn_at_sync         => mon_bsn_at_sync_arr(i),
      mon_nof_sop             => mon_nof_sop_arr(i),
      mon_nof_err             => mon_nof_err_arr(i),
      mon_nof_valid           => mon_nof_valid_arr(i),
      mon_latency             => mon_latency_arr(i)
    );

    u_mon : entity work.dp_bsn_monitor_v2
    generic map (
      g_sync_timeout  => g_sync_timeout,
      g_error_bi      => g_error_bi
    )
    port map (
      rst                    => dp_rst,
      clk                    => dp_clk,

      -- ST interface
      in_siso                => in_siso_arr(i),
      in_sosi                => in_sosi_arr(i),
      ref_sync               => ref_sync,

      -- MM interface
      -- . control
      mon_evt                 => mon_evt_arr(i),  -- pulses when new monitor data is available regarding the previous sync interval
      mon_sync                => OPEN,
      mon_sync_timeout        => mon_sync_timeout_arr(i),
      -- . siso
      mon_ready_stable        => mon_ready_stable_arr(i),
      mon_xon_stable          => mon_xon_stable_arr(i),
      -- . sosi
      mon_bsn_at_sync         => mon_bsn_at_sync_arr(i),
      mon_nof_sop             => mon_nof_sop_arr(i),
      mon_nof_err             => mon_nof_err_arr(i),
      mon_nof_valid           => mon_nof_valid_arr(i),
      mon_latency             => mon_latency_arr(i)
    );
  end generate;
end str;
