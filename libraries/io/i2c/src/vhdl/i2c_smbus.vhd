-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, common_lib;
use ieee.std_logic_1164.all;
--USE IEEE.numeric_std.ALL;
use ieee.std_logic_arith.all;
use common_lib.common_pkg.all;
use work.i2c_pkg.all;
use work.i2c_smbus_pkg.all;

entity i2c_smbus is
  generic (
    g_i2c_phy                 : t_c_i2c_phy;
    g_clock_stretch_sense_scl : boolean := false
  );
  port (
    -- GENERIC Signal
    gs_sim      : in boolean := false;
    clk         : in std_logic;
    rst         : in std_logic;
    in_dat      : in std_logic_vector(c_byte_w - 1 downto 0);
    in_req      : in std_logic;
    out_dat     : out std_logic_vector(c_byte_w - 1 downto 0);
    out_val     : out std_logic;
    out_err     : out std_logic;
    out_ack     : out std_logic;
    st_idle     : out std_logic;
    st_end      : out std_logic;
    scl         : inout std_logic;
    sda         : inout std_logic
  );
end entity;

architecture rtl of i2c_smbus is
  constant c_i2c_octet_sz : natural := 9;

  -- CONSTANT Signals that depend on GENERIC Signal gs_sim
  signal cs_clk_cnt       : unsigned(15 downto 0);
  signal cs_comma_w       : natural;

  signal pid         : integer range 0 to 255;
  signal nxt_pid     : integer range 0 to 255;
  signal pix         : natural range 0 to SMBUS_PROTOCOL'high;
  signal nxt_pix     : natural;
  signal adr         : std_logic_vector(6 downto 0);
  signal nxt_adr     : std_logic_vector(6 downto 0);
  signal cnt         : integer range 0 to 255;
  signal nxt_cnt     : integer range 0 to 255;

  signal op          : OPCODE;
  signal nxt_op      : OPCODE;

  signal rdy         : std_logic;
  signal nrst        : std_logic;
  signal srst        : std_logic_vector(0 to 2);
  signal nxt_srst    : std_logic_vector(srst'range);

  signal scl_i       : std_logic;
  signal sda_i       : std_logic;
  signal sda_i_reg   : std_logic;
  signal scl_o       : std_logic;
  signal sda_o       : std_logic;
  signal scl_oen     : std_logic;
  signal sda_oen     : std_logic;
  signal scl_oe      : std_logic;
  signal scl_oe_comma_reg : std_logic;
  signal sda_oe      : std_logic;
  signal sda_oe_reg  : std_logic;

  -- Insert comma after start and after each octet to support slow I2C slave
  signal scl_cnt      : integer range 0 to c_i2c_octet_sz;
  signal nxt_scl_cnt  : integer range 0 to c_i2c_octet_sz;
  signal scl_m        : std_logic;  -- master driven scl, used to detect scl edge change independent of slow scl
  signal nxt_scl_m    : std_logic;
  signal sda_m        : std_logic;  -- master driven sda, used to detect scl edge change independent of slow sda
  signal nxt_sda_m    : std_logic;
  signal scl_m_dly    : std_logic;
  signal sda_m_dly    : std_logic;
  signal start_detect : std_logic;
  signal octet_end    : std_logic;
  signal scl_m_n      : std_logic;
  signal comma_hi     : std_logic;
  signal comma        : std_logic;
  signal comma_dly    : std_logic;
  signal comma_evt    : std_logic;
  signal comma_sc_low : std_logic;
  signal comma_sc_low_yes : std_logic;  -- drives comma_sc_low when comma time is supported
  signal comma_sc_low_no  : std_logic;  -- drives comma_sc_low when comma time is not supported

  signal i2c_start   : std_logic;
  signal i2c_stop    : std_logic;
  signal i2c_read    : std_logic;
  signal i2c_write   : std_logic;
  signal i2c_ack_in  : std_logic;
  signal i2c_dat_in  : std_logic_vector(7 downto 0);
  signal i2c_dat_out : std_logic_vector(7 downto 0);
  signal i2c_cmd_ack : std_logic;
  signal i2c_ack_out : std_logic;

  signal i_out_dat   : std_logic_vector(out_dat'range);
  signal nxt_out_dat : std_logic_vector(out_dat'range);
  signal nxt_out_val : std_logic;
  signal i_out_err   : std_logic;
  signal nxt_out_err : std_logic;

  signal op_val      : std_logic;
  signal nxt_op_val  : std_logic;

  signal ld_to         : std_logic;
  signal to_index      : natural range 0 to c_smbus_timeout_nof_byte-1;
  signal nxt_to_index  : natural range 0 to c_smbus_timeout_nof_byte-1;
  signal to_cnt_en     : std_logic;
  signal to_cnt        : std_logic_vector(c_smbus_timeout_w - 1 downto 0);
  signal to_value      : std_logic_vector(c_smbus_timeout_word_w - 1 downto 0);
  signal nxt_to_value  : std_logic_vector(c_smbus_timeout_word_w - 1 downto 0);
  signal timeout       : std_logic;
  signal nxt_timeout   : std_logic;

  -- Attributes for Quartus. Fixes the issues:
  -- - For Arria10* it solves the I2C issue: bidirectional pin drive out '1' instead of 'Z'
  -- - Removes the Warning (12620): Input port OE of I/O output buffer is not connected, but the atom is driving a bi-direct...
  attribute keep: boolean;
  attribute keep of scl_o: signal is true;
  attribute keep of sda_o: signal is true;
begin
  -- CONSTANT Signals dependent on GENERIC Signal gs_sim
  cs_clk_cnt <= conv_unsigned(g_i2c_phy.clk_cnt, cs_clk_cnt'length) when gs_sim = false else
                conv_unsigned(c_i2c_clk_cnt_sim, cs_clk_cnt'length);
  cs_comma_w <= g_i2c_phy.comma_w when gs_sim = false else c_i2c_comma_w_dis;

  out_dat <= i_out_dat;
  out_err <= i_out_err;

  scl_oe <= not scl_oen;
  sda_oe <= not sda_oen;

  scl <= scl_o when scl_oe_comma_reg = '1' else 'Z';  -- note scl_o is fixed '0' in i2c_bit
  sda <= sda_o when sda_oe_reg       = '1' else 'Z';  -- note sda_o is fixed '0' in i2c_bit

  -- Use internal scl to avoid edge detection problems in i2c_byte which may occur due to large speed difference
  -- between the system clock (200 MHz) and the rising edge of the I2C pull up. This effectively also disables
  -- support for slow slave or multi master bus arbitriation, but that is acceptable, because these features are
  -- not used for RCU and TDS in LOFAR.

  gen_i2c_smbus_scl_oe_comma_reg : if g_clock_stretch_sense_scl = false generate
    scl_i <= not scl_oe_comma_reg;
  end generate;

  gen_i2c_smbus_scl : if g_clock_stretch_sense_scl = true generate
    scl_i <= scl;
  end generate;

  sda_i <= sda;

  nrst <= not rst;

  p_srst : process(rst, clk)
  begin
    if rst = '1' then
      srst             <= (others => '1');
      scl_oe_comma_reg <= '0';
      sda_oe_reg       <= '0';
      sda_i_reg        <= '0';
    elsif rising_edge(clk) then
      srst             <= '0' & srst(0 to srst'high - 1);
      scl_oe_comma_reg <= scl_oe or comma_sc_low;
      sda_oe_reg       <= sda_oe;
      sda_i_reg        <= sda_i;  -- sample SDA line
    end if;
  end process;

  byte : entity work.i2c_byte
  generic map (
    g_clock_stretch_sense_scl => g_clock_stretch_sense_scl
  )
  port map (
    clk         => clk,
    rst         => srst(srst'high),
    ena         => '1',
    clk_cnt     => cs_clk_cnt,
    nReset      => nrst,
    read        => i2c_read,
    write       => i2c_write,
    start       => i2c_start,
    stop        => i2c_stop,
    ack_in      => i2c_ack_in,
    cmd_ack     => i2c_cmd_ack,
    Din         => i2c_dat_in,
    Dout        => i2c_dat_out,
    ack_out     => i2c_ack_out,
    scl_i       => scl_i,
    scl_o       => scl_o,
    scl_oen     => scl_oen,
    sda_i       => sda_i,
    sda_o       => sda_o,
    sda_oen     => sda_oen
  );

  comma_sc_low <= comma_sc_low_no when cs_comma_w = 0 else comma_sc_low_yes;

  no_comma : if g_i2c_phy.comma_w = 0 generate
    comma_sc_low_no <= '0';
  end generate;

  gen_comma : if g_i2c_phy.comma_w > 0 generate
    p_comma_reg : process(rst, clk)
    begin
      if rst = '1' then
        scl_m     <= '0';
        sda_m     <= '0';
        scl_m_dly <= '0';
        sda_m_dly <= '0';
        scl_cnt   <= 0;
        comma_dly <= '0';
     elsif rising_edge(clk) then
        scl_m     <= nxt_scl_m;
        sda_m     <= nxt_sda_m;
        scl_m_dly <= scl_m;
        sda_m_dly <= sda_m;
        scl_cnt   <= nxt_scl_cnt;
        comma_dly <= comma;
     end if;
    end process;

    nxt_scl_m <= scl_oen;
    nxt_sda_m <= sda_oen;

    start_detect <= scl_m_dly and scl_m and sda_m_dly and not sda_m;
    octet_end <= '1' when scl_cnt = c_i2c_octet_sz else '0';

    bit_count : process (scl_cnt, start_detect, octet_end, scl_m_dly, scl_m)
    begin
      nxt_scl_cnt <= scl_cnt;
      if start_detect = '1' or octet_end = '1' then
        nxt_scl_cnt <= 0;
      elsif scl_m_dly = '0' and scl_m /= '0' then
        nxt_scl_cnt <= scl_cnt + 1;
      end if;
    end process;

    comma_hi <= start_detect or octet_end;
    scl_m_n <= not scl_m;

    u_comma : entity common_lib.common_switch
    generic map (
      g_rst_level => '0'
    )
    port map (
      clk         => clk,
      rst         => rst,
      switch_high => comma_hi,
      switch_low  => scl_m_n,
      out_level   => comma
    );

    comma_evt <= not comma and comma_dly;

    u_comma_sc_low : entity common_lib.common_pulse_extend
    generic map (
      g_rst_level    => '0',
      g_p_in_level   => '1',
      g_ep_out_level => '1',
      g_extend_w     => g_i2c_phy.comma_w
    )
    port map (
      rst     => rst,
      clk     => clk,
      p_in    => comma_evt,
      ep_out  => comma_sc_low_yes
    );
  end generate;  -- gen_comma

  regs : process(rst, clk)
  begin
    if rst = '1' then
      pix       <= 0;
      pid       <= 0;
      cnt       <= 0;
      op        <= OP_IDLE;
      op_val    <= '1';
      adr       <= (others => '0');
      i_out_dat <= (others => '0');
      i_out_err <= '0';
      out_val   <= '0';
      to_index  <= 0;
      to_value  <= (others => '0');
      timeout   <= '0';
   elsif rising_edge(clk) then
      pix       <= nxt_pix;
      pid       <= nxt_pid;
      op        <= nxt_op;
      op_val    <= nxt_op_val;
      cnt       <= nxt_cnt;
      adr       <= nxt_adr;
      i_out_dat <= nxt_out_dat;
      i_out_err <= nxt_out_err;
      out_val   <= nxt_out_val;
      to_index  <= nxt_to_index;
      to_value  <= nxt_to_value;
      timeout   <= nxt_timeout;
   end if;
  end process;

  get_op : process(pid, pix, op, rdy)
  begin
    nxt_pix <= pix;
    nxt_op_val <= '1';
    if op = OP_IDLE and rdy = '1' then
      nxt_pix    <=  0;
      nxt_op_val <= '0';
    elsif rdy = '1' then
      nxt_pix  <= pix + 1;
      nxt_op_val <= '0';
    end if;

    if pid > c_smbus_unknown_protocol then
      nxt_op <= SMBUS_PROTOCOLS(SMBUS_C_END)(pix);
    else
      nxt_op <= SMBUS_PROTOCOLS(pid)(pix);
    end if;
  end process;

  decode_opcode : process(adr, cnt, to_index, to_value, i_out_dat, i_out_err, pid,
                          op_val, op, in_req, in_dat, sda_i_reg,
                          i2c_cmd_ack, i2c_ack_out, i2c_dat_out, timeout)
  begin
    nxt_adr <= adr;
    nxt_cnt <= cnt;

    -- default values
    i2c_start    <= '0';
    i2c_stop     <= '0';
    i2c_read     <= '0';
    i2c_write    <= '0';
    i2c_ack_in   <= '0';
    i2c_dat_in   <= (others => '0');

    nxt_to_index <= to_index;
    nxt_to_value <= to_value;
    ld_to        <= '0';

    -- default output values
    nxt_out_dat <= i_out_dat;
    nxt_out_err <= i_out_err;
    nxt_out_val <= '0';
    out_ack <= '0';
    st_idle <= '0';
    st_end  <= '0';

    nxt_pid <= pid;

    rdy <= '0';

    -- generate the i2c control signals
    if op_val = '1' then
      case op is
        when OP_IDLE =>
          st_idle <= '1';
          if in_req = '1' then
            nxt_pid      <= conv_integer(unsigned(in_dat));
            nxt_out_err  <= '0';
            out_ack      <= '1';
            rdy          <= '1';
          end if;

        when OP_RD_SDA =>
          if in_req = '1' then
            nxt_out_err  <= not To_X01(sda_i_reg);  -- expect pull up
            rdy          <= '1';
          end if;

        when OP_END =>
          st_end  <= '1';
          out_ack <= '1';
          rdy     <= '1';

        when OP_LD_ADR =>
          if in_req = '1' then
            nxt_adr <= in_dat(6 downto 0);
            out_ack <= '1';
            rdy     <= '1';
          end if;

        when OP_LD_CNT =>
          if in_req = '1' then
            nxt_cnt <= conv_integer(unsigned(in_dat));
            out_ack <= '1';
            rdy     <= '1';
          end if;

        when OP_WR_ADR_WR  =>
          if i2c_cmd_ack = '1' then
            nxt_out_err  <= To_X01(i2c_ack_out);
            rdy          <= '1';
          else
            i2c_start    <= '1';
            i2c_write    <= '1';
            i2c_dat_in   <= adr & '0';
          end if;

        when OP_WR_ADR_RD  =>
          if i2c_cmd_ack = '1' then
            nxt_out_err  <= To_X01(i2c_ack_out);
            rdy          <= '1';
          else
            i2c_start    <= '1';
            i2c_write    <= '1';
            i2c_dat_in   <= adr & '1';
          end if;

        when OP_WR_CNT  =>
          if i2c_cmd_ack = '1' then
            nxt_out_err  <= To_X01(i2c_ack_out);
            rdy          <= '1';
          else
            i2c_write    <= '1';
            i2c_dat_in   <= std_logic_vector(conv_unsigned(cnt, 8));
          end if;

        when OP_WR_DAT  =>
          if i2c_cmd_ack = '1' then
            nxt_out_err  <= To_X01(i2c_ack_out);
            out_ack      <= '1';
            rdy          <= '1';
          else
            if in_req = '1' then
              i2c_write  <= '1';
              i2c_dat_in <= in_dat;
            end if;
          end if;

        when OP_WR_BLOCK  =>
          if i2c_cmd_ack = '1' then
            nxt_out_err  <= To_X01(i2c_ack_out);
            out_ack      <= '1';
            nxt_cnt      <= cnt - 1;
            if cnt = 1 then
              rdy <= '1';
            end if;
          else
            if in_req = '1' then
              i2c_write  <= '1';
              i2c_dat_in <= in_dat;
            end if;
          end if;

        when OP_RD_ACK  =>
          if i2c_cmd_ack = '1' then
            nxt_out_dat  <= To_X01(i2c_dat_out);
            nxt_out_val  <= '1';
            rdy          <= '1';
          else
            i2c_read     <= '1';
            i2c_ack_in   <= '0';
          end if;

        when OP_RD_NACK  =>
          if i2c_cmd_ack = '1' then
            nxt_out_dat  <= To_X01(i2c_dat_out);
            nxt_out_val  <= '1';
            rdy          <= '1';
          else
            i2c_read     <= '1';
            i2c_ack_in   <= '1';
          end if;

        when OP_RD_BLOCK  =>
          if i2c_cmd_ack = '1' then
            nxt_out_dat  <= To_X01(i2c_dat_out);
            nxt_out_val  <= '1';
            nxt_cnt      <= cnt - 1;
            if cnt = 1 then
              rdy <= '1';
            end if;
          else
            i2c_read     <= '1';
            if cnt = 1 then
              i2c_ack_in <= '1';
            else
              i2c_ack_in <= '0';
            end if;
          end if;

        when OP_STOP =>
          if i2c_cmd_ack = '1' then
            rdy          <= '1';
          else
            i2c_stop     <= '1';
          end if;

        when OP_LD_TIMEOUT =>
          if in_req = '1' then
            for i in 0 to c_smbus_timeout_nof_byte-1 loop
              if i = to_index then
                nxt_to_value(7 + i * 8 downto i * 8) <= in_dat;
              end if;
            end loop;
            if to_index = c_smbus_timeout_nof_byte-1 then
              nxt_to_index <= 0;
              ld_to <= '1';
            else
              nxt_to_index <= to_index + 1;
            end if;
            out_ack <= '1';
            rdy     <= '1';
          end if;

        when OP_WAIT =>
          if timeout = '1' then
            nxt_to_index <= 0;
            rdy          <= '1';
          end if;

        when others =>
          nxt_out_err <= '1';
          rdy         <= '1';
          report "Illegal opcode ";
      end case;
    end if;
  end process;

  to_cnt_en <= not timeout;

  u_timeout : entity common_lib.common_counter
  generic map(
    g_width     => to_cnt'length
  )
  port map(
    rst     => rst,
    clk     => clk,
    cnt_clr => ld_to,
    cnt_en  => to_cnt_en,
    count   => to_cnt
  );

  nxt_timeout <= '1' when unsigned(to_cnt) > unsigned(to_value(to_cnt'range)) else '0';
end architecture;
