-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_distribute is
end tb_tb_dp_distribute;

architecture tb of tb_tb_dp_distribute is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 10
  -- > run -all --> OK

  --                                                                           in_en,    out_ready, nof_repeat, code_channel_lo, tx_use_fifo, tx_fifo_fill, rx_use_fifo, rx_fifo_fill, data_init_offset, frame_len_offset, nof_input, nof_serial, transpose
  -- Different n --> m serial m --> n combinations
  u_rnd_rnd_1_1                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                1,         1,          true);
  u_rnd_rnd_1_2                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                1,         2,          true);
  u_rnd_rnd_2_1                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                2,         1,          true);
  u_rnd_rnd_3_1                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                3,         1,          true);
  u_rnd_rnd_1_3                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                1,         3,          true);
  u_rnd_rnd_3_2                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                3,         2,          true);
  u_rnd_rnd_2_3                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                2,         3,          true);
  u_rnd_rnd_4_4                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                4,         4,          true);
  u_rnd_rnd_4_4_wires              : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                4,         4,          false);

  -- Different flow control for 4 --> 3 serial 3 --> 4
  u_rnd_rnd_4_3                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_rnd_rnd_3_4                    : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);
  u_rnd_act_4_3                    : entity work.tb_dp_distribute generic map (e_random, e_active,  50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_rnd_act_3_4                    : entity work.tb_dp_distribute generic map (e_random, e_active,  50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);
  u_rnd_pls_4_3                    : entity work.tb_dp_distribute generic map (e_random, e_pulse,   50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_rnd_pls_3_4                    : entity work.tb_dp_distribute generic map (e_random, e_pulse,   50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);

  u_act_rnd_4_3                    : entity work.tb_dp_distribute generic map (e_active, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_act_rnd_3_4                    : entity work.tb_dp_distribute generic map (e_active, e_random,  50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);
  u_act_act_4_3                    : entity work.tb_dp_distribute generic map (e_active, e_active,  50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_act_act_3_4                    : entity work.tb_dp_distribute generic map (e_active, e_active,  50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);
  u_act_pls_4_3                    : entity work.tb_dp_distribute generic map (e_active, e_pulse,   50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_act_pls_3_4                    : entity work.tb_dp_distribute generic map (e_active, e_pulse,   50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);

  u_pls_rnd_4_3                    : entity work.tb_dp_distribute generic map (e_pulse,  e_random,  50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_pls_rnd_3_4                    : entity work.tb_dp_distribute generic map (e_pulse,  e_random,  50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);
  u_pls_act_4_3                    : entity work.tb_dp_distribute generic map (e_pulse,  e_active,  50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_pls_act_3_4                    : entity work.tb_dp_distribute generic map (e_pulse,  e_active,  50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);
  u_pls_pls_4_3                    : entity work.tb_dp_distribute generic map (e_pulse,  e_pulse,   50,         false,           false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_pls_pls_3_4                    : entity work.tb_dp_distribute generic map (e_pulse,  e_pulse,   50,         false,           false,       0,            false,       0,            10,               0,                3,         4,          true);

  -- Different frame lengths per input for 4 --> 3 serial 3 --> 4
  u_rnd_rnd_4_3_len                : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            0,                20,               4,         3,          true);
  u_rnd_rnd_3_4_len                : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            0,                20,               3,         4,          true);
  u_rnd_rnd_4_4_len_wires          : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           false,       0,            false,       0,            0,                20,               4,         4,          false);

  -- Using FIFOs for 4 --> 3 serial 3 --> 4
  u_rnd_rnd_4_3_fifo_tx            : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           true,        15,           false,       0,            10,               0,                4,         3,          true);
  u_rnd_rnd_3_4_fifo_tx            : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           true,        15,           false,       0,            10,               0,                3,         4,          true);
  u_rnd_rnd_4_4_fifo_tx_wires      : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           true,        15,           false,       0,            10,               0,                4,         4,          false);
  u_rnd_rnd_4_3_fifo_tx_rx         : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           true,        15,           true,        15,           10,               0,                4,         3,          true);
  u_rnd_rnd_3_4_fifo_tx_rx         : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           true,        15,           true,        15,           10,               0,                3,         4,          true);
  u_rnd_rnd_4_4_fifo_tx_rx_wires   : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         false,           true,        15,           true,        15,           10,               0,                4,         4,          false);

  -- Using DP packet coding for 4 --> 3 serial 3 --> 4
  u_rnd_rnd_4_3_coding             : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         true,            false,       0,            false,       0,            10,               0,                4,         3,          true);
  u_rnd_rnd_3_4_coding             : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         true,            false,       0,            false,       0,            10,               0,                3,         4,          true);
  u_rnd_rnd_3_4_coding_wires       : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         true,            false,       0,            false,       0,            10,               0,                4,         4,          false);
  u_rnd_rnd_4_3_coding_fifo_tx     : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         true,            true,        15,           false,       0,            10,               0,                4,         3,          true);
  u_rnd_rnd_3_4_coding_fifo_tx     : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         true,            true,        15,           false,       0,            10,               0,                3,         4,          true);
  u_rnd_rnd_4_3_coding_fifo_tx_rx  : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         true,            true,        15,           true,        15,           10,               0,                4,         3,          true);
  u_rnd_rnd_3_4_coding_fifo_tx_rx  : entity work.tb_dp_distribute generic map (e_random, e_random,  50,         true,            true,        15,           true,        15,           10,               0,                3,         4,          true);
end tb;
