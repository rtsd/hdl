// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
Q06Ha0mp8wIvuiNr6bHqukAfgs1ClUL26NMGLMHpunw1PQQPCz6edzTG73zfSiJM
Aj/LewGvm6VB7iQ1tKRCqu51i+Zeo3EPb8leRUc785g0+H164CYURoU8URWI4aGl
1zvGq+AZ+d9AWhMhde8CiPqXo373a5cwTTNy4HdZ9Gs=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 6432 )
`pragma protect data_block
jaB2fdkYgNPQX0dVeTerkuo9OgEiQZUZxdqxKLkDs3wt/S7SqR1A7Iu+F5suyrm2
jYatcqjjtr719D92fk/NamGu6NDRQ6WWcm/uOBoLsnwFKaLlqShjb2DVUBy8q5Q8
rv/yhPn2nciWBxpzJPZn408TRRf1tlQktHsAHezr1pdTdUI6Wy0rcCuodCUe7R8h
dWMFcyA4yaGjE/cfmSpOS82hnOz+wh4M1ELSrjBVKYEn1jwjnvypnoywSkMSXHql
xGlsYl3SnYhf5j+XMqBTwwJrcMrRuh6LDpbEKEMBMI4jxzXUR9JgOJG0KHCDdvJP
XxAYLNyKHdlCyQhk0xx6KVMLyaHy7Xr6FOkea+VdufxW5D7agWVPVB+cCn662mKv
mkxM3FCZSLRyCdZ7vJuW7UKPpxrVN+NhW+o0R4IPpq+JozL/J9SD/zbBVJF99ePx
+ideXNsHLFOOmaQsYLZk+yoRkZLo9fyF9uLfe5HrbVRGznjFb0wkKDIbi/c05zXF
8pVoiLtaANIX3rZogjqMZEPFfRYhw1tOWK7lYCM9P2T38ItkrlCoYYDMXN8eUc0U
ienpvkS0LueWKSmZe+Y009ImjSY99pbr1Lkym0G461sPIsUpDtMc7tG/QGmfPIao
YuskkxWSmasWoJ//L1y4xMraezSiW2xq6Lb+JeWR3VcU+EMM71Klt2i0azPh8MG8
nH2ewGKZszewzcZY0QeiZZc0z4Kv2BMZ5913wheiZraWHTzMLADLBkGif6OHcZ0K
8WJi7jbywC19P06O2rpA7ZXc/m350JVbLSrSXczW63VM21SQS3N/KPG63Mi8haxu
A4qy50WYrGiuOSNNotnGtXYapQcAM5dkkZsKOhIkg2zm3nhA/XzcGOoJMtzFvbQu
bA7O0t21O1G8x8S22DL02Pny8M1Db8v3c17sg6WHweZAds+Em3gfHz/er8CIIDZJ
C1+T4vQ1nOGjUiRV+03bBaeXhMpiiDj0kDtQ+pfixxnA52POxmhgGZJB4YXH8WHh
I+CEdRiLLZoWa407WqYB3l755F3Zt6cHjB+p2eEc0rVe62I1LHTqtwBEpaEJ2p/e
Qt9Odqk9rKcjjj8L+ekM7vhOTKNZs61KcjgpoCQDtZNZ6gxVi65A9njkwZxVpJ2+
MRUQ3MObHTzr+k9zaE2bMWPt/9EfIOdou1Jw/ypSLziq1Ti5Dx7LTYL5gpZbPUvJ
cNNmkv7K1pKWHHTDnVyaOWEhmouCua/vziQgDqCorGY+EMCr+viSmW2hm+eNAOag
RG76ImxJYy1HuB/uMkrcikeEJK77Jcca3W+vFoa/UpavmPzNj/ec+ks87ujadt+q
JHws5+RG8iXShIb5XUx2vfpeQpRFTqIYCGNpoKpwQ9v5yrUNJ8vJmVQlKXLemfWp
34T2SjRCo/OHkSyxwVZ5SSWKzulMaUKFnqMlzspW4ZV7IdCcPKSI9XAx1e/zxBCH
84xCY0Wq03TaLfC99LzroTD+ReiLDUAUDqmEGf4AHO0XUeaW6RjwYBWQ0u62T0+e
aIppgvZA0qmKna205/Y3Ff8JVBVzoSehxwqUz+NaRbgTEiiy68nVwiqRnCGc6Hi7
BDel0IUTwHvB6hV7MOvAA+Q7P7XJXa5Rt6rU2oBIh6jO6yRiMABya8lLxbvubDTT
ZrQ7GALxZnPBa+X1GUe2pKjTqv8+Hz6qPjG68uxponHx9GP6jqM/3okIM5PQkiB7
3oWGrl0s9MEcySGKebPCBY6ut7J6FUN06vYYdsTq/xe492VqtDEG1gKlMOq64jJB
j6RYtqN+jsHMT7eqjndf3+xlS3QgQBNKcuLozYPJ25Fl3ULXKXD4eddhWEO0ff0J
degGhbZB1vHtCQm/mkxRwN75jCQR8wzvKR23AF0YSC2vbVoL1Ippgsg9TxVBY4EL
FXk40pL4qJPP6gMmjk7AU2BDFJd6CHyWguhFQu4vtmo3OmXobGNh6aKV8PXjrQYp
7BMNBlbIbQNTi5pqfzQiaIM4HBOiryvV7TsXD5GA6OiC+o4Z5+JORlQpJrPjc3mN
63UDprWTCI98ohUEDTy/SKV0aqAB8cwTe+J5vAgHSZn357Ei1iknFKSU554KgMXY
IUr2YfkLllRo9O7oYD/qAtE0CjNLeTDiCXveE1uChDqEh3COhOOHYaPoQC/kk2fF
7gJzZsHnbeXWzj2BUSvYkdAqgrrhCUUicKMbNcjA6tMmZs2cYMcJ8LkbDVXg7c5N
QHj7D+fof9BpznGFjuNcEMctCg9e9Wv197PYmA0/mQrKXeso27BHlyTC8pexnfFN
mQHcPCnTSndtgBsF65F4wyxeYXyTVOaGriMPvJEdLYPMta4w//driS4vMhy6eng2
FPCAYM9dHhR2gJt/Yz148wp9B6zKHl+qxuV+aVCzoAwH4vMjG4yPrsQD5t2GrmEi
ghgF7VB9Znkc5UPEzaUHfbda1fQsYFTQvPDpZyo+Q0eD5C2FysjYzwHnYErwkaE2
58vzPWwDMoEIbTTg+ARanFkTEMS1Cp/1LtW6u32qsvfzyA6l6dnNCg1MWrZ8uBx7
nscw1atwbSRxki3i5OdxqcALZHoNr1R8BUS7CEU98Fn09xSpV4kqL5Nqqi8GbsSS
Xu3yOFWrj93nARNBbtE2nDz0Ovm/NJU8JRZ2yrg2SLCj1Vc7VFv6lZnGNPG/zfAI
fC6aTajOqyKanus/IGTXP85tirjF2Z29w64NB/8Z1/gpTiG6aev2ZvisAFfx4wyo
CgxWl+8zDRrkzhdabLD5QHkSwTHo3Mnf7YAz9xSAGSLqrO5xD5Hp40Q8ELci9pIZ
crz3V0o2BC0XZEHaUtibmLA63u5f3dibOhBeHeSS0rZ0jxJWcdjDJ2xVxiWUrr1V
AfRSyb+vRs8TrK+m+X4e2Q+Aon7L2+nv76QrZ5u4v5evkmYVdxoQbZqtugNzYlRg
lVjLC0Zr17H0npV95vQNdO7xBSaW4c8Ql1D7siGN+5F9U4peNktUygZUrmxE+kqo
o4b0V6yDdnut0xLT43nVilfn5YnA2BYTE04UgZljGLnpX07CAawpTlEvfDk5H536
32ijjbjj74TLVRf+tVL68c0TTt7L9+JkmNRk5Kw7/hqqnV/nEuAU7hJG2KLBAx6E
/3JppV2AFupX2RpI+bNZbc57GQMvTZmH5ann2znTT2vSi+O64TWE5wcMplksppf9
wpIWiwyo0k232b5/Pm8x3FkPrfWcDOtGLgaOFN2lNPuVaKGDDgPwkciKHi48LKPl
Wujigfie1ASyYw+n9z2/ldQ9sXJfVFDl6bjuLIbpdcfbq8ha2v0zNwdLLlscl1W8
dQB6yr6DTG8Cq0wdIgRPPugcTl2VfMbzDf9+HaMVhBB1T6QSoLy4UoLDz/xrwtV4
AIGlNRW2sDvYh8OrgxdVexIZflXOaGXl+LRwB+oW4wLUiU0GIwDIfgZhlP73WDUt
8deW81MbQRC9TLDGBtcjjj63dByWULNQmcqpnEvfphSzvgbGCOVcbg3zc/XtXH5K
VY44GQ68rD8+NagRGwGCpBi8aaQE+TVIMM7SSN4d1fDoVbg5u//BG91a5EpfP3Td
vLOhwgPkdXg3TiRCP7Ncu44SMRq/yF1TLlUCS8yrW0uW/U5WDKHM70z99ch6wQrp
ehlF1dgKdcID8GPqQEzJxqdmK1LJ5eoTUGCF1UsBy6f/IQORyot6jXRiOgf0csMc
oRDXhbLvQzPhKk4+ypWfC5ZtIkW4eU2EUtSN4f3Wv9O1A8rJ3U4hpdDchZ34+Nkq
b19+jGAFuQd5OA/fdoiY3zsgfVjR4snv+A10XMhBRGzdzPBSiSEaWiWBDcKcaiVM
UN+UQxLeT36pC/CbzcD3xijgC0gAOurJ3ssjzYFKmh/1XzfOcbI0KTR1X07TXLg0
cMu6mKi10NxlfgLP0dAqMasP8C0mxfAlGs64O/zlqle9QMd8ds4iRMm0CHnebTll
058kDoQimBPx+sFn60dyfGwAimFCkT8041RXzMMYA4oh5ih0HP7CMLl6j2r388jy
w6h41lfeZdp/tGglPqEiAp6UQNxOFFZm23eSwXk4QdPcjaRc4leaOyYeHy2vQmej
sP/4bRK5TiIq8Jzg5TOLqo7lGjkm8Lm+o8KtwnXDjIp2FMRHiN89IQh4b0N8DMrD
JBeYhp6xrH9HvfdJxLQ2rpj/ijWbVZBxyh1ugIlcfcJ5jUjwALuL7cc8JdZMrfLM
MdU9116DF21zjmaq2acN5KoDgYeV2jGM3l9TqslQpthafm1+LzTNqHyhi/ThqvSq
9KfnPHgZmZtUFBj0I6Ct4ErGpRICZ6Wjr3IasMp4MeJSCNsjXx+XdP41FUjou36R
gvMJxwIOM5LFfEo8Tw6/Ru0a3rI5VmSj2j8NK9SeGROoLnlUPSP5NWsCc3Aj9TvF
gjFPDrZ62AbKrSUxhkqVlh9w7BErc+AEWMd3k0lJiUa2dcm1B4p2Ddxy9wfpQFyv
aN2U9+6P10XDyyUhgc8tXe7jxKHqLNFwS5ktloAa+FbqG5hZUXc9kl5WwtgzkS4e
51fr+ExUoIOCZgmz8jImJNfEwr6zvRPDZQjZm4mTBx1CEv+fmqEChSR0RT4OEsp9
6/6nxFzEsuMBTCnpyQnqjabLVFZSR37aLKpjoDnJlCR0UEgPMGYgBg1RnU9mlJ95
uyOn8+1nnrUEO0jnXujJ4+ZdCDFPogvlpGwT5F8VQVlLxxi5PToZdzt+bNZT25VA
4RV1+Kx20VBsmpip8EFmwrvWf7VOYnKaf0AjYSrF9fBKABLNlZImMMFLJy+iFXKA
gcZ90NpUvrQp8od4vmHxgjGNphekOdCj4TM7DVphTcERggL/kI34a7bnp43nCmV/
uJVQuhzsgZ7QPgHhccoRDURFdtFqSSfxhplsH2DhJdukueDrdRPyF+UbMwi/n2WD
1+VehYqiUGGD1zhVcQ3cgQ20xZLyKj89MdAzXlU9QWTRshxT/YTt5K2NhyQU2ctX
iPa69ENxeDPOUYTxitighE7Pf9t5EMGAXYvjjA2U6Lg/CMKzGUc9HcuT/n2lZLvD
/RRfUMKBrAhXvP9840cA4iTywdUl0fF26eY0xjJmmvIc1qw9p6wBQrVNYEBDQV+D
CDJmEuSeSWnsBBV2S4prAkvtoEUHc3HtX2+na0eIIQHxYmD4iYh7l9ioeY9NLNL+
Ojmxi3nV+NYGBAw/mU9cxptIzIYUg4vUuPJx2ZZ3nQvehl9sqA7VTEyY9gOoL3sn
ytBORGR63sjRkW0+QX+P+iT/vnLrjzKENZS2kXtko7vU397qwcyLNGA4fESkPyig
S9F7iIHOWtHwtQQOLKBNO5nhRp+Yrs2O7Xe33EINZp3WFwQGCJMeRrJYveMIBSr/
QicgQmVtjO9iZgFJdZNpiozFl8tSaFSVYvTfdujcnGNw5XMxip2ycvkRFo2CU3s7
LHdmRUIRItPwLoK0XnyV8y3z46FfO++ofFIx1ba88N8NrQFNiXvJXRSLAD/kGynH
nHQaHiTGkDqx7BOv7A+8Z2FILJdAHYzcgd5RPDlsa/e8aoOaTCRxHPinowCQGCZA
RPLC/c3fZg2UGHgwIu0K6TbWRdrm0qUIPjf3PsyFhDNOHIqO/YrjKkE0AX52Gbu7
HvMwA/LcZKT1uot+MasM79rudnxqnpvXDltaxIpuws9Pu8KOzhN1SG250KTBN5t7
5oCI/MmbTKbkrJmeIQ/clAH/V22wWeLoFTE1LmMVN+6XJn5ce3BF3FapP06EgT8g
mazzBrSLIwZ1wtar03XHojr3QTID5iZH25YVr8DYyfxPPg2XnczC+q+hELc64Dy6
/FH7Tnq8pvBmKBMPIIAeeZaT/8sQiuhwEFWRVeBgvK55deRLPjc6jSQRk2JwEUfR
nppijspJZsKuYXEAt1m5IYCCIpvjj4vaM9Gv/BABdbrHb7mbVBw2wEvIq9uQZTCW
Aw9cC64bbZaBcSaIew4wLeZfb3jLHds1bGK24UCAWnDEArmXFmivw857A6Mt2uMI
vRB4RFzVbj1UlvgF+DKwqI1s9ZhxFnePqbTtqmg/tv0c+WZv+GYO0zuHG+nY3U9H
OA2vqliwQ+ho8i33PTPCj/GbG1iUXfJQPWW9b4PTyTL4bvPEjoc31z30mXPg67K9
OlHMPvV+7DzM70wrNVounjLN2AzOVt4RNFG4g9ILCbK4dlOcWOujHbX/4Vs6NxYm
lJEiVn35AY5T69FMp3e0dXiLrjtnMJPK9zNzAqs/2/+Kin1fyIlCzL0PX+sMJDC7
CGcM3Ehea0UWSEtCcH2ii1Q/rZR/m6MbnjZBcYmk2FOm4mFRuA1/+xqPvcqpbyeN
yD8kY4PP+AkbaTLuhWxeN5Y2T+90Dzc0l6oFsIfkwVmhY43Our0JtHBtgsVg00hH
D1KPmezQFO03HIOiLn/lesIVJi+mnXOKPbGxtOr9tit8nEH3v0Ugsu85StFbNeyC
5r+3CsIL/cU/NVEv6bOAJ8xBl6ax2LjgM7Tg2AVTEJw4+Z02CVFAMiFFu3NW7+Wj
TC1/ITeTHnfpHwJiPGwMWEqYJlSlxxNonfgH5CIAEftK+xZ6ZuRxdSlaQHhoZdLq
ayB4VIRSHZktOxR73+ovQstPw/VPowLb2PEKsxtiuC+Um1TIz7foVjXgAc+/5+b5
bGyr6Kz+OQd8yhDaAcVTZDcbGvXpIFMtDhFIN8rs8/GT/py3O3L98cUdWuiNSTXr
V+tDseIgKusrLYBSXuNYPbJUa0zjoE6fFLeOUeIEKXqxSQlQxGyP/XU/yCXUYYYV
kEEz3Np/dDIfT4oL7M9988pYAeWhsk7/0XYOXW/HX8vywYv59ravWCBRQx/IEXin
x5V0GT1UJ3nKIDvu8a3eWFnvlf0bfIdjp3vXxM5oBQ3tmPmp1xPPtUhhjBMWAR/W
0hIyx8v1B5saWP6qcihU9q1wvdQeE0EUB5K2GV2SAnCEsb0vVZD4GpofCjfCQLjj
QCmVuBl575cywNvsBAOVVmamdLfZMy1O8n3/uACiy8rWNJUZx3avNzEy0VkUA/Ot
RQf/o7dpFsjLPw223k/1yjvlIITdYpopnxHd05ZHjvcdLDpbQxDJEmiuaovsZ5Lr
GfXSlboGOVRYnUoXknxw1vCtcTA9Ny++zN0TUXMvQLS9Rh5nwvNMB3wpftjuE3uq
+3fxr3HwgSZCkAyxlc+kVploNS30fPdakTcL9Qf9CfzKVRRXU/LaE9PS/F38g9l5
dC0OM3ue1kAQY/B/nuiEjYspt5cO1hFeC5RC+o7o8AxwfDAc7x36lSz9J6de+FAQ
8wmVtYWgzLTJy1Vsy4+L+NTBRkym1VWGt930AjWw+CllucjPd6qBaNdgrRVPohvm
IeHHv6iOh0k6IoZqomr153xDhLqnpHv0L3OzKnz4Ri2/ggIYaZI8jQUnojKp5uSL
S6ZD8KB3st3KKFTmzlFybps21LccgHTsEWqWnLncelrD7zYLqtBUHqKQlgl9cO/d
CwBYYeD9rrVqcU+dgDJsA5sAWhTdtfbUeaAY99mtM/2gI5lyw4Y+ebsqfcFuoaDX
ieCl3aoKd5ZQwLseapL6cFH32tZd0UDGe/m+BNbaB4wA+jwWSxhBXA7D0z5DgRju
ptvAbMZqWi6TyuCwy0/VuZ8Y9NbJmhUp8caEWedPBTJjvocZV1SjRzWfqcUi/Z2e
zzC55F8FvOJhwVuDSwRb5OwSIFEZj2oRNC10tqrfF2RkU/rW9xbq2KFQSYUprJRL
qqUQ0TY+/gt8zWaV1Ywe6tp3aSfW4Fc19yJmZ4nTebs3gN7Ow+q1HRjfYN0Mb91h
JMKGrrQSYimFGXZR/b99OQ5qGo2Cjst6Cf0vdx7QbUm1hnPmOkaIdzFqRgiIDMdf
uQufeIzVDc77gv+erMIi9pb2ETkpK9Ef3eVfV6q4wwQmsSdk/rcmtkizUbqggjHl
WQHGxrcrdTyZP9Pxyti/N58GE5ACht04CXka30pskx7EySXtgWKjMo4U6n20MGt7
P/QG5nzkyPEZwhpxjDGbDZehPkw47EGcx8vlct6F0Xw/ohQJ8NoNCJAqoI/rZm+N
ZPA+K/OrwfGkrY7KwXxKHbYOXQO75BC1n8PahRDKAQzkipRgd5aiLN5W+6984peJ
aZ294FpOtMo1wmYlEIpPXSaavdZXdUgVRmOv2+XFGG3kcC51SjMTLCXcnRq6X63z
QGasag7nboeCyRONLDMiZfYA8YFyS5bYMDcoHXX6ojL5KVc47DyUoirWKfM1f1sl
D6Snn4ic699uNTr4iI0fCv5uuP58UfOgBIgKntdY36uN+RGSF1lzWFgW32k7AEsp
17SnoYmaBdarIvCECRXhb17AnAWPppvYrmrjZpoWoXOhIsz/YYnZ6K2/RI5HxQNE
z3dZ/P7ojJ3d+4AiaM8z+B+8qWhd76NKoE4AUYwcT8Jc9Yt1NZbWJNgqmAxsCFv2
kQxC7ryHRBJsyDqpMBVDnxuk5WY2wtjdXe+zvrxYrTDEGDjR4AkIU2/01NZgNaa7
S5StSQQQJ8pJl0s/lbQYEdA1yme6OgA6fcTpaJLReezQfGMN/WaDnhbP4xuyJLH2

`pragma protect end_protected
