-------------------------------------------------------------------------------
--
-- Copyright (C) 2012-2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for unb2c_test.
-- Description:
--   The DUT can be targeted at unb 0, node 3 with the same Python scripts
--   that are used on hardware.
-- Usage:
--   On command line do:
--     > run_modelsim & (to start Modeslim)
--
--   In Modelsim do:
--     > lp unb2c_test
--     > mk clean all (only first time to clean all libraries)
--     > mk all (to compile all libraries that are needed for unb2c_test)
--     . load tb_unb1_test simulation by double clicking the tb_unb2c_test icon
--     > as 10 (to view signals in Wave Window)
--     > run 100 us (or run -all)
--
--   On command line do:
--     > python $UPE_GEAR/peripherals/util_system_info.py --gn 3 -n 0 -v 5 --sim
--     > python $UPE_GEAR/peripherals/util_ppsh.py --gn 3 -n 1 -v 5 --sim
--

library IEEE, common_lib, unb2c_board_lib, i2c_lib, technology_lib, tech_pll_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use common_lib.tb_common_pkg.all;
use technology_lib.technology_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity tb_unb2c_test is
  generic (
    g_design_name   : string  := "unb2c_test";
    g_sim_model_ddr : boolean := false;
    g_factory_image : boolean := false
  );
end tb_unb2c_test;

architecture tb of tb_unb2c_test is
  constant c_sim             : boolean := true;

  constant c_ddr_MB_I        : t_c_tech_ddr := c_tech_ddr4_4g_1600m;  -- DDR4 has no master or slave, so no need to check number of MB
  constant c_ddr_MB_II       : t_c_tech_ddr := c_tech_ddr4_4g_1600m;  -- DDR4 has no master or slave, so no need to check number of MB

  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 3;  -- Node 3
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_unb_nr, c_unb2c_board_nof_uniboard_w) & TO_UVEC(c_node_nr, c_unb2c_board_nof_chip_w);

  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);

  constant c_cable_delay          : time := 12 ns;
  constant c_eth_clk_period       : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_clk_period           : time := 5 ns;
  constant c_sa_clk_period        : time := tech_pll_clk_644_period;  -- 644 MHz
  constant c_sb_clk_period        : time := tech_pll_clk_644_period;  -- 644 MHz
  constant c_bck_ref_clk_period   : time := tech_pll_clk_644_period;  -- 644 MHz
  constant c_mb_I_ref_clk_period  : time := 40 ns;  -- 25 MHz
  constant c_mb_II_ref_clk_period : time := 40 ns;  -- 25 MHz
  constant c_pps_period           : natural := 1000;

  -- DUT
  signal clk                 : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal pps_rst             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic_vector(1 downto 0);
  signal eth_txp             : std_logic_vector(1 downto 0);
  signal eth_rxp             : std_logic_vector(1 downto 0);

  signal VERSION             : std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0) := c_version;
  signal ID                  : std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0)      := c_id;
  signal TESTIO              : std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

  signal qsfp_led            : std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0);

  -- DDR reference clocks
  signal mb_I_ref_clk        : std_logic := '1';  -- Reference clock for MB_I
  signal mb_II_ref_clk       : std_logic := '1';  -- Reference clock for MB_II

  -- DDR4 PHY interface
  signal MB_I_IN             : t_tech_ddr4_phy_in;
  signal MB_I_IO             : t_tech_ddr4_phy_io;
  signal MB_I_OU             : t_tech_ddr4_phy_ou;

  signal MB_II_IN            : t_tech_ddr4_phy_in;
  signal MB_II_IO            : t_tech_ddr4_phy_io;
  signal MB_II_OU            : t_tech_ddr4_phy_ou;

  -- 10GbE
  signal sa_clk              : std_logic := '1';
  signal sb_clk              : std_logic := '1';
  signal bck_ref_clk         : std_logic := '1';

  -- Serial I/O
  signal si_lpbk_0           : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
  signal si_lpbk_1           : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
  signal si_lpbk_2           : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
  signal si_lpbk_3           : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
  signal si_lpbk_4           : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);
  signal si_lpbk_5           : std_logic_vector(c_unb2c_board_tr_qsfp.bus_w - 1 downto 0);

  signal si_lpbk_6           : std_logic_vector(c_unb2c_board_tr_ring.bus_w - 1 downto 0);
  signal si_lpbk_7           : std_logic_vector(c_unb2c_board_tr_ring.bus_w - 1 downto 0);

  signal si_lpbk_8           : std_logic_vector(c_unb2c_board_tr_back.bus_w - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  clk           <= not clk           after c_clk_period / 2;  -- External clock (200 MHz)
  eth_clk(0)    <= not eth_clk(0)    after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  eth_clk(1)    <= not eth_clk(1)    after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  sa_clk        <= not sa_clk        after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)
  sb_clk        <= not sb_clk        after c_sb_clk_period / 2;  -- Serial Gigabit IO sb clock (644 MHz)
  bck_ref_clk   <= not bck_ref_clk   after c_bck_ref_clk_period / 2;  -- Serial Gigabit IO bck_ref clock (644 MHz)
  mb_I_ref_clk  <= not mb_I_ref_clk  after c_mb_I_ref_clk_period / 2;  -- MB I reference clock (25 MHz)
  mb_II_ref_clk <= not mb_II_ref_clk after c_mb_II_ref_clk_period / 2;  -- MB II reference clock (25 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, clk, pps);

  ------------------------------------------------------------------------------
  -- 1GbE Loopback model
  ------------------------------------------------------------------------------
  eth_rxp(0) <= transport eth_txp(0) after c_cable_delay;
  eth_rxp(1) <= transport eth_txp(1) after c_cable_delay;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_unb2c_test : entity work.unb2c_test
  generic map (
    g_design_name   => g_design_name,
    g_sim           => c_sim,
    g_sim_unb_nr    => c_unb_nr,
    g_sim_node_nr   => c_node_nr,
    g_sim_model_ddr => g_sim_model_ddr,
    g_factory_image => g_factory_image
  )
  port map (
    -- GENERAL
    CLK         => clk,
    PPS         => pps,
    WDI         => WDI,
    INTA        => INTA,
    INTB        => INTB,

    -- Others
    VERSION     => VERSION,
    ID          => ID,
    TESTIO      => TESTIO,

    -- 1GbE Control Interface
    ETH_CLK     => eth_clk,
    ETH_SGIN    => eth_rxp,
    ETH_SGOUT   => eth_txp,

    -- Transceiver clocks
    SA_CLK      => sa_clk,
    SB_CLK      => sb_clk,
    BCK_REF_CLK => bck_ref_clk,

    -- DDR reference clocks
    MB_I_REF_CLK  => mb_I_ref_clk,
    MB_II_REF_CLK => mb_II_ref_clk,

    -- Serial I/O
--    QSFP_0_TX  => si_lpbk_0,
--    QSFP_0_RX  => si_lpbk_0,
--    QSFP_1_TX  => si_lpbk_1,
--    QSFP_1_RX  => si_lpbk_1,
--    QSFP_2_TX  => si_lpbk_2,
--    QSFP_2_RX  => si_lpbk_2,
--    QSFP_3_TX  => si_lpbk_3,
--    QSFP_3_RX  => si_lpbk_3,
--    QSFP_4_TX  => si_lpbk_4,
--    QSFP_4_RX  => si_lpbk_4,
--    QSFP_5_TX  => si_lpbk_5,
--    QSFP_5_RX  => si_lpbk_5,
--
--    RING_0_TX  => si_lpbk_6,
--    RING_0_RX  => si_lpbk_6,
--    RING_1_TX  => si_lpbk_7,
--    RING_1_RX  => si_lpbk_7,
--
--    BCK_TX     => si_lpbk_8,
--    BCK_RX     => si_lpbk_8,

    -- SO-DIMM Memory Bank I
    MB_I_IN    => MB_I_IN,
    MB_I_IO    => MB_I_IO,
    MB_I_OU    => MB_I_OU,

    -- SO-DIMM Memory Bank II
    MB_II_IN   => MB_II_IN,
    MB_II_IO   => MB_II_IO,
    MB_II_OU   => MB_II_OU,

    -- Leds
    QSFP_LED   => qsfp_led
  );

  ------------------------------------------------------------------------------
  -- UniBoard DDR4
  ------------------------------------------------------------------------------

  u_tech_ddr_memory_model_MB_I : entity tech_ddr_lib.tech_ddr_memory_model
  generic map (
    g_tech_ddr => c_ddr_MB_I
  )
  port map (
    -- DDR4 PHY interface
    mem4_in => MB_I_OU,
    mem4_io => MB_I_IO
  );

  u_tech_ddr_memory_model_MB_II : entity tech_ddr_lib.tech_ddr_memory_model
  generic map (
    g_tech_ddr => c_ddr_MB_II
  )
  port map (
    -- DDR4 PHY interface
    mem4_in => MB_II_OU,
    mem4_io => MB_II_IO
  );
end tb;
