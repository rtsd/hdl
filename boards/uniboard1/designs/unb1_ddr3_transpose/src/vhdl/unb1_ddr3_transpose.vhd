-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, eth_lib, diag_lib, dp_lib, ddr3_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use eth_lib.eth_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use ddr3_lib.ddr3_pkg.all;

entity unb1_ddr3_transpose is
  generic (
    g_sim         : boolean      := false;  -- Overridden by TB
    g_sim_unb_nr  : natural      := 0;
    g_sim_node_nr : natural      := 0;
    g_design_name : string       := "unb1_ddr3_transpose";
    g_design_note : string       := "Test Design";
    --g_technology  : NATURAL      := c_tech_stratixiv;
    g_stamp_date  : natural      := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time  : natural      := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn   : natural      := 0;  -- SVN revision    -- set by QSF
    g_use_MB_I    : natural      := 1  -- 1: use MB_I  0: do not use
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc      : inout std_logic;
    sens_sd      : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk      : in    std_logic;
    ETH_SGIN     : in    std_logic;
    ETH_SGOUT    : out   std_logic;

    -- SO-DIMM Memory Bank I
    MB_I_IN       : in    t_tech_ddr3_phy_in := c_tech_ddr3_phy_in_x;
    MB_I_IO       : inout t_tech_ddr3_phy_io;
    MB_I_OU       : out   t_tech_ddr3_phy_ou
  );
end unb1_ddr3_transpose;

architecture str of unb1_ddr3_transpose is
  -- Firmware version x.y
  constant c_fw_version             : t_unb1_board_fw_version := (0, 9);
  -- In simulation we don't need the 1GbE core for MM control, deselect it in c_use_phy based on g_sim
  constant c_use_phy                : t_c_unb1_board_use_phy  := (sel_a_b(g_sim, 0, 1), 0, 0, 0, 1, 0, 0, 1);

  -- Compose the Constants for the DUT
  constant c_wr_chunksize            : positive   := sel_a_b(g_sim, 64, 64);  -- 256);
  constant c_wr_nof_chunks           : positive   := sel_a_b(g_sim,  1,  1);  -- 1);
  constant c_rd_chunksize            : positive   := sel_a_b(g_sim, 16, 16);  -- 16);
  constant c_rd_nof_chunks           : positive   := sel_a_b(g_sim,  4,  4);  -- 16);
  constant c_gapsize                 : natural    := sel_a_b(g_sim,  0,  0);  -- 0);
  constant c_nof_blocks              : positive   := sel_a_b(g_sim,  4,  4);  -- 16);

  constant c_ddr3_seq_conf           : t_ddr3_seq := (c_wr_chunksize,
                                                      c_wr_nof_chunks,
                                                      c_rd_chunksize,
                                                      c_rd_nof_chunks,
                                                      c_gapsize,
                                                      c_nof_blocks);

  constant c_blocksize               : positive := c_wr_nof_chunks * c_wr_chunksize;

  constant c_ddr                     : t_c_ddr3_phy := c_ddr3_phy_4g;
  constant c_mts                     : natural  := 800;  -- 1066; --800
  constant c_phy                     : natural  := 1;
  constant c_data_w                  : natural  := 8;
  constant c_nof_streams             : positive := 4;
  constant c_frame_size_in           : positive := c_wr_chunksize;
  constant c_frame_size_out          : positive := c_frame_size_in;
  constant c_nof_blk_per_sync        : positive := sel_a_b(g_sim, 16, 1600000);
  constant c_ena_pre_transpose       : boolean  := false;

  -- Custom definitions of constants
  constant c_bg_block_len           : natural  := c_blocksize * c_rd_chunksize;
  constant c_db_block_len           : natural  := c_blocksize * c_rd_chunksize;

  -- Configuration of the block generator:
  constant c_bg_buf_dat_w           : positive := c_nof_complex * c_data_w;
  constant c_bg_buf_adr_w           : positive := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_prefix    : string   := "UNUSED";  -- "../../../src/hex/tb_bg_dat";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, 128, 1);

  -- Configuration of the databuffers:
  constant c_db_data_w              : positive := c_diag_db_max_data_w;
  constant c_db_buf_nof_data        : positive := c_db_block_len;
  constant c_db_buf_use_sync        : boolean  := true;
  constant c_db_data_type_re        : t_diag_data_type_enum := e_real;
  constant c_db_data_type_im        : t_diag_data_type_enum := e_imag;

  -- System
  signal cs_sim                     : std_logic;
  signal xo_clk                     : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_locked                  : std_logic;
  signal mm_rst                     : std_logic;

  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;
  signal dp_pps                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- eth1g
  signal eth1g_tse_clk              : std_logic;
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;
  --SIGNAL eth1g_led                  : t_tse_led;

  -- Blockgenerator
  signal bg_siso_arr                : t_dp_siso_arr(c_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(c_nof_streams - 1 downto 0);

  signal reg_diag_bg_mosi           : t_mem_mosi;
  signal reg_diag_bg_miso           : t_mem_miso;

  signal ram_diag_bg_mosi           : t_mem_mosi;
  signal ram_diag_bg_miso           : t_mem_miso;

  -- Databuffer
  signal ram_diag_data_buf_re_mosi  : t_mem_mosi;
  signal ram_diag_data_buf_re_miso  : t_mem_miso;

  signal reg_diag_data_buf_re_mosi  : t_mem_mosi;
  signal reg_diag_data_buf_re_miso  : t_mem_miso;

  signal ram_diag_data_buf_im_mosi  : t_mem_mosi;
  signal ram_diag_data_buf_im_miso  : t_mem_miso;

  signal reg_diag_data_buf_im_mosi  : t_mem_mosi;
  signal reg_diag_data_buf_im_miso  : t_mem_miso;

  -- DDR3 Transpose
  signal ram_ss_ss_transp_mosi      : t_mem_mosi;
  signal ram_ss_ss_transp_miso      : t_mem_miso;

  -- BNS Monitor
  signal reg_bsn_monitor_mosi       : t_mem_mosi;
  signal reg_bsn_monitor_miso       : t_mem_miso;

  signal reg_io_ddr_mosi            : t_mem_mosi;
  signal reg_io_ddr_miso            : t_mem_miso;

  signal bsn_sosi_arr               : t_dp_sosi_arr(1 downto 0) := (others => c_dp_sosi_rst);

  signal out_sosi_arr               : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_siso_arr               : t_dp_siso_arr(c_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

  signal ddr_ref_rst                : std_logic;
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim         => g_sim,
    g_design_name => g_design_name,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_stamp_svn   => g_stamp_svn,
    g_fw_version  => c_fw_version,
    g_mm_clk_freq => c_unb1_board_mm_clk_freq_50M,
    g_use_phy     => c_use_phy,
    g_dp_clk_use_pll => false,
    g_aux         => c_unb1_board_aux
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,
    mm_rst                   => mm_rst,

    dp_rst                   => OPEN,  -- dp_rst,
    dp_clk                   => OPEN,  -- dp_clk,
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,
    -- . system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,
    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,
    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,  -- 125 MHz from xo_clk PLL in SOPC system
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    sens_sc                  => sens_sc,
    sens_sd                  => sens_sd,
    -- . 1GbE Control Interface
    ETH_clk                  => ETH_clk,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_unb_ddr3_transpose
  generic map (
    g_sim           => g_sim,
    g_sim_unb_nr    => g_sim_unb_nr,
    g_sim_node_nr   => g_sim_node_nr,
    g_frame_size_in => c_frame_size_in,
    g_nof_streams   => c_nof_streams,
    g_ddr3_seq      => c_ddr3_seq_conf
   )
  port map(
    xo_clk                    => xo_clk,
    xo_rst_n                  => xo_rst_n,
    xo_rst                    => xo_rst,

    mm_rst                    => mm_rst,
    mm_clk                    => mm_clk,
    mm_locked                 => mm_locked,

    -- PIOs
    pout_wdi                  => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi              => reg_wdi_mosi,
    reg_wdi_miso              => reg_wdi_miso,

    -- system_info
    reg_unb_system_info_mosi  => reg_unb_system_info_mosi,
    reg_unb_system_info_miso  => reg_unb_system_info_miso,
    rom_unb_system_info_mosi  => rom_unb_system_info_mosi,
    rom_unb_system_info_miso  => rom_unb_system_info_miso,

    -- UniBoard I2C sensors
    reg_unb_sens_mosi         => reg_unb_sens_mosi,
    reg_unb_sens_miso         => reg_unb_sens_miso,

    -- PPSH
    reg_ppsh_mosi             => reg_ppsh_mosi,
    reg_ppsh_miso             => reg_ppsh_miso,

    -- eth1g
    eth1g_tse_clk             => eth1g_tse_clk,
    eth1g_mm_rst              => eth1g_mm_rst,
    eth1g_tse_mosi            => eth1g_tse_mosi,
    eth1g_tse_miso            => eth1g_tse_miso,
    eth1g_reg_mosi            => eth1g_reg_mosi,
    eth1g_reg_miso            => eth1g_reg_miso,
    eth1g_reg_interrupt       => eth1g_reg_interrupt,
    eth1g_ram_mosi            => eth1g_ram_mosi,
    eth1g_ram_miso            => eth1g_ram_miso,

    -- Blockgenerator
    reg_diag_bg_mosi          => reg_diag_bg_mosi,
    reg_diag_bg_miso          => reg_diag_bg_miso,
    ram_diag_bg_mosi          => ram_diag_bg_mosi,
    ram_diag_bg_miso          => ram_diag_bg_miso,

    -- DDR3 transpose
    ram_ss_ss_transp_mosi     => ram_ss_ss_transp_mosi,
    ram_ss_ss_transp_miso     => ram_ss_ss_transp_miso,

    -- Databuffers
    ram_diag_data_buf_im_mosi => ram_diag_data_buf_im_mosi,
    ram_diag_data_buf_im_miso => ram_diag_data_buf_im_miso,
    reg_diag_data_buf_im_mosi => reg_diag_data_buf_im_mosi,
    reg_diag_data_buf_im_miso => reg_diag_data_buf_im_miso,
    ram_diag_data_buf_re_mosi => ram_diag_data_buf_re_mosi,
    ram_diag_data_buf_re_miso => ram_diag_data_buf_re_miso,
    reg_diag_data_buf_re_mosi => reg_diag_data_buf_re_mosi,
    reg_diag_data_buf_re_miso => reg_diag_data_buf_re_miso,

    -- BSN monitor
    reg_bsn_monitor_mosi      => reg_bsn_monitor_mosi,
    reg_bsn_monitor_miso      => reg_bsn_monitor_miso,

    reg_io_ddr_mosi           => reg_io_ddr_mosi,
    reg_io_ddr_miso           => reg_io_ddr_miso

  );

  -----------------------------------------------------------------------------
  -- Node function
  -----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_nof_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr         => bg_siso_arr,
    out_sosi_arr         => bg_sosi_arr
  );

  u_bsn_monitor : entity dp_lib.mms_dp_bsn_monitor
  generic map (
    g_nof_streams        => 2,  -- Check one input and one output stream
    g_cross_clock_domain => true,
    g_bsn_w              => c_dp_stream_bsn_w,
    g_cnt_sop_w          => c_word_w,
    g_cnt_valid_w        => c_word_w,
    g_log_first_bsn      => true
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    reg_mosi    => reg_bsn_monitor_mosi,
    reg_miso    => reg_bsn_monitor_miso,

    -- Streaming clock domain
    dp_rst      => dp_rst,
    dp_clk      => dp_clk,
    in_siso_arr => (others => c_dp_siso_rdy),
    in_sosi_arr => bsn_sosi_arr
  );

  bsn_sosi_arr(0) <= bg_sosi_arr(0);
  bsn_sosi_arr(1) <= out_sosi_arr(0);

  u_areset_ddr_ref_rst : entity common_lib.common_areset
  generic map(
    g_rst_level => '1',
    g_delay_len => 40
  )
  port map(
    clk     => CLK,
    in_rst  => mm_rst,
    out_rst => ddr_ref_rst
  );

  u_ddr3_T: entity ddr3_lib.ddr3_transpose
  generic map(
    g_sim                 => g_sim,
    g_nof_streams         => c_nof_streams,
    g_in_dat_w            => c_bg_buf_dat_w / c_nof_complex,
    g_frame_size_in       => c_frame_size_in,
    g_frame_size_out      => c_frame_size_out,
    g_nof_blk_per_sync    => c_nof_blk_per_sync,
    g_use_complex         => true,
    g_ena_pre_transp      => c_ena_pre_transpose,
    g_phy                 => c_phy,
    g_mts                 => c_mts,
    g_ddr3_seq            => c_ddr3_seq_conf
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_ref_clk            => CLK,
    dp_ref_rst            => ddr_ref_rst,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    dp_out_clk            => dp_clk,
    dp_out_rst            => dp_rst,

    reg_io_ddr_mosi       => reg_io_ddr_mosi,
    reg_io_ddr_miso       => reg_io_ddr_miso,

    snk_out_arr           => bg_siso_arr,
    snk_in_arr            => bg_sosi_arr,
    -- ST source
    src_in_arr            => out_siso_arr,
    src_out_arr           => out_sosi_arr,

    ram_ss_ss_transp_mosi => ram_ss_ss_transp_mosi,
    ram_ss_ss_transp_miso => ram_ss_ss_transp_miso,

    ser_term_ctrl_out     => OPEN,
    par_term_ctrl_out     => OPEN,

    ser_term_ctrl_in      => OPEN,
    par_term_ctrl_in      => OPEN,

    phy_in                => MB_I_in,
    phy_io                => MB_I_io,
    phy_ou                => MB_I_ou
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer real
  ----------------------------------------------------------------------------
  u_data_buf_re : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams     => c_nof_streams,
    g_data_type       => c_db_data_type_re,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
     -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_re_mosi,
    ram_data_buf_miso => ram_diag_data_buf_re_miso,
    reg_data_buf_mosi => reg_diag_data_buf_re_mosi,
    reg_data_buf_miso => reg_diag_data_buf_re_miso,
    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer imag
  ----------------------------------------------------------------------------
  u_data_buf_im : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams     => c_nof_streams,
    g_data_type       => c_db_data_type_im,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_im_mosi,
    ram_data_buf_miso => ram_diag_data_buf_im_miso,
    reg_data_buf_mosi => reg_diag_data_buf_im_mosi,
    reg_data_buf_miso => reg_diag_data_buf_im_miso,
    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );
end str;
