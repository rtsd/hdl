-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, common_mult_lib, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

-- Purpose:
--   Maintain a set of accumulators and output their values at every in_sync.
-- Description:
-- . The products of two input streams are accumulated per block. The block
--   size is g_nof_mux*g_nof_stat. The nof accumulators is equal to the block
--   size. The nof blocks that get accumulated depends on in_sync, because a
--   new accumulation starts every time when in_sync pulses. Also when in_sync
--   pulses then after some latency the accumulation values of the previous
--   in_sync interval become available at the out_* ports.
-- . If g_complex = FALSE then only the real power statistic out_re is calculated,
--   else also the imaginary power statistic out_im. The real power statistic
--   is used for auto power calulations of a complex input, by connecting the
--   signal to both input a and b. The imaginary is power statistic is used when
--   the cross power needs to be calculated between 2 different complex inputs.
-- Remarks:
-- . The required accumulator width depends the input data width and the nof of
--   block, i.e. the nof accumulations. E.g. for 18b*18b = 36b products and
--   200000 accumulations yielding 18b bit growth so in total 36b+18b = 54b for
--   the accumulators.
-- . The nof accumulators determines the size (c_mem_acc) of the internal
--   accumulator memory.
-- . Using g_nof_mux>1 allows distinghuising different streams with a block.
--   The g_nof_mux does not impact the address range instead it impacts the
--   out_val_m strobes that can be used as wr_en to the corresponding statistics
--   output register in a range of g_nof_mux statistics output registers.

entity st_calc is
  generic (
    g_technology   : natural := c_tech_select_default;
    g_nof_mux      : natural := 1;
    g_nof_stat     : natural := 512;
    g_in_dat_w     : natural := 18;  -- = input data width
    g_out_dat_w    : natural := 54;  -- = accumulator width for the input data products, so >> 2*g_in_dat_w
    g_out_adr_w    : natural := 9;  -- = ceil_log2(g_nof_stat)
    g_complex      : boolean := false
  );
  port (
    rst            : in   std_logic;
    clk            : in   std_logic;
    clken          : in   std_logic := '1';
    in_ar          : in   std_logic_vector(g_in_dat_w - 1 downto 0);
    in_ai          : in   std_logic_vector(g_in_dat_w - 1 downto 0);
    in_br          : in   std_logic_vector(g_in_dat_w - 1 downto 0);
    in_bi          : in   std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val         : in   std_logic;
    in_sync        : in   std_logic;
    out_adr        : out  std_logic_vector(g_out_adr_w - 1 downto 0);
    out_re         : out  std_logic_vector(g_out_dat_w - 1 downto 0);
    out_im         : out  std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val        : out  std_logic;  -- Use when g_nof_mux = 1, else leave OPEN
    out_val_m      : out  std_logic_vector(g_nof_mux - 1 downto 0)  -- Use when g_nof_mux > 1, else leave OPEN
  );
end;

architecture str of st_calc is
  constant c_complex_mult_variant  : string := sel_a_b(g_in_dat_w <= c_dsp_mult_w, "IP", "RTL");

  constant c_mux_w           : natural := true_log2(g_nof_mux);
  constant c_adr_w           : natural := c_mux_w + g_out_adr_w;  -- = = ceil_log2(g_nof_mux*g_nof_stat)

  constant c_dly_rd          : natural := 2;
  constant c_dly_mul         : natural := 3;
  constant c_dly_acc         : natural := 2;
  constant c_dly_out         : natural := 0;

  constant c_mult_w          : natural := 2 * g_in_dat_w;

  constant c_acc_w           : natural := g_out_dat_w;
  constant c_acc_hold_load   : boolean := true;

  constant c_rd_latency      : natural := 2;
  constant c_mem_acc         : t_c_mem := (c_rd_latency, c_adr_w,  c_acc_w, g_nof_mux * g_nof_stat, 'X');  -- 1 M9K

  signal mult_re       : std_logic_vector(c_mult_w - 1 downto 0);
  signal mult_im       : std_logic_vector(c_mult_w - 1 downto 0);

  signal reg_ar        : std_logic_vector(in_ar'range);
  signal reg_ai        : std_logic_vector(in_ai'range);
  signal reg_br        : std_logic_vector(in_br'range);
  signal reg_bi        : std_logic_vector(in_bi'range);
  signal reg_val       : std_logic;
  signal reg_sync      : std_logic;

  signal nxt_reg_ar    : std_logic_vector(in_ar'range);
  signal nxt_reg_ai    : std_logic_vector(in_ai'range);
  signal nxt_reg_br    : std_logic_vector(in_br'range);
  signal nxt_reg_bi    : std_logic_vector(in_bi'range);
  signal nxt_reg_val   : std_logic;
  signal nxt_reg_sync  : std_logic;

  signal acc_load      : std_logic;

  signal rd_en         : std_logic;
  signal rd_adr        : std_logic_vector(c_adr_w - 1 downto 0);
  signal rd_re         : std_logic_vector(c_acc_w - 1 downto 0);
  signal rd_im         : std_logic_vector(c_acc_w - 1 downto 0);

  signal wr_en         : std_logic;
  signal wr_adr        : std_logic_vector(c_adr_w - 1 downto 0);
  signal wr_re         : std_logic_vector(c_acc_w - 1 downto 0);
  signal wr_im         : std_logic_vector(c_acc_w - 1 downto 0);

  signal out_adr_m     : std_logic_vector(c_adr_w - 1 downto 0);
begin
  regs: process(rst, clk)
  begin
    if rst = '1' then
      reg_ar   <= (others => '0');
      reg_ai   <= (others => '0');
      reg_br   <= (others => '0');
      reg_bi   <= (others => '0');
      reg_val  <= '0';
      reg_sync <= '0';
    elsif rising_edge(clk) then
      reg_ar   <= nxt_reg_ar;
      reg_ai   <= nxt_reg_ai;
      reg_br   <= nxt_reg_br;
      reg_bi   <= nxt_reg_bi;
      reg_val  <= nxt_reg_val;
      reg_sync <= nxt_reg_sync;
    end if;
  end process;

  nxt_reg_ar   <= in_ar when in_val = '1' else reg_ar;
  nxt_reg_ai   <= in_ai when in_val = '1' else reg_ai;
  nxt_reg_br   <= in_br when in_val = '1' else reg_br;
  nxt_reg_bi   <= in_bi when in_val = '1' else reg_bi;
  nxt_reg_val  <= in_val;
  nxt_reg_sync <= in_sync;

  -- ctrl block: generates all ctrl signals
  ctrl: entity work.st_ctrl
  generic map (
    g_nof_mux    => g_nof_mux,
    g_nof_stat   => g_nof_stat,
    g_adr_w      => c_adr_w,
    g_dly_rd     => c_dly_rd,
    g_dly_mul    => c_dly_mul,
    g_dly_acc    => c_dly_acc,
    g_dly_out    => c_dly_out
  )
  port map (
    rst          => rst,
    clk          => clk,
    in_sync      => reg_sync,
    in_val       => reg_val,
    rd_en        => rd_en,
    rd_adr       => rd_adr,
    rd_val       => OPEN,
    mult_val     => OPEN,
    acc_load     => acc_load,
    wr_en        => wr_en,
    wr_adr       => wr_adr,
    out_val      => out_val,
    out_val_m    => out_val_m,
    out_adr      => out_adr_m
  );

  out_adr <= out_adr_m(c_adr_w - 1 downto c_mux_w);

  -- complex multiplier: computes a * conj(b)
  --mul: ENTITY common_lib.common_complex_mult(str)
  mul: entity common_mult_lib.common_complex_mult
  generic map (
    g_technology       => g_technology,
    g_variant          => c_complex_mult_variant,
    g_in_a_w           => in_ar'LENGTH,
    g_in_b_w           => in_br'LENGTH,
    g_out_p_w          => mult_re'LENGTH,
    g_conjugate_b      => true,  -- use conjugate product for cross power
    g_pipeline_input   => 1,
    g_pipeline_product => 0,
    g_pipeline_adder   => 1,
    g_pipeline_output  => 1  -- 1+0+1+1 = 3 = c_dly_mul
  )
  port map (
    clk        => clk,
    clken      => clken,
    in_ar      => reg_ar,
    in_ai      => reg_ai,
    in_br      => reg_br,
    in_bi      => reg_bi,
    out_pr     => mult_re,
    out_pi     => mult_im
  );

  -- accumulator for real part
  acc_re: entity work.st_acc
  generic map (
    g_dat_w           => c_mult_w,
    g_acc_w           => c_acc_w,
    g_hold_load       => c_acc_hold_load,
    g_pipeline_input  => 1,
    g_pipeline_output => c_dly_acc - 1
  )
  port map (
    clk         => clk,
    clken       => clken,
    in_load     => acc_load,
    in_dat      => mult_re,
    in_acc      => rd_re,
    out_acc     => wr_re
  );

  -- accumulator memory for real part
  ram_re: entity common_lib.common_ram_r_w
  generic map (
    g_technology => g_technology,
    g_ram        => c_mem_acc,
    g_init_file  => "UNUSED"
  )
  port map (
    rst       => rst,
    clk       => clk,
    clken     => clken,
    wr_en     => wr_en,
    wr_adr    => wr_adr,
    wr_dat    => wr_re,
    rd_en     => rd_en,
    rd_adr    => rd_adr,
    rd_dat    => rd_re,
    rd_val    => open
  );

  out_re <= rd_re;  -- c_dly_out = 0

  -- imaginary part is optional
  no_im: if g_complex = false generate
    out_im <= (others => '0');
  end generate;

  gen_im: if g_complex = true generate
    -- accumulator
    acc_im: entity work.st_acc
    generic map (
      g_dat_w           => c_mult_w,
      g_acc_w           => c_acc_w,
      g_hold_load       => c_acc_hold_load,
      g_pipeline_input  => 1,
      g_pipeline_output => c_dly_acc - 1
    )
    port map (
      clk         => clk,
      clken       => clken,
      in_load     => acc_load,
      in_dat      => mult_im,
      in_acc      => rd_im,
      out_acc     => wr_im
    );

    -- dual port memory
    ram_im: entity common_lib.common_ram_r_w
    generic map (
      g_technology => g_technology,
      g_ram        => c_mem_acc,
      g_init_file  => "UNUSED"
    )
    port map (
      rst       => rst,
      clk       => clk,
      clken     => clken,
      wr_en     => wr_en,
      wr_adr    => wr_adr,
      wr_dat    => wr_im,
      rd_en     => rd_en,
      rd_adr    => rd_adr,
      rd_dat    => rd_im,
      rd_val    => open
    );

    out_im <= rd_im;  -- c_dly_out = 0
  end generate;
end str;
