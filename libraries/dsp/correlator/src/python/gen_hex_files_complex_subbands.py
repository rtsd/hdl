###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from common import *
from common_dsp import *
from mem_init_file import list_to_hex

# Purpose:
# . Generate the NOF_INPUTS complex subband HEX files for the block generators.
# Description:
# . 

NOF_INPUTS = 24 # Must be even when NOF_FOLDS>0
NOF_FOLDS = 0 
NOF_INPUT_STREAMS = NOF_INPUTS/pow(2, NOF_FOLDS)
COMPLEX_WIDTH = 8 
NOF_CHANNELS = 64
NOF_WORDS_PER_BLOCK = NOF_CHANNELS*pow(2, NOF_FOLDS)

MAX_AMPLITUDE = pow(2, COMPLEX_WIDTH)/2-1 # Stay in the positive bit range
AMPL_INCR = 1
AMPL_LO = MAX_AMPLITUDE-NOF_CHANNELS*AMPL_INCR
PHASE_INCR = 1

MEM_WIDTH  = COMPLEX_WIDTH*2
MEM_DEPTH  = NOF_WORDS_PER_BLOCK

PATH = "../hex"
FILENAME = "complex_subbands"

# ===========================================================================
# Create the lists of complex subbands:
# . 1 deg. phase shift between the inputs
# . Amplitude increases with increasing channel index
# ===========================================================================

input_lists = []
phase_deg = 0
for input_nr in range(NOF_INPUTS):
    channel_block = [] # one channel block carries NOF_CHANNELS channel samples
    ampl = AMPL_LO
    phase_deg = phase_deg + PHASE_INCR
    for channel_nr in range(NOF_CHANNELS):
        ampl = ampl + AMPL_INCR
        print 'input_nr',input_nr, 'channel_nr', channel_nr, 'ampl',ampl,'phase_deg', phase_deg
        sample = complex_phasor_to_binomial(ampl, phase_deg)
        channel_block.append(sample)
    input_lists.append(channel_block)

# =============================================
# Convert complex type to concatenated unsigned
# =============================================
for input_nr in range(NOF_INPUTS):
    for word in range(NOF_CHANNELS):
        re = int(round(input_lists[input_nr][word].real))
        im = int(round(input_lists[input_nr][word].imag))
        print 'Input', input_nr, 'Channel', word, 're,im', re, im
        re_bits = CommonBits(re, COMPLEX_WIDTH)
        im_bits = CommonBits(im, COMPLEX_WIDTH)
        concat_bits = re_bits & im_bits
        input_lists[input_nr][word] = concat_bits.data

# ==========================================================
# Interleave the lists if user wants folded correlator input
# . Note: no support for more than 1 fold yet.
# ==========================================================
if NOF_FOLDS==1:
    input_stream_lists = []
    for input_stream_nr in range(NOF_INPUT_STREAMS):
        input_stream_lists.append( interleave([input_lists[2*input_stream_nr], input_lists[2*input_stream_nr+1]] ) )
elif NOF_FOLDS==0:
    input_stream_lists = input_lists

# ====================
# Write the HEX files
# ====================
for input_stream_nr in range(NOF_INPUT_STREAMS):
    list_to_hex( input_stream_lists[input_stream_nr], PATH+"/"+FILENAME+"_"+str(COMPLEX_WIDTH)+'b_fold_'+str(NOF_FOLDS)+'_'+str(input_stream_nr)+".hex", MEM_WIDTH, MEM_DEPTH)

