-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the reorder_transpose unit
--           To be used in conjunction with python testscript: ../python/tc_reorder_transpose.py
--
--
-- Usage:
--   > as 8
--   > run -all (about 300 us is enough)
--   > run python script in separate terminal: "python tc_reorder_transpose.py --unb 0 --fn 0 --sim"
--   > Stop the simulation manually in Modelsim by pressing the stop-button.
--   > Evalute u_dr_mem_ctrl/u_io_driver/ctlr_mosi in the WAVE window for wr and rd activity.

library IEEE, common_lib, mm_lib, diag_lib, dp_lib, io_ddr_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use work.reorder_pkg.all;

--
--  CONSTANT c_wr_chunksize           : NATURAL := 176;  --240 for 6-bit or 176 for 8-bit; --g_bf.nof_weights;
--  CONSTANT c_rd_chunksize           : NATURAL := 16;  --32;
--  CONSTANT c_rd_nof_chunks          : NATURAL := 11;  --15 for 6-bit or 11 for 8-bit
--  CONSTANT c_rd_interval            : NATURAL := c_rd_chunksize;
--  CONSTANT c_gapsize                : NATURAL := 0;
--  CONSTANT c_nof_blocks             : NATURAL := sel_a_b(g_sim, 16, 800000); --800000  781250);
--  CONSTANT c_bsn_sync_time_out      : NATURAL := (c_nof_blocks * g_bf.nof_weights * 10)/8;
--
--  CONSTANT c_reorder_seq_conf       : t_reorder_seq := (c_wr_chunksize,
--                                                        c_rd_chunksize,
--                                                        c_rd_nof_chunks,
--                                                        c_rd_interval,
--                                                        c_gapsize,
--                                                        c_nof_blocks);
--
--

entity tb_reorder_transpose is
  generic (
    g_wr_chunksize     : positive := 176;  -- 256;
    g_rd_chunksize     : positive := 16;
    g_rd_nof_chunks    : positive := 11;  -- 16;
    g_rd_interval      : positive := 16;  -- When pre-transpose is used equal to g_rd_chunksize.
    g_gapsize          : natural  := 0;
    g_nof_blocks       : positive := 64;
    g_nof_streams      : positive := 4;
    g_in_dat_w         : positive := 8;
    g_mem_dat_w        : natural  := 256;  -- The data width to the attached memory.
    g_frame_size_in    : natural  := 256;
    g_frame_size_out   : natural  := 176;
    g_ena_pre_transp   : boolean  := true
 );
end tb_reorder_transpose;

architecture tb of tb_reorder_transpose is
  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period      : time := 100 ps;
  constant c_dp_clk_period      : time := 5 ns;
  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';
  signal dp_rst_in              : std_logic;
  signal dp_clk_in              : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  signal reg_diag_bg_mosi          : t_mem_mosi;
  signal reg_diag_bg_miso          : t_mem_miso;

  signal ram_diag_bg_mosi          : t_mem_mosi;
  signal ram_diag_bg_miso          : t_mem_miso;

  signal ram_diag_data_buf_re_mosi : t_mem_mosi;
  signal ram_diag_data_buf_re_miso : t_mem_miso;

  signal reg_diag_data_buf_re_mosi : t_mem_mosi;
  signal reg_diag_data_buf_re_miso : t_mem_miso;

  signal ram_diag_data_buf_im_mosi : t_mem_mosi;
  signal ram_diag_data_buf_im_miso : t_mem_miso;

  signal reg_diag_data_buf_im_mosi : t_mem_mosi;
  signal reg_diag_data_buf_im_miso : t_mem_miso;

  -- DUT
  signal ram_ss_ss_transp_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal ram_ss_ss_transp_miso     : t_mem_miso := c_mem_miso_rst;

  signal reg_dp_sync_checker_mosi  : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_sync_checker_miso  : t_mem_miso := c_mem_miso_rst;

  signal reg_io_ddr_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal reg_io_ddr_miso           : t_mem_miso := c_mem_miso_rst;

  -- Compose the Constants for the DUT
  constant c_reorder_seq_conf       : t_reorder_seq := (g_wr_chunksize,
                                                        g_rd_chunksize,
                                                        g_rd_nof_chunks,
                                                        g_rd_interval,
                                                        g_gapsize,
                                                        g_nof_blocks);

  constant c_tech_ddr               : t_c_tech_ddr := c_tech_ddr3_4g_800m_master;

  constant c_min_fifo_size          : positive := 256;
  constant c_blocksize              : positive := g_wr_chunksize + g_gapsize;
  constant c_pagesize               : positive := g_nof_blocks * c_blocksize;
  constant c_mem_size               : positive := 2 * c_pagesize;
  constant c_mem_size_w             : positive := ceil_log2(c_mem_size);

  constant c_wr_fifo_depth          : natural  := sel_a_b(c_blocksize > c_min_fifo_size, c_blocksize, c_min_fifo_size);  -- c_blocksize * 2;
  constant c_rd_fifo_depth          : natural  := sel_a_b(c_blocksize > c_min_fifo_size, c_blocksize, c_min_fifo_size);  -- c_blocksize * 2;

  constant c_use_complex            : boolean  := true;
  constant c_total_data_w           : natural  := g_nof_streams * g_in_dat_w;
  constant c_complex_data_w         : natural  := c_total_data_w * c_nof_complex;
  constant c_data_w                 : natural  := sel_a_b(c_use_complex, c_complex_data_w, c_total_data_w);

  constant c_ctrl_ref_clk_period    : time  := 5000 ps;

  -- Custom definitions of constants
  constant c_bg_block_len           : natural  := g_frame_size_in * g_nof_blocks;
  constant c_db_block_len           : natural  := g_frame_size_in * g_nof_blocks;

  -- Configuration of the block generator:
  constant c_bg_nof_output_streams  : positive := g_nof_streams;
  constant c_bg_buf_dat_w           : positive := c_nof_complex * g_in_dat_w;
  constant c_bg_buf_adr_w           : positive := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_prefix    : string   := "UNUSED";  -- "../../../src/hex/tb_bg_dat";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, 128, 1);

  -- Configuration of the databuffers:
  constant c_db_nof_streams         : positive := g_nof_streams;
  constant c_db_data_w              : positive := c_diag_db_max_data_w;
  constant c_db_buf_nof_data        : positive := c_db_block_len;
  constant c_db_buf_use_sync        : boolean  := true;
  constant c_db_data_type_re        : t_diag_data_type_enum := e_real;
  constant c_db_data_type_im        : t_diag_data_type_enum := e_imag;

  signal bg_siso_arr                : t_dp_siso_arr(c_bg_nof_output_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(c_bg_nof_output_streams - 1 downto 0);
  signal dut_in_arr                 : t_dp_sosi_arr(c_bg_nof_output_streams - 1 downto 0);

  signal out_sosi_arr               : t_dp_sosi_arr(c_bg_nof_output_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_siso_arr               : t_dp_siso_arr(c_bg_nof_output_streams - 1 downto 0) := (others => c_dp_siso_rdy);

  -- Signals to interface with the DDR conroller and memory model.
  signal ctlr_dvr_miso              : t_mem_ctlr_miso;
  signal ctlr_dvr_mosi              : t_mem_ctlr_mosi;

  signal to_mem_siso                : t_dp_siso := c_dp_siso_rdy;
  signal to_mem_sosi                : t_dp_sosi;
  signal from_mem_siso              : t_dp_siso := c_dp_siso_rdy;
  signal from_mem_sosi              : t_dp_sosi;

  signal phy_in                     : t_tech_ddr3_phy_in;
  signal phy_io                     : t_tech_ddr3_phy_io;
  signal phy_ou                     : t_tech_ddr3_phy_ou;
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk_in <= not dp_clk_in after c_dp_clk_period / 2;
  dp_rst_in <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  u_mm_file_reg_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_BG")
                                           port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

  u_mm_file_ram_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_BG")
                                           port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

  u_mm_file_ram_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_re_mosi, ram_diag_data_buf_re_miso);

  u_mm_file_reg_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_re_mosi, reg_diag_data_buf_re_miso);

  u_mm_file_ram_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_im_mosi, ram_diag_data_buf_im_miso);

  u_mm_file_reg_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_im_mosi, reg_diag_data_buf_im_miso);

  u_mm_file_ram_ss_ss_transp     : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_SS_SS_WIDE")
                                           port map(mm_rst, mm_clk, ram_ss_ss_transp_mosi, ram_ss_ss_transp_miso);

  u_mm_file_reg_dp_sync_checker  : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DP_SYNC_CHECKER")
                                           port map(mm_rst, mm_clk, reg_dp_sync_checker_mosi, reg_dp_sync_checker_miso);

  u_mm_file_reg_io_ddr           : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_IO_DDR")
                                           port map(mm_rst, mm_clk, reg_io_ddr_mosi, reg_io_ddr_miso);

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_bg_nof_output_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr         => bg_siso_arr,
    out_sosi_arr         => bg_sosi_arr
  );

  gen_chang_in_imaginary_part : process(bg_sosi_arr)
  begin
    for i in 0 to c_bg_nof_output_streams - 1 loop
      dut_in_arr(i)    <= bg_sosi_arr(i);
      dut_in_arr(i).im <= bg_sosi_arr(i).bsn(c_dp_stream_dsp_data_w - 1 downto 0);
    end loop;
  end process;

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut: entity work.reorder_transpose
  generic map(
    g_nof_streams      => c_bg_nof_output_streams,
    g_in_dat_w         => c_bg_buf_dat_w / c_nof_complex,
    g_frame_size_in    => g_frame_size_in,
    g_frame_size_out   => g_frame_size_out,
    g_use_complex      => c_use_complex,
    g_mem_dat_w        => g_mem_dat_w,
    g_ena_pre_transp   => g_ena_pre_transp,
    g_reorder_seq      => c_reorder_seq_conf
  )
  port map (
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,

    -- ST sink
    snk_out_arr              => bg_siso_arr,
    snk_in_arr               => dut_in_arr,

    -- ST source
    src_in_arr               => out_siso_arr,
    src_out_arr              => out_sosi_arr,

    ram_ss_ss_transp_mosi    => ram_ss_ss_transp_mosi,
    ram_ss_ss_transp_miso    => ram_ss_ss_transp_miso,

    reg_dp_sync_checker_mosi => reg_dp_sync_checker_mosi,
    reg_dp_sync_checker_miso => reg_dp_sync_checker_miso,

    -- Control interface to the external memory
    dvr_miso                 => ctlr_dvr_miso,
    dvr_mosi                 => ctlr_dvr_mosi,

    -- Data interface to the external memory
    to_mem_src_out           => to_mem_sosi,
    to_mem_src_in            => to_mem_siso,

    from_mem_snk_in          => from_mem_sosi,
    from_mem_snk_out         => from_mem_siso

  );

  u_ddr_mem_ctrl : entity io_ddr_lib.io_ddr
  generic map(
    g_tech_ddr               => c_tech_ddr,  -- : t_c_tech_ddr;
    g_cross_domain_dvr_ctlr  => false,  -- TRUE,   --  : BOOLEAN := TRUE;
    g_wr_data_w              => c_data_w,  -- : NATURAL := 32;
    g_wr_fifo_depth          => c_wr_fifo_depth,  -- : NATURAL := 128;     -- >=16                             , defined at DDR side of the FIFO.
    g_rd_fifo_depth          => c_rd_fifo_depth,  -- : NATURAL := 256;     -- >=16 AND >g_tech_ddr.maxburstsize, defined at DDR side of the FIFO.
    g_rd_data_w              => c_data_w,  -- : NATURAL := 32;
    g_wr_flush_mode          => "SYN",  -- : STRING := "VAL";  -- "VAL", "SOP", "SYN"
    g_wr_flush_use_channel   => false,  -- : BOOLEAN := FALSE;
    g_wr_flush_start_channel => 0,  -- : NATURAL := 0;
    g_wr_flush_nof_channels  => 1  -- : POSITIVE := 1
  )
  port map (
    -- DDR reference clock
    ctlr_ref_clk  => dp_clk_in,
    ctlr_ref_rst  => dp_rst_in,

    -- DDR controller clock domain
    ctlr_clk_out  => dp_clk,  -- output clock of the ddr controller is used as DP clk.
    ctlr_rst_out  => dp_rst,

    ctlr_clk_in   => dp_clk,
    ctlr_rst_in   => dp_rst,

    -- MM clock + reset
    mm_rst        => mm_rst,
    mm_clk        => mm_clk,

    -- MM interface
    reg_io_ddr_mosi => reg_io_ddr_mosi,
    reg_io_ddr_miso => reg_io_ddr_miso,

    -- Driver clock domain
    dvr_clk       => dp_clk,
    dvr_rst       => dp_rst,

    dvr_miso      => ctlr_dvr_miso,
    dvr_mosi      => ctlr_dvr_mosi,

    -- Write FIFO clock domain
    wr_clk        => dp_clk,
    wr_rst        => dp_rst,

    wr_fifo_usedw => OPEN,
    wr_sosi       => to_mem_sosi,
    wr_siso       => to_mem_siso,

    -- Read FIFO clock domain
    rd_clk        => dp_clk,
    rd_rst        => dp_rst,

    rd_fifo_usedw => OPEN,
    rd_sosi       => from_mem_sosi,
    rd_siso       => from_mem_siso,

    -- DDR3 PHY external interface
    phy3_in       => phy_in,
    phy3_io       => phy_io,
    phy3_ou       => phy_ou
  );

  u_tech_ddr_memory_model : entity tech_ddr_lib.tech_ddr_memory_model
  generic map (
    g_tech_ddr => c_tech_ddr
  )
  port map (
    mem3_in => phy_ou,
    mem3_io => phy_io
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer real
  ----------------------------------------------------------------------------
  u_data_buf_re : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams     => c_db_nof_streams,
    g_data_type       => c_db_data_type_re,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
     -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_re_mosi,
    ram_data_buf_miso => ram_diag_data_buf_re_miso,
    reg_data_buf_mosi => reg_diag_data_buf_re_mosi,
    reg_data_buf_miso => reg_diag_data_buf_re_miso,
    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer imag
  ----------------------------------------------------------------------------
  u_data_buf_im : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams     => c_db_nof_streams,
    g_data_type       => c_db_data_type_im,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_im_mosi,
    ram_data_buf_miso => ram_diag_data_buf_im_miso,
    reg_data_buf_mosi => reg_diag_data_buf_im_mosi,
    reg_data_buf_miso => reg_diag_data_buf_im_miso,
    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );
end tb;
