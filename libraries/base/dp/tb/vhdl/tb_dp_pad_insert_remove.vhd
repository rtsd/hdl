-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

-- Purpose: Verify dp_pad_insert and dp_pad_remove
-- Description:
--   p_stimuli --> dp_pad_insert --> dp_pad_remove --> verify
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

entity tb_dp_pad_insert_remove is
  generic (
    g_data_w       : natural := 16;
    g_symbol_w     : natural := 8;  -- g_data_w/g_symbol_w must be an integer
    g_nof_symbols  : natural := 21;
    g_nof_padding  : natural := 2;  -- must be > 0 and  <= g_data_w/g_symbol_w
    g_in_en        : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    g_out_ready    : t_dp_flow_control_enum := e_active  -- always e_active, e_random or e_pulse flow control
  );
end tb_dp_pad_insert_remove;

architecture tb of tb_dp_pad_insert_remove is
  -- tb default
  constant c_rl                       : natural := 1;
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  -- tb specific
  constant c_nof_repeat               : natural := 100;
  constant c_bsn_w                    : natural := 16;
  constant c_symbol_init              : natural := 0;
  constant c_symbol_mod               : integer := 2**g_symbol_w;  -- used to avoid TO_UVEC warning for smaller g_symbol_w : "NUMERIC_STD.TO_UNSIGNED: vector truncated"
  constant c_channel_init             : integer := 0;
  constant c_err_init                 : natural := 0;
  constant c_sync_period              : natural := 7;
  constant c_sync_offset              : natural := 2;

  -- tb default
  signal tb_end               : std_logic := '0';
  signal clk                  : std_logic := '1';
  signal rst                  : std_logic := '1';

  signal random_0             : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1             : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0              : std_logic;
  signal pulse_1              : std_logic;
  signal pulse_en             : std_logic := '1';

  signal in_en                : std_logic := '1';

  -- tb verify
  signal verify_en            : std_logic := '0';
  signal verify_done          : std_logic := '0';

  signal prev_out_ready       : std_logic_vector(0 to c_rl);
  signal prev_out_data        : std_logic_vector(g_data_w - 1 downto 0);
  signal prev_out_bsn         : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '1');  -- = -1
  signal prev_out_channel     : std_logic_vector(c_dp_stream_channel_w - 1 downto 0) := TO_SVEC(c_channel_init - 1, c_dp_stream_channel_w);
  signal prev_out_err         : std_logic_vector(c_dp_stream_error_w - 1 downto 0) := TO_SVEC(c_err_init - 1, c_dp_stream_error_w);
  signal expected_out_bsn     : std_logic_vector(c_bsn_w - 1 downto 0);
  signal expected_out_channel : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal expected_out_err     : std_logic_vector(c_dp_stream_error_w - 1 downto 0);

  -- tb dut
  signal in_siso              : t_dp_siso;
  signal in_sosi              : t_dp_sosi;
  signal in_data              : std_logic_vector(g_data_w - 1 downto 0);
  signal in_bsn               : std_logic_vector(c_bsn_w - 1 downto 0);
  signal in_sync              : std_logic;
  signal in_val               : std_logic;
  signal in_sop               : std_logic;
  signal in_eop               : std_logic;
  signal in_empty             : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  signal in_channel           : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal in_err               : std_logic_vector(c_dp_stream_error_w - 1 downto 0);

  signal pad_siso             : t_dp_siso;
  signal pad_sosi             : t_dp_sosi;
  signal pad_data             : std_logic_vector(g_data_w - 1 downto 0);
  signal pad_bsn              : std_logic_vector(c_bsn_w - 1 downto 0);
  signal pad_sync             : std_logic;
  signal pad_val              : std_logic;
  signal pad_sop              : std_logic;
  signal pad_eop              : std_logic;
  signal pad_empty            : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  signal pad_channel          : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal pad_err              : std_logic_vector(c_dp_stream_error_w - 1 downto 0);

  signal out_siso             : t_dp_siso := c_dp_siso_hold;  -- default xon = '1' is needed for proc_dp_gen_block_data()
  signal out_sosi             : t_dp_sosi;
  signal out_data             : std_logic_vector(g_data_w - 1 downto 0);
  signal out_bsn              : std_logic_vector(c_bsn_w - 1 downto 0);
  signal out_sync             : std_logic;
  signal out_val              : std_logic;
  signal out_sop              : std_logic;
  signal out_eop              : std_logic;
  signal out_empty            : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  signal out_channel          : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal out_err              : std_logic_vector(c_dp_stream_error_w - 1 downto 0);

  signal out_gap              : std_logic := '1';
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en          <= '1'                     when g_in_en = e_active      else
                    random_0(random_0'high) when g_in_en = e_random      else
                    pulse_0                 when g_in_en = e_pulse;

  out_siso.ready <= '1'                     when g_out_ready = e_active  else
                    random_1(random_1'high) when g_out_ready = e_random  else
                    pulse_1                 when g_out_ready = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli : process
    variable v_sync      : std_logic := '0';
    variable v_bsn       : std_logic_vector(c_bsn_w - 1 downto 0) := (others => '0');
    variable v_symbol    : natural := c_symbol_init;
    variable v_channel   : natural := c_channel_init;
    variable v_err       : natural := c_err_init;
  begin
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Begin of stimuli
    for R in 0 to c_nof_repeat - 1 loop
      v_sync := sel_a_b(R mod c_sync_period = c_sync_offset, '1', '0');  -- v_bsn = R
      proc_dp_gen_block_data(c_rl, true, g_data_w, g_symbol_w, v_symbol, 0, 0, g_nof_symbols, v_channel, v_err, v_sync, TO_DP_BSN(R), clk, in_en, in_siso, in_sosi);
      v_bsn     := INCR_UVEC(v_bsn, 1);
      v_symbol  := (v_symbol + g_nof_symbols) mod c_symbol_mod;
      v_channel := v_channel + 1;
      v_err     := v_err + 1;
      --proc_common_wait_some_cycles(clk, 10);               -- create gap between frames
    end loop;

    -- End of stimuli
    expected_out_bsn     <= INCR_UVEC(v_bsn, -1);
    expected_out_channel <= TO_UVEC(v_channel - 1, c_dp_stream_channel_w);
    expected_out_err     <= TO_UVEC(v_err - 1, c_dp_stream_error_w);

    proc_common_wait_some_cycles(clk, 50);  -- depends on stream control
    verify_done <= '1';
    proc_common_wait_some_cycles(clk, 1);
    verify_done <= '0';
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  verify_en <= '1' when rising_edge(clk) and out_sosi.sop = '1';  -- verify enable after first output sop

  -- SOSI control
  proc_dp_verify_valid(c_rl, clk, verify_en, out_siso.ready, prev_out_ready, out_val);  -- Verify that the output valid fits with the output ready latency
  proc_dp_verify_gap_invalid(clk, out_val, out_sop, out_eop, out_gap);  -- Verify that the output valid is low between blocks from eop to sop

  -- SOSI data
  -- . verify that the output is incrementing symbols, like the input stimuli
  proc_dp_verify_symbols(c_rl, g_data_w, g_symbol_w, clk, verify_en, out_siso.ready, out_sosi.valid, out_sosi.eop, out_data, out_sosi.empty, prev_out_data);

  -- SOSI bsn, err, channel
  -- . verify that the output is incrementing like the input stimuli
  proc_dp_verify_data("out_sosi.bsn",     c_rl, clk, verify_en, out_siso.ready, out_sop, out_bsn, prev_out_bsn);
  proc_dp_verify_data("out_sosi.channel", c_rl, clk, verify_en, out_siso.ready, out_eop, out_channel, prev_out_channel);
  proc_dp_verify_data("out_sosi.err",     c_rl, clk, verify_en, out_siso.ready, out_eop, out_err, prev_out_err);

  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_en, out_sync, out_sop, out_bsn);

  -- . verify that the stimuli have been applied at all
  proc_dp_verify_value(e_equal, clk, verify_done, expected_out_bsn,     prev_out_bsn);
  proc_dp_verify_value(e_equal, clk, verify_done, expected_out_channel, prev_out_channel);
  proc_dp_verify_value(e_equal, clk, verify_done, expected_out_err,     prev_out_err);

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_pad_insert : entity work.dp_pad_insert
  generic map (
    g_data_w      => g_data_w,
    g_symbol_w    => g_symbol_w,
    g_nof_padding => g_nof_padding
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_out     => in_siso,
    snk_in      => in_sosi,
    src_in      => pad_siso,
    src_out     => pad_sosi
  );

  u_pad_remove : entity work.dp_pad_remove
  generic map (
    g_data_w      => g_data_w,
    g_symbol_w    => g_symbol_w,
    g_nof_padding => g_nof_padding
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_out     => pad_siso,
    snk_in      => pad_sosi,
    src_in      => out_siso,
    src_out     => out_sosi
  );

  -- Map to slv to ease monitoring in wave window
  in_data     <= in_sosi.data(g_data_w - 1 downto 0);
  in_val      <= in_sosi.valid;
  in_sop      <= in_sosi.sop;
  in_eop      <= in_sosi.eop;
  in_empty    <= in_sosi.empty(in_empty'range);
  in_channel  <= in_sosi.channel(in_channel'range);
  in_err      <= in_sosi.err(in_err'range);
  in_sync     <= in_sosi.sync;
  in_bsn      <= in_sosi.bsn(c_bsn_w - 1 downto 0);

  pad_data    <= pad_sosi.data(g_data_w - 1 downto 0);
  pad_val     <= pad_sosi.valid;
  pad_sop     <= pad_sosi.sop;
  pad_eop     <= pad_sosi.eop;
  pad_empty   <= pad_sosi.empty(in_empty'range);
  pad_channel <= pad_sosi.channel(in_channel'range);
  pad_err     <= pad_sosi.err(in_err'range);
  pad_sync    <= pad_sosi.sync;
  pad_bsn     <= pad_sosi.bsn(c_bsn_w - 1 downto 0);

  out_data    <= out_sosi.data(g_data_w - 1 downto 0);
  out_val     <= out_sosi.valid;
  out_sop     <= out_sosi.sop;
  out_eop     <= out_sosi.eop;
  out_empty   <= out_sosi.empty(in_empty'range);
  out_channel <= out_sosi.channel(in_channel'range);
  out_err     <= out_sosi.err(in_err'range);
  out_sync    <= out_sosi.sync;
  out_bsn     <= out_sosi.bsn(c_bsn_w - 1 downto 0);
end tb;
