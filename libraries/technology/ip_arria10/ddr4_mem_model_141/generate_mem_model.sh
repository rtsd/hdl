#!/bin/bash
# -------------------------------------------------------------------------- #
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>           
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands                             
#                                                                           
# This program is free software: you can redistribute it and/or modify      
# it under the terms of the GNU General Public License as published by      
# the Free Software Foundation, either version 3 of the License, or         
# (at your option) any later version.                                       
#                                                                           
# This program is distributed in the hope that it will be useful,           
# but WITHOUT ANY WARRANTY; without even the implied warranty of            
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             
# GNU General Public License for more details.                              
#                                                                           
# You should have received a copy of the GNU General Public License         
# along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
# -------------------------------------------------------------------------- #
#
# Purpose: Generate DDR4 memory model with Qsys
# Description:
#   Generate the IP in a separate generated/ subdirectory.
#
# Usage:
#
#   Unfortunately the example design with the memory model cannot be created from the qsys-generate command line, because
#   there is no option to invoke this.
#   Therefore first open ip_arria10_ddr4_4g_1600.qsys in the Quartus QUI and then click the 'Example Design...' button.
#   This will then created the emif_0_example_design/ directory that contains the TCL scripts to create the example design.
#
#   ./generate_mem_model.sh <path to example design directory>
#
#   eg. for ddr4_4g_1600:
#
#   ./generate_mem_model.sh ../ddr4_4g_1600/emif_0_example_design
#
#   eg. for ddr4_8g_2400:
#
#   ./generate_mem_model.sh ../ddr4_8g_2400/ddr4_inst_example_design

# Tool settings for selected target "unb2" with arria10
. ${RADIOHDL_GEAR}/quartus/set_quartus unb2

#qsys-generate --help

EXAMPLE_DESIGN_DIR=${1}
MODEL_DIR=${RADIOHDL}/libraries/technology/ip_arria10/ddr4_mem_model_141

cd ${EXAMPLE_DESIGN_DIR}
quartus_sh -t make_sim_design.tcl VHDL

cd ${MODEL_DIR}
echo "pwd = ${PWD}"
echo "Run diff_mem_model.sh"
. diff_mem_model.sh ${EXAMPLE_DESIGN_DIR}
