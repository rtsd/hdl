-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use std.textio.all;
use IEEE.std_logic_textio.all;
use common_lib.common_str_pkg.all;

-- Purpose:
-- . Play back a stream recorded by dp_stream_recorder.
-- Description:
-- . Playback starts as soon as the connected sink indicates to be ready by
--   asserting both XON and READY.
-- . See dp_stream_recorder.vhd

entity dp_stream_player is
  generic (
    g_playback_file : string := "dp_stream_recorder.rec"
  );
  port (
    dp_clk  : in  std_logic;
    src_out : out t_dp_sosi;
    src_in  : in  t_dp_siso
  );
end dp_stream_player;

architecture beh of dp_stream_player is
  constant c_nof_chars_per_line : natural := 144;

  signal playback_start : boolean := false;
  file   playback_file  : TEXT;
begin
  p_playback_start : process(src_in.xon)
    variable v_open_status : file_open_status;
  begin
    if src_in.xon = '1' and playback_start = false then
      -- Open the playback file
      file_open(v_open_status, playback_file, g_playback_file, read_mode);
      -- Start playing
      playback_start <= true;
    end if;
  end process;

  p_playback : process(dp_clk, src_in)
    variable v_line     : LINE;
    variable v_line_str : string(1 to c_nof_chars_per_line);
  begin
    if playback_start = true then
      if rising_edge(dp_clk) then
        if src_in.ready = '1' then
          if not endfile(playback_file) then
            -- Read line from file
            readline(playback_file, v_line);
            read(v_line, v_line_str);
            -- Assign record fields
            src_out.sync    <= hex_to_slv(v_line_str(  1 to   1))(0);
            src_out.bsn     <= hex_to_slv(v_line_str(  3 to  18));
            src_out.data(255 downto 0) <= hex_to_slv(v_line_str( 20 to  83));  -- FIXME Should support full width of 768 (new in RadioHDL)
            src_out.re      <= hex_to_slv(v_line_str( 85 to 100));
            src_out.im      <= hex_to_slv(v_line_str(102 to 117));
            src_out.valid   <= hex_to_slv(v_line_str(119 to 119))(0);
            src_out.sop     <= hex_to_slv(v_line_str(121 to 121))(0);
            src_out.eop     <= hex_to_slv(v_line_str(123 to 123))(0);
            src_out.empty(7 downto 0) <= hex_to_slv(v_line_str(125 to 126));  -- FIXME Should support full width also.
            src_out.channel <= hex_to_slv(v_line_str(128 to 135));
            src_out.err     <= hex_to_slv(v_line_str(137 to 144));
          end if;
        end if;
      end if;
    end if;
  end process;
end beh;
