%% writes R2SDF input stimulis, full range uniform  for {B} bits input
%% signal and {N} point FFT
% 
%-------------------------------------------------------------------------%
%   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
%   Copyright (C) 2009-2011
%   ASTRON (Netherlands Institute for Radio Astronomy)
%   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
% 
%   This file is part of the UniBoard software suite.
%   The file is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%-------------------------------------------------------------------------%
%%

clear all; close all; clc;

j=1i; 
K=10;       % log2(# of points)
B=8;       % # of input bits

P=2.^K; 

nOfFrames= 8;


RE= 1;
IM= 2;

inFileName= strcat('../data/uniNoise_in.txt');

'test signal'
x=zeros(nOfFrames*P,2);
high=2^(B-1)-1;

x(:,RE)=  round(high*(2*rand(nOfFrames*P,1)-1));
x(:,IM)=  round(high*(2*rand(nOfFrames*P,1)-1));


'writing test signal to file'
inFileName
dlmwrite(inFileName,x(1:nOfFrames*P,:),'delimiter',',');
    
