------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.unb1_board_pkg.all;

entity unb1_board_back_io is
  generic (
    g_bus_w : natural := 4
  );
  port (
    tx_serial_2arr  : in  t_unb1_board_back_sl_2arr := (others => (others => '0'));
    rx_serial_2arr  : out t_unb1_board_back_sl_2arr;

    -- Serial I/O
    -- . hard IP transceivers busses
    BN_BI_0_TX      : out std_logic_vector(g_bus_w - 1 downto 0);
    BN_BI_0_RX      : in  std_logic_vector(g_bus_w - 1 downto 0) := (others => '0');
    BN_BI_1_TX      : out std_logic_vector(g_bus_w - 1 downto 0);
    BN_BI_1_RX      : in  std_logic_vector(g_bus_w - 1 downto 0) := (others => '0');
    BN_BI_2_TX      : out std_logic_vector(g_bus_w - 1 downto 0);
    BN_BI_2_RX      : in  std_logic_vector(g_bus_w - 1 downto 0) := (others => '0');
    -- . soft IP transceivers bus (typically not used on UniBoard)
    BN_BI_3_TX      : out std_logic_vector(g_bus_w - 1 downto 0);
    BN_BI_3_RX      : in  std_logic_vector(g_bus_w - 1 downto 0) := (others => '0')
  );
end unb1_board_back_io;

architecture str of unb1_board_back_io is
begin
  -- Map the serial streams to the back
  wires : for I in 0 to g_bus_w - 1 generate
    BN_BI_0_TX(I)        <= tx_serial_2arr(0)(I);
    BN_BI_1_TX(I)        <= tx_serial_2arr(1)(I);
    BN_BI_2_TX(I)        <= tx_serial_2arr(2)(I);
    BN_BI_3_TX(I)        <= tx_serial_2arr(3)(I);

    rx_serial_2arr(0)(I) <= BN_BI_0_RX(I);
    rx_serial_2arr(1)(I) <= BN_BI_1_RX(I);
    rx_serial_2arr(2)(I) <= BN_BI_2_RX(I);
    rx_serial_2arr(3)(I) <= BN_BI_3_RX(I);
  end generate;
end;
