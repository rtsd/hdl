--------------------------------------------------------------------------------
--
-- Author: Daniel van der Schuur
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

package corr_permutor_pkg is
  -- 3D array of dimensions nof_folding_cycles*nof_outputs*2 input indices
  type t_corr_permutation_table is array (integer range <>, integer range <>) of t_natural_arr(1 downto 0);

  function corr_permute(nof_inputs : natural; nof_folding_cycles : natural) return t_corr_permutation_table;
end corr_permutor_pkg;

package body corr_permutor_pkg is
  -----------------------------------------------------------------------------
  -- Create a table with input->output permutation indices: returns all unique
  -- pairs (permutations) of the input indices 0...nof_inputs-1.
  -- . Originally used to generate permutation wiring directly from the inputs,
  --   this function now uses the same principles but supports folding.
  -----------------------------------------------------------------------------
  function corr_permute(nof_inputs : natural; nof_folding_cycles : natural) return t_corr_permutation_table is
    constant c_nof_permutations : natural := nof_inputs * (nof_inputs + 1) / 2;
    constant c_nof_outputs      : natural := c_nof_permutations / nof_folding_cycles;
    constant c_input_index_arr  : t_natural_arr(0 to nof_inputs - 1) := array_init(0, nof_inputs, 1);  -- array_init resembles Python's range(nof_inputs). We're using a TO range because array_init also uses a TO range.
    variable v_input_index_mat  : t_integer_matrix(nof_inputs - 1 downto 0, nof_inputs - 1 downto 0);
    variable v_out_index        : natural;
    variable v_duplicates       : natural;
    variable v_output_index_mat : t_integer_matrix(c_nof_permutations - 1 downto 0, 2 - 1 downto 0);  -- Array of permutation pairs
    variable v_result           : t_corr_permutation_table(nof_folding_cycles - 1 downto 0, c_nof_outputs - 1 downto 0);
  begin
    -----------------------------------------------------------------------------
    -- Create a 2d array of the input indices:
    -- . For each input number (1st dimension), all input numbers are present in
    --   the second dimension.
    --   . (0,(0,1,2,3)),(1,(0,1,2,3)), .. etc
    --   . Duplicating the indices in the 2nd dimension eases the permutation
    --     assignment done below.
    -----------------------------------------------------------------------------
    for i in 0 to nof_inputs - 1 loop
      for j in 0 to nof_inputs - 1 loop
        v_input_index_mat(i, j) := c_input_index_arr(i);
      end loop;

    end loop;

    -----------------------------------------------------------------------------
    -- Permutation table, 4-input example:
    -- ====================================
    --         0    1    2    3    4    5    6    7    8    9 <-- v_output_index_mat[c_nof_permutations][2]
    --  _      |    |    |    |    |    |    |    |    |    |
    -- |0|0-->[0]   |    |    |    |    |    |    |    |    |
    -- | |1------->[0]   |    |    |    |    |    |    |    |
    -- | |2---------|-->[0]   |    |    |    |    |    |    |
    -- |_|3---------|----|-->[0]   |    |    |    |    |    |
    -- |1|0------->[1]   |    |    |    |    |    |    |    |
    -- | |1--------------|----|-->[1]   |    |    |    |    |
    -- | |2--------------|----|------->[1]   |    |    |    |
    -- |_|3--------------|----|---------|-->[1]   |    |    |
    -- |2|0------------>[2]   |         |    |    |    |    |
    -- | |1-------------------|------->[2]   |    |    |    |
    -- | |2-------------------|--------------|-->[2]   |    |
    -- |_|3-------------------|--------------|------->[2]   |
    -- |3|0----------------->[3]             |         |    |
    -- | |1-------------------------------->[3]        |    |
    -- | |2------------------------------------------>[3]   |
    -- |_|3----------------------------------------------->[3]
    --  ^
    --  |
    --  v_input_index_mat[nof_inputs][nof_inputs]
    --
    -- Observe the following:
    -- . Inverse indexing: E.g. permutation[5], pair index 1 gets assigned
    --   indices opposite to those of pair index 0:
    --   . v_output_index_mat(5)(0) <= v_input_index_mat(1)(2);
    --     v_output_index_mat(5)(1) <= v_input_index_mat(2)(1);
    -- . The number of inputs that carry duplicates increments by one
    --   for each input increment (only pair index 0 shown):
    --   . v_output_index_mat(0)(0) <= v_input_index_mat(0)(0); --v_input_index_mat(0): 0 duplicates
    --     v_output_index_mat(1)(0) <= v_input_index_mat(0)(1);
    --     v_output_index_mat(2)(0) <= v_input_index_mat(0)(2);
    --     v_output_index_mat(3)(0) <= v_input_index_mat(0)(3);
    --     -- Duplicate              v_input_index_mat(1)(0); --v_input_index_mat(1): 1 duplicate
    --     v_output_index_mat(4)(0) <= v_input_index_mat(1)(1);
    --     v_output_index_mat(5)(0) <= v_input_index_mat(1)(2);
    --     v_output_index_mat(6)(0) <= v_input_index_mat(1)(3);
    --     -- Duplicate              v_input_index_mat(2)(0); --v_input_index_mat(2): 2 duplicates
    --     -- Duplicate              v_input_index_mat(2)(1);
    --     v_output_index_mat(7)(0) <= v_input_index_mat(2)(2);
    --     v_output_index_mat(8)(0) <= v_input_index_mat(2)(3);
    --     -- Duplicate              v_input_index_mat(3)(0); --v_input_index_mat(3): 3 duplicates
    --     -- Duplicate              v_input_index_mat(3)(1);
    --     -- Duplicate              v_input_index_mat(3)(2);
    --     v_output_index_mat(9)(0) <= v_input_index_mat(3)(3);
    -----------------------------------------------------------------------------
    v_out_index  := 0;
    v_duplicates := 0;
    for i in 0 to nof_inputs - 1 loop
      v_duplicates := i;
      for j in 0 to nof_inputs - 1 loop
        -- Create our permutation pair by using inverse indices (j,i) and (i,j)
        v_output_index_mat(v_out_index, 0) := v_input_index_mat(i, j);
        v_output_index_mat(v_out_index, 1) := v_input_index_mat(j, i);
        if v_duplicates = 0 then
          -- We created a unique pair. Keep incrementing the index.
          v_out_index := v_out_index + 1;
        else
          -- We just created a duplicate permutation pair. Don't increment
          -- v_out_index so that permutation pair will get re-assigned in the
          -- next loop iteration.
          v_duplicates := v_duplicates - 1;
        end if;
      end loop;
    end loop;

    -----------------------------------------------------------------------------
    -- Take our flat (single cycle) table and split it into nof_folding_cycles
    -- subtables (creates a 3D array); provides an input/output mapping for each
    -- folding clock cycle.
    -----------------------------------------------------------------------------
    for i in 0 to nof_folding_cycles - 1 loop
      for j in 0 to c_nof_outputs - 1 loop
        v_result(i, j)(0) := v_output_index_mat(i * c_nof_outputs + j, 0);
        v_result(i, j)(1) := v_output_index_mat(i * c_nof_outputs + j, 1);
      end loop;
    end loop;

    return v_result;
  end corr_permute;
end corr_permutor_pkg;
