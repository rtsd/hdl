%% poly_real.m
%
% Polyphase filterbank, real input
% Optionally performs frequency rotation for oversampling > 1
%
%% Input 
% a: single column real sample vector
% filt: Coefficient filter, size (2n*k,1)
% n: Number of output channels
% ovs: oversampling factor, default is 1
% rotate: if specified and > 0 then rotate frequencies to 
%     compensate oversampling phase
%
%% Output
% x:  matrix, with rows for frequency and columns for time
%
%% Code
%
function x=poly_real(a, filt, n, ovs, rotate)
  na=size(a,1);	% Recover size of input
  rot = 0; 
  r0=0;
  n2=2*n;
  if  nargin >= 4
      n1 = round(n*2/ovs);% n1 is oversampled output period
      if (nargin > 4)  && (rotate > 0)
         rot = n1;
      end;
  else
      n1 = 2*n;		% Default: oversampling = 1 (no ovs)
  end
  nf = size(filt,1); 
  ns = nf/(n2);  	% Number of segments in filter
  x=zeros(1, n);	% Output matrix: rows = time, cols = freq
  i0=1;			% Starting point in input data
  j=1;			% Index of output row (time)
  % Loop over time segments. 
  % Each time segment is an FFT of 2n points taken on nf points 
  %   convolved with the filter
  % Successive segments are separated by 2n/ovs = 2*n1 points 
  % Only positive frequencies are preserved (input is real) 
  %
  while i0+nf <= na
      t = zeros(n2,1);    % filtered and folded samples
      for i=0:(ns-1)      % accumulate filtered data in polyphase flter
                          % filter is "folded" on 2n points
          t = t + a((i0+i*n2):(i0+(i+1)*n2-1)).*filt((i*n2+1):((i+1)*n2));
      end
      if (rot == 0) 
          f = fft(t);     % Fourier transform
      else                % Fourier transform with linear phase 
          f=fft(circshift(t,r0));
          r0 = mod(r0+rot,n2);
      end 
      x(j,:) = f(1:n)';   % copy data in output vector (only pos. freqs)
      i0 = i0+n1;
      j = j+1;
  end
end
