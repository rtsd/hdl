-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- as 10
-- run -all

entity tb_tb_uth is
end tb_tb_uth;

architecture tb of tb_tb_uth is
  constant c_phy_link_valid_support : boolean := true;
  constant c_nof_repeat             : natural := 10;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- in_en = active, src_in.ready = active
  u_hold_adapt_act_act         : entity work.tb_uth generic map ( "HOLD", "ADAPT",     c_phy_link_valid_support, e_active, e_active, c_nof_repeat);
  u_hold_hold_act_act          : entity work.tb_uth generic map ( "HOLD",  "HOLD",     c_phy_link_valid_support, e_active, e_active, c_nof_repeat);
  u_delay_adapt_act_act        : entity work.tb_uth generic map ("DELAY", "ADAPT",     c_phy_link_valid_support, e_active, e_active, c_nof_repeat);
  u_delay_hold_act_act         : entity work.tb_uth generic map ("DELAY",  "HOLD",     c_phy_link_valid_support, e_active, e_active, c_nof_repeat);

  -- in_en = active, src_in.ready = active and no valid support on the link, this requires always active streaming control
  u_noval_hold_adapt_act_act   : entity work.tb_uth generic map ( "HOLD", "ADAPT", not c_phy_link_valid_support, e_active, e_active, c_nof_repeat);
  u_noval_hold_hold_act_act    : entity work.tb_uth generic map ( "HOLD",  "HOLD", not c_phy_link_valid_support, e_active, e_active, c_nof_repeat);
  u_noval_delay_adapt_act_act  : entity work.tb_uth generic map ("DELAY", "ADAPT", not c_phy_link_valid_support, e_active, e_active, c_nof_repeat);
  u_noval_delay_hold_act_act   : entity work.tb_uth generic map ("DELAY",  "HOLD", not c_phy_link_valid_support, e_active, e_active, c_nof_repeat);

  -- in_en = active, src_in.ready = random
  u_hold_adapt_act_rnd         : entity work.tb_uth generic map ( "HOLD", "ADAPT",     c_phy_link_valid_support, e_active, e_random, c_nof_repeat);
  u_hold_hold_act_rnd          : entity work.tb_uth generic map ( "HOLD",  "HOLD",     c_phy_link_valid_support, e_active, e_random, c_nof_repeat);
  u_delay_adapt_act_rnd        : entity work.tb_uth generic map ("DELAY", "ADAPT",     c_phy_link_valid_support, e_active, e_random, c_nof_repeat);
  u_delay_hold_act_rnd         : entity work.tb_uth generic map ("DELAY",  "HOLD",     c_phy_link_valid_support, e_active, e_random, c_nof_repeat);

  -- in_en = random, src_in.ready = random
  u_hold_adapt_rnd_rnd         : entity work.tb_uth generic map ( "HOLD", "ADAPT",     c_phy_link_valid_support, e_random, e_random, c_nof_repeat);
  u_hold_hold_rnd_rnd          : entity work.tb_uth generic map ( "HOLD",  "HOLD",     c_phy_link_valid_support, e_random, e_random, c_nof_repeat);
  u_delay_adapt_rnd_rnd        : entity work.tb_uth generic map ("DELAY", "ADAPT",     c_phy_link_valid_support, e_random, e_random, c_nof_repeat);
  u_delay_hold_rnd_rnd         : entity work.tb_uth generic map ("DELAY",  "HOLD",     c_phy_link_valid_support, e_random, e_random, c_nof_repeat);

  -- in_en = active, src_in.ready = pulse
  u_hold_adapt_act_pls         : entity work.tb_uth generic map ( "HOLD", "ADAPT",     c_phy_link_valid_support, e_active, e_pulse, c_nof_repeat);
  u_hold_hold_act_pls          : entity work.tb_uth generic map ( "HOLD",  "HOLD",     c_phy_link_valid_support, e_active, e_pulse, c_nof_repeat);
  u_delay_adapt_act_pls        : entity work.tb_uth generic map ("DELAY", "ADAPT",     c_phy_link_valid_support, e_active, e_pulse, c_nof_repeat);
  u_delay_hold_act_pls         : entity work.tb_uth generic map ("DELAY",  "HOLD",     c_phy_link_valid_support, e_active, e_pulse, c_nof_repeat);

  -- in_en = pulse, src_in.ready = pulse
  u_hold_adapt_pls_pls         : entity work.tb_uth generic map ( "HOLD", "ADAPT",     c_phy_link_valid_support, e_pulse, e_pulse, c_nof_repeat);
  u_hold_hold_pls_pls          : entity work.tb_uth generic map ( "HOLD",  "HOLD",     c_phy_link_valid_support, e_pulse, e_pulse, c_nof_repeat);
  u_delay_adapt_pls_pls        : entity work.tb_uth generic map ("DELAY", "ADAPT",     c_phy_link_valid_support, e_pulse, e_pulse, c_nof_repeat);  -- much faster than the uth_tx(rtl_hold)
  u_delay_hold_pls_pls         : entity work.tb_uth generic map ("DELAY",  "HOLD",     c_phy_link_valid_support, e_pulse, e_pulse, c_nof_repeat);  -- much faster than the uth_tx(rtl_hold)
end tb;
