-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, common_mult_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_field_pkg.all;

-- Purpose:
-- . Calculate UDP total length and IP total length fields
-- Description:
-- . The length fields are calculated based on the status outputs
--   of dp_split and dp_packet_merge:
--   . udp_total_length = c_udp_header_len + nof_data_bytes + c_user_hdr_len
--     . c_udp_header_len = 8 (fixed) bytes
--     . nof_data_bytes   = nof_words_per_block * nof_blocks_per_packet * 4 bytes
--     . c_user_hdr_len   = nof bytes in remaining header
--   . ip_total_length = udp_total_length - c_ip_header_len

entity dp_offload_tx_len_calc is
  generic (
    g_technology      : natural := c_tech_select_default;
    g_hdr_field_arr   : t_common_field_arr;
    g_data_w          : natural;  -- Must be a power of 2
    g_word_align_len  : natural := 2
  );
  port (
    rst     : in  std_logic;
    clk     : in  std_logic;

    nof_words_per_block   : in std_logic_vector;
    nof_blocks_per_packet : in std_logic_vector;

    udp_total_length : out std_logic_vector(15 downto 0);
    ip_total_length  : out std_logic_vector(15 downto 0)
  );
end dp_offload_tx_len_calc;

architecture str of dp_offload_tx_len_calc is
  constant c_eth_header_len    : natural := 14;
  constant c_ip_header_len     : natural := 20;
  constant c_udp_header_len    : natural := 8;

  constant c_total_hdr_len     : natural := field_slv_len(g_hdr_field_arr) / c_byte_w;
  constant c_user_hdr_len      : natural := c_total_hdr_len - g_word_align_len - c_eth_header_len - c_ip_header_len - c_udp_header_len;

  constant c_product_w         : natural := nof_words_per_block'length + nof_blocks_per_packet'length;
  constant c_nof_bits_to_shift : natural := true_log2(g_data_w / c_byte_w);  -- number of bits to shift left to yield nof_data_bytes)
  constant c_shift_left_int    : integer := -1 * c_nof_bits_to_shift;
  constant c_adder_in_w        : natural := 16;

  signal nof_data_words        : std_logic_vector(c_product_w - 1 downto 0);
  signal nof_data_bytes        : std_logic_vector(c_product_w + c_nof_bits_to_shift - 1 downto 0);

  signal udp_adder_in          : std_logic_vector(3 * c_adder_in_w - 1 downto 0);
  signal udp_adder_out         : std_logic_vector(c_adder_in_w + 2 - 1 downto 0);

  signal ip_adder_in           : std_logic_vector(2 * c_adder_in_w - 1 downto 0);
  signal ip_adder_out          : std_logic_vector(c_adder_in_w + 1 - 1 downto 0);
begin
  ---------------------------------------------------------------------------------------
  -- Calculate number of payload words
  ---------------------------------------------------------------------------------------
  u_common_mult: entity common_mult_lib.common_mult
  generic map (
    g_technology       => g_technology,
    g_variant          => "RTL",
    g_in_a_w           => nof_words_per_block'LENGTH,
    g_in_b_w           => nof_blocks_per_packet'LENGTH,
    g_out_p_w          => c_product_w,
    g_pipeline_input   => 0,
    g_pipeline_product => 0,
    g_pipeline_output  => 0,
    g_representation   => "UNSIGNED"
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_a    => nof_words_per_block,
    in_b    => nof_blocks_per_packet,
    out_p   => nof_data_words
  );

  ---------------------------------------------------------------------------------------
  -- Calculate number of payload bytes by bit shifting to the left
  ---------------------------------------------------------------------------------------
  nof_data_bytes <= SHIFT_UVEC( RESIZE_UVEC(nof_data_words, c_product_w + c_nof_bits_to_shift), c_shift_left_int);

  ---------------------------------------------------------------------------------------
  -- Calculate UDP total length
  ---------------------------------------------------------------------------------------
  udp_adder_in <= RESIZE_UVEC(nof_data_bytes, c_adder_in_w) & TO_UVEC(c_udp_header_len, c_adder_in_w) & TO_UVEC(c_user_hdr_len, c_adder_in_w);

  u_common_adder_tree_udp : entity common_lib.common_adder_tree(str)
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => 0,
    g_nof_inputs     => 3,
    g_dat_w          => c_adder_in_w,
    g_sum_w          => c_adder_in_w + 2
  )
  port map (
    clk    => clk,
    in_dat => udp_adder_in,
    sum    => udp_adder_out
  );

  udp_total_length <= udp_adder_out(c_adder_in_w - 1 downto 0);

  ---------------------------------------------------------------------------------------
  -- Calculate IP total length
  ---------------------------------------------------------------------------------------
  ip_adder_in <= RESIZE_UVEC(udp_adder_out, c_adder_in_w) & TO_UVEC(c_ip_header_len, c_adder_in_w);

  u_common_adder_tree_ip : entity common_lib.common_adder_tree(str)
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => 0,
    g_nof_inputs     => 2,
    g_dat_w          => c_adder_in_w,
    g_sum_w          => c_adder_in_w + 1
  )
  port map (
    clk    => clk,
    in_dat => ip_adder_in,
    sum    => ip_adder_out
  );

  ip_total_length <= ip_adder_out(c_adder_in_w - 1 downto 0);
end str;
