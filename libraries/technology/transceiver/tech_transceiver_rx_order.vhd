--------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity tech_transceiver_rx_order is
  generic (
    g_data_w  : natural := 16
  );
  port (
    rx_clk       : in  std_logic;
    rx_rst       : in  std_logic;

    rx_data_in   : in  std_logic_vector(g_data_w - 1 downto 0);
    rx_ctrl_in   : in  std_logic_vector(g_data_w / c_byte_w - 1 downto 0);

    rx_data_out  : out std_logic_vector(g_data_w - 1 downto 0);
    rx_valid_out : out std_logic
  );
end entity;

architecture rtl of tech_transceiver_rx_order is
  signal in_hi_dat   : std_logic_vector(g_data_w / 2 - 1 downto 0);
  signal in_lo_dat   : std_logic_vector(g_data_w / 2 - 1 downto 0);
  signal in_hi_val   : std_logic;
  signal in_lo_val   : std_logic;

  signal nxt_rx_data_out : std_logic_vector(rx_data_out'range);
  signal nxt_rx_valid_out : std_logic;
  signal odd_dat     : std_logic_vector(in_lo_dat'range);
  signal nxt_odd_dat : std_logic_vector(odd_dat'range);
  signal odd_val     : std_logic;
  signal nxt_odd_val : std_logic;
begin
  in_hi_dat <= rx_data_in(g_data_w - 1 downto g_data_w / 2);
  in_hi_val <= not rx_ctrl_in(rx_ctrl_in'high);

  in_lo_dat <= rx_data_in(g_data_w / 2 - 1 downto 0);
  in_lo_val <= not rx_ctrl_in(rx_ctrl_in'low);

  regs: process (rx_rst, rx_clk)
  begin
    if rx_rst = '1' then
      rx_valid_out <= '0';
      rx_data_out <= (others => '0');
      odd_val <= '0';
      odd_dat <= (others => '0');
    elsif rising_edge(rx_clk) then
      rx_valid_out <= nxt_rx_valid_out;
      rx_data_out <= nxt_rx_data_out;
      odd_val <= nxt_odd_val;
      odd_dat <= nxt_odd_dat;
    end if;
  end process;

  odd_proc : process (in_hi_dat, in_hi_val, in_lo_dat, in_lo_val, odd_dat, odd_val)
  begin
    nxt_odd_val <= '0';

    if in_hi_val = '1' and in_lo_val = '0' and odd_val = '0' then
      nxt_odd_val <= '1';
    elsif in_hi_val = '1' and in_lo_val = '1' and odd_val = '1' then
      nxt_odd_val <= '1';
    end if;

    nxt_odd_dat <= in_hi_dat;
  end process;

  out_proc : process (in_hi_dat, in_hi_val, in_lo_dat, in_lo_val, odd_dat, odd_val)
  begin
    nxt_rx_valid_out <= (in_hi_val and in_lo_val) or (odd_val and (in_hi_val or in_lo_val));
    nxt_rx_data_out <= (others => '0');

    if odd_val = '0' then
      nxt_rx_data_out(2 * g_data_w / 2 - 1 downto 0) <= in_hi_dat & in_lo_dat;
    else
      nxt_rx_data_out(2 * g_data_w / 2 - 1 downto 0) <= in_lo_dat & odd_dat;
    end if;
  end process;
end rtl;
