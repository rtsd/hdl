-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--   Provide input ready control and use output ready control to the FIFO.
--   Pass sop and eop along with the data through the FIFO if g_use_ctrl=TRUE.
--   Default the RL=1, use g_fifo_rl=0 for a the show ahead FIFO.
-- Description:
--   Provide the sink ready for FIFO write control and use source ready for
--   FIFO read access. The sink ready output is derived from FIFO almost full.
--   Data without framing can use g_use_ctrl=FALSE to avoid implementing two
--   data bits for sop and eop in the FIFO word width. Idem for g_use_sync,
--   g_use_empty, g_use_channel and g_use_error.
-- Remark:
-- . The bsn, empty, channel and error fields are valid at the sop and or eop.
--   Therefore alternatively these fields can be passed on through a separate
--   FIFO, with only one entry per frame, to save FIFO memory in case
--   concatenating them makes the FIFO word width larger than a standard
--   memory data word width.
-- . The FIFO makes that the src_in.ready and snk_out.ready are not
--   combinatorially connected, so this can ease the timing closure for the
--   ready signal.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_fifo_core is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_note_is_ful    : boolean := true;  -- when TRUE report NOTE when FIFO goes full, fifo overflow is always reported as FAILURE
    g_use_dual_clock : boolean := false;
    g_use_lut_sc     : boolean := false;  -- when TRUE then force using LUTs instead of block RAM for single clock FIFO (bot available for dual clock FIFO)
    g_data_w         : natural := 16;  -- Should be 2 times the c_complex_w if g_use_complex = TRUE
    g_data_signed    : boolean := false;  -- TRUE extends g_data_w bits with the sign bit, FALSE pads g_data_w bits with zeros.
    g_bsn_w          : natural := 1;
    g_empty_w        : natural := 1;
    g_channel_w      : natural := 1;
    g_error_w        : natural := 1;
    g_use_bsn        : boolean := false;
    g_use_empty      : boolean := false;
    g_use_channel    : boolean := false;
    g_use_error      : boolean := false;
    g_use_sync       : boolean := false;
    g_use_ctrl       : boolean := true;  -- sop & eop
    g_use_complex    : boolean := false;  -- TRUE feeds the concatenated complex fields (im & re) through the FIFO instead of the data field.
    g_fifo_size      : natural := 512;  -- (16+2) * 512 = 1 M9K, g_data_w+2 for sop and eop
    g_fifo_af_margin : natural := 4;  -- >=4, Nof words below max (full) at which fifo is considered almost full for snk_out.ready
    g_fifo_af_xon    : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_fifo_rl        : natural := 1
  );
  port (
    wr_rst      : in  std_logic;
    wr_clk      : in  std_logic;
    rd_rst      : in  std_logic;
    rd_clk      : in  std_logic;
    -- Monitor FIFO filling
    wr_ful      : out std_logic;  -- corresponds to the carry bit of wr_usedw when FIFO is full
    wr_usedw    : out std_logic_vector(ceil_log2(g_fifo_size) - 1 downto 0);
    rd_usedw    : out std_logic_vector(ceil_log2(g_fifo_size) - 1 downto 0);
    rd_emp      : out std_logic;
    -- ST sink
    snk_out     : out t_dp_siso;
    snk_in      : in  t_dp_sosi;
    -- ST source
    src_in      : in  t_dp_siso;
    src_out     : out t_dp_sosi
  );
end dp_fifo_core;

architecture str of dp_fifo_core is
  constant c_use_data         : boolean := true;
  constant c_ctrl_w           : natural := 2;  -- sop and eop

  constant c_complex_w        : natural := smallest(c_dp_stream_dsp_data_w, g_data_w / 2);  -- needed to cope with g_data_w > 2*c_dp_stream_dsp_data_w

  constant c_fifo_almost_full : natural := g_fifo_size - g_fifo_af_margin;  -- FIFO almost full level for snk_out.ready
  constant c_fifo_almost_xon  : natural := g_fifo_size - g_fifo_af_xon;  -- FIFO almost full level for snk_out.xon
  constant c_fifo_dat_w       : natural := func_slv_concat_w(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl,
                                                             g_data_w,   g_bsn_w,   g_empty_w,   g_channel_w,   g_error_w,   1,          c_ctrl_w);  -- concat via FIFO

  signal nxt_snk_out   : t_dp_siso := c_dp_siso_rst;

  signal arst          : std_logic;

  signal wr_data_complex : std_logic_vector(2 * c_complex_w - 1 downto 0);
  signal wr_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal rd_data         : std_logic_vector(g_data_w - 1 downto 0);

  signal fifo_wr_dat   : std_logic_vector(c_fifo_dat_w - 1 downto 0);
  signal fifo_wr_req   : std_logic;
  signal fifo_wr_ful   : std_logic;
  signal wr_init       : std_logic := '0';
  signal fifo_wr_usedw : std_logic_vector(wr_usedw'range);

  signal fifo_rd_dat   : std_logic_vector(c_fifo_dat_w - 1 downto 0) := (others => '0');
  signal fifo_rd_val   : std_logic;
  signal fifo_rd_req   : std_logic;
  signal fifo_rd_emp   : std_logic;
  signal fifo_rd_usedw : std_logic_vector(rd_usedw'range);

  signal wr_sync       : std_logic_vector(0 downto 0);
  signal rd_sync       : std_logic_vector(0 downto 0);
  signal wr_ctrl       : std_logic_vector(1 downto 0);
  signal rd_ctrl       : std_logic_vector(1 downto 0);

  signal rd_siso       : t_dp_siso;
  signal rd_sosi       : t_dp_sosi := c_dp_sosi_rst;  -- initialize default values for unused sosi fields
begin
  -- Output monitor FIFO filling
  wr_ful   <= fifo_wr_ful;
  wr_usedw <= fifo_wr_usedw;
  rd_usedw <= fifo_rd_usedw;
  rd_emp   <= fifo_rd_emp;

  p_wr_clk: process(wr_clk, wr_rst)
  begin
    if wr_rst = '1' then
      snk_out <= c_dp_siso_rst;
    elsif rising_edge(wr_clk) then
      snk_out <= nxt_snk_out;
    end if;
  end process;

  wr_sync(0) <= snk_in.sync;
  wr_ctrl    <= snk_in.sop & snk_in.eop;

  -- Assign the snk_in data field or concatenated complex fields to the FIFO wr_data depending on g_use_complex
  wr_data_complex <= snk_in.im(c_complex_w - 1 downto 0) & snk_in.re(c_complex_w - 1 downto 0);
  wr_data         <= snk_in.data(g_data_w - 1 downto 0) when g_use_complex = false else RESIZE_UVEC(wr_data_complex, g_data_w);

  -- fifo wr wires
  fifo_wr_req <= snk_in.valid;
  fifo_wr_dat <= func_slv_concat(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl,
                                 wr_data,
                                 snk_in.bsn(        g_bsn_w - 1 downto 0),
                                 snk_in.empty(    g_empty_w - 1 downto 0),
                                 snk_in.channel(g_channel_w - 1 downto 0),
                                 snk_in.err(      g_error_w - 1 downto 0),
                                 wr_sync,
                                 wr_ctrl);

  -- pass on frame level flow control from src_in.xon to upstream snk_out.xon, and
  -- add flow contol dependent on whether the fifo can fit another block
  nxt_snk_out.xon <= src_in.xon when unsigned(fifo_wr_usedw) <= c_fifo_almost_xon else '0';

  -- up stream use fifo almost full to control snk_out.ready
  nxt_snk_out.ready <= not wr_init when unsigned(fifo_wr_usedw) < c_fifo_almost_full else '0';

  gen_common_fifo_sc : if g_use_dual_clock = false generate
    u_common_fifo_sc : entity common_lib.common_fifo_sc
    generic map (
      g_technology => g_technology,
      g_note_is_ful => g_note_is_ful,
      g_use_lut   => g_use_lut_sc,
      g_dat_w     => c_fifo_dat_w,
      g_nof_words => g_fifo_size
    )
    port map (
      rst      => rd_rst,
      clk      => rd_clk,
      wr_dat   => fifo_wr_dat,
      wr_req   => fifo_wr_req,
      wr_ful   => fifo_wr_ful,
      rd_dat   => fifo_rd_dat,
      rd_req   => fifo_rd_req,
      rd_emp   => fifo_rd_emp,
      rd_val   => fifo_rd_val,
      usedw    => fifo_rd_usedw
    );

    wr_init <= '0';  -- to avoid no driver warning in synthesis
    fifo_wr_usedw <= fifo_rd_usedw;
  end generate;

  gen_common_fifo_dc : if g_use_dual_clock = true generate
    u_common_fifo_dc : entity common_lib.common_fifo_dc
    generic map (
      g_technology => g_technology,
      g_note_is_ful => g_note_is_ful,
      g_dat_w     => c_fifo_dat_w,
      g_nof_words => g_fifo_size
    )
    port map (
      rst     => arst,
      wr_clk  => wr_clk,
      wr_dat  => fifo_wr_dat,
      wr_req  => fifo_wr_req,
      wr_init_out => wr_init,
      wr_ful  => fifo_wr_ful,
      wrusedw => fifo_wr_usedw,
      rd_clk  => rd_clk,
      rd_dat  => fifo_rd_dat,
      rd_req  => fifo_rd_req,
      rd_emp  => fifo_rd_emp,
      rdusedw => fifo_rd_usedw,
      rd_val  => fifo_rd_val
    );

    arst <= wr_rst or rd_rst;
  end generate;

  -- Extract the data from the wide FIFO output SLV. rd_data will be assigned to rd_sosi.data or rd_sosi.im & rd_sosi.re depending on g_use_complex.
  rd_data <= func_slv_extract(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl, g_data_w, g_bsn_w, g_empty_w, g_channel_w, g_error_w, 1, c_ctrl_w, fifo_rd_dat, 0);

  -- fifo rd wires
  -- SISO
  fifo_rd_req <= rd_siso.ready;

  -- SOSI
  rd_sosi.data    <= RESIZE_DP_SDATA(rd_data) when g_data_signed = true else RESIZE_DP_DATA(rd_data);
  rd_sosi.re      <= RESIZE_DP_DSP_DATA(rd_data(  c_complex_w - 1 downto 0));
  rd_sosi.im      <= RESIZE_DP_DSP_DATA(rd_data(2 * c_complex_w - 1 downto c_complex_w));
  rd_sosi.bsn     <= RESIZE_DP_BSN(func_slv_extract(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl, g_data_w, g_bsn_w, g_empty_w, g_channel_w, g_error_w, 1, c_ctrl_w, fifo_rd_dat, 1));
  rd_sosi.empty   <= RESIZE_DP_EMPTY(func_slv_extract(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl, g_data_w, g_bsn_w, g_empty_w, g_channel_w, g_error_w, 1, c_ctrl_w, fifo_rd_dat, 2));
  rd_sosi.channel <= RESIZE_DP_CHANNEL(func_slv_extract(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl, g_data_w, g_bsn_w, g_empty_w, g_channel_w, g_error_w, 1, c_ctrl_w, fifo_rd_dat, 3));
  rd_sosi.err     <= RESIZE_DP_ERROR(func_slv_extract(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl, g_data_w, g_bsn_w, g_empty_w, g_channel_w, g_error_w, 1, c_ctrl_w, fifo_rd_dat, 4));
  rd_sync         <= func_slv_extract(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl, g_data_w, g_bsn_w, g_empty_w, g_channel_w, g_error_w, 1, c_ctrl_w, fifo_rd_dat, 5);
  rd_ctrl         <= func_slv_extract(c_use_data, g_use_bsn, g_use_empty, g_use_channel, g_use_error, g_use_sync, g_use_ctrl, g_data_w, g_bsn_w, g_empty_w, g_channel_w, g_error_w, 1, c_ctrl_w, fifo_rd_dat, 6);

  rd_sosi.sync    <= fifo_rd_val and rd_sync(0);
  rd_sosi.valid   <= fifo_rd_val;
  rd_sosi.sop     <= fifo_rd_val and rd_ctrl(1);
  rd_sosi.eop     <= fifo_rd_val and rd_ctrl(0);

  u_ready_latency : entity work.dp_latency_adapter
  generic map (
    g_in_latency  => 1,
    g_out_latency => g_fifo_rl
  )
  port map (
    rst       => rd_rst,
    clk       => rd_clk,
    -- ST sink
    snk_out   => rd_siso,
    snk_in    => rd_sosi,
    -- ST source
    src_in    => src_in,
    src_out   => src_out
  );
end str;
