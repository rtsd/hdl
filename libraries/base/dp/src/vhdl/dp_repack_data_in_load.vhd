--------------------------------------------------------------------------------
--
-- Copyright (C) 2025
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------
-- Author: Eric Kooistra
-- Purpose:
--   The dp_repack_data_in_load stage for dp_repack_data, using data load in.
-- Description:
--   Load in snk_in.data from high to low index in dat_arr. The dp_repack_data_in_load can handle empty words without
--   having to apply backpressure and is therefore suitable for snk_in.data without gaps in the valid.
library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity dp_repack_data_in_load is
  generic (
    g_bypass          : boolean := false;
    g_in_dat_w        : natural;
    g_in_nof_words    : natural;
    g_in_symbol_w     : natural := 1  -- default 1 for snk_in.empty in nof bits, else use power of 2
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    snk_out          : out t_dp_siso;
    snk_in           : in  t_dp_sosi;

    src_in           : in  t_dp_siso;
    src_out          : out t_dp_sosi;
    src_out_data     : out std_logic_vector(g_in_dat_w * g_in_nof_words - 1 downto 0)
  );
end dp_repack_data_in_load;

architecture rtl of dp_repack_data_in_load is
  constant c_in_buf_dat_w      : natural := g_in_dat_w * g_in_nof_words;
  constant c_bit_cnt_max       : natural := c_in_buf_dat_w;
  constant c_in_empty_lo       : natural := true_log2(g_in_symbol_w);

  type t_dat_arr  is array (integer range <>) of std_logic_vector(g_in_dat_w - 1 downto 0);

  type t_reg is record
    dat_arr       : t_dat_arr(g_in_nof_words - 1 downto 0);  -- internally use dat_arr[] to represent v.src_out.data
    src_out       : t_dp_sosi;  -- sosi output
    src_out_data  : std_logic_vector(c_in_buf_dat_w - 1 downto 0);  -- Use seperate slv to carry the sosi data as
                        -- c_in_buf_dat_w can be larger than c_dp_stream_data_w.
    hold_out      : t_dp_sosi;  -- hold snk_in.sync/sop/eop until end of section and then hold valid src_out until
                                -- src_in.ready
    load_index    : natural range 0 to g_in_nof_words;  -- count nof in_dat shifted in subsection
    dat_bit_cnt   : natural range 0 to c_bit_cnt_max;  -- actual nof bits in subsection
    pack_bit_cnt  : natural range 0 to c_bit_cnt_max;  -- count nof bits in subsection
  end record;

  signal data_vec   : std_logic_vector(c_in_buf_dat_w - 1 downto 0);

  signal r_snk_out  : t_dp_siso := c_dp_siso_rdy;
  signal r          : t_reg;
  signal nxt_r      : t_reg;

  -- Debug signals
  signal snk_in_data        : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal i_src_out          : t_dp_sosi;
  signal i_src_out_data     : std_logic_vector(c_in_buf_dat_w - 1 downto 0);

  signal dbg_g_in_dat_w   : natural := g_in_dat_w;
  signal dbg_in_nof_words : natural := g_in_nof_words;
  signal dbg_in_symbol_w  : natural := g_in_symbol_w;
  signal dbc_in_buf_dat_w : natural := c_in_buf_dat_w;
begin
  snk_in_data <= snk_in.data(g_in_dat_w - 1 downto 0);

  src_out      <= i_src_out;
  src_out_data <= i_src_out_data;

  gen_bypass : if g_bypass = true generate
    snk_out        <= src_in;
    i_src_out      <= snk_in;
    i_src_out_data <= RESIZE_UVEC(snk_in.data, c_in_buf_dat_w);
  end generate;

  no_bypass : if g_bypass = false generate
    p_comb : process(rst, r, snk_in, data_vec, src_in)
      variable v : t_reg;
    begin
      ------------------------------------------------------------------------
      -- Default
      v := r;
      v.src_out.sync  := '0';
      v.src_out.valid := '0';
      v.src_out.sop   := '0';
      v.src_out.eop   := '0';

      --------------------------------------------------------------------------
      -- Function
      if r.hold_out.valid = '0' then

        -- Clear hold_out for new output valid (= new subsection)
        if r.src_out.valid = '1' then
          v.hold_out := c_dp_sosi_rst;
        end if;

        -- Capture the snk_in block info that is valid at sop and eop
        if snk_in.sop = '1' then
          v.hold_out.sop    := '1';
          v.hold_out.sync   := snk_in.sync;
          v.src_out.bsn     := snk_in.bsn;
          v.src_out.channel := snk_in.channel;
        end if;
        if snk_in.eop = '1' then
          v.hold_out.eop    := '1';
          v.hold_out.empty  := SHIFT_UVEC(snk_in.empty, -c_in_empty_lo);  -- use snk_in.empty as offset for
                                                                          -- src_out.empty in nof bits
          v.src_out.err     := snk_in.err;
        end if;

        -- Capture the data per subsection in a block
        if snk_in.valid = '1' then
          -- load snk_in.data into v.dat_arr from high index g_in_nof_words - 1 to low index per subsection
          v.dat_arr(r.load_index) := snk_in.data(g_in_dat_w - 1 downto 0);

          -- Pack subsection control
          if r.load_index > 0 then
            -- Prepare next load_index
            v.load_index := r.load_index - 1;

            -- Count number of bits loaded in subsection
            v.pack_bit_cnt := r.pack_bit_cnt + g_in_dat_w;

            if snk_in.eop = '1' then
              -- Early end of pack subsection, with v.load_index number of empty words. Occurs when snk_in block does
              -- not have an integer multiple of g_in_nof_words
              v.dat_bit_cnt := v.pack_bit_cnt;  -- capture the current subsection pack_bit_cnt
              v.load_index := g_in_nof_words - 1;
              v.pack_bit_cnt := 0;
              v.hold_out.valid := '1';  -- the function has new data to output
            end if;
          else
            -- Default end of pack subsection, last word loaded in of complete subsection. Occurs during block
            -- or in the last subsection of a snk_in block with an integer multiple of g_in_nof_words.
            v.dat_bit_cnt := c_in_buf_dat_w;  -- set default pack_bit_cnt for complete subsection
            v.load_index := g_in_nof_words - 1;
            v.pack_bit_cnt := 0;
            v.hold_out.valid := '1';  -- the function has new data to output
          end if;
        end if;

        -- pass on the v.dat_arr as data vector
        v.src_out_data := data_vec;
        v.src_out.data := RESIZE_DP_DATA(data_vec);

        -- pass on dat_bit_cnt via DP empty field
        v.src_out.empty := INCR_UVEC(v.hold_out.empty, c_in_buf_dat_w - v.dat_bit_cnt);

        -- output input stage into output stage when ready, else hold_out.valid to signal pending output
        if v.hold_out.valid = '1' then
          if src_in.ready = '1' then
            v.src_out.valid := '1';
            v.src_out.sync  := v.hold_out.sync;
            v.src_out.sop   := v.hold_out.sop;
            v.src_out.eop   := v.hold_out.eop;
            v.hold_out.valid := '0';
          end if;
        end if;
      else
        -- pending output
        if src_in.ready = '1' then
          v.src_out.valid := '1';
          v.src_out.sync  := r.hold_out.sync;
          v.src_out.sop   := r.hold_out.sop;
          v.src_out.eop   := r.hold_out.eop;
          v.hold_out.valid := '0';
        end if;
      end if;

      ------------------------------------------------------------------------
      -- Reset and nxt_r
      if rst = '1' then
        v.src_out       := c_dp_sosi_rst;
        v.hold_out      := c_dp_sosi_rst;
        v.load_index    := g_in_nof_words - 1;
        v.dat_bit_cnt   := 0;
        v.pack_bit_cnt  := 0;
      end if;

      nxt_r <= v;
    end process;

    --------------------------------------------------------------------------
    -- p_reg
    r <= nxt_r when rising_edge(clk);

    --------------------------------------------------------------------------
    -- Wires
    p_data_vec : process(nxt_r)
    begin
      for I in 0 to g_in_nof_words - 1 loop
        data_vec((I + 1) * g_in_dat_w - 1 downto I * g_in_dat_w) <= nxt_r.dat_arr(I);
      end loop;
    end process;

    --------------------------------------------------------------------------
    -- Wired output
    i_src_out <= r.src_out;
    i_src_out_data <= r.src_out_data;
    --------------------------------------------------------------------------
    -- Flow control

    -- local function flow control
    r_snk_out <= c_dp_siso_rdy;  -- input shift in stage function is always ready

    -- combined local and remote src_in flow control
    snk_out.ready <= r_snk_out.ready when nxt_r.hold_out.valid = '0' else src_in.ready;  -- if there is pending output then the src_in ready determines the flow control
    snk_out.xon   <= src_in.xon;  -- just pass on the xon/off frame flow control
  end generate;
end rtl;
