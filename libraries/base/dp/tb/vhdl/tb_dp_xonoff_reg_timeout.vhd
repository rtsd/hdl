-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

--
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tb_dp_xonoff_reg_timeout is
end tb_dp_xonoff_reg_timeout;

architecture tb of tb_dp_xonoff_reg_timeout is
  constant clk_period : time := 20 ns;  -- 50 MHz
  constant st_clk_period : time := 5000 ps;  -- 200 MHz

  -- Minimum nof clk cycles between eop and sop
  constant c_500ns_latency : natural := 500 / 20;
  constant c_timeout_2us   : natural := 2000 / 20;

  signal tb_end        : std_logic := '0';
  signal clk           : std_logic := '1';
  signal st_clk        : std_logic := '1';
  signal rst           : std_logic;

  signal sla_in_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal sla_out_miso  : t_mem_miso := c_mem_miso_rst;

  signal xonoff_reg    : std_logic_vector(0 downto 0);
begin
  clk <= not clk or tb_end after clk_period / 2;
  st_clk <= not st_clk or tb_end after st_clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  p_stimuli : process
  begin
    wait until rst = '0';
    for I in 0 to c_500ns_latency - 1 loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- First
    ----------------------------------------------------------------------------
    sla_in_mosi.wrdata(0) <= '1';
    sla_in_mosi.wr <= '1';
    wait until rising_edge(clk);
    sla_in_mosi.wrdata(0) <= '0';
    sla_in_mosi.wr <= '0';
    for I in 0 to c_500ns_latency - 1 loop wait until rising_edge(clk); end loop;
    assert xonoff_reg(0) = '1'
      report "xonoff_reg was not set correctly"
      severity ERROR;

    sla_in_mosi.wrdata(0) <= '1';
    sla_in_mosi.wr <= '1';
    wait until rising_edge(clk);
    sla_in_mosi.wrdata(0) <= '0';
    sla_in_mosi.wr <= '0';
    for I in 0 to c_timeout_2us - 1 loop wait until rising_edge(clk); end loop;
    assert xonoff_reg(0) = '0'
      report "Timeout did not work"
      severity ERROR;

    sla_in_mosi.wrdata(0) <= '1';
    sla_in_mosi.wr <= '1';
    wait until rising_edge(clk);
    sla_in_mosi.wrdata(0) <= '0';
    sla_in_mosi.wr <= '0';
    for I in 0 to c_500ns_latency - 1 loop wait until rising_edge(clk); end loop;
    assert xonoff_reg(0) = '1'
      report "xonoff_reg was not set correctly after recovering from timeout"
      severity ERROR;
    tb_end <= '1';
    assert false
      report "Simulation tb_dp_xonoff_reg_timeout finished."
      severity NOTE;
    wait;
  end process;

  u_dut : entity work.dp_xonoff_reg_timeout
  generic map (
    g_mm_timeout  => 1,
    g_sim         => true
  )
  port map (
    mm_rst        => rst,
    mm_clk        => clk,
    st_rst        => rst,
    st_clk        => st_clk,

    sla_in        => sla_in_mosi,
    sla_out       => sla_out_miso,

    xonoff_reg    => xonoff_reg
  );
end tb;
