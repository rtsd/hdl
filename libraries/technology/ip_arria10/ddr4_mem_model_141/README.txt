README.txt for $HDL_WORK/libraries/technology/ip_arria10/ddr4_mem_model

1) DDR4 memory simulation model
2) Automated scripts and one time manual actions
3) Usage
3) Remarks


1) DDR4 memory simulation model

The DDR memory model is obtained from the example design that can be generated with Qsys when a DDR IP component is defined.
The first DDR4 component that was created is available via:

  $env(HDL_WORK)/libraries/technology/ip_arria10/ddr4_4g_1600/ip_arria10_ddr4_4g_1600.qsys

Unfortunately the example design needs to be created via the GUI by pressing the 'Example Design...' button, because the qsys-generate command
that is used in ddr4_4g_1600/generate_ip.sh to create the component does not have an option to also create the example design. After that the
ddr4_mem_model/generate_mem_model.sh needs to be ran to roll out the directories and files of the example design. 
The manual intervenion via the GUI is stands in the way of an automated flow. Therefore the files in the example design that describe the DDR
memory model have been copied to a fixed location in SVN. In this way it is no longer necessary to create the example design.

In the example design for ddr4_4g_1600 the generic core files of the DDR model are located at:

  $env(HDL_WORK)/libraries/technology/ip_arria10/ddr4_4g_1600/emif_0_example_design/sim/altera_emif_mem_model_core_ddr4_141

The size specific entity of the DDR model is created in:

  $env(HDL_WORK)/libraries/technology/ip_arria10/ddr4_4g_1600/emif_0_example_design/sim/altera_emif_mem_model_141/ed_sim_altera_emif_mem_model_141_z3tvrmq.vhd

The generic core files of the DDR memory model are the same for every DDR size. These files only depend on the Quartus tool version as indicated by 141
(Quartus 14.1) in their name. Therefore the generic core files have been copied to:

  $env(HDL_WORK)/libraries/technology/ip_arria10/ddr4_mem_model_141/sim/ddr4_core

The size specific DDR component file is copied to:

  $env(HDL_WORK)/libraries/technology/ip_arria10/ddr4_mem_model_141/sim/ddr4_4g_1600/ed_sim_altera_emif_mem_model_141_z3tvrmq.vhd
  
and other size DDR component files can be store there as well.

The ddr4_mem_model_141/sim is kept in SVN. Hence for each new size of DDR memory the example design needs to be created only once. The size specific
entity of that example design is then copied and kept at ddr4_mem_model_141/sim in SVN. The generic core files of the DDR model are already in SVN,
but it is good to manually check with Linux 'diff' as in diff_mem_model.sh that they are indeed the same as for a newly generated example design.

  ddr4_mem_model_141/sim/ddr4_core         # DDR core model files
  ddr4_mem_model_141/sim/ddr4_4g_1600      # specific DDR4 size model file
  ddr4_mem_model_141/sim/ddr4_8g_2400      # specific DDR4 size model file
  

2) Automated scripts and one time manual actions

Relative to $HDL_WORK/libraries/technology/ip_arria10/:

- ddr4_4g_1600/ip_arria10_ddr4_4g_1600.qsys : Qsys definition file for size and speed specific DDR4 controller
- ddr4_4g_1600/generate_ip.sh               : Use qsys-generate to create the size and speed specific DDR4 controller

- GUI --> open ip_arria10_ddr4_4g_1600.qsys --> press the 'Example Design...' button --> run ddr4_mem_model/generate_mem_model.sh:
  . ddr4_4g_1600/emif_0_example_design/sim/altera_emif_mem_model_core_ddr4_141/*
  . ddr4_4g_1600/emif_0_example_design/sim/altera_emif_mem_model_141/ed_sim_altera_emif_mem_model_141_z3tvrmq.vhd
  
- Copied these DDR memory model files to ddr4_mem_model_141/sim and commited them to SVN

- ddr4_mem_model/compile_ip.tcl             : Modelsim compile HDL script

The ddr4_mem_model/compile_ip.tcl script compiles the HDL files of the DDR4 memory model in the ddr4_mem_model Modelsim project. Therefore
the ddr4_mem_model/compile_ip.tcl is listed at the modelsim_compile_ip_files key in the ddr4_mem_model/hdllib.cfg

The DDR model ed_sim_altera_emif_mem_model_core_ddr4_141.vhd file assumes that the generic core files are from a VHDL library called
ed_sim_altera_emif_mem_model_core_ddr4_141. Therefore this name is used as hdl_library_clause_name in the ddr4_mem_model/hdllib.cfg.

Using a dedicated ddr4_mem_model_<version> directory for the DDR memory model is necessary because then it can have a hdllib.cfg and a
hdl_library_clause_name. Furthermore it is necessary to have a directrory per version, because each version will have another VHDL library
name and per hdllib.cfg there can only be one hdl_library_clause_name. 


3) Usage

a) Existing DDR4 memory models
   Since the DDR4 memory models are in SVN they do not need tobe generated.
   
b) New DDR4 memory models

   The following procedure shows how the models in SVN can be recreated or how a new device specific DDR4 model can be created.
   With the DDR4 memory models for the core files and the specific devices ddr4_4g_1600 and ddr4_8g_2400 the following yield no diff.

     > cd ddr4_4g_1600/
     > ./generate_ip.sh 
     > run_quartus unb2 &  # manually open ip_arria10_ddr4_4g_1600.qsys and press the 'Example Design...' button 
     >
     > cd ../ddr4_8g_2400/
     > ./generate_ip.sh 
     > run_quartus unb2 &  # manually open ip_arria10_ddr4_84g_2400.qsys and press the 'Example Design...' button 
     > 
     > cd ../ddr4_mem_model_141/
     > ./generate_mem_model.sh ../ddr4_4g_1600/emif_0_example_design
     > ./generate_mem_model.sh ../ddr4_8g_2400/ddr4_inst_example_design



4) Remarks

a) If the example design for a specific size DDR could have been created automatically, then probably it would have been preferred to not keep the
   DDR4 memory model files in SVN.
   
b) If a new version of Quartus is used then it may be necessary to recreate all the example design model files in a new directory
   ddr4_mem_model_<version>. However the DDR memory model is plain HDL so strictly speaking it does not depend FPGA device or
   Quartus version, therefore it should be possible to keep on using the ddr4_mem_model_141. Still a newer version of Quartus
   may provide a better and faster model.

c) The Quartus 14.1 DDR4 IP and memory model simulate much slower than the DDR3 IP and memory model of Quartus 11.1 that are used for UniBoard1.
   In simulation it is the memory storage and the access rate that are important, DDR3 and DDR4 are quite similar from that point of view.
   A work around to speed up simulation in applications could therefore be to use the io_ddr with DDR3 IP to model DDR4. For the DDR4 controller
   test bench it is of course still necessary to use the DDR4 IP and DDR4 memory model, because that test bench verifies the DDR4 functionality
   in simulation and shows that it will also work on hardware.
