-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- Verify multiple variations of tb_dp_block_validate_length
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_block_validate_length is
end tb_tb_dp_block_validate_length;

architecture tb of tb_tb_dp_block_validate_length is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  constant c_blk_per_sync : natural := 5;
  constant c_data_per_blk : natural := 9;
  constant c_exp_length   : natural := 9;
  constant c_err_bi       : natural := 3;
begin
--    g_nof_blocks_per_sync  : NATURAL := 5;
--    g_nof_data_per_blk     : NATURAL := 9;
--    g_expected_length      : NATURAL := 3;
--    g_err_bi               : NATURAL := 3

  u_equal   : entity work.tb_dp_block_validate_length generic map(c_blk_per_sync, c_data_per_blk, c_exp_length,     c_err_bi);  -- g_expected_length = g_nof_data_per_blk
  u_smaller : entity work.tb_dp_block_validate_length generic map(c_blk_per_sync, c_data_per_blk, c_exp_length - 3, c_err_bi);  -- g_expected_length < g_nof_data_per_blk
  u_larger  : entity work.tb_dp_block_validate_length generic map(c_blk_per_sync, c_data_per_blk, c_exp_length + 3, c_err_bi);  -- g_expected_length > g_nof_data_per_blk
end tb;
