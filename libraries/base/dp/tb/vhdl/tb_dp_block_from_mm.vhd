-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker
-- Purpose:
-- . Test bench for dp_block_from_mm.vhd and dp_block_to_mm.vhd
--
-- Description:
-- . data flow:
--   p_init_ram --> u_ram_rd --> u_dp_block_from_mm --> dp_block_to_mm --> u_ram_wr --> p_verify_read
-- . process flow:
--   p_init_ram --> p_transfer --> p_verify_read / p_verify_check.
--   . p_init_ram, initializes u_ram_rd with ascending values.
--   . p_transfer, u_dp_block_from_mm reads data (using g_data_size and g_step size)
--     from u_ram_rd and stream it to u_dp_block_to_mm which write it to u_ram_wr.
--   . p_verify_read, set ram address (using g_data_size and g_step_size) to check and
--     read value from ram, also set expected ram value.
--   . p_verify_check, check if ram_value is equal to expected value.
--
-- Remark:
-- . g_user_size and g_reverse_word_order are verified in tb_sdp_statistics_offload.vhd
-- --------------------------------------------------------------------------

-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_block_from_mm is
  generic (
    g_data_size : natural := 2;
    g_step_size : natural := 4;
    g_nof_data  : natural := 512
  );
end tb_dp_block_from_mm;

architecture tb of tb_dp_block_from_mm is
  constant c_nof_blocks    : natural := g_step_size / g_data_size;
  constant c_ram_data_size : natural := g_nof_data * g_data_size * c_nof_blocks;
  constant c_ram_adr_w     : natural := ceil_log2(c_ram_data_size);

  constant c_ram           : t_c_mem := (1, c_ram_adr_w, c_word_w, 2**c_ram_adr_w, '0');

  constant c_init          : natural := 42;  -- inital data counter value, should be > 0 for better test coverage.

  signal tb_end            : std_logic := '0';
  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';

  signal start_pulse       : std_logic := '0';
  signal start_address     : natural := 0;
  signal start_address_dly : natural := 0;
  signal block_done        : std_logic;

  signal rd_mosi           : t_mem_mosi;
  signal rd_miso           : t_mem_miso := c_mem_miso_rst;

  signal blk_sosi          : t_dp_sosi;
  signal blk_siso          : t_dp_siso := c_dp_siso_rdy;

  signal wr_mosi           : t_mem_mosi := c_mem_mosi_rst;

  -- needed for init and verify
  signal ram_wr_en         : std_logic := '0';
  signal ram_wr_adr        : std_logic_vector(c_ram.adr_w - 1 downto 0) := (others => '0');
  signal ram_wr_dat        : std_logic_vector(c_ram.dat_w - 1 downto 0) := (others => '0');
  signal ram_rd_en         : std_logic := '0';
  signal ram_rd_adr        : std_logic_vector(c_ram.adr_w - 1 downto 0);
  signal ram_rd_dat        : std_logic_vector(c_ram.dat_w - 1 downto 0);
  signal ram_rd_val        : std_logic;
  signal ram_prev_rd_val   : std_logic;

  signal rd_nxt_data       : natural := 0;
  signal rd_data           : natural := 0;

  signal stop_address      : natural := 0;

  signal init_done         : std_logic := '0';
  signal transfer_done     : std_logic := '0';
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- STIMULI
  ------------------------------------------------------------------------------

  start_address_dly <= start_address when rising_edge(clk);  -- dp_block_to_mm is 1 clock behind so set address also 1 clock later.

  p_init_ram : process
  begin
    ram_wr_en <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);
    for i in 0 to c_ram_data_size - 1 loop
      ram_wr_adr <= TO_UVEC(         i, c_ram.adr_w);
      ram_wr_dat <= TO_UVEC(c_init + i, c_ram.dat_w);
      ram_wr_en  <= '1';
      proc_common_wait_some_cycles(clk, 1);
    end loop;
    ram_wr_en <= '0';

    init_done <= '1';
    wait;
  end process;

  p_transfer : process
  begin
    start_pulse   <= '0';
    start_address <= 0;
    proc_common_wait_until_high(clk, init_done);
    for i in 0 to c_nof_blocks - 1 loop
      start_address <= i * g_data_size;

      start_pulse <= '1';
      proc_common_wait_some_cycles(clk, 1);
      start_pulse <= '0';

      stop_address  <= start_address + (g_nof_data - 1) * g_step_size + g_data_size - 1;
      proc_common_wait_until_high(clk, block_done);
    end loop;
    proc_common_wait_some_cycles(clk, 1);  -- Needed for dp_block_to_mm to proccess last word.
    transfer_done <= '1';
    wait;
  end process;

  p_verify_transfer : process
  begin
    proc_common_wait_until_high(clk, init_done);
    while tb_end = '0' loop
      wait until rising_edge(clk);
      if block_done = '1' then
        assert stop_address = TO_UINT(wr_mosi.wrdata(c_ram.dat_w - 1 downto 0)) - c_init
          report "wrong data at mm_done signal, must be same as stop_address + c_init"
          severity ERROR;
      end if;
    end loop;
    wait;
  end process;

  p_read_ram : process
  begin
    ram_rd_en  <= '0';
    ram_rd_adr <= TO_UVEC(0,  c_ram.adr_w);
    proc_common_wait_until_high(clk, transfer_done);
    ram_rd_en  <= '1';
    for i in 0 to c_ram_data_size - 1 loop
      ram_rd_adr <= TO_UVEC(i, c_ram.adr_w);
      proc_common_wait_some_cycles(clk, 1);
    end loop;
    ram_rd_en <= '0';

    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  ram_prev_rd_val <= ram_rd_val when rising_edge(clk);

  rd_data <= TO_UINT(ram_rd_dat);
  p_verify_read_ram_data: process
  begin
    rd_nxt_data  <= c_init;
    proc_common_wait_until_high(clk, transfer_done);
    while tb_end = '0' loop
      wait until rising_edge(clk);
      if ram_rd_val = '1' then
        assert rd_data = rd_nxt_data
          report "wrong order of RAM values"
          severity ERROR;
        assert rd_data - c_init <= stop_address
          report "wrong RAM values, greater than block size"
          severity ERROR;
        rd_nxt_data <= rd_nxt_data + 1;
      end if;
      if ram_rd_val = '0' and ram_prev_rd_val = '1' then  -- If ram_rd_val goes from hi tot lo.
        assert rd_data - c_init = stop_address
          report "wrong last RAM values, not same as block size"
          severity ERROR;
      end if;
    end loop;
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUT, dp_block_from_mm and dp_block_to_mm
  ------------------------------------------------------------------------------
  -- RAM with test data
  u_ram_rd: entity common_lib.common_ram_r_w
  generic map (
    g_ram => c_ram
  )
  port map (
    rst    => rst,
    clk    => clk,
    wr_en  => ram_wr_en,
    wr_adr => ram_wr_adr,
    wr_dat => ram_wr_dat,
    rd_en  => rd_mosi.rd,
    rd_adr => rd_mosi.address(c_ram.adr_w - 1 downto 0),
    rd_dat => rd_miso.rddata(c_ram.dat_w - 1 downto 0),
    rd_val => rd_miso.rdval
  );

  -- DUT, dp_block_from_mm
  u_dp_block_from_mm: entity work.dp_block_from_mm
  generic map (
    g_user_size => g_data_size,
    g_data_size => g_data_size,
    g_step_size => g_step_size,
    g_nof_data  => g_nof_data
  )
  port map (
    rst           => rst,
    clk           => clk,
    start_pulse   => start_pulse,
    start_address => start_address,
    mm_done       => block_done,
    mm_mosi       => rd_mosi,
    mm_miso       => rd_miso,
    out_sosi      => blk_sosi,
    out_siso      => blk_siso
  );

  -- DUT, dp_block_to_mm
  u_dp_block_to_mm: entity work.dp_block_to_mm
  generic map (
    g_data_size => g_data_size,
    g_step_size => g_step_size,
    g_nof_data  => g_nof_data
  )
  port map (
    rst           => rst,
    clk           => clk,
    start_address => start_address_dly,
    mm_mosi       => wr_mosi,
    in_sosi       => blk_sosi
  );

  -- RAM with transferred data
  u_ram_wr: entity common_lib.common_ram_r_w
  generic map (
    g_ram => c_ram
  )
  port map (
    rst    => rst,
    clk    => clk,
    wr_en  => wr_mosi.wr,
    wr_adr => wr_mosi.address(c_ram.adr_w - 1 downto 0),
    wr_dat => wr_mosi.wrdata(c_ram.dat_w - 1 downto 0),
    rd_en  => ram_rd_en,
    rd_adr => ram_rd_adr,
    rd_dat => ram_rd_dat,
    rd_val => ram_rd_val
  );
end tb;
