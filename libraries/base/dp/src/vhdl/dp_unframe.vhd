--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_packetizing_pkg.all;

-- Reuse from LOFAR rad_unframe.vhd and rad_unframe(rtl).vhd

-- Purpose:
--   Extract the payload data from a frame:
--     -----------------------
--     | fsn | dat ... | brc |
--     -----------------------
--       sof             eof
--       val   val ...   val
--   and output the frame fsn and brc information as signals:
--     . dat[]
--     . fsn[]
--     . sync
--     . val
--     . sof
--     . eof
--     . sof_sync
--
-- Description:
--   The dp_unframe is the reverse of dp_frame. The output sof and eof mark the
--   start and end of the output dat. The output fsn signal is valid at the
--   output sof and remains valid. The output sync is a pulse and is valid one
--   cycle before output sof. The output err signal reflects the brc value and
--   is valid at the output eof and set to 0 at the next input sof.

entity dp_unframe is
  generic (
    g_fsn_w          : positive := c_dp_max_w;
    g_dat_w          : positive := c_dp_max_w
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    in_dat           : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val           : in  std_logic;
    in_sof           : in  std_logic;
    in_eof           : in  std_logic;

    out_fsn          : out std_logic_vector(g_fsn_w - 1 downto 0);
    out_sync         : out std_logic;  -- LOFAR style sync before sof
    sof_sync         : out std_logic;  -- DP style sync at sof
    out_dat          : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val          : out std_logic;
    out_sof          : out std_logic;
    out_eof          : out std_logic;
    out_err          : out std_logic
  );
begin
-- synthesis translate_off
  assert g_fsn_w <= g_dat_w
    report "g_fsn_w must be smaller than or equal to g_dat_w"
    severity ERROR;
-- synthesis translate_on
end dp_unframe;

architecture rtl of dp_unframe is
  -- Use unregistered input to save logic, use registered input to ease achieving timing constrains.
  constant c_input_reg   : boolean := false;

  constant c_brc_ok      : std_logic_vector(in_dat'range) := std_logic_vector(to_unsigned(c_dp_brc_ok, in_dat'length));
  constant c_brc_err     : std_logic_vector(in_dat'range) := std_logic_vector(to_unsigned(c_dp_brc_err, in_dat'length));

  -- Registered inputs (see c_input_reg)
  signal in_dat_reg      : std_logic_vector(in_dat'range);
  signal in_val_reg      : std_logic;
  signal in_sof_reg      : std_logic;
  signal in_eof_reg      : std_logic;

  -- Hold input sof and then adjust it to next input val to put out_sof on first valid out_dat word (in_sof is on fsn)
  signal sof_hld         : std_logic;
  signal nxt_sof_hld     : std_logic;
  signal sync_hld        : std_logic;  -- hold input sync from FSN to adjust to output sof as sof_sync
  signal nxt_sync_hld    : std_logic;

  signal i_out_fsn       : std_logic_vector(out_fsn'range);
  signal nxt_out_fsn     : std_logic_vector(out_fsn'range);
  signal nxt_out_sync    : std_logic;
  signal i_out_err       : std_logic;
  signal nxt_out_err     : std_logic;

  -- Delay output dat to get out_eof on last valid out_dat word (in_eof is on brc)
  signal dat_adj         : std_logic_vector(out_dat'range);
  signal nxt_dat_adj     : std_logic_vector(dat_adj'range);
  signal val_adj         : std_logic;
  signal nxt_val_adj     : std_logic;
  signal sof_adj         : std_logic;
  signal nxt_sof_adj     : std_logic;
  signal sync_adj        : std_logic;
  signal nxt_sync_adj    : std_logic;

  signal i_out_dat       : std_logic_vector(out_dat'range);
  signal nxt_out_dat     : std_logic_vector(out_dat'range);
  signal nxt_out_val     : std_logic;
  signal nxt_out_sof     : std_logic;
  signal nxt_out_eof     : std_logic;
  signal nxt_sof_sync    : std_logic;
begin
  out_fsn <= i_out_fsn;
  out_err <= i_out_err;
  out_dat <= i_out_dat;

  gen_input_reg : if c_input_reg = true generate
    p_reg : process (clk, rst)
    begin
      if rst = '1' then
        in_dat_reg  <= (others => '0');
        in_val_reg  <= '0';
        in_sof_reg  <= '0';
        in_eof_reg  <= '0';
      elsif rising_edge(clk) then
        in_dat_reg  <= in_dat;
        in_val_reg  <= in_val;
        in_sof_reg  <= in_sof;
        in_eof_reg  <= in_eof;
      end if;
    end process;
  end generate;

  no_input_reg : if c_input_reg = false generate
    in_dat_reg  <= in_dat;
    in_val_reg  <= in_val;
    in_sof_reg  <= in_sof;
    in_eof_reg  <= in_eof;
  end generate;

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      sof_hld     <= '0';
      sync_hld    <= '0';
      i_out_fsn   <= (others => '0');
      out_sync    <= '0';
      i_out_err   <= '0';
      dat_adj     <= (others => '0');
      val_adj     <= '0';
      sof_adj     <= '0';
      sync_adj    <= '0';
      sof_sync    <= '0';
      i_out_dat   <= (others => '0');
      out_val     <= '0';
      out_sof     <= '0';
      out_eof     <= '0';
    elsif rising_edge(clk) then
      sof_hld     <= nxt_sof_hld;
      sync_hld    <= nxt_sync_hld;
      i_out_fsn   <= nxt_out_fsn;
      out_sync    <= nxt_out_sync;
      i_out_err   <= nxt_out_err;
      dat_adj     <= nxt_dat_adj;
      val_adj     <= nxt_val_adj;
      sof_adj     <= nxt_sof_adj;
      sync_adj    <= nxt_sync_adj;
      sof_sync    <= nxt_sof_sync;
      i_out_dat   <= nxt_out_dat;
      out_val     <= nxt_out_val;
      out_sof     <= nxt_out_sof;
      out_eof     <= nxt_out_eof;
    end if;
  end process;

  p_output: process(in_dat_reg, in_val_reg, sync_hld, sof_hld, in_sof_reg, in_eof_reg, dat_adj, val_adj, sof_adj, sync_adj, i_out_dat, i_out_fsn, i_out_err)
  begin
    nxt_sof_hld  <= sof_hld;  -- hold input sof to first input dat
    nxt_sync_hld <= sync_hld;  -- hold input sync to first input dat
    nxt_out_fsn  <= i_out_fsn;  -- hold out_fsn during a frame
    nxt_out_sync <= '0';
    nxt_out_err  <= i_out_err;  -- hold out_err until next sof

    nxt_dat_adj  <= dat_adj;  -- delay out_dat, val, sof to meet out_eof
    nxt_val_adj  <= val_adj;
    nxt_sof_adj  <= sof_adj;
    nxt_sync_adj <= sync_adj;

    nxt_out_dat  <= i_out_dat;
    nxt_out_val  <= '0';
    nxt_out_sof  <= '0';
    nxt_out_eof  <= '0';
    nxt_sof_sync <= '0';

    if in_val_reg = '1' then
      nxt_sof_hld <= '0';
      nxt_sync_hld <= '0';
      nxt_dat_adj <= in_dat_reg;
      nxt_val_adj <= '0';
      nxt_sof_adj <= '0';
      nxt_sync_adj <= '0';
      nxt_out_dat <= dat_adj;  -- dat output
      nxt_out_val <= val_adj;  -- val output
      nxt_out_sof <= sof_adj;  -- sof output
      nxt_sof_sync <= sync_adj;  -- sync output
      if in_sof_reg = '1' then
        nxt_sof_hld  <= '1';
        nxt_sync_hld <= in_dat_reg(out_fsn'high);  -- sync pulse
        nxt_out_fsn  <= in_dat_reg(out_fsn'range);  -- new fsn
        nxt_out_sync <= in_dat_reg(out_fsn'high);  -- sync pulse
        nxt_out_err  <= '0';  -- clear previous err
      elsif sof_hld = '1' then
        nxt_val_adj <= '1';  -- val adjust
        nxt_sof_adj <= '1';  -- sof adjust
        nxt_sync_adj <= sync_hld;  -- sync adjust
      elsif in_eof_reg = '1' then  -- eof output
        nxt_out_eof <= '1';
        nxt_out_err <= '0';  -- set err based on brc, already valid at out_eof
        if in_dat_reg /= c_brc_ok then
          nxt_out_err <= '1';
        end if;
      else
        nxt_val_adj <= '1';  -- val adjust
      end if;
    end if;
  end process;
end rtl;
