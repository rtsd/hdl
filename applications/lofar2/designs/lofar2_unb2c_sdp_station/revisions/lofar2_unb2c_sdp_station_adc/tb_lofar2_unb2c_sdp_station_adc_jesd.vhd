-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra, R. van der Walle
-- Purpose: Self-checking testbench for simulating restart of JESD interface
--          rx_clk domain input section in lofar2_unb2c_sdp_station_adc
--
-- Description:
-- * Run JESD204B interface
--   FOR I IN 0 TO c_nof_restarts LOOP
--     . Run WG and ADUH monitor similar as in tb_lofar2_unb2c_sdp_station_adc
--     . Reset the AIT rx_clk domain input section via PIO_JESD_CTRL
--     . Reinit signal input links via JESD204B
--   END LOOP
-- * Uses c_si_dis to view signal input disable via JESD_CTRL.
--
-- Conclusion:
-- * JESD_CTRL reset does reset the JESD204B IP and also the rx_clk and rx_rst,
--   so it is necessary to first stop the BSN source to avoid that corrupted
--   data blocks will enter the dp_clk domain, due to rx_rst of the rx_clk/
--   dp_clk domain FIFO.
-- * JESD204B link reinit does restart the signal input link. For link reinit
--   it is not necessary to stop the BSN source, because then the rx_clk
--   remains running and the rx_rst remains inactive.
-- It is not clear whether both reset and reinit are needed to recover a link,
-- (e.g. after RCU2 off/on) or whether one of them is enough.
--
-- Usage:
--   > as 7    # default
--   > as 16   # for detailed debugging of JESD204B IP
--   > run -a
--
-- View u_sdp_station/ait_sosi_arr, to see that only complete blocks are
-- passed on. If necessary also manually add missing other arrays like
-- u_sdp_station/u_ait/gen_rx_ait/u_rx_ait/st_sosi_arr
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2c_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, lofar2_unb2c_sdp_station_lib, tech_jesd204b_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use tech_jesd204b_lib.tech_jesd204b_pkg.all;

entity tb_lofar2_unb2c_sdp_station_adc_jesd is
end tb_lofar2_unb2c_sdp_station_adc_jesd;

architecture tb of tb_lofar2_unb2c_sdp_station_adc_jesd is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);
  constant c_init_bsn        : natural := 17;  -- some recognizable value >= 0
  constant c_si_dis          : natural := 2;  -- disable signal input 2 in range 0:c_sdp_S_pn_1

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;

  constant c_tb_clk_period       : time := 10 ns;  -- 100 ps; -- use fast tb_clk to speed up M&C

  constant c_nof_restarts        : natural := 1;
  constant c_nof_block_per_sync  : natural := 7;  -- use short interval to speed up simulation
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_pps_period          : time := c_nof_clk_per_sync * c_ext_clk_period;

  constant c_percentage          : real := 0.05;  -- percentage that actual value may differ from expected value, due to WG rounding
  constant c_lo_factor           : real := 1.0 - c_percentage;  -- lower boundary
  constant c_hi_factor           : real := 1.0 + c_percentage;  -- higher boundary

  -- Tx JESD
  constant c_nof_jesd204b_tx      : natural := 3;  -- <= c_sdp_S_pn = 12

  -- WG
  constant c_bsn_start_wg         : natural := c_init_bsn + 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  constant c_ampl_sp_0            : natural := c_sdp_FS_adc / 2;  -- = 0.5 * FS, so in number of lsb
  constant c_wg_freq_offset       : real := 0.0 / 11.0;  -- in freq_unit
  constant c_subband_sp_0         : real := 102.0;  -- Select subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
  constant c_exp_wg_power_sp_0    : real := real(c_ampl_sp_0**2) / 2.0 * real(c_nof_clk_per_sync);

  -- ADUH
  constant c_mon_buffer_nof_samples : natural := 512;  -- samples per stream

  -- MM
  constant c_mm_file_reg_ppsh             : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "PIO_PPS";
  constant c_mm_file_reg_bsn_source_v2    : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_bsn_scheduler_wg : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_reg_aduh_mon         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_ADUH_MONITOR";
  constant c_mm_file_reg_dp_shiftram      : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DP_SHIFTRAM";
  constant c_mm_file_jesd204b             : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "JESD204B";
  constant c_mm_file_pio_jesd_ctrl        : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "PIO_JESD_CTRL";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0);

  signal pps_rst             : std_logic := '1';
  signal gen_pps             : std_logic := '0';

  -- Input delay
  signal rd_input_delay      : natural;

  -- WG
  signal dbg_c_exp_wg_power_sp_0 : real := c_exp_wg_power_sp_0;
  signal sp_samples              : t_integer_arr(0 to c_mon_buffer_nof_samples - 1) := (others => 0);
  signal sp_sample               : integer := 0;
  signal sp_power_sum            : std_logic_vector(63 downto 0);
  signal current_bsn_wg          : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal ext_pps             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_txp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal JESD204B_SYSREF     : std_logic;
  signal JESD204B_SYNC_N     : std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);

  -- Tx jesd204b source
  constant c_tx_avs_clk_period       : time := 20 ns;

  signal mm_rst                      : std_logic := '1';
  signal tx_avs_clk                  : std_logic := '0';
  signal tx_avs_rst_n                : std_logic := '0';

  signal txlink_clk                  : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal dev_sync_n                  : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);

  signal tx_avs_chipselect           : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0) := (others => '0');
  signal tx_avs_address              : t_slv_8_arr(c_nof_jesd204b_tx - 1 downto 0) := (others => (others => '0'));
  signal tx_avs_read                 : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0) := (others => '0');
  signal tx_avs_readdata             : t_slv_32_arr(c_nof_jesd204b_tx - 1 downto 0);

  signal txphy_clk                   : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal txlink_rst_n                : std_logic;
  signal tx_analogreset              : std_logic_vector(0 downto 0);
  signal tx_digitalreset             : std_logic_vector(0 downto 0);
  signal tx_bonding_clocks           : std_logic_vector(5 downto 0) := (others => '0');
  signal bonding_clock_0             : std_logic := '0';
  signal bonding_clock_1             : std_logic := '0';
  signal bonding_clock_2             : std_logic := '0';
  signal bonding_clock_3             : std_logic := '0';
  signal bonding_clock_4             : std_logic := '0';
  signal bonding_clock_5             : std_logic := '0';
  signal pll_locked                  : std_logic_vector(0 downto 0);

  signal jesd204b_tx_link_data_arr   : t_slv_32_arr(c_nof_jesd204b_tx - 1 downto 0);
  signal jesd204b_tx_link_valid      : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal jesd204b_tx_link_ready      : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal jesd204b_tx_frame_ready     : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);

  signal jesd204b_sync_adc_n         : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);

  -- Rx jesd204b IP registers
  signal reg_jesd204b_rx_err_enable        : std_logic_vector(c_word_w - 1 downto 0);
  signal reg_jesd204b_rx_err_link_reinit   : std_logic_vector(c_word_w - 1 downto 0);
  signal reg_jesd204b_rx_syncn_sysref_ctrl : std_logic_vector(c_word_w - 1 downto 0);

  signal reg_jesd204b_rx_err0        : std_logic_vector(tech_jesd204b_field_rx_err0_w - 1 downto 0);
  signal reg_jesd204b_rx_err1        : std_logic_vector(tech_jesd204b_field_rx_err1_w - 1 downto 0);
  signal reg_jesd204b_csr_rbd_count  : std_logic_vector(tech_jesd204b_field_csr_rbd_count_w - 1 downto 0);
  signal reg_jesd204b_csr_dev_syncn  : std_logic_vector(tech_jesd204b_field_csr_dev_syncn_w - 1 downto 0);

  -- Rx jesd204b ctrl register
  signal pio_jesd_ctrl               : std_logic_vector(c_word_w - 1 downto 0);
  signal pio_jesd_ctrl_enable        : std_logic_vector(c_sdp_jesd_ctrl_enable_w - 1 downto 0);
  signal pio_jesd_ctrl_reset         : std_logic;

  -- Debug signals to track progress of p_stimuli in Wave Window
  signal dbg_restart                 : natural := 0;
  signal dbg_bsn_source_en           : std_logic := '0';
  signal dbg_jesd_ctrl_reset_ignore  : std_logic := '0';
  signal dbg_jesd_ctrl_reset         : std_logic := '0';
  signal dbg_read_jesd204b           : std_logic := '0';
  signal dbg_link_reinit             : std_logic := '0';

  -- Read JESD204B IP status per signal input c_si
  procedure proc_read_jesd204b(c_si                        : in natural;
                               signal rd_clk               : in std_logic;
                               signal rd_data              : inout std_logic_vector(c_word_w - 1 downto 0);
                               signal dbg_read             : out std_logic;
                               signal rx_err_enable        : out std_logic_vector(c_word_w - 1 downto 0);
                               signal rx_err_link_reinit   : out std_logic_vector(c_word_w - 1 downto 0);
                               signal rx_syncn_sysref_ctrl : out std_logic_vector(c_word_w - 1 downto 0);
                               signal rx_err0              : out std_logic_vector(tech_jesd204b_field_rx_err0_w - 1 downto 0);
                               signal rx_err1              : out std_logic_vector(tech_jesd204b_field_rx_err1_w - 1 downto 0);
                               signal csr_rbd_count        : out std_logic_vector(tech_jesd204b_field_csr_rbd_count_w - 1 downto 0);
                               signal csr_dev_syncn        : out std_logic_vector(tech_jesd204b_field_csr_dev_syncn_w - 1 downto 0)) is
    constant c_offset : natural :=  c_si * tech_jesd204b_port_span;
  begin
    dbg_read <= '1';
    mmf_mm_bus_rd(c_mm_file_jesd204b, 2, c_offset + tech_jesd204b_field_rx_err_enable_adr, rd_data, rd_clk);
    rx_err_enable <= rd_data;
    mmf_mm_bus_rd(c_mm_file_jesd204b, c_offset + tech_jesd204b_field_rx_err_link_reinit_adr, rd_data, rd_clk);
    rx_err_link_reinit <= rd_data;
    mmf_mm_bus_rd(c_mm_file_jesd204b, c_offset + tech_jesd204b_field_rx_syncn_sysref_ctrl_adr, rd_data, rd_clk);
    rx_syncn_sysref_ctrl <= rd_data;
    mmf_mm_bus_rd(c_mm_file_jesd204b, c_offset + tech_jesd204b_field_rx_err0_adr, rd_data, rd_clk);
    rx_err0 <= rd_data(tech_jesd204b_field_rx_err0_hi downto tech_jesd204b_field_rx_err0_lo);
    mmf_mm_bus_rd(c_mm_file_jesd204b, c_offset + tech_jesd204b_field_rx_err1_adr, rd_data, rd_clk);
    rx_err1 <= rd_data(tech_jesd204b_field_rx_err1_hi downto tech_jesd204b_field_rx_err1_lo);
    mmf_mm_bus_rd(c_mm_file_jesd204b, c_offset + tech_jesd204b_field_csr_rbd_count_adr, rd_data, rd_clk);
    csr_rbd_count <= rd_data(tech_jesd204b_field_csr_rbd_count_hi downto tech_jesd204b_field_csr_rbd_count_lo);
    mmf_mm_bus_rd(c_mm_file_jesd204b, c_offset + tech_jesd204b_field_csr_dev_syncn_adr, rd_data, rd_clk);
    csr_dev_syncn <= rd_data(tech_jesd204b_field_csr_dev_syncn_hi downto tech_jesd204b_field_csr_dev_syncn_lo);
    dbg_read <= '0';
  end;

  procedure proc_read_jesd204b_arr(signal rd_clk               : in std_logic;
                                   signal rd_data              : inout std_logic_vector(c_word_w - 1 downto 0);
                                   signal dbg_read             : out std_logic;
                                   signal rx_err_enable        : out std_logic_vector(c_word_w - 1 downto 0);
                                   signal rx_err_link_reinit   : out std_logic_vector(c_word_w - 1 downto 0);
                                   signal rx_syncn_sysref_ctrl : out std_logic_vector(c_word_w - 1 downto 0);
                                   signal rx_err0              : out std_logic_vector(tech_jesd204b_field_rx_err0_w - 1 downto 0);
                                   signal rx_err1              : out std_logic_vector(tech_jesd204b_field_rx_err1_w - 1 downto 0);
                                   signal csr_rbd_count        : out std_logic_vector(tech_jesd204b_field_csr_rbd_count_w - 1 downto 0);
                                   signal csr_dev_syncn        : out std_logic_vector(tech_jesd204b_field_csr_dev_syncn_w - 1 downto 0)) is
  begin
    for I in 0 to c_sdp_S_pn - 1 loop
      proc_read_jesd204b(I, rd_clk, rd_data, dbg_read,
                         rx_err_enable,
                         rx_err_link_reinit,
                         rx_syncn_sysref_ctrl,
                         rx_err0,
                         rx_err1,
                         csr_rbd_count,
                         csr_dev_syncn);
    end loop;
  end;
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk(0) <= not eth_clk(0) after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(10, c_nof_clk_per_sync, '1', pps_rst, ext_clk, gen_pps);
  JESD204B_SYSREF <= gen_pps;
  ext_pps <= gen_pps;

  ------------------------------------------------------------------------------
  -- DUT with JESD204B Rx
  ------------------------------------------------------------------------------
  u_lofar_unb2c_sdp_station_adc : entity lofar2_unb2c_sdp_station_lib.lofar2_unb2c_sdp_station
  generic map (
    g_design_name            => "lofar2_unb2c_sdp_station_adc",
    g_design_note            => "",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr,
    g_bsn_nof_clk_per_sync   => c_nof_clk_per_sync,
    g_scope_selected_subband => natural(c_subband_sp_0)
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => ext_pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK      => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => JESD204B_SYSREF,
    JESD204B_SYNC_N => JESD204B_SYNC_N
  );

  -----------------------------------------------------------------------------
  -- Use a JESD204b Tx instance to model the ADCs
  -- . copied from tb_tech_jesd204b.vhd
  -----------------------------------------------------------------------------

  -- Clocks and resets
  tx_avs_clk <= not tx_avs_clk after c_tx_avs_clk_period / 2;

  mm_rst <= '1', '0' after 800 ns;
  tx_avs_rst_n <= '0', '1' after 23500 ns;
  tx_analogreset(0) <= '1', '0' after 18500 ns;
  tx_digitalreset(0) <= '1', '0' after 23000 ns;
  txlink_rst_n <= '0', '1' after 25500 ns;
  pll_locked(0) <= '0', '1' after 1000 ns;

  -- Create bonding clocks
  bonding_clock_5 <= not bonding_clock_5 after 250 ps;
  bonding_clock_4 <= not bonding_clock_4 after 250 ps;
  bonding_clock_3 <= not bonding_clock_3 after 500 ps;
  bonding_clock_2 <= not bonding_clock_2 after 500 ps;
  bonding_clock_0 <= not bonding_clock_0 after 2500 ps;

  p_bonding_clock_1 : process
  begin
    bonding_clock_1 <= '0';
    wait for 4000 ps;
    bonding_clock_1 <= '1';
    wait for 1000 ps;
  end process;

  tx_bonding_clocks(5) <= transport bonding_clock_5 after 4890 ps;
  tx_bonding_clocks(4) <= transport bonding_clock_4 after 4640 ps;
  tx_bonding_clocks(3) <= transport bonding_clock_3 after 4920 ps;
  tx_bonding_clocks(2) <= transport bonding_clock_2 after 4930 ps;
  tx_bonding_clocks(1) <= transport bonding_clock_1 after 7490 ps;
  tx_bonding_clocks(0) <= transport bonding_clock_0 after 4000 ps;

  gen_jesd204b_tx : for i in 0 to c_nof_jesd204b_tx - 1 generate
    -- Tb DAC
    u_tech_jesd204b_tx : entity tech_jesd204b_lib.tech_jesd204b_tx
    port map (
      csr_cf                     => OPEN,
      csr_cs                     => OPEN,
      csr_f                      => OPEN,
      csr_hd                     => OPEN,
      csr_k                      => OPEN,
      csr_l                      => OPEN,
      csr_lane_powerdown         => open,  -- out
      csr_m                      => OPEN,
      csr_n                      => OPEN,
      csr_np                     => OPEN,
      csr_tx_testmode            => OPEN,
      csr_tx_testpattern_a       => OPEN,
      csr_tx_testpattern_b       => OPEN,
      csr_tx_testpattern_c       => OPEN,
      csr_tx_testpattern_d       => OPEN,
      csr_s                      => OPEN,
      dev_sync_n                 => dev_sync_n(i),  -- out
      jesd204_tx_avs_chipselect  => tx_avs_chipselect(i),
      jesd204_tx_avs_address     => tx_avs_address(i),
      jesd204_tx_avs_read        => tx_avs_read(i),
      jesd204_tx_avs_readdata    => tx_avs_readdata(i),
      jesd204_tx_avs_waitrequest => open,
      jesd204_tx_avs_write       => '0',
      jesd204_tx_avs_writedata   => (others => '0'),
      jesd204_tx_avs_clk         => tx_avs_clk,
      jesd204_tx_avs_rst_n       => tx_avs_rst_n,
      jesd204_tx_dlb_data        => open,  -- debug/loopback testing
      jesd204_tx_dlb_kchar_data  => open,  -- debug/loopback testing
      jesd204_tx_frame_ready     => jesd204b_tx_frame_ready(i),
      jesd204_tx_frame_error     => '0',
      jesd204_tx_int             => OPEN,  -- Connected to status IO in example design
      jesd204_tx_link_data       => jesd204b_tx_link_data_arr(i),  -- in
      jesd204_tx_link_valid      => jesd204b_tx_link_valid(i),  -- in
      jesd204_tx_link_ready      => jesd204b_tx_link_ready(i),  -- out
      mdev_sync_n                => dev_sync_n(i),  -- in
      pll_locked                 => pll_locked,  -- in
      sync_n                     => jesd204b_sync_adc_n(i),  -- in
      tx_analogreset             => tx_analogreset,
      tx_bonding_clocks          => tx_bonding_clocks,  -- : in  std_logic_vector(5 downto 0)  := (others => 'X'); -- clk
      tx_cal_busy                => open,
      tx_digitalreset            => tx_digitalreset,
      tx_serial_data             => JESD204B_SERIAL_DATA(i downto i),
      txlink_clk                 => txlink_clk(i),
      txlink_rst_n_reset_n       => txlink_rst_n,
      txphy_clk                  => txphy_clk(i downto i),
      somf                       => OPEN,
      sysref                     => JESD204B_SYSREF
    );

    -- One JESD204B_SYNC_N per RCU2
    jesd204b_sync_adc_n(i) <= JESD204B_SYNC_N(i / c_sdp_S_rcu);

    -- Generate same test pattern for all ADC
    p_tx_data : process (JESD204B_REFCLK, mm_rst)
      variable v_data  : integer := 0;
      variable v_even_sample : boolean := true;
    begin
      if mm_rst = '1' then
         jesd204b_tx_link_data_arr(i) <= (others => '0');
         jesd204b_tx_link_valid(i) <= '0';
         txlink_clk(i) <= '0';
         v_data := 0;
         v_even_sample := true;
       elsif rising_edge(JESD204B_REFCLK) then
         txlink_clk(i) <= not txlink_clk(i);

         -- Incrementing data in c_sdp_W_adc_jesd = 16 bits
         -- . use range c_sdp_W_adc_jesd-1 to avoid simulation warnings:
         --   Warning: NUMERIC_STD.TO_SIGNED: vector truncated
         --   Time: 164635 ns  Iteration: 0  Region: /tb_lofar2_unb2c_sdp_station_adc_jesd/gen_jesd204b_tx(2)
         v_data := (v_data + 1) mod 2**(c_sdp_W_adc_jesd - 1);

         -- Frame the data to 32 bits at half the rate
         if jesd204b_tx_link_ready(i) = '0' then
           v_even_sample := true;
         else
           v_even_sample := not v_even_sample;
         end if;
         if v_even_sample = true then
           jesd204b_tx_link_data_arr(i)(c_sdp_W_adc_jesd - 1 downto 0) <= TO_SVEC(v_data, c_sdp_W_adc_jesd);
           jesd204b_tx_link_valid(i) <= '0';
         else
           jesd204b_tx_link_data_arr(i)(2 * c_sdp_W_adc_jesd - 1 downto c_sdp_W_adc_jesd) <= TO_SVEC(v_data, c_sdp_W_adc_jesd);
           jesd204b_tx_link_valid(i) <= '1';
         end if;
       end if;
    end process;

  end generate;

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_offset                : natural;
    variable v_word                  : std_logic_vector(c_word_w - 1 downto 0);
    variable v_int                   : integer;
    variable v_bsn                   : natural;
    variable v_sp_power_sum_0        : real;
    variable v_sp_subband_power      : real;
    variable v_W, v_T, v_U, v_S, v_B : natural;  -- array indicies
    variable v_exp_input_delay       : natural;
  begin
    dbg_restart <= 0;
    for REP in 0 to c_nof_restarts loop
      -- Wait for DUT power up after reset or after AIT rx_clk domain restart
      wait for 2 us;

      ----------------------------------------------------------------------------
      -- Set and readback input delay for si = 0
      ----------------------------------------------------------------------------
      v_exp_input_delay := 10 + REP;
      mmf_mm_bus_wr(c_mm_file_reg_dp_shiftram, 0, v_exp_input_delay, tb_clk);
      proc_common_wait_cross_clock_domain_latency(tb_clk, ext_clk);
      mmf_mm_bus_rd(c_mm_file_reg_dp_shiftram, 0, rd_data, tb_clk);
      rd_input_delay <= TO_UINT(rd_data);

      ----------------------------------------------------------------------------
      -- Enable BS
      ----------------------------------------------------------------------------
      mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 2,         c_init_bsn, tb_clk);  -- Init BSN
      mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 3,                  0, tb_clk);  -- Write high part activates the init BSN
      mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 1, c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
      mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0,       16#00000003#, tb_clk);  -- Enable BS at PPS

      -- Release PPS pulser, to get first PPS now and to start BSN source
      wait for 1 us;
      pps_rst <= '0';
      dbg_bsn_source_en <= '1';  -- marker in wave window

      ---------------------------------------------------------------------------
      -- Read JESD_CTRL register
      ---------------------------------------------------------------------------
      -- Note: pio_jesd_ctrl_enable works in fact as disable, so '0' enables
      --       signal input and '1' disables it, and read yields
      --       pio_jesd_ctrl = 0.
      mmf_mm_bus_rd(c_mm_file_pio_jesd_ctrl, 0, rd_data, tb_clk);
      pio_jesd_ctrl <= rd_data;
      pio_jesd_ctrl_enable <= rd_data(c_sdp_jesd_ctrl_enable_w - 1 downto 0);
      pio_jesd_ctrl_reset <= rd_data(c_sdp_jesd_ctrl_reset_bi);

      ----------------------------------------------------------------------------
      -- Enable WG
      ----------------------------------------------------------------------------
      --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
      --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
      --   1 : phase[15:0]
      --   2 : freq[30:0]
      --   3 : ampl[16:0]
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 1, integer(  0.0 * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 2, integer((c_subband_sp_0 + c_wg_freq_offset) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 3, integer(real(c_ampl_sp_0) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl

      -- Read current BSN
      mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
      mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
      proc_common_wait_some_cycles(tb_clk, 1);

      -- Write scheduler BSN to trigger start of WG at next block
      v_bsn := TO_UINT(current_bsn_wg) + 2;
      assert v_bsn <= c_bsn_start_wg
        report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
        severity ERROR;
      v_bsn := c_bsn_start_wg;
      mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, v_bsn, tb_clk);  -- first write low then high part
      mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,     0, tb_clk);  -- assume v_bsn < 2**31-1

      ----------------------------------------------------------------------------
      -- Wait for enough WG data and start of sync interval
      ----------------------------------------------------------------------------
      mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                              "UNSIGNED", rd_data, ">=", c_init_bsn + c_nof_block_per_sync * 2,  -- this is the wait until condition
                              c_sdp_T_sub, tb_clk);

      ----------------------------------------------------------------------------
      -- Read JESD204B IP status per signal input
      ----------------------------------------------------------------------------
      for I in 0 to c_sdp_S_pn - 1 loop
        proc_read_jesd204b(I, tb_clk, rd_data, dbg_read_jesd204b,
                           reg_jesd204b_rx_err_enable,
                           reg_jesd204b_rx_err_link_reinit,
                           reg_jesd204b_rx_syncn_sysref_ctrl,
                           reg_jesd204b_rx_err0,
                           reg_jesd204b_rx_err1,
                           reg_jesd204b_csr_rbd_count,
                           reg_jesd204b_csr_dev_syncn);
        proc_common_wait_some_cycles(tb_clk, 10);

        -----------------------------------------------------------------------
        -- Verify expected Rx JESD204B IP values for active signal inputs
        -----------------------------------------------------------------------
        if I < c_nof_jesd204b_tx then
          assert unsigned(reg_jesd204b_rx_err_enable) = tech_jesd204b_field_rx_err_enable_reset
            report "Wrong rx_err_enable_reset: " & integer'image(TO_SINT(reg_jesd204b_rx_err_enable)) & " /= " & integer'image(tech_jesd204b_field_rx_err_enable_reset)
            severity ERROR;
          assert unsigned(reg_jesd204b_rx_err_link_reinit) = tech_jesd204b_field_rx_err_link_reinit_reset
            report "Wrong rx_err_link_reinit_reset: " & integer'image(TO_SINT(reg_jesd204b_rx_err_link_reinit)) & " /= " & integer'image(tech_jesd204b_field_rx_err_link_reinit_reset)
            severity ERROR;
        end if;
      end loop;

      ---------------------------------------------------------------------------
      -- Read ADUH monitor power sum
      ---------------------------------------------------------------------------
      mmf_mm_bus_rd(c_mm_file_reg_aduh_mon, 2, rd_data, tb_clk);  -- read low part
      sp_power_sum(31 downto 0) <= rd_data;
      mmf_mm_bus_rd(c_mm_file_reg_aduh_mon, 3, rd_data, tb_clk);  -- read high part
      sp_power_sum(63 downto 32) <= rd_data;
      proc_common_wait_some_cycles(tb_clk, 1);

      ---------------------------------------------------------------------------
      -- Verify sp_power_sum
      ---------------------------------------------------------------------------
      -- Convert STD_LOGIC_VECTOR sp_power_sum to REAL
      v_sp_power_sum_0 := real(real(TO_UINT(sp_power_sum(61 downto 30))) * real(2**30) + real(TO_UINT(sp_power_sum(29 downto 0))));

      assert v_sp_power_sum_0 > c_lo_factor * c_exp_wg_power_sp_0
        report "Wrong SP power for SP 0"
        severity ERROR;
      assert v_sp_power_sum_0 < c_hi_factor * c_exp_wg_power_sp_0
        report "Wrong SP power for SP 0"
        severity ERROR;

      -- Try to reset via JESD_CTRL. This JESD_CTRL should be ignored.
      --   Note: Awkward way to set MSbit without negative integer warning, using TO_SINT(v_word).
      dbg_jesd_ctrl_reset_ignore <= '1';  -- marker in wave window
      -- apply JESD_CTRL reset
      v_word := (others => '0');
      v_word(c_sdp_jesd_ctrl_reset_bi) := '1';  -- reset
      mmf_mm_bus_wr(c_mm_file_pio_jesd_ctrl, 0, TO_SINT(v_word), tb_clk);
      proc_common_wait_cross_clock_domain_latency(tb_clk, ext_clk);
      mmf_mm_bus_rd(c_mm_file_pio_jesd_ctrl, 0, rd_data, tb_clk);
      pio_jesd_ctrl <= rd_data;
      pio_jesd_ctrl_enable <= rd_data(c_sdp_jesd_ctrl_enable_w - 1 downto 0);
      pio_jesd_ctrl_reset <= rd_data(c_sdp_jesd_ctrl_reset_bi);
      proc_common_wait_some_cycles(tb_clk, 1);
      assert pio_jesd_ctrl_reset = '0'
        report "JESD_CTRL reset should be ignored when BSN source is on."
        severity ERROR;
      -- remove JESD_CTRL reset
      v_word := (others => '0');
      v_word(c_sdp_jesd_ctrl_reset_bi) := '0';  -- reset
      mmf_mm_bus_wr(c_mm_file_pio_jesd_ctrl, 0, TO_SINT(v_word), tb_clk);
      proc_common_wait_cross_clock_domain_latency(tb_clk, ext_clk);
      mmf_mm_bus_rd(c_mm_file_pio_jesd_ctrl, 0, rd_data, tb_clk);
      pio_jesd_ctrl <= rd_data;
      pio_jesd_ctrl_enable <= rd_data(c_sdp_jesd_ctrl_enable_w - 1 downto 0);
      pio_jesd_ctrl_reset <= rd_data(c_sdp_jesd_ctrl_reset_bi);
      dbg_jesd_ctrl_reset_ignore <= '0';

      ----------------------------------------------------------------------------
      -- Restart AIT
      -- . JESD_CTRL reset stops JESD204B OUT rx_clk and asserts JESD204B OUT
      --   rx_rst, so it is necessary to stop the BSN source, to ensure that
      --   the rx_clk/dp_clk domain FIFO runs empty before rx_rst is asserted, so
      --   that no corrupt data blocks will enter the dp_clk domain.
      ----------------------------------------------------------------------------

      -- 1) Disable BSN source to allow rx_clk/dp_clk domain FIFO to run empty
      mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0, 16#00000000#, tb_clk);

      -- . Wait until BSN source has finished last block, use ext_clk as 200 MHz
      --   equivalent for dp_clk, and wait for > 1 block of c_sdp_N_fft samples
      proc_common_wait_some_cycles(ext_clk, c_sdp_N_fft * 2);
      dbg_bsn_source_en <= '0';  -- marker in wave window

      -- 2) Reset via JESD_CTRL c_sdp_jesd_ctrl_reset_bi = 31 and read back
      --   Note: Awkward way to set MSbit without negative integer warning, using TO_SINT(v_word).
      dbg_jesd_ctrl_reset <= '1';  -- marker in wave window
      v_word := (others => '0');
      v_word(c_sdp_jesd_ctrl_reset_bi) := '1';  -- reset
      mmf_mm_bus_wr(c_mm_file_pio_jesd_ctrl, 0, TO_SINT(v_word), tb_clk);
      proc_common_wait_cross_clock_domain_latency(tb_clk, ext_clk);
      mmf_mm_bus_rd(c_mm_file_pio_jesd_ctrl, 0, rd_data, tb_clk);
      pio_jesd_ctrl <= rd_data;
      pio_jesd_ctrl_enable <= rd_data(c_sdp_jesd_ctrl_enable_w - 1 downto 0);
      pio_jesd_ctrl_reset <= rd_data(c_sdp_jesd_ctrl_reset_bi);
      proc_common_wait_some_cycles(tb_clk, 1);
      assert pio_jesd_ctrl_reset = '1'
        report "JESD_CTRL reset should be applied when BSN source is off."
        severity ERROR;

      wait for 1 us;
      -- Read Rx JESD_204B IP status during reset
      proc_read_jesd204b_arr(tb_clk, rd_data, dbg_read_jesd204b,
                             reg_jesd204b_rx_err_enable,
                             reg_jesd204b_rx_err_link_reinit,
                             reg_jesd204b_rx_syncn_sysref_ctrl,
                             reg_jesd204b_rx_err0,
                             reg_jesd204b_rx_err1,
                             reg_jesd204b_csr_rbd_count,
                             reg_jesd204b_csr_dev_syncn);

      -- Read input delay during reset
      mmf_mm_bus_rd(c_mm_file_reg_dp_shiftram, 0, rd_data, tb_clk);
      rd_input_delay <= TO_UINT(rd_data);
      proc_common_wait_some_cycles(tb_clk, 1);
      assert rd_input_delay = v_exp_input_delay
        report "wrong rd_input_delay during JESD reset."
        severity ERROR;

      -- Hold JESD_CTRL reset for > one sync period, so also during a JESD204B_SYSREF pulse,
      -- to see that JESD_CTRL reset stops JESD204B OUT rx_sysref too.
      wait for c_pps_period;

      -- . Re-enable via JESD_CTRL and read back
      v_word := (others => '0');  -- release reset
      v_word(c_si_dis) := '1';  -- disable one signal input
      mmf_mm_bus_wr(c_mm_file_pio_jesd_ctrl, 0, TO_SINT(v_word), tb_clk);
      proc_common_wait_cross_clock_domain_latency(tb_clk, ext_clk);
      mmf_mm_bus_rd(c_mm_file_pio_jesd_ctrl, 0, rd_data, tb_clk);
      pio_jesd_ctrl <= rd_data;
      pio_jesd_ctrl_enable <= rd_data(c_sdp_jesd_ctrl_enable_w - 1 downto 0);
      pio_jesd_ctrl_reset <= rd_data(c_sdp_jesd_ctrl_reset_bi);
      dbg_jesd_ctrl_reset <= '0';  -- marker in wave window

      -- Read input delay after reset
      mmf_mm_bus_rd(c_mm_file_reg_dp_shiftram, 0, rd_data, tb_clk);
      rd_input_delay <= TO_UINT(rd_data);
      proc_common_wait_some_cycles(tb_clk, 1);
      assert rd_input_delay = v_exp_input_delay
        report "wrong rd_input_delay after JESD reset."
        severity ERROR;

      -- Wait for a JESD204B_SYSREF pulse
      wait for c_pps_period;
      -- Read Rx JESD_204B IP status
      proc_read_jesd204b_arr(tb_clk, rd_data, dbg_read_jesd204b,
                             reg_jesd204b_rx_err_enable,
                             reg_jesd204b_rx_err_link_reinit,
                             reg_jesd204b_rx_syncn_sysref_ctrl,
                             reg_jesd204b_rx_err0,
                             reg_jesd204b_rx_err1,
                             reg_jesd204b_csr_rbd_count,
                             reg_jesd204b_csr_dev_syncn);

      -- 3) Reinit the JESD204B link per signal input
      dbg_link_reinit <= '1';  -- marker in wave window
      for I in 0 to c_sdp_S_pn - 1 loop
        v_int := tech_jesd204b_field_rx_syncn_sysref_ctrl_link_reinit +
                 tech_jesd204b_field_rx_syncn_sysref_ctrl_sysref_alwayson;
        mmf_mm_bus_wr(c_mm_file_jesd204b, v_offset + tech_jesd204b_field_rx_syncn_sysref_ctrl_adr, v_int, tb_clk);
      end loop;

      wait for 1 us;
      -- Read Rx JESD_204B IP status
      proc_read_jesd204b_arr(tb_clk, rd_data, dbg_read_jesd204b,
                             reg_jesd204b_rx_err_enable,
                             reg_jesd204b_rx_err_link_reinit,
                             reg_jesd204b_rx_syncn_sysref_ctrl,
                             reg_jesd204b_rx_err0,
                             reg_jesd204b_rx_err1,
                             reg_jesd204b_csr_rbd_count,
                             reg_jesd204b_csr_dev_syncn);

      for I in 0 to c_sdp_S_pn - 1 loop
        v_int := tech_jesd204b_field_rx_syncn_sysref_ctrl_sysref_alwayson;
        mmf_mm_bus_wr(c_mm_file_jesd204b, v_offset + tech_jesd204b_field_rx_syncn_sysref_ctrl_adr, v_int, tb_clk);
      end loop;
      dbg_link_reinit <= '0';  -- marker in wave window

      -- Wait for a JESD204B_SYSREF pulse
      wait for c_pps_period;
      -- Read Rx JESD_204B IP status
      proc_read_jesd204b_arr(tb_clk, rd_data, dbg_read_jesd204b,
                             reg_jesd204b_rx_err_enable,
                             reg_jesd204b_rx_err_link_reinit,
                             reg_jesd204b_rx_syncn_sysref_ctrl,
                             reg_jesd204b_rx_err0,
                             reg_jesd204b_rx_err1,
                             reg_jesd204b_csr_rbd_count,
                             reg_jesd204b_csr_dev_syncn);
      -- Count restart loops
      dbg_restart <= dbg_restart + 1;
    end loop;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    wait for 10 us;
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;
end tb;
