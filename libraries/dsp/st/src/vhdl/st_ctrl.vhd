-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity st_ctrl is
  generic (
    g_nof_mux    : natural := 1;
    g_nof_stat   : natural := 512;
    g_adr_w      : natural := 9;  -- ceil_log2(g_nof_mux*g_nof_stat)
    g_dly_rd     : natural := 1;
    g_dly_mul    : natural := 4;
    g_dly_acc    : natural := 2;
    g_dly_out    : natural := 2
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;

    in_sync      : in  std_logic;
    in_val       : in  std_logic;

    rd_en        : out std_logic;
    rd_adr       : out std_logic_vector(g_adr_w - 1 downto 0);
    rd_val       : out std_logic;

    mult_val     : out std_logic;
    acc_load     : out std_logic;

    wr_en        : out std_logic;
    wr_adr       : out std_logic_vector(g_adr_w - 1 downto 0);

    out_val      : out std_logic;
    out_val_m    : out std_logic_vector(g_nof_mux - 1 downto 0);
    out_adr      : out std_logic_vector(g_adr_w - 1 downto 0)
  );
end;

architecture rtl of st_ctrl is
 constant c_mux_w     : natural := true_log2(g_nof_mux);

 constant c_tin_mul   : natural := 0;
 constant c_tot_mul   : natural := c_tin_mul + g_dly_mul;

 constant c_tin_acc   : natural := c_tot_mul;
 constant c_tot_acc   : natural := c_tin_acc + g_dly_acc;

 constant c_tin_wr    : natural := c_tot_acc;

 constant c_tin_rd    : natural := c_tin_acc - g_dly_rd;
 constant c_tot_rd    : natural := c_tin_acc;

 constant c_tin_out   : natural := c_tot_rd;
 constant c_tot_out   : natural := c_tin_out + g_dly_out;

 signal dly_val       : std_logic_vector(0 to c_tin_wr);
 signal dly_sync      : std_logic_vector(0 to c_tin_wr);
 signal dly_load      : std_logic_vector(c_tin_rd to c_tin_wr);

 signal i_rd_adr      : std_logic_vector(rd_adr'range);
 signal nxt_rd_adr    : std_logic_vector(rd_adr'range);

 signal i_wr_adr      : std_logic_vector(wr_adr'range);
 signal nxt_wr_adr    : std_logic_vector(wr_adr'range);

 signal i_out_adr     : std_logic_vector(out_adr'range);
 signal nxt_out_adr   : std_logic_vector(out_adr'range);

 signal i_out_val     : std_logic;

 signal nxt_load      : std_logic;
begin
  -- hardwired

  dly_val (0) <= in_val;
  dly_sync(0) <= in_sync;

  rd_en    <= dly_val (c_tin_rd);
  rd_val   <= dly_val (c_tot_rd);

  mult_val <= dly_val(c_tin_acc);
  acc_load <= dly_load(c_tin_acc) or (not dly_val(c_tin_acc));

  wr_en     <= dly_val(c_tin_wr);
  i_out_val <= dly_load(c_tot_out) and dly_val(c_tot_out);

  rd_adr   <= i_rd_adr;
  wr_adr   <= i_wr_adr;
  out_adr  <= i_out_adr;
  out_val  <= i_out_val;

  no_mux : if g_nof_mux = 1 generate
    out_val_m <= (others => 'X');
  end generate;

  gen_mux : if g_nof_mux > 1 generate
    p_out_val_m: process (i_out_val, i_out_adr)
    begin
      out_val_m <= (others => '0');
      for i in 0 to g_nof_mux - 1 loop
        if unsigned(i_out_adr(c_mux_w - 1 downto 0)) = i then
          out_val_m(i) <= i_out_val;
        end if;
      end loop;
    end process;
  end generate;

  -- registers
  regs: process(rst, clk)
  begin
    if rst = '1' then
      i_rd_adr  <= (others => '0');
      i_wr_adr  <= (others => '0');
      i_out_adr <= (others => '0');
      dly_load  <= (others => '1');
      dly_val (dly_val 'low + 1 to dly_val 'high)  <= (others => '0');
      dly_sync(dly_sync'low + 1 to dly_sync'high)  <= (others => '0');
    elsif rising_edge(clk) then
      i_rd_adr  <= nxt_rd_adr;
      i_wr_adr  <= nxt_wr_adr;
      i_out_adr <= nxt_out_adr;
      dly_load  <= nxt_load & dly_load(dly_load'low to dly_load'high - 1);
      dly_val (dly_val 'low + 1 to dly_val 'high) <= dly_val  (dly_val 'low to dly_val 'high - 1);
      dly_sync(dly_sync'low + 1 to dly_sync'high) <= dly_sync (dly_sync'low to dly_sync'high - 1);
    end if;
  end process;

  rd_ctrl: process(i_rd_adr, dly_load, dly_val, dly_sync)
  begin
    nxt_load   <= dly_load(dly_load'low);
    nxt_rd_adr <= i_rd_adr;
    if dly_sync(c_tin_rd) = '1' then
      nxt_rd_adr <= (others => '0');
      nxt_load   <= '1';
    elsif dly_val(c_tin_rd) = '1' then
      if unsigned(i_rd_adr) = g_nof_mux * g_nof_stat - 1 then
        nxt_rd_adr <= (others => '0');
        nxt_load   <= '0';
      else
        nxt_rd_adr <= std_logic_vector(unsigned(i_rd_adr) + 1);
      end if;
    end if;
  end process;

  out_ctrl: process(i_out_adr, dly_val, dly_sync)
  begin
    nxt_out_adr   <= i_out_adr;
    if dly_sync(c_tot_out) = '1' then
      nxt_out_adr <= (others => '0');
    elsif dly_val(c_tot_out) = '1' then
      if unsigned(i_out_adr) = g_nof_mux * g_nof_stat - 1 then
        nxt_out_adr <= (others => '0');
      else
        nxt_out_adr <= std_logic_vector(unsigned(i_out_adr) + 1);
      end if;
    end if;
  end process;

  wr_ctrl: process(i_wr_adr, dly_val, dly_sync)
  begin
    nxt_wr_adr   <= i_wr_adr;
    if dly_sync(c_tin_wr) = '1' then
      nxt_wr_adr <= (others => '0');
    elsif dly_val(c_tin_wr) = '1' then
      if unsigned(i_wr_adr) = g_nof_mux * g_nof_stat - 1 then
        nxt_wr_adr <= (others => '0');
      else
        nxt_wr_adr <= std_logic_vector(unsigned(i_wr_adr) + 1);
      end if;
    end if;
  end process;
end rtl;
