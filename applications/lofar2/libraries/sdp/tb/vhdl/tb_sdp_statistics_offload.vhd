-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: P. Donker, E. Kooistra

-- Purpose:
-- . test bench for sdp_statistics_offload.vhd
-- Description:
--
-- https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+filterbank
-- . See figure 4.8
--
-- Usage:
-- > as 8
-- > run -a
-- . for header: view rx_offload_sosi and the rx_sdp_stat_header.app fields
-- . for payload: view rx_val, rx_data and exp_data
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, ring_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.tb_common_pkg.all;
  use common_lib.tb_common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use common_lib.common_field_pkg.all;
  use common_lib.common_str_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use ring_lib.ring_pkg.all;
  use work.sdp_pkg.all;
  use work.tb_sdp_pkg.all;

entity tb_sdp_statistics_offload is
  generic (
    -- All
    g_fast_mm_clk              : boolean := true;  -- When TRUE use 1 GHz mm_clk  to speed up simulation, else use
                                                   -- 100 MHz mm_clk for real speed of u_dp_block_from_mm_dc in
                                                   -- sdp_statistics_offload
    g_statistics_type          : string := "XST";
    g_offload_node_time        : natural := 50;
    g_offload_packet_time      : natural := 5;
    g_offload_scale_w          : natural := 0;  -- 0 = default
    g_ctrl_interval_size       : natural := 1;  -- 1 = default
    g_ctrl_interval_size_min   : natural := 1;  -- 1 = default
    g_reverse_word_order       : boolean := true;  -- when TRUE then stream LSB word after MSB word.
    g_gn_index                 : natural := 4;  -- global node (GN) index, must be in range(O_rn, O_rn + N_rn),
                                                -- use > 0 to see effect of g_offload_node_time
    g_nof_sync                 : natural := 3;  -- simulate some sync periods, choose >= 3
    -- BST
    g_beamset_id               : natural := 0;  -- < c_sdp_N_beamsets
    -- XST
    g_O_rn                     : natural := 0;  -- GN index of first ring node (RN)
    g_N_rn                     : natural := 8;  -- <= c_sdp_N_rn_max = 16, number of nodes in ring
    g_P_sq                     : natural := 9;  -- <= c_sdp_P_sq, nof available correlator cells
    g_nof_crosslets            : natural := 7;  -- <= c_sdp_N_crosslets_max
    g_crosslets_direction      : natural := 1  -- > 0 for crosslet transport in positive direction (incrementing RN),
                                               -- else 0 for negative direction
  );
end tb_sdp_statistics_offload;

architecture tb of tb_sdp_statistics_offload is
  constant c_dp_clk_period : time := 5 ns;  -- 200 MHz
  constant c_mm_clk_period : time := sel_a_b(g_fast_mm_clk, 1,  10) * 1 ns;
  constant c_mm_dp_clk_ratio : natural := sel_a_b(c_mm_clk_period > c_dp_clk_period,
                                                  c_mm_clk_period / c_dp_clk_period, 1);

  constant c_cross_clock_domain_latency : natural := 20;

  constant c_offload_node_time : natural := g_offload_node_time * g_gn_index;

  -- Use sim default dst and src MAC, IP, UDP port from sdp_pkg.vhd and based on g_gn_index
  constant c_node_eth_src_mac  : std_logic_vector(47 downto 0) := c_sdp_stat_eth_src_mac_47_16 &
                                                                  func_sdp_gn_index_to_mac_15_0(g_gn_index);
  constant c_node_ip_src_addr  : std_logic_vector(31 downto 0) := c_sdp_stat_ip_src_addr_31_16 &
                                                                  func_sdp_gn_index_to_ip_15_0(g_gn_index);
  constant c_node_udp_src_port : std_logic_vector(15 downto 0) := func_sdp_get_stat_udp_src_port(g_statistics_type,
                                                                                                 g_gn_index);

  -- Used mm_adresses on mm bus "enable_mosi/miso".
  constant c_reg_enable_mm_addr_enable : natural := 0;

  -- header fields
  constant c_nof_statistics_per_packet : natural := func_sdp_get_stat_nof_statistics_per_packet(g_statistics_type);
  constant c_udp_total_length          : natural := func_sdp_get_stat_udp_total_length(g_statistics_type);
  constant c_ip_total_length           : natural := func_sdp_get_stat_ip_total_length(g_statistics_type);
  constant c_marker                    : natural := func_sdp_get_stat_marker(g_statistics_type);
  constant c_nof_signal_inputs         : natural := func_sdp_get_stat_nof_signal_inputs(g_statistics_type);
  constant c_nof_packets_max           : natural := func_sdp_get_stat_nof_packets(
                                                        g_statistics_type, c_sdp_S_pn, g_P_sq, c_sdp_N_crosslets_max);

  constant c_exp_ip_header_checksum    : natural := 0;  -- 0 in this local tb, calculated by IO eth when used in design

  constant c_exp_sdp_info  :  t_sdp_info := (TO_UVEC(7, 6),  -- antenna_field_index
                                             TO_UVEC(601, 10),  -- station_id
                                              '0',  -- antenna_band_index
                                              x"FFFFFFFF",  -- observation_id
                                              b"01",  -- nyquist_zone_index, 0 = first, 1 = second, 2 = third
                                              '1',  -- f_adc, 0 = 160 MHz, 1 = 200 MHz
                                              '0',  -- fsub_type, 0 = critically sampled, 1 = oversampled
                                              '0',  -- beam_repositioning_flag
                                              x"1400"  -- block_period = 5120
                                            );

  constant c_exp_ring_info  :  t_ring_info := (TO_UVEC(g_O_rn, 8),  -- GN index of first GN in ring
                                               TO_UVEC(g_N_rn, 8),  -- number of GN in ring
                                              '0',  -- use_cable_to_next_rn
                                              '0'  -- use_cable_to_previous_rn
                                              );

  constant c_beamlet_index : natural := g_beamset_id * c_sdp_S_sub_bf;

  --CONSTANT c_crosslets_info_rec  : t_sdp_crosslets_info := (
  --                                   offset_arr => (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
  --                                   step => 16);
  constant c_crosslets_info_rec  : t_sdp_crosslets_info := (
                                     offset_arr => (0, 1, 2, 3, 4, 5, 6, 10, 11, 12, 13, 14, 15, 16, 17),
                                     step => 7);
  constant c_crosslets_info_slv  : std_logic_vector(c_sdp_crosslets_info_reg_w - 1 downto 0) :=
                                     func_sdp_map_crosslets_info(c_crosslets_info_rec);

  -- Crosslets settings
  constant c_mm_nof_crosslets : std_logic_vector(c_sdp_nof_crosslets_reg_w - 1 downto 0) :=
                                  TO_UVEC(g_nof_crosslets, c_sdp_nof_crosslets_reg_w);
  constant c_nof_used_P_sq    : natural := smallest(g_N_rn / 2 + 1,
                                                    g_P_sq);  -- number of used correlator cells <= g_P_sq
  constant c_rx_nof_packets   : natural := func_sdp_get_stat_nof_packets(g_statistics_type,
                                                                         c_sdp_S_pn, c_nof_used_P_sq, g_nof_crosslets);

  -- payload data
  constant c_packet_size : natural := c_nof_statistics_per_packet * c_sdp_W_statistic_sz;

  -- Define statistics RAM size for c_nof_packets_max.
  constant c_ram_size  : natural := c_packet_size * c_nof_packets_max;
  constant c_ram_w     : natural := ceil_log2(c_ram_size);
  --CONSTANT c_ram_buf   : t_c_mem := (c_mem_ram_rd_latency, c_ram_w,  32, 2**c_ram_w, 'X');
  constant c_ram_buf   : t_c_mem := (1, c_ram_w,  32, 2**c_ram_w, 'X');

  -- RAM dimensions
  -- . nof_statistics_per_packet = c_mm_nof_data * c_mm_data_size / c_sdp_W_statistic_sz
  constant c_mm_user_size        : natural := func_sdp_get_stat_from_mm_user_size(g_statistics_type);
  constant c_mm_data_size        : natural := func_sdp_get_stat_from_mm_data_size(g_statistics_type);
  constant c_mm_step_size        : natural := func_sdp_get_stat_from_mm_step_size(g_statistics_type);
  constant c_mm_nof_data         : natural := func_sdp_get_stat_from_mm_nof_data(g_statistics_type);
  constant c_mm_ram_size         : natural := c_mm_nof_data * c_mm_data_size * c_nof_packets_max;  -- = c_ram_size

  constant c_mm_nof_step         : natural := c_mm_step_size / c_mm_data_size;
  constant c_mm_Xsq_span         : natural := 2**ceil_log2(c_sdp_N_crosslets_max * c_packet_size);
                                              -- XST: 2**ceil_log2(7 * 576) = 4096

  -- Define block timing.
  constant c_bsn_init            : natural := 0;
  -- Sufficient c_nof_block_per_sync to fit more than c_nof_packets_max offload packets per sync interval.
  constant c_nof_block_per_sync  : natural := 3 +
                                              c_mm_dp_clk_ratio * (ceil_div(c_offload_node_time, c_packet_size) +
                                                                   c_nof_packets_max);
  constant c_nof_clk_per_block   : natural := c_packet_size;
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_nof_clk_per_block;

  signal tb_end : std_logic := '0';

  signal dp_clk : std_logic := '1';  -- Digital data path clock = 200 MHz (deser factor 4);
  signal dp_rst : std_logic;

  signal mm_clk : std_logic := '1';  -- MM control clock = 50 MHz
  signal mm_rst : std_logic;

  signal master_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal master_miso : t_mem_miso;

  signal enable_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal enable_miso : t_mem_miso;

  signal hdr_dat_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal hdr_dat_miso : t_mem_miso;

  signal offload_rx_hdr_dat_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal offload_rx_hdr_dat_miso : t_mem_miso;

  signal new_interval            : std_logic := '0';
  signal in_sosi                 : t_dp_sosi := c_dp_sosi_rst;
  signal in_crosslets_info_rec   : t_sdp_crosslets_info;

  signal sdp_offload_data        : std_logic_vector(c_word_w - 1 downto 0);  -- 32 bit
  signal sdp_offload_sosi        : t_dp_sosi;
  signal sdp_offload_siso        : t_dp_siso := c_dp_siso_rst;

  signal rx_offload_en           : std_logic := '0';
  signal rx_offload_data         : std_logic_vector(c_word_w - 1 downto 0);  -- 32 bit
  signal rx_offload_sosi         : t_dp_sosi := c_dp_sosi_rst;
  signal rx_offload_sop_cnt      : natural := 0;
  signal rx_offload_eop_cnt      : natural := 0;

  signal rx_hdr_fields_out       : std_logic_vector(1023 downto 0);
  signal rx_hdr_fields_raw       : std_logic_vector(1023 downto 0) := (others => '0');
  signal rx_sdp_stat_header      : t_sdp_stat_header;
  signal exp_sdp_stat_header     : t_sdp_stat_header;

  signal exp_sdp_info            : t_sdp_info := c_exp_sdp_info;
  signal cur_dp_bsn              : natural;
  signal exp_dp_bsn              : natural;
  signal exp_sst_signal_input    : natural;
  signal exp_bst_beamlet_index   : natural;
  signal cur_X_sq_cell           : natural;
  signal cur_crosslet            : natural;
  signal exp_subband_index       : natural;
  signal exp_xst_signal_input_A  : natural;
  signal exp_xst_signal_input_B  : natural;

  signal rx_val                  : std_logic := '0';
  signal rx_data                 : natural;
  signal exp_data                : natural;

  signal gn_index                : natural := g_gn_index;  -- this node GN
  signal rn_index                : natural := g_gn_index - g_O_rn;  -- this node RN
  signal source_rn               : natural;  -- source node RN
  signal source_gn               : natural;  -- source node GN

  signal weighted_subbands_flag  : std_logic := '0';

  -- Signals used for starting processes.
  signal ram_wr_data      : std_logic_vector(c_ram_buf.dat_w - 1 downto 0);
  signal ram_wr_addr      : std_logic_vector(c_ram_buf.adr_w - 1 downto 0);
  signal ram_wr_en        : std_logic;
  signal init_ram_done    : std_logic := '0';

  signal rx_sync_cnt      : integer := 0;
  signal rx_packet_cnt    : natural := 0;
  signal rx_valid_cnt     : natural := 0;

  -- Debug signals, to view in Wave window
  signal dbg_c_mm_dp_clk_ratio           : natural := c_mm_dp_clk_ratio;
  signal dbg_c_nof_statistics_per_packet : natural := c_nof_statistics_per_packet;
  signal dbg_c_udp_total_length          : natural := c_udp_total_length;
  signal dbg_c_ip_total_length           : natural := c_ip_total_length;
  signal dbg_c_marker                    : natural := c_marker;
  signal dbg_c_nof_signal_inputs         : natural := c_nof_signal_inputs;
  signal dbg_c_nof_packets_max           : natural := c_nof_packets_max;
  signal dbg_c_rx_nof_packets            : natural := c_rx_nof_packets;
  signal dbg_c_beamlet_index             : natural := c_beamlet_index;
  signal dbg_c_packet_size               : natural := c_packet_size;
  signal dbg_c_mm_user_size              : natural := c_mm_user_size;
  signal dbg_c_mm_data_size              : natural := c_mm_data_size;
  signal dbg_c_mm_step_size              : natural := c_mm_step_size;
  signal dbg_c_mm_nof_data               : natural := c_mm_nof_data;
  signal dbg_c_mm_ram_size               : natural := c_mm_ram_size;
  signal dbg_c_mm_nof_step               : natural := c_mm_nof_step;
  signal dbg_c_mm_Xsq_span               : natural := c_mm_Xsq_span;
  signal dbg_c_ram_size                  : natural := c_ram_size;
  signal dbg_c_crosslets_info_rec  : t_sdp_crosslets_info := c_crosslets_info_rec;
  signal dbg_c_crosslets_info_slv  : std_logic_vector(c_sdp_crosslets_info_reg_w - 1 downto 0) := c_crosslets_info_slv;
  signal dbg_c_nof_block_per_sync  : natural := c_nof_block_per_sync;
  signal dbg_c_nof_clk_per_block   : natural := c_nof_clk_per_block;
  signal dbg_c_nof_clk_per_sync    : natural := c_nof_clk_per_sync;
begin
  -- Check consistency of constant value dependencies
  assert c_ram_size = c_mm_ram_size
    report "Wrong derivation of RAM size."
    severity FAILURE;

  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;

  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;

  -- Fill statistics RAM with data, data is same as address number.
  p_mm_statistics_ram : process
  begin
    ram_wr_en <= '0';
    -- Initialyze
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    for i in 0 to c_ram_buf.nof_dat - 1 loop
      ram_wr_addr <= TO_UVEC(i, c_ram_buf.adr_w);
      ram_wr_data <= TO_UVEC(i, c_ram_buf.dat_w);
      ram_wr_en   <= '1';
      proc_common_wait_some_cycles(mm_clk, 1);
    end loop;
    ram_wr_en <= '0';

    init_ram_done <= '1';
    wait;
  end process;

  -- Start the input when statistics RAM is initialized
  p_in_sosi : process
  begin
    in_sosi.bsn <= TO_DP_BSN(c_bsn_init);
    proc_common_wait_until_high(mm_clk, init_ram_done);
    proc_common_wait_some_cycles(dp_clk, 10);
    -- Mark first in_sosi.sync interval, starting and ending somewhat before in_sosi.sync, to
    -- ensure that one in_sosi.sync occurs during new_interval. Use c_nof_clk_per_block as
    -- sufficient margin to allow for some latency between new_interval and in_sosi.sync.
    new_interval <= '1';
    proc_common_wait_some_cycles(dp_clk, c_nof_clk_per_block);
    -- Start active in_sosi
    in_sosi.valid <= '1';
    in_crosslets_info_rec <= c_crosslets_info_rec;
    while true loop
      -- One in_sosi.sync interval per LOOP
      for i in 0 to c_nof_block_per_sync - 1 loop
        -- End new_interval somewhat before next in_sosi.sync
        if i = c_nof_block_per_sync - 1 then
          new_interval <= '0';
        end if;
        -- One in_sosi.sop/eop block per LOOP
        for j in 0 to c_nof_clk_per_block - 1 loop
          in_sosi.sync  <= '0';
          in_sosi.sop   <= '0';
          in_sosi.eop   <= '0';
          if i = 0 and j = 0 then
            in_sosi.sync <= '1';
          end if;
          if i = 0 and j = 1 then
            -- Increment crosslets_info offsets for next sync interval
            in_crosslets_info_rec <= func_sdp_step_crosslets_info(in_crosslets_info_rec);
          end if;
          if j = 0 then
            in_sosi.sop  <= '1';
            in_sosi.bsn  <= INCR_UVEC(in_sosi.bsn, 1);
          end if;
          if j = c_nof_clk_per_block - 1 then
            in_sosi.eop  <= '1';
          end if;
          proc_common_wait_some_cycles(dp_clk, 1);
        end loop;
      end loop;
    end loop;
    wait;
  end process;

  -- Enable the statistics offload when input is running
  p_enable_trigger : process
  begin
    proc_common_wait_until_low(dp_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);
    -- Write statistics offload destination MAC/IP/UDP
    -- . obtain relative MM word addresses e.g. from lofar2_unb2c_sdp_station.mmap or from sdp.peripheral.yaml
    proc_mem_mm_bus_wr(16#17#, TO_UINT(c_sdp_stat_udp_dst_port),
                       mm_clk, hdr_dat_miso, hdr_dat_mosi);
    proc_mem_mm_bus_wr(16#19#, TO_SINT(c_sdp_stat_ip_dst_addr),
                       mm_clk, hdr_dat_miso, hdr_dat_mosi);  -- use signed to fit 32 b in INTEGER
    proc_mem_mm_bus_wr(16#28#, TO_SINT(c_sdp_stat_eth_dst_mac(31 downto 0)),
                       mm_clk, hdr_dat_miso, hdr_dat_mosi);  -- use signed to fit 32 b in INTEGER
    proc_mem_mm_bus_wr(16#29#, TO_UINT(c_sdp_stat_eth_dst_mac(47 downto 32)),
                       mm_clk, hdr_dat_miso, hdr_dat_mosi);

    -- Enable common variable delay.
    proc_mem_mm_bus_wr(c_reg_enable_mm_addr_enable, 1, mm_clk, enable_miso, enable_mosi);
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, 1);
    rx_offload_en <= '1';
    wait;
  end process;

  -- Counters to time expected exp_sdp_stat_header fields per offload packet
  p_test_counters : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- Count rx_offload_sosi packets
      if rx_offload_sosi.sop = '1' then
        rx_offload_sop_cnt <= rx_offload_sop_cnt + 1;  -- early count
      end if;
      if rx_offload_sosi.eop = '1' then
        rx_offload_eop_cnt <= rx_offload_eop_cnt + 1;  -- after count
      end if;
    end if;
  end process;

  rx_sync_cnt <= rx_sync_cnt + 1 when rising_edge(dp_clk) and rx_offload_sosi.sync = '1';

  -- derive current X_sq correlator cell index
  cur_X_sq_cell <= (rx_offload_eop_cnt / g_nof_crosslets) mod c_nof_used_P_sq;
  -- derive current N_crosslets index index
  cur_crosslet <= rx_offload_eop_cnt mod g_nof_crosslets;

  -- derive source RN index
  source_rn <= func_ring_nof_hops_to_source_rn(cur_X_sq_cell, rn_index, g_N_rn, g_crosslets_direction);
  source_gn <= g_O_rn + source_rn;

  -- Prepare exp_sdp_stat_header before rx_offload_sosi.eop, so that p_exp_sdp_stat_header can
  -- verify it at rx_offload_sosi.eop.

  -- For all statistics
  cur_dp_bsn <= c_bsn_init + 1 + rx_sync_cnt * c_nof_block_per_sync;  -- in current sync interval
  exp_dp_bsn <= cur_dp_bsn when rising_edge(dp_clk) and rx_offload_sosi.sync = '1';  -- previous sync interval
  -- SST, SST_OS
  exp_sst_signal_input <= (rx_packet_cnt mod c_sdp_S_pn) + c_sdp_S_pn * gn_index;  -- Using MOD c_sdp_S_pn for SST_OS.

  gen_fsub_type : if g_statistics_type = "SST_OS" generate
    exp_sdp_info.fsub_type <= '0' when rx_packet_cnt < c_sdp_S_pn else '1';
  end generate;

  -- BST
  exp_bst_beamlet_index <= c_beamlet_index;
  -- XST
  -- . prepare expected XST subband_index
  exp_subband_index <=
      (c_crosslets_info_rec.offset_arr(cur_crosslet) + rx_sync_cnt * c_crosslets_info_rec.step) mod c_sdp_N_sub;

  -- . prepare expected XST signal_input_A index
  exp_xst_signal_input_A <= (gn_index mod c_sdp_N_pn_max) * c_sdp_S_pn;

  -- . prepare expected XST signal_input_B index, assume crosslet transport in positive direction
  exp_xst_signal_input_B <= (source_gn mod c_sdp_N_pn_max) * c_sdp_S_pn;

  exp_sdp_stat_header <= func_sdp_compose_stat_header(c_exp_ip_header_checksum,
                                                      exp_sdp_info,
                                                      g_statistics_type,
                                                      weighted_subbands_flag,
                                                      gn_index,
                                                      c_nof_block_per_sync,
                                                      exp_sst_signal_input,
                                                      c_beamlet_index,
                                                      exp_subband_index,
                                                      exp_xst_signal_input_A,
                                                      exp_xst_signal_input_B,
                                                      exp_dp_bsn);

  rx_sdp_stat_header <= func_sdp_map_stat_header(rx_hdr_fields_raw);

  p_verify_header : process(rx_offload_sosi)
    variable v_bool : boolean;
  begin
    -- Prepare exp_sdp_stat_header before rx_offload_sosi.eop, so that it can be verified at rx_offload_sosi.eop
    if rx_offload_sosi.eop = '1' then
      v_bool := func_sdp_verify_stat_header(g_statistics_type, rx_sdp_stat_header, exp_sdp_stat_header);
    end if;
  end process;

  -- Count number of packets in a sync interval.
  p_rx_packet_cnt : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if rx_offload_sosi.sync = '1' then
        rx_packet_cnt <= 0;
      elsif rx_offload_sosi.eop = '1' then
        rx_packet_cnt <= rx_packet_cnt + 1;
      end if;
    end if;
  end process;

  -- Verify number of packets per sync interval
  p_verify_nof_packets : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if rx_offload_sosi.sync = '1' and rx_sync_cnt > 1 then
        assert rx_packet_cnt = c_rx_nof_packets
          report "Wrong number of packets per sync interval"
          severity ERROR;
      end if;
    end if;
  end process;

  p_verify_nof_valid_per_packet : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if rx_offload_sosi.eop = '1' then
        rx_valid_cnt <= 0;
        assert rx_valid_cnt = c_packet_size - 1
          report "Wrong number of valid per packet"
          severity ERROR;
      elsif rx_offload_sosi.valid = '1' then
        rx_valid_cnt <= rx_valid_cnt + 1;
      end if;
    end if;
  end process;

  p_verify_payload : process(dp_clk)
    variable W            : natural;  -- 32bit Words
    variable D            : natural;  -- Data values of c_mm_data_size
    variable S            : natural;  -- Statistic values of c_sdp_W_statistic_sz
    variable P            : natural;  -- Packet count
    variable I, J, K, L   : natural;  -- auxiliary index
    variable U            : natural;  -- real sUbband SST values
    variable B            : natural;  -- dual polarization Beamlet BST values
    variable X            : natural;  -- complex crosslet XST values
    variable v_rx_data    : natural;  -- received (rx) 32bit word
    variable v_exp_data   : natural;  -- expected (exp) 32bit word
  begin
    if rising_edge(dp_clk) then
      rx_val <= '0';
      v_rx_data := TO_UINT(rx_offload_sosi.data);
      if rx_offload_sosi.valid = '1' then
        if g_statistics_type = "SST" or g_statistics_type = "SST_OS" then
          --        Indices:
          --         W:    0     1      2     3      4     5 ...  1022  1023
          -- U = D = S:    0            1            2             511
          --         I:    0     1      0     1      0     1 ...     0     1
          --    P: J: Words values:
          --    0  0       0     1      4     5      8     9 ...  2044  2045
          --    1  1       2     3      6     7     10    11 ...  2046  2047
          --    2  0    2048  2049   2052  2053   2056  2057 ...  4092  4093
          --    3  1    2050  2051   2054  2055   2058  2059 ...  4094  4095
          --   ...                                           ...
          --   11  1   10242 10243  10246 10247  10250 10251 ... 12286 12287
          --
          -- g_reverse_word_order = TRUE: swaps odd and even W columns, because
          --   c_mm_user_size = c_sdp_W_statistic_sz = 2

          W := rx_valid_cnt;  -- range c_packet_size = 1024 32bit Words
          S := W / c_sdp_W_statistic_sz;  -- range c_nof_statistics_per_packet = 512 Statistic values
          D := S;  -- range c_mm_nof_data = 512 Data values, because
                                            -- c_mm_data_size / c_sdp_W_statistic_sz = 1
          U := S;  -- range c_sdp_N_sub = 512 SST values
          I := W mod c_mm_user_size;  -- range c_mm_user_size = c_sdp_W_statistic_sz = 2 words
          P := rx_packet_cnt mod c_rx_nof_packets;  -- range c_nof_packets_max = 12 = c_sdp_S_pn packets
          J := P mod c_mm_nof_step;  -- range c_mm_nof_step = 2 = c_sdp_Q_fft

          v_exp_data := S * 4;  -- due to c_mm_step_size = 4 = c_sdp_W_statistic_sz * c_sdp_Q_fft;
          if I = sel_a_b(g_reverse_word_order, 0, 1) then
            v_exp_data := v_exp_data + 1;  -- due to c_mm_user_size = 2
          end if;
          if J = 1 then
            v_exp_data := v_exp_data + 2;  -- due to c_sdp_W_statistic_sz = 2 and c_mm_nof_step = 2 > 1
          end if;
          v_exp_data := v_exp_data + (P / 2) * 2048;  -- due to c_packet_size = 1024 and c_mm_nof_step = 2 > 1
          if g_statistics_type = "SST" then
            assert v_exp_data = v_rx_data
              report "Wrong SST payload data Rx"
              severity ERROR;
          end if;
          if g_statistics_type = "SST_OS" then
            assert v_exp_data = v_rx_data
              report "Wrong SST_OS payload data Rx"
              severity ERROR;
          end if;

        elsif g_statistics_type = "BST" then
          --    Indices:
          --     W:    0     1      2     3      4     5 ...  1948  1949   1950  1951
          --     S:    0            1            2       ...   974          975
          -- B = D:    0                         1       ...   487
          --     I:    0     1      0     1      0       ...     0     1      0     1
          --  P:  Words values:
          --  0        0     1      2     3      4     5 ...  1948  1949   1950  1951
          --
          -- g_reverse_word_order = TRUE: swaps odd and even W columns, because
          --   c_mm_user_size = c_sdp_W_statistic_sz = 2

          W := rx_valid_cnt;  -- range c_packet_size = 1952
          S := W / c_sdp_W_statistic_sz;  -- range c_nof_statistics_per_packet = 976 Statistic values
          D := S / c_sdp_N_pol_bf;  -- range c_mm_nof_data = 488 Data values, because
                                            -- c_mm_data_size / c_sdp_W_statistic_sz = 2 = c_sdp_N_pol_bf
          B := D;  -- range c_sdp_S_sub_bf = 488 dual polarization BST values
          I := W mod c_mm_user_size;  -- range c_mm_user_size = c_sdp_W_statistic_sz = 2 words
          P := rx_packet_cnt mod c_rx_nof_packets;  -- range c_nof_packets_max = 1 packet

          v_exp_data := S * c_mm_user_size;  -- c_mm_user_size = 2
          if g_reverse_word_order = false then
            v_exp_data := v_exp_data + I;
          else
            v_exp_data := v_exp_data - I + c_mm_user_size-1;
          end if;
          assert v_exp_data = v_rx_data
            report "Wrong BST payload data Rx"
            severity ERROR;

        elsif g_statistics_type = "XST" then
          -- . c_nof_used_P_sq = 4
          -- . g_nof_crosslets = 3
          -- . c_sdp_N_crosslets_max = 7 --> c_mm_Xsq_span = 2**ceil_log2(7 * 576) = 4096
          --
          --      W:        0     1      2     3      4     5 ...   572   573   574   575
          --      S:        0            1            2       ...   286         287
          --  X = D:        0                         1       ...   143
          --      I:        0     1      0     1      0       ...     0     1     0     1
          --   P: J: K: Word values:
          --   0  0  0      0     1      2     3      4     5 ...   572   573   574   575
          --   1  1       576                                 ...
          --   2  2      1052                                 ...
          --
          --   3  0  1   4096                                 ...
          --   4  1      4672                                 ...
          --   5  2      5244                                 ...
          --   ...
          --   9  0  3  12288                                 ...
          --  10  1     12864                                 ...
          --  11  2     13436                                 ...
          --
          -- g_reverse_word_order = TRUE: swaps odd and even W columns, because
          --   c_mm_user_size = c_sdp_W_statistic_sz = 2

          W := rx_valid_cnt;  -- range c_packet_size = 576
          S := W / c_sdp_W_statistic_sz;  -- range c_nof_statistics_per_packet = 288 Statistic values
          D := S / c_nof_complex;  -- range c_mm_nof_data = 144 Data values, because
                                   -- c_mm_data_size / c_sdp_W_statistic_sz = 2 = c_nof_complex
          X := D;  -- range c_sdp_X_sq = 144 complex XST values
          I := W mod c_mm_user_size;  -- range c_mm_user_size = c_sdp_W_statistic_sz = 2 words
          P := rx_packet_cnt mod c_rx_nof_packets;  -- range c_nof_packets_max = c_nof_used_P_sq *
                                                    -- g_nof_crosslets packets
          J := P mod g_nof_crosslets;  -- range g_nof_crosslets
          K := P / g_nof_crosslets;  -- range c_nof_used_P_sq

          v_exp_data := S * c_mm_user_size;  -- c_mm_user_size = 2
          if g_reverse_word_order = false then
            v_exp_data := v_exp_data + I;
          else
            v_exp_data := v_exp_data - I + c_mm_user_size-1;
          end if;
          v_exp_data := v_exp_data + J * c_packet_size;  -- c_packet_size = 576
          v_exp_data := v_exp_data + K * c_mm_Xsq_span;  -- c_mm_Xsq_span = 4096
          assert v_exp_data = v_rx_data
            report "Wrong XST payload data Rx"
            severity ERROR;
        end if;
        -- for debug in wave window
        rx_val <= '1';
        rx_data <= v_rx_data;
        exp_data <= v_exp_data;
      end if;
    end if;
  end process;

  p_dp_end : process
  begin
    proc_common_wait_until_high(mm_clk, init_ram_done);
    proc_common_wait_some_cycles(dp_clk, g_nof_sync * c_nof_clk_per_sync);  -- will show some sync periods
    tb_end <= '1';
    wait;
  end process;

  u_ram: entity common_lib.common_ram_crw_crw
  generic map (
    g_ram => c_ram_buf
  )
  port map (
    -- MM write port clock domain.
    rst_a    => mm_rst,
    clk_a    => mm_clk,
    wr_en_a  => ram_wr_en,
    wr_dat_a => ram_wr_data,
    adr_a    => ram_wr_addr,

    -- DP read only port clock domain.
    rst_b    => mm_rst,
    clk_b    => mm_clk,
    adr_b    => master_mosi.address(c_ram_buf.adr_w - 1 downto 0),
    rd_en_b  => master_mosi.rd,
    rd_dat_b => master_miso.rddata(c_ram_buf.dat_w - 1 downto 0),
    rd_val_b => master_miso.rdval
  );

  u_rx : entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams         => 1,
    g_data_w              => c_word_w,
    g_hdr_field_arr       => c_sdp_stat_hdr_field_arr,
    g_remove_crc          => false,
    g_crc_nof_words       => 0
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    reg_hdr_dat_mosi      => offload_rx_hdr_dat_mosi,
    reg_hdr_dat_miso      => offload_rx_hdr_dat_miso,

    snk_in_arr(0)         => sdp_offload_sosi,
    snk_out_arr(0)        => sdp_offload_siso,

    src_out_arr(0)        => rx_offload_sosi,

    hdr_fields_out_arr(0) => rx_hdr_fields_out,
    hdr_fields_raw_arr(0) => rx_hdr_fields_raw
  );

  -- SDP info
  u_dut: entity work.sdp_statistics_offload
  generic map (
    g_statistics_type        => g_statistics_type,
    g_offload_node_time      => g_offload_node_time,
    g_offload_packet_time    => g_offload_packet_time,
    g_offload_scale_w        => g_offload_scale_w,
    g_ctrl_interval_size_min => g_ctrl_interval_size_min,
    g_reverse_word_order     => g_reverse_word_order,
    g_beamset_id             => g_beamset_id,
    g_P_sq                   => g_P_sq,
    g_crosslets_direction    => g_crosslets_direction
  )
  port map (
    mm_clk => mm_clk,
    mm_rst => mm_rst,

    dp_clk => dp_clk,
    dp_rst => dp_rst,

    -- MM
    master_mosi      => master_mosi,
    master_miso      => master_miso,

    reg_enable_mosi  => enable_mosi,
    reg_enable_miso  => enable_miso,

    reg_hdr_dat_mosi => hdr_dat_mosi,
    reg_hdr_dat_miso => hdr_dat_miso,

    -- ST
    in_sosi            => in_sosi,
    new_interval       => new_interval,
    ctrl_interval_size => g_ctrl_interval_size,

    out_sosi         => sdp_offload_sosi,
    out_siso         => sdp_offload_siso,

    -- Inputs from other blocks
    eth_src_mac             => c_node_eth_src_mac,
    udp_src_port            => c_node_udp_src_port,
    ip_src_addr             => c_node_ip_src_addr,

    gn_index                => gn_index,
    ring_info               => c_exp_ring_info,
    sdp_info                => c_exp_sdp_info,
    weighted_subbands_flag  => weighted_subbands_flag,

    nof_crosslets           => c_mm_nof_crosslets,
    prev_crosslets_info_rec => in_crosslets_info_rec
  );

  -- Verify crosslets_info functions
  assert c_crosslets_info_rec = func_sdp_map_crosslets_info(c_crosslets_info_slv)
    report "Error in func_sdp_map_crosslets_info()"
    severity FAILURE;

  -- To view the 32 bit 1GbE offload data more easily in the Wave window
  sdp_offload_data <= sdp_offload_sosi.data(c_word_w - 1 downto 0);
  rx_offload_data <= rx_offload_sosi.data(c_word_w - 1 downto 0);
end tb;
