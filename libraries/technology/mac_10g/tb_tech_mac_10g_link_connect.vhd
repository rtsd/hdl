-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Reusable test bench link connection for the tech_mac_10g
-- Description:
-- . Support XGMII layer connect
-- . Support PHY serial layer connect with optional link_fault
-- . Support link delay

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity tb_tech_mac_10g_link_connect is
  generic (
    g_loopback    : boolean := true;  -- default TRUE for loopback tx to rx, else use FALSE to connect tx-tx, rx-rx between two tb devices
    g_link_delay  : time :=  0 ns
  );
  port (
    link_fault    : in  std_logic := '0';  -- when '1' then forces rx_serial='0' and xaui_rx="0000"

    -- XGMII layer connect
    xgmii_tx_data : in  std_logic_vector(c_xgmii_w - 1 downto 0) := (others => 'X');  -- 72 bit
    xgmii_rx_data : out std_logic_vector(c_xgmii_w - 1 downto 0);  -- 72 bit

    -- 10GBASE-R serial layer connect
    serial_tx     : in  std_logic := 'X';
    serial_rx     : out std_logic;  -- connects to delayed serial_tx when g_loopback=TRUE else to serial_rx_in
    serial_tx_out : out std_logic;  -- connects to delayed serial_tx
    serial_rx_in  : in  std_logic := 'X';  -- used when g_loopback=FALSE

    -- XAUI serial layer connect
    xaui_tx       : in  std_logic_vector(c_nof_xaui_lanes - 1 downto 0) := (others => 'X');
    xaui_rx       : out std_logic_vector(c_nof_xaui_lanes - 1 downto 0);  -- connects to delayed xaui_tx when g_loopback=TRUE else to xaui_rx_in
    xaui_tx_out   : out std_logic_vector(c_nof_xaui_lanes - 1 downto 0);  -- connects to delayed xaui_tx
    xaui_rx_in    : in  std_logic_vector(c_nof_xaui_lanes - 1 downto 0) := (others => 'X')  -- used when g_loopback=FALSE
  );
end tb_tech_mac_10g_link_connect;

architecture tb of tb_tech_mac_10g_link_connect is
  signal serial_tx_dly   : std_logic;
  signal xaui_tx_dly     : std_logic_vector(c_nof_xaui_lanes - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- XGMII layer link
  -----------------------------------------------------------------------------
  xgmii_rx_data <= transport xgmii_tx_data after g_link_delay;

  -----------------------------------------------------------------------------
  -- 10GBASE-R serial layer link
  -----------------------------------------------------------------------------
  serial_tx_dly <= transport serial_tx after g_link_delay;
  serial_tx_out <= serial_tx_dly;

  p_serial_rx : process(serial_tx_dly, serial_rx_in, link_fault)
  begin
    if g_loopback = true then
      serial_rx <= serial_tx_dly;
    else
      serial_rx <= serial_rx_in;
    end if;
    if link_fault = '1' then
      serial_rx <= '0';
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- XAUI serial layer link
  -----------------------------------------------------------------------------
  xaui_tx_dly <= transport xaui_tx after g_link_delay;
  xaui_tx_out <= xaui_tx_dly;

  p_xaui_rx : process(xaui_tx_dly, xaui_rx_in, link_fault)
  begin
    if g_loopback = true then
      xaui_rx <= xaui_tx_dly;
    else
      xaui_rx <= xaui_rx_in;
    end if;
    if link_fault = '1' then
      xaui_rx <= (others => '0');
    end if;
  end process;
end tb;
