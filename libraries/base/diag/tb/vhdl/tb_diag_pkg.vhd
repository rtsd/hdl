--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Test bench package for diag library

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.diag_pkg.all;

package tb_diag_pkg is
  -- Test modes for diag seq
  type t_tb_diag_seq_mode_enum is (
    s_off,
    s_expect_ok,
    s_expect_error,
    s_expect_no_result
  );

  procedure proc_diag_seq_read_all(constant c_stream : in  natural;
                                   signal   mm_clk   : in  std_logic;
                                   signal   tx_miso  : in  t_mem_miso;
                                   signal   tx_mosi  : out t_mem_mosi;
                                   signal   rx_miso  : in  t_mem_miso;
                                   signal   rx_mosi  : out t_mem_mosi;
                                   signal   rd_reg   : out t_diag_seq_mm_reg);  -- read all MM reg

  procedure proc_diag_seq_tx_enable(constant c_stream  : in  natural;
                                    constant c_pattern : in  string;  -- "PSRG", "CNTR"
                                    constant c_tx_init : in  natural;
                                    constant c_tx_mod  : in  natural;
                                    signal   mm_clk    : in  std_logic;
                                    signal   dp_clk    : in  std_logic;
                                    signal   tx_miso   : in  t_mem_miso;  -- tx ctrl
                                    signal   tx_mosi   : out t_mem_mosi;
                                    signal   rx_miso   : in  t_mem_miso;  -- rx ctrl
                                    signal   rx_mosi   : out t_mem_mosi;
                                    signal   rd_reg    : out t_diag_seq_mm_reg);  -- read all MM reg

  procedure proc_diag_seq_rx_enable(constant c_stream  : in  natural;
                                    constant c_pattern : in  string;  -- "PSRG", "CNTR"
                                    signal   mm_clk    : in  std_logic;
                                    signal   dp_clk    : in  std_logic;
                                    signal   tx_miso   : in  t_mem_miso;  -- tx ctrl
                                    signal   tx_mosi   : out t_mem_mosi;
                                    signal   rx_miso   : in  t_mem_miso;  -- rx ctrl
                                    signal   rx_mosi   : out t_mem_mosi;
                                    signal   rd_reg    : out t_diag_seq_mm_reg);  -- read all MM reg

  procedure proc_diag_seq_tx_disable(constant c_stream : in  natural;
                                     signal   mm_clk   : in  std_logic;
                                     signal   dp_clk   : in  std_logic;
                                     signal   tx_miso  : in  t_mem_miso;  -- tx ctrl
                                     signal   tx_mosi  : out t_mem_mosi;
                                     signal   rx_miso  : in  t_mem_miso;  -- rx ctrl
                                     signal   rx_mosi  : out t_mem_mosi;
                                     signal   rd_reg   : out t_diag_seq_mm_reg);  -- read all MM reg

  procedure proc_diag_seq_rx_disable(constant c_stream : in  natural;
                                     signal   mm_clk   : in  std_logic;
                                     signal   dp_clk   : in  std_logic;
                                     signal   tx_miso  : in  t_mem_miso;  -- tx ctrl
                                     signal   tx_mosi  : out t_mem_mosi;
                                     signal   rx_miso  : in  t_mem_miso;  -- rx ctrl
                                     signal   rx_mosi  : out t_mem_mosi;
                                     signal   rd_reg   : out t_diag_seq_mm_reg);  -- read all MM reg

  procedure proc_diag_seq_rx_write_steps(constant c_stream    : in  natural;
                                         constant c_steps_arr : in  t_integer_arr(c_diag_seq_rx_reg_nof_steps - 1 downto 0);
                                         signal   mm_clk      : in  std_logic;
                                         signal   dp_clk      : in  std_logic;
                                         signal   tx_miso     : in  t_mem_miso;  -- tx ctrl
                                         signal   tx_mosi     : out t_mem_mosi;
                                         signal   rx_miso     : in  t_mem_miso;  -- rx ctrl
                                         signal   rx_mosi     : out t_mem_mosi;
                                         signal   rd_reg      : out t_diag_seq_mm_reg);  -- read all MM reg

  procedure proc_diag_seq_verify(constant c_stream  : in    natural;
                                 signal   mm_clk    : in    std_logic;
                                 signal   tx_miso   : in    t_mem_miso;
                                 signal   tx_mosi   : out   t_mem_mosi;
                                 signal   rx_miso   : in    t_mem_miso;
                                 signal   rx_mosi   : out   t_mem_mosi;
                                 signal   tb_mode   : inout t_tb_diag_seq_mode_enum;
                                 signal   tb_verify : out   std_logic;
                                 signal   rd_reg    : inout t_diag_seq_mm_reg);  -- read all MM reg

  -- Measure ADC/WG input power and determine effective sine amplitude
  procedure proc_diag_measure_cw_statistics(constant c_nof_samples : in natural;  -- number of samples per in_start interval
                                            signal dp_clk          : in std_logic;
                                            signal in_dat          : in std_logic_vector;
                                            signal in_start        : in std_logic;  -- start of interval, e.g. sop or sync
                                            signal in_val          : in std_logic;
                                            signal track_max       : inout real;  -- store local tracker in signal
                                            signal track_min       : inout real;  -- store local tracker in signal
                                            signal accum_mean      : inout real;  -- store local accumulator in signal
                                            signal accum_power     : inout real;  -- store local accumulator in signal
                                            signal measured_max    : out real;  -- maximum sample value
                                            signal measured_min    : out real;  -- minimum sample value
                                            signal measured_mean   : out real;  -- average sample value (DC)
                                            signal measured_power  : out real;  -- average sample power
                                            signal measured_ampl   : out real);  -- corresponding sine amplitude

  -- Measure ADC/WG amplitude and phase using local sin and cos
  procedure proc_diag_measure_cw_ampl_and_phase(constant c_nof_samples   : in natural;  -- number of samples per in_start interval
                                                constant c_fft_size      : in natural;  -- number of points of FFT
                                                constant c_sub           : in real;  -- subband index
                                                signal dp_clk            : in std_logic;
                                                signal in_dat            : in std_logic_vector;
                                                signal in_start          : in std_logic;  -- start of integration interval, e.g. sop or sync
                                                signal in_val            : in std_logic;
                                                signal in_cnt            : in natural;  -- sample index in c_fft_size
                                                signal ref_I             : out real;  -- output local I as signal for debugging in wave window
                                                signal ref_Q             : out real;  -- output local Q as signal for debugging in wave window
                                                signal accum_I           : inout real;  -- store local I accumulator in signal
                                                signal accum_Q           : inout real;  -- store local Q accumulator in signal
                                                signal measured_ampl     : out real;  -- measured CW amplitude
                                                signal measured_phase    : out real;  -- measured CW phase in radials
                                                signal measured_phase_Ts : out real);  -- measured CW phase in sample periods

  procedure proc_diag_measure_cw_ampl_and_phase(constant c_fft_size      : in natural;  -- number of points of FFT = number of samples per in_start interval
                                                constant c_sub           : in real;
                                                signal dp_clk            : in std_logic;
                                                signal in_dat            : in std_logic_vector;
                                                signal in_start          : in std_logic;
                                                signal in_val            : in std_logic;
                                                signal in_cnt            : in natural;
                                                signal ref_I             : out real;
                                                signal ref_Q             : out real;
                                                signal accum_I           : inout real;
                                                signal accum_Q           : inout real;
                                                signal measured_ampl     : out real;
                                                signal measured_phase    : out real;
                                                signal measured_phase_Ts : out real);

  -- Use estimated CW to determine noise power in input sine (e.g. WG sine)
  procedure proc_diag_measure_cw_noise_power(constant c_nof_samples      : in natural;  -- number of samples per integration interval
                                             constant c_fft_size         : in natural;  -- number of points of FFT
                                             constant c_sub              : in real;  -- subband index
                                             signal dp_clk               : in std_logic;
                                             signal in_dat               : in std_logic_vector;
                                             signal in_start             : in std_logic;  -- start of integration interval, e.g. sop or sync
                                             signal in_val               : in std_logic;
                                             signal in_cnt               : in natural;  -- sample index in c_fft_size
                                             signal cw_ampl              : in real;  -- estimated CW amplitude of in_dat
                                             signal cw_phase             : in real;  -- estimated CW phase of in_dat
                                             signal cw_dat               : out integer;  -- estimated CW
                                             signal cw_noise             : out real;  -- estimated CW quantization noise
                                             signal accum_noise_power    : inout real;  -- store noise power accumulator in signal
                                             signal measured_noise_power : out real);  -- measured noise power in in_dat

  procedure proc_diag_measure_cw_noise_power(constant c_fft_size         : in natural;  -- number of points of FFT = number of samples per in_start interval
                                             constant c_sub              : in real;
                                             signal dp_clk               : in std_logic;
                                             signal in_dat               : in std_logic_vector;
                                             signal in_start             : in std_logic;
                                             signal in_val               : in std_logic;
                                             signal in_cnt               : in natural;
                                             signal cw_ampl              : in real;
                                             signal cw_phase             : in real;
                                             signal cw_dat               : out integer;
                                             signal cw_noise             : out real;
                                             signal accum_noise_power    : inout real;
                                             signal measured_noise_power : out real);
end tb_diag_pkg;

package body tb_diag_pkg is
  procedure proc_diag_seq_read_all(constant c_stream : in  natural;
                                   signal   mm_clk   : in  std_logic;
                                   signal   tx_miso  : in  t_mem_miso;
                                   signal   tx_mosi  : out t_mem_mosi;
                                   signal   rx_miso  : in  t_mem_miso;
                                   signal   rx_mosi  : out t_mem_mosi;
                                   signal   rd_reg   : out t_diag_seq_mm_reg) is  -- read all MM reg
    constant c_tx_offset : natural := c_stream * 2**c_diag_seq_tx_reg_adr_w;
    constant c_rx_offset : natural := c_stream * 2**c_diag_seq_rx_reg_adr_w;
  begin
    ---------------------------------------------------------------------------
    -- Readback ctrl
    ---------------------------------------------------------------------------
    -- . read back Tx data init
    proc_mem_mm_bus_rd(c_tx_offset + 1, mm_clk, tx_miso, tx_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_reg.tx_init <= tx_miso.rddata(c_word_w - 1 downto 0);
    -- . read back Tx data modulo
    proc_mem_mm_bus_rd(c_tx_offset + 3, mm_clk, tx_miso, tx_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_reg.tx_mod <= tx_miso.rddata(c_word_w - 1 downto 0);
    -- . read back Tx control
    proc_mem_mm_bus_rd(c_tx_offset + 0, mm_clk, tx_miso, tx_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_reg.tx_ctrl <= tx_miso.rddata(c_word_w - 1 downto 0);
    -- . read back Rx control
    proc_mem_mm_bus_rd(c_rx_offset + 0, mm_clk, rx_miso, rx_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_reg.rx_ctrl <= rx_miso.rddata(c_word_w - 1 downto 0);

    ---------------------------------------------------------------------------
    -- Readback rx_steps
    ---------------------------------------------------------------------------
    for I in 0 to c_diag_seq_rx_reg_nof_steps - 1 loop
      proc_mem_mm_bus_rd(c_rx_offset + c_diag_seq_rx_reg_nof_steps_wi + I, mm_clk, rx_miso, rx_mosi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      rd_reg.rx_steps(I) <= TO_SINT(rx_miso.rddata(c_word_w - 1 downto 0));
    end loop;

    ---------------------------------------------------------------------------
    -- Read cnt and stat
    ---------------------------------------------------------------------------
    -- . read rx_stat
    proc_mem_mm_bus_rd(c_rx_offset + 1, mm_clk, rx_miso, rx_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_reg.rx_stat <= rx_miso.rddata(c_word_w - 1 downto 0);
    -- . read rx_cnt
    proc_mem_mm_bus_rd(c_rx_offset + 2, mm_clk, rx_miso, rx_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_reg.rx_cnt <= rx_miso.rddata(c_word_w - 1 downto 0);
    -- . read tx_cnt
    proc_mem_mm_bus_rd(c_tx_offset + 2, mm_clk, tx_miso, tx_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_reg.tx_cnt <= tx_miso.rddata(c_word_w - 1 downto 0);
    -- . read rx_sample
    proc_mem_mm_bus_rd(c_rx_offset + 3, mm_clk, rx_miso, rx_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_reg.rx_sample <= rx_miso.rddata(c_word_w - 1 downto 0);
  end proc_diag_seq_read_all;

  procedure proc_diag_seq_tx_enable(constant c_stream  : in  natural;
                                    constant c_pattern : in  string;  -- "PSRG", "CNTR"
                                    constant c_tx_init : in  natural;
                                    constant c_tx_mod  : in  natural;
                                    signal   mm_clk    : in  std_logic;
                                    signal   dp_clk    : in  std_logic;
                                    signal   tx_miso   : in  t_mem_miso;  -- tx ctrl
                                    signal   tx_mosi   : out t_mem_mosi;
                                    signal   rx_miso   : in  t_mem_miso;  -- rx ctrl
                                    signal   rx_mosi   : out t_mem_mosi;
                                    signal   rd_reg    : out t_diag_seq_mm_reg) is  -- read all MM reg
    constant c_tx_offset : natural := c_stream * 2**c_diag_seq_tx_reg_adr_w;
    constant c_en        : natural := 1;
    variable v_sel       : natural;
    variable v_ctlr      : natural;
  begin
    if c_pattern = "PSRG" then
      v_sel  := 0;  -- pseudo random data
    else
      v_sel  := 1;  -- counter data
    end if;
    v_ctlr := v_sel * 2 + c_en;  -- bits [1:0]
    -- Enable Tx
    proc_mem_mm_bus_wr(c_tx_offset + 0, v_ctlr, mm_clk, tx_miso, tx_mosi);
    proc_mem_mm_bus_wr(c_tx_offset + 1, c_tx_init, mm_clk, tx_miso, tx_mosi);
    proc_mem_mm_bus_wr(c_tx_offset + 3, c_tx_mod, mm_clk, tx_miso, tx_mosi);
    proc_common_wait_some_cycles(mm_clk, dp_clk, 10);  -- wait for clock domain crossing
    proc_diag_seq_read_all(c_stream, mm_clk, tx_miso, tx_mosi, rx_miso, rx_mosi, rd_reg);
  end proc_diag_seq_tx_enable;

  procedure proc_diag_seq_rx_enable(constant c_stream  : in  natural;
                                    constant c_pattern : in  string;  -- "PSRG", "CNTR"
                                    signal   mm_clk    : in  std_logic;
                                    signal   dp_clk    : in  std_logic;
                                    signal   tx_miso   : in  t_mem_miso;  -- tx ctrl
                                    signal   tx_mosi   : out t_mem_mosi;
                                    signal   rx_miso   : in  t_mem_miso;  -- rx ctrl
                                    signal   rx_mosi   : out t_mem_mosi;
                                    signal   rd_reg    : out t_diag_seq_mm_reg) is  -- read all MM reg
    constant c_rx_offset : natural := c_stream * 2**c_diag_seq_rx_reg_adr_w;
    constant c_en        : natural := 1;
    variable v_sel       : natural;
    variable v_ctlr      : natural;
  begin
    if c_pattern = "PSRG" then
      v_sel  := 0;  -- pseudo random data
    else
      v_sel  := 1;  -- counter data
    end if;
    v_ctlr := v_sel * 2 + c_en;  -- bits [1:0]
    proc_mem_mm_bus_wr(c_rx_offset + 0, v_ctlr, mm_clk, rx_miso, rx_mosi);
    proc_common_wait_some_cycles(mm_clk, dp_clk, 10);  -- wait for clock domain crossing
    proc_diag_seq_read_all(c_stream, mm_clk, tx_miso, tx_mosi, rx_miso, rx_mosi, rd_reg);
  end proc_diag_seq_rx_enable;

  procedure proc_diag_seq_tx_disable(constant c_stream : in  natural;
                                     signal   mm_clk   : in  std_logic;
                                     signal   dp_clk   : in  std_logic;
                                     signal   tx_miso  : in  t_mem_miso;  -- tx ctrl
                                     signal   tx_mosi  : out t_mem_mosi;
                                     signal   rx_miso  : in  t_mem_miso;  -- rx ctrl
                                     signal   rx_mosi  : out t_mem_mosi;
                                     signal   rd_reg   : out t_diag_seq_mm_reg) is  -- read all MM reg
    constant c_tx_offset : natural := c_stream * 2**c_diag_seq_tx_reg_adr_w;
  begin
    proc_mem_mm_bus_wr(c_tx_offset + 0, 0, mm_clk, tx_miso, tx_mosi);
    proc_common_wait_some_cycles(mm_clk, dp_clk, 10);  -- wait for clock domain crossing
    proc_diag_seq_read_all(c_stream, mm_clk, tx_miso, tx_mosi, rx_miso, rx_mosi, rd_reg);
  end proc_diag_seq_tx_disable;

  procedure proc_diag_seq_rx_disable(constant c_stream : in  natural;
                                     signal   mm_clk   : in  std_logic;
                                     signal   dp_clk   : in  std_logic;
                                     signal   tx_miso  : in  t_mem_miso;  -- tx ctrl
                                     signal   tx_mosi  : out t_mem_mosi;
                                     signal   rx_miso  : in  t_mem_miso;  -- rx ctrl
                                     signal   rx_mosi  : out t_mem_mosi;
                                     signal   rd_reg   : out t_diag_seq_mm_reg) is  -- read all MM reg
    constant c_rx_offset : natural := c_stream * 2**c_diag_seq_rx_reg_adr_w;
  begin
    proc_mem_mm_bus_wr(c_rx_offset + 0, 0, mm_clk, rx_miso, rx_mosi);
    proc_common_wait_some_cycles(mm_clk, dp_clk, 10);  -- wait for clock domain crossing
    proc_diag_seq_read_all(c_stream, mm_clk, tx_miso, tx_mosi, rx_miso, rx_mosi, rd_reg);
  end proc_diag_seq_rx_disable;

  procedure proc_diag_seq_rx_write_steps(constant c_stream    : in  natural;
                                         constant c_steps_arr : in  t_integer_arr(c_diag_seq_rx_reg_nof_steps - 1 downto 0);
                                         signal   mm_clk      : in  std_logic;
                                         signal   dp_clk      : in  std_logic;
                                         signal   tx_miso     : in  t_mem_miso;  -- tx ctrl
                                         signal   tx_mosi     : out t_mem_mosi;
                                         signal   rx_miso     : in  t_mem_miso;  -- rx ctrl
                                         signal   rx_mosi     : out t_mem_mosi;
                                         signal   rd_reg      : out t_diag_seq_mm_reg) is  -- read all MM reg
    constant c_rx_offset : natural := c_stream * 2**c_diag_seq_rx_reg_adr_w;
    constant c_en        : natural := 1;
    variable v_sel       : natural;
    variable v_ctlr      : natural;
  begin
    -- Write rx_steps
    for I in 0 to c_diag_seq_rx_reg_nof_steps - 1 loop
      proc_mem_mm_bus_wr(c_rx_offset + c_diag_seq_rx_reg_nof_steps_wi + I, c_steps_arr(I), mm_clk, rx_miso, rx_mosi);
    end loop;
    proc_common_wait_some_cycles(mm_clk, dp_clk, 10);  -- wait for clock domain crossing
    proc_diag_seq_read_all(c_stream, mm_clk, tx_miso, tx_mosi, rx_miso, rx_mosi, rd_reg);
  end proc_diag_seq_rx_write_steps;

  procedure proc_diag_seq_verify(constant c_stream  : in    natural;
                                 signal   mm_clk    : in    std_logic;
                                 signal   tx_miso   : in    t_mem_miso;
                                 signal   tx_mosi   : out   t_mem_mosi;
                                 signal   rx_miso   : in    t_mem_miso;
                                 signal   rx_mosi   : out   t_mem_mosi;
                                 signal   tb_mode   : inout t_tb_diag_seq_mode_enum;
                                 signal   tb_verify : out   std_logic;
                                 signal   rd_reg    : inout t_diag_seq_mm_reg) is  -- read all MM reg
   variable v_rx_stat   : std_logic_vector(c_word_w - 1 downto 0);
   variable v_rx_sample : std_logic_vector(c_word_w - 1 downto 0);
   variable v_rx_cnt    : natural;
   variable v_tx_cnt    : natural;
  begin
    -- Read all
    proc_diag_seq_read_all(c_stream, mm_clk, tx_miso, tx_mosi, rx_miso, rx_mosi, rd_reg);
    proc_common_wait_some_cycles(mm_clk, 1);
    v_rx_stat   := rd_reg.rx_stat;
    v_rx_sample := rd_reg.rx_sample;
    v_rx_cnt    := TO_UINT(rd_reg.rx_cnt);
    v_tx_cnt    := TO_UINT(rd_reg.tx_cnt);

    -- Issue tb_verify pulse eg. to easy recognition in Wave window
    tb_verify <= '1';
    proc_common_wait_some_cycles(mm_clk, 1);
    tb_verify <= '0';

    -- Verify
    if tb_mode = s_expect_ok then
      if v_rx_stat(1) /= '0' then
        report "Wrong diag result: no valid result."
          severity ERROR;
      elsif v_rx_stat(0) /= '0' then
        report "Wrong diag result: one or more data errors."
          severity ERROR;
      end if;

      -- Read rx_cnt and tx_cnt again after some cycles
      proc_diag_seq_read_all(c_stream, mm_clk, tx_miso, tx_mosi, rx_miso, rx_mosi, rd_reg);
      proc_common_wait_some_cycles(mm_clk, 1);
      if v_rx_cnt = 0 then
        report "Wrong diag result: rx_cnt = 0."
          severity ERROR;
      elsif v_rx_cnt >= TO_UINT(rd_reg.rx_cnt) then
        report "Wrong diag result: rx_cnt did not increase."
          severity ERROR;
      end if;
      if v_tx_cnt = 0 then
        report "Wrong diag result: tx_cnt = 0."
          severity ERROR;
      elsif v_tx_cnt >= TO_UINT(rd_reg.tx_cnt) then
        report "Wrong diag result: tx_cnt did not increase."
          severity ERROR;
      end if;
      -- Read rx_sample again after some cycles
      if unsigned(v_rx_sample) = 0 then
        report "Wrong diag sample: no valid sample."
          severity ERROR;
      elsif v_rx_sample = rd_reg.rx_sample then
        report "Wrong diag sample: sample did not change."
          severity ERROR;
      end if;
    elsif tb_mode = s_expect_error then
      if v_rx_stat(1) /= '0' then
        report "Wrong diag result: no valid result."
          severity ERROR;
      elsif v_rx_stat(0) /= '1' then
        report "Wrong diag result: must detect data errors."
          severity ERROR;
      end if;
      if v_rx_cnt = 0 then
        report "Wrong diag result: rx_cnt = 0."
          severity ERROR;
      end if;
      if v_tx_cnt = 0 then
        report "Wrong diag result: tx_cnt = 0."
          severity ERROR;
      end if;
    elsif tb_mode = s_expect_no_result then
      if v_rx_stat(1) /= '1' then
        report "Wrong diag result: must indicate no valid result."
          severity ERROR;
      end if;
      if v_rx_cnt /= 0 then
        report "Wrong diag result: rx_cnt /= 0."
          severity ERROR;
      end if;
      if unsigned(v_rx_sample) /= 0 then
        report "Wrong diag sample: rx_sample /= 0."
          severity ERROR;
      end if;
    elsif tb_mode = s_off then
      if v_rx_cnt /= 0 then
        report "Wrong diag result: rx_cnt /= 0."
          severity ERROR;
      end if;
      if v_tx_cnt /= 0 then
        report "Wrong diag result: tx_cnt /= 0."
          severity ERROR;
      end if;
      if unsigned(v_rx_sample) /= 0 then
        report "Wrong diag sample: rx_sample /= 0."
          severity ERROR;
      end if;
    else
      report "Unknown verify mode"
        severity FAILURE;
    end if;
  end proc_diag_seq_verify;

  ---------------------------------------------------------------------------
  -- PROCEDURE proc_diag_measure_cw_statistics()
  -- Purpose: Measure ADC/WG input power and determine effective sine amplitude
  -- Description:
  -- . ast = ADC statistics
  -- . assume integer subband number, so there are integer subband number of sine periods per c_N_fft
  --   input samples interval. Hence DC = 0 over each block of c_N_fft input samples, so no need to
  --   measure DC.
  -- . accumulate samples during interval and calculate effective amplitude.
  ---------------------------------------------------------------------------
  procedure proc_diag_measure_cw_statistics(constant c_nof_samples : in natural;  -- number of samples per in_start interval
                                            signal dp_clk          : in std_logic;
                                            signal in_dat          : in std_logic_vector;
                                            signal in_start        : in std_logic;  -- start of interval, e.g. sop or sync
                                            signal in_val          : in std_logic;
                                            signal track_max       : inout real;  -- store local tracker in signal
                                            signal track_min       : inout real;  -- store local tracker in signal
                                            signal accum_mean      : inout real;  -- store local accumulator in signal
                                            signal accum_power     : inout real;  -- store local accumulator in signal
                                            signal measured_max    : out real;  -- maximum sample value
                                            signal measured_min    : out real;  -- minimum sample value
                                            signal measured_mean   : out real;  -- average sample value (DC)
                                            signal measured_power  : out real;  -- average sample power
                                            signal measured_ampl   : out real) is  -- corresponding sine amplitude
    constant c_Nsamples : real := real(c_nof_samples);
    constant c_dat      : real := real(TO_SINT(in_dat));
    constant c_mean     : real := accum_mean / c_Nsamples;
    constant c_power    : real := accum_power / c_Nsamples;
    constant c_ampl     : real := SQRT(c_power * 2.0);
  begin
    if rising_edge(dp_clk) then
      if in_start = '1' then
        -- Hold last interval max, min and accumulation
        measured_max   <= track_max;
        measured_min   <= track_min;
        measured_power <= c_power;
        measured_ampl  <= c_ampl;
        -- Start new interval accumulation
        track_max <= real'low;
        track_min <= real'high;
        accum_mean <= c_dat;
        accum_power <= (abs(c_dat))**2.0;  -- Must use ABS() with ** of real, because (negative)**2.0 yields error and value 0.0, also must use brackets (ABS()) to avoid compile error
      elsif in_val = '1' then
        -- Detect and accumulate during interval
        track_max <= largest(track_max, c_dat);
        track_min <= smallest(track_min, c_dat);
        accum_mean <= accum_mean + c_dat;
        accum_power <= accum_power + (abs(c_dat))**2.0;
      end if;
    end if;
  end proc_diag_measure_cw_statistics;

  ---------------------------------------------------------------------------
  -- PROCEDURE proc_diag_measure_cw_ampl_and_phase()
  -- Purpose: Measure ADC/WG amplitude and phase using local sin and cos
  -- Description:
  -- * similar as done by add_clock_cw_statistics() in https://git.astron.nl/rtsd/upe_gear/-/blob/master/base/ADC_functions.py
  -- * Measure amplitude and phase of WG
  --   . assume integer subband number, so there are integer subband number of sine periods per c_Nsamples input samples.
  --     Hence DC = 0 over each block of c_Nsamples input samples, so no need to measure DC.
  --   . the integration interval is marked by in_start and has c_Nsamples input samples. If c_Nsamples = c_Nfft, then
  --     use in_start = sosi.sop, else if c_Nsamples > c_Nfft then use in_start = sosi.sync. Using sosi.sync allows
  --     using fractional sub subband index. Using sosi.sop requires using integer sub index, to avoid DC during the
  --     integration interval, but simulates faster.
  --   . time t in sample clock periods t = n * Ts, in simulation use Ts = 1, so unit sample clock period.
  --   . WG period is T = (c_Nfft / sub) * Ts, WG radial frequency is omega = 2*pi/T = 2*pi*sub/c_Nfft for Ts = 1,
  --     phase is phi and amplitude is A
  --
  --       WG(t) = A*sin(omega * t + phi) --> WG(n) = A*sin(omega * n + phi) for Ts = 1
  --
  --   . Create local sin and cos, so:
  --
  --       I(t) = sin(omega*t) --> I(n) = sin(n * 2*pi*sub/c_Nfft),
  --       Q(t) = cos(omega*t) --> Q(n) = cos(n * 2*pi*sub/c_Nfft)
  --
  --   . multiply the WG(t) by I(t) = sin(omega*t) and by Q(t) = cos(omega*t), and sum over c_Nsamples input samples, so
  --     over an integer number of periods. Using gonio formula for sin*sin and cos*sin and omitting the term with
  --     2*omega, because it cancels in the accumulation over an ineteger number of periods, yields:
  --
  --       accum_I = c_Nsamples * A/2 * cos(phi)
  --       accum_Q = c_Nsamples * A/2 * sin(phi)
  --
  --     ==>
  --
  --       phi = atan2(accum_Q, accum_I)  [rad]  --> * 1/omega to get it in sample period units
  --       A   = 2/c_Nsamples * sqrt(accum_I**2 + accum_Q**2)
  --
  --   . the sine power of the perfect reference CW (carrier wave) is:
  --       cwPower = (A**2)/2
  ---------------------------------------------------------------------------
  procedure proc_diag_measure_cw_ampl_and_phase(constant c_nof_samples   : in natural;  -- number of samples per in_start interval
                                                constant c_fft_size      : in natural;  -- number of points of FFT
                                                constant c_sub           : in real;  -- subband index
                                                signal dp_clk            : in std_logic;
                                                signal in_dat            : in std_logic_vector;
                                                signal in_start          : in std_logic;  -- start of integration interval, e.g. sop or sync
                                                signal in_val            : in std_logic;
                                                signal in_cnt            : in natural;  -- sample index in c_fft_size
                                                signal ref_I             : out real;  -- output local I as signal for debugging in wave window
                                                signal ref_Q             : out real;  -- output local Q as signal for debugging in wave window
                                                signal accum_I           : inout real;  -- store local I accumulator in signal
                                                signal accum_Q           : inout real;  -- store local Q accumulator in signal
                                                signal measured_ampl     : out real;  -- measured CW amplitude
                                                signal measured_phase    : out real;  -- measured CW phase in radials
                                                signal measured_phase_Ts : out real) is  -- measured CW phase in sample periods
    constant c_Nsamples    : real := real(c_nof_samples);
    constant c_Nfft        : real := real(c_fft_size);
    constant c_omega : real := MATH_2_PI * c_sub / c_Nfft;
    constant c_lat   : real := 1.0 * c_omega;  -- the I and Q start when in_val='1', this introduces 1 dp_clk latency, so adjust phase of ref_I and ref_Q to compensate
    constant c_k     : real := real(in_cnt);
    constant c_angle : real := (c_k * c_omega + c_lat) mod MATH_2_PI;  -- keep angle in SIN(), COS() within 2pi to avoid "Error: XLOCAL <= 0.0 after reduction in COS(X)"
    constant c_I     : real := SIN(c_angle);  -- I = in phase reference
    constant c_Q     : real := COS(c_angle);  -- Q = quadrature reference
    constant c_dat   : real := real(TO_SINT(in_dat));
    constant c_phase : real := ARCTAN(accum_Q, accum_I + c_eps);
    constant c_ampl  : real := COMPLEX_RADIUS(accum_I, accum_Q) * 2.0 / c_Nsamples;
  begin
    if rising_edge(dp_clk) then
      -- Output reference I and Q for debugging in wave window
      if in_val = '1' then
        ref_I <= c_I;
        ref_Q <= c_Q;
      end if;
      if in_start = '1' then
        -- Derive amplitude and phase from last interval accumulations
        measured_ampl <= c_ampl;
        measured_phase <= c_phase;
        measured_phase_Ts <= c_phase / c_omega;
        -- Start new interval accumulation
        accum_I <= c_dat * c_I;
        accum_Q <= c_dat * c_Q;
      elsif in_val = '1' then
        -- Accumulate during interval
        accum_I <= accum_I + c_dat * c_I;
        accum_Q <= accum_Q + c_dat * c_Q;
      end if;
    end if;
  end proc_diag_measure_cw_ampl_and_phase;

  procedure proc_diag_measure_cw_ampl_and_phase(constant c_fft_size      : in natural;  -- number of points of FFT = number of samples per in_start interval
                                                constant c_sub           : in real;
                                                signal dp_clk            : in std_logic;
                                                signal in_dat            : in std_logic_vector;
                                                signal in_start          : in std_logic;
                                                signal in_val            : in std_logic;
                                                signal in_cnt            : in natural;
                                                signal ref_I             : out real;
                                                signal ref_Q             : out real;
                                                signal accum_I           : inout real;
                                                signal accum_Q           : inout real;
                                                signal measured_ampl     : out real;
                                                signal measured_phase    : out real;
                                                signal measured_phase_Ts : out real) is
  begin
    proc_diag_measure_cw_ampl_and_phase(c_fft_size, c_fft_size, c_sub, dp_clk,
                                        in_dat, in_start, in_val, in_cnt,
                                        ref_I, ref_Q, accum_I, accum_Q,
                                        measured_ampl, measured_phase, measured_phase_Ts);
  end proc_diag_measure_cw_ampl_and_phase;

  ---------------------------------------------------------------------------
  -- PROCEDURE proc_diag_measure_cw_noise_power()
  -- Purpose: Use estimated CW to determine noise power in input sine (e.g. WG sine)
  -- Description:
  -- * Use cw_ampl and cw_phase obtained by proc_diag_measure_cw_noise_power().
  -- * Measure quantization noise power in WG. Use measured A and phi of WG as perfect reference CW (carrier wave)
  --   to measure the noise power in the WG sine in in_dat:
  --
  --     sumNoisePower = sum[ (CW(t) - WG(t))**2 ]
  --     noisePower = sumNoisePower / c_Nsamples
  --
  -- * Externally determine the signal to noise ratio (SNR) of the WG sine:
  --   . The theortical SNR of the WG sine with ADC quantization noise power of 1/12 is:
  --
  --       SNR = 20*log10((ampl**2)/2 * 12) [dB]
  --
  --   . The measured SNR of the WG follows from:
  --
  --       SNR = 10*log10(cwPower / noisePower) [dB]
  ---------------------------------------------------------------------------
  procedure proc_diag_measure_cw_noise_power(constant c_nof_samples      : in natural;  -- number of samples per integration interval
                                             constant c_fft_size         : in natural;  -- number of points of FFT
                                             constant c_sub              : in real;  -- subband index
                                             signal dp_clk               : in std_logic;
                                             signal in_dat               : in std_logic_vector;
                                             signal in_start             : in std_logic;  -- start of integration interval, e.g. sop or sync
                                             signal in_val               : in std_logic;
                                             signal in_cnt               : in natural;  -- sample index in c_fft_size
                                             signal cw_ampl              : in real;  -- estimated CW amplitude of in_dat
                                             signal cw_phase             : in real;  -- estimated CW phase of in_dat
                                             signal cw_dat               : out integer;  -- estimated CW
                                             signal cw_noise             : out real;  -- estimated CW quantization noise
                                             signal accum_noise_power    : inout real;  -- store noise power accumulator in signal
                                             signal measured_noise_power : out real) is  -- measured noise power in in_dat
    constant c_Nsamples    : real := real(c_nof_samples);
    constant c_Nfft        : real := real(c_fft_size);
    constant c_omega       : real := MATH_2_PI * c_sub / c_Nfft;
    constant c_lat         : real := 1.0 * c_omega;  -- adjust phase of c_cw to compensate for measurement latency
    constant c_k           : real := real(in_cnt);
    constant c_dat         : real := real(TO_SINT(in_dat));
    constant c_angle       : real := (c_k * c_omega + cw_phase + c_lat) mod MATH_2_PI;  -- keep angle in SIN(), COS() within 2pi to avoid "Error: XLOCAL <= 0.0 after reduction in COS(X)"
    constant c_cw          : real := cw_ampl * SIN(c_angle);
    constant c_noise       : real := c_cw - c_dat;
    constant c_noise_power : real := (abs(c_noise))**2.0;
  begin
    if rising_edge(dp_clk) then
      if in_val = '1' then
        cw_dat <= integer(c_cw);
        cw_noise <= c_noise;
      end if;
      if in_start = '1' then
        -- Derive noise power from last interval accumulations
        measured_noise_power <= accum_noise_power / c_Nsamples;
        -- Start new interval accumulation
        accum_noise_power <= c_noise_power;
      elsif in_val = '1' then
        -- Accumulate during interval
        accum_noise_power <= accum_noise_power + c_noise_power;
      end if;
    end if;
  end proc_diag_measure_cw_noise_power;

  procedure proc_diag_measure_cw_noise_power(constant c_fft_size         : in natural;  -- number of points of FFT = number of samples per in_start interval
                                             constant c_sub              : in real;
                                             signal dp_clk               : in std_logic;
                                             signal in_dat               : in std_logic_vector;
                                             signal in_start             : in std_logic;
                                             signal in_val               : in std_logic;
                                             signal in_cnt               : in natural;
                                             signal cw_ampl              : in real;
                                             signal cw_phase             : in real;
                                             signal cw_dat               : out integer;
                                             signal cw_noise             : out real;
                                             signal accum_noise_power    : inout real;
                                             signal measured_noise_power : out real) is
  begin
    proc_diag_measure_cw_noise_power(c_fft_size, c_fft_size, c_sub, dp_clk,
                                     in_dat, in_start, in_val, in_cnt,
                                     cw_ampl, cw_phase, cw_dat, cw_noise,
                                     accum_noise_power, measured_noise_power);
  end proc_diag_measure_cw_noise_power;
end tb_diag_pkg;
