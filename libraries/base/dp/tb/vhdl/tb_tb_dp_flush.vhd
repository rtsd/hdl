-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_flush
-- Description:
-- Usage:
-- > as 2
-- > run -all

entity tb_tb_dp_flush is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
end tb_tb_dp_flush;

architecture tb of tb_tb_dp_flush is
  constant c_nof_repeat : natural := 100;
begin
  --                                              g_rl, g_framed_xon, g_framed_xoff , g_in_en, g_out_ready, g_nof_repeat
  u_0_frm_frm_act_act : entity work.tb_dp_flush generic map (0, true,  true,  e_active, e_active, c_nof_repeat);
  u_0_frm_frm_rnd_rnd : entity work.tb_dp_flush generic map (0, true,  true,  e_random, e_random, c_nof_repeat);
  u_1_frm_frm_act_act : entity work.tb_dp_flush generic map (1, true,  true,  e_active, e_active, c_nof_repeat);
  u_1_frm_frm_rnd_rnd : entity work.tb_dp_flush generic map (1, true,  true,  e_random, e_random, c_nof_repeat);

  u_0_str_str_act_act : entity work.tb_dp_flush generic map (0, false, false, e_active, e_active, c_nof_repeat);
  u_0_str_str_rnd_rnd : entity work.tb_dp_flush generic map (0, false, false, e_random, e_random, c_nof_repeat);
  u_1_str_str_act_act : entity work.tb_dp_flush generic map (1, false, false, e_active, e_active, c_nof_repeat);
  u_1_str_str_rnd_rnd : entity work.tb_dp_flush generic map (1, false, false, e_random, e_random, c_nof_repeat);

  u_0_frm_str_act_act : entity work.tb_dp_flush generic map (0, true,  false, e_active, e_active, c_nof_repeat);
  u_0_frm_str_rnd_rnd : entity work.tb_dp_flush generic map (0, true,  false, e_random, e_random, c_nof_repeat);
  u_1_frm_str_act_act : entity work.tb_dp_flush generic map (1, true,  false, e_active, e_active, c_nof_repeat);
  u_1_frm_str_rnd_rnd : entity work.tb_dp_flush generic map (1, true,  false, e_random, e_random, c_nof_repeat);

  u_0_str_frm_act_act : entity work.tb_dp_flush generic map (0, false, true,  e_active, e_active, c_nof_repeat);
  u_0_str_frm_rnd_rnd : entity work.tb_dp_flush generic map (0, false, true,  e_random, e_random, c_nof_repeat);
  u_1_str_frm_act_act : entity work.tb_dp_flush generic map (1, false, true,  e_active, e_active, c_nof_repeat);
  u_1_str_frm_rnd_rnd : entity work.tb_dp_flush generic map (1, false, true,  e_random, e_random, c_nof_repeat);
end tb;
