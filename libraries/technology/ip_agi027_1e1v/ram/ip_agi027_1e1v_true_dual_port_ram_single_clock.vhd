-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   D.F. Brouwer
-- Purpose:
--   RadioHDL wrapper
-- Description:
--   Quartus II VHDL Template
--   True Dual-Port RAM with single clock
--   Read-during-write on port A or B returns newly written data
--   Read-during-write on port A and B returns unknown data.
-- Reference:
--   Copied from ip_arria10_e2sg/ip_arria10_e2sg_true_dual_port_ram_dual_clock.vhd
--   and the inferred Altera code was obtained using template insert with
--   Quartus 14.0a10.
-- Remark:
--   The orignal file is for True Dual-Port RAM with dual clock. However, that is
--   not supported by the Agilex 7 in the way it is used before Agilex 7.

library ieee;
use ieee.std_logic_1164.all;

entity ip_agi027_1e1v_true_dual_port_ram_single_clock is
  generic
  (
    DATA_WIDTH : natural := 8;
    ADDR_WIDTH : natural := 6
  );
  port
  (
    clk   : in std_logic;
    addr_a  : in natural range 0 to 2**ADDR_WIDTH - 1;
    addr_b  : in natural range 0 to 2**ADDR_WIDTH - 1;
    data_a  : in std_logic_vector((DATA_WIDTH - 1) downto 0);
    data_b  : in std_logic_vector((DATA_WIDTH - 1) downto 0);
    we_a  : in std_logic := '1';
    we_b  : in std_logic := '1';
    q_a   : out std_logic_vector((DATA_WIDTH - 1) downto 0);
    q_b   : out std_logic_vector((DATA_WIDTH - 1) downto 0)
  );
end ip_agi027_1e1v_true_dual_port_ram_single_clock;

architecture rtl of ip_agi027_1e1v_true_dual_port_ram_single_clock is
  -- Build a 2-D array type for the RAM
  subtype word_t is std_logic_vector((DATA_WIDTH - 1) downto 0);
  type memory_t is array(2**ADDR_WIDTH - 1 downto 0) of word_t;

  -- Declare the RAM
  shared variable ram : memory_t;
begin
  process(clk)
  begin
  if(rising_edge(clk)) then
    -- Port A
    if(we_a = '1') then
      ram(addr_a) := data_a;
    end if;
    q_a <= ram(addr_a);
    -- Port B
    if(we_b = '1') then
      ram(addr_b) := data_b;
    end if;
    q_b <= ram(addr_b);
  end if;
  end process;
end rtl;
