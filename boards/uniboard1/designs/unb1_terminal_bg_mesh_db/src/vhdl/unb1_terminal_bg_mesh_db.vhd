------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, dp_lib, eth_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use dp_lib.dp_stream_pkg.all;
use eth_lib.eth_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;

entity unb1_terminal_bg_mesh_db is
  generic (
    -- General
    g_design_name   : string  := "unb1_terminal_bg_mesh_db";
    g_design_note   : string  := "UNUSED";
    g_sim           : boolean := false;  -- Overridden by TB
    g_sim_level     : natural := 0;
    g_sim_unb_nr    : natural := 0;
    g_sim_node_nr   : natural := 0;
    g_stamp_date    : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time    : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn     : natural := 0  -- SVN revision    -- set by QSF
  );
  port (
   -- GENERAL
    CLK                    : in    std_logic;  -- System Clock
    PPS                    : in    std_logic;  -- System Sync
    WDI                    : out   std_logic;  -- Watchdog Clear
    INTA                   : inout std_logic;  -- FPGA interconnect line
    INTB                   : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION                : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID                     : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO                 : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc                : inout std_logic;
    sens_sd                : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk                : in    std_logic;
    ETH_SGIN               : in    std_logic;
    ETH_SGOUT              : out   std_logic;

    -- Transceiver clocks
    SB_CLK                 : in  std_logic := '0';  -- TR clock FN-BN    (mesh)

    -- Serial I/O
    FN_BN_0_TX             : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_0_RX             : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_1_TX             : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_1_RX             : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_2_TX             : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_2_RX             : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0');
    FN_BN_3_TX             : out std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
    FN_BN_3_RX             : in  std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0) := (others => '0')
  );
end unb1_terminal_bg_mesh_db;

architecture str of unb1_terminal_bg_mesh_db is
  constant c_use_phy                 : t_c_unb1_board_use_phy := (1, 0, 1, 0, 0, 0, 0, 1);
  constant c_fw_version              : t_unb1_board_fw_version := (1, 0);  -- firmware version x.y

  constant c_use_bg                  : boolean := true;
  constant c_node_type               : t_e_unb1_board_node := e_any;  -- or e_fn, or e_bn
  constant c_nof_bus                 : natural := 4;  -- one bus to each of the 4 nodes on the other side of the mesh
  constant c_usr_use_complex         : boolean := false;  -- when TRUE transport sosi im & re fields via DP data, else transport sosi data via DP data
  constant c_usr_data_w              : natural := 16;  -- <= 32, to avoid need for DP packet data packing and to fit on the tr_nonbonded PHY data width of 32 bit
  constant c_usr_frame_len           : natural := 128;  -- 20;
  constant c_usr_nof_streams         : natural := 3;  -- number of user streams per bus
  constant c_phy_nof_serial          : natural := 3;  -- up to 4 serial lanes per bus
  constant c_phy_gx_mbps             : natural := 6250;  -- 5000;
  constant c_phy_rx_fifo_size        : natural := c_bram_m9k_fifo_depth;  -- g_fifos=TRUE in mms_tr_nonbonded, choose to use full BRAM size = 256 for FIFO depth at output from PHY
  constant c_phy_ena_reorder         : boolean := true;
  constant c_use_tx                  : boolean := true;
  constant c_tx_input_use_fifo       : boolean := true;  -- Tx input uses FIFO to create slack for inserting DP Packet and Uthernet frame headers
  constant c_tx_input_fifo_size      : natural := c_bram_m9k_fifo_depth;  -- g_tx_input_use_fifo=TRUE, choose to use full BRAM size = 256 for FIFO depth at input to uth_terminal_tx
  constant c_tx_input_fifo_fill      : natural := 0;
  constant c_use_rx                  : boolean := true;
  constant c_rx_output_use_fifo      : boolean := true;  -- Rx output provides FIFOs to ensure that dp_distribute does not get blocked due to substantial backpressure on another output
  constant c_rx_output_fifo_size     : natural := c_bram_m9k_fifo_depth;  -- g_rx_output_use_fifo, choose to use full BRAM size = 256 for FIFO depth at output of uth_terminal_rx
  constant c_rx_output_fifo_fill     : natural := 128;
  constant c_rx_timeout_w            : natural := 0;  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive uth_rx snk_in.valid
  constant c_mon_select              : natural := 0;
  constant c_mon_nof_words           : natural := 1024;
  constant c_mon_use_sync            : boolean := true;
  constant c_uth_len_max             : natural := 255;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
  constant c_uth_typ_ofs             : natural := 256;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
  constant c_aux                     : t_c_unb1_board_aux := c_unb1_board_aux;
  constant c_use_bsn_align           : boolean := true;  -- default TRUE, support FALSE for packet flow debugging purposes (faster synthesis)
  constant c_use_data_buf            : boolean := true;

  constant c_mesh_mon_select         : natural := 1;  -- > 0 = enable SOSI data buffers monitor via MM
  constant c_mesh_mon_nof_words      : natural := c_unb1_board_peripherals_mm_reg_default.ram_diag_db_buf_size;  -- = 1024
  constant c_mesh_mon_use_sync       : boolean := true;  -- when TRUE use dp_pps to trigger the data buffer capture, else new data capture after read access of last data word

  constant c_reg_diag_db_adr_w       : natural := 5;

  -- System
  signal cs_sim                      : std_logic;
  signal xo_clk                      : std_logic;
  signal xo_rst                      : std_logic;
  signal xo_rst_n                    : std_logic;
  signal mm_clk                      : std_logic;
  signal mm_locked                   : std_logic;
  signal mm_rst                      : std_logic;
  signal cal_clk                     : std_logic;
  signal epcs_clk                    : std_logic;

  signal dp_rst                      : std_logic;
  signal dp_clk                      : std_logic;
  signal dp_pps                      : std_logic;

  signal this_chip_id                : std_logic_vector(c_unb1_board_nof_chip_w - 1 downto 0);  -- [2:0], so range 0-3 for FN and range 4-7 BN

  -- PIOs
  signal pout_debug_wave             : std_logic_vector(c_word_w - 1 downto 0);
  signal pout_wdi                    : std_logic;

  -- WDI override
  signal reg_wdi_mosi                : t_mem_mosi;
  signal reg_wdi_miso                : t_mem_miso;

  -- WDI override
  signal reg_ppsh_mosi               : t_mem_mosi;
  signal reg_ppsh_miso               : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi    : t_mem_mosi;
  signal reg_unb_system_info_miso    : t_mem_miso;
  signal rom_unb_system_info_mosi    : t_mem_mosi;
  signal rom_unb_system_info_miso    : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi           : t_mem_mosi;  -- mms_unb_sens registers
  signal reg_unb_sens_miso           : t_mem_miso;

  -- eth1g
  signal eth1g_mm_rst                : std_logic;
  signal eth1g_tse_clk               : std_logic;
  signal eth1g_tse_mosi              : t_mem_mosi := c_mem_mosi_rst;  -- ETH TSE MAC registers
  signal eth1g_tse_miso              : t_mem_miso;
  signal eth1g_reg_mosi              : t_mem_mosi := c_mem_mosi_rst;  -- ETH control and status registers
  signal eth1g_reg_miso              : t_mem_miso;
  signal eth1g_reg_interrupt         : std_logic;  -- Interrupt
  signal eth1g_ram_mosi              : t_mem_mosi := c_mem_mosi_rst;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso              : t_mem_miso;

  signal tx_usr_siso_2arr            : t_unb1_board_mesh_siso_2arr := (others => (others => c_dp_siso_rdy));
  signal tx_usr_sosi_2arr            : t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));  -- Tx
  signal rx_usr_siso_2arr            : t_unb1_board_mesh_siso_2arr := (others => (others => c_dp_siso_rdy));
  signal rx_usr_sosi_2arr            : t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));  -- Rx

  -- tr_mesh
  signal tx_serial_2arr              : t_unb1_board_mesh_sl_2arr;  -- Tx
  signal rx_serial_2arr              : t_unb1_board_mesh_sl_2arr;  -- Rx support for diagnostics

  -- MM tr_nonbonded with diagnostics
  signal reg_tr_nonbonded_mosi       : t_mem_mosi := c_mem_mosi_rst;
  signal reg_tr_nonbonded_miso       : t_mem_miso;

  signal reg_diagnostics_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diagnostics_miso        : t_mem_miso;

  -- MM diag_data_buffer_mesh
  signal ram_mesh_diag_data_buf_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal ram_mesh_diag_data_buf_miso : t_mem_miso;

  -- MM diag_block_generator
  signal ram_diag_bg_mosi            : t_mem_mosi := c_mem_mosi_rst;
  signal ram_diag_bg_miso            : t_mem_miso;
  signal reg_diag_bg_mosi            : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diag_bg_miso            : t_mem_miso;

  -- MM diag_data_buffer (main)
  signal ram_diag_data_buf_mosi      : t_mem_mosi := c_mem_mosi_rst;
  signal ram_diag_data_buf_miso      : t_mem_miso;
  signal reg_diag_data_buf_mosi      : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diag_data_buf_miso      : t_mem_miso;

  -- MM bsn_monitor
  signal reg_bsn_monitor_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_monitor_miso        : t_mem_miso;
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim             => g_sim,
    g_design_name     => g_design_name,
    g_design_note     => g_design_note,
    g_stamp_date      => g_stamp_date,
    g_stamp_time      => g_stamp_time,
    g_stamp_svn       => g_stamp_svn,
    g_fw_version      => c_fw_version,
    g_sim_flash_model => not(g_sim),
    g_mm_clk_freq     => c_unb1_board_mm_clk_freq_125M,
    g_use_phy         => c_use_phy,
    g_aux             => c_unb1_board_aux,
    g_dp_clk_use_pll  => true,
    g_xo_clk_use_pll  => true
  )
  port map (
    -- Clock and reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk_out               => mm_clk,
    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    mm_locked                => mm_locked,
    mm_locked_out            => mm_locked,

    epcs_clk                 => epcs_clk,
    epcs_clk_out             => epcs_clk,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    cal_rec_clk              => cal_clk,
    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_tse_clk_out        => eth1g_tse_clk,
    eth1g_tse_clk            => eth1g_tse_clk,
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    sens_sc                  => sens_sc,
    sens_sd                  => sens_sd,
    -- . 1GbE Control Interface
    ETH_clk                  => ETH_clk,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_inst_mmm_unb1_terminal_bg_mesh_db : entity work.mmm_unb1_terminal_bg_mesh_db
  generic map(
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
  )
  port map(
    mm_clk                      =>  mm_clk,
    mm_rst                      =>  mm_rst,
    pout_wdi                    =>  pout_wdi,
    reg_wdi_mosi                =>  reg_wdi_mosi,
    reg_wdi_miso                =>  reg_wdi_miso,
    reg_unb_system_info_mosi    =>  reg_unb_system_info_mosi,
    reg_unb_system_info_miso    =>  reg_unb_system_info_miso,
    rom_unb_system_info_mosi    =>  rom_unb_system_info_mosi,
    rom_unb_system_info_miso    =>  rom_unb_system_info_miso,
    reg_unb_sens_mosi           =>  reg_unb_sens_mosi,
    reg_unb_sens_miso           =>  reg_unb_sens_miso,
    reg_ppsh_mosi               =>  reg_ppsh_mosi,
    reg_ppsh_miso               =>  reg_ppsh_miso,
    eth1g_mm_rst                =>  eth1g_mm_rst,
    eth1g_reg_interrupt         =>  eth1g_reg_interrupt,
    eth1g_ram_mosi              =>  eth1g_ram_mosi,
    eth1g_ram_miso              =>  eth1g_ram_miso,
    eth1g_reg_mosi              =>  eth1g_reg_mosi,
    eth1g_reg_miso              =>  eth1g_reg_miso,
    eth1g_tse_mosi              =>  eth1g_tse_mosi,
    eth1g_tse_miso              =>  eth1g_tse_miso,
    reg_diag_data_buf_mosi      =>  reg_diag_data_buf_mosi,
    reg_diag_data_buf_miso      =>  reg_diag_data_buf_miso,
    ram_diag_data_buf_mosi      =>  ram_diag_data_buf_mosi,
    ram_diag_data_buf_miso      =>  ram_diag_data_buf_miso,
    reg_diag_bg_mosi            =>  reg_diag_bg_mosi,
    reg_diag_bg_miso            =>  reg_diag_bg_miso,
    ram_diag_bg_mosi            =>  ram_diag_bg_mosi,
    ram_diag_bg_miso            =>  ram_diag_bg_miso,
    reg_diagnostics_mosi        =>  reg_diagnostics_mosi,
    reg_diagnostics_miso        =>  reg_diagnostics_miso,
    reg_tr_nonbonded_mosi       =>  reg_tr_nonbonded_mosi,
    reg_tr_nonbonded_miso       =>  reg_tr_nonbonded_miso,
    ram_mesh_diag_data_buf_mosi =>  ram_mesh_diag_data_buf_mosi,
    ram_mesh_diag_data_buf_miso =>  ram_mesh_diag_data_buf_miso,
    reg_bsn_monitor_mosi        =>  reg_bsn_monitor_mosi,
    reg_bsn_monitor_miso        =>  reg_bsn_monitor_miso
  );

  -----------------------------------------------------------------------------
  -- Node function: Terminals and data buffer
  -----------------------------------------------------------------------------
  u_terminal_mesh : entity work.node_unb1_terminal_bg_mesh_db
  generic map(
    g_sim                     => g_sim,
    g_sim_level               => g_sim_level,

    -- BLOCK GENERATOR
    g_use_bg                  => c_use_bg,

    -- MESH TERMINAL
    -- System
    g_node_type               => c_node_type,
    g_nof_bus                 => c_nof_bus,
    -- User
    g_usr_use_complex         => c_usr_use_complex,
    g_usr_data_w              => c_usr_data_w,
    g_usr_frame_len           => c_usr_frame_len,
    g_usr_nof_streams         => c_usr_nof_streams,
    -- Phy
    g_phy_nof_serial          => c_phy_nof_serial,
    g_phy_gx_mbps             => c_phy_gx_mbps,
    g_phy_rx_fifo_size        => c_phy_rx_fifo_size,
    g_phy_ena_reorder         => c_phy_ena_reorder,
    -- Tx
    g_use_tx                  => c_use_tx,
    g_tx_input_use_fifo       => c_tx_input_use_fifo,
    g_tx_input_fifo_size      => c_tx_input_fifo_size,
    g_tx_input_fifo_fill      => c_tx_input_fifo_fill,
    -- Rx
    g_use_rx                  => c_use_rx,
    g_rx_output_use_fifo      => c_rx_output_use_fifo,
    g_rx_output_fifo_size     => c_rx_output_fifo_size,
    g_rx_output_fifo_fill     => c_rx_output_fifo_fill,
    g_rx_timeout_w            => c_rx_timeout_w,
    -- Monitoring
    g_mon_select              => c_mon_select,
    g_mon_nof_words           => c_mon_nof_words,
    g_mon_use_sync            => c_mon_use_sync,
    -- UTH
    g_uth_len_max             => c_uth_len_max,
    g_uth_typ_ofs             => c_uth_typ_ofs,

    -- Auxiliary Interface
    g_aux                     => c_unb1_board_aux,
    -- BSN ALIGNER
    g_use_bsn_align           => c_use_bsn_align,
    -- DATA BUFFER
    g_use_data_buf            => c_use_data_buf
  )
  port map(
    -- System
    chip_id                     => this_chip_id,
    mm_rst                      => mm_rst,
    mm_clk                      => mm_clk,
    dp_rst                      => dp_rst,
    dp_clk                      => dp_clk,
    dp_pps                      => dp_pps,
    tr_mesh_clk                 => SB_CLK,
    cal_clk                     => cal_clk,

    -- MM interface
    reg_diag_bg_mosi            => reg_diag_bg_mosi,
    reg_diag_bg_miso            => reg_diag_bg_miso,
    ram_diag_bg_mosi            => ram_diag_bg_mosi,
    ram_diag_bg_miso            => ram_diag_bg_miso,
    ram_diag_data_buf_mosi      => ram_diag_data_buf_mosi,
    ram_diag_data_buf_miso      => ram_diag_data_buf_miso,
    reg_diag_data_buf_mosi      => reg_diag_data_buf_mosi,
    reg_diag_data_buf_miso      => reg_diag_data_buf_miso,
    reg_tr_nonbonded_mosi       => reg_tr_nonbonded_mosi,
    reg_tr_nonbonded_miso       => reg_tr_nonbonded_miso,
    reg_diagnostics_mosi        => reg_diagnostics_mosi,
    reg_diagnostics_miso        => reg_diagnostics_miso,
    ram_mesh_diag_data_buf_mosi => ram_mesh_diag_data_buf_mosi,
    ram_mesh_diag_data_buf_miso => ram_mesh_diag_data_buf_miso,
    reg_bsn_monitor_mosi        => reg_bsn_monitor_mosi,
    reg_bsn_monitor_miso        => reg_bsn_monitor_miso,

    -- Datapath User interface (4 nodes)(4 input streams)
    tx_usr_siso_2arr            => tx_usr_siso_2arr,
    tx_usr_sosi_2arr            => tx_usr_sosi_2arr,
    rx_usr_siso_2arr            => rx_usr_siso_2arr,
    rx_usr_sosi_2arr            => rx_usr_sosi_2arr,

    -- Mesh serial interface (tr_nonbonded)
    tx_serial_2arr              => tx_serial_2arr,
    rx_serial_2arr              => rx_serial_2arr
  );

  -----------------------------------------------------------------------------
  -- Mesh I/O
  -----------------------------------------------------------------------------
  no_tr_mesh : if c_use_phy.tr_mesh = 0 generate
    rx_serial_2arr <= (others => (others => '0'));
  end generate;

  gen_tr_mesh : if c_use_phy.tr_mesh /= 0 generate
    u_mesh_io : entity unb1_board_lib.unb1_board_mesh_io
    generic map (
      g_bus_w => c_unb1_board_tr_mesh.bus_w
    )
    port map (
      tx_serial_2arr => tx_serial_2arr,
      rx_serial_2arr => rx_serial_2arr,

      -- Serial I/O
      FN_BN_0_TX     => FN_BN_0_TX,
      FN_BN_0_RX     => FN_BN_0_RX,
      FN_BN_1_TX     => FN_BN_1_TX,
      FN_BN_1_RX     => FN_BN_1_RX,
      FN_BN_2_TX     => FN_BN_2_TX,
      FN_BN_2_RX     => FN_BN_2_RX,
      FN_BN_3_TX     => FN_BN_3_TX,
      FN_BN_3_RX     => FN_BN_3_RX
    );
  end generate;
end;
