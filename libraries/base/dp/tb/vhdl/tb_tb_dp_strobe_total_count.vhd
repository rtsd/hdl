-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Verify multiple variations of tb_dp_strobe_total_count
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_strobe_total_count is
end tb_tb_dp_strobe_total_count;

architecture tb of tb_tb_dp_strobe_total_count is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --g_mm_w                 : NATURAL := c_word_w;
  --g_count_w              : NATURAL := 16;
  --g_nof_blocks_per_sync  : NATURAL := 10;
  --g_nof_valid_per_blk    : NATURAL := 10;
  --g_nof_sync             : NATURAL := 10;
  --g_gap_size             : NATURAL := 0;

  -- Use g_nof_blocks_per_sync * g_nof_valid_per_blk = 100 valid per sync and
  -- use g_nof_sync = 10, so maximum expected valid count is 900.
  -- Maximum expected sop count is 90
  -- Maximum expected sync count is 9

  -- Default usage
  u_mm32b_cnt16b_no_gap  : entity work.tb_dp_strobe_total_count generic map(32, 16,  10, 10,  10, 0);
  u_mm32b_cnt16b_gap     : entity work.tb_dp_strobe_total_count generic map(32, 16,  10, 10,  10, 3);

  -- Check MM high word and counter overflow (clipping)
  u_mm8b_cnt16b_high     : entity work.tb_dp_strobe_total_count generic map(8, 16,  10, 10,  10, 3);  -- use high part
  u_mm8b_cnt9b_overflow  : entity work.tb_dp_strobe_total_count generic map(8,  9,  10, 10,  10, 3);  -- cause overflow to clip count high part
  u_mm8b_cnt7b_overflow  : entity work.tb_dp_strobe_total_count generic map(8,  7,  10, 10,  10, 3);  -- cause overflow to clip count low part
end tb;
