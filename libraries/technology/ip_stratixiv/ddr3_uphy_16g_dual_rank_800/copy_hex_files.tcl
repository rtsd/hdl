#------------------------------------------------------------------------------
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on Megawizard-generated file msim_setup.tcl.

set IP_DIR "$env(HDL_BUILD_DIR)/unb1/qmegawiz/ip_stratixiv_ddr3_uphy_16g_dual_rank_800_sim"

# Copy ROM/RAM files to simulation directory
if {[file isdirectory $IP_DIR]} {
    file copy -force $IP_DIR/ip_stratixiv_ddr3_uphy_16g_dual_rank_800/ip_stratixiv_ddr3_uphy_16g_dual_rank_800_s0_sequencer_mem.hex ./
    file copy -force $IP_DIR/ip_stratixiv_ddr3_uphy_16g_dual_rank_800/ip_stratixiv_ddr3_uphy_16g_dual_rank_800_s0_AC_ROM.hex ./
    file copy -force $IP_DIR/ip_stratixiv_ddr3_uphy_16g_dual_rank_800/ip_stratixiv_ddr3_uphy_16g_dual_rank_800_s0_inst_ROM.hex ./
}
