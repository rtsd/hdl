-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra, 27 jul 2017
-- Purpose:
--   Force the sosi data,re,im field at a certain index in series or pass on.
--
--   See also dp_force_data_parallel.vhd to force blocks of data.
-- Description:
--   The force_en input dynamically enables or disables the entire force
--   function.
--
--   The force data, re, im values are controlled via MM and are kept, so they
--   do not support auto-increment. The force value, zero_n and index control
--   are signal inputs to allow control via MM:
--   . Dependent on force_value data at the index can be forced to a value or
--     passed on transparently.
--   . Dependent on force_zero data not at the index is forced to zero or passed
--     on transparently.
--
--   The generics g_index_period and g_index_sample_block_n define how the
--   force_index is used.
--   . Dependent on g_index_sample_block_n the index counts on the valid or on
--     the sop. If the index counts on the valid and is reset by the sop then
--     the value at that index in each block is forced. If the index counts on
--     the sop and is reset by the sync then the entire block of values at that
--     index in each sync interval is forced.
--   . Typically When g_index_sample_block_n = TRUE then set g_index_period is
--     the block size. When g_index_sample_block_n = FALSE then set
--     g_index_period is the number of blocks per sync interval.
--
--   force_en = '0': pass on src_out = snk_in
--   force_en = '1':
--      when at force_index:
--         force_value = '1': force src_out data,re,im to force_data,re,im
--         force_value = '0': pass on src_out = snk_in
--      when not at force_index:
--         force_zero_n = '0': force src_out data,re,im to zero
--         force_zero_n = '1': pass on src_out = snk_in
--
--    Use force_zero_n instead of force_zero to preserve backwards
--    compatibility when force zero MM control was not supported yet and
--    then default force zero was active.
-- Usage:
-- . DSP tests: The amplitude of constant data is useful to verify scaling and
--   requantization.
-- . Data move test: Forcing constant (zero) data is useful for to trace data
--   when the data gets reordered.
-- Remarks:
-- . Ready latency = 1
-- . When force_en='0' then this dp_force_data_serial is equivalent to
--   dp_pipeline.
-- . The sosi.data and sosi.re, sosi.im fields can be forced independently.
--   However in the stream typically only the sosi.data is used or only the
--   complex sosi.re, sosi.im. Hence, when the sosi.data is used then the
--   force_re and force_im are void and when the sosi.re, sosi.im are used
--   then the force_data is void.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_force_data_serial is
  generic (
    g_dat_w                : natural := 32;  -- must be <= 32 to fit INTEGER range
    g_index_period         : natural := 10;  -- number of indices in time
    g_index_sample_block_n : boolean := false  -- when TRUE sample index in block, else block index in sync interval
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    -- MM control
    force_en      : in  std_logic := '0';
    force_value   : in  std_logic := '0';  -- when '1' force value when at force_index, else pass on snk_in when at force_index
    force_zero_n  : in  std_logic := '0';  -- when '0' force zero when not at force_index, else pass on snk_in when not at force_index
    force_data    : in  integer := 0;  -- force value used for sosi.data
    force_re      : in  integer := 0;  -- force value used for sosi.re
    force_im      : in  integer := 0;  -- force value used for sosi.im
    force_index   : in  natural := 0;  -- sample index or block index in time dependent on g_index_sample_block_n
    -- ST sink
    snk_out       : out t_dp_siso;
    snk_in        : in  t_dp_sosi;
    -- ST source
    src_in        : in  t_dp_siso := c_dp_siso_rdy;
    src_out       : out t_dp_sosi
  );
end dp_force_data_serial;

architecture str of dp_force_data_serial is
  constant c_cnt_w   : natural := ceil_log2(g_index_period);

  signal cnt_clr     : std_logic;
  signal cnt_en      : std_logic;
  signal cnt         : std_logic_vector(c_cnt_w - 1 downto 0);

  signal data_in     : t_dp_sosi;
  signal force_zero  : std_logic;
begin
  cnt_clr <= snk_in.sop   when g_index_sample_block_n = true else snk_in.sync;
  cnt_en  <= snk_in.valid when g_index_sample_block_n = true else snk_in.sop;

  force_zero <= not force_zero_n;

  u_common_counter : entity common_lib.common_counter
  generic map (
    g_latency   => 0,  -- default 1 for registered count output, use 0 for immediate combinatorial count output
    g_width     => c_cnt_w,
    g_max       => g_index_period
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => cnt_clr,
    cnt_en  => cnt_en,
    count   => cnt
  );

  p_comb : process(snk_in, cnt, force_en, force_index, force_value, force_zero, force_data, force_re, force_im)
  begin
    -- default transparant so keep snk_in
    data_in <= snk_in;
    if force_en = '1' then
      if unsigned(cnt) = force_index then
        -- at force_index
        if force_value = '1' then
          -- force constant
          data_in.data <= TO_DP_SDATA(force_data);
          data_in.re   <= TO_DP_DSP_DATA(force_re);
          data_in.im   <= TO_DP_DSP_DATA(force_im);
        end if;
      else
        -- not at force_index
        if force_zero = '1' then
          data_in.data <= TO_DP_SDATA(0);
          data_in.re   <= TO_DP_DSP_DATA(0);
          data_in.im   <= TO_DP_DSP_DATA(0);
        end if;
      end if;
    end if;
  end process;

  u_dp_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => 1
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => snk_out,
    snk_in       => data_in,
    -- ST source
    src_in       => src_in,
    src_out      => src_out
  );
end str;
