-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: R. van der Walle
-- Purpose: Verify mms_dp_scale
-- Description:
-- Usage:
-- > as 10
-- > run -all
-- The tb is self stopping and self checking.
-- The tb tests if input data is scaled by the expected scaling factor.
-- The tb can be simple as the components in the DUT are already verified.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_mms_dp_scale is
end tb_mms_dp_scale;

architecture tb of tb_mms_dp_scale is
  constant c_mm_clk_period              : time := 20 ns;
  constant c_dp_clk_period              : time := 10 ns;
  constant c_cross_clock_domain_latency : natural := 20;

  constant c_complex_data : boolean := true;
  constant c_complex_gain : boolean := false;
  constant c_gain_w       : natural := 8;
  constant c_in_dat_w     : natural := 9;
  constant c_in_length    : integer :=  2**(c_in_dat_w - 3) - 1;  -- Expected sosi does not take clipping into account
  constant c_in_max       : integer :=  2**(c_in_dat_w - 3) - 1;
  constant c_in_min       : integer := -2**(c_in_dat_w - 3);
  constant c_out_dat_w    : natural := c_in_dat_w;

  -- Configure DUT by setting gain to 2**2(=4) and lsb to 1
  -- resulting in an expected scale factor (c_exp_gain_re) of 2.0
  -- which is used in the verification process.
  constant c_lsb_w        : natural := 1;
  constant c_gain_re      : integer := 2**2;
  constant c_exp_gain_re  : integer := 2**(2 - c_lsb_w);

  signal tb_end           : std_logic := '0';
  signal mm_clk           : std_logic := '1';
  signal mm_rst           : std_logic := '1';
  signal dp_clk           : std_logic := '1';
  signal dp_rst           : std_logic := '1';

  signal stimuli_en       : std_logic := '0';
  signal verify_en        : std_logic := '0';

  signal cnt_re           : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal cnt_im           : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal cnt_val          : std_logic;

  signal in_sosi          : t_dp_sosi;
  signal in_sosi_dly      : t_dp_sosi;
  signal exp_sosi         : t_dp_sosi;
  signal out_sosi         : t_dp_sosi;

  signal reg_gain_re_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal reg_gain_re_miso   : t_mem_miso;
  signal reg_gain_im_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal reg_gain_im_miso   : t_mem_miso;
begin
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------
  proc_dp_cnt_dat(dp_rst, dp_clk, stimuli_en, cnt_val, cnt_re);

  -- derive cnt_im linearly from cnt_re such that abs(complex(cnt_re, cnt_im)) <= amplitude
  -- the principle is: suppose re normalized to 1 then im = 1-re when re>=0 else im = -1-re when re<0
  cnt_im <= TO_SVEC(sel_a_b(TO_SINT(cnt_re) >= 0, c_in_max, c_in_min) - TO_SINT(cnt_re), c_in_dat_w);

  in_sosi.data  <= RESIZE_DP_SDATA(cnt_re);
  in_sosi.re    <= RESIZE_DP_DSP_DATA(cnt_re);
  in_sosi.im    <= RESIZE_DP_DSP_DATA(cnt_im);
  in_sosi.valid <= cnt_val;

  u_in_sosi_dly : entity work.dp_pipeline
  generic map (
    g_pipeline   => 3  -- latency of DUT
  )
  port map (
    rst      => dp_rst,
    clk      => dp_clk,
    snk_in   => in_sosi,
    src_out  => in_sosi_dly
  );

  p_mm_stimuli : process
  begin
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(dp_clk, 5);

    proc_mem_mm_bus_wr(0, c_gain_re, mm_clk, reg_gain_re_miso, reg_gain_re_mosi);

    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);

    stimuli_en <= '1';
    proc_common_wait_some_cycles(dp_clk, 4);

    verify_en <= '1';
    proc_common_wait_some_cycles(dp_clk, c_in_length);
    verify_en <= '0';

    tb_end <= '1';
    wait;
  end process;

  -- Verify out_sosi_arr
  p_exp_sosi : process(in_sosi_dly)
  begin
    exp_sosi.re <= TO_DP_DSP_DATA(c_exp_gain_re * TO_SINT(in_sosi_dly.re));
    exp_sosi.im <= TO_DP_DSP_DATA(c_exp_gain_re * TO_SINT(in_sosi_dly.im));
  end process;

  p_verify : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if verify_en = '1' then
        assert signed(out_sosi.re) = signed(exp_sosi.re)
          report "Unexpected complex real data, with real gain"
          severity ERROR;
        assert signed(out_sosi.im) = signed(exp_sosi.im)
          report "Unexpected complex imag data, with real gain"
          severity ERROR;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
    u_dut : entity work.mms_dp_scale
    generic map (
      g_complex_data => c_complex_data,
      g_complex_gain => c_complex_gain,
      g_gain_w       => c_gain_w,
      g_in_dat_w     => c_in_dat_w,
      g_out_dat_w    => c_out_dat_w,
      g_lsb_w        => c_lsb_w
    )
    port map (
      -- Clocks and reset
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,

      -- MM access to gain
      reg_gain_re_mosi  => reg_gain_re_mosi,
      reg_gain_re_miso  => reg_gain_re_miso,
      reg_gain_im_mosi  => reg_gain_im_mosi,
      reg_gain_im_miso  => reg_gain_im_miso,

      -- ST
      in_sosi           => in_sosi,
      out_sosi          => out_sosi
    );
end tb;
