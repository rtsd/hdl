--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mm_tx_framer is
  generic(
    g_technology    : natural := c_tech_select_default;
    g_dat_out_w     : natural;
    g_rd_fifo_depth : natural := 128
    );
  port (
    mm_clk                  : in  std_logic;
    mm_rst                  : in  std_logic;

    tx_clk                  : in  std_logic;
    tx_rst                  : in  std_logic;

    ctrl_mosi               : in  t_mem_mosi := c_mem_mosi_rst;  -- No need to connect the CTRL MM bus if this instance is not the master
    ctrl_miso               : out t_mem_miso;

    data_mosi               : in  t_mem_mosi := c_mem_mosi_rst;
    data_miso               : out t_mem_miso;

    data_out_default        : in  std_logic_vector(g_dat_out_w - 1 downto 0) := (others => '0');
    data_out                : out std_logic_vector(g_dat_out_w - 1 downto 0);

    master_release          : out std_logic;  -- If used this instance will provide master release control for other instance(s)
    slave_release           : in  std_logic := '0'  -- If this instance is slave of another instance, the an MM write to the master's MM release will
                                                   -- also release the frames in the slave(s)
    );
end mm_tx_framer;

architecture str of mm_tx_framer is
  constant c_wr_rd_ratio   : natural := g_dat_out_w / c_word_w;
  constant c_wr_fifo_depth : natural := g_rd_fifo_depth * c_wr_rd_ratio;

  signal mm_to_fifo_sosi : t_dp_sosi;
  signal mm_availw       : std_logic_vector(c_word_w - 1 downto 0);
  signal wr_usedw        : std_logic_vector(ceil_log2(c_wr_fifo_depth) - 1 downto 0);

  signal fifo_siso       : t_dp_siso;
  signal fifo_out_sosi   : t_dp_sosi;

  signal release         : std_logic := '0';
begin
  master_release <= release;

  u_data_dp_fifo_from_mm : entity dp_lib.dp_fifo_from_mm
  generic map (
    g_fifo_size  => c_wr_fifo_depth
  )
  port map (
    rst           => mm_rst,
    clk           => mm_clk,
    -- ST soure connected to FIFO input
    src_out       => mm_to_fifo_sosi,
    usedw         => wr_usedw,
    -- Control for FIFO read access
    mm_wr         => data_mosi.wr,
    mm_wrdata     => data_mosi.wrdata(c_word_w - 1 downto 0),
    mm_usedw      => OPEN,
    mm_availw     => mm_availw
  );

  u_mm_to_dp_fifo : entity dp_lib.dp_fifo_dc_mixed_widths
  generic map (
    g_technology        => g_technology,
    g_wr_data_w         => c_word_w,
    g_rd_data_w         => g_dat_out_w,
    g_use_ctrl          => false,
    g_wr_fifo_size      => c_wr_fifo_depth
  )
  port map (
    wr_rst   => mm_rst,
    wr_clk   => mm_clk,
    rd_rst   => tx_rst,
    rd_clk   => tx_clk,

    snk_out  => OPEN,
    snk_in   => mm_to_fifo_sosi,

    wr_usedw => wr_usedw,  -- Using wr_usedw because that side is in mm_clk domain
    rd_usedw => OPEN,
    rd_emp   => OPEN,

    src_in   => fifo_siso,
    src_out  => fifo_out_sosi
  );

  data_out <= fifo_out_sosi.data(g_dat_out_w - 1 downto 0) when fifo_out_sosi.valid = '1' and (release = '1' or slave_release = '1') else data_out_default;
  fifo_siso.ready <= release or slave_release;

  u_reg : entity work.mm_tx_framer_reg
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,

    tx_clk            => tx_clk,
    tx_rst            => tx_rst,

    sla_in            => ctrl_mosi,
    sla_out           => ctrl_miso,

    mm_availw         => mm_availw,
    tx_release        => release

  );
end str;
