-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide streaming access to and from the nodes on the other side
--          of the UniBoard mesh.
-- Description:
-- Data flow:
-- * g_use_tx=TRUE
--    tx_usr_sosi_2arr--> uth_terminal_tx
--   tx_term_sosi_2arr--> unb1_board_mesh_reorder_tx
--    tx_phy_sosi_2arr--> map(I)(J) to (I*g_phy_nof_serial + J)
--     tx_phy_sosi_arr--> mms_tr_nonbonded
--       tx_serial_arr--> map(I*g_phy_nof_serial + J) to (I)(J)
--                    -->tx_serial_2arr
-- * g_use_rx=TRUE
--      rx_serial_2arr--> map(I)(J) to (I*g_phy_nof_serial + J)
--       rx_serial_arr--> mms_tr_nonbonded
--     rx_phy_sosi_arr--> map(I*g_phy_nof_serial + J) to (I)(J)
--    rx_phy_sosi_2arr--> unb1_board_mesh_reorder_rx
--   rx_term_sosi_2arr--> uth_terminal_rx
--                    -->rx_usr_sosi_2arr
--
-- Remark:
-- . The number of user input streams is defined by the width of the mesh bus,
--   so g_usr_nof_streams = c_unb1_board_tr.bus_w = 4 to be able to use
--   t_unb1_board_mesh_sosi_2arr.
-- . The mesh reorder logic is always instantiated, but when g_phy_ena_reorder
--   = FALSE then no reodering is done. In simulation the g_phy_ena_reorder
--   must match the g_reorder setting for unb1_board_mesh_model_*.vhd. On
--   hardware use the default g_phy_ena_reorder = TRUE.

library IEEE, common_lib, dp_lib, uth_lib, tr_nonbonded_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.unb1_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_packet_pkg.all;
use uth_lib.uth_pkg.all;

entity unb1_board_terminals_mesh is
  generic (
    g_sim                     : boolean := false;
    g_sim_level               : natural := 0;
    -- System
    g_node_type               : t_e_unb1_board_node := e_any;  -- or e_fn, or e_bn
    g_nof_bus                 : natural := 4;  -- one bus to each of the 4 nodes on the other side of the mesh
    -- User
    g_usr_use_complex         : boolean := false;  -- when TRUE transport sosi im & re fields via DP data, else transport sosi data via DP data
    g_usr_data_w              : natural := 32;  -- <= 32, to avoid need for DP packet data packing and to fit on the tr_nonbonded PHY data width of 32 bit
    g_usr_frame_len           : natural := 20;
    g_usr_nof_streams         : natural := 4;  -- number of user streams per bus
    -- Phy
    g_phy_nof_serial          : natural := 3;  -- up to 4 serial lanes per bus
    g_phy_gx_mbps             : natural := 5000;
    g_phy_rx_fifo_size        : natural := c_bram_m9k_fifo_depth;  -- g_fifos=TRUE in mms_tr_nonbonded, choose to use full BRAM size = 256 for FIFO depth at output from PHY
    g_phy_ena_reorder         : boolean := true;
    -- Tx
    g_use_tx                  : boolean := true;
    g_tx_input_use_fifo       : boolean := true;  -- Tx input uses FIFO to create slack for inserting DP Packet and Uthernet frame headers
    g_tx_input_fifo_size      : natural := c_bram_m9k_fifo_depth;  -- g_tx_input_use_fifo=TRUE, choose to use full BRAM size = 256 for FIFO depth at input to uth_terminal_tx
    g_tx_input_fifo_fill      : natural := 0;
    -- Rx
    g_use_rx                  : boolean := true;
    g_rx_output_use_fifo      : boolean := true;  -- Rx output provides FIFOs to ensure that dp_distribute does not get blocked due to substantial backpressure on another output
    g_rx_output_fifo_size     : natural := c_bram_m9k_fifo_depth;  -- g_rx_output_use_fifo, choose to use full BRAM size = 256 for FIFO depth at output of uth_terminal_rx
    g_rx_output_fifo_fill     : natural := 0;
    g_rx_timeout_w            : natural := 0;  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive uth_rx snk_in.valid
    -- Monitoring
    g_mon_select              : natural := 0;  -- 0 = no SOSI data buffers monitor via MM
                                                   -- 1 = enable monitor the Rx UTH packets per serial lane after the tr_nonbonded
                                                   -- 2 = enable monitor the Rx UTH packets per serial lane after the mesh reorder
                                                   -- 3 = enable monitor the Rx DP  packets per serial lane after the uth_rx
                                                   -- 4 = enable monitor the Rx DP  packets per user stream after the dp_distribute
                                                   -- 5 = enable monitor the Tx UTH packets per serial lane to the tr_nonbonded
                                                   -- 6 = enable monitor the Tx UTH packets per serial lane to the mesh reorder
    g_mon_nof_words           : natural := 1024;
    g_mon_use_sync            : boolean := true;
    -- UTH
    g_uth_len_max             : natural := 255;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
    g_uth_typ_ofs             : natural := 256  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
  );
  port (
    chip_id                : in  std_logic_vector(c_unb1_board_aux.chip_id_w - 1 downto 0) := (others => '0');  -- [2:0]

    mm_rst                 : in  std_logic;
    mm_clk                 : in  std_logic;
    dp_rst                 : in  std_logic;
    dp_clk                 : in  std_logic;
    dp_sync                : in  std_logic := '0';
    tr_clk                 : in  std_logic;
    cal_clk                : in  std_logic;

    -- User interface (4 nodes)(4 input streams)
    tx_usr_siso_2arr       : out t_unb1_board_mesh_siso_2arr;
    tx_usr_sosi_2arr       : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));  -- Tx
    rx_usr_siso_2arr       : in  t_unb1_board_mesh_siso_2arr := (others => (others => c_dp_siso_rdy));
    rx_usr_sosi_2arr       : out t_unb1_board_mesh_sosi_2arr;  -- Rx

    -- Serial (tr_nonbonded)
    tx_serial_2arr         : out t_unb1_board_mesh_sl_2arr;  -- Tx
    rx_serial_2arr         : in  t_unb1_board_mesh_sl_2arr := (others => (others => '0'));  -- Rx

    -- MM Control
    -- . tr_nonbonded
    reg_tr_nonbonded_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_tr_nonbonded_miso  : out t_mem_miso;
    reg_diagnostics_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diagnostics_miso   : out t_mem_miso;

    -- . monitor data buffer
    ram_diag_data_buf_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_diag_data_buf_miso : out t_mem_miso
  );
end unb1_board_terminals_mesh;

architecture str of unb1_board_terminals_mesh is
  -- DP/UTH packet
  constant c_packet_data_w            : natural := 32;  -- fixed 32, packet data width for DP packet and for UTH packet, must be >= g_usr_data_w to avoid need for data packing

  -- tr_nonbonded
  constant c_phy_data_w               : natural := 32;  -- fixed 32, tr_nonbonded supports 32b for generic g_phy_gx_mbps, must be >= c_packet_data_w to avoid loosing the higher bits
  constant c_phy_nof_gx               : natural := g_nof_bus * g_phy_nof_serial;

  -- g_mon_select
  constant c_usr_nof_streams          : natural := g_nof_bus * g_usr_nof_streams;
  constant c_mon_nof_streams          : natural := sel_n(g_mon_select, c_phy_nof_gx,
                                                                       c_phy_nof_gx,
                                                                       c_phy_nof_gx,
                                                                       c_phy_nof_gx,
                                                                       c_usr_nof_streams,
                                                                       c_phy_nof_gx,
                                                                       c_phy_nof_gx);
  constant c_mon_data_w               : natural := sel_n(g_mon_select, c_packet_data_w,
                                                                       c_packet_data_w,
                                                                       c_packet_data_w,
                                                                       c_packet_data_w,
                                                                       g_usr_data_w,
                                                                       c_packet_data_w,
                                                                       c_packet_data_w);

  -- uth terminals
  signal tx_term_siso_2arr          : t_unb1_board_mesh_siso_2arr;
  signal tx_term_sosi_2arr          : t_unb1_board_mesh_sosi_2arr;  -- Tx
  signal rx_term_siso_2arr          : t_unb1_board_mesh_siso_2arr;
  signal rx_term_sosi_2arr          : t_unb1_board_mesh_sosi_2arr;  -- Rx

  -- g_mon_select
  signal mon_sosi_arr               : t_dp_sosi_arr(c_mon_nof_streams - 1 downto 0);  -- selected sosi for the SOSI data monitor
  signal mon_rx_term_pkt_sosi_2arr  : t_unb1_board_mesh_sosi_2arr;
  signal mon_rx_term_dist_sosi_2arr : t_unb1_board_mesh_sosi_2arr;

  -- g_phy_ena_reorder
  signal tx_phy_siso_2arr           : t_unb1_board_mesh_siso_2arr;
  signal tx_phy_sosi_2arr           : t_unb1_board_mesh_sosi_2arr;  -- Tx
  signal rx_phy_siso_2arr           : t_unb1_board_mesh_siso_2arr;
  signal rx_phy_sosi_2arr           : t_unb1_board_mesh_sosi_2arr;  -- Rx

  -- tr_nonbonded
  signal tx_phy_siso_arr            : t_dp_siso_arr(c_phy_nof_gx - 1 downto 0);
  signal tx_phy_sosi_arr            : t_dp_sosi_arr(c_phy_nof_gx - 1 downto 0);  -- Tx
  signal rx_phy_siso_arr            : t_dp_siso_arr(c_phy_nof_gx - 1 downto 0);
  signal rx_phy_sosi_arr            : t_dp_sosi_arr(c_phy_nof_gx - 1 downto 0);  -- Rx

  signal tx_serial_arr              : std_logic_vector(c_phy_nof_gx - 1 downto 0);
  signal rx_serial_arr              : std_logic_vector(c_phy_nof_gx - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- g_mon_select
  ------------------------------------------------------------------------------

  mon_rx_mesh : if g_mon_select = 1 generate
    gen_i : for I in 0 to c_unb1_board_tr.nof_bus - 1 generate
      gen_j : for J in 0 to g_phy_nof_serial - 1 generate
        mon_sosi_arr(I * g_phy_nof_serial + J) <= rx_phy_sosi_2arr(I)(J);
      end generate;
    end generate;
  end generate;

  mon_rx_term_uth : if g_mon_select = 2 generate
    gen_i : for I in 0 to c_unb1_board_tr.nof_bus - 1 generate
      gen_j : for J in 0 to g_phy_nof_serial - 1 generate
        mon_sosi_arr(I * g_phy_nof_serial + J) <= rx_term_sosi_2arr(I)(J);
      end generate;
    end generate;
  end generate;

  mon_rx_term_pkt : if g_mon_select = 3 generate
    gen_i : for I in 0 to c_unb1_board_tr.nof_bus - 1 generate
      gen_j : for J in 0 to g_phy_nof_serial - 1 generate
        mon_sosi_arr(I * g_phy_nof_serial + J) <= mon_rx_term_pkt_sosi_2arr(I)(J);
      end generate;
    end generate;
  end generate;

  mon_rx_term_dist : if g_mon_select = 4 generate
    gen_i : for I in 0 to c_unb1_board_tr.nof_bus - 1 generate
      gen_j : for J in 0 to g_usr_nof_streams - 1 generate
        mon_sosi_arr(I * g_usr_nof_streams + J) <= mon_rx_term_dist_sosi_2arr(I)(J);
      end generate;
    end generate;
  end generate;

  mon_tx_mesh : if g_mon_select = 5 generate
    gen_i : for I in 0 to c_unb1_board_tr.nof_bus - 1 generate
      gen_j : for J in 0 to g_phy_nof_serial - 1 generate
        mon_sosi_arr(I * g_phy_nof_serial + J) <= tx_phy_sosi_2arr(I)(J);
      end generate;
    end generate;
  end generate;

  mon_tx_term_uth : if g_mon_select = 6 generate
    gen_i : for I in 0 to c_unb1_board_tr.nof_bus - 1 generate
      gen_j : for J in 0 to g_phy_nof_serial - 1 generate
        mon_sosi_arr(I * g_phy_nof_serial + J) <= tx_term_sosi_2arr(I)(J);
      end generate;
    end generate;
  end generate;

  no_monitor : if g_mon_select = 0 generate
    ram_diag_data_buf_miso <= c_mem_miso_rst;
  end generate;

  gen_monitor : if g_mon_select >= 1 generate
    u_data_buf : entity diag_lib.mms_diag_data_buffer
    generic map (
      g_nof_streams  => c_mon_nof_streams,
      g_data_w       => c_mon_data_w,  -- stream data width must be <= c_word_w = 32b, the MM word width
      g_buf_nof_data => g_mon_nof_words,  -- nof words per data buffer
      g_buf_use_sync => g_mon_use_sync  -- when TRUE start filling the buffer after the in_sync, else after the last word was read
    )
    port map (
      -- System
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => dp_rst,
      dp_clk            => dp_clk,
      -- MM interface
      ram_data_buf_mosi => ram_diag_data_buf_mosi,
      ram_data_buf_miso => ram_diag_data_buf_miso,
      -- ST interface
      in_sync           => dp_sync,  -- input sync pulse in ST dp_clk domain that starts data buffer when g_use_in_sync = TRUE
      in_sosi_arr       => mon_sosi_arr
    );
  end generate;

  ------------------------------------------------------------------------------
  -- Terminals
  ------------------------------------------------------------------------------

  u_unb1_board_mesh_uth_terminals_bidir : entity work.unb1_board_mesh_uth_terminals_bidir
  generic map (
    -- User
    g_usr_nof_streams     => g_usr_nof_streams,
    g_usr_use_complex     => g_usr_use_complex,
    g_usr_data_w          => g_usr_data_w,
    g_usr_frame_len       => g_usr_frame_len,
    -- DP/UTH packet
    g_packet_data_w       => c_packet_data_w,
    -- Phy
    g_phy_nof_serial      => g_phy_nof_serial,
    -- Tx
    g_use_tx              => g_use_tx,
    g_tx_input_use_fifo   => g_tx_input_use_fifo,
    g_tx_input_fifo_size  => g_tx_input_fifo_size,
    g_tx_input_fifo_fill  => g_tx_input_fifo_fill,
    -- Rx
    g_use_rx              => g_use_rx,
    g_rx_output_use_fifo  => g_rx_output_use_fifo,
    g_rx_output_fifo_size => g_rx_output_fifo_size,
    g_rx_output_fifo_fill => g_rx_output_fifo_fill,
    g_rx_timeout_w        => g_rx_timeout_w,
    -- UTH
    g_uth_len_max         => g_uth_len_max,
    g_uth_typ_ofs         => g_uth_typ_ofs
  )
  port map (
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,

    -- User
    tx_dp_sosi_2arr  => tx_usr_sosi_2arr,
    tx_dp_siso_2arr  => tx_usr_siso_2arr,

    rx_dp_sosi_2arr  => rx_usr_sosi_2arr,
    rx_dp_siso_2arr  => rx_usr_siso_2arr,

    -- Phy
    tx_uth_sosi_2arr => tx_term_sosi_2arr,
    tx_uth_siso_2arr => tx_term_siso_2arr,

    rx_uth_sosi_2arr => rx_term_sosi_2arr,
    rx_uth_siso_2arr => rx_term_siso_2arr,

    -- Monitoring
    rx_mon_pkt_sosi_2arr  => mon_rx_term_pkt_sosi_2arr,
    rx_mon_dist_sosi_2arr => mon_rx_term_dist_sosi_2arr
  );

  ------------------------------------------------------------------------------
  -- Compensate for mesh reorder (g_phy_ena_reorder)
  ------------------------------------------------------------------------------

  u_tx : entity work.unb1_board_mesh_reorder_tx
  generic map (
    g_node_type => g_node_type,
    g_reorder   => g_phy_ena_reorder
  )
  port map (
    chip_id          => chip_id,
    clk              => dp_clk,
    tx_usr_sosi_2arr => tx_term_sosi_2arr,  -- g_use_tx
    rx_usr_siso_2arr => rx_term_siso_2arr,  -- g_use_rx
    tx_phy_sosi_2arr => tx_phy_sosi_2arr,
    rx_phy_siso_2arr => rx_phy_siso_2arr
  );

  u_rx : entity work.unb1_board_mesh_reorder_rx
  generic map (
    g_node_type => g_node_type,
    g_reorder   => g_phy_ena_reorder
  )
  port map (
    chip_id          => chip_id,
    clk              => dp_clk,
    rx_phy_sosi_2arr => rx_phy_sosi_2arr,
    tx_phy_siso_2arr => tx_phy_siso_2arr,
    rx_usr_sosi_2arr => rx_term_sosi_2arr,  -- g_use_rx
    tx_usr_siso_2arr => tx_term_siso_2arr  -- g_use_tx
  );

  ------------------------------------------------------------------------------
  -- GX serial interface level (g_sim_level)
  ------------------------------------------------------------------------------

  -- Map 1-dim array on 2-dim array
  gen_bus : for I in g_nof_bus - 1 downto 0 generate
    gen_lane : for J in g_phy_nof_serial - 1 downto 0 generate
       -- SOSI
       tx_phy_sosi_arr(I * g_phy_nof_serial + J) <= tx_phy_sosi_2arr(I)(J);
       tx_phy_siso_2arr(I)(J)                  <= tx_phy_siso_arr(I * g_phy_nof_serial + J);

       rx_phy_sosi_2arr(I)(J)                  <= rx_phy_sosi_arr(I * g_phy_nof_serial + J);
       rx_phy_siso_arr(I * g_phy_nof_serial + J) <= rx_phy_siso_2arr(I)(J);

       -- Serial
       tx_serial_2arr(I)(J)                  <= tx_serial_arr(I * g_phy_nof_serial + J);
       rx_serial_arr(I * g_phy_nof_serial + J) <= rx_serial_2arr(I)(J);
    end generate;
  end generate;

  u_tr_nonbonded : entity tr_nonbonded_lib.mms_tr_nonbonded
  generic map (
    g_sim           => g_sim,
    g_sim_level     => g_sim_level,
    g_data_w        => c_phy_data_w,
    g_nof_gx        => c_phy_nof_gx,
    g_mbps          => g_phy_gx_mbps,
    g_tx            => g_use_tx,
    g_rx            => g_use_rx,
    g_rx_fifo_depth => g_phy_rx_fifo_size
  )
  port map (
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,

    st_rst               => dp_rst,
    st_clk               => dp_clk,

    tr_clk               => tr_clk,
    cal_rec_clk          => cal_clk,

    --Serial data I/O
    tx_dataout           => tx_serial_arr,
    rx_datain            => rx_serial_arr,

    --Streaming I/O
    snk_out_arr          => tx_phy_siso_arr,
    snk_in_arr           => tx_phy_sosi_arr,

    src_in_arr           => rx_phy_siso_arr,
    src_out_arr          => rx_phy_sosi_arr,

    tr_nonbonded_mm_mosi => reg_tr_nonbonded_mosi,
    tr_nonbonded_mm_miso => reg_tr_nonbonded_miso,

    diagnostics_mm_mosi  => reg_diagnostics_mosi,
    diagnostics_mm_miso  => reg_diagnostics_miso
  );
end str;
