# ##########################################################################
# Copyright 2021
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################
# Derived from lofar2_unb2b_sdp_station_pins.tcl and unb2b_board/quartus/pinning/unb2b_10GbE_pins.tcl.
# We cannot source unb2b_10GbE_pins.tcl as it causes synthesis errors when assignments are defined 
# for transceiver pins that are not in the top-level design
source $::env(HDL_WORK)/boards/uniboard2b/libraries/unb2b_board/quartus/pinning/unb2b_minimal_pins.tcl
# unb2b_jesd204_pins contains undesired QSFP pinning
#source $::env(HDL_WORK)/boards/uniboard2b/libraries/unb2b_board/quartus/pinning/unb2b_jesd204b_pins.tcl
#
#=====================
# QSFP pins
# ====================

set_location_assignment PIN_AL32 -to CLKUSR

set_location_assignment PIN_Y36 -to SA_CLK
set_instance_assignment -name IO_STANDARD LVDS -to SA_CLK
# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SA_CLK

set_location_assignment PIN_AH9 -to SB_CLK
set_instance_assignment -name IO_STANDARD LVDS -to SB_CLK
# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SB_CLK


set_global_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON


set_location_assignment PIN_AT31 -to QSFP_RST

set_location_assignment PIN_AY33 -to QSFP_SCL[0]
set_location_assignment PIN_AY32 -to QSFP_SCL[1]
set_location_assignment PIN_AY30 -to QSFP_SCL[2]
set_location_assignment PIN_AN33 -to QSFP_SCL[3]
set_location_assignment PIN_AN31 -to QSFP_SCL[4]
set_location_assignment PIN_AJ33 -to QSFP_SCL[5]
set_location_assignment PIN_BA32 -to QSFP_SDA[0]
set_location_assignment PIN_BA31 -to QSFP_SDA[1]
set_location_assignment PIN_AP33 -to QSFP_SDA[2]
set_location_assignment PIN_AM33 -to QSFP_SDA[3]
set_location_assignment PIN_AK33 -to QSFP_SDA[4]
set_location_assignment PIN_AH32 -to QSFP_SDA[5]

set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SDA[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_SCL[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_RST

# QSFP_0_RX
set_location_assignment PIN_AN38 -to QSFP_0_RX[0]
set_location_assignment PIN_AM40 -to QSFP_0_RX[1]
set_location_assignment PIN_AK40 -to QSFP_0_RX[2]
set_location_assignment PIN_AJ38 -to QSFP_0_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_0_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_0_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_0_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_0_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_0_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_0_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_0_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_0_RX[3]

# QSFP_0_TX
set_location_assignment PIN_AN42 -to QSFP_0_TX[0]
set_location_assignment PIN_AM44 -to QSFP_0_TX[1]
set_location_assignment PIN_AK44 -to QSFP_0_TX[2]
set_location_assignment PIN_AJ42 -to QSFP_0_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[3]


# RING pinning location
set_location_assignment PIN_AP40 -to RING_0_RX[0]
set_location_assignment PIN_AR38 -to RING_0_RX[1]
set_location_assignment PIN_AT40 -to RING_0_RX[2]
set_location_assignment PIN_AU38 -to RING_0_RX[3]
set_location_assignment PIN_AP44 -to RING_0_TX[0]
set_location_assignment PIN_AR42 -to RING_0_TX[1]
set_location_assignment PIN_AT44 -to RING_0_TX[2]
set_location_assignment PIN_AU42 -to RING_0_TX[3]
set_location_assignment PIN_H40 -to RING_1_RX[0]
set_location_assignment PIN_J38 -to RING_1_RX[1]
set_location_assignment PIN_F40 -to RING_1_RX[2]
set_location_assignment PIN_G38 -to RING_1_RX[3]
set_location_assignment PIN_H44 -to RING_1_TX[0]
set_location_assignment PIN_J42 -to RING_1_TX[1]
set_location_assignment PIN_G42 -to RING_1_TX[2]
set_location_assignment PIN_F44 -to RING_1_TX[3]

#RING_0 RX assignments
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[3]

#RING_1 RX assignments
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[3]

#RING_0 TX assignments
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[3]

#RING_1 TX assignments
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[3]

