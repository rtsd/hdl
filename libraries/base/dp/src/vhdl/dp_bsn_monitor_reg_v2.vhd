-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Reinier vd Walle
-- Purpose: Provide MM slave register for dp_bsn_monitor
-- Description:
--
--  Read only monitor register for streams with sync.
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |              sync timeout = [2], ready_stable = [1], xon_stable = [0] |  0
--  |-----------------------------------------------------------------------|
--  |                            bsn_at_sync[31: 0]                         |  1
--  |-----------------------------------------------------------------------|
--  |                            bsn_at_sync[63:32]                         |  2
--  |-----------------------------------------------------------------------|
--  |                                nof_sop[31: 0]                         |  3
--  |-----------------------------------------------------------------------|
--  |                              nof_valid[31: 0]                         |  4
--  |-----------------------------------------------------------------------|
--  |                                nof_err[31: 0]                         |  5
--  |-----------------------------------------------------------------------|
--  |                                latency[31: 0]                         |  6
--  |-----------------------------------------------------------------------|
-- --------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity dp_bsn_monitor_reg_v2 is
  generic (
    g_cross_clock_domain : boolean := true  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
  );
  port (
    -- Clocks and reset
    mm_rst                  : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk                  : in  std_logic;  -- memory-mapped bus clock
    st_rst                  : in  std_logic;  -- reset synchronous with st_clk
    st_clk                  : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in                  : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out                 : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    mon_evt                 : in  std_logic;  -- pulses when new monitor data is available regarding the previous sync interval
    mon_sync_timeout        : in  std_logic;
    -- . siso
    mon_ready_stable        : in  std_logic;
    mon_xon_stable          : in  std_logic;
    -- . sosi
    mon_bsn_at_sync         : in  std_logic_vector;
    mon_nof_sop             : in  std_logic_vector;
    mon_nof_err             : in  std_logic_vector;
    mon_nof_valid           : in  std_logic_vector;
    mon_latency             : in  std_logic_vector
  );
end dp_bsn_monitor_reg_v2;

architecture str of dp_bsn_monitor_reg_v2 is
  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => 3,
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => 7,
                                  init_sl  => '0');

  -- Registers in st_clk domain
  signal mon_reg      : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0) := (others => '0');
begin
  -- Register mapping
  mon_reg(         3 - 1 downto          0) <= mon_sync_timeout & mon_ready_stable & mon_xon_stable;
  mon_reg(1 * c_word_w - 1 downto          3) <= (others => '0');
  mon_reg(3 * c_word_w - 1 downto 1 * c_word_w) <= RESIZE_UVEC(mon_bsn_at_sync, c_longword_w);
  mon_reg(4 * c_word_w - 1 downto 3 * c_word_w) <= RESIZE_UVEC(mon_nof_sop,     c_word_w);
  mon_reg(5 * c_word_w - 1 downto 4 * c_word_w) <= RESIZE_UVEC(mon_nof_valid,   c_word_w);
  mon_reg(6 * c_word_w - 1 downto 5 * c_word_w) <= RESIZE_UVEC(mon_nof_err,     c_word_w);
  mon_reg(7 * c_word_w - 1 downto 6 * c_word_w) <= RESIZE_UVEC(mon_latency,     c_word_w);

  u_reg : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_in_new_latency     => 1,  -- mon_evt to mon_reg has latency 1 in dp_bsn_monitor
    g_readback           => false,
    g_reg                => c_mm_reg
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => st_rst,
    st_clk      => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => sla_in,
    sla_out     => sla_out,

    -- MM registers in st_clk domain
    reg_wr_arr  => OPEN,
    reg_rd_arr  => OPEN,
    in_new      => mon_evt,
    in_reg      => mon_reg,  -- read only
    out_reg     => open  -- no write
  );
end str;
