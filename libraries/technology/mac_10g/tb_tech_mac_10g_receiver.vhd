-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Reusable test bench stimuli for tech_mac_10g receiver
-- Description:

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use WORK.tb_tech_mac_10g_pkg.all;

entity tb_tech_mac_10_receiver is
  generic (
    --   g_data_type = c_tb_tech_mac_10g_data_type_symbols  = 0
    --   g_data_type = c_tb_tech_mac_10g_data_type_counter  = 1
    g_data_type             : natural := c_tb_tech_mac_10g_data_type_counter
  );
  port (
    mm_init        : in  std_logic;
    total_header   : in  t_network_total_header;
    rx_clk         : in  std_logic;
    rx_sosi        : in  t_dp_sosi;
    rx_siso        : out t_dp_siso;
    rx_toggle      : out std_logic  -- toggle after every received packet
  );
end tb_tech_mac_10_receiver;

architecture tb of tb_tech_mac_10_receiver is
  signal i_rx_toggle   : std_logic := '0';
begin
  rx_toggle <= i_rx_toggle;

  p_ff_receiver : process
  begin
    -- . Avalon ST
    rx_siso <= c_dp_siso_hold;

    while mm_init /= '0' loop
      wait until rising_edge(rx_clk);
    end loop;

    -- Receive forever
    while true loop
      proc_tech_mac_10g_rx_packet(total_header, g_data_type, rx_clk, rx_sosi, rx_siso);
      i_rx_toggle <= not i_rx_toggle;
    end loop;

    wait;
  end process;
end tb;
