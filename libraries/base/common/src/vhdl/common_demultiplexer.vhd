-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_components_pkg.all;

-- Purpose: Assign input to one of g_nof_out output streams based on out_sel input
-- Description: The output streams are concatenated into one SLV.
-- Remarks:
-- . Same scheme for pipeline handling and g_nof_out=1 handling as in common_select_symbol

entity common_demultiplexer is
  generic (
    g_pipeline_in  : natural := 0;
    g_pipeline_out : natural := 0;
    g_nof_out      : natural;
    g_dat_w        : natural
 );
  port (
    rst         : in  std_logic := '0';
    clk         : in  std_logic := '0';  -- for g_pipeline_* = 0 no rst and clk are needed, because then the demultiplexer works combinatorialy

    in_dat      : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val      : in  std_logic;

    out_sel     : in  std_logic_vector(ceil_log2(g_nof_out) - 1 downto 0);
    out_dat     : out std_logic_vector(g_nof_out * g_dat_w - 1 downto 0);
    out_val     : out std_logic_vector(g_nof_out        - 1 downto 0)
  );
end;

architecture rtl of common_demultiplexer is
  constant c_sel_w    : natural := out_sel'length;

  signal in_dat_reg    : std_logic_vector(in_dat'range);
  signal in_val_reg    : std_logic;

  signal out_sel_reg   : std_logic_vector(out_sel'range);

  signal sel_dat       : std_logic_vector(g_nof_out * g_dat_w - 1 downto 0);
  signal sel_val       : std_logic_vector(g_nof_out        - 1 downto 0);
begin
  -- pipeline input
  u_pipe_in_dat  : common_pipeline    generic map ("SIGNED", g_pipeline_in, 0, g_dat_w, g_dat_w) port map (rst, clk, '1', '0', '1', in_dat,  in_dat_reg);
  u_pipe_in_val  : common_pipeline_sl generic map (          g_pipeline_in, 0, false)            port map (rst, clk, '1', '0', '1', in_val,  in_val_reg);

  u_pipe_out_sel : common_pipeline    generic map ("SIGNED", g_pipeline_in, 0, c_sel_w, c_sel_w) port map (rst, clk, '1', '0', '1', out_sel, out_sel_reg);

  -- select combinatorialy
  no_sel : if g_nof_out = 1 generate
    sel_dat    <= in_dat_reg;
    sel_val(0) <= in_val_reg;
  end generate;

  gen_sel : if g_nof_out > 1 generate
    p_sel : process(out_sel_reg, in_dat_reg, in_val_reg)
    begin
      sel_val <= (others => '0');
      for I in g_nof_out - 1 downto 0 loop
        sel_dat((I + 1) * g_dat_w - 1 downto I * g_dat_w) <= in_dat_reg;  -- replicate in_dat to all outputs, this requires less logic than default forcing invalid outputs to 0
        if TO_UINT(out_sel_reg) = I then
          sel_val(I) <= in_val_reg;  -- let out_sel determine which output is valid
        end if;
      end loop;
    end process;
  end generate;

  -- pipeline output
  u_pipe_out_dat : common_pipeline generic map ("SIGNED", g_pipeline_out, 0, g_nof_out * g_dat_w, g_nof_out * g_dat_w) port map (rst, clk, '1', '0', '1', sel_dat, out_dat);
  u_pipe_out_val : common_pipeline generic map ("SIGNED", g_pipeline_out, 0, g_nof_out,         g_nof_out        ) port map (rst, clk, '1', '0', '1', sel_val, out_val);
end rtl;
