-------------------------------------------------------------------------------
--
-- Copyright (C) 2012-2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use i2c_lib.i2c_pkg.all;
use work.unb2_board_pkg.all;

entity unb2_board_sens is
  generic (
    g_sim             : boolean := false;
    g_i2c_peripheral  : natural;
    g_clk_freq        : natural := 100 * 10**6;  -- clk frequency in Hz
    g_temp_high       : natural := 85;
    g_sens_nof_result : natural;  -- Should match nof read bytes via I2C in the unb2_board_sens_ctrl SEQUENCE list
    g_comma_w         : natural := 0
  );
  port (
    rst          : in    std_logic;
    clk          : in    std_logic;
    start        : in    std_logic;
    -- i2c bus
    scl          : inout std_logic;
    sda          : inout std_logic;
    -- read results
    sens_evt     : out   std_logic;
    sens_err     : out   std_logic;
    sens_data    : out   t_slv_8_arr(0 to g_sens_nof_result - 1)
  );
end entity;

architecture str of unb2_board_sens is
  -- I2C clock rate settings
  constant c_sens_clk_cnt      : natural := sel_a_b(g_sim, 1, func_i2c_calculate_clk_cnt(g_clk_freq / 10**6));  -- define I2C clock rate
  --CONSTANT c_sens_comma_w      : NATURAL := 13;  -- 2**c_i2c_comma_w * system clock period comma time after I2C start and after each octet
                                                -- 0 = no comma time

-- octave:4> t=1/50e6
-- t =  2.0000e-08
-- octave:5> delay=2^13 * t
-- delay =  1.6384e-04
-- octave:6> delay/t
-- ans =  8192
-- octave:7> log2(ans)
-- ans =  13
-- octave:8> log2(delay/t)
-- ans =  13

  --CONSTANT c_sens_phy          : t_c_i2c_phy := (c_sens_clk_cnt, c_sens_comma_w);
  constant c_sens_phy          : t_c_i2c_phy := (c_sens_clk_cnt, g_comma_w);

  signal smbus_in_dat  : std_logic_vector(c_byte_w - 1 downto 0);
  signal smbus_in_val  : std_logic;
  signal smbus_out_dat : std_logic_vector(c_byte_w - 1 downto 0);
  signal smbus_out_val : std_logic;
  signal smbus_out_err : std_logic;
  signal smbus_out_ack : std_logic;
  signal smbus_out_end : std_logic;
begin
  gen_unb2_board_sens_ctrl : if g_i2c_peripheral = c_i2c_peripheral_sens generate
    u_unb2_board_sens_ctrl : entity work.unb2_board_sens_ctrl
    generic map (
      g_sim        => g_sim,
      g_nof_result => g_sens_nof_result,
      g_temp_high  => g_temp_high
    )
    port map (
      clk         => clk,
      rst         => rst,
      start       => start,
      in_dat      => smbus_out_dat,
      in_val      => smbus_out_val,
      in_err      => smbus_out_err,
      in_ack      => smbus_out_ack,
      in_end      => smbus_out_end,
      out_dat     => smbus_in_dat,
      out_val     => smbus_in_val,
      result_val  => sens_evt,
      result_err  => sens_err,
      result_dat  => sens_data
    );
  end generate;

  gen_unb2_board_pmbus_ctrl : if g_i2c_peripheral = c_i2c_peripheral_pmbus generate
    u_unb2_board_pmbus_ctrl : entity work.unb2_board_pmbus_ctrl
    generic map (
      g_sim        => g_sim,
      g_nof_result => g_sens_nof_result,
      g_temp_high  => g_temp_high
    )
    port map (
      clk         => clk,
      rst         => rst,
      start       => start,
      in_dat      => smbus_out_dat,
      in_val      => smbus_out_val,
      in_err      => smbus_out_err,
      in_ack      => smbus_out_ack,
      in_end      => smbus_out_end,
      out_dat     => smbus_in_dat,
      out_val     => smbus_in_val,
      result_val  => sens_evt,
      result_err  => sens_err,
      result_dat  => sens_data
    );
  end generate;

  gen_unb2_board_hmc_ctrl : if g_i2c_peripheral = c_i2c_peripheral_hmc generate
    u_unb2_board_hmc_ctrl : entity work.unb2_board_hmc_ctrl
    generic map (
      g_sim        => g_sim,
      g_nof_result => g_sens_nof_result,
      g_temp_high  => g_temp_high
    )
    port map (
      clk         => clk,
      rst         => rst,
      start       => start,
      in_dat      => smbus_out_dat,
      in_val      => smbus_out_val,
      in_err      => smbus_out_err,
      in_ack      => smbus_out_ack,
      in_end      => smbus_out_end,
      out_dat     => smbus_in_dat,
      out_val     => smbus_in_val,
      result_val  => sens_evt,
      result_err  => sens_err,
      result_dat  => sens_data
    );
  end generate;

  u_i2c_smbus : entity i2c_lib.i2c_smbus
  generic map (
    g_i2c_phy                 => c_sens_phy,
    g_clock_stretch_sense_scl => true
  )
  port map (
    gs_sim      => g_sim,
    clk         => clk,
    rst         => rst,
    in_dat      => smbus_in_dat,
    in_req      => smbus_in_val,
    out_dat     => smbus_out_dat,
    out_val     => smbus_out_val,
    out_err     => smbus_out_err,
    out_ack     => smbus_out_ack,
    st_end      => smbus_out_end,
    scl         => scl,
    sda         => sda
  );
end architecture;
