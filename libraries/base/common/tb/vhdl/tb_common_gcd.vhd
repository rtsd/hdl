-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra 2022
-- Purpose: Test bench for gcd() in common_pkg.vhd
-- Usage:
-- > run -a

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_gcd is
end tb_common_gcd;

architecture tb of tb_common_gcd is
begin
  assert gcd( 0, 10) = 10
    report "Wrong gcd( 0, 10)"
    severity ERROR;
  assert gcd( 1, 1) = 1
    report "Wrong gcd( 1, 1)"
    severity ERROR;
  assert gcd(10, 1) = 1
    report "Wrong gcd(10, 1)"
    severity ERROR;
  assert gcd(10, 3) = 1
    report "Wrong gcd(10, 3)"
    severity ERROR;
  assert gcd(10, 4) = 2
    report "Wrong gcd(10, 4)"
    severity ERROR;
  assert gcd(10, 5) = 5
    report "Wrong gcd(10, 5)"
    severity ERROR;
  assert gcd(16, 4) = 4
    report "Wrong gcd(16, 4)"
    severity ERROR;
  assert gcd(15, 5) = 5
    report "Wrong gcd(15, 5)"
    severity ERROR;
  assert gcd(17, 17) = 17
    report "Wrong gcd(17, 17)"
    severity ERROR;
  assert gcd(17, 4) = 1
    report "Wrong gcd(17, 4)"
    severity ERROR;
end tb;
