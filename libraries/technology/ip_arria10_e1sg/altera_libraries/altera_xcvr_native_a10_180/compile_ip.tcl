#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist   

vmap  altera_xcvr_native_a10_180       ./work/
vmap  altera_common_sv_packages        ./work/


set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_phy_10gbase_r_48/sim"

# common dependencies
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/altera_xcvr_native_a10_functions_h.sv"                                                               -work altera_common_sv_packages       
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_resync.sv"                                                     -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_arbiter.sv"                                                    -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/mentor/alt_xcvr_resync.sv"                                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/mentor/alt_xcvr_arbiter.sv"                                             -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/twentynm_pcs.sv"                                                        -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/twentynm_pma.sv"                                                        -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/twentynm_xcvr_avmm.sv"                                                  -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/twentynm_xcvr_native.sv"                                                -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/mentor/twentynm_pcs.sv"                                                 -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/mentor/twentynm_pma.sv"                                                 -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/mentor/twentynm_xcvr_avmm.sv"                                           -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/mentor/twentynm_xcvr_native.sv"                                         -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/a10_avmm_h.sv"                                                          -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_pipe_retry.sv"                                          -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_avmm_csr.sv"                                            -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_prbs_accum.sv"                                          -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_odi_accel.sv"                                           -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_arb.sv"                                            -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/altera_xcvr_native_pcie_dfe_params_h.sv"                                -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/pcie_mgmt_commands_h.sv"                                                -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/pcie_mgmt_functions_h.sv"                                               -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/pcie_mgmt_program.sv"                                                   -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/pcie_mgmt_cpu.sv"                                                       -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/pcie_mgmt_master.sv"                                                    -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/altera_xcvr_native_pcie_dfe_ip.sv"                                      -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/reconfig/altera_xcvr_native_a10_reconfig_parameters.sv"                 -L altera_common_sv_packages -work altera_xcvr_native_a10_180  

# phy_10gbase_r_48
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_phy_10gbase_r_48_altera_xcvr_native_a10_180_y6b7ffi.sv" -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_y6b7ffi.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180  

# phy_10gbase_r_24
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_phy_10gbase_r_24/sim"
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_phy_10gbase_r_24_altera_xcvr_native_a10_180_mhfwvwa.sv" -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_mhfwvwa.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180  

# phy_10gbase_r_12
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_phy_10gbase_r_12/sim"
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_phy_10gbase_r_12_altera_xcvr_native_a10_180_fs3onwi.sv" -L altera_common_sv_packages -work altera_xcvr_native_a10_180      
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_fs3onwi.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180  

# phy_10gbase_r_4
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_phy_10gbase_r_4/sim"
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_phy_10gbase_r_4_altera_xcvr_native_a10_180_d2amdia.sv"  -L altera_common_sv_packages -work altera_xcvr_native_a10_180     
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_d2amdia.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180  

# phy_10gbase_r_3
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_phy_10gbase_r_3/sim"
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_phy_10gbase_r_3_altera_xcvr_native_a10_180_skxmbpy.sv"  -L altera_common_sv_packages -work altera_xcvr_native_a10_180     
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_skxmbpy.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180  

# phy_10gbase_r
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_phy_10gbase_r/sim"
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_phy_10gbase_r_altera_xcvr_native_a10_180_nbxifma.sv"    -L altera_common_sv_packages -work altera_xcvr_native_a10_180   
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_nbxifma.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180 

# tse_sgmii_gx
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_tse_sgmii_gx/sim"
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_tse_sgmii_gx_altera_xcvr_native_a10_180_k23srea.sv"     -L altera_common_sv_packages -work altera_xcvr_native_a10_180            
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_k23srea.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180  

# jesd204b rx
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_jesd204b_rx_200MHz/sim"
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_jesd204b_rx_200MHz_altera_xcvr_native_a10_180_vcpx3ja.sv"      -L altera_common_sv_packages -work altera_xcvr_native_a10_180            
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_vcpx3ja.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180  

# jesd204b tx
set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_jesd204b_tx/sim"
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/ip_arria10_e1sg_jesd204b_tx_altera_xcvr_native_a10_180_q3qhp5a.sv"      -L altera_common_sv_packages -work altera_xcvr_native_a10_180            
  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/alt_xcvr_native_rcfg_opt_logic_q3qhp5a.sv"                              -L altera_common_sv_packages -work altera_xcvr_native_a10_180  
