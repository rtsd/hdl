-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

-- Purpose: This test bench verifies both dp_fifo_from_mm and dp_fifo_to_mm.
-- Usage:
--   > as 10
--   > run -all (run > c_run_time / 100 us and c_run_time >>> c_fifo_size)
--
-- Description:
--   The test bench structure is:
--
--   p_wr_stimuli -> dp_fifo_from_mm -> FIFO -> dp_fifo_to_mm -> p_rd_stimuli
--
--   The verify code at the end verifies that the data goes through correctly
--   and that sufficient data values have been applied.
--
-- Remark:
-- . The stimuli are applied via mm_wr and mm_rd, which are controlled in a
--   random manner.
-- . To ensure that all relevant conditions have been covered it is necessary
--   to run as long as necessary so that wr_availw_is_0='1' and
--   rd_usedw_is_0='1' have occurred.

entity tb_dp_fifo_to_mm is
end tb_dp_fifo_to_mm;

architecture tb of tb_dp_fifo_to_mm is
  constant clk_period        : time := 10 ns;

  constant c_run_time        : natural := 9000;

  constant c_rl              : natural := 1;  -- Fixed RL = 1

  constant c_fifo_size       : natural := 32;
  constant c_fifo_usedw_w    : natural := ceil_log2(c_fifo_size);
  constant c_fifo_af_margin  : natural := 1;  -- >= 1

  constant c_mm_data_w       : natural := 16;  -- must be >= c_fifo_usedw_w

  type t_state is (s_idle, s_one_go, s_random);

  signal tb_end              : std_logic := '0';
  signal rst                 : std_logic;
  signal clk                 : std_logic := '1';

  signal random_wr           : std_logic_vector(c_fifo_usedw_w   downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_rd           : std_logic_vector(c_fifo_usedw_w + 1 downto 0) := (others => '0');

  -- DUT
  signal mm_wr               : std_logic;
  signal mm_wrdata           : std_logic_vector(c_mm_data_w - 1 downto 0) := (others => '0');
  signal wr_sosi             : t_dp_sosi := c_dp_sosi_rst;

  signal fifo_full           : std_logic;
  signal fifo_usedw          : std_logic_vector(c_fifo_usedw_w - 1 downto 0);
  signal wr_availw           : std_logic_vector(c_mm_data_w - 1 downto 0);
  signal rd_usedw            : std_logic_vector(c_mm_data_w - 1 downto 0);

  signal mm_rd               : std_logic;
  signal mm_rddata           : std_logic_vector(c_mm_data_w - 1 downto 0) := (others => '0');
  signal mm_rdval            : std_logic := '0';
  signal rd_siso             : t_dp_siso;
  signal rd_sosi             : t_dp_sosi := c_dp_sosi_rst;

  -- Verify
  signal verify_en           : std_logic;
  signal verify_done         : std_logic := '0';
  signal ready               : std_logic := '1';
  signal state_wr            : t_state;
  signal state_rd            : t_state;
  signal burst_wr            : natural;
  signal burst_rd            : natural;
  signal wr_availw_is_0      : std_logic;
  signal rd_usedw_is_0       : std_logic;

  signal prev_mm_rddata      : std_logic_vector(c_mm_data_w - 1 downto 0);
  signal expected_mm_rddata  : std_logic_vector(c_mm_data_w - 1 downto 0) := (others => '0');
begin
  -- as 10
  -- run 10 ms

  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_wr <= func_common_random(random_wr) when rising_edge(clk);
  random_rd <= func_common_random(random_rd) when rising_edge(clk);

  mm_wrdata <= INCR_UVEC(mm_wrdata, 1) when rising_edge(clk) and mm_wr = '1';

  verify_en <= '0', '1' after clk_period * 50;

  wr_availw_is_0 <= '1' when unsigned(wr_availw) = 0 else '0';
  rd_usedw_is_0  <= '1' when unsigned(rd_usedw) = 0  else '0';

  p_wr_stimuli : process
    variable v_burst : natural;
  begin
    mm_wr    <= '0';
    state_wr <= s_idle;
    wait until rst = '0';
    proc_common_wait_some_cycles(clk, 10);

    while true loop
      mm_wr <= '0';
      case state_wr is
        when s_idle =>
          v_burst := TO_UINT(random_wr(c_fifo_usedw_w - 1 downto 0));  -- no write for random nof cycles < g_fifo_size
          if unsigned(wr_availw) < c_fifo_size / 4 then
            state_wr <= s_one_go;
          else
            state_wr <= s_random;
          end if;
            state_wr <= s_one_go;
        when s_one_go =>
          if unsigned(wr_availw) > 0 then
            mm_wr <= '1';
            v_burst := TO_UINT(wr_availw);  -- use wr_availw to try to fill the FIFO in one go
          end if;
          state_wr <= s_idle;
        when s_random =>
          if unsigned(wr_availw) > 0 then
            mm_wr <= '1';
            v_burst := (TO_UINT(wr_availw) * TO_UINT(random_wr)) / 2**random_wr'length;  -- determine a random write burst size < wr_availw to fill part of the FIFO
          end if;
          state_wr <= s_idle;
      end case;
      if v_burst = 0 then v_burst := 1; end if;
      burst_wr <= v_burst;
      proc_common_wait_some_cycles(clk, v_burst);  -- clock the burst size
    end loop;
  end process;

  p_rd_stimuli : process
    variable v_burst : natural;
  begin
    mm_rd    <= '0';
    state_rd <= s_idle;
    wait until rst = '0';
    proc_common_wait_some_cycles(clk, 10);

    while true loop
      mm_rd <= '0';
      case state_rd is
        when s_idle =>
          v_burst := TO_UINT(random_rd(c_fifo_usedw_w - 1 downto 0));  -- no read for random nof cycles < g_fifo_size
          if unsigned(rd_usedw) < c_fifo_size / 4 then
            state_rd <= s_one_go;
          else
            state_rd <= s_random;
          end if;
            state_rd <= s_one_go;
        when s_one_go =>
          if unsigned(rd_usedw) > 0 then
            mm_rd <= '1';
            v_burst := TO_UINT(rd_usedw);  -- use rd_usedw to try to empty the FIFO in one go
          end if;
          state_rd <= s_idle;
        when s_random =>
          if unsigned(rd_usedw) > 0 then
            mm_rd <= '1';
            v_burst := (TO_UINT(rd_usedw) * TO_UINT(random_rd)) / 2**random_rd'length;  -- determine a random read burst size < rd_usedw to empty part of the FIFO
          end if;
          state_rd <= s_idle;
      end case;
      if v_burst = 0 then v_burst := 1; end if;
      burst_rd <= v_burst;
      proc_common_wait_some_cycles(clk, v_burst);  -- clock the burst size
    end loop;
  end process;

  u_from_mm : entity work.dp_fifo_from_mm
  generic map (
    g_fifo_size      => c_fifo_size,
    g_fifo_af_margin => c_fifo_af_margin,
    g_mm_word_w      => c_mm_data_w
  )
  port map (
    rst           => rst,
    clk           => clk,
    -- ST soure connected to FIFO input
    src_out       => wr_sosi,
    usedw         => fifo_usedw,
    -- Control for FIFO read access
    mm_wr         => mm_wr,
    mm_wrdata     => mm_wrdata,
    mm_usedw      => OPEN,
    mm_availw     => wr_availw
  );

  u_fifo : entity work.dp_fifo_sc
  generic map (
    g_data_w         => c_mm_data_w,
    g_use_ctrl       => false,
    g_fifo_size      => c_fifo_size,
    g_fifo_af_margin => c_fifo_af_margin,
    g_fifo_rl        => c_rl
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => wr_sosi,
    -- Monitor FIFO filling
    wr_ful      => fifo_full,
    usedw       => fifo_usedw,
    rd_emp      => OPEN,
    -- ST source
    src_in      => rd_siso,
    src_out     => rd_sosi
  );

  rd_siso.ready <= mm_rd;

  u_to_mm : entity work.dp_fifo_to_mm
  generic map (
    g_fifo_size      => c_fifo_size,
    g_mm_word_w      => c_mm_data_w
  )
  port map (
    rst           => rst,
    clk           => clk,
    -- ST sink connected to FIFO output
    snk_out       => rd_siso,
    snk_in        => rd_sosi,
    usedw         => fifo_usedw,
    -- Control for FIFO read access
    mm_rd         => mm_rd,
    mm_rddata     => mm_rddata,
    mm_usedw      => rd_usedw
  );

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      -- Verify that FIFO never runs full
      assert fifo_full = '0'
        report "TB : FIFO full must not occur"
        severity ERROR;

      -- Verify that the read data is in not too much behind the write data
      assert unsigned(mm_wrdata) <= unsigned(mm_rddata) + c_fifo_size
        report "TB : Too large difference value"
        severity ERROR;
    end if;
  end process;

  -- Verify that stimuli have run at all by checking that the read data has reached at least a certain value at the verify done pulse
  expected_mm_rddata <= TO_UVEC(c_run_time / 3, c_mm_data_w);
  verify_done <= '0', '1' after clk_period * c_run_time, '0' after clk_period * (c_run_time+1);
  tb_end      <= '0', '1' after clk_period * (c_run_time+100);
  proc_dp_verify_value(e_at_least, clk, verify_done, expected_mm_rddata, mm_rddata);

  -- Verify that the read data is incrementing data
  mm_rdval <= mm_rd when rising_edge(clk);
  proc_common_verify_data(c_rl, clk, verify_en, ready, mm_rdval, mm_rddata, prev_mm_rddata);
end tb;
