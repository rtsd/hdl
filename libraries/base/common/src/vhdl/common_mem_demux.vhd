-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Purpose: Decompose a single MM interface into an array of MM interfaces.
-- Description:
--   Inverse operation of common_mem_mux.
--
--                                      g_rd_latency
--                                     ______________
--        use index of mosi_arr        |            |
--        with active rd or wr  ---+-->| delay line |--\
--                                 |   |____________|  |
--                                 |                   |
--                   selected      v                   |
--   mosi_arr -----> mosi_address[h:w]-----------------------------> mosi
--                                                     |
--                                            selected v
--   miso_arr <-------------------------------miso_arr[ ]<--------- miso
--
--        . not selected mosi_arr are ignored
--        . not selected miso_arr get c_mem_miso_rst
--
-- Remarks:
-- . In simulation selecting an unused element address will cause a simulation
--   failure. Therefore the element index is only accepted when it is in the
--   g_nof_mosi-1 DOWNTO 0 range.
-- . In case common_mem_demux and common_mem_mux would be used in series, then
--   only the one needs to account for g_rd_latency>0, the other can use 0.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity common_mem_demux is
  generic (
    g_nof_mosi    : positive := 256;  -- Number of memory interfaces in the array.
    g_mult_addr_w : positive := 8;  -- Address width of each memory-interface element in the muliplexed array.
    g_rd_latency  : natural := 0
  );
  port (
    clk      : in  std_logic := '0';  -- only used when g_rd_latency > 0
    mosi_arr : in  t_mem_mosi_arr(g_nof_mosi - 1 downto 0);
    miso_arr : out t_mem_miso_arr(g_nof_mosi - 1 downto 0);
    mosi     : out t_mem_mosi;
    miso     : in  t_mem_miso := c_mem_miso_rst
  );
end common_mem_demux;

architecture rtl of common_mem_demux is
  constant c_index_w        : natural := ceil_log2(g_nof_mosi);
  constant c_total_addr_w   : natural := c_index_w + g_mult_addr_w;

  signal index_arr : t_natural_arr(0 to g_rd_latency);
  signal index_rw  : natural;  -- read or write access
  signal index_rd  : natural;  -- read response
begin
  gen_single : if g_nof_mosi = 1 generate
    mosi        <= mosi_arr(0);
    miso_arr(0) <= miso;
  end generate;

  gen_multiple : if g_nof_mosi > 1 generate
    -- The activated element of the array is detected here
    p_index : process(mosi_arr)
    begin
      index_arr(0) <= 0;
      for I in 0 to g_nof_mosi - 1 loop
        if mosi_arr(I).wr = '1' or mosi_arr(I).rd = '1' then
          index_arr(0) <= I;
        end if;
      end loop;
    end process;

    -- Pipeline the index of the activated element to account for the read latency
    p_clk : process(clk)
    begin
      if rising_edge(clk) then
        index_arr(1 to g_rd_latency) <= index_arr(0 to g_rd_latency - 1);
      end if;
    end process;

    index_rw <= index_arr(0);
    index_rd <= index_arr(g_rd_latency);

    -- Master access, can be write or read
    p_mosi : process(mosi_arr, index_rw)
    begin
      mosi <= mosi_arr(index_rw);
      mosi.address(c_total_addr_w - 1 downto g_mult_addr_w) <= TO_UVEC(index_rw, c_index_w);
    end process;

    -- Slave response to read access after g_rd_latency clk cycles
    p_miso : process(miso, index_rd)
    begin
      miso_arr <= (others => c_mem_miso_rst);
      miso_arr(index_rd) <= miso;
    end process;
  end generate;
end rtl;
