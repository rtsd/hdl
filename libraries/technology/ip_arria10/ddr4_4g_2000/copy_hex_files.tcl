#------------------------------------------------------------------------------
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on Qsys-generated file generated/sim/mentor/msim_setup.tcl

set IP_DIR "$env(HDL_BUILD_DIR)/unb2/qsys-generate/sim"

# Copy ROM/RAM files to simulation directory
if {[file isdirectory $IP_DIR]} {
    file copy -force $IP_DIR/../altera_emif_arch_nf_150/sim/ip_arria10_ddr4_4g_2000_altera_emif_arch_nf_150_mhuabmq_seq_cal_sim.hex ./
    file copy -force $IP_DIR/../altera_emif_arch_nf_150/sim/ip_arria10_ddr4_4g_2000_altera_emif_arch_nf_150_mhuabmq_seq_cal_synth.hex ./
    file copy -force $IP_DIR/../altera_emif_arch_nf_150/sim/ip_arria10_ddr4_4g_2000_altera_emif_arch_nf_150_mhuabmq_seq_params_sim.hex ./
    file copy -force $IP_DIR/../altera_emif_arch_nf_150/sim/ip_arria10_ddr4_4g_2000_altera_emif_arch_nf_150_mhuabmq_seq_params_synth.hex ./
}
