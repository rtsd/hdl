README.txt for $HDL_WORK/libraries/technology/ip_arria10/phy_10gbase_r

1) Porting
2) IP component
3) Compilation, simulation and verification
4) Synthesis
5) Remarks


1) Porting

The phy_10gbase_r IP is new for Arria10. However it serves the similar purpose as the phy_xaui IP for Stratix IV.
There are several Qsys variants for the phy_10gbase_r IP:

  - ip_arria10_phy_10gbase_r.qsys    generates a component with  1 10GBASE_R transceiver.
  - ip_arria10_phy_10gbase_r_24.qsys generates a component with 24 10GBASE_R transceivers.
  - ip_arria10_phy_10gbase_r_48.qsys generates a component with 48 10GBASE_R transceivers.


2) IP component

The generated IP is not kept in SVN, only the Qsys source file:

  ip_arria10_phy_10gbase_r.qsys

Therefore first the IP needs to be generated using:

  generate_ip.sh


3) Compilation, simulation and verification

The genrated IP also contains a msim_setup.tcl file that was used to manually create:

  compile_ip.tcl
  
This compile_ip.tcl is in the hdllib.cfg and gets compiled before the other code.


4) Synthesis

No synthesis trials were done, because this will implicitely be done when the IP is used in a design. The QIP file:

  ip_arria10_phy_10gbase_r.qip

is included in the hdllib.cfg and contains what is needed to synthesize the IP.


5) Remarks

a) Use generated IP specific library clause name

  The generated ip_arria10_<lib_name>.vhd uses an IP specific library name. Therefore the hdllib.cfg uses the IP
  specific library as library claus name to make it known:
  
    hdl_lib_name = ip_arria10_<lib_name>
    hdl_library_clause_name = ip_arria10_<lib_name>_<ip_specific>
    
 