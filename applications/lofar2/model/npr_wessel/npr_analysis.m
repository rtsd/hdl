
%NPR_ANALYSIS Near perfect reconstruction analysis filter bank.
%  Y = NPR_ANALYSIS(COEFF,X) separates the input signal X into channels.
%  Each channel is a row in Y. COEFF is a two dimensional array containing
%  the filter coefficients. The number of rows in Y will be twice the
%  number of rows in COEFF.
%
%  See also npr_synthesis, npr_coeff.
%
% (c) 2007 - 2022 Wessel Lubberhuizen
function y = npr_analysis(coeff,x)
% number of channels
N=size(coeff,1);
% number of slices
M=ceil(length(x)/N);
% number of samples
L=M*N;
% create polyphase input signals
x1=reshape(x,N,M);
% apply frequency shift
phi=mod(0:L-1,2*N)*(pi/N);
x2=x.*exp(1i*phi);
x2=reshape(x2,N,M);
% apply channel filters
coeff = fliplr(coeff);
for i=1:N
    x1(i,:) = filter(coeff(i,:),1,x1(i,:));
    x2(i,:) = filter(coeff(i,:),1,x2(i,:));
end
% apply dft
y1 = ifft(x1,[],1)*N;
y2 = ifft(x2,[],1)*N;
% assemble even and odd channels
y = [reshape(y1,1,N,M); reshape(y2,1,N,M)];
y = reshape(y,2*N,M);
