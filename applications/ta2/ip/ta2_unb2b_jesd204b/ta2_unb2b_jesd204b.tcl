post_message "Running ta2_unb2b_jesd204b script"
set radiohdl_build $::env(HDL_BUILD_DIR)
#============================================================
# Files and basic settings
#============================================================

# Local HDL files
set_global_assignment -name VHDL_FILE ip/ta2_unb2b_jesd204b/ta2_unb2b_jesd204b.vhd

# All used HDL library *_lib.qip files in order, copied from ta2_unb2b_jesd204b.qsf in RadioHDL build directory. 
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/technology/technology_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_ram/ip_arria10_e1sg_ram_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_memory/tech_memory_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_fifo/ip_arria10_e1sg_fifo_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_fifo/tech_fifo_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_ddio/ip_arria10_e1sg_ddio_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_iobuf/tech_iobuf_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tst/tst_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/common/common_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/mm/mm_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_mult/ip_arria10_mult_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_complex_mult_rtl/ip_arria10_complex_mult_rtl_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_complex_mult_rtl_canonical/ip_arria10_complex_mult_rtl_canonical_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_complex_mult/ip_arria10_e1sg_complex_mult_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_mult_add4/ip_arria10_e1sg_mult_add4_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_mult_add2/ip_arria10_e1sg_mult_add2_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_mult/tech_mult_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/common_mult/common_mult_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/easics/easics_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/dp/dp_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_pll/tech_pll_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_jesd204b/ip_arria10_e1sg_jesd204b_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_jesd204b/tech_jesd204b_lib.qip"
