-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose:
--   Derive a new block and sync interval from an input bs_sosi stream, and
--   use the Raw sample Sequence Number (RSN) as rs_sosi.bsn according to the
--   PPS and RSN grid defined in [1].
-- Description:
--   The BSN in the input bs_sosi counts blocks of g_bs_block_size samples
--   since t_epoch = 1 Jan 1970 [1]. The RSN can start at any BSN, using
--   RSN = BSN * g_bs_block_size as initial value. The block size
--   g_rs_block_size of the RSN is free to choose and will typically differ
--   from the BSN block size g_bs_block_size. Therefore counting back to
--   t_epoch the very first RSN block in the output rs_sosi will typically
--   not start at t_epoch. The RSN will start at 0 at t_epoch, because it
--   counts samples. Therefore choose to let the bsn field in the rs_sosi
--   record carry the RSN of the first sample in the block, instead of
--   counting blocks. The RSN thus increments by g_rs_block_size (instead of
--   by 1) for every block.
-- Features:
-- . The rs_sosi timing is derived from the input bs_sosi, but the rs_sosi
--   uses RSN as rs_sosi.bsn and has its own block and sync intervals.
-- . Assumption is that every clk cycle carries valid data when the
--   bs_sosi.valid is '1', so no gaps in bs_sosi.valid.
-- . The bs_sosi can stop and restart. It (re)starts with bs_sosi.sync and
--   bs_sosi.sop when bs_sosi.valid becomes '1' and it stops when
--   bs_sosi.valid becomes '0' immediately after a bs_sosi.eop.
-- . The rs_sosi block size is g_rs_block_size.
-- . The rs_sosi sync interval has g_nof_clk_per_sync. If g_nof_clk_per_sync
--   is not a multiple of g_rs_block_size, then the rs_sosi.sync will occur at
--   the next start of block.
-- . The initial RSN in rs_sosi.bsn is bs_sosi.bsn * g_bs_block_size, where
--   g_bs_block_size is the input block size of bs_sosi.
-- . The rs_sosi starts when the bs_sosi starts, so when bs_sosi.sync = '1'
--   and bs_sosi.valid becomes '1'.
-- . The rs_sosi ends after the rs_sosi.eop, when the bs_sosi ends. There
--   may be undefined filler data in the last rs_sosi block to finish it
--   after the rs_sosi.eop.
-- Remark:
-- . The bs_sosi typically comes from dp_bsn_source_v2.vhd
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+Timing+in+Station

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_rsn_source is
  generic (
    g_bs_block_size     : natural := 256;  -- input bs_sosi block size, >= 3, see state machine
    g_rs_block_size     : natural := 256;  -- output rs_sosi block size, >= 3, see state machine
    g_nof_clk_per_sync  : natural := 200 * 10**6;
    g_bsn_w             : natural := 64
  );
  port (
    rst                 : in  std_logic;
    clk                 : in  std_logic;

    -- Input stream sosi control using BSN
    bs_sosi             : in  t_dp_sosi;  -- input reference stream using BSN

    -- Output stream sosi control using RSN
    nof_clk_per_sync    : in  std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(g_nof_clk_per_sync, c_word_w);
    rs_sosi             : out t_dp_sosi;  -- output stream using RSN and g_rs_block_size, g_nof_clk_per_sync
    rs_restart          : out std_logic;  -- = rs_sosi.sync for first sync after bs_sosi.valid went high
    rs_new_interval     : out std_logic  -- = active during first rs_sosi.sync interval
  );
end dp_rsn_source;

architecture rtl of dp_rsn_source is
  constant c_bs_block_size_cnt_w : natural := ceil_log2(g_bs_block_size) + 1;  -- +1 because value 2**n requires n + 1 bits
  constant c_rs_block_size_cnt_w : natural := ceil_log2(g_rs_block_size) + 1;  -- +1 because value 2**n requires n + 1 bits
  constant c_rsn_product_w       : natural := g_bsn_w + c_bs_block_size_cnt_w;

  type t_state_enum is (s_off, s_on_sop, s_on, s_on_eop);

  signal state       : t_state_enum;
  signal nxt_state   : t_state_enum;
  signal prev_state  : t_state_enum;

  signal rsn                 : std_logic_vector(c_rsn_product_w - 1 downto 0);

  signal nxt_sync            : std_logic;
  signal sync                : std_logic;
  signal sync_size_cnt       : std_logic_vector(c_word_w - 1 downto 0);
  signal nxt_sync_size_cnt   : std_logic_vector(c_word_w - 1 downto 0);

  signal rs_block_size_cnt      : std_logic_vector(c_rs_block_size_cnt_w - 1 downto 0);
  signal nxt_rs_block_size_cnt  : std_logic_vector(c_rs_block_size_cnt_w - 1 downto 0);

  signal i_rs_sosi           : t_dp_sosi := c_dp_sosi_init;
  signal nxt_rs_sosi         : t_dp_sosi;

  signal i_rs_restart        : std_logic;
  signal nxt_rs_restart      : std_logic;
  signal i_rs_new_interval   : std_logic;
  signal reg_rs_new_interval : std_logic;
begin
  rs_sosi <= i_rs_sosi;
  rs_restart <= i_rs_restart;
  rs_new_interval <= i_rs_new_interval;

  rsn <= MULT_UVEC(bs_sosi.bsn(g_bsn_w - 1 downto 0), TO_UVEC(g_bs_block_size, c_bs_block_size_cnt_w));

  p_state : process(bs_sosi, nxt_sync, sync, sync_size_cnt, nof_clk_per_sync,
                    state, prev_state,
                    i_rs_sosi, rs_block_size_cnt, rsn)
  begin
    -- Maintain sync_size_cnt for nof_clk_per_sync
    -- . nof_clk_per_sync is the number of clk per sync interval and the
    --   average number of clk per rs_sosi.sync interval, due to that the
    --   rs_sosi.sync has to occur at the rs_sosi.sop of a block.
    -- . The sync_size_cnt is started in s_off at the bs_sosi.sync.
    -- . The sync interval is nof_clk_per_sync, so when the sync_size_cnt
    --   wraps, then the sync is used to ensure that rs_sosi.sync will
    --   pulse at the rs_sosi.sop of the next block.
    nxt_sync <= sync;
    nxt_sync_size_cnt <= INCR_UVEC(sync_size_cnt, 1);
    if unsigned(sync_size_cnt) = unsigned(nof_clk_per_sync) - 1 then
      nxt_sync <= '1';  -- will set rs_sosi.sync on next rs_sosi.sop
      nxt_sync_size_cnt <= (others => '0');
    end if;
    if i_rs_sosi.sync = '1' then
      nxt_sync <= '0';  -- clear when sync has been applied in rs_sosi
    end if;

    -- State machine for rs_sosi
    nxt_state                   <= state;
    nxt_rs_sosi                 <= i_rs_sosi;  -- hold rs_sosi.bsn
    nxt_rs_sosi.sync            <= '0';
    nxt_rs_sosi.valid           <= '0';
    nxt_rs_sosi.sop             <= '0';
    nxt_rs_sosi.eop             <= '0';
    nxt_rs_block_size_cnt       <= rs_block_size_cnt;

    case state is
      when s_off =>
        nxt_rs_sosi <= c_dp_sosi_rst;
        nxt_rs_sosi.bsn <= RESIZE_DP_BSN(rsn);  -- RSN fits in g_bsn_w
        nxt_sync <= '0';
        nxt_sync_size_cnt <= (others => '0');
        nxt_rs_block_size_cnt <= (others => '0');
        if bs_sosi.sync = '1' then
          nxt_rs_sosi.sync <= '1';
          nxt_rs_sosi.sop <= '1';
          nxt_rs_sosi.valid <= '1';
          nxt_state <= s_on;
        end if;

      -- using separate states s_on_sop and s_on_eop instead of only
      -- s_on state and rs_block_size_cnt, cause that g_rs_block_size must be
      -- >= 3, but that is fine.
      when s_on_sop =>
        -- Start of block
        nxt_rs_sosi.sop <= '1';
        nxt_rs_sosi.valid  <= '1';
        nxt_state <= s_on;
        -- rs_block_size_cnt = 0 at rs_sosi.sop
        nxt_rs_block_size_cnt <= (others => '0');
        -- after first block, increment bsn per block
        if prev_state = s_on_eop then
          nxt_rs_sosi.bsn <= INCR_DP_BSN(i_rs_sosi.bsn, g_rs_block_size, g_bsn_w);  -- RSN
        end if;
        -- check for pending sync
        if nxt_sync = '1' then
          nxt_rs_sosi.sync <= '1';
        end if;

      when s_on =>
        -- During block
        nxt_rs_sosi.valid <= '1';
        -- rs_block_size_cnt increments to determine end of block
        nxt_rs_block_size_cnt <= INCR_UVEC(rs_block_size_cnt, 1);
        if unsigned(rs_block_size_cnt) >= g_rs_block_size - 3 then
          nxt_state <= s_on_eop;
        end if;

      when s_on_eop =>
        -- End of block
        nxt_rs_sosi.eop <= '1';
        nxt_rs_sosi.valid <= '1';
        nxt_state <= s_on_sop;
        nxt_rs_block_size_cnt <= INCR_UVEC(rs_block_size_cnt, 1);
        -- rs_block_size_cnt is dont care at at rs_sosi.eop
        -- accept dp_off after eop, to avoid fractional blocks
        if bs_sosi.valid = '0' then
          nxt_state <= s_off;
        end if;

      when others =>  -- reover from undefined state
        nxt_state <= s_off;
    end case;
  end process;

  -- rs_sosi.valid transition from 0 to 1 is a rs_restart, use nxt_rs_restart
  -- to have rs_restart at first rs_sosi.sync and rs_sosi.sop.
  nxt_rs_restart <= nxt_rs_sosi.valid and not i_rs_sosi.valid;

  i_rs_new_interval <= '1' when i_rs_restart = '1' else
                       '0' when i_rs_sosi.sync = '1' else
                       reg_rs_new_interval;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      prev_state          <= s_off;
      state               <= s_off;
      i_rs_sosi           <= c_dp_sosi_rst;
      sync_size_cnt       <= (others => '0');
      sync                <= '0';
      rs_block_size_cnt   <= (others => '0');
      i_rs_restart        <= '0';
      reg_rs_new_interval <= '0';
    elsif rising_edge(clk) then
      prev_state          <= state;
      state               <= nxt_state;
      i_rs_sosi           <= nxt_rs_sosi;
      sync_size_cnt       <= nxt_sync_size_cnt;
      sync                <= nxt_sync;
      rs_block_size_cnt   <= nxt_rs_block_size_cnt;
      i_rs_restart        <= nxt_rs_restart;
      reg_rs_new_interval <= i_rs_new_interval;
    end if;
  end process;
end rtl;
