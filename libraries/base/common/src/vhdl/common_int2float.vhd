-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose:
--   Convert signed integer to semi-floating point number.
-- Description:
--   If the output data width <= the input data width then output data MSbit is
--   a one bit exponent and the rest of the output data bits are the signed
--   mantissa. When the exponent bit is '1' then the mantissa should be
--   mutliplied with the factor 2^(input width-(output width-1)) to obtain the
--   floating point value, otherwise factor is 1. For example:
--
--   in_dat[53:0] -> out_dat[31]   = exponent bit '0' for factor 1 or '1' for
--                                   factor 2^(54-(32-1))=2^23
--                   out_dat[30:0] = signed mantissa
--
--   If the output data width > the input data width then the output data
--   exponent will be '0' and the output data will have no loss in accuracy.
-- Remark:
-- . The function uses truncation (so not rounding). For small numbers with
--   exponent bit is '0' the manitissa represents the input exactly, for
--   large numbers with exponent bit is '1' the manitissa is a truncated value
--   of the input data.
-- . The pipeline is 1 because the output is registered or when 2 then also the
--   input will be registered.

entity common_int2float is
  generic (
    g_pipeline  : natural := 1
  );
  port (
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_dat     : in   std_logic_vector;
    out_dat    : out  std_logic_vector
  );
end common_int2float;

architecture rtl of common_int2float is
  signal reg_dat      : std_logic_vector(in_dat'range);
  signal nxt_reg_dat  : std_logic_vector(in_dat'range);
  signal nxt_out_dat  : std_logic_vector(out_dat'range);
begin
  -- registers
  gen_reg_input : if g_pipeline = 2 generate
    p_reg_input : process(clk)
    begin
      if rising_edge(clk) then
        if clken = '1' then
          reg_dat <= in_dat;
        end if;
      end if;
    end process;
  end generate;

  no_reg_input : if g_pipeline = 1 generate
    reg_dat <= in_dat;
  end generate;

  assert g_pipeline = 1 or g_pipeline = 2
    report "common_int2float: pipeline value not supported"
    severity FAILURE;

  p_clk : process(clk)
  begin
    if rising_edge(clk) then
      if clken = '1' then
        out_dat <= nxt_out_dat;
      end if;
    end if;
  end process;

  map_float : if out_dat'length > in_dat'length generate
    nxt_out_dat <= '0' & RESIZE_SVEC(reg_dat, out_dat'length - 1);
  end generate;

  gen_float : if out_dat'length <= in_dat'length generate
    p_float: process (reg_dat)
    begin
      if unsigned(reg_dat(in_dat'high downto out_dat'high - 1)) = 0 or
           signed(reg_dat(in_dat'high downto out_dat'high - 1)) = -1 then
        nxt_out_dat <= '0' & reg_dat(out_dat'high - 1 downto 0);
      else
        nxt_out_dat <= '1' & reg_dat(in_dat'high downto in_dat'high - out_dat'high + 1);
      end if;
    end process;
  end generate;
end rtl;
