-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_ddreg_slv
-- Usage:
--   > as 5
--   > run -all
--
-- Description: See common_ddreg.vhd

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_ddreg_slv is
end tb_common_ddreg_slv;

architecture tb of tb_common_ddreg_slv is
  constant in_clk_period   : time :=  5 ns;

  constant c_tb_interval   : natural := 100;
  constant c_in_dat_w      : natural := 8;

  signal tb_end         : std_logic := '0';
  signal in_clk         : std_logic := '0';
  signal in_dat         : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');

  signal rst            : std_logic;
  signal out_clk        : std_logic := '0';
  signal out_dat        : std_logic_vector(2 * c_in_dat_w - 1 downto 0);
  signal out_dat_hi     : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal out_dat_lo     : std_logic_vector(c_in_dat_w - 1 downto 0);
begin
  in_clk  <= not in_clk  or tb_end after in_clk_period / 2;
  out_clk <= not out_clk when rising_edge(in_clk);

  rst <= '1', '0' after in_clk_period * 10;

  in_dat <= INCR_UVEC(in_dat, 1) when rising_edge(in_clk);

  p_in_stimuli : process
  begin
    proc_common_wait_some_cycles(out_clk, c_tb_interval);
    tb_end <= '1';
    wait;
  end process;

  u_common_ddreg_slv : entity work.common_ddreg_slv
  generic map (
    g_in_delay_len  => 1,
    g_out_delay_len => c_meta_delay_len
  )
  port map (
    in_clk      => in_clk,
    in_dat      => in_dat,
    rst         => rst,
    out_clk     => out_clk,
    out_dat_hi  => out_dat_hi,
    out_dat_lo  => out_dat_lo
  );

  out_dat <= out_dat_hi & out_dat_lo;
end tb;
