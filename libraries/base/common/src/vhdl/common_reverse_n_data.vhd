--------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Author:
-- . Eric Kooistra, 14 Feb 2023
-- Purpose:
-- . Reverse the order of time multiplexed, serial data per set of every
--   g_reverse_len data values in time.
-- Description:
-- . The implementation does:
--     serial in_data -->
--     parallel demux_data_vec -->
--     reversed parallel reverse_data_vec -->
--     serial out_data
-- . The first in_val after rst release is treated as start of the time
--   multiplexed sets of data.
-- . For example with g_reverse_len = 3, then input in_data 012_345_678
--   becomes 210_543_876 output out_data.
-- . If in_eop is used to mark the end of in_data blocks, then the in_data
--   blocks size should be an integer multiple of g_reverse_len. If after rst
--   release, the in_val is always marking all in_data block data, then it is
--   fine to leave in_eop = '0', so not connected.
-- Remark:
-- . Typically g_reverse_len should not be too large (~< 4), because then the
--   implementation takes relatively too much logic.
-- . This common_reverse_n_data.vhd is used in dp_reverse_n.vhd and verified
--   in tb_dp_reverse_n.vhd.

entity common_reverse_n_data is
  generic (
    -- Pipeline: 0 for combinatorial, > 0 for registers
    g_pipeline_demux_in  : natural := 1;  -- serial to parallel demux
    g_pipeline_demux_out : natural := 0;
    g_pipeline_mux_in    : natural := 0;  -- parallel to serial mux
    g_pipeline_mux_out   : natural := 1;
    g_reverse_len        : natural := 2;
    g_data_w             : natural := 16
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    in_data     : in  std_logic_vector(g_data_w - 1 downto 0);
    in_val      : in  std_logic;
    in_eop      : in  std_logic := '0';
    out_data    : out std_logic_vector(g_data_w - 1 downto 0);
    out_val     : out std_logic
  );
end common_reverse_n_data;

architecture str of common_reverse_n_data is
  constant c_pipeline_total : natural := g_pipeline_demux_in + g_pipeline_demux_out +
                                         g_reverse_len - 1 +
                                         g_pipeline_mux_in + g_pipeline_mux_out;

  constant c_sel_w          : natural := ceil_log2(g_reverse_len);

  signal in_sel             : std_logic_vector(c_sel_w - 1 downto 0);
  signal nxt_in_sel         : std_logic_vector(c_sel_w - 1 downto 0);

  signal demux_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal demux_data_vec     : std_logic_vector(g_reverse_len * g_data_w - 1 downto 0);
  signal demux_val_vec      : std_logic_vector(g_reverse_len - 1 downto 0);

  signal reverse_sel        : std_logic_vector(c_sel_w - 1 downto 0);
  signal reverse_data_vec   : std_logic_vector(g_reverse_len * g_data_w - 1 downto 0);
  signal reverse_val_vec    : std_logic_vector(g_reverse_len - 1 downto 0);
  signal reverse_val        : std_logic;
begin
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      in_sel <= (others => '0');
    elsif rising_edge(clk) then
      in_sel <= nxt_in_sel;
    end if;
  end process;

  p_in_sel : process(in_sel, in_val, in_eop)
  begin
    nxt_in_sel <= in_sel;

    -- Map multiplexed input to parallel demultiplexed signal index in_sel
    if in_val = '1' then
      if unsigned(in_sel) < g_reverse_len - 1 then
        nxt_in_sel <= INCR_UVEC(in_sel, 1);
      else
        nxt_in_sel <= (others => '0');
      end if;
    end if;

    -- Force that serial to parallel mapping restarts at next sop, in case
    -- in_data block length is not an integer multiple of g_reverse_len
    if in_eop = '1' then
      nxt_in_sel <= (others => '0');
    end if;
  end process;

  u_common_demultiplexer : entity work.common_demultiplexer
  generic map (
    g_pipeline_in  => g_pipeline_demux_in,
    g_pipeline_out => g_pipeline_demux_out,
    g_nof_out      => g_reverse_len,
    g_dat_w        => g_data_w
  )
  port map (
    rst         => rst,
    clk         => clk,

    in_dat      => in_data,
    in_val      => in_val,

    out_sel     => in_sel,
    out_dat     => demux_data_vec,
    out_val     => demux_val_vec
  );

  -- All g_reverse_len parts in demux_data_vec carry the same data, the
  -- demux_val_vec determines for which demux stream it is. Use demux_data
  -- only for view in wave window.
  demux_data <= demux_data_vec(g_data_w - 1 downto 0);

  -- Reverse is done by delaying the demultiplexed signal and reversing their
  -- indices. The delay per parallel signal is 2*I to reverse them in time.
  -- The latency of gen_reverse from in_val to out_val is g_reverse_len-1.
  -- The timing diagram shows how gen_reverse and u_mux_n_to_one work, without
  -- additional demux and mux pipelining to focus on the reverse function.
  --
  -- in_data               a b c d e f g h i j k l m
  --                       _________________________
  -- in_val              _|
  --
  -- in_sel                0 1 2 0 1 2 0 1 2 0 1 2 0
  --
  -- demux_data            a b c d e f g h i j k l m   = same in each demux_data_vec[]
  -- demux_data_vec[0]     a b c d e f g h i j k l m
  -- demux_data_vec[1]     a b c d e f g h i j k l m
  -- demux_data_vec[2]     a b c d e f g h i j k l m
  --                       _     _     _     _     _
  -- demux_val_vec[0]    _| |___| |___| |___| |___| |
  --                         _     _     _     _
  -- demux_val_vec[1]    ___| |___| |___| |___| |___
  --                           _     _     _     _
  -- demux_val_vec[2]    _____| |___| |___| |___| |_
  --
  -- reverse_data_vec[0]   a b c d e f g h i j k l m    = demux_data_vec[2] with 0 delay
  -- reverse_data_vec[1]       a b c d e f g h i j k l  = demux_data_vec[1] with 2 delay
  -- reverse_data_vec[2]           a b c d e f g h i j  = demux_data_vec[0] with 4 delay
  --                           _     _     _     _
  -- reverse_val_vec[0]  _____| |___| |___| |___| |___  = demux_val_vec[2] with 0 delay
  --                             _     _     _     _
  -- reverse_val_vec[1]  _______| |___| |___| |___| |_  = demux_val_vec[1] with 2 delay
  --                               _     _     _     _
  -- reverse_val_vec[2]  _________| |___| |___| |___| | = demux_val_vec[0] with 4 delay
  --                           ________________________
  -- reverse_val         _____|                         = vector_or(reverse_val_vec)
  --
  -- reverse_sel               0 1 2 0 1 2 0 1 2 0 1 2  = in_sel delayed by g_reverse_len-1
  --
  -- out_data                  c b a f e d i h g l k j
  --                           ________________________
  -- out_val             _____|
  --
  gen_reverse : for I in 0 to g_reverse_len - 1 generate
    u_reverse_data : entity work.common_pipeline
    generic map (
      g_pipeline   => 2 * I,
      g_in_dat_w   => g_data_w,
      g_out_dat_w  => g_data_w
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => demux_data_vec((g_reverse_len - 1 - I + 1) * g_data_w - 1 downto (g_reverse_len - 1 - I) * g_data_w),
      out_dat => reverse_data_vec((I + 1) * g_data_w - 1 downto I * g_data_w)
    );

    u_reverse_val : entity work.common_pipeline_sl
    generic map (
      g_pipeline  => 2 * I
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => demux_val_vec(g_reverse_len - 1 - I),
      out_dat => reverse_val_vec(I)
    );
  end generate;

  reverse_val <= vector_or(reverse_val_vec);

  -- pipeline in_sel to align reverse_sel to reverse_data_vec and reverse_val_vec
  u_pipe_sel : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline_demux_in + g_pipeline_demux_out + g_reverse_len - 1,
    g_in_dat_w  => c_sel_w,
    g_out_dat_w => c_sel_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sel,
    out_dat => reverse_sel
  );

  u_common_multiplexer : entity work.common_multiplexer
  generic map (
    g_pipeline_in  => g_pipeline_mux_in,
    g_pipeline_out => g_pipeline_mux_out,
    g_nof_in       => g_reverse_len,
    g_dat_w        => g_data_w
  )
  port map (
    rst         => rst,
    clk         => clk,

    in_sel      => reverse_sel,
    in_dat      => reverse_data_vec,
    in_val      => reverse_val,

    out_dat     => out_data,
    out_val     => out_val  -- = in_val delayed by c_pipeline_total
  );
end str;
