-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Purpose: Verify different architectures of common_complex_mult
-- Description:
--   p_verify verifies that the instances of common_complex_mult all yield the
--   expected results and ASSERTs an ERROR in case they differ.
-- Usage:
-- > as 12
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

library IEEE, common_lib, technology_lib, tech_mult_lib, ip_stratixiv_mult_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_common_complex_mult is
  generic (
    g_variant          : string := "IP";  -- "RTL" or "IP"
    g_in_dat_w         : natural := 18;
    g_conjugate_b      : boolean := true;  -- When FALSE p = a * b, else p = a * conj(b)
    g_pipeline_input   : natural := 1;
    g_pipeline_product : natural := 0;
    g_pipeline_adder   : natural := 1;
    g_pipeline_output  : natural := 1
  );
end tb_common_complex_mult;

architecture tb of tb_common_complex_mult is
  constant clk_period        : time := 10 ns;
  constant c_pipeline        : natural := g_pipeline_input + g_pipeline_product + g_pipeline_adder + g_pipeline_output;

  -- g_in_dat_w*2 for multiply and +1 for adder to fit largest im product value
  constant c_out_dat_w       : natural := g_in_dat_w * 2 + 1;

  constant c_max             : integer :=  2**(g_in_dat_w - 1) - 1;
  constant c_min             : integer := -2**(g_in_dat_w - 1);

  constant c_small           : integer := smallest(5, c_max);

  constant c_technology      : natural := c_tech_select_default;

  signal tb_end              : std_logic := '0';
  signal rst                 : std_logic;
  signal clk                 : std_logic := '0';

  signal random              : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences

  -- Input
  signal in_ar               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_ai               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_br               : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_bi               : std_logic_vector(g_in_dat_w - 1 downto 0);

  signal in_val              : std_logic;  -- in_val is only passed on to out_val, not used by multiplier itself

  -- Results
  -- . expected valid and DUT valid, only depends on in_val and c_pipeline
  signal result_val_expected : std_logic;
  signal result_val_dut      : std_logic;

  -- . product result
  signal ref_result_re       : std_logic_vector(c_out_dat_w - 1 downto 0);  -- combinatorial result
  signal ref_result_im       : std_logic_vector(c_out_dat_w - 1 downto 0);
  signal result_re_expected  : std_logic_vector(c_out_dat_w - 1 downto 0);  -- pipelined results
  signal result_re_dut       : std_logic_vector(c_out_dat_w - 1 downto 0);
  signal result_im_expected  : std_logic_vector(c_out_dat_w - 1 downto 0);
  signal result_im_dut       : std_logic_vector(c_out_dat_w - 1 downto 0);

  -- Debug signals to view constant values in wave window
  signal dbg_variant         : string(g_variant'range) := g_variant;
  signal dbg_in_dat_w        : natural := g_in_dat_w;
  signal dbg_conjugate_b     : boolean := g_conjugate_b;
  signal dbg_pipeline        : natural := c_pipeline;
begin
  clk <= (not clk) or tb_end after clk_period / 2;

  random <= func_common_random(random) when rising_edge(clk);

  in_val <= random(random'high);

  -- run -all
  p_in_stimuli : process
  begin
    rst <= '1';
    in_ar <= TO_SVEC(0, g_in_dat_w);
    in_br <= TO_SVEC(0, g_in_dat_w);
    in_ai <= TO_SVEC(0, g_in_dat_w);
    in_bi <= TO_SVEC(0, g_in_dat_w);
    wait until rising_edge(clk);
    proc_common_wait_some_cycles(clk, 10);
    rst <= '0';
    proc_common_wait_some_cycles(clk, 10);

    -- Some more special combinations with max and min
    in_ar <= TO_SVEC( c_max, g_in_dat_w);  -- p*p - p*p + j ( p*p + p*p) = 0 + j 2pp  or  p*p + p*p + j (-p*p + p*p) = 2pp + j 0
    in_ai <= TO_SVEC( c_max, g_in_dat_w);
    in_br <= TO_SVEC( c_max, g_in_dat_w);
    in_bi <= TO_SVEC( c_max, g_in_dat_w);
    wait until rising_edge(clk);

    -- Skip (c_min + j*c_min) * (c_min + j*c_min), because that just does not fit in 2*g_in_dat_w bits of complex multiplier
    --in_ar <= TO_SVEC( c_min, g_in_dat_w);
    --in_ai <= TO_SVEC( c_min, g_in_dat_w);
    --in_br <= TO_SVEC( c_min, g_in_dat_w);
    --in_bi <= TO_SVEC( c_min, g_in_dat_w);
    --WAIT UNTIL rising_edge(clk);

    if dbg_conjugate_b = false then
      in_ar <= TO_SVEC( c_max, g_in_dat_w);
      in_ai <= TO_SVEC( c_max, g_in_dat_w);
      in_br <= TO_SVEC( c_min, g_in_dat_w);
      in_bi <= TO_SVEC( c_min, g_in_dat_w);
    else
      in_ar <= TO_SVEC( c_min, g_in_dat_w);
      in_ai <= TO_SVEC( c_min, g_in_dat_w);
      in_br <= TO_SVEC( c_max, g_in_dat_w);
      in_bi <= TO_SVEC( c_max, g_in_dat_w);
    end if;

    wait until rising_edge(clk);
    in_ar <= TO_SVEC( c_max, g_in_dat_w);
    in_ai <= TO_SVEC( c_max, g_in_dat_w);
    in_br <= TO_SVEC(-c_max, g_in_dat_w);
    in_bi <= TO_SVEC(-c_max, g_in_dat_w);
    wait until rising_edge(clk);
    in_ar <= TO_SVEC( c_min, g_in_dat_w);
    in_ai <= TO_SVEC( c_min, g_in_dat_w);
    in_br <= TO_SVEC(-c_max, g_in_dat_w);
    in_bi <= TO_SVEC(-c_max, g_in_dat_w);
    wait until rising_edge(clk);
    in_ar <= TO_SVEC(0, g_in_dat_w);
    in_br <= TO_SVEC(0, g_in_dat_w);
    in_ai <= TO_SVEC(0, g_in_dat_w);
    in_bi <= TO_SVEC(0, g_in_dat_w);
    wait until rising_edge(clk);
    proc_common_wait_some_cycles(clk, 10);

    -- Some special combinations close to max, min
    in_ar <= TO_SVEC( c_max,   g_in_dat_w);
    in_ai <= TO_SVEC( c_max - 1, g_in_dat_w);
    in_br <= TO_SVEC( c_max - 2, g_in_dat_w);
    in_bi <= TO_SVEC( c_max - 3, g_in_dat_w);
    wait until rising_edge(clk);
    in_ar <= TO_SVEC( c_min,   g_in_dat_w);
    in_ai <= TO_SVEC( c_min + 1, g_in_dat_w);
    in_br <= TO_SVEC( c_min + 2, g_in_dat_w);
    in_bi <= TO_SVEC( c_min + 3, g_in_dat_w);
    wait until rising_edge(clk);
    in_ar <= TO_SVEC( c_max,   g_in_dat_w);
    in_ai <= TO_SVEC( c_max - 1, g_in_dat_w);
    in_br <= TO_SVEC( c_min + 2, g_in_dat_w);
    in_bi <= TO_SVEC( c_min + 3, g_in_dat_w);
    wait until rising_edge(clk);
    in_ar <= TO_SVEC( c_max,   g_in_dat_w);
    in_ai <= TO_SVEC( c_max - 1, g_in_dat_w);
    in_br <= TO_SVEC(-c_max + 2, g_in_dat_w);
    in_bi <= TO_SVEC(-c_max + 3, g_in_dat_w);
    wait until rising_edge(clk);
    in_ar <= TO_SVEC( c_min,   g_in_dat_w);
    in_ai <= TO_SVEC( c_min + 1, g_in_dat_w);
    in_br <= TO_SVEC(-c_max + 2, g_in_dat_w);
    in_bi <= TO_SVEC(-c_max + 3, g_in_dat_w);
    wait until rising_edge(clk);
    in_ar <= TO_SVEC(0, g_in_dat_w);
    in_br <= TO_SVEC(0, g_in_dat_w);
    in_ai <= TO_SVEC(0, g_in_dat_w);
    in_bi <= TO_SVEC(0, g_in_dat_w);
    wait until rising_edge(clk);
    proc_common_wait_some_cycles(clk, 100);

    -- Try all small combinations
    for I in - c_small to c_small loop
      for J in - c_small to c_small loop
        for K in - c_small to c_small loop
          for L in - c_small to c_small loop
            in_ar <= TO_SVEC(I, g_in_dat_w);
            in_ai <= TO_SVEC(K, g_in_dat_w);
            in_br <= TO_SVEC(J, g_in_dat_w);
            in_bi <= TO_SVEC(L, g_in_dat_w);
            wait until rising_edge(clk);
          end loop;
          proc_common_wait_some_cycles(clk, 10);
        end loop;
        proc_common_wait_some_cycles(clk, 100);
      end loop;
      proc_common_wait_some_cycles(clk, 1000);
    end loop;

    tb_end <= '1';
    wait;
  end process;

  -- Expected combinatorial complex multiply ref_result
  ref_result_re <= func_complex_multiply(in_ar, in_ai, in_br, in_bi, g_conjugate_b, "RE", c_out_dat_w);
  ref_result_im <= func_complex_multiply(in_ar, in_ai, in_br, in_bi, g_conjugate_b, "IM", c_out_dat_w);

  u_result_re : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => c_out_dat_w,
    g_out_dat_w      => c_out_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => ref_result_re,
    out_dat => result_re_expected
  );

  u_result_im : entity common_lib.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => c_out_dat_w,
    g_out_dat_w      => c_out_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => ref_result_im,
    out_dat => result_im_expected
  );

  u_result_val_expected : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline    => c_pipeline,
    g_reset_value => 0
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => in_val,
    out_dat => result_val_expected
  );

  u_dut : entity work.common_complex_mult
  generic map (
    g_technology       => c_technology,
    g_variant          => g_variant,
    g_in_a_w           => g_in_dat_w,
    g_in_b_w           => g_in_dat_w,
    g_out_p_w          => c_out_dat_w,
    g_conjugate_b      => g_conjugate_b,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_adder   => g_pipeline_adder,
    g_pipeline_output  => g_pipeline_output
  )
  port map (
    rst        => rst,
    clk        => clk,
    clken      => '1',
    in_ar      => in_ar,
    in_ai      => in_ai,
    in_br      => in_br,
    in_bi      => in_bi,
    in_val     => in_val,
    out_pr     => result_re_dut,
    out_pi     => result_im_dut,
    out_val    => result_val_dut
  );

  p_verify : process(rst, clk)
  begin
    if rst = '0' then
      if rising_edge(clk) then
        assert result_re_dut = result_re_expected
          report "Error: RE wrong result, " & slv_to_str(result_re_dut) & " /= " & slv_to_str(result_re_expected)
          severity ERROR;
        assert result_im_dut = result_im_expected
          report "Error: IM wrong result, " & slv_to_str(result_im_dut) & " /= " & slv_to_str(result_im_expected)
          severity ERROR;
        assert result_val_dut = result_val_expected
          report "Error: VAL wrong result"
          severity ERROR;
      end if;
    end if;
  end process;
end tb;
