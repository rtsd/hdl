#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

""" Functional test case for the wpfb_unit entity.
    This testcase should be used to test the different types of configuration
    of the wpfb unit, meaning both wide- and narrowband configurations. 
    
    Wideband configuration: g_wb_factor > 1 and g_nof_chan = 0
    Narrowband configuration: g_nof_chan > 0 and g_wb_factor = 1
    
    Both aforementioned configurations can be parallelized using 
    the g_nof_wb_streams generic.
    
    The testcase applies the same input stimuli to all inputs and checks
    if all generated spectrums are equal. 
    
   Usage:

   > python tc_mmf_wpfb_unit_functional.py --unb 0 --bn 0 --sim 


"""

###############################################################################
# System imports
import test_case
import node_io
import pi_diag_block_gen
import pi_diag_data_buffer
import pi_fil_ppf_w
import dsp_test
import sys, os
import subprocess
import pylab as pl
import numpy as np
import scipy as sp
import random
from tools import *
from common import *

# Create a test case object
tc = test_case.Testcase('TB - ', '')

# Constants/Generics that are shared between VHDL and Python
# Name                   Value   Default   Description
# START_VHDL_GENERICS
g_wb_factor        = 1              # The Wideband factor (must be power of 2)
g_nof_wb_streams   = 8              # The number of parallel wideband streams 
g_nof_chan         = 1              # The exponent (for 2) that defines the number of time-multiplexed input channels
g_nof_points       = 64             # The size of the FFT
g_nof_taps         = 8              # The number of taps in the filter
g_in_dat_w         = 6              # Input width of the FFT
g_out_dat_w        = 16             # Output width of the FFT
g_use_separate     = False          # When False: FFT input is considered Complex. When True: FFT input is 2xReal
g_nof_blocks       = 8              # The number of FFT blocks that are simulated per BG period # (must be >= 1 and power of 2 due to that BG c_bg_ram_size must be > 1 and power of 2 to avoid unused addresses)
# END_VHDL_GENERICS

# Overwrite generics with argumented generics from autoscript or command line. 
if tc.generics != None:  
    g_wb_factor      = tc.generics['g_wb_factor']
    g_nof_wb_streams = tc.generics['g_nof_wb_streams']
    g_nof_chan       = tc.generics['g_nof_chan']      
    g_nof_points     = tc.generics['g_nof_points']    
    g_nof_taps       = tc.generics['g_nof_taps']      
    g_in_dat_w       = tc.generics['g_in_dat_w']      
    g_out_dat_w      = tc.generics['g_out_dat_w']     
    g_use_separate   = tc.generics['g_use_separate']  
    g_nof_blocks     = tc.generics['g_nof_blocks']    

tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test bench for wpfb_unit entity with %d points and %d taps' % (g_nof_points, g_nof_taps))
tc.append_log(1, '>>>         This is a functional test bench that can be used to test the' )
tc.append_log(1, '>>>         differesnt types of configuaration. ' )
tc.append_log(3, '>>>')
tc.append_log(3, '')
tc.set_result('PASSED')

if g_nof_chan != 0 and g_wb_factor != 1 :
    g_nof_chan = 0
    tc.append_log(2, 'WARNING: Forced g_nof_chan=0 because g_wb_factor is not 1. When wb_factor > 1 it is not ')
    tc.append_log(2, '         possible to use time-multiplexed input channels.')
    
tc.append_log(3, '')
tc.append_log(3, '>>> VHDL generic settings:')
tc.append_log(3, '')
tc.append_log(3, ' g_wb_factor      = %d' % g_wb_factor     )
tc.append_log(3, ' g_nof_wb_streams = %d' % g_nof_wb_streams)
tc.append_log(3, ' g_nof_chan       = %d' % g_nof_chan      )
tc.append_log(3, ' g_nof_points     = %d' % g_nof_points    )
tc.append_log(3, ' g_nof_taps       = %d' % g_nof_taps      )
tc.append_log(3, ' g_in_dat_w       = %d' % g_in_dat_w      )
tc.append_log(3, ' g_out_dat_w      = %d' % g_out_dat_w     )
tc.append_log(3, ' g_use_separate   = %s' % g_use_separate  )
tc.append_log(3, ' g_nof_blocks     = %d' % g_nof_blocks    )
tc.append_log(3, '')

# Python waveform constants
c_real_select        = 'sine'     # select waveform: 'sine', 'square', 'impulse', 'noise', 'gaussian', 'zero' (default)
c_real_ampl          = 1.0        # normalized full scale amplitude for sine, square, impulse and noise input
c_real_phase         = 0.0        # sine phase in degrees
c_real_freq          = 2          # nof periods per c_nof_points
c_real_index         = 1          # time index within c_nof_points
c_real_noiselevel    = 1.0        # added ADC noise level in units of 1 bit
c_real_seed          = 1          # random seed for noise signal

c_imag_select        = 'gaussian' # select waveform: 'sine', 'square', 'impulse', 'noise', 'gaussian', 'zero' (default)
c_imag_ampl          = 1.0
c_imag_ampl          = 0.0        # ... comment this line to enable gaussian
c_imag_phase         = 45.0
c_imag_freq          = 5
c_imag_index         = 1
c_imag_noiselevel    = 0.0
c_imag_seed          = 2

tc.append_log(3, '')
tc.append_log(3, '>>> Waveform settings:')
tc.append_log(3, '')
tc.append_log(3, ' c_real_select     = %s'    % c_real_select)
tc.append_log(3, ' c_real_ampl       = %7.3f' % c_real_ampl)
tc.append_log(3, ' c_real_phase      = %7.3f' % c_real_phase)
tc.append_log(3, ' c_real_freq       = %7.3f' % c_real_freq)
tc.append_log(3, ' c_real_index      = %d'    % c_real_index)
tc.append_log(3, ' c_real_noiselevel = %7.3f' % c_real_noiselevel)
tc.append_log(3, ' c_real_seed       = %7.3f' % c_real_seed)
tc.append_log(3, '')
tc.append_log(3, ' c_imag_select     = %s'    % c_imag_select)
tc.append_log(3, ' c_imag_ampl       = %7.3f' % c_imag_ampl)
tc.append_log(3, ' c_imag_phase      = %7.3f' % c_imag_phase)
tc.append_log(3, ' c_imag_freq       = %7.3f' % c_imag_freq)
tc.append_log(3, ' c_imag_index      = %d'    % c_imag_index)
tc.append_log(3, ' c_imag_noiselevel = %7.3f' % c_imag_noiselevel)
tc.append_log(3, ' c_imag_seed       = %7.3f' % c_imag_seed)
tc.append_log(3, '')


# Python specific constants
c_nof_input_channels = 2**g_nof_chan
c_nof_streams        = g_nof_wb_streams*c_nof_input_channels
c_nof_bg_streams     = g_nof_wb_streams*g_wb_factor
c_frame_size         = c_nof_input_channels*g_nof_points/g_wb_factor         
c_nof_integrations   = g_nof_blocks                            # The number of spectrums that are averaged, must be <= c_nof_blocks
c_channel_length     = g_nof_blocks*g_nof_points               # number of time series samples per input channel
c_bg_ram_size        = g_nof_blocks*c_frame_size
c_stimuli_length     = c_nof_input_channels*c_channel_length   # total number of time series samples for the BG that generates all channels
c_integration_length = c_nof_integrations*g_nof_points         # number of time series samples per input channel that are used for integration
c_blocks_per_sync    = g_nof_taps+4                            # choose sync interval little bit longer than the WPFB impulse response

c_dp_clk_period_ns   = 5
c_subband_period_ns  = c_frame_size * c_dp_clk_period_ns       # Subband period in ns = BSN block time in ns
c_sync_interval_ns   = c_subband_period_ns*c_blocks_per_sync   # sync interval in ns


# Create access object for nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

# Create block generator instance
bg = pi_diag_block_gen.PiDiagBlockGen(tc, io, c_nof_bg_streams, ramSizePerChannel=c_bg_ram_size)

# Create databuffer instances
db_re = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = 'REAL', nofStreams=c_nof_bg_streams, ramSizePerStream=c_stimuli_length/g_wb_factor)
db_im = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = 'IMAG', nofStreams=c_nof_bg_streams, ramSizePerStream=c_stimuli_length/g_wb_factor)

# Create dsp_test instance for helpful methods
dsp_test = dsp_test.DspTest(g_nof_points, g_in_dat_w, g_out_dat_w)

def gen_filter_hex_files(c_nof_taps = 4, c_wb_factor = 4, c_nof_points = 16):
    # Create mif files for coefficient memories. 
    create_mifs_cmd = "python ../../../filter/src/python/create_mifs.py -t %d -w %d -b %d " % (c_nof_taps, c_wb_factor, c_nof_points)
    cm = subprocess.Popen(create_mifs_cmd, cwd=r'../../../filter/src/python/', shell=True, close_fds=True)
    return

if __name__ == "__main__":      
    
    ###############################################################################
    # Generate the hex files for simulation. If hex files do not yet 
    # exist it is required to start this script twice. 
    ###############################################################################
    gen_filter_hex_files(g_nof_taps, g_wb_factor, g_nof_points)
    
    ###############################################################################
    #
    # Write the Wideband Poly Phase Filter Bank input stimuli to the BG
    #
    ###############################################################################        
    
    # Prepare stimuli order for block generator
    xf_real = []
    xf_imag = []
    for i in range(c_nof_streams):
        xf_real.append(dsp_test.create_waveform(sel=c_real_select, ampl=c_real_ampl, phase=c_real_phase, freq=c_real_freq, timeIndex=c_real_index, seed=c_real_seed, noiseLevel=c_real_noiselevel, length=c_channel_length))
        xf_imag.append(dsp_test.create_waveform(sel=c_imag_select, ampl=c_imag_ampl, phase=c_imag_phase, freq=c_imag_freq, timeIndex=c_imag_index, seed=c_imag_seed, noiseLevel=c_imag_noiselevel, length=c_channel_length)) 
    
    xq_real=[]
    xq_imag=[]
    for i in range(c_nof_streams):
        xq_real.append(dsp_test.adc_quantize_waveform(xf_real[i]))
        xq_imag.append(dsp_test.adc_quantize_waveform(xf_imag[i]))
           
    streams = []
    for i in range(c_nof_streams):
        streams.append(dsp_test.concatenate_two_lists(xq_real[i], xq_imag[i], g_in_dat_w))
       
    # Apply the alternated order due to the channels and create the wb_streams 
    wb_stream = []
    for h in range(g_nof_wb_streams):
        chnl_stream = [] 
        for i in range(c_channel_length):
            for j in range(c_nof_input_channels):
                chnl_stream.append(streams[h*c_nof_input_channels + j][i])   
        wb_stream.append(chnl_stream)  
    
    # Use the wb_streams to create the actual streams for the block generator
    # Write the stimuli to the block generator
    for h in range(g_nof_wb_streams):
        for i in range(g_wb_factor):
            send_data = []   
            for j in range(c_channel_length/g_wb_factor):  
                for k in range(c_nof_input_channels):      
                    send_data.append(wb_stream[h][g_wb_factor*c_nof_input_channels*j+i*c_nof_input_channels+k])     #
            bg.write_waveform_ram(data=send_data, channelNr= h*g_wb_factor + i)

    # Write setting for the block generator:
    bg.write_block_gen_settings(samplesPerPacket=c_frame_size, blocksPerSync=c_blocks_per_sync, gapSize=0, memLowAddr=0, memHighAddr=c_bg_ram_size-1, BSNInit=0)
     
    # enable the block generator       
    bg.write_enable()
    
    # Poll the diag_data_buffer to check if the response is there.
    # Retry after 3 seconds so we don't issue too many MM reads in case of simulation.
    # do_until_ge(db_re.read_nof_words, ms_retry=3000, val=c_stimuli_length/g_wb_factor, s_timeout=3600)
    
    # Run simulator to into the second sync interval to ensure a fresh second capture in diag_data_buffer with stable WPFB output (because c_sync_interval_ns > c_wpfb_response_ns)
    cur_time = io.simIO.getSimTime()
    print "cur_time=" + str(cur_time)
    print "c_sync_interval_ns=" + str(c_sync_interval_ns)
    
    do_until_gt(io.simIO.getSimTime, cur_time[0] + c_sync_interval_ns*5, s_timeout=3600)

    
    ###############################################################################
    #
    # Read FFT output from data buffer
    #
    ###############################################################################
    
    # Read the spectrums from the databuffer
    X_real = []
    X_imag = []
    for i in range(c_nof_bg_streams):
        X_real.append(db_re.read_data_buffer(streamNr=i, n=c_stimuli_length/g_wb_factor, radix='dec', width=g_out_dat_w))
        X_imag.append(db_im.read_data_buffer(streamNr=i, n=c_stimuli_length/g_wb_factor, radix='dec', width=g_out_dat_w))
    
    # re-arrange output data  
    spectrums_real = []
    spectrums_imag = [] 
    if(g_wb_factor > 1):   # For wideband
        for h in range(g_nof_wb_streams):
            spectrum_real = []
            spectrum_imag = []
            for i in range(g_wb_factor):
                for k in range(c_channel_length/g_wb_factor):
                    spectrum_real.append(X_real[h*g_wb_factor+i][0][k])
                    spectrum_imag.append(X_imag[h*g_wb_factor+i][0][k])
            spectrums_real.append(spectrum_real)
            spectrums_imag.append(spectrum_imag)   
    else:                    # For narrowband
        for h in range(g_nof_wb_streams):
            for j in range(c_nof_input_channels):
                spectrum_real = []
                spectrum_imag = []
                for k in range(g_nof_blocks):
                    for l in range(g_nof_points):                                                         
                        spectrum_real.append(X_real[h][0][k*g_nof_points*c_nof_input_channels+j*g_nof_points+l])
                        spectrum_imag.append(X_imag[h][0][k*g_nof_points*c_nof_input_channels+j*g_nof_points+l])
                spectrums_real.append(spectrum_real)
                spectrums_imag.append(spectrum_imag)  
    
    # Check if all output spectrums are equal. 
    for i in range(g_nof_wb_streams*c_nof_input_channels): 
        for j in range(c_channel_length):
            if spectrums_real[0][j] != spectrums_real[i][j]:
               tc.append_log(2, '>>> Real part of spectrums are unequal')  
               tc.set_result('FAILED') 
            if spectrums_imag[0][j] != spectrums_imag[i][j]:      
               tc.append_log(2, '>>> Imag part of spectrums are unequal')   
               tc.set_result('FAILED')
    
    tc.append_log(5, 'spectrums_real')   
    for i in range(len(spectrums_real[0])):  
        tc.append_log(5, 'spectrums_real = %d ' % spectrums_real[0][i] ) 
    tc.append_log(5, 'spectrums_imag')
    for i in range(len(spectrums_imag[0])):
        tc.append_log(5, 'spectrums_imag = %d ' % spectrums_imag[0][i] ) 

    tc.append_log(3, '')
    tc.append_log(3, '>>>')
    tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
    tc.append_log(3, '>>>')
    
    sys.exit(tc.get_result())

