-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib, technology_lib, diag_lib, dp_lib, lofar2_ddrctrl_lib, tech_ddr_lib, lofar2_sdp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use diag_lib.diag_pkg.all;

entity lofar2_unb2c_ddrctrl is
  generic (
    g_design_name       : string  := "lofar2_unb2c_ddrctrl";
    g_design_note       : string  := "UNUSED";
    g_technology        : natural := c_tech_arria10_e2sg;
    g_sim               : boolean := false;  -- Overridden by TB
    g_sim_unb_nr        : natural := 0;
    g_sim_node_nr       : natural := 0;
    g_stamp_date        : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time        : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id       : string  := "";  -- revision id    -- set by QSF
    g_factory_image     : boolean := false;
    g_protect_addr_range: boolean := false
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGIN     : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

    QSFP_LED     : out   std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0);

    -- DDR reference clocks
    MB_I_REF_CLK  : in   std_logic := '0';  -- Reference clock for MB_I
    --MB_II_REF_CLK : IN   STD_LOGIC := '0';  -- Reference clock for MB_II

    -- SO-DIMM Memory Bank I
    MB_I_IN      : in    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    MB_I_IO      : inout t_tech_ddr4_phy_io;
    MB_I_OU      : out   t_tech_ddr4_phy_ou

    -- SO-DIMM Memory Bank II
    --MB_II_IN     : IN    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    --MB_II_IO     : INOUT t_tech_ddr4_phy_io;
    --MB_II_OU     : OUT   t_tech_ddr4_phy_ou;

  );
end lofar2_unb2c_ddrctrl;

architecture str of lofar2_unb2c_ddrctrl is
  -- Firmware version x.y
  -- If x >= 2, rom_info starts on 0x10000 and max size = 0x8192 words
  constant c_fw_version             : t_unb2c_board_fw_version := (2, 0);
  constant c_mm_clk_freq            : natural := c_unb2c_board_mm_clk_freq_125M;
  constant c_bs_block_size          : natural := 1024;
  constant c_bs_bsn_w               : natural := 64;
  constant c_nof_streams            : natural := 12;
  constant c_data_w                 : natural := 14;

  -- wg_wideband_arr
  constant c_wg_buf_directory       : string  := "data/";
  constant c_wg_buf_dat_w           : natural := 18;  -- default value of WG that fits 14 bits of ADC data
  constant c_wg_buf_addr_w          : natural := 10;  -- default value of WG for 1024 samples
  constant c_bsn_nof_clk_per_sync   : natural := sel_a_b(g_sim, 16 * c_bs_block_size, c_sdp_N_clk_per_sync);

  -- ddrctrl
  constant c_tech_ddr               : t_c_tech_ddr          := func_tech_sel_ddr(g_sim, c_tech_ddr4_sim_16k, c_tech_ddr4_8g_1600m);
  constant c_stop_percentage        : natural               := 80;

  -- diag_data_buffer
  constant c_data_type              : t_diag_data_type_enum := e_data;
  constant c_buf_nof_words          : natural               := c_bs_block_size;

  -- diag_bsn_buffer
  constant c_bsn_type               : t_diag_data_type_enum := e_real;

  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic := '0';
  signal st_pps                     : std_logic;

  signal st_rst                     : std_logic := '0';
  signal st_clk                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- FPGA sensors
  signal reg_fpga_temp_sens_mosi     : t_mem_mosi;
  signal reg_fpga_temp_sens_miso     : t_mem_miso;
  signal reg_fpga_voltage_sens_mosi  : t_mem_mosi;
  signal reg_fpga_voltage_sens_miso  : t_mem_miso;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- EPCS read
  signal reg_dpmm_data_mosi         : t_mem_mosi;
  signal reg_dpmm_data_miso         : t_mem_miso;
  signal reg_dpmm_ctrl_mosi         : t_mem_mosi;
  signal reg_dpmm_ctrl_miso         : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi         : t_mem_mosi;
  signal reg_mmdp_data_miso         : t_mem_miso;
  signal reg_mmdp_ctrl_mosi         : t_mem_mosi;
  signal reg_mmdp_ctrl_miso         : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi              : t_mem_mosi;
  signal reg_epcs_miso              : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi              : t_mem_mosi;
  signal reg_remu_miso              : t_mem_miso;

  -- Scrap RAM
  signal ram_scrap_mosi             : t_mem_mosi;
  signal ram_scrap_miso             : t_mem_miso;

  -- QSFP leds
  signal qsfp_green_led_arr         : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr           : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);

  -- bsn_source_v2
  signal  reg_bsn_source_v2_mosi    : t_mem_mosi;
  signal  reg_bsn_source_v2_miso    : t_mem_miso;
  signal  bs_sosi                   : t_dp_sosi;

  -- bsn_scheduler
  signal  reg_bsn_scheduler_mosi : t_mem_mosi;
  signal  reg_bsn_scheduler_miso : t_mem_miso;
  signal  trigger_wg                : std_logic;

  -- diag_wg_wideband_arr
  signal  reg_wg_wideband_arr_mosi               : t_mem_mosi;
  signal  reg_wg_wideband_arr_miso               : t_mem_miso;
  signal  ram_wg_wideband_arr_mosi               : t_mem_mosi;
  signal  ram_wg_wideband_arr_miso               : t_mem_miso;
  signal  wg_sosi_arr               : t_dp_sosi_arr(c_nof_streams - 1 downto 0);

  -- stop_in
  signal  reg_stop_in_mosi          : t_mem_mosi;
  signal  reg_stop_in_miso          : t_mem_miso;
  signal  stop_in_arr               : std_logic_vector(32 - 1 downto 0);

  -- ddrctrl
  signal  st_sosi_arr               : t_dp_sosi_arr(c_nof_streams - 1 downto 0);
  signal  out_siso                  : t_dp_siso                               := c_dp_siso_rst;
  signal  out_sosi_arr_ddrctrl      : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal  in_sosi_arr_data_buf      : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);

  signal  reg_ddrctrl_ctrl_state_mosi : t_mem_mosi;
  signal  reg_ddrctrl_ctrl_state_miso : t_mem_miso;
  signal  ddrctrl_ctrl_state        : std_logic_vector(32 - 1 downto 0);

  signal  reg_io_ddr_mosi           : t_mem_mosi;
  signal  reg_io_ddr_miso           : t_mem_miso;

  signal  phy3_in                   : t_tech_ddr3_phy_in;
  signal  phy3_io                   : t_tech_ddr3_phy_io;
  signal  phy3_ou                   : t_tech_ddr3_phy_ou;
  signal  phy4_io                   : t_tech_ddr4_phy_io;
  signal  phy4_ou                   : t_tech_ddr4_phy_ou;

  -- data_buffer
  signal  reg_data_buf_mosi         : t_mem_mosi;
  signal  reg_data_buf_miso         : t_mem_miso;
  signal  ram_data_buf_mosi         : t_mem_mosi;
  signal  ram_data_buf_miso         : t_mem_miso;
  signal  reg_rx_seq_data_mosi      : t_mem_mosi;
  signal  reg_rx_seq_data_miso      : t_mem_miso;
  signal  out_wr_data_done_arr      : std_logic_vector(c_nof_streams - 1 downto 0);

  -- bsn_buffer
  signal  reg_bsn_buf_mosi          : t_mem_mosi;
  signal  reg_bsn_buf_miso          : t_mem_miso;
  signal  ram_bsn_buf_mosi          : t_mem_mosi;
  signal  ram_bsn_buf_miso          : t_mem_miso;
  signal  reg_rx_seq_bsn_mosi       : t_mem_mosi;
  signal  reg_rx_seq_bsn_miso       : t_mem_miso;
  signal  out_wr_bsn_done_arr       : std_logic_vector(c_nof_streams - 1 downto 0);

  signal  mb_I_ref_rst              : std_logic;
begin
  out_siso.ready        <= vector_and(not out_wr_data_done_arr) and vector_and(not out_wr_bsn_done_arr);

  p_sosi_arr : process(out_sosi_arr_ddrctrl)
  begin
    in_sosi_arr_data_buf          <= out_sosi_arr_ddrctrl;
    store_bsn_in_re : for I in 0 to c_nof_streams - 1 loop
      in_sosi_arr_data_buf(I).re  <= out_sosi_arr_ddrctrl(I).bsn;
    end loop;
  end process;

  u_bsn_source_v2 : entity dp_lib.mms_dp_bsn_source_v2
  generic map (
    g_cross_clock_domain     => true,
    g_block_size             => c_bs_block_size,
    g_nof_clk_per_sync       => c_bsn_nof_clk_per_sync,
    g_bsn_w                  => c_bs_bsn_w
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => st_rst,
    dp_clk            => st_clk,
    dp_pps            => st_pps,

    -- Memory-mapped clock domain
    reg_mosi          => reg_bsn_source_v2_mosi,
    reg_miso          => reg_bsn_source_v2_miso,

    -- Streaming clock domain
    bs_sosi           => bs_sosi,

    bs_restart        => open
  );

  u_bsn_trigger_wg : entity dp_lib.mms_dp_bsn_scheduler
  generic map (
    g_cross_clock_domain => true,
    g_bsn_w              => c_bs_bsn_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    reg_mosi    => reg_bsn_scheduler_mosi,
    reg_miso    => reg_bsn_scheduler_miso,

    -- Streaming clock domain
    dp_rst      => st_rst,
    dp_clk      => st_clk,

    snk_in      => bs_sosi,  -- only uses eop (= block sync), bsn[]
    trigger_out => trigger_wg
  );

  u_wg_arr : entity diag_lib.mms_diag_wg_wideband_arr
  generic map (
    g_nof_streams        => c_nof_streams,
    g_cross_clock_domain => true,
    g_buf_dir            => c_wg_buf_directory,

    -- Wideband parameters
    g_wideband_factor    => 1,

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w          => c_wg_buf_dat_w,
    g_buf_addr_w         => c_wg_buf_addr_w,
    g_calc_support       => true,
    g_calc_gain_w        => 1,
    g_calc_dat_w         => c_sdp_W_adc
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,

    reg_mosi            => reg_wg_wideband_arr_mosi,
    reg_miso            => reg_wg_wideband_arr_miso,

    buf_mosi            => ram_wg_wideband_arr_mosi,
    buf_miso            => ram_wg_wideband_arr_miso,

    -- Streaming clock domain
    st_rst              => st_rst,
    st_clk              => st_clk,
    st_restart          => trigger_wg,

    out_sosi_arr        => wg_sosi_arr
  );

  gen_concat : for I in 0 to c_sdp_S_pn - 1 generate
    p_sosi : process(wg_sosi_arr(I), bs_sosi)
    begin
      st_sosi_arr(I)       <= bs_sosi;
      st_sosi_arr(I).data  <= wg_sosi_arr(I).data;
    end process;
  end generate;

  u_stop_in_reg : entity common_lib.mms_common_reg
  port map (
    -- Memory-mapped clock domain
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,

    reg_mosi            => reg_stop_in_mosi,
    reg_miso            => reg_stop_in_miso,

    -- Streaming clock domain
    st_rst              => st_rst,
    st_clk              => st_clk,

    in_reg              => stop_in_arr,
    out_reg             => stop_in_arr
  );

  u_ddrctrl_ctrl_state_reg : entity common_lib.mms_common_reg
  port map (
    -- Memory-mapped clock domain
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,

    reg_mosi            => reg_ddrctrl_ctrl_state_mosi,
    reg_miso            => reg_ddrctrl_ctrl_state_miso,

    -- Streaming clock domain
    st_rst              => st_rst,
    st_clk              => st_clk,

    in_reg              => ddrctrl_ctrl_state,
    out_reg             => open
  );

  u_ddrctrl : entity lofar2_ddrctrl_lib.ddrctrl
  generic map (
    g_tech_ddr        => c_tech_ddr,
    g_sim_model       => g_sim,
    g_technology      => g_technology,
    g_nof_streams     => c_nof_streams,
    g_data_w          => c_data_w,
    g_stop_percentage => c_stop_percentage,
    g_block_size      => c_bs_block_size
  )
  port map (
    clk               => st_clk,
    rst               => st_rst,
    ctlr_ref_clk      => MB_I_REF_CLK,
    ctlr_ref_rst      => mb_I_ref_rst,
    mm_clk            => mm_clk,
    mm_rst            => mm_rst,
    in_sosi_arr       => st_sosi_arr,
    stop_in           => stop_in_arr(0),
    out_sosi_arr      => out_sosi_arr_ddrctrl,
    out_siso          => out_siso,
    ddrctrl_ctrl_state => ddrctrl_ctrl_state,

    reg_io_ddr_mosi   => reg_io_ddr_mosi,
    reg_io_ddr_miso   => reg_io_ddr_miso,

    --PHY
    phy3_in           => phy3_in,
    phy3_io           => phy3_io,
    phy3_ou           => phy3_ou,
    phy4_in           => MB_I_IN,
    phy4_io           => MB_I_IO,
    phy4_ou           => MB_I_OU
  );

  u_diag_data_buffer : entity diag_lib.mms_diag_data_buffer_dev
  generic map (
    g_technology      => g_technology,

    -- General
    g_nof_streams     => c_nof_streams,

    -- DB settings
    g_data_type       => c_data_type,
    g_data_w          => c_word_w,
    g_buf_nof_data    => c_buf_nof_words,
    g_buf_use_sync    => false,
    g_use_steps       => false,
    g_nof_steps       => c_diag_seq_rx_reg_nof_steps
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,

    reg_data_buf_mosi => reg_data_buf_mosi,
    reg_data_buf_miso => reg_data_buf_miso,

    ram_data_buf_mosi => ram_data_buf_mosi,
    ram_data_buf_miso => ram_data_buf_miso,

    -- Streaming clock domain
    dp_rst            => st_rst,
    dp_clk            => st_clk,

    in_sync           => st_pps,
    in_sosi_arr       => in_sosi_arr_data_buf,
    out_wr_done_arr   => out_wr_data_done_arr
  );

  u_diag_bsn_buffer : entity diag_lib.mms_diag_data_buffer_dev
  generic map (
    g_technology      => g_technology,

    -- General
    g_nof_streams     => c_nof_streams,

    -- DB settings
    g_data_type       => c_bsn_type,
    g_data_w          => c_dp_stream_bsn_w,
    g_buf_nof_data    => c_buf_nof_words,
    g_buf_use_sync    => false,
    g_use_steps       => false,
    g_nof_steps       => c_diag_seq_rx_reg_nof_steps
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,

    reg_data_buf_mosi => reg_bsn_buf_mosi,
    reg_data_buf_miso => reg_bsn_buf_miso,

    ram_data_buf_mosi => ram_bsn_buf_mosi,
    ram_data_buf_miso => ram_bsn_buf_miso,

    -- Streaming clock domain
    dp_rst            => st_rst,
    dp_clk            => st_clk,

    in_sync           => st_pps,
    in_sosi_arr       => in_sosi_arr_data_buf,
    out_wr_done_arr   => out_wr_bsn_done_arr
  );

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb2c_board_lib.ctrl_unb2c_board
  generic map (
    g_sim                => g_sim,
    g_technology         => g_technology,
    g_design_name        => g_design_name,
    g_design_note        => g_design_note,
    g_stamp_date         => g_stamp_date,
    g_stamp_time         => g_stamp_time,
    g_revision_id        => g_revision_id,
    g_fw_version         => c_fw_version,
    g_mm_clk_freq        => c_mm_clk_freq,
    g_eth_clk_freq       => c_unb2c_board_eth_clk_freq_125M,
    g_aux                => c_unb2c_board_aux,
    g_factory_image      => g_factory_image,
    g_protect_addr_range => g_protect_addr_range
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => st_rst,
    dp_clk                   => st_clk,
    dp_pps                   => st_pps,
    dp_rst_in                => st_rst,
    dp_clk_in                => st_clk,

    mb_I_ref_rst             => mb_I_ref_rst,
    MB_I_REF_CLK             => MB_I_REF_CLK,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- scrap ram
    ram_scrap_mosi           => ram_scrap_mosi,
    ram_scrap_miso           => ram_scrap_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,

    -- . 1GbE Control Interface
--    ETH_clk                  => ETH_CLK(0),
--    ETH_SGIN                 => ETH_SGIN(0),
--    ETH_SGOUT                => ETH_SGOUT(0)

    ETH_clk                  => ETH_CLK(1),
    ETH_SGIN                 => ETH_SGIN(1),
    ETH_SGOUT                => ETH_SGOUT(1)
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_lofar2_unb2c_ddrctrl
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
   )
  port map(
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    -- PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- Remote Update
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- Scrap RAM
    ram_scrap_mosi           => ram_scrap_mosi,
    ram_scrap_miso           => ram_scrap_miso,

    -- bsn_source_v2
    reg_bsn_source_v2_mosi   => reg_bsn_source_v2_mosi,
    reg_bsn_source_v2_miso   => reg_bsn_source_v2_miso,

    -- bsn_scheduler
    reg_bsn_scheduler_mosi => reg_bsn_scheduler_mosi,
    reg_bsn_scheduler_miso => reg_bsn_scheduler_miso,

    -- wg_wideband_arr
    reg_wg_wideband_arr_mosi             => reg_wg_wideband_arr_mosi,
    reg_wg_wideband_arr_miso             => reg_wg_wideband_arr_miso,
    ram_wg_wideband_arr_mosi             => ram_wg_wideband_arr_mosi,
    ram_wg_wideband_arr_miso             => ram_wg_wideband_arr_miso,

    -- stop_in
    reg_stop_in_mosi        => reg_stop_in_mosi,
    reg_stop_in_miso        => reg_stop_in_miso,

    -- ddrctrl_ctrl_state
    reg_ddrctrl_ctrl_state_mosi => reg_ddrctrl_ctrl_state_mosi,
    reg_ddrctrl_ctrl_state_miso => reg_ddrctrl_ctrl_state_miso,

    -- io_ddr
    reg_io_ddr_mosi         => reg_io_ddr_mosi,
    reg_io_ddr_miso         => reg_io_ddr_miso,

    -- data_buffer
    reg_data_buf_mosi       => reg_data_buf_mosi,
    reg_data_buf_miso       => reg_data_buf_miso,
    ram_data_buf_mosi       => ram_data_buf_mosi,
    ram_data_buf_miso       => ram_data_buf_miso,
    reg_rx_seq_data_mosi    => reg_rx_seq_data_mosi,
    reg_rx_seq_data_miso    => reg_rx_seq_data_miso,

    -- bsn_buffer
    reg_bsn_buf_mosi        => reg_bsn_buf_mosi,
    reg_bsn_buf_miso        => reg_bsn_buf_miso,
    ram_bsn_buf_mosi        => ram_bsn_buf_mosi,
    ram_bsn_buf_miso        => ram_bsn_buf_miso,
    reg_rx_seq_bsn_mosi     => reg_rx_seq_bsn_mosi,
    reg_rx_seq_bsn_miso     => reg_rx_seq_bsn_miso

  );

  u_front_led : entity unb2c_board_lib.unb2c_board_qsfp_leds
  generic map (
    g_sim           => g_sim,
    g_factory_image => g_factory_image,
    g_nof_qsfp      => c_unb2c_board_tr_qsfp.nof_bus,
    g_pulse_us      => 1000 / (10**9 / c_mm_clk_freq)  -- nof clk cycles to get us period
  )
  port map (
    rst             => mm_rst,
    clk             => mm_clk,
    green_led_arr   => qsfp_green_led_arr,
    red_led_arr     => qsfp_red_led_arr
  );

  u_front_io : entity unb2c_board_lib.unb2c_board_front_io
  generic map (
    g_nof_qsfp_bus => c_unb2c_board_tr_qsfp.nof_bus
  )
  port map (
    green_led_arr => qsfp_green_led_arr,
    red_led_arr   => qsfp_red_led_arr,
    QSFP_LED      => QSFP_LED
  );
end str;
