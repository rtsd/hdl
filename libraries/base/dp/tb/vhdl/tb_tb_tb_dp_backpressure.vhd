-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Regression multi TB for DP library
-- Description:
--   All TB are self checking. If some component TB fails then simulate that TB
--   seperately to find the cause.
-- Usage:
-- > as 3
-- > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.tb_dp_pkg.all;

entity tb_tb_tb_dp_backpressure is
end tb_tb_tb_dp_backpressure;

architecture tb of tb_tb_tb_dp_backpressure is
begin
  -- Single TB
  u_tb_dp_fifo_to_mm              : entity work.tb_dp_fifo_to_mm;
  u_tb_dp_latency_adapter         : entity work.tb_dp_latency_adapter;
  u_tb_dp_shiftreg                : entity work.tb_dp_shiftreg;

  -- Multi TB
  u_tb_tb_dp_block_gen            : entity work.tb_tb_dp_block_gen;
  u_tb_tb_dp_bsn_align            : entity work.tb_tb_dp_bsn_align;
  u_tb_tb_dp_concat               : entity work.tb_tb_dp_concat;
  u_tb_tb_dp_demux                : entity work.tb_tb_dp_demux;
  u_tb_tb_dp_distribute           : entity work.tb_tb_dp_distribute;
  u_tb_tb_dp_example_dut          : entity work.tb_tb_dp_example_dut;
  u_tb_tb_dp_fifo_dc              : entity work.tb_tb_dp_fifo_dc;
  u_tb_tb_dp_fifo_dc_mixed_widths : entity work.tb_tb_dp_fifo_dc_mixed_widths;
  u_tb_tb_dp_fifo_fill            : entity work.tb_tb_dp_fifo_fill;
  u_tb_tb_dp_fifo_fill_sc         : entity work.tb_tb_dp_fifo_fill_sc;
  u_tb_tb_dp_fifo_info            : entity work.tb_tb_dp_fifo_info;
  u_tb_tb_dp_fifo_sc              : entity work.tb_tb_dp_fifo_sc;
  u_tb_tb_dp_flush                : entity work.tb_tb_dp_flush;
  u_tb_tb_dp_latency_fifo         : entity work.tb_tb_dp_latency_fifo;
  u_tb_tb_dp_mux                  : entity work.tb_tb_dp_mux;
  u_tb_tb_dp_packet               : entity work.tb_tb_dp_packet;
  u_tb_tb_dp_packet_merge         : entity work.tb_tb_dp_packet_merge;
  u_tb_tb_dp_pad_insert_remove    : entity work.tb_tb_dp_pad_insert_remove;
  u_tb_tb_dp_pipeline             : entity work.tb_tb_dp_pipeline;
  u_tb_tb_dp_pipeline_ready       : entity work.tb_tb_dp_pipeline_ready;
  u_tb_tb_dp_repack_data          : entity work.tb_tb_dp_repack_data;
  u_tb_tb_dp_split                : entity work.tb_tb_dp_split;

  -- Extra multi TB
  u_tb_tb2_dp_demux               : entity work.tb_tb2_dp_demux;
  u_tb_tb2_dp_mux                 : entity work.tb_tb2_dp_mux;

  u_tb_tb3_dp_demux               : entity work.tb_tb3_dp_demux;
  u_tb_tb3_dp_mux                 : entity work.tb_tb3_dp_mux;
end tb;
