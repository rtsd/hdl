library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_common_int2float is
end tb_common_int2float;

architecture tb of tb_common_int2float is
  constant clk_period   : time := 10 ns;

  -- use smaller values to ease use of 32 bit integers
  constant c_in_dat_w   : natural := 8;
  constant c_out_dat_w  : natural := 6;
  constant c_exp        : integer := 2**(c_in_dat_w - c_out_dat_w + 1);
  constant c_pipeline   : natural := 2;

  signal clk               : std_logic := '0';

  signal exp               : integer := c_exp;
  signal in_dat            : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal in_dat1           : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');
  signal in_dat2           : std_logic_vector(c_in_dat_w - 1 downto 0) := (others => '0');  -- delay by c_pipeline to compare with out_dat
  signal out_dat           : std_logic_vector(c_out_dat_w - 1 downto 0) := (others => '0');
  signal out_dat_exp       : std_logic;
  signal out_dat_man       : std_logic_vector(c_out_dat_w - 2 downto 0);

  signal in_val            : std_logic := '0';
  signal in_val1           : std_logic := '0';
  signal in_val2           : std_logic := '0';
  signal out_val           : std_logic := '0';

  signal in_dat_value      : integer := 0;
  signal nxt_in_dat_value  : integer;
  signal out_dat_value     : integer := 0;
  signal nxt_out_dat_value : integer;
  signal out_exp_value     : std_logic := '0';
  signal out_diff_value    : integer := 0;
begin
  clk <= not clk after clk_period / 2;

  p_dly : process(clk)
  begin
    if rising_edge(clk) then
      in_dat1 <= in_dat;
      in_dat2 <= in_dat1;
      in_val1 <= in_val;
      in_val2 <= in_val1;
      out_val <= in_val2;
    end if;
  end process;

  out_dat_exp <= out_dat(out_dat'high);
  out_dat_man <= out_dat(out_dat'high - 1 downto 0);

  p_reg : process(clk)
  begin
    if rising_edge(clk) then
      in_dat_value  <= nxt_in_dat_value;
      out_dat_value <= nxt_out_dat_value;
      out_exp_value <= out_dat_exp;
    end if;
  end process;

  nxt_in_dat_value  <= TO_SINT(in_dat2);
  nxt_out_dat_value <= TO_SINT(out_dat_man) when out_dat_exp = '0' else TO_SINT(out_dat_man) * c_exp;

  out_diff_value <= in_dat_value - out_dat_value;

  p_stimuli : process
  begin
    in_val <= '0';
    in_dat <= TO_SVEC(0, in_dat'length);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    for I in - 2**(c_in_dat_w - 1) to 2**(c_in_dat_w - 1) - 1 loop
      in_val <= '1';
      in_dat <= TO_SVEC( I, in_dat'length);
      wait until rising_edge(clk);
    end loop;
    in_val <= '0';
    wait;
  end process;

  u_float : entity work.common_int2float
  generic map (
    g_pipeline  => c_pipeline
  )
  port map (
    clk        => clk,
    in_dat     => in_dat,
    out_dat    => out_dat
  );
end tb;
