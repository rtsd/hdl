#------------------------------------------------------------------------------
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

set IPMODEL "SIM";
#set IPMODEL "PHY";

if {$IPMODEL=="PHY"} {

    # This file is based on Qsys-generated file msim_setup.tcl.
    set IP_DIR "$env(HDL_BUILD_DIR)/"
        
    #vlib ./work/         ;# Assume library work already exists
    vmap ip_arria10_e3sge3_ddio_in_1_altera_gpio_core_151  ./work/
    vmap ip_arria10_e3sge3_ddio_in_1_altera_gpio_151       ./work/
    vmap ip_arria10_e3sge3_ddio_out_1_altera_gpio_core_151 ./work/
    vmap ip_arria10_e3sge3_ddio_out_1_altera_gpio_151      ./work/    
    
    # Quartus QIP uses the libraries, so map these here to work/ to be able to use these libraries also in simulation.
    # However by instantiating the compenents as components instead of as entities it is not necessary to know the
    # library in simiulation. Therefore these lines can be commented:
    #vmap ip_arria10_e3sge3_ddio_in_1 ./work/
    #vmap ip_arria10_e3sge3_ddio_out_1 ./work/
    
    vlog -sv "$IP_DIR/../altera_gpio_core20_151/sim/mentor/altera_gpio.sv"                   -work ip_arria10_e3sge3_ddio_in_1_altera_gpio_core_151
    vlog -sv "$IP_DIR/../altera_gpio_core20_151/sim/mentor/altera_gpio.sv"                   -work ip_arria10_e3sge3_ddio_out_1_altera_gpio_core_151
    
    vcom     "$IP_DIR/../altera_gpio_151/sim/ip_arria10_e3sge3_ddio_in_1_altera_gpio_151_ia6gnqq.vhd"  -work ip_arria10_e3sge3_ddio_in_1_altera_gpio_151     
    vcom     "$IP_DIR/ip_arria10_e3sge3_ddio_in_1.vhd"                                                                                               
    
    vcom     "$IP_DIR/../altera_gpio_151/sim/ip_arria10_e3sge3_ddio_out_1_altera_gpio_151_pqwimxi.vhd" -work ip_arria10_e3sge3_ddio_out_1_altera_gpio_151     
    vcom     "$IP_DIR/ip_arria10_e3sge3_ddio_out_1.vhd"                                                                                                    
    
} else {

    # This file uses a behavioral model because the PHY model does not compile OK, see README.txt.
    set SIM_DIR "$env(HDL_WORK)/libraries/technology/ip_arria10_e3sge3/ddio/sim/"
    
    vcom "$SIM_DIR/ip_arria10_e3sge3_ddio_in_1.vhd"
    vcom "$SIM_DIR/ip_arria10_e3sge3_ddio_out_1.vhd"
    vcom "$SIM_DIR/tb_ip_arria10_e3sge3_ddio_1.vhd"
    
}


