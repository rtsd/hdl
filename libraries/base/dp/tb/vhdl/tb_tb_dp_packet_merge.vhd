-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_packet_merge
-- Description:
-- Usage:
-- > as 8, to see that correct instances are generated
-- > run -all

entity tb_tb_dp_packet_merge is
end tb_tb_dp_packet_merge;

architecture tb of tb_tb_dp_packet_merge is
  constant c_nof_repeat            : natural := 100;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --     -- general
  --     g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
  --     g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
  --     -- specific
  --     g_pipeline_ready         : boolean := true;
  --     g_data_w                 : natural := 4;
  --     g_nof_repeat             : natural := 20;
  --     g_nof_pkt                : natural := 3;
  --     g_pkt_len                : natural := 29;
  --     g_pkt_gap                : natural := 0;
  --     g_align_at_sync          : boolean := false;
  --     g_verify_bsn_err         : boolean := false;
  --     g_bsn_increment          : natural := 0;
  --     g_bsn_err_at_pkt_index   : natural := 3;

  u_act_act_8_nof_0      : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 0, 29, 0, false, false, 1, 0);
  u_act_act_8_nof_1      : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 1, 29, 0, false, false, 1, 0);
  u_act_act_8_nof_2      : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 2, 29, 0, false, false, 1, 0);
  u_act_act_8_nof_3      : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 3, 29, 0, false, false, 1, 0);
  u_act_act_8_nof_4      : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 4, 29, 0, false, false, 1, 0);
  u_act_act_8_nof_5      : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 5, 29, 0, false, false, 1, 0);
  u_act_act_8_nof_6      : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 6, 29, 0, false, false, 1, 0);
  u_act_act_8_nof_7      : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 7, 29, 0, false, false, 1, 0);

  u_rnd_act_8_nof_3      : entity work.tb_dp_packet_merge generic map ( e_random, e_active,  true, 8, c_nof_repeat, 3, 29, 0, false, false, 2, 0);
  u_rnd_rnd_8_nof_3_comb : entity work.tb_dp_packet_merge generic map ( e_random, e_random, false, 8, c_nof_repeat, 3, 29, 0, false, false, 3, 0);
  u_rnd_rnd_8_nof_3_reg  : entity work.tb_dp_packet_merge generic map ( e_random, e_random,  true, 8, c_nof_repeat, 3, 29, 0, false, false, 3, 0);
  u_pls_act_8_nof_3      : entity work.tb_dp_packet_merge generic map ( e_pulse,  e_active,  true, 8, c_nof_repeat, 3, 29, 0, false, false, 4, 0);
  u_pls_rnd_8_nof_3      : entity work.tb_dp_packet_merge generic map ( e_pulse,  e_random,  true, 8, c_nof_repeat, 3, 29, 0, false, false, 5, 0);
  u_pls_pls_8_nof_3      : entity work.tb_dp_packet_merge generic map ( e_pulse,  e_pulse,   true, 8, c_nof_repeat, 3, 29, 0, false, false, 6, 0);

  u_rnd_act_8_nof_1      : entity work.tb_dp_packet_merge generic map ( e_random, e_active, true, 8, c_nof_repeat, 1, 29, 0, false, false, 1, 0);
  u_rnd_act_8_nof_3_gap  : entity work.tb_dp_packet_merge generic map ( e_random, e_active, true, 8, c_nof_repeat, 3, 29, 17, false, false, 1, 0);

  u_act_act_8_nof_3_no_err : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 3, 29, 0, false, true, 0, 10);
  u_act_act_8_nof_3_err_10 : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 3, 29, 0, false, true, 1, 10);
  u_act_act_8_nof_3_err_11 : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 3, 29, 0, false, true, 1, 11);
  u_act_act_8_nof_3_err_12 : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 3, 29, 0, false, true, 1, 12);
  u_act_act_8_nof_3_err_13 : entity work.tb_dp_packet_merge generic map ( e_active, e_active, true, 8, c_nof_repeat, 3, 29, 0, false, true, 1, 13);
end tb;
