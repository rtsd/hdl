-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--USE ip_arria10_temp_sense_altera_temp_sense_150.ip_arria10_temp_sense_pkg.all;
use work.tech_fpga_temp_sens_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

library ip_arria10_temp_sense_altera_temp_sense_150;
library ip_arria10_e3sge3_temp_sense_altera_temp_sense_151;
library ip_arria10_e1sg_temp_sense_altera_temp_sense_180;
library ip_arria10_e2sg_temp_sense_altera_temp_sense_1910;

entity tech_fpga_temp_sens is
  generic (
    g_technology : natural := c_tech_select_default
  );
  port (
    corectl : in  std_logic             := '0';  -- corectl.corectl
    eoc     : out std_logic;  -- eoc.eoc
    reset   : in  std_logic             := '0';  -- reset.reset
    tempout : out std_logic_vector(9 downto 0)  -- tempout.tempout
  );
end tech_fpga_temp_sens;

architecture str of tech_fpga_temp_sens is
begin
  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_temp_sense
    port map (
      corectl => corectl,  -- corectl.corectl
      reset   => reset,  -- reset.reset
      tempout => tempout,  -- tempout.tempout
      eoc     => eoc  -- eoc.eoc
    );
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_temp_sense
    port map (
      corectl => corectl,  -- corectl.corectl
      reset   => reset,  -- reset.reset
      tempout => tempout,  -- tempout.tempout
      eoc     => eoc  -- eoc.eoc
    );
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_temp_sense
    port map (
      corectl => corectl,  -- corectl.corectl
      reset   => reset,  -- reset.reset
      tempout => tempout,  -- tempout.tempout
      eoc     => eoc  -- eoc.eoc
    );
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
      u0 : ip_arria10_e2sg_temp_sense
        port map (
            corectl => corectl,  -- corectl.corectl
            reset   => reset,  -- reset.reset
            tempout => tempout,  -- tempout.tempout
            eoc     => eoc  -- eoc.eoc
        );
  end generate;
end architecture;
