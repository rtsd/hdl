# ------------------------------------------------------------------------------
#
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ------------------------------------------------------------------------------
#
# Author: D.F. Brouwer
# Description:
#   This file is based on generated file mentor/msim_setup.tcl.
#   - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
#   - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
#   - replace QSYS_SIMDIR by IP_DIR
#   - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist

set IPMODEL "SIM";
#set IPMODEL "PHY";

if {$IPMODEL=="PHY"} { 
    # OUTDATED AND NOT USED!!
    # This file is based on Qsys-generated file msim_setup.tcl.
    set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_ddio_in_1/sim"
        
    #vlib ./work/         ;# Assume library work already exists
    vmap ip_agi027_1e1v_ddio_in_1_altera_gpio_core10_2100  ./work/
    vmap ip_agi027_1e1v_ddio_in_1_altera_gpio_2100         ./work/
    
    vlog -sv "$IP_DIR/../altera_gpio_core10_2100/sim/mentor/altera_gpio.sv"                       -work ip_agi027_1e1v_ddio_in_1_altera_gpio_core10_2100
    
    vcom     "$IP_DIR/../altera_gpio_2100/sim/ip_agi027_1e1v_ddio_in_1_altera_gpio_2100_nhqe4ta.vhd"  -work ip_agi027_1e1v_ddio_in_1_altera_gpio_2100
    vcom     "$IP_DIR/ip_agi027_1e1v_ddio_in_1.vhd"


    set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_ddio_out_1/sim"

    #vlib ./work/         ;# Assume library work already exists
    vmap ip_agi027_1e1v_ddio_out_1_altera_gpio_core10_2100 ./work/
    vmap ip_agi027_1e1v_ddio_out_1_altera_gpio_2100        ./work/    
    
    vlog -sv "$IP_DIR/../altera_gpio_core10_2100/sim/mentor/altera_gpio.sv"                       -work ip_agi027_1e1v_ddio_out_1_altera_gpio_core10_2100
    
    vcom     "$IP_DIR/../altera_gpio_2100/sim/ip_agi027_1e1v_ddio_out_1_altera_gpio_2100_e4tgwdq.vhd" -work ip_agi027_1e1v_ddio_out_1_altera_gpio_2100
    vcom     "$IP_DIR/ip_agi027_1e1v_ddio_out_1.vhd"

} else {

    # This file uses a behavioral model because the PHY model does not compile OK, see README.txt.
    set SIM_DIR "$env(HDL_WORK)/libraries/technology/ip_agi027_1e1v/ddio/sim/"
    
    vcom "$SIM_DIR/ip_agi027_1e1v_ddio_in_1.vhd"
    vcom "$SIM_DIR/ip_agi027_1e1v_ddio_out_1.vhd"
    vcom "$SIM_DIR/tb_ip_agi027_1e1v_ddio_1.vhd"
    
}
