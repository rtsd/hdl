-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- Purpose: Determine when there is an active frame
-- Description:
-- . The frame_busy goes high combinatorially with the in_sop and low after the
--   in_eop.
-- . The frame_idle = NOT frame_busy.
-- Remark:

entity common_frame_busy is
  port (
    rst        : in  std_logic;
    clk        : in  std_logic;
    in_sop     : in  std_logic;
    in_eop     : in  std_logic;
    frame_idle : out std_logic;
    frame_busy : out std_logic
  );
end common_frame_busy;

architecture str of common_frame_busy is
  signal in_frm            : std_logic;
  signal nxt_in_frm        : std_logic;

  signal i_frame_busy      : std_logic;
begin
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      in_frm <= '0';
    elsif rising_edge(clk) then
      in_frm <= nxt_in_frm;
    end if;
  end process;

  nxt_in_frm <= '1' when in_sop = '1' else '0' when in_eop = '1' else in_frm;

  -- Due to one clk cycle latency in_frm active does not cover the in_sop, so therefor also require NOT in_sop for frame_busy
  i_frame_busy <= in_sop or in_frm;

  frame_busy <=     i_frame_busy;
  frame_idle <= not i_frame_busy;
end str;
