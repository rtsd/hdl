% Spectral leakage PPF/IPPF

% S0 - The prototype lowpass filter impulse response h0[n]:
% --------------------------------------------------------------

numblocks=200;
numtaps=16; % length of each polyphase component
% sampling frequency ~200MHz clock at station, but at cobalt, 1024 blocks are added
fsample=200e6/1024;
numbands = 256;                 % number of banks (channels)
L=numtaps*numbands; % each polyphase component will have numtaps taps

% Kaiser filter: rolloff [f1 f2] = [fsample/numbands fsample/numbands*1.4] (Hz) 
% [1 0] : lowpass, passband=1, stopband=0
% deviations allowed [10^(0.5/20) 10^(-91/20)] (ripple)
C=kaiserord([fsample/numbands fsample/numbands*1.4],[1 0],[10^(0.5/20) 10^(-91/20)],fsample,'cell');

h0=fir1(C{:});
% % or do this to use prototype used by matlab
% channelizer=dsp.Channelizer('StopbandAttenuation',140,'NumFrequencyBands',numbands);
% channelizer.NumTapsPerBand=16;
% % to get back polyphase components p=polyphase(channelizer)
% horig=channelizer.coeffs.Numerator;
% %h0=horig;

% extend length of h a round number to match 
L0 = length(h0);
if L0<L,
 h=zeros(L,1);
 h(1:L0)=h0;
else,
 numtaps=ceil(L0/numbands);
 L=numtaps*numbands;
 h=zeros(L,1);
 h(1:L0)=h0;
end


% S1 - Create the analysis filter-bank
% -----------------------------------------------------------------------------------------
% ordering of polyphase components
ppf_updown=0; % if =1, reverse order
% S2 - Create the polyphase components 
% ---------------------------------------------------------------------------------------------------
numpoly = numbands;             % polyphase component number = decimation ratio = number of channels
hhap = zeros(numpoly, L/numpoly);  % ANALYSIS filter bank array

M = numpoly;                    % polyphase system decimation ratio
if ppf_updown==1,
 hhap=flipud(reshape(h,numpoly,L/numpoly));
else
 hhap=reshape(h,numpoly,L/numpoly);
end

% S3 - Design  synthesis (complementary) filter bank :
% -----------------------------------------------------------------
% S4 - Obtain the polyphase components for each one of synthesis filters
% ----------------------------------------------------------------------------------------------------
hhsp = zeros(numpoly, L/numpoly);  % hhsp Synthesis filter bank array

% g(n)=h(L-1-n) means flipping left - right
% not flipping gives ripples
hhsp=fliplr(hhap);

% S5 - Generate the test input signal
% -----------------------------------
N = numblocks*numbands;
%wav_in = cos(0.01491*pi*[0:N-1]);        % pure sine tone
%wav_in = exp(j*(0.01491*pi*[0:N-1])); % complex input
%wav_in= sinc(0.001*[0:N-1]).*exp(j*(0.01491*pi*[0:N-1]));

% tone frequency, not too high to avoid aliasing, 
% the PPF passband is fsample/numbands, so ftone < 0.5*fsample/numbands
ftone=320e3;
% time t -> [0:N-1]/fsample
wav_in=exp(j*(2*pi*ftone*[0:N-1]/fsample));


% S6 - Apply test signal to the filterbank: ANALYSIS STAGE :
% -----------------------------------------------------------
yyd = zeros(numbands, floor(N/numbands));   % decimated outputs..
M = numbands;
for k=1:1:numbands       
    temp = conv([wav_in(k:M:end),0], hhap(k,:));
    % use following to include the starting zero values
    %yyd(k,:) = temp(1:floor(N/numbands));
    yyd(k,:) = temp(floor(L/(2*M))+1 : floor(L/(2*M))+N/numbands);
end

%  S66: FFT channelizer (FFT->IFFT pair)
Y=fft(yyd,[],1);


x=reshape(wav_in,1,N);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% apply delay/bandpass correction
phramp=(1+0.2j)*exp(j*(2*pi*(3.5)/M)*([1:M]-1));
% reshape into bands x blocks
X=Y;
for ci=1:N/M,
 Y(:,ci)=Y(:,ci).*phramp(:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% S7 - Apply SYNTHESIS filterbank on the decimated signal :
% ---------------------------------------------------------
ys = zeros(1, N);

yc=ifft(Y,[],1); % equal to yyd
temp = zeros(1, N+L-1);
for k=1:numbands
    temp(k:numbands:N+L-numbands) = conv( yc(k,:) , hhsp(k,:) );
end
ys = temp(L/2+1:L/2+N);    
ys = numbands*ys;


% SX - DISPLAY RESULTS:
% ---------------------
figure(1);
wav_in1=reshape(x,numbands,numblocks);
Win=fftshift(fft(wav_in1,[],1));
plot(abs(Win));

figure(2);
wav_rec=reshape(ys,numbands,numblocks);
Wout=fftshift(fft(wav_rec,[],1));
Wout=Wout/norm(Wout);
plot(abs(Wout));


% synthesizer=dsp.ChannelSynthesizer('StopbandAttenuation',140,'NumTapsPerBand',16);
% Yc=channelizer(transpose(x));
% Yc=transpose(Yc);
% for ci=1:N/M,
%  Yc(:,ci)=Yc(:,ci).*phramp(:);
% end
% Yc=transpose(Yc);
% yo=synthesizer(Yc);


% figure(3)
% wav_rec1=reshape(yo,numbands,numblocks);
% Wout1=fftshift(fft(wav_rec1,[],1));
% Wout1=Wout1/norm(Wout1);
% plot(abs(Wout1));
