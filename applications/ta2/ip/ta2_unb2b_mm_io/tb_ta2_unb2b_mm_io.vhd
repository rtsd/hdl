-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . tb for ta2_unb2b_mm_io
-- Description:
-- Simple testbench that provides MM stimuli of writing a
-- register and then reading it back.
-- . Usage -> as 10; run -a
-- --------------------------------------------------------------------------
library IEEE, common_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_ta2_unb2b_mm_io is
end tb_ta2_unb2b_mm_io;

architecture tb of tb_ta2_unb2b_mm_io is
  constant c_data_value : natural := 10;

  signal tb_end : std_logic := '0';
  signal clk    : std_logic := '1';
  signal rst    : std_logic := '1';

  signal reg_a : std_logic_vector(31 downto 0) := (others => '0');
  signal reg_b : std_logic_vector(31 downto 0) := (others => '0');
  signal reg_c : std_logic_vector(31 downto 0) := (others => '0');
  signal reg_d : std_logic_vector(31 downto 0) := (others => '0');

  signal in_sosi  : t_dp_sosi := c_dp_sosi_rst;
  signal in_siso  : t_dp_siso := c_dp_siso_rst;
  signal out_sosi : t_dp_sosi := c_dp_sosi_rst;
  signal out_siso : t_dp_siso := c_dp_siso_rst;

  signal mm_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal mm_miso : t_mem_miso := c_mem_miso_rst;

  signal busy : boolean := false;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  p_verify : process
  begin
    wait until tb_end = '1';
    assert TO_UINT(mm_miso.rddata(31 downto 0)) = c_data_value
      report "Wrong read data."
      severity ERROR;
  end process;

  u_dut : entity work.ta2_unb2b_mm_io
  generic map(
    g_use_opencl => false
  )
  port map(
    -- Memory-mapped clock domain
    mm_rst       => rst,
    mm_clk       => clk,

    mm_mosi     => mm_mosi,
    mm_miso     => mm_miso,

    -- Streaming clock domain
    kernel_reset     => rst,
    kernel_clk       => clk,

    -- ST sinks
    snk_out  => in_siso,
    snk_in   => in_sosi,
    -- ST source
    src_in   => out_siso,
    src_out  => out_sosi
  );

  p_stim_mm : process
  begin
    wait until rst = '0';
    proc_common_wait_some_cycles(clk, 15);  -- give dc fifos time to initialize
    proc_mem_mm_bus_wr(333, c_data_value, clk, mm_miso, mm_mosi);  -- write value to unused address.
    proc_mem_mm_bus_wr(0, c_data_value, clk, mm_miso, mm_mosi);  -- write value to address 0.
    proc_mem_mm_bus_wr(1, c_data_value+1, clk, mm_miso, mm_mosi);  -- write value +1 to address 1.
    proc_mem_mm_bus_wr(2, c_data_value+2, clk, mm_miso, mm_mosi);  -- write value +2 to address 2.
    proc_mem_mm_bus_wr(3, c_data_value+3, clk, mm_miso, mm_mosi);  -- write value +3 to address 3.

    proc_common_wait_some_cycles(clk, 5);
    proc_mem_mm_bus_rd(333, clk, mm_miso, mm_mosi);  -- read address from undefined address.
    proc_mem_mm_bus_rd(3, clk, mm_miso, mm_mosi);  -- read address 3.
    proc_mem_mm_bus_rd(2, clk, mm_miso, mm_mosi);  -- read address 2.
    proc_mem_mm_bus_rd(1, clk, mm_miso, mm_mosi);  -- read address 1.
    proc_mem_mm_bus_rd(0, clk, mm_miso, mm_mosi);  -- read address 0.

    proc_common_wait_some_cycles(clk, 15);

    tb_end <= '1';
    wait;
  end process;

  -- simulate an OpenCL kernel response (rl=0)
  p_busy : process
  begin
    while tb_end = '0' loop
      if in_sosi.valid = '1' then
        proc_common_wait_some_cycles(clk, 1);
        busy <= true;
        proc_common_wait_some_cycles(clk, 3);  -- simulate request time
        busy <= false;
      else
        proc_common_wait_some_cycles(clk, 1);
      end if;
    end loop;
    wait;
  end process;

  p_stim_st : process(out_sosi, in_siso, busy)
  begin
    in_sosi.valid <= '0';
    if out_sosi.valid = '1' then
      if out_sosi.data(64) = '1' then  -- Write request
        if TO_UINT(out_sosi.data(63 downto 56)) = 0 then
          reg_a <= out_sosi.data(31 downto 0);
        elsif TO_UINT(out_sosi.data(63 downto 56)) = 1 then
          reg_b <= TO_UVEC(TO_UINT(out_sosi.data(31 downto 24)) + 1, 8) & out_sosi.data(23 downto 0);
        elsif TO_UINT(out_sosi.data(63 downto 56)) = 2 then
          reg_c <= out_sosi.data(31 downto 0);
        elsif TO_UINT(out_sosi.data(63 downto 56)) = 3 then
          reg_d <= TO_UVEC(TO_UINT(out_sosi.data(31 downto 24)) + 2, 8) & out_sosi.data(23 downto 0);
        end if;
        out_siso.ready <= '1';
      else  -- read request
        if not busy then
          out_siso.ready <= '1';
          in_sosi.valid <= '1';
          if TO_UINT(out_sosi.data(63 downto 56)) = 0 then
            in_sosi.data(31 downto 0) <= reg_a;
          elsif TO_UINT(out_sosi.data(63 downto 56)) = 1 then
            in_sosi.data(31 downto 0) <= reg_b;
          elsif TO_UINT(out_sosi.data(63 downto 56)) = 2 then
            in_sosi.data(31 downto 0) <= reg_c;
          elsif TO_UINT(out_sosi.data(63 downto 56)) = 3 then
            in_sosi.data(31 downto 0) <= reg_d;
          else
            in_sosi.data(31 downto 0) <= (others => '0');
          end if;
        end if;
      end if;
    end if;
  end process;
end tb;
