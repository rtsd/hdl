-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE;
use IEEE.std_logic_1164.all;

package tech_fpga_temp_sens_component_pkg is
  component ip_arria10_temp_sense is
    port (
      corectl : in  std_logic := '0';  -- corectl.corectl
      eoc     : out std_logic;  -- eoc.eoc
      reset   : in  std_logic := '0';  -- reset.reset
      tempout : out std_logic_vector(9 downto 0)  -- tempout.tempout
    );
  end component;

  component ip_arria10_e3sge3_temp_sense is
    port (
      corectl : in  std_logic := '0';  -- corectl.corectl
      eoc     : out std_logic;  -- eoc.eoc
      reset   : in  std_logic := '0';  -- reset.reset
      tempout : out std_logic_vector(9 downto 0)  -- tempout.tempout
    );
  end component;

  component ip_arria10_e1sg_temp_sense is
    port (
      corectl : in  std_logic := '0';  -- corectl.corectl
      eoc     : out std_logic;  -- eoc.eoc
      reset   : in  std_logic := '0';  -- reset.reset
      tempout : out std_logic_vector(9 downto 0)  -- tempout.tempout
    );
  end component;

  component ip_arria10_e2sg_temp_sense is
      port (
          corectl : in  std_logic := '0';  -- corectl.corectl
          eoc     : out std_logic;  -- eoc.eoc
          reset   : in  std_logic := '0';  -- reset.reset
          tempout : out std_logic_vector(9 downto 0)  -- tempout.tempout
      );
  end component;
end tech_fpga_temp_sens_component_pkg;
