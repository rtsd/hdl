README.txt for $HDL_WORK/libraries/technology/ip_arria10/tse_sgmii_gx

The ip_arria10_tse_sgmii_gx IP was ported to Quartus 14.0a10 for Arria10 by creating it in Qsys using the same parameter settings as the ip_arria10_tse_sgmii_lvds, but with GX IO.

The tb_ip_arria10_tse_sgmii_gx.vhd verifies the DUT and simulates OK.

For more information see: $HDL_WORK/libraries/technology/ip_arria10/tse_sgmii_lvds/README.txt

