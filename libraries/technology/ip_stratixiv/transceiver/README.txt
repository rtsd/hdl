README.txt for ip_stratixiv_transceiver library


1) ip_stratixiv_hssi*


2) ip_stratixiv_gxb_reconfig*

  In theory the gxb_reconfig IP are the same, but in practise they can differ slightly between different Quartus versions. The tr_nonbonded, tse_sgmii_gx
  and tr_xaui libraries each use a newly generated gxb_reconfig. Trying to let them all use the latest version of the gxb_reconfig IP would require
  validation on hardware. To avoid having to do this preserve the various versions of gxb_reconfig that were created in time. Versions overview:
  
             reconfig_fromgxb         used in library
             bus width
     v9.1  :      g_nof_gx    * 17    tr_nonbonded
     v10.1 : ceil(g_nof_gx/4) * 17    tse_sgmii_gx
     v11.1 : ceil(g_nof_gx/4) * 17    tr_xaui

 . The gxb_reconfig for v9.1 uses wider reconfig_fromgxb bus width.
 . The reconfig_fromgxb for v10.1 and v11.1 seem idendical except for different internal signal namings.
   
