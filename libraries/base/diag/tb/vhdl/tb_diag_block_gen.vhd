--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author: Eric Kooistra
-- Purpose: test bench for diag_block_gen
--   The tb is self-stopping and self-checking.
-- Usage:
--   > as 5
--   > run -all

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.common_math_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.diag_pkg.all;

entity tb_diag_block_gen is
  generic (
    -- general
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    -- specific
    g_nof_samples_per_packet : natural := 2;  -- >= 2
    g_nof_blocks_per_sync    : natural := 10;  -- >= 1
    g_gapsize                : natural := 32;  -- >= 0
    g_buf_adr_w      : natural := 7;  -- Waveform buffer address width (requires corresponding c_buf_file)
    g_buf_dat_w      : natural := 32;  -- Waveform buffer stored data width (requires corresponding c_buf_file)
    g_try_phasor     : boolean := false  -- use TRUE to see BG phasor in wave window with out_sosi.re/im in radix
                                         -- decimal and analogue format, no self test
  );
end tb_diag_block_gen;

architecture tb of tb_diag_block_gen is
  constant clk_period       : time    := 10 ns;
  constant c_timeout        : natural := 1000;
  constant c_offtime        : natural := 200;
  constant c_runtime        : natural := 1500;

  -- Default settings
  constant c_buf            : t_c_mem := (latency  => 1,
                                          adr_w    => g_buf_adr_w,
                                          dat_w    => g_buf_dat_w,
                                          nof_dat  => 2**g_buf_adr_w,  -- = 2**adr_w
                                          init_sl  => '0');

  constant c_buf_file   : string := sel_a_b(c_buf.adr_w = 7 and c_buf.dat_w = 32, "data/diag_block.hex", "UNUSED");

  constant c_cntr_init  : integer := 0;
  constant c_cntr_incr  : integer := 1;
  constant c_cntr_arr   : t_slv_32_arr(c_buf.nof_dat - 1 downto 0) := flip(array_init(c_cntr_init, c_buf.nof_dat, c_cntr_incr));

  -- Phasor: exp(j*angle) = cos(angle) + j*sin(angle)
  -- A complex FFT of N points has N bins or channels: ch = -N/2:0:N/2-1.
  -- To create an FFT input phasor with frequency in the middle of a channel use FREQ = ch.
  constant c_phasor_nof_points    : natural := 96;
  constant c_phasor_dat_w         : natural := g_buf_dat_w / c_nof_complex;
  constant c_phasor_unit_ampl     : real := real(2**(c_phasor_dat_w - 1) - 1);  -- max = full scale - 1
  constant c_phasor_ampl          : real := 1.0 * c_phasor_unit_ampl;  -- use g_phasor_ampl <= 1.0 to avoid wrapping
  constant c_phasor_freq          : real := 2.0;  -- in range -N/2 : N/2-1
  constant c_phasor_phase         : real := 0.0;
  constant c_phasor_exp_arr       : t_slv_32_arr := common_math_create_look_up_table_phasor(c_phasor_nof_points,
                                                                                            c_phasor_dat_w,
                                                                                            c_phasor_ampl,
                                                                                            c_phasor_freq,
                                                                                            c_phasor_phase);

  -- Default BG control
  constant c_bg_ctrl    : t_diag_block_gen := (        '0',
                                                       '0',
                                               TO_UVEC(g_nof_samples_per_packet, c_diag_bg_samples_per_packet_w),
                                               TO_UVEC(g_nof_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                               TO_UVEC(g_gapsize, c_diag_bg_gapsize_w),
                                               TO_UVEC( 0, c_diag_bg_mem_low_adrs_w),
                                               TO_UVEC(95, c_diag_bg_mem_high_adrs_w),
                                               TO_UVEC(42, c_diag_bg_bsn_init_w));
  constant c_bg_period  : natural := TO_UINT(c_bg_ctrl.samples_per_packet) + TO_UINT(c_bg_ctrl.gapsize);

  -- Some alternative BG control settings
  constant c_alternative_mem_low_adrs       : natural := 1;
  constant c_alternative_mem_high_adrs      : natural := 64;
  constant c_alternative_data_gap           : natural := 1 + c_alternative_mem_low_adrs;

  -- Another BG control for verifying XON
  constant c_bg_ctrl2   : t_diag_block_gen := (        '0',
                                                       '0',
                                               TO_UVEC(17, c_diag_bg_samples_per_packet_w),
                                               TO_UVEC(g_nof_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                               TO_UVEC( 0, c_diag_bg_gapsize_w),
                                               TO_UVEC( 0, c_diag_bg_mem_low_adrs_w),
                                               TO_UVEC(16, c_diag_bg_mem_high_adrs_w),
                                               TO_UVEC( 0, c_diag_bg_bsn_init_w));

  constant c_bg_period2 : natural := TO_UINT(c_bg_ctrl2.samples_per_packet) + TO_UINT(c_bg_ctrl2.gapsize);

  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '1';

  signal verify_en      : std_logic := '0';
  signal verify_en_data : std_logic;

  signal mm_buf_arr     : t_slv_32_arr(c_buf.nof_dat - 1 downto 0);
  signal mm_buf_mosi    : t_mem_mosi := c_mem_mosi_rst;
  signal mm_buf_miso    : t_mem_miso;

  signal bg_buf_mosi    : t_mem_mosi := c_mem_mosi_rst;
  signal bg_buf_miso    : t_mem_miso;

  signal bg_ctrl        : t_diag_block_gen;
  signal bg_ctrl_hold   : t_diag_block_gen;

  signal random         : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal toggle         : std_logic := '0';
  signal out_siso_bg    : t_dp_siso := c_dp_siso_rdy;
  signal out_siso       : t_dp_siso;
  signal out_sosi       : t_dp_sosi;
  signal prev_out_sosi  : t_dp_sosi;
  signal hold_sop       : std_logic := '0';
  signal last_size      : natural;
  signal exp_size       : natural;
  signal cnt_size       : natural;
begin
  rst <= '1', '0' after clk_period * 10;

  clk <= not clk or tb_end after clk_period / 2;

  p_stimuli : process
  begin
    out_siso <= c_dp_siso_rdy;
    bg_ctrl <= c_bg_ctrl;
    proc_common_wait_until_low(clk, rst);  -- Wait until reset has finished
    proc_common_wait_some_cycles(clk, 10);

    -- Write waveform buffer
    if g_try_phasor = false then
      proc_mem_write_ram(c_cntr_arr, clk, mm_buf_mosi);
      -- Readback waveform buffer
      proc_mem_read_ram(clk, mm_buf_mosi, mm_buf_miso, mm_buf_arr);
    else
      proc_mem_write_ram(c_phasor_exp_arr, clk, mm_buf_mosi);
    end if;

    -- Run with a gap
    bg_ctrl.enable <= '1';
    proc_dp_verify_run_some_cycles(5, c_runtime, 0, clk, verify_en);

    -- Run without gap
    bg_ctrl.enable <= '0';
    proc_common_wait_some_cycles(clk, c_offtime);
    bg_ctrl.gapsize <= TO_UVEC(0, c_diag_bg_gapsize_w);
    bg_ctrl.enable <= '1';
    proc_dp_verify_run_some_cycles(5, c_runtime, 0, clk, verify_en);

    -- Run with non-aligned memory settings
    bg_ctrl.enable <= '0';
    proc_common_wait_some_cycles(clk, c_offtime);
    bg_ctrl.gapsize <= c_bg_ctrl.gapsize;
    bg_ctrl.mem_low_adrs <= TO_UVEC(c_alternative_mem_low_adrs, c_diag_bg_mem_low_adrs_w);
    bg_ctrl.mem_high_adrs <= TO_UVEC(c_alternative_mem_high_adrs, c_diag_bg_mem_high_adrs_w);
    bg_ctrl.enable <= '1';
    proc_dp_verify_run_some_cycles(5, c_runtime, 0, clk, verify_en);

    -- Run with XON flow control (verified by proc_dp_verify_block_size)
    bg_ctrl.enable <= '0';
    proc_common_wait_some_cycles(clk, c_offtime);
    bg_ctrl <= c_bg_ctrl2;
    bg_ctrl.enable <= '1';
    for I in 0 to c_bg_period2 * 3 loop
      out_siso <= c_dp_siso_rdy;
      proc_common_wait_some_cycles(clk, c_bg_period2 * 3 + 1);
      out_siso <= c_dp_siso_rst;
      proc_common_wait_some_cycles(clk, c_bg_period2 * 3);
    end loop;

    -- End simulation
    tb_end <= '1';
    wait;
  end process;

  -- Verification
  exp_size <= TO_UINT(bg_ctrl.samples_per_packet);
  last_size <= exp_size'last_value;

  -- Wait for out_sosi.valid to ensure that prev_out_sosi is updated, before releasing verify_en_data
  p_verify_en_data : process
    constant c_timeout : natural := c_runtime / 2;
  begin
    verify_en_data <= '0';
    proc_common_wait_until_high(clk, verify_en);
    proc_common_wait_until_high(c_timeout, clk, out_sosi.valid);
    verify_en_data <= '1';
    proc_common_wait_until_low(clk, verify_en);
    verify_en_data <= '0';
  end process;

  proc_dp_verify_data("BSN",                                                          clk, verify_en_data, out_sosi.sop,   out_sosi.bsn,  prev_out_sosi.bsn);
  proc_dp_verify_data("DATA", TO_UINT(bg_ctrl.mem_high_adrs), c_alternative_data_gap, clk, verify_en_data, out_sosi.valid, out_sosi.data, prev_out_sosi.data);

  proc_dp_verify_sop_and_eop(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, hold_sop);
  proc_dp_verify_block_size(last_size, exp_size, clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, cnt_size);

  -- Waveform buffer
  u_buf : entity common_lib.common_ram_crw_crw
  generic map (
    g_ram       => c_buf,
    g_init_file => c_buf_file
  )
  port map (
    rst_a     => '0',
    rst_b     => '0',
    clk_a     => clk,
    clk_b     => clk,
    wr_en_a   => mm_buf_mosi.wr,
    wr_en_b   => '0',
    wr_dat_a  => mm_buf_mosi.wrdata(c_buf.dat_w - 1 downto 0),
    wr_dat_b  => (others => '0'),
    adr_a     => mm_buf_mosi.address(c_buf.adr_w - 1 downto 0),
    adr_b     => bg_buf_mosi.address(c_buf.adr_w - 1 downto 0),
    rd_en_a   => mm_buf_mosi.rd,
    rd_en_b   => bg_buf_mosi.rd,
    rd_dat_a  => mm_buf_miso.rddata(c_buf.dat_w - 1 downto 0),
    rd_dat_b  => bg_buf_miso.rddata(c_buf.dat_w - 1 downto 0),
    rd_val_a  => mm_buf_miso.rdval,
    rd_val_b  => bg_buf_miso.rdval
  );

  u_dut : entity work.diag_block_gen
  generic map(
    g_buf_dat_w  => c_buf.dat_w,
    g_buf_addr_w => c_buf.adr_w
  )
  port map (
    rst         => rst,
    clk         => clk,
    buf_addr    => bg_buf_mosi.address(c_buf.adr_w - 1 downto 0),
    buf_rden    => bg_buf_mosi.rd,
    buf_rddat   => bg_buf_miso.rddata(c_buf.dat_w - 1 downto 0),
    buf_rdval   => bg_buf_miso.rdval,
    ctrl        => bg_ctrl,
    ctrl_hold   => bg_ctrl_hold,
    out_siso    => out_siso_bg,
    out_sosi    => out_sosi
  );

  random <= func_common_random(random) when rising_edge(clk);
  toggle <= not toggle when rising_edge(clk);

  out_siso_bg.xon <= out_siso.xon;
  out_siso_bg.ready <= '1'                 when g_flow_control_verify = e_active  else
                       random(random'high) when g_flow_control_verify = e_random  else
                       toggle              when g_flow_control_verify = e_pulse;
end tb;
