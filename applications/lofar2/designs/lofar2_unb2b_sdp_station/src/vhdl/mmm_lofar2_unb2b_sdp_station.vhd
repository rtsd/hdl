-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, mm_lib, lofar2_sdp_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use unb2b_board_lib.unb2b_board_pkg.all;
  use unb2b_board_lib.unb2b_board_peripherals_pkg.all;
  use mm_lib.mm_file_pkg.all;
  use mm_lib.mm_file_unb_pkg.all;
  use work.qsys_lofar2_unb2b_sdp_station_pkg.all;
  use lofar2_sdp_lib.sdp_pkg.all;

entity mmm_lofar2_unb2b_sdp_station is
  generic (
    g_sim         : boolean := false;  -- FALSE: use QSYS; TRUE: use mm_file I/O
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0
 );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_copi             : out t_mem_copi;
    reg_wdi_cipo             : in  t_mem_cipo;

    -- system_info
    reg_unb_system_info_copi : out t_mem_copi;
    reg_unb_system_info_cipo : in  t_mem_cipo;
    rom_unb_system_info_copi : out t_mem_copi;
    rom_unb_system_info_cipo : in  t_mem_cipo;

    -- UniBoard I2C sensors
    reg_unb_sens_copi        : out t_mem_copi;
    reg_unb_sens_cipo        : in  t_mem_cipo;

    reg_fpga_temp_sens_copi   : out t_mem_copi;
    reg_fpga_temp_sens_cipo   : in  t_mem_cipo;
    reg_fpga_voltage_sens_copi: out t_mem_copi;
    reg_fpga_voltage_sens_cipo: in  t_mem_cipo;

    reg_unb_pmbus_copi       : out t_mem_copi;
    reg_unb_pmbus_cipo       : in  t_mem_cipo;

    -- PPSH
    reg_ppsh_copi            : out t_mem_copi;
    reg_ppsh_cipo            : in  t_mem_cipo;

    -- eth1g
    eth1g_mm_rst             : out std_logic;
    eth1g_tse_copi           : out t_mem_copi;
    eth1g_tse_cipo           : in  t_mem_cipo;
    eth1g_reg_copi           : out t_mem_copi;
    eth1g_reg_cipo           : in  t_mem_cipo;
    eth1g_reg_interrupt      : in  std_logic;
    eth1g_ram_copi           : out t_mem_copi;
    eth1g_ram_cipo           : in  t_mem_cipo;

    -- EPCS read
    reg_dpmm_data_copi       : out t_mem_copi;
    reg_dpmm_data_cipo       : in  t_mem_cipo;
    reg_dpmm_ctrl_copi       : out t_mem_copi;
    reg_dpmm_ctrl_cipo       : in  t_mem_cipo;

    -- EPCS write
    reg_mmdp_data_copi       : out t_mem_copi;
    reg_mmdp_data_cipo       : in  t_mem_cipo;
    reg_mmdp_ctrl_copi       : out t_mem_copi;
    reg_mmdp_ctrl_cipo       : in  t_mem_cipo;

    -- EPCS status/control
    reg_epcs_copi            : out t_mem_copi;
    reg_epcs_cipo            : in  t_mem_cipo;

    -- Remote Update
    reg_remu_copi            : out t_mem_copi;
    reg_remu_cipo            : in  t_mem_cipo;

    -- Jesd control
    jesd204b_copi            : out t_mem_copi;
    jesd204b_cipo            : in  t_mem_cipo;

    -- Dp shiftram
    reg_dp_shiftram_copi     : out t_mem_copi;
    reg_dp_shiftram_cipo     : in  t_mem_cipo;

    -- Bsn source
    reg_bsn_source_v2_copi   : out t_mem_copi;
    reg_bsn_source_v2_cipo   : in  t_mem_cipo;

    -- bsn schduler for wg trigger
    reg_bsn_scheduler_copi   : out t_mem_copi;
    reg_bsn_scheduler_cipo   : in  t_mem_cipo;

    -- BSN Monitor
    reg_bsn_monitor_input_copi : out t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_input_cipo : in  t_mem_cipo := c_mem_cipo_rst;

    -- MM wideband waveform generator registers [0,1,2,3] for signal paths [A,B,C,D]
    reg_wg_copi                   : out t_mem_copi;
    reg_wg_cipo                   : in  t_mem_cipo;
    ram_wg_copi                   : out t_mem_copi;
    ram_wg_cipo                   : in  t_mem_cipo;

    -- Bsn databuffer
    ram_diag_data_buf_bsn_copi    : out t_mem_copi;
    ram_diag_data_buf_bsn_cipo    : in  t_mem_cipo;
    reg_diag_data_buf_bsn_copi    : out t_mem_copi;
    reg_diag_data_buf_bsn_cipo    : in  t_mem_cipo;

    -- ST Histogram
    ram_st_histogram_copi         : out t_mem_copi;
    ram_st_histogram_cipo         : in  t_mem_cipo;

    -- Aduh
    reg_aduh_monitor_copi         : out t_mem_copi;
    reg_aduh_monitor_cipo         : in  t_mem_cipo;

    -- Subband statistics
    ram_st_sst_copi               : out t_mem_copi;
    ram_st_sst_cipo               : in  t_mem_cipo;

    -- Filter coefficients
    ram_fil_coefs_copi            : out t_mem_copi;
    ram_fil_coefs_cipo            : in  t_mem_cipo;

    -- Spectral Inversion
    reg_si_copi                   : out t_mem_copi;
    reg_si_cipo                   : in  t_mem_cipo;

   -- Equalizer gains
   ram_equalizer_gains_copi       : out t_mem_copi;
   ram_equalizer_gains_cipo       : in  t_mem_cipo;
   ram_equalizer_gains_cross_copi : out t_mem_copi;
   ram_equalizer_gains_cross_cipo : in  t_mem_cipo;

   -- DP Selector
   reg_dp_selector_copi           : out t_mem_copi;
   reg_dp_selector_cipo           : in  t_mem_cipo;

   -- SDP Info
   reg_sdp_info_copi              : out t_mem_copi;
   reg_sdp_info_cipo              : in  t_mem_cipo;

   -- RING Info
   reg_ring_info_copi             : out t_mem_copi;
   reg_ring_info_cipo             : in  t_mem_cipo;

   -- Beamlet Subband Select
   ram_ss_ss_wide_copi            : out t_mem_copi;
   ram_ss_ss_wide_cipo            : in  t_mem_cipo;

   -- Local BF bf weights
   ram_bf_weights_copi            : out t_mem_copi;
   ram_bf_weights_cipo            : in  t_mem_cipo;

   -- BF bsn aligner_v2
   reg_bsn_align_v2_bf_copi       : out t_mem_copi;
   reg_bsn_align_v2_bf_cipo       : in  t_mem_cipo;

   -- BF bsn aligner_v2 bsn monitors
   reg_bsn_monitor_v2_rx_align_bf_copi : out t_mem_copi;
   reg_bsn_monitor_v2_rx_align_bf_cipo : in  t_mem_cipo;
   reg_bsn_monitor_v2_aligned_bf_copi  : out t_mem_copi;
   reg_bsn_monitor_v2_aligned_bf_cipo  : in  t_mem_cipo;

   -- mms_dp_scale Scale Beamlets
   reg_bf_scale_copi              : out t_mem_copi;
   reg_bf_scale_cipo              : in  t_mem_cipo;

   -- Beamlet Data Output (BDO) header fields
   -- . single destination, used when revision.nof_bdo_destinations_max = 1
   reg_hdr_dat_copi               : out t_mem_copi;
   reg_hdr_dat_cipo               : in  t_mem_cipo;
   -- . multiple destinations, used when revision.nof_bdo_destinations_max > 1
   reg_bdo_destinations_copi      : out t_mem_copi;
   reg_bdo_destinations_cipo      : in  t_mem_cipo;

   -- Beamlet Data Output xonoff
   reg_dp_xonoff_copi             : out t_mem_copi;
   reg_dp_xonoff_cipo             : in  t_mem_cipo;

   -- BF ring lane info
   reg_ring_lane_info_bf_copi                 : out t_mem_copi;
   reg_ring_lane_info_bf_cipo                 : in  t_mem_cipo;

   -- BF ring bsn monitor rx
   reg_bsn_monitor_v2_ring_rx_bf_copi         : out t_mem_copi;
   reg_bsn_monitor_v2_ring_rx_bf_cipo         : in  t_mem_cipo;

   -- BF ring bsn monitor tx
   reg_bsn_monitor_v2_ring_tx_bf_copi         : out t_mem_copi;
   reg_bsn_monitor_v2_ring_tx_bf_cipo         : in  t_mem_cipo;

   -- BF ring validate err
   reg_dp_block_validate_err_bf_copi          : out t_mem_copi;
   reg_dp_block_validate_err_bf_cipo          : in  t_mem_cipo;

   -- BF ring bsn at sync
   reg_dp_block_validate_bsn_at_sync_bf_copi  : out t_mem_copi;
   reg_dp_block_validate_bsn_at_sync_bf_cipo  : in  t_mem_cipo;

   -- Beamlet Statistics (BST)
   ram_st_bst_copi                : out t_mem_copi;
   ram_st_bst_cipo                : in  t_mem_cipo;

   -- Subband Statistics offload
   reg_stat_enable_sst_copi       : out t_mem_copi;
   reg_stat_enable_sst_cipo       : in  t_mem_cipo;

   -- Statistics header info
   reg_stat_hdr_dat_sst_copi      : out t_mem_copi;
   reg_stat_hdr_dat_sst_cipo      : in  t_mem_cipo;

   -- Crosslet Statistics offload
   reg_stat_enable_xst_copi       : out t_mem_copi;
   reg_stat_enable_xst_cipo       : in  t_mem_cipo;

   -- Crosslet Statistics header info
   reg_stat_hdr_dat_xst_copi      : out t_mem_copi;
   reg_stat_hdr_dat_xst_cipo      : in  t_mem_cipo;

   -- Beamlet Statistics offload
   reg_stat_enable_bst_copi       : out t_mem_copi;
   reg_stat_enable_bst_cipo       : in  t_mem_cipo;

   -- Beamlet Statistics header info
   reg_stat_hdr_dat_bst_copi      : out t_mem_copi;
   reg_stat_hdr_dat_bst_cipo      : in  t_mem_cipo;

   -- crosslets_info
   reg_crosslets_info_copi        : out t_mem_copi;
   reg_crosslets_info_cipo        : in  t_mem_cipo;

   -- crosslets_info
   reg_nof_crosslets_copi         : out t_mem_copi;
   reg_nof_crosslets_cipo         : in  t_mem_cipo;

   -- bsn_sync_scheduler_xsub
   reg_bsn_sync_scheduler_xsub_copi    : out t_mem_copi;
   reg_bsn_sync_scheduler_xsub_cipo    : in  t_mem_cipo;

   -- st_xsq (XST)
   ram_st_xsq_copi                : out t_mem_copi;
   ram_st_xsq_cipo                : in  t_mem_cipo;

   -- 10 GbE mac
   reg_nw_10GbE_mac_copi          : out t_mem_copi;
   reg_nw_10GbE_mac_cipo          : in  t_mem_cipo;

   -- 10 GbE eth
   reg_nw_10GbE_eth10g_copi       : out t_mem_copi;
   reg_nw_10GbE_eth10g_cipo       : in  t_mem_cipo;

   -- XST bsn aligner_v2
   reg_bsn_align_v2_xsub_copi                : out t_mem_copi;
   reg_bsn_align_v2_xsub_cipo                : in  t_mem_cipo;

   -- XST bsn aligner_v2 bsn monitors
   reg_bsn_monitor_v2_rx_align_xsub_copi     : out t_mem_copi;
   reg_bsn_monitor_v2_rx_align_xsub_cipo     : in  t_mem_cipo;
   reg_bsn_monitor_v2_aligned_xsub_copi      : out t_mem_copi;
   reg_bsn_monitor_v2_aligned_xsub_cipo      : in  t_mem_cipo;

   -- XST UDP offload bsn monitor
   reg_bsn_monitor_v2_xst_offload_copi       : out t_mem_copi;
   reg_bsn_monitor_v2_xst_offload_cipo       : in  t_mem_cipo;

   -- BST UDP offload bsn monitor
   reg_bsn_monitor_v2_bst_offload_copi       : out t_mem_copi;
   reg_bsn_monitor_v2_bst_offload_cipo       : in  t_mem_cipo;

   -- Beamlet output bsn monitor
   reg_bsn_monitor_v2_beamlet_output_copi    : out t_mem_copi;
   reg_bsn_monitor_v2_beamlet_output_cipo    : in  t_mem_cipo;

   -- SST UDP offload bsn monitor
   reg_bsn_monitor_v2_sst_offload_copi       : out t_mem_copi;
   reg_bsn_monitor_v2_sst_offload_cipo       : in  t_mem_cipo;

   -- XST ring lane info
   reg_ring_lane_info_xst_copi    : out t_mem_copi;
   reg_ring_lane_info_xst_cipo    : in  t_mem_cipo;

   -- XST ring bsn monitor rx
   reg_bsn_monitor_v2_ring_rx_xst_copi: out t_mem_copi;
   reg_bsn_monitor_v2_ring_rx_xst_cipo: in  t_mem_cipo;

   -- XST ring bsn monitor tx
   reg_bsn_monitor_v2_ring_tx_xst_copi : out t_mem_copi;
   reg_bsn_monitor_v2_ring_tx_xst_cipo : in  t_mem_cipo;

   -- XST ring validate err
   reg_dp_block_validate_err_xst_copi : out t_mem_copi;
   reg_dp_block_validate_err_xst_cipo : in  t_mem_cipo;

   -- XST ring bsn at sync
   reg_dp_block_validate_bsn_at_sync_xst_copi : out t_mem_copi;
   reg_dp_block_validate_bsn_at_sync_xst_cipo : in  t_mem_cipo;

   -- XST ring MAC
   reg_tr_10GbE_mac_copi          : out t_mem_copi;
   reg_tr_10GbE_mac_cipo          : in  t_mem_cipo;

   -- XST ring ETH
   reg_tr_10GbE_eth10g_copi       : out t_mem_copi;
   reg_tr_10GbE_eth10g_cipo       : in  t_mem_cipo;

   -- Scrap ram
   ram_scrap_copi                 : out t_mem_copi;
   ram_scrap_cipo                 : in  t_mem_cipo;

   -- Jesd reset control
   jesd_ctrl_copi                 : out t_mem_copi;
   jesd_ctrl_cipo                 : in  t_mem_cipo
 );
end mmm_lofar2_unb2b_sdp_station;

architecture str of mmm_lofar2_unb2b_sdp_station is
  constant c_sim_node_nr   : natural := g_sim_node_nr;
  constant c_sim_node_type : string(1 to 2) := "FN";

  -- Use shorter alias name for c_unb2b_board_peripherals_mm_reg_default to easier fit line length, and without b, c
  -- to make mmm file for unb2b and unb2c more equal.
  constant c_unb2_mm_reg_default : t_c_unb2b_board_peripherals_mm_reg := c_unb2b_board_peripherals_mm_reg_default;

  signal i_reset_n         : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $UPE/sim.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    u_mm_file_reg_unb_system_info : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
      port map(mm_rst, mm_clk, reg_unb_system_info_copi, reg_unb_system_info_cipo);

    u_mm_file_rom_unb_system_info : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
      port map(mm_rst, mm_clk, rom_unb_system_info_copi, rom_unb_system_info_cipo);

    u_mm_file_reg_wdi : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
      port map(mm_rst, mm_clk, reg_wdi_copi, reg_wdi_cipo);

    u_mm_file_reg_unb_sens : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
      port map(mm_rst, mm_clk, reg_unb_sens_copi, reg_unb_sens_cipo);

    u_mm_file_reg_unb_pmbus : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_PMBUS")
      port map(mm_rst, mm_clk, reg_unb_pmbus_copi, reg_unb_pmbus_cipo);

    u_mm_file_reg_fpga_temp_sens : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_TEMP_SENS")
      port map(mm_rst, mm_clk, reg_fpga_temp_sens_copi, reg_fpga_temp_sens_cipo);

    u_mm_file_reg_fpga_voltage_sens : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_VOLTAGE_SENS")
      port map(mm_rst, mm_clk, reg_fpga_voltage_sens_copi, reg_fpga_voltage_sens_cipo);

    u_mm_file_reg_ppsh : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
      port map(mm_rst, mm_clk, reg_ppsh_copi, reg_ppsh_cipo);

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the
    -- ethernet <-> MM gateway.
    u_mm_file_reg_eth : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
      port map(mm_rst, mm_clk, eth1g_reg_copi, eth1g_reg_cipo);

    -- Must use exact g_mm_rd_latency = 1 instead of default 2, because JESD204B IP forces rddata = 0 after it has
    -- been read
    u_mm_file_jesd204b : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "JESD204B", '1', 1)
      port map(mm_rst, mm_clk, jesd204b_copi, jesd204b_cipo);

    u_mm_file_pio_jesd_ctrl : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_JESD_CTRL")
      port map(mm_rst, mm_clk, jesd_ctrl_copi, jesd_ctrl_cipo);

    u_mm_file_reg_dp_shiftram : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_SHIFTRAM")
      port map(mm_rst, mm_clk, reg_dp_shiftram_copi, reg_dp_shiftram_cipo);

    u_mm_file_reg_bsn_source_v2 : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_SOURCE_V2")
      port map(mm_rst, mm_clk, reg_bsn_source_v2_copi, reg_bsn_source_v2_cipo);

    u_mm_file_reg_bsn_scheduler : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_SCHEDULER")
      port map(mm_rst, mm_clk, reg_bsn_scheduler_copi, reg_bsn_scheduler_cipo);

    u_mm_file_reg_bsn_monitor_input : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_INPUT")
      port map(mm_rst, mm_clk, reg_bsn_monitor_input_copi, reg_bsn_monitor_input_cipo);

    u_mm_file_reg_wg : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WG")
      port map(mm_rst, mm_clk, reg_wg_copi, reg_wg_cipo);
    u_mm_file_ram_wg : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_WG")
      port map(mm_rst, mm_clk, ram_wg_copi, ram_wg_cipo);

    u_mm_file_ram_diag_data_buf_bsn : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_BSN")
      port map(mm_rst, mm_clk, ram_diag_data_buf_bsn_copi, ram_diag_data_buf_bsn_cipo);
    u_mm_file_reg_diag_data_buf_bsn : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_BSN")
      port map(mm_rst, mm_clk, reg_diag_data_buf_bsn_copi, reg_diag_data_buf_bsn_cipo);

    u_mm_file_ram_st_histogram : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_ST_HISTOGRAM")
      port map(mm_rst, mm_clk, ram_st_histogram_copi, ram_st_histogram_cipo);

    u_mm_file_reg_aduh_monitor : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_ADUH_MONITOR")
      port map(mm_rst, mm_clk, reg_aduh_monitor_copi, reg_aduh_monitor_cipo);

    u_mm_file_ram_st_sst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_ST_SST")
      port map(mm_rst, mm_clk, ram_st_sst_copi, ram_st_sst_cipo);

    u_mm_file_ram_fil_coefs : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_FIL_COEFS")
      port map(mm_rst, mm_clk, ram_fil_coefs_copi, ram_fil_coefs_cipo);

    u_mm_file_reg_si : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_SI")
      port map(mm_rst, mm_clk, reg_si_copi, reg_si_cipo);

    u_mm_file_ram_equalizer_gains : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_EQUALIZER_GAINS")
      port map(mm_rst, mm_clk, ram_equalizer_gains_copi, ram_equalizer_gains_cipo);
    u_mm_file_ram_equalizer_gains_cross : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_EQUALIZER_GAINS_CROSS")
      port map(mm_rst, mm_clk, ram_equalizer_gains_cross_copi, ram_equalizer_gains_cross_cipo);

    u_mm_file_reg_dp_selector : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_SELECTOR")
      port map(mm_rst, mm_clk, reg_dp_selector_copi, reg_dp_selector_cipo);

    u_mm_file_reg_sdp_info : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_SDP_INFO")
      port map(mm_rst, mm_clk, reg_sdp_info_copi, reg_sdp_info_cipo);

    u_mm_file_reg_ring_info : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_RING_INFO")
      port map(mm_rst, mm_clk, reg_ring_info_copi, reg_ring_info_cipo);

    u_mm_file_ram_ss_ss_wide : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_SS_SS_WIDE")
      port map(mm_rst, mm_clk, ram_ss_ss_wide_copi, ram_ss_ss_wide_cipo);

    u_mm_file_ram_bf_weights : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_BF_WEIGHTS")
      port map(mm_rst, mm_clk, ram_bf_weights_copi, ram_bf_weights_cipo);

    u_mm_file_reg_bf_scale : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BF_SCALE")
      port map(mm_rst, mm_clk, reg_bf_scale_copi, reg_bf_scale_cipo);

    u_mm_file_reg_hdr_dat : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_HDR_DAT")
      port map(mm_rst, mm_clk, reg_hdr_dat_copi, reg_hdr_dat_cipo);

    u_mm_file_reg_bdo_destinations : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BDO_DESTINATIONS")
      port map(mm_rst, mm_clk, reg_bdo_destinations_copi, reg_bdo_destinations_cipo);

    u_mm_file_reg_dp_xonoff : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_XONOFF")
      port map(mm_rst, mm_clk, reg_dp_xonoff_copi, reg_dp_xonoff_cipo);

    u_mm_file_ram_st_bst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_ST_BST")
      port map(mm_rst, mm_clk, ram_st_bst_copi, ram_st_bst_cipo);

    u_mm_file_reg_stat_enable_sst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STAT_ENABLE_SST")
      port map(mm_rst, mm_clk, reg_stat_enable_sst_copi, reg_stat_enable_sst_cipo);

    u_mm_file_reg_stat_hdr_info_sst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STAT_HDR_DAT_SST")
      port map(mm_rst, mm_clk, reg_stat_hdr_dat_sst_copi, reg_stat_hdr_dat_sst_cipo);

    u_mm_file_reg_stat_enable_xst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STAT_ENABLE_XST")
      port map(mm_rst, mm_clk, reg_stat_enable_xst_copi, reg_stat_enable_xst_cipo);

    u_mm_file_reg_stat_hdr_info_xst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STAT_HDR_DAT_XST")
      port map(mm_rst, mm_clk, reg_stat_hdr_dat_xst_copi, reg_stat_hdr_dat_xst_cipo);

    u_mm_file_reg_stat_enable_bst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STAT_ENABLE_BST")
      port map(mm_rst, mm_clk, reg_stat_enable_bst_copi, reg_stat_enable_bst_cipo);

    u_mm_file_reg_stat_hdr_info_bst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STAT_HDR_DAT_BST")
      port map(mm_rst, mm_clk, reg_stat_hdr_dat_bst_copi, reg_stat_hdr_dat_bst_cipo);

    u_mm_file_reg_crosslets_info : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_CROSSLETS_INFO")
      port map(mm_rst, mm_clk, reg_crosslets_info_copi, reg_crosslets_info_cipo);

    u_mm_file_reg_nof_crosslets : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_NOF_CROSSLETS")
      port map(mm_rst, mm_clk, reg_nof_crosslets_copi, reg_nof_crosslets_cipo);

    u_mm_file_reg_bsn_sync_scheduler_xsub : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_SYNC_SCHEDULER_XSUB")
      port map(mm_rst, mm_clk, reg_bsn_sync_scheduler_xsub_copi, reg_bsn_sync_scheduler_xsub_cipo);

    u_mm_file_ram_st_xsq : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_ST_XSQ")
      port map(mm_rst, mm_clk, ram_st_xsq_copi, ram_st_xsq_cipo);

    u_mm_file_reg_nw_10GbE_mac : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_NW_10GBE_MAC")
      port map(mm_rst, mm_clk, reg_nw_10GbE_mac_copi, reg_nw_10GbE_mac_cipo);

    u_mm_file_reg_nw_10GbE_eth10g : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_NW_10GBE_ETH10G")
      port map(mm_rst, mm_clk, reg_nw_10GbE_eth10g_copi, reg_nw_10GbE_eth10g_cipo);

    u_mm_file_reg_bsn_align_v2_bf : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_ALIGN_V2_BF")
      port map(mm_rst, mm_clk, reg_bsn_align_v2_bf_copi, reg_bsn_align_v2_bf_cipo);

    u_mm_file_reg_bsn_monitor_v2_rx_align_bf : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RX_ALIGN_BF")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_rx_align_bf_copi, reg_bsn_monitor_v2_rx_align_bf_cipo);

    u_mm_file_reg_bsn_monitor_v2_aligned_bf : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_ALIGNED_BF")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_aligned_bf_copi, reg_bsn_monitor_v2_aligned_bf_cipo);

    u_mm_file_reg_ring_lane_info_bf : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_RING_LANE_INFO_BF")
      port map(mm_rst, mm_clk, reg_ring_lane_info_bf_copi, reg_ring_lane_info_bf_cipo);

    u_mm_file_reg_bsn_monitor_v2_ring_rx_bf : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RING_RX_BF")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_ring_rx_bf_copi, reg_bsn_monitor_v2_ring_rx_bf_cipo);

    u_mm_file_reg_bsn_monitor_v2_ring_tx_bf : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RING_TX_BF")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_ring_tx_bf_copi, reg_bsn_monitor_v2_ring_tx_bf_cipo);

    u_mm_file_reg_dp_block_validate_err_bf : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_BLOCK_VALIDATE_ERR_BF")
      port map(mm_rst, mm_clk, reg_dp_block_validate_err_bf_copi, reg_dp_block_validate_err_bf_cipo);

    u_mm_file_reg_dp_block_validate_bsn_at_sync_bf : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) &
                  "REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC_BF")
      port map(mm_rst, mm_clk, reg_dp_block_validate_bsn_at_sync_bf_copi, reg_dp_block_validate_bsn_at_sync_bf_cipo);

    u_mm_file_reg_bsn_align_v2_xsub : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_ALIGN_V2_XSUB")
      port map(mm_rst, mm_clk, reg_bsn_align_v2_xsub_copi, reg_bsn_align_v2_xsub_cipo);

    u_mm_file_reg_bsn_monitor_v2_rx_align_xsub : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) &
                  "REG_BSN_MONITOR_V2_RX_ALIGN_XSUB")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_rx_align_xsub_copi, reg_bsn_monitor_v2_rx_align_xsub_cipo);

    u_mm_file_reg_bsn_monitor_v2_aligned_xsub : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_ALIGNED_XSUB")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_aligned_xsub_copi, reg_bsn_monitor_v2_aligned_xsub_cipo);

    u_mm_file_reg_bsn_monitor_v2_sst_offload : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_SST_OFFLOAD")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_sst_offload_copi, reg_bsn_monitor_v2_sst_offload_cipo);

    u_mm_file_reg_bsn_monitor_v2_bst_offload : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_BST_OFFLOAD")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_bst_offload_copi, reg_bsn_monitor_v2_bst_offload_cipo);

    u_mm_file_reg_bsn_monitor_v2_beamlet_output : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) &
                  "REG_BSN_MONITOR_V2_BEAMLET_OUTPUT")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_beamlet_output_copi, reg_bsn_monitor_v2_beamlet_output_cipo);

    u_mm_file_reg_bsn_monitor_v2_xst_offload : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_XST_OFFLOAD")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_xst_offload_copi, reg_bsn_monitor_v2_xst_offload_cipo);

    u_mm_file_reg_ring_lane_info_xst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_RING_LANE_INFO_XST")
      port map(mm_rst, mm_clk, reg_ring_lane_info_xst_copi, reg_ring_lane_info_xst_cipo);

    u_mm_file_reg_bsn_monitor_v2_ring_rx_xst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RING_RX_XST")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_ring_rx_xst_copi, reg_bsn_monitor_v2_ring_rx_xst_cipo);

    u_mm_file_reg_bsn_monitor_v2_ring_tx_xst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RING_TX_XST")
      port map(mm_rst, mm_clk, reg_bsn_monitor_v2_ring_tx_xst_copi, reg_bsn_monitor_v2_ring_tx_xst_cipo);

    u_mm_file_reg_dp_block_validate_err_xst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_BLOCK_VALIDATE_ERR_XST")
      port map(mm_rst, mm_clk, reg_dp_block_validate_err_xst_copi, reg_dp_block_validate_err_xst_cipo);

    u_mm_file_reg_dp_block_validate_bsn_at_sync_xst : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) &
                  "REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC_XST")
      port map(mm_rst, mm_clk, reg_dp_block_validate_bsn_at_sync_xst_copi, reg_dp_block_validate_bsn_at_sync_xst_cipo);

    u_mm_file_reg_tr_10GbE_mac : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_MAC")
      port map(mm_rst, mm_clk, reg_tr_10GbE_mac_copi, reg_tr_10GbE_mac_cipo);

    u_mm_file_reg_tr_10GbE_eth10g : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_ETH10G")
      port map(mm_rst, mm_clk, reg_tr_10GbE_eth10g_copi, reg_tr_10GbE_eth10g_cipo);

    u_mm_file_ram_scrap : mm_file
      generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_SCRAP")
      port map(mm_rst, mm_clk, ram_scrap_copi, ram_scrap_cipo);
    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(mm_clk, c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  i_reset_n <= not mm_rst;
  ----------------------------------------------------------------------------
  -- QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys : if g_sim = false generate
    u_qsys : qsys_lofar2_unb2b_sdp_station
    port map (

      clk_clk       => mm_clk,
      reset_reset_n => i_reset_n,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2b_board.
      pio_wdi_external_connection_export => pout_wdi,

      avs_eth_0_reset_export           => eth1g_mm_rst,
      avs_eth_0_clk_export             => OPEN,
      avs_eth_0_tse_address_export     => eth1g_tse_copi.address(c_unb2_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export       => eth1g_tse_copi.wr,
      avs_eth_0_tse_read_export        => eth1g_tse_copi.rd,
      avs_eth_0_tse_writedata_export   => eth1g_tse_copi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export    => eth1g_tse_cipo.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export => eth1g_tse_cipo.waitrequest,
      avs_eth_0_reg_address_export     => eth1g_reg_copi.address(c_unb2_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export       => eth1g_reg_copi.wr,
      avs_eth_0_reg_read_export        => eth1g_reg_copi.rd,
      avs_eth_0_reg_writedata_export   => eth1g_reg_copi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export    => eth1g_reg_cipo.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export     => eth1g_ram_copi.address(c_unb2_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export       => eth1g_ram_copi.wr,
      avs_eth_0_ram_read_export        => eth1g_ram_copi.rd,
      avs_eth_0_ram_writedata_export   => eth1g_ram_copi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export    => eth1g_ram_cipo.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export             => eth1g_reg_interrupt,

      reg_unb_sens_reset_export     => OPEN,
      reg_unb_sens_clk_export       => OPEN,
      reg_unb_sens_address_export   => reg_unb_sens_copi.address(c_unb2_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      reg_unb_sens_write_export     => reg_unb_sens_copi.wr,
      reg_unb_sens_writedata_export => reg_unb_sens_copi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_read_export      => reg_unb_sens_copi.rd,
      reg_unb_sens_readdata_export  => reg_unb_sens_cipo.rddata(c_word_w - 1 downto 0),

      reg_unb_pmbus_reset_export     => OPEN,
      reg_unb_pmbus_clk_export       => OPEN,
      reg_unb_pmbus_address_export   => reg_unb_pmbus_copi.address(
          c_unb2_mm_reg_default.reg_unb_pmbus_adr_w - 1 downto 0),
      reg_unb_pmbus_write_export     => reg_unb_pmbus_copi.wr,
      reg_unb_pmbus_writedata_export => reg_unb_pmbus_copi.wrdata(c_word_w - 1 downto 0),
      reg_unb_pmbus_read_export      => reg_unb_pmbus_copi.rd,
      reg_unb_pmbus_readdata_export  => reg_unb_pmbus_cipo.rddata(c_word_w - 1 downto 0),

      reg_fpga_temp_sens_reset_export     => OPEN,
      reg_fpga_temp_sens_clk_export       => OPEN,
      reg_fpga_temp_sens_address_export   => reg_fpga_temp_sens_copi.address(
          c_unb2_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export     => reg_fpga_temp_sens_copi.wr,
      reg_fpga_temp_sens_writedata_export => reg_fpga_temp_sens_copi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export      => reg_fpga_temp_sens_copi.rd,
      reg_fpga_temp_sens_readdata_export  => reg_fpga_temp_sens_cipo.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_reset_export     => OPEN,
      reg_fpga_voltage_sens_clk_export       => OPEN,
      reg_fpga_voltage_sens_address_export   => reg_fpga_voltage_sens_copi.address(
          c_unb2_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export     => reg_fpga_voltage_sens_copi.wr,
      reg_fpga_voltage_sens_writedata_export => reg_fpga_voltage_sens_copi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export      => reg_fpga_voltage_sens_copi.rd,
      reg_fpga_voltage_sens_readdata_export  => reg_fpga_voltage_sens_cipo.rddata(c_word_w - 1 downto 0),

      rom_system_info_reset_export     => OPEN,
      rom_system_info_clk_export       => OPEN,
--    ToDo: This has changed in the peripherals package
--      rom_system_info_address_export   => rom_unb_system_info_copi.address(9 DOWNTO 0),
      rom_system_info_address_export   => rom_unb_system_info_copi.address(
          c_unb2_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      rom_system_info_write_export     => rom_unb_system_info_copi.wr,
      rom_system_info_writedata_export => rom_unb_system_info_copi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export      => rom_unb_system_info_copi.rd,
      rom_system_info_readdata_export  => rom_unb_system_info_cipo.rddata(c_word_w - 1 downto 0),

      pio_system_info_reset_export     => OPEN,
      pio_system_info_clk_export       => OPEN,
      pio_system_info_address_export   => reg_unb_system_info_copi.address(
          c_unb2_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export     => reg_unb_system_info_copi.wr,
      pio_system_info_writedata_export => reg_unb_system_info_copi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export      => reg_unb_system_info_copi.rd,
      pio_system_info_readdata_export  => reg_unb_system_info_cipo.rddata(c_word_w - 1 downto 0),

      pio_pps_reset_export     => OPEN,
      pio_pps_clk_export       => OPEN,
      pio_pps_address_export   => reg_ppsh_copi.address(c_unb2_mm_reg_default.reg_ppsh_adr_w - 1 downto 0),
      pio_pps_write_export     => reg_ppsh_copi.wr,
      pio_pps_writedata_export => reg_ppsh_copi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export      => reg_ppsh_copi.rd,
      pio_pps_readdata_export  => reg_ppsh_cipo.rddata(c_word_w - 1 downto 0),

      reg_wdi_reset_export     => OPEN,
      reg_wdi_clk_export       => OPEN,
      reg_wdi_address_export   => reg_wdi_copi.address(0 downto 0),
      reg_wdi_write_export     => reg_wdi_copi.wr,
      reg_wdi_writedata_export => reg_wdi_copi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export      => reg_wdi_copi.rd,
      reg_wdi_readdata_export  => reg_wdi_cipo.rddata(c_word_w - 1 downto 0),

      reg_remu_reset_export     => OPEN,
      reg_remu_clk_export       => OPEN,
      reg_remu_address_export   => reg_remu_copi.address(c_unb2_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export     => reg_remu_copi.wr,
      reg_remu_writedata_export => reg_remu_copi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export      => reg_remu_copi.rd,
      reg_remu_readdata_export  => reg_remu_cipo.rddata(c_word_w - 1 downto 0),

      jesd204b_reset_export     => OPEN,
      jesd204b_clk_export       => OPEN,
      jesd204b_address_export   => jesd204b_copi.address(c_sdp_jesd204b_addr_w - 1 downto 0),
      jesd204b_write_export     => jesd204b_copi.wr,
      jesd204b_writedata_export => jesd204b_copi.wrdata(c_word_w - 1 downto 0),
      jesd204b_read_export      => jesd204b_copi.rd,
      jesd204b_readdata_export  => jesd204b_cipo.rddata(c_word_w - 1 downto 0),

      pio_jesd_ctrl_reset_export     => OPEN,
      pio_jesd_ctrl_clk_export       => OPEN,
      pio_jesd_ctrl_address_export   => jesd_ctrl_copi.address(c_sdp_jesd_ctrl_addr_w - 1 downto 0),
      pio_jesd_ctrl_write_export     => jesd_ctrl_copi.wr,
      pio_jesd_ctrl_writedata_export => jesd_ctrl_copi.wrdata(c_word_w - 1 downto 0),
      pio_jesd_ctrl_read_export      => jesd_ctrl_copi.rd,
      pio_jesd_ctrl_readdata_export  => jesd_ctrl_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_input_address_export   => reg_bsn_monitor_input_copi.address(
          c_sdp_reg_bsn_monitor_input_addr_w - 1 downto 0),
      reg_bsn_monitor_input_clk_export       => OPEN,
      reg_bsn_monitor_input_read_export      => reg_bsn_monitor_input_copi.rd,
      reg_bsn_monitor_input_readdata_export  => reg_bsn_monitor_input_cipo.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_input_reset_export     => OPEN,
      reg_bsn_monitor_input_write_export     => reg_bsn_monitor_input_copi.wr,
      reg_bsn_monitor_input_writedata_export => reg_bsn_monitor_input_copi.wrdata(c_word_w - 1 downto 0),

      -- waveform generators (multiplexed)
      reg_wg_clk_export       => OPEN,
      reg_wg_reset_export     => OPEN,
      reg_wg_address_export   => reg_wg_copi.address(c_sdp_reg_wg_addr_w - 1 downto 0),
      reg_wg_read_export      => reg_wg_copi.rd,
      reg_wg_readdata_export  => reg_wg_cipo.rddata(c_word_w - 1 downto 0),
      reg_wg_write_export     => reg_wg_copi.wr,
      reg_wg_writedata_export => reg_wg_copi.wrdata(c_word_w - 1 downto 0),

      ram_wg_clk_export       => OPEN,
      ram_wg_reset_export     => OPEN,
      ram_wg_address_export   => ram_wg_copi.address(c_sdp_ram_wg_addr_w - 1 downto 0),
      ram_wg_read_export      => ram_wg_copi.rd,
      ram_wg_readdata_export  => ram_wg_cipo.rddata(c_word_w - 1 downto 0),
      ram_wg_write_export     => ram_wg_copi.wr,
      ram_wg_writedata_export => ram_wg_copi.wrdata(c_word_w - 1 downto 0),

      reg_dp_shiftram_clk_export       => OPEN,
      reg_dp_shiftram_reset_export     => OPEN,
      reg_dp_shiftram_address_export   => reg_dp_shiftram_copi.address(c_sdp_reg_dp_shiftram_addr_w - 1 downto 0),
      reg_dp_shiftram_read_export      => reg_dp_shiftram_copi.rd,
      reg_dp_shiftram_readdata_export  => reg_dp_shiftram_cipo.rddata(c_word_w - 1 downto 0),
      reg_dp_shiftram_write_export     => reg_dp_shiftram_copi.wr,
      reg_dp_shiftram_writedata_export => reg_dp_shiftram_copi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_source_v2_clk_export       => OPEN,
      reg_bsn_source_v2_reset_export     => OPEN,
      reg_bsn_source_v2_address_export   => reg_bsn_source_v2_copi.address(c_sdp_reg_bsn_source_v2_addr_w - 1 downto 0),
      reg_bsn_source_v2_read_export      => reg_bsn_source_v2_copi.rd,
      reg_bsn_source_v2_readdata_export  => reg_bsn_source_v2_cipo.rddata(c_word_w - 1 downto 0),
      reg_bsn_source_v2_write_export     => reg_bsn_source_v2_copi.wr,
      reg_bsn_source_v2_writedata_export => reg_bsn_source_v2_copi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_scheduler_clk_export       => OPEN,
      reg_bsn_scheduler_reset_export     => OPEN,
      reg_bsn_scheduler_address_export   => reg_bsn_scheduler_copi.address(c_sdp_reg_bsn_scheduler_addr_w - 1 downto 0),
      reg_bsn_scheduler_read_export      => reg_bsn_scheduler_copi.rd,
      reg_bsn_scheduler_readdata_export  => reg_bsn_scheduler_cipo.rddata(c_word_w - 1 downto 0),
      reg_bsn_scheduler_write_export     => reg_bsn_scheduler_copi.wr,
      reg_bsn_scheduler_writedata_export => reg_bsn_scheduler_copi.wrdata(c_word_w - 1 downto 0),

      reg_epcs_reset_export     => OPEN,
      reg_epcs_clk_export       => OPEN,
      reg_epcs_address_export   => reg_epcs_copi.address(c_unb2_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export     => reg_epcs_copi.wr,
      reg_epcs_writedata_export => reg_epcs_copi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export      => reg_epcs_copi.rd,
      reg_epcs_readdata_export  => reg_epcs_cipo.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_reset_export     => OPEN,
      reg_dpmm_ctrl_clk_export       => OPEN,
      reg_dpmm_ctrl_address_export   => reg_dpmm_ctrl_copi.address(0 downto 0),
      reg_dpmm_ctrl_write_export     => reg_dpmm_ctrl_copi.wr,
      reg_dpmm_ctrl_writedata_export => reg_dpmm_ctrl_copi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export      => reg_dpmm_ctrl_copi.rd,
      reg_dpmm_ctrl_readdata_export  => reg_dpmm_ctrl_cipo.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_reset_export     => OPEN,
      reg_mmdp_data_clk_export       => OPEN,
      reg_mmdp_data_address_export   => reg_mmdp_data_copi.address(0 downto 0),
      reg_mmdp_data_write_export     => reg_mmdp_data_copi.wr,
      reg_mmdp_data_writedata_export => reg_mmdp_data_copi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export      => reg_mmdp_data_copi.rd,
      reg_mmdp_data_readdata_export  => reg_mmdp_data_cipo.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_reset_export     => OPEN,
      reg_dpmm_data_clk_export       => OPEN,
      reg_dpmm_data_address_export   => reg_dpmm_data_copi.address(0 downto 0),
      reg_dpmm_data_read_export      => reg_dpmm_data_copi.rd,
      reg_dpmm_data_readdata_export  => reg_dpmm_data_cipo.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export     => reg_dpmm_data_copi.wr,
      reg_dpmm_data_writedata_export => reg_dpmm_data_copi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_reset_export     => OPEN,
      reg_mmdp_ctrl_clk_export       => OPEN,
      reg_mmdp_ctrl_address_export   => reg_mmdp_ctrl_copi.address(0 downto 0),
      reg_mmdp_ctrl_read_export      => reg_mmdp_ctrl_copi.rd,
      reg_mmdp_ctrl_readdata_export  => reg_mmdp_ctrl_cipo.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export     => reg_mmdp_ctrl_copi.wr,
      reg_mmdp_ctrl_writedata_export => reg_mmdp_ctrl_copi.wrdata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_bsn_clk_export       => OPEN,
      ram_diag_data_buffer_bsn_reset_export     => OPEN,
      ram_diag_data_buffer_bsn_address_export   => ram_diag_data_buf_bsn_copi.address(
          c_sdp_ram_diag_data_buf_bsn_addr_w - 1 downto 0),
      ram_diag_data_buffer_bsn_write_export     => ram_diag_data_buf_bsn_copi.wr,
      ram_diag_data_buffer_bsn_writedata_export => ram_diag_data_buf_bsn_copi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_bsn_read_export      => ram_diag_data_buf_bsn_copi.rd,
      ram_diag_data_buffer_bsn_readdata_export  => ram_diag_data_buf_bsn_cipo.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_bsn_reset_export     => OPEN,
      reg_diag_data_buffer_bsn_clk_export       => OPEN,
      reg_diag_data_buffer_bsn_address_export   => reg_diag_data_buf_bsn_copi.address(
          c_sdp_reg_diag_data_buf_bsn_addr_w - 1 downto 0),
      reg_diag_data_buffer_bsn_write_export     => reg_diag_data_buf_bsn_copi.wr,
      reg_diag_data_buffer_bsn_writedata_export => reg_diag_data_buf_bsn_copi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_bsn_read_export      => reg_diag_data_buf_bsn_copi.rd,
      reg_diag_data_buffer_bsn_readdata_export  => reg_diag_data_buf_bsn_cipo.rddata(c_word_w - 1 downto 0),

      ram_st_histogram_clk_export       => OPEN,
      ram_st_histogram_reset_export     => OPEN,
      ram_st_histogram_address_export   => ram_st_histogram_copi.address(c_sdp_ram_st_histogram_addr_w - 1 downto 0),
      ram_st_histogram_write_export     => ram_st_histogram_copi.wr,
      ram_st_histogram_writedata_export => ram_st_histogram_copi.wrdata(c_word_w - 1 downto 0),
      ram_st_histogram_read_export      => ram_st_histogram_copi.rd,
      ram_st_histogram_readdata_export  => ram_st_histogram_cipo.rddata(c_word_w - 1 downto 0),

      reg_aduh_monitor_reset_export     => OPEN,
      reg_aduh_monitor_clk_export       => OPEN,
      reg_aduh_monitor_address_export   => reg_aduh_monitor_copi.address(c_sdp_reg_aduh_monitor_addr_w - 1 downto 0),
      reg_aduh_monitor_write_export     => reg_aduh_monitor_copi.wr,
      reg_aduh_monitor_writedata_export => reg_aduh_monitor_copi.wrdata(c_word_w - 1 downto 0),
      reg_aduh_monitor_read_export      => reg_aduh_monitor_copi.rd,
      reg_aduh_monitor_readdata_export  => reg_aduh_monitor_cipo.rddata(c_word_w - 1 downto 0),

      ram_fil_coefs_clk_export       => OPEN,
      ram_fil_coefs_reset_export     => OPEN,
      ram_fil_coefs_address_export   => ram_fil_coefs_copi.address(c_sdp_ram_fil_coefs_addr_w - 1 downto 0),
      ram_fil_coefs_write_export     => ram_fil_coefs_copi.wr,
      ram_fil_coefs_writedata_export => ram_fil_coefs_copi.wrdata(c_word_w - 1 downto 0),
      ram_fil_coefs_read_export      => ram_fil_coefs_copi.rd,
      ram_fil_coefs_readdata_export  => ram_fil_coefs_cipo.rddata(c_word_w - 1 downto 0),

      ram_st_sst_clk_export       => OPEN,
      ram_st_sst_reset_export     => OPEN,
      ram_st_sst_address_export   => ram_st_sst_copi.address(c_sdp_ram_st_sst_addr_w - 1 downto 0),
      ram_st_sst_write_export     => ram_st_sst_copi.wr,
      ram_st_sst_writedata_export => ram_st_sst_copi.wrdata(c_word_w - 1 downto 0),
      ram_st_sst_read_export      => ram_st_sst_copi.rd,
      ram_st_sst_readdata_export  => ram_st_sst_cipo.rddata(c_word_w - 1 downto 0),

      reg_si_clk_export       => OPEN,
      reg_si_reset_export     => OPEN,
      reg_si_address_export   => reg_si_copi.address(c_sdp_reg_si_addr_w - 1 downto 0),
      reg_si_write_export     => reg_si_copi.wr,
      reg_si_writedata_export => reg_si_copi.wrdata(c_word_w - 1 downto 0),
      reg_si_read_export      => reg_si_copi.rd,
      reg_si_readdata_export  => reg_si_cipo.rddata(c_word_w - 1 downto 0),

      ram_equalizer_gains_clk_export       => OPEN,
      ram_equalizer_gains_reset_export     => OPEN,
      ram_equalizer_gains_address_export   => ram_equalizer_gains_copi.address(
          c_sdp_ram_equalizer_gains_addr_w - 1 downto 0),
      ram_equalizer_gains_write_export     => ram_equalizer_gains_copi.wr,
      ram_equalizer_gains_writedata_export => ram_equalizer_gains_copi.wrdata(c_word_w - 1 downto 0),
      ram_equalizer_gains_read_export      => ram_equalizer_gains_copi.rd,
      ram_equalizer_gains_readdata_export  => ram_equalizer_gains_cipo.rddata(c_word_w - 1 downto 0),

      ram_equalizer_gains_cross_clk_export       => OPEN,
      ram_equalizer_gains_cross_reset_export     => OPEN,
      ram_equalizer_gains_cross_address_export   => ram_equalizer_gains_cross_copi.address(
          c_sdp_ram_equalizer_gains_addr_w - 1 downto 0),
      ram_equalizer_gains_cross_write_export     => ram_equalizer_gains_cross_copi.wr,
      ram_equalizer_gains_cross_writedata_export => ram_equalizer_gains_cross_copi.wrdata(c_word_w - 1 downto 0),
      ram_equalizer_gains_cross_read_export      => ram_equalizer_gains_cross_copi.rd,
      ram_equalizer_gains_cross_readdata_export  => ram_equalizer_gains_cross_cipo.rddata(c_word_w - 1 downto 0),

      reg_dp_selector_clk_export       => OPEN,
      reg_dp_selector_reset_export     => OPEN,
      reg_dp_selector_address_export   => reg_dp_selector_copi.address(c_sdp_reg_dp_selector_addr_w - 1 downto 0),
      reg_dp_selector_write_export     => reg_dp_selector_copi.wr,
      reg_dp_selector_writedata_export => reg_dp_selector_copi.wrdata(c_word_w - 1 downto 0),
      reg_dp_selector_read_export      => reg_dp_selector_copi.rd,
      reg_dp_selector_readdata_export  => reg_dp_selector_cipo.rddata(c_word_w - 1 downto 0),

      reg_sdp_info_clk_export       => OPEN,
      reg_sdp_info_reset_export     => OPEN,
      reg_sdp_info_address_export   => reg_sdp_info_copi.address(c_sdp_reg_sdp_info_addr_w - 1 downto 0),
      reg_sdp_info_write_export     => reg_sdp_info_copi.wr,
      reg_sdp_info_writedata_export => reg_sdp_info_copi.wrdata(c_word_w - 1 downto 0),
      reg_sdp_info_read_export      => reg_sdp_info_copi.rd,
      reg_sdp_info_readdata_export  => reg_sdp_info_cipo.rddata(c_word_w - 1 downto 0),

      reg_ring_info_clk_export       => OPEN,
      reg_ring_info_reset_export     => OPEN,
      reg_ring_info_address_export   => reg_ring_info_copi.address(c_sdp_reg_ring_info_addr_w - 1 downto 0),
      reg_ring_info_write_export     => reg_ring_info_copi.wr,
      reg_ring_info_writedata_export => reg_ring_info_copi.wrdata(c_word_w - 1 downto 0),
      reg_ring_info_read_export      => reg_ring_info_copi.rd,
      reg_ring_info_readdata_export  => reg_ring_info_cipo.rddata(c_word_w - 1 downto 0),

      ram_ss_ss_wide_clk_export       => OPEN,
      ram_ss_ss_wide_reset_export     => OPEN,
      ram_ss_ss_wide_address_export   => ram_ss_ss_wide_copi.address(c_sdp_ram_ss_ss_wide_addr_w - 1 downto 0),
      ram_ss_ss_wide_write_export     => ram_ss_ss_wide_copi.wr,
      ram_ss_ss_wide_writedata_export => ram_ss_ss_wide_copi.wrdata(c_word_w - 1 downto 0),
      ram_ss_ss_wide_read_export      => ram_ss_ss_wide_copi.rd,
      ram_ss_ss_wide_readdata_export  => ram_ss_ss_wide_cipo.rddata(c_word_w - 1 downto 0),

      ram_bf_weights_clk_export       => OPEN,
      ram_bf_weights_reset_export     => OPEN,
      ram_bf_weights_address_export   => ram_bf_weights_copi.address(c_sdp_ram_bf_weights_addr_w - 1 downto 0),
      ram_bf_weights_write_export     => ram_bf_weights_copi.wr,
      ram_bf_weights_writedata_export => ram_bf_weights_copi.wrdata(c_word_w - 1 downto 0),
      ram_bf_weights_read_export      => ram_bf_weights_copi.rd,
      ram_bf_weights_readdata_export  => ram_bf_weights_cipo.rddata(c_word_w - 1 downto 0),

      reg_bf_scale_clk_export       => OPEN,
      reg_bf_scale_reset_export     => OPEN,
      reg_bf_scale_address_export   => reg_bf_scale_copi.address(c_sdp_reg_bf_scale_addr_w - 1 downto 0),
      reg_bf_scale_write_export     => reg_bf_scale_copi.wr,
      reg_bf_scale_writedata_export => reg_bf_scale_copi.wrdata(c_word_w - 1 downto 0),
      reg_bf_scale_read_export      => reg_bf_scale_copi.rd,
      reg_bf_scale_readdata_export  => reg_bf_scale_cipo.rddata(c_word_w - 1 downto 0),

      reg_hdr_dat_clk_export       => OPEN,
      reg_hdr_dat_reset_export     => OPEN,
      reg_hdr_dat_address_export   => reg_hdr_dat_copi.address(c_sdp_reg_bf_hdr_dat_addr_w - 1 downto 0),
      reg_hdr_dat_write_export     => reg_hdr_dat_copi.wr,
      reg_hdr_dat_writedata_export => reg_hdr_dat_copi.wrdata(c_word_w - 1 downto 0),
      reg_hdr_dat_read_export      => reg_hdr_dat_copi.rd,
      reg_hdr_dat_readdata_export  => reg_hdr_dat_cipo.rddata(c_word_w - 1 downto 0),

      reg_bdo_destinations_clk_export       => OPEN,
      reg_bdo_destinations_reset_export     => OPEN,
      reg_bdo_destinations_address_export   => reg_bdo_destinations_copi.address(
          c_sdp_reg_bdo_destinations_info_w - 1 downto 0),
      reg_bdo_destinations_write_export     => reg_bdo_destinations_copi.wr,
      reg_bdo_destinations_writedata_export => reg_bdo_destinations_copi.wrdata(c_word_w - 1 downto 0),
      reg_bdo_destinations_read_export      => reg_bdo_destinations_copi.rd,
      reg_bdo_destinations_readdata_export  => reg_bdo_destinations_cipo.rddata(c_word_w - 1 downto 0),

      reg_dp_xonoff_clk_export       => OPEN,
      reg_dp_xonoff_reset_export     => OPEN,
      reg_dp_xonoff_address_export   => reg_dp_xonoff_copi.address(c_sdp_reg_dp_xonoff_addr_w - 1 downto 0),
      reg_dp_xonoff_write_export     => reg_dp_xonoff_copi.wr,
      reg_dp_xonoff_writedata_export => reg_dp_xonoff_copi.wrdata(c_word_w - 1 downto 0),
      reg_dp_xonoff_read_export      => reg_dp_xonoff_copi.rd,
      reg_dp_xonoff_readdata_export  => reg_dp_xonoff_cipo.rddata(c_word_w - 1 downto 0),

      ram_st_bst_clk_export       => OPEN,
      ram_st_bst_reset_export     => OPEN,
      ram_st_bst_address_export   => ram_st_bst_copi.address(c_sdp_ram_st_bst_addr_w - 1 downto 0),
      ram_st_bst_write_export     => ram_st_bst_copi.wr,
      ram_st_bst_writedata_export => ram_st_bst_copi.wrdata(c_word_w - 1 downto 0),
      ram_st_bst_read_export      => ram_st_bst_copi.rd,
      ram_st_bst_readdata_export  => ram_st_bst_cipo.rddata(c_word_w - 1 downto 0),

      reg_stat_enable_sst_clk_export       => OPEN,
      reg_stat_enable_sst_reset_export     => OPEN,
      reg_stat_enable_sst_address_export   => reg_stat_enable_sst_copi.address(
          c_sdp_reg_stat_enable_addr_w - 1 downto 0),
      reg_stat_enable_sst_write_export     => reg_stat_enable_sst_copi.wr,
      reg_stat_enable_sst_writedata_export => reg_stat_enable_sst_copi.wrdata(c_word_w - 1 downto 0),
      reg_stat_enable_sst_read_export      => reg_stat_enable_sst_copi.rd,
      reg_stat_enable_sst_readdata_export  => reg_stat_enable_sst_cipo.rddata(c_word_w - 1 downto 0),

      reg_stat_hdr_dat_sst_clk_export       => OPEN,
      reg_stat_hdr_dat_sst_reset_export     => OPEN,
      reg_stat_hdr_dat_sst_address_export   => reg_stat_hdr_dat_sst_copi.address(
          c_sdp_reg_stat_hdr_dat_addr_w - 1 downto 0),
      reg_stat_hdr_dat_sst_write_export     => reg_stat_hdr_dat_sst_copi.wr,
      reg_stat_hdr_dat_sst_writedata_export => reg_stat_hdr_dat_sst_copi.wrdata(c_word_w - 1 downto 0),
      reg_stat_hdr_dat_sst_read_export      => reg_stat_hdr_dat_sst_copi.rd,
      reg_stat_hdr_dat_sst_readdata_export  => reg_stat_hdr_dat_sst_cipo.rddata(c_word_w - 1 downto 0),

      reg_stat_enable_xst_clk_export       => OPEN,
      reg_stat_enable_xst_reset_export     => OPEN,
      reg_stat_enable_xst_address_export   => reg_stat_enable_xst_copi.address(
          c_sdp_reg_stat_enable_addr_w - 1 downto 0),
      reg_stat_enable_xst_write_export     => reg_stat_enable_xst_copi.wr,
      reg_stat_enable_xst_writedata_export => reg_stat_enable_xst_copi.wrdata(c_word_w - 1 downto 0),
      reg_stat_enable_xst_read_export      => reg_stat_enable_xst_copi.rd,
      reg_stat_enable_xst_readdata_export  => reg_stat_enable_xst_cipo.rddata(c_word_w - 1 downto 0),

      reg_stat_hdr_dat_xst_clk_export       => OPEN,
      reg_stat_hdr_dat_xst_reset_export     => OPEN,
      reg_stat_hdr_dat_xst_address_export   => reg_stat_hdr_dat_xst_copi.address(
          c_sdp_reg_stat_hdr_dat_addr_w - 1 downto 0),
      reg_stat_hdr_dat_xst_write_export     => reg_stat_hdr_dat_xst_copi.wr,
      reg_stat_hdr_dat_xst_writedata_export => reg_stat_hdr_dat_xst_copi.wrdata(c_word_w - 1 downto 0),
      reg_stat_hdr_dat_xst_read_export      => reg_stat_hdr_dat_xst_copi.rd,
      reg_stat_hdr_dat_xst_readdata_export  => reg_stat_hdr_dat_xst_cipo.rddata(c_word_w - 1 downto 0),

      reg_stat_enable_bst_clk_export       => OPEN,
      reg_stat_enable_bst_reset_export     => OPEN,
      reg_stat_enable_bst_address_export   => reg_stat_enable_bst_copi.address(
          c_sdp_reg_stat_enable_bst_addr_w - 1 downto 0),
      reg_stat_enable_bst_write_export     => reg_stat_enable_bst_copi.wr,
      reg_stat_enable_bst_writedata_export => reg_stat_enable_bst_copi.wrdata(c_word_w - 1 downto 0),
      reg_stat_enable_bst_read_export      => reg_stat_enable_bst_copi.rd,
      reg_stat_enable_bst_readdata_export  => reg_stat_enable_bst_cipo.rddata(c_word_w - 1 downto 0),

      reg_stat_hdr_dat_bst_clk_export       => OPEN,
      reg_stat_hdr_dat_bst_reset_export     => OPEN,
      reg_stat_hdr_dat_bst_address_export   => reg_stat_hdr_dat_bst_copi.address(
          c_sdp_reg_stat_hdr_dat_bst_addr_w - 1 downto 0),
      reg_stat_hdr_dat_bst_write_export     => reg_stat_hdr_dat_bst_copi.wr,
      reg_stat_hdr_dat_bst_writedata_export => reg_stat_hdr_dat_bst_copi.wrdata(c_word_w - 1 downto 0),
      reg_stat_hdr_dat_bst_read_export      => reg_stat_hdr_dat_bst_copi.rd,
      reg_stat_hdr_dat_bst_readdata_export  => reg_stat_hdr_dat_bst_cipo.rddata(c_word_w - 1 downto 0),

      reg_crosslets_info_clk_export       => OPEN,
      reg_crosslets_info_reset_export     => OPEN,
      reg_crosslets_info_address_export   => reg_crosslets_info_copi.address(
          c_sdp_reg_crosslets_info_addr_w - 1 downto 0),
      reg_crosslets_info_write_export     => reg_crosslets_info_copi.wr,
      reg_crosslets_info_writedata_export => reg_crosslets_info_copi.wrdata(c_word_w - 1 downto 0),
      reg_crosslets_info_read_export      => reg_crosslets_info_copi.rd,
      reg_crosslets_info_readdata_export  => reg_crosslets_info_cipo.rddata(c_word_w - 1 downto 0),

      reg_nof_crosslets_clk_export       => OPEN,
      reg_nof_crosslets_reset_export     => OPEN,
      reg_nof_crosslets_address_export   => reg_nof_crosslets_copi.address(
          c_sdp_reg_nof_crosslets_addr_w - 1 downto 0),
      reg_nof_crosslets_write_export     => reg_nof_crosslets_copi.wr,
      reg_nof_crosslets_writedata_export => reg_nof_crosslets_copi.wrdata(c_word_w - 1 downto 0),
      reg_nof_crosslets_read_export      => reg_nof_crosslets_copi.rd,
      reg_nof_crosslets_readdata_export  => reg_nof_crosslets_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_sync_scheduler_xsub_clk_export       => OPEN,
      reg_bsn_sync_scheduler_xsub_reset_export     => OPEN,
      reg_bsn_sync_scheduler_xsub_address_export   => reg_bsn_sync_scheduler_xsub_copi.address(
          c_sdp_reg_bsn_sync_scheduler_xsub_addr_w - 1 downto 0),
      reg_bsn_sync_scheduler_xsub_write_export     => reg_bsn_sync_scheduler_xsub_copi.wr,
      reg_bsn_sync_scheduler_xsub_writedata_export => reg_bsn_sync_scheduler_xsub_copi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_sync_scheduler_xsub_read_export      => reg_bsn_sync_scheduler_xsub_copi.rd,
      reg_bsn_sync_scheduler_xsub_readdata_export  => reg_bsn_sync_scheduler_xsub_cipo.rddata(c_word_w - 1 downto 0),

      ram_st_xsq_clk_export       => OPEN,
      ram_st_xsq_reset_export     => OPEN,
      ram_st_xsq_address_export   => ram_st_xsq_copi.address(c_sdp_ram_st_xsq_arr_addr_w - 1 downto 0),
      ram_st_xsq_write_export     => ram_st_xsq_copi.wr,
      ram_st_xsq_writedata_export => ram_st_xsq_copi.wrdata(c_word_w - 1 downto 0),
      ram_st_xsq_read_export      => ram_st_xsq_copi.rd,
      ram_st_xsq_readdata_export  => ram_st_xsq_cipo.rddata(c_word_w - 1 downto 0),

      reg_nw_10GbE_mac_clk_export       => OPEN,
      reg_nw_10GbE_mac_reset_export     => OPEN,
      reg_nw_10GbE_mac_address_export   => reg_nw_10GbE_mac_copi.address(c_sdp_reg_nw_10GbE_mac_addr_w - 1 downto 0),
      reg_nw_10GbE_mac_write_export     => reg_nw_10GbE_mac_copi.wr,
      reg_nw_10GbE_mac_writedata_export => reg_nw_10GbE_mac_copi.wrdata(c_word_w - 1 downto 0),
      reg_nw_10GbE_mac_read_export      => reg_nw_10GbE_mac_copi.rd,
      reg_nw_10GbE_mac_readdata_export  => reg_nw_10GbE_mac_cipo.rddata(c_word_w - 1 downto 0),

      reg_nw_10GbE_eth10g_clk_export       => OPEN,
      reg_nw_10GbE_eth10g_reset_export     => OPEN,
      reg_nw_10GbE_eth10g_address_export   => reg_nw_10GbE_eth10g_copi.address(
          c_sdp_reg_nw_10GbE_eth10g_addr_w - 1 downto 0),
      reg_nw_10GbE_eth10g_write_export     => reg_nw_10GbE_eth10g_copi.wr,
      reg_nw_10GbE_eth10g_writedata_export => reg_nw_10GbE_eth10g_copi.wrdata(c_word_w - 1 downto 0),
      reg_nw_10GbE_eth10g_read_export      => reg_nw_10GbE_eth10g_copi.rd,
      reg_nw_10GbE_eth10g_readdata_export  => reg_nw_10GbE_eth10g_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_align_v2_bf_clk_export       => OPEN,
      reg_bsn_align_v2_bf_reset_export     => OPEN,
      reg_bsn_align_v2_bf_address_export   => reg_bsn_align_v2_bf_copi.address(
          c_sdp_reg_bsn_align_v2_bf_addr_w - 1 downto 0),
      reg_bsn_align_v2_bf_write_export     => reg_bsn_align_v2_bf_copi.wr,
      reg_bsn_align_v2_bf_writedata_export => reg_bsn_align_v2_bf_copi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_align_v2_bf_read_export      => reg_bsn_align_v2_bf_copi.rd,
      reg_bsn_align_v2_bf_readdata_export  => reg_bsn_align_v2_bf_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_rx_align_bf_clk_export       => OPEN,
      reg_bsn_monitor_v2_rx_align_bf_reset_export     => OPEN,
      reg_bsn_monitor_v2_rx_align_bf_address_export   => reg_bsn_monitor_v2_rx_align_bf_copi.address(
          c_sdp_reg_bsn_monitor_v2_rx_align_bf_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_rx_align_bf_write_export     => reg_bsn_monitor_v2_rx_align_bf_copi.wr,
      reg_bsn_monitor_v2_rx_align_bf_writedata_export => reg_bsn_monitor_v2_rx_align_bf_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_rx_align_bf_read_export      => reg_bsn_monitor_v2_rx_align_bf_copi.rd,
      reg_bsn_monitor_v2_rx_align_bf_readdata_export  => reg_bsn_monitor_v2_rx_align_bf_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_aligned_bf_clk_export       => OPEN,
      reg_bsn_monitor_v2_aligned_bf_reset_export     => OPEN,
      reg_bsn_monitor_v2_aligned_bf_address_export   => reg_bsn_monitor_v2_aligned_bf_copi.address(
          c_sdp_reg_bsn_monitor_v2_aligned_bf_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_aligned_bf_write_export     => reg_bsn_monitor_v2_aligned_bf_copi.wr,
      reg_bsn_monitor_v2_aligned_bf_writedata_export => reg_bsn_monitor_v2_aligned_bf_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_aligned_bf_read_export      => reg_bsn_monitor_v2_aligned_bf_copi.rd,
      reg_bsn_monitor_v2_aligned_bf_readdata_export  => reg_bsn_monitor_v2_aligned_bf_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_align_v2_xsub_clk_export       => OPEN,
      reg_bsn_align_v2_xsub_reset_export     => OPEN,
      reg_bsn_align_v2_xsub_address_export   => reg_bsn_align_v2_xsub_copi.address(
          c_sdp_reg_bsn_align_v2_xsub_addr_w - 1 downto 0),
      reg_bsn_align_v2_xsub_write_export     => reg_bsn_align_v2_xsub_copi.wr,
      reg_bsn_align_v2_xsub_writedata_export => reg_bsn_align_v2_xsub_copi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_align_v2_xsub_read_export      => reg_bsn_align_v2_xsub_copi.rd,
      reg_bsn_align_v2_xsub_readdata_export  => reg_bsn_align_v2_xsub_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_rx_align_xsub_clk_export       => OPEN,
      reg_bsn_monitor_v2_rx_align_xsub_reset_export     => OPEN,
      reg_bsn_monitor_v2_rx_align_xsub_address_export   => reg_bsn_monitor_v2_rx_align_xsub_copi.address(
          c_sdp_reg_bsn_monitor_v2_rx_align_xsub_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_rx_align_xsub_write_export     => reg_bsn_monitor_v2_rx_align_xsub_copi.wr,
      reg_bsn_monitor_v2_rx_align_xsub_writedata_export => reg_bsn_monitor_v2_rx_align_xsub_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_rx_align_xsub_read_export      => reg_bsn_monitor_v2_rx_align_xsub_copi.rd,
      reg_bsn_monitor_v2_rx_align_xsub_readdata_export  => reg_bsn_monitor_v2_rx_align_xsub_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_aligned_xsub_clk_export       => OPEN,
      reg_bsn_monitor_v2_aligned_xsub_reset_export     => OPEN,
      reg_bsn_monitor_v2_aligned_xsub_address_export   => reg_bsn_monitor_v2_aligned_xsub_copi.address(
          c_sdp_reg_bsn_monitor_v2_aligned_xsub_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_aligned_xsub_write_export     => reg_bsn_monitor_v2_aligned_xsub_copi.wr,
      reg_bsn_monitor_v2_aligned_xsub_writedata_export => reg_bsn_monitor_v2_aligned_xsub_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_aligned_xsub_read_export      => reg_bsn_monitor_v2_aligned_xsub_copi.rd,
      reg_bsn_monitor_v2_aligned_xsub_readdata_export  => reg_bsn_monitor_v2_aligned_xsub_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_sst_offload_clk_export       => OPEN,
      reg_bsn_monitor_v2_sst_offload_reset_export     => OPEN,
      reg_bsn_monitor_v2_sst_offload_address_export   => reg_bsn_monitor_v2_sst_offload_copi.address(
          c_sdp_reg_bsn_monitor_v2_sst_offload_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_sst_offload_write_export     => reg_bsn_monitor_v2_sst_offload_copi.wr,
      reg_bsn_monitor_v2_sst_offload_writedata_export => reg_bsn_monitor_v2_sst_offload_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_sst_offload_read_export      => reg_bsn_monitor_v2_sst_offload_copi.rd,
      reg_bsn_monitor_v2_sst_offload_readdata_export  => reg_bsn_monitor_v2_sst_offload_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_bst_offload_clk_export       => OPEN,
      reg_bsn_monitor_v2_bst_offload_reset_export     => OPEN,
      reg_bsn_monitor_v2_bst_offload_address_export   => reg_bsn_monitor_v2_bst_offload_copi.address(
          c_sdp_reg_bsn_monitor_v2_bst_offload_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_bst_offload_write_export     => reg_bsn_monitor_v2_bst_offload_copi.wr,
      reg_bsn_monitor_v2_bst_offload_writedata_export => reg_bsn_monitor_v2_bst_offload_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_bst_offload_read_export      => reg_bsn_monitor_v2_bst_offload_copi.rd,
      reg_bsn_monitor_v2_bst_offload_readdata_export  => reg_bsn_monitor_v2_bst_offload_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_beamlet_output_clk_export       => OPEN,
      reg_bsn_monitor_v2_beamlet_output_reset_export     => OPEN,
      reg_bsn_monitor_v2_beamlet_output_address_export   => reg_bsn_monitor_v2_beamlet_output_copi.address(
          c_sdp_reg_bsn_monitor_v2_beamlet_output_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_beamlet_output_write_export     => reg_bsn_monitor_v2_beamlet_output_copi.wr,
      reg_bsn_monitor_v2_beamlet_output_writedata_export => reg_bsn_monitor_v2_beamlet_output_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_beamlet_output_read_export      => reg_bsn_monitor_v2_beamlet_output_copi.rd,
      reg_bsn_monitor_v2_beamlet_output_readdata_export  => reg_bsn_monitor_v2_beamlet_output_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_xst_offload_clk_export       => OPEN,
      reg_bsn_monitor_v2_xst_offload_reset_export     => OPEN,
      reg_bsn_monitor_v2_xst_offload_address_export   => reg_bsn_monitor_v2_xst_offload_copi.address(
          c_sdp_reg_bsn_monitor_v2_xst_offload_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_xst_offload_write_export     => reg_bsn_monitor_v2_xst_offload_copi.wr,
      reg_bsn_monitor_v2_xst_offload_writedata_export => reg_bsn_monitor_v2_xst_offload_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_xst_offload_read_export      => reg_bsn_monitor_v2_xst_offload_copi.rd,
      reg_bsn_monitor_v2_xst_offload_readdata_export  => reg_bsn_monitor_v2_xst_offload_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_ring_lane_info_bf_clk_export       => OPEN,
      reg_ring_lane_info_bf_reset_export     => OPEN,
      reg_ring_lane_info_bf_address_export   => reg_ring_lane_info_bf_copi.address(
          c_sdp_reg_ring_lane_info_bf_addr_w - 1 downto 0),
      reg_ring_lane_info_bf_write_export     => reg_ring_lane_info_bf_copi.wr,
      reg_ring_lane_info_bf_writedata_export => reg_ring_lane_info_bf_copi.wrdata(c_word_w - 1 downto 0),
      reg_ring_lane_info_bf_read_export      => reg_ring_lane_info_bf_copi.rd,
      reg_ring_lane_info_bf_readdata_export  => reg_ring_lane_info_bf_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_ring_rx_bf_clk_export       => OPEN,
      reg_bsn_monitor_v2_ring_rx_bf_reset_export     => OPEN,
      reg_bsn_monitor_v2_ring_rx_bf_address_export   => reg_bsn_monitor_v2_ring_rx_bf_copi.address(
          c_sdp_reg_bsn_monitor_v2_ring_rx_bf_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_rx_bf_write_export     => reg_bsn_monitor_v2_ring_rx_bf_copi.wr,
      reg_bsn_monitor_v2_ring_rx_bf_writedata_export => reg_bsn_monitor_v2_ring_rx_bf_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_rx_bf_read_export      => reg_bsn_monitor_v2_ring_rx_bf_copi.rd,
      reg_bsn_monitor_v2_ring_rx_bf_readdata_export  => reg_bsn_monitor_v2_ring_rx_bf_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_ring_tx_bf_clk_export       => OPEN,
      reg_bsn_monitor_v2_ring_tx_bf_reset_export     => OPEN,
      reg_bsn_monitor_v2_ring_tx_bf_address_export   => reg_bsn_monitor_v2_ring_tx_bf_copi.address(
          c_sdp_reg_bsn_monitor_v2_ring_tx_bf_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_tx_bf_write_export     => reg_bsn_monitor_v2_ring_tx_bf_copi.wr,
      reg_bsn_monitor_v2_ring_tx_bf_writedata_export => reg_bsn_monitor_v2_ring_tx_bf_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_tx_bf_read_export      => reg_bsn_monitor_v2_ring_tx_bf_copi.rd,
      reg_bsn_monitor_v2_ring_tx_bf_readdata_export  => reg_bsn_monitor_v2_ring_tx_bf_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_dp_block_validate_err_bf_clk_export       => OPEN,
      reg_dp_block_validate_err_bf_reset_export     => OPEN,
      reg_dp_block_validate_err_bf_address_export   => reg_dp_block_validate_err_bf_copi.address(
          c_sdp_reg_dp_block_validate_err_bf_addr_w - 1 downto 0),
      reg_dp_block_validate_err_bf_write_export     => reg_dp_block_validate_err_bf_copi.wr,
      reg_dp_block_validate_err_bf_writedata_export => reg_dp_block_validate_err_bf_copi.wrdata(c_word_w - 1 downto 0),
      reg_dp_block_validate_err_bf_read_export      => reg_dp_block_validate_err_bf_copi.rd,
      reg_dp_block_validate_err_bf_readdata_export  => reg_dp_block_validate_err_bf_cipo.rddata(c_word_w - 1 downto 0),

      reg_dp_block_validate_bsn_at_sync_bf_clk_export       => OPEN,
      reg_dp_block_validate_bsn_at_sync_bf_reset_export     => OPEN,
      reg_dp_block_validate_bsn_at_sync_bf_address_export   => reg_dp_block_validate_bsn_at_sync_bf_copi.address(
          c_sdp_reg_dp_block_validate_bsn_at_sync_bf_addr_w - 1 downto 0),
      reg_dp_block_validate_bsn_at_sync_bf_write_export     => reg_dp_block_validate_bsn_at_sync_bf_copi.wr,
      reg_dp_block_validate_bsn_at_sync_bf_writedata_export => reg_dp_block_validate_bsn_at_sync_bf_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_dp_block_validate_bsn_at_sync_bf_read_export      => reg_dp_block_validate_bsn_at_sync_bf_copi.rd,
      reg_dp_block_validate_bsn_at_sync_bf_readdata_export  => reg_dp_block_validate_bsn_at_sync_bf_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_ring_lane_info_xst_clk_export       => OPEN,
      reg_ring_lane_info_xst_reset_export     => OPEN,
      reg_ring_lane_info_xst_address_export   => reg_ring_lane_info_xst_copi.address(
          c_sdp_reg_ring_lane_info_xst_addr_w - 1 downto 0),
      reg_ring_lane_info_xst_write_export     => reg_ring_lane_info_xst_copi.wr,
      reg_ring_lane_info_xst_writedata_export => reg_ring_lane_info_xst_copi.wrdata(c_word_w - 1 downto 0),
      reg_ring_lane_info_xst_read_export      => reg_ring_lane_info_xst_copi.rd,
      reg_ring_lane_info_xst_readdata_export  => reg_ring_lane_info_xst_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_ring_rx_xst_clk_export       => OPEN,
      reg_bsn_monitor_v2_ring_rx_xst_reset_export     => OPEN,
      reg_bsn_monitor_v2_ring_rx_xst_address_export   => reg_bsn_monitor_v2_ring_rx_xst_copi.address(
          c_sdp_reg_bsn_monitor_v2_ring_rx_xst_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_rx_xst_write_export     => reg_bsn_monitor_v2_ring_rx_xst_copi.wr,
      reg_bsn_monitor_v2_ring_rx_xst_writedata_export => reg_bsn_monitor_v2_ring_rx_xst_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_rx_xst_read_export      => reg_bsn_monitor_v2_ring_rx_xst_copi.rd,
      reg_bsn_monitor_v2_ring_rx_xst_readdata_export  => reg_bsn_monitor_v2_ring_rx_xst_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_ring_tx_xst_clk_export       => OPEN,
      reg_bsn_monitor_v2_ring_tx_xst_reset_export     => OPEN,
      reg_bsn_monitor_v2_ring_tx_xst_address_export   => reg_bsn_monitor_v2_ring_tx_xst_copi.address(
          c_sdp_reg_bsn_monitor_v2_ring_tx_xst_addr_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_tx_xst_write_export     => reg_bsn_monitor_v2_ring_tx_xst_copi.wr,
      reg_bsn_monitor_v2_ring_tx_xst_writedata_export => reg_bsn_monitor_v2_ring_tx_xst_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_ring_tx_xst_read_export      => reg_bsn_monitor_v2_ring_tx_xst_copi.rd,
      reg_bsn_monitor_v2_ring_tx_xst_readdata_export  => reg_bsn_monitor_v2_ring_tx_xst_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_dp_block_validate_err_xst_clk_export       => OPEN,
      reg_dp_block_validate_err_xst_reset_export     => OPEN,
      reg_dp_block_validate_err_xst_address_export   => reg_dp_block_validate_err_xst_copi.address(
          c_sdp_reg_dp_block_validate_err_xst_addr_w - 1 downto 0),
      reg_dp_block_validate_err_xst_write_export     => reg_dp_block_validate_err_xst_copi.wr,
      reg_dp_block_validate_err_xst_writedata_export => reg_dp_block_validate_err_xst_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_dp_block_validate_err_xst_read_export      => reg_dp_block_validate_err_xst_copi.rd,
      reg_dp_block_validate_err_xst_readdata_export  => reg_dp_block_validate_err_xst_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_dp_block_validate_bsn_at_sync_xst_clk_export       => OPEN,
      reg_dp_block_validate_bsn_at_sync_xst_reset_export     => OPEN,
      reg_dp_block_validate_bsn_at_sync_xst_address_export   => reg_dp_block_validate_bsn_at_sync_xst_copi.address(
          c_sdp_reg_dp_block_validate_bsn_at_sync_xst_addr_w - 1 downto 0),
      reg_dp_block_validate_bsn_at_sync_xst_write_export     => reg_dp_block_validate_bsn_at_sync_xst_copi.wr,
      reg_dp_block_validate_bsn_at_sync_xst_writedata_export => reg_dp_block_validate_bsn_at_sync_xst_copi.wrdata(
          c_word_w - 1 downto 0),
      reg_dp_block_validate_bsn_at_sync_xst_read_export      => reg_dp_block_validate_bsn_at_sync_xst_copi.rd,
      reg_dp_block_validate_bsn_at_sync_xst_readdata_export  => reg_dp_block_validate_bsn_at_sync_xst_cipo.rddata(
          c_word_w - 1 downto 0),

      reg_tr_10GbE_mac_clk_export       => OPEN,
      reg_tr_10GbE_mac_reset_export     => OPEN,
      reg_tr_10GbE_mac_address_export   => reg_tr_10GbE_mac_copi.address(c_sdp_reg_tr_10GbE_mac_addr_w - 1 downto 0),
      reg_tr_10GbE_mac_write_export     => reg_tr_10GbE_mac_copi.wr,
      reg_tr_10GbE_mac_writedata_export => reg_tr_10GbE_mac_copi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10GbE_mac_read_export      => reg_tr_10GbE_mac_copi.rd,
      reg_tr_10GbE_mac_readdata_export  => reg_tr_10GbE_mac_cipo.rddata(c_word_w - 1 downto 0),

      reg_tr_10GbE_eth10g_clk_export       => OPEN,
      reg_tr_10GbE_eth10g_reset_export     => OPEN,
      reg_tr_10GbE_eth10g_address_export   => reg_tr_10GbE_eth10g_copi.address(
          c_sdp_reg_tr_10GbE_eth10g_addr_w - 1 downto 0),
      reg_tr_10GbE_eth10g_write_export     => reg_tr_10GbE_eth10g_copi.wr,
      reg_tr_10GbE_eth10g_writedata_export => reg_tr_10GbE_eth10g_copi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10GbE_eth10g_read_export      => reg_tr_10GbE_eth10g_copi.rd,
      reg_tr_10GbE_eth10g_readdata_export  => reg_tr_10GbE_eth10g_cipo.rddata(c_word_w - 1 downto 0),

      ram_scrap_clk_export       => OPEN,
      ram_scrap_reset_export     => OPEN,
      ram_scrap_address_export   => ram_scrap_copi.address(9 - 1 downto 0),
      ram_scrap_write_export     => ram_scrap_copi.wr,
      ram_scrap_writedata_export => ram_scrap_copi.wrdata(c_word_w - 1 downto 0),
      ram_scrap_read_export      => ram_scrap_copi.rd,
      ram_scrap_readdata_export  => ram_scrap_cipo.rddata(c_word_w - 1 downto 0)
   );
  end generate;
end str;
