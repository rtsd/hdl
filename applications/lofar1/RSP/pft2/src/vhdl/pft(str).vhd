library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library pft2_lib;
use pft2_lib.pft_pkg.all;

architecture str of pft is
  function pft_dat_w ( output_w : in natural; mode : in PFT_MODE_TYPE) return natural is
    variable dat_w : natural;
  begin
    if mode = PFT_MODE_REAL2  then
      dat_w := output_w;
    else
      dat_w := output_w;
    end if;
    return dat_w;
  end;

  constant c_nof_stages      : natural := g_fft_size_w / 2;
  constant c_stage_dat_w     : natural := c_pft_stage_dat_w;
  constant c_pft_dat_w       : natural := pft_dat_w(g_out_dat_w, g_mode);

  type stage_rec is record
    re   : std_logic_vector(c_stage_dat_w - 1 downto 0);
    im   : std_logic_vector(c_stage_dat_w - 1 downto 0);
    val  : std_logic;
    sync : std_logic;
  end record;

  type stage_arr is array(c_nof_stages - 2 downto 0) of stage_rec;

  signal switch_re        : std_logic_vector(in_re'range);
  signal switch_im        : std_logic_vector(in_im'range);
  signal switch_val       : std_logic;
  signal switch_sync      : std_logic;

  signal stage            : stage_arr;

  signal pft_re           : std_logic_vector(c_pft_dat_w - 1 downto 0);
  signal pft_im           : std_logic_vector(c_pft_dat_w - 1 downto 0);
  signal pft_val          : std_logic;
  signal pft_sync         : std_logic;

  signal buf_re           : std_logic_vector(c_pft_dat_w - 1 downto 0);
  signal buf_im           : std_logic_vector(c_pft_dat_w - 1 downto 0);
  signal buf_en           : std_logic;
  signal buf_val          : std_logic;
  signal buf_adr          : std_logic_vector(g_fft_size_w - 1 downto 0);
  signal buf_sync         : std_logic;
  signal buf_rdy          : std_logic;

  signal sep_re           : std_logic_vector(out_re'range);
  signal sep_im           : std_logic_vector(out_im'range);
  signal sep_val          : std_logic;
  signal sep_sync         : std_logic;

  signal unswitch_re      : std_logic_vector(out_re'range);
  signal unswitch_im      : std_logic_vector(out_im'range);
  signal unswitch_val     : std_logic;
  signal unswitch_sync    : std_logic;

  -- synthesis translate_off
  signal bin       : std_logic_vector(g_fft_size_w - 1 downto 0) := (others => '0');
  signal band      : std_logic_vector(g_fft_size_w - 2 downto 0);
  signal fft_x_re  : std_logic_vector(out_re'range);
  signal fft_x_im  : std_logic_vector(out_im'range);
  signal fft_y_re  : std_logic_vector(out_re'range);
  signal fft_y_im  : std_logic_vector(out_im'range);
  signal power     : std_logic_vector(2 * g_out_dat_w - 1 downto 0);
  signal power_x   : std_logic_vector(2 * g_out_dat_w - 1 downto 0);
  signal power_y   : std_logic_vector(2 * g_out_dat_w - 1 downto 0);
  -- synthesis translate_on
begin
  -- The pipelined fft is composed of a number of consecutive stages.
  -- The output of each stage is used as input for the next stage.
  -- NB. The first stage has index c_nof_stages-1, the last stage has index 0.

  switch: entity pft2_lib.pft_switch
  generic map (
    g_dat_w    => g_in_dat_w,
    g_fft_sz_w => g_fft_size_w
  )
  port map (
    rst          => rst,
    clk          => clk,
    in_val       => in_val,
    in_sync      => in_sync,
    in_re        => in_re,
    in_im        => in_im,
    switch_en    => switch_en,
    out_re       => switch_re,
    out_im       => switch_im,
    out_val      => switch_val,
    out_sync     => switch_sync
  );

  first_gen : if (c_nof_stages > 1) generate
    first_stage : entity pft2_lib.pft_stage
      generic map (
        g_index          => c_nof_stages - 1,
        g_in_dat_w       => g_in_dat_w,
        g_out_dat_w      => c_stage_dat_w
      )
      port map (
        in_re            => switch_re,
        in_im            => switch_im,
        in_val           => switch_val,
        in_sync          => switch_sync,
        out_re           => stage(c_nof_stages - 2).re,
        out_im           => stage(c_nof_stages - 2).im,
        out_val          => stage(c_nof_stages - 2).val,
        out_sync         => stage(c_nof_stages - 2).sync,
        clk              => clk,
        rst              => rst
      );
  end generate;

  middle_gen : for i in c_nof_stages - 2 downto 1 generate
    middle_stage : entity pft2_lib.pft_stage
      generic map (
        g_index          => i,
        g_in_dat_w       => c_stage_dat_w,
        g_out_dat_w      => c_stage_dat_w
      )
      port map (
        in_re            => stage(i).re,
        in_im            => stage(i).im,
        in_val           => stage(i).val,
        in_sync          => stage(i).sync,
        out_re           => stage(i - 1).re,
        out_im           => stage(i - 1).im,
        out_val          => stage(i - 1).val,
        out_sync         => stage(i - 1).sync,
        clk              => clk,
        rst              => rst
      );
  end generate;

  last_gen : if c_nof_stages > 1 generate
    last_stage : entity pft2_lib.pft_stage
      generic map (
        g_index          => 0,
        g_in_dat_w       => c_stage_dat_w,
        g_out_dat_w      => c_pft_dat_w
      )
      port map (
        in_re            => stage(0).re,
        in_im            => stage(0).im,
        in_val           => stage(0).val,
        in_sync          => stage(0).sync,
        out_re           => pft_re,
        out_im           => pft_im,
        out_val          => pft_val,
        out_sync         => pft_sync,
        clk              => clk,
        rst              => rst
      );
  end generate;

  only_gen : if c_nof_stages = 1 generate
    only_stage : entity pft2_lib.pft_stage
        generic map (
          g_index          => 0,
          g_in_dat_w       => g_in_dat_w,
          g_out_dat_w      => c_pft_dat_w
        )
        port map (
          in_re            => in_re,
          in_im            => in_im,
          in_val           => in_val,
          in_sync          => in_sync,
          out_re           => pft_re,
          out_im           => pft_im,
          out_val          => pft_val,
          out_sync         => pft_sync,
          clk              => clk,
          rst              => rst
        );
  end generate;

  -- In "BITREV" mode, fft output is in bit reversed order.
  none_gen : if g_mode = PFT_MODE_BITREV generate
    sep_re   <= pft_re;
    sep_im   <= pft_im;
    sep_val  <= pft_val;
    sep_sync <= pft_sync;
  end generate;

  buf_gen : if g_mode /= PFT_MODE_BITREV generate
    buf : entity pft2_lib.pft_buffer
      generic map (
        g_fft_size_w      => g_fft_size_w,
        g_dat_w           => c_pft_dat_w
      )
      port map (
        wr_re             => pft_re,
        wr_im             => pft_im,
        wr_val            => pft_val,
        wr_sync           => pft_sync,
        rd_re             => buf_re,
        rd_im             => buf_im,
        rd_adr            => buf_adr,
        rd_en             => buf_en,
        rd_val            => buf_val,
        rd_sync           => buf_sync,
        rd_rdy            => buf_rdy,
        clk               => clk,
        rst               => rst
      );
  end generate;

   reverse_gen : if g_mode = PFT_MODE_COMPLEX generate
    reverse : entity pft2_lib.pft_reverse
      generic map (
        g_fft_sz          => 2**g_fft_size_w,
        g_fft_sz_w        => g_fft_size_w,
        g_data_w          => c_pft_dat_w
      )
      port map (
        rddata_re         => buf_re,
        rddata_im         => buf_im,
        rdaddr            => buf_adr,
        rden              => buf_en,
        rdval             => buf_val,
        rdsync            => buf_sync,
        page_rdy          => buf_rdy,
        out_dat_re        => sep_re,
        out_dat_im        => sep_im,
        out_val           => sep_val,
        out_sync          => sep_sync,
        clk               => clk,
        rst               => rst
      );
  end generate;

  separate_gen : if g_mode = PFT_MODE_REAL2 generate
    separate : entity pft2_lib.pft_separate
      generic map (
        g_fft_sz          => 2**g_fft_size_w,
        g_fft_sz_w        => g_fft_size_w,
        g_rd_dat_w        => c_pft_dat_w,
        g_out_dat_w       => g_out_dat_w
      )
      port map (
        rddata_re         => buf_re,
        rddata_im         => buf_im,
        rdaddr            => buf_adr,
        rden              => buf_en,
        rdval             => buf_val,
        rdsync            => buf_sync,
        page_rdy          => buf_rdy,
        out_dat_re        => sep_re,
        out_dat_im        => sep_im,
        out_val           => sep_val,
        out_sync          => sep_sync,
        clk               => clk,
        rst               => rst
      );
  end generate;

  unswitch: entity pft2_lib.pft_unswitch
  generic map (
    g_dat_w    => g_out_dat_w,
    g_fft_sz_w => g_fft_size_w
  )
  port map (
    rst          => rst,
    clk          => clk,
    in_val       => sep_val,
    in_sync      => sep_sync,
    in_re        => sep_re,
    in_im        => sep_im,
    switch_en    => switch_en,
    out_re       => unswitch_re,
    out_im       => unswitch_im,
    out_val      => unswitch_val,
    out_sync     => unswitch_sync
  );

  -- calculate the power. This is intended to be used in simulations only.

  -- synthesis translate_off
    determine_bin : process (clk)
    begin
      if rising_edge(clk) then
        if unswitch_val = '1' then
          bin <= std_logic_vector(unsigned(bin) + 1);
        end if;
      end if;
    end process;

    band  <= bin(bin'high downto 1);

    power <= std_logic_vector(   signed(unswitch_re) * signed(unswitch_re)
                               + signed(unswitch_im) * signed(unswitch_im)
                              ) when unswitch_val = '1' else (others => '0');

    -- Wave window: View fft_re, fft_im in analogue format
    -- Wave window: View power in binary format to get a spectrum diagram

    -- power_x <= power WHEN bin(0) = '0' ELSE power_x;
    -- power_y <= power WHEN bin(0) = '1' ELSE power_y;

    -- Use clk to avoid limit cycle pulses in power_x and power_y
    demux_power : process(clk)
    begin
      if falling_edge(clk) then
        if unswitch_val = '1' then
          if bin(0) = '0' then
            fft_x_re <= unswitch_re;
            fft_x_im <= unswitch_im;
            power_x  <= power;
          else
            fft_y_re <= unswitch_re;
            fft_y_im <= unswitch_im;
            power_y  <= power;
          end if;
        end if;
      end if;
    end process;
  -- synthesis translate_on

  out_re   <= unswitch_re;
  out_im   <= unswitch_im;
  out_val  <= unswitch_val;
  out_sync <= unswitch_sync;
end str;
