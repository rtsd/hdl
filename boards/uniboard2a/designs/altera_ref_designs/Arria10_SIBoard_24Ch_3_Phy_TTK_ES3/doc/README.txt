10G Transceiver ToolKit (Arria10_SIBoard_24Ch_3_Phy_TTK_ES3)
  an Altera reference design originally downloaded from
  www.alterawiki.com/wiki/High_Speed_Transceiver_Demo_Designs_For_Current_and_Older_Families


This Altera reference design has been modified for Uniboard^2.
The following 5 revisions have been made to test the 10G links group-by-group:

  ./Arria10_SIBoard_24Ch_3_Phy_TTK_ES3_15_1_1_Uniboard12.qar
  ./Arria10_SIBoard_24Ch_3_Phy_TTK_ES3_15_1_1_Uniboard-24qsfp-48bck-24ring.qar
  ./Arria10_SIBoard_24Ch_3_Phy_TTK_ES3_15_1_1_Uniboard-24qsfp-24bck.qar
  ./Arria10_SIBoard_24Ch_3_Phy_TTK_ES3_15_1_1_Uniboard-24ring.qar
  ./Arria10_SIBoard_24Ch_3_Phy_TTK_ES3_15_1_1_Uniboard-24qsfp-48bck.qar

Here the 5 projects are archived in the Altera Archive file format (.QAR).
In Quartus these projects can be extracted and recompiled.
After programming the compiled .SOF file in the Uniboard^2 FPGA(s) use the
10G Transceiver Toolkit in Quartus->Tools->System Debugging Tools->Transceiver Toolkit

