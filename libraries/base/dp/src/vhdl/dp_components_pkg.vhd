-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Keep parameters of DP components
-- Description:

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

package dp_components_pkg is
  constant c_dp_clk_MHz                               : natural := 200;
  constant c_dp_sync_timeout                          : natural := c_dp_clk_MHz * 10**6 + c_dp_clk_MHz * 10**5;  -- 10% margin for nominal 1 s

  constant c_dp_bsn_monitor_v2_reg_adr_w              : natural := ceil_log2(7);  -- = 3
  constant c_dp_bsn_monitor_v2_reg_adr_span           : natural := 2**c_dp_bsn_monitor_v2_reg_adr_w;  -- = 8

  constant c_dp_strobe_total_count_reg_nof_words      : natural := 15 * 2 + 2;  -- = 32
  constant c_dp_strobe_total_count_reg_adr_w          : natural := ceil_log2(c_dp_strobe_total_count_reg_nof_words);  -- = 5
  constant c_dp_strobe_total_count_reg_adr_span       : natural := 2**c_dp_strobe_total_count_reg_adr_w;  -- = 32
  constant c_dp_strobe_total_count_reg_nof_counts_max : natural := 2**c_dp_strobe_total_count_reg_adr_w / 2 - 1;  -- = 15
  constant c_dp_strobe_total_count_reg_clear_adr      : natural := c_dp_strobe_total_count_reg_nof_counts_max * 2;  -- after counters in REGMAP
  constant c_dp_strobe_total_count_reg_flush_adr      : natural := c_dp_strobe_total_count_reg_nof_counts_max * 2 + 1;
end dp_components_pkg;

package body dp_components_pkg is
end dp_components_pkg;
