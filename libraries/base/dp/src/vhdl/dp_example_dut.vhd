--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
--   The dp_example_dut serves as a template for a streaming component with
--   flow control.
--
--
-- Block diagram:
--
--   A) Streaming interface
--            ____________________
--           |  dp_example_dut    |
--           |                    |
--       <---|snk_out      src_in |<---
--       --->|snk_in      src_out |--->
--           |____________________|
--
--
--   B) Flow control
--
--      RL=1                        RL=1               RL=1
--        .                           .                  .
--        .        /-----------------------------------------\
--        .        |                  .           _____  .   |
--        .        |   /------\     nxt_r        |     | r   |
--        .        \-->|      |---*-------*----->|p_reg|-----*---> src_out
--        .            |      |   |       |      |_____|
--     snk_in -------->|p_func|<--|-------|--------------*-------- src_in
--                     |      |   |       |              |
--                     |      |   |       v              |
--                     |      |   |   /-------\          |
--                     |      |   |   |p_flow |          |
--                     \------/   |   \-------/          |
--                                |       |              |
--           nxt_r.hold_out.valid |       |              |
--                                v       |              |
--                                /|      |r_snk_out     |
--                               |0|------/              |
--    snk_out <------------------| |                     |
--                               |1|---------------------/
--                                \|
--
-- Description:
--   The example DUT provides a VHDL template for implementing a streaming
--   function with snk_in.valid and snk_out / src_in flow control. The
--   VHDL template uses the Gaisler two-process coding style. this style
--   has a combinatorial process that describes the function and a clocked
--   process that contains all registers. The register nicely fits the
--   requirement that the streaming component should introduce 1 pipeline
--   latency. In this way it becomes easier to maintain the ready latency
--   of RL=1 from input snk to output src.
--   The example function in the dp_example_dut is a streaming pipeline
--   stage. Hence the dp_example_dut.vhd is equivalent to dp_pipeline.vhd.
--
-- . p_func = functionality:
--   The functionality of the DP component is defined in the p_func process.
--   The p_func process is purely cominatorial, ie it has no clock input.
--   The rest of the block diagram is needed to fit support input valid and
--   output ready flow control and to pipeline the processing such that
--   the function can be implementend within 1 clock cycle.
--
-- . p_reg = local storage and pipelining:
--   The storage is done in a register r and this r contains local storage
--   and the src_out. The p_reg contains all registers, so it is the only
--   process that has a clock input. The r.src_out can directly be wired to
--   src_out. The latency from snk_in to src_out must be 1 clock cycle. If
--   more pipelining is needed then the functionality should be split over
--   separate components that can each again have a latency of 1.
--
-- . p_flow = local flow control:
--   If the functionality does not add or remove valid data from the snk_in
--   input stream, then the src_in can be passed on directly to snk_out.
--   However if the functionality needs to apply backpressure itself or if it
--   can accept input data when the src_in is not ready then it can implement
--   p_flow to apply backpressure via r_snk_out.
--
-- . nxt_r:
--   Note that the function yields nxt_r that gets pipeline stored in r. The
--   nxt_r is combinatorially related to snk_in so it does not change the RL.
--   The p_flow also works combinatorially because it uses nxt_r and via
--   p_func also src_in. Hence in total the flow control logic does not change
--   the RL because snk_out relaies combinatorially on snk_in.valid, nxt_r
--   and src_in.ready.
--
-- . nxt_r.hold_out.valid = combine local and remote flow control:
--   When the functionality in p_func is about to yield new valid output then
--   nxt_r.hold_out.valid='1' and then the remote src-in is passed on to
--   snk_out. If p_func is not yet about to yield new valid output then
--   nxt_r.hold_out.valid='0' and then the local r_snk_out is passed on to
--   snk_out.
--
-- . src_out sosi control fields
--   The src_out sosi control fields (sync, sop, eop, valid) can only become
--   active when the src_in.ready='1'. If the function has new valid output
--   and src_in.ready is '0' then the function has to hold the src_out sosi
--   control in r.hold_out.
--
-- . src_out sosi data fields
--   The src_out sosi data fields (, data, re,  im, bsn, channel, empty, err)
--   can immediately be registered into r.src_out, because they are only
--   accepted when the corresponding sosi control field (valid, sop, eop) has
--   become active. Therefore the sosi dat fields are not kept in r.hold_out.
--   Typically a function that does not use all sosi fields should still pass
--   them on if that is not inappropriate.
--
-- Design steps:
-- . First determine how to implement the function it self and setup the basic
--   structure in VHDL, eg dtermine what fields in r will be needed.
-- . The initial structure for the function should already be more or less
--   correct such that the default behaviour or simplest behaviour of the
--   function can already be verified with the test bench. Hence the testbench
--   and the component are developed simultaneously.
-- . Next the more detailed functionality, special cases or extra features of
--   the function can be tested one at a time, again starting with the easiest
--   items, until the whole functinality has been covered. This step by step
--   approach makes that the bugs appear one by one instead of all together.
--   Without a step by step approach the bugs are too big to solve.
-- . It is important to keep the function implementation as clean as possible.
--   This can be achieved by only manipulating what is needed and using
--   default options where possible. In practise this implies eg avoiding
--   nested if-then-else statements, don't force assign a value if holding the
--   current value is fine too, etc.
-- . It is important to also verify the DUT for the corner cases of the
--   stimuli. For example block sizeof 1, 2, more, and max. If a certain value
--   is use then verify also what happens if <, = or >. If a design works for
--   the corner cases then this ensures that it works for all values. If a
--   design works for the typical cases then that is no garantuee that it
--   will workfor all situations.
-- . The development steps can becovered in a single testbench or they may also
--   be covered by representing them as tb instances in a multi tb testbench.
--   During the development the multi tb ensures that bug fixes do not
--   compromise functionality that did already verify ok. When the component
--   is finished, then the multi tb forms the regression test for the component
--   and should be added to the regression tb of the library.
-- . Typically the external flow control by src_in and snk_in is verified as
--   after the component has verified ok for e_active. Often it is sufficient
--   to only verify e_random for both snk_in and src_in, but sometimes it can
--   also be useful to try e_pulse and combinations of eg e_active for snk_in
--   and e_random for snk_out.
--
-- Remarks:
-- . dp_repack_data provides two usage examples of this dp_example_dut and of
--   the dp_stream_stimuli and dp_stream_verify components that are used in
--   the test bench.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity dp_example_dut is
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    snk_out          : out t_dp_siso;
    snk_in           : in  t_dp_sosi;

    src_in           : in  t_dp_siso;
    src_out          : out t_dp_sosi
  );
end dp_example_dut;

architecture rtl of dp_example_dut is
  type t_reg is record
    src_out       : t_dp_sosi;  -- sosi output
    hold_out      : t_dp_sosi;  -- hold snk_in.sync/sop/eop until end of section and then hold valid src_out until src_in.ready
  end record;

  signal r_snk_out  : t_dp_siso := c_dp_siso_rdy;
  signal r          : t_reg;
  signal nxt_r      : t_reg;
begin
  p_func : process(rst, r, snk_in, src_in)
    variable v : t_reg;
  begin
    ------------------------------------------------------------------------
    -- Default
    v := r;
    v.src_out.sync  := '0';
    v.src_out.valid := '0';
    v.src_out.sop   := '0';
    v.src_out.eop   := '0';

    --------------------------------------------------------------------------
    -- Function
    if r.hold_out.valid = '0' then

      -- Clear hold_out for new output valid
      if r.src_out.sop = '1' then
        v.hold_out.sync := '0';
        v.hold_out.sop  := '0';
      end if;
      if r.src_out.eop = '1' then
        v.hold_out.eop := '0';
      end if;

      -- Capture the snk_in block info that is valid at sop and eop
      if snk_in.sop = '1' then
        v.hold_out.sop     := '1';
        v.hold_out.sync    := snk_in.sync;
        v.src_out.bsn      := snk_in.bsn;
        v.src_out.channel  := snk_in.channel;
      end if;
      if snk_in.eop = '1' then
        v.hold_out.eop    := '1';
        v.src_out.empty   := snk_in.empty;
        v.src_out.err     := snk_in.err;
      end if;

      if snk_in.valid = '1' then
        --< start of component function >

        v.src_out.data := snk_in.data;  -- pipeline function as example

        v.hold_out.valid := '1';  -- the function has new data to output

        --< end of component function >
      end if;

      -- output input stage into output stage when ready, else hold_out.valid to signal pending output
      if v.hold_out.valid = '1' then
        if src_in.ready = '1' then
          v.src_out.valid := '1';
          v.src_out.sync  := v.hold_out.sync;
          v.src_out.sop   := v.hold_out.sop;
          v.src_out.eop   := v.hold_out.eop;
          v.hold_out.valid := '0';
        end if;
      end if;
    else
      -- pending output waiting for src_in (could use v, but using r is equivalent and may ease timing closure)
      if src_in.ready = '1' then
        v.src_out.valid := '1';
        v.src_out.sync  := r.hold_out.sync;
        v.src_out.sop   := r.hold_out.sop;
        v.src_out.eop   := r.hold_out.eop;
        v.hold_out.valid := '0';
      end if;
    end if;

    ------------------------------------------------------------------------
    -- Reset and nxt_r
    if rst = '1' then
      v.src_out       := c_dp_sosi_rst;
      v.hold_out      := c_dp_sosi_rst;
    end if;

    nxt_r <= v;
  end process;

  --------------------------------------------------------------------------
  -- p_reg
  r <= nxt_r when rising_edge(clk);

  --------------------------------------------------------------------------
  -- Wired output
  src_out <= r.src_out;

  --------------------------------------------------------------------------
  -- Flow control

  -- local function flow control
  p_flow : process(nxt_r)
  begin
    r_snk_out <= c_dp_siso_rdy;

    -- < force r_snk_out.ready='0' based on nxt_r if function needs to apply backpressure >
  end process;

  -- combined local and remote src_in flow control
  snk_out.ready <= r_snk_out.ready when nxt_r.hold_out.valid = '0' else src_in.ready;  -- if there is pending output then the src_in ready determines the flow control
  snk_out.xon   <= src_in.xon;  -- just pass on the xon/off frame flow control
end rtl;
