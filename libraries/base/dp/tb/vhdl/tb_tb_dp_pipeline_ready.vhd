-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

-- > as 2
-- > run -all --> OK

entity tb_tb_dp_pipeline_ready is
end tb_tb_dp_pipeline_ready;

architecture tb of tb_tb_dp_pipeline_ready is
  constant c_nof_repeat : natural := 50;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --                                                               in_en,    src_in.ready, in_latency, out_latency, nof repeat,
  -- Random flow control for different RL
  u_rnd_rnd_0_0    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     0,          0,           c_nof_repeat);
  u_rnd_rnd_1_0    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     1,          0,           c_nof_repeat);
  u_rnd_rnd_0_1    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     0,          1,           c_nof_repeat);
  u_rnd_rnd_2_0    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     2,          0,           c_nof_repeat);
  u_rnd_rnd_0_2    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     0,          2,           c_nof_repeat);
  u_rnd_rnd_2_1    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     2,          1,           c_nof_repeat);
  u_rnd_rnd_1_2    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     1,          2,           c_nof_repeat);
  u_rnd_rnd_2_2    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     2,          2,           c_nof_repeat);

  -- Other flow control for fixed RL
  u_act_act_1_1    : entity work.tb_dp_pipeline_ready generic map (e_active, e_active,     1,          1,           c_nof_repeat);
  u_act_rnd_1_1    : entity work.tb_dp_pipeline_ready generic map (e_active, e_random,     1,          1,           c_nof_repeat);
  u_act_pls_1_1    : entity work.tb_dp_pipeline_ready generic map (e_active, e_pulse,      1,          1,           c_nof_repeat);

  u_rnd_act_1_1    : entity work.tb_dp_pipeline_ready generic map (e_random, e_active,     1,          1,           c_nof_repeat);
  u_rnd_rnd_1_1    : entity work.tb_dp_pipeline_ready generic map (e_random, e_random,     1,          1,           c_nof_repeat);
  u_rnd_pls_1_1    : entity work.tb_dp_pipeline_ready generic map (e_random, e_pulse,      1,          1,           c_nof_repeat);

  u_pls_act_1_1    : entity work.tb_dp_pipeline_ready generic map (e_pulse,  e_active,     1,          1,           c_nof_repeat);
  u_pls_rnd_1_1    : entity work.tb_dp_pipeline_ready generic map (e_pulse,  e_random,     1,          1,           c_nof_repeat);
  u_pls_pls_1_1    : entity work.tb_dp_pipeline_ready generic map (e_pulse,  e_pulse,      1,          1,           c_nof_repeat);
end tb;
