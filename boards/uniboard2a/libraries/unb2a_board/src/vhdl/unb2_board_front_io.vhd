------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.unb2_board_pkg.all;

entity unb2_board_front_io is
  generic (
    g_nof_qsfp_bus : natural := c_unb2_board_tr_qsfp.nof_bus
  );
  port (
    serial_tx_arr  : in  std_logic_vector(g_nof_qsfp_bus * c_unb2_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    serial_rx_arr  : out std_logic_vector(g_nof_qsfp_bus * c_unb2_board_tr_qsfp.bus_w - 1 downto 0);

    green_led_arr  : in  std_logic_vector(g_nof_qsfp_bus - 1 downto 0) := (others => '0');
    red_led_arr    : in  std_logic_vector(g_nof_qsfp_bus - 1 downto 0) := (others => '0');

    QSFP_RX        : in  t_unb2_board_qsfp_bus_2arr(g_nof_qsfp_bus - 1 downto 0) := (others => (others => '0'));
    QSFP_TX        : out t_unb2_board_qsfp_bus_2arr(g_nof_qsfp_bus - 1 downto 0);

    --QSFP_SDA       : INOUT STD_LOGIC_VECTOR(c_unb2_board_tr_qsfp.i2c_w-1 downto 0);
    --QSFP_SCL       : INOUT STD_LOGIC_VECTOR(c_unb2_board_tr_qsfp.i2c_w-1 downto 0);
    --QSFP_RST       : INOUT STD_LOGIC;

    QSFP_LED       : out   std_logic_vector(c_unb2_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end unb2_board_front_io;

architecture str of unb2_board_front_io is
  -- help signals so we can iterate through buses
  signal si_tx_2arr : t_unb2_board_qsfp_bus_2arr(g_nof_qsfp_bus - 1 downto 0);
  signal si_rx_2arr : t_unb2_board_qsfp_bus_2arr(g_nof_qsfp_bus - 1 downto 0);
begin
  gen_leds : for i in 0 to g_nof_qsfp_bus - 1 generate
    QSFP_LED(i * 2)   <=  green_led_arr(i);
    QSFP_LED(i * 2 + 1) <=  red_led_arr(i);
  end generate;

  gen_buses : for i in 0 to g_nof_qsfp_bus - 1 generate
    QSFP_TX(i)    <= si_tx_2arr(i);
    si_rx_2arr(i) <= QSFP_RX(i);
  end generate;

  gen_wire_bus : for i in 0 to g_nof_qsfp_bus - 1 generate
    gen_wire_signals : for j in 0 to c_unb2_board_tr_qsfp.bus_w - 1 generate
        si_tx_2arr(i)(j) <= serial_tx_arr(i * c_unb2_board_tr_qsfp.bus_w + j);
        serial_rx_arr(i * c_unb2_board_tr_qsfp.bus_w + j) <= si_rx_2arr(i)(j);
    end generate;
  end generate;
end;
