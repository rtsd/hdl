-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: The bf_unit implements a beamformer algorithm. The algorithm takes a
--          subband sample of all antenna inputs and then calculates all the beamlets
--          for these subband samples by feeding the multiplier with the according weight
--          factors. A set of ss_wide instantiations is used for data distribution over
--          the multipliers and is used to retreive the same subband sample multiple
--          times.
--
--          The bf_unit connects the memory with the weightfactors and the output of the
--          ss_wide selection unit to the complex multiplier. The output of the multiplier
--          is fed to a an adder tree for accumulation. After accumulation the data is
--          passed on to two quantizers in parallel. First quantizer is used to shape
--          the data for the beamlet statistics unit. Second quantizer is used to shape
--          the data for the "normal" output that can be passed on to for instance a correlator.
--
--          The weight-memories can be pre-initialized for simulation using .hex files. The
--          naming convention for these files is:
--
--              weights_x_y.hex
--
--              where "weights" is the generic g_bf_weights_file_name
--                    "x" is the bf_unit number
--                    "y" is the signal path numner.
--
--          RAM init files (.hex files) only work when g_weights_write_only is set to FALSE.
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library common_lib, common_mult_lib, technology_lib, dp_lib, st_lib, reorder_lib;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.bf_pkg.all;

entity bf_unit is
  generic (
    g_technology            : natural := c_tech_select_default;
    g_bf                    : t_c_bf  := c_bf;
    g_bf_weights_file_name  : string  := "UNUSED";  -- "UNUSED" or relative path to e.g. the bf/build/data/weights hex file for adr_w=8 and dat_w=32
    g_ss_wide_file_prefix   : string  := "UNUSED";  -- path_to_file
    g_weights_write_only    : boolean := false  -- When set to TRUE the M9K blocks are forced to Simple Dual Port mode. When FALSE it is True Dual Port.
  );
  port (
    -- System
    dp_rst                  : in  std_logic := '0';
    dp_clk                  : in  std_logic;
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;

    -- MM interface
    ram_ss_ss_wide_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    ram_ss_ss_wide_miso     : out t_mem_miso := c_mem_miso_rst;
    ram_bf_weights_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    ram_bf_weights_miso     : out t_mem_miso := c_mem_miso_rst;
    ram_st_sst_mosi         : in  t_mem_mosi := c_mem_mosi_rst;  -- Power statistics memory
    ram_st_sst_miso         : out t_mem_miso := c_mem_miso_rst;
    reg_st_sst_mosi         : in  t_mem_mosi := c_mem_mosi_rst;  -- Power statistics register
    reg_st_sst_miso         : out t_mem_miso := c_mem_miso_rst;

    -- ST interface
    in_sosi_arr             : in  t_dp_sosi_arr(g_bf.nof_input_streams - 1 downto 0);  -- subbands; one or more signal paths per datapath input stream
    in_siso_arr             : out t_dp_siso_arr(g_bf.nof_input_streams - 1 downto 0);
    out_raw_sosi            : out t_dp_sosi;  -- original    raw beamlets output with c_sum_w bits.
    out_bst_sosi            : out t_dp_sosi;  -- requantized 16b beamlets output that is also used for internal BST.
    out_qua_sosi            : out t_dp_sosi  -- requantized  8b beamlets output.
  );
end bf_unit;

architecture str of bf_unit is
  -- Operational mode:
  constant c_unit_w                      : positive := g_bf.in_dat_w + g_bf.in_weight_w - c_sign_w;  -- skip double sign bit
  constant c_prod_w                      : positive := c_unit_w + c_sum_of_prod_w;  -- keep bit for sum of products in complex multiply
  constant c_gain_w                      : integer  := largest(g_bf.bst_gain_w, g_bf.out_gain_w);  -- keep internal c_sum_w as wide as necessary to fit both BST and qua output
                                                                                                   -- no need to account for adder bit growth of ceil_log2(g_bf.nof_signal_paths),
                                                                                                   -- because default BF sum should not clip to allow next stage of BF
  constant c_sum_w                       : positive := c_unit_w + c_gain_w;  -- note use c_gain_w >= 1 if complex sum of products bit growth has to be preserved
  constant c_bst_lsb_w                   : natural  := c_unit_w + g_bf.bst_gain_w - g_bf.bst_dat_w;
  constant c_out_lsb_w                   : natural  := c_unit_w + g_bf.out_gain_w - g_bf.out_dat_w;

  constant c_conjugate                   : boolean  := false;
  constant c_nof_signal_paths_per_stream : positive := g_bf.nof_signal_paths / g_bf.nof_input_streams;
  constant c_nof_subbands_per_stream     : positive := c_nof_signal_paths_per_stream * g_bf.nof_subbands;
  constant c_xst_enable                  : boolean  := true;

  constant c_weights_buf : t_c_mem := (latency  => 1,
                                       adr_w    => ceil_log2(g_bf.nof_weights),
                                       dat_w    => c_nof_complex * g_bf.in_weight_w,
                                       nof_dat  => g_bf.nof_weights,
                                       init_sl  => '0');
  -- Latencies
  constant c_input_latency         : natural := 1;  -- due to r
  constant c_prod_latency          : natural := 3;
  constant c_adder_stage_latency   : natural := 1;
  constant c_nof_adder_stages      : natural := ceil_log2(g_bf.nof_signal_paths);
  constant c_adder_tree_latency    : natural := c_nof_adder_stages * c_adder_stage_latency;
  constant c_bf_prod_latency       : natural := c_input_latency + c_prod_latency;
  constant c_bf_sum_latency        : natural := c_bf_prod_latency + c_adder_tree_latency;
  constant c_bf_unit_latency       : natural := c_bf_sum_latency;

  type t_slv_data_in_arr    is array (integer range <>) of std_logic_vector(g_bf.in_dat_w - 1 downto 0);
  type t_slv_weight_in_arr  is array (integer range <>) of std_logic_vector(g_bf.in_weight_w - 1 downto 0);
  type t_slv_prod_arr       is array (integer range <>) of std_logic_vector(c_prod_w - 1 downto 0);

  type reg_type is record
    bf_in_sosi_arr : t_dp_sosi_arr( g_bf.nof_signal_paths - 1 downto 0);
  end record;

  -- Debug signals to view constants in Wave Window
  signal dbg_g_bf            : t_c_bf := g_bf;
  signal dbg_c_unit_w        : positive := c_unit_w;
  signal dbg_c_prod_w        : positive := c_prod_w;
  signal dbg_c_gain_w        : integer  := c_gain_w;
  signal dbg_c_sum_w         : positive := c_sum_w;
  signal dbg_c_bst_lsb_w     : natural  := c_bst_lsb_w;
  signal dbg_c_out_lsb_w     : natural  := c_out_lsb_w;

  signal mult_miso_arr       : t_mem_miso_arr(g_bf.nof_signal_paths - 1 downto 0) := (others => c_mem_miso_rst);  -- MM interfaces between weights-memory and complex multipliers.

  signal r, rin              : reg_type;

  signal ss_wide_in_sosi_arr : t_dp_sosi_arr( g_bf.nof_signal_paths - 1 downto 0);  -- for each signal path a datapath stream interface.
  signal ss_wide_in_siso_arr : t_dp_siso_arr( g_bf.nof_signal_paths - 1 downto 0);  -- for each signal path a datapath stream interface.
  signal bf_in_sosi_arr      : t_dp_sosi_arr( g_bf.nof_signal_paths - 1 downto 0);  -- for each signal path a datapath stream interface.

  signal ram_ss_ss_wide_mosi_arr : t_mem_mosi_arr(g_bf.nof_input_streams - 1 downto 0);
  signal ram_ss_ss_wide_miso_arr : t_mem_miso_arr(g_bf.nof_input_streams - 1 downto 0);

  signal mm_bf_weights_miso  : t_mem_miso := c_mem_miso_rst;
  signal mm_weight_mosi_arr  : t_mem_mosi_arr(g_bf.nof_signal_paths - 1 downto 0);  -- for each input a mm interface for writing the weight factors
  signal mm_weight_miso_arr  : t_mem_miso_arr(g_bf.nof_signal_paths - 1 downto 0) := (others => c_mem_miso_rst);  -- for each input a mm interface for writing the weight factors

  signal data_re_arr         : t_slv_data_in_arr(   g_bf.nof_signal_paths - 1 downto 0);
  signal data_im_arr         : t_slv_data_in_arr(   g_bf.nof_signal_paths - 1 downto 0);

  signal weight_addr         : std_logic_vector(c_weights_buf.adr_w - 1 downto 0);  -- Address for the weight factors memory
  signal weight_re_arr       : t_slv_weight_in_arr(g_bf.nof_signal_paths - 1 downto 0);
  signal weight_im_arr       : t_slv_weight_in_arr(g_bf.nof_signal_paths - 1 downto 0);

  signal prod_re_arr         : t_slv_prod_arr(           g_bf.nof_signal_paths - 1 downto 0);
  signal prod_im_arr         : t_slv_prod_arr(           g_bf.nof_signal_paths - 1 downto 0);
  signal prod_re_vec         : std_logic_vector(c_prod_w * g_bf.nof_signal_paths - 1 downto 0);
  signal prod_im_vec         : std_logic_vector(c_prod_w * g_bf.nof_signal_paths - 1 downto 0);

  signal sum_re              : std_logic_vector(c_sum_w - 1 downto 0);
  signal sum_im              : std_logic_vector(c_sum_w - 1 downto 0);

  signal piped_sosi          : t_dp_sosi;
  signal beams_raw_sosi      : t_dp_sosi;
  signal beams_bst_sosi      : t_dp_sosi;

  signal dbg_weights_write_only : boolean := g_weights_write_only;
  signal wr_last_weight         : std_logic_vector(c_word_w - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Input registers
  ------------------------------------------------------------------------------
  comb : process(r, bf_in_sosi_arr)
    variable v : reg_type;
  begin
    v                := r;
    v.bf_in_sosi_arr := bf_in_sosi_arr;
    rin              <= v;
  end process comb;

  regs : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      r <= rin;
    end if;
  end process;

  u_mem_mux_ss_wide : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_bf.nof_input_streams,
    g_mult_addr_w => ceil_log2(g_bf.nof_weights * c_nof_signal_paths_per_stream)
  )
  port map (
    mosi     => ram_ss_ss_wide_mosi,
    miso     => ram_ss_ss_wide_miso,
    mosi_arr => ram_ss_ss_wide_mosi_arr,
    miso_arr => ram_ss_ss_wide_miso_arr
  );

  ------------------------------------------------------------------------------
  -- The beamformer unit
  ------------------------------------------------------------------------------
  -- A set of ss_wide units is used to distribute the incoming subbands to the data-inputs of the
  -- beamformer multipliers.
  gen_ss_wide : for I in 0 to g_bf.nof_input_streams - 1 generate
    gen_copy_input : for J in 0 to c_nof_signal_paths_per_stream - 1 generate
      ss_wide_in_sosi_arr(I * c_nof_signal_paths_per_stream + J) <= in_sosi_arr(I);
    end generate;

    in_siso_arr(I) <= ss_wide_in_siso_arr(I * c_nof_signal_paths_per_stream);

    u_ss_wide : entity reorder_lib.reorder_col_wide
    generic map (
      g_wb_factor          => c_nof_signal_paths_per_stream,
      g_dsp_data_w         => g_bf.in_dat_w,
      g_nof_ch_in          => c_nof_subbands_per_stream,
      g_nof_ch_sel         => g_bf.nof_weights,
      g_select_file_prefix => g_ss_wide_file_prefix
    )
    port map (
      mm_rst               => mm_rst,
      mm_clk               => mm_clk,
      dp_rst               => dp_rst,
      dp_clk               => dp_clk,

      -- Memory Mapped
      ram_ss_ss_wide_mosi  => ram_ss_ss_wide_mosi_arr(I),
      ram_ss_ss_wide_miso  => ram_ss_ss_wide_miso_arr(I),

      -- Streaming
      input_sosi_arr       => ss_wide_in_sosi_arr((I + 1) * c_nof_signal_paths_per_stream - 1 downto I * c_nof_signal_paths_per_stream),
      input_siso_arr       => ss_wide_in_siso_arr((I + 1) * c_nof_signal_paths_per_stream - 1 downto I * c_nof_signal_paths_per_stream),
      output_sosi_arr      => bf_in_sosi_arr((I + 1) * c_nof_signal_paths_per_stream - 1 downto I * c_nof_signal_paths_per_stream)
    );
  end generate;

  -- Support readback of the last BF weight write to have some minimal readback functionality
  -- . verified with tb_bf_unit and with tb_apertif_unb1_fn_beamformer_trans
  gen_read_last_weight : if g_weights_write_only = true generate
    p_read_last_weight : process(mm_clk)
    begin
      if rising_edge(mm_clk) then
        -- Capture last write
        if ram_bf_weights_mosi.wr = '1' then
          wr_last_weight <= ram_bf_weights_mosi.wrdata(c_word_w - 1 downto 0);
        end if;
        -- Read last write
        ram_bf_weights_miso.rdval <= '0';
        if ram_bf_weights_mosi.rd = '1' then
          ram_bf_weights_miso.rddata <= RESIZE_MEM_DATA(wr_last_weight);
          ram_bf_weights_miso.rdval  <= '1';
        end if;
      end if;
    end process;
  end generate;

  -- All BF weights can be readback
  gen_read_all_weights : if g_weights_write_only = false generate
    ram_bf_weights_miso <= mm_bf_weights_miso;
  end generate;

  -- Combine the internal array of mm interfaces to one array that is connected to the port of bf_unit
  u_mem_mux_weight : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_bf.nof_signal_paths,
    g_mult_addr_w => ceil_log2(g_bf.nof_weights)
  )
  port map (
    mosi     => ram_bf_weights_mosi,
    miso     => mm_bf_weights_miso,
    mosi_arr => mm_weight_mosi_arr,
    miso_arr => mm_weight_miso_arr
  );

  gen_bf : for I in 0 to g_bf.nof_signal_paths - 1 generate
    -- Instantiate a weight factor memory for each input stage:
    u_weight_ram : entity common_lib.common_ram_crw_crw
    generic map (
      g_technology     => g_technology,
      g_ram            => c_weights_buf,
      g_init_file      => sel_a_b(g_bf_weights_file_name = "UNUSED", g_bf_weights_file_name, g_bf_weights_file_name & "_" & natural'image(I) & ".hex"),
      g_true_dual_port => not(g_weights_write_only)
    )
    port map (
      -- MM side
      rst_a     => mm_rst,
      clk_a     => mm_clk,
      wr_en_a   => mm_weight_mosi_arr(I).wr,
      wr_dat_a  => mm_weight_mosi_arr(I).wrdata(c_weights_buf.dat_w - 1 downto 0),
      adr_a     => mm_weight_mosi_arr(I).address(c_weights_buf.adr_w - 1 downto 0),
      rd_en_a   => mm_weight_mosi_arr(I).rd,
      rd_dat_a  => mm_weight_miso_arr(I).rddata(c_weights_buf.dat_w - 1 downto 0),
      rd_val_a  => mm_weight_miso_arr(I).rdval,
      -- MULT side
      rst_b     => dp_rst,
      clk_b     => dp_clk,
      wr_en_b   => '0',
      wr_dat_b  => (others => '0'),
      adr_b     => weight_addr,
      rd_en_b   => '1',
      rd_dat_b  => mult_miso_arr(I).rddata(c_weights_buf.dat_w - 1 downto 0),
      rd_val_b  => open
    );

    data_re_arr(I) <= r.bf_in_sosi_arr(I).re(g_bf.in_dat_w - 1 downto 0);
    data_im_arr(I) <= r.bf_in_sosi_arr(I).im(g_bf.in_dat_w - 1 downto 0);

    weight_re_arr(I) <= mult_miso_arr(I).rddata(  g_bf.in_weight_w - 1 downto 0);
    weight_im_arr(I) <= mult_miso_arr(I).rddata(2 * g_bf.in_weight_w - 1 downto g_bf.in_weight_w);

    u_multiplier : entity common_mult_lib.common_complex_mult
    generic map (
      g_technology       => g_technology,
      g_variant          => "IP",
      g_in_a_w           => g_bf.in_weight_w,
      g_in_b_w           => g_bf.in_dat_w,
      g_out_p_w          => c_prod_w,
      g_conjugate_b      => c_conjugate,
      g_pipeline_input   => 1,
      g_pipeline_product => 0,
      g_pipeline_adder   => 1,
      g_pipeline_output  => 1
    )
    port map (
      rst        => dp_rst,
      clk        => dp_clk,
      in_ar      => weight_re_arr(I),
      in_ai      => weight_im_arr(I),
      in_br      => data_re_arr(I),
      in_bi      => data_im_arr(I),
      out_pr     => prod_re_arr(I),
      out_pi     => prod_im_arr(I)
    );

    -- Map the product array to a vector for the adder tree input
    prod_re_vec((I + 1) * c_prod_w - 1 downto I * c_prod_w) <= prod_re_arr(I);
    prod_im_vec((I + 1) * c_prod_w - 1 downto I * c_prod_w) <= prod_im_arr(I);
  end generate gen_bf;

  -- One adder tree for the real part
  u_adder_tree_re : entity common_lib.common_adder_tree(str)
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_adder_stage_latency,
    g_nof_inputs     => g_bf.nof_signal_paths,
    g_dat_w          => c_prod_w,
    g_sum_w          => c_sum_w
  )
  port map (
    clk    => dp_clk,
    in_dat => prod_re_vec,
    sum    => sum_re
  );

  -- One adder tree for the imaginary part
  u_adder_tree_im : entity common_lib.common_adder_tree(str)
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_adder_stage_latency,
    g_nof_inputs     => g_bf.nof_signal_paths,
    g_dat_w          => c_prod_w,
    g_sum_w          => c_sum_w
  )
  port map (
    clk    => dp_clk,
    in_dat => prod_im_vec,
    sum    => sum_im
  );

  ------------------------------------------------------------------------------
  -- Counter used to create addresses for the weight memory
  ------------------------------------------------------------------------------
  weight_adrs_cnt : entity common_lib.common_counter
  generic map(
    g_latency   => 1,
    g_init      => 0,
    g_width     => c_weights_buf.adr_w,
    g_max       => g_bf.nof_weights - 1,
    g_step_size => 1
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    cnt_clr => bf_in_sosi_arr(0).eop,
    cnt_en  => bf_in_sosi_arr(0).valid,
    count   => weight_addr
  );

  ------------------------------------------------------------------------------
  -- Pipeline to align the sosi control
  ------------------------------------------------------------------------------
  u_dp_pipeline : entity dp_lib.dp_pipeline
  generic map(
    g_pipeline   => c_bf_unit_latency
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => bf_in_sosi_arr(0),
    -- ST source
    src_out      => piped_sosi
  );

  process(piped_sosi, sum_re, sum_im)
  begin
    beams_raw_sosi <= piped_sosi;
    beams_raw_sosi.re <= RESIZE_DP_DSP_DATA(sum_re);
    beams_raw_sosi.im <= RESIZE_DP_DSP_DATA(sum_im);
  end process;

  ------------------------------------------------------------------------------
  -- Beamlets output
  ------------------------------------------------------------------------------
  -- Pass on raw beams data without any further requantization
  out_raw_sosi <= beams_raw_sosi;

  -- Requantize for internal BST and out_bst_sosi output
  u_dp_requantize_bst : entity dp_lib.dp_requantize
  generic map (
    g_complex             => true,
    g_representation      => "SIGNED",
    g_lsb_w               => c_bst_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => false,  -- default BF should not clip
    g_msb_clip_symmetric  => false,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_sum_w,
    g_out_dat_w           => g_bf.bst_dat_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => beams_raw_sosi,
    src_out    => beams_bst_sosi,
    out_ovr    => open
  );

  out_bst_sosi <= beams_bst_sosi;

  -- Requantize for out_qua_sosi output
  u_dp_requantize_out : entity dp_lib.dp_requantize
  generic map (
    g_complex             => true,
    g_representation      => "SIGNED",
    g_lsb_w               => c_out_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => false,  -- default BF should not clip
    g_msb_clip_symmetric  => false,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_sum_w,
    g_out_dat_w           => g_bf.out_dat_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => beams_raw_sosi,
    src_out    => out_qua_sosi,
    out_ovr    => open
  );

  ------------------------------------------------------------------------------
  -- Internal BST
  ------------------------------------------------------------------------------
  u_beamlet_statistics : entity st_lib.st_sst
  generic map(
    g_technology    => g_technology,
    g_nof_stat      => g_bf.nof_weights,
    g_xst_enable    => c_xst_enable,
    g_in_data_w     => g_bf.bst_dat_w,
    g_stat_data_w   => g_bf.stat_data_w,
    g_stat_data_sz  => g_bf.stat_data_sz
  )
  port map (
    mm_rst          => mm_rst,
    mm_clk          => mm_clk,
    dp_rst          => dp_rst,
    dp_clk          => dp_clk,
    in_complex      => beams_bst_sosi,
    ram_st_sst_mosi => ram_st_sst_mosi,
    ram_st_sst_miso => ram_st_sst_miso,
    reg_st_sst_mosi => reg_st_sst_mosi,
    reg_st_sst_miso => reg_st_sst_miso
  );
end str;
