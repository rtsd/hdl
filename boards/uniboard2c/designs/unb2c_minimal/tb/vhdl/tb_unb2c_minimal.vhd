-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for unb2c_minimal.
-- Description:
--   The DUT can be targeted at unb 0, node 3 with the same Python scripts
--   that are used on hardware.
-- Usage:
--   On command line do:
--     > run_modelsim & (to start Modeslim)
--
--   In Modelsim do:
--     > lp unb2c_minimal
--     > mk clean all (only first time to clean all libraries)
--     > mk all (to compile all libraries that are needed for unb2c_minimal)
--     . load tb_unb1_minimal simulation by double clicking the tb_unb2c_minimal icon
--     > as 10 (to view signals in Wave Window)
--     > run 100 us (or run -all)
--
--   On command line do:
--     > python $UPE_GEAR/peripherals/util_system_info.py --gn 3 -n 0 -v 5 --sim
--     > python $UPE_GEAR/peripherals/util_ppsh.py --gn 3 -n 1 -v 5 --sim
--

library IEEE, common_lib, unb2c_board_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use common_lib.tb_common_pkg.all;
use i2c_lib.i2c_dev_unb2_pkg.all;
use i2c_lib.i2c_commander_unb2_pmbus_pkg.all;

entity tb_unb2c_minimal is
    generic (
      g_design_name : string  := "unb2c_minimal";
      g_sim_unb_nr  : natural := 0;  -- UniBoard 0
      g_sim_node_nr : natural := 3  -- Node 3
    );
end tb_unb2c_minimal;

architecture tb of tb_unb2c_minimal is
  constant c_sim             : boolean := true;

  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 3;  -- Node 3
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_unb_nr, c_unb2c_board_nof_uniboard_w) & TO_UVEC(c_node_nr, c_unb2c_board_nof_chip_w);

  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);

  constant c_cable_delay     : time := 12 ns;
  constant c_eth_clk_period  : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_clk_period      : time := 5 ns;
  constant c_pps_period      : natural := 1000;

  -- DUT
  signal clk                 : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal pps_rst             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic_vector(1 downto 0);
  signal eth_txp             : std_logic_vector(1 downto 0);
  signal eth_rxp             : std_logic_vector(1 downto 0);

  signal VERSION             : std_logic_vector(c_unb2c_board_aux.version_w - 1 downto 0) := c_version;
  signal ID                  : std_logic_vector(c_unb2c_board_aux.id_w - 1 downto 0)      := c_id;
  signal TESTIO              : std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);

  signal qsfp_led            : std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  clk     <= not clk after c_clk_period / 2;  -- External clock (200 MHz)
  eth_clk(0) <= not eth_clk(0) after c_eth_clk_period / 2;  -- Ethernet ref clock (25 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, clk, pps);

  ------------------------------------------------------------------------------
  -- 1GbE Loopback model
  ------------------------------------------------------------------------------
  eth_rxp(0) <= transport eth_txp(0) after c_cable_delay;

  eth_rxp(1) <= '0';
  eth_txp(1) <= '0';

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_unb2c_minimal : entity work.unb2c_minimal
    generic map (
      g_sim         => c_sim,
      g_sim_unb_nr  => c_unb_nr,
      g_sim_node_nr => c_node_nr,
      g_design_name => g_design_name
    )
    port map (
      -- GENERAL
      CLK         => clk,
      PPS         => pps,
      WDI         => WDI,
      INTA        => INTA,
      INTB        => INTB,

      -- Others
      VERSION     => VERSION,
      ID          => ID,
      TESTIO      => TESTIO,

      -- 1GbE Control Interface
      ETH_clk     => eth_clk,
      ETH_SGIN    => eth_rxp,
      ETH_SGOUT   => eth_txp,

      QSFP_LED    => qsfp_led
    );
end tb;
