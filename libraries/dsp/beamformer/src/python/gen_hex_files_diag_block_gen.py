###############################################################################
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from common import *
from mem_init_file import list_to_hex

# Purpose:
# . Generate HEX files with complex counter data 
# Description:
# .

PATH = "../hex"
FILENAME = "mms_diag_block_gen_0"

NOF_WORDS = 32
COMPLEX_WIDTH = 8

MEM_WIDTH = 2*COMPLEX_WIDTH
MEM_DEPTH = NOF_WORDS

###############################################################################
# Create the data list
###############################################################################
complex_data = NOF_WORDS * [complex( 1, 1)]
concat_data = concat_complex(complex_data, COMPLEX_WIDTH)

print complex_data
print concat_data

###############################################################################
# Write the HEX file
###############################################################################
list_to_hex(concat_data, PATH+"/"+FILENAME+".hex", MEM_WIDTH, MEM_DEPTH)

