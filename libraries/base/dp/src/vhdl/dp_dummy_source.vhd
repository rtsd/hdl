-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose :
--   Provide packetized dummy values when sink is ready

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_dummy_source is
  generic (
    g_dummy_nof_data   : natural := 100;  -- Nof words per block
    g_dummy_bsn        : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0)      := x"DDDDDDDD_DDDDDDDD";
    g_dummy_data       : std_logic_vector(c_dp_stream_data_w - 1 downto 0)     := x"DDDDDDDD_DDDDDDDD_DDDDDDDD_DDDDDDDD_DDDDDDDD_DDDDDDDD_DDDDDDDD_DDDDDDDD";
    g_dummy_re         : std_logic_vector(c_dp_stream_dsp_data_w - 1 downto 0) := x"DDDDDDDD_DDDDDDDD";
    g_dummy_im         : std_logic_vector(c_dp_stream_dsp_data_w - 1 downto 0) := x"DDDDDDDD_DDDDDDDD";
    g_dummy_empty      : std_logic_vector(c_dp_stream_empty_w - 1 downto 0)    := x"DD";
    g_dummy_channel    : std_logic_vector(c_dp_stream_channel_w - 1 downto 0)  := x"DDDDDDDD";
    g_dummy_err        : std_logic_vector(c_dp_stream_error_w - 1 downto 0)    := x"DDDDDDDD"
   );
  port (
    rst                : in  std_logic;
    clk                : in  std_logic;

    src_out            : out t_dp_sosi;
    src_in             : in  t_dp_siso
  );
end dp_dummy_source;

architecture rtl of dp_dummy_source is
  constant c_dp_sosi_dummy : t_dp_sosi := ('0', g_dummy_bsn, g_dummy_data, g_dummy_re, g_dummy_im, '0', '0', '0', g_dummy_empty, g_dummy_channel, g_dummy_err);

  signal block_sosi : t_dp_sosi;
begin
  src_out.valid   <= block_sosi.valid;
  src_out.sop     <= block_sosi.sop;
  src_out.eop     <= block_sosi.eop;

  src_out.sync    <= c_dp_sosi_dummy.sync;
  src_out.data    <= c_dp_sosi_dummy.data;
  src_out.bsn     <= c_dp_sosi_dummy.bsn;
  src_out.re      <= c_dp_sosi_dummy.re;
  src_out.im      <= c_dp_sosi_dummy.im;
  src_out.empty   <= c_dp_sosi_dummy.empty;
  src_out.channel <= c_dp_sosi_dummy.channel;
  src_out.err     <= c_dp_sosi_dummy.err;

  u_dp_block_gen: entity work.dp_block_gen
  generic map (
    g_nof_data => g_dummy_nof_data
  )
  port map (
    rst        => rst,
    clk        => clk,

    src_in     => src_in,
    src_out    => block_sosi,

    en         => '1'
  );
end rtl;
