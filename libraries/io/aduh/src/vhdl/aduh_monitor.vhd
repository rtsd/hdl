-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose : Monitor ADC statistics for the 4 ADCs on ADU
-- Description :
--   For one input signal path provide MM access to:
--   . ADC mean
--   . ADC power
--   . ADC data buffer time samples stored in big endian order:
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |          t0[7:0]           t1[7:0]           t2[7:0]           t3[7:0]|  0
--  |-----------------------------------------------------------------------|
--  |          t4[7:0]           t5[7:0]           t6[7:0]           t7[7:0]|  1
--  |-----------------------------------------------------------------------|
--  |                                 ...                                   | ..
--  |-----------------------------------------------------------------------|
--  |       t1020[7:0]        t1021[7:0]        t1022[7:0]        t1023[7:0]|255
--  |-----------------------------------------------------------------------|
--
-- Remarks:

entity aduh_monitor is
  generic (
    g_symbol_w             : natural := 8;
    g_nof_symbols_per_data : natural := 4;  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    g_nof_accumulations    : natural := 800 * 10**6;  -- integration time in symbols, defines internal accumulator widths
    g_buffer_nof_symbols   : natural := 1024;
    g_buffer_use_sync      : boolean := false  -- when TRUE start filling the buffer after the in_sync, else after the last word was read
  );
  port (
    mm_rst         : in  std_logic;
    mm_clk         : in  std_logic;

    buf_mosi       : in  t_mem_mosi;  -- read and overwrite access to the data buffer
    buf_miso       : out t_mem_miso;

    -- Streaming inputs
    st_rst         : in  std_logic;
    st_clk         : in  std_logic;

    in_sosi        : in t_dp_sosi;  -- Signal path with data 4 800MHz 8b samples in time per one 32b word @ 200MHz

    -- Monitor outputs
    stat_mean_sum  : out std_logic_vector(63 downto 0);  -- use fixed 64 bit sum width
    stat_pwr_sum   : out std_logic_vector(63 downto 0);  -- use fixed 64 bit sum width
    stat_sync      : out std_logic  -- at the stat_sync there are new mean_sum and pwr_sum statistics available
  );
end aduh_monitor;

architecture str of aduh_monitor is
  constant c_data_w          : natural := g_nof_symbols_per_data * g_symbol_w;  -- = 32, must be <= 32 to fit the u_data_buffer
  constant c_stat_w          : natural := 2 * c_word_w;  -- support upto 64 bit sum width, this is more than enough without truncation and no accumulator overflow
  constant c_buffer_nof_data : natural := g_buffer_nof_symbols / g_nof_symbols_per_data;
begin
  u_mean : entity work.aduh_mean_sum
  generic map (
    g_symbol_w             => g_symbol_w,
    g_nof_symbols_per_data => g_nof_symbols_per_data,  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    g_nof_accumulations    => g_nof_accumulations,  -- integration time in symbols
    g_sum_truncate         => false,  -- when TRUE truncate (keep MS part) else resize (keep sign and LS part)
    g_sum_w                => c_stat_w  -- typcially MM word width = 32
  )
  port map (
    clk         => st_clk,
    rst         => st_rst,

    -- Streaming inputs
    in_data     => in_sosi.data(c_data_w - 1 downto 0),
    in_val      => in_sosi.valid,
    in_sync     => in_sosi.sync,

    -- Accumulation outputs
    sum         => stat_mean_sum,
    sum_sync    => open
  );

  u_power : entity work.aduh_power_sum
  generic map (
    g_symbol_w             => g_symbol_w,
    g_nof_symbols_per_data => g_nof_symbols_per_data,  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    g_nof_accumulations    => g_nof_accumulations,  -- integration time in symbols
    g_pwr_sum_truncate     => false,  -- when TRUE truncate (keep MS part) else resize (keep sign and LS part)
    g_pwr_sum_w            => c_stat_w  -- typcially MM word width = 32
  )
  port map (
    clk          => st_clk,
    rst          => st_rst,

    -- Streaming inputs
    in_data      => in_sosi.data(c_data_w - 1 downto 0),
    in_val       => in_sosi.valid,
    in_sync      => in_sosi.sync,

    -- Accumulation outputs
    pwr_sum      => stat_pwr_sum,
    pwr_sum_sync => stat_sync
  );

  u_data_mon: entity diag_lib.diag_data_buffer
  generic map (
    g_data_w      => c_data_w,  -- <= c_word_w = 32b, the MM word width
    g_nof_data    => c_buffer_nof_data,
    g_use_in_sync => g_buffer_use_sync  -- when TRUE start filling the buffer after the in_sync, else after the last word was read
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,

    ram_mm_mosi => buf_mosi,  -- read and overwrite access to the data buffer
    ram_mm_miso => buf_miso,

    -- Streaming clock domain
    st_rst      => st_rst,
    st_clk      => st_clk,

    in_data     => in_sosi.data(c_data_w - 1 downto 0),
    in_sync     => in_sosi.sync,
    in_val      => in_sosi.valid
  );
end str;
