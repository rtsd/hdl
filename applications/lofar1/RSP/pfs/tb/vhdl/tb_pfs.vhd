-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: Testbench for Polyphase FIR filter
--
-- Usage:
-- > as 5
-- > run -a
-- The tb is self stpping, but not self checking
-- In Wave Window:
-- . Copy pfs_dat_x
-- . View pfs_dat_x in decimal radix and analog format (right click)

library IEEE, pfs_lib, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_pfs is
end tb_pfs;

architecture tb of tb_pfs is
  constant clk_period   : time := 5.0 ns;

  signal in_dat_x       : std_logic_vector(11 downto 0) := (others => '0');
  signal in_dat_y       : std_logic_vector(11 downto 0) := (others => '0');
  signal in_val         : std_logic := '0';
  signal in_sync        : std_logic := '0';
  signal pfs_dat_x      : std_logic_vector(17 downto 0);
  signal pfs_dat_y      : std_logic_vector(17 downto 0);
  signal pfs_val        : std_logic;
  signal pfs_sync       : std_logic;
  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';
  signal tb_end         : std_logic := '0';
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '0' after 3 * clk_period;

  pfs : entity pfs_lib.pfs
  generic map (
    g_nof_bands         => 1024,
    g_nof_taps          => 16 * 1024,
    g_in_dat_w          => 12,
    g_out_dat_w         => 18,
    g_coef_dat_w        => 16
  )
  port map (
    in_dat_x            => in_dat_x,
    in_dat_y            => in_dat_y,
    in_val              => in_val,
    in_sync             => in_sync,
    out_dat_x           => pfs_dat_x,
    out_dat_y           => pfs_dat_y,
    out_val             => pfs_val,
    out_sync            => pfs_sync,
    restart             => '0',
    clk                 => clk,
    rst                 => rst
  );

  -- Keep in_dat_x high for one slice to get the filter impules response. Using amplitude 1024
  -- yields the FIR coefficients, this amplitude compensates the PFS scaling of 2^4/2^14. The
  -- FIR coefficients come out in reversed order per each slice. The total reponse takes 16
  -- slices, because the FIR filter has 16 taps.
  impulse_in : process
  begin
    in_val <= '0';
    in_dat_x <= TO_UVEC(0, in_dat_x'length);
    wait for 1 us;
    wait until rising_edge(clk);
    -- LOFAR1 sync is active one clk cycle before sync interval
    -- create sync for first sync interval
    in_sync <= '1';
    wait until rising_edge(clk);
    in_sync <= '0';
    in_val <= '1';
    for k in 1 to 2 loop

      -- issue impulse
      for j in 1 to 16 loop
        for i in 1 to 1024 loop
          if j = 1 then
            in_dat_x <= TO_UVEC(2**10, in_dat_x'length);
          else
            in_dat_x <= TO_UVEC(0, in_dat_x'length);
          end if;
          wait until rising_edge(clk);
        end loop;
      end loop;

      -- continue some more per sync interval
      proc_common_wait_some_cycles(clk, 1024 * 4 - 1);  -- -1 to create sync for next sync interval
      in_sync <= '1';
      wait until rising_edge(clk);
      in_sync <= '0';
    end loop;
    -- continue some more to observe last pfs_sync
    proc_common_wait_some_cycles(clk, 1024 * 1);
    tb_end <= '1';
    wait;
  end process;
end tb;
