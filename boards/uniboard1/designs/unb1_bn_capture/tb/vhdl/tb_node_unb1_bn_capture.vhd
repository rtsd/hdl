-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for node_bn_capture
--
-- Features:
-- . Perform I2C read access to ADU.
-- . Schedule WG start at a BSN
-- . Optionally add stimuli for DDR3 storage in p_reg_ddr3_stimuli and
--   enable the ddr3_I and or ddr3_II in g_use_phy.
--
-- Usage:
-- . > as 10
-- . > run -a
-- . Observe in Wave Window that the WG indeed do start. Therefore manually
--   view sp_sosi_arr at the sample rate using the 4 SP scope_sosi_arr().data
--   in node_unb1_bn_capture/unb1_bn_capture_input/aduh_quad_scope/
--   dp_wideband_sp_arr_scope and viewing these integer data fields as
--   Radix=decimal and Format=analogue.
-- . The self test only verifies the exp_result_data for the ADU I2C access.
--   The tb_end depends on reg stimuli done and I2C stimuli done so that
--   proves that the tb has run.

library IEEE, common_lib, dp_lib, unb1_board_lib, diag_lib, aduh_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.unb1_bn_capture_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use aduh_lib.aduh_dd_pkg.all;
use i2c_lib.i2c_pkg.all;
use i2c_lib.i2c_commander_pkg.all;
use i2c_lib.i2c_commander_aduh_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;

entity tb_node_unb1_bn_capture is
  generic (
    -- TYPE t_c_unb_use_phy IS RECORD
    --   eth1g   : NATURAL;
    --   eth10g  : NATURAL;
    --   tr_mesh : NATURAL;
    --   tr_back : NATURAL;
    --   ddr3_I  : NATURAL;
    --   ddr3_II : NATURAL;
    --   adc     : NATURAL;
    --   wdi     : NATURAL;
    -- END RECORD;
    g_use_phy     : t_c_unb1_board_use_phy := (0, 0, 0, 0, 0, 0, 1, 0);
    g_enable_wg   : boolean := true
  );
end tb_node_unb1_bn_capture;

architecture tb of tb_node_unb1_bn_capture is
  -- UniBoard
  constant c_sim                   : boolean := true;
  constant c_version               : std_logic_vector(1 downto 0) := "00";
  constant c_id_bn0                : std_logic_vector(7 downto 0) := "00000100";
  constant c_fw_version            : t_unb1_board_fw_version := (1, 0);

  constant c_bn_capture_sp_sim     : t_c_bn_capture_sp := (800, 1024, 48 * 1024, 4 * 1024, true);  -- 800 MSps, block size 1024 samples, 48 blocks per sync interval, monitor buffer 4096 samples using sync
  constant c_bn_capture            : t_c_bn_capture := (c_unb1_board_mm_clk_freq_50M,
                                                        c_unb1_board_ext_clk_freq_200M,
                                                        c_bn_capture_sp_sim);

  constant c_eth_clk_period        : time := 40 ns;  -- 25 MHz XO on UniBoard
  constant c_mm_clk_period         : time := 20 ns;  -- 50 MHz MM clock, altpll in SOPC will make this from the ETH 25 MHz XO clock

  constant c_cable_delay           : time := 12 ns;

  -- ADU
  constant c_ai                    : t_c_aduh_dd_ai := (4, 2, 2, 8, 2, 2, false, false, c_aduh_delays);  -- keep DCLK_RST = '0', others as in c_aduh_dd_ai
  --CONSTANT c_ai                    : t_c_aduh_dd_ai := (4, 2, 2, 8, 2, 2, TRUE, TRUE, c_aduh_delays);  -- invert DCLK_RST, others as in c_aduh_dd_ai
  --CONSTANT c_ai                    : t_c_aduh_dd_ai := c_aduh_dd_ai;

  constant c_dp_factor             : natural := c_ai.rx_factor * c_ai.dd_factor;
  constant c_wideband_factor       : natural := c_dp_factor;  -- = 4

  constant c_sample_freq           : natural := c_wideband_factor * c_unb1_board_ext_clk_freq_200M / 10**6;  -- 800 MSps
  constant c_sample_period         : time := (10**6 / c_sample_freq) * 1 ps;  -- 1250 ns
  constant c_ext_clk_freq          : natural := c_unb1_board_ext_clk_freq_200M;  -- 200 MHz external reference clock for data path processing
  constant c_ext_clk_period        : time := c_wideband_factor * c_sample_period;  -- 200 MHz

  constant c_ana_diff              : natural := 16;  -- analogue offset value between the port A, B, C, D, use 0 to have same data on all ports

  -- ADC
  signal ANA_DAT                   : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- Common ADC reference data source
  signal ANA_A                     : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port A
  signal ANA_B                     : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port B
  signal ANA_C                     : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port C
  signal ANA_D                     : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port D
  signal ANA_OVR                   : std_logic := '0';
  signal SCLK                      : std_logic := '1';  -- central sample clock = 800 MHz
  signal DCLK_AB                   : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_CD                   : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_RST_AB               : std_logic;  -- synchronise digital lvds clock
  signal DCLK_RST_CD               : std_logic;  -- synchronise digital lvds clock
  signal DIG_A                     : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_B                     : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_C                     : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_D                     : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_OVR_AB                : std_logic := '0';
  signal DIG_OVR_CD                : std_logic := '0';

  signal test_pattern_en           : std_logic := '0';

  signal ADC_AB_SCL                : std_logic;
  signal ADC_AB_SDA                : std_logic;
  signal ADC_CD_SCL                : std_logic;
  signal ADC_CD_SDA                : std_logic;

  -- Test bench
  signal tb_end                    : std_logic := '0';
  signal reg_input_stimuli_done    : std_logic := '0';
  signal i2c_adu_stimuli_done      : std_logic := '0';

  -- DUT
  signal mm_clk                    : std_logic := '0';
  signal mm_locked                 : std_logic := '0';
  signal mm_rst                    : std_logic;
  signal ext_clk                   : std_logic := '0';
  signal ext_pps                   : std_logic;
  signal WDI                       : std_logic;
  signal INTA                      : std_logic;
  signal INTB                      : std_logic;

  signal dp_rst                    : std_logic;
  signal dp_clk                    : std_logic;
  signal dp_pps                    : std_logic;

  signal eth_clk                   : std_logic := '0';
  signal eth_txp                   : std_logic;
  signal eth_rxp                   : std_logic;

  signal VERSION                   : std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
  signal ID                        : std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
  signal TESTIO                    : std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

  signal pout_debug_wave           : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
  signal pout_wdi                  : std_logic := '0';
  signal pin_system_info           : std_logic_vector(c_word_w - 1 downto 0);
  signal pin_pps                   : std_logic_vector(c_word_w - 1 downto 0);

  signal pin_pps_toggle            : std_logic;
  signal pin_pps_capture_cnt       : std_logic_vector(ceil_log2(c_ext_clk_freq) - 1 downto 0);  -- counts the number of clock cycles between subsequent pps pulses

  -- MM bsn source
  signal current_bsn_wg            : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := (others => '0');
  signal current_bsn_sp            : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := (others => '0');
  signal nof_block_per_sync        : std_logic_vector(c_word_w - 1 downto 0);

  signal reg_bsn_source_mosi       : t_mem_mosi := c_mem_mosi_rst;  -- Start a BSN timestamp
  signal reg_bsn_source_miso       : t_mem_miso;

  -- MM bsn schedule WG
  signal reg_bsn_scheduler_wg_mosi : t_mem_mosi := c_mem_mosi_rst;  -- Schedule WG restart at a BSN, read current BSN
  signal reg_bsn_scheduler_wg_miso : t_mem_miso;

  -- Wideband Waveform Generators [0,1,2,3] for signal paths [A,B,C,D]
  signal reg_wg_mosi_arr           : t_mem_mosi_arr(0 to c_ai.nof_sp - 1);
  signal reg_wg_miso_arr           : t_mem_miso_arr(0 to c_ai.nof_sp - 1);
  signal ram_wg_mosi_arr           : t_mem_mosi_arr(0 to c_ai.nof_sp - 1);
  signal ram_wg_miso_arr           : t_mem_miso_arr(0 to c_ai.nof_sp - 1);

  -- Commander [0,1] for ADU-[AB,CD]
  signal reg_commander_mosi_arr    : t_mem_mosi_arr(0 to c_ai.nof_adu - 1);
  signal reg_commander_miso_arr    : t_mem_miso_arr(0 to c_ai.nof_adu - 1);
  signal ram_protocol_mosi_arr     : t_mem_mosi_arr(0 to c_ai.nof_adu - 1);
  signal ram_protocol_miso_arr     : t_mem_miso_arr(0 to c_ai.nof_adu - 1);
  signal ram_result_mosi_arr       : t_mem_mosi_arr(0 to c_ai.nof_adu - 1);
  signal ram_result_miso_arr       : t_mem_miso_arr(0 to c_ai.nof_adu - 1);

  -- Commander results [0,1] for ADU-[AB,CD]
  signal cmdr_protocol_status      : t_natural_arr(0 to c_ai.nof_adu - 1);
  signal cmdr_result_data          : t_natural_arr(0 to c_ai.nof_adu - 1);
  signal exp_result_data           : t_natural_arr(0 to c_ai.nof_adu - 1) := (0, 60);  -- expected temp 60 degrees in adu_half
  signal cmdr_result_error_cnt     : t_natural_arr(0 to c_ai.nof_adu - 1);
  signal exp_result_error_cnt      : t_natural_arr(0 to c_ai.nof_adu - 1) := (0, 0);

  -- MM bsn schedule SP
  signal reg_bsn_scheduler_sp_on_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_scheduler_sp_on_miso : t_mem_miso;
  signal reg_bsn_scheduler_sp_off_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bsn_scheduler_sp_off_miso : t_mem_miso;
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------

  tb_end <= '0', reg_input_stimuli_done and i2c_adu_stimuli_done after c_mm_clk_period * 100;

  eth_clk <= not eth_clk or tb_end after c_eth_clk_period / 2;  -- 1GbE XO clock (25 MHz)

  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;  -- MM clock (50 MHz)
  mm_locked <= '0', '1' after c_mm_clk_period * 7;

  ext_clk <= not ext_clk or tb_end after c_ext_clk_period / 2;  -- External clock (200 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  VERSION <= c_version;
  ID <= c_id_bn0;

  ----------------------------------------------------------------------------
  -- Monitor PIO
  ----------------------------------------------------------------------------

  pin_pps_toggle      <= pin_pps(31);
  pin_pps_capture_cnt <= pin_pps(pin_pps_capture_cnt'length - 1 downto 0);

  ----------------------------------------------------------------------------
  -- Stimuli for MM reg_input slave port
  ----------------------------------------------------------------------------

  p_reg_input_stimuli : process
    variable v_bsn : natural;
  begin
    reg_bsn_source_mosi       <= c_mem_mosi_rst;
    reg_bsn_scheduler_wg_mosi <= c_mem_mosi_rst;
    reg_wg_mosi_arr           <= (others => c_mem_mosi_rst);
    ram_wg_mosi_arr           <= (others => c_mem_mosi_rst);
    ext_pps <= '0';

    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    ----------------------------------------------------------------------------
    -- Enable BS
    ----------------------------------------------------------------------------

    -- Issue a PPS pulse to restart the BS due to the pending mm_bs_restart_evt
    -- BSN source register settings
    -- * address 0
    --    mm_bs_enable         : bit 0
    --    mm_bs_restart_evt    : bit 1   -- write '1' indicates the event
    -- * address 2, 3
    --    mm_bs_init_bsn       : BSN[47:0]
    proc_mem_mm_bus_wr(3, 16#00000000#, mm_clk, reg_bsn_source_mosi);
    proc_mem_mm_bus_wr(2, 16#00000000#, mm_clk, reg_bsn_source_mosi);  -- Init BSN = 0
    proc_mem_mm_bus_wr(0, 16#00000001#, mm_clk, reg_bsn_source_mosi);  -- Enable BS (start without using PPS)
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Wait for as long as we want ADU data to be processed
    proc_common_wait_some_cycles(mm_clk, 1000);

    ----------------------------------------------------------------------------
    -- Enable WG
    ----------------------------------------------------------------------------

    if g_enable_wg = true then
      -- * address offset = 16
      -- * address  0,  1,  2,  3: WG 0 control
      -- * address  4,  5,  6,  7: WG 1 control
      -- * address  8,  9, 10, 11: WG 2 control
      -- * address 12, 13, 14, 15: WG 3 control
      --   . mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
      --     nof_samples[31:16]  --> <= c_ram_wg_size=1024
      --   . phase[15:0]
      --   . freq[30:0]
      --   . ampl[16:0]
      proc_mem_mm_bus_wr(0, 2 + 1024 * 2**16, mm_clk, reg_wg_mosi_arr(0));  -- mode repeat, nof_sample
      proc_mem_mm_bus_wr(0, 2 + 1024 * 2**16, mm_clk, reg_wg_mosi_arr(1));
      proc_mem_mm_bus_wr(0, 2 + 1024 * 2**16, mm_clk, reg_wg_mosi_arr(2));
      proc_mem_mm_bus_wr(0, 2 + 1024 * 2**16, mm_clk, reg_wg_mosi_arr(3));
      proc_common_wait_some_cycles(mm_clk, 10);

      -- Read current BSN
      -- * address 0, 1: BSN current
      proc_mem_mm_bus_rd(0, mm_clk, reg_bsn_scheduler_wg_mosi);
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
      current_bsn_wg(31 downto  0) <= reg_bsn_scheduler_wg_miso.rddata(31 downto 0);
      proc_mem_mm_bus_rd(1, mm_clk, reg_bsn_scheduler_wg_mosi);
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
      current_bsn_wg(47 downto 32) <= reg_bsn_scheduler_wg_miso.rddata(15 downto 0);
      proc_common_wait_some_cycles(mm_clk, 1);

      -- Write scheduler BSN to trigger start of WG at next block
      v_bsn := TO_UINT(current_bsn_wg) + 2;
      proc_mem_mm_bus_wr(0, v_bsn, mm_clk, reg_bsn_scheduler_wg_mosi);  -- first write low then high part
      proc_mem_mm_bus_wr(1,     0, mm_clk, reg_bsn_scheduler_wg_mosi);  -- assume v_bsn < 2**31-1

      -- Continue forever with WG data
    end if;
    proc_common_wait_some_cycles(mm_clk, 1000);
    reg_input_stimuli_done <= '1';
    wait;
  end process;

  ----------------------------------------------------------------------------
  -- Stimuli for MM I2C ADU slave ports
  ----------------------------------------------------------------------------

  p_mm_i2c_adu_stimuli : process
    -- Commander MM register word indexes
    constant c_protocol_status_wi   : natural := 3 * c_i2c_cmdr_aduh_protocol_commander.nof_protocols;
    constant c_result_error_cnt_wi  : natural := 3 * c_i2c_cmdr_aduh_protocol_commander.nof_protocols + 1;
    constant c_result_data_wi       : natural := 3 * c_i2c_cmdr_aduh_protocol_commander.nof_protocols + 2;

    constant c_A : natural range 0 to c_ai.nof_adu - 1 := 1;  -- only try ADUH index (1) = ADU-CD
    constant c_P : natural := 0;  -- 0 = c_i2c_adu_max1617_protocol_list_read_temp --> expected temp 60 degrees in adu_half
  begin
    reg_commander_mosi_arr <= (others => c_mem_mosi_rst);
    ram_protocol_mosi_arr  <= (others => c_mem_mosi_rst);
    ram_result_mosi_arr    <= (others => c_mem_mosi_rst);

    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    -- VHDL does not allow using static indexed *_arr(I) in proc_mem_mm_bus_wr() and proc_mem_mm_bus_rd(),
    -- therefore implement this without using a LOOP and only for c_A=0 for ADU-AB or c_A=1 for ADU-CD.

    -- Issue protocol list c_P
    proc_mem_mm_bus_wr(c_P, 1, mm_clk, reg_commander_mosi_arr(c_A));

    -- Wait for protocol done
    while cmdr_protocol_status(c_A) /= c_i2c_cmdr_state_done loop
      -- read commander protocol status register
      proc_mem_mm_bus_rd(c_protocol_status_wi, mm_clk, reg_commander_miso_arr(c_A), reg_commander_mosi_arr(c_A));
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
      cmdr_protocol_status(c_A) <= TO_UINT(reg_commander_miso_arr(c_A).rddata);
      proc_common_wait_some_cycles(mm_clk, 1);
    end loop;

    -- Read commander result error count
    proc_mem_mm_bus_rd(c_result_error_cnt_wi, mm_clk, reg_commander_miso_arr(c_A), reg_commander_mosi_arr(c_A));
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    cmdr_result_error_cnt(c_A) <= TO_UINT(reg_commander_miso_arr(c_A).rddata);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert cmdr_result_error_cnt(c_A) = 0
      report "The result error count is not 0"
      severity ERROR;

    -- Read commander result data
    for J in 0 to c_i2c_cmdr_aduh_nof_result_data_arr(c_P) - 1 loop
      proc_mem_mm_bus_rd(c_result_data_wi + J, mm_clk, reg_commander_miso_arr(c_A), reg_commander_mosi_arr(c_A));
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
      cmdr_result_data(c_A) <= TO_UINT(reg_commander_miso_arr(c_A).rddata);
      proc_common_wait_some_cycles(mm_clk, 1);
    end loop;

    -- Wait for protocol idle
    while cmdr_protocol_status(c_A) /= c_i2c_cmdr_state_idle loop
      -- read commander protocol status register
      proc_mem_mm_bus_rd(c_protocol_status_wi, mm_clk, reg_commander_miso_arr(c_A), reg_commander_mosi_arr(c_A));
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
      cmdr_protocol_status(c_A) <= TO_UINT(reg_commander_miso_arr(c_A).rddata);
      proc_common_wait_some_cycles(mm_clk, 1);
    end loop;
    proc_common_wait_some_cycles(mm_clk, 100);
    i2c_adu_stimuli_done <= '1';
    assert cmdr_result_data = exp_result_data
      report "Unexpected ADU temperature read via I2C"
      severity ERROR;
    assert cmdr_result_error_cnt = exp_result_error_cnt
      report "Unexpected ADU I2C access error count > 0"
      severity ERROR;
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim                     => c_sim,
    g_fw_version              => c_fw_version,
    g_mm_clk_freq             => c_bn_capture.mm_clk_freq,
    g_dp_clk_freq             => c_bn_capture.dp_clk_freq,
    g_use_phy                 => g_use_phy,
    g_aux                     => c_unb1_board_aux
  )
  port map (
    -- System
    -- Clock an reset signals
    -- System
    cs_sim                   => OPEN,
    xo_clk                   => OPEN,
    xo_rst_n                 => OPEN,

    mm_clk                   => mm_clk,  -- 50 MHz
    mm_locked                => mm_locked,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,  -- 200 MHz from CLK system clock
    dp_pps                   => dp_pps,  -- PPS in dp_clk domain
    dp_rst_in                => dp_rst,  -- externally wire OUT dp_rst to dp_rst_in to avoid delta cycle difference on dp_clk
    dp_clk_in                => dp_clk,  -- externally wire OUT dp_clk to dp_clk_in to avoid delta cycle difference on dp_clk (due to dp_clk <= i_dp_clk assignment)

     -- PIOs
    pout_debug_wave          => pout_debug_wave,
    pout_wdi                 => pout_wdi,

    -- eth1g
    eth1g_tse_clk            => '0',
    eth1g_mm_rst             => '1',
    eth1g_tse_mosi           => c_mem_mosi_rst,
    eth1g_tse_miso           => OPEN,
    eth1g_reg_mosi           => c_mem_mosi_rst,
    eth1g_reg_miso           => OPEN,
    eth1g_reg_interrupt      => OPEN,
    eth1g_ram_mosi           => c_mem_mosi_rst,
    eth1g_ram_miso           => OPEN,

    -- FPGA pins
    -- . General
    CLK                      => ext_clk,
    PPS                      => ext_pps,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . 1GbE Control Interface
    ETH_clk                  => eth_clk,
    ETH_SGIN                 => eth_rxp,
    ETH_SGOUT                => eth_txp
  );

  u_node : entity work.node_unb1_bn_capture
  generic map (
    -- General
    g_sim         => c_sim,
    -- BN capture specific
    g_bn_capture  => c_bn_capture,
    -- Use PHY Interface
    g_use_phy     => g_use_phy,
    -- Auxiliary Interface
    g_aux         => c_unb1_board_aux,
    -- ADC Interface
    g_ai          => c_ai
  )
  port map (
    --
    -- >>> SOPC system with conduit peripheral MM bus
    --
    -- System
    mm_rst                 => mm_rst,
    mm_clk                 => mm_clk,

    dp_rst                 => dp_rst,
    dp_clk                 => dp_clk,
    dp_pps                 => dp_pps,

    ext_clk                => ext_clk,

    -- MM bsn source
    reg_bsn_source_mosi       => reg_bsn_source_mosi,
    reg_bsn_source_miso       => reg_bsn_source_miso,

    -- MM bsn schedule WG
    reg_bsn_scheduler_wg_mosi => reg_bsn_scheduler_wg_mosi,
    reg_bsn_scheduler_wg_miso => reg_bsn_scheduler_wg_miso,

    -- MM registers [0,1,2,3] for wideband waveform generators [A,B,C,D]
    reg_wg_mosi_arr           => reg_wg_mosi_arr,
    reg_wg_miso_arr           => reg_wg_miso_arr,
    ram_wg_mosi_arr           => ram_wg_mosi_arr,
    ram_wg_miso_arr           => ram_wg_miso_arr,

    -- MM registers [0,1] for I2C access with ADU AB and with ADU CD
    reg_commander_mosi_arr    => reg_commander_mosi_arr,
    reg_commander_miso_arr    => reg_commander_miso_arr,
    ram_protocol_mosi_arr     => ram_protocol_mosi_arr,
    ram_protocol_miso_arr     => ram_protocol_miso_arr,
    ram_result_mosi_arr       => ram_result_mosi_arr,
    ram_result_miso_arr       => ram_result_miso_arr,

    -- MM registers to enable and disable signal path
    reg_bsn_scheduler_sp_on_mosi  => reg_bsn_scheduler_sp_on_mosi,
    reg_bsn_scheduler_sp_on_miso  => reg_bsn_scheduler_sp_on_miso,
    reg_bsn_scheduler_sp_off_mosi => reg_bsn_scheduler_sp_off_mosi,
    reg_bsn_scheduler_sp_off_miso => reg_bsn_scheduler_sp_off_miso,

    -- ADC Interface
    ADC_BI_A               => DIG_A,
    ADC_BI_B               => DIG_B,
    ADC_BI_A_CLK           => DCLK_AB,
    ADC_BI_A_CLK_RST       => DCLK_RST_AB,
    ADC_BI_C               => DIG_C,
    ADC_BI_D               => DIG_D,
    ADC_BI_D_CLK           => DCLK_CD,
    ADC_BI_D_CLK_RST       => DCLK_RST_CD,

    ADC_AB_SCL             => ADC_AB_SCL,
    ADC_AB_SDA             => ADC_AB_SDA,
    ADC_CD_SCL             => ADC_CD_SCL,
    ADC_CD_SDA             => ADC_CD_SDA
  );

  -----------------------------------------------------------------------------
  -- ADU0 model and ADU1 model for BN port A,B and C,D
  -----------------------------------------------------------------------------

  -- Same sample clock for all ADC
  SCLK <= not SCLK or tb_end after c_sample_period / 2;

  -- Same analogue reference signal for all ADC, use incrementing data to ease the verification
  ANA_DAT <= INCR_UVEC(ANA_DAT, 1) when rising_edge(SCLK);
  ANA_A   <= INCR_UVEC(ANA_DAT, 0 * c_ana_diff);
  ANA_B   <= INCR_UVEC(ANA_DAT, 1 * c_ana_diff);
  ANA_C   <= INCR_UVEC(ANA_DAT, 2 * c_ana_diff);
  ANA_D   <= INCR_UVEC(ANA_DAT, 3 * c_ana_diff);
  ANA_OVR <= not ANA_OVR when rising_edge(SCLK);  -- simple overflow model used for both ADC

  -- National ADC
  u_adc_AB : entity aduh_lib.adu_half
  port map (
    AI              => TO_SINT(ANA_A),
    AQ              => TO_SINT(ANA_B),
    AOVR            => ANA_OVR,
    CLK             => SCLK,
    DCLK            => DCLK_AB,
    DCLK_RST        => DCLK_RST_AB,
    DI              => DIG_A,
    DQ              => DIG_B,
    OVR             => DIG_OVR_AB,
    SCL             => ADC_AB_SCL,
    SDA             => ADC_AB_SDA,
    test_pattern_en => test_pattern_en
  );

  u_adc_CD : entity aduh_lib.adu_half
  port map (
    AI              => TO_SINT(ANA_C),
    AQ              => TO_SINT(ANA_D),
    AOVR            => ANA_OVR,
    CLK             => SCLK,
    DCLK            => DCLK_CD,
    DCLK_RST        => DCLK_RST_CD,
    DI              => DIG_C,
    DQ              => DIG_D,
    OVR             => DIG_OVR_CD,
    SCL             => ADC_CD_SCL,
    SDA             => ADC_CD_SDA,
    test_pattern_en => test_pattern_en
  );

  ------------------------------------------------------------------------------
  -- 1GbE Loopback model
  ------------------------------------------------------------------------------

  eth_rxp <= transport eth_txp after c_cable_delay;
end tb;
