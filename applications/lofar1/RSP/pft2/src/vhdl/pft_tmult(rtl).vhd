-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library common_mult_lib;
library common_lib;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

architecture rtl of pft_tmult is
  constant c_nof_twids      : natural := 16 * 2**(2 * (g_index - 1));
  constant c_adr_w          : natural := 2 + 2 * g_index;

  constant c_mult_in_w      : natural := 18;
  constant c_coeff_w        : natural := 16;
  constant c_mult_out_w     : natural := c_mult_in_w + c_coeff_w - 1;

  constant c_twid_rom       : t_c_mem := (latency  => 2,
                                          adr_w    => c_adr_w,
                                          dat_w    => 2 * c_coeff_w,  -- complex
                                          nof_dat  => 3 * c_nof_twids / 4,  -- <= 2**g_addr_w
                                          init_sl  => '0');

  constant c_twid_file      : string  :=
    "data/twiddle_" & natural'image(c_coeff_w)
    & "_" & natural'image(g_index) & ".hex";  -- Quartus .hex extension, replaced by .bin in common_rom works for XST
  --CONSTANT c_twid_file      : STRING  :=
  --  "../../../../../pft2/src/data/twiddle_" & NATURAL'IMAGE(c_coeff_w)
  --  & "_" & NATURAL'IMAGE(g_index) & ".bin";    -- Synplify fails on file extension change to .bin in common_rom and requires extra ../

  constant c_read_pipeline      : natural := 1;
  constant c_mult_pipeline_input   : natural := 1;  -- 0 or 1
  constant c_mult_pipeline_product : natural := 0;  -- 0 or 1
  constant c_mult_pipeline_adder   : natural := 1;  -- 0 or 1
  constant c_mult_pipeline_output  : natural := 1;  -- >= 0
  constant c_mult_pipeline         : natural := c_mult_pipeline_input + c_mult_pipeline_product + c_mult_pipeline_adder + c_mult_pipeline_output;  -- = 3
  constant c_round_pipeline_in  : natural := 1;
  constant c_round_pipeline_out : natural := 1;
  constant c_round_pipeline     : natural := c_round_pipeline_in + c_round_pipeline_out;
  constant c_pipeline           : natural := c_round_pipeline + c_mult_pipeline + c_round_pipeline;

  signal reg_val            : std_logic_vector(c_pipeline-1 downto 0);
  signal nxt_reg_val        : std_logic_vector(reg_val'range);
  signal reg_sync           : std_logic_vector(c_pipeline-1 downto 0);

  signal nxt_reg_sync       : std_logic_vector(reg_sync'range);

  signal adr                : std_logic_vector(c_adr_w - 1 downto 0);
  signal nxt_adr            : std_logic_vector(c_adr_w - 1 downto 0);

  signal cnt                : std_logic_vector(c_adr_w - 1 downto 0);
  signal nxt_cnt            : std_logic_vector(cnt'range);

  signal mult_in_re         : std_logic_vector(c_mult_in_w - 1 downto 0);
  signal mult_in_im         : std_logic_vector(c_mult_in_w - 1 downto 0);

  signal mult_out_re        : std_logic_vector(c_mult_out_w - 1 downto 0);
  signal mult_out_im        : std_logic_vector(c_mult_out_w - 1 downto 0);

  signal coeff_dat          : std_logic_vector(2 * c_coeff_w - 1 downto 0);
  signal coeff_re           : std_logic_vector(c_coeff_w - 1 downto 0);
  signal coeff_im           : std_logic_vector(c_coeff_w - 1 downto 0);
begin
  p_regs : process (clk, rst)
  begin
    if rst = '1' then
      reg_val     <= (others => '0');
      reg_sync    <= (others => '0');
      cnt         <= (others => '0');
      adr         <= (others => '0');
      coeff_re    <= (others => '0');
      coeff_im    <= (others => '0');
    elsif rising_edge(clk) then
      reg_val     <= nxt_reg_val;
      reg_sync    <= nxt_reg_sync;
      cnt         <= nxt_cnt;
      adr         <= nxt_adr;
      coeff_re    <= coeff_dat(coeff_re'range);
      coeff_im    <= coeff_dat(coeff_dat'high downto coeff_re'length);
    end if;
  end process;

  p_cnt : process (cnt, in_val, in_sync)
  begin
    nxt_cnt <= cnt;
    if in_sync = '1' then
      nxt_cnt <= (others => '0');
    elsif in_val = '1' then
      nxt_cnt <= std_logic_vector(unsigned(cnt) + 1);
    end if;
  end process;

  p_adr : process (adr, cnt, reg_sync)
  begin
    nxt_adr <= adr;
    if unsigned(adr) = 3 * c_nof_twids / 4 - 1 or reg_sync(reg_sync'high) = '1' then
      nxt_adr <= (others => '0');
    elsif unsigned(adr) > 0 or unsigned(cnt) = c_nof_twids / 4 - 1 then
      nxt_adr <= std_logic_vector(unsigned(adr) + 1);
    end if;
  end process;

  nxt_reg_val   <= in_val  & reg_val(reg_val'high  downto 1);
  nxt_reg_sync  <= in_sync & reg_sync(reg_sync'high downto 1);
  out_val       <= reg_val(0);
  out_sync      <= reg_sync(0);

  u_coeff : entity common_lib.common_rom
  generic map (
    g_ram        => c_twid_rom,
    g_init_file  => c_twid_file
  )
  port map (
    rst          => rst,
    clk          => clk,
    rd_adr       => adr,
    rd_dat       => coeff_dat
  );

  u_rnd1 : entity common_lib.common_complex_round
  generic map (
    g_representation  => "SIGNED",
    g_round           => true,
    g_round_clip      => false,
    g_pipeline_input  => c_round_pipeline_in,
    g_pipeline_output => c_round_pipeline_out,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => c_mult_in_w
  )
  port map (
    in_re          => in_re,
    in_im          => in_im,
    out_re         => mult_in_re,
    out_im         => mult_in_im,
    clk            => clk
  );

  u_cmult : entity common_mult_lib.common_complex_mult
  generic map (
    g_variant     => "IP",
    g_in_a_w      => c_mult_in_w,
    g_in_b_w      => c_coeff_w,
    g_out_p_w     => c_mult_out_w,
    g_conjugate_b => false,
    g_pipeline_input   => c_mult_pipeline_input,  -- 0 or 1
    g_pipeline_product => c_mult_pipeline_product,  -- 0 or 1
    g_pipeline_adder   => c_mult_pipeline_adder,  -- 0 or 1
    g_pipeline_output  => c_mult_pipeline_output  -- >= 0
  )
  port map (
    in_ar         => mult_in_re,
    in_ai         => mult_in_im,
    in_br         => coeff_re,
    in_bi         => coeff_im,
    out_pr        => mult_out_re,
    out_pi        => mult_out_im,
    clk           => clk
  );

  u_rnd2 : entity common_lib.common_complex_round
  generic map (
    g_representation  => "SIGNED",
    g_round           => true,
    g_round_clip      => false,
    g_pipeline_input  => c_round_pipeline_in,
    g_pipeline_output => c_round_pipeline_out,
    g_in_dat_w        => c_mult_out_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    in_re          => mult_out_re,
    in_im          => mult_out_im,
    out_re         => out_re,
    out_im         => out_im,
    clk            => clk
  );
end rtl;
