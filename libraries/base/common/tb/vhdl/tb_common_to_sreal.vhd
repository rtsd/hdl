-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 13 okt 2021
-- Purpose: Verify to_sreal() and to_svec() from common_pkg.vhd
-- Description:
--   The tb shows how a REAL value is represented by a fixed point SLV. The
--   signed REAL value is denoted as s(w, p) as defined and explained in []:
--
--     s = sign bit, value +1 or -1
--     w = width in number of bits of the SLV
--     p = position of the fixed point in the SLV, p = 0 for integers, p > 0
--         for p bit fraction, p < 0 for scale factor 2**p
--
--   The resolution of the REAL value is r = 1/2**p.
--
--   . to_svec() converts a fixed point INTEGER or SLV into a REAL value
--   . to_sreal() converts a REAL value into a fixed point SLV or INTEGER
--
--   The tb verifies that a_slv(w-1:0) = to_svec(to_sreal(a_slv(w-1:0))).
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L3+SDP+Decision%3A+Definition+of+fixed+point+numbers
--
-- Usage:
-- > as 5, observe signed signals with radix decimal, and observe unsigned
--         signals with radix unsigend.
-- > run -all
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.math_real.all;
use work.common_pkg.all;

entity tb_common_to_sreal is
end tb_common_to_sreal;

architecture tb of tb_common_to_sreal is
  constant clk_period   : time    := 10 ns;

  constant c_resolution_w : natural := 5;
  constant c_width        : natural := 4;
  constant c_min          : integer := -2**(c_width - 1);
  constant c_max          : integer :=  2**(c_width - 1) - 1;

  signal tb_end           : std_logic := '0';
  signal clk              : std_logic := '1';
  signal rst              : std_logic := '1';

  signal a_real           : real := 0.0;
  signal a_sint           : integer := 0;
  signal a_uint           : natural := 0;
  signal a_slv            : std_logic_vector(c_width - 1 downto 0) := (others => '0');  -- signed slv
  signal a_ulv            : std_logic_vector(c_width - 1 downto 0) := (others => '0');  -- unsigned slv
  signal dbg_resolution_w : integer := 0;
  signal dbg_resolution   : real := 0.0;

  procedure proc_wait_some_cycles(signal clk          : in  std_logic;
                                         c_nof_cycles : in  natural) is
  begin
    for I in 0 to c_nof_cycles - 1 loop wait until rising_edge(clk); end loop;
  end proc_wait_some_cycles;
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  -- Testbench end
  p_tb : process
    variable v_slv : std_logic_vector(c_width - 1 downto 0) := (others => '0');
    variable v_real : real;
  begin
    -- Simulator evaluates process until first WAIT at startup 0 ps, but
    -- simulator does not recognise WAIT in proc_wait_some_cycles() then.
    -- Therefore add WAIT here to avoid ASSERT messages that occur later.
    wait until rising_edge(clk);
    proc_wait_some_cycles(clk, 10);

    -- Try all v_slv for c_width and resolutions beyond c_width
    for R in - c_resolution_w to c_resolution_w loop
      dbg_resolution_w <= R;
      dbg_resolution <= 1.0 / 2.0**REAL(R);
      for I in c_min to c_max loop
        v_slv := TO_SVEC(I, c_width);
        -- Convert fixed point v_slv with binary point at resolution width R, to real and back to slv
        v_real := TO_SREAL(v_slv, R);
        -- Show as signals in Wave window
        a_real <= v_real;
        a_sint <= TO_SINT(v_real, c_width, R);
        a_slv <= TO_SVEC(v_real, c_width, R);
        if I >= 0 then
          a_uint <= TO_UINT(v_real, c_width, R);
          a_ulv <= TO_UVEC(v_real, c_width, R);
        end if;
        wait until rising_edge(clk);
        -- Verify
        assert a_sint = I
          report "Wrong real to integer conversion for I = " & integer'image(I)
          severity ERROR;
        assert a_slv = v_slv
          report "Wrong real to SLV conversion for I = " & integer'image(I)
          severity ERROR;
        if I >= 0 then
          assert a_uint = I
            report "Wrong real to natural conversion for I = " & natural'image(I)
            severity ERROR;
          assert a_ulv = v_slv
            report "Wrong real to unsigned SLV conversion for I = " & natural'image(I)
            severity ERROR;
        end if;
      end loop;
      proc_wait_some_cycles(clk, 10);
    end loop;

    proc_wait_some_cycles(clk, 10);

    -- Try overflow
    -- . No overflow with 4 bit integers for -16.49 : +15.49
    v_real := -9.51; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -9.49; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -8.51; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -8.49; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -7.51; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -7.49; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -6.51; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -6.49; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  6.49; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  6.51; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  7.49; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  7.51; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    proc_wait_some_cycles(clk, 5);

    -- . Just overflow with 4 bit integers for -16.5 : +15.5
    v_real := -15.5; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  15.5; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    proc_wait_some_cycles(clk, 5);

    -- . Negative clip to 0 for unsigned
    v_real := -3.0; a_real <= v_real; a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    assert a_uint = 0
      report "Unexpected TO_UINT(< 0) : " & integer'image(a_uint) & " /= 0"
      severity ERROR;
    assert TO_UINT(a_ulv) = 0
      report "Unexpected TO_UVEC(< 0) : " & integer'image(TO_UINT(a_ulv)) & " /= 0"
      severity ERROR;

    proc_wait_some_cycles(clk, 5);
    -- . Large overflow with 4 bit integers for << -16.5 : >> +15.5
    v_real := -68.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -58.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -48.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -38.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -28.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real := -18.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  18.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  28.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  38.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  48.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  58.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);
    v_real :=  68.0; a_real <= v_real; a_sint <= TO_SINT(v_real, 4, 0); a_slv <= TO_SVEC(v_real, 4, 0); wait until rising_edge(clk);
                                       a_uint <= TO_UINT(v_real, 4, 0); a_ulv <= TO_UVEC(v_real, 4, 0); wait until rising_edge(clk);

    proc_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  -- TO_SINT() and TO_SVEC() must always yield same result
  assert a_sint = TO_SINT(a_slv)
    report "Unexpected difference between TO_SINT() and TO_SVEC() :" &
           integer'image(a_sint) & " /= " & integer'image(TO_SINT(a_slv))
    severity ERROR;

  -- TO_UINT() and TO_UVEC() must always yield same result
  assert a_uint = TO_UINT(a_ulv)
    report "Unexpected difference between TO_UINT() and TO_UVEC() :" &
           integer'image(a_uint) & " /= " & integer'image(TO_SINT(a_ulv))
    severity ERROR;
end tb;
