-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Assert valid, sop and eop only when they are valid.
-- Description:
--   . Assert valid regarding the ready latency
--   . Assert sop and eop only when valid is active
--   . Pass the SOSI data signals on as wires
--   . If g_boolean_error=TRUE then vector-OR the snk_in.err into a bit 0 of
--     src_out.err, where '0' = OK and '1' = error.
-- Remark:
--   . All assignments are combinatorial, so no clock latency

entity dp_validate is
  generic (
    g_ready_latency : natural := 0;
    g_boolean_error : boolean := false
  );
  port (
    -- ST sink
    snk_out : out t_dp_siso;
    snk_in  : in  t_dp_sosi;
    -- ST source
    src_in  : in  t_dp_siso;
    src_out : out t_dp_sosi
  );
end dp_validate;

architecture rtl of dp_validate is
  signal valid : std_logic;
begin
  -- SISO ready
  snk_out <= src_in;

  -- SOSI
  -- The downstream sink ready latency must match that of the upstream source.
  -- For ready latency 0 the src_out.valid must only be interpreted when the
  -- src_in.ready is asserted. The src_in.ready input needs to be passed on via
  -- the snk_out.ready output of this component.
  -- According do the Avalon specification in case the ready latency > 0 then
  -- the valid will only be asserted by the upstream source after ready latency
  -- cycles when its src_in.ready was asserted. Hence then no (delayed) check
  -- on the src_in.ready is needed.
  valid <= snk_in.valid and src_in.ready when g_ready_latency = 0 else snk_in.valid;

  p_sosi : process(snk_in, valid)
  begin
    src_out <= snk_in;

    -- According do the Avalon specification sop and eop are only interpreted
    -- when valid is asserted, so to be save AND them with valid.
    src_out.valid <= valid;
    src_out.sop   <= valid and snk_in.sop;
    src_out.eop   <= valid and snk_in.eop;

    -- Combine error vector into one bit if necessary
    if g_boolean_error = true then
      src_out.err <= (others => '0');
      if unsigned(snk_in.err) /= 0 then
        src_out.err(0) <= '1';
      end if;
    end if;
  end process;
end rtl;
