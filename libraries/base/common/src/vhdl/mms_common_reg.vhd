-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : MMS for common_reg_r_w_dc
-- Description: Wrapper, see common_reg_r_w_dc.vhd

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mms_common_reg is
  generic (
    -- TYPE t_c_mem IS RECORD
    --   latency   : NATURAL;    -- read latency
    --   adr_w     : NATURAL;
    --   dat_w     : NATURAL;
    --   nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
    --   init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
    g_mm_reg : t_c_mem := c_mem_reg  -- =  (1,  1, 32,  1, 'X')
  );
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock
    st_rst     : in  std_logic;  -- reset synchronous with st_clk
    st_clk     : in  std_logic;  -- other clock domain clock

    -- MM bus access in memory-mapped clock domain
    reg_mosi   : in  t_mem_mosi;
    reg_miso   : out t_mem_miso;

    -- MM register IO in ST clock domain
    -- 1) Connect out_reg to in_reg for write and readback register.
    -- 2) Do not connect out_reg to in_reg for seperate write only register and read only register at the same address.
    -- 3) Leave out_reg OPEN for read only register.
    in_reg     : in  std_logic_vector(g_mm_reg.dat_w * g_mm_reg.nof_dat - 1 downto 0);
    out_reg    : out std_logic_vector(g_mm_reg.dat_w * g_mm_reg.nof_dat - 1 downto 0)
  );
end mms_common_reg;

architecture str of mms_common_reg is
  constant c_init_reg   : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := (others => g_mm_reg.init_sl);
begin
  u_common_reg_r_w_dc : entity work.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => true,
    g_readback           => false,
    g_reg                => g_mm_reg,
    g_init_reg           => c_init_reg
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => st_rst,
    st_clk         => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in         => reg_mosi,
    sla_out        => reg_miso,

    -- MM registers in st_clk domain
    reg_wr_arr     => OPEN,
    reg_rd_arr     => OPEN,
    in_reg         => in_reg,
    out_reg        => out_reg
  );
end str;
