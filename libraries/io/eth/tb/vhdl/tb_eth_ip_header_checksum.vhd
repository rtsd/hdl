-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose:
-- . Test bench for eth_ip_header_checksum.
-- Description:
--                 u_tx                       DUT                        u_rx
--                 ___________________        ___________________        ___________________
--                |dp_offload_tx_v3   |      |eth_ip_header_     |      |dp_offload_rx      |
-- stimuli_src -->|                   |----->|checksum           |----->|                   |--> verify_snk
--                | in            out |      | in            out |   |  | in            out |
--                |___________________|      |___________________|   |  |___________________|
--                                                                   |
--  i                                                         link_offload_sosi
--
--   The verification of the header - data block boundary is controlled via
--   g_symbol_w:
--   . g_symbol_w = g_data_w : boundary at g_data_w
--   . g_symbol_w < g_data_w : boundary at g_symbol_w, by reducing the number
--     of the header dp_bsn field by 1 symbol. If the c_bsn_w <= 32 then the
--     header MM interface only needs one MM word to read the header, so
--     therefore there are two sizes of c_expected_tx_hdr_word_arr_* and
--     c_expected_rx_hdr_word_arr_*.
--
-- Remarks:
-- . The g_flow_control_verify has to be e_active, otherwise the tb fails,
--   probably due to limitation in dp_offload_rx.vhd.
-- . It appears that the tx_hdr_word read values are the MM write values, so
--   read of value from logic fields (with MM override '0', e.g. dp_bsn,
--   eth_src_mac) is not supported.
-- . testbench is based on tb_offload_tx_v3.
--
-- Usage:
-- > as 10
-- > run -all
--

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_eth_ip_header_checksum is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_pulse;  -- always e_active, e_random or e_pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    g_print_en               : boolean := true;
    -- specific
    g_data_w                 : natural := 64;
    g_symbol_w               : natural := 8;
    g_empty                  : natural := 6;  -- number of empty symbols in header when g_symbol_w < g_data_w, must be < c_nof_symbols_per_data
    g_pkt_len                : natural := 240;
    g_pkt_gap                : natural := 16
  );
end tb_eth_ip_header_checksum;

architecture tb of tb_eth_ip_header_checksum is
  constant c_mm_clk_period : time := 1 ns;
  constant c_dp_clk_period : time := 5 ns;

  -- Simulate header / payload boundary at g_data_w boundary or at g_symbol_w
  -- boundary with one empty symbol, by adapting the size of the header dp_bsn
  -- field:
  -- . If g_symbol_w = g_data_w then boundary is at g_data_w, so empty is 0.
  -- . If g_symbol_w < g_data_w then boundary is at last symbol, so empty is 1.
  constant c_nof_symbols_per_data     : natural := g_data_w / g_symbol_w;
  constant c_nof_symbols_per_bsn      : natural := c_dp_stream_bsn_w / g_symbol_w;  -- = 64 / g_symbol_w
  constant c_bsn_w                    : natural := sel_a_b(c_nof_symbols_per_data = 1,
                                                           g_symbol_w *  c_nof_symbols_per_bsn,
                                                           g_symbol_w * (c_nof_symbols_per_bsn - g_empty));
  constant c_use_shortened_header     : boolean := c_bsn_w <= c_word_w;

  -- dp_stream_stimuli
  constant c_stimuli_pulse_active     : natural := 3;
  constant c_stimuli_pulse_period     : natural := 4;

  -- dp_stream_verify
  constant c_verify_pulse_active      : natural := 1;
  constant c_verify_pulse_period      : natural := 5;

  constant c_data_max                 : unsigned(g_data_w - 1 downto 0) := (others => '1');
  constant c_dsp_max                  : unsigned(g_data_w - 1 downto 0) := (others => '1');

  constant c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := TO_DP_SOSI_UNSIGNED('0', '0', '0', '0', c_data_max, c_dsp_max, c_dsp_max, c_unsigned_0, c_unsigned_0, c_unsigned_0, c_unsigned_0);
  constant c_verify_snk_in_cnt_gap    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_ones;  -- default only accept increment +1

  constant c_expected_pkt_len         : natural := g_pkt_len;
  constant c_sync_period              : natural := 5;
  constant c_sync_offset              : natural := 2;
  constant c_data_init                : natural := 17;
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := TO_DP_BSN(0);
  constant c_nof_sync                 : natural := 3;
  constant c_nof_packets              : natural := c_sync_period * c_nof_sync;

  constant c_hdr_len                  : natural := ceil_div(448, g_data_w);
  constant c_wait_last_evt            : natural := 100 + c_nof_packets * c_hdr_len;

  -----------------------------------------------------------------------------
  -- Tx offload
  -----------------------------------------------------------------------------
  -- From apertif_udp_offload_pkg.vhd:
  constant c_udp_offload_nof_hdr_fields          : natural := 3 + 12 + 4 + 3;  -- 22, 448b; 7 64b words
  constant c_udp_offload_nof_hdr_words_default   : natural := 26;  -- 23 single word + 3 double word = 26 32b words
  constant c_udp_offload_nof_hdr_words_shortened : natural := c_udp_offload_nof_hdr_words_default - 1;
  constant c_udp_offload_nof_hdr_words           : natural := sel_a_b(c_use_shortened_header, c_udp_offload_nof_hdr_words_shortened, c_udp_offload_nof_hdr_words_default);

  -- Notes:
  -- . pre-calculated ip_header_checksum is valid only for UNB0, FN0 targeting IP 10.10.10.10
  -- . udp_total_length = 176 beamlets * 64b / 8b = 1408B + 14 DP bytes + 8 UDP bytes = 1430B
  constant c_udp_offload_hdr_field_arr : t_common_field_arr(c_udp_offload_nof_hdr_fields - 1 downto 0) := (  -- index
         ( field_name_pad("eth_dst_mac"            ), "RW",      48, field_default(x"001B214368AC") ),  -- 21
         ( field_name_pad("eth_src_mac"            ), "RW",      48, field_default(x"0123456789AB") ),  -- 20
         ( field_name_pad("eth_type"               ), "RW",      16, field_default(x"0800") ),  -- 19
         ( field_name_pad("ip_version"             ), "RW",       4, field_default(4) ),  -- 18
         ( field_name_pad("ip_header_length"       ), "RW",       4, field_default(5) ),  -- 17
         ( field_name_pad("ip_services"            ), "RW",       8, field_default(0) ),  -- 16
         ( field_name_pad("ip_total_length"        ), "RW",      16, field_default(1450) ),  -- 15
         ( field_name_pad("ip_identification"      ), "RW",      16, field_default(0) ),  -- 14
         ( field_name_pad("ip_flags"               ), "RW",       3, field_default(2) ),  -- 13
         ( field_name_pad("ip_fragment_offset"     ), "RW",      13, field_default(0) ),  -- 12
         ( field_name_pad("ip_time_to_live"        ), "RW",       8, field_default(127) ),  -- 11
         ( field_name_pad("ip_protocol"            ), "RW",       8, field_default(17) ),  -- 10
         ( field_name_pad("ip_header_checksum"     ), "RW",      16, field_default(0) ),  -- 9, will be calculated by DUT
         ( field_name_pad("ip_src_addr"            ), "RW",      32, field_default(x"C0A80009") ),  -- 8
         ( field_name_pad("ip_dst_addr"            ), "RW",      32, field_default(x"C0A80001") ),  -- 7
         ( field_name_pad("udp_src_port"           ), "RW",      16, field_default(0) ),  -- 6
         ( field_name_pad("udp_dst_port"           ), "RW",      16, field_default(0) ),  -- 5
         ( field_name_pad("udp_total_length"       ), "RW",      16, field_default(1430) ),  -- 4
         ( field_name_pad("udp_checksum"           ), "RW",      16, field_default(0) ),  -- 3
         ( field_name_pad("dp_reserved"            ), "RW",      47, field_default(x"010203040506") ),  -- 2
         ( field_name_pad("dp_sync"                ), "RW",       1, field_default(0) ),  -- 1
         ( field_name_pad("dp_bsn"                 ), "RW", c_bsn_w, field_default(0) ) );  -- 0

  -- TX: Corresponding storage of c_udp_offload_hdr_field_arr in MM register words
  -- . Note: It appears that the tx_hdr_word read values are the MM write values, so read of value from logic fields (with MM override '0', e.g. dp_bsn, eth_src_mac) is not supported.
  constant c_expected_tx_hdr_word_arr_default : t_slv_32_arr(0 to c_udp_offload_nof_hdr_words_default - 1) := (  -- word address
                                                                             X"00000000",  -- 0   = dp_bsn[31:0]        -- readback is MM value, not the logic value
                                                                             X"00000000",  -- 1   = dp_bsn[c_bsn_w-1:32]
                                                                             X"00000000",  -- 2   = dp_sync
                                                                             X"03040506",  -- 3   = dp_reserved[31:0]
                                                                             X"00000102",  -- 4   = dp_reserved[47:32]
                                                                             X"00000000",  -- 5   = udp_checksum
                                                                             X"00000596",  -- 6   = udp_total_length
                                                                             X"00000000",  -- 7   = udp_dst_port
                                                                             X"00000000",  -- 8   = udp_src_port        -- readback is MM value, not the logic value
                                                                             X"C0A80001",  -- 9   = ip_dst_addr
                                                                             X"C0A80009",  -- 10  = ip_src_addr
                                                                             X"00000000",  -- 11  = ip_header_checksum
                                                                             X"00000011",  -- 12  = ip_protocol
                                                                             X"0000007F",  -- 13  = ip_time_to_live
                                                                             X"00000000",  -- 14  = ip_fragment_offset
                                                                             X"00000002",  -- 15  = ip_flags
                                                                             X"00000000",  -- 16  = ip_identification
                                                                             X"000005AA",  -- 17  = ip_total_length
                                                                             X"00000000",  -- 18  = ip_services
                                                                             X"00000005",  -- 19  = ip_header_length
                                                                             X"00000004",  -- 20  = ip_version
                                                                             X"00000800",  -- 21  = eth_type[15:0]
                                                                             X"456789AB",  -- 22  = eth_src_mac[31:0]   -- readback is MM value, not the logic value
                                                                             X"00000123",  -- 23  = eth_src_mac[47:32]
                                                                             X"214368AC",  -- 24  = eth_dst_mac[31:0]
                                                                             X"0000001B");  -- 25  = eth_dst_mac[47:32]

  constant c_expected_tx_hdr_word_arr_shortened : t_slv_32_arr(0 to c_udp_offload_nof_hdr_words_shortened - 1) := (  -- word address
                                                                             X"00000000",  -- 0   = dp_bsn[c_bsn_w-1:0] -- readback is MM value, not the logic value
                                                                             X"00000000",  -- 1   = dp_sync
                                                                             X"03040506",  -- 2   = dp_reserved[31:0]
                                                                             X"00000102",  -- 3   = dp_reserved[47:32]
                                                                             X"00000000",  -- 4   = udp_checksum
                                                                             X"00000596",  -- 5   = udp_total_length
                                                                             X"00000000",  -- 6   = udp_dst_port
                                                                             X"00000000",  -- 7   = udp_src_port        -- readback is MM value, not the logic value
                                                                             X"C0A80001",  -- 8   = ip_dst_addr
                                                                             X"C0A80009",  -- 9   = ip_src_addr
                                                                             X"00000000",  -- 10  = ip_header_checksum
                                                                             X"00000011",  -- 11  = ip_protocol
                                                                             X"0000007F",  -- 12  = ip_time_to_live
                                                                             X"00000000",  -- 13  = ip_fragment_offset
                                                                             X"00000002",  -- 14  = ip_flags
                                                                             X"00000000",  -- 15  = ip_identification
                                                                             X"000005AA",  -- 16  = ip_total_length
                                                                             X"00000000",  -- 17  = ip_services
                                                                             X"00000005",  -- 18  = ip_header_length
                                                                             X"00000004",  -- 19  = ip_version
                                                                             X"00000800",  -- 20  = eth_type[15:0]
                                                                             X"456789AB",  -- 21  = eth_src_mac[31:0]   -- readback is MM value, not the logic value
                                                                             X"00000123",  -- 22  = eth_src_mac[47:32]
                                                                             X"214368AC",  -- 23  = eth_dst_mac[31:0]
                                                                             X"0000001B");  -- 24  = eth_dst_mac[47:32]

  -- RX: Corresponding storage of c_udp_offload_hdr_field_arr in MM register words
  constant c_expected_rx_hdr_word_arr_default : t_slv_32_arr(0 to c_udp_offload_nof_hdr_words_default - 1) := (  -- word address
                                                                             X"00000002",  -- 0   = dp_bsn[31:0]        -- dynamic value obtained from simulation
                                                                             X"00000000",  -- 1   = dp_bsn[c_bsn_w-1:32]
                                                                             X"00000001",  -- 2   = dp_sync             -- dynamic value obtained from simulation
                                                                             X"03040506",  -- 3   = dp_reserved[31:0]
                                                                             X"00000102",  -- 4   = dp_reserved[47:32]
                                                                             X"00000000",  -- 5   = udp_checksum
                                                                             X"00000596",  -- 6   = udp_total_length
                                                                             X"00000000",  -- 7   = udp_dst_port
                                                                             X"00000000",  -- 8   = udp_src_port
                                                                             X"C0A80001",  -- 9   = ip_dst_addr
                                                                             X"C0A80009",  -- 10  = ip_src_addr
                                                                             X"000074E8",  -- 11  = ip_header_checksum
                                                                             X"00000011",  -- 12  = ip_protocol
                                                                             X"0000007F",  -- 13  = ip_time_to_live
                                                                             X"00000000",  -- 14  = ip_fragment_offset
                                                                             X"00000002",  -- 15  = ip_flags
                                                                             X"00000000",  -- 16  = ip_identification
                                                                             X"000005AA",  -- 17  = ip_total_length
                                                                             X"00000000",  -- 18  = ip_services
                                                                             X"00000005",  -- 19  = ip_header_length
                                                                             X"00000004",  -- 20  = ip_version
                                                                             X"00000800",  -- 21  = eth_type[15:0]
                                                                             X"86080000",  -- 22  = eth_src_mac[31:0]   -- readback is the logic value x"00228608" & id_backplane = 0 & id_chip = 0 (c_NODE_ID = 0)
                                                                             X"00000022",  -- 23  = eth_src_mac[47:32]
                                                                             X"214368AC",  -- 24  = eth_dst_mac[31:0]
                                                                             X"0000001B");  -- 25  = eth_dst_mac[47:32]

  constant c_expected_rx_hdr_word_arr_shortened : t_slv_32_arr(0 to c_udp_offload_nof_hdr_words_shortened - 1) := (  -- word address
                                                                             X"00000002",  -- 0   = dp_bsn[c_bsn_w-1:0] -- dynamic value obtained from simulation
                                                                             X"00000001",  -- 1   = dp_sync             -- dynamic value obtained from simulation
                                                                             X"03040506",  -- 2   = dp_reserved[31:0]
                                                                             X"00000102",  -- 3   = dp_reserved[47:32]
                                                                             X"00000000",  -- 4   = udp_checksum
                                                                             X"00000596",  -- 5   = udp_total_length
                                                                             X"00000000",  -- 6   = udp_dst_port
                                                                             X"00000000",  -- 7   = udp_src_port
                                                                             X"C0A80001",  -- 8   = ip_dst_addr
                                                                             X"C0A80009",  -- 9   = ip_src_addr
                                                                             X"000074E8",  -- 10  = ip_header_checksum
                                                                             X"00000011",  -- 11  = ip_protocol
                                                                             X"0000007F",  -- 12  = ip_time_to_live
                                                                             X"00000000",  -- 13  = ip_fragment_offset
                                                                             X"00000002",  -- 14  = ip_flags
                                                                             X"00000000",  -- 15  = ip_identification
                                                                             X"000005AA",  -- 16  = ip_total_length
                                                                             X"00000000",  -- 17  = ip_services
                                                                             X"00000005",  -- 18  = ip_header_length
                                                                             X"00000004",  -- 19  = ip_version
                                                                             X"00000800",  -- 20  = eth_type[15:0]
                                                                             X"86080000",  -- 21  = eth_src_mac[31:0]   -- readback is the logic value x"00228608" & id_backplane = 0 & id_chip = 0 (c_NODE_ID = 0)
                                                                             X"00000022",  -- 22  = eth_src_mac[47:32]
                                                                             X"214368AC",  -- 23  = eth_dst_mac[31:0]
                                                                             X"0000001B");  -- 24  = eth_dst_mac[47:32]

  -- Override ('1') only the Ethernet fields so we can use MM defaults there
  constant c_hdr_field_ovr_init : std_logic_vector(c_udp_offload_nof_hdr_fields - 1 downto 0) := "101" & "111111111111" & "1111" & "100";

  constant c_NODE_ID                    : std_logic_vector(7 downto 0) := TO_UVEC(0, 8);

  signal id_backplane                   : std_logic_vector(c_byte_w - 1 downto 0);
  signal id_chip                        : std_logic_vector(c_byte_w - 1 downto 0);

  signal dp_fifo_sc_src_in              : t_dp_siso := c_dp_siso_rdy;
  signal dp_fifo_sc_src_out             : t_dp_sosi;

  signal dp_offload_tx_snk_in_arr       : t_dp_sosi_arr(0 downto 0);
  signal dp_offload_tx_snk_out_arr      : t_dp_siso_arr(0 downto 0);

  signal tx_hdr_fields_in_arr           : t_slv_1024_arr(0 downto 0);
  signal tx_hdr_fields_out_arr          : t_slv_1024_arr(0 downto 0);

  signal tx_hdr_word                    : std_logic_vector(c_word_w - 1 downto 0);
  signal rx_hdr_word                    : std_logic_vector(c_word_w - 1 downto 0);

  signal reg_dp_offload_tx_hdr_dat_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_offload_tx_hdr_dat_miso : t_mem_miso;

  -----------------------------------------------------------------------------
  -- Link
  -----------------------------------------------------------------------------

  signal tx_offload_sosi_arr       : t_dp_sosi_arr(0 downto 0);
  signal tx_offload_siso_arr       : t_dp_siso_arr(0 downto 0);

  signal link_offload_sosi_arr     : t_dp_sosi_arr(0 downto 0);
  signal link_offload_siso_arr     : t_dp_siso_arr(0 downto 0);

  -----------------------------------------------------------------------------
  -- Rx offload
  -----------------------------------------------------------------------------
  signal dp_offload_rx_src_out_arr      : t_dp_sosi_arr(0 downto 0);
  signal dp_offload_rx_src_in_arr       : t_dp_siso_arr(0 downto 0);

  signal rx_hdr_fields_out_arr          : t_slv_1024_arr(0 downto 0);
  signal rx_hdr_fields_raw_arr          : t_slv_1024_arr(0 downto 0);

  signal reg_dp_offload_rx_hdr_dat_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_offload_rx_hdr_dat_miso : t_mem_miso;

  -----------------------------------------------------------------------------
  -- Test
  -----------------------------------------------------------------------------
  signal mm_clk                     : std_logic := '1';
  signal mm_rst                     : std_logic := '1';
  signal dp_clk                     : std_logic := '1';
  signal dp_rst                     : std_logic := '1';
  signal tb_end                     : std_logic := '0';

  signal stimuli_src_in             : t_dp_siso := c_dp_siso_rdy;
  signal stimuli_src_out            : t_dp_sosi;
  signal stimuli_src_out_data       : std_logic_vector(g_data_w - 1 downto 0);

  signal verify_snk_in_enable       : t_dp_sosi_sl := c_dp_sosi_sl_rst;
  signal last_snk_in                : t_dp_sosi;
  signal last_snk_in_evt            : std_logic;
  signal verify_last_snk_in_evt     : t_dp_sosi_sl := c_dp_sosi_sl_rst;

  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal verify_snk_in_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal prev_verify_snk_in_data    : std_logic_vector(g_data_w - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  u_dp_stream_stimuli : entity dp_lib.dp_stream_stimuli
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 15,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_stimuli_pulse_active,
    g_pulse_period   => c_stimuli_pulse_period,
    g_flow_control   => g_flow_control_stimuli,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_data_init      => c_data_init,
    g_bsn_init       => c_bsn_init,
    -- specific
    g_in_dat_w       => g_data_w,
    g_nof_repeat     => c_nof_packets,
    g_pkt_len        => g_pkt_len,
    g_pkt_gap        => g_pkt_gap,
    g_wait_last_evt  => c_wait_last_evt
  )
  port map (
    rst                 => dp_rst,
    clk                 => dp_clk,

    -- Generate stimuli
    src_in              => stimuli_src_in,
    src_out             => stimuli_src_out,

    -- End of stimuli
    last_snk_in         => last_snk_in,  -- expected verify_snk_in after end of stimuli
    last_snk_in_evt     => last_snk_in_evt,  -- trigger verify to verify the last_snk_in
    tb_end              => tb_end  -- signal end of tb as far as this dp_stream_stimuli is concerned
  );

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Select fields that need to be verified
  -- . during the test
  verify_snk_in_enable.sync    <= '1';
  verify_snk_in_enable.bsn     <= '1';
  verify_snk_in_enable.data    <= '1';
  verify_snk_in_enable.re      <= '0';
  verify_snk_in_enable.im      <= '0';
  verify_snk_in_enable.valid   <= '1';
  verify_snk_in_enable.sop     <= '1';
  verify_snk_in_enable.eop     <= '1';
  verify_snk_in_enable.empty   <= '0';
  verify_snk_in_enable.channel <= '0';
  verify_snk_in_enable.err     <= '0';

  -- . after the test
  verify_last_snk_in_evt.sync    <= last_snk_in_evt;
  verify_last_snk_in_evt.bsn     <= last_snk_in_evt;  -- thanks to using rx_hdr_fields_raw_arr for bsn field
  verify_last_snk_in_evt.data    <= last_snk_in_evt;
  verify_last_snk_in_evt.re      <= '0';
  verify_last_snk_in_evt.im      <= '0';
  verify_last_snk_in_evt.valid   <= last_snk_in_evt;
  verify_last_snk_in_evt.sop     <= last_snk_in_evt;
  verify_last_snk_in_evt.eop     <= last_snk_in_evt;
  verify_last_snk_in_evt.empty   <= '0';
  verify_last_snk_in_evt.channel <= '0';
  verify_last_snk_in_evt.err     <= '0';

  u_dp_stream_verify : entity dp_lib.dp_stream_verify
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 14,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_verify_pulse_active,
    g_pulse_period   => c_verify_pulse_period,
    g_flow_control   => g_flow_control_verify,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_snk_in_cnt_max => c_verify_snk_in_cnt_max,
    g_snk_in_cnt_gap => c_verify_snk_in_cnt_gap,
    -- specific
    g_in_dat_w       => g_data_w,
    g_pkt_len        => c_expected_pkt_len
  )
  port map (
    rst                        => dp_rst,
    clk                        => dp_clk,

    -- Verify data
    snk_out                    => verify_snk_out,
    snk_in                     => verify_snk_in,

    -- During stimuli
    verify_snk_in_enable       => verify_snk_in_enable,  -- enable verify to verify that the verify_snk_in fields are incrementing

    -- End of stimuli
    expected_snk_in            => last_snk_in,  -- expected verify_snk_in after end of stimuli
    verify_expected_snk_in_evt => verify_last_snk_in_evt  -- trigger verify to verify the last_snk_in
  );

  ------------------------------------------------------------------------------
  -- offload Tx
  ------------------------------------------------------------------------------
  stimuli_src_in <= c_dp_siso_rdy;

  -- Use FIFO to handle backpressure just like in a network design.
  u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
  generic map (
    g_data_w         => g_data_w,
    g_bsn_w          => 64,
    g_use_sync       => true,
    g_use_bsn        => true,
    g_fifo_size      => 1024
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,

    snk_out     => OPEN,  -- stimuli_src_in
    snk_in      => stimuli_src_out,

    src_in      => dp_fifo_sc_src_in,
    src_out     => dp_fifo_sc_src_out
  );

  dp_offload_tx_snk_in_arr(0) <= dp_fifo_sc_src_out;
  dp_fifo_sc_src_in           <= dp_offload_tx_snk_out_arr(0);

  -- Extract the chip and backplane numbers from c_NODE_ID
  id_backplane <= RESIZE_UVEC(c_NODE_ID(7 downto 3), c_byte_w);
  id_chip      <= RESIZE_UVEC(c_NODE_ID(2 downto 0), c_byte_w);

  -- Wire the hardwired header fields to DP signals and c_NODE_ID
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "eth_src_mac" ) downto field_lo(c_udp_offload_hdr_field_arr, "eth_src_mac"     )) <= x"00228608" & id_backplane & id_chip;
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "udp_src_port") downto field_lo(c_udp_offload_hdr_field_arr, "udp_src_port"    )) <= x"D0" & c_NODE_ID;
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "udp_dst_port") downto field_lo(c_udp_offload_hdr_field_arr, "udp_dst_port"    )) <= x"D0" & c_NODE_ID;
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "ip_src_addr" ) downto field_lo(c_udp_offload_hdr_field_arr, "ip_src_addr"     )) <= x"0A63" & id_backplane & INCR_UVEC(id_chip, 1);

  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "dp_sync"     ) downto field_lo(c_udp_offload_hdr_field_arr, "dp_sync"         )) <= slv(dp_offload_tx_snk_in_arr(0).sync);
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "dp_bsn"      ) downto field_lo(c_udp_offload_hdr_field_arr, "dp_bsn"          )) <=     dp_offload_tx_snk_in_arr(0).bsn(c_bsn_w - 1 downto 0);

  u_tx : entity dp_lib.dp_offload_tx_v3
  generic map (
    g_nof_streams    => 1,
    g_data_w         => g_data_w,
    g_symbol_w       => g_symbol_w,
    g_hdr_field_arr  => c_udp_offload_hdr_field_arr,
    g_hdr_field_sel  => c_hdr_field_ovr_init
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    reg_hdr_dat_mosi      => reg_dp_offload_tx_hdr_dat_mosi,
    reg_hdr_dat_miso      => reg_dp_offload_tx_hdr_dat_miso,

    snk_in_arr            => dp_offload_tx_snk_in_arr,
    snk_out_arr           => dp_offload_tx_snk_out_arr,

    src_out_arr           => tx_offload_sosi_arr,
    src_in_arr            => tx_offload_siso_arr,

    hdr_fields_in_arr     => tx_hdr_fields_in_arr,
    hdr_fields_out_arr    => tx_hdr_fields_out_arr
  );

  ------------------------------------------------------------------------------
  -- DUT: IP header CRC checksum
  ------------------------------------------------------------------------------
  u_dut : entity work.eth_ip_header_checksum
  generic map (
    g_data_w => g_data_w,
    g_hdr_field_arr => c_udp_offload_hdr_field_arr
  )
  port map (
    rst               => dp_rst,
    clk               => dp_clk,

    src_out           => link_offload_sosi_arr(0),
    snk_in            => tx_offload_sosi_arr(0),

    src_in            => link_offload_siso_arr(0),
    snk_out           => tx_offload_siso_arr(0),

    hdr_fields_slv_in => tx_hdr_fields_out_arr(0)
  );

  p_rd_tx_hdr_words : process
    variable v_word : std_logic_vector(c_word_w - 1 downto 0);
  begin
    proc_common_wait_until_hi_lo(dp_clk, tx_offload_sosi_arr(0).sync);
    print_str("", g_print_en);
    for I in 0 to c_udp_offload_nof_hdr_words - 1 loop
      proc_mem_mm_bus_rd(I, mm_clk, reg_dp_offload_tx_hdr_dat_mosi);
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
      v_word := reg_dp_offload_tx_hdr_dat_miso.rddata(31 downto 0);
      -- Log word in transcript window
      print_str("tx_hdr_word(" & int_to_str(I) & ") = " & slv_to_hex(v_word), g_print_en);
      -- View word in wave window
      tx_hdr_word <= v_word;
      -- Verify expected word
      if c_use_shortened_header then
        assert c_expected_tx_hdr_word_arr_shortened(I) = v_word
          report "Unexpected tx_hdr_word at address " & int_to_str(I) & ", expected " & slv_to_hex(c_expected_tx_hdr_word_arr_shortened(I))
          severity ERROR;
      else
        assert c_expected_tx_hdr_word_arr_default(I) = v_word
          report "Unexpected tx_hdr_word at address " & int_to_str(I) & ", expected " & slv_to_hex(c_expected_tx_hdr_word_arr_default(I))
          severity ERROR;
      end if;
    end loop;
    print_str("", g_print_en);
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- offload Rx
  ------------------------------------------------------------------------------
  u_rx : entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams         => 1,
    g_data_w              => g_data_w,
    g_symbol_w            => g_symbol_w,
    g_hdr_field_arr       => c_udp_offload_hdr_field_arr,
    g_remove_crc          => false,
    g_crc_nof_words       => 0
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    reg_hdr_dat_mosi      => reg_dp_offload_rx_hdr_dat_mosi,
    reg_hdr_dat_miso      => reg_dp_offload_rx_hdr_dat_miso,

    snk_in_arr            => link_offload_sosi_arr,
    snk_out_arr           => link_offload_siso_arr,

    src_out_arr           => dp_offload_rx_src_out_arr,
    src_in_arr            => dp_offload_rx_src_in_arr,

    hdr_fields_out_arr    => rx_hdr_fields_out_arr,
    hdr_fields_raw_arr    => rx_hdr_fields_raw_arr
  );

  p_restore_sync_bsn : process(dp_offload_rx_src_out_arr, rx_hdr_fields_out_arr)
  begin
    verify_snk_in      <= dp_offload_rx_src_out_arr(0);
    verify_snk_in.sync <=          sl(rx_hdr_fields_out_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "dp_sync") downto field_lo(c_udp_offload_hdr_field_arr, "dp_sync" )));
    verify_snk_in.bsn  <= RESIZE_UVEC(rx_hdr_fields_raw_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "dp_bsn" ) downto field_lo(c_udp_offload_hdr_field_arr, "dp_bsn"  )), c_dp_stream_bsn_w);
  end process;

  dp_offload_rx_src_in_arr    <= (others => c_dp_siso_rdy);
  dp_offload_rx_src_in_arr(0) <= verify_snk_out;

  p_rd_rx_hdr_words : process
    variable v_word : std_logic_vector(c_word_w - 1 downto 0);
  begin
    proc_common_wait_until_hi_lo(dp_clk, verify_snk_in.sync);
    -- Check first packet after sync with dp_sync = 1
    -- wait some latency until header fields of this sync packet are available via MM
    proc_common_wait_some_cycles(dp_clk, c_hdr_len + 10);
    print_str("", g_print_en);
    for I in 0 to c_udp_offload_nof_hdr_words - 1 loop
      proc_mem_mm_bus_rd(I, mm_clk, reg_dp_offload_rx_hdr_dat_mosi);
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
      v_word := reg_dp_offload_rx_hdr_dat_miso.rddata(31 downto 0);
      -- Log word in transcript window
      print_str("rx_hdr_word(" & int_to_str(I) & ") : " & slv_to_hex(v_word), g_print_en);
      -- View word in wave window
      rx_hdr_word <= v_word;
      -- Verify expected word
      if c_use_shortened_header then
        assert v_word = c_expected_rx_hdr_word_arr_shortened(I)
          report "Unexpected rx_hdr_word at address " & int_to_str(I) & ", expected " & slv_to_hex(c_expected_rx_hdr_word_arr_shortened(I))
          severity ERROR;
      else
        assert v_word = c_expected_rx_hdr_word_arr_default(I)
          report "Unexpected rx_hdr_word at address " & int_to_str(I) & ", expected " & slv_to_hex(c_expected_rx_hdr_word_arr_default(I))
          severity ERROR;
      end if;
    end loop;
    print_str("", g_print_en);

    -- Check dp_bsn and dp_sync of second packet after sync with dp_sync = 0
    proc_common_wait_until_hi_lo(dp_clk, verify_snk_in.sop);
    -- wait some latency until header fields of this sync packet are available via MM
    proc_common_wait_some_cycles(dp_clk, c_hdr_len + 5);
    -- dp_bsn lo
    proc_mem_mm_bus_rd(0, mm_clk, reg_dp_offload_rx_hdr_dat_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    v_word := reg_dp_offload_rx_hdr_dat_miso.rddata(31 downto 0);
    rx_hdr_word <= v_word;  -- View word in wave window
    if c_use_shortened_header then
      assert v_word = INCR_UVEC(c_expected_rx_hdr_word_arr_shortened(0), 1)
        report "Unexpected dp_bsn from MM"
        severity ERROR;
    else
      assert v_word = INCR_UVEC(c_expected_rx_hdr_word_arr_default(0), 1)
        report "Unexpected dp_bsn from MM"
        severity ERROR;
    end if;
    -- dp_sync
    if c_use_shortened_header then
      proc_mem_mm_bus_rd(1, mm_clk, reg_dp_offload_rx_hdr_dat_mosi);
    else
      proc_mem_mm_bus_rd(2, mm_clk, reg_dp_offload_rx_hdr_dat_mosi);
    end if;
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    v_word := reg_dp_offload_rx_hdr_dat_miso.rddata(31 downto 0);
    rx_hdr_word <= v_word;  -- View word in wave window
    assert v_word = TO_UVEC(0, 32)
      report "Unexpected dp_sync from MM"
      severity ERROR;

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Auxiliary
  ------------------------------------------------------------------------------

  -- Map to slv to ease monitoring in wave window
  stimuli_src_out_data <= stimuli_src_out.data(g_data_w - 1 downto 0);
  verify_snk_in_data   <= verify_snk_in.data(g_data_w - 1 downto 0);
end tb;
