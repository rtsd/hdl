-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Tb for fft_switch.vhd + fft_unswitch.vhd
-- Description:
--
--  p_in_val --> u_fft_switch --> mux --> u_fft_unswitch --> demux --> p_verify
--
--  . p_in_val creates blocks of in_val, with or without g_in_val_gaps
--  . in_a and in_b are offset counter data that increment at in_val
--  . fft_switch uses an lfsr per input to randomly negate or keep the input
--  . mux models that the FFT complex output is multiplexes a, b in time
--  . fft_unswitch use the same lfsr as fft_switch to undo the random negate
--    on the multiplexed a, b output
--  . demux demultiplexes the output so that it can be compared to the delayed
--    input
--  . p_verify checks that the output is equal to the delayed input.
--
-- Remark:
-- . The fft_switch and fft_unswitch only use in_val, the other strobes sop,
--   eop and sync are only for tb debugging purposes to recognize the in_val
--   data blocks of c_nof_clk_per_block samples in the Wave window.
-- . The g_increment_at_val determines whether the in_re, in_im increment at
--   every sample (at in_val), or at every block of samples (at in_eop).
--   Default use g_increment_at_val = TRUE. Increment at eop is for debugging
--   purposes.
--
-- Usage:
-- > as 5
-- > run -a
-- # view a,b and re,im signals in radix decimal

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use work.fft_pkg.all;

entity tb_fft_switch is
  generic (
    g_instance_index     : natural := 0;
    g_switch_en          : boolean := true;
    g_in_val_gaps        : boolean := true;
    g_increment_at_val   : boolean := true;
    g_fft_size_w         : natural := 3;
    g_nof_clk_per_sync   : natural := 32;
    g_nof_sync           : natural := 2
  );
end tb_fft_switch;

architecture tb of tb_fft_switch is
  constant clk_period      : time := 10 ns;

  constant c_dat_w         : natural := 16;

  constant c_nof_clk_per_block      : natural := 2**g_fft_size_w;
  constant c_nof_block_per_sync_max : natural := ceil_div(g_nof_clk_per_sync, c_nof_clk_per_block);
  constant c_nof_block_per_sync_min : natural := g_nof_clk_per_sync / c_nof_clk_per_block;

  constant c_dly           : natural := 4;  -- pipeling in fft_switch, mux,  fft_unswitch and demux

  constant c_switch_seed1  : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := fft_switch_new_seed(c_fft_switch_seed1, g_instance_index);
  constant c_switch_seed2  : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := fft_switch_new_seed(c_fft_switch_seed2, g_instance_index);

  signal tb_end            : std_logic := '0';
  signal rst               : std_logic := '1';
  signal clk               : std_logic := '0';

  -- Use fixed input A, B values
  signal in_a              : std_logic_vector(c_dat_w - 1 downto 0) := TO_SVEC(3, c_dat_w);
  signal in_b              : std_logic_vector(c_dat_w - 1 downto 0) := TO_SVEC(13, c_dat_w);
  signal in_val            : std_logic := '0';
  signal in_sop            : std_logic := '0';
  signal in_eop            : std_logic := '0';
  signal in_sync           : std_logic := '0';

  signal switch_a          : std_logic_vector(c_dat_w - 1 downto 0);
  signal switch_b          : std_logic_vector(c_dat_w - 1 downto 0);
  signal switch_val        : std_logic;
  signal prev1_switch_a    : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev1_switch_b    : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev2_switch_a    : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev2_switch_b    : std_logic_vector(c_dat_w - 1 downto 0);

  signal mux_toggle        : std_logic := '0';
  signal mux_re            : std_logic_vector(c_dat_w - 1 downto 0) := (others => '0');
  signal mux_im            : std_logic_vector(c_dat_w - 1 downto 0) := (others => '0');
  signal mux_val           : std_logic := '0';

  signal unswitch_re       : std_logic_vector(c_dat_w - 1 downto 0);
  signal unswitch_im       : std_logic_vector(c_dat_w - 1 downto 0);
  signal unswitch_val      : std_logic := '0';
  signal prev1_unswitch_re : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev1_unswitch_im : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev2_unswitch_re : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev2_unswitch_im : std_logic_vector(c_dat_w - 1 downto 0);

  signal out_toggle        : std_logic := '0';
  signal out_a             : std_logic_vector(c_dat_w - 1 downto 0);
  signal out_b             : std_logic_vector(c_dat_w - 1 downto 0);
  signal out_val           : std_logic;
  signal out_sop           : std_logic := '0';
  signal out_eop           : std_logic := '0';
  signal out_sync          : std_logic := '0';

  signal dly_val           : std_logic_vector(0 to c_dly) := (others => '0');
  signal dly_a             : t_integer_arr(0 to c_dly) := (others => 0);
  signal dly_b             : t_integer_arr(0 to c_dly) := (others => 0);
  signal exp_val           : std_logic := '0';
  signal exp_a             : integer;
  signal exp_b             : integer;

  signal verify_en         : std_logic := '0';
begin
  clk <= not clk or tb_end after clk_period / 2;

  p_in_val : process
  begin
    rst <= '1';
    in_val <= '0';
    proc_common_wait_some_cycles(clk, 10);
    rst <= '0';
    proc_common_wait_some_cycles(clk, 10);
    for I in 0 to g_nof_sync - 1 loop
      for J in 0 to c_nof_block_per_sync_max - 1 loop
        for K in 0 to c_nof_clk_per_block - 1 loop
          if g_in_val_gaps and K = 0 then
            in_val <= '0';  -- insert a one cycle gap
            proc_common_wait_some_cycles(clk, 1);
          end if;
          in_val <= '1';
          proc_common_wait_some_cycles(clk, 1);
        end loop;
      end loop;
    end loop;
    proc_common_wait_some_cycles(clk, g_nof_clk_per_sync * g_nof_sync);
    tb_end <= '1';
    wait;
  end process;

  -- Create in strobes for debugging
  u_in_strobes : entity common_lib.common_create_strobes_from_valid
  generic map (
    g_pipeline          => false,
    g_nof_clk_per_sync  => g_nof_clk_per_sync,
    g_nof_clk_per_block => c_nof_clk_per_block
  )
  port map (
    rst       => rst,
    clk       => clk,
    in_val    => in_val,
    out_val   => OPEN,  -- out_val = in_val, because g_pipeline = FALSE
    out_sop   => in_sop,
    out_eop   => in_eop,
    out_sync  => in_sync
  );

  gen_increment_at_val : if g_increment_at_val = true generate
    in_a <= INCR_SVEC(in_a, 1) when rising_edge(clk) and in_val = '1';
    in_b <= INCR_SVEC(in_b, 1) when rising_edge(clk) and in_val = '1';
  end generate;

  gen_increment_at_eop : if g_increment_at_val = false generate
    in_a <= INCR_SVEC(in_a, 1) when rising_edge(clk) and in_eop = '1';
    in_b <= INCR_SVEC(in_b, 1) when rising_edge(clk) and in_eop = '1';
  end generate;

  u_fft_switch : entity work.fft_switch
  generic map (
    g_switch_en => g_switch_en,
    g_seed1     => c_switch_seed1,
    g_seed2     => c_switch_seed2,
    g_fft_sz_w  => g_fft_size_w,
    g_dat_w     => c_dat_w
  )
  port map (
    in_re      => in_a,
    in_im      => in_b,
    in_val     => in_val,
    out_re     => switch_a,
    out_im     => switch_b,
    out_val    => switch_val,
    clk        => clk,
    rst        => rst
  );

  -- Model A, B multiplexing part of FFT
  --                   0  1  2 ..  N-1
  --        switch_a: a0 a1 a2 .. aN-1
  --        switch_b: b0 b1 b2 .. bN-1
  --  prev1_switch_a:    a0 a1 ..      aN-1
  --  prev1_switch_b:    b0 b1 ..      bN-1
  --  prev2_switch_a:       a0 ..           aN-1
  --  prev2_switch_b:       b0 ..           bN-1
  --      mux_toggle:  0  1  0  1  0 ..  1    0
  --                      0  1  2  3 ..  N-2  N-1
  --          mux_re:    a0 b0 a2 b2 .. aN-2 bN-2
  --          mux_im:    a1 b1 a3 b3 .. aN-1 bN-1

  prev1_switch_a <=       switch_a when rising_edge(clk) and switch_val = '1';
  prev1_switch_b <=       switch_b when rising_edge(clk) and switch_val = '1';
  prev2_switch_a <= prev1_switch_a when rising_edge(clk) and switch_val = '1';
  prev2_switch_b <= prev1_switch_b when rising_edge(clk) and switch_val = '1';

  mux_toggle <= not mux_toggle when rising_edge(clk) and switch_val = '1';

  mux_re  <= prev1_switch_a when mux_toggle = '1' else prev2_switch_b;  -- a0, b0, ..
  mux_im  <=       switch_a when mux_toggle = '1' else prev1_switch_b;  -- a1, b1, ..
  mux_val <= switch_val when rising_edge(clk);

  u_fft_unswitch : entity work.fft_unswitch
  generic map (
    g_switch_en => g_switch_en,
    g_seed1     => c_switch_seed1,
    g_seed2     => c_switch_seed2,
    g_fft_sz_w  => g_fft_size_w,
    g_dat_w     => c_dat_w
  )
  port map (
    in_re      => mux_re,
    in_im      => mux_im,
    in_val     => mux_val,
    out_re     => unswitch_re,
    out_im     => unswitch_im,
    out_val    => unswitch_val,
    clk        => clk,
    rst        => rst
  );

  -- Demultiplex output to ease verification
  --                      0  1  2  3 ..  N-2  N-1
  --        unswitch_re: a0 b0 a2 b2 .. aN-2 bN-2
  --        unswitch_im: a1 b1 a3 b3 .. aN-1 bN-1
  --  prev1_unswitch_re:    a0 b0 a2 b2 .. aN-2 bN-2
  --  prev1_unswitch_im:    a1 b1 a3 b3 .. aN-1 bN-1
  --  prev2_unswitch_re:       a0 b0 a2 b2 .. aN-2 bN-2
  --  prev2_unswitch_im:       a1 b1 a3 b3 .. aN-1 bN-1
  --        out_toggle:   0  1  0  1  0
  --                         0  1 .. N-1
  --             out_a:     a0 a1 ..aN-1
  --             out_b:     b0 b1 ..bN-1

  prev1_unswitch_re <=       unswitch_re when rising_edge(clk) and unswitch_val = '1';
  prev1_unswitch_im <=       unswitch_im when rising_edge(clk) and unswitch_val = '1';
  prev2_unswitch_re <= prev1_unswitch_re when rising_edge(clk) and unswitch_val = '1';
  prev2_unswitch_im <= prev1_unswitch_im when rising_edge(clk) and unswitch_val = '1';

  out_toggle <= not out_toggle when rising_edge(clk) and unswitch_val = '1';

  out_a     <= prev1_unswitch_re when out_toggle = '1' else prev2_unswitch_im;  -- a0, a1, ..
  out_b     <=       unswitch_re when out_toggle = '1' else prev1_unswitch_im;  -- b0, b1, ..
  out_val   <= unswitch_val when rising_edge(clk);

  -- Create out strobes for debugging
  u_out_strobes : entity common_lib.common_create_strobes_from_valid
  generic map (
    g_pipeline          => false,
    g_nof_clk_per_sync  => g_nof_clk_per_sync,
    g_nof_clk_per_block => c_nof_clk_per_block
  )
  port map (
    rst       => rst,
    clk       => clk,
    in_val    => out_val,
    out_val   => OPEN,  -- out_val = in_val, because g_pipeline = FALSE
    out_sop   => out_sop,
    out_eop   => out_eop,
    out_sync  => out_sync
  );

  -- Account for pipeling in fft_switch, mux,  fft_unswitch and demux
  dly_val(0) <= in_val;
  dly_a(0) <= TO_SINT(in_a);
  dly_b(0) <= TO_SINT(in_b);
  dly_val(1 to c_dly) <= dly_val(0 to c_dly - 1) when rising_edge(clk);
  dly_a(1 to c_dly) <= dly_a(0 to c_dly - 1) when rising_edge(clk);
  dly_b(1 to c_dly) <= dly_b(0 to c_dly - 1) when rising_edge(clk);
  exp_val <= dly_val(c_dly);
  exp_a <= dly_a(c_dly);
  exp_b <= dly_b(c_dly);

  verify_en <= '1' when exp_val = '1';

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if verify_en = '1' then
        if exp_val = '1' then
          assert TO_SINT(out_a) = exp_a
            report "Wrong out_re"
            severity ERROR;
          assert TO_SINT(out_b) = exp_b
            report "Wrong out_im"
            severity ERROR;
        end if;
        assert out_val = exp_val
          report "Wrong out_val"
          severity ERROR;
      end if;
    end if;
  end process;
end tb;
