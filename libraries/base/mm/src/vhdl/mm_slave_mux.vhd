-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Combines an array of MM interfaces into a single MM interface.
-- Description:
--   Wraps common_mem_mux.vhd.
-- Remark:
--   No need for g_rd_latency pipelining, so pure combinatorial and no need
--   for clk. If necessary apply pipelining via mm_bus.vhd.
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mm_slave_mux is
  generic (
    g_broadcast   : boolean := false;  -- TRUE use port[0] to access all, else use separate ports
    g_nof_mosi    : positive := 256;  -- Number of slave memory interfaces in the array.
    g_mosi_addr_w : positive := 8  -- Address width per slave
  );
  port (
    mosi     : in  t_mem_mosi;
    miso     : out t_mem_miso;
    mosi_arr : out t_mem_mosi_arr(g_nof_mosi - 1 downto 0);
    miso_arr : in  t_mem_miso_arr(g_nof_mosi - 1 downto 0) := (others => c_mem_miso_rst)
  );
end mm_slave_mux;

architecture str of mm_slave_mux is
begin
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_broadcast   => g_broadcast,
    g_nof_mosi    => g_nof_mosi,
    g_mult_addr_w => g_mosi_addr_w,
    g_rd_latency  => 0
  )
  port map (
    clk      => '0',  -- only used when g_rd_latency > 0
    mosi     => mosi,
    miso     => miso,
    mosi_arr => mosi_arr,
    miso_arr => miso_arr
  );
end str;
