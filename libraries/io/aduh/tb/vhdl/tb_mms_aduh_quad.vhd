-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Usage:
-- > as 10
-- > run 20 us
-- > p_verify_res should report no errors

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.aduh_dd_pkg.all;

entity tb_mms_aduh_quad is
end tb_mms_aduh_quad;

architecture tb of tb_mms_aduh_quad is
  --TYPE t_c_aduh_dd_ai IS RECORD
  --  nof_sp      : NATURAL;          -- = 4
  --  nof_adu     : NATURAL;          -- = 2
  --  nof_ports   : NATURAL;          -- = 2
  --  port_w      : NATURAL;          -- = 8
  --  dd_factor   : NATURAL;          -- = 2
  --  rx_factor   : NATURAL;          -- = 2
  --  deskew      : t_c_aduh_delays;  -- = (0, 0, (OTHERS=>0), (OTHERS=>0), (OTHERS=>0), (OTHERS=>0))  -- clock: a, b, data: a, b, c, d
  --END RECORD;
  constant c_ai                   : t_c_aduh_dd_ai := c_aduh_dd_ai;  -- use defaults

  constant c_dp_factor            : natural := c_ai.rx_factor * c_ai.dd_factor;  -- = 4 = 2 * 2

  constant c_sample_period        : time := 1250 ps;  -- 800 MTps
  constant c_dp_clk_period        : time := c_sample_period * c_dp_factor;  -- 200 MHz
  constant c_mm_clk_period        : time := 20 ns;  -- 50 MHz

  constant c_aduh_verify_val_bi   : natural := 12;
  constant c_aduh_verify_res_hi   : natural := 8;
  constant c_aduh_verify_res_mask : natural := 2**(c_aduh_verify_res_hi + 1) - 1;

  constant c_dp_phs_clk_step     : time := c_dp_clk_period / 32;  -- the PLL can output clocks with phase shifts of 360/32 = 11.25 degrees
  constant c_dp_phs_clk_period   : natural := 32;  -- number of dp_clk periods per dp_phs_clk period
  constant c_nof_dp_phs_clk      : natural := 1;

  procedure proc_verify_bist_expect_ok(aduh_a_verify, aduh_b_verify, aduh_c_verify, aduh_d_verify : std_logic_vector) is
  begin
    assert aduh_a_verify(c_aduh_verify_val_bi) = '1'
      report "ADU-A bist did not run"
      severity ERROR;
    assert aduh_b_verify(c_aduh_verify_val_bi) = '1'
      report "ADU-B bist did not run"
      severity ERROR;
    assert aduh_c_verify(c_aduh_verify_val_bi) = '1'
      report "ADU-C bist did not run"
      severity ERROR;
    assert aduh_d_verify(c_aduh_verify_val_bi) = '1'
      report "ADU-D bist did not run"
      severity ERROR;
    assert TO_UINT(aduh_a_verify(c_aduh_verify_res_hi downto 0)) = 0
      report "ADU-A bist went wrong"
      severity ERROR;
    assert TO_UINT(aduh_b_verify(c_aduh_verify_res_hi downto 0)) = 0
      report "ADU-B bist went wrong"
      severity ERROR;
    assert TO_UINT(aduh_c_verify(c_aduh_verify_res_hi downto 0)) = 0
      report "ADU-C bist went wrong"
      severity ERROR;
    assert TO_UINT(aduh_d_verify(c_aduh_verify_res_hi downto 0)) = 0
      report "ADU-D bist went wrong"
      severity ERROR;
  end;

  procedure proc_verify_bist_expect_errors(aduh_a_verify, aduh_b_verify, aduh_c_verify, aduh_d_verify : std_logic_vector) is
  begin
    assert aduh_a_verify(c_aduh_verify_val_bi) = '1'
      report "ADU-A bist did not run"
      severity ERROR;
    assert aduh_b_verify(c_aduh_verify_val_bi) = '1'
      report "ADU-B bist did not run"
      severity ERROR;
    assert aduh_c_verify(c_aduh_verify_val_bi) = '1'
      report "ADU-C bist did not run"
      severity ERROR;
    assert aduh_d_verify(c_aduh_verify_val_bi) = '1'
      report "ADU-D bist did not run"
      severity ERROR;
    assert TO_UINT(aduh_a_verify(c_aduh_verify_res_hi downto 0)) = c_aduh_verify_res_mask
      report "ADU-A bist error detection went wrong"
      severity ERROR;
    assert TO_UINT(aduh_b_verify(c_aduh_verify_res_hi downto 0)) = c_aduh_verify_res_mask
      report "ADU-B bist error detection went wrong"
      severity ERROR;
    assert TO_UINT(aduh_c_verify(c_aduh_verify_res_hi downto 0)) = c_aduh_verify_res_mask
      report "ADU-C bist error detection went wrong"
      severity ERROR;
    assert TO_UINT(aduh_d_verify(c_aduh_verify_res_hi downto 0)) = c_aduh_verify_res_mask
      report "ADU-D bist error detection went wrong"
      severity ERROR;
  end;

  signal tb_end              : std_logic := '0';

  -- Analogue
  signal SCLK                : std_logic := '1';  -- central sample clock = 800 MHz
  signal DCLK_AB             : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_CD             : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DIG_A               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_B               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_C               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_D               : std_logic_vector(c_ai.port_w - 1 downto 0);

  -- Digital streaming
  signal dp_clk              : std_logic := '1';  -- digital data path clock = 200 MHz (deser factor 4);
  signal dp_rst              : std_logic;
  signal dp_phs_cnt          : natural := 0;
  signal dp_phs_clk          : std_logic := '1';
  signal dp_phs_clk_vec      : std_logic_vector(c_nof_dp_phs_clk - 1 downto 0);

  signal aduh_sosi_arr       : t_dp_sosi_arr(0 to c_ai.nof_sp - 1);

  -- MM Interface
  signal mm_clk              : std_logic := '1';  -- MM control clock = 50 MHz
  signal mm_rst              : std_logic;

  -- MM aduh quad register
  signal aduh_ab_locked      : std_logic_vector(c_word_w - 1 downto 0);
  signal aduh_cd_locked      : std_logic_vector(c_word_w - 1 downto 0);
  signal aduh_a_verify       : std_logic_vector(c_word_w - 1 downto 0);
  signal aduh_b_verify       : std_logic_vector(c_word_w - 1 downto 0);
  signal aduh_c_verify       : std_logic_vector(c_word_w - 1 downto 0);
  signal aduh_d_verify       : std_logic_vector(c_word_w - 1 downto 0);

  signal reg_mosi            : t_mem_mosi := c_mem_mosi_rst;  -- ADUH locked status and pattern verify for two half ADUs so 4 ADC inputs A, B, C and D
  signal reg_miso            : t_mem_miso;

  signal test_pattern_en     : std_logic;
  signal state               : string(1 to 8) := "INIT    ";
begin
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;

  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;

  -- Create DP phase reference clock
  p_dp_phs_clk : process(dp_clk)
  begin
    if c_dp_phs_clk_period = 1 then
      dp_phs_clk <= dp_clk;
    elsif rising_edge(dp_clk) then
      if dp_phs_cnt mod (c_dp_phs_clk_period / 2) = 0 then
        dp_phs_clk <= not dp_phs_clk;
      end if;
      if dp_phs_cnt = c_dp_phs_clk_period - 1 then
        dp_phs_cnt <= 0;
      else
        dp_phs_cnt <= dp_phs_cnt + 1;
      end if;
    end if;
  end process;

  gen_dp_phs_clk_vec : for I in c_nof_dp_phs_clk - 1 downto 0 generate
    dp_phs_clk_vec(I) <= transport dp_phs_clk after I * c_dp_phs_clk_step;
  end generate;

  ----------------------------------------------------------------------------
  -- Stimuli for MM REG slave port
  ----------------------------------------------------------------------------

  p_mm_reg_stimuli : process
  begin
    reg_mosi         <= c_mem_mosi_rst;
    test_pattern_en  <= '1';

    ----------------------------------------------------------------------------
    -- Initialisations
    ----------------------------------------------------------------------------

    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Read aduh locked status for AB, CD
    proc_common_wait_some_cycles(mm_clk, 20);
    state <= "LOCKED  ";
    proc_mem_mm_bus_rd(0, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_ab_locked <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(1, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_cd_locked <= reg_miso.rddata(31 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert TO_UINT(aduh_ab_locked) = 1
      report "ADU-AB in not locked"
      severity ERROR;
    assert TO_UINT(aduh_cd_locked) = 1
      report "ADU-CD in not locked"
      severity ERROR;

    ----------------------------------------------------------------------------
    -- Expect aduh BIST OK for A, B, C, D
    ----------------------------------------------------------------------------
    proc_common_wait_some_cycles(mm_clk, 10);
    state <= "BIST_OK ";
    proc_mem_mm_bus_rd(2, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_a_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(3, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_b_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(4, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_c_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(5, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_d_verify <= reg_miso.rddata(31 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    proc_verify_bist_expect_ok(aduh_a_verify, aduh_b_verify, aduh_c_verify, aduh_d_verify);

    test_pattern_en  <= '0';
    -- Read aduh locked status for AB, CD
    proc_common_wait_some_cycles(mm_clk, 10);
    proc_mem_mm_bus_rd(0, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_ab_locked <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(1, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_cd_locked <= reg_miso.rddata(31 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    assert TO_UINT(aduh_ab_locked) = 3
      report "ADU-AB in not locked stable"
      severity ERROR;
    assert TO_UINT(aduh_cd_locked) = 3
      report "ADU-CD in not locked stable"
      severity ERROR;

    ----------------------------------------------------------------------------
    -- Expect aduh BIST errors for A, B, C, D
    ----------------------------------------------------------------------------
    proc_common_wait_some_cycles(mm_clk, 10);
    state <= "BIST_ERR";
    proc_mem_mm_bus_rd(2, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_a_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(3, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_b_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(4, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_c_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(5, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_d_verify <= reg_miso.rddata(31 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    proc_verify_bist_expect_errors(aduh_a_verify, aduh_b_verify, aduh_c_verify, aduh_d_verify);

    ----------------------------------------------------------------------------
    -- Expect aduh BIST errors for A, B, C, D
    ----------------------------------------------------------------------------
    proc_common_wait_some_cycles(mm_clk, 10);
    test_pattern_en  <= '1';
    proc_common_wait_some_cycles(mm_clk, 100);
    state <= "BIST_ERR";
    proc_mem_mm_bus_rd(2, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_a_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(3, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_b_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(4, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_c_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(5, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_d_verify <= reg_miso.rddata(31 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    proc_verify_bist_expect_errors(aduh_a_verify, aduh_b_verify, aduh_c_verify, aduh_d_verify);

    ----------------------------------------------------------------------------
    -- Expect aduh BIST OK for A, B, C, D
    ----------------------------------------------------------------------------
    proc_common_wait_some_cycles(mm_clk, 10);
    state <= "BIST_OK ";
    proc_mem_mm_bus_rd(2, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_a_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(3, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_b_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(4, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_c_verify <= reg_miso.rddata(31 downto 0);
    proc_mem_mm_bus_rd(5, mm_clk, reg_mosi); proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk); aduh_d_verify <= reg_miso.rddata(31 downto 0);
    proc_common_wait_some_cycles(mm_clk, 1);
    proc_verify_bist_expect_ok(aduh_a_verify, aduh_b_verify, aduh_c_verify, aduh_d_verify);

    proc_common_wait_some_cycles(mm_clk, 10);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- ADUH_QUAD for ADC port [ABCD]
  ------------------------------------------------------------------------------

  u_dut : entity work.mms_aduh_quad
  generic map (
    -- General
    g_cross_clock_domain => true,
    -- ADC Interface
    g_ai                 => c_aduh_dd_ai
  )
  port map (
    -- ADC Interface
    -- . ADU_AB
    ADC_BI_A          => DIG_A,
    ADC_BI_B          => DIG_B,
    ADC_BI_A_CLK      => DCLK_AB,
    ADC_BI_A_CLK_RST  => OPEN,

    -- . ADU_CD
    ADC_BI_C          => DIG_A,
    ADC_BI_D          => DIG_D,
    ADC_BI_D_CLK      => DCLK_CD,
    ADC_BI_D_CLK_RST  => OPEN,

    -- MM clock domain
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,

    reg_mosi          => reg_mosi,
    reg_miso          => reg_miso,

    -- Streaming clock domain
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
    dp_phs_clk_vec    => dp_phs_clk_vec,

    -- . data
    aduh_sosi_arr     => aduh_sosi_arr
  );

  -- Scope in Wave Window for debug purpose
  u_scope : entity work.aduh_quad_scope
  generic map (
    -- General
    g_sim  => true,
    -- ADC Interface
    g_ai   => c_aduh_dd_ai
  )
  port map (
    -- Digital processing clock
    DCLK         => dp_clk,

    -- Streaming samples (can be from ADU or from internal WG)
    sp_sosi_arr  => aduh_sosi_arr
  );

  ------------------------------------------------------------------------------
  -- ADU-[AB, CD] ADC ports both in test pattern mode
  ------------------------------------------------------------------------------

  -- Same sample clock for all ADC
  SCLK <= (not SCLK) or tb_end after c_sample_period / 2;

  -- National ADC
  u_adc_ab : entity work.adu_half
  port map (
    CLK             => SCLK,
    DCLK            => DCLK_AB,
    DCLK_RST        => '0',
    DI              => DIG_A,
    DQ              => DIG_B,

    test_pattern_en => test_pattern_en
  );

  -- National ADC
  u_adc_cd : entity work.adu_half
  port map (
    CLK             => SCLK,
    DCLK            => DCLK_CD,
    DCLK_RST        => '0',
    DI              => DIG_C,
    DQ              => DIG_D,

    test_pattern_en => test_pattern_en
  );
end tb;
