-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

-- Purpose: Provide timing pulses for interval 1 us, 1 ms and 1 s

entity common_pulser_us_ms_s is
  generic (
    g_pulse_us    : natural := 125;  -- nof clk cycles to get us period
    g_pulse_ms    : natural := 1000;  -- nof pulse_us pulses to get ms period
    g_pulse_s     : natural := 1000  -- nof pulse_ms pulses to get s period
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    sync         : in  std_logic := '0';
    pulse_us     : out std_logic;  -- pulses after every g_pulse_us                      clock cycles
    pulse_ms     : out std_logic;  -- pulses after every g_pulse_us*g_pulse_ms           clock cycles
    pulse_s      : out std_logic  -- pulses after every g_pulse_us*g_pulse_ms*g_pulse_s clock cycles
  );
end common_pulser_us_ms_s;

architecture str of common_pulser_us_ms_s is
  signal pulse_us_pp     : std_logic;  -- register to align with pulse_ms
  signal pulse_us_p      : std_logic;  -- register to align with pulse_s
  signal pulse_us_reg    : std_logic;  -- output register
  signal i_pulse_us      : std_logic;

  signal pulse_ms_p      : std_logic;  -- register to align with pulse_s
  signal pulse_ms_reg    : std_logic;  -- output register
  signal i_pulse_ms      : std_logic;

  signal pulse_s_reg     : std_logic;  -- output register
  signal i_pulse_s       : std_logic;
begin
  -- register output pulses to ease timing closure
  pulse_us  <= '0' when rst = '1' else i_pulse_us when rising_edge(clk);
  pulse_ms  <= '0' when rst = '1' else i_pulse_ms when rising_edge(clk);
  pulse_s   <= '0' when rst = '1' else i_pulse_s  when rising_edge(clk);

  p_clk : process(clk)
  begin
    if rising_edge(clk) then
      pulse_us_p   <= pulse_us_pp;
      pulse_us_reg <= pulse_us_p;
      pulse_ms_reg <= pulse_ms_p;
      i_pulse_us   <= pulse_us_reg;
      i_pulse_ms   <= pulse_ms_reg;
      i_pulse_s    <= pulse_s_reg;
    end if;
  end process;

  u_common_pulser_us : entity common_lib.common_pulser
  generic map (
    g_pulse_period => g_pulse_us,
    g_pulse_phase  => g_pulse_us - 1
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => '1',
    pulse_en       => '1',
    pulse_clr      => sync,
    pulse_out      => pulse_us_pp
  );

  u_common_pulser_ms : entity common_lib.common_pulser
  generic map (
    g_pulse_period => g_pulse_ms,
    g_pulse_phase  => g_pulse_ms - 1
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => '1',
    pulse_en       => pulse_us_pp,
    pulse_clr      => sync,
    pulse_out      => pulse_ms_p
  );

  u_common_pulser_s : entity common_lib.common_pulser
  generic map (
    g_pulse_period => g_pulse_s,
    g_pulse_phase  => g_pulse_s - 1
  )
  port map (
    rst            => rst,
    clk            => clk,
    clken          => '1',
    pulse_en       => pulse_ms_p,
    pulse_clr      => sync,
    pulse_out      => pulse_s_reg
  );
end str;
