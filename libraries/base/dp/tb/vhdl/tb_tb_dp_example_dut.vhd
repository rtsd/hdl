-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_example_dut
-- Description:
-- Usage:
-- > as 6
-- > run -all

entity tb_tb_dp_example_dut is
end tb_tb_dp_example_dut;

architecture tb of tb_tb_dp_example_dut is
  constant c_nof_repeat  : natural := 10;

  constant c_flow        : t_dp_flow_control_enum_arr := c_dp_flow_control_enum_arr;
  constant c_bool        : t_nat_boolean_arr := c_nat_boolean_arr;

  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- general
  -- g_flow_control_stimuli   : t_dp_flow_control_enum := e_pulse;   -- always active, random or pulse flow control
  -- g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
  -- -- specific
  -- g_no_dut                 : BOOLEAN:= TRUE;
  -- g_in_dat_w               : NATURAL := 32;
  -- g_nof_repeat             : NATURAL := 5;
  -- g_pkt_len                : NATURAL := 16;
  -- g_pkt_gap                : NATURAL := 4

  g_flow_control_stimuli : for I in 0 to 2 generate  -- 0 = e_active, 1 = e_random, 2 = e_pulse
    g_flow_control_verify : for J in 0 to 2 generate  -- 0 = e_active, 1 = e_random, 2 = e_pulse
      gen_bool_dut : for K in 0 to 1 generate  -- 0 = TRUE, 1= FALSE
        u_dut : entity work.tb_dp_example_dut generic map (c_flow(I), c_flow(J), c_bool(K), 16, c_nof_repeat, 17, 4);
      end generate;
    end generate;
  end generate;
end tb;
