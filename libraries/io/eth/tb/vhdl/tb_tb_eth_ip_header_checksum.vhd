-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: R. vd Walle
-- Purpose: Regression multi tb for eth_ip_header_checksum
-- Description:
-- Usage:
-- > as 4
-- > run -all

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;  -- for t_dp_flow_control_enum

entity tb_tb_eth_ip_header_checksum is
end tb_tb_eth_ip_header_checksum;

architecture tb of tb_tb_eth_ip_header_checksum is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- general
  -- g_flow_control_stimuli   : t_dp_flow_control_enum := e_pulse;  -- always e_active, e_random or e_pulse flow control
  -- g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
  -- g_print_en               : BOOLEAN := TRUE;
  -- -- specific
  -- g_data_w                 : NATURAL := 64;
  -- g_symbol_w               : NATURAL := 16;
  -- g_empty                  : NATURAL := 6;   -- number of empty symbols in header when g_symbol_w < g_data_w, must be < c_nof_symbols_per_data
  -- g_pkt_len                : NATURAL := 240;
  -- g_pkt_gap                : NATURAL := 16

  u_pls_act_data_w_256                : entity work.tb_eth_ip_header_checksum generic map (e_pulse,  e_active, false, 256, 64, 0, 240, 16);
  u_act_act_data_w_512_no_gap         : entity work.tb_eth_ip_header_checksum generic map (e_active, e_active, false, 512,  8, 0, 240,  0);
  u_pls_act_data_w_64_no_gap          : entity work.tb_eth_ip_header_checksum generic map (e_pulse,  e_active, false, 64,  64, 0, 240,  0);
  u_rnd_act_data_w_32                 : entity work.tb_eth_ip_header_checksum generic map (e_random, e_active, false, 32,   8, 0, 240, 16);
  u_rnd_act_data_w_8                  : entity work.tb_eth_ip_header_checksum generic map (e_random, e_active, false,  8,   8, 0, 240, 16);
  --u_act_rnd_data_w                    : ENTITY work.tb_eth_ip_header_checksum GENERIC MAP (e_active, e_random, FALSE, 64, 64, 0, 240, 16);  -- dp_offload_rx requires e_active
  u_rnd_act_data_64_symbol_8_empty_1  : entity work.tb_eth_ip_header_checksum generic map (e_random, e_active, false, 64,   8, 1, 240, 16);
  u_rnd_act_data_64_symbol_8_empty_6  : entity work.tb_eth_ip_header_checksum generic map (e_random, e_active, false, 64,   8, 6, 240, 16);
  u_rnd_act_data_64_symbol_16         : entity work.tb_eth_ip_header_checksum generic map (e_random, e_active, false, 64,  16, 1, 240, 16);
  u_rnd_act_data_64_symbol_32         : entity work.tb_eth_ip_header_checksum generic map (e_random, e_active, false, 64,  32, 1, 240, 16);
  u_rnd_act_data_32_symbol_8          : entity work.tb_eth_ip_header_checksum generic map (e_random, e_active, false, 32,   8, 1, 240, 16);
  u_rnd_act_data_32_symbol_16         : entity work.tb_eth_ip_header_checksum generic map (e_random, e_active, false, 32,  16, 1, 240, 16);
end tb;
