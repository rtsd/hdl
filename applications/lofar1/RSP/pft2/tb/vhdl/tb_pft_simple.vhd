library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library pft2_lib, common_lib;
use pft2_lib.pft_pkg.all;
use common_lib.common_pkg.all;

entity tb_pft is
end tb_pft;

architecture tb of tb_pft is
  constant clk_period   : time      := 10 ns;
  constant rst_period   : time      := 3 * clk_period;
  constant g_in_dat_w   : natural   := 18;
  constant g_out_dat_w  : natural   := 18;
  constant g_fft_size   : natural   := 1024;
  constant g_fft_size_w : natural   := 10;
  constant g_pps_ps     : natural   := 2048;  -- 8192;

  signal cnt            : std_logic_vector(32 downto 0) := (others => '0');
  signal nxt_cnt        : std_logic_vector(32 downto 0) := (others => '0');

  signal dc             : integer := 0;

  signal in_x           : std_logic_vector(g_in_dat_w - 1 downto 0) := (others => '0');
  signal in_y           : std_logic_vector(g_in_dat_w - 1 downto 0) := (others => '0');
  signal in_val         : std_logic := '0';
  signal in_sync        : std_logic := '0';

  signal switch_en      : std_logic := '0';

  signal out_re         : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal out_im         : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal out_val        : std_logic;
  signal out_sync       : std_logic;

  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';
begin
  regs : process(rst, clk)
  begin
    if rst = '1' then
      cnt   <= (others => '0');
    elsif rising_edge(clk) then
      cnt   <= nxt_cnt;
   end if;
  end process;

  nxt_cnt   <= std_logic_vector(unsigned(cnt) + 1) when in_sync = '0' else (others => '0');

  pft : entity work.pft
  generic map (
    g_fft_size_w   => g_fft_size_w,
    g_in_dat_w     => g_in_dat_w,
    g_out_dat_w    => g_out_dat_w,
    g_mode         => PFT_MODE_REAL2
  )
  port map (
    in_re          => in_x,
    in_im          => in_y,
    in_val         => in_val,
    in_sync        => in_sync,
    switch_en      => switch_en,
    out_re         => out_re,
    out_im         => out_im,
    out_val        => out_val,
    out_sync       => out_sync,
    clk            => clk,
    rst            => rst
  );

  clk  <= not(clk) after clk_period / 2;
  rst  <= '0' after rst_period;

  switch_en <= '0';

--   in_sync <= '1' WHEN UNSIGNED(cnt)=g_pps_ps-1 ELSE '0';
--   in_val  <= '1' WHEN UNSIGNED(cnt)=732;

  input_ctrl : process
  begin
    in_sync <= '0';
    in_val  <= '0';
    for I in 1 to 2 loop
      -- sync
      wait until unsigned(cnt) = g_pps_ps - 10;
      in_sync <= '1';
      wait for 10 * clk_period;
      in_sync <= '0';

      -- val
      wait until unsigned(cnt) = 1024;
--      WAIT UNTIL UNSIGNED(cnt)=731;
      in_val  <= '1';

      for J in 1 to 10 loop
        -- sync
        wait until unsigned(cnt) = g_pps_ps - 1;
        in_sync <= '1';
        wait for clk_period;
        in_sync <= '0';
      end loop;

      -- val
      wait until unsigned(cnt) = 100;
      in_val  <= '0';
    end loop;
    wait;
  end process;

--   -----------------------------------------------------------------------------
--   --
--   -- X = Y is sliding impulse
--   --
--   -----------------------------------------------------------------------------
--   in_gen: PROCESS
--   BEGIN
--     FOR I IN 0 TO g_fft_size-1 LOOP         -- Slide impulse
--       FOR J IN 1 TO 1 LOOP                  -- Repeat impulse
--         IF in_val='1' THEN
--           WAIT UNTIL UNSIGNED(nxt_cnt(g_fft_size_w-1 DOWNTO 0))=I;
--           in_x <= STD_LOGIC_VECTOR(TO_SIGNED(131000,g_in_dat_w));
--           WAIT FOR clk_period;
--           in_x <= STD_LOGIC_VECTOR(TO_SIGNED(0,g_in_dat_w));
--           WAIT FOR clk_period;
--         ELSE
--           WAIT UNTIL in_val='1';
--         END IF;
--       END LOOP;
--     END LOOP;
--   END PROCESS;
--   in_y <= in_x;

  -----------------------------------------------------------------------------
  --
  -- X = sinus Fs/2 and Y = DC
  --
  -----------------------------------------------------------------------------
  in_gen: process (clk, rst)
  begin
    if rst = '1' then
      in_x <= std_logic_vector(to_signed(300, g_in_dat_w));
      in_y <= std_logic_vector(to_signed(250, g_in_dat_w));
    elsif rising_edge(clk) then
      if cnt(0) = '1' then
        if in_val = '1' then
           in_x <= std_logic_vector(-signed(in_x));
           in_y <= std_logic_vector(signed(in_y));
        end if;
      end if;
    end if;
  end process;

--   -----------------------------------------------------------------------------
--   --
--   -- X, Y is impulse 0, impulse 1 or DC
--   --
--   -----------------------------------------------------------------------------
--   --dc <= 0;
--   dc <= 300;
--   in_gen: PROCESS (clk,rst)
--   BEGIN
--     IF rst='1' THEN
--       in_x <= STD_LOGIC_VECTOR(TO_SIGNED(0,g_in_dat_w));
--       in_y <= STD_LOGIC_VECTOR(TO_SIGNED(dc,g_in_dat_w));
--     ELSIF rising_edge(clk) THEN
--       in_x <= STD_LOGIC_VECTOR(TO_SIGNED(0,g_in_dat_w));
--       in_y <= STD_LOGIC_VECTOR(TO_SIGNED(dc,g_in_dat_w));
--       IF UNSIGNED(nxt_cnt(g_fft_size_w-1 DOWNTO 0))=0 THEN
--         in_x <= STD_LOGIC_VECTOR(TO_SIGNED(0,g_in_dat_w));           -- DC 0
--         in_y <= STD_LOGIC_VECTOR(TO_SIGNED(dc,g_in_dat_w));           -- DC
--         in_x <= STD_LOGIC_VECTOR(TO_SIGNED(131000,g_in_dat_w));      -- impulse 0
-- --         in_y <= STD_LOGIC_VECTOR(TO_SIGNED(131000,g_in_dat_w));      -- impulse 0
--       END IF;
--       IF UNSIGNED(nxt_cnt(g_fft_size_w-1 DOWNTO 0))=1 THEN
--         in_x <= STD_LOGIC_VECTOR(TO_SIGNED(0,g_in_dat_w));           -- DC 0
--         in_y <= STD_LOGIC_VECTOR(TO_SIGNED(dc,g_in_dat_w));           -- DC
-- --         in_x <= STD_LOGIC_VECTOR(TO_SIGNED(131000,g_in_dat_w));      -- impulse 1
-- --         in_y <= STD_LOGIC_VECTOR(TO_SIGNED(131000,g_in_dat_w));      -- impulse 1
--       END IF;
--     END IF;
--   END PROCESS;
end tb;
