-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
----------------------------------------------------------------------------------
-- Purpose:                                                                     --
--   Can be used to calculate and insert the checksum for the IP header         --
--   in a 10GbE package, if the correct longwords are provided.                 --
-- Description:                                                                 --
--   Determine the 16 bit 1-complement checksum according to IPv4 for the valid --
--   words between snk_in.sop and the last ip header field.                     --
--   After calculation, the checksum is inserted in the outgoing stream         --
----------------------------------------------------------------------------------

entity tr_10GbE_ip_checksum is
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;

    src_out       : out t_dp_sosi;
    snk_in        : in  t_dp_sosi;

    src_in        : in  t_dp_siso;
    snk_out       : out t_dp_siso
  );
end tr_10GbE_ip_checksum;

architecture rtl of tr_10GbE_ip_checksum is
  constant c_cin_w          : natural := 4;  -- bit width of carry
  constant c_pipeline_delay : natural := 2;

  signal sum                 : unsigned(c_halfword_w + c_cin_w - 1 downto 0) := (others => '0');
  signal checksum            : std_logic_vector(c_halfword_w - 1 downto 0);
  signal cnt_clr, cnt_p_clr  : std_logic;
  signal cnt_en, cnt_p_en    : std_logic;
  signal count, count_p      : std_logic_vector(31 downto 0);
  signal dp_pipeline_src_out : t_dp_sosi;
begin
  -------------------------------------------------
  -- process to calculate the ip_header_checksum --
  -------------------------------------------------
  p_calc_chksum : process(clk, rst)
  begin
    if rst = '1' then
      sum <= (others => '0');

    elsif rising_edge(clk) then
      if cnt_clr = '1' then
        sum <= (others => '0');

      elsif cnt_en = '1' then
        case TO_UINT(count) is
          when 0 =>  -- 0 is the cycle after the sop due to the common_counter latency
            sum <= sum + unsigned(snk_in.data(c_halfword_w - 1 downto 0));  -- ip_version, ip_header_length, ip_services
          when 1 =>
            sum <= sum + unsigned(snk_in.data(c_halfword_w * 4 - 1 downto c_halfword_w * 3))  -- ip_total_length
                       + unsigned(snk_in.data(c_halfword_w * 3 - 1 downto c_halfword_w * 2))  -- ip_identification
                       + unsigned(snk_in.data(c_halfword_w * 2 - 1 downto c_halfword_w))  -- ip_flags, ip_fragment_offset
                       + unsigned(snk_in.data(c_halfword_w   - 1 downto 0));  -- ip_time_to_live, ip_protocol
          when 2 =>  -- skip ip_header_checksum
            sum <= sum + unsigned(snk_in.data(c_halfword_w * 3 - 1 downto c_halfword_w * 2))  -- ip_src_addr(1/2)
                       + unsigned(snk_in.data(c_halfword_w * 2 - 1 downto c_halfword_w))  -- ip_src_addr(2/2)
                       + unsigned(snk_in.data(c_halfword_w   - 1 downto 0));  -- ip_dst_addr(1/2)
          when 3 =>
            sum <= sum + unsigned(snk_in.data(c_halfword_w * 4 - 1 downto c_halfword_w * 3));  -- ip_dst_addr(2/2)

          when others =>
        end case;
      end if;
    end if;
  end process;

  ---------------------------------------------------
  -- process to insert checksum in outgoing stream --
  ---------------------------------------------------
  checksum <= not(std_logic_vector(sum(c_halfword_w - 1 downto 0) + sum(sum'high downto c_halfword_w)));  -- checksum = inverted (sum + carry)
  p_insert_chksum : process(dp_pipeline_src_out, checksum, count_p)
  begin
    src_out <= dp_pipeline_src_out;
    if TO_UINT(count_p) = 2 then
      src_out.data(c_halfword_w * 4 - 1 downto c_halfword_w * 3) <= checksum;
    end if;
  end process;

  ------------------------------------------------------------------------------------------
  -- using common_counter to keep track of the word alignment during checksum calculation --
  ------------------------------------------------------------------------------------------
  cnt_en <= snk_in.valid;  -- only count when valid
  cnt_clr <= snk_in.sop;  -- restart counter on sop

  u_calc_counter : entity common_lib.common_counter
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => cnt_clr,
    cnt_en  => cnt_en,
    count   => count
  );
  ----------------------------------------------------------------------------------------
  -- using common_counter to keep track of the word alignment during checksum insertion --
  ----------------------------------------------------------------------------------------
  cnt_p_en <= dp_pipeline_src_out.valid;  -- only count when valid
  cnt_p_clr <= dp_pipeline_src_out.sop;  -- restart counter on sop

  u_pipe_counter : entity common_lib.common_counter
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => cnt_p_clr,
    cnt_en  => cnt_p_en,
    count   => count_p
  );

  -------------------------------------------------------------------------------
  -- using dp_pipeline to make room for the checksum calculation and insertion --
  -------------------------------------------------------------------------------
  u_dp_pipeline : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline   => c_pipeline_delay
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => snk_out,
    snk_in       => snk_in,
    -- ST source
    src_in       => src_in,
    src_out      => dp_pipeline_src_out
  );
end rtl;
