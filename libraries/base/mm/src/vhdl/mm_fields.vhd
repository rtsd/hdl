-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Define fields in an SLV that can be read/written via an MM interface.
-- Description:
-- . This is basically a wrapper around common_reg_r_w_dc, but with the
--   addition of a generic field description array and package functions
--   to ease the definition and assignment of individual fields within
--   the i/o SLVs.
-- . Each field defined in g_field_arr will get its own 32-bit MM register(s)
--   based on its defined length:
--   . <32 bits = one dedicated 32-bit register for that field
--   . >32 bits = multiple dedicated 32-bit registers for that field
-- . The register mode can be "RO" for input from slv_in (e.g. status) or "RW"
--   for output via slv_out (e.g. control). Other modes are not supported.
--   Hence the length of the reg_slv_* signals is equal to slv_in'LENGTH +
--   slv_out'LENGTH.
-- . The figure below shows how the following example field array would be mapped.
--
--   c_my_field_arr:= (( "my_field_2", "RW",  2 ),
--                     ( "my_field_1", "RO",  2 ),
--                     ( "my_field_0", "RO",  1 ));
--
--   ----------------------------------------------------------------------------------------------------------------
--   | slv_in             reg_slv_in_arr   reg_slv_in     common_reg_r_w     reg_slv_out                     slv_out|
--   |                                                                                                              |
--   |                            __          __          ______________          __                                |
--   |                         w0|f0|      w0|f0|        |0             |      w0|  |                               |
--   |                           |  | =====> |  | =====> |      RO      | =====> |  | =====>                        |
--   |                           |  |        |  |        |              |        |  |                               |
--   |   __                      |--|        |--|        |--------------|        |--|                               |
--   |  |f0|                   w1|f1|      w1|f1|        |1             |      w1|  |                          __   |
--   |  |f1| ==field_map_in==>   |f1| =====> |f1| =====> |      RO      | =====> |  | =====> field_map_out==> |f2|  |
--   |  |f1|                     |  |        |  |        |              |        |  |                         |f2|  |
--   |                           |--|        |--|        |--------------|        |--|                               |
--   |                         w2|  |      w2|f2|        |2             |      w2|f2|                               |
--   |                           |  |  /===> |f2| =====> |      RW      | =====> |f2| ==+==>                        |
--   |                           |__|  |     |__|        |______________|        |__|   |                           |
--   |                                 |                                                |                           |
--   |                                 \================================================/                           |
--   |                                                                                                              |
--   ----------------------------------------------------------------------------------------------------------------
--   . slv_in  = 3 bits wide
--   . slv_out = 2 bits wide (= my_field_2 which is looped back to reg_slv_in because it is defined "RW")
--   . reg_reg_slv_in_arr, reg_slv_in, reg_slv_out = 3*c_word_w bits wide
-- Remarks:

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;

entity mm_fields is
  generic (
    g_cross_clock_domain : boolean := true;
    g_use_slv_in_val     : boolean := true;  -- use TRUE when slv_in_val is used, use FALSE to save logic when always slv_in_val='1'
    g_field_arr          : t_common_field_arr
  );
  port (
    mm_rst     : in  std_logic;
    mm_clk     : in  std_logic;

    mm_mosi    : in  t_mem_mosi;
    mm_miso    : out t_mem_miso;

    slv_rst    : in  std_logic;
    slv_clk    : in  std_logic;

    --fields in these SLVs are defined by g_field_arr
    slv_in     : in  std_logic_vector(field_slv_in_len( g_field_arr) - 1 downto 0) := (others => '0');  -- slv of all "RO" fields in g_field_arr
    slv_in_val : in  std_logic := '0';  -- strobe to signal that slv_in is valid and needs to be captured

    slv_out    : out std_logic_vector(field_slv_out_len(g_field_arr) - 1 downto 0)  -- slv of all "RW" fields in g_field_arr
  );
end mm_fields;

architecture str of mm_fields is
  constant c_reg_nof_words : natural := field_nof_words(g_field_arr, c_word_w);

  constant c_reg           : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(c_reg_nof_words),
                                         dat_w    => c_word_w,
                                         nof_dat  => c_reg_nof_words,
                                         init_sl  => '0');

  constant c_slv_out_defaults : std_logic_vector(field_slv_out_len(g_field_arr) - 1 downto 0) := field_map_defaults(g_field_arr);
  -- Map the default values onto c_init_reg
  constant c_init_reg : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := RESIZE_UVEC(field_map_in(g_field_arr, c_slv_out_defaults, c_reg.dat_w, "RW"), c_mem_reg_init_w);

  signal slv_in_arr         : std_logic_vector(c_reg.dat_w * c_reg.nof_dat - 1 downto 0);
  signal reg_slv_in_arr     : std_logic_vector(c_reg.dat_w * c_reg.nof_dat - 1 downto 0);
  signal nxt_reg_slv_in_arr : std_logic_vector(c_reg.dat_w * c_reg.nof_dat - 1 downto 0);

  signal reg_slv_in         : std_logic_vector(c_reg.dat_w * c_reg.nof_dat - 1 downto 0);
  signal reg_slv_out        : std_logic_vector(c_reg.dat_w * c_reg.nof_dat - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- reg_slv_out is persistent (always valid) while slv_in is not. Register
  -- slv_in_arr so reg_slv_in is persistent also.
  -----------------------------------------------------------------------------
  gen_capture_input : if g_use_slv_in_val = true generate
    p_clk : process(slv_clk, slv_rst)
    begin
      if slv_rst = '1' then
        reg_slv_in_arr <= (others => '0');
      elsif rising_edge(slv_clk) then
        reg_slv_in_arr <= nxt_reg_slv_in_arr;
      end if;
    end process;

    nxt_reg_slv_in_arr <= slv_in_arr when slv_in_val = '1' else reg_slv_in_arr;
  end generate;

  gen_wire_input : if g_use_slv_in_val = false generate
    reg_slv_in_arr <= slv_in_arr;
  end generate;

  -----------------------------------------------------------------------------
  -- Field mapping
  -----------------------------------------------------------------------------
  -- Extract the all input fields ("RO") from slv_in and assign them to slv_in_arr
  slv_in_arr <= field_map_in(g_field_arr, slv_in, c_reg.dat_w, "RO");

  -- Map reg_slv_out onto slv_out for the write fields
  slv_out <= field_map_out(g_field_arr, reg_slv_out, c_reg.dat_w);

  -- Create the correct reg_slv_in using fields from both reg_slv_in_arr ("RO") reg_slv_out ("RW")
  reg_slv_in <= field_map(g_field_arr, reg_slv_in_arr, reg_slv_out, c_reg.dat_w);

  -----------------------------------------------------------------------------
  -- Actual MM <-> SLV R/W functionality is provided by common_reg_r_w_dc
  -----------------------------------------------------------------------------
  u_common_reg_r_w_dc : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_readback           => false,
    g_reg                => c_reg,
    g_init_reg           => c_init_reg
  )
  port map (
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => slv_rst,
    st_clk      => slv_clk,

    sla_in      => mm_mosi,
    sla_out     => mm_miso,

    in_reg      => reg_slv_in,
    out_reg     => reg_slv_out
  );
end str;
