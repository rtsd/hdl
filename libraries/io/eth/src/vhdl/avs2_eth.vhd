-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use work.eth_pkg.all;

entity avs2_eth is
  port (
    ---------------------------------------------------------------------------
    -- Clock interface
    ---------------------------------------------------------------------------
    csi_mm_reset            : in std_logic;
    csi_mm_clk              : in std_logic;

    ---------------------------------------------------------------------------
    -- Memory Mapped Slave interface
    ---------------------------------------------------------------------------
    -- TSE MAC
    -- . MOSI
    mms_tse_address         : in  std_logic_vector(c_tech_tse_byte_addr_w - 1 downto 0);
    mms_tse_write           : in  std_logic;
    mms_tse_read            : in  std_logic;
    mms_tse_writedata       : in  std_logic_vector(c_word_w - 1 downto 0);
    -- . MISO
    mms_tse_readdata        : out std_logic_vector(c_word_w - 1 downto 0);
    mms_tse_waitrequest     : out std_logic;

    -- ETH registers
    -- . MOSI
    mms_reg_address         : in  std_logic_vector(c_eth_reg_addr_w - 1 downto 0);
    mms_reg_write           : in  std_logic;
    mms_reg_read            : in  std_logic;
    mms_reg_writedata       : in  std_logic_vector(c_word_w - 1 downto 0);
    -- . MISO
    mms_reg_readdata        : out std_logic_vector(c_word_w - 1 downto 0);

    -- ETH packet RAM
    -- . MOSI
    mms_ram_address         : in  std_logic_vector(c_eth_ram_addr_w - 1 downto 0);
    mms_ram_write           : in  std_logic;
    mms_ram_read            : in  std_logic;
    mms_ram_writedata       : in  std_logic_vector(c_word_w - 1 downto 0);
    -- . MISO
    mms_ram_readdata        : out std_logic_vector(c_word_w - 1 downto 0);

    ---------------------------------------------------------------------------
    -- Avalon Interrupt Sender interface: ins_*
    ---------------------------------------------------------------------------
    ins_interrupt_irq       : out std_logic;

    ---------------------------------------------------------------------------
    -- Avalon Conduit interfaces: coe_*_export
    ---------------------------------------------------------------------------
    -- PHY interface
    coe_eth_clk_export      : in std_logic;
    coe_eth_txp_export      : out std_logic;
    coe_eth_rxp_export      : in  std_logic;

    -- LED
    coe_led_an_export       : out std_logic;
    coe_led_link_export     : out std_logic;
    coe_led_disp_err_export : out std_logic;
    coe_led_char_err_export : out std_logic;
    coe_led_crs_export      : out std_logic;
    coe_led_col_export      : out std_logic
  );
end avs2_eth;

architecture wrap of avs2_eth is
  -- Wrap all records to STD_LOGIC

  -- ST UDP interface
  signal udp_tx_snk_in_arr : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal udp_rx_src_in_arr : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0);

  -- MM interface
  signal tse_sla_in        : t_mem_mosi;  -- ETH TSE MAC registers
  signal tse_sla_out       : t_mem_miso;
  signal reg_sla_in        : t_mem_mosi;  -- ETH control and status registers
  signal reg_sla_out       : t_mem_miso;
  signal ram_sla_in        : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal ram_sla_out       : t_mem_miso;

  -- LED interface
  signal tse_led           : t_tech_tse_led;
begin
  -- Run internal ST at MM clock
  -- Disable UDP off-load interface
  udp_tx_snk_in_arr <= (others => c_dp_sosi_rst);  -- default not valid if not used
  udp_rx_src_in_arr <= (others => c_dp_siso_rdy);  -- default ready if not used

  -- TSE MAC
  -- . MOSI
  tse_sla_in.address  <= RESIZE_MEM_ADDRESS(mms_tse_address);
  tse_sla_in.wr       <= mms_tse_write;
  tse_sla_in.rd       <= mms_tse_read;
  tse_sla_in.wrdata   <= RESIZE_MEM_DATA(mms_tse_writedata);
  -- . MISO
  mms_tse_readdata    <= tse_sla_out.rddata(c_word_w - 1 downto 0);
  mms_tse_waitrequest <= tse_sla_out.waitrequest;

  -- ETH registers
  -- . MOSI
  reg_sla_in.address  <= RESIZE_MEM_ADDRESS(mms_reg_address);
  reg_sla_in.wr       <= mms_reg_write;
  reg_sla_in.rd       <= mms_reg_read;
  reg_sla_in.wrdata   <= RESIZE_MEM_DATA(mms_reg_writedata);
  -- . MISO
  mms_reg_readdata    <= reg_sla_out.rddata(c_word_w - 1 downto 0);

  -- ETH packet RAM
  -- . MOSI
  ram_sla_in.address  <= RESIZE_MEM_ADDRESS(mms_ram_address);
  ram_sla_in.wr       <= mms_ram_write;
  ram_sla_in.rd       <= mms_ram_read;
  ram_sla_in.wrdata   <= RESIZE_MEM_DATA(mms_ram_writedata);
  -- . MISO
  mms_ram_readdata    <= ram_sla_out.rddata(c_word_w - 1 downto 0);

  -- LEDs
  coe_led_an_export       <= tse_led.an;
  coe_led_link_export     <= tse_led.link;
  coe_led_disp_err_export <= tse_led.disp_err;
  coe_led_char_err_export <= tse_led.char_err;
  coe_led_crs_export      <= tse_led.crs;
  coe_led_col_export      <= tse_led.col;

  u_eth : entity work.eth
  port map (
    -- Clocks and reset
    mm_rst            => csi_mm_reset,  -- reset synchronous with mm_clk
    mm_clk            => csi_mm_clk,  -- memory-mapped bus clock
    eth_clk           => coe_eth_clk_export,  -- ethernet phy reference clock
    st_rst            => csi_mm_reset,  -- reset synchronous with st_clk
    st_clk            => csi_mm_clk,  -- packet stream clock

    -- UDP transmit interface
    udp_tx_snk_in_arr  => udp_tx_snk_in_arr,
    udp_tx_snk_out_arr => OPEN,
    -- UDP receive interface
    udp_rx_src_in_arr  => udp_rx_src_in_arr,
    udp_rx_src_out_arr => OPEN,

    -- Memory Mapped Slaves
    tse_sla_in        => tse_sla_in,  -- ETH TSE MAC registers
    tse_sla_out       => tse_sla_out,
    reg_sla_in        => reg_sla_in,  -- ETH control and status registers
    reg_sla_out       => reg_sla_out,
    reg_sla_interrupt => ins_interrupt_irq,  -- ETH interrupt
    ram_sla_in        => ram_sla_in,  -- ETH rx frame and tx frame memory
    ram_sla_out       => ram_sla_out,

    -- PHY interface
    eth_txp           => coe_eth_txp_export,
    eth_rxp           => coe_eth_rxp_export,

    -- LED interface
    tse_led           => tse_led
  );
end wrap;
