--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Provide an array of counters with dimension carryover
-- Description:
-- . Provides g_nof_counters counters,[g_nof_counters-1]..[c0]
-- . Every counter is specified like Python's range(start,stop,step).
-- . c0 changes the fastest, c4 changes the slowest
-- . Faster changing dimensions carry over in slower changing dimensions
--   when dimension maximum is reached
-- . The outputs are in sync with / apply to src_out.
-- . range(0,1,1) = [0] is the smallest count range
-- Usage:
-- . The count values themselves (c0..c4) are very useful to tag streaming
--   data with corresponding ID indices.
-- . The extra outputs (e.g. c0_min, c0_max) can be used to trigger other
--   logic when minimum/maximum values per dimension are reached.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_counter_func is
  generic (
    g_nof_counters    : natural := 1;
    g_range_start     : t_nat_natural_arr;  -- range must fit (g_nof_counters-1 DOWNTO 0)
    g_range_stop      : t_nat_natural_arr;  -- range must fit (g_nof_counters-1 DOWNTO 0)
    g_range_step      : t_nat_natural_arr  -- range must fit (g_nof_counters-1 DOWNTO 0)
  );
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;

    count_en    : in  std_logic;

    count_offset_in_arr : in  t_nat_natural_arr(g_nof_counters - 1 downto 0) := (others => 0);
    count_src_out_arr   : out t_dp_sosi_arr(g_nof_counters - 1 downto 0)
  );
end dp_counter_func;

architecture str of dp_counter_func is
  -- force downto range for unconstraint g_range generics
  constant c_range_len             : natural := g_range_start'length;  -- g_nof_counters must be <= c_range_len
  constant c_range_start           : t_nat_natural_arr(c_range_len - 1 downto 0) := g_range_start;
  constant c_range_stop            : t_nat_natural_arr(c_range_len - 1 downto 0) := g_range_stop;
  constant c_range_step            : t_nat_natural_arr(c_range_len - 1 downto 0) := g_range_step;

  constant c_max_count_w : natural := 32;

  signal count_en_arr   : std_logic_vector(g_nof_counters - 1 downto 0);
  signal check_max_arr  : std_logic_vector(g_nof_counters - 1 downto 0);
  signal count_init_arr : std_logic_vector(g_nof_counters - 1 downto 0);
  signal count_min_arr  : std_logic_vector(g_nof_counters - 1 downto 0);
  signal count_max_arr  : std_logic_vector(g_nof_counters - 1 downto 0);
  type t_count_arr is array(g_nof_counters - 1 downto 0) of std_logic_vector(c_max_count_w - 1 downto 0);
  signal count_arr      : t_count_arr;
begin
  --------------------------------------------------------------------------------
  -- Counter control inputs
  -------------------------------------------------------------------------------
  gen_dp_counter_func_single_input : for i in 0 to g_nof_counters - 1 generate
    gen_c0 : if i = 0 generate
      count_en_arr(i)  <= count_en;
      check_max_arr(i) <= count_en;
    end generate;

    gen_c1_upwards : if i > 0 generate
       count_en_arr(i)  <= count_init_arr(i - 1) or count_min_arr(i - 1);
       check_max_arr(i) <= count_max_arr(i - 1);
    end generate;

  end generate;

  --------------------------------------------------------------------------------
  -- Array of dp_counter_func_single instances
  -------------------------------------------------------------------------------
  gen_dp_counter_func_single : for i in 0 to g_nof_counters - 1 generate
    u_dp_counter_func_single : entity work.dp_counter_func_single
    generic map (
      g_range_start => c_range_start(i),
      g_range_stop  => c_range_stop(i),
      g_range_step  => c_range_step(i)
    )
    port map (
      rst        => rst,
      clk        => clk,

      count_en   => count_en_arr(i),
      check_max  => check_max_arr(i),
      count_offset => count_offset_in_arr(i),

      count      => count_arr(i),
      count_init => count_init_arr(i),
      count_min  => count_min_arr(i),
      count_max  => count_max_arr(i)
    );
  end generate;

  --------------------------------------------------------------------------------
  -- Counter outputs
  -------------------------------------------------------------------------------
  gen_dp_counter_func_single_output : for i in 0 to g_nof_counters - 1 generate
    count_src_out_arr(i).sync <= '0';  -- not used, force to '0' to avoid toggling between '0' and 'X' in Wave window
                                       -- when sync is passed on through other components
    count_src_out_arr(i).sop <= count_min_arr(i);
    count_src_out_arr(i).eop <= count_max_arr(i);
    count_src_out_arr(i).valid <= count_en;
    count_src_out_arr(i).data <= RESIZE_DP_DATA(count_arr(i));
  end generate;
end str;
