-------------------------------------------------------------------------------
--
-- Copyright 2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose:
-- . This package contains transient data buffer (TBUF) specific constants.
-- Description: See [1]
-- References:
-- . [1] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Transient+buffer+raw+data
-------------------------------------------------------------------------------
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_field_pkg.all;
use work.sdp_pkg.all;

package sdp_tbuf_pkg is
  -----------------------------------------------------------------------------
  -- TBUF registers
  -----------------------------------------------------------------------------
  constant c_sdp_tbuf_registers_nof_hdr_fields : natural := 16;

  -- 'downto' order to fit mm_fields
  constant c_sdp_tbuf_registers_field_arr : t_common_field_arr(c_sdp_tbuf_registers_nof_hdr_fields - 1 downto 0) :=
    -- The expected logic FF usage is 9 * 31 + 3 * 1 + 3 * 64 + 1 * 6 ~= 480 FF.
    ( (field_name_pad("dump_done            "), "RO",          1, field_default(0)),
      (field_name_pad("dump_enables         "), "RW", c_sdp_A_pn, field_default(0)),
      (field_name_pad("dump_start_rsn       "), "RW",         64, field_default(0)),
      (field_name_pad("dump_nof_pages       "), "RW",         32, field_default(0)),
      (field_name_pad("dump_start_page      "), "RW",         32, field_default(0)),
      (field_name_pad("dump_interpacket_gap "), "RW",         32, field_default(0)),
      (field_name_pad("recorded_last_rsn    "), "RO",         64, field_default(0)),
      (field_name_pad("recorded_first_rsn   "), "RO",         64, field_default(0)),
      (field_name_pad("recorded_nof_samples "), "RO",         32, field_default(0)),
      (field_name_pad("recorded_last_page   "), "RO",         32, field_default(0)),
      (field_name_pad("recorded_first_page  "), "RO",         32, field_default(0)),
      (field_name_pad("recorded_nof_pages   "), "RO",         32, field_default(0)),
      (field_name_pad("record_enable        "), "RW",          1, field_default(0)),
      (field_name_pad("record_all           "), "RW",          1, field_default(0)),
      (field_name_pad("nof_pages_in_buffer  "), "RO",         32, field_default(0)),
      (field_name_pad("nof_samples_per_block"), "RO",         32, field_default(0))
    );

  -- 'to' order as in document [1] to ease view in wave window
  type t_sdp_tbuf_registers is record                                         -- word index
    nof_samples_per_block       : natural;                                    -- 0
    nof_pages_in_buffer         : natural;                                    -- 1
    record_all                  : std_logic;                                  -- 2
    record_enable               : std_logic;                                  -- 3
    recorded_nof_pages          : natural;                                    -- 4
    recorded_first_page         : natural;                                    -- 5
    recorded_last_page          : natural;                                    -- 6
    recorded_nof_samples        : natural;                                    -- 7
    recorded_first_rsn          : std_logic_vector(63 downto 0);              -- 8
    recorded_last_rsn           : std_logic_vector(63 downto 0);              -- 10
    dump_interpacket_gap        : natural;                                    -- 12
    dump_start_page             : natural;                                    -- 13
    dump_nof_pages              : natural;                                    -- 14
    dump_start_rsn              : std_logic_vector(63 downto 0);              -- 15
    dump_enables                : std_logic_vector(c_sdp_A_pn - 1 downto 0);  -- 17
    dump_done                   : std_logic;                                  -- 18
  end record;

  constant c_sdp_tbuf_registers_rst :t_sdp_tbuf_registers :=
    (0, 0, '0', '0', 0, 0, 0, 0, (others => '0'), (others => '0'),
     0, 0, 0, (others => '0'), (others => '0'), '0');
end sdp_tbuf_pkg;
