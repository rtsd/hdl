-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_packetizing is
end tb_tb_dp_packetizing;

architecture tb of tb_tb_dp_packetizing is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 10
  -- > run 30 us                 --> OK

  -- Try different packing : GENERIC MAP (g_usr_nof_words => , g_phy_nof_words => , g_usr_dat_w => , g_phy_dat_w => )

  u_1_1_10_10 : entity work.tb_dp_packetizing generic map (1, 1, 10, 10);  -- 1/1 and try different c_phy_dat_w
  u_1_1_10_16 : entity work.tb_dp_packetizing generic map (1, 1, 10, 16);
  u_1_1_10_24 : entity work.tb_dp_packetizing generic map (1, 1, 10, 24);
  u_2_1_8_16  : entity work.tb_dp_packetizing generic map (2, 1,  8, 16);  -- 2/1
  u_1_2_16_8  : entity work.tb_dp_packetizing generic map (1, 2, 16,  8);  -- 1/2
  u_3_1_8_24  : entity work.tb_dp_packetizing generic map (3, 1,  8, 24);  -- 3/1
  u_1_3_24_8  : entity work.tb_dp_packetizing generic map (1, 3, 24,  8);  -- 1/3
  u_3_2_16_24 : entity work.tb_dp_packetizing generic map (3, 2, 16, 24);  -- 3/2
  u_2_3_24_16 : entity work.tb_dp_packetizing generic map (2, 3, 24, 16);  -- 2/3
  u_4_1_8_32  : entity work.tb_dp_packetizing generic map (4, 1,  8, 32);  -- 4/1
  u_1_4_32_8  : entity work.tb_dp_packetizing generic map (1, 4, 32,  8);  -- 1/4
end tb;
