-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program IS free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program IS distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

-- Purpose:
-- Description:
--   Strip IPv4 headers to 20 bytes
--   Set ihl to 5 (5 * 4 = 20 bytes)
--   Ajust IPv4 Packet Total Length field
--   IP Header Checksum is set to 0x0000 and should be ignored
--   The checksum could be updated using https://tools.ietf.org/html/rfc1624
-- Remarks:
--  . c_this_snk_latency = 1
--  . c_this_src_latency = 1

  ------------------------------------------------------------------------------
  -- IPv4 Packet
  --
  --  0       3 4     7 8            15 16   18 19                        31  wi
  -- |----------------------------------------------------------------------|
  -- | Version |  ihl  |    Services    |    IPv4 Packet Total Length       |  4
  -- |----------------------------------------------------------------------|
  -- |       Identification             | Flags |    Fragment Offset        |  5
  -- |----------------------------------------------------------------------|
  -- |     TTL         |    Protocol    |      Header Checksum              |  6
  -- |----------------------------------------------------------------------|
  -- |              Source IP Address                                       |  7
  -- |----------------------------------------------------------------------|
  -- |              Destination IP Address                                  |  8
  -- |----------------------------------------------------------------------|
  -- |              Options ...                                             |
  -- |----------------------------------------------------------------------|

entity eth_ihl_to_20 is
  generic (
    incoming_ihl   : natural := 20
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    -- Streaming Sink
    snk_in         : in  t_dp_sosi;
    snk_out        : out t_dp_siso;

    -- Streaming Source
    src_in         : in  t_dp_siso;
    src_out        : out t_dp_sosi
  );
end eth_ihl_to_20;

architecture rtl of eth_ihl_to_20 is
  signal i_src_out        : t_dp_sosi;
  signal i_src_in         : t_dp_siso;

  type state_type is (Idle, Eth_DestMAC0, Eth_DestMAC1, Eth_SrcMAC0, Eth_SrcMAC1, IPv4_lengths, IPv4_ID, IPv4_TTL, IPv4_SrcIP, IPv4_DestIP, IPv4_Options, IPv4_Payload);
  signal state   : state_type;

  signal ihl : unsigned(c_network_ip_header_length_w - 1 downto 0);
begin
  -- Pass on frame level flow control
  snk_out.xon <= i_src_in.xon;

  -- No change in ready latency, c_this_snk_latency = c_this_src_latency
  snk_out.ready <= i_src_in.ready;

  u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
  generic map (
    g_data_w    => c_eth_data_w,
    g_use_bsn   => false,
    g_use_sync  => false,
    g_fifo_size => 10
   )
  port map (
    rst        => rst,
    clk        => clk,

    snk_in     => i_src_out,
    snk_out    => i_src_in,

    src_out    => src_out,
    src_in     => src_in
  );

  process(clk, rst)
  begin
    if rst = '1' then
      state <= Idle;
      ihl   <= to_unsigned(0, ihl'length);
    elsif (rising_edge(clk)) then
      i_src_out <= snk_in;
      case state is
        when Idle =>
          if snk_in.sop = '1' and snk_in.valid = '1' then
            state <= Eth_DestMAC0;
          end if;

        when Eth_DestMAC0 =>
          if snk_in.valid = '1' then
            state <= Eth_DestMAC1;
          end if;

        when Eth_DestMAC1  =>
          if snk_in.valid = '1' then
            state <= Eth_SrcMAC0;
          end if;

        when Eth_SrcMAC0  =>
          if snk_in.valid = '1' then
            if snk_in.data(c_network_eth_type_slv'range) = TO_UVEC(c_network_eth_type_ip, c_network_eth_type_w) then  -- if IPv4 frame
              state <= Eth_SrcMAC1;
            else
              state <= Idle;
            end if;
          end if;

        when Eth_SrcMAC1  =>
          if snk_in.valid = '1' then
            if snk_in.data(27 downto 24) = TO_UVEC(c_network_ip_header_length, c_network_ip_header_length_w) then  -- if ihl = 5 words = 20 bytes, nothing to do
              state <= Idle;
            else
              state <= IPv4_lengths;
              i_src_out.data(27 downto 24) <= TO_UVEC(c_network_ip_header_length, c_network_ip_header_length_w);  -- make IHL = 5 words = 20 bytes
              i_src_out.data(c_network_ip_total_length_w - 1 downto 0) <= std_logic_vector(
                                             unsigned(snk_in.data(c_network_ip_total_length_w - 1 downto 0)) -
                                           ((unsigned(snk_in.data(27 downto 24)) - to_unsigned(c_network_ip_header_length, c_network_ip_header_length_w)) & "00")
                                                                                        );  -- correct Total Length = input Total Length - (input IHL - fixed IHL),
              ihl <= unsigned(snk_in.data(27 downto 24));  -- save input IHL
            end if;
          end if;

        when IPv4_lengths  =>
          if snk_in.valid = '1' then
            state <= IPv4_ID;
            ihl <= ihl - 1;
          end if;

        when IPv4_ID  =>
          if snk_in.valid = '1' then
            state <= IPv4_TTL;
            i_src_out.data(c_network_ip_header_checksum_w - 1 downto 0) <= TO_UVEC(0, c_network_ip_header_checksum_w);
            ihl <= ihl - 1;
          end if;

        when IPv4_TTL  =>
          if snk_in.valid = '1' then
            state <= IPv4_SrcIP;
            ihl <= ihl - 1;
          end if;

        when IPv4_SrcIP  =>
          if snk_in.valid = '1' then
            state <= IPv4_DestIP;
            ihl <= ihl - 1;
          end if;

        when IPv4_DestIP  =>
          if snk_in.valid = '1' then
            i_src_out.valid <= '0';  -- do not allow option words to get through this module
            state <= IPv4_Options;
            ihl <= ihl - 1;
            if ihl = 2 then
              state <= IPv4_Payload;
            end if;
          end if;

        when IPv4_Options  =>
          if snk_in.valid = '1' then
            i_src_out.valid <= '0';  -- do not allow option words to get through this module
            ihl <= ihl - 1;
            if ihl = 2 then
              state <= IPv4_Payload;
            end if;
          end if;

        when IPv4_Payload =>
          if snk_in.eop = '1' and snk_in.valid = '1' then
            state <= Idle;
          end if;

        when others =>
          state <= Idle;
      end case;
    end if;
  end process;
end rtl;
