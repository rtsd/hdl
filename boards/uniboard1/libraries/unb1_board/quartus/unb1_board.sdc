###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

set_time_format -unit ns -decimal_places 3

create_clock -name {CLK} -period 5.000 -waveform { 0.000 2.500 } [get_ports {CLK}]
create_clock -name {ETH_CLK} -period 40.000 -waveform { 0.000 20.000 } [get_ports {ETH_clk}]
create_clock -name {SB_CLK} -period 6.400 -waveform { 0.000 3.200 } [get_ports {SB_CLK}]
create_clock -name {SA_CLK} -period 6.400 -waveform { 0.000 3.200 } [get_ports {SA_CLK}]
create_clock -name {ADC_BI_A_CLK} -period 2.500 -waveform { 0.000 1.250 } [get_ports {ADC_BI_A_CLK}]
create_clock -name {ADC_BI_D_CLK} -period 2.500 -waveform { 0.000 1.250 } [get_ports {ADC_BI_D_CLK}]

derive_pll_clocks

derive_clock_uncertainty

# Effectively set false path from this clock to all other clocks
set_clock_groups -asynchronous -group [get_clocks altera_reserved_tck]
set_clock_groups -asynchronous -group [get_clocks SB_CLK]
set_clock_groups -asynchronous -group [get_clocks SA_CLK]
set_clock_groups -asynchronous -group [get_clocks ETH_CLK]

set_clock_groups -asynchronous -group [get_clocks SA_CLK~input~INSERTED_REFCLK_DIVIDER|clkout]

set_clock_groups -asynchronous -group [get_clocks *u_sopc|the_altpll_0|sd1|pll7|clk[0]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|the_altpll_0|sd1|pll7|clk[1]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|the_altpll_0|sd1|pll7|clk[2]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|the_altpll_0|sd1|pll7|clk[3]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|altpll_0|sd1|pll7|clk[0]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|altpll_0|sd1|pll7|clk[1]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|altpll_0|sd1|pll7|clk[2]]
set_clock_groups -asynchronous -group [get_clocks *u_sopc|altpll_0|sd1|pll7|clk[3]]

# QSYS, second PLL
set_clock_groups -asynchronous -group [get_clocks *|altpll_1|sd1|pll7|clk[0]]

set_clock_groups -asynchronous -group [get_clocks CLK]
set_clock_groups -asynchronous -group [get_clocks {*|altpll_component|auto_generated|pll1|clk[0]}]
set_clock_groups -asynchronous -group [get_clocks {*|altpll_component|auto_generated|pll1|clk[1]}]
set_clock_groups -asynchronous -group [get_clocks {*|altpll_component|auto_generated|pll1|clk[2]}]
set_clock_groups -asynchronous -group [get_clocks {*|altpll_component|auto_generated|pll1|clk[3]}]
set_clock_groups -asynchronous -group [get_clocks ADC_BI_A_CLK]
set_clock_groups -asynchronous -group [get_clocks ADC_BI_D_CLK]

# Transceivers: ALTGX generated RX clock
set_clock_groups -asynchronous -group [get_clocks {*|receive_pcs0|clkout}]
# Transceivers: ALTGX generated TX clock
set_clock_groups -asynchronous -group [get_clocks {*|transmit_pcs0|clkout}]
# Transceivers: XAUI/10GbE generated clock
set_clock_groups -asynchronous -group [get_clocks {*|central_clk_div0|coreclkout}]

# DDR3 controller-generated clock
set_clock_groups -asynchronous -group [get_clocks {*|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0_pll_afi_clk}]
