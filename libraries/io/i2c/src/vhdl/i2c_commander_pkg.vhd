-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.i2c_pkg.all;
use work.i2c_smbus_pkg.all;

package i2c_commander_pkg is
  -- I2C commander settings
  --
  constant c_i2c_cmdr_state_idle    : natural := 0;  -- no protocol active
  constant c_i2c_cmdr_state_pending : natural := 1;  -- protocol pending by write event, but not yet activated by sync
  constant c_i2c_cmdr_state_busy    : natural := 2;  -- protocol I2C accesses are busy
  constant c_i2c_cmdr_state_done    : natural := 3;  -- protocol I2C accesses finished
  constant c_i2c_cmdr_state_max     : natural := 3;
  constant c_i2c_cmdr_state_w       : natural := ceil_log2(c_i2c_cmdr_state_max);

  constant c_i2c_cmdr_max_nof_protocols : natural := 16;  -- fixed maximum number of protocol lists that t_c_i2cmdr record can support
  constant c_i2c_cmdr_result_cnt_w      : natural := 16;  -- more than sufficiently large range for most practical cases
  constant c_i2c_cmdr_nof_read_byte_max : natural := 8;  -- Assume 8 read bytes per protocol list are sufficent, so c_i2c_cmdr_nof_read_byte_max=8 and use t_i2c_cmdr_natural_arr with range 0 to 7

  type t_i2c_cmdr_natural_arr is array (integer range 0 to c_i2c_cmdr_nof_read_byte_max - 1) of natural;
  type t_i2c_cmdr_natural_mat is array (integer range <>) of t_i2c_cmdr_natural_arr;

  type t_c_i2c_cmdr_commander is record
    nof_protocols         : natural;  -- must be <= c_i2cmdr_max_nof_protocols
    protocol_offset_arr   : t_natural_arr(0 to c_i2c_cmdr_max_nof_protocols - 1);
    result_expected_arr   : t_slv_32_arr(0 to c_i2c_cmdr_max_nof_protocols - 1);
    result_cnt_w          : natural;  -- the nof result bytes that a protocol list may yield is <= 2**c_i2c_protocol_adr_w
    nof_result_data_max   : natural;  -- nof data bytes that a protocol list may maximally read
  end record;

  function func_i2c_cmdr_mm_reg_nof_dat(rec : t_c_i2c_cmdr_commander) return natural;

  function func_i2c_cmdr_sel_a_b(sel : boolean; a, b : t_i2c_cmdr_natural_arr) return t_i2c_cmdr_natural_arr;
  function func_i2c_cmdr_sel_a_b(sel : boolean; a, b : t_c_i2c_cmdr_commander) return t_c_i2c_cmdr_commander;

  -- Void protocol lists for the I2C commander
  constant c_i2c_cmdr_protocol_list_nop             : t_nat_natural_arr(0 downto 0) := (others => SMBUS_C_NOP);
  constant c_i2c_cmdr_protocol_list_end             : t_nat_natural_arr(0 downto 0) := (others => SMBUS_C_END);
  constant c_i2c_cmdr_protocol_list_sample_sda      : t_nat_natural_arr := (SMBUS_C_SAMPLE_SDA, 0, 0, 0, 0, SMBUS_C_END);

  -- Expected result list for the protocol lists, read yields (data, ok=0), write or control (like end, timeout) yield (ok=0):
  -- . entries 0 should also be 0 in the result buffer
  -- . entries 1 indicate a read octet that needs to be stored for the user
  constant c_i2c_cmdr_expected_mask_none            : std_logic_vector := RESIZE_UVEC("0", c_word_w);
  constant c_i2c_cmdr_expected_mask_end             : std_logic_vector := RESIZE_UVEC("0", c_word_w);
  constant c_i2c_cmdr_expected_mask_sample_sda      : std_logic_vector := RESIZE_UVEC("0", c_word_w);
  constant c_i2c_cmdr_expected_mask_read_one_byte   : std_logic_vector := RESIZE_UVEC("01", c_word_w);
  constant c_i2c_cmdr_expected_mask_read_one_word   : std_logic_vector := RESIZE_UVEC("011", c_word_w);

  constant c_i2c_cmdr_nof_result_data_none          : natural := 0;
  constant c_i2c_cmdr_nof_result_data_end           : natural := 0;
  constant c_i2c_cmdr_nof_result_data_sample_sda    : natural := 0;
  constant c_i2c_cmdr_nof_result_data_read_one_byte : natural := 1;
  constant c_i2c_cmdr_nof_result_data_read_one_word : natural := 2;

  constant c_i2c_cmdr_expected_x                    : natural := 254;  -- do not use 255, because that matches pull-up all ones
  constant c_i2c_cmdr_expected_data_none_arr        : t_i2c_cmdr_natural_arr := (c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x,
                                                                                 c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x,
                                                                                 c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x,
                                                                                 c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x);

  constant c_i2c_cmdr_mem_block_sz                  : natural := 1024;  -- assign t_c_mem.nof_dat in blocks of 1024 = 1 M9K
end i2c_commander_pkg;

package body i2c_commander_pkg is
  function func_i2c_cmdr_mm_reg_nof_dat(rec : t_c_i2c_cmdr_commander) return natural is
  -- Example MM register map defined for:
  -- . c_nof_protocols   = 2
  -- . c_nof_result_data_max = 3
  --
  --    0 = protocol 0
  --    1 = protocol 1
  --    2 = protocol offset 0
  --    3 = protocol offset 1
  --    4 = result_expected 0
  --    5 = result_expected 1
  --    6 = protocol status
  --    7 = result_error_cnt
  --    8 = result_data byte 0
  --    9 = result_data byte 1
  --   10 = result_data byte 2
  --
  -- so return mm_reg_nof_dat = 3*2 + 1 + 1 + 3 = 11
  begin
    return 3 * rec.nof_protocols + 1 + 1 + rec.nof_result_data_max;
  end;

  function func_i2c_cmdr_sel_a_b(sel : boolean; a, b : t_i2c_cmdr_natural_arr) return t_i2c_cmdr_natural_arr is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function func_i2c_cmdr_sel_a_b(sel : boolean; a, b : t_c_i2c_cmdr_commander) return t_c_i2c_cmdr_commander is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;
end i2c_commander_pkg;
