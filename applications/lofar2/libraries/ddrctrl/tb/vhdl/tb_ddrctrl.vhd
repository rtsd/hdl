-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: Self checking and self-stopping tb for ddrctrl.vhd
-- Usage:
-- > run -a

library IEEE, common_lib, technology_lib, tech_ddr_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use technology_lib.technology_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_ddrctrl is
  generic (
    g_tech_ddr                : t_c_tech_ddr                                            := c_tech_ddr4_8g_1600m;  -- type of memory
    g_nof_streams             : positive                                                := 12;  -- number of input streams
    g_data_w                  : natural                                                 := 14;  -- data with of input data vectors
    g_technology              : natural                                                 := c_tech_select_default;
    g_tech_ddr3               : t_c_tech_ddr                                            := c_tech_ddr3_4g_800m_master;
    g_tech_ddr4               : t_c_tech_ddr                                            := c_tech_ddr4_4g_1600m;
    g_stop_percentage         : natural                                                 := 80;  -- percentage there needs to be already written in the ddr memory when a stop gets triggered
    g_block_size              : natural                                                 := 1024  -- amount of samples that goes into one bsn
  );
end tb_ddrctrl;

architecture tb of tb_ddrctrl is
  constant c_sim_model          : boolean                                               := true;  -- determens if this is a simulation

  -- Select DDR3 or DDR4 dependent on the technology and sim model
  constant  c_mem_ddr           : t_c_tech_ddr                                          := func_tech_sel_ddr(g_technology, g_tech_ddr3, g_tech_ddr4);
  constant  c_sim_ddr           : t_c_tech_ddr                                          := func_tech_sel_ddr(g_technology, c_tech_ddr3_sim_16k, c_tech_ddr4_sim_16k);
  constant  c_tech_ddr          : t_c_tech_ddr                                          := func_tech_sel_ddr(c_sim_model, c_sim_ddr, c_mem_ddr);

  -- constants for readability
  constant  c_ctrl_data_w       : natural                                               := func_tech_ddr_ctlr_data_w(c_tech_ddr);  -- 576
  constant  c_in_data_w         : natural                                               := g_nof_streams * g_data_w;  -- output data with, 168

  -- constants for testbench
  constant  c_clk_freq          : natural                                               := 200;  -- clock frequency in MHz
  constant  c_clk_period        : time                                                  := (10**6 / c_clk_freq) * 1 ps;  -- clock priod, 5 ns
  constant  c_mm_clk_freq       : natural                                               := 100;  -- mm clock frequency in MHz
  constant  c_mm_clk_period     : time                                                  := (10**6 / c_mm_clk_freq) * 1 ps;  -- mm clock period, 10 ns
  constant  c_stop_value_for_j  : natural                                               := 14180;

  -- constant for checking output data
  constant  c_adr_w             : natural                                               := func_tech_ddr_ctlr_address_w(c_tech_ddr);  -- the lengt of the address vector, for simulation this is smaller, otherwise the simulation would take to long, 27
  constant  c_max_adr           : natural                                               := 2**(c_adr_w) - 1;  -- the maximal address that is possible within the vector length of the address
  constant  c_output_stop_adr   : natural                                               := (c_max_adr + 1) - ((((c_max_adr + 1) / 64) * g_stop_percentage / 100) * 64);
  constant  c_output_ds         : natural                                               := 144;
  constant  c_bim               : natural                                               := (c_max_adr * c_ctrl_data_w) / (g_block_size * g_nof_streams * g_data_w);  -- the amount of whole blocks that fit in memory.
  constant  c_adr_per_b         : natural                                               := ((g_block_size * g_nof_streams * g_data_w) / c_ctrl_data_w) + 1;  -- rounding error removes the amount of extra addresses.

  -- the amount of addresses used
  constant  c_nof_adr           : natural                                               := (c_bim * g_block_size * g_nof_streams * g_data_w) / c_ctrl_data_w;  -- rounding error removes the amount of extra addresses.

  -- the amount of overflow after one block is written
  constant  c_of_pb             : natural                                               := (g_block_size * g_nof_streams * g_data_w) - (((g_block_size * g_nof_streams * g_data_w) / c_ctrl_data_w) * c_ctrl_data_w);  -- amount of overflow after one block is written to memory

  function c_of_after_nof_adr_init return natural is
    variable temp               : natural                                               := 0;
  begin
    for I in 0 to c_bim - 1 loop
      if temp + c_of_pb < c_ctrl_data_w then
        temp := temp + c_of_pb;
      else
        temp := temp + c_of_pb - c_ctrl_data_w;
      end if;
    end loop;
    return temp;
  end function c_of_after_nof_adr_init;

  -- the amount of overflow into the address: c_nof_adr     NOT YET USED DELETE AFTER L2SDP-706
  constant  c_of_after_nof_adr  : natural                                               := c_of_after_nof_adr_init;

  -- function for making total data vector
  function  c_total_vector_init return std_logic_vector is
    variable temp               : std_logic_vector(g_data_w * g_nof_streams * c_bim * g_block_size-1 downto 0);
    variable conv               : std_logic_vector(32 - 1 downto 0) := (others => '0');  -- removes a warning
  begin
    for I in 0 to c_bim * g_block_size-1 loop
      conv                                     := TO_UVEC(I, 32);
      for J in 0 to g_nof_streams - 1 loop
        temp(g_data_w * ((I * g_nof_streams) + J + 1) - 1 downto g_data_w * ((I * g_nof_streams) + j)) := conv(g_data_w - 1 downto 0);
      end loop;
    end loop;
    return temp;
  end function c_total_vector_init;

  -- constant for running the test
  constant  c_total_vector    : std_logic_vector(g_data_w * g_nof_streams * c_bim * g_block_size-1 downto 0) := c_total_vector_init;  -- vector which contains all input data vectors to make it easy to fill ctr_vector

  signal    c_total_vector_length : natural := c_total_vector'length;

  constant  c_check           : natural                                               := 32;
  constant  c_check_bottom    : natural                                               := 5;
  constant  c_ones            : std_logic_vector(c_check - c_check_bottom - 1 downto 0)   := (others => '1');

  -- input signals for ddrctrl.vhd
  signal    clk               : std_logic                                             := '1';
  signal    rst               : std_logic                                             := '0';
  signal    mm_clk            : std_logic                                             := '0';
  signal    mm_rst            : std_logic                                             := '0';
  signal    in_sosi_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0)               := (others => c_dp_sosi_init);  -- input data signal for ddrctrl_pack.vhd
  signal    stop_in           : std_logic                                             := '0';
  signal    out_sosi_arr      : t_dp_sosi_arr(g_nof_streams - 1 downto 0)               := (others => c_dp_sosi_init);
  signal    out_siso          : t_dp_siso;

  -- testbench signal
  signal    tb_end            : std_logic                                             := '0';  -- signal to turn the testbench off

  -- signals for running test
  signal    in_data_cnt       : natural                                               := 0;  -- signal which contains the amount of times there has been input data for ddrctrl_repack.vhd
  signal    test_running      : std_logic                                             := '0';  -- signal to tell wheter the testing has started
  signal    bsn_cnt           : natural                                               := g_block_size-1;

  -- signals for checking the output data
  signal    output_data_cnt   : natural                                               := 0;

  -- PHY
  signal    phy3_io           : t_tech_ddr3_phy_io;
  signal    phy3_ou           : t_tech_ddr3_phy_ou;
  signal    phy4_io           : t_tech_ddr4_phy_io;
  signal    phy4_ou           : t_tech_ddr4_phy_ou;
begin
  -- generating clock
  clk               <= not clk or tb_end after c_clk_period / 2;
  mm_clk            <= not mm_clk or tb_end after c_mm_clk_period / 2;
  out_siso.xon      <= '1';

  -- excecuting test
  p_test : process

  variable  out_siso_ready  : natural := 0;
  begin
    -- start the test
    out_siso.ready        <= '1';
    tb_end                <= '0';
    in_sosi_arr(0).valid  <= '0';
    wait until rising_edge(clk);  -- align to rising edge
    wait for c_clk_period * 4;
    rst <= '1';
    mm_rst <= '1';
    wait for c_clk_period * 1;
    rst <= '0';
    mm_rst <= '0';
    test_running <= '1';
    wait for c_clk_period * 1;

    -- wr fifo has delay of 4 clockcylces after reset

    -- filling the input data vectors with the corresponding numbers
    run_multiple_times : for K in 0 to 7 loop
      make_data : for J in 0 to c_bim * g_block_size-1 loop
        in_data_cnt     <= in_data_cnt + 1;
        fill_in_sosi_arr : for I in 0 to g_nof_streams - 1 loop
          if bsn_cnt = g_block_size-1 then
            if I = 0 then
              bsn_cnt <= 0;
            end if;
            in_sosi_arr(I).sop <= '1';
            in_sosi_arr(I).eop <= '0';
            in_sosi_arr(I).bsn <= INCR_UVEC(in_sosi_arr(I).bsn, 1);
          else
            if I = 0 then
              bsn_cnt <= bsn_cnt + 1;
            end if;
            in_sosi_arr(I).sop <= '0';
          end if;
          if bsn_cnt = g_block_size-2 then
            in_sosi_arr(I).eop <= '1';
          end if;
          in_sosi_arr(I).data(g_data_w - 1 downto 0)   <= c_total_vector(g_data_w * (I + 1) + J * c_in_data_w - 1 downto g_data_w * I + J * c_in_data_w);
          in_sosi_arr(I).valid <= '1';
        end loop;

        if (K = 1 or K = 3) and J = c_stop_value_for_J then
          stop_in <= '1';
        else
          stop_in <= '0';
        end if;

        for L in 0 to 30 loop
          if k = 1 and J = c_bim * g_block_size * 85 / 100 + 60 + L then
            out_siso_ready := out_siso_ready + 1;
          end if;
          if k = 1 and J = c_bim * g_block_size * 85 / 100 + 160 + L then
            out_siso_ready := out_siso_ready + 1;
          end if;
        end loop;

        if out_siso_ready = 0 then
          out_siso.ready  <= '1';
        else
          out_siso.ready  <= '0';
          out_siso_ready  := 0;
        end if;

        wait for c_clk_period * 1;
      end loop;
    end loop;

    in_sosi_arr(0).valid <= '0';
    test_running      <= '0';

    -- stopping the testbench
    wait for c_clk_period * g_block_size;
    tb_end <= '1';
    assert false
      report "Test: OK"
      severity FAILURE;
  end process;

  p_checking_output_data : process  -- first do tickets L2SDP-708 and L2SDP-707 before finsishing this is worth time
  begin
    wait until rising_edge(clk);
    if out_sosi_arr(0).valid = '1' then
      for I in 0 to g_nof_streams - 1 loop
        if c_output_stop_adr + output_data_cnt <= c_max_adr then
          --ASSERT out_sosi_arr(I).data(c_in_data_w-1 DOWNTO 0) = c_total_vector(g_data_w*(I+1)+(c_output_stop_adr+output_data_cnt)*c_ctrl_data_w-1 DOWNTO g_data_w*I+(c_output_stop_adr+output_data_cnt)*c_ctrl_data_w) REPORT "wrong output data at: " & NATURAL'image(c_output_stop_adr+output_data_cnt) SEVERITY ERROR;
        else
          --ASSERT out_sosi_arr(I).data(c_in_data_w-1 DOWNTO 0) = c_total_vector(g_data_w*(I+1)+(c_output_stop_adr+output_data_cnt-c_max_adr)*c_ctrl_data_w-1 DOWNTO g_data_w*I+(c_output_stop_adr+output_data_cnt-c_max_adr)*c_ctrl_data_w) REPORT "wrong output data at: " & NATURAL'image(c_output_stop_adr+output_data_cnt) SEVERITY ERROR;
        end if;
      end loop;
      output_data_cnt <= output_data_cnt + 1;
    end if;
    wait for c_clk_period * 1;
  end process;

  -- DUT
  u_ddrctrl : entity work.ddrctrl
  generic map (
    g_tech_ddr        => c_tech_ddr,
    g_sim_model       => c_sim_model,
    g_technology      => g_technology,
    g_nof_streams     => g_nof_streams,
    g_data_w          => g_data_w,
    g_stop_percentage => g_stop_percentage,
    g_block_size      => g_block_size
  )
  port map (
    clk               => clk,
    rst               => rst,
    ctlr_ref_clk      => clk,
    ctlr_ref_rst      => rst,
    mm_clk            => mm_clk,
    mm_rst            => mm_rst,
    in_sosi_arr       => in_sosi_arr,
    stop_in           => stop_in,
    out_sosi_arr      => out_sosi_arr,
    out_siso          => out_siso,

    --PHY
    phy3_io           => phy3_io,
    phy3_ou           => phy3_ou,
    phy4_io           => phy4_io,
    phy4_ou           => phy4_ou
  );
end tb;
