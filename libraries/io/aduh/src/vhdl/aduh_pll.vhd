-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.aduh_pll_pkg.all;

-- Purpose: ADUH = ADU Handler using a PLL. Handles the LVDS Rx interface
--          between a BN and the corresponding ADCs on one or two ADUs
-- Description:
--   The aduh_pll merely wires up one or two lvdsh_pll dependent on
--   g_ai.nof_clocks = 1 or 2 to receive the ADU samples.
--
--   Each UniBoard connects to 2 ADU. Ports A and B carry the data from ADU1
--   and ports C and D carry the data from ADU0. The aduh_pll can take the data
--   input from nof_adu = 1 or 2 ADUs. Each ADU has fixed nof_ports = 2 signal
--   paths. The aduh_pll outputs 4 signal path data streams A, B, C, D in the
--   dp_clk domain. If nof_adu=2 then all 4 signal paths are active. If nof_adu
--   = 1 then only the input from ADU0 so ports C and D is used.
--
--   The aduh_pll can use one or two LVDS data reference clocks, ADC_BI_A_CLK
--   and ADC_BI_D_CLK. Use g_ai.nof_clocks = 2 if it should be possible to use
--   the data from ADU1 when ADU0 is not present, or vice versa. The
--   ADC_BI_B_CLK and ADC_BI_C_CLK are not used. If g_ai.nof_clocks = 1 and
--   use_lvds_clk = TRUE then only ADC_BI_D_CLK is used. If g_ai.nof_clocks = 1
--   and use_lvds_clk = FALSE then the dp_clk is used as LVDS data reference
--   clock.
--
--   The port data needs to be deserialized by a dp_deser_factor = 4, because
--   the ADC sample clock frequency is lvds_data_rate = 800 Mbps and the data
--   path processing clock frequency is 200 MHz (= 800 Mbps / dp_deser_factor).
--   The ADC_BI_A_CLK and ADC_BI_D_CLK that come with the ADU data run at
--   400 MHz (= 800 MHz / lvds_deser_factor). Hence the lvdsh_pll supports
--   using a LVDS reference clock that has a different rate than the
--   de-serialized clock.
--
--   The restart input is a level signal that resets the aduh_pll input when
--   active.
--
-- Remarks:
-- . The unused BN_BI IO may be used for other signalling if necessary:
--   - Input ADC overflow indication ADC_BI_*_OVR per ADU (so one bit per two
--     ADCs)
--   - Output ADC 400 MHz DDR clock phase reset ADC_BI_*_CLK_RST per ADU
-- . The ADU control via the ADC_SCL[3:0], ADC_SDA[3:0] I2C can be handled at
--   SOPC system level, by means of the avs_i2c_master MM peripheral. Each pair
--   of signal paths on ADU has an I2C control interface, therefor only 2 of
--   the 4 ADC_SCL and ADC_SDA lines will need to be used per BN.
-- . The serial control of the adc08d1020 ADC needs to be done by means of bit
--   banging via the I2C IO-expander on ADU.
-- . If use_lvds_clk=TRUE and nof_clocks=1 then ADC_BI_D_CLK is used, because:
--   - ADC_BI_A_CLK pairs with PLL_R3, but PLL_R3 is also used by the TSE clock
--   - ADC_BI_D_CLK pairs with PLL_R2

entity aduh_pll is
  generic (
    g_ai  : t_c_aduh_pll_ai := c_aduh_pll_ai
  );
  port (
    -- LVDS Interface

    -- . ADU_AB
    ADC_BI_AB_OVR    : in    std_logic := '0';  -- the ADU outputs one overflow bit, shared per 2 signal paths A,B
    ADC_BI_A         : in    std_logic_vector(g_ai.port_w - 1 downto 0);  -- fixed ADC_BI port width port_w = 8
    ADC_BI_B         : in    std_logic_vector(g_ai.port_w - 1 downto 0);  -- each ADC_BI port carries the data from 1 signal path
    ADC_BI_A_CLK     : in    std_logic;  -- lvdsh_clk(1)
    ADC_BI_A_CLK_RST : out   std_logic;  -- release synchronises port A DCLK divider

    -- . ADU_CD
    ADC_BI_CD_OVR    : in    std_logic := '0';
    ADC_BI_C         : in    std_logic_vector(g_ai.port_w - 1 downto 0);
    ADC_BI_D         : in    std_logic_vector(g_ai.port_w - 1 downto 0);  -- ports A,B connect to one ADU, ports C,D connect to another ADU
    ADC_BI_D_CLK     : in    std_logic;  -- lvdsh_clk(0)
    ADC_BI_D_CLK_RST : out   std_logic;  -- release synchronises port D DCLK divider

    -- DP Interface
    dp_clk           : in    std_logic;

    -- . Control
    restart          : in    std_logic_vector(g_ai.nof_clocks - 1 downto 0);  -- [  0] = [       ADU_CD] Shared   restart control for ADU-CD (nof_adu=1) or both ADU-AB and ADU-CD (nof_adu=2)
                                                                            -- [1:0] = [ADU_AB ADU_CD] Seperate restart control for per ADU (nof_adu=2)
    delay_settings   : in    t_natural_arr(func_aduh_pll_lvds_dat_w(g_ai) - 1 downto 0) := (others => 0);  -- [ovrAB, portA, portB, ovrCD, portC, portD] or [ovrCD, portC, portD] : IOE data delay settings when g_use_dpa = FALSE
    cda_settings     : in    t_natural_arr(func_aduh_pll_lvds_dat_w(g_ai) - 1 downto 0) := (others => 0);  -- [ovrAB, portA, portB, ovrCD, portC, portD] or [ovrCD, portC, portD] : channel data alignment settings

    -- . Streaming
    src_out          : out   t_dp_sosi_arr(0 to g_ai.nof_sp - 1)  -- = [0:3] = ADC_BI ports [A,B,C,D] when nof_adu=2,
                                                                -- = [0:3] = ADC_BI ports [0,0,C,D] when nof_adu=1
  );
end aduh_pll;

architecture str of aduh_pll is
  -----------------------------------------------------------------------------
  -- LVDS domain
  -----------------------------------------------------------------------------

  constant c_lvds_clk_freq       : natural := g_ai.lvds_data_rate / g_ai.lvds_deser_factor;  -- 400 MHz, when lvds_clk from the adc08d1020 ADC on ADU provides the DDR of the sample clock
                                                                                            -- 200 MHz, when lvdsh_clk is connected to the dp_clk
  -- Treat the LVDS input interface per reference clock, so use 1 or 2 lvdsh_pll
  constant c_lvdsh_dat_w         : natural := func_aduh_pll_lvdsh_dat_w(g_ai);  -- lvdsh_dat_w = lvds_dat_w / g_ai.nof_clocks
  constant c_rcvdh_dat_w         : natural := c_lvdsh_dat_w * g_ai.dp_deser_factor;

  type t_lvdsh_dat_arr is array (integer range <>) of std_logic_vector(c_lvdsh_dat_w - 1 downto 0);
  type t_rcvdh_dat_arr is array (integer range <>) of std_logic_vector(c_rcvdh_dat_w - 1 downto 0);

  -- The dp_dat carries dp_deser_factor=4 samples for t0,t1,t2,t3 at 31:0, the optional overflow bit is carried via dp_ovr
  constant c_dp_dat_w            : natural := g_ai.port_w * g_ai.dp_deser_factor;  -- [31:0] = [t0,t1,t2,t3]

  type t_dp_dat_arr   is array (integer range <>) of std_logic_vector(c_dp_dat_w - 1 downto 0);

  -----------------------------------------------------------------------------
  -- LVDS clock domain
  -----------------------------------------------------------------------------

  -- LVDS output
  signal lvdsh_clk_rst           : t_sl_arr(       g_ai.nof_clocks - 1 downto 0);

  -- LVDS input
  signal lvdsh_clk               : t_sl_arr(       g_ai.nof_clocks - 1 downto 0);
  signal lvdsh_dat               : t_lvdsh_dat_arr(g_ai.nof_clocks - 1 downto 0);  -- serialized LVDS Rx input data

  -----------------------------------------------------------------------------
  -- Data Path clock domain, after clock domain crossing FIFO in LVDSH
  -----------------------------------------------------------------------------

  -- Index related to g_ai.nof_clocks, similar as for lvdsh_dat
  signal rcvdh_dat               : t_rcvdh_dat_arr(g_ai.nof_clocks - 1 downto 0);  -- received de-serialized data
  signal rcvdh_val               : t_sl_arr(       g_ai.nof_clocks - 1 downto 0);
  signal porth_dat               : t_rcvdh_dat_arr(g_ai.nof_clocks - 1 downto 0);  -- received transpose rewired data
  signal porth_val               : t_sl_arr(       g_ai.nof_clocks - 1 downto 0);

  -- Index related to g_ai.nof_adu, similar as for src_out
  signal dp_ovr                  : t_sl_arr(    0 to g_ai.nof_sp - 1);  -- [0:3]=[AB,AB,CD,CD] or [0:3]=[ 0, 0, CD,CD], vector OR of porth_dat overflow bits at t0,t1,t2,t3
  signal dp_dat                  : t_dp_dat_arr(0 to g_ai.nof_sp - 1);  -- [0:3]=[ A, B, C, D] or [0:3]=[ 0, 0,  C, D]
  signal dp_val                  : t_sl_arr(    0 to g_ai.nof_sp - 1);  -- [0:3]=[ A, B, C, D] or [0:3]=[ 0, 0,  C, D]
begin
  -- nof_clocks  use_lvds_clk  supported
  --   1           TRUE          YES
  --   1           FALSE         YES       , use dp_clk
  --   2           TRUE          YES
  --   2           FALSE         NO
  assert not(g_ai.use_lvds_clk = false and g_ai.nof_clocks > 1)
    report "aduh_pll.vhd: when the dp_clk is used to capture the LVDS data, then nof_clocks must be 1"
    severity FAILURE;

  -- nof_clocks  nof_adu  Supported
  --   1           1        YES
  --   1           2        YES
  --   2           1        NO
  --   2           2        YES
  assert not(g_ai.nof_clocks > g_ai.nof_adu)
    report "aduh_pll.vhd: when only one ADU is used then the nof clocks must be 1"
    severity FAILURE;

  -----------------------------------------------------------------------------
  -- Prepare lvdsh_pll input
  -----------------------------------------------------------------------------

  -- nof_clocks  nof_adu  Supported  LVDSH[1]  LVDSH[0]
  --   1           1        YES         -          oCD,  o = overflow bit
  --   1           2        YES         -       oABoCD
  --   2           2        YES       oAB          oCD

  input_one_lvdsh_pll : if g_ai.nof_clocks = 1 generate  -- Use 1 clock, so there can be 1 or 2 ADUs
    ADC_BI_D_CLK_RST <= lvdsh_clk_rst(0);  -- ADU lvdsh_clk phase reset control
    lvdsh_clk(0) <= ADC_BI_D_CLK when g_ai.use_lvds_clk = true else dp_clk;  -- Only use the lvdsh_clk from ADU on port CD or use the dp_clk as LVDS Rx reference clock
    one_adu : if g_ai.nof_adu = 1 generate  -- Use ADU on port CD
      no_ovr  : if g_ai.nof_ovr = 0 generate lvdsh_dat(0) <=                 ADC_BI_C & ADC_BI_D; end generate;
      gen_ovr : if g_ai.nof_ovr = 1 generate lvdsh_dat(0) <= ADC_BI_CD_OVR & ADC_BI_C & ADC_BI_D; end generate;
    end generate;
    two_adu : if g_ai.nof_adu = 2 generate  -- Use both ADUs
      no_ovr  : if g_ai.nof_ovr = 0 generate lvdsh_dat(0) <=                 ADC_BI_A & ADC_BI_B &                 ADC_BI_C & ADC_BI_D; end generate;
      gen_ovr : if g_ai.nof_ovr = 1 generate lvdsh_dat(0) <= ADC_BI_AB_OVR & ADC_BI_A & ADC_BI_B & ADC_BI_CD_OVR & ADC_BI_C & ADC_BI_D; end generate;
    end generate;
  end generate;

  input_two_lvdsh_pll : if g_ai.nof_clocks = 2 generate  -- Use 2 clocks, so there are also 2 ADU
    ADC_BI_A_CLK_RST <= lvdsh_clk_rst(1);
    ADC_BI_D_CLK_RST <= lvdsh_clk_rst(0);

    lvdsh_clk(1) <= ADC_BI_A_CLK;
    lvdsh_clk(0) <= ADC_BI_D_CLK;

    no_ovr  : if g_ai.nof_ovr = 0 generate lvdsh_dat(1) <=                 ADC_BI_A & ADC_BI_B; lvdsh_dat(0) <=                 ADC_BI_C & ADC_BI_D; end generate;
    gen_ovr : if g_ai.nof_ovr = 1 generate lvdsh_dat(1) <= ADC_BI_AB_OVR & ADC_BI_A & ADC_BI_B; lvdsh_dat(0) <= ADC_BI_CD_OVR & ADC_BI_C & ADC_BI_D; end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- One or two LVDS Handlers
  -----------------------------------------------------------------------------

  gen_clk : for I in g_ai.nof_clocks - 1 downto 0 generate
    u_lvdsh_pll : entity work.lvdsh_pll
    generic map (
      g_lvds_w           => c_lvdsh_dat_w,
      g_lvds_data_rate   => g_ai.lvds_data_rate,
      g_lvds_clk_freq    => c_lvds_clk_freq,
      g_lvds_clk_phase   => g_ai.lvds_clk_phase,
      g_use_lvds_clk_rst => g_ai.use_lvds_clk_rst,
      g_deser_factor     => g_ai.dp_deser_factor,
      g_use_dpa          => g_ai.use_dpa
    )
    port map (
      -- PHY LVDS Interface
      lvds_clk_rst      => lvdsh_clk_rst(I),
      lvds_clk          => lvdsh_clk(I),  -- the lvdsh_clk frequency is c_lvds_clk_freq
      lvds_dat          => lvdsh_dat(I),

      -- DP Streaming Interface
      dp_clk            => dp_clk,  -- the dp_clk frequency is g_lvds_data_rate / g_dp_deser_factor

      -- . Control
      dp_lvds_reset     => restart(I),
      dp_delay_settings => delay_settings((I + 1) * c_lvdsh_dat_w - 1 downto I * c_lvdsh_dat_w),
      dp_cda_settings   => cda_settings((I + 1) * c_lvdsh_dat_w - 1 downto I * c_lvdsh_dat_w),

      -- . Streaming
      dp_dat            => rcvdh_dat(I),
      dp_val            => rcvdh_val(I)
    );
  end generate;  -- gen_clk

  -----------------------------------------------------------------------------
  -- Handle lvdsh_pll output
  -----------------------------------------------------------------------------

  -- Example:
  --
  -- lvdsh_dat[16:0][  t0, t1, t2, t3]
  --                J= 3,  2,  1,  0
  -- I=16             67, 66, 65, 64 -- OVR_CD
  --   15             63, 62, 61, 60 -- Port C
  --   14             59, 58, 57, 56
  --   13             55, 54, 53, 52
  --   12             51, 50, 49, 48
  --   11             47, 46, 45, 44
  --   10             43, 42, 41, 40
  --    9             39, 38, 37, 36
  --    8             35, 34, 33, 32
  --    7             31, 30, 29, 28 -- Port D
  --    6             27, 26, 25, 24
  --    5             23, 22, 21, 20
  --    4             19, 18, 17, 16
  --    3             15, 14, 13, 12
  --    2             11, 10,  9,  8
  --    1              7,  6,  5,  4
  --    0              3,  2,  1,  0
  --
  -- So:
  --   nof_adu=1, nof_clocks=1, nof_ovr=1:  lvdsh    --> rcvdh_dat[0  ][ 67:0] =                                                       [0][OVR_CD(t0:t3), C(t0:t3)(b7:b0), D(t0:t3)(b7:b0)]
  --                                        p_rewire --> porth_dat[0  ][ 67:0] =                                                       [0][OVR_CD(t0:t3), C(b7:b0)(t0:t3), D(b7:b0)(t0:t3)]
  --
  -- and idem for the other supported cases:
  --   nof_adu=1, nof_clocks=1, nof_ovr=0:  lvdsh    --> rcvdh_dat[0  ][ 63:0] =                                                       [0][               C(t0:t3)(b7:b0), D(t0:t3)(b7:b0)]
  --                                        p_rewire --> porth_dat[0  ][ 63:0] =                                                       [0][               C(b7:b0)(t0:t3), D(b7:b0)(t0:t3)]
  --
  --   nof_adu=2, nof_clocks=1, nof_ovr=1:  lvdsh    --> rcvdh_dat[0  ][135:0] = [0][OVR_AB(t0:t3), A(t0:t3)(b7:b0), B(t0:t3)(b7:b0),      OVR_CD(t0:t3), C(t0:t3)(b7:b0), D(t0:t3)(b7:b0)]
  --                                        p_rewire --> porth_dat[0  ][135:0] = [0][OVR_AB(t0:t3), A(b7:b0)(t0:t3), B(b7:b0)(t0:t3),      OVR_CD(t0:t3), C(b7:b0)(t0:t3), D(b7:b0)(t0:t3)]
  --
  --   nof_adu=2, nof_clocks=1, nof_ovr=0:  lvdsh    --> rcvdh_dat[0  ][127:0] = [0][               A(t0:t3)(b7:b0), B(t0:t3)(b7:b0)                      C(t0:t3)(b7:b0), D(t0:t3)(b7:b0)]
  --                                        p_rewire --> porth_dat[0  ][127:0] = [0][               A(b7:b0)(t0:t3), B(b7:b0)(t0:t3)                      C(b7:b0)(t0:t3), D(b7:b0)(t0:t3)]
  --
  --   nof_adu=2, nof_clocks=2, nof_ovr=1:  lvdsh    --> rcvdh_dat[1:0][ 67:0] = [1][OVR_AB(t0:t3), A(t0:t3)(b7:b0), B(t0:t3)(b7:b0)], [0][OVR_CD(t0:t3), C(t0:t3)(b7:b0), D(t0:t3)(b7:b0)]
  --                                        p_rewire --> porth_dat[1:0][ 67:0] = [1][OVR_AB(t0:t3), A(b7:b0)(t0:t3), B(b7:b0)(t0:t3)], [0][OVR_CD(t0:t3), C(b7:b0)(t0:t3), D(b7:b0)(t0:t3)]
  --
  --   nof_adu=2, nof_clocks=2, nof_ovr=0:  lvdsh    --> rcvdh_dat[1:0][ 63:0] = [1][               A(t0:t3)(b7:b0), B(t0:t3)(b7:b0)], [0][               C(t0:t3)(b7:b0), D(t0:t3)(b7:b0)]
  --                                        p_rewire --> porth_dat[1:0][ 63:0] = [1][               A(b7:b0)(t0:t3), B(b7:b0)(t0:t3)], [0][               C(b7:b0)(t0:t3), D(b7:b0)(t0:t3)]

  -- * Generate transpose for each supported case, combining cases makes the code less clear because various combinations are possible
  -- * Wire porth_dat(g_ai.nof_clocks-1:0) to dp_dat(0:g_ai.nof_adu-1)
  -- * Use vector OR of the porth overflow bits per ADU
  -- * Wire dp_ovr and dp_dat to src_out for each supported case
  --   . src_out[0:3] = [A,B,C,D] if nof_adu=2
  --   . src_out[0:3] = [0,0,C,D] if nof_adu=1
  --     Each data field contains dp_deser_factor=4 number of ADC samples that are each g_ai.port_w=8 bits wide,
  --     so e.g. for port A: src_out[0][31:0]=[A0,A1,A2,A3] whereby A0 is the first sample in time, A1 the next, etc.
  --     If the OVR_AC bit is used then the OR of the dp_deser_factor=4 overflow bits is passed on via src_out[0][32]
  --     and via src_out[1][32]
  porth_val <= rcvdh_val;
  gen_dp_1_1_1 : if g_ai.nof_adu = 1 and g_ai.nof_clocks = 1 and g_ai.nof_ovr = 1 generate
    porth_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  ) <=           rcvdh_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  );  -- bit  OVR_CD
    porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port C
    porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port D
    -- port [0:1]=[A,B] is not used
    dp_ovr(2) <= dp_ovr(3);  -- bit  OVR_C = OVR_CD
    dp_ovr(3) <= vector_or(porth_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w));  -- bit  OVR_D = OVR_CD
    dp_dat(2) <=           porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w);  -- port C
    dp_dat(3) <=           porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w);  -- port D
    dp_val(2) <=           porth_val(0);  -- port C
    dp_val(3) <=           porth_val(0);  -- port D

    src_out(0)       <= c_dp_sosi_rst;
    src_out(1)       <= c_dp_sosi_rst;
    src_out(2).data  <= RESIZE_DP_DATA(dp_ovr(2) & dp_dat(2));  -- port C
    src_out(3).data  <= RESIZE_DP_DATA(dp_ovr(3) & dp_dat(3));  -- port D
    src_out(2).valid <=                            dp_val(2);  -- port C
    src_out(3).valid <=                            dp_val(3);  -- port D
  end generate;

  gen_dp_1_1_0 : if g_ai.nof_adu = 1 and g_ai.nof_clocks = 1 and g_ai.nof_ovr = 0 generate
    porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port C
    porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port D
    -- port [0:1]=[A,B] is not used
    dp_dat(2) <= porth_dat(0)(2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w);  -- port C
    dp_dat(3) <= porth_dat(0)(1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w);  -- port D
    dp_val(2) <= porth_val(0);  -- port C
    dp_val(3) <= porth_val(0);  -- port D

    src_out(0)       <= c_dp_sosi_rst;
    src_out(1)       <= c_dp_sosi_rst;
    src_out(2).data  <= RESIZE_DP_DATA(dp_dat(2));  -- port C
    src_out(3).data  <= RESIZE_DP_DATA(dp_dat(3));  -- port D
    src_out(2).valid <=                dp_val(2);  -- port C
    src_out(3).valid <=                dp_val(3);  -- port D
  end generate;

  gen_dp_2_1_1 : if g_ai.nof_adu = 2 and g_ai.nof_clocks = 1 and g_ai.nof_ovr = 1 generate
    porth_dat(0)(g_ai.dp_deser_factor + 4 * c_dp_dat_w   downto 4 * c_dp_dat_w + 1) <=           rcvdh_dat(0)(g_ai.dp_deser_factor + 4 * c_dp_dat_w   downto 4 * c_dp_dat_w + 1);  -- bit  OVR_AB
    porth_dat(0)(                       4 * c_dp_dat_w   downto 3 * c_dp_dat_w + 1) <= transpose(rcvdh_dat(0)(                       4 * c_dp_dat_w   downto 3 * c_dp_dat_w + 1), g_ai.dp_deser_factor, g_ai.port_w);  -- port A
    porth_dat(0)(                       3 * c_dp_dat_w   downto 2 * c_dp_dat_w + 1) <= transpose(rcvdh_dat(0)(                       3 * c_dp_dat_w   downto 2 * c_dp_dat_w + 1), g_ai.dp_deser_factor, g_ai.port_w);  -- port B
    porth_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  ) <=           rcvdh_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  );  -- bit  OVR_CD
    porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port C
    porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port D
    dp_ovr(0) <= dp_ovr(1);  -- bit  OVR_A = OVR_AB
    dp_ovr(1) <= vector_or(porth_dat(0)(g_ai.dp_deser_factor + 4 * c_dp_dat_w   downto 4 * c_dp_dat_w + 1));  -- bit  OVR_B = OVR_AB
    dp_ovr(2) <= dp_ovr(3);  -- bit  OVR_C = OVR_CD
    dp_ovr(3) <= vector_or(porth_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  ));  -- bit  OVR_D = OVR_CD
    dp_dat(0) <=           porth_dat(0)(                       4 * c_dp_dat_w   downto 3 * c_dp_dat_w + 1);  -- port A
    dp_dat(1) <=           porth_dat(0)(                       3 * c_dp_dat_w   downto 2 * c_dp_dat_w + 1);  -- port B
    dp_dat(2) <=           porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  );  -- port C
    dp_dat(3) <=           porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  );  -- port D
    dp_val(0) <=           porth_val(0);  -- port A
    dp_val(1) <=           porth_val(0);  -- port B
    dp_val(2) <=           porth_val(0);  -- port C
    dp_val(3) <=           porth_val(0);  -- port D

    src_out(0).data  <= RESIZE_DP_DATA(dp_ovr(0) & dp_dat(0));  -- port A
    src_out(1).data  <= RESIZE_DP_DATA(dp_ovr(1) & dp_dat(1));  -- port B
    src_out(2).data  <= RESIZE_DP_DATA(dp_ovr(2) & dp_dat(2));  -- port C
    src_out(3).data  <= RESIZE_DP_DATA(dp_ovr(3) & dp_dat(3));  -- port D
    src_out(0).valid <=                            dp_val(0);  -- port A
    src_out(1).valid <=                            dp_val(1);  -- port B
    src_out(2).valid <=                            dp_val(2);  -- port C
    src_out(3).valid <=                            dp_val(3);  -- port D
  end generate;

  gen_dp_2_1_0 : if g_ai.nof_adu = 2 and g_ai.nof_clocks = 1 and g_ai.nof_ovr = 0 generate
    porth_dat(0)(                       4 * c_dp_dat_w - 1 downto 3 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       4 * c_dp_dat_w - 1 downto 3 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port A
    porth_dat(0)(                       3 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       3 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port B
    porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port C
    porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port D
    dp_dat(0) <= porth_dat(0)(4 * c_dp_dat_w - 1 downto 3 * c_dp_dat_w);  -- port A
    dp_dat(1) <= porth_dat(0)(3 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w);  -- port B
    dp_dat(2) <= porth_dat(0)(2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w);  -- port C
    dp_dat(3) <= porth_dat(0)(1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w);  -- port D
    dp_val(0) <= porth_val(0);  -- port A
    dp_val(1) <= porth_val(0);  -- port B
    dp_val(2) <= porth_val(0);  -- port C
    dp_val(3) <= porth_val(0);  -- port D

    src_out(0).data  <= RESIZE_DP_DATA(dp_dat(0));  -- port A
    src_out(1).data  <= RESIZE_DP_DATA(dp_dat(1));  -- port B
    src_out(2).data  <= RESIZE_DP_DATA(dp_dat(2));  -- port C
    src_out(3).data  <= RESIZE_DP_DATA(dp_dat(3));  -- port D
    src_out(0).valid <=                dp_val(0);  -- port A
    src_out(1).valid <=                dp_val(1);  -- port B
    src_out(2).valid <=                dp_val(2);  -- port C
    src_out(3).valid <=                dp_val(3);  -- port D
  end generate;

  gen_dp_2_2_1 : if g_ai.nof_adu = 2 and g_ai.nof_clocks = 2 and g_ai.nof_ovr = 1 generate
    porth_dat(1)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  ) <=           rcvdh_dat(1)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  );  -- bit  OVR_AB
    porth_dat(1)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ) <= transpose(rcvdh_dat(1)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port A
    porth_dat(1)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ) <= transpose(rcvdh_dat(1)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port B
    porth_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  ) <=           rcvdh_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w  );  -- bit  OVR_CD
    porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port C
    porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port D
    dp_ovr(0) <= dp_ovr(1);  -- bit  OVR_A = OVR_AB
    dp_ovr(1) <= vector_or(porth_dat(1)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w));  -- bit  OVR_B = OVR_AB
    dp_ovr(2) <= dp_ovr(3);  -- bit  OVR_C = OVR_CD
    dp_ovr(3) <= vector_or(porth_dat(0)(g_ai.dp_deser_factor + 2 * c_dp_dat_w - 1 downto 2 * c_dp_dat_w));  -- bit  OVR_D = OVR_CD
    dp_dat(0) <=           porth_dat(1)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w);  -- port A
    dp_dat(1) <=           porth_dat(1)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w);  -- port B
    dp_dat(2) <=           porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w);  -- port C
    dp_dat(3) <=           porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w);  -- port D
    dp_val(0) <=           porth_val(1);  -- port A
    dp_val(1) <=           porth_val(1);  -- port B
    dp_val(2) <=           porth_val(0);  -- port C
    dp_val(3) <=           porth_val(0);  -- port D

    src_out(0).data  <= RESIZE_DP_DATA(dp_ovr(0) & dp_dat(0));  -- port A
    src_out(1).data  <= RESIZE_DP_DATA(dp_ovr(1) & dp_dat(1));  -- port B
    src_out(2).data  <= RESIZE_DP_DATA(dp_ovr(2) & dp_dat(2));  -- port C
    src_out(3).data  <= RESIZE_DP_DATA(dp_ovr(3) & dp_dat(3));  -- port D
    src_out(0).valid <=                            dp_val(0);  -- port A
    src_out(1).valid <=                            dp_val(1);  -- port B
    src_out(2).valid <=                            dp_val(2);  -- port C
    src_out(3).valid <=                            dp_val(3);  -- port D
  end generate;

  gen_dp_2_2_0 : if g_ai.nof_adu = 2 and g_ai.nof_clocks = 2 and g_ai.nof_ovr = 0 generate
    porth_dat(1)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ) <= transpose(rcvdh_dat(1)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port A
    porth_dat(1)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ) <= transpose(rcvdh_dat(1)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port B
    porth_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port C
    porth_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ) <= transpose(rcvdh_dat(0)(                       1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w  ), g_ai.dp_deser_factor, g_ai.port_w);  -- port D
    dp_dat(0) <= porth_dat(1)(2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w);  -- port A
    dp_dat(1) <= porth_dat(1)(1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w);  -- port B
    dp_dat(2) <= porth_dat(0)(2 * c_dp_dat_w - 1 downto 1 * c_dp_dat_w);  -- port C
    dp_dat(3) <= porth_dat(0)(1 * c_dp_dat_w - 1 downto 0 * c_dp_dat_w);  -- port D
    dp_val(0) <= porth_val(1);  -- port A
    dp_val(1) <= porth_val(1);  -- port B
    dp_val(2) <= porth_val(0);  -- port C
    dp_val(3) <= porth_val(0);  -- port D

    src_out(0).data  <= RESIZE_DP_DATA(dp_dat(0));  -- port A
    src_out(1).data  <= RESIZE_DP_DATA(dp_dat(1));  -- port B
    src_out(2).data  <= RESIZE_DP_DATA(dp_dat(2));  -- port C
    src_out(3).data  <= RESIZE_DP_DATA(dp_dat(3));  -- port D
    src_out(0).valid <=                dp_val(0);  -- port A
    src_out(1).valid <=                dp_val(1);  -- port B
    src_out(2).valid <=                dp_val(2);  -- port C
    src_out(3).valid <=                dp_val(3);  -- port D
  end generate;

end str;
