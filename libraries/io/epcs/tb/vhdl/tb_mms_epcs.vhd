--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

--Note: This simulation requires:
-- 1) optimization to be enabled
-- 2) 1 femtosecond resolution

-- Run for 10ms.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;

entity tb_mms_epcs is
end entity tb_mms_epcs;

architecture str of tb_mms_epcs is
  constant c_sim                   : boolean := true;
  constant c_mm_clk_period         : time    := 8 ns;
  constant c_epcs_clk_period       : time    := 50 ns;

  constant c_offset_epcs_addr      : natural := 0;
  constant c_offset_epcs_rden      : natural := 1;
  constant c_offset_epcs_read      : natural := 2;
  constant c_offset_epcs_write     : natural := 3;

  -- See tb/m25p128_model/lib/TimingData.vhd
  constant c_m25p128_page_program_time : time := 2.5 ms;

  signal mm_clk                : std_logic := '0';
  signal mm_rst                : std_logic;

  signal epcs_clk              : std_logic := '0';
  signal epcs_rst              : std_logic;

  signal epcs_mosi             : t_mem_mosi := c_mem_mosi_rst;
  signal epcs_miso             : t_mem_miso;

  signal dpmm_ctrl_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal dpmm_ctrl_miso        : t_mem_miso;

  signal dpmm_data_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal dpmm_data_miso        : t_mem_miso;

  signal mmdp_ctrl_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal mmdp_ctrl_miso        : t_mem_miso;

  signal mmdp_data_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal mmdp_data_miso        : t_mem_miso;

  signal tb_rd_usedw           : std_logic_vector(c_word_w - 1 downto 0);
  signal tb_rd_data            : std_logic_vector(c_word_w - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------

  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  mm_clk <= not mm_clk after c_mm_clk_period / 2;  -- MM clock (125 MHz)

  epcs_rst <= '1', '0' after c_epcs_clk_period * 7;
  epcs_clk <= not epcs_clk after c_epcs_clk_period / 2;  -- EPCS clock for ASMI_PARALLEL, 20MHz

  ----------------------------------------------------------------------------
  -- Stimuli for MM reg_input slave port
  ----------------------------------------------------------------------------

  p_reg_input_stimuli : process
  begin
    epcs_mosi      <= c_mem_mosi_rst;

    dpmm_ctrl_mosi <= c_mem_mosi_rst;
    dpmm_data_mosi <= c_mem_mosi_rst;

    mmdp_ctrl_mosi <= c_mem_mosi_rst;
    mmdp_data_mosi <= c_mem_mosi_rst;

    -- m25p128 sim model requires this delay before it allows write operations, see timingdata.vhd
    wait for 5 ms;

    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 1);

    -- Fill up the EPCS write FIFO (via dp_fifo_from_mm = mmdp) with one EPCS page worth of data (decimal(0x22334455))
    for i in 0 to 63 loop
      proc_mem_mm_bus_wr(0, 573785173, mm_clk, mmdp_data_mosi);
      proc_common_wait_some_cycles(mm_clk, 4);
    end loop;

    proc_common_wait_some_cycles(mm_clk, 1500);

    -- Send the address we want to write to
    proc_mem_mm_bus_wr(c_offset_epcs_addr, 300000, mm_clk, epcs_mosi);
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Now write WRITE command
    proc_mem_mm_bus_wr(c_offset_epcs_write, 1, mm_clk, epcs_mosi);
    proc_common_wait_some_cycles(mm_clk, 10);

    wait for c_m25p128_page_program_time;

    -- First assert READ_ENABLE
    proc_mem_mm_bus_wr(c_offset_epcs_rden, 1, mm_clk, epcs_mosi);
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Now write READ command. The EPCS FIFO will fill up with read data
    proc_mem_mm_bus_wr(c_offset_epcs_read, 1, mm_clk, epcs_mosi);
    proc_common_wait_some_cycles(mm_clk, 10);

    wait for 1000 us;

    -- Pre-read usedw for the loop
    proc_mem_mm_bus_rd(0, mm_clk, dpmm_ctrl_mosi);
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    tb_rd_usedw <= dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0);
    proc_common_wait_some_cycles(mm_clk, 10);

    while unsigned(tb_rd_usedw) > 0 loop
      wait until rising_edge(mm_clk);

      proc_mem_mm_bus_rd(0, mm_clk, dpmm_ctrl_mosi);
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);

      tb_rd_usedw <= dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0);
      proc_common_wait_some_cycles(mm_clk, 10);

      for i in 0 to TO_UINT(tb_rd_usedw) loop
        proc_common_wait_some_cycles(mm_clk, 10);

        proc_mem_mm_bus_rd(0, mm_clk, dpmm_data_mosi);
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);

        tb_rd_data <= dpmm_data_miso.rddata(c_word_w - 1 downto 0);
      end loop;  -- read usedw words
    end loop;  -- while usedw>0

    wait;
  end process;

  u_mms_epcs: entity work.mms_epcs
  port map (
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    epcs_clk           => epcs_clk,

    epcs_mosi          => epcs_mosi,
    epcs_miso          => epcs_miso,

    dpmm_ctrl_mosi     => dpmm_ctrl_mosi,
    dpmm_ctrl_miso     => dpmm_ctrl_miso,

    dpmm_data_mosi     => dpmm_data_mosi,
    dpmm_data_miso     => dpmm_data_miso,

    mmdp_ctrl_mosi     => mmdp_ctrl_mosi,
    mmdp_ctrl_miso     => mmdp_ctrl_miso,

    mmdp_data_mosi     => mmdp_data_mosi,
    mmdp_data_miso     => mmdp_data_miso
  );
end architecture str;
