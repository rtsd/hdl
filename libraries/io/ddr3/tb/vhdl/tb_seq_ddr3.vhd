-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the seq_ddr3 unit
--           To be used in conjunction with python testscript: ../python/tc_seq_ddr3.py
--
--
-- Usage:
--   > as 8
--   > run -all
--   > run python script in separate terminal: "python tc_seq_ddr3.py --unb 0 --fn 0 --sim"
--   > Stop the simulation manually in Modelsim by pressing the stop-button.
--   > Evalute the WAVE window.

library IEEE, tech_ddr_lib, common_lib, mm_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use work.ddr3_pkg.all;

entity tb_seq_ddr3 is
  generic (
    g_wr_chunksize    : positive := 240;
    g_wr_nof_chunks   : positive := 1;
    g_rd_chunksize    : positive := 16;
    g_rd_nof_chunks   : positive := 15;
    g_gapsize         : natural  := 16;
    g_nof_blocks      : positive := 5
 );
end tb_seq_ddr3;

architecture tb of tb_seq_ddr3 is
  constant c_sim                : boolean := true;

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period      : time := 8 ns;  -- 200 ps;
  constant c_dp_clk_period      : time := 5 ns;
  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic := '1';
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  signal reg_diag_bg_mosi          : t_mem_mosi;
  signal reg_diag_bg_miso          : t_mem_miso;

  signal ram_diag_bg_mosi          : t_mem_mosi;
  signal ram_diag_bg_miso          : t_mem_miso;

  signal ram_diag_data_buf_mosi : t_mem_mosi;
  signal ram_diag_data_buf_miso : t_mem_miso;

  signal reg_diag_data_buf_mosi : t_mem_mosi;
  signal reg_diag_data_buf_miso : t_mem_miso;

  -- DUT
  signal reg_ddr3_mosi             : t_mem_mosi := c_mem_mosi_rst;
  signal reg_ddr3_miso             : t_mem_miso := c_mem_miso_rst;

  -- Compose the Constants for the DUT
  constant c_ddr3_seq_conf           : t_ddr3_seq := (g_wr_chunksize,
                                                      g_wr_nof_chunks,
                                                      g_rd_chunksize,
                                                      g_rd_nof_chunks,
                                                      g_gapsize,
                                                      g_nof_blocks);

  constant c_blocksize               : positive := g_wr_nof_chunks * g_wr_chunksize;
  constant c_page_size               : positive := c_blocksize * g_nof_blocks;

  constant c_ddr                     : t_c_ddr3_phy := c_ddr3_phy_4g;
  constant c_mts                     : natural := 1066;  -- 1066; --800
  constant c_phy                     : natural := 1;
  constant c_data_w                  : natural := 64;

  constant c_ctrl_ref_clk_period     : time  := 5000 ps;

  -- Custom definitions of constants
  constant c_bg_block_len           : natural  := c_page_size;
  constant c_db_block_len           : natural  := c_page_size;

  -- Configuration of the block generator:
  constant c_bg_nof_output_streams  : positive := 1;
  constant c_bg_buf_dat_w           : positive := c_nof_complex * 8;
  constant c_bg_buf_adr_w           : positive := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_prefix    : string   := "../../../src/hex/tb_bg_dat";
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, 1, 1);

  -- Configuration of the databuffers:
  constant c_db_nof_streams         : positive := 1;
  constant c_db_data_w              : positive := c_diag_db_max_data_w;
  constant c_db_buf_nof_data        : positive := c_db_block_len;
  constant c_db_buf_use_sync        : boolean  := false;
  constant c_db_data_type           : t_diag_data_type_enum := e_data;

  signal bg_siso_arr                : t_dp_siso_arr(1 - 1 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(1 - 1 downto 0);

  signal out_sosi_arr               : t_dp_sosi_arr(1 - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_siso_arr               : t_dp_siso_arr(1 - 1 downto 0) := (others => c_dp_siso_rdy);

  -- Siganls to connect the memory driver with the mm register interface
  signal ctlr_ref_clk         : std_logic    := '0';
  signal ctlr_rst             : std_logic    := '1';
  signal ctlr_gen_clk         : std_logic;
  signal ctlr_gen_rst         : std_logic;

  signal ctlr_rdy             : std_logic;
  signal ctlr_init_done       : std_logic;

  signal dvr_start_addr       : t_ddr3_addr;
  signal dvr_end_addr         : t_ddr3_addr;

  signal dvr_en               : std_logic;
  signal dvr_wr_not_rd        : std_logic;
  signal dvr_done             : std_logic;

  -- Signals to interface with the DDR3 memory model.
  signal phy_in               : t_tech_ddr3_phy_in;
  signal phy_io               : t_tech_ddr3_phy_io;
  signal phy_ou               : t_tech_ddr3_phy_ou;

  signal ras_n : std_logic_vector(0 downto 0);
  signal cas_n : std_logic_vector(0 downto 0);
  signal we_n  : std_logic_vector(0 downto 0);
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ctlr_ref_clk <= not ctlr_ref_clk after c_ctrl_ref_clk_period / 2;
  ctlr_rst     <= '1', '0' after c_ctrl_ref_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  -- TB
  u_mm_file_reg_diag_bg       : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_BG")
                                        port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

  u_mm_file_ram_diag_bg       : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_BG")
                                        port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

  u_mm_file_ram_diag_data_buf : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_DATA_BUFFER")
                                        port map(mm_rst, mm_clk, ram_diag_data_buf_mosi, ram_diag_data_buf_miso);

  u_mm_file_reg_diag_data_buf : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_DATA_BUFFER")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_mosi, reg_diag_data_buf_miso);

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams        => c_bg_nof_output_streams,
    g_buf_dat_w          => c_bg_buf_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    en_sync              => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi     => reg_diag_bg_mosi,
    reg_bg_ctrl_miso     => reg_diag_bg_miso,
    ram_bg_data_mosi     => ram_diag_bg_mosi,
    ram_bg_data_miso     => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr         => bg_siso_arr,
    out_sosi_arr         => bg_sosi_arr
  );

  ----------------------------------------------------------------------------
  -- DUT: Device Under Test
  ----------------------------------------------------------------------------
  u_dut: entity work.seq_ddr3
  generic map(
    g_ddr      => c_ddr,
    g_mts      => c_mts,
    g_phy      => c_phy,
    g_data_w   => c_data_w,
    g_ddr3_seq => c_ddr3_seq_conf
  )
  port map (
    ctlr_ref_clk       => ctlr_ref_clk,
    ctlr_rst           => ctlr_rst,

    ctlr_gen_clk       => open,
    ctlr_gen_rst       => open,

    wr_clk             => dp_clk,
    wr_rst             => dp_rst,

    wr_sosi            => bg_sosi_arr(0),
    wr_siso            => bg_siso_arr(0),

    flush_ena          => '0',

    rd_sosi            => out_sosi_arr(0),
    rd_siso            => out_siso_arr(0),

    rd_clk             => dp_clk,
    rd_rst             => dp_rst,

    ddr3_in            => phy_in,
    ddr3_io            => phy_io,
    ddr3_ou            => phy_ou
  );

  gen_u_800_model : if c_phy > 0 generate
    u_4gb_800_ddr3_model : component alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en
    generic map (
      MEM_IF_ADDR_WIDTH            => 15,
      MEM_IF_ROW_ADDR_WIDTH        => 15,
      MEM_IF_COL_ADDR_WIDTH        => 10,
      MEM_IF_CS_PER_RANK           => 1,
      MEM_IF_CONTROL_WIDTH         => 1,
      MEM_IF_DQS_WIDTH             => 8,
      MEM_IF_CS_WIDTH              => 2,
      MEM_IF_BANKADDR_WIDTH        => 3,
      MEM_IF_DQ_WIDTH              => 64,
      MEM_IF_CK_WIDTH              => 2,
      MEM_IF_CLK_EN_WIDTH          => 2,
      DEVICE_WIDTH                 => 1,
      MEM_TRCD                     => 6,
      MEM_TRTP                     => 3,
      MEM_DQS_TO_CLK_CAPTURE_DELAY => 100,
      MEM_CLK_TO_DQS_CAPTURE_DELAY => 100000,
      MEM_IF_ODT_WIDTH             => 2,
      MEM_MIRROR_ADDRESSING_DEC    => 0,
      MEM_REGDIMM_ENABLED          => false,
      DEVICE_DEPTH                 => 1,
      MEM_GUARANTEED_WRITE_INIT    => false,
      MEM_VERBOSE                  => true,
      MEM_INIT_EN                  => false,
      MEM_INIT_FILE                => "",
      DAT_DATA_WIDTH               => 32
    )
    port map (
      mem_a       => phy_ou.a(c_ddr.a_w - 1 downto 0),
      mem_ba      => phy_ou.ba,
      mem_ck      => phy_ou.ck,
      mem_ck_n    => phy_ou.ck_n,
      mem_cke     => phy_ou.cke(c_ddr.cs_w - 1 downto 0),
      mem_cs_n    => phy_ou.cs_n(c_ddr.cs_w - 1 downto 0),
      mem_dm      => phy_ou.dm,
      mem_ras_n   => ras_n,
      mem_cas_n   => cas_n,
      mem_we_n    => we_n,
      mem_reset_n => phy_ou.reset_n,
      mem_dq      => phy_io.dq,
      mem_dqs     => phy_io.dqs,
      mem_dqs_n   => phy_io.dqs_n,
      mem_odt     => phy_ou.odt
    );

    ras_n(0) <= phy_ou.ras_n;
    cas_n(0) <= phy_ou.cas_n;
    we_n(0)  <= phy_ou.we_n;
  end generate;

  ----------------------------------------------------------------------------
  -- Sink: data buffer real
  ----------------------------------------------------------------------------
  u_data_buf : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams     => c_db_nof_streams,
    g_data_type       => c_db_data_type,
    g_data_w          => c_db_data_w,
    g_buf_nof_data    => c_db_buf_nof_data,
    g_buf_use_sync    => c_db_buf_use_sync
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,
     -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_mosi,
    ram_data_buf_miso => ram_diag_data_buf_miso,
    reg_data_buf_mosi => reg_diag_data_buf_mosi,
    reg_data_buf_miso => reg_diag_data_buf_miso,
    -- ST interface
    in_sync           => OPEN,
    in_sosi_arr       => out_sosi_arr
  );
end tb;
