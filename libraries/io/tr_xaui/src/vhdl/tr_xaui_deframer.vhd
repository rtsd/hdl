-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tr_xaui_deframer is
  port (
    rx_clk     : in std_logic;
    rx_rst     : in std_logic;

    xgmii_rx_d : in std_logic_vector(c_xgmii_data_w - 1 downto 0);
    xgmii_rx_c : in std_logic_vector(c_xgmii_nof_lanes - 1 downto 0);

    src_out    : out t_dp_sosi
  );
end tr_xaui_deframer;

architecture rtl of tr_xaui_deframer is
  constant c_xgmii_c_start_lo : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0) := c_xgmii_c_start(c_xgmii_nof_lanes / 2 - 1 downto 0);  -- 0x1
  constant c_xgmii_c_term_lo  : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0) := c_xgmii_c_term (c_xgmii_nof_lanes / 2 - 1 downto 0);  -- 0x8

  -- We need to look at the data when we receive the c_xgmii_c_start_lo as control, as the control bits happen to be the same during lane alignment.
  -- In that case, the incoming data word is 0x01000009C on both the LS and MS word, and we don't want to mistake that for data.
  constant c_xgmii_d_start_lo  : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0) :=  c_xgmii_d_start(c_xgmii_data_w / 2 - 1 downto 0);  -- 0x000000FB

  type t_state_enum is (s_init, s_gap, s_data, s_data_misaligned);

  signal prev_state         : t_state_enum;
  signal state              : t_state_enum;
  signal nxt_state          : t_state_enum;

  -- XGMII RX data & control IN:
  signal xgmii_rx_d_hi      : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);
  signal xgmii_rx_d_lo      : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);

  signal prev_xgmii_rx_d_hi : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);

  signal xgmii_rx_c_hi      : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0);
  signal xgmii_rx_c_lo      : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0);

  -- RX data & valid OUT:
  signal rx_data_hi         : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);
  signal rx_data_lo         : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);
  signal rx_data_val        : std_logic;

  signal nxt_rx_data_hi     : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);
  signal nxt_rx_data_lo     : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);
  signal nxt_rx_data_val    : std_logic;
begin
  src_out.data(c_xgmii_data_w - 1 downto 0) <= rx_data_hi & rx_data_lo;
  src_out.valid                           <= rx_data_val;

  xgmii_rx_d_hi <= xgmii_rx_d(c_xgmii_data_w  - 1 downto c_xgmii_data_w / 2);
  xgmii_rx_d_lo <= xgmii_rx_d(c_xgmii_data_w / 2 - 1 downto 0);

  xgmii_rx_c_hi <= xgmii_rx_c(c_xgmii_nof_lanes  - 1 downto c_xgmii_nof_lanes / 2);
  xgmii_rx_c_lo <= xgmii_rx_c(c_xgmii_nof_lanes / 2 - 1 downto 0);

  -- FSM function if word boundary was 64b (it's not):
  -- =================================================
  -- xgmii_rx_d   I  I  S  D0 D1 D2 D3 T I I I S  D4  D5

  -- nxt_rx_sosi: G  G  G  D0 D1 D2 D3 G G G G G  D4  D5
  -- State  :     G  G  G  D  D  D  D  G G G G G  D   D

  -- FSM function, word boundary is 32b, tx_data_lo aligned to rx_data_lo:
  -- =====================================================================
  -- nxt_rx_sosi_hi = xgmii_rx_d_hi (during s_data)
  -- nxt_rx_sosi_lo = xgmii_rx_d_lo (during s_data)
  -- -----------------------
  -- xgmii_rx_d_hi:  I_hi  I_hi  S_hi  D0_hi D1_hi D2_hi D3_hi T_hi I_hi I_hi I_hi S_hi  D4_hi  D5_hi
  -- xgmii_rx_d_lo:  I_lo  I_lo  S_lo  D0_lo D1_lo D2_lo D3_lo T_lo I_lo I_lo I_lo S_lo  D4_lo  D5_lo

  -- nxt_rx_data_hi: G     G     G     D0_hi D1_hi D2_hi D3_hi G_hi G_hi G_hi G_hi G_hi  D4_hi  D5_hi
  -- nxt_rx_data_lo: G     G     G     D0_lo D1_lo D2_lo D3_lo G_lo G_lo G_lo G_lo G_lo  D4_lo  D5_lo
  -- State  :        G     G     G     D     D     D     D     D    G    G    G    G     D      D
  --                             ^     ^                       ^
  --                             |     |                       TERM(LS portion) detected: nxt_rx_data = GAP during last s_data
  --                             |     nxt_rx_data<=DATA during all s_data cycles but the last
  --                             START(LS portion) detected on xgmii_rx_d_lo: nxt_state <= s_data

  -- FSM function, word boundary is 32b, tx_data_lo aligned to rx_data_hi:
  -- =====================================================================
  -- We need to use the previous rx_data_hi:
  -- nxt_rx_sosi_hi = xgmii_rx_d_lo      (during s_data_misaligned)
  -- nxt_rx_sosi_lo = prev_xgmii_rx_d_hi (during s_data_misaligned)
  -- ---------------------------
  -- xgmii_rx_d_hi:       I_lo  S_lo  D0_lo D1_lo D2_lo D3_lo T_lo  I_lo I_lo I_lo S_lo D4_lo D5_lo
  -- xgmii_rx_d_lo:       I_hi  I_hi  S_hi  D0_hi D1_hi D2_hi D3_hi T_hi I_hi I_hi I_hi S_hi  D4_hi D5_hi
  -- prev_xgmii_rx_d_hi:        I_lo  S_lo  D0_lo D1_lo D2_lo D3_lo T_lo I_lo I_lo I_lo S_lo  D4_lo D5_lo

  -- nxt_rx_data_hi       G     G     G     D0_hi D1_hi D2_hi D3_hi G_hi G_hi G_hi G_hi G_hi  D4_hi D5_hi
  -- nxt_rx_data_lo       G     G     G     D0_lo D1_lo D2_lo D3_lo G_lo G_lo G_lo G_lo G_lo  D4_lo D5_lo
  -- State  :             G     G     D     D     D     D     D     G    G    G    G    G     D     D
  --                            ^     ^                       ^
  --                            |     |                       TERM(LS portion) detected: nxt_rx_data = DATA during last s_data_misaligned
  --                            |     nxt_rx_data<=GAP during first s_data_misaligned cycle (prev_state=s_gap)
  --                            START(LS portion) detected on xgmii_rx_d_hi: nxt_state <= s_data_misaligned

  p_clk : process(rx_clk, rx_rst)
  begin
    if rx_rst = '1' then
      state               <= s_init;
      prev_state          <= s_init;
      prev_xgmii_rx_d_hi  <= (others => '0');
      rx_data_hi          <= (others => '0');
      rx_data_lo          <= (others => '0');
      rx_data_val         <= '0';
    elsif rising_edge(rx_clk) then
      state               <= nxt_state;
      prev_state          <= state;
      prev_xgmii_rx_d_hi  <= xgmii_rx_d_hi;
      rx_data_hi          <= nxt_rx_data_hi;
      rx_data_lo          <= nxt_rx_data_lo;
      rx_data_val         <= nxt_rx_data_val;
    end if;
  end process;

  -- Note: we only need the XGMII control as input to determine gaps and (mis)alignment
  p_state : process(state, prev_state, prev_xgmii_rx_d_hi, xgmii_rx_c, xgmii_rx_c_lo, xgmii_rx_c_hi, xgmii_rx_d_hi, xgmii_rx_d_lo)
  begin
    nxt_state  <= state;

  case state is

    when s_gap =>
      nxt_rx_data_hi  <= (others => '0');
      nxt_rx_data_lo  <= (others => '0');
      nxt_rx_data_val <= '0';
      if xgmii_rx_c_lo = c_xgmii_c_start_lo and xgmii_rx_d_lo = c_xgmii_d_start_lo then
        -- Data happens to be aligned correctly
        nxt_state <= s_data;
      elsif xgmii_rx_c_hi = c_xgmii_c_start_lo and xgmii_rx_d_hi = c_xgmii_d_start_lo then
        -- Data misaligned
        nxt_state <= s_data_misaligned;
      end if;

    when s_data =>
      nxt_rx_data_hi  <= xgmii_rx_d_hi;
      nxt_rx_data_lo  <= xgmii_rx_d_lo;
      nxt_rx_data_val <= '1';
      if xgmii_rx_c_lo = c_xgmii_c_term_lo then
        -- As we're aligned properly, we can expect a TERM char in the LS word.
        -- The MS word will contain the MS portion of the c_xgmii_c_term,
        -- and after that we expect idle patterns, so nxt_rx_data is a GAP.
        nxt_state       <= s_gap;
        nxt_rx_data_hi  <= (others => '0');
        nxt_rx_data_lo  <= (others => '0');
        nxt_rx_data_val <= '0';
      elsif xgmii_rx_c = c_xgmii_c_init then
        -- Data interrupted due to re-initialization
        nxt_state <= s_gap;
      end if;

    when s_data_misaligned =>
      nxt_rx_data_hi  <= xgmii_rx_d_lo;
      nxt_rx_data_lo  <= prev_xgmii_rx_d_hi;
      nxt_rx_data_val <= '1';
      if prev_state = s_gap then
        nxt_rx_data_hi  <= (others => '0');
        nxt_rx_data_lo  <= (others => '0');
        nxt_rx_data_val <= '0';
      end if;
      if xgmii_rx_c_hi = c_xgmii_c_term_lo then
        -- As we're misaligned, we can expect the LS portion of c_xgmii_c_term on the MS position.
        nxt_state <= s_gap;
      elsif xgmii_rx_c = c_xgmii_c_init then
        -- Data interrupted due to re-initialization
        nxt_state <= s_gap;
      end if;

    when others =>
      nxt_state       <= s_gap;
      nxt_rx_data_hi  <= (others => '0');
      nxt_rx_data_lo  <= (others => '0');
      nxt_rx_data_val <= '0';
    end case;
  end process;
end rtl;
