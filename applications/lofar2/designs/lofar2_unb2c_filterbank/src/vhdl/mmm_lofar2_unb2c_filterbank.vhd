-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib, mm_lib, lofar2_sdp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use unb2c_board_lib.unb2c_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use work.qsys_lofar2_unb2c_filterbank_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity mmm_lofar2_unb2c_filterbank is
  generic (
    g_sim         : boolean := false;  -- FALSE: use QSYS; TRUE: use mm_file I/O
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0
  );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi             : out t_mem_mosi;
    reg_wdi_miso             : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi : out t_mem_mosi;
    reg_unb_system_info_miso : in  t_mem_miso;
    rom_unb_system_info_mosi : out t_mem_mosi;
    rom_unb_system_info_miso : in  t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        : out t_mem_mosi;
    reg_unb_sens_miso        : in  t_mem_miso;

    reg_fpga_temp_sens_mosi   : out t_mem_mosi;
    reg_fpga_temp_sens_miso   : in  t_mem_miso;
    reg_fpga_voltage_sens_mosi: out t_mem_mosi;
    reg_fpga_voltage_sens_miso: in  t_mem_miso;

    reg_unb_pmbus_mosi       : out t_mem_mosi;
    reg_unb_pmbus_miso       : in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi            : out t_mem_mosi;
    reg_ppsh_miso            : in  t_mem_miso;

    -- eth1g
    eth1g_mm_rst             : out std_logic;
    eth1g_tse_mosi           : out t_mem_mosi;
    eth1g_tse_miso           : in  t_mem_miso;
    eth1g_reg_mosi           : out t_mem_mosi;
    eth1g_reg_miso           : in  t_mem_miso;
    eth1g_reg_interrupt      : in  std_logic;
    eth1g_ram_mosi           : out t_mem_mosi;
    eth1g_ram_miso           : in  t_mem_miso;

    -- EPCS read
    reg_dpmm_data_mosi       : out t_mem_mosi;
    reg_dpmm_data_miso       : in  t_mem_miso;
    reg_dpmm_ctrl_mosi       : out t_mem_mosi;
    reg_dpmm_ctrl_miso       : in  t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi       : out t_mem_mosi;
    reg_mmdp_data_miso       : in  t_mem_miso;
    reg_mmdp_ctrl_mosi       : out t_mem_mosi;
    reg_mmdp_ctrl_miso       : in  t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi            : out t_mem_mosi;
    reg_epcs_miso            : in  t_mem_miso;

    -- Remote Update
    reg_remu_mosi            : out t_mem_mosi;
    reg_remu_miso            : in  t_mem_miso;

    -- Jesd control
    jesd204b_mosi            : out t_mem_mosi;
    jesd204b_miso            : in  t_mem_miso;

    -- Dp shiftram
    reg_dp_shiftram_mosi     : out t_mem_mosi;
    reg_dp_shiftram_miso     : in  t_mem_miso;

    -- Bsn source
    reg_bsn_source_mosi      : out t_mem_mosi;
    reg_bsn_source_miso      : in  t_mem_miso;

    -- bsn schduler for wg trigger
    reg_bsn_scheduler_mosi   : out t_mem_mosi;
    reg_bsn_scheduler_miso   : in  t_mem_miso;

    -- BSN Monitor
    reg_bsn_monitor_input_mosi : out t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_monitor_input_miso : in  t_mem_miso := c_mem_miso_rst;

    -- MM wideband waveform generator registers [0,1,2,3] for signal paths [A,B,C,D]
    reg_wg_mosi                   : out t_mem_mosi;
    reg_wg_miso                   : in  t_mem_miso;
    ram_wg_mosi                   : out t_mem_mosi;
    ram_wg_miso                   : in  t_mem_miso;

    -- JESD databuffer
    ram_diag_data_buf_jesd_mosi   : out t_mem_mosi;
    ram_diag_data_buf_jesd_miso   : in  t_mem_miso;
    reg_diag_data_buf_jesd_mosi   : out t_mem_mosi;
    reg_diag_data_buf_jesd_miso   : in  t_mem_miso;

    -- Bsn databuffer
    ram_diag_data_buf_bsn_mosi    : out t_mem_mosi;
    ram_diag_data_buf_bsn_miso    : in  t_mem_miso;
    reg_diag_data_buf_bsn_mosi    : out t_mem_mosi;
    reg_diag_data_buf_bsn_miso    : in  t_mem_miso;

    -- Aduh
    ram_aduh_monitor_mosi         : out t_mem_mosi;
    ram_aduh_monitor_miso         : in  t_mem_miso;
    reg_aduh_monitor_mosi         : out t_mem_mosi;
    reg_aduh_monitor_miso         : in  t_mem_miso;

    -- Subband statistics
    ram_st_sst_mosi               : out t_mem_mosi;
    ram_st_sst_miso               : in  t_mem_miso;

    -- Filter coefficients
    ram_fil_coefs_mosi            : out t_mem_mosi;
    ram_fil_coefs_miso            : in  t_mem_miso;

    -- Spectral Inversion
    reg_si_mosi                   : out t_mem_mosi;
    reg_si_miso                   : in  t_mem_miso;

     -- Equalizer gains
     ram_equalizer_gains_mosi     : out t_mem_mosi;
     ram_equalizer_gains_miso     : in  t_mem_miso;

     -- DP Selector
     reg_dp_selector_mosi         : out t_mem_mosi;
     reg_dp_selector_miso         : in  t_mem_miso;

    -- Scrap ram
    ram_scrap_mosi                : out t_mem_mosi;
    ram_scrap_miso                : in  t_mem_miso
  );
end mmm_lofar2_unb2c_filterbank;

architecture str of mmm_lofar2_unb2c_filterbank is
  constant c_sim_node_nr   : natural := g_sim_node_nr;
  constant c_sim_node_type : string(1 to 2) := "FN";

  signal i_reset_n         : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $UPE/sim.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    u_mm_file_reg_unb_system_info    : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                               port map(mm_rst, mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info    : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                               port map(mm_rst, mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                               port map(mm_rst, mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_unb_sens           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
                                               port map(mm_rst, mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso );

    u_mm_file_reg_unb_pmbus          : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_PMBUS")
                                               port map(mm_rst, mm_clk, reg_unb_pmbus_mosi, reg_unb_pmbus_miso );

    u_mm_file_reg_fpga_temp_sens     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_TEMP_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_temp_sens_mosi, reg_fpga_temp_sens_miso );

    u_mm_file_reg_fpga_voltage_sens  :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_VOLTAGE_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_voltage_sens_mosi, reg_fpga_voltage_sens_miso );

    u_mm_file_reg_ppsh               : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                               port map(mm_rst, mm_clk, reg_ppsh_mosi, reg_ppsh_miso );

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    u_mm_file_reg_eth                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                               port map(mm_rst, mm_clk, eth1g_reg_mosi, eth1g_reg_miso );

    u_mm_file_jesd204b               : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "JESD204B")
                                                port map(mm_rst, mm_clk, jesd204b_mosi, jesd204b_miso );

    u_mm_file_reg_dp_shiftram        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_SHIFTRAM")
                                                port map(mm_rst, mm_clk, reg_dp_shiftram_mosi, reg_dp_shiftram_miso );

    u_mm_file_reg_bsn_source         : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_SOURCE")
                                                port map(mm_rst, mm_clk, reg_bsn_source_mosi, reg_bsn_source_miso );

    u_mm_file_reg_bsn_scheduler      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_SCHEDULER")
                                                port map(mm_rst, mm_clk, reg_bsn_scheduler_mosi, reg_bsn_scheduler_miso );

    u_mm_file_reg_bsn_monitor_input  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_INPUT")
                                                port map(mm_rst, mm_clk, reg_bsn_monitor_input_mosi, reg_bsn_monitor_input_miso );

    u_mm_file_reg_wg                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WG")
                                                port map(mm_rst, mm_clk, reg_wg_mosi, reg_wg_miso );
    u_mm_file_ram_wg                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_WG")
                                               port map(mm_rst, mm_clk, ram_wg_mosi, ram_wg_miso );

    u_mm_file_ram_diag_data_buf_jesd : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUF_JESD")
                                               port map(mm_rst, mm_clk, ram_diag_data_buf_jesd_mosi, ram_diag_data_buf_jesd_miso );
    u_mm_file_reg_diag_data_buf_jesd : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUF_JESD")
                                               port map(mm_rst, mm_clk, reg_diag_data_buf_jesd_mosi, reg_diag_data_buf_jesd_miso );

    u_mm_file_ram_diag_data_buf_bsn  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUF_BSN")
                                               port map(mm_rst, mm_clk, ram_diag_data_buf_bsn_mosi, ram_diag_data_buf_bsn_miso );
    u_mm_file_reg_diag_data_buf_bsn  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUF_BSN")
                                               port map(mm_rst, mm_clk, reg_diag_data_buf_bsn_mosi, reg_diag_data_buf_bsn_miso );

    u_mm_file_ram_aduh_monitor       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_ADUH_MONITOR")
                                               port map(mm_rst, mm_clk, ram_aduh_monitor_mosi, ram_aduh_monitor_miso );
    u_mm_file_reg_aduh_monitor       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_ADUH_MONITOR")
                                               port map(mm_rst, mm_clk, reg_aduh_monitor_mosi, reg_aduh_monitor_miso );

    u_mm_file_ram_st_sst             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_ST_SST")
                                               port map(mm_rst, mm_clk, ram_st_sst_mosi, ram_st_sst_miso );

    u_mm_file_ram_fil_coefs          : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_FIL_COEFS")
                                               port map(mm_rst, mm_clk, ram_fil_coefs_mosi, ram_fil_coefs_miso );

    u_mm_file_reg_si                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_SI")
                                              port map(mm_rst, mm_clk, reg_si_mosi, reg_si_miso );

    u_mm_file_ram_equalizer_gains    : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_EQUALIZER_GAINS")
                                               port map(mm_rst, mm_clk, ram_equalizer_gains_mosi, ram_equalizer_gains_miso );

    u_mm_file_reg_dp_selector        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DP_SELECTOR")
                                              port map(mm_rst, mm_clk, reg_dp_selector_mosi, reg_dp_selector_miso );

    u_mm_file_ram_scrap              : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_SCRAP")
                                               port map(mm_rst, mm_clk, ram_scrap_mosi, ram_scrap_miso );
    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(mm_clk, c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  i_reset_n <= not mm_rst;
  ----------------------------------------------------------------------------
  -- QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys : if g_sim = false generate
    u_qsys : qsys_lofar2_unb2c_filterbank
    port map (

      clk_clk                                   => mm_clk,
      reset_reset_n                             => i_reset_n,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2c_board.
      pio_wdi_external_connection_export        => pout_wdi,

      avs_eth_0_reset_export                    => eth1g_mm_rst,
      avs_eth_0_clk_export                      => OPEN,
      avs_eth_0_tse_address_export              => eth1g_tse_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                => eth1g_tse_mosi.wr,
      avs_eth_0_tse_read_export                 => eth1g_tse_mosi.rd,
      avs_eth_0_tse_writedata_export            => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export             => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export          => eth1g_tse_miso.waitrequest,
      avs_eth_0_reg_address_export              => eth1g_reg_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                => eth1g_reg_mosi.wr,
      avs_eth_0_reg_read_export                 => eth1g_reg_mosi.rd,
      avs_eth_0_reg_writedata_export            => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export             => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export              => eth1g_ram_mosi.address(c_unb2c_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                => eth1g_ram_mosi.wr,
      avs_eth_0_ram_read_export                 => eth1g_ram_mosi.rd,
      avs_eth_0_ram_writedata_export            => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export             => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                      => eth1g_reg_interrupt,

      reg_unb_sens_reset_export                 => OPEN,
      reg_unb_sens_clk_export                   => OPEN,
      reg_unb_sens_address_export               => reg_unb_sens_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      reg_unb_sens_write_export                 => reg_unb_sens_mosi.wr,
      reg_unb_sens_writedata_export             => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_read_export                  => reg_unb_sens_mosi.rd,
      reg_unb_sens_readdata_export              => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_unb_pmbus_reset_export                => OPEN,
      reg_unb_pmbus_clk_export                  => OPEN,
      reg_unb_pmbus_address_export              => reg_unb_pmbus_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_unb_pmbus_adr_w - 1 downto 0),
      reg_unb_pmbus_write_export                => reg_unb_pmbus_mosi.wr,
      reg_unb_pmbus_writedata_export            => reg_unb_pmbus_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_pmbus_read_export                 => reg_unb_pmbus_mosi.rd,
      reg_unb_pmbus_readdata_export             => reg_unb_pmbus_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_temp_sens_reset_export           => OPEN,
      reg_fpga_temp_sens_clk_export             => OPEN,
      reg_fpga_temp_sens_address_export         => reg_fpga_temp_sens_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export           => reg_fpga_temp_sens_mosi.wr,
      reg_fpga_temp_sens_writedata_export       => reg_fpga_temp_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export            => reg_fpga_temp_sens_mosi.rd,
      reg_fpga_temp_sens_readdata_export        => reg_fpga_temp_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_reset_export        => OPEN,
      reg_fpga_voltage_sens_clk_export          => OPEN,
      reg_fpga_voltage_sens_address_export      => reg_fpga_voltage_sens_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export        => reg_fpga_voltage_sens_mosi.wr,
      reg_fpga_voltage_sens_writedata_export    => reg_fpga_voltage_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export         => reg_fpga_voltage_sens_mosi.rd,
      reg_fpga_voltage_sens_readdata_export     => reg_fpga_voltage_sens_miso.rddata(c_word_w - 1 downto 0),

      rom_system_info_reset_export              => OPEN,
      rom_system_info_clk_export                => OPEN,
--    ToDo: This has changed in the peripherals package
      rom_system_info_address_export            => rom_unb_system_info_mosi.address(9 downto 0),
--      rom_system_info_address_export            => rom_unb_system_info_mosi.address(c_unb2c_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w-1 DOWNTO 0),
      rom_system_info_write_export              => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export          => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export               => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export           => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_system_info_reset_export              => OPEN,
      pio_system_info_clk_export                => OPEN,
      pio_system_info_address_export            => reg_unb_system_info_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export              => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export          => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export               => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export           => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_pps_reset_export                      => OPEN,
      pio_pps_clk_export                        => OPEN,
--    ToDo: This has changed in the peripherals package
      pio_pps_address_export                    => reg_ppsh_mosi.address(0 downto 0),
--      pio_pps_address_export                    => reg_ppsh_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_ppsh_adr_w-1 DOWNTO 0),
      pio_pps_write_export                      => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                  => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                       => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                   => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),

      reg_wdi_reset_export                      => OPEN,
      reg_wdi_clk_export                        => OPEN,
      reg_wdi_address_export                    => reg_wdi_mosi.address(0 downto 0),
      reg_wdi_write_export                      => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                  => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                       => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                   => reg_wdi_miso.rddata(c_word_w - 1 downto 0),

      reg_remu_reset_export                     => OPEN,
      reg_remu_clk_export                       => OPEN,
      reg_remu_address_export                   => reg_remu_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                     => reg_remu_mosi.wr,
      reg_remu_writedata_export                 => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                      => reg_remu_mosi.rd,
      reg_remu_readdata_export                  => reg_remu_miso.rddata(c_word_w - 1 downto 0),

      jesd204b_reset_export                     => OPEN,
      jesd204b_clk_export                       => OPEN,
      jesd204b_address_export                   => jesd204b_mosi.address(c_sdp_jesd204b_addr_w - 1 downto 0),
      jesd204b_write_export                     => jesd204b_mosi.wr,
      jesd204b_writedata_export                 => jesd204b_mosi.wrdata(c_word_w - 1 downto 0),
      jesd204b_read_export                      => jesd204b_mosi.rd,
      jesd204b_readdata_export                  => jesd204b_miso.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_input_address_export      => reg_bsn_monitor_input_mosi.address(c_sdp_reg_bsn_monitor_input_addr_w - 1 downto 0),
      reg_bsn_monitor_input_clk_export          => OPEN,
      reg_bsn_monitor_input_read_export         => reg_bsn_monitor_input_mosi.rd,
      reg_bsn_monitor_input_readdata_export     => reg_bsn_monitor_input_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_input_reset_export        => OPEN,
      reg_bsn_monitor_input_write_export        => reg_bsn_monitor_input_mosi.wr,
      reg_bsn_monitor_input_writedata_export    => reg_bsn_monitor_input_mosi.wrdata(c_word_w - 1 downto 0),

      -- waveform generators (multiplexed)
      reg_wg_clk_export                         => OPEN,
      reg_wg_reset_export                       => OPEN,
      reg_wg_address_export                     => reg_wg_mosi.address(c_sdp_reg_wg_addr_w - 1 downto 0),
      reg_wg_read_export                        => reg_wg_mosi.rd,
      reg_wg_readdata_export                    => reg_wg_miso.rddata(c_word_w - 1 downto 0),
      reg_wg_write_export                       => reg_wg_mosi.wr,
      reg_wg_writedata_export                   => reg_wg_mosi.wrdata(c_word_w - 1 downto 0),

      ram_wg_clk_export                         => OPEN,
      ram_wg_reset_export                       => OPEN,
      ram_wg_address_export                     => ram_wg_mosi.address(c_sdp_ram_wg_addr_w - 1 downto 0),
      ram_wg_read_export                        => ram_wg_mosi.rd,
      ram_wg_readdata_export                    => ram_wg_miso.rddata(c_word_w - 1 downto 0),
      ram_wg_write_export                       => ram_wg_mosi.wr,
      ram_wg_writedata_export                   => ram_wg_mosi.wrdata(c_word_w - 1 downto 0),

      reg_dp_shiftram_clk_export                => OPEN,
      reg_dp_shiftram_reset_export              => OPEN,
      reg_dp_shiftram_address_export            => reg_dp_shiftram_mosi.address(c_sdp_reg_dp_shiftram_addr_w - 1 downto 0),
      reg_dp_shiftram_read_export               => reg_dp_shiftram_mosi.rd,
      reg_dp_shiftram_readdata_export           => reg_dp_shiftram_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_shiftram_write_export              => reg_dp_shiftram_mosi.wr,
      reg_dp_shiftram_writedata_export          => reg_dp_shiftram_mosi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_source_clk_export                 => OPEN,
      reg_bsn_source_reset_export               => OPEN,
      reg_bsn_source_address_export             => reg_bsn_source_mosi.address(c_sdp_reg_bsn_source_addr_w - 1 downto 0),
      reg_bsn_source_read_export                => reg_bsn_source_mosi.rd,
      reg_bsn_source_readdata_export            => reg_bsn_source_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_source_write_export               => reg_bsn_source_mosi.wr,
      reg_bsn_source_writedata_export           => reg_bsn_source_mosi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_scheduler_clk_export              => OPEN,
      reg_bsn_scheduler_reset_export            => OPEN,
      reg_bsn_scheduler_address_export          => reg_bsn_scheduler_mosi.address(c_sdp_reg_bsn_scheduler_addr_w - 1 downto 0),
      reg_bsn_scheduler_read_export             => reg_bsn_scheduler_mosi.rd,
      reg_bsn_scheduler_readdata_export         => reg_bsn_scheduler_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_scheduler_write_export            => reg_bsn_scheduler_mosi.wr,
      reg_bsn_scheduler_writedata_export        => reg_bsn_scheduler_mosi.wrdata(c_word_w - 1 downto 0),

      reg_epcs_reset_export                     => OPEN,
      reg_epcs_clk_export                       => OPEN,
      reg_epcs_address_export                   => reg_epcs_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                     => reg_epcs_mosi.wr,
      reg_epcs_writedata_export                 => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                      => reg_epcs_mosi.rd,
      reg_epcs_readdata_export                  => reg_epcs_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_reset_export                => OPEN,
      reg_dpmm_ctrl_clk_export                  => OPEN,
      reg_dpmm_ctrl_address_export              => reg_dpmm_ctrl_mosi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                => reg_dpmm_ctrl_mosi.wr,
      reg_dpmm_ctrl_writedata_export            => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                 => reg_dpmm_ctrl_mosi.rd,
      reg_dpmm_ctrl_readdata_export             => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_reset_export                => OPEN,
      reg_mmdp_data_clk_export                  => OPEN,
      reg_mmdp_data_address_export              => reg_mmdp_data_mosi.address(0 downto 0),
      reg_mmdp_data_write_export                => reg_mmdp_data_mosi.wr,
      reg_mmdp_data_writedata_export            => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                 => reg_mmdp_data_mosi.rd,
      reg_mmdp_data_readdata_export             => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_reset_export                => OPEN,
      reg_dpmm_data_clk_export                  => OPEN,
      reg_dpmm_data_address_export              => reg_dpmm_data_mosi.address(0 downto 0),
      reg_dpmm_data_read_export                 => reg_dpmm_data_mosi.rd,
      reg_dpmm_data_readdata_export             => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                => reg_dpmm_data_mosi.wr,
      reg_dpmm_data_writedata_export            => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_reset_export                => OPEN,
      reg_mmdp_ctrl_clk_export                  => OPEN,
      reg_mmdp_ctrl_address_export              => reg_mmdp_ctrl_mosi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                 => reg_mmdp_ctrl_mosi.rd,
      reg_mmdp_ctrl_readdata_export             => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                => reg_mmdp_ctrl_mosi.wr,
      reg_mmdp_ctrl_writedata_export            => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      ram_diag_data_buf_bsn_clk_export          => OPEN,
      ram_diag_data_buf_bsn_reset_export        => OPEN,
      ram_diag_data_buf_bsn_address_export      => ram_diag_data_buf_bsn_mosi.address(c_sdp_ram_diag_data_buf_bsn_addr_w - 1 downto 0),
      ram_diag_data_buf_bsn_write_export        => ram_diag_data_buf_bsn_mosi.wr,
      ram_diag_data_buf_bsn_writedata_export    => ram_diag_data_buf_bsn_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buf_bsn_read_export         => ram_diag_data_buf_bsn_mosi.rd,
      ram_diag_data_buf_bsn_readdata_export     => ram_diag_data_buf_bsn_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buf_bsn_reset_export        => OPEN,
      reg_diag_data_buf_bsn_clk_export          => OPEN,
      reg_diag_data_buf_bsn_address_export      => reg_diag_data_buf_bsn_mosi.address(c_sdp_reg_diag_data_buf_bsn_addr_w - 1 downto 0),
      reg_diag_data_buf_bsn_write_export        => reg_diag_data_buf_bsn_mosi.wr,
      reg_diag_data_buf_bsn_writedata_export    => reg_diag_data_buf_bsn_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buf_bsn_read_export         => reg_diag_data_buf_bsn_mosi.rd,
      reg_diag_data_buf_bsn_readdata_export     => reg_diag_data_buf_bsn_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buf_jesd_clk_export         => OPEN,
      ram_diag_data_buf_jesd_reset_export       => OPEN,
      ram_diag_data_buf_jesd_address_export     => ram_diag_data_buf_jesd_mosi.address(c_sdp_ram_diag_data_buf_jesd_addr_w - 1 downto 0),
      ram_diag_data_buf_jesd_write_export       => ram_diag_data_buf_jesd_mosi.wr,
      ram_diag_data_buf_jesd_writedata_export   => ram_diag_data_buf_jesd_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buf_jesd_read_export        => ram_diag_data_buf_jesd_mosi.rd,
      ram_diag_data_buf_jesd_readdata_export    => ram_diag_data_buf_jesd_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buf_jesd_reset_export       => OPEN,
      reg_diag_data_buf_jesd_clk_export         => OPEN,
      reg_diag_data_buf_jesd_address_export     => reg_diag_data_buf_jesd_mosi.address(c_sdp_reg_diag_data_buf_jesd_addr_w - 1 downto 0),
      reg_diag_data_buf_jesd_write_export       => reg_diag_data_buf_jesd_mosi.wr,
      reg_diag_data_buf_jesd_writedata_export   => reg_diag_data_buf_jesd_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buf_jesd_read_export        => reg_diag_data_buf_jesd_mosi.rd,
      reg_diag_data_buf_jesd_readdata_export    => reg_diag_data_buf_jesd_miso.rddata(c_word_w - 1 downto 0),

      reg_aduh_monitor_reset_export             => OPEN,
      reg_aduh_monitor_clk_export               => OPEN,
      reg_aduh_monitor_address_export           => reg_aduh_monitor_mosi.address(c_sdp_reg_aduh_monitor_addr_w - 1 downto 0),
      reg_aduh_monitor_write_export             => reg_aduh_monitor_mosi.wr,
      reg_aduh_monitor_writedata_export         => reg_aduh_monitor_mosi.wrdata(c_word_w - 1 downto 0),
      reg_aduh_monitor_read_export              => reg_aduh_monitor_mosi.rd,
      reg_aduh_monitor_readdata_export          => reg_aduh_monitor_miso.rddata(c_word_w - 1 downto 0),

      ram_fil_coefs_clk_export                  => OPEN,
      ram_fil_coefs_reset_export                => OPEN,
      ram_fil_coefs_address_export              => ram_fil_coefs_mosi.address(c_sdp_ram_fil_coefs_addr_w - 1 downto 0),
      ram_fil_coefs_write_export                => ram_fil_coefs_mosi.wr,
      ram_fil_coefs_writedata_export            => ram_fil_coefs_mosi.wrdata(c_word_w - 1 downto 0),
      ram_fil_coefs_read_export                 => ram_fil_coefs_mosi.rd,
      ram_fil_coefs_readdata_export             => ram_fil_coefs_miso.rddata(c_word_w - 1 downto 0),

      ram_st_sst_clk_export                     => OPEN,
      ram_st_sst_reset_export                   => OPEN,
      ram_st_sst_address_export                 => ram_st_sst_mosi.address(c_sdp_ram_st_sst_addr_w - 1 downto 0),
      ram_st_sst_write_export                   => ram_st_sst_mosi.wr,
      ram_st_sst_writedata_export               => ram_st_sst_mosi.wrdata(c_word_w - 1 downto 0),
      ram_st_sst_read_export                    => ram_st_sst_mosi.rd,
      ram_st_sst_readdata_export                => ram_st_sst_miso.rddata(c_word_w - 1 downto 0),

      reg_si_clk_export                         => OPEN,
      reg_si_reset_export                       => OPEN,
      reg_si_address_export                     => reg_si_mosi.address(c_sdp_reg_si_addr_w - 1 downto 0),
      reg_si_write_export                       => reg_si_mosi.wr,
      reg_si_writedata_export                   => reg_si_mosi.wrdata(c_word_w - 1 downto 0),
      reg_si_read_export                        => reg_si_mosi.rd,
      reg_si_readdata_export                    => reg_si_miso.rddata(c_word_w - 1 downto 0),

      ram_equalizer_gains_clk_export            => OPEN,
      ram_equalizer_gains_reset_export          => OPEN,
      ram_equalizer_gains_address_export        => ram_equalizer_gains_mosi.address(c_sdp_ram_equalizer_gains_addr_w - 1 downto 0),
      ram_equalizer_gains_write_export          => ram_equalizer_gains_mosi.wr,
      ram_equalizer_gains_writedata_export      => ram_equalizer_gains_mosi.wrdata(c_word_w - 1 downto 0),
      ram_equalizer_gains_read_export           => ram_equalizer_gains_mosi.rd,
      ram_equalizer_gains_readdata_export       => ram_equalizer_gains_miso.rddata(c_word_w - 1 downto 0),

      reg_dp_selector_clk_export                => OPEN,
      reg_dp_selector_reset_export              => OPEN,
      reg_dp_selector_address_export            => reg_dp_selector_mosi.address(c_sdp_reg_dp_selector_addr_w - 1 downto 0),
      reg_dp_selector_write_export              => reg_dp_selector_mosi.wr,
      reg_dp_selector_writedata_export          => reg_dp_selector_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dp_selector_read_export               => reg_dp_selector_mosi.rd,
      reg_dp_selector_readdata_export           => reg_dp_selector_miso.rddata(c_word_w - 1 downto 0),

      ram_scrap_clk_export                      => OPEN,
      ram_scrap_reset_export                    => OPEN,
      ram_scrap_address_export                  => ram_scrap_mosi.address(9 - 1 downto 0),
      ram_scrap_write_export                    => ram_scrap_mosi.wr,
      ram_scrap_writedata_export                => ram_scrap_mosi.wrdata(c_word_w - 1 downto 0),
      ram_scrap_read_export                     => ram_scrap_mosi.rd,
      ram_scrap_readdata_export                 => ram_scrap_miso.rddata(c_word_w - 1 downto 0)
    );
  end generate;
end str;
