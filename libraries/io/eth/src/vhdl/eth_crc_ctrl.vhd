-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

-- Purpose:
-- Description:
--   Replace the CRC word by the TSE MAC stream error field information.
--   Hence the CRC field now gets filled with an enumerated value:
--   0=OK, >0 AND odd = Error
-- Remarks:
--  . c_this_snk_latency = 1
--  . c_this_src_latency = 1

entity eth_crc_ctrl is
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    -- Streaming Sink
    snk_in_err     : in  std_logic_vector;  -- error vector from TSE MAC t_eth_stream
    snk_in         : in  t_dp_sosi;
    snk_out        : out t_dp_siso;

    -- Streaming Source
    src_in         : in  t_dp_siso;
    src_out        : out t_dp_sosi;  -- replaced CRC word by snk_in_err, so CRC word /=0 indicates any TSE error
    src_out_err    : out std_logic  -- flag snk_in_err/=0 at src_out.eop
  );
end eth_crc_ctrl;

architecture rtl of eth_crc_ctrl is
  constant c_tail_nof_words : natural := 2;  -- if empty /=0 then the CRC is straddled over the last two packet words
  constant c_tail_high      : natural := c_tail_nof_words - 1;

  signal i_src_out         : t_dp_sosi;

  signal cur_tail_inputs   : t_dp_sosi_arr(0 to c_tail_high);
  signal new_tail_inputs   : t_dp_sosi_arr(0 to c_tail_high);

  signal in_err            : std_logic_vector(c_eth_data_w - 1 downto 0);
  signal in_err_hold       : std_logic_vector(snk_in_err'range);
  signal nxt_in_err_hold   : std_logic_vector(snk_in_err'range);
begin
  src_out <= i_src_out;

  -- Pass on frame level flow control
  snk_out.xon <= src_in.xon;

  -- No change in ready latency, c_this_snk_latency = c_this_src_latency
  snk_out.ready <= src_in.ready;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      in_err_hold <= (others => '0');
    elsif rising_edge(clk) then
      in_err_hold <= nxt_in_err_hold;
    end if;
  end process;

  -- Hold the snk_in_err for src_out_err.
  nxt_in_err_hold <= (others => '0')              when snk_in.sop = '1' else
                      in_err(in_err_hold'range) when snk_in.eop = '1' else in_err_hold;

  -- Output the src_out_err signal from src_out.eop until the next src_out.sop
  u_eop_extend : entity dp_lib.dp_eop_extend
  port map (
    rst        => rst,
    clk        => clk,
    in_sop     => i_src_out.sop,
    in_eop     => in_err_hold(0),  -- Checking only bit [0] is sufficient, because bit [0] contains the logic OR of bits [5:1]
    eop_extend => src_out_err
  );

  -- Replace the CRC word in the 1 or 2 words of the Ethernet tail by the snk_in_err status
  --in_err <= TO_SVEC(-1, c_eth_data_w);  -- use odd number for test purposes, because bit [0] must contain the logic OR of bits [1:5]
  in_err <= RESIZE_UVEC(snk_in_err, c_eth_data_w);

  p_crc_replace : process(cur_tail_inputs, in_err)
  begin
    -- default
    new_tail_inputs <= cur_tail_inputs;

    -- tail replace: replace the CRC word with the in_err when the eop has arrived
    if cur_tail_inputs(0).eop = '1' then
      new_tail_inputs(0).data(c_eth_data_w - 1 downto 0) <= in_err;  -- cur_tail_inputs(0).empty = 0
      case to_integer(unsigned(cur_tail_inputs(0).empty(c_eth_empty_w - 1 downto 0))) is
        when 1 => new_tail_inputs(0).data(c_eth_data_w - 1 downto 0) <=                  in_err(3 * c_byte_w - 1 downto          0) & c_slv0(1 * c_byte_w - 1 downto          0);
                  new_tail_inputs(1).data(c_eth_data_w - 1 downto 0) <= cur_tail_inputs(1).data(4 * c_byte_w - 1 downto 1 * c_byte_w) & in_err(4 * c_byte_w - 1 downto 3 * c_byte_w);
        when 2 => new_tail_inputs(0).data(c_eth_data_w - 1 downto 0) <=                  in_err(2 * c_byte_w - 1 downto          0) & c_slv0(2 * c_byte_w - 1 downto          0);
                  new_tail_inputs(1).data(c_eth_data_w - 1 downto 0) <= cur_tail_inputs(1).data(4 * c_byte_w - 1 downto 2 * c_byte_w) & in_err(4 * c_byte_w - 1 downto 2 * c_byte_w);
        when 3 => new_tail_inputs(0).data(c_eth_data_w - 1 downto 0) <=                  in_err(1 * c_byte_w - 1 downto          0) & c_slv0(3 * c_byte_w - 1 downto          0);
                  new_tail_inputs(1).data(c_eth_data_w - 1 downto 0) <= cur_tail_inputs(1).data(4 * c_byte_w - 1 downto 3 * c_byte_w) & in_err(4 * c_byte_w - 1 downto 1 * c_byte_w);
        when others => null;
      end case;
    end if;
  end process;

  -- The tail shift register
  u_tail_reg : entity dp_lib.dp_shiftreg
  generic map (
    g_output_reg     => false,
    g_flush_eop      => true,
    g_modify_support => true,
    g_nof_words      => c_tail_nof_words
  )
  port map (
    rst                 => rst,
    clk                 => clk,
    -- ST sink
    snk_out             => OPEN,
    snk_in              => snk_in,
    -- Control shift register contents
    cur_shiftreg_inputs => cur_tail_inputs,
    new_shiftreg_inputs => new_tail_inputs,
    -- ST source
    src_in              => src_in,
    src_out             => i_src_out
  );
end rtl;
