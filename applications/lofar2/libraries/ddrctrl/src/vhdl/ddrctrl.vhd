-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: Folding a stream of data into a mm data configuration so it can be
-- stored in a DDR RAM-stick.
--
-- Description:
--  First the data from the sosi array gets collected into one data vector.
--  After that this data vector gets resized to the right size data vector in
--  order to make it storable in a DDR RAM-stick.
--  After that a address gets assigned to the data so the data can be found back.
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
--  The maximum value of the address is determend by g_tech_ddr.

library IEEE, technology_lib, tech_ddr_lib, common_lib, dp_lib, io_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use io_ddr_lib.all;

entity ddrctrl is
  generic (
    g_tech_ddr        : t_c_tech_ddr;  -- type of memory
    g_sim_model       : boolean                                             := true;  -- determens if this is a simulation
    g_technology      : natural                                             := c_tech_select_default;
    g_nof_streams     : natural                                             := 12;  -- number of input streams
    g_data_w          : natural                                             := 14;  -- data with of input data vectors
    g_stop_percentage : natural                                             := 50;
    g_block_size      : natural                                             := 1024
  );
  port (
    clk               : in  std_logic                                       := '0';
    rst               : in  std_logic;
    ctlr_ref_clk      : in  std_logic;
    ctlr_ref_rst      : in  std_logic;
    mm_clk            : in  std_logic                                       := '0';
    mm_rst            : in  std_logic                                       := '0';
    in_sosi_arr       : in  t_dp_sosi_arr;  -- input data
    stop_in           : in  std_logic                                       := '0';

    out_sosi_arr      : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)         := (others => c_dp_sosi_rst);
    out_siso          : in  t_dp_siso                                       := c_dp_siso_rst;
    ddrctrl_ctrl_state: out std_logic_vector(32 - 1 downto 0);

    term_ctrl_out     : out   t_tech_ddr3_phy_terminationcontrol;
    term_ctrl_in      : in    t_tech_ddr3_phy_terminationcontrol            := c_tech_ddr3_phy_terminationcontrol_rst;

    -- IO_DDR monitor data
    reg_io_ddr_mosi   : in    t_mem_mosi  := c_mem_mosi_rst;
    reg_io_ddr_miso   : out   t_mem_miso;

    -- DDR3 PHY external interface
    phy3_in           : in    t_tech_ddr3_phy_in                            := c_tech_ddr3_phy_in_x;
    phy3_io           : inout t_tech_ddr3_phy_io;
    phy3_ou           : out   t_tech_ddr3_phy_ou;

    -- DDR4 PHY external interface
    phy4_in           : in    t_tech_ddr4_phy_in                            := c_tech_ddr4_phy_in_x;
    phy4_io           : inout t_tech_ddr4_phy_io;
    phy4_ou           : out   t_tech_ddr4_phy_ou
  );
end ddrctrl;

architecture str of ddrctrl is
  -- constant for readability
  constant  c_io_ddr_data_w     : natural                                   := func_tech_ddr_ctlr_data_w(g_tech_ddr);  -- 576
  constant  c_wr_fifo_depth     : natural                                   := 256;  -- defined at DDR side of the FIFO, >=16 and independent of wr burst size, default >= 256 because 32b*256 fits in 1 M9K so c_ctlr_data_w=256b will require 8 M9K
  constant  c_rd_fifo_depth     : natural                                   := 256;  -- defined at DDR side of the FIFO, >=16 AND > max number of rd burst sizes (so > c_rd_fifo_af_margin), default >= 256 because 32b*256 fits in 1 M9K so c_ctlr_data_w=256b will require 8 M9K
  constant  c_wr_fifo_uw_w      : natural                                   := ceil_log2(c_wr_fifo_depth * (func_tech_ddr_ctlr_data_w(g_tech_ddr) / c_io_ddr_data_w));
  constant  c_rd_fifo_uw_w      : natural                                   := ceil_log2(c_rd_fifo_depth * (func_tech_ddr_ctlr_data_w(g_tech_ddr) / c_io_ddr_data_w));

  constant  c_burstsize         : natural                                   := g_tech_ddr.maxburstsize;
  constant  c_adr_w             : natural                                   := func_tech_ddr_ctlr_address_w(g_tech_ddr);  -- the lengt of the address vector, for simulation this is smaller, otherwise the simulation would take to long, 27
  constant  c_max_adr           : natural                                   := 2**(c_adr_w) - 1;  -- the maximal address that is possible within the vector length of the address
  constant  c_adr_per_b         : natural                                   := ((g_block_size * g_nof_streams * g_data_w) / c_io_ddr_data_w) + 1;  -- rounding error removes the amount of extra addresses.
  constant  c_bim               : natural                                   := natural(floor(real(c_max_adr) / real(c_adr_per_b)));

  signal    s_adr_per_b         : natural                                   := c_adr_per_b;

  -- the amount of addresses used
  constant  c_nof_adr           : natural                                   := c_bim * c_adr_per_b;

  signal    s_nof_adr           : natural                                   := c_nof_adr;

  -- the amount of overflow after one block is written
  constant  c_of_pb             : natural                                   := (g_block_size * g_nof_streams * g_data_w) - (((g_block_size * g_nof_streams * g_data_w) / c_io_ddr_data_w) * c_io_ddr_data_w);  -- amount of overflow after one block is written to memory

  constant  c_aof_full_burst    : natural                                   := c_nof_adr / c_burstsize;
  constant  c_last_burstsize    : natural                                   := c_nof_adr - (c_aof_full_burst * c_burstsize);

  signal    s_last_burstsize    : natural                                   := c_last_burstsize;

  -- signals for connecting the components
  signal    ctrl_clk              : std_logic;
  signal    ctrl_rst              : std_logic;
  signal    rst_ddrctrl_input_ac  : std_logic;
  signal    out_of                : natural                                     := 0;
  signal    out_sosi              : t_dp_sosi                                   := c_dp_sosi_init;
  signal    out_adr               : natural                                     := 0;
  signal    dvr_mosi              : t_mem_ctlr_mosi                             := c_mem_ctlr_mosi_rst;
  signal    dvr_miso              : t_mem_ctlr_miso                             := c_mem_ctlr_miso_rst;
  signal    wr_sosi               : t_dp_sosi                                   := c_dp_sosi_init;
  signal    rd_siso               : t_dp_siso                                   := c_dp_siso_rst;
  signal    rd_sosi               : t_dp_sosi                                   := c_dp_sosi_init;
  signal    stop                  : std_logic;
  signal    wr_fifo_usedw         : std_logic_vector(c_wr_fifo_uw_w - 1 downto 0);
  signal    rd_fifo_usedw         : std_logic_vector(c_rd_fifo_uw_w - 1 downto 0);
  signal    rd_ready              : std_logic;
  signal    inp_bsn_adr           : natural;
  signal    bsn_co                : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal    data_stopped          : std_logic;

  signal    state_vec             : std_logic_vector(1 downto 0);
  signal    ddrctrl_ctrl_state_local  : std_logic_vector(32 - 1 downto 0);
  signal    ctlr_wr_flush_en      : std_logic;
begin
  rd_siso.ready <= rd_ready;
  rd_siso.xon   <= '1';
  ddrctrl_ctrl_state(5 downto 0)  <= ddrctrl_ctrl_state_local(5 downto 0);
  ddrctrl_ctrl_state(7 downto 6)  <= state_vec(1 downto 0);
  ddrctrl_ctrl_state(32 - 1 downto 8)  <= ddrctrl_ctrl_state_local(32 - 1 downto 8);

  -- input to io_ddr
  u_ddrctrl_input : entity work.ddrctrl_input
  generic map(
    g_tech_ddr                => g_tech_ddr,
    g_nof_streams             => g_nof_streams,
    g_data_w                  => g_data_w,
    g_max_adr                 => c_nof_adr,
    g_bim                     => c_bim,
    g_of_pb                   => c_of_pb,
    g_block_size              => g_block_size
  )
  port map(
    clk                       => clk,
    rst                       => rst,
    rst_ddrctrl_input_ac      => rst_ddrctrl_input_ac,
    in_sosi_arr               => in_sosi_arr,
    in_stop                   => stop,
    out_sosi                  => out_sosi,
    out_adr                   => out_adr,
    out_bsn_adr               => inp_bsn_adr,
    out_data_stopped          => data_stopped
  );

  -- functions as a fifo buffer for input data into the sdram stick. also manages input to sdram stick.
  u_io_ddr : entity io_ddr_lib.io_ddr
  generic map(
    g_sim_model               => g_sim_model,
    g_technology              => g_technology,
    g_tech_ddr                => g_tech_ddr,
    g_cross_domain_dvr_ctlr   => false,
    g_wr_data_w               => c_io_ddr_data_w,
    g_wr_fifo_depth           => c_wr_fifo_depth,
    g_rd_fifo_depth           => c_rd_fifo_depth,
    g_rd_data_w               => c_io_ddr_data_w,
    g_wr_flush_mode           => "VAL",
    g_wr_flush_use_channel    => false,
    g_wr_flush_start_channel  => 0,
    g_wr_flush_nof_channels   => 1
    )
    port map(

    -- DDR reference clock
    ctlr_ref_clk              => ctlr_ref_clk,
    ctlr_ref_rst              => ctlr_ref_rst,

    -- DDR controller clock domain
    ctlr_clk_out              => ctrl_clk,
    ctlr_rst_out              => ctrl_rst,

    ctlr_clk_in               => ctrl_clk,
    ctlr_rst_in               => ctrl_rst,

    -- MM clock + reset
    mm_rst                    => mm_rst,
    mm_clk                    => mm_clk,

    -- MM interface
    reg_io_ddr_mosi           => reg_io_ddr_mosi,
    reg_io_ddr_miso           => reg_io_ddr_miso,
    state_vec                 => state_vec,
    ctlr_wr_flush_en_o        => ctlr_wr_flush_en,

    -- Driver clock domain
    dvr_clk                   => clk,
    dvr_rst                   => rst,

    dvr_miso                  => dvr_miso,
    dvr_mosi                  => dvr_mosi,

    -- Write FIFO clock domain
    wr_clk                    => clk,
    wr_rst                    => rst,

    wr_fifo_usedw             => wr_fifo_usedw,
    wr_sosi                   => wr_sosi,
    wr_siso                   => open,

    -- Read FIFO clock domain
    rd_clk                    => clk,
    rd_rst                    => rst,

    rd_fifo_usedw             => rd_fifo_usedw,
    rd_sosi                   => rd_sosi,
    rd_siso                   => rd_siso,

    term_ctrl_out             => term_ctrl_out,
    term_ctrl_in              => term_ctrl_in,

    -- DDR3 PHY external interface
    phy3_in                   => phy3_in,
    phy3_io                   => phy3_io,
    phy3_ou                   => phy3_ou,

    -- DDR4 PHY external interface
    phy4_in                   => phy4_in,
    phy4_io                   => phy4_io,
    phy4_ou                   => phy4_ou
  );

  -- reading ddr memory
  u_ddrctrl_output : entity work.ddrctrl_output
  generic map(
    g_technology              => g_technology,
    g_tech_ddr                => g_tech_ddr,
    g_sim_model               => g_sim_model,
    g_in_data_w               => c_io_ddr_data_w,
    g_nof_streams             => g_nof_streams,
    g_data_w                  => g_data_w,
    g_block_size              => g_block_size,
    g_bim                     => c_bim
  )
  port map(
    clk                       => clk,
    rst                       => rst,

    in_sosi                   => rd_sosi,
    in_bsn                    => bsn_co,

    out_sosi_arr              => out_sosi_arr,
    out_siso                  => out_siso,
    out_ready                 => rd_ready
  );

  -- controller of ddrctrl
  u_ddrctrl_controller : entity work.ddrctrl_controller
  generic map(
    g_tech_ddr                => g_tech_ddr,
    g_stop_percentage         => g_stop_percentage,
    g_nof_streams             => g_nof_streams,
    g_out_data_w              => g_data_w,
    g_wr_data_w               => c_io_ddr_data_w,
    g_rd_fifo_depth           => c_rd_fifo_depth,
    g_rd_data_w               => c_io_ddr_data_w,
    g_block_size              => g_block_size,
    g_wr_fifo_uw_w            => c_wr_fifo_uw_w,
    g_rd_fifo_uw_w            => c_rd_fifo_uw_w,
    g_max_adr                 => c_nof_adr,
    g_burstsize               => c_burstsize,
    g_last_burstsize          => c_last_burstsize,
    g_adr_per_b               => c_adr_per_b,
    g_bim                     => c_bim
  )
  port map(
    clk                       => clk,
    rst                       => rst,

    -- ddrctrl_input
    inp_of                    => out_of,
    inp_sosi                  => out_sosi,
    inp_adr                   => out_adr,
    inp_bsn_adr               => inp_bsn_adr,
    inp_data_stopped          => data_stopped,
    rst_ddrctrl_input_ac      => rst_ddrctrl_input_ac,

    -- io_ddr
    dvr_mosi                  => dvr_mosi,
    dvr_miso                  => dvr_miso,
    wr_sosi                   => wr_sosi,
    wr_siso                   => c_dp_siso_rdy,
    wr_fifo_usedw             => wr_fifo_usedw,
    rd_fifo_usedw             => rd_fifo_usedw,
    ctlr_wr_flush_en          => ctlr_wr_flush_en,
    flush_state               => state_vec,

    -- ddrctrl_output
    outp_bsn                  => bsn_co,

    -- ddrctrl_controller
    stop_in                   => stop_in,
    stop_out                  => stop,
    ddrctrl_ctrl_state        => ddrctrl_ctrl_state_local
  );
end str;
