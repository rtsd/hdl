-----------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for mms_diag_block_gen
--   The tb is self-stopping and self-checking.
-- Usage:
--   > as 8
--   > run -all
--   Observe tb_state and check the out_sosi_0 fields if data is OK.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use std.textio.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.diag_pkg.all;

entity tb_mms_diag_block_gen is
  generic (
    g_use_usr_input        : boolean := false;
    g_use_bg               : boolean := true;
    g_use_tx_seq           : boolean := true;
    g_use_bg_buffer_ram    : boolean := false;
    g_usr_bypass_xonoff    : boolean := false;
    g_flow_control_verify  : t_dp_flow_control_enum := e_active;  -- always active, or random flow control
    g_nof_repeat           : natural := 5;
    g_nof_streams          : natural := 1;
    g_gap_size             : natural := 0
  );
end tb_mms_diag_block_gen;

architecture tb of tb_mms_diag_block_gen is
  constant clk_period           : time := 10 ns;

  constant c_verify_usr_rflow   : boolean := g_flow_control_verify /= e_active;
  constant c_use_user_fifo      : boolean := c_verify_usr_rflow;

  constant c_file_name_prefix   : string  := "data/bf_in_data";
  constant c_buf_dat_w          : natural := 32;
  constant c_buf_addr_w         : natural := 7;
  constant c_block_size         : natural := 96;

  constant c_bsn_offset         : natural := 42;
  constant c_sync_period        : natural := 10;
  constant c_sync_offset        : natural := c_bsn_offset mod c_sync_period;

  constant c_period_size        : natural := c_block_size + g_gap_size;

  constant c_mem_lo_addr        : natural := 0;
  constant c_mem_hi_addr        : natural := c_block_size-1;

  constant c_pend_period        : natural :=  1 * c_period_size;
  constant c_on_period          : natural := 10 * c_period_size;
  constant c_off_period         : natural := 10 * c_period_size;
  constant c_end_period         : natural := 10 * c_period_size;

  constant c_usr_fifo_size      : natural := 2 * g_nof_repeat * c_on_period;  -- choose user input FIFO in tb sufficiently large to not overflow before tb_end

  constant c_bsn_max            : unsigned(c_dp_stream_bsn_w - 1 downto 0) := (others => '1');
  constant c_bsn_gap            : unsigned(c_dp_stream_bsn_w - 1 downto 0) := to_unsigned(ceil_div(c_off_period, c_period_size), c_dp_stream_bsn_w);  -- depends on c_off_period

  constant c_tx_seq_init        : natural := 10;

  type t_tb_state is (s_init, s_bg_enable_sync, s_bg_enable, s_bg_disable, s_user, s_xoff, s_xon);

  signal tb_end                     : std_logic := '0';
  signal rst                        : std_logic;
  signal clk                        : std_logic := '1';
  signal en_sync                    : std_logic;
  signal tb_state                   : t_tb_state;
  signal prev_tb_state              : t_tb_state;
  signal random                     : std_logic_vector(15 downto 0) := (others => '0');
  signal out_siso_ready             : std_logic;
  signal out_siso_xon               : std_logic;

  signal ram_bg_data_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal ram_bg_data_miso           : t_mem_miso := c_mem_miso_rst;
  signal reg_bg_ctrl_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal reg_bg_ctrl_miso           : t_mem_miso := c_mem_miso_rst;
  signal reg_tx_seq_mosi            : t_mem_mosi := c_mem_mosi_rst;
  signal reg_tx_seq_miso            : t_mem_miso := c_mem_miso_rst;

  signal usr_src_in_arr             : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal usr_src_out_arr            : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal usr_src_out_0_data         : std_logic_vector(c_buf_dat_w - 1 downto 0);
  signal usr_fifo_src_in_arr        : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal usr_fifo_src_out_arr       : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal out_siso_arr               : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal out_sosi_arr               : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal prev_out_sosi_0            : t_dp_sosi;
  signal out_sosi_0                 : t_dp_sosi;
  signal out_sosi_0_data            : std_logic_vector(c_buf_dat_w - 1 downto 0);
  signal prev_out_sosi_0_data_u0    : std_logic_vector(c_buf_dat_w - 1 downto 0);
  signal prev_out_sosi_0_data_u1    : std_logic_vector(c_buf_dat_w - 1 downto 0);

  signal bg_en                      : std_logic := '0';
  signal bg_en_dly                  : std_logic := '0';
  signal verify_sosi_arr_en         : std_logic := '0';
  signal verify_bsn_en              : std_logic := '0';
  signal verify_sync_en             : std_logic := '0';
  signal verify_data_en             : std_logic := '0';
  signal verify_tx_seq_en           : std_logic := '0';
  signal verify_no_bg_buffer_ram_en : std_logic := '0';
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 5;

  p_dut_bg_ctrl : process
  begin
    bg_en <= '0';
    en_sync <= '0';
    out_siso_xon <= '1';  -- Flow control XON

    if c_verify_usr_rflow = true then
      tb_state <= s_user;
      proc_common_wait_some_cycles(clk, c_on_period);  -- On time
    else
      -- BG init
      tb_state <= s_init;
      proc_common_wait_until_low(clk, rst);  -- Wait until reset has finished
      proc_common_wait_some_cycles(clk, 10);  -- Wait an additional amount of cycles
      proc_mem_mm_bus_wr(1,   c_block_size, clk, reg_bg_ctrl_mosi);  -- Set the nof samples per block
      proc_mem_mm_bus_wr(2,  c_sync_period, clk, reg_bg_ctrl_mosi);  -- Set the nof blocks per sync
      proc_mem_mm_bus_wr(3,     g_gap_size, clk, reg_bg_ctrl_mosi);  -- Set the gapsize
      proc_mem_mm_bus_wr(4,  c_mem_lo_addr, clk, reg_bg_ctrl_mosi);  -- Set the mem low address
      proc_mem_mm_bus_wr(5,  c_mem_hi_addr, clk, reg_bg_ctrl_mosi);  -- Set the mem high address
      proc_mem_mm_bus_wr(6,   c_bsn_offset, clk, reg_bg_ctrl_mosi);  -- Set the lower part of the initial bsn
      proc_mem_mm_bus_wr(7,              0, clk, reg_bg_ctrl_mosi);  -- Set the higher part of the initial bsn

      -- BG enable at en_sync pulse
      proc_mem_mm_bus_wr(0,              3, clk, reg_bg_ctrl_mosi);  -- Enable the block generator at en_sync pulse.
      tb_state <= s_bg_enable_sync;
      proc_common_wait_some_cycles(clk, c_pend_period);  -- Wait an additional amount of cycles
      proc_common_gen_pulse(clk, en_sync);  -- Issue the en_sync pulse to start the enabled block generator
      bg_en <= '1';
      proc_common_wait_some_cycles(clk, c_on_period);  -- On time
      proc_common_wait_until_high(clk, out_sosi_0.valid);  -- Wait until block generator [0] is active
      proc_common_wait_some_cycles(clk, 20);  -- Wait an additional amount of cycles

      for I in 0 to g_nof_repeat - 1 loop
        -- BG disable
        proc_mem_mm_bus_wr(0,   0, clk, reg_bg_ctrl_mosi);  -- Disable the block generator.
        tb_state <= s_bg_disable;
        bg_en <= '0';
        proc_common_wait_some_cycles(clk, c_off_period);  -- Off time

        -- BG enable
        proc_mem_mm_bus_wr(0,   1, clk, reg_bg_ctrl_mosi);  -- Enable the block generator.
        tb_state <= s_bg_enable;
        bg_en <= '1';
        proc_common_wait_some_cycles(clk, c_on_period);  -- On time
        proc_common_wait_until_high(clk, out_sosi_0.valid);  -- Wait until block generator [0] is active
        proc_common_wait_some_cycles(clk, 20);  -- Wait an additional amount of cycles
      end loop;

      for I in 0 to g_nof_repeat - 1 loop
        -- BG flow off
        out_siso_xon <= '0';  -- Flow control XON
        tb_state <= s_xoff;
        bg_en <= '0';
        proc_common_wait_some_cycles(clk, c_off_period);  -- Off time

        -- BG flow on
        out_siso_xon <= '1';  -- Flow control XON
        tb_state <= s_xon;
        bg_en <= '1';
        proc_common_wait_some_cycles(clk, c_on_period);  -- On time
      end loop;
    end if;

    proc_common_wait_some_cycles(clk, c_end_period);  -- Some extra time
    tb_end <= '1';
    wait;
  end process;

  p_dut_tx_seq_ctrl : process
  begin
    proc_common_wait_until_low(clk, rst);  -- Wait until reset has finished
    proc_common_wait_some_cycles(clk, 10);  -- Wait an additional amount of cycles
    proc_mem_mm_bus_wr(1, c_tx_seq_init, clk, reg_tx_seq_mosi);  -- Init Tx sequence
    proc_common_wait_some_cycles(clk, 10);  -- Wait for some latency
    proc_mem_mm_bus_wr(0, c_diag_seq_tx_reg_en_cntr, clk, reg_tx_seq_mosi);  -- Enable Tx CNTR sequence
    wait;
  end process;

  -------------------------------------------------
  -- User input modelled by another BG
  -------------------------------------------------
  u_user : entity work.mms_diag_block_gen
  generic map (
    g_nof_streams        => g_nof_streams,
    g_buf_dat_w          => c_buf_dat_w,
    g_buf_addr_w         => c_buf_addr_w,
    g_file_name_prefix   => c_file_name_prefix,
    g_diag_block_gen_rst => c_diag_block_gen_enabled  -- user BG is default enabled
  )
  port map (
    -- System
    mm_rst           => rst,
    mm_clk           => clk,
    dp_rst           => rst,
    dp_clk           => clk,
    en_sync          => '0',
    -- ST interface
    out_siso_arr     => usr_src_in_arr,
    out_sosi_arr     => usr_src_out_arr
  );

  -- Use sufficiently large FIFO to provide siso.ready flow control to the user input
  no_user_fifo : if c_use_user_fifo = false generate
    usr_src_in_arr       <= usr_fifo_src_in_arr;
    usr_fifo_src_out_arr <= usr_src_out_arr;
  end generate;

  gen_user_fifo : if c_use_user_fifo = true generate
    gen_nof_streams : for I in 0 to g_nof_streams - 1 generate
      u_user_fifo : entity dp_lib.dp_fifo_sc
      generic map (
        g_data_w         => c_buf_dat_w,
        g_bsn_w          => c_dp_stream_bsn_w,
        g_empty_w        => c_dp_stream_empty_w,
        g_channel_w      => c_dp_stream_channel_w,
        g_error_w        => c_dp_stream_error_w,
        g_use_bsn        => true,
        g_use_empty      => true,
        g_use_channel    => true,
        g_use_error      => true,
        g_use_sync       => true,
        g_use_ctrl       => true,
        g_use_complex    => false,
        g_fifo_size      => c_usr_fifo_size,
        g_fifo_af_margin => 4,
        g_fifo_rl        => 1
      )
      port map (
        rst         => rst,
        clk         => clk,
        -- Monitor FIFO filling
        wr_ful      => OPEN,
        usedw       => OPEN,
        rd_emp      => OPEN,
        -- ST sink
        snk_out     => usr_src_in_arr(I),
        snk_in      => usr_src_out_arr(I),
        -- ST source
        src_in      => usr_fifo_src_in_arr(I),
        src_out     => usr_fifo_src_out_arr(I)
      );
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  random <= func_common_random(random) when rising_edge(clk);

  out_siso_ready <= '1'                 when g_flow_control_verify = e_active  else
                    random(random'high) when g_flow_control_verify = e_random  else
                    '1';

  p_out_siso_arr : process(out_siso_ready, out_siso_xon)
  begin
    for I in 0 to g_nof_streams - 1 loop
      out_siso_arr(I).ready <= '1';
      out_siso_arr(I).xon <= out_siso_xon;
    end loop;
    if c_verify_usr_rflow = true then
      out_siso_arr(0).ready <= out_siso_ready;  -- only verify flow control via stream 0
    end if;
  end process;

  -------------------------------------------------
  -- Device under test
  -------------------------------------------------
  u_dut : entity work.mms_diag_block_gen
  generic map (
    -- Generate configurations
    g_use_usr_input      => g_use_usr_input,
    g_use_bg             => g_use_bg,
    g_use_tx_seq         => g_use_tx_seq,
    -- General
    g_nof_streams        => g_nof_streams,
    -- BG settings
    g_use_bg_buffer_ram  => g_use_bg_buffer_ram,
    g_buf_dat_w          => c_buf_dat_w,
    g_buf_addr_w         => c_buf_addr_w,
    g_file_name_prefix   => c_file_name_prefix,
    g_diag_block_gen_rst => c_diag_block_gen_rst,  -- user BG is default disabled, MM controlled by p_dut_bg_ctrl
    -- User input multiplexer option
    g_usr_bypass_xonoff  => g_usr_bypass_xonoff,
    -- Tx_seq
    g_seq_dat_w          => c_buf_dat_w  -- >= 1, test sequence data width. Choose g_seq_dat_w <= g_buf_dat_w
  )
  port map (
    -- System
    mm_rst           => rst,
    mm_clk           => clk,
    dp_rst           => rst,
    dp_clk           => clk,
    en_sync          => en_sync,
    -- MM interface
    ram_bg_data_mosi => ram_bg_data_mosi,
    ram_bg_data_miso => ram_bg_data_miso,
    reg_bg_ctrl_mosi => reg_bg_ctrl_mosi,
    reg_bg_ctrl_miso => reg_bg_ctrl_miso,
    reg_tx_seq_mosi  => reg_tx_seq_mosi,
    reg_tx_seq_miso  => reg_tx_seq_miso,
    -- ST interface
    usr_siso_arr     => usr_fifo_src_in_arr,
    usr_sosi_arr     => usr_fifo_src_out_arr,
    out_siso_arr     => out_siso_arr,
    out_sosi_arr     => out_sosi_arr
  );

  -------------------------------------------------
  -- Verification
  -------------------------------------------------

  prev_tb_state <= tb_state when rising_edge(clk);

  usr_src_out_0_data <= usr_src_out_arr(0).data(c_buf_dat_w - 1 downto 0);

  out_sosi_0      <= out_sosi_arr(0);  -- use BG[0] as reference
  out_sosi_0_data <= out_sosi_0.data(c_buf_dat_w - 1 downto 0);

  -- Verify : Equal data in all g_nof_streams
  verify_sosi_arr_en <= '1' when bg_en_dly = '1';

  p_compare_out_sosi_arr : process(clk)
  begin
    if rising_edge(clk) then
      if verify_sosi_arr_en = '1' then
        for I in 0 to g_nof_streams - 1 loop
          -- All outputs have the same framing
          assert out_sosi_0.sync = out_sosi_arr(I).sync
            report "tb_mms_diag_block_gen: Wrong out_sosi_arr(*).sync";
          assert out_sosi_0.bsn = out_sosi_arr(I).bsn
            report "tb_mms_diag_block_gen: Wrong out_sosi_arr(*).bsn";
          assert out_sosi_0.im = out_sosi_arr(I).im
            report "tb_mms_diag_block_gen: Wrong out_sosi_arr(*).im";
          assert out_sosi_0.valid = out_sosi_arr(I).valid
            report "tb_mms_diag_block_gen: Wrong out_sosi_arr(*).valid";
          assert out_sosi_0.sop = out_sosi_arr(I).sop
            report "tb_mms_diag_block_gen: Wrong out_sosi_arr(*).sop";
          assert out_sosi_0.eop = out_sosi_arr(I).eop
            report "tb_mms_diag_block_gen: Wrong out_sosi_arr(*).eop";

          if g_use_bg_buffer_ram = true then
            -- Only the re part differs per output given the BG buffer data from c_file_name_prefix = "data/bf_in_data"
            assert TO_UINT(out_sosi_0.re) = TO_UINT(out_sosi_arr(I).re) - I * 4
              report "tb_mms_diag_block_gen: Wrong out_sosi_arr(*).re";
          else
            assert out_sosi_0.im = out_sosi_arr(I).im
              report "tb_mms_diag_block_gen: Wrong out_sosi_arr(*).im";
          end if;
        end loop;
      end if;
    end if;
  end process;

  -- Verify : BSN
  p_verify_bsn_en : process
  begin
    proc_common_wait_until_evt(clk, bg_en);
    verify_bsn_en <= '0';
    proc_common_wait_some_cycles(clk, 3 * c_period_size);
    verify_bsn_en <= '1';
    wait until rising_edge(clk);
  end process;

  proc_dp_verify_data("BSN", c_bsn_max, c_bsn_gap, clk, verify_bsn_en, out_sosi_0.sop, out_sosi_0.bsn, prev_out_sosi_0.bsn);  -- verify both BG and user

  -- Verify : sync
  bg_en_dly <= bg_en'delayed(5 * clk_period);
  verify_sync_en <= verify_bsn_en and bg_en_dly when g_use_bg = true else '0';  -- only verify BG sync interval (ignore user sync interval)

  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_sync_en, out_sosi_0.sync, out_sosi_0.sop, out_sosi_0.bsn);

  -- Verify : Tx counter data
  -- . g_use_tx_seq=TRUE
  -- . g_use_bg_buffer_ram=FALSE
  p_verify_data_en : process
  begin
    verify_data_en <= '0';
    if g_use_usr_input = true then
      proc_common_wait_until_high(clk, usr_fifo_src_out_arr(0).valid);
    end if;
    if g_use_bg = true then
      proc_common_wait_until_evt(clk, bg_en);
    end if;
    proc_common_wait_some_cycles(clk, 2 * c_period_size);
    verify_data_en <= '1';

    while true loop
      if tb_state = s_xoff then
        verify_data_en <= '0';
      elsif prev_tb_state = s_xoff then
        proc_common_wait_some_cycles(clk, 2 * c_period_size);
        verify_data_en <= '1';
      end if;
      wait until rising_edge(clk);
    end loop;
  end process;

  verify_tx_seq_en           <= '1' when verify_data_en = '1' and g_use_tx_seq = true                                else '0';
  verify_no_bg_buffer_ram_en <= '1' when verify_data_en = '1' and g_use_tx_seq = false and g_use_bg_buffer_ram = false else '0';

  proc_dp_verify_data("Tx sequence counter data",                   0, 0, clk, verify_tx_seq_en,           out_sosi_0.valid, out_sosi_0_data, prev_out_sosi_0_data_u0);
  proc_dp_verify_data("No BG buffer RAM counter data", c_block_size-1, 0, clk, verify_no_bg_buffer_ram_en, out_sosi_0.valid, out_sosi_0_data, prev_out_sosi_0_data_u1);

  -- Verify : User input bypass all
  p_compare_usr_bypass_all : process(clk)
  begin
    if g_use_usr_input = true and g_use_bg = false and g_use_tx_seq = false then
      if rising_edge(clk) then
        assert out_sosi_arr = usr_fifo_src_out_arr
          report "tb_mms_diag_block_gen: out_sosi_arr   /= usr_src_out_arr";
        assert usr_fifo_src_in_arr = out_siso_arr
          report "tb_mms_diag_block_gen: usr_src_in_arr /= out_siso_arr";
      end if;
    end if;
  end process;
end tb;
