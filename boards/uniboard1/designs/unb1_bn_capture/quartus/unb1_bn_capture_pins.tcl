# Pin assignments
# -- GENERAL: clk, pps, wdi, inta, intb
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_general_pins.tcl
# -- 1GbE Control Interface
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_1Gbe_pins.tcl
# -- I2C Interface to Sensors
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_sensor_pins.tcl
# -- Other: version[1:0], id[7:0], testio[7:0]
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_other_pins.tcl
# -- BN_BI ADC pins
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/BACK_NODE_adc_pins.tcl


