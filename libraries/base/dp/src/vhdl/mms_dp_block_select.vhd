-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Eric Kooistra
-- Purpose:
-- . Provide MM interface to dp_block_select
-- Description:
--   See dp_block_select.vhd for functional description.
--   All g_nof_streams use the same index_lo and index_hi settings.
--   The default index_lo and index_hi are set by g_index_lo and g_index_hi.
--   The index_lo and index_hi can be read and set via the MM interface.

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_block_select is
  generic (
    g_nof_streams          : natural := 1;
    g_nof_blocks_per_sync  : natural;
    g_index_lo             : natural := 0;
    g_index_hi             : natural
  );
  port (
    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;
    dp_rst       : in  std_logic;
    dp_clk       : in  std_logic;
    -- Control
    reg_mosi     : in  t_mem_mosi;
    reg_miso     : out t_mem_miso;
    -- ST sink
    snk_out_arr  : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr   : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr   : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr  : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_block_select;

architecture str of mms_dp_block_select is
  constant c_field_arr : t_common_field_arr(1 downto 0) := ( (field_name_pad("index_hi"), "RW", 32, field_default(g_index_hi) ),
                                                             (field_name_pad("index_lo"), "RW", 32, field_default(g_index_lo) ));

  signal mm_fields_out : std_logic_vector(field_slv_out_len(c_field_arr) - 1 downto 0);

  signal index_lo      : natural;
  signal index_hi      : natural;
begin
  -- Use same control for all streams
  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    g_use_slv_in_val  => false,  -- use FALSE to save logic when always slv_in_val='1'
    g_field_arr       => c_field_arr
  )
  port map (
    mm_clk     => mm_clk,
    mm_rst     => mm_rst,

    mm_mosi    => reg_mosi,
    mm_miso    => reg_miso,

    slv_clk    => dp_clk,
    slv_rst    => dp_rst,

    slv_out    => mm_fields_out
  );

  index_lo <= TO_UINT(mm_fields_out(field_hi(c_field_arr, "index_lo") downto field_lo(c_field_arr, "index_lo")));
  index_hi <= TO_UINT(mm_fields_out(field_hi(c_field_arr, "index_hi") downto field_lo(c_field_arr, "index_hi")));

  gen_dp_block_select : for I in 0 to g_nof_streams - 1 generate
    u_dp_block_select : entity work.dp_block_select
    generic map (
      g_nof_blocks_per_sync => g_nof_blocks_per_sync,
      g_index_lo            => g_index_lo,
      g_index_hi            => g_index_hi
    )
    port map (
      rst          => dp_rst,
      clk          => dp_clk,
      -- Control
      index_lo     => index_lo,
      index_hi     => index_hi,
      -- ST sink
      snk_out      => snk_out_arr(I),
      snk_in       => snk_in_arr(I),
      -- ST source
      src_in       => src_in_arr(I),
      src_out      => src_out_arr(I)
    );
  end generate;
end str;
