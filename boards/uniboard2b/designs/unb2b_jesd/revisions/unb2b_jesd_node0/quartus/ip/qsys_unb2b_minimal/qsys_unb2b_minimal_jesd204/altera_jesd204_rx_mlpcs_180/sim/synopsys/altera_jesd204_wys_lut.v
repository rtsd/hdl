// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
Io8PrRAGzmVolhstWbYrBFZmz/X8ZzJZ/L7fCIMO3pLLApb/3Tid/GDdktIR4Xip
K3bb/LwBUrqSmzTnRJ5Jp7ANXxZvO9tQoq/9c/KOuF1mHnMLr2A59tRBJOJsvUOS
kf1hiIrx/p+ZH3VGFeP5GtlalNgU18iG7vbE1rJpNLo=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 1904 )
`pragma protect data_block
9wNplROa9MJa1c0KN4mN2/BeYV/RVc4zstJrQobt2t2KjdVcUm1v1PLFh0ORCs2b
akT5UPBHytxTPKW9DHWbOTYJEU5r598TGIWi6IOvYGN40FX6LtFAgxA5gJaAxfL6
MdcXKLe4M5HsgT1RZkn9MOEXIbHQfYLOwJTA7CQrgW8HVIxrnNlqT3GEMv/mW3fw
yWOA2HQiyRgSOWDFSnnnwz2usnltz3dDAMZjGNuefveltJzIz1j6kBYTRqb08l3t
K/NqpXH/09hveQlEIg+ct9ktbPiQZmZpo5Ln18okOFL3dneKfQRTefYnvp2whKc9
UJ4eKdX4oLWCn9mMZEdWDbvyaNLgyXnNi8vLk+S0SEDIj2ImIEBuWVZxOxegXBVz
6ruG8d75R1dO/2vwGOq+fiuF9JyFgLf1bhzLqL8C/yXOTxzt3RFHnFdjZKSoMc4Z
KV1hW6UL/ehhTOJAD540GCfi0ZdSLz/JwDkuDKzs2heJ28dPzrQhsVYmG+Ec6c65
J5uBDurn1l10xOwfN+QVjlSsV+A37cXe6Q/6sl1raUK01d9zxdKK88QtZyGuz8u7
ss50ClkVbv4L/7QKZsY1x9KSW/4EubN/AxvzdjbNkWbngNAnD+cAUZHJc2NRl2D4
bUAiaQ2hkYbxHMZKIUYr+32l+b3YqLjaSdNzhZdQVyMEQW60ci065gHAEaMohU/Y
Cna0uIsA96UnWWIx/kyyF9cdXgsqb3yVmEA4t9jnjZqrBKvdlFl9Hsh+ztyq964o
A778JnpPLCFVFLFFN/uThvoO4Nyjc8dR+I289zUUl/Q0dPdzcvzVjww4RaBrZ0r3
nZhNT2FbQpAJ/2WCTNzZl0shBQdnfzdYvcHGN1a7rZ/D5/+5ZJv9OPLTG9kMW/iT
m8exXn6+7qaYn3q0w5TwVZyrIbAfwab+ApzMAIeAJ3qABMtxGgMr9Bux/Cf8dLg7
VbKdHj+qBw1fUrtjqTt3Qdt00O5xkjM33eEekE55uw+iuTJlIU5T90Ay3+Ssf629
1wdJrsD4GPsYLi6O+pKq0/fAXe5LD1Qss0njeKvRi92IM6iYwGXmH2FWmW8QmWOF
OvUMx8MffALGcH0MsEmq3A/P7AWd2p8jlfetqq591rvgT4nXUOfJlwfYT0F8exIu
ihXpJGAXFWNbxXRlH511JhTOl+l4tm56IJcCpGxMid4WtsQ6VeJG8TURCk6WTJbW
GaLh+XK2Ucu/MjhV2lKxfidzFuHWQuQrHDikYHdsWiCyuJxyJP/RG1XFSUls5j3h
SpMxf1sHcMGXJrk57Yoh0QTERoDCwjcXgxULPQ4aWR52PSwAL0fjufsiQco6rV4b
5pM7ZFy8syQwKkG9nNGBN6G+qV2AKClot0HwWBAp/VESS1cGqGbbp/WyapyQ37Ix
N56hD5wpj5bgszq0IGwBnPXijk56ceYpDUCXHKjXn+Suk4OFrha/VBax+0uDkLiQ
GAw6LJN8UYzG/Ae83bXb5ye9UGpCZkwFlSiGCpeoXntrWaiCdgNOVoLpeJUdVPKE
JsgBy2JLnStDQgmWTCfiBMw35Ph/IoKyA1NWO1F+E3i0ed+WwknMgI9LFfcsfpRW
xYYoOezNzTyJ1Gs5+mzCIM74I7o6wmxagOkcRoZeN/N6o2sRm+bH6k9yB093b5Xh
hACFKSnLD9xayhh+5fLF8A1PEdecAeH0N9CYw5zPcf4uHprYGF94PUiai0QaQwpy
P6fiJmTtXs1Nub9TIhMZ2RR1/1Ye6aEQROqQPiiQX7ocQyARdU68UZP6GZSGE4ZB
y8WiofZOrBI9K4WpzU1NcjMdT5UIUxXEwhA4oee6Y3En3jWOmvEUW8UMvr0hnlK2
ivktcSIXEF0rTgNCrNygjArqXx4mw8/9khpG52py26vPw4K3tTi9LU/BZvWf7KY3
Ywzfd1geN3CHipURMaqEklr1vDPrUdjpqOZHYMsgym2jtgIfkOM4/1meEBgwqprf
A2JPx0zcy39FJTSILFVKgsJ6X0Np7hxFxae72lA+5m3c5ei7YMVH0EbJcetXPQsm
SXIoYUyrKMviTDI5uhheVjmOgFxhswBR2bPJZObpJdy+vHH/15EljMsputPYGIwK
zseavc9GNVA97UwoyGu4ZLdi6MOq/nK2OB91c9gytCG0NVNZZjbux+XVra7Y7VC0
vWH0vxuGu5ccuc/v21imavLe0rswAVjRB5gvhiWnBiMAMcAwYvQgaIfu3X5w2pBX
SDWEiLK+nPQNshqGVcRdPNiLtUamj0kZBJ1MEqnqTy/tGhUKNFE8TydkKPI/hkeM
dJ0UL098KatYn44jMRkNx2d96bdZf0+1uUsxsWNbxlCKgysDz0jpMF0xBs5ZlagR
gywNa1pIbhUFV/v89BCSyXi32Y+VLcRb6tcIYuGqcggTKz1/3CNoI92ngL/47MmP
XMnpt3eMUqOAqH+x8XvAuWUJuwYo/lVToF6hyIQL28YveblatFVmbYbi3Z8KwseS
QhtdcQbBY2HgCThRzczVy0iqOzE3Gl4+ksJaohaByFY=

`pragma protect end_protected
