-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Move the valid input data through a shift register to have access to
--   g_nof_words, before outputing them.
-- Description:
--   This dp_shiftreg puts the valid input data through a shift register. When
--   the shift register is full then every new valid input also causes a valid
--   output.
--   When g_flush_eop is TRUE then shift register gets flushed if it contains
--   an eop, provided that the output is ready.
--   Note that for g_nof_words = 1 the dp_shiftreg still differs from
--   dp_pipeline, because dp_pipeline outputs valid data when the
--   src_in.ready='1', while dp_shiftreg outputs valid data when it gets
--   pushed out by valid input data or by flush eop.
--   Via cur_shiftreg_inputs for all words of the current shift register is
--   available externally for monitoring. When g_modify_support is TRUE then
--   the input for all the words in de shift register can be modified via
--   new_shiftreg_inputs register. Note that cur_shiftreg_inputs(0) = snk_in.
--   Use g_output_reg=TRUE to register the src_out SOSI control signals,
--   because they have a combinatorial AND-delay if g_output_reg=FALSE.
-- Remarks:
-- . Ready latency = 1

entity dp_shiftreg is
  generic (
    g_output_reg     : boolean := false;
    g_flush_eop      : boolean := true;
    g_modify_support : boolean := false;
    g_nof_words      : natural := 1  -- >= 1, shift register length
  );
  port (
    rst                 : in  std_logic;
    clk                 : in  std_logic;
    -- ST sink
    snk_out             : out t_dp_siso;
    snk_in              : in  t_dp_sosi;
    -- Control shift register contents
    cur_shiftreg_inputs : out t_dp_sosi_arr(0 to g_nof_words - 1);
    new_shiftreg_inputs : in  t_dp_sosi_arr(0 to g_nof_words - 1) := (others => c_dp_sosi_rst);
    -- ST source
    src_in              : in  t_dp_siso;
    src_out             : out t_dp_sosi
  );
end dp_shiftreg;

architecture rtl of dp_shiftreg is
  constant c_high  : natural := g_nof_words - 1;  -- shift stream data from 0 to g_nof_words-1

  signal prev_src_in           : t_dp_siso;
  signal flush                 : std_logic;
  signal shift                 : std_logic;

  signal i_cur_shiftreg_inputs : t_dp_sosi_arr(0 to c_high);
  signal shiftreg_inputs       : t_dp_sosi_arr(0 to c_high);

  signal shiftreg              : t_dp_sosi_arr(0 to c_high);
  signal nxt_shiftreg          : t_dp_sosi_arr(0 to c_high);

  signal shiftreg_out          : t_dp_sosi;
begin
  cur_shiftreg_inputs <= i_cur_shiftreg_inputs;

  -- Pass on frame level flow control
  snk_out.xon <= src_in.xon;

  -- No change in ready latency
  snk_out.ready <= src_in.ready;  -- no need to allow snk_out.ready when shift register is not full yet while src_in.ready is low

  ------------------------------------------------------------------------------
  -- Shift register
  ------------------------------------------------------------------------------

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      prev_src_in <= c_dp_siso_rst;
      shiftreg    <= (others => c_dp_sosi_rst);
    elsif rising_edge(clk) then
      prev_src_in <= src_in;
      shiftreg    <= nxt_shiftreg;
    end if;
  end process;

  no_flush_eop : if g_flush_eop = false generate
    flush <= '0';
  end generate;

  gen_flush_eop : if g_flush_eop = true generate
    p_eop : process(prev_src_in, shiftreg)
      variable v_eop : std_logic := '0';
    begin
      flush <= '0';
      if prev_src_in.ready = '1' then  -- like valid, flush must also fit to RL=1, therefore use prev_src_in.ready
        flush <= func_dp_stream_arr_or(shiftreg, "EOP");
      end if;
    end process;
  end generate;

  shift <= snk_in.valid or flush;

  p_shiftreg : process(snk_in, shift, shiftreg, shiftreg_inputs)
  begin
    nxt_shiftreg <= shiftreg;
    if shift = '1' then
      nxt_shiftreg <= shiftreg_inputs;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Shift register contents control
  ------------------------------------------------------------------------------

  -- Make shift register available for external monitoring
  p_monitor : process(snk_in, shiftreg)
  begin
    i_cur_shiftreg_inputs(0)           <= snk_in;
    --i_cur_shiftreg_inputs(1 TO c_high) <= shiftreg(0 TO c_high-1);  -- causes "Fatal: (SIGSEGV) Bad handle or reference" in Modelsim, therefore use LOOP
    for I in 1 to c_high loop
      i_cur_shiftreg_inputs(I) <= shiftreg(I - 1);
    end loop;
  end process;

  -- Support for shift register modification
  no_modification : if g_modify_support = false generate
    shiftreg_inputs <= i_cur_shiftreg_inputs;
  end generate;

  gen_modification : if g_modify_support = true generate
    shiftreg_inputs <= new_shiftreg_inputs;
  end generate;

  ------------------------------------------------------------------------------
  -- Output control
  ------------------------------------------------------------------------------

  p_shiftreg_out : process(shiftreg, shift)
  begin
    shiftreg_out       <= shiftreg(c_high);
    -- The shiftreg contents is valid, but the shiftreg output is only valid when it is shifted out,
    -- therefore AND the SOSI control with shift
    shiftreg_out.sync  <= shiftreg(c_high).sync  and shift;
    shiftreg_out.valid <= shiftreg(c_high).valid and shift;
    shiftreg_out.sop   <= shiftreg(c_high).sop   and shift;
    shiftreg_out.eop   <= shiftreg(c_high).eop   and shift;
  end process;

  no_output_reg : if g_output_reg = false generate
    src_out <= shiftreg_out;
  end generate;

  gen_output_reg : if g_output_reg = true generate
    u_output : entity work.dp_pipeline
    port map (
      rst         => rst,
      clk         => clk,
      -- ST sink
      snk_out     => OPEN,
      snk_in      => shiftreg_out,
      -- ST source
      src_in      => src_in,
      src_out     => src_out
    );
  end generate;
end rtl;
