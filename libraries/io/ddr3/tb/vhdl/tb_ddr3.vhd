--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- This testbench tests the different type of DDR controllers:
--
--                        - aphy_4g_800
--                        - aphy_4g_1066
--                        - uphy_4g_800_master
--                        - uphy_4g_1066_master
--                        - uphy_4g_800_slave
--                        - uphy_4g_1066_slave
--
-- The DUT can be selected, using the c_phy and c_mts constants.
--
-- Testbench is selftesting:
--
-- > run -all
--
-- Known issues: The memory model used for the aphy_4g_1066 simulation
--               gives an error at the end of the write cyclus.
--

library IEEE, tech_ddr_lib, common_lib, dp_lib, diagnostics_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use work.ddr3_pkg.all;

entity tb_ddr3 is
end entity tb_ddr3;

architecture str of tb_ddr3 is
  constant c_phy              : natural      := 1;  -- 0: ALTMEMPHY  1: UNIPHY_MASTER 2: UNIPHY_SLAVE
  constant c_mts              : natural      := 800;  -- Available options: 800 and 1066

  constant c_ctlr_ref_clk_per : time         := 5 ns;  -- 200 MHz
  constant c_ddr              : t_c_ddr3_phy := c_ddr3_phy_4g;

  constant c_data_w           : natural      := 256;  -- 32;

  signal ctlr_ref_clk         : std_logic    := '0';
  signal ctlr_rst             : std_logic    := '1';
  signal tb_end               : std_logic    := '0';
  signal ctlr_gen_clk         : std_logic;
  signal ctlr_gen_rst         : std_logic;

  signal ctlr_rdy             : std_logic;
  signal ctlr_init_done       : std_logic;

  signal dvr_start_addr       : t_ddr3_addr;
  signal dvr_end_addr         : t_ddr3_addr;

  signal dvr_en               : std_logic;
  signal dvr_wr_not_rd        : std_logic;
  signal dvr_done             : std_logic;

  signal wr_siso              : t_dp_siso;
  signal wr_sosi              : t_dp_sosi;

  signal wr_siso_pre_fifo     : t_dp_siso;
  signal wr_sosi_pre_fifo     : t_dp_sosi;

  signal rd_siso              : t_dp_siso;
  signal rd_sosi              : t_dp_sosi;

  signal src_diag_en          : std_logic;
  signal src_val_cnt          : std_logic_vector(31 downto 0);

  signal snk_diag_en          : std_logic;
  signal snk_diag_res         : std_logic;
  signal snk_diag_res_val     : std_logic;
  signal snk_val_cnt          : std_logic_vector(31 downto 0);

  signal phy_in               : t_tech_ddr3_phy_in;
  signal phy_io               : t_tech_ddr3_phy_io;
  signal phy_ou               : t_tech_ddr3_phy_ou;

  signal ras_n                : std_logic_vector(0 downto 0);
  signal cas_n                : std_logic_vector(0 downto 0);
  signal we_n                 : std_logic_vector(0 downto 0);

  signal flush_ena            : std_logic;
begin
  ctlr_ref_clk   <= not(ctlr_ref_clk) or tb_end after c_ctlr_ref_clk_per / 2;
  ctlr_rst       <= '0' after 100 ns;

  dvr_start_addr <= c_ddr3_addr_lo;
  dvr_end_addr   <= c_ddr3_addr_hi_sim;

  flush_ena      <= '0';

  p_stimuli : process
  begin
    tb_end        <= '0';
    dvr_en        <= '0';
    src_diag_en   <= '0';
    dvr_wr_not_rd <= '0';
    snk_diag_en   <= '0';

    wait until ctlr_init_done = '1';
    for i in 0 to 1 loop
      wait until rising_edge(ctlr_gen_clk);  -- Give the driver FSM a cycle to go into idle mode
    end loop;

    -- START WRITE
    src_diag_en   <= '1';
    dvr_wr_not_rd <= '1';
    dvr_en        <= '1';

    wait until rising_edge(ctlr_gen_clk);

    dvr_en        <= '0';

    -- WRITE DONE
    wait until dvr_done = '1';

    src_diag_en   <= '0';

    -- START READ
    snk_diag_en   <= '1';
    dvr_wr_not_rd <= '0';
    dvr_en        <= '1';

    wait until rising_edge(ctlr_gen_clk);

    dvr_en        <= '0';

    -- READ DONE
    wait until dvr_done = '1';

    wait for 2 us;  -- 'Done' means all requests are posted. Wait for the last read data to arrive.

    assert snk_diag_res_val = '1'
      report "[ERROR] DIAG_RES INVALID!"
      severity FAILURE;
    assert snk_diag_res = '0'
      report "[ERROR] NON-ZERO DIAG_RES!"
      severity FAILURE;
    assert false
      report "[OK] Test passed."
      severity NOTE;
    tb_end        <= '1';

    wait;
  end process;

  u_diagnostics: entity diagnostics_lib.diagnostics
  generic map (
    g_dat_w             => c_data_w,
    g_nof_streams       => 1
     )
  port map (
    rst                 => ctlr_gen_rst,
    clk                 => ctlr_gen_clk,

    snk_out_arr(0)      => rd_siso,
    snk_in_arr(0)       => rd_sosi,
    snk_diag_en(0)      => snk_diag_en,
    snk_diag_md(0)      => '1',
    snk_diag_res(0)     => snk_diag_res,
    snk_diag_res_val(0) => snk_diag_res_val,
    snk_val_cnt(0)      => snk_val_cnt,

    src_out_arr(0)      => wr_sosi,
    src_in_arr(0)       => wr_siso,
    src_diag_en(0)      => src_diag_en,
    src_diag_md(0)      => '1',
    src_val_cnt(0)      => src_val_cnt
  );

  gen_uphy_4g_model : if c_phy > 0 generate
    u_4gb_ddr3_model : component alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en
    generic map (
      MEM_IF_ADDR_WIDTH            => 15,
      MEM_IF_ROW_ADDR_WIDTH        => 15,
      MEM_IF_COL_ADDR_WIDTH        => 10,
      MEM_IF_CS_PER_RANK           => 1,
      MEM_IF_CONTROL_WIDTH         => 1,
      MEM_IF_DQS_WIDTH             => 8,
      MEM_IF_CS_WIDTH              => 2,
      MEM_IF_BANKADDR_WIDTH        => 3,
      MEM_IF_DQ_WIDTH              => 64,
      MEM_IF_CK_WIDTH              => 2,
      MEM_IF_CLK_EN_WIDTH          => 2,
      DEVICE_WIDTH                 => 1,
      MEM_TRCD                     => 6,
      MEM_TRTP                     => 3,
      MEM_DQS_TO_CLK_CAPTURE_DELAY => 100,
      MEM_CLK_TO_DQS_CAPTURE_DELAY => 100000,
      MEM_IF_ODT_WIDTH             => 2,
      MEM_MIRROR_ADDRESSING_DEC    => 0,
      MEM_REGDIMM_ENABLED          => false,
      DEVICE_DEPTH                 => 1,
      MEM_GUARANTEED_WRITE_INIT    => false,
      MEM_VERBOSE                  => true,
      MEM_INIT_EN                  => false,
      MEM_INIT_FILE                => "",
      DAT_DATA_WIDTH               => 32
    )
    port map (
      mem_a       => phy_ou.a(c_ddr.a_w - 1 downto 0),  -- memory.mem_a
      mem_ba      => phy_ou.ba,  -- .mem_ba
      mem_ck      => phy_ou.ck,  -- .mem_ck
      mem_ck_n    => phy_ou.ck_n,  -- .mem_ck_n
      mem_cke     => phy_ou.cke(c_ddr.cs_w - 1 downto 0),  -- .mem_cke
      mem_cs_n    => phy_ou.cs_n(c_ddr.cs_w - 1 downto 0),  -- .mem_cs_n
      mem_dm      => phy_ou.dm,  -- .mem_dm
      mem_ras_n   => ras_n,  -- .mem_ras_n
      mem_cas_n   => cas_n,  -- .mem_cas_n
      mem_we_n    => we_n,  -- .mem_we_n
      mem_reset_n => phy_ou.reset_n,  -- .mem_reset_n
      mem_dq      => phy_io.dq,  -- .mem_dq
      mem_dqs     => phy_io.dqs,  -- .mem_dqs
      mem_dqs_n   => phy_io.dqs_n,  -- .mem_dqs_n
      mem_odt     => phy_ou.odt  -- .mem_odt
    );

    ras_n(0) <= phy_ou.ras_n;
    cas_n(0) <= phy_ou.cas_n;
    we_n(0)  <= phy_ou.we_n;
  end generate;

  u_ddr3_module: entity work.ddr3
  generic map(
    g_phy              => c_phy,
    g_mts              => c_mts,
    g_ddr              => c_ddr,
    g_wr_data_w        => c_data_w,
    g_rd_data_w        => c_data_w
  )
  port map (
    ctlr_ref_clk       => ctlr_ref_clk,
    ctlr_rst           => ctlr_rst,

    ctlr_gen_clk       => ctlr_gen_clk,
    ctlr_gen_rst       => ctlr_gen_rst,

    ctlr_init_done     => ctlr_init_done,
    ctlr_rdy           => ctlr_rdy,

    dvr_start_addr     => dvr_start_addr,
    dvr_end_addr       => dvr_end_addr,
    dvr_en             => dvr_en,
    dvr_wr_not_rd      => dvr_wr_not_rd,
    dvr_done           => dvr_done,

    wr_clk             => ctlr_gen_clk,
    wr_rst             => ctlr_gen_rst,

    wr_sosi            => wr_sosi,
    wr_siso            => wr_siso,

    flush_ena          => flush_ena,

    rd_sosi            => rd_sosi,
    rd_siso            => rd_siso,

    rd_clk             => ctlr_gen_clk,
    rd_rst             => ctlr_gen_rst,

    phy_ou             => phy_ou,
    phy_io             => phy_io,
    phy_in             => phy_in
  );
end architecture str;
