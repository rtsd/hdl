-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Scale samples per block of valid samples in a stream via MM
-- Description:
-- The mms_dp_scale.vhd component consists of mms_dp_gain.vhd and dp_requantize.
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_scale is
  generic (
    -- mms_dp_gain generics
    g_complex_data     : boolean := true;
    g_complex_gain     : boolean := false;
    g_gain_init_re     : integer := 1;
    g_gain_init_im     : integer := 0;
    g_gain_w           : natural := 16;
    g_in_dat_w         : natural := 9;

    -- dp_requantize generics
    g_out_dat_w           : natural := 8;
    g_lsb_w               : natural := 16;
    g_lsb_round           : boolean := true;  -- when TRUE ROUND else TRUNCATE the input LSbits
    g_lsb_round_clip      : boolean := false;  -- when TRUE round clip to +max to avoid wrapping to output -min (signed) or 0 (unsigned) due to rounding
    g_msb_clip            : boolean := true;  -- when TRUE CLIP else WRAP the input MSbits
    g_msb_clip_symmetric  : boolean := false  -- when TRUE CLIP signed symmetric to +c_smax and -c_smax, else to +c_smax and c_smin_symm
                                                  -- for wrapping when g_msb_clip=FALSE the g_msb_clip_symmetric is ignored, so signed wrapping is done asymmetric
  );
  port (
    -- System
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;
    dp_rst            : in  std_logic;
    dp_clk            : in  std_logic;

    -- MM interface
    reg_gain_re_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_gain_re_miso  : out t_mem_miso;
    reg_gain_im_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_gain_im_miso  : out t_mem_miso;

    reg_gain_re       : out std_logic_vector(g_gain_w - 1 downto 0);
    reg_gain_im       : out std_logic_vector(g_gain_w - 1 downto 0);

    -- ST interface
    in_sosi           : in  t_dp_sosi;
    out_sosi          : out t_dp_sosi
  );
end mms_dp_scale;

architecture str of mms_dp_scale is
  constant c_dp_requantize_complex : boolean := g_complex_gain or g_complex_data;
  constant c_gain_out_dat_w        : natural := g_gain_w + g_in_dat_w - 1;  -- -1 to compensate for double sign-bit

  signal dp_gain_out_sosi : t_dp_sosi;
begin
  ---------------------------------------------------------------
  -- Gain
  ---------------------------------------------------------------
  u_mms_dp_gain : entity work.mms_dp_gain
  generic map (
    g_complex_data    => g_complex_data,
    g_complex_gain    => g_complex_gain,
    g_gain_init_re    => g_gain_init_re,
    g_gain_init_im    => g_gain_init_im,
    g_gain_w          => g_gain_w,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => c_gain_out_dat_w
  )
  port map (
    -- System
    mm_rst => mm_rst,
    mm_clk => mm_clk,
    dp_rst => dp_rst,
    dp_clk => dp_clk,

    -- MM interface
    reg_gain_re_mosi => reg_gain_re_mosi,
    reg_gain_re_miso => reg_gain_re_miso,
    reg_gain_im_mosi => reg_gain_im_mosi,
    reg_gain_im_miso => reg_gain_im_miso,

    reg_gain_re      => reg_gain_re,
    reg_gain_im      => reg_gain_im,

    in_sosi  =>  in_sosi,
    out_sosi =>  dp_gain_out_sosi
  );

  ---------------------------------------------------------------
  -- Requantize
  ---------------------------------------------------------------
  u_dp_requantize : entity work.dp_requantize
  generic map (
    g_complex             => c_dp_requantize_complex,
    g_representation      => "SIGNED",
    g_lsb_w               => g_lsb_w,
    g_lsb_round           => g_lsb_round,
    g_lsb_round_clip      => g_lsb_round_clip,
    g_msb_clip            => g_msb_clip,
    g_msb_clip_symmetric  => g_msb_clip_symmetric,
    g_in_dat_w            => c_gain_out_dat_w,
    g_out_dat_w           => g_out_dat_w
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    -- ST sink
    snk_in  => dp_gain_out_sosi,
    -- ST source
    src_out => out_sosi
  );
end str;
