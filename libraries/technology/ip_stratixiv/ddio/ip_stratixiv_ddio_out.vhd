-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Double data rate FPGA output or register single data rate FPGA output
-- Description:
--   Using the MegaWizard simply provides a GUI to set and select the generics
--   and IO. First used MegaWizard to learn how this works and what the default
--   values are:
--     %UNB%\Firmware\modules\common\src\ip\MegaWizard\ddio_out.vhd
--   Then directly used the component to define the instance. The component
--   comes from:
--     %QUARTUS_ROOTDIR%\..\quartus\eda\sim_lib\altera_mf_components.vhd
--
--   component altddio_out
--       generic (
--           width                  : positive;  -- required parameter
--           power_up_high          : string := "OFF";
--           oe_reg                 : string := "UNUSED";
--           extend_oe_disable      : string := "UNUSED";
--           invert_output          : string := "OFF";
--           intended_device_family : string := "Stratix";
--           lpm_hint               : string := "UNUSED";
--           lpm_type               : string := "altddio_out" );
--       port (
--           datain_h   : in std_logic_vector(width-1 downto 0);
--           datain_l   : in std_logic_vector(width-1 downto 0);
--           outclock   : in std_logic;
--           outclocken : in std_logic := '1';
--           aset       : in std_logic := '0';
--           aclr       : in std_logic := '0';
--           sset       : in std_logic := '0';
--           sclr       : in std_logic := '0';
--           oe         : in std_logic := '1';
--           dataout    : out std_logic_vector(width-1 downto 0);
--           oe_out     : out std_logic_vector(width-1 downto 0) );
--   end component;

library IEEE;
use IEEE.std_logic_1164.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

entity ip_stratixiv_ddio_out is
  generic(
    g_device_family : string  := "Stratix IV";
    g_width         : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
end ip_stratixiv_ddio_out;

architecture str of ip_stratixiv_ddio_out is
begin
  ddio : ALTDDIO_OUT
  generic map (
    extend_oe_disable => "OFF",
    intended_device_family => g_device_family,
    invert_output => "OFF",
    lpm_hint => "UNUSED",
    lpm_type => "altddio_out",
    oe_reg => "UNREGISTERED",
    power_up_high => "OFF",
    width => g_width
  )
  port map (
    aclr => rst,
    datain_h => in_dat_hi,
    datain_l => in_dat_lo,
    outclock => in_clk,
    outclocken => in_clk_en,
    dataout => out_dat
  );
end str;
