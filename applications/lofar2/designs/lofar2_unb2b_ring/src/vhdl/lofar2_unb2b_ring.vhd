-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:
--   Design for Lofar2 Ring interface testing.
-- Description:
--   Unb2b version for lab testing, see https://support.astron.nl/confluence/x/jyu7Ag.
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, diag_lib, dp_lib, tech_jesd204b_lib, wpfb_lib, lofar2_sdp_lib, tech_pll_lib, tr_10gbe_lib, eth_lib, ring_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_field_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use unb2b_board_lib.unb2b_board_peripherals_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use work.lofar2_unb2b_ring_pkg.all;
use eth_lib.eth_pkg.all;
use ring_lib.ring_pkg.all;

entity lofar2_unb2b_ring is
  generic (
    g_design_name            : string  := "lofar2_unb2b_ring";
    g_design_note            : string  := "UNUSED";
    g_sim                    : boolean := false;  -- Overridden by TB
    g_sim_sync_timeout       : natural := c_sdp_sim.sync_timeout;
    g_sim_unb_nr             : natural := c_sdp_sim.unb_nr;
    g_sim_node_nr            : natural := c_sdp_sim.node_nr;
    g_stamp_date             : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time             : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id            : string  := "";  -- revision ID     -- set by QSF
    g_factory_image          : boolean := false;
    g_protect_addr_range     : boolean := false
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK        : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines
    -- front transceivers QSFP1 for 10GbE output to CEP.
    QSFP_0_RX     : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_0_TX     : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);

    -- ring transceivers
    RING_0_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');  -- Using qsfp bus width also for ring interfaces
    RING_0_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
    RING_1_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    RING_1_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);

    -- LEDs
    QSFP_LED     : out   std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0)

  );
end lofar2_unb2b_ring;

architecture str of lofar2_unb2b_ring is
  -- Revision parameters
  constant c_revision_select        : t_lofar2_unb2b_ring_config := func_sel_revision_rec(g_design_name);
  constant c_nof_lanes              : natural := c_revision_select.N_ring_lanes;
  -- Firmware version x.y
  constant c_fw_version             : t_unb2b_board_fw_version := (2, 0);
  constant c_mm_clk_freq            : natural := c_unb2b_board_mm_clk_freq_100M;
  constant c_lofar2_sample_clk_freq : natural := c_sdp_N_clk_per_sync;  -- fixed 200 MHz for LOFAR2.0 stage 1

   -- QSFP
  constant c_nof_qsfp_bus           : natural := 1;
  constant c_nof_streams_qsfp       : natural := c_unb2b_board_tr_qsfp.bus_w * c_nof_qsfp_bus;  -- 4

  -- RING
  constant c_nof_ring_bus           : natural := 2;
  constant c_ring_bus_w             : natural := 4;  -- Using 4 phisically, there are 12
  constant c_nof_streams_ring       : natural := c_ring_bus_w * c_nof_ring_bus;  -- c_unb2b_board_tr_ring.bus_w*c_nof_ring_bus; -- 8

  -- 10GbE
  constant c_nof_even_lanes         : natural := ceil_div(c_nof_lanes, 2);  -- nof lanes transmitting in positive direction. Using ceil_div as c_nof_lanes might be odd.
  constant c_nof_odd_lanes          : natural := c_nof_lanes / 2;  -- nof lanes transmitting in negative direction. Note for c_nof_lanes = 1 -> c_nof_odd_lanes = 1 / 2 => 0 which is desired.
  constant c_nof_mac                : natural := 3 * c_nof_even_lanes;  -- must match one of the MAC IP variations, e.g. 3, 12, 24, 48

  constant c_lane_data_w               : natural := 64;
  constant c_lane_packet_length        : natural := c_sdp_V_ring_pkt_len_max - c_ring_dp_hdr_field_size;  -- = 48 longwords per packet, so the maximum data rate with a packetrate of 195312.5 and all 16 nodes combined = 16 * 48 / 1024 * 64 * 200M = 9.6 Gb/s
  constant c_use_dp_layer              : boolean := true;
  constant c_nof_rx_monitors           : natural := c_sdp_N_pn_max;
  constant c_nof_tx_monitors           : natural := c_sdp_N_pn_max;
  constant c_err_bi                    : natural := 0;
  constant c_nof_err_counts            : natural := c_revision_select.N_error_counts;
  constant c_bsn_at_sync_check_channel : natural := 1;
  constant c_validate_channel          : boolean := true;
  constant c_validate_channel_mode     : string  := "=";
  constant c_fifo_tx_fill              : natural := c_lane_packet_length + sel_a_b(c_use_dp_layer, c_ring_dp_hdr_field_size, c_ring_eth_hdr_field_size);  -- total packet length
  constant c_fifo_tx_size              : natural := 2 * c_lane_packet_length;
  constant c_lofar2_sync_timeout       : natural := c_lofar2_sample_clk_freq + c_lofar2_sample_clk_freq / 10;  -- 10% margin.
  constant c_sync_timeout              : natural := sel_a_b(g_sim, g_sim_sync_timeout, c_lofar2_sync_timeout );
  constant c_nof_if                    : natural := 3;  -- 3 different interfaces, QSFP, RING_0 and RING_1
  constant c_qsfp_if_offset            : natural := 0;  -- QSFP signals are indexed at c_nof_if * I.
  constant c_ring_0_if_offset          : natural := 1;  -- RING_0 signals are indexed at c_nof_if * I + 1.
  constant c_ring_1_if_offset          : natural := 2;  -- RING_1 signals are indexed at c_nof_if * I + 2.

  constant c_addr_w_reg_ring_lane_info                : natural := 1;
  constant c_addr_w_reg_bsn_monitor_v2_ring_rx        : natural := ceil_log2(c_nof_rx_monitors) + 3;
  constant c_addr_w_reg_bsn_monitor_v2_ring_tx        : natural := ceil_log2(c_nof_tx_monitors) + 3;
  constant c_addr_w_reg_dp_block_validate_err         : natural := ceil_log2(c_nof_err_counts + 3);
  constant c_addr_w_reg_dp_block_validate_bsn_at_sync : natural := ceil_log2(3);

  constant c_reg_ring_input_select     : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(c_nof_lanes),
                                         dat_w    => 1,
                                         nof_dat  => c_nof_lanes,
                                         init_sl  => '0');  -- default use lane input = 0, 1 = local input.

  signal gn_index                   : natural;
  signal this_rn                    : std_logic_vector(c_byte_w - 1 downto 0);

  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic := '0';

  signal dp_pps                     : std_logic;
  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_copi               : t_mem_copi := c_mem_copi_rst;
  signal reg_wdi_cipo               : t_mem_cipo := c_mem_cipo_rst;

  -- PPSH
  signal reg_ppsh_copi              : t_mem_copi := c_mem_copi_rst;
  signal reg_ppsh_cipo              : t_mem_cipo := c_mem_cipo_rst;

  -- UniBoard system info
  signal reg_unb_system_info_copi   : t_mem_copi := c_mem_copi_rst;
  signal reg_unb_system_info_cipo   : t_mem_cipo := c_mem_cipo_rst;
  signal rom_unb_system_info_copi   : t_mem_copi := c_mem_copi_rst;
  signal rom_unb_system_info_cipo   : t_mem_cipo := c_mem_cipo_rst;

  -- UniBoard I2C sens
  signal reg_unb_sens_copi          : t_mem_copi := c_mem_copi_rst;
  signal reg_unb_sens_cipo          : t_mem_cipo := c_mem_cipo_rst;

  -- pm bus
  signal reg_unb_pmbus_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_unb_pmbus_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- FPGA sensors
  signal reg_fpga_temp_sens_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_fpga_temp_sens_cipo     : t_mem_cipo := c_mem_cipo_rst;
  signal reg_fpga_voltage_sens_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_fpga_voltage_sens_cipo  : t_mem_cipo := c_mem_cipo_rst;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_copi             : t_mem_copi := c_mem_copi_rst;  -- ETH TSE MAC registers
  signal eth1g_tse_cipo             : t_mem_cipo := c_mem_cipo_rst;
  signal eth1g_reg_copi             : t_mem_copi := c_mem_copi_rst;  -- ETH control and status registers
  signal eth1g_reg_cipo             : t_mem_cipo := c_mem_cipo_rst;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_copi             : t_mem_copi := c_mem_copi_rst;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_cipo             : t_mem_cipo := c_mem_cipo_rst;

  -- EPCS read
  signal reg_dpmm_data_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_dpmm_data_cipo         : t_mem_cipo := c_mem_cipo_rst;
  signal reg_dpmm_ctrl_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_dpmm_ctrl_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- EPCS write
  signal reg_mmdp_data_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_mmdp_data_cipo         : t_mem_cipo := c_mem_cipo_rst;
  signal reg_mmdp_ctrl_copi         : t_mem_copi := c_mem_copi_rst;
  signal reg_mmdp_ctrl_cipo         : t_mem_cipo := c_mem_cipo_rst;

  -- EPCS status/control
  signal reg_epcs_copi              : t_mem_copi := c_mem_copi_rst;
  signal reg_epcs_cipo              : t_mem_cipo := c_mem_cipo_rst;

  -- Remote Update
  signal reg_remu_copi              : t_mem_copi := c_mem_copi_rst;
  signal reg_remu_cipo              : t_mem_cipo := c_mem_cipo_rst;

  -- Scrap ram
  signal ram_scrap_copi             : t_mem_copi := c_mem_copi_rst;
  signal ram_scrap_cipo             : t_mem_cipo := c_mem_cipo_rst;

  ----------------------------------------------
  -- 10 GbE
  ----------------------------------------------
  signal reg_tr_10GbE_mac_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_tr_10GbE_mac_cipo      : t_mem_cipo := c_mem_cipo_rst;

  signal reg_tr_10GbE_eth10g_copi   : t_mem_copi := c_mem_copi_rst;
  signal reg_tr_10GbE_eth10g_cipo   : t_mem_cipo := c_mem_cipo_rst;

  signal reg_diag_bg_copi                            : t_mem_copi := c_mem_copi_rst;
  signal reg_diag_bg_cipo                            : t_mem_cipo := c_mem_cipo_rst;
  signal ram_diag_bg_copi                            : t_mem_copi := c_mem_copi_rst;
  signal ram_diag_bg_cipo                            : t_mem_cipo := c_mem_cipo_rst;

  signal reg_dp_xonoff_lane_copi                     : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_xonoff_lane_cipo                     : t_mem_cipo := c_mem_cipo_rst;

  signal reg_dp_xonoff_local_copi                    : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_xonoff_local_cipo                    : t_mem_cipo := c_mem_cipo_rst;

  signal reg_ring_info_copi                          : t_mem_copi := c_mem_copi_rst;
  signal reg_ring_info_cipo                          : t_mem_cipo := c_mem_cipo_rst;

  signal reg_ring_lane_info_copi                     : t_mem_copi := c_mem_copi_rst;
  signal reg_ring_lane_info_cipo                     : t_mem_cipo := c_mem_cipo_rst;

  signal reg_bsn_monitor_v2_ring_rx_copi             : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_ring_rx_cipo             : t_mem_cipo := c_mem_cipo_rst;

  signal reg_bsn_monitor_v2_ring_tx_copi             : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_ring_tx_cipo             : t_mem_cipo := c_mem_cipo_rst;

  signal reg_dp_block_validate_err_copi              : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_block_validate_err_cipo              : t_mem_cipo := c_mem_cipo_rst;

  signal reg_dp_block_validate_bsn_at_sync_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_block_validate_bsn_at_sync_cipo      : t_mem_cipo := c_mem_cipo_rst;

  signal reg_ring_lane_info_copi_arr                 : t_mem_copi_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_ring_lane_info_cipo_arr                 : t_mem_cipo_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_bsn_monitor_v2_ring_rx_copi_arr         : t_mem_copi_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_ring_rx_cipo_arr         : t_mem_cipo_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_bsn_monitor_v2_ring_tx_copi_arr         : t_mem_copi_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_ring_tx_cipo_arr         : t_mem_cipo_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_dp_block_validate_err_copi_arr          : t_mem_copi_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_dp_block_validate_err_cipo_arr          : t_mem_cipo_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_dp_block_validate_bsn_at_sync_copi_arr  : t_mem_copi_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_dp_block_validate_bsn_at_sync_cipo_arr  : t_mem_cipo_arr(c_nof_lanes - 1 downto 0) := (others => c_mem_cipo_rst);

  signal from_lane_sosi_arr               : t_dp_sosi_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal to_lane_sosi_arr                 : t_dp_sosi_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);

  signal dp_mux_snk_out_2arr              : t_dp_siso_2arr_2(c_nof_lanes - 1 downto 0);
  signal dp_mux_snk_in_2arr               : t_dp_sosi_2arr_2(c_nof_lanes - 1 downto 0);

  signal dp_xonoff_lane_src_out_arr       : t_dp_sosi_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_xonoff_lane_src_in_arr        : t_dp_siso_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_siso_rdy);
  signal dp_xonoff_local_snk_in_arr       : t_dp_sosi_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_xonoff_local_src_out_arr      : t_dp_sosi_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_xonoff_local_src_in_arr       : t_dp_siso_arr(c_nof_lanes - 1 downto 0) := (others => c_dp_siso_rdy);

  signal lane_rx_cable_even_sosi_arr      : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal lane_rx_board_even_sosi_arr      : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal lane_tx_cable_even_sosi_arr      : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal lane_tx_board_even_sosi_arr      : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal lane_rx_cable_odd_sosi_arr       : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal lane_rx_board_odd_sosi_arr       : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal lane_tx_cable_odd_sosi_arr       : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal lane_tx_board_odd_sosi_arr       : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0) := (others => c_dp_sosi_rst);

  signal tr_10gbe_src_out_arr             : t_dp_sosi_arr(c_nof_mac - 1 downto 0) := (others => c_dp_sosi_rst);
  signal tr_10gbe_snk_in_arr              : t_dp_sosi_arr(c_nof_mac - 1 downto 0) := (others => c_dp_sosi_rst);
  signal tr_10gbe_src_in_arr              : t_dp_siso_arr(c_nof_mac - 1 downto 0) := (others => c_dp_siso_rdy);
  signal tr_10gbe_snk_out_arr             : t_dp_siso_arr(c_nof_mac - 1 downto 0) := (others => c_dp_siso_rdy);

  signal bs_sosi                          : t_dp_sosi := c_dp_sosi_rst;
  signal local_sosi                       : t_dp_sosi := c_dp_sosi_rst;

  signal ring_info                        : t_ring_info;

  -- 10GbE
  signal tr_ref_clk_312                    : std_logic;
  signal tr_ref_clk_156                    : std_logic;
  signal tr_ref_rst_156                    : std_logic;
  signal i_QSFP_TX                         : t_unb2b_board_qsfp_bus_2arr(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0) := (others => (others => '0'));
  signal i_QSFP_RX                         : t_unb2b_board_qsfp_bus_2arr(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0) := (others => (others => '0'));
  signal i_RING_TX                         : t_unb2b_board_qsfp_bus_2arr(c_nof_ring_bus - 1 downto 0) := (others => (others => '0'));
  signal i_RING_RX                         : t_unb2b_board_qsfp_bus_2arr(c_nof_ring_bus - 1 downto 0) := (others => (others => '0'));

  signal tr_10gbe_serial_tx_arr            : std_logic_vector(c_nof_mac - 1 downto 0) := (others => '0');
  signal tr_10gbe_serial_rx_arr            : std_logic_vector(c_nof_mac - 1 downto 0) := (others => '0');

  signal unb2_board_front_io_serial_tx_arr : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus * c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
  signal unb2_board_front_io_serial_rx_arr : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus * c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');

  signal this_bck_id                       : std_logic_vector(c_unb2b_board_nof_uniboard_w - 1 downto 0);
  signal this_chip_id                      : std_logic_vector(c_unb2b_board_nof_chip_w - 1 downto 0);

  -- QSFP LEDS
  signal qsfp_green_led_arr                : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr                  : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);

  signal unb2_board_qsfp_leds_tx_siso_arr : t_dp_siso_arr(c_unb2b_board_tr_qsfp.nof_bus * c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => c_dp_siso_rst);
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb2b_board_lib.ctrl_unb2b_board
  generic map (
    g_sim                     => g_sim,
    g_design_name             => g_design_name,
    g_design_note             => g_design_note,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_revision_id             => g_revision_id,
    g_fw_version              => c_fw_version,
    g_mm_clk_freq             => c_mm_clk_freq,
    g_eth_clk_freq            => c_unb2b_board_eth_clk_freq_125M,
    g_aux                     => c_unb2b_board_aux,
    g_factory_image           => g_factory_image,
    g_protect_addr_range      => g_protect_addr_range,
    g_dp_clk_freq             => c_unb2b_board_ext_clk_freq_200M,
    g_dp_clk_use_pll          => false
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,  -- Can be external 200MHz, or PLL generated
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    this_chip_id             => this_chip_id,
    this_bck_id              => this_bck_id,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_copi,
    reg_remu_miso            => reg_remu_cipo,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_copi,
    reg_dpmm_data_miso       => reg_dpmm_data_cipo,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_copi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_cipo,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_copi,
    reg_mmdp_data_miso       => reg_mmdp_data_cipo,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_copi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_cipo,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_copi,
    reg_epcs_miso            => reg_epcs_cipo,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_copi,
    reg_wdi_miso             => reg_wdi_cipo,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_copi,
    reg_unb_system_info_miso => reg_unb_system_info_cipo,
    rom_unb_system_info_mosi => rom_unb_system_info_copi,
    rom_unb_system_info_miso => rom_unb_system_info_cipo,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_copi,
    reg_unb_sens_miso        => reg_unb_sens_cipo,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_copi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_cipo,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_copi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_cipo,

    reg_unb_pmbus_mosi       => reg_unb_pmbus_copi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_cipo,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_copi,
    reg_ppsh_miso            => reg_ppsh_cipo,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_copi,
    eth1g_tse_miso           => eth1g_tse_cipo,
    eth1g_reg_mosi           => eth1g_reg_copi,
    eth1g_reg_miso           => eth1g_reg_cipo,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_copi,
    eth1g_ram_miso           => eth1g_ram_cipo,

    ram_scrap_mosi           => ram_scrap_copi,
    ram_scrap_miso           => ram_scrap_cipo,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    SENS_SC                  => SENS_SC,
    SENS_SD                  => SENS_SD,
    -- PM bus
    PMBUS_SC                 => PMBUS_SC,
    PMBUS_SD                 => PMBUS_SD,
    PMBUS_ALERT              => PMBUS_ALERT,

    -- . 1GbE Control Interface
    ETH_clk                  => ETH_CLK,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM controller
  -----------------------------------------------------------------------------
  u_mmc : entity work.mmc_lofar2_unb2b_ring
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
   )
  port map(
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- mm interfaces for control
    reg_wdi_copi                           => reg_wdi_copi,
    reg_wdi_cipo                           => reg_wdi_cipo,
    reg_unb_system_info_copi               => reg_unb_system_info_copi,
    reg_unb_system_info_cipo               => reg_unb_system_info_cipo,
    rom_unb_system_info_copi               => rom_unb_system_info_copi,
    rom_unb_system_info_cipo               => rom_unb_system_info_cipo,
    reg_unb_sens_copi                      => reg_unb_sens_copi,
    reg_unb_sens_cipo                      => reg_unb_sens_cipo,
    reg_unb_pmbus_copi                     => reg_unb_pmbus_copi,
    reg_unb_pmbus_cipo                     => reg_unb_pmbus_cipo,
    reg_fpga_temp_sens_copi                => reg_fpga_temp_sens_copi,
    reg_fpga_temp_sens_cipo                => reg_fpga_temp_sens_cipo,
    reg_fpga_voltage_sens_copi             => reg_fpga_voltage_sens_copi,
    reg_fpga_voltage_sens_cipo             => reg_fpga_voltage_sens_cipo,
    reg_ppsh_copi                          => reg_ppsh_copi,
    reg_ppsh_cipo                          => reg_ppsh_cipo,
    eth1g_mm_rst                           => eth1g_mm_rst,
    eth1g_tse_copi                         => eth1g_tse_copi,
    eth1g_tse_cipo                         => eth1g_tse_cipo,
    eth1g_reg_copi                         => eth1g_reg_copi,
    eth1g_reg_cipo                         => eth1g_reg_cipo,
    eth1g_reg_interrupt                    => eth1g_reg_interrupt,
    eth1g_ram_copi                         => eth1g_ram_copi,
    eth1g_ram_cipo                         => eth1g_ram_cipo,
    reg_dpmm_data_copi                     => reg_dpmm_data_copi,
    reg_dpmm_data_cipo                     => reg_dpmm_data_cipo,
    reg_dpmm_ctrl_copi                     => reg_dpmm_ctrl_copi,
    reg_dpmm_ctrl_cipo                     => reg_dpmm_ctrl_cipo,
    reg_mmdp_data_copi                     => reg_mmdp_data_copi,
    reg_mmdp_data_cipo                     => reg_mmdp_data_cipo,
    reg_mmdp_ctrl_copi                     => reg_mmdp_ctrl_copi,
    reg_mmdp_ctrl_cipo                     => reg_mmdp_ctrl_cipo,
    reg_epcs_copi                          => reg_epcs_copi,
    reg_epcs_cipo                          => reg_epcs_cipo,
    reg_remu_copi                          => reg_remu_copi,
    reg_remu_cipo                          => reg_remu_cipo,
    reg_bsn_monitor_v2_ring_rx_copi        => reg_bsn_monitor_v2_ring_rx_copi,
    reg_bsn_monitor_v2_ring_rx_cipo        => reg_bsn_monitor_v2_ring_rx_cipo,
    reg_bsn_monitor_v2_ring_tx_copi        => reg_bsn_monitor_v2_ring_tx_copi,
    reg_bsn_monitor_v2_ring_tx_cipo        => reg_bsn_monitor_v2_ring_tx_cipo,
    reg_diag_bg_copi                       => reg_diag_bg_copi,
    reg_diag_bg_cipo                       => reg_diag_bg_cipo,
    ram_diag_bg_copi                       => ram_diag_bg_copi,
    ram_diag_bg_cipo                       => ram_diag_bg_cipo,
    reg_ring_lane_info_copi                => reg_ring_lane_info_copi,
    reg_ring_lane_info_cipo                => reg_ring_lane_info_cipo,
    reg_dp_xonoff_lane_copi                => reg_dp_xonoff_lane_copi,
    reg_dp_xonoff_lane_cipo                => reg_dp_xonoff_lane_cipo,
    reg_dp_xonoff_local_copi               => reg_dp_xonoff_local_copi,
    reg_dp_xonoff_local_cipo               => reg_dp_xonoff_local_cipo,
    reg_dp_block_validate_err_copi         => reg_dp_block_validate_err_copi,
    reg_dp_block_validate_err_cipo         => reg_dp_block_validate_err_cipo,
    reg_dp_block_validate_bsn_at_sync_copi => reg_dp_block_validate_bsn_at_sync_copi,
    reg_dp_block_validate_bsn_at_sync_cipo => reg_dp_block_validate_bsn_at_sync_cipo,
    reg_ring_info_copi                     => reg_ring_info_copi,
    reg_ring_info_cipo                     => reg_ring_info_cipo,
    reg_tr_10GbE_mac_copi                  => reg_tr_10GbE_mac_copi,
    reg_tr_10GbE_mac_cipo                  => reg_tr_10GbE_mac_cipo,
    reg_tr_10GbE_eth10g_copi               => reg_tr_10GbE_eth10g_copi,
    reg_tr_10GbE_eth10g_cipo               => reg_tr_10GbE_eth10g_cipo,
    ram_scrap_copi                         => ram_scrap_copi,
    ram_scrap_cipo                         => ram_scrap_cipo
  );

  -----------------------------------------------------------------------------
  -- MM Mux
  -----------------------------------------------------------------------------
  u_mem_mux_ring_lane_info : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_nof_lanes,
    g_mult_addr_w => c_addr_w_reg_ring_lane_info
  )
  port map (
    mosi     => reg_ring_lane_info_copi,
    miso     => reg_ring_lane_info_cipo,
    mosi_arr => reg_ring_lane_info_copi_arr,
    miso_arr => reg_ring_lane_info_cipo_arr
  );

  u_mem_mux_bsn_monitor_v2_ring_rx : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_nof_lanes,
    g_mult_addr_w => c_addr_w_reg_bsn_monitor_v2_ring_rx
  )
  port map (
    mosi     => reg_bsn_monitor_v2_ring_rx_copi,
    miso     => reg_bsn_monitor_v2_ring_rx_cipo,
    mosi_arr => reg_bsn_monitor_v2_ring_rx_copi_arr,
    miso_arr => reg_bsn_monitor_v2_ring_rx_cipo_arr
  );

  u_mem_mux_bsn_monitor_v2_ring_tx : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_nof_lanes,
    g_mult_addr_w => c_addr_w_reg_bsn_monitor_v2_ring_tx
  )
  port map (
    mosi     => reg_bsn_monitor_v2_ring_tx_copi,
    miso     => reg_bsn_monitor_v2_ring_tx_cipo,
    mosi_arr => reg_bsn_monitor_v2_ring_tx_copi_arr,
    miso_arr => reg_bsn_monitor_v2_ring_tx_cipo_arr
  );

  u_mem_mux_dp_block_validate_err : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_nof_lanes,
    g_mult_addr_w => c_addr_w_reg_dp_block_validate_err
  )
  port map (
    mosi     => reg_dp_block_validate_err_copi,
    miso     => reg_dp_block_validate_err_cipo,
    mosi_arr => reg_dp_block_validate_err_copi_arr,
    miso_arr => reg_dp_block_validate_err_cipo_arr
  );

  u_mem_mux_dp_block_validate_bsn_at_sync : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_nof_lanes,
    g_mult_addr_w => c_addr_w_reg_dp_block_validate_bsn_at_sync
  )
  port map (
    mosi     => reg_dp_block_validate_bsn_at_sync_copi,
    miso     => reg_dp_block_validate_bsn_at_sync_cipo,
    mosi_arr => reg_dp_block_validate_bsn_at_sync_copi_arr,
    miso_arr => reg_dp_block_validate_bsn_at_sync_cipo_arr
  );

  -----------------------------------------------------------------------------
  -- MMP diag_block_gen
  -----------------------------------------------------------------------------
  u_mmp_diag_block_gen : entity diag_lib.mms_diag_block_gen
  port map (
    mm_rst  => mm_rst,
    mm_clk  => mm_clk,
    dp_rst  => dp_rst,
    dp_clk  => dp_clk,
    en_sync => dp_pps,

   reg_bg_ctrl_mosi => reg_diag_bg_copi,
   reg_bg_ctrl_miso => reg_diag_bg_cipo,
   ram_bg_data_mosi => ram_diag_bg_copi,
   ram_bg_data_miso => ram_diag_bg_cipo,

   out_sosi_arr(0)  => local_sosi
  );
  bs_sosi <= local_sosi;

  -----------------------------------------------------------------------------
  -- MMP dp_xonoff from_lane_sosi
  -----------------------------------------------------------------------------
  u_mmp_dp_xonoff_lane : entity dp_lib.mms_dp_xonoff
  generic map (
    g_nof_streams   => c_nof_lanes,
    g_default_value => '1'  -- default enabled, because standard behaviour is to only pass on packets from lane.
  )
  port map (
    mm_rst => mm_rst,
    mm_clk => mm_clk,

    reg_mosi => reg_dp_xonoff_lane_copi,
    reg_miso => reg_dp_xonoff_lane_cipo,

    dp_rst  => dp_rst,
    dp_clk  => dp_clk,

    snk_out_arr => OPEN,
    snk_in_arr  => from_lane_sosi_arr,

    src_in_arr  => dp_xonoff_lane_src_in_arr,
    src_out_arr => dp_xonoff_lane_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- MMP dp_xonoff local_sosi
  -----------------------------------------------------------------------------
  gen_copy_local: for I in 0 to c_nof_lanes - 1 generate
    dp_xonoff_local_snk_in_arr(I) <= local_sosi;  -- copy local sosi to all lanes
  end generate;

  u_mmp_dp_xonoff_local : entity dp_lib.mms_dp_xonoff
  generic map (
    g_nof_streams   => c_nof_lanes,
    g_default_value => '0'  -- default disabled, because standard behaviour is to only pass on packets from lane.
  )
  port map (
    mm_rst => mm_rst,
    mm_clk => mm_clk,

    reg_mosi => reg_dp_xonoff_local_copi,
    reg_miso => reg_dp_xonoff_local_cipo,

    dp_rst => dp_rst,
    dp_clk => dp_clk,

    snk_out_arr => OPEN,
    snk_in_arr  => dp_xonoff_local_snk_in_arr,

    src_in_arr  => dp_xonoff_local_src_in_arr,
    src_out_arr => dp_xonoff_local_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- DP Mux
  -----------------------------------------------------------------------------
  gen_dp_mux: for I in 0 to c_nof_lanes - 1 generate
    dp_xonoff_lane_src_in_arr(I)  <= dp_mux_snk_out_2arr(I)(0);
    dp_xonoff_local_src_in_arr(I) <= dp_mux_snk_out_2arr(I)(1);
    dp_mux_snk_in_2arr(I)(0)      <= dp_xonoff_lane_src_out_arr(I);
    dp_mux_snk_in_2arr(I)(1)      <= dp_xonoff_local_src_out_arr(I);

    u_dp_mux : entity dp_lib.dp_mux
    generic map (
      g_append_channel_lo => false,
      g_sel_ctrl_invert   => true,
      g_use_fifo          => true,
      g_bsn_w             => c_longword_w,
      g_data_w            => c_lane_data_w,
      g_in_channel_w      => c_byte_w,
      g_error_w           => c_nof_err_counts,
      g_use_bsn           => true,
      g_use_in_channel    => true,
      g_use_error         => true,
      g_use_sync          => true,
      -- Using fifo size of 2 * packet length for both inputs of the mux as a packet might arrive on both the local and remote input.
      g_fifo_size         => array_init(2 * c_lane_packet_length, 2)
    )
    port map (
      rst => dp_rst,
      clk => dp_clk,

      snk_out_arr => dp_mux_snk_out_2arr(I),
      snk_in_arr  => dp_mux_snk_in_2arr(I),

      src_in  => c_dp_siso_rdy,
      src_out => to_lane_sosi_arr(I)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Ring info
  -----------------------------------------------------------------------------
  u_ring_info : entity ring_lib.ring_info
  port map (
    mm_rst => mm_rst,
    mm_clk => mm_clk,

    dp_clk => dp_clk,
    dp_rst => dp_rst,

    reg_copi => reg_ring_info_copi,
    reg_cipo => reg_ring_info_cipo,

    ring_info => ring_info
  );

  -- Use full c_byte_w range of ID for gn_index and ring_info.O_rn
  gn_index <= TO_UINT(ID);
  this_rn <= TO_UVEC(gn_index - TO_UINT(ring_info.O_rn), c_byte_w) when rising_edge(dp_clk);  -- Using register to ease timing closure.

  -----------------------------------------------------------------------------
  -- Ring lane even indices.
  -----------------------------------------------------------------------------
  gen_even_lanes: for I in 0 to c_nof_even_lanes - 1 generate
    u_ring_lane : entity ring_lib.ring_lane
    generic map (
      g_lane_direction            => 1,  -- transport in positive direction.
      g_lane_data_w               => c_lane_data_w,
      g_lane_packet_length        => c_lane_packet_length,
      g_use_dp_layer              => c_use_dp_layer,
      g_nof_rx_monitors           => c_nof_rx_monitors,
      g_nof_tx_monitors           => c_nof_tx_monitors,
      g_err_bi                    => c_err_bi,
      g_nof_err_counts            => c_nof_err_counts,
      g_bsn_at_sync_check_channel => c_bsn_at_sync_check_channel,
      g_validate_channel          => c_validate_channel,
      g_validate_channel_mode     => c_validate_channel_mode,
      g_sync_timeout              => c_sync_timeout
    )
    port map (
      mm_rst => mm_rst,
      mm_clk => mm_clk,
      dp_clk => dp_clk,
      dp_rst => dp_rst,

      from_lane_sosi     => from_lane_sosi_arr(2 * I),  -- even indices
      to_lane_sosi       => to_lane_sosi_arr(2 * I),
      lane_rx_cable_sosi => lane_rx_cable_even_sosi_arr(I),
      lane_rx_board_sosi => lane_rx_board_even_sosi_arr(I),
      lane_tx_cable_sosi => lane_tx_cable_even_sosi_arr(I),
      lane_tx_board_sosi => lane_tx_board_even_sosi_arr(I),
      bs_sosi            => bs_sosi,

      reg_ring_lane_info_copi                => reg_ring_lane_info_copi_arr(2 * I),
      reg_ring_lane_info_cipo                => reg_ring_lane_info_cipo_arr(2 * I),
      reg_bsn_monitor_v2_ring_rx_copi        => reg_bsn_monitor_v2_ring_rx_copi_arr(2 * I),
      reg_bsn_monitor_v2_ring_rx_cipo        => reg_bsn_monitor_v2_ring_rx_cipo_arr(2 * I),
      reg_bsn_monitor_v2_ring_tx_copi        => reg_bsn_monitor_v2_ring_tx_copi_arr(2 * I),
      reg_bsn_monitor_v2_ring_tx_cipo        => reg_bsn_monitor_v2_ring_tx_cipo_arr(2 * I),
      reg_dp_block_validate_err_copi         => reg_dp_block_validate_err_copi_arr(2 * I),
      reg_dp_block_validate_err_cipo         => reg_dp_block_validate_err_cipo_arr(2 * I),
      reg_dp_block_validate_bsn_at_sync_copi => reg_dp_block_validate_bsn_at_sync_copi_arr(2 * I),
      reg_dp_block_validate_bsn_at_sync_cipo => reg_dp_block_validate_bsn_at_sync_cipo_arr(2 * I),

      this_rn   => this_rn,
      N_rn      => ring_info.N_rn,
      rx_select => ring_info.use_cable_to_previous_rn,
      tx_select => ring_info.use_cable_to_next_rn
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Ring lane odd indices.
  -----------------------------------------------------------------------------
  gen_odd_lanes : for I in 0 to c_nof_odd_lanes - 1 generate
    u_ring_lane : entity ring_lib.ring_lane
    generic map (
      g_lane_direction            => 0,  -- transport in negative direction.
      g_lane_data_w               => c_lane_data_w,
      g_lane_packet_length        => c_lane_packet_length,
      g_use_dp_layer              => c_use_dp_layer,
      g_nof_rx_monitors           => c_nof_rx_monitors,
      g_nof_tx_monitors           => c_nof_tx_monitors,
      g_err_bi                    => c_err_bi,
      g_nof_err_counts            => c_nof_err_counts,
      g_bsn_at_sync_check_channel => c_bsn_at_sync_check_channel,
      g_validate_channel          => c_validate_channel,
      g_validate_channel_mode     => c_validate_channel_mode,
      g_sync_timeout              => c_sync_timeout
    )
    port map (
      mm_rst => mm_rst,
      mm_clk => mm_clk,
      dp_clk => dp_clk,
      dp_rst => dp_rst,

      from_lane_sosi     => from_lane_sosi_arr(2 * I + 1),  -- odd indices
      to_lane_sosi       => to_lane_sosi_arr(2 * I + 1),
      lane_rx_cable_sosi => lane_rx_cable_odd_sosi_arr(I),
      lane_rx_board_sosi => lane_rx_board_odd_sosi_arr(I),
      lane_tx_cable_sosi => lane_tx_cable_odd_sosi_arr(I),
      lane_tx_board_sosi => lane_tx_board_odd_sosi_arr(I),
      bs_sosi            => bs_sosi,

      reg_ring_lane_info_copi                => reg_ring_lane_info_copi_arr(2 * I + 1),
      reg_ring_lane_info_cipo                => reg_ring_lane_info_cipo_arr(2 * I + 1),
      reg_bsn_monitor_v2_ring_rx_copi        => reg_bsn_monitor_v2_ring_rx_copi_arr(2 * I + 1),
      reg_bsn_monitor_v2_ring_rx_cipo        => reg_bsn_monitor_v2_ring_rx_cipo_arr(2 * I + 1),
      reg_bsn_monitor_v2_ring_tx_copi        => reg_bsn_monitor_v2_ring_tx_copi_arr(2 * I + 1),
      reg_bsn_monitor_v2_ring_tx_cipo        => reg_bsn_monitor_v2_ring_tx_cipo_arr(2 * I + 1),
      reg_dp_block_validate_err_copi         => reg_dp_block_validate_err_copi_arr(2 * I + 1),
      reg_dp_block_validate_err_cipo         => reg_dp_block_validate_err_cipo_arr(2 * I + 1),
      reg_dp_block_validate_bsn_at_sync_copi => reg_dp_block_validate_bsn_at_sync_copi_arr(2 * I + 1),
      reg_dp_block_validate_bsn_at_sync_cipo => reg_dp_block_validate_bsn_at_sync_cipo_arr(2 * I + 1),

      this_rn   => this_rn,
      N_rn      => ring_info.N_rn,
      rx_select => ring_info.use_cable_to_next_rn,  -- reverse tx/rx select for odd indices.
      tx_select => ring_info.use_cable_to_previous_rn
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Combine seperate signals into array for tr_10GbE
  -----------------------------------------------------------------------------
  gen_combine: for I in 0 to c_nof_even_lanes - 1 generate
    -- QSFP_RX
    lane_rx_cable_even_sosi_arr(I) <= tr_10gbe_src_out_arr(c_nof_if * I + c_qsfp_if_offset) when ring_info.use_cable_to_previous_rn = '1' else c_dp_sosi_rst;  -- use_cable_to_previous_rn=1 -> even lanes receive from cable
    lane_rx_cable_odd_sosi_arr(I)  <= tr_10gbe_src_out_arr(c_nof_if * I + c_qsfp_if_offset) when ring_info.use_cable_to_next_rn = '1'     else c_dp_sosi_rst;  -- use_cable_to_next_rn=1 -> odd lanes receive from cable
    -- QSFP_TX
    tr_10gbe_snk_in_arr(c_nof_if * I + c_qsfp_if_offset) <= lane_tx_cable_even_sosi_arr(I) when ring_info.use_cable_to_next_rn = '1'     else  -- use_cable_to_next_rn=1 -> even lanes transmit to cable
                                                            lane_tx_cable_odd_sosi_arr(I)  when ring_info.use_cable_to_previous_rn = '1' else c_dp_sosi_rst;  -- use_cable_to_previous_rn=1 -> odd lanes transmit to cable

    -- RING_0_RX even lanes receive from RING_0 (from the left)
    lane_rx_board_even_sosi_arr(I) <= tr_10gbe_src_out_arr(c_nof_if * I + c_ring_0_if_offset);
    -- RING_0_TX odd lanes transmit to RING_0 (to the left)
    tr_10gbe_snk_in_arr(c_nof_if * I + c_ring_0_if_offset) <= lane_tx_board_odd_sosi_arr(I);

    -- RING_1_RX odd lanes receive from RING_1 (from the right)
    lane_rx_board_odd_sosi_arr(I) <= tr_10gbe_src_out_arr(c_nof_if * I + c_ring_1_if_offset);
    -- RING_1_TX even lanes transmit to RING_1 (to the right)
    tr_10gbe_snk_in_arr(c_nof_if * I + c_ring_1_if_offset) <= lane_tx_board_even_sosi_arr(I);
  end generate;

  -----------------------------------------------------------------------------
  -- tr_10GbE
  -----------------------------------------------------------------------------
  u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
  generic map (
    g_sim           => g_sim,
    g_sim_level     => 1,
    g_nof_macs      => c_nof_mac,
    g_direction     => "TX_RX",
    g_tx_fifo_fill  => c_fifo_tx_fill,
    g_tx_fifo_size  => c_fifo_tx_size
  )
  port map (
    -- Transceiver PLL reference clock
    tr_ref_clk_644        => SA_CLK,
    tr_ref_clk_312        => tr_ref_clk_312,
    tr_ref_clk_156        => tr_ref_clk_156,
    tr_ref_rst_156        => tr_ref_rst_156,

    -- MM interface
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    reg_mac_mosi          => reg_tr_10GbE_mac_copi,
    reg_mac_miso          => reg_tr_10GbE_mac_cipo,

    reg_eth10g_mosi       => reg_tr_10GbE_eth10g_copi,
    reg_eth10g_miso       => reg_tr_10GbE_eth10g_cipo,

    -- DP interface
    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    src_out_arr           => tr_10gbe_src_out_arr,
    src_in_arr            => tr_10gbe_src_in_arr,

    snk_out_arr           => tr_10gbe_snk_out_arr,
    snk_in_arr            => tr_10gbe_snk_in_arr,

    -- Serial IO
    serial_tx_arr         => tr_10gbe_serial_tx_arr,
    serial_rx_arr         => tr_10gbe_serial_rx_arr
  );

  -----------------------------------------------------------------------------
  -- Seperate serial tx/rx array
  -----------------------------------------------------------------------------
  -- Seperating the one large serial tx/rx array from tr_10GbE to the 3 port arrays:
  -- QSFP port, RING_0 port and RING_1 port.
  gen_seperate: for I in 0 to c_nof_even_lanes - 1 generate
    -- QSFP_TX
    unb2_board_front_io_serial_tx_arr(I) <= tr_10gbe_serial_tx_arr(c_nof_if * I + c_qsfp_if_offset);
    -- QSFP_RX
    tr_10gbe_serial_rx_arr(c_nof_if * I + c_qsfp_if_offset) <= unb2_board_front_io_serial_rx_arr(I);

    -- RING_0_TX
    i_RING_TX(0)(I) <= tr_10gbe_serial_tx_arr(c_nof_if * I + c_ring_0_if_offset);
    -- RING_0_RX
    tr_10gbe_serial_rx_arr(c_nof_if * I + c_ring_0_if_offset) <= i_RING_RX(0)(I);

    -- RING_1_TX
    i_RING_TX(1)(I) <= tr_10gbe_serial_tx_arr(c_nof_if * I + c_ring_1_if_offset);
    -- RING_1_RX
    tr_10gbe_serial_rx_arr(c_nof_if * I + c_ring_1_if_offset) <= i_RING_RX(1)(I);
  end generate;

  ---------
  -- PLL
  ---------
  u_tech_pll_xgmii_mac_clocks : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
  port map (
    refclk_644 => SA_CLK,
    rst_in     => mm_rst,
    clk_156    => tr_ref_clk_156,
    clk_312    => tr_ref_clk_312,
    rst_156    => tr_ref_rst_156,
    rst_312    => open
  );

  ------------
  -- Front IO
  ------------
  -- put the QSFP_TX/RX ports into arrays
  i_QSFP_RX(0) <= QSFP_0_RX;
  QSFP_0_TX <= i_QSFP_TX(0);

  u_front_io : entity unb2b_board_lib.unb2b_board_front_io
  generic map (
    g_nof_qsfp_bus => c_unb2b_board_tr_qsfp.nof_bus
  )
  port map (
    serial_tx_arr => unb2_board_front_io_serial_tx_arr,
    serial_rx_arr => unb2_board_front_io_serial_rx_arr,

    green_led_arr => qsfp_green_led_arr,
    red_led_arr   => qsfp_red_led_arr,

    QSFP_RX       => i_QSFP_RX,
    QSFP_TX       => i_QSFP_TX,

    QSFP_LED      => QSFP_LED
  );

  ------------
  -- RING IO
  ------------
  i_RING_RX(0) <= RING_0_RX;
  i_RING_RX(1) <= RING_1_RX;
  RING_0_TX <= i_RING_TX(0);
  RING_1_TX <= i_RING_TX(1);

  ------------
  -- LEDs
  ------------
  unb2_board_qsfp_leds_tx_siso_arr(0) <=  tr_10gbe_snk_out_arr(0);
  u_front_led : entity unb2b_board_lib.unb2b_board_qsfp_leds
  generic map (
    g_sim           => g_sim,
    g_factory_image => g_factory_image,
    g_nof_qsfp      => c_unb2b_board_tr_qsfp.nof_bus,
    g_pulse_us      => 1000 / (10**9 / c_mm_clk_freq)  -- nof clk cycles to get us period
  )
  port map (
    rst             => mm_rst,
    clk             => mm_clk,
    green_led_arr   => qsfp_green_led_arr,
    red_led_arr     => qsfp_red_led_arr,

    tx_siso_arr     => unb2_board_qsfp_leds_tx_siso_arr
  );
end str;
