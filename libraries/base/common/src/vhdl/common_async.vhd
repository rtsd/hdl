-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Clock an asynchronous din into the clk clock domain
-- Description:
--   The delay line combats the potential meta-stability of clocked in data.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity common_async is
  generic (
    g_rising_edge : boolean := true;
    g_rst_level   : std_logic := '0';
    g_delay_len   : positive := c_meta_delay_len  -- use common_pipeline if g_delay_len=0 for wires only is also needed
  );
  port (
    rst  : in  std_logic := '0';
    clk  : in  std_logic;
    din  : in  std_logic;
    dout : out std_logic
  );
end;

architecture rtl of common_async is
  signal din_meta : std_logic_vector(0 to g_delay_len - 1) := (others => g_rst_level);

  -- Synthesis constraint to ensure that register is kept in this instance region
  attribute preserve : boolean;
  attribute preserve of din_meta : signal is true;
begin
  p_clk : process (rst, clk)
  begin
    if g_rising_edge = true then
      -- Default use rising edge
      if rst = '1' then
        din_meta <= (others => g_rst_level);
      elsif rising_edge(clk) then
        din_meta <= din & din_meta(0 to din_meta'high - 1);
      end if;
    else
      -- also support using falling edge
      if rst = '1' then
        din_meta <= (others => g_rst_level);
      elsif falling_edge(clk) then
        din_meta <= din & din_meta(0 to din_meta'high - 1);
      end if;
    end if;
  end process;

  dout <= din_meta(din_meta'high);
end rtl;
