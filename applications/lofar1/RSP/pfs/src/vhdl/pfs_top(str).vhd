library IEEE, pfs_lib;
use IEEE.std_logic_1164.all;

architecture str of pfs_top is
  signal reg_in_dat_x : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal reg_in_dat_y : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal reg_in_val   : std_logic;
  signal reg_in_sync  : std_logic;
  signal d_out_dat_x  : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal d_out_dat_y  : std_logic_vector(g_out_dat_w - 1 downto 0);
  signal d_out_val    : std_logic;
  signal d_out_sync   : std_logic;
begin
  registers : process(clk)
  begin
    if rising_edge(clk) then
      reg_in_dat_x  <= in_dat_x;
      reg_in_dat_y  <= in_dat_y;
      reg_in_val    <= in_val;
      reg_in_sync   <= in_sync;
      out_dat_x     <= d_out_dat_x;
      out_dat_y     <= d_out_dat_y;
      out_val       <= d_out_val;
      out_sync      <= d_out_sync;
    end if;
  end process;

  pfs : entity pfs_lib.pfs
  generic map (
    g_nof_bands  => g_nof_bands,
    g_nof_taps   => g_nof_taps,
    g_in_dat_w   => g_in_dat_w,
    g_out_dat_w  => g_out_dat_w,
    g_coef_dat_w => g_coef_dat_w
  )
  port map (
    in_dat_x     => reg_in_dat_x,
    in_dat_y     => reg_in_dat_y,
    in_val       => reg_in_val,
    in_sync      => reg_in_sync,
    out_dat_x    => d_out_dat_x,
    out_dat_y    => d_out_dat_y,
    out_val      => d_out_val,
    out_sync     => d_out_sync,
    clk          => clk,
    rst          => rst,
    restart      => '0'
  );
end str;
