-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Instantiate FIFO IP with generics
-- Description:
--   Copied component declaration and instance example from generated/fifo_140/sim/ip_arria10_e1sg_fifo_dc_mixed_widths_fifo_140_5csdcfa.vhd

library ieee;
use ieee.std_logic_1164.all;

library technology_lib;
use technology_lib.technology_pkg.all;

library altera_mf;
use altera_mf.all;

entity ip_arria10_e1sg_fifo_dc_mixed_widths is
  generic (
    g_nof_words : natural := 1024;  -- FIFO size in nof wr_dat words
    g_wrdat_w   : natural := 20;
    g_rddat_w   : natural := 10
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
end ip_arria10_e1sg_fifo_dc_mixed_widths;

architecture SYN of ip_arria10_e1sg_fifo_dc_mixed_widths is
  component  dcfifo_mixed_widths
  generic (
    intended_device_family  : string;
    lpm_numwords  : natural;
    lpm_showahead  : string;
    lpm_type  : string;
    lpm_width  : natural;
    lpm_widthu  : natural;
    lpm_widthu_r  : natural;
    lpm_width_r  : natural;
    overflow_checking  : string;
    rdsync_delaypipe  : natural;
    read_aclr_synch  : string;
    underflow_checking  : string;
    use_eab  : string;
    write_aclr_synch  : string;
    wrsync_delaypipe  : natural
  );
  port (
    aclr   : in std_logic;
    data   : in std_logic_vector(data'range);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q   : out std_logic_vector(q'range);
    rdempty   : out std_logic;
    rdusedw   : out std_logic_vector(rdusedw'range);
    wrfull   : out std_logic;
    wrusedw   : out std_logic_vector(wrusedw'range)
  );
  end component;
begin
  dcfifo_mixed_widths_component : dcfifo_mixed_widths
  generic map (
    intended_device_family  => "Arria 10",
    lpm_numwords  => g_nof_words,
    lpm_showahead  => "OFF",
    lpm_type  => "dcfifo_mixed_widths",
    lpm_width  => g_wrdat_w,
    lpm_widthu  => tech_ceil_log2(g_nof_words),
    lpm_widthu_r  => tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w),
    lpm_width_r  => g_rddat_w,
    overflow_checking  => "ON",
    rdsync_delaypipe  => 5,
    read_aclr_synch  => "OFF",
    underflow_checking  => "ON",
    use_eab  => "ON",
    write_aclr_synch  => "ON",
    wrsync_delaypipe  => 5
  )
  port map (
    aclr => aclr,
    data => data,
    rdclk => rdclk,
    rdreq => rdreq,
    wrclk => wrclk,
    wrreq => wrreq,
    q => q,
    rdempty => rdempty,
    rdusedw => rdusedw,
    wrfull => wrfull,
    wrusedw => wrusedw
  );
end SYN;
