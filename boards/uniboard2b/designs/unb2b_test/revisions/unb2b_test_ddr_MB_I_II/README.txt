

Simulation
----------
-> Read ../../doc/README first until step 3
Modelsim instructions:

    # in Modelsim do:
    lp unb2a_test_ddr_MB_I_II
    mk all
    # now double click on testbench file
    as 10
    run 500us


    # while the simulation runs... in another terminal/bash session do:
    cd unb2a_test/tb/python

    # To read out the design_name; do:
    python tc_unb2_test.py --sim --unb 0 --fn 3 --seq INFO 

    # To test the ddr4 modules; do:
    python tc_unb2_test_ddr.py --sim --unb 0 --fn 3 -v 5 -s I,II --rep 1 -n 1000

    # to end simulation in Modelsim do:
    quit -sim



Testing on hardware
-------------------
-> Read ../../doc/README first until step 5

# (assume that the Uniboard is --unb 1   -> check the dipswitches or backpanel-slotnumber)

# To read out the design_name; do:
python tc_unb2_test.py --unb 1 --fn 0:3 --seq REGMAP,INFO

# To test the ddr4 modules:
python tc_unb2_test_ddr.py --unb 1 --fn 0:3 -v 5 -s I,II --rep 1 -n 10000000
# --rep N  (N is number of runs. If N=-1 run continuously and break with ctrl-c key)

