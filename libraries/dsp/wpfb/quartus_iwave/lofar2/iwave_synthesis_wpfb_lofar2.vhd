-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author:
-- . D.F. Brouwer
-- Design name:
-- . iwave_synthesis_wpfb_lofar2
-- Purpose:
-- . Wrapper for wpfb (wideband polyphase filterbank with ports for subband
--   statistics and streaming interfaces) synthesis design for iwave Agilex 7
--   (c_tech_agi027_1e1v).
-- . Implements the functionality of the subband filterbank (Fsub) using the
--   LOFAR2 design parameters.
-- Description:
-- . The subband filterbank seperates the incoming timestamped ADC samples into
--   512 frequency bands called subbands.
-- . It implements a critically sampled poly-phase filterbank (PFB). The PFB
--   consists of a poly-phase finite impulse response (PFIR) filter per real
--   input and a complex fast fourier transform (FFT) per 2 real inputs.
-- . The number of points of the FFT is 1024.
-- Description:
-- . The quantized subbands are output (and used for the SST) because it uses more logic.
--    ____
--   |    |
--   |    |--raw----> X (not included in synthesis)
--   |WPFB|
--   |    |--quant--> wpfb_unit_out_quant_sosi_arr (more logic = critical timepath is longer)
--   |____|

library IEEE, common_lib, dp_lib, rTwoSDF_lib, common_mult_lib, fft_lib, wpfb_lib, filter_lib, si_lib, st_lib, mm_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use rTwoSDF_lib.rTwoSDFPkg.all;
use filter_lib.fil_pkg.all;
use fft_lib.fft_pkg.all;
use wpfb_lib.wpfb_pkg.all;

entity iwave_synthesis_wpfb_lofar2 is
  generic (
    g_wpfb                       : t_wpfb  := c_wpfb_lofar2_subbands_l2ts_18b; -- Instance of the wpfb type (t_wpfb) which is the same config as defined in c_sdp_wpfb_subbands
    g_nof_streams                : natural := 12   -- Number of signals per processing node
  );
  port (
    dp_clk                       : in  std_logic;  -- Same clock, reset for dp as for mm, because of compatibility of Agilex 7.
    dp_rst                       : in  std_logic;  -- ,, ,,
    mm_clk                       : in  std_logic;  -- ,, ,,
    mm_rst                       : in  std_logic;  -- ,, ,,
    reg_si_copi                  : in  t_mem_copi := c_mem_copi_rst;
    reg_si_cipo                  : out t_mem_cipo;
    ram_fil_coefs_copi           : in  t_mem_copi := c_mem_copi_rst;
    ram_fil_coefs_cipo           : out t_mem_cipo;
    ram_st_sst_copi              : in  t_mem_copi := c_mem_copi_rst;  -- Subband statistics registers
    ram_st_sst_cipo              : out t_mem_cipo;
    in_sosi_arr                  : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    out_quant_sosi_arr           : out t_dp_sosi_arr(g_wpfb.nof_wb_streams * g_wpfb.wb_factor - 1 downto 0)
  );
end iwave_synthesis_wpfb_lofar2;

architecture str of iwave_synthesis_wpfb_lofar2 is
  -- FIR coeffs file is copied to build data/ dir by design revision hdllib.cfg
  -- from: $HDL_WORK/libraries/dsp/filter/src/hex
  constant c_coefs_file_prefix        : string := "data/Coeffs16384Kaiser-quant_1wb";

  constant c_nof_pfb                  : natural := g_wpfb.nof_wb_streams * g_wpfb.wb_factor; -- Default 6 PFB units, is 6 signal output pairs
  constant c_in_dat_w                 : natural := g_wpfb.fil_in_dat_w; -- Default number of input bits of the ADC is 14

  constant c_si_pipeline              : natural := 1;

  signal si_sosi_arr                  : t_dp_sosi_arr(g_nof_streams - 1 downto 0)  := (others => c_dp_sosi_rst);

  signal wpfb_unit_in_sosi_arr        : t_dp_sosi_arr(c_nof_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal wpfb_unit_out_quant_sosi_arr : t_dp_sosi_arr(c_nof_pfb - 1 downto 0) := (others => c_dp_sosi_rst);

begin
  ---------------------------------------------------------------
  -- SPECTRAL INVERSION
  ---------------------------------------------------------------
  u_si_arr : entity si_lib.si_arr
  generic map (
    g_nof_streams => g_nof_streams,
    g_pipeline    => c_si_pipeline,
    g_dat_w       => c_in_dat_w
  )
  port map(
    in_sosi_arr  => in_sosi_arr,
    out_sosi_arr => si_sosi_arr,

    reg_si_mosi  => reg_si_copi,
    reg_si_miso  => reg_si_cipo,

    mm_rst       => mm_rst,
    mm_clk       => mm_clk,
    dp_clk       => dp_clk,
    dp_rst       => dp_rst
  );

  ---------------------------------------------------------------
  -- POLY-PHASE FILTERBANK
  ---------------------------------------------------------------
  -- Connect the 12 ADC streams to the re and im fields of the PFB input.
  p_pfb_streams : process(si_sosi_arr)
  begin
    for I in 0 to c_nof_pfb - 1 loop
      wpfb_unit_in_sosi_arr(I) <= si_sosi_arr(2 * I);
      wpfb_unit_in_sosi_arr(I).re <= RESIZE_DP_DSP_DATA(si_sosi_arr(2 * I).data);
      wpfb_unit_in_sosi_arr(I).im <= RESIZE_DP_DSP_DATA(si_sosi_arr(2 * I + 1).data);
    end loop;
  end process;

  -- PFB
  u_wpfb_unit_dev : entity wpfb_lib.wpfb_unit_dev
  generic map (
    g_wpfb                => g_wpfb,
    g_use_prefilter       => true,
    g_stats_ena           => false,               -- Enables the statistics unit. For wpfb_unit_dev default true
    g_use_bg              => false,
    g_coefs_file_prefix   => c_coefs_file_prefix, -- File prefix for the coefficients files. For wpfb_unit_dev default "data/coefs_wide"
    g_restart_on_valid    => false                -- For wpfb_unit_dev default true
  )
  port map (
    dp_rst                => dp_rst,
    dp_clk                => dp_clk,
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    ram_fil_coefs_mosi    => ram_fil_coefs_copi,
    ram_fil_coefs_miso    => ram_fil_coefs_cipo,
    ram_st_sst_mosi       => ram_st_sst_copi,     -- Subband statistics registers
    ram_st_sst_miso       => ram_st_sst_cipo,
    in_sosi_arr           => wpfb_unit_in_sosi_arr,
    out_quant_sosi_arr    => wpfb_unit_out_quant_sosi_arr
  );

  out_quant_sosi_arr <= wpfb_unit_out_quant_sosi_arr;
end str;
