--IP Functional Simulation Model
--VERSION_BEGIN 11.1SP2 cbx_mgl 2012:01:25:21:26:09:SJ cbx_simgen 2012:01:25:21:25:27:SJ  VERSION_END


-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

 LIBRARY sgate;
 USE sgate.sgate_pack.all;

--synthesis_resources = lut 75 mux21 83 oper_decoder 2 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  multiplexer_0001 IS 
	 PORT 
	 ( 
		 clk	:	IN  STD_LOGIC;
		 in0_data	:	IN  STD_LOGIC_VECTOR (63 DOWNTO 0);
		 in0_empty	:	IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 in0_endofpacket	:	IN  STD_LOGIC;
		 in0_error	:	IN  STD_LOGIC_VECTOR (1 DOWNTO 0);
		 in0_ready	:	OUT  STD_LOGIC;
		 in0_startofpacket	:	IN  STD_LOGIC;
		 in0_valid	:	IN  STD_LOGIC;
		 in1_data	:	IN  STD_LOGIC_VECTOR (63 DOWNTO 0);
		 in1_empty	:	IN  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 in1_endofpacket	:	IN  STD_LOGIC;
		 in1_error	:	IN  STD_LOGIC_VECTOR (1 DOWNTO 0);
		 in1_ready	:	OUT  STD_LOGIC;
		 in1_startofpacket	:	IN  STD_LOGIC;
		 in1_valid	:	IN  STD_LOGIC;
		 out_channel	:	OUT  STD_LOGIC;
		 out_data	:	OUT  STD_LOGIC_VECTOR (63 DOWNTO 0);
		 out_empty	:	OUT  STD_LOGIC_VECTOR (2 DOWNTO 0);
		 out_endofpacket	:	OUT  STD_LOGIC;
		 out_error	:	OUT  STD_LOGIC_VECTOR (1 DOWNTO 0);
		 out_ready	:	IN  STD_LOGIC;
		 out_startofpacket	:	OUT  STD_LOGIC;
		 out_valid	:	OUT  STD_LOGIC;
		 reset_n	:	IN  STD_LOGIC
	 ); 
 END multiplexer_0001;

 ARCHITECTURE RTL OF multiplexer_0001 IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_0_545q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_10_681q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_11_680q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_12_679q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_13_678q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_14_677q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_15_676q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_16_675q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_17_674q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_18_673q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_19_672q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_1_690q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_20_671q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_21_670q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_22_669q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_23_668q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_24_667q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_25_666q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_26_665q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_27_664q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_28_663q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_29_662q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_2_689q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_30_661q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_31_660q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_32_659q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_33_658q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_34_657q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_35_656q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_36_655q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_37_654q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_38_653q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_39_652q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_3_688q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_40_651q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_41_650q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_42_649q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_43_648q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_44_647q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_45_646q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_46_645q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_47_644q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_48_643q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_49_642q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_4_687q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_50_641q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_51_640q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_52_639q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_53_638q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_54_637q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_55_636q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_56_635q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_57_634q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_58_633q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_59_632q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_5_686q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_60_631q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_61_630q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_62_629q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_63_628q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_64_627q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_65_626q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_66_625q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_67_624q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_68_623q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_69_622q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_6_685q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_70_621q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_71_620q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_7_684q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_8_683q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_9_682q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_619q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_packet_in_progress_3q	:	STD_LOGIC := '0';
	 SIGNAL	multiplexer_0001_select_14q	:	STD_LOGIC := '0';
	 SIGNAL  wire_nO_w224w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_nO_w_lg_multiplexer_0001_packet_in_progress_3q2w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	wire_multiplexer_0001_decision_1m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_decision_2m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_in0_ready_93m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_in1_ready_91m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_470m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_471m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_packet_in_progress_13m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_packet_in_progress_9m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_select_12m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_select_8m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_endofpacket_88m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_0_86m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_10_76m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_11_75m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_12_74m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_13_73m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_14_72m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_15_71m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_16_70m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_17_69m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_18_68m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_19_67m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_1_85m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_20_66m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_21_65m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_22_64m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_23_63m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_24_62m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_25_61m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_26_60m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_27_59m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_28_58m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_29_57m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_2_84m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_30_56m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_31_55m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_32_54m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_33_53m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_34_52m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_35_51m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_36_50m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_37_49m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_38_48m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_39_47m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_3_83m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_40_46m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_41_45m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_42_44m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_43_43m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_44_42m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_45_41m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_46_40m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_47_39m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_48_38m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_49_37m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_4_82m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_50_36m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_51_35m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_52_34m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_53_33m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_54_32m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_55_31m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_56_30m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_57_29m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_58_28m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_59_27m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_5_81m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_60_26m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_61_25m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_62_24m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_63_23m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_64_22m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_65_21m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_66_20m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_67_19m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_68_18m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_69_17m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_6_80m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_70_16m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_7_79m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_8_78m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_payload_9_77m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_multiplexer_0001_selected_valid_87m_dataout	:	STD_LOGIC;
	 SIGNAL  wire_multiplexer_0001_selected_valid_87m_w_lg_dataout3w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_multiplexer_0001_decoder0_15_i	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_multiplexer_0001_decoder0_15_o	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
	 SIGNAL  wire_multiplexer_0001_decoder1_92_i	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_multiplexer_0001_decoder1_92_o	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
	 SIGNAL  wire_w_lg_in0_valid151w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_in1_valid148w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  s_wire_multiplexer_0001_always2_11_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_multiplexer_0001_always2_7_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_always1_472_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_in_ready_468_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_vcc :	STD_LOGIC;
 BEGIN

	wire_w_lg_in0_valid151w(0) <= NOT in0_valid;
	wire_w_lg_in1_valid148w(0) <= NOT in1_valid;
	in0_ready <= wire_multiplexer_0001_in0_ready_93m_dataout;
	in1_ready <= wire_multiplexer_0001_in1_ready_91m_dataout;
	out_channel <= multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_71_620q;
	out_data <= ( multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_70_621q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_69_622q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_68_623q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_67_624q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_66_625q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_65_626q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_64_627q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_63_628q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_62_629q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_61_630q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_60_631q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_59_632q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_58_633q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_57_634q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_56_635q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_55_636q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_54_637q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_53_638q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_52_639q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_51_640q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_50_641q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_49_642q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_48_643q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_47_644q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_46_645q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_45_646q
 & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_44_647q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_43_648q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_42_649q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_41_650q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_40_651q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_39_652q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_38_653q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_37_654q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_36_655q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_35_656q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_34_657q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_33_658q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_32_659q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_31_660q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_30_661q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_29_662q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_28_663q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_27_664q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_26_665q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_25_666q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_24_667q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_23_668q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_22_669q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_21_670q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_20_671q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_19_672q
 & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_18_673q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_17_674q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_16_675q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_15_676q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_14_677q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_13_678q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_12_679q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_11_680q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_10_681q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_9_682q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_8_683q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_7_684q);
	out_empty <= ( multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_6_685q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_5_686q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_4_687q);
	out_endofpacket <= multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_3_688q;
	out_error <= ( multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_2_689q & multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_1_690q);
	out_startofpacket <= multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_0_545q;
	out_valid <= multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_619q;
	s_wire_multiplexer_0001_always2_11_dataout <= ((wire_multiplexer_0001_selected_valid_87m_dataout AND wire_multiplexer_0001_selected_endofpacket_88m_dataout) AND s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_in_ready_468_dataout);
	s_wire_multiplexer_0001_always2_7_dataout <= (wire_nO_w_lg_multiplexer_0001_packet_in_progress_3q2w(0) AND wire_multiplexer_0001_selected_valid_87m_w_lg_dataout3w(0));
	s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_always1_472_dataout <= (wire_multiplexer_0001_selected_valid_87m_dataout AND s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_in_ready_468_dataout);
	s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_in_ready_468_dataout <= (out_ready OR wire_nO_w224w(0));
	s_wire_vcc <= '1';
	PROCESS (clk, reset_n)
	BEGIN
		IF (reset_n = '0') THEN
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_0_545q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_10_681q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_11_680q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_12_679q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_13_678q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_14_677q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_15_676q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_16_675q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_17_674q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_18_673q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_19_672q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_1_690q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_20_671q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_21_670q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_22_669q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_23_668q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_24_667q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_25_666q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_26_665q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_27_664q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_28_663q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_29_662q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_2_689q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_30_661q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_31_660q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_32_659q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_33_658q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_34_657q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_35_656q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_36_655q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_37_654q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_38_653q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_39_652q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_3_688q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_40_651q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_41_650q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_42_649q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_43_648q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_44_647q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_45_646q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_46_645q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_47_644q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_48_643q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_49_642q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_4_687q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_50_641q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_51_640q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_52_639q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_53_638q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_54_637q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_55_636q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_56_635q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_57_634q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_58_633q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_59_632q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_5_686q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_60_631q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_61_630q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_62_629q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_63_628q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_64_627q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_65_626q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_66_625q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_67_624q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_68_623q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_69_622q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_6_685q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_70_621q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_71_620q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_7_684q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_8_683q <= '0';
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_9_682q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
			IF (s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_always1_472_dataout = '1') THEN
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_0_545q <= wire_multiplexer_0001_selected_payload_0_86m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_10_681q <= wire_multiplexer_0001_selected_payload_10_76m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_11_680q <= wire_multiplexer_0001_selected_payload_11_75m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_12_679q <= wire_multiplexer_0001_selected_payload_12_74m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_13_678q <= wire_multiplexer_0001_selected_payload_13_73m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_14_677q <= wire_multiplexer_0001_selected_payload_14_72m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_15_676q <= wire_multiplexer_0001_selected_payload_15_71m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_16_675q <= wire_multiplexer_0001_selected_payload_16_70m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_17_674q <= wire_multiplexer_0001_selected_payload_17_69m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_18_673q <= wire_multiplexer_0001_selected_payload_18_68m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_19_672q <= wire_multiplexer_0001_selected_payload_19_67m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_1_690q <= wire_multiplexer_0001_selected_payload_1_85m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_20_671q <= wire_multiplexer_0001_selected_payload_20_66m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_21_670q <= wire_multiplexer_0001_selected_payload_21_65m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_22_669q <= wire_multiplexer_0001_selected_payload_22_64m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_23_668q <= wire_multiplexer_0001_selected_payload_23_63m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_24_667q <= wire_multiplexer_0001_selected_payload_24_62m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_25_666q <= wire_multiplexer_0001_selected_payload_25_61m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_26_665q <= wire_multiplexer_0001_selected_payload_26_60m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_27_664q <= wire_multiplexer_0001_selected_payload_27_59m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_28_663q <= wire_multiplexer_0001_selected_payload_28_58m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_29_662q <= wire_multiplexer_0001_selected_payload_29_57m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_2_689q <= wire_multiplexer_0001_selected_payload_2_84m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_30_661q <= wire_multiplexer_0001_selected_payload_30_56m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_31_660q <= wire_multiplexer_0001_selected_payload_31_55m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_32_659q <= wire_multiplexer_0001_selected_payload_32_54m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_33_658q <= wire_multiplexer_0001_selected_payload_33_53m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_34_657q <= wire_multiplexer_0001_selected_payload_34_52m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_35_656q <= wire_multiplexer_0001_selected_payload_35_51m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_36_655q <= wire_multiplexer_0001_selected_payload_36_50m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_37_654q <= wire_multiplexer_0001_selected_payload_37_49m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_38_653q <= wire_multiplexer_0001_selected_payload_38_48m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_39_652q <= wire_multiplexer_0001_selected_payload_39_47m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_3_688q <= wire_multiplexer_0001_selected_payload_3_83m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_40_651q <= wire_multiplexer_0001_selected_payload_40_46m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_41_650q <= wire_multiplexer_0001_selected_payload_41_45m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_42_649q <= wire_multiplexer_0001_selected_payload_42_44m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_43_648q <= wire_multiplexer_0001_selected_payload_43_43m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_44_647q <= wire_multiplexer_0001_selected_payload_44_42m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_45_646q <= wire_multiplexer_0001_selected_payload_45_41m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_46_645q <= wire_multiplexer_0001_selected_payload_46_40m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_47_644q <= wire_multiplexer_0001_selected_payload_47_39m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_48_643q <= wire_multiplexer_0001_selected_payload_48_38m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_49_642q <= wire_multiplexer_0001_selected_payload_49_37m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_4_687q <= wire_multiplexer_0001_selected_payload_4_82m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_50_641q <= wire_multiplexer_0001_selected_payload_50_36m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_51_640q <= wire_multiplexer_0001_selected_payload_51_35m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_52_639q <= wire_multiplexer_0001_selected_payload_52_34m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_53_638q <= wire_multiplexer_0001_selected_payload_53_33m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_54_637q <= wire_multiplexer_0001_selected_payload_54_32m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_55_636q <= wire_multiplexer_0001_selected_payload_55_31m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_56_635q <= wire_multiplexer_0001_selected_payload_56_30m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_57_634q <= wire_multiplexer_0001_selected_payload_57_29m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_58_633q <= wire_multiplexer_0001_selected_payload_58_28m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_59_632q <= wire_multiplexer_0001_selected_payload_59_27m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_5_686q <= wire_multiplexer_0001_selected_payload_5_81m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_60_631q <= wire_multiplexer_0001_selected_payload_60_26m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_61_630q <= wire_multiplexer_0001_selected_payload_61_25m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_62_629q <= wire_multiplexer_0001_selected_payload_62_24m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_63_628q <= wire_multiplexer_0001_selected_payload_63_23m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_64_627q <= wire_multiplexer_0001_selected_payload_64_22m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_65_626q <= wire_multiplexer_0001_selected_payload_65_21m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_66_625q <= wire_multiplexer_0001_selected_payload_66_20m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_67_624q <= wire_multiplexer_0001_selected_payload_67_19m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_68_623q <= wire_multiplexer_0001_selected_payload_68_18m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_69_622q <= wire_multiplexer_0001_selected_payload_69_17m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_6_685q <= wire_multiplexer_0001_selected_payload_6_80m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_70_621q <= wire_multiplexer_0001_selected_payload_70_16m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_71_620q <= multiplexer_0001_select_14q;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_7_684q <= wire_multiplexer_0001_selected_payload_7_79m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_8_683q <= wire_multiplexer_0001_selected_payload_8_78m_dataout;
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_payload_9_682q <= wire_multiplexer_0001_selected_payload_9_77m_dataout;
			END IF;
		END IF;
	END PROCESS;
	PROCESS (clk, reset_n)
	BEGIN
		IF (reset_n = '0') THEN
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_619q <= '0';
				multiplexer_0001_packet_in_progress_3q <= '0';
				multiplexer_0001_select_14q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
				multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_619q <= wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_471m_dataout;
				multiplexer_0001_packet_in_progress_3q <= wire_multiplexer_0001_packet_in_progress_13m_dataout;
				multiplexer_0001_select_14q <= wire_multiplexer_0001_select_12m_dataout;
		END IF;
	END PROCESS;
	wire_nO_w224w(0) <= NOT multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_619q;
	wire_nO_w_lg_multiplexer_0001_packet_in_progress_3q2w(0) <= NOT multiplexer_0001_packet_in_progress_3q;
	wire_multiplexer_0001_decision_1m_dataout <= in1_valid AND NOT(in0_valid);
	wire_multiplexer_0001_decision_2m_dataout <= wire_multiplexer_0001_decision_1m_dataout WHEN wire_multiplexer_0001_decoder1_92_o(1) = '1'  ELSE in1_valid;
	wire_multiplexer_0001_in0_ready_93m_dataout <= wire_w_lg_in0_valid151w(0) WHEN wire_multiplexer_0001_decoder1_92_o(1) = '1'  ELSE s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_in_ready_468_dataout;
	wire_multiplexer_0001_in1_ready_91m_dataout <= s_wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_in_ready_468_dataout WHEN wire_multiplexer_0001_decoder1_92_o(1) = '1'  ELSE wire_w_lg_in1_valid148w(0);
	wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_470m_dataout <= multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_619q AND NOT(out_ready);
	wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_471m_dataout <= wire_multiplexer_0001_multiplexer_0001_1stage_pipeline_outpipe_out_valid_470m_dataout OR wire_multiplexer_0001_selected_valid_87m_dataout;
	wire_multiplexer_0001_packet_in_progress_13m_dataout <= wire_multiplexer_0001_packet_in_progress_9m_dataout AND NOT(s_wire_multiplexer_0001_always2_11_dataout);
	wire_multiplexer_0001_packet_in_progress_9m_dataout <= multiplexer_0001_packet_in_progress_3q OR NOT(s_wire_multiplexer_0001_always2_7_dataout);
	wire_multiplexer_0001_select_12m_dataout <= wire_multiplexer_0001_decision_2m_dataout WHEN s_wire_multiplexer_0001_always2_11_dataout = '1'  ELSE wire_multiplexer_0001_select_8m_dataout;
	wire_multiplexer_0001_select_8m_dataout <= wire_multiplexer_0001_decision_2m_dataout WHEN s_wire_multiplexer_0001_always2_7_dataout = '1'  ELSE multiplexer_0001_select_14q;
	wire_multiplexer_0001_selected_endofpacket_88m_dataout <= in1_endofpacket WHEN wire_multiplexer_0001_decoder1_92_o(1) = '1'  ELSE in0_endofpacket;
	wire_multiplexer_0001_selected_payload_0_86m_dataout <= in1_startofpacket WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_startofpacket;
	wire_multiplexer_0001_selected_payload_10_76m_dataout <= in1_data(3) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(3);
	wire_multiplexer_0001_selected_payload_11_75m_dataout <= in1_data(4) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(4);
	wire_multiplexer_0001_selected_payload_12_74m_dataout <= in1_data(5) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(5);
	wire_multiplexer_0001_selected_payload_13_73m_dataout <= in1_data(6) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(6);
	wire_multiplexer_0001_selected_payload_14_72m_dataout <= in1_data(7) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(7);
	wire_multiplexer_0001_selected_payload_15_71m_dataout <= in1_data(8) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(8);
	wire_multiplexer_0001_selected_payload_16_70m_dataout <= in1_data(9) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(9);
	wire_multiplexer_0001_selected_payload_17_69m_dataout <= in1_data(10) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(10);
	wire_multiplexer_0001_selected_payload_18_68m_dataout <= in1_data(11) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(11);
	wire_multiplexer_0001_selected_payload_19_67m_dataout <= in1_data(12) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(12);
	wire_multiplexer_0001_selected_payload_1_85m_dataout <= in1_error(0) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_error(0);
	wire_multiplexer_0001_selected_payload_20_66m_dataout <= in1_data(13) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(13);
	wire_multiplexer_0001_selected_payload_21_65m_dataout <= in1_data(14) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(14);
	wire_multiplexer_0001_selected_payload_22_64m_dataout <= in1_data(15) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(15);
	wire_multiplexer_0001_selected_payload_23_63m_dataout <= in1_data(16) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(16);
	wire_multiplexer_0001_selected_payload_24_62m_dataout <= in1_data(17) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(17);
	wire_multiplexer_0001_selected_payload_25_61m_dataout <= in1_data(18) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(18);
	wire_multiplexer_0001_selected_payload_26_60m_dataout <= in1_data(19) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(19);
	wire_multiplexer_0001_selected_payload_27_59m_dataout <= in1_data(20) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(20);
	wire_multiplexer_0001_selected_payload_28_58m_dataout <= in1_data(21) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(21);
	wire_multiplexer_0001_selected_payload_29_57m_dataout <= in1_data(22) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(22);
	wire_multiplexer_0001_selected_payload_2_84m_dataout <= in1_error(1) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_error(1);
	wire_multiplexer_0001_selected_payload_30_56m_dataout <= in1_data(23) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(23);
	wire_multiplexer_0001_selected_payload_31_55m_dataout <= in1_data(24) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(24);
	wire_multiplexer_0001_selected_payload_32_54m_dataout <= in1_data(25) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(25);
	wire_multiplexer_0001_selected_payload_33_53m_dataout <= in1_data(26) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(26);
	wire_multiplexer_0001_selected_payload_34_52m_dataout <= in1_data(27) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(27);
	wire_multiplexer_0001_selected_payload_35_51m_dataout <= in1_data(28) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(28);
	wire_multiplexer_0001_selected_payload_36_50m_dataout <= in1_data(29) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(29);
	wire_multiplexer_0001_selected_payload_37_49m_dataout <= in1_data(30) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(30);
	wire_multiplexer_0001_selected_payload_38_48m_dataout <= in1_data(31) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(31);
	wire_multiplexer_0001_selected_payload_39_47m_dataout <= in1_data(32) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(32);
	wire_multiplexer_0001_selected_payload_3_83m_dataout <= in1_endofpacket WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_endofpacket;
	wire_multiplexer_0001_selected_payload_40_46m_dataout <= in1_data(33) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(33);
	wire_multiplexer_0001_selected_payload_41_45m_dataout <= in1_data(34) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(34);
	wire_multiplexer_0001_selected_payload_42_44m_dataout <= in1_data(35) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(35);
	wire_multiplexer_0001_selected_payload_43_43m_dataout <= in1_data(36) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(36);
	wire_multiplexer_0001_selected_payload_44_42m_dataout <= in1_data(37) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(37);
	wire_multiplexer_0001_selected_payload_45_41m_dataout <= in1_data(38) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(38);
	wire_multiplexer_0001_selected_payload_46_40m_dataout <= in1_data(39) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(39);
	wire_multiplexer_0001_selected_payload_47_39m_dataout <= in1_data(40) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(40);
	wire_multiplexer_0001_selected_payload_48_38m_dataout <= in1_data(41) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(41);
	wire_multiplexer_0001_selected_payload_49_37m_dataout <= in1_data(42) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(42);
	wire_multiplexer_0001_selected_payload_4_82m_dataout <= in1_empty(0) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_empty(0);
	wire_multiplexer_0001_selected_payload_50_36m_dataout <= in1_data(43) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(43);
	wire_multiplexer_0001_selected_payload_51_35m_dataout <= in1_data(44) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(44);
	wire_multiplexer_0001_selected_payload_52_34m_dataout <= in1_data(45) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(45);
	wire_multiplexer_0001_selected_payload_53_33m_dataout <= in1_data(46) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(46);
	wire_multiplexer_0001_selected_payload_54_32m_dataout <= in1_data(47) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(47);
	wire_multiplexer_0001_selected_payload_55_31m_dataout <= in1_data(48) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(48);
	wire_multiplexer_0001_selected_payload_56_30m_dataout <= in1_data(49) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(49);
	wire_multiplexer_0001_selected_payload_57_29m_dataout <= in1_data(50) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(50);
	wire_multiplexer_0001_selected_payload_58_28m_dataout <= in1_data(51) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(51);
	wire_multiplexer_0001_selected_payload_59_27m_dataout <= in1_data(52) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(52);
	wire_multiplexer_0001_selected_payload_5_81m_dataout <= in1_empty(1) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_empty(1);
	wire_multiplexer_0001_selected_payload_60_26m_dataout <= in1_data(53) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(53);
	wire_multiplexer_0001_selected_payload_61_25m_dataout <= in1_data(54) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(54);
	wire_multiplexer_0001_selected_payload_62_24m_dataout <= in1_data(55) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(55);
	wire_multiplexer_0001_selected_payload_63_23m_dataout <= in1_data(56) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(56);
	wire_multiplexer_0001_selected_payload_64_22m_dataout <= in1_data(57) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(57);
	wire_multiplexer_0001_selected_payload_65_21m_dataout <= in1_data(58) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(58);
	wire_multiplexer_0001_selected_payload_66_20m_dataout <= in1_data(59) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(59);
	wire_multiplexer_0001_selected_payload_67_19m_dataout <= in1_data(60) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(60);
	wire_multiplexer_0001_selected_payload_68_18m_dataout <= in1_data(61) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(61);
	wire_multiplexer_0001_selected_payload_69_17m_dataout <= in1_data(62) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(62);
	wire_multiplexer_0001_selected_payload_6_80m_dataout <= in1_empty(2) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_empty(2);
	wire_multiplexer_0001_selected_payload_70_16m_dataout <= in1_data(63) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(63);
	wire_multiplexer_0001_selected_payload_7_79m_dataout <= in1_data(0) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(0);
	wire_multiplexer_0001_selected_payload_8_78m_dataout <= in1_data(1) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(1);
	wire_multiplexer_0001_selected_payload_9_77m_dataout <= in1_data(2) WHEN wire_multiplexer_0001_decoder0_15_o(1) = '1'  ELSE in0_data(2);
	wire_multiplexer_0001_selected_valid_87m_dataout <= in1_valid WHEN wire_multiplexer_0001_decoder1_92_o(1) = '1'  ELSE in0_valid;
	wire_multiplexer_0001_selected_valid_87m_w_lg_dataout3w(0) <= NOT wire_multiplexer_0001_selected_valid_87m_dataout;
	wire_multiplexer_0001_decoder0_15_i(0) <= ( multiplexer_0001_select_14q);
	multiplexer_0001_decoder0_15 :  oper_decoder
	  GENERIC MAP (
		width_i => 1,
		width_o => 2
	  )
	  PORT MAP ( 
		i => wire_multiplexer_0001_decoder0_15_i,
		o => wire_multiplexer_0001_decoder0_15_o
	  );
	wire_multiplexer_0001_decoder1_92_i(0) <= ( multiplexer_0001_select_14q);
	multiplexer_0001_decoder1_92 :  oper_decoder
	  GENERIC MAP (
		width_i => 1,
		width_o => 2
	  )
	  PORT MAP ( 
		i => wire_multiplexer_0001_decoder1_92_i,
		o => wire_multiplexer_0001_decoder1_92_o
	  );

 END RTL; --multiplexer_0001
--synopsys translate_on
--VALID FILE
