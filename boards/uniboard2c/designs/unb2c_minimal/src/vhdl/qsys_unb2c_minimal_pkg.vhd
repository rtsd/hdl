-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_unb2c_minimal_pkg is
    ----------------------------------------------------------------------
    -- this component declaration is copy-pasted from Quartus QSYS builder
    ----------------------------------------------------------------------

    component qsys_unb2c_minimal is
        port (
            avs_eth_0_reset_export                 : out std_logic;  -- export
            avs_eth_0_clk_export                   : out std_logic;  -- export
            avs_eth_0_tse_address_export           : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_tse_write_export             : out std_logic;  -- export
            avs_eth_0_tse_read_export              : out std_logic;  -- export
            avs_eth_0_tse_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_tse_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_tse_waitrequest_export       : in  std_logic                     := 'X';  -- export
            avs_eth_0_reg_address_export           : out std_logic_vector(3 downto 0);  -- export
            avs_eth_0_reg_write_export             : out std_logic;  -- export
            avs_eth_0_reg_read_export              : out std_logic;  -- export
            avs_eth_0_reg_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reg_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_ram_address_export           : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_ram_write_export             : out std_logic;  -- export
            avs_eth_0_ram_read_export              : out std_logic;  -- export
            avs_eth_0_ram_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_ram_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_irq_export                   : in  std_logic                     := 'X';  -- export
            clk_clk                                : in  std_logic                     := 'X';  -- clk
            reset_reset_n                          : in  std_logic                     := 'X';  -- reset_n
            pio_pps_reset_export                   : out std_logic;  -- export
            pio_pps_clk_export                     : out std_logic;  -- export
            pio_pps_address_export                 : out std_logic_vector(1 downto 0);  -- export
            pio_pps_write_export                   : out std_logic;  -- export
            pio_pps_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            pio_pps_read_export                    : out std_logic;  -- export
            pio_pps_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_system_info_reset_export           : out std_logic;  -- export
            pio_system_info_clk_export             : out std_logic;  -- export
            pio_system_info_address_export         : out std_logic_vector(4 downto 0);  -- export
            pio_system_info_write_export           : out std_logic;  -- export
            pio_system_info_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            pio_system_info_read_export            : out std_logic;  -- export
            pio_system_info_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_wdi_external_connection_export     : out std_logic;  -- export
            ram_scrap_reset_export                 : out std_logic;  -- export
            ram_scrap_clk_export                   : out std_logic;  -- export
            ram_scrap_address_export               : out std_logic_vector(8 downto 0);  -- export
            ram_scrap_write_export                 : out std_logic;  -- export
            ram_scrap_writedata_export             : out std_logic_vector(31 downto 0);  -- export
            ram_scrap_read_export                  : out std_logic;  -- export
            ram_scrap_readdata_export              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_ctrl_reset_export             : out std_logic;  -- export
            reg_dpmm_ctrl_clk_export               : out std_logic;  -- export
            reg_dpmm_ctrl_address_export           : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_ctrl_write_export             : out std_logic;  -- export
            reg_dpmm_ctrl_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_ctrl_read_export              : out std_logic;  -- export
            reg_dpmm_ctrl_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_data_reset_export             : out std_logic;  -- export
            reg_dpmm_data_clk_export               : out std_logic;  -- export
            reg_dpmm_data_address_export           : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_data_write_export             : out std_logic;  -- export
            reg_dpmm_data_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_data_read_export              : out std_logic;  -- export
            reg_dpmm_data_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_epcs_reset_export                  : out std_logic;  -- export
            reg_epcs_clk_export                    : out std_logic;  -- export
            reg_epcs_address_export                : out std_logic_vector(2 downto 0);  -- export
            reg_epcs_write_export                  : out std_logic;  -- export
            reg_epcs_writedata_export              : out std_logic_vector(31 downto 0);  -- export
            reg_epcs_read_export                   : out std_logic;  -- export
            reg_epcs_readdata_export               : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_temp_sens_reset_export        : out std_logic;  -- export
            reg_fpga_temp_sens_clk_export          : out std_logic;  -- export
            reg_fpga_temp_sens_address_export      : out std_logic_vector(2 downto 0);  -- export
            reg_fpga_temp_sens_write_export        : out std_logic;  -- export
            reg_fpga_temp_sens_writedata_export    : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_temp_sens_read_export         : out std_logic;  -- export
            reg_fpga_temp_sens_readdata_export     : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_voltage_sens_reset_export     : out std_logic;  -- export
            reg_fpga_voltage_sens_clk_export       : out std_logic;  -- export
            reg_fpga_voltage_sens_address_export   : out std_logic_vector(3 downto 0);  -- export
            reg_fpga_voltage_sens_write_export     : out std_logic;  -- export
            reg_fpga_voltage_sens_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_voltage_sens_read_export      : out std_logic;  -- export
            reg_fpga_voltage_sens_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_ctrl_reset_export             : out std_logic;  -- export
            reg_mmdp_ctrl_clk_export               : out std_logic;  -- export
            reg_mmdp_ctrl_address_export           : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_ctrl_write_export             : out std_logic;  -- export
            reg_mmdp_ctrl_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_ctrl_read_export              : out std_logic;  -- export
            reg_mmdp_ctrl_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_data_reset_export             : out std_logic;  -- export
            reg_mmdp_data_clk_export               : out std_logic;  -- export
            reg_mmdp_data_address_export           : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_data_write_export             : out std_logic;  -- export
            reg_mmdp_data_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_data_read_export              : out std_logic;  -- export
            reg_mmdp_data_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_remu_reset_export                  : out std_logic;  -- export
            reg_remu_clk_export                    : out std_logic;  -- export
            reg_remu_address_export                : out std_logic_vector(2 downto 0);  -- export
            reg_remu_write_export                  : out std_logic;  -- export
            reg_remu_writedata_export              : out std_logic_vector(31 downto 0);  -- export
            reg_remu_read_export                   : out std_logic;  -- export
            reg_remu_readdata_export               : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wdi_reset_export                   : out std_logic;  -- export
            reg_wdi_clk_export                     : out std_logic;  -- export
            reg_wdi_address_export                 : out std_logic_vector(0 downto 0);  -- export
            reg_wdi_write_export                   : out std_logic;  -- export
            reg_wdi_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_wdi_read_export                    : out std_logic;  -- export
            reg_wdi_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            rom_system_info_reset_export           : out std_logic;  -- export
            rom_system_info_clk_export             : out std_logic;  -- export
            rom_system_info_address_export         : out std_logic_vector(12 downto 0);  -- export
            rom_system_info_write_export           : out std_logic;  -- export
            rom_system_info_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            rom_system_info_read_export            : out std_logic;  -- export
            rom_system_info_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X')  -- export
        );
    end component qsys_unb2c_minimal;
end qsys_unb2c_minimal_pkg;
