2021-12-7 Eric Kooistra (Jira L2SDP-215)

Investigation of FilterTaskForce.zip contents.

1) Scope
2) FilterTaskForce
   a) Powerpoint presentations:
   b) Word documents:
   c) MATLAB files:
   d) Coefficient files



1) Scope

I checked the FilterTaskForce.zip that Andre Gunst still had with documents
and Matlab code for the Polyphase Filterbank that is used in LOFAR1.

Unfortunately the documents and notes are not very clear. The most useful
things are:
- the PFB drawings in filterbanks2.ppt
- the filter specification in MeasurementsEffect.ppt

The pfs_coeff_final.m generates the Coefficient_16KKaiser.dat, these FIR
coefficients are almost the same as in:

  git/hdl/libraries/dsp/filter/src/hex/Coeffs16384Kaiser-quant.dat

that is used in LOFAR1, but unfortunately not exactly the same. Hence the
most useful files are pfs_coeff_final.m and Coefficient_16KKaiser.dat. For
further analysis in LOFAR2 these files are copied to:

  git/hdl/applications/lofar2/model



2a) FTF Powerpoint presentations:

* Feb  9  2004 'Filter test bench algorithm verification.ppt' : not clear and not useful.
* Feb 10  2004 '1KHz RFI Impact.ppt':

  Plot of output power and phase over PFB bands, the phase is linear per band
  and over all bands.

* Feb 25  2004 'RFI study.ppt' : not clear and not useful.
* Feb 27  2004  Filter_presentation.ppt : not clear and not useful.
* May 14  2004 'Filter Task Force(final).ppt' : not clear and not useful.
* May 14  2004  filterbanks2.ppt
  Diagram of FIR and FFT for PFB, with L = M*N = 256 * 16 = 4096 coefficients.
  . M polyphases and M point FFT
  . N taps per polyhase

* Jul  9  2004  FIRprototype.ppt : not clear and not useful.
* Jul  9  2004  FIRprototype2.ppt : not clear and not useful.
* Sep 17  2004  Filtermeeting2.ppt : not clear and not useful, except:

  The filter design method fircls1 LSQ based methods gives good result for the
  basic 2-4K design. An interpolation should be applied in order to realize
  4K -> 16K coefficient generation. (Add 16K coefficients within the 4K
  coefficients). Two methods are possible:
  - Classic: 0 padding + FIR (long if good filter quality)
  - FFT : creation of the bandwidth in padding zeros directly in the frequency domain.
  ==> FFT interpolation yields best results

* Oct 11  2004  MeasurementsEffect.ppt: not clear and not useful, except:

  Stop band: -90.5dB
  Pass band ripple: -0.45 dB
  -3dB -> 40dB: 23.3%
  -3dB -> 90.5dB: 32.89%

* Feb 10  2005 'Quantization results 200MHz.ppt' : not useful.


2b) FTF  Word documents:

* Dec 15  2003  Memoonbasebandshift.doc
  Idea to shift the bands per station, so that the subband aliasing decorrelates at
  the centrals processing. This was not used further.

* May 14  2004 'Problematic choice of the filter.doc'
  Some known pros and cons of critically sampled and oversampled PFB.

* Nov 23  2004  MinuteFTF2806.doc : not clear and not useful.

RTF documents : all not useful

* Oct 27  2003 'minutes 3.rtf'
* Jan 22  2004 'Minutes filter-22-01.rtf'
* Feb 27  2004  minutesFTF26-02.rtf
* Mar 10  2004 'RFI discussion.rtf'
* Mar 22  2004 'FTF - minutes_22-03-2004.rtf'
* Jun 28  2004 'Document Plan.rtf'

Oct  7  2003 'FTFarchive/Lofar station digital filtering.doc' : Forming the Filter task force
Nov 14  2003  FTFarchive/FIRresult14-11.doc : not useful.
Dec  9  2003  FTFarchive/STW-09-12.doc : Tasks and persons overview

Excel file :  all not useful

* Oct 26  2004 filter2.xls
* Oct 26  2004 filter.xls


2c) FTF  MATLAB files:

* Aug 16  2004 pfs_coeff.m : Blackman
* Aug 19  2004 pfs_coeff2.m : Hanning
* Oct 26  2004 pfs_coeff_final.m : Kaiser
  - copied to: /dop466_0/kooistra/git/hdl/applications/lofar2/model/pfs_coeff_final.m
  - generates Coefficient_16KKaiser.dat


2d) FTF  Coefficient files

* Aug 19  2004 Coefficient_16K.dat
* Oct 27  2004 Coefficient_16KKaiser.dat
  - copied to: /dop466_0/kooistra/git/hdl/applications/lofar2/model/data/Coefficient_16KKaiser.dat


