-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Scope component to show the concateneated DP SOSI data at the SCLK
--          sample rate
-- Description:
--   The scope supports g_nof_streams streams (signal paths).
--   The DP SOSI data is parallel data that consists of g_wideband_factor
--   concatenated data samples per field. The data can be in the SOSI data
--   field or in case of complex data in the SOSI re and im fields.
--   The output scope_*_arr can be viewed in analogue format in the Wave
--   Window to observe the signal shape at the SCLK sample rate.
-- Remark:
-- . Only for simulation.
-- . When g_use_sclk=TRUE then the input SCLK is used. Else the SCLK is derived
--   from the DCLK so that it does not have to be applied via an input. This
--   eases the use of this scope within a design.
-- . The concatenated parallel data width is g_wideband_factor * g_dat_w and
--   must fit the sosi.data, re, im fields. Therefor use g_complex to avoid
--   out of range failure on re, im in case data has too large g_dat_w to also
--   fit for re, im, which can happen because c_dp_stream_data_w >>
--   c_dp_stream_dsp_data_w.
-- . In this dp_wideband_sp_arr_scope the input is one or more wideband streams
--   and the input sosi array has size g_nof_streams, so the wideband data is
--   carried by g_wideband_factor concatenated symbols in the data field or in
--   the (re, im) fields.
--   In dp_wideband_wb_arr_scope the input is only one wideband stream
--   and the input sosi array has size g_wideband_factor, so there the wideband
--   data is carried via the sosi array dimension.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.dp_stream_pkg.all;

entity dp_wideband_sp_arr_scope is
  generic (
    g_sim                 : boolean := false;
    g_use_sclk            : boolean := true;
    g_complex             : boolean := false;
    g_nof_streams         : natural := 4;
    g_wideband_factor     : natural := 4;  -- Wideband rate factor = 4 for dp_clk processing frequency is 200 MHz frequency and SCLK sample frequency Fs is 800 MHz
    g_wideband_big_endian : boolean := false;  -- When true sp_sosi_arr[].data[3:0] = sample[t0,t1,t2,t3], else when false : sp_sosi_arr[].data[3:0] = sample[t3,t2,t1,t0]
    g_dat_w               : natural := 8  -- Actual g_dat_w width of the data field or g_dat_w/2 width in case of the re field, im field
  );
  port (
    -- Digital processing clk
    DCLK            : in std_logic := '0';

    -- Sampling clk, for simulation only
    SCLK            : in std_logic := '0';  -- SCLK rate = g_wideband_factor * DCLK rate

    -- Streaming input samples for g_nof_streams
    sp_sosi_arr     : in t_dp_sosi_arr(g_nof_streams - 1 downto 0);  -- = [3:0] = Signal Paths [D,C,B,A]

    -- Scope output samples for g_nof_streams
    scope_sosi_arr  : out t_dp_sosi_integer_arr(g_nof_streams - 1 downto 0)
  );
end dp_wideband_sp_arr_scope;

architecture beh of dp_wideband_sp_arr_scope is
  signal SCLKi           : std_logic;  -- sampling clk, for simulation only
  signal scope_cnt_arr   : t_natural_arr(g_nof_streams - 1 downto 0);
  signal st_sosi_arr     : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
begin
  sim_only : if g_sim = true generate
    use_sclk : if g_use_sclk = true generate
      SCLKi <= SCLK;  -- no worry about the delta cycle delay from SCLK to SCLKi
    end generate;

    gen_sclk : if g_use_sclk = false generate
      proc_common_dclk_generate_sclk(g_wideband_factor, DCLK, SCLKi);
    end generate;

    -- View sp_sosi_arr at the sample rate using st_sosi_arr
    gen_arr : for I in 0 to g_nof_streams - 1 generate
      p_st_sosi_arr : process(SCLKi)
        variable vI : natural;
      begin
        if rising_edge(SCLKi) then
          if g_wideband_big_endian = true then
            vI := g_wideband_factor - 1 - scope_cnt_arr(I);
          else
            vI := scope_cnt_arr(I);
          end if;
          scope_cnt_arr(I) <= 0;
          if sp_sosi_arr(I).valid = '1' and scope_cnt_arr(I) < g_wideband_factor - 1 then
            scope_cnt_arr(I) <= scope_cnt_arr(I) + 1;
          end if;
          st_sosi_arr(I)         <= sp_sosi_arr(I);
          -- Default set unused sp_sosi_arr(I) fields to 0 to avoid Warning: NUMERIC_STD.TO_INTEGER: metavalue detected within func_dp_stream_slv_to_integer()
          st_sosi_arr(I).data    <= (others => '0');
          st_sosi_arr(I).re      <= (others => '0');
          st_sosi_arr(I).im      <= (others => '0');
          st_sosi_arr(I).empty   <= (others => '0');
          st_sosi_arr(I).channel <= (others => '0');
          st_sosi_arr(I).err     <= (others => '0');
          if g_complex then
            st_sosi_arr(I).re <= RESIZE_DP_DSP_DATA(sp_sosi_arr(I).re((g_wideband_factor - vI) * g_dat_w - 1 downto (g_wideband_factor - vI - 1) * g_dat_w));
            st_sosi_arr(I).im <= RESIZE_DP_DSP_DATA(sp_sosi_arr(I).im((g_wideband_factor - vI) * g_dat_w - 1 downto (g_wideband_factor - vI - 1) * g_dat_w));
          else
            st_sosi_arr(I).data <= RESIZE_DP_SDATA(sp_sosi_arr(I).data((g_wideband_factor - vI) * g_dat_w - 1 downto (g_wideband_factor - vI - 1) * g_dat_w));
          end if;
        end if;
      end process;

      -- Map sosi to SLV of actual g_dat_w to allow observation in Wave Window in analogue format
      scope_sosi_arr(I) <= func_dp_stream_slv_to_integer(st_sosi_arr(I), g_dat_w);
    end generate;
  end generate;
end beh;
