-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb1_board_lib, technology_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity tb_unb1_ddr3_reorder_dual_rank is
    generic (
      g_design_name : string       := "unb1_ddr3_reorder_dual_rank";
      g_design_note : string       := "Reference Reorder";
      g_sim_unb_nr  : natural      := 0;  -- UniBoard 0
      g_sim_node_nr : natural      := 7;  -- Back node 3
      g_tech_ddr    : t_c_tech_ddr := c_tech_ddr3_4g_800m_master
    );
end tb_unb1_ddr3_reorder_dual_rank;

architecture tb of tb_unb1_ddr3_reorder_dual_rank is
begin
  u_tb_revision : entity work.tb_unb1_ddr3_reorder
    generic map(
      g_design_name => g_design_name,
      g_design_note => g_design_note,
      g_sim_unb_nr  => g_sim_unb_nr,
      g_sim_node_nr => g_sim_node_nr,
      g_tech_ddr    => g_tech_ddr
  );
end tb;
