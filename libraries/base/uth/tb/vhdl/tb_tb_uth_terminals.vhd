-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- as 2
-- run -all

entity tb_tb_uth_terminals is
end tb_tb_uth_terminals;

architecture tb of tb_tb_uth_terminals is
  constant c_phy_link_valid_support : boolean := true;
  constant c_nof_repeat             : natural := 100;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --                                                                   in_en,    out_ready, nof_repeat,   phy_fifo_size, uth_rx_timeout_w, tx_use_fifo, tx_fifo_fill, rx_use_fifo, rx_fifo_fill, fifo_use_sync, fifo_use_bsn, nof_input, nof_serial
  u_rnd_rnd_4_3            : entity work.tb_uth_terminals generic map (e_random, e_random,  c_nof_repeat, 0,             0,                false,        0,           false,        0,           false,         false,        4,         3);
  u_rnd_rnd_3_4            : entity work.tb_uth_terminals generic map (e_random, e_random,  c_nof_repeat, 0,             0,                false,        0,           false,        0,           false,         false,        3,         4);
  u_rnd_rnd_4_3_fifo       : entity work.tb_uth_terminals generic map (e_random, e_random,  c_nof_repeat, 0,             0,                true,        15,           true,        15,           false,         false,        4,         3);
  u_rnd_rnd_3_4_fifo       : entity work.tb_uth_terminals generic map (e_random, e_random,  c_nof_repeat, 0,             0,                true,        15,           true,        15,           false,         false,        3,         4);
  u_rnd_rnd_4_3_fifo_sync  : entity work.tb_uth_terminals generic map (e_random, e_random,  c_nof_repeat, 0,             0,                true,        15,           true,        15,           true,          true,         4,         3);
  u_rnd_rnd_3_4_fifo_sync  : entity work.tb_uth_terminals generic map (e_random, e_random,  c_nof_repeat, 0,             0,                true,        15,           true,        15,           true,          true,         3,         4);
end tb;
