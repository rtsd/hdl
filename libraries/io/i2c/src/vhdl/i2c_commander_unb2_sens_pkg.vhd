-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: I2C commander settings for the UniBoard2 I2C SENS bus
--          sensors.
-- Remark:
-- . Adapted from i2c_commander_unbh_pkg
--   reduce the number of protocol lists to 4

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.i2c_pkg.all;
use work.i2c_commander_pkg.all;
use work.i2c_dev_unb2_pkg.all;  -- slave addresses and commands for all unb2 i2c interfaces

package i2c_commander_unb2_sens_pkg is
  ------------------------------------------------------------------------------
  -- Local constants (with prefix 'k_')
  ------------------------------------------------------------------------------

  -- Define the protocol lists for the commander (c_i2c_cmdr_max_nof_protocols=16)
  constant k_protocol_list_0      : t_nat_natural_arr := c_i2c_unb2_sens_max1617_protocol_list_read_config;
  constant k_protocol_list_1      : t_nat_natural_arr := c_i2c_unb2_sens_max1617_protocol_list_read_temp;
  constant k_protocol_list_2      : t_nat_natural_arr := c_i2c_unb2_sens_protocol_list_read_all;
  constant k_protocol_list_3      : t_nat_natural_arr := c_i2c_cmdr_protocol_list_end;

  constant k_protocol_ram_init : t_nat_natural_arr := k_protocol_list_0  &
                                                      k_protocol_list_1  &
                                                      k_protocol_list_2  &
                                                      k_protocol_list_3;

  -- Define the corresponding mask words for the result data
  constant k_expected_mask_0      : std_logic_vector := c_i2c_unb2_sens_max1617_expected_mask_read_config;
  constant k_expected_mask_1      : std_logic_vector := c_i2c_unb2_sens_max1617_expected_mask_read_temp;
  constant k_expected_mask_2      : std_logic_vector := c_i2c_unb2_sens_expected_mask_read_all;
  constant k_expected_mask_3      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_4      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_5      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_6      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_7      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_8      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_9      : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_10     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_11     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_12     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_13     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_14     : std_logic_vector := c_i2c_cmdr_expected_mask_end;
  constant k_expected_mask_15     : std_logic_vector := c_i2c_cmdr_expected_mask_end;

  constant k_expected_mask_arr : t_slv_32_arr(0 to c_i2c_cmdr_max_nof_protocols - 1) := (k_expected_mask_0,
                                                                                       k_expected_mask_1,
                                                                                       k_expected_mask_2,
                                                                                       k_expected_mask_3,
                                                                                       k_expected_mask_4,
                                                                                       k_expected_mask_5,
                                                                                       k_expected_mask_6,
                                                                                       k_expected_mask_7,
                                                                                       k_expected_mask_8,
                                                                                       k_expected_mask_9,
                                                                                       k_expected_mask_10,
                                                                                       k_expected_mask_11,
                                                                                       k_expected_mask_12,
                                                                                       k_expected_mask_13,
                                                                                       k_expected_mask_14,
                                                                                       k_expected_mask_15);

  -- Define the corresponding expected nof read data
  constant k_nof_result_data_0    : natural := c_i2c_unb2_sens_max1617_nof_result_data_read_config;
  constant k_nof_result_data_1    : natural := c_i2c_unb2_sens_max1617_nof_result_data_read_temp;
  constant k_nof_result_data_2    : natural := c_i2c_unb2_sens_nof_result_data_read_all;
  constant k_nof_result_data_3    : natural := c_i2c_cmdr_nof_result_data_none;

  constant k_nof_result_data_arr : t_nat_natural_arr := (k_nof_result_data_0,
                                                         k_nof_result_data_1,
                                                         k_nof_result_data_2,
                                                         k_nof_result_data_3);

  -- Define the corresponding protocol list offsets
  constant k_protocol_ofs_0  : natural := 0;
  constant k_protocol_ofs_1  : natural := k_protocol_list_0'length  + k_protocol_ofs_0;
  constant k_protocol_ofs_2  : natural := k_protocol_list_1'length  + k_protocol_ofs_1;
  constant k_protocol_ofs_3  : natural := k_protocol_list_2'length  + k_protocol_ofs_2;
  constant k_protocol_ofs_4  : natural := 0 + k_protocol_ofs_3;
  constant k_protocol_ofs_5  : natural := 0 + k_protocol_ofs_4;
  constant k_protocol_ofs_6  : natural := 0 + k_protocol_ofs_5;
  constant k_protocol_ofs_7  : natural := 0 + k_protocol_ofs_6;
  constant k_protocol_ofs_8  : natural := 0 + k_protocol_ofs_7;
  constant k_protocol_ofs_9  : natural := 0 + k_protocol_ofs_8;
  constant k_protocol_ofs_10 : natural := 0 + k_protocol_ofs_9;
  constant k_protocol_ofs_11 : natural := 0 + k_protocol_ofs_10;
  constant k_protocol_ofs_12 : natural := 0 + k_protocol_ofs_11;
  constant k_protocol_ofs_13 : natural := 0 + k_protocol_ofs_12;
  constant k_protocol_ofs_14 : natural := 0 + k_protocol_ofs_13;
  constant k_protocol_ofs_15 : natural := 0 + k_protocol_ofs_14;

  constant k_protocol_ofs_arr : t_natural_arr(0 to c_i2c_cmdr_max_nof_protocols - 1) := (k_protocol_ofs_0,
                                                                                       k_protocol_ofs_1,
                                                                                       k_protocol_ofs_2,
                                                                                       k_protocol_ofs_3,
                                                                                       k_protocol_ofs_4,
                                                                                       k_protocol_ofs_5,
                                                                                       k_protocol_ofs_6,
                                                                                       k_protocol_ofs_7,
                                                                                       k_protocol_ofs_8,
                                                                                       k_protocol_ofs_9,
                                                                                       k_protocol_ofs_10,
                                                                                       k_protocol_ofs_11,
                                                                                       k_protocol_ofs_12,
                                                                                       k_protocol_ofs_13,
                                                                                       k_protocol_ofs_14,
                                                                                       k_protocol_ofs_15);

  -- RAM sizes
  constant k_protocol_ram_nof_dat : natural := ceil_div(k_protocol_ram_init'LENGTH, c_i2c_cmdr_mem_block_sz) * c_i2c_cmdr_mem_block_sz;
  constant k_protocol_ram_adr_w   : natural := ceil_log2(k_protocol_ram_nof_dat);
  constant k_result_adr_w         : natural := k_protocol_ram_adr_w - 1;  -- assume < 1 result byte per 2 protocol bytes
  constant k_result_nof_dat       : natural := 2**k_result_adr_w;

  -- Commander settings
  constant k_nof_protocols        : natural := 3;  -- Must be >= actually used nof protocol lists in k_protocol_ram_init and <= c_i2c_cmdr_max_nof_protocols=16 protocols.
  constant k_nof_result_data_max  : natural := 32;  -- Must be >= the maximum nof read bytes in any protocol list in k_protocol_ram_init
  constant k_result_cnt_w         : natural := k_result_adr_w;

  ------------------------------------------------------------------------------
  -- Global constants (with prefix 'c_i2c_cmdr_unbh_')
  ------------------------------------------------------------------------------

  constant c_i2c_cmdr_unb2_sens_protocol_ram_init   : t_nat_natural_arr := k_protocol_ram_init;
  constant c_i2c_cmdr_unb2_sens_nof_result_data_arr : t_nat_natural_arr := k_nof_result_data_arr;

  constant c_i2c_cmdr_unb2_sens_i2c_mm              : t_c_i2c_mm := (c_i2c_control_adr_w,
                                                                k_protocol_ram_adr_w,
                                                                k_protocol_ram_nof_dat,
                                                                k_result_adr_w,
                                                                k_result_nof_dat);

  constant c_i2c_cmdr_unb2_sens_protocol_commander  : t_c_i2c_cmdr_commander := (k_nof_protocols,
                                                                            k_protocol_ofs_arr,
                                                                            k_expected_mask_arr,
                                                                            k_result_cnt_w,
                                                                            k_nof_result_data_max);
end i2c_commander_unb2_sens_pkg;

package body i2c_commander_unb2_sens_pkg is
end i2c_commander_unb2_sens_pkg;
