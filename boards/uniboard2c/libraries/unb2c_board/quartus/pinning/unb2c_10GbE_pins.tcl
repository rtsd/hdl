
set_location_assignment PIN_AL32 -to CLKUSR
set_instance_assignment -name IO_STANDARD "1.8 V" -to CLKUSR



set_location_assignment PIN_Y36 -to SA_CLK
set_location_assignment PIN_Y35 -to "SA_CLK(n)"

set_instance_assignment -name IO_STANDARD LVDS -to SA_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "SA_CLK(n)"

# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SA_CLK


set_location_assignment PIN_AH9 -to SB_CLK
set_location_assignment PIN_AH10 -to "SB_CLK(n)"

set_instance_assignment -name IO_STANDARD LVDS -to SB_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "SB_CLK(n)"

# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SB_CLK


set_location_assignment PIN_V9 -to BCK_REF_CLK
set_location_assignment PIN_V10 -to "BCK_REF_CLK(n)"
set_instance_assignment -name IO_STANDARD LVDS -to BCK_REF_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "BCK_REF_CLK(n)"



set_global_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON

# QSFP_0_RX
set_location_assignment PIN_AN38 -to QSFP_0_RX[0]
set_location_assignment PIN_AM40 -to QSFP_0_RX[1]
set_location_assignment PIN_AK40 -to QSFP_0_RX[2]
set_location_assignment PIN_AJ38 -to QSFP_0_RX[3]
set_location_assignment PIN_AN37 -to "QSFP_0_RX[0](n)"
set_location_assignment PIN_AM39 -to "QSFP_0_RX[1](n)"
set_location_assignment PIN_AK39 -to "QSFP_0_RX[2](n)"
set_location_assignment PIN_AJ37 -to "QSFP_0_RX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_0_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_0_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_0_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_0_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_0_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_0_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_0_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_0_RX[3]

# QSFP_0_TX
set_location_assignment PIN_AN42 -to QSFP_0_TX[0]
set_location_assignment PIN_AM44 -to QSFP_0_TX[1]
set_location_assignment PIN_AK44 -to QSFP_0_TX[2]
set_location_assignment PIN_AJ42 -to QSFP_0_TX[3]
set_location_assignment PIN_AN41 -to "QSFP_0_TX[0](n)"
set_location_assignment PIN_AM43 -to "QSFP_0_TX[1](n)"
set_location_assignment PIN_AK43 -to "QSFP_0_TX[2](n)"
set_location_assignment PIN_AJ41 -to "QSFP_0_TX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_0_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[3]


# QSFP_1_RX
set_location_assignment PIN_AC38 -to QSFP_1_RX[0]
set_location_assignment PIN_AD40 -to QSFP_1_RX[1]
set_location_assignment PIN_AF40 -to QSFP_1_RX[2]
set_location_assignment PIN_AG38 -to QSFP_1_RX[3]
set_location_assignment PIN_AC37 -to "QSFP_1_RX[0](n)"
set_location_assignment PIN_AD39 -to "QSFP_1_RX[1](n)"
set_location_assignment PIN_AF39 -to "QSFP_1_RX[2](n)"
set_location_assignment PIN_AG37 -to "QSFP_1_RX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_1_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_1_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_1_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_1_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_1_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_1_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_1_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_1_RX[3]


#
# QSFP_1_TX
set_location_assignment PIN_AC42 -to QSFP_1_TX[0]
set_location_assignment PIN_AD44 -to QSFP_1_TX[1]
set_location_assignment PIN_AF44 -to QSFP_1_TX[2]
set_location_assignment PIN_AG42 -to QSFP_1_TX[3]
set_location_assignment PIN_AC41 -to "QSFP_1_TX[0](n)"
set_location_assignment PIN_AD43 -to "QSFP_1_TX[1](n)"
set_location_assignment PIN_AF43 -to "QSFP_1_TX[2](n)"
set_location_assignment PIN_AG41 -to "QSFP_1_TX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_1_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_1_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_1_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_1_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_1_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_1_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_1_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_1_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_1_TX[3]




# QSFP_2_RX
set_location_assignment PIN_AL38 -to QSFP_2_RX[0]
set_location_assignment PIN_AH40 -to QSFP_2_RX[1]
set_location_assignment PIN_AE38 -to QSFP_2_RX[2]
set_location_assignment PIN_AB40 -to QSFP_2_RX[3]
set_location_assignment PIN_AL37 -to "QSFP_2_RX[0](n)"
set_location_assignment PIN_AH39 -to "QSFP_2_RX[1](n)"
set_location_assignment PIN_AE37 -to "QSFP_2_RX[2](n)"
set_location_assignment PIN_AB39 -to "QSFP_2_RX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_2_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_2_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_2_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_2_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_2_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_2_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_2_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_2_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_2_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_2_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_2_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_2_RX[3]



# QSFP_2_TX
set_location_assignment PIN_AL42 -to QSFP_2_TX[0]
set_location_assignment PIN_AH44 -to QSFP_2_TX[1]
set_location_assignment PIN_AE42 -to QSFP_2_TX[2]
set_location_assignment PIN_AB44 -to QSFP_2_TX[3]
set_location_assignment PIN_AL41 -to "QSFP_2_TX[0](n)"
set_location_assignment PIN_AH43 -to "QSFP_2_TX[1](n)"
set_location_assignment PIN_AE41 -to "QSFP_2_TX[2](n)"
set_location_assignment PIN_AB43 -to "QSFP_2_TX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_2_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_2_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_2_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_2_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_2_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_2_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_2_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_2_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_2_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_2_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_2_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_2_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_2_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_2_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_2_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_2_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_2_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_2_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_2_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_2_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_2_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_2_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_2_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_2_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_2_TX[3]



# QSFP_3_RX
set_location_assignment PIN_W38 -to QSFP_3_RX[0]
set_location_assignment PIN_T40 -to QSFP_3_RX[1]
set_location_assignment PIN_N38 -to QSFP_3_RX[2]
set_location_assignment PIN_K40 -to QSFP_3_RX[3]
set_location_assignment PIN_W37 -to "QSFP_3_RX[0](n)"
set_location_assignment PIN_T39 -to "QSFP_3_RX[1](n)"
set_location_assignment PIN_N37 -to "QSFP_3_RX[2](n)"
set_location_assignment PIN_K39 -to "QSFP_3_RX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_3_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_3_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_3_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_3_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_3_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_3_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_3_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_3_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_3_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_3_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_3_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_3_RX[3]


# QSFP_3_TX
set_location_assignment PIN_W42 -to QSFP_3_TX[0]
set_location_assignment PIN_T44 -to QSFP_3_TX[1]
set_location_assignment PIN_N42 -to QSFP_3_TX[2]
set_location_assignment PIN_K44 -to QSFP_3_TX[3]
set_location_assignment PIN_W41 -to "QSFP_3_TX[0](n)"
set_location_assignment PIN_T43 -to "QSFP_3_TX[1](n)"
set_location_assignment PIN_N41 -to "QSFP_3_TX[2](n)"
set_location_assignment PIN_K43 -to "QSFP_3_TX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_3_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_3_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_3_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_3_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_3_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_3_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_3_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_3_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_3_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_3_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_3_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_3_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_3_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_3_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_3_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_3_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_3_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_3_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_3_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_3_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_3_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_3_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_3_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_3_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_3_TX[3]


# QSFP_4_RX
set_location_assignment PIN_AA38 -to QSFP_4_RX[0]
set_location_assignment PIN_Y40 -to QSFP_4_RX[1]
set_location_assignment PIN_V40 -to QSFP_4_RX[2]
set_location_assignment PIN_U38 -to QSFP_4_RX[3]
set_location_assignment PIN_AA37 -to "QSFP_4_RX[0](n)"
set_location_assignment PIN_Y39 -to "QSFP_4_RX[1](n)"
set_location_assignment PIN_V39 -to "QSFP_4_RX[2](n)"
set_location_assignment PIN_U37 -to "QSFP_4_RX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_4_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_4_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_4_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_4_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_4_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_4_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_4_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_4_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_4_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_4_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_4_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_4_RX[3]


# QSFP_4_TX
set_location_assignment PIN_AA42 -to QSFP_4_TX[0]
set_location_assignment PIN_Y44 -to QSFP_4_TX[1]
set_location_assignment PIN_V44 -to QSFP_4_TX[2]
set_location_assignment PIN_U42 -to QSFP_4_TX[3]
set_location_assignment PIN_AA41 -to "QSFP_4_TX[0](n)"
set_location_assignment PIN_Y43 -to "QSFP_4_TX[1](n)"
set_location_assignment PIN_V43 -to "QSFP_4_TX[2](n)"
set_location_assignment PIN_U41 -to "QSFP_4_TX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_4_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_4_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_4_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_4_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_4_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_4_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_4_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_4_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_4_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_4_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_4_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_4_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_4_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_4_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_4_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_4_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_4_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_4_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_4_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_4_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_4_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_4_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_4_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_4_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_4_TX[3]


# QSFP_5_RX
set_location_assignment PIN_L38 -to QSFP_5_RX[0]
set_location_assignment PIN_M40 -to QSFP_5_RX[1]
set_location_assignment PIN_P40 -to QSFP_5_RX[2]
set_location_assignment PIN_R38 -to QSFP_5_RX[3]
set_location_assignment PIN_L37 -to "QSFP_5_RX[0](n)"
set_location_assignment PIN_M39 -to "QSFP_5_RX[1](n)"
set_location_assignment PIN_P39 -to "QSFP_5_RX[2](n)"
set_location_assignment PIN_R37 -to "QSFP_5_RX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_RX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_RX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_RX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_5_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_5_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_5_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_5_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_5_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_5_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_5_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_5_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_5_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                  QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                            QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to         QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to         QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to         QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to         QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to         QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to         QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to         QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to             QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to               QSFP_5_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to               QSFP_5_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                          QSFP_5_RX[3]



# QSFP_5_TX
set_location_assignment PIN_L42 -to QSFP_5_TX[0]
set_location_assignment PIN_M44 -to QSFP_5_TX[1]
set_location_assignment PIN_P44 -to QSFP_5_TX[2]
set_location_assignment PIN_R42 -to QSFP_5_TX[3]
set_location_assignment PIN_L41 -to "QSFP_5_TX[0](n)"
set_location_assignment PIN_M43 -to "QSFP_5_TX[1](n)"
set_location_assignment PIN_P43 -to "QSFP_5_TX[2](n)"
set_location_assignment PIN_R41 -to "QSFP_5_TX[3](n)"


#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_TX[0]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_TX[1]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_TX[2]
#set_instance_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON -to QSFP_5_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_5_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_5_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_5_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_5_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_5_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_5_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_5_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_5_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_5_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_5_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_5_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_5_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_5_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_5_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_5_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_5_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_5_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_5_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_5_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_5_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_5_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_5_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_5_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_5_TX[3]



set_location_assignment PIN_AC7 -to BCK_RX[23]
set_location_assignment PIN_AC8 -to "BCK_RX[23](n)"
set_location_assignment PIN_AB5 -to BCK_RX[22]
set_location_assignment PIN_AB6 -to "BCK_RX[22](n)"
set_location_assignment PIN_AE7 -to BCK_RX[21]
set_location_assignment PIN_AE8 -to "BCK_RX[21](n)"
set_location_assignment PIN_AD5 -to BCK_RX[20]
set_location_assignment PIN_AD6 -to "BCK_RX[20](n)"
set_location_assignment PIN_AG7 -to BCK_RX[19]
set_location_assignment PIN_AG8 -to "BCK_RX[19](n)"
set_location_assignment PIN_AF5 -to BCK_RX[18]
set_location_assignment PIN_AF6 -to "BCK_RX[18](n)"
set_location_assignment PIN_AJ7 -to BCK_RX[17]
set_location_assignment PIN_AJ8 -to "BCK_RX[17](n)"
set_location_assignment PIN_AH5 -to BCK_RX[16]
set_location_assignment PIN_AH6 -to "BCK_RX[16](n)"
set_location_assignment PIN_AL7 -to BCK_RX[15]
set_location_assignment PIN_AL8 -to "BCK_RX[15](n)"
set_location_assignment PIN_AK5 -to BCK_RX[14]
set_location_assignment PIN_AK6 -to "BCK_RX[14](n)"
set_location_assignment PIN_AN7 -to BCK_RX[13]
set_location_assignment PIN_AN8 -to "BCK_RX[13](n)"
set_location_assignment PIN_AM5 -to BCK_RX[12]
set_location_assignment PIN_AM6 -to "BCK_RX[12](n)"
set_location_assignment PIN_AR7 -to BCK_RX[11]
set_location_assignment PIN_AR8 -to "BCK_RX[11](n)"
set_location_assignment PIN_AP5 -to BCK_RX[10]
set_location_assignment PIN_AP6 -to "BCK_RX[10](n)"
set_location_assignment PIN_AU7 -to BCK_RX[9]
set_location_assignment PIN_AU8 -to "BCK_RX[9](n)"
set_location_assignment PIN_AT5 -to BCK_RX[8]
set_location_assignment PIN_AT6 -to "BCK_RX[8](n)"
set_location_assignment PIN_AW7 -to BCK_RX[7]
set_location_assignment PIN_AW8 -to "BCK_RX[7](n)"
set_location_assignment PIN_AV5 -to BCK_RX[6]
set_location_assignment PIN_AV6 -to "BCK_RX[6](n)"
set_location_assignment PIN_BA7 -to BCK_RX[5]
set_location_assignment PIN_BA8 -to "BCK_RX[5](n)"
set_location_assignment PIN_AY5 -to BCK_RX[4]
set_location_assignment PIN_AY6 -to "BCK_RX[4](n)"
set_location_assignment PIN_BC7 -to BCK_RX[3]
set_location_assignment PIN_BC8 -to "BCK_RX[3](n)"
set_location_assignment PIN_BB5 -to BCK_RX[2]
set_location_assignment PIN_BB6 -to "BCK_RX[2](n)"
set_location_assignment PIN_AY9 -to BCK_RX[1]
set_location_assignment PIN_AY10 -to "BCK_RX[1](n)"
set_location_assignment PIN_BB9 -to BCK_RX[0]
set_location_assignment PIN_BB10 -to "BCK_RX[0](n)"





set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[4]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[4]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[5]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[5]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[6]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[6]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[7]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[7]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[8]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[8]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[9]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[9]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[10]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[10]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[11]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[11]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[12]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[12]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[12]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[13]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[13]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[13]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[14]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[14]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[14]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[15]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[15]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[15]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[16]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[16]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[16]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[17]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[17]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[17]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[18]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[18]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[18]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[19]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[19]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[19]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[20]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[20]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[20]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[21]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[21]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[21]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[22]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[22]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[22]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                BCK_RX[23]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                BCK_RX[23]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_RX[23]



set_location_assignment PIN_AC3 -to BCK_TX[23]
set_location_assignment PIN_AB1 -to BCK_TX[22]
set_location_assignment PIN_AE3 -to BCK_TX[21]
set_location_assignment PIN_AD1 -to BCK_TX[20]
set_location_assignment PIN_AG3 -to BCK_TX[19]
set_location_assignment PIN_AF1 -to BCK_TX[18]
set_location_assignment PIN_AJ3 -to BCK_TX[17]
set_location_assignment PIN_AH1 -to BCK_TX[16]
set_location_assignment PIN_AL3 -to BCK_TX[15]
set_location_assignment PIN_AK1 -to BCK_TX[14]
set_location_assignment PIN_AN3 -to BCK_TX[13]
set_location_assignment PIN_AM1 -to BCK_TX[12]
set_location_assignment PIN_AR3 -to BCK_TX[11]
set_location_assignment PIN_AP1 -to BCK_TX[10]
set_location_assignment PIN_AU3 -to BCK_TX[9]
set_location_assignment PIN_AT1 -to BCK_TX[8]
set_location_assignment PIN_AW3 -to BCK_TX[7]
set_location_assignment PIN_AV1 -to BCK_TX[6]
set_location_assignment PIN_BB1 -to BCK_TX[5]
set_location_assignment PIN_AY1 -to BCK_TX[4]
set_location_assignment PIN_BD5 -to BCK_TX[3]
set_location_assignment PIN_BA3 -to BCK_TX[2]
set_location_assignment PIN_BC3 -to BCK_TX[1]
set_location_assignment PIN_BD9 -to BCK_TX[0]
set_location_assignment PIN_BD10 -to "BCK_TX[0](n)"
set_location_assignment PIN_BC4 -to "BCK_TX[1](n)"
set_location_assignment PIN_BA4 -to "BCK_TX[2](n)"
set_location_assignment PIN_BD6 -to "BCK_TX[3](n)"
set_location_assignment PIN_AY2 -to "BCK_TX[4](n)"
set_location_assignment PIN_BB2 -to "BCK_TX[5](n)"
set_location_assignment PIN_AV2 -to "BCK_TX[6](n)"
set_location_assignment PIN_AW4 -to "BCK_TX[7](n)"
set_location_assignment PIN_AT2 -to "BCK_TX[8](n)"
set_location_assignment PIN_AU4 -to "BCK_TX[9](n)"
set_location_assignment PIN_AP2 -to "BCK_TX[10](n)"
set_location_assignment PIN_AR4 -to "BCK_TX[11](n)"
set_location_assignment PIN_AM2 -to "BCK_TX[12](n)"
set_location_assignment PIN_AN4 -to "BCK_TX[13](n)"
set_location_assignment PIN_AK2 -to "BCK_TX[14](n)"
set_location_assignment PIN_AL4 -to "BCK_TX[15](n)"
set_location_assignment PIN_AH2 -to "BCK_TX[16](n)"
set_location_assignment PIN_AJ4 -to "BCK_TX[17](n)"
set_location_assignment PIN_AF2 -to "BCK_TX[18](n)"
set_location_assignment PIN_AG4 -to "BCK_TX[19](n)"
set_location_assignment PIN_AD2 -to "BCK_TX[20](n)"
set_location_assignment PIN_AE4 -to "BCK_TX[21](n)"
set_location_assignment PIN_AB2 -to "BCK_TX[22](n)"
set_location_assignment PIN_AC4 -to "BCK_TX[23](n)"





set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[4]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[4]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[4]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[4]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[4]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[4]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[5]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[5]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[5]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[5]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[5]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[5]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[6]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[6]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[6]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[6]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[6]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[6]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[7]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[7]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[7]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[7]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[7]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[7]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[8]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[8]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[8]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[8]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[8]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[8]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[9]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[9]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[9]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[9]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[9]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[9]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[10]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[10]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[10]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[10]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[10]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[10]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[11]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[11]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[11]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[11]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[11]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[11]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[12]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[12]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[12]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[13]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[13]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[13]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[14]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[14]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[14]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[15]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[15]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[15]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[16]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[16]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[16]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[17]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[17]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[17]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[18]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[18]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[18]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[19]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[19]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[19]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[20]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[20]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[20]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[21]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[21]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[21]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[22]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[22]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[22]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  BCK_TX[23]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     BCK_TX[23]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to BCK_TX[23]




set_location_assignment PIN_AP40 -to RING_0_RX[0]
set_location_assignment PIN_AR38 -to RING_0_RX[1]
set_location_assignment PIN_AT40 -to RING_0_RX[2]
set_location_assignment PIN_AU38 -to RING_0_RX[3]
set_location_assignment PIN_AP39 -to "RING_0_RX[0](n)"
set_location_assignment PIN_AR37 -to "RING_0_RX[1](n)"
set_location_assignment PIN_AT39 -to "RING_0_RX[2](n)"
set_location_assignment PIN_AU37 -to "RING_0_RX[3](n)"

set_location_assignment PIN_AP44 -to RING_0_TX[0]
set_location_assignment PIN_AR42 -to RING_0_TX[1]
set_location_assignment PIN_AT44 -to RING_0_TX[2]
set_location_assignment PIN_AU42 -to RING_0_TX[3]
set_location_assignment PIN_AP43 -to "RING_0_TX[0](n)"
set_location_assignment PIN_AR41 -to "RING_0_TX[1](n)"
set_location_assignment PIN_AT43 -to "RING_0_TX[2](n)"
set_location_assignment PIN_AU41 -to "RING_0_TX[3](n)"

set_location_assignment PIN_H40 -to RING_1_RX[0]
set_location_assignment PIN_J38 -to RING_1_RX[1]
set_location_assignment PIN_F40 -to RING_1_RX[2]
set_location_assignment PIN_G38 -to RING_1_RX[3]
set_location_assignment PIN_H39 -to "RING_1_RX[0](n)"
set_location_assignment PIN_J37 -to "RING_1_RX[1](n)"
set_location_assignment PIN_F39 -to "RING_1_RX[2](n)"
set_location_assignment PIN_G37 -to "RING_1_RX[3](n)"

set_location_assignment PIN_H44 -to RING_1_TX[0]
set_location_assignment PIN_J42 -to RING_1_TX[1]
set_location_assignment PIN_G42 -to RING_1_TX[2]
set_location_assignment PIN_F44 -to RING_1_TX[3]
set_location_assignment PIN_H43 -to "RING_1_TX[0](n)"
set_location_assignment PIN_J41 -to "RING_1_TX[1](n)"
set_location_assignment PIN_G41 -to "RING_1_TX[2](n)"
set_location_assignment PIN_F43 -to "RING_1_TX[3](n)"


set_location_assignment PIN_AV40 -to RING_0_RX[4]
set_location_assignment PIN_AW38 -to RING_0_RX[5]
set_location_assignment PIN_AY40 -to RING_0_RX[6]
set_location_assignment PIN_BA38 -to RING_0_RX[7]
set_location_assignment PIN_AV39 -to "RING_0_RX[4](n)"
set_location_assignment PIN_AW37 -to "RING_0_RX[5](n)"
set_location_assignment PIN_AY39 -to "RING_0_RX[6](n)"
set_location_assignment PIN_BA37 -to "RING_0_RX[7](n)"

set_location_assignment PIN_BB40 -to RING_0_RX[8]
set_location_assignment PIN_BC38 -to RING_0_RX[9]
set_location_assignment PIN_AY36 -to RING_0_RX[10]
set_location_assignment PIN_BB36 -to RING_0_RX[11]
set_location_assignment PIN_BB39 -to "RING_0_RX[8](n)"
set_location_assignment PIN_BC37 -to "RING_0_RX[9](n)"
set_location_assignment PIN_AY35 -to "RING_0_RX[10](n)"
set_location_assignment PIN_BB35 -to "RING_0_RX[11](n)"

set_location_assignment PIN_AV44 -to RING_0_TX[4]
set_location_assignment PIN_AW42 -to RING_0_TX[5]
set_location_assignment PIN_AY44 -to RING_0_TX[6]
set_location_assignment PIN_BB44 -to RING_0_TX[7]
set_location_assignment PIN_AV43 -to "RING_0_TX[4](n)"
set_location_assignment PIN_AW41 -to "RING_0_TX[5](n)"
set_location_assignment PIN_AY43 -to "RING_0_TX[6](n)"
set_location_assignment PIN_BB43 -to "RING_0_TX[7](n)"


set_location_assignment PIN_BA42 -to RING_0_TX[8]
set_location_assignment PIN_BD40 -to RING_0_TX[9]
set_location_assignment PIN_BC42 -to RING_0_TX[10]
set_location_assignment PIN_BD36 -to RING_0_TX[11]
set_location_assignment PIN_BA41 -to "RING_0_TX[8](n)"
set_location_assignment PIN_BD39 -to "RING_0_TX[9](n)"
set_location_assignment PIN_BC41 -to "RING_0_TX[10](n)"
set_location_assignment PIN_BD35 -to "RING_0_TX[11](n)"


set_location_assignment PIN_D40 -to RING_1_RX[4]
set_location_assignment PIN_E38 -to RING_1_RX[5]
set_location_assignment PIN_F36 -to RING_1_RX[6]
set_location_assignment PIN_C38 -to RING_1_RX[7]
set_location_assignment PIN_D39 -to "RING_1_RX[4](n)"
set_location_assignment PIN_E37 -to "RING_1_RX[5](n)"
set_location_assignment PIN_F35 -to "RING_1_RX[6](n)"
set_location_assignment PIN_C37 -to "RING_1_RX[7](n)"

set_location_assignment PIN_B36 -to RING_1_RX[8]
set_location_assignment PIN_D36 -to RING_1_RX[9]
set_location_assignment PIN_E34 -to RING_1_RX[10]
set_location_assignment PIN_C34 -to RING_1_RX[11]
set_location_assignment PIN_B35 -to "RING_1_RX[8](n)"
set_location_assignment PIN_D35 -to "RING_1_RX[9](n)"
set_location_assignment PIN_E33 -to "RING_1_RX[10](n)"
set_location_assignment PIN_C33 -to "RING_1_RX[11](n)"

set_location_assignment PIN_E42 -to RING_1_TX[4]
set_location_assignment PIN_D44 -to RING_1_TX[5]
set_location_assignment PIN_B44 -to RING_1_TX[6]
set_location_assignment PIN_C42 -to RING_1_TX[7]
set_location_assignment PIN_E41 -to "RING_1_TX[4](n)"
set_location_assignment PIN_D43 -to "RING_1_TX[5](n)"
set_location_assignment PIN_B43 -to "RING_1_TX[6](n)"
set_location_assignment PIN_C41 -to "RING_1_TX[7](n)"

set_location_assignment PIN_B40 -to RING_1_TX[8]
set_location_assignment PIN_A42 -to RING_1_TX[9]
set_location_assignment PIN_A38 -to RING_1_TX[10]
set_location_assignment PIN_A34 -to RING_1_TX[11]
set_location_assignment PIN_B39 -to "RING_1_TX[8](n)"
set_location_assignment PIN_A41 -to "RING_1_TX[9](n)"
set_location_assignment PIN_A37 -to "RING_1_TX[10](n)"
set_location_assignment PIN_A33 -to "RING_1_TX[11](n)"


set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[4]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[4]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[5]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[5]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[6]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[6]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[7]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[7]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[8]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[8]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[9]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[9]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[10]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[10]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[11]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[11]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[4]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[4]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[4]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[5]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[5]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[5]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[6]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[6]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[6]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[7]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[7]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[7]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[8]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[8]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[8]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[9]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[9]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[9]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[10]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[10]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[10]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[11]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[11]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[11]



set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[4]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[4]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[4]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[4]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[4]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[4]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[5]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[5]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[5]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[5]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[5]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[5]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[6]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[6]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[6]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[6]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[6]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[6]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[7]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[7]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[7]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[7]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[7]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[7]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[8]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[8]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[8]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[8]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[8]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[8]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[9]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[9]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[9]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[9]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[9]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[9]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[10]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[10]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[10]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[10]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[10]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[10]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[11]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[11]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[11]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[11]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[11]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[11]


set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[4]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[4]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[4]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[4]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[4]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[4]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[5]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[5]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[5]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[5]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[5]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[5]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[6]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[6]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[6]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[6]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[6]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[6]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[7]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[7]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[7]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[7]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[7]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[7]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[8]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[8]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[8]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[8]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[8]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[8]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[9]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[9]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[9]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[9]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[9]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[9]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[10]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[10]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[10]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[10]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[10]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[10]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[11]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[11]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[11]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[11]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[11]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[11]


set_location_assignment PIN_AT31 -to QSFP_RST
set_instance_assignment -name IO_STANDARD "1.8 V" -to QSFP_RST

