// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HS2^H.6HA[*UPQ9=_0*5"3]8G2,^&,!*EA>6BJ=(94P$*GI??*]S%MP  
H#(="W_8^X<HI$Y#JY+0HWRAIQ_U"9BW;(5\.+ D\O$F!,&%]<-"$*P  
HTNBD%@ZL[&E2Q,C;)>N"/.O-.UGA)!K@(?]BA$B716$?J>9S:CNEO   
HB/[I%*]WW0/&2H]#AO]-_2"V)@HR#);/;A&A*0HS[ KLJ/QHUFQ3T0  
HB$SYA2+5$?D"#S9^XRIZIC48PE@3H6$GG3+VS2FBLBFN%,A>T29<T@  
`pragma protect encoding=(enctype="uuencode",bytes=13744       )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@8"4? HW!U^U*#(64#N)%C'OX^N&Q'WO2\#"W*C#A"E, 
@!U.]/EZ(88^/] "-R+#X?-Y=O;;RXWCE+Z2ME[Y*7;@ 
@#.\Y!5'+!(_!6S%Y>?X#<(48*_RR'%1C<>I[H!13:*T 
@">#DM.J;SG2)Y8 ^#@98H@'O3>X!-W3@FX6 ?K$:B88 
@QBB!>:Q:R.I*'T=J)-%P82\7* ]5[R;K:]-\K$YA98P 
@45;TQ4":LW*&WJ9O( 4U^Q\V;(BB?1)4=O6$5P)JG 8 
@Z8XP 0->:*KEQ-B^?9U': H&4#*G*LZ85LUK5P@DJ<  
@M#L[2*VYP^.Y4LH]S!U7X9!;UZTZ1CIUQX;N)N*U#G( 
@'>ML3#7PS/S*77T:V9 ">G-&)I>A+T7XV!@9]HRS:MH 
@8:;=OX%(868>=64=I?!G8?Q()N4D408CO!6U(%)&A,P 
@2CTSJ3K%COEVDH1[I&?^)<'RM"-29#N;;,Z="Y&9"N  
@1AS&6#X+*6MFVM<UXFTHGMRRY+R&:#YWPYNXU;H]VGX 
@JV]JN*##YMY"9D)X!WC.><88UBD]"IXNE9,R1K;)U"P 
@TSU+ N]48Q5BUB28$H%"RLD=SH'*W5I(,770&N,BC@0 
@8'*V@#[^X&O^-NNIY<[%01F'>+9U,G"1]++F"L[S_\@ 
@9@%%+:W[[V]R(N[\^?61IZ$D&-$O:F_QH.SJ'*A'N($ 
@Q]G$!O,#+=5+CS(0%,@6)Y&EL<>6Q(GNP'U[8_I.*6, 
@U0;J7$JNK>>-K-A<7UA[+?;+@"*,('F)5RZ[W [ 2!\ 
@^+]E.Y#8/S7&&7>@R:J@FH-[VEB-; C1Y.GW[@>+G@@ 
@XE=?3:=P1W]=3@S\L29E)OG=U//#ST53K2*J%"W\,D< 
@9$B1;[:EGS-\4>($'_%&-2Q*$9,%<](9V"=J_H1R"ND 
@_S0=GI@+S)&)FJ9CY\\+@WM1)*$T@?4$HX5=WK"DK9T 
@*AX+M<+5ZW@$MO<$>\)!$PCR-(R]TS9EU><]<5&+B:X 
@UB9.1DW49W;L&>A'YSYLW\]UP4U6XG65,D#0;/9%590 
@LER$^AHXW2W9U W2 ND1;B[>U4;Z6:SDB!'<[,?D&W@ 
@*,&FB)#UTO<IWL,*T,T3"IVWHZI\7 QQ=$9C%B=5#RD 
@)?*[O:L***;LRYG\QWH_1Q*$R%(H SOU,:%GE_);SAT 
@8*8' ]_*G*$?K-YDPL^2BFE3-HFU@HBON2-,_4010#D 
@0TP=:>G$G@>!-'0(B._7^^@RP5-<%20J@W$'#.:Z%]  
@:BJOS5EA7?#@-]Q6*>IX1A-'K?TH9K R*_'H52_T0,\ 
@['0U>J<<24@5]QU./$$C\;:Z#U'\-#Q=E9Q.5!F$_/@ 
@,_B/BM#<N#9;T%FPK_?[9UL)%+O6SEMO([3%,>2-*5H 
@!%W44:1%^8)R$ZN0*X*_<9X%?6X1I#><JQ]NE\9&ETH 
@=Y@ B0EF*,9'6:YJ2; !9&1)I'<Y_AW6B: @K#N5#8L 
@PAT""*&N+9<(H6N;8N4=WJ!CM2%SZ/'[+./MAZB6&<< 
@D_'2]ZX_]PS^,CS\T]*N'()Q8. \]-:LS[9F\&S]%D( 
@9*_W0D9P)I9<QN^.FMGM5.GS:U5L"J;?RZ#G,T @]I$ 
@J5R,38I?:+=(,M(+\32&TAR6;6_[][H@K/1]$)8<?!< 
@PF4E=X&E#X2S;U^*J/E=R*8[0>P2;0^/SMP3Q)$B$K8 
@>PY7O,D!FC;#>G\W_P*3K16\Z(JW5[I^@M7D4+CA',( 
@OLU$8W")9V$X9C;K*EA>:P25H'\TOKVF0-6_AO<B('< 
@JKV%+:59,,[26'"O\])":J76Z-<'_Q@V,I16Z4JASZL 
@J&Q0<7[E+R=M]-A#-L9IUS8&L:'YO5#0[2=B^BX\#"4 
@OU4I6UB,T>+1E1^A[B]-P1WDS4A8<M.^<&F_\ R3Z#0 
@,IKNW<6C#44;S>:32=5G#:Q$$@2[UJQIG64#=41&]3< 
@8;=.&E\%3-;OU'(&W5>I8CS5&TPU)+##532]YN9FK:$ 
@RF^+%%3,"%GWM4Y*PWOW(<X^QEP.@=B4B[8:+;M-=8T 
@.#43.GC,8-H4F"M?ZV056&!-,N]/%1P1D3ZL >ZK=ZP 
@AR(-A'%6ZME[TLECX!?OGTWH;4#[T2OY'IW-(>M<:U  
@0T>LBG3PBV]O+C&4+P K.X$N>03=?C9IFH!4AM1\N*@ 
@UW-;+6V<+-TC041'?40Q*HV6]Y[D]51ZP.)@;9VM7/  
@IZ6+280T$)N!KHA]+!\>7?3XEM/KVN^M?T+7^SX&$#\ 
@"*%^<Z<I-!H.@$.V&"\P+>92(5PG1#$IST U'1 +]/, 
@?:\4B9M#H&(X+'ZI8N'XJ&/>DLVC#Z.%9!LB+358$IL 
@KZ?/E^][DJ+?@STNC<[+D)FR+DE2S%60-'CNZCF]( P 
@49H"YVMI/Y_6/(],6V6"TW,47Q*QLU0<=:ZLNAG<*&0 
@<_IH^H8X!1>'.S-&;0<<YAO$T9RRMH^D>'XL:*B\G50 
@-R%R#_B-DZ$K\@6R./\M-[\I-A.=L,]>%9@:_!@DK(, 
@'"\;Q<2W%=\B,\4DES#-A@3&>RA9BG$JIDV/QIX_G H 
@1,B%R5F7RZ-!L5-!2M4KWR7:VR?I>J*%<W2X-O&'PB, 
@K,9)>!R\C#8PN__L9!4SV>5S>XLG8>?[4J/K..6"R[L 
@K'Q5RTIODBR5&XZT4SS&;CH+"V*:_W+U<,4L%P=>!JL 
@L$/DM56$%A]/RDL2?_/$!#Q)E\T,9<H_:3M#0C9NS1( 
@[DF^GD"?'_9HY!];#5.M/(6*),L-0]&N+':U/3-E^D, 
@DM:6\QNLK0*E1\E*FWMB])]M,I#^L7Z4@^#LYAM54 L 
@ B&$AR_2$5R@B CWXY;E>I,O[H"QQ<_## ]<.B_J0?D 
@[W/J[)VJ.!]H%>.#)S2 ],7'_WT5=!BD-G[T"-A>MQ  
@B;;V/P8(/?4Q$UIE5 &)"Q9CT*&!+.H+F11U'L.<-S8 
@1@-B-3MXY6AMR]_L6+#$?(](@"$C?&BJ.#R_8UC<.5  
@Z+XKEX0YJN=*FH^F:[#2<@@T3/*#^5];/8-S@C>YLY$ 
@/I<VBY0W2'.#W"K7A#$#F9-?JTA_<\#L"E*6VI9Q"#0 
@XQJE\:CS?-FENO:%8<%L2]L+QRCV<#2;'8!Q385,*:$ 
@A:>ZBYA_5ANT&MSI+-YN"Q./RZ:'\D0))!?]1+;&3H< 
@TWU%)4@7?<8>3H]*]A%\<#P :_G3W&0/V]T%LSR*_.( 
@Z-<S]4<JX_@O6$KDND\'FS]!_X@O!TJ$.R/BW2CZ=W$ 
@832$BDO FKB[ZJ; OK7-R@NF:F;X;*)H970JQ)P&$)$ 
@'C&L^-!(.C^^_D'0M.=^#C[!4[L1P1-BB/?.AYB_P=< 
@>#X"113]@XIY&$-Z1V];DX? _E.(]GG(] $[J@6E:#L 
@N?!'%U(!RY02VV@HNN+&"6&?73ZPY'#2/'$909NTR(4 
@\!M'#P76+%IOB$_9E?P"@K%S_&;/'U'AV7ML(/<D,*( 
@18I3P.%O,FE/;V#D(W,WDBO]'8T6LSJ<\,M$[S;W\,( 
@RZ69&LQ#D3XL-'A_,EU/^ AE^;D ".CQ>=ZC$HSVQ"X 
@6N2?[,DXAUV3<((X&%=*2>AD3P'/-7T4>CG] ,];*V$ 
@N=-O%N0NY2#PWI] 3%A7$4N!#??C=)&W29/14!F&:3< 
@!X-/L%R!N2W#7!P!.W.^T^R$%_?VH'CKO?X*.&?(E*$ 
@C;N/C+[@/I,5PR]/KDA[GIN'6! #OZUCMSX]SVCC4>P 
@'308?^19,:$7NU*(3FC>;_-]3AH--3U8;2E<I9=%<\@ 
@GWB7$OW.ZBQ] 4(!K(#Y8Y*5+MI.!(T^3LM!'#.#91\ 
@.<X^R:9>:/3U8Y;\A_+A;3"N&T&V&);GDG1,TC[,.&D 
@/.R6$(%L94DOH;JD4HT]YT)+N[HKM7)$O>A%V_:)EI< 
@1UVMSFN[:[\[".'SC9M443D*8Z=C0]-]9:6KOKK&9%\ 
@N+:>(_GC$%Q\Q)%[3PW:WA9<V.2$::1L*U*4L7_<1?T 
@I@2"HO3_?S)S#V$,,210#.;=C(5RBT.+3-07G[IN%JX 
@TZ<0+/:?.Q0)MSM^LVW%Y#PGJZWQKUVR:8+!H-0]K#8 
@80:3F*L5@*>R=5)9MM#QW75I4/W='#NV%N]9KG-/[(( 
@S!8YF3^<SDN/+%7/./[$UP$%=K#:K!EU;I?9$7/YB:X 
@K19(]"\T3L=R07<GB/FI_;T?=./N%?ECPX.-5]OL@ID 
@8#3K,MR('Q 84NZ2>--/WY0FODSW[AQK"V%Q'\O:%>H 
@7Q3]AC?Y#G$C7 1VN9'/97.V]XJ U0YTK^&(U*FEX2< 
@TY:R#D*D^(7S^#7NYF7Z69HI;G'4 :>I/3X&_RB!FSH 
@ 2S&J0'^X[&R.$N?3Z#EL3'=LAIXHM@@-R'A-XE#N_P 
@D:QE4@I*8<?%ZX'@S;(@I/K9?FDNU.1J %@,6D.:;.4 
@*9"<<U>$QBJTS10;B!@G^  6\^^WP"3R_0_+0*BS?@X 
@(@LP@0%#LN7->^I9LW=#?2/1(F=ZV(9#]^?77MR$*ET 
@\@\"N,.MG=L;1&<>>SIQZ() 3/,B \0%Q@@)T!W%%A\ 
@;$/NZVZ/JQXDO2 Z,W22%-RM^RT%4XX@EGP'83C< 2T 
@I>7@%R EI)8V7 #YB.6^@ 7D6:V1U,R*E/+[0B;#@K, 
@;9^'5H$$SV7<NYZZ05J3+0:&AQZ*$#1DB<EE=#_1DWT 
@"UO+=C@<Q&_0DD*%NLN"]1(>S5MO9_-QP=4W'0XNI<\ 
@!TT%/OQI!HP2P0^S@ES4L:-F,$[_:;0X<I&V>>C;M*L 
@8V\(C58DPJAK$;0A9F01!O_7#0Y2WUI:O;Y59ITGCPP 
@AS"JX_4XFC-P1"R8U1F"IKE-D1:?@"%X*M#3KNTV 68 
@W5<0Q=_!@8C$R@ZG2U/K$[F*%X%OB/;I/-,F=H/B+X$ 
@ P.8HGBUWS/6GSTB_2)OAS09$0<JLTUX@*C6*8&,Z"D 
@S)>,+I\()^8 2#W%;C$=48UK@ULG=1,%T0@_6^I&$MT 
@?2Y!;;%V#T"$88M0H3\QTP0<OU@H^6IJX!UR\?K<?PT 
@3^</DU!9F;$PEXXB-UBI@5\2]%"FQ[H//&"OS?E4.(X 
@?'CV3(/%[ +"#9D^A6^_40Z:BE.XL)-U0 2E_Z;\E<\ 
@.\C-L6Y-T)[_NNQV$%'11"KBE@*0[W<3G#MB>M9/+>D 
@KNPE4]V9"0<;6?]JFI=[*YQGA5AY'A'GV52,%@X3WY, 
@,[=L'%//W&0(:;!:D&$E,:5D_M_IU2C75]CV?ZJN*L, 
@3G29?D@"[Q^$S^1?Y86F&G?=,+M29X\']'R?O7,7)"0 
@9=/A\__AI_]&WU*6=!1RJ6WNS3%Z:Q&&M4FMD"Y^#-@ 
@!&HN\<-;8QQ\YB-AUDK6A\HCOPVTY= V8U[<"S)ZI%L 
@JP0/#<+?]TS,T+QX;E7+]257+:5@*P8N?(PL#7!*KDX 
@1F!&:A>B5A3\0A1CG WMET_@<?F]NU $0'$/:O+@M5D 
@'_:9\"'&XHG*8+H_]YE;<E.8NF\V@B"?0U#PQ@?"3T< 
@L4Z];45U^;,0,/JHUPD#%9I_.HFAIO&S^M#@G<&4R8D 
@R/%:E<2AQ1$3U57Q"7B2 :)<WRAD*!ZR/>&%]TY#+B< 
@19I]W:SH/G&^8@*W"F9O##S:"!+"U$:XSE^LF1SP ', 
@[+_T#H\,J!XFDB0N:1DZC;J<W-VX:MG&GKMDVS"_DS\ 
@&N,%;+:WC#?/A+7^SV4%Q/\B35[O2N?+V0BHUYW1/.4 
@16S*'/38MCMTOFC4DSBOE2!DE_J-H8S:.>LI X-6F'$ 
@[D^:O]M8-F40 *D=*@]6-!D.00$GS1@D\OP@0#+7K]0 
@<Y8IQQP7*,H$>:((FO<9&LZ1R \)U:$U0N<D^R;D%!0 
@Y<L!H &P5Z"#7Y-M7&21H1)^4@&?(XIKNSBHN,+G@)L 
@Y.U!<[.4QO$';=BB7\>=&3 3V&]G*[3]59"O;V"<R$< 
@1(C5N62LUZE4T&TJ5HY]Y88'550;IJCG'3^L=4D=UR( 
@M"RRB@\9G?)BCG<0)+6(M!VJ45=Q@Z#?!<T$N;K25A@ 
@'\>SIAF)#Q^JO8_NM7)>^S>@:+1_H"N$T9S,]<)@,48 
@#;P''Y*U?O0M1O_#4.V^5+GK*O8Q][;'RCA779ZA"A8 
@X#"YDE2;-E%[ QA>%RHKF=L'?F:*>,XH;*;BQ95@HJ, 
@?0G#X!/J\W+Q5,WBVGW19@K[S[WRUH=$S12#,+%*=^4 
@BK:XHZ<MX][B66/#[;<[>I[S9X_60V=1;Z:A=;!IBN, 
@&R >#C\*4 I>A(Y .&FBSA@2.YPFW62B<]")I'$]D9, 
@ ZQ==T (Q+P]@66J:^FSR #S"J^>I-@DF*9USRRXS<X 
@C/_,\C8SY\1'I<9G#HL8\IA\%5I%#YZ+))F_989G2A@ 
@FT<"R*(^E %XW<K.$_DKR7K[%X6[7\1<;$4_,%-W+;H 
@N>+5IL;7YSN2\BAHXJ>SAJ4\B7!ANR15Z-\MUU'Z0]D 
@>M1?.P-^LA<N(-Y6)AE;?9#$4B*RRB.7[!NV8^1:S2L 
@R?40KRZ3!I[93(MQ@B-(.R4OL^(+[G49UT%2#,<#2%D 
@5*(V7<%D%K0N7^/N-B1118=W&EL[F;6XJ8_8D;UR'Y, 
@XX^W@VJ_$YDC1H7#;;)P!<2H&MJIT:C/5-W/\NI8\   
@"\H?O="&C?J:M=-/Q*./[K]L H#202D%R>"P>:P%A)T 
@ Z]K+3U*;9POHWBFT$TG:6U<WA+<%&NV_B!]UQ^$W?D 
@B44%E"^AG8,6^,L3ZT9C0+1+V1NS4;1H5VNQ6#I-3<X 
@"@PF!N%*K*!DO-N#@741\4^H'!T8XG&G?IY>'/LV' 8 
@T9L]@%R'=:4@5?[SS#@E=,@%7D.IPUZQW;HK !<H1_, 
@[&2_I:^@.(;3D05IB,9A'F3 Z&Q$+**\+@Z_@:F!6KH 
@.Y0-_PN62+8=BI5;99#Z"6&LM"R\,B7+PP>0+'6:* X 
@#DW19>[OJD3_)K3ACE^), H[HZ:RVY;Z'0?@"[3(PD\ 
@\MG1S'.I<7WBVN?4@%VM]T8Z=&6]CP)$?>[YDLEU;), 
@_6HMGY+6ZK.!:QW6NN6[WXEBDI=,$=Y)TG55?X58[MH 
@(7I&@"LD&#<9L&:=A!=GT,F U9&>@M#S;V.CNQF@*0T 
@O:W.G]L>N<?A:YWI*W?LSI'(N4>"ILK*E:+EIS.[8FP 
@3'GO3X"]9*"<"\(:H,GHB$=($]TZCSVF/"=NN1U%J-< 
@'\R4#+ZG(($5B,]OQ#Z*-GNXO;K&2BFPWFI,CQMJH4L 
@8!N0#".1D4)\OAHDB87U("\W NI992/WZ#,3M;JVF\H 
@^K5S*B"NH]<6AR[C-QFLN,1,5'%,(6SUK7G&')2!>4\ 
@L^@ZFF#=A"ZN)ZSB^X8E?G'G89UY>FOZHW3S^<[E^L< 
@DT>V_SB]@DN-V?J5JLCIIX0IC8[\0E>)^]$2M4SK$N  
@H2''F(2V'N//?2S$2GB "LJ%K>='I\W!S0^KIP<8.,X 
@1IOD,P[>3T#*#W^TI#0RS@7YH3 HIOUWGW\H+Z ,*'L 
@"%U,R'F==!^O:W%K7@)KT.[T2,.([X^T^;I6!7ZDPQ$ 
@Q'[C%N?*6OE8LTGNJAX8<HQK!QCR7G *?CP %?Q#BO4 
@TA+ :--<FC.4G5BFLE+^#UFA60F:A6E]#I0[]0B><KT 
@31GP"IL2*$CTQQM 6#WKT8$LJ!]]YZ;D DJ!O4$>>T( 
@1F\ "[BN)',$\4I[E3)"&N@$%L'1HLJP^E6R]Y3ZC10 
@G &#5LUS;.#W9:QS?J[9-8FY2V:QVOFM>JFU6T8*8MX 
@P \K9]BH,O!*A/B!DXQ0FTBQ.4V.R$)M.D=&=V PNO\ 
@\)0"-T( 9'VM$569%C'>;&F>5\^3O]F(AR=(.]S5&BH 
@]>X YY[P-G7GYW"Y+C&<B 6YYRE :V3W.2XX,N9+P>( 
@['O6;U76,U3T8F9!QOK"6+F"I0Z(K[BC;:R;RO$';X@ 
@OP704 $M^(G .B0$IJ<^?A+HI%&I$DY]8->S;4$1-K0 
@F_+F^]$=FERX1:C=UT_GT0"U8(K5SK ^D6OD7^?/'RX 
@UG@CS3HGM=@7VU? (&&4.X7%NN7^X*W7VP9FQ4=3,Q  
@\UTBJ(SH*_O"'YU_3/E1I!PR<L48RP2]R,(%?C!PUL0 
@67( 9]\5*Y\GN#>73=_<]%YP&QE?NA8ZNQU:B\0SJ;H 
@EF5YAU5Y.$7\3->CRC$B<FT-.)^(8X!FZ'6U7.2+:Y< 
@8XB]?&>D=3IL#2T(P0G]$''[7S'-2[: 7M3.T[6#JNH 
@6=\7K=FBZ#;RO5&Y4O%6KH>8E?-:(EJ?E%IP*:GID/4 
@A_5H [#+8 J/,*5@?T<U$;4-I76=3T?XV8E?8EFM54P 
@M]+OWB59;5'O<F&EPI1K0EQ&X:.516>NP56J;&EF;2L 
@6?-1POU;=?4!<0S?\%@G!6_3GKJ8XN$FJ$3L-YX"]4, 
@+U,#V@E]>G)_3]?*E-T*<KW)B$.]Y_EGB;0.9RY!@W, 
@"8.&"#S0 ^ RJ2,!H4"1X?A5SQKLW4K].9_FGXJ*,&, 
@J5)J<XW(N.-.%Z+2K$97$&$V^XL3NEA'2%D&$+(,B^( 
@_^ %WBNMVHW"ACU/<Y_^KLUXC?J=XC5OFJLR2R5=T<$ 
@&C;!BQ80G:E1QM($?@!$BM^YS-@4:1XM6,!F_RAFD]\ 
@3=8DMI%!E$VP24<_: 0&KW66UZU)/0C_;B_6"S"WQ7L 
@F(0**R*1ZSFGF0__0X,II)?XTD=_&^LOLZ\Z+J/1WL4 
@Q(KY2\B5VVV_^N=I"[64-6C\_$P3H>8^GD!JO*/Y;L  
@@\+QM+(8X;LZYVU%OMILT1)::^/IAR'==NP)-D!C^O@ 
@,]8/8,CN1SO__2BTL>(D5?@$E7Z_E@5RIH=2XE5G"CX 
@C':9)OO'##+_W3LDFHNO](2'!5Z<=52O1M0FU[@-KQL 
@\^;X%%-(U42F!@Z*E0R2T5%Q$BL ;_S6\R0$G0Q8RMX 
@<)+%(21FYQ/S.I\I%UYB)<3+B/0J]"9?#T1'\)2F&P  
@VT*32X4_.M]FC$$GS\ F4#ID=?2 (=SCX!+M%\@==5T 
@9%#ZJ\?&A] GALGVPNJ!4G1BZ5T/Q1F08[0+UD@"(.X 
@2R_G(?'<W<*^\!G,M$8O:<IP$T"7]G<#KO_CL[@=%(< 
@6'Q<N^["+?'U9O=RXI2YHH#Q["G+6<7 -JW\]U(!-_$ 
@ R-%B!PQ=BH<^X#TA""<3+$%1TF5&P&?IBMHJ:".D*( 
@IC[DE2/P]V?D^4$P</:AI9LZ[_I^'X"O#;MHD3ET:N4 
@GI,S2+V!'G6/!T ;G__-U:FF:GIAQE/=SI=:S:[?0PP 
@9U@@@2=>4,L+ S5#LJ8<:'4]&R2;<I3R:9EU6[<O38< 
@HSP<=A?PKDJV/L' +YE8DR7YJN-YE,VNG5,@A2Y6IC  
@*7]8_EHO:/4<TGE  \24WUMPL3*2+I?(E!K8EWV]\,\ 
@._ +P#K A%][0^<B^LT"CI3CQ(8)32LAK"%93B/+KW  
@35[YI \/M0"M$-&'G1&@#AN17>S6SXITDJ-C7>0D6T$ 
@):%81PXK_ITGV2=9^*U0442% [C,H;GEJB++X[,@%/L 
@&PJ$P\H#)%)5GR.K,J9!1:@9=QI#]P)NWW3"O$L>+R8 
@%F; U1WX;5KN%*$UE8#_?%EHO #E9.VBJS@=(LZU]VP 
@_VN/0(-IBX;Y<VW()-)0YWK*X!O$%$I4 +D9_CJ3YJX 
@3(-1^"V"\6WAQH@A@Q6"#UU KK4I2\<)''KQ?,(5,4D 
@"6"'_8_&-"XOQ5M,ECZAAEF >+;^12"Z#>LZ-8 Z'$H 
@_U\@*,,N&?(F?,P];Q:4Q4=?V&X+82535GPSCZ3'"6T 
@MHT^LE#F: ->1(D%3=0])>1[XF&.P[BEUFOC"7_HX+D 
@X&N>P%Q+C+S]*8@VB.*#1"=6;2)Z\*!?Y$&&^:<JZ 0 
@- /<QQ<"M(\GD,EXGEJ4%6V Z^  N7+'-^T+Z(71[$, 
@,SD,1=ORB\#X%'93XV^3\$M20QDA"&E"F6LP%P8N2=X 
@(R[TZL$K)2=G\<.<12%^M(!2?=]V6V.Q%3L&9IZQ&X@ 
@B_U)\/&2-QJ<*0<-(]AZ4(LNVT?Y3W0.P45F&I2 GVH 
@!^=C.YNS_AS$[#%@2&%X1,*"W;2.96I+X)&.#;5YQZ, 
@ES4ADWT=]Y# O@=;0!*O&\QM28]9,<F[K5)D#DW,60T 
@7Q.9?F%BQF0O,D0S"Z"FA<^$[8E_LL+H7-\D0WB;(K\ 
@RTSVQ7(8N)4EI'6E:E^OK4<HI@&.\<(+8H?5"=L-[R@ 
@8QXTW<\%E9\<\XU7;4ZJ%OI_J]B8?=K\VV>4LV<EIWL 
@Z9%]$WR\*KNYJ^)<#:]1=_/>R*.C109X7G%%ITMSY 0 
@'RE<A+8<Q.5</$Y0@6D+V\L*+^@>P]&[AA_CQ_CZ%T, 
@;\^.<SH;]1,]E;/+U+@YDC15O;XN%UW*98&(IQNEA-X 
@Z"++[2RX;2OI+K8O0MJX6%A3,Z$<R:VJK 2K2"-\H.< 
@XP[$4$>*+4R+#\9&*=2-MKLSD)M(;O5[L.S*B*\V#@$ 
@:)WAHKK$EY+3R_A7 #QJJ%'.C H[1C4HO<"NJ*0SX#T 
@B',,6E</89]$^T1(J;@4QP&NJIOXI'#\$7:.)(./N4D 
@,J9QY@R9UKJL,3X+$H<-(A$/Y]BO+K'D0,5160G:*UL 
@%EUOQE.F:4%]-D:6W6AH531C<CVF/!3*TWU_UT< #>$ 
@H!W(,LAC&W+G8PVML1./Q;QR$OI0V"TFN-VWR3T* 7  
@>Z6[KKXRFCH=AU2?\:CN1#?H+M1*LZJ549P0%JEB(\< 
@(O+.7OO&]+4TG+40L?0Y;EX!FL[5WS9-%5R@!XI AN@ 
@C\L4]UORK<#+B#M@T@RGL>/CA^$!Z,&8EENX0 (D(?< 
@S-3?X(KH;LO$*N@0MLEN0#@-P'56NR_PI$LI6('E_%, 
@&O-MM:;N 7[):P2X1 @/N99N;]@8_R1QU-TSKJO $'X 
@,H%<V^JM*16ESII$#E8^KQ[?4-PM2*W3@D]"AZ:-N>P 
@+(>TZN[(N+FJ"_$8,%]Z!K+R5GZ\]2=:9K)'U- L204 
@MM0]<U)9*P*QJG3UK,$X$/,L3NH4=':^BLP<,#,]7!4 
@2%:N<3R6[6ZHVUE>UWG,?9?67K/&O8"\W?96VAI'@[8 
@T.3B0O7H)_^E3PJDEK0^[A-'P.!JE7V@+I=6<[E\-N4 
@CMR\?$P43X3U-]7A)UT23+<PH ].OC"]GL4ONW!Y:*X 
@2RL<0XE_$QH.M+M_AA?@WSJH4)^"IYNZL6:*2I],4E@ 
@->B9)%47A6)S*7H?4QA?WZ@2H1=MKZ7U/'=9[Z>/.7$ 
@>KJ\%H0 _<>(\$_(M>4<(9B3%N]K$?Q!G2(+2EKK6&L 
@>YAQE:!=T@RIU!Z^N>MD@V'D)0#<04XHS\2'8T=*/<T 
@GF=I*.*Y4Y5_+Q3F%Y,L,K/\%K>)?C7M*J@A8A.Q'@8 
@*7OTR$1RH=5R6&\<%547I(^J?V@[:/#CC_!Z"$<*#H  
@0;Z%N=AH67YMHXJEZ_QJU1$5/_U94<H,BKD"MU$K]VX 
@:UVZIXU* I8 HZR_BRD5V(P@[W7ZK1KV/>0)3='"UE$ 
@>:O&?&>P]MF49O_L$0F$4D?;(@2[*U0!8YQF5<V.S0( 
@!TG:%"7&*KS%0)PJ%2!=PO5S\0;:2WHD=-!L<D5<2O@ 
@TW5R!]U!,K0+ ATR8$?,CT*,BCP_HE[U;,"JO%3)+3@ 
@7D[XJ-[EGO(1 /GDQCR)?W?K1DH+_J%8W/*OJ:O&6EP 
@>'T'%C=""(/. @-A@[=R'12UK48OZ&U#MH?I[$SE$XX 
@(/ ;4[NFJPDA"ANDKS0 <O?-HU3;/!,RC;UFF#7X2?X 
@KOL?^ %BTU2HU8P]Y'I6&X3-,'QG0RQQS$J4XN0I4@0 
@Y05"K_8TA\FEK6<@;]  ,$#$RH?D'RX\L#"O6R\S0+D 
@>$Y(<>6$O*=M\H0_^M\KL +JQ61VVS4Z?FBQ$-B%62$ 
@3+><)WO"4^B\:<_$:FJWSJ]_QVW>>* ZXNA9I$S&W < 
@1O_L 6_#W'?8U2JV1*B?/M<"4<Y!G:,)R;]4*R0\4%D 
@SU;3%:*$;E?W<AO<8$CM"PZV\_ZQOJ"\XU_/]9E>S/L 
@B;^-A:WW-[0H3X*+BY]75&X44#O_,^,_[:U^$"*!]1D 
@62HU[Y4NZ+@2^B5[KW3H2(0$H,8)L(H\EH&B.TC):I4 
@LK]IS)6E==$AGC0OQNZGSDA8ON[ZTD$"UBH4C&(&!94 
@O((ZK_VDQUJDW<5@'@O+F3@5*($Q+IH'Z@H>P@)FS ( 
@QSDEH##%3%.K>(L%"@L,<CG4]:E3-X(-#YM^(3, C/P 
@M^()0@B(B_[2YE['M^V\PE6NQD*;P+.X$T5UV.UWG=8 
@"X2::+T713\"AI% IQ7"74U9S /X12R7;C&40EH[W@  
@23S3VN>1ZY,:K>7SZBBQ07CPU$DR+O@P&,\]7I>[YRT 
@6587,>&9D/8+?H 8$@JWTM/[:+E_?U!W?:/.*A+Z>J$ 
@E>$7[[ZV8:Z@J@?:F 6=QH@*);7Y_G)\4GBS,K93TJH 
@UAK/V\-X,_K>%B^8.1\=T$^/;FUE,CQ1@.8EE0BCX/  
@;CP@DGOY?Y^S(9_7!#Q+M$UIQJ7U%\ZL2-KHO4CP],P 
@+=A\I400_+"AZF^PR]W_?B#?AL5 H)9P8ZR0*-6N8LT 
@2=POB+ZRCJ5N?\3ME-$5RQZ]7PZN'M&UYG=-33N6[+X 
@*->D\K?/2K/_HK?IS.FB7"XI[2F<,,N3B\3)%&TAW\< 
@KG2+4D<4!RMWZ4@91OS!5NNM7UON2$[3>G@B#5D<"QX 
@S]$<PLL&@#CP)H.S!%ISN#)$A37;%"=V1&WRB0H%VG, 
@BS<=%E"P4T?;<Y;?]S?0Y@ZQ2-!>!I?_HA,0__BL.X@ 
@:M1D]C[=P#%/2B&)L^X P]J@0AA/(,?Q509+AI&8,0, 
@K^RMR<M^7>2DPQ_VAAR=>'\'S3%?SE?&/? PP/!((T@ 
@^@N\#F*A]UR*ZJAI6P9^J'$B.(DGC$U,C![PTN#T^OH 
@W,Z(W1##9])7L%/J,.&-3VQ$!OHASC_FMV,_F')\+V( 
@P5%%BQ7QB7_5:QQYFJE88]*H[ ^@UQ<L4L8#9'/*W?  
@ =*#HOK=0L8E>K[V^F@NHBG*Z>^&?F-S^<O7KM@._+8 
@$/?QQKK/<^TE:$8CKA.P&AIBB]<[4(<Y3ZS4 /SS$?4 
@40V)X[B@.:L4S]*C?8L&8:"8C.$-=%ANQ?L>P,%9P3H 
@"GW%LB\\5>W;.@]3V_M%B&.[82*37M]\' Q[UPO-CBD 
@U\Q+P.JTK,GUOTM:_U]A NI((3#B9D&K5Z=&T[U.4(D 
@T]PZ^ATJ\FF!%FL_JDW/%:?M:%@'TQ)("24A>UD"H , 
@G*+P2AA;:/I:B2#XCYEW$M85&IQJ[[[O!\Z K?/1N=4 
@/CFI"66EWZ8)K_:6^.TE,9S)BF'2(E$\TI#>&WQ2=6X 
@AJ#?E?ENNL4Q.MV(M8]0@_Z9:[*NE/P'WE4S,$QJ QX 
@1X@\1,FTL/9EV>:6[ZS8_#O_U*]+W=EP93*I;M9N,P, 
@*YK=$(&VCM)-E?8K]S;09,N%&+C;&:C>5D)XZK.9WG\ 
@29^(@I(I'4 *IW-8'I.I92KS<I+"%/_&Z7R2A:GJB78 
@)G6?6JX42(\=+?BMC+$"/+#J^:V,O')2+K=$]4_T*GP 
@!>T@QZB>?KJ !1V,?VT4!34L)AL26=[G&-D<LZ\"!XT 
@JHSQ.^;-OKIS ZS(Q9)>!NAZ5$#Z-B28($;V?>[E?I, 
@NC;<7_U]-M?-&?O:O0[%PNQP^MQ[(CA084(97[9U);D 
@0S'%T-_%'2[0]$6AH4;;H:AH?:AV4:+@Y<*.E9A:YR  
@\OBMSZV08' [P3; %/V0X;G'L=;O.SCWUR* 0T1C4=P 
@DO?_WS(G9856CWS%0D5"'E./JK&]$&08SE$J!;+*@>$ 
@9QH[- ?DTM ZP=,;@EQ::.O[Z>ZUU5A]AN_/*V0#@J$ 
@UK,1HZ@_2KO6VS1CV&2;.?;1Z)<!@75,R1.QWSD%NN$ 
@NTOP*H1(2>U9AJUC"3P;V0\?EQSKFLURSTZMT>C/W4( 
@6"?3VG[HVL\OJGLL.S,UH*<FK][\MM/B.K(_LG0Z\]X 
@J+(5.M$Z \"E:QE'@R_5A[\7O\A6$;%_DPT24(&\V@L 
@H7BJNYX.+4,@F6UI5@X_";T$7)&201B'?[.1--OAGXD 
@(CO85H>4-+B6?A>?-=-%&!,I/R']F\"OT?A*5.*\PO, 
@.:O5?[#J4D9/'\R+]7A/J(V,F@ -T(XNC%7,:;)6',< 
@0E'0D)%!06A3:DFEZ;X'EU; R"JB7#Z:$ 3Y9BV* 3P 
@N$>:2-3G'0.M6MC2EJPVG7=RYEHD*=W+Q+V'/6!2K*8 
@;R3!"?.Y+ *UI[D';4R<:G\T)K!ALI#6B4K">0@FBZ0 
@K^LTU]2U,FX+@&0-%*Z/%W*P3C44LN!=CO3J5S>F=$H 
@H*181],-S^/*I)6T\(4#P,0NSFL*AI9'K-:_?_$.CM0 
@(2&M[S'-?C2 ]YWH,B?_XM6*_MH*F"DRFIOUS:$1S:  
@CH;<.PO%WIQ)$UX\==X@4I^(?K_W\^TL3U_(._[&CJ$ 
@Z?0FD3?:2G]Q'H-41Y=7,W_D>",+*QJ.SGTOV;)Q^*X 
@&PXV_)NMWV&B<%6O1T=U0R+.:5[@1>-L"K8CMF_5FZP 
@IYZQQ8#PR#*=Y@L66:'I&$[NR&^\AZXCWLU&:805\9$ 
@FTD\N]8'CSB:K)K"(;HK1"/^\F8)5M>JVU(?\TG0ND@ 
@7_5R7HM+0C$_D?/+!]0^T\MYG3V%P53O[O+ Z(O/V'( 
@FD8*AZ<+A94 Q>G/G==Y'OX5>4,H>EMSZM-6FPU*![L 
@3).5_&QZ;WADQZ7LCW*PZT#0SB429,3>T/!31K?67YL 
@3I64*V1,#,+/_L%[-%C$RSPK00GX..F)JDF2WBP(DS  
@Y4XMO$^"^;:S,9[1P*R%"([0^^/<)]YS9F#51.2\3(X 
@:J\24%1R/P4F;5;#9$19"?$'U%!4,.C7LGP]?C5S>P4 
@?<JQ)I'?A^1= .#L@34,Z^SZ5B%GI4_[S#O_J'[SW,T 
@Y6_D5()FS?@=5W(19D)N$(?11;=^Q>I/Z9[OJDQ%F4X 
@DZ:<D/!_(=U*BZ!F\>"3279N<#C53@O]P9KYV4PBD/H 
@/T)$:.+T\Q6B2;IY)LBVTK=^]8ZAP!WA"-*=HJW6O<@ 
@-%D=[Y>P6 _R7\]I&AKIP]_8*>(E$H1:*+#G1HFS7$H 
@)WY_5'L.4HS6.'Y,(Q?YH))46)A,<D-:QVDX9N/:(+, 
@#%E+;!64+%_TMXGQBTAE;2MA0X9<TZRZSS:="QEV*', 
@>QOSMNI9[=+N(-J%<^I/F,+$HGW+P<XG9R;(<]6AT)0 
@OE[DRIS(1PUCE>E*DC)$]'4LC:;>P6=+Y,I\1,"4&U< 
@=;WJ:<>4NHN,D4Y9@BTAJ3AZ/C>BG94VQIQ_&#/!_+D 
@0G/B0NR@"L3<LIS/F@ZI0)YG=?VC>9.";M$5!>XAK&( 
@DMX'<33DG#]XFL+VVNCIE\)5@?G \W;!=M3\-,/^."L 
@%G(MI"I/!$9:*HFF&QJ\=F$4FP;-ICP'F;RF>@I<FDH 
@A/>N'_R%N/.'*?;0'34($0"D4K2H6[=A]\N)?JYM(<@ 
@_@:9ORYE>TYRK"@QBB3\OXN4L;)9M@X*_4V%C-4>-*D 
@1;"[U629F:WGA7]6D:!P6JY'@*%,&5?EOM& '%&Z?<\ 
@Y3I)NT9U%D!\D*&CM]UDLJ\P$DU>X.S0%X* CCNJ]C< 
@BYH/8V"<[*WH7H+BF+O:1'=/Z.4>XJ[J U$N+-0W"/, 
@S>*\);0[LRB;:M*THP-U=,6_D'](.76S_353!SPJ3Q$ 
@(:(.$]E3@PR_#%YWY%ZF$+CW(P68(S&!9N6VD72V!3< 
@F[H97.LJO%:S=%L.<NH9GB L/[&C*9= W8L+[<1C,8< 
@83!VBL3L9W!OZ+%>7V?!$K8HL&B<5U@QMS4'A (&Q:< 
@*OKK K[LZU CV(;=U/KF]!7+.J%DL96:[#VP]0"=<@$ 
@,Y?2C4$F]*9P=M1'J\GPW4X^%7>^OM0\1BDQ,N8ZA?4 
@R5_V)0GK5]9+&YN([KSHT>-8/(A6< 0<:Q8B8S@\0!\ 
@PJS^G.>]2@'8:UJ9]$!O9F.0G$?"KF:'  HJAWFGQ(X 
@Q-B@[R1YHXIQ+U!>A7 YL""UL/%E!8-]#2\)0*)J!?@ 
@WXNGWH&>#65SLT1B9#>MML8U^\XKAS,7KP#*?2X-:3, 
@N4RP2];4TFXX#H6D8&* ^B&#V"V#LJ 1$'=FK-FC0I$ 
@( :W2G8FB<8FP\T]K+&A48B(1T+-/!88-L_,W(Q.&Y0 
@$MTUX$H1= >FEPQ81&:$G!V^N7'GQ(RCQ*)D&M)=T^  
@0&"1M=1Z3T!.E2T,ZXF#HVUBG(478>(EA@&=9V_Y4!( 
@?KJB%BEO'DLW:[7NR%"BQ GQ)N^4K4/=@5J-8^OM]1( 
@P>T3FKLA5@0P7 <(7AQY'^[[,T@GMLTX+&OBD+(#T2@ 
@G$_Y_9+K=UUXM_<2!=2^H]3(+K4ON*BJJWC%5D+RQVX 
@1S59MH_.J^!L'!J]XE."[0V&/H6N?74@3#1&%]C@DI< 
@;5[P R/2T*BC[ REI]_%D.H%)M-#;3;$S$_VP(]Z?3\ 
@^MDQ=0-8/-7. _H>[@SL;N.LZX'9:\$](Z?1]?00J&@ 
@/"W?VCTO('.J-Q0_6Q3_S1I!E4#@8$,TA<&50RWO-@@ 
@O>"C.-X +?#G[ /(!,X\CBGWUZ""AO.?N.L)\I,\? T 
@UL4#C5;*:S@'!7X;N1CT*I$3X[57CF:[B1;(#.Z >?( 
@J5 JN&2C>*2LE0@<+F/_82GD:MEQ+BCWJ 3M7XZ9U0P 
@>!'3',_.L:MC'6Q._AKL(VGHKB1KD:< YY6NOWOL;F0 
@632]:;BCV,0FNU)A2N^<PTH,3]5LE)W^&A$CDZX'LAL 
@Z^Q8+P1EU7BSHR<S%W4 0Y/RG*J_.[_B@]Y'[M[6/OH 
@+E4LKL'Z7?>':^,]]%5<F=.GS&IWB8HZUOOH#)3IE8@ 
@X^W?7A5<QJAEZQPS9\U<BO6='-EUJ=UNC"GO:;Z7\VP 
@< PH5)_4EO24CM<+&A!DF3 _CP.H.%("68S$P'2:W@P 
@2_W6@54H4"%K5P?E5ZOE[X^ 6!&Z7?&*J=FG-("-K/D 
@A.:<&$5 R!^ZERIZ'5E?#Y"HO"_=?6^NNO@"Y+ZVL1T 
@<J-"I:G3UEZ..#78LPQ,5OI)9L&LR<P^AL#'75[PGDH 
@U@Z3KVBTFR% [)3I,73^K-FE1O*-6*ON0RL+:=,():  
@F/*U8N^#N2&=AXUO,"+(8(9GRUJ"=$VA>HK9'AT9'_4 
@&L8V0]/]D<4!=7Y9 $IRFSO@;V1*RX<;.D)Q*K-O06( 
@:5V2'VG\$LK##]*ST]V(<V%L% __%D=CV5E-.<5=W,T 
@M-]=S'DD:I2_8&$\Q!(NT3/F*4]B/3Z@IB4F!2$%UXH 
@5CW8^K/IT/P9GSVYR!3J80+U^@%HR^;16N:U]2ZM(T0 
@ZQ[O>CU3P7O-#GG!:6CXZ50^N-;7,NP8!9GG;G;>%8$ 
@Q6"4X;$_E-S%W[@6JVR)]P]UL[#Y]7;@R7 EPHPU*MH 
@ N]B8T,_(:9#1U2X.<$$\/M?-'.9UKL4Z9[#S'W09U( 
@!D&U@=S:8VU^';]^];K6>!C3:+DXFD3*)E_N!<=X0/T 
@;8.'63GBLN'I1&41\MSHLLI 0\1_F%;J.JE2^4+R_6( 
@XUZ6V-R#9$:D4P!*Q-H6+'MZ?LUASFB@U-6762X7G1T 
@RO/E2_17JV(&0FGQV'U@65#9)\HW?M,'"]$]C#A1'[4 
@C6&<ITU0ELCHFU74-U(@+U8JFSL^D6O^)'K O3+29@< 
@&CZ#<A7VQHQ.%8@[WN[8YT PZ&'B(JT24.Y$%5:KS3X 
@S,=1%<%T2>88!L=IH;?AOK(5XG!E-6@*K+/G@!M.^?, 
@#_W[#Q'L1VVQZHBOFE!!P#$O";L-!B2TOX/]\FS;E38 
@X</<'DID)A71W!,%_'<70<E$*G7MSF$^*W%M[_]FF#L 
@E4'34/E&<*'89A73#"Y%H(+KB^.'U61_:6?1 A<6:3P 
@_W>$KI$:D>K#<(N+@L8&Q6ZK(;C_.L1DE9OFZRYIE#T 
@0=#WJ<$<#U9TJ/<TRV[+!I!+L7(^\Y%T]8<<FQ 4H9L 
@OIA8]G++<[2@;HDN?]?0^__W0NO*PZUES:P#]\_8248 
@$9G"X/GGX):76H;)DM&*V*O<3+T!5: ",7E995<YT$T 
@*!]$>:7#"UD>)V'1*2E7$M>] I)<C"TIM%5Y;;0ALZ< 
@8%#] ZP$ )=^#P RF7S1E#?(L3AIJ]<8&+%ZG*!\L3@ 
@V-0[Z>U&#\6!$7M!0^F>@I5)'ZKJ^ZF0 IHEZS3E:=P 
@5,(8)6KTT55D.4P;F_AI"0_;C2K'VPC5]BW0 ^P78=< 
@2N>?_>OT(C-W"'U?\J4U)#^Q4I;F-C/F[@H:N$;&+2( 
@5B3ZF$:'O^1-)<I[ZC\AR/SXOM1A&84*O*^Q:KHUE[$ 
@'*5'L)6,(]*.(GEI"[MSVUP<,OH\2<^C7)';>R1;:8X 
@M>J6(":^[(\5O")QHKI]99_\%[+M^)C-I<5,X&&*",P 
@18]S3[EJ.QE[]J+]AB0I-8/)'(!""*VUD3UGNLS0L-8 
@FKDQ9T;EIXD49[/]ZSK",2)?/<MJX39=]W'FT'/=[>, 
0Y'?=_2A87H%)/C9A90:54   
`pragma protect end_protected
