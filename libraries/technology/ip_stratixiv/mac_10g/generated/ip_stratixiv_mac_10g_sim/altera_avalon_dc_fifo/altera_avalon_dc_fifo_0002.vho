--IP Functional Simulation Model
--VERSION_BEGIN 11.1SP2 cbx_mgl 2012:01:25:21:26:09:SJ cbx_simgen 2012:01:25:21:25:27:SJ  VERSION_END


-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

 LIBRARY altera_mf;
 USE altera_mf.altera_mf_components.all;

 LIBRARY sgate;
 USE sgate.sgate_pack.all;

--synthesis_resources = altera_std_synchronizer 10 altsyncram 1 lut 39 mux21 10 oper_add 2 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  altera_avalon_dc_fifo_0002 IS 
	 PORT 
	 ( 
		 in_clk	:	IN  STD_LOGIC;
		 in_data	:	IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
		 in_ready	:	OUT  STD_LOGIC;
		 in_reset_n	:	IN  STD_LOGIC;
		 in_valid	:	IN  STD_LOGIC;
		 out_clk	:	IN  STD_LOGIC;
		 out_data	:	OUT  STD_LOGIC_VECTOR (15 DOWNTO 0);
		 out_ready	:	IN  STD_LOGIC;
		 out_reset_n	:	IN  STD_LOGIC;
		 out_valid	:	OUT  STD_LOGIC
	 ); 
 END altera_avalon_dc_fifo_0002;

 ARCHITECTURE RTL OF altera_avalon_dc_fifo_0002 IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_0_u_668_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_1_u_667_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_2_u_666_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_3_u_665_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_4_u_664_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_0_u_690_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_1_u_689_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_2_u_688_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_3_u_687_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_4_u_686_dout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_address_a	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_address_b	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_data_a	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_0_133q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_10_210q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_11_209q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_12_208q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_13_207q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_14_206q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_15_205q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_1_219q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_2_218q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_3_217q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_4_216q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_5_215q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_6_214q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_7_213q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_8_212q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_9_211q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_valid_204q	:	STD_LOGIC := '0';
	 SIGNAL  wire_ni_w97w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_0_131q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_1_115q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_2_114q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_3_113q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_4_112q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_0_202q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_1_168q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_2_167q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_3_166q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_4_165q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_empty_137q	:	STD_LOGIC := '0';
	 SIGNAL  wire_nlO_w42w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_full_142q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_0_110q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_1_109q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_2_108q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_3_107q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_4_106q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_0_163q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_1_147q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_2_146q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_3_145q	:	STD_LOGIC := '0';
	 SIGNAL	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_4_144q	:	STD_LOGIC := '0';
	 SIGNAL  wire_nO_w1w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_0_129m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_1_128m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_2_127m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_3_126m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_0_122m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_1_121m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_2_120m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_3_119m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_4_118m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_4_125m_dataout	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_a	:	STD_LOGIC_VECTOR (4 DOWNTO 0);
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_b	:	STD_LOGIC_VECTOR (4 DOWNTO 0);
	 SIGNAL  wire_gnd	:	STD_LOGIC;
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_o	:	STD_LOGIC_VECTOR (4 DOWNTO 0);
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_a	:	STD_LOGIC_VECTOR (4 DOWNTO 0);
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_b	:	STD_LOGIC_VECTOR (4 DOWNTO 0);
	 SIGNAL  wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_o	:	STD_LOGIC_VECTOR (4 DOWNTO 0);
	 SIGNAL  s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_empty_460_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_internal_out_ready_183_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_116_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_123_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_vcc :	STD_LOGIC;
 BEGIN

	wire_gnd <= '0';
	in_ready <= wire_nO_w1w(0);
	out_data <= ( altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_15_205q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_14_206q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_13_207q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_12_208q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_11_209q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_10_210q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_9_211q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_8_212q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_7_213q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_6_214q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_5_215q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_4_216q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_3_217q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_2_218q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_1_219q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_0_133q);
	out_valid <= altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_valid_204q;
	s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_empty_460_dataout <= (((((NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_0_129m_dataout XOR ((((wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_4_u_686_dout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_0_u_690_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_3_u_687_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_2_u_688_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_1_u_689_dout))) AND (NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_1_128m_dataout XOR (((wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_4_u_686_dout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_1_u_689_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_3_u_687_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_2_u_688_dout)))) AND (NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_2_127m_dataout XOR ((wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_4_u_686_dout
 XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_2_u_688_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_3_u_687_dout)))) AND (NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_3_126m_dataout XOR (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_4_u_686_dout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_3_u_687_dout)))) AND (NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_4_125m_dataout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_4_u_686_dout)));
	s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_internal_out_ready_183_dataout <= (out_ready OR wire_ni_w97w(0));
	s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_116_dataout <= (in_valid AND wire_nO_w1w(0));
	s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_123_dataout <= (wire_nlO_w42w(0) AND s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_internal_out_ready_183_dataout);
	s_wire_vcc <= '1';
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_0_u_668 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => in_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_0_202q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_0_u_668_dout,
		reset_n => in_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_1_u_667 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => in_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_1_168q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_1_u_667_dout,
		reset_n => in_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_2_u_666 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => in_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_2_167q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_2_u_666_dout,
		reset_n => in_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_3_u_665 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => in_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_3_166q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_3_u_665_dout,
		reset_n => in_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_4_u_664 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => in_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_4_165q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_4_u_664_dout,
		reset_n => in_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_0_u_690 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => out_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_0_163q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_0_u_690_dout,
		reset_n => out_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_1_u_689 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => out_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_1_147q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_1_u_689_dout,
		reset_n => out_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_2_u_688 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => out_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_2_146q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_2_u_688_dout,
		reset_n => out_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_3_u_687 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => out_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_3_145q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_3_u_687_dout,
		reset_n => out_reset_n
	  );
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_4_u_686 :  altera_std_synchronizer
	  GENERIC MAP (
		depth => 2
	  )
	  PORT MAP ( 
		clk => out_clk,
		din => altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_4_144q,
		dout => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_write_crosser_altera_std_synchronizer_sync_4_u_686_dout,
		reset_n => out_reset_n
	  );
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_address_a <= ( altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_3_107q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_2_108q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_1_109q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_0_110q);
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_address_b <= ( wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_3_126m_dataout & wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_2_127m_dataout & wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_1_128m_dataout & wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_0_129m_dataout);
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_data_a <= ( in_data(15 DOWNTO 0));
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605 :  altsyncram
	  GENERIC MAP (
		ADDRESS_ACLR_A => "NONE",
		ADDRESS_ACLR_B => "NONE",
		ADDRESS_REG_B => "CLOCK1",
		BYTE_SIZE => 8,
		BYTEENA_ACLR_A => "NONE",
		BYTEENA_ACLR_B => "NONE",
		BYTEENA_REG_B => "CLOCK1",
		CLOCK_ENABLE_CORE_A => "USE_INPUT_CLKEN",
		CLOCK_ENABLE_CORE_B => "USE_INPUT_CLKEN",
		CLOCK_ENABLE_INPUT_A => "NORMAL",
		CLOCK_ENABLE_INPUT_B => "NORMAL",
		CLOCK_ENABLE_OUTPUT_A => "NORMAL",
		CLOCK_ENABLE_OUTPUT_B => "NORMAL",
		ENABLE_ECC => "FALSE",
		INDATA_ACLR_A => "NONE",
		INDATA_ACLR_B => "NONE",
		INDATA_REG_B => "CLOCK1",
		INIT_FILE_LAYOUT => "PORT_A",
		INTENDED_DEVICE_FAMILY => "Stratix IV",
		NUMWORDS_A => 16,
		NUMWORDS_B => 16,
		OPERATION_MODE => "DUAL_PORT",
		OUTDATA_ACLR_A => "NONE",
		OUTDATA_ACLR_B => "NONE",
		OUTDATA_REG_A => "UNREGISTERED",
		OUTDATA_REG_B => "UNREGISTERED",
		RAM_BLOCK_TYPE => "AUTO",
		RDCONTROL_ACLR_B => "NONE",
		RDCONTROL_REG_B => "CLOCK1",
		READ_DURING_WRITE_MODE_MIXED_PORTS => "DONT_CARE",
		READ_DURING_WRITE_MODE_PORT_A => "NEW_DATA_NO_NBE_READ",
		READ_DURING_WRITE_MODE_PORT_B => "NEW_DATA_NO_NBE_READ",
		WIDTH_A => 16,
		WIDTH_B => 16,
		WIDTH_BYTEENA_A => 1,
		WIDTH_BYTEENA_B => 1,
		WIDTH_ECCSTATUS => 3,
		WIDTHAD_A => 4,
		WIDTHAD_B => 4,
		WRCONTROL_ACLR_A => "NONE",
		WRCONTROL_ACLR_B => "NONE",
		WRCONTROL_WRADDRESS_REG_B => "CLOCK1",
		lpm_hint => "WIDTH_BYTEENA=1"
	  )
	  PORT MAP ( 
		address_a => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_address_a,
		address_b => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_address_b,
		clock0 => in_clk,
		clock1 => out_clk,
		data_a => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_data_a,
		q_b => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b,
		wren_a => s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_116_dataout
	  );
	PROCESS (out_clk, out_reset_n)
	BEGIN
		IF (out_reset_n = '0') THEN
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_0_133q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_10_210q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_11_209q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_12_208q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_13_207q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_14_206q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_15_205q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_1_219q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_2_218q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_3_217q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_4_216q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_5_215q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_6_214q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_7_213q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_8_212q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_9_211q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_valid_204q <= '0';
		ELSIF (out_clk = '1' AND out_clk'event) THEN
			IF (s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_internal_out_ready_183_dataout = '1') THEN
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_0_133q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(0);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_10_210q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(10);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_11_209q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(11);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_12_208q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(12);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_13_207q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(13);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_14_206q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(14);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_15_205q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(15);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_1_219q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(1);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_2_218q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(2);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_3_217q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(3);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_4_216q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(4);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_5_215q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(5);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_6_214q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(6);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_7_213q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(7);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_8_212q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(8);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_payload_9_211q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altsyncram_mem_605_q_b(9);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_valid_204q <= wire_nlO_w42w(0);
			END IF;
		END IF;
	END PROCESS;
	wire_ni_w97w(0) <= NOT altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_valid_204q;
	PROCESS (out_clk, out_reset_n)
	BEGIN
		IF (out_reset_n = '0') THEN
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_0_131q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_1_115q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_2_114q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_3_113q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_4_112q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_0_202q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_1_168q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_2_167q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_3_166q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_4_165q <= '0';
		ELSIF (out_clk = '1' AND out_clk'event) THEN
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_0_131q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_0_129m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_1_115q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_1_128m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_2_114q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_2_127m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_3_113q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_3_126m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_4_112q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_4_125m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_0_202q <= (altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_1_115q XOR altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_0_131q);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_1_168q <= (altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_2_114q XOR altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_1_115q);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_2_167q <= (altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_3_113q XOR altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_2_114q);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_3_166q <= (altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_4_112q XOR altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_3_113q);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_gray_4_165q <= altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_4_112q;
		END IF;
	END PROCESS;
	PROCESS (out_clk, out_reset_n)
	BEGIN
		IF (out_reset_n = '0') THEN
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_empty_137q <= '1';
		ELSIF (out_clk = '1' AND out_clk'event) THEN
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_empty_137q <= s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_empty_460_dataout;
		END IF;
		if (now = 0 ns) then
			altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_empty_137q <= '1' after 1 ps;
		end if;
	END PROCESS;
	wire_nlO_w42w(0) <= NOT altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_empty_137q;
	PROCESS (in_clk, in_reset_n)
	BEGIN
		IF (in_reset_n = '0') THEN
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_full_142q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_0_110q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_1_109q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_2_108q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_3_107q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_4_106q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_0_163q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_1_147q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_2_146q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_3_145q <= '0';
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_4_144q <= '0';
		ELSIF (in_clk = '1' AND in_clk'event) THEN
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_full_142q <= ((wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_4_118m_dataout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_4_u_664_dout) AND ((((NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_0_122m_dataout XOR ((((wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_4_u_664_dout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_0_u_668_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_3_u_665_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_2_u_666_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_1_u_667_dout))) AND (NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_1_121m_dataout XOR (((wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_4_u_664_dout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_1_u_667_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_3_u_665_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_2_u_666_dout
)))) AND (NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_2_120m_dataout XOR ((wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_4_u_664_dout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_2_u_666_dout) XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_3_u_665_dout)))) AND (NOT (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_3_119m_dataout XOR (wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_4_u_664_dout XOR wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_altera_dcfifo_synchronizer_bundle_read_crosser_altera_std_synchronizer_sync_3_u_665_dout)))));
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_0_110q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_0_122m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_1_109q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_1_121m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_2_108q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_2_120m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_3_107q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_3_119m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_4_106q <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_4_118m_dataout;
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_0_163q <= (altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_1_109q XOR altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_0_110q);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_1_147q <= (altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_2_108q XOR altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_1_109q);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_2_146q <= (altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_3_107q XOR altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_2_108q);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_3_145q <= (altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_4_106q XOR altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_3_107q);
				altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_gray_4_144q <= altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_4_106q;
		END IF;
	END PROCESS;
	wire_nO_w1w(0) <= NOT altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_full_142q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_0_129m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_o(0) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_123_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_0_131q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_1_128m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_o(1) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_123_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_1_115q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_2_127m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_o(2) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_123_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_2_114q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_mem_rd_ptr_3_126m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_o(3) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_123_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_3_113q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_0_122m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_o(0) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_116_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_0_110q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_1_121m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_o(1) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_116_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_1_109q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_2_120m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_o(2) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_116_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_2_108q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_3_119m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_o(3) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_116_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_3_107q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_4_118m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_o(4) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_in_wr_ptr_116_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_4_106q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_4_125m_dataout <= wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_o(4) WHEN s_wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_next_out_rd_ptr_123_dataout = '1'  ELSE altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_4_112q;
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_a <= ( altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_4_106q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_3_107q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_2_108q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_1_109q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_in_wr_ptr_0_110q);
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_b <= ( "0" & "0" & "0" & "0" & "1");
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117 :  oper_add
	  GENERIC MAP (
		sgate_representation => 0,
		width_a => 5,
		width_b => 5,
		width_o => 5
	  )
	  PORT MAP ( 
		a => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_a,
		b => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_b,
		cin => wire_gnd,
		o => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add0_117_o
	  );
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_a <= ( altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_4_112q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_3_113q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_2_114q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_1_115q & altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_out_rd_ptr_0_131q);
	wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_b <= ( "0" & "0" & "0" & "0" & "1");
	altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124 :  oper_add
	  GENERIC MAP (
		sgate_representation => 0,
		width_a => 5,
		width_b => 5,
		width_o => 5
	  )
	  PORT MAP ( 
		a => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_a,
		b => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_b,
		cin => wire_gnd,
		o => wire_altera_avalon_dc_fifo_0002_altera_avalon_dc_fifo_rxtx_dc_fifo_pauselen_add1_124_o
	  );

 END RTL; --altera_avalon_dc_fifo_0002
--synopsys translate_on
--VALID FILE
