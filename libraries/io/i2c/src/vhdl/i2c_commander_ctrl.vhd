-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: I2C commander state machine
-- Description: See i2c_commander.vhd
-- Remark:

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.i2c_pkg.all;
use work.i2c_commander_pkg.all;

entity i2c_commander_ctrl is
  generic (
    g_i2c_cmdr  : t_c_i2c_cmdr_commander;
    g_i2c_mm    : t_c_i2c_mm := c_i2c_mm
  );
  port (
    -- Clocks and reset
    rst                      : in  std_logic;  -- reset synchronous with clk
    clk                      : in  std_logic;  -- memory-mapped bus clock

    -- MM registers
    protocol_wr              : in  std_logic;
    protocol_index           : in  natural range 0 to g_i2c_cmdr.nof_protocols - 1;
    protocol_offset_arr      : in  t_natural_arr(0 to g_i2c_cmdr.nof_protocols - 1);
    result_expected_arr      : in  t_slv_32_arr(0 to g_i2c_cmdr.nof_protocols - 1);
    protocol_status_rd       : in  std_logic;
    protocol_status          : out std_logic_vector(c_word_w - 1 downto 0);
    result_error_cnt         : out std_logic_vector(c_word_w - 1 downto 0);
    result_data_arr          : out t_slv_32_arr(0 to g_i2c_cmdr.nof_result_data_max - 1);

    -- I2C protocol control
    protocol_offset          : out natural;
    protocol_activate        : out std_logic;
    protocol_activate_ack    : in  std_logic;
    result_wr_en             : in  std_logic;
    result_wr_adr            : in  std_logic_vector(g_i2c_mm.result_adr_w - 1 downto 0);
    result_wr_dat            : in  std_logic_vector(c_byte_w - 1 downto 0);
    result_ready_evt         : in  std_logic
  );
end entity;

architecture rtl of i2c_commander_ctrl is
  constant c_expected_bi_w       : natural := ceil_log2(c_word_w);

  signal state                   : natural range 0 to c_i2c_cmdr_state_max;
  signal nxt_state               : natural;
  signal i_protocol_activate     : std_logic;
  signal nxt_protocol_activate   : std_logic;
  signal i_protocol_offset       : natural;
  signal nxt_protocol_offset     : natural;
  signal result_expected         : std_logic_vector(c_word_w - 1 downto 0);
  signal nxt_result_expected     : std_logic_vector(c_word_w - 1 downto 0);
  signal expected_bi             : natural range 0 to c_word_w - 1;  -- bit index in result_expected
  signal error_cnt               : std_logic_vector(g_i2c_mm.protocol_adr_w - 1 downto 0);
  signal nxt_error_cnt           : std_logic_vector(g_i2c_mm.protocol_adr_w - 1 downto 0);
  signal data_cnt                : natural range 0 to g_i2c_cmdr.nof_result_data_max;
  signal nxt_data_cnt            : natural;
  signal data_arr                : t_slv_32_arr(0 to g_i2c_cmdr.nof_result_data_max);
  signal nxt_data_arr            : t_slv_32_arr(0 to g_i2c_cmdr.nof_result_data_max);
begin
  protocol_offset   <= i_protocol_offset;
  protocol_activate <= i_protocol_activate;

  protocol_status  <= TO_UVEC(state, c_word_w);
  result_error_cnt <= RESIZE_UVEC(error_cnt, c_word_w);
  result_data_arr  <= data_arr(0 to g_i2c_cmdr.nof_result_data_max - 1);

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      state               <= c_i2c_cmdr_state_idle;
      i_protocol_activate <= '0';
      i_protocol_offset   <= 0;
      result_expected     <= (others => '0');
      error_cnt           <= (others => '0');
      data_cnt            <= 0;
      data_arr            <= (others => (others => '0'));
    elsif rising_edge(clk) then
      state               <= nxt_state;
      i_protocol_activate <= nxt_protocol_activate;
      i_protocol_offset   <= nxt_protocol_offset;
      result_expected     <= nxt_result_expected;
      error_cnt           <= nxt_error_cnt;
      data_cnt            <= nxt_data_cnt;
      data_arr            <= nxt_data_arr;
    end if;
  end process;

  p_state : process(state, i_protocol_offset, result_expected,
                    protocol_index, protocol_offset_arr, result_expected_arr,
                    protocol_wr, protocol_activate_ack, result_ready_evt, protocol_status_rd)
  begin
    nxt_state <= state;
    nxt_protocol_activate <= '0';
    nxt_protocol_offset <= i_protocol_offset;
    nxt_result_expected <= result_expected;
    case state is
      when c_i2c_cmdr_state_idle =>
        if protocol_wr = '1' then
          nxt_state <= c_i2c_cmdr_state_pending;
          nxt_protocol_activate <= '1';
          nxt_protocol_offset <= protocol_offset_arr(protocol_index);
          nxt_result_expected <= result_expected_arr(protocol_index);
        end if;
      when c_i2c_cmdr_state_pending =>
        if protocol_activate_ack = '1' then
          nxt_state <= c_i2c_cmdr_state_busy;
        end if;
      when c_i2c_cmdr_state_busy =>
        if result_ready_evt = '1' then
          nxt_state <= c_i2c_cmdr_state_done;
        end if;
      when others =>  -- = c_i2c_cmdr_state_done
        if protocol_status_rd = '1' then
          nxt_state <= c_i2c_cmdr_state_idle;
        end if;
    end case;
  end process;

  -- The bits in result_expected mark the control and data bytes, it is used periodically, so for more than 32 result bytes the expected result must be the same
  -- . for protocol lists with only control and write I2C accesses all result_expected mask bits are '0'
  -- . for protocol lists with both control, write I2C and read I2C accesses only the reads have result_expected mask bit '1'
  expected_bi <= TO_UINT(result_wr_adr(c_expected_bi_w - 1 downto 0));

  p_result : process(error_cnt, data_cnt, data_arr, i_protocol_activate, result_wr_en, result_expected, expected_bi, result_wr_dat)
  begin
    nxt_error_cnt <= error_cnt;
    nxt_data_cnt  <= data_cnt;
    nxt_data_arr  <= data_arr;
    if i_protocol_activate = '1' then
      nxt_error_cnt <= (others => '0');
      nxt_data_cnt  <= 0;
      nxt_data_arr  <= (others => (others => '0'));
    elsif result_wr_en = '1' then
      if result_expected(expected_bi) = '0' then
        -- the result_expected at the bit index contains '0' so it marks a read control byte that has to be zero to be OK
        if TO_UINT(result_wr_dat) /= 0 then
          nxt_error_cnt <= INCR_UVEC(error_cnt, 1);
        end if;
      else
        -- the result_expected at the bit index contains '1' so it marks an I2C read data byte
        nxt_data_arr(data_cnt) <= RESIZE_UVEC(result_wr_dat, c_word_w);  -- store one I2C read data byte per MM word
        nxt_data_cnt <= data_cnt + 1;
      end if;
    end if;
  end process;
end rtl;
