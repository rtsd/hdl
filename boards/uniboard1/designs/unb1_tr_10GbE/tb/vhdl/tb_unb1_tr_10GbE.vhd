-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- Description:
-- Usage:

library IEEE, common_lib, unb1_board_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_unb1_tr_10GbE is
end tb_unb1_tr_10GbE;

architecture tb of tb_unb1_tr_10GbE is
  constant c_sim             : boolean := true;

  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 7;  -- Back node 3
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_unb_nr, c_unb1_board_nof_uniboard_w) & TO_UVEC(c_node_nr, c_unb1_board_nof_chip_w);

  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb1_board_fw_version := (1, 0);

  constant c_cable_delay     : time := 12 ns;
  constant c_eth_clk_period  : time := 40 ns;  -- 25 MHz XO on UniBoard
  constant c_sa_clk_period   : time := 6.4 ns;
  constant c_clk_period      : time := 5 ns;
  constant c_pps_period      : natural := 1000;

  -- DUT
  signal clk                 : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal pps_rst             : std_logic := '0';
  signal sa_clk              : std_logic := '1';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic;
  signal eth_rxp             : std_logic;

  signal VERSION             : std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0) := c_version;
  signal ID                  : std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0)      := c_id;
  signal TESTIO              : std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal si_fn_0_tx          : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0) := (others => '0');

  signal fn_bn_0_tx          : std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
  signal fn_bn_1_tx          : std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
  signal fn_bn_2_tx          : std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
  signal fn_bn_3_tx          : std_logic_vector(c_unb1_board_tr_mesh.bus_w - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  clk     <= not clk after c_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (25 MHz)
  sa_clk  <= not sa_clk  after c_sa_clk_period / 2;

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, clk, pps);

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_unb1_tr_10GbE : entity work.unb1_tr_10GbE
    generic map (
      g_sim         => c_sim
    )
    port map (
      -- GENERAL
      CLK         => clk,
      PPS         => pps,
      WDI         => WDI,
      INTA        => INTA,
      INTB        => INTB,

      sens_sc     => sens_scl,
      sens_sd     => sens_sda,

      -- Others
      VERSION     => VERSION,
      ID          => ID,
      TESTIO      => TESTIO,

      -- 1GbE Control Interface
      ETH_clk     => eth_clk,
      ETH_SGIN    => eth_rxp,
      ETH_SGOUT   => eth_txp,

      -- Transceiver clocks
      SA_CLK      => sa_clk,

      -- Serial I/O
      SI_FN_0_RX  => si_fn_0_tx,
      SI_FN_1_RX  => si_fn_0_tx,
      SI_FN_2_RX  => si_fn_0_tx,
      SI_FN_3_RX  => si_fn_0_tx
    );
end tb;
