-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, tech_flash_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_flash_lib.tech_flash_component_pkg.all;

entity mms_remu is
  generic (
    g_technology : natural := c_tech_select_default
  );
  port (
    mm_rst          : in    std_logic;
    mm_clk          : in    std_logic;

    epcs_clk        : in    std_logic;

    -- MM registers
    remu_mosi       : in    t_mem_mosi := c_mem_mosi_rst;
    remu_miso       : out   t_mem_miso
  );
end mms_remu;

architecture str of mms_remu is
  constant c_param_w       : natural := 3;
  constant c_data_w        : natural := tech_flash_data_w(g_technology);

  signal epcs_rst          : std_logic;

  signal remu_busy         : std_logic;
  signal remu_data_out     : std_logic_vector(c_data_w - 1 downto 0);

  signal fall_remu_reconfigure  : std_logic;
  signal fall_remu_read_param   : std_logic;
  signal fall_remu_write_param  : std_logic;
  signal fall_remu_param        : std_logic_vector(c_param_w - 1 downto 0);
  signal fall_remu_data_in      : std_logic_vector(c_data_w - 1 downto 0);

  signal nxt_fall_remu_reconfigure  : std_logic;
  signal nxt_fall_remu_read_param   : std_logic;
  signal nxt_fall_remu_write_param  : std_logic;
  signal nxt_fall_remu_param        : std_logic_vector(c_param_w - 1 downto 0);
  signal nxt_fall_remu_data_in      : std_logic_vector(c_data_w - 1 downto 0);
begin
  u_remu: entity tech_flash_lib.tech_flash_remote_update
  generic map (
    g_technology  => g_technology
  )
  port map (
    clock         => epcs_clk,
    param         => fall_remu_param,
    read_param    => fall_remu_read_param,
    reconfig      => fall_remu_reconfigure,
    reset         => epcs_rst,
    reset_timer   => '0',
    busy          => remu_busy,
    data_out      => remu_data_out,
    write_param   => fall_remu_write_param,
    data_in       => fall_remu_data_in
   );

  u_remu_reg: entity work.remu_reg
  generic map (
    g_data_w  => c_data_w
  )
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,

    epcs_rst          => epcs_rst,
    epcs_clk          => epcs_clk,

    sla_in            => remu_mosi,
    sla_out           => remu_miso,

    reconfigure       => nxt_fall_remu_reconfigure,
    read_param        => nxt_fall_remu_read_param,
    param             => nxt_fall_remu_param,
    busy              => remu_busy,

    data_out          => remu_data_out,
    write_param       => nxt_fall_remu_write_param,
    data_in           => nxt_fall_remu_data_in
  );

  -- The Remote Upgrade megafunction uses an ASMI_PARALLEL instance internally and therefore requires
  -- the same clocking scheme as the EPCS module:

  -- The ASMI_PARALLEL requires that data is set up on the falling edge. The following rising
  -- edge of the ASMI_PARALLEL's 20MHz clkin clocks in the data. The data is required to be held until
  -- the next falling edge:
  --  __ __ __ __ __ __
  -- _||_||_||_||_||_||_
  --         ____
  -- ________|  |_______
  --
  -- All used components expect rising edge clocked signals, so we're reclocking the relevant signals
  -- with falling-edge clocked registers:
  p_epcs_clk: process(epcs_clk, epcs_rst)
  begin
    if epcs_rst = '1' then
      fall_remu_reconfigure <= '0';
      fall_remu_read_param  <= '0';
      fall_remu_write_param <= '0';
      fall_remu_param       <= (others => '0');
      fall_remu_data_in     <= (others => '0');
    elsif falling_edge(epcs_clk) then
      fall_remu_reconfigure <= nxt_fall_remu_reconfigure;
      fall_remu_read_param  <= nxt_fall_remu_read_param;
      fall_remu_write_param <= nxt_fall_remu_write_param;
      fall_remu_param       <= nxt_fall_remu_param;
      fall_remu_data_in     <= nxt_fall_remu_data_in;
    end if;
  end process;

  u_common_areset: entity common_lib.common_areset
  port map (
    in_rst             => mm_rst,
    clk                => epcs_clk,
    out_rst            => epcs_rst
  );
end str;
