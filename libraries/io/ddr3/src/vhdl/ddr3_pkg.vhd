-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use IEEE.numeric_std.all;

package ddr3_pkg is
  -- DDR3 (definitions similar as in ug_altmemphy.pdf)
  type t_c_ddr3_phy is record
    a_w                               : natural;  -- = 16;
    a_row_w                           : natural;  -- = 16;  --  = a_w, row address width, via a_w lines
    a_col_w                           : natural;  -- = 10;  -- <= a_w, col address width, via a_w lines
    ba_w                              : natural;  -- = 3;
    dq_w                              : natural;  -- = 64;
    dqs_w                             : natural;  -- = 8;   --  = dq_w / nof_dq_per_dqs;
    dm_w                              : natural;  -- = 8;
    cs_w                              : natural;  -- = 2;
    clk_w                             : natural;  -- = 2;
  end record;

  constant c_ddr3_phy                 : t_c_ddr3_phy := (16, 16, 10, 3, 64, 8, 8, 2, 2);
  constant c_ddr3_phy_4g              : t_c_ddr3_phy := (15, 15, 10, 3, 64, 8, 8, 2, 2);

  type t_ddr3_phy_in is record
    evt              : std_logic;
    oct_rup          : std_logic;
    oct_rdn          : std_logic;
    nc               : std_logic;  -- not connected, needed to be able to initialize constant record which has to have more than one field in VHDL
  end record;

  type t_ddr3_phy_io is record  -- Do not use this type in Quartus! Use the _sel version instead.
    dq               : std_logic_vector(c_ddr3_phy.dq_w - 1 downto 0);  -- data bus
    dqs              : std_logic_vector(c_ddr3_phy.dqs_w - 1 downto 0);  -- data strobe bus
    dqs_n            : std_logic_vector(c_ddr3_phy.dqs_w - 1 downto 0);
    scl              : std_logic;  -- I2C
    sda              : std_logic;
  end record;

  type t_ddr3_phy_ou is record
    a                : std_logic_vector(c_ddr3_phy.a_w - 1 downto 0);  -- row and column address
    ba               : std_logic_vector(c_ddr3_phy.ba_w - 1 downto 0);  -- bank address
    dm               : std_logic_vector(c_ddr3_phy.dm_w - 1 downto 0);  -- data mask bus
    cas_n            : std_logic;  -- _VECTOR(0 DOWNTO 0);                   -- column address strobe
    ras_n            : std_logic;  -- _VECTOR(0 DOWNTO 0);                   -- row address strobe
    we_n             : std_logic;  -- _VECTOR(0 DOWNTO 0);                   -- write enable signal
    reset_n          : std_logic;  -- reset signal
    ck               : std_logic_vector(c_ddr3_phy.clk_w - 1 downto 0);  -- clock, positive edge clock
    ck_n             : std_logic_vector(c_ddr3_phy.clk_w - 1 downto 0);  -- clock, negative edge clock
    odt              : std_logic_vector(c_ddr3_phy.cs_w - 1 downto 0);  -- on-die termination control signal
    cke              : std_logic_vector(c_ddr3_phy.cs_w - 1 downto 0);  -- clock enable
    cs_n             : std_logic_vector(c_ddr3_phy.cs_w - 1 downto 0);  -- chip select
  end record;

  constant c_ddr3_phy_in_rst   : t_ddr3_phy_in := ('0', 'X', 'X', 'X');
  constant c_ddr3_phy_io_rst   : t_ddr3_phy_io := ((others => '0'), (others => '0'), (others => '0'), '0', '0');
  constant c_ddr3_phy_ou_rst   : t_ddr3_phy_ou := ((others => '0'), (others => '0'), (others => '0'), '0', '0', '0', '0', (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'));

  type t_ddr3_phy_in_arr is array(natural range <>) of t_ddr3_phy_in;
  type t_ddr3_phy_io_arr is array(natural range <>) of t_ddr3_phy_io;
  type t_ddr3_phy_ou_arr is array(natural range <>) of t_ddr3_phy_ou;

  type t_ddr3_addr is record
    chip      : std_logic_vector(ceil_log2(c_ddr3_phy.cs_w)  - 1 downto 0);  -- Note: The controller interprets the chip address as logical address (NOT individual chip sel lines), hence ceil_log2
    bank      : std_logic_vector(          c_ddr3_phy.ba_w   - 1 downto 0);
    row       : std_logic_vector(          c_ddr3_phy.a_row_w - 1 downto 0);
    column    : std_logic_vector(          c_ddr3_phy.a_col_w - 1 downto 0);
  end record;

  type t_ddr3_addr_arr is array(natural range <>) of t_ddr3_addr;

  constant c_ddr3_ctlr_data_w           : natural := 256;  -- = 64 (PHY dq width) * 2 (use both PHY clock edges) * 2 (PHY transfer at double rate)
  constant c_ddr3_ctlr_rsl              : natural := c_ddr3_ctlr_data_w / c_ddr3_phy.dq_w;  -- =4
  constant c_ddr3_ctlr_rsl_w            : natural := ceil_log2(c_ddr3_ctlr_rsl);
  constant c_ddr3_ctlr_maxburstsize     : natural := 64;
  constant c_ddr3_ctlr_maxburstsize_w   : natural := ceil_log2(c_ddr3_ctlr_maxburstsize+1);
  constant c_ddr3_ctrl_nof_latent_reads : natural := 100;  -- The downside to having a command cue: even after de-asserting read requests, the ALTMEMPHY keeps processing your cued read requests.
                                                           -- This makes sure 100 words are still available in the read FIFO after it de-asserted its siso.ready signal towards the ddr3 read side.

  constant c_ddr3_phy_oct_w             : natural := 14;
  constant c_ddr3_phy_oct_rs            : std_logic_vector := TO_UVEC(0, c_ddr3_phy_oct_w);
  constant c_ddr3_phy_oct_rt            : std_logic_vector := TO_UVEC(0, c_ddr3_phy_oct_w);

  constant c_ddr3_addr_lo               : t_ddr3_addr := ((others => '0'), (others => '0'), (others => '0'),  (others => '0'));
  constant c_ddr3_address_lo            : natural := 0;

  constant c_ddr3_addr_hi_4gb           : t_ddr3_addr := ((others => '1'), (others => '1'), (others => '1'),  TO_UVEC(2**c_ddr3_phy_4g.a_col_w - c_ddr3_ctlr_rsl, c_ddr3_phy_4g.a_col_w));

  -- 4 rows * 1024 cols  * 64 bits / 128bits per associative mem array element = 2048 = default ALTMEMPHY mem_model array depth.
  constant c_ddr3_addr_hi_sim           : t_ddr3_addr := ((others => '0'), (others => '0'), TO_UVEC(3, c_ddr3_phy.a_row_w),  TO_UVEC(2**c_ddr3_phy_4g.a_col_w - c_ddr3_ctlr_rsl, c_ddr3_phy_4g.a_col_w));
  constant c_ddr3_address_hi_sim        : natural := 4092;  -- TB uses generated mem model with 2ki addresses of 128k - so the array holds 4096 64-bit words. End address is 4092 (resolution=4: last write=4092,4093,4094,4095.)

  type t_ddr3_seq is record
    wr_chunksize    : positive;  -- := 64;
    wr_nof_chunks   : positive;  -- := 1;
    rd_chunksize    : positive;  -- := 16;
    rd_nof_chunks   : positive;  -- := 4;
    gapsize         : natural;  -- := 0;
    nof_blocks      : positive;  -- := 5;
  end record;

  constant c_ddr3_seq : t_ddr3_seq := (64, 1, 16, 4, 0, 5);

  -- Manually derived VHDL entity from Verilog module $HDL_BUILD_DIR/ip_stratixiv_ddr3_uphy_4g_800_master.v
  component ip_stratixiv_ddr3_uphy_4g_800_master is
  port (
    pll_ref_clk                : in    std_logic;  -- pll_ref_clk.clk
    global_reset_n             : in    std_logic;  -- global_reset.reset_n
    soft_reset_n               : in    std_logic;  -- soft_reset.reset_n
    afi_clk                    : out   std_logic;  -- afi_clk.clk
    afi_half_clk               : out   std_logic;  -- afi_half_clk.clk
    afi_reset_n                : out   std_logic;  -- afi_reset.reset_n
    mem_a                      : out   std_logic_vector(14 downto 0);  -- memory.mem_a
    mem_ba                     : out   std_logic_vector(2 downto 0);  -- .mem_ba
    mem_ck                     : out   std_logic_vector(1 downto 0);  -- .mem_ck
    mem_ck_n                   : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_cke                    : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n                   : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_dm                     : out   std_logic_vector(7 downto 0);  -- .mem_dm
    mem_ras_n                  : out   std_logic;  -- .mem_ras_n
    mem_cas_n                  : out   std_logic;  -- .mem_cas_n
    mem_we_n                   : out   std_logic;  -- .mem_we_n
    mem_reset_n                : out   std_logic;  -- .mem_reset_n
    mem_dq                     : inout std_logic_vector(63 downto 0);  -- .mem_dq
    mem_dqs                    : inout std_logic_vector(7 downto 0);  -- .mem_dqs
    mem_dqs_n                  : inout std_logic_vector(7 downto 0);  -- .mem_dqs_n
    mem_odt                    : out   std_logic_vector(1 downto 0);  -- .mem_odt
    avl_ready                  : out   std_logic;  -- avl.waitrequest_n
    avl_burstbegin             : in    std_logic;  -- .beginbursttransfer
    avl_addr                   : in    std_logic_vector(26 downto 0);  -- .address
    avl_rdata_valid            : out   std_logic;  -- .readdatavalid
    avl_rdata                  : out   std_logic_vector(255 downto 0);  -- .readdata
    avl_wdata                  : in    std_logic_vector(255 downto 0);  -- .writedata
    avl_be                     : in    std_logic_vector(31 downto 0);  -- .byteenable
    avl_read_req               : in    std_logic;  -- .read
    avl_write_req              : in    std_logic;  -- .write
    avl_size                   : in    std_logic_vector(6 downto 0);  -- .burstcount
    local_init_done            : out   std_logic;  -- status.local_init_done
    local_cal_success          : out   std_logic;  -- .local_cal_success
    local_cal_fail             : out   std_logic;  -- .local_cal_fail
    oct_rdn                    : in    std_logic;  -- oct.rdn
    oct_rup                    : in    std_logic;  -- .rup
    seriesterminationcontrol   : out   std_logic_vector(13 downto 0);  -- oct_sharing.seriesterminationcontrol
    parallelterminationcontrol : out   std_logic_vector(13 downto 0);  -- .parallelterminationcontrol
    pll_mem_clk                : out   std_logic;  -- pll_sharing.pll_mem_clk
    pll_write_clk              : out   std_logic;  -- .pll_write_clk
    pll_write_clk_pre_phy_clk  : out   std_logic;  -- .pll_write_clk_pre_phy_clk
    pll_addr_cmd_clk           : out   std_logic;  -- .pll_addr_cmd_clk
    pll_locked                 : out   std_logic;  -- .pll_locked
    pll_avl_clk                : out   std_logic;  -- .pll_avl_clk
    pll_config_clk             : out   std_logic;  -- .pll_config_clk
    dll_delayctrl              : out   std_logic_vector(5 downto 0)  -- dll_sharing.dll_delayctrl
  );
  end component;

  -- Manually derived VHDL entity from Verilog module $HDL_BUILD_DIR/ip_stratixiv_ddr3_uphy_4g_800_slave.v
  -- . diff with master is that only master has oct_* inputs and that the *terminationcontrol are inputs for the slave
  component ip_stratixiv_ddr3_uphy_4g_800_slave is
  port (
    pll_ref_clk                : in    std_logic;  -- pll_ref_clk.clk
    global_reset_n             : in    std_logic;  -- global_reset.reset_n
    soft_reset_n               : in    std_logic;  -- soft_reset.reset_n
    afi_clk                    : out   std_logic;  -- afi_clk.clk
    afi_half_clk               : out   std_logic;  -- afi_half_clk.clk
    afi_reset_n                : out   std_logic;  -- afi_reset.reset_n
    mem_a                      : out   std_logic_vector(14 downto 0);  -- memory.mem_a
    mem_ba                     : out   std_logic_vector(2 downto 0);  -- .mem_ba
    mem_ck                     : out   std_logic_vector(1 downto 0);  -- .mem_ck
    mem_ck_n                   : out   std_logic_vector(1 downto 0);  -- .mem_ck_n
    mem_cke                    : out   std_logic_vector(1 downto 0);  -- .mem_cke
    mem_cs_n                   : out   std_logic_vector(1 downto 0);  -- .mem_cs_n
    mem_dm                     : out   std_logic_vector(7 downto 0);  -- .mem_dm
    mem_ras_n                  : out   std_logic;  -- .mem_ras_n
    mem_cas_n                  : out   std_logic;  -- .mem_cas_n
    mem_we_n                   : out   std_logic;  -- .mem_we_n
    mem_reset_n                : out   std_logic;  -- .mem_reset_n
    mem_dq                     : inout std_logic_vector(63 downto 0);  -- .mem_dq
    mem_dqs                    : inout std_logic_vector(7 downto 0);  -- .mem_dqs
    mem_dqs_n                  : inout std_logic_vector(7 downto 0);  -- .mem_dqs_n
    mem_odt                    : out   std_logic_vector(1 downto 0);  -- .mem_odt
    avl_ready                  : out   std_logic;  -- avl.waitrequest_n
    avl_burstbegin             : in    std_logic;  -- .beginbursttransfer
    avl_addr                   : in    std_logic_vector(26 downto 0);  -- .address
    avl_rdata_valid            : out   std_logic;  -- .readdatavalid
    avl_rdata                  : out   std_logic_vector(255 downto 0);  -- .readdata
    avl_wdata                  : in    std_logic_vector(255 downto 0);  -- .writedata
    avl_be                     : in    std_logic_vector(31 downto 0);  -- .byteenable
    avl_read_req               : in    std_logic;  -- .read
    avl_write_req              : in    std_logic;  -- .write
    avl_size                   : in    std_logic_vector(6 downto 0);  -- .burstcount
    local_init_done            : out   std_logic;  -- status.local_init_done
    local_cal_success          : out   std_logic;  -- .local_cal_success
    local_cal_fail             : out   std_logic;  -- .local_cal_fail
    seriesterminationcontrol   : in    std_logic_vector(13 downto 0);  -- oct_sharing.seriesterminationcontrol
    parallelterminationcontrol : in    std_logic_vector(13 downto 0);  -- .parallelterminationcontrol
    pll_mem_clk                : out   std_logic;  -- pll_sharing.pll_mem_clk
    pll_write_clk              : out   std_logic;  -- .pll_write_clk
    pll_write_clk_pre_phy_clk  : out   std_logic;  -- .pll_write_clk_pre_phy_clk
    pll_addr_cmd_clk           : out   std_logic;  -- .pll_addr_cmd_clk
    pll_locked                 : out   std_logic;  -- .pll_locked
    pll_avl_clk                : out   std_logic;  -- .pll_avl_clk
    pll_config_clk             : out   std_logic;  -- .pll_config_clk
    dll_delayctrl              : out   std_logic_vector(5 downto 0)  -- dll_sharing.dll_delayctrl
  );
  end component;

  component alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en is
  generic (
    MEM_IF_ADDR_WIDTH            : integer := 0;
    MEM_IF_ROW_ADDR_WIDTH        : integer := 0;
    MEM_IF_COL_ADDR_WIDTH        : integer := 0;
    MEM_IF_CS_PER_RANK           : integer := 0;
    MEM_IF_CONTROL_WIDTH         : integer := 0;
    MEM_IF_DQS_WIDTH             : integer := 0;
    MEM_IF_CS_WIDTH              : integer := 0;
    MEM_IF_BANKADDR_WIDTH        : integer := 0;
    MEM_IF_DQ_WIDTH              : integer := 0;
    MEM_IF_CK_WIDTH              : integer := 0;
    MEM_IF_CLK_EN_WIDTH          : integer := 0;
    DEVICE_WIDTH                 : integer := 1;
    MEM_TRCD                     : integer := 0;
    MEM_TRTP                     : integer := 0;
    MEM_DQS_TO_CLK_CAPTURE_DELAY : integer := 0;
    MEM_CLK_TO_DQS_CAPTURE_DELAY : integer := 0;
    MEM_IF_ODT_WIDTH             : integer := 0;
    MEM_MIRROR_ADDRESSING_DEC    : integer := 0;
    MEM_REGDIMM_ENABLED          : boolean := false;
    DEVICE_DEPTH                 : integer := 1;
    MEM_GUARANTEED_WRITE_INIT    : boolean := false;
    MEM_VERBOSE                  : boolean := true;
    MEM_INIT_EN                  : boolean := false;
    MEM_INIT_FILE                : string  := "";
    DAT_DATA_WIDTH               : integer := 32
  );
  port (
    mem_a       : in    std_logic_vector(14 downto 0) := (others => 'X');  -- mem_a
    mem_ba      : in    std_logic_vector(2 downto 0)  := (others => 'X');  -- mem_ba
    mem_ck      : in    std_logic_vector(1 downto 0)  := (others => 'X');  -- mem_ck
    mem_ck_n    : in    std_logic_vector(1 downto 0)  := (others => 'X');  -- mem_ck_n
    mem_cke     : in    std_logic_vector(1 downto 0)  := (others => 'X');  -- mem_cke
    mem_cs_n    : in    std_logic_vector(1 downto 0)  := (others => 'X');  -- mem_cs_n
    mem_dm      : in    std_logic_vector(7 downto 0)  := (others => 'X');  -- mem_dm
    mem_ras_n   : in    std_logic_vector(0 downto 0)  := (others => 'X');  -- mem_ras_n
    mem_cas_n   : in    std_logic_vector(0 downto 0)  := (others => 'X');  -- mem_cas_n
    mem_we_n    : in    std_logic_vector(0 downto 0)  := (others => 'X');  -- mem_we_n
    mem_reset_n : in    std_logic                     := 'X';  -- mem_reset_n
    mem_dq      : inout std_logic_vector(63 downto 0) := (others => 'X');  -- mem_dq
    mem_dqs     : inout std_logic_vector(7 downto 0)  := (others => 'X');  -- mem_dqs
    mem_dqs_n   : inout std_logic_vector(7 downto 0)  := (others => 'X');  -- mem_dqs_n
    mem_odt     : in    std_logic_vector(1 downto 0)  := (others => 'X')  -- mem_odt
  );
  end component alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en;
end ddr3_pkg;

package body ddr3_pkg is
end ddr3_pkg;
