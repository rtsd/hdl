
namespace eval qsys_unb2b_minimal_cpu_0 {
  proc get_memory_files {QSYS_SIMDIR} {
    set memory_files [list]
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_ociram_default_contents.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_ociram_default_contents.dat"
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_rf_ram_b.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_rf_ram_b.mif"
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_ociram_default_contents.mif"
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_rf_ram_b.dat"
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_rf_ram_a.mif"
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_rf_ram_a.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_nios2_gen2_unit_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_unit_180_k3tyfta_rf_ram_a.dat"
    return $memory_files
  }
  
  proc get_common_design_files {QSYS_SIMDIR} {
    set design_files [dict create]
    return $design_files
  }
  
  proc get_design_files {QSYS_SIMDIR} {
    set design_files [dict create]
    error "Skipping VCS script generation since VHDL file $QSYS_SIMDIR/../altera_nios2_gen2_180/sim/qsys_unb2b_minimal_cpu_0_altera_nios2_gen2_180_drycr4y.vhd is required for simulation"
  }
  
  proc get_elab_options {SIMULATOR_TOOL_BITNESS} {
    set ELAB_OPTIONS ""
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ELAB_OPTIONS
  }
  
  
  proc get_sim_options {SIMULATOR_TOOL_BITNESS} {
    set SIM_OPTIONS ""
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $SIM_OPTIONS
  }
  
  
  proc get_env_variables {SIMULATOR_TOOL_BITNESS} {
    set ENV_VARIABLES [dict create]
    set LD_LIBRARY_PATH [dict create]
    dict set ENV_VARIABLES "LD_LIBRARY_PATH" $LD_LIBRARY_PATH
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ENV_VARIABLES
  }
  
  
}
