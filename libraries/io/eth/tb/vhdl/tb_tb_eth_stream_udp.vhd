-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Multi test bench for eth_stream_udp
-- Description:
--
-- Usage:
--   > as 8
--   > run -all

library IEEE, diag_lib;
use IEEE.std_logic_1164.all;
use diag_lib.diag_pkg.all;
use work.tb_eth_tester_pkg.all;

entity tb_tb_eth_stream_udp is
end tb_tb_eth_stream_udp;

architecture tb of tb_tb_eth_stream_udp is
  -- Tb
  constant c_eth_clk_MHz   : natural := 125;
  constant c_st_clk_MHz    : natural := 200;
  constant c_nof_sync      : natural := 2;
  constant c_nof_blk       : natural := 3;  -- nof_blk per sync

  -- Tx packet size and gap size in octets
  constant c_block_len     : natural := 50;
  constant c_link_len      : natural := func_eth_tester_eth_packet_on_link_length(c_block_len);

  -- For near maximum 1Gbps link rate the c_block_len + c_gap_len_min time
  -- in the st_clk domain equals c_link_len time in eth_clk domain.
  constant c_gap_len_min   : natural := c_link_len * c_st_clk_MHz / c_eth_clk_MHz - c_block_len;

  -- Choose c_gap_len somewhat larger to have packet link rate < 1 Gbps
  constant c_gap_len       : natural := c_gap_len_min * 2;  -- for g_nof_streams = 1

  -- BG ctrl
  constant c_high          : natural := c_diag_bg_mem_max_adr;  -- = 2**24

  constant c_bg_ctrl       : t_diag_block_gen_integer := ('1', '1', c_block_len,  c_nof_blk, c_gap_len, 0, c_high, 0);  -- for first stream
begin
--  g_tb_index         : NATURAL := 0;  -- use to incremental delay logging from tb instances in tb_tb
--  g_nof_sync         : NATURAL := 2;  -- number of BG sync intervals to set c_run_time
--  g_udp_port_match   : BOOLEAN := TRUE;
--
--  -- t_diag_block_gen_integer =
--  --   sl:  enable
--  --   sl:  enable_sync
--  --   nat: samples_per_packet
--  --   nat: blocks_per_sync
--  --   nat: gapsize
--  --   nat: mem_low_adrs
--  --   nat: mem_high_adrs
--  --   nat: bsn_init
--  g_bg_ctrl    : t_diag_block_gen_integer := ('1', '1', 50, 3, 200, 0, c_diag_bg_mem_max_adr, 0)  -- for first stream

  u_udp          : entity work.tb_eth_stream_udp generic map (0, c_nof_sync,  true, c_bg_ctrl);
  u_udp_mismatch : entity work.tb_eth_stream_udp generic map (1, c_nof_sync, false, c_bg_ctrl);
end tb;
