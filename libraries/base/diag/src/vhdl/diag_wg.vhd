-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Sine waveform generator
-- Description:
-- . Based on diag_waveproc from LOFAR1.
-- . Monitor the active WG ctrl:
--   - WG ctrl.mode = off takes effect immediately
--   - WG ctrl.ampl takes effect immediately
--   - Changing WG ctrl.phase and ctrl.freq require a restart to take effect,
--     to have synchronous phase relation between different WG.
-- Remarks:
-- . For WG sine periods that integer fit in the WG buffer size the carrier
--   wWave (CW) frequency is exact. For fractional WG frequencies, for which
--   the CW period does not fit the WG buffer size, the WG frequency is exact
--   only on average.
-- . The signal to noise ratio (SNR) of the carrier wave (CW) sine is:
--     SNR = g_calc_dat_w * 6.02 dB + 1.76 dB,
--   provided that the CW period fits the buffer size.
---. For fractional WG frequencies the effective number of bits of the WG is
--   g_buf_addr_w - 1, because the WG buffer contains a full sine period. The
--   -1 bit or factor 1/2 comes from that the rising slope and falling slope
--   of the WG buffer sine map the period resolution to the amplitude
--   resolution. Hence for fraction WG frequencies the effective SNR of the
--   CW becomes:
--     SNR = (g_buf_addr_w-1) * 6.02 dB + 1.76 dB.
--   Hence for g_buf_addr_w = 10, the effective SNR of the generated
--   CW with fractional frequency is SNR ~= 56 dB, even if g_calc_dat_w > 9.

library IEEE, common_lib, common_mult_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.diag_pkg.all;
use technology_lib.technology_select_pkg.all;

entity diag_wg is
  generic (
    g_technology        : natural := c_tech_select_default;
    g_buf_dat_w         : natural := 18;  -- Use >= g_calc_dat_w and typically <= DSP multiply 18x18 element
    g_buf_addr_w        : natural := 11;  -- Waveform buffer size 2**g_buf_addr_w nof samples
                                             -- . in calc mode fill the entire buffer with one sinus wave, ctrl.phase and ctrl.freq will map on the entire range
                                             -- . in single or repeat mode fill the buffer with an arbitrary signal and define actual the period via ctrl.nof_samples
    g_rate_factor       : natural := 1;  -- Default 1 for unit frequency Fs, else g_rate_factor * Fs using g_rate_factor nof parallel outputs
    g_rate_offset       : natural := 0;  -- Selects which of the parallel outputs [0:g_rate_factor-1] this WG should generate
    g_calc_support      : boolean := true;  -- When FALSE then calc mode falls back to repeat mode to save logic.
    g_calc_gain_w       : natural := 1;  -- Normalized range [0 1>  maps to fixed point range [0:2**c_diag_wg_ampl_w>
                                             -- . use gain 2**0             = 1 to have full scale without clipping
                                             -- . use gain 2**g_calc_gain_w > 1 to cause clipping
    g_calc_dat_w        : natural := 12  -- Effective range of the WG out_dat
  );
  port (
    rst                  : in  std_logic;
    clk                  : in  std_logic;
    restart              : in  std_logic;

    buf_addr             : out std_logic_vector(g_buf_addr_w - 1 downto 0);
    buf_rden             : out std_logic;
    buf_rddat            : in  std_logic_vector(g_buf_dat_w - 1 downto 0);
    buf_rdval            : in  std_logic;

    ctrl                 : in  t_diag_wg;
    mon_ctrl             : out t_diag_wg;

    out_ovr              : out std_logic;
    out_dat              : out std_logic_vector(g_buf_dat_w - 1 downto 0);
    out_val              : out std_logic;
    out_sync             : out std_logic  -- Pulse to mark WG start
  );
end diag_wg;

architecture rtl of diag_wg is
  constant c_mult_pipeline_input   : natural := 1;  -- 0 or 1
  constant c_mult_pipeline_product : natural := 1;  -- 0 or 1
  constant c_mult_pipeline_output  : natural := 0;  -- >= 0
  constant c_mult_pipeline         : natural := c_mult_pipeline_input + c_mult_pipeline_product + c_mult_pipeline_output;

  constant c_clip_pipeline         : natural := 1;
  constant c_round_pipeline        : natural := 1;

  constant c_sync_dly              : natural := 2;  -- Set to 2 to create a sync that is alligned with the sop.
  constant c_calc_pipeline         : natural := c_mult_pipeline + c_round_pipeline + c_clip_pipeline;

  constant c_ampl_w                : natural := c_diag_wg_ampl_w + 1;  -- extra MSBit equal to '0' to have amplitude as signed value
  constant c_mult_w                : natural := g_buf_dat_w + c_ampl_w;  -- signed multiply

  -- Shift_left 1 to skip 'double sign bit' is safe, because +-rddat * +ampl will have MSbits "00" or "11", so the sign bit is preserved.
  constant c_prod_w                : natural := c_mult_w - 1;

  -- Round and account for the gain factor to support functional WG clipping.
  constant c_round_w               : natural := g_calc_dat_w + g_calc_gain_w;

  -- Clip to account for the gain factor
  constant c_calc_full_scale       : natural := 2**(g_calc_dat_w - 1) - 1;

  type state_enum is (
    s_off,
    s_init,
    s_single,
    s_repeat,
    s_calc
  );

  signal state                 : state_enum;
  signal nxt_state             : state_enum;
  signal prev_state            : state_enum;
  signal idle                  : std_logic;

  signal i_mon_ctrl            : t_diag_wg;
  signal nxt_mon_ctrl          : t_diag_wg;
  signal nof_samples           : std_logic_vector(g_buf_addr_w downto 0);  -- only use effective range of nof_samples+1
  signal nxt_nof_samples       : std_logic_vector(g_buf_addr_w downto 0);
  signal sample_cnt            : natural range 0 to 2**g_buf_addr_w - 1;
  signal nxt_sample_cnt        : natural;
  signal sample_step           : natural range 0 to g_rate_factor;
  signal nxt_sample_step       : natural;
  signal init_repeat_done      : std_logic;

  signal phase                 : std_logic_vector(ctrl.freq'length - 1 downto 0);
  signal nxt_phase             : std_logic_vector(phase'range);
  signal phase_step            : std_logic_vector(phase'range);
  signal nxt_phase_step        : std_logic_vector(phase'range);
  signal init_phase_cnt        : natural range 0 to g_rate_factor;
  signal nxt_init_phase_cnt    : natural;
  signal init_calc_done        : std_logic;

  signal nxt_buf_addr          : std_logic_vector(buf_addr'range);
  signal nxt_buf_rden          : std_logic;

  signal ctrl_ampl             : std_logic_vector(c_ampl_w - 1 downto 0);
  signal mult_dat              : std_logic_vector(c_mult_w - 1 downto 0);
  signal prod_dat              : std_logic_vector(c_prod_w - 1 downto 0);
  signal round_dat             : std_logic_vector(c_round_w - 1 downto 0);
  signal clip_dat              : std_logic_vector(c_round_w - 1 downto 0);
  signal clip_ovr              : std_logic;

  signal buf_rdval_dly         : std_logic;

  signal init_sync             : std_logic;
  signal nxt_init_sync         : std_logic;
  signal sync_dly_default      : std_logic;
  signal sync_dly_calc         : std_logic;

  signal nxt_out_ovr           : std_logic;
  signal nxt_out_dat           : std_logic_vector(out_dat'range);
  signal nxt_out_val           : std_logic;
  signal nxt_out_sync          : std_logic;
begin
  mon_ctrl <= i_mon_ctrl;

  registers : process(clk, rst)
  begin
    if rst = '1' then
      -- Internal registers.
      nof_samples           <= (others => '0');
      state                 <= s_off;
      prev_state            <= s_off;
      sample_cnt            <= 0;
      sample_step           <= 0;
      phase                 <= (others => '0');
      phase_step            <= (others => '0');
      init_phase_cnt        <= 0;
      init_sync             <= '0';
      -- Output registers.
      i_mon_ctrl            <= c_diag_wg_rst;
      buf_addr              <= (others => '0');
      buf_rden              <= '0';
      out_ovr               <= '0';
      out_dat               <= (others => '0');
      out_val               <= '0';
      out_sync              <= '0';
    elsif rising_edge(clk) then
      -- Internal registers.
      nof_samples           <= nxt_nof_samples;
      state                 <= nxt_state;
      prev_state            <= state;
      sample_cnt            <= nxt_sample_cnt;
      sample_step           <= nxt_sample_step;
      phase                 <= nxt_phase;
      phase_step            <= nxt_phase_step;
      init_phase_cnt        <= nxt_init_phase_cnt;
      init_sync             <= nxt_init_sync;
      -- Output registers.
      i_mon_ctrl            <= nxt_mon_ctrl;
      buf_addr              <= nxt_buf_addr;
      buf_rden              <= nxt_buf_rden;
      out_ovr               <= nxt_out_ovr;
      out_dat               <= nxt_out_dat;
      out_val               <= nxt_out_val;
      out_sync              <= nxt_out_sync;
    end if;
  end process;

  idle <= '1' when state = s_off or state = s_init else '0';

  p_state_machine : process(state, restart, ctrl, init_repeat_done, init_calc_done, sample_cnt, sample_step, nof_samples)
  begin
    nxt_state <= state;
    nxt_init_sync <= '0';
    if unsigned(ctrl.mode) = c_diag_wg_mode_off then
      nxt_state <= s_off;  -- WG off takes effect immediately
    end if;
    if restart = '1' then
      nxt_state <= s_init;  -- Restart the WG
    else
      case state is
        when s_init =>
          case to_integer(unsigned(ctrl.mode)) is
            when c_diag_wg_mode_single => if init_repeat_done = '1'                            then nxt_state <= s_single; nxt_init_sync <= '1'; end if;
            when c_diag_wg_mode_repeat => if init_repeat_done = '1'                            then nxt_state <= s_repeat; nxt_init_sync <= '1'; end if;
            when c_diag_wg_mode_calc   => if g_calc_support = false and init_repeat_done = '1' then nxt_state <= s_repeat; nxt_init_sync <= '1'; end if;
                                          if g_calc_support = true  and init_calc_done  = '1' then nxt_state <= s_calc;   nxt_init_sync <= '1'; end if;
            when others                => nxt_state <= s_off;
          end case;
        when s_single =>
          if sample_cnt + sample_step >= unsigned(nof_samples) then
            nxt_state <= s_off;
          end if;
        when others => null;  -- continue in state
      end case;
    end if;
  end process;

  -- Mode SINGLE or REPEAT
  nxt_nof_samples <= ctrl.nof_samples(nof_samples'range);

  p_sample_counter : process(restart, state, sample_cnt, sample_step, nof_samples)
    variable v_sample_incr : natural;
  begin
    nxt_sample_cnt  <= sample_cnt;
    nxt_sample_step <= sample_step;

    -- Init
    init_repeat_done <= '0';
    if restart = '1' then
      nxt_sample_cnt  <= g_rate_offset;
      nxt_sample_step <= g_rate_factor;
    else
      -- Typically nof_samples will be much larger than g_rate_offset. However if nof_samples is smaller
      -- than the rate offset, then sequentially calculate:
      -- . sample_cnt = g_rate_offset MOD nof_samples
      -- . sample_step = g_rate_factor MOD nof_samples
      -- This will at most take a few clock cycles. Assume the state machine will be in s_off for that long.
      if sample_cnt >= unsigned(nof_samples) then
        nxt_sample_cnt <= sample_cnt - TO_UINT(nof_samples);
      end if;
      if sample_step >= unsigned(nof_samples) then
        nxt_sample_step <= sample_step - TO_UINT(nof_samples);
      elsif state = s_init then
        init_repeat_done <= '1';
      end if;

      -- Busy
      if state = s_single or state = s_repeat then
        v_sample_incr := sample_cnt + sample_step;
        if v_sample_incr < unsigned(nof_samples) then
          nxt_sample_cnt <= v_sample_incr;
        else
          nxt_sample_cnt <= v_sample_incr - TO_UINT(nof_samples);
        end if;
      end if;
    end if;
  end process;

  -- Mode CALC
  p_phase_calculation : process(restart, init_phase_cnt, state, phase, phase_step, ctrl)
  begin
    nxt_phase          <= phase;
    nxt_phase_step     <= phase_step;
    nxt_init_phase_cnt <= init_phase_cnt;

    -- Init
    init_calc_done <= '0';
    if restart = '1' then
      nxt_phase <= (others => '0');
      nxt_phase(phase'high downto phase'high - ctrl.phase'length + 1) <= ctrl.phase;  -- initalize phase
      nxt_phase_step <= (others => '0');  -- initalize phase increment
      nxt_init_phase_cnt <= 0;
    else
      -- Determine the initial phase for each g_rate_offset and the frequency phase step. This can be done sequentially
      -- and will take g_rate_factor nof clock cycles. Assume the state machine will be in s_off for that long.
      if init_phase_cnt < g_rate_factor then
        nxt_init_phase_cnt <= init_phase_cnt + 1;
        if init_phase_cnt < g_rate_offset then
          nxt_phase <= std_logic_vector(unsigned(phase) + unsigned(ctrl.freq));  -- sequentially calculate: g_rate_offset * ctrl.freq
        end if;
        nxt_phase_step <= std_logic_vector(unsigned(phase_step) + unsigned(ctrl.freq));  -- sequentially calculate: g_rate_factor * ctrl.freq
      elsif state = s_init then
        init_calc_done <= '1';
      end if;

      -- Busy
      if state = s_calc then
        nxt_phase <= std_logic_vector(unsigned(phase) + unsigned(phase_step));
      end if;
    end if;
  end process;

  p_address_generator : process(restart, state, sample_cnt, phase)
  begin
    nxt_buf_addr <= (others => '0');  -- reset address when s_off
    nxt_buf_rden <= '0';

    if state = s_single or state = s_repeat then
      nxt_buf_addr <= std_logic_vector(to_unsigned(sample_cnt, buf_addr'length));
      nxt_buf_rden <= '1';
    elsif state = s_calc then
      nxt_buf_addr <= std_logic_vector(u_round(phase, phase'length - buf_addr'length));  -- For unsigned round the combinatorial function u_round is used.
      nxt_buf_rden <= '1';
    end if;
  end process;

  ctrl_ampl <= '0' & ctrl.ampl;

  p_mon_ctrl : process(i_mon_ctrl, ctrl, prev_state, state)
  begin
    nxt_mon_ctrl <= i_mon_ctrl;
    if TO_UINT(ctrl.mode) = c_diag_wg_mode_off then
      -- WG immediately goes into off state
      nxt_mon_ctrl <= ctrl;
    elsif prev_state = s_init and prev_state /= state then
      -- WG holds ctrl, when it goes into active state (s_single, s_repeat, or s_calc)
      nxt_mon_ctrl <= ctrl;
    end if;
    -- These MM ctrl fields always take effect immediately in all WG states
    nxt_mon_ctrl.ampl <= ctrl.ampl;
    nxt_mon_ctrl.nof_samples <= ctrl.nof_samples;
  end process;

  mult : entity common_mult_lib.common_mult
  generic map (
    g_technology       => g_technology,
    g_variant          => "RTL",
    g_in_a_w           => g_buf_dat_w,
    g_in_b_w           => c_ampl_w,
    g_out_p_w          => c_mult_w,
    g_pipeline_input   => c_mult_pipeline_input,
    g_pipeline_product => c_mult_pipeline_product,
    g_pipeline_output  => c_mult_pipeline_output
  )
  port map (
    rst        => rst,
    clk        => clk,
    in_a       => buf_rddat,
    in_b       => ctrl_ampl,
    out_p      => mult_dat
  );

  -- Skip the double-sign bit
  prod_dat <= mult_dat(c_prod_w - 1 downto 0);

  u_round : entity common_lib.common_round
  generic map(
    g_representation  => "SIGNED",
    g_round           => true,
    g_round_clip      => true,
    g_pipeline_input  => 0,
    g_pipeline_output => c_round_pipeline,
    g_in_dat_w        => c_prod_w,
    g_out_dat_w       => c_round_w
  )
  port map (
    clk        => clk,
    in_dat     => prod_dat,
    out_dat    => round_dat
  );

  u_clip : entity common_lib.common_clip
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_clip_pipeline,
    g_full_scale     => to_unsigned(c_calc_full_scale, g_calc_dat_w)
  )
  port map (
    rst      => rst,
    clk      => clk,
    in_dat   => round_dat,
    out_dat  => clip_dat,
    out_ovr  => clip_ovr
  );

  u_rdval_delay : entity common_lib.common_pipeline
  generic map (
    g_pipeline   => c_calc_pipeline,
    g_in_dat_w   => 1,
    g_out_dat_w  => 1
  )
  port map (
       rst      =>     rst,
       clk      =>     clk,
       in_clr   =>     idle,
       in_dat   => slv(buf_rdval),
    sl(out_dat) =>     buf_rdval_dly
  );

  u_sync_default_delay : entity common_lib.common_pipeline
  generic map (
    g_pipeline   => c_sync_dly,
    g_in_dat_w   => 1,
    g_out_dat_w  => 1
  )
  port map (
       rst      =>     rst,
       clk      =>     clk,
       in_clr   =>     idle,
       in_dat   => slv(init_sync),
    sl(out_dat) =>     sync_dly_default
  );

  u_sync_calc_delay : entity common_lib.common_pipeline
  generic map (
    g_pipeline   => c_calc_pipeline,
    g_in_dat_w   => 1,
    g_out_dat_w  => 1
  )
  port map (
       rst      =>     rst,
       clk      =>     clk,
       in_clr   =>     idle,
       in_dat   => slv(sync_dly_default),
    sl(out_dat) =>     sync_dly_calc
  );

  output_proc : process(state, buf_rdval_dly, clip_dat, clip_ovr, buf_rdval, buf_rddat, sync_dly_default, sync_dly_calc)
  begin
    nxt_out_dat  <= (others => '0');
    nxt_out_ovr  <= '0';
    nxt_out_val  <= '0';
    nxt_out_sync <= '0';

    if state = s_calc then
      -- Calc mode
      nxt_out_sync <= sync_dly_calc;
      if buf_rdval_dly = '1' then
        nxt_out_dat  <= RESIZE_SVEC(clip_dat, g_buf_dat_w);
        nxt_out_ovr  <= clip_ovr;
        nxt_out_val  <= '1';
      end if;
    else
      -- Other modes
      nxt_out_sync <= sync_dly_default;
      if buf_rdval = '1' then
        nxt_out_dat <= buf_rddat;
        nxt_out_val <= '1';
      end if;
    end if;
  end process;
end rtl;
