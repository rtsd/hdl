-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package aduh_pll_pkg is
  -- ADU Interface
  type t_c_aduh_pll_ai is record
    nof_sp            : natural;  -- = 4;     -- Fixed support 4 signal paths A,B,C,D, whether they contain active data depends on nof_adu
    nof_adu           : natural;  -- = 2;     -- When 2 ADUs then use all 4 ports A,B,C,D, one ADU on ports A,B and one ADU on ports C,D,
                                              -- when 1 ADU then only use ports C,D
    nof_ports         : natural;  -- = 2;     -- Fixed 2 ADC BI ports per ADU
    port_w            : natural;  -- = 8;     -- Fixed 8 bit ADC BI port width, the ADC sample width is also 8 bit
    nof_ovr           : natural;  -- = 0;     -- There is 1 overflow bit per ADU, use 0 to ignore the overflow input
    lvds_data_rate    : natural;  -- = 800;   -- The ADC sample rate is 800 Msps, so the LVDS rate is 800 Mbps per ADC BI data line,
    use_dpa           : boolean;  -- = TRUE;  -- When TRUE use LVDS_RX with DPA, else used fixed IOE delays and/or lvds_clk_phase instead of DPA
    use_lvds_clk      : boolean;  -- = TRUE;  -- When TRUE use the one or both ADC BI lvds_clk, else use the single dp_clk to capture the lvds data
    use_lvds_clk_rst  : boolean;  -- = FALSE; -- When TRUE then support reset pulse to ADU to align the lvds_clk to the dp_clk, else no support
    lvds_clk_phase    : natural;  -- = 0;     -- Use PLL phase 0 for edge aligned, phase 180 for center aligned. Only for no DPA
    nof_clocks        : natural;  -- = 2;     -- Must be <= nof_adu
                                              -- 1 --> Use ADC BI clock D or dp_clk for one or both ADU
                                              -- 2 --> Use ADC BI clock A for/from ADU-AB and clock D for/from the ADU-CD
    lvds_deser_factor : natural;  -- = 2;     -- The ADC sampled data comes in with a DDR lvds_clk, so lvds_data_rate / 2 or
                                              -- the 4 when the Data Path clock dp_clk is also used as LVDS data reference lvds_clk clock
    dp_deser_factor   : natural;  -- = 4;     -- The Data Path clock dp_clk frequency is 200 MHz, so lvds_data_rate / 4
  end record;

  constant c_aduh_pll_ai  : t_c_aduh_pll_ai := (4, 2, 2, 8, 0, 800, true, true, false, 0, 2, 2, 4);

  function func_aduh_pll_adu_dat_w(ai : t_c_aduh_pll_ai) return natural;  -- LVDS data width per ADU: two ADCs with one optional overflow bit
  function func_aduh_pll_lvds_dat_w(ai : t_c_aduh_pll_ai) return natural;  -- LVDS data width per ADUH: one or two ADUs
  function func_aduh_pll_lvdsh_dat_w(ai : t_c_aduh_pll_ai) return natural;  -- LVDS data width per LVDSH: dependent on whether one or two LVDS reference clocks are used
end aduh_pll_pkg;

package body aduh_pll_pkg is
  function func_aduh_pll_adu_dat_w(ai : t_c_aduh_pll_ai) return natural is
  begin
    -- fixed      fixed
    -- nof_ports  port_w  nov_ovr
    --     2         8       0    --> 16, two ADC with each 8 bit samples and no overflow bit, or
    --     2         8       1    --> 17, two ADC with each 8 bit samples and sharing 1 overflow bit
    return ai.nof_ports * ai.port_w + ai.nof_ovr;
  end;

  function func_aduh_pll_lvds_dat_w(ai : t_c_aduh_pll_ai) return natural is
  begin
    -- nof_adu  nov_ovr
    --    1        0      --> 16 bit
    --    1        1      --> 17 bit
    --    2        0      --> 32 bit
    --    2        1      --> 34 bit
    return ai.nof_adu * func_aduh_pll_adu_dat_w(ai);
  end;

  function func_aduh_pll_lvdsh_dat_w(ai : t_c_aduh_pll_ai) return natural is
  begin
    -- nof_adu  c_nof_clocks
    --   1           1        -->                             one LVDSH for ADU on CD --> 1 LVDSH
    --   2           1        --> one LVDSH for ADU on AB and one LVDSH for ADU on CD --> 2 LVDSH
    --   2           2        --> one LVDSH for ADU on AB and           for ADU on CD --> 1 LVDSH
    return func_aduh_pll_lvds_dat_w(ai) / ai.nof_clocks;
  end;
end aduh_pll_pkg;
