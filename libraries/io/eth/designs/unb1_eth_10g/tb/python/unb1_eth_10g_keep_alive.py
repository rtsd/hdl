
import test_case
import node_io
import os
import subprocess
import sys
import time
import threading
from shell import ssh_cmd_bkgnd
from commands import *

lcu = "dop36"
script_name = "util_dp_xonoff.py" #should be located in $UPE_GEAR/peripherals
sleep_time = 5

# Create a test case object
tc = test_case.Testcase('UTIL - ', '')
    
def remote_cmd(machine, cmd, backgnd = False):
    if backgnd:
        ssh_cmd_bkgnd(machine, cmd)        
    else:       
        cmd = 'ssh -f -f -ty'+ ' ' + machine + ' ' + cmd
        output = getstatusoutput(cmd)[1]
        retu_list = []
        for line in output.rstrip().split('\n'):
            retu_list.append(line)
        return retu_list    
  
#runs the given command line on the given lcu. when back == True, the command will run in the background and will not return any feedback 
def run_command(cmd, lcu_name = 'local', back = False):   
    if lcu_name == 'local':
        output = subprocess.Popen( cmd, stdout=subprocess.PIPE, shell=True )
        
    elif back:
        remote_cmd(lcu_name, cmd, True)
        return
    else:
        output = remote_cmd(lcu_name, cmd)   
    line_list = []
    i = 0
    while (1):
        if lcu_name == 'local':
            line = output.stdout.readline()
        else:
            if i < len(output):                   
                line = output[i] + '\n'
            else:
                break
            i += 1
        if line:
            line_list.append(line)
        else:               
            break
              
    return line_list
    

        
def set_lcupath(lcu_name):
    cmd = "ssh " + lcu_name + " 'printf $UPE_GEAR'"
    output = run_command(cmd)[0] + "/peripherals/"
    return output
    
                


unbNrs =  str(tc.unbNrs)
fnNrs = str(tc.fnNrs)
bnNrs = str(tc.bnNrs)
gpString = tc.gpString

unbNrs = unbNrs.replace("[", "")
unbNrs = unbNrs.replace("]", "")
unbNrs = unbNrs.replace(" ", "")
fnNrs  =  fnNrs.replace("[", "")
fnNrs  =  fnNrs.replace("]", "")
fnNrs  =  fnNrs.replace(" ", "")
bnNrs  =  bnNrs.replace("[", "")
bnNrs  =  bnNrs.replace("]", "")
bnNrs  =  bnNrs.replace(" ", "")

if not unbNrs == "":
    unbNrs = " --unb "+unbNrs
if not fnNrs == "": 
    fnNrs = " --fn " +fnNrs
if not bnNrs == "":
    bnNrs = " --bn " +bnNrs
if not gpString == "":
    gpString = " -s " + gpString
    

remote_script_path = set_lcupath(lcu) + script_name
cmd = 'python %s' % remote_script_path
cmd += unbNrs + fnNrs + bnNrs + " -n 2" + gpString
print cmd

def foo():
    next_call = time.time()
    while True:
     
        print run_command(cmd, lcu, False)
        
        next_call = next_call+sleep_time;
        time.sleep(next_call - time.time())
        
        
timerThread = threading.Thread(target=foo)
timerThread.daemon = True
timerThread.start()



while(1):
    time.sleep(10)