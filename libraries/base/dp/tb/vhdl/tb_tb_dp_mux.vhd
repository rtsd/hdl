-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_mux is
end tb_tb_dp_mux;

architecture tb of tb_tb_dp_mux is
  -- Use array_init() function to assign value to an unconstrained array of size 1.
  -- Simply doing:
  --   CONSTANT c_natural_arr_init : t_natural_arr := (1024);
  -- or:
  --   CONSTANT c_natural_arr_init : t_natural_arr := t_natural_arr(1024);
  -- does not work, and we do not want to constrain the array generic in the entity.

  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 10
  -- > run 300 us --> OK

  -- Try MUX settings : GENERIC MAP (g_dut_use_empty, g_dut_use_in_channel, g_dut_use_sync, g_mode, g_dut_nof_input , g_dut_use_fifo, g_dut_fifo_size, g_dut_fifo_fill)

  u_nof_1            : entity work.tb_dp_mux generic map (false, false, false, 0, 1, false, array_init(1024, 1), array_init(   0, 1));
  u_nof_2            : entity work.tb_dp_mux generic map (false, false, false, 0, 2, false, array_init(1024, 2), array_init(   0, 2));
  u_nof_3            : entity work.tb_dp_mux generic map (false, false, false, 0, 3, false, array_init(1024, 3), array_init(   0, 3));
  u_nof_1_mode_1     : entity work.tb_dp_mux generic map (false, false, false, 1, 1, false, array_init(1024, 1), array_init(   0, 1));
  u_nof_2_mode_1     : entity work.tb_dp_mux generic map (false, false, false, 1, 2, false, array_init(1024, 2), array_init(   0, 2));
  u_nof_3_mode_1     : entity work.tb_dp_mux generic map (false, false, false, 1, 3, false, array_init(1024, 3), array_init(   0, 3));
  u_nof_1_channel    : entity work.tb_dp_mux generic map (false, true,  false, 0, 1, false, array_init(1024, 1), array_init(   0, 1));
  u_nof_2_channel    : entity work.tb_dp_mux generic map (false, true,  false, 0, 2, false, array_init(1024, 2), array_init(   0, 2));
  u_nof_3_channel    : entity work.tb_dp_mux generic map (false, true,  false, 0, 3, false, array_init(1024, 3), array_init(   0, 3));
  u_nof_3_empty      : entity work.tb_dp_mux generic map (true,  false, false, 0, 3, false, array_init(1024, 3), array_init(   0, 3));
  u_nof_3_sync       : entity work.tb_dp_mux generic map (false, false, true,  0, 3, false, array_init(1024, 3), array_init(   0, 3));

  u_nof_1_fifo       : entity work.tb_dp_mux generic map (false, false, false, 0, 1, true,  array_init(1024, 1), array_init(   0, 1));
  u_nof_2_fifo       : entity work.tb_dp_mux generic map (false, false, false, 0, 2, true,  array_init(1024, 2), array_init(   0, 2));
  u_nof_3_fifo       : entity work.tb_dp_mux generic map (false, false, false, 0, 3, true,  array_init(1024, 3), array_init(   0, 3));
  u_nof_3_fifo_empty : entity work.tb_dp_mux generic map (true,  false, false, 0, 3, true,  array_init(1024, 3), array_init(   0, 3));
  u_nof_3_fifo_sync  : entity work.tb_dp_mux generic map (false, false, true,  0, 3, true,  array_init(1024, 3), array_init(   0, 3));

  -- > run 330 us --> OK
  u_nof_2_fill       : entity work.tb_dp_mux generic map (false, false, false, 0, 2, true,  array_init(1024, 2), (25, 5));
  u_nof_3_fill       : entity work.tb_dp_mux generic map (false, false, false, 0, 3, true,  array_init(1024, 3), (8, 12, 17));
  u_nof_3_fill_empty : entity work.tb_dp_mux generic map (true,  false, false, 0, 3, true,  array_init(1024, 3), (8, 12, 17));
  u_nof_3_fill_sync  : entity work.tb_dp_mux generic map (false, false, true,  0, 3, true,  array_init(1024, 3), (8, 12, 17));
end tb;
