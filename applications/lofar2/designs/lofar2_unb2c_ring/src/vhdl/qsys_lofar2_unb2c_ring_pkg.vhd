-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_lofar2_unb2c_ring_pkg is
  -----------------------------------------------------------------------------
  -- this component declaration is copy-pasted from Quartus platform designer:
  -----------------------------------------------------------------------------

    component qsys_lofar2_unb2c_ring is
        port (
            avs_eth_0_clk_export                               : out std_logic;  -- export
            avs_eth_0_irq_export                               : in  std_logic                     := 'X';  -- export
            avs_eth_0_ram_address_export                       : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_ram_read_export                          : out std_logic;  -- export
            avs_eth_0_ram_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_ram_write_export                         : out std_logic;  -- export
            avs_eth_0_ram_writedata_export                     : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reg_address_export                       : out std_logic_vector(3 downto 0);  -- export
            avs_eth_0_reg_read_export                          : out std_logic;  -- export
            avs_eth_0_reg_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_reg_write_export                         : out std_logic;  -- export
            avs_eth_0_reg_writedata_export                     : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reset_export                             : out std_logic;  -- export
            avs_eth_0_tse_address_export                       : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_tse_read_export                          : out std_logic;  -- export
            avs_eth_0_tse_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_tse_waitrequest_export                   : in  std_logic                     := 'X';  -- export
            avs_eth_0_tse_write_export                         : out std_logic;  -- export
            avs_eth_0_tse_writedata_export                     : out std_logic_vector(31 downto 0);  -- export
            clk_clk                                            : in  std_logic                     := 'X';  -- clk
            pio_pps_address_export                             : out std_logic_vector(1 downto 0);  -- export
            pio_pps_clk_export                                 : out std_logic;  -- export
            pio_pps_read_export                                : out std_logic;  -- export
            pio_pps_readdata_export                            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_pps_reset_export                               : out std_logic;  -- export
            pio_pps_write_export                               : out std_logic;  -- export
            pio_pps_writedata_export                           : out std_logic_vector(31 downto 0);  -- export
            pio_system_info_address_export                     : out std_logic_vector(4 downto 0);  -- export
            pio_system_info_clk_export                         : out std_logic;  -- export
            pio_system_info_read_export                        : out std_logic;  -- export
            pio_system_info_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_system_info_reset_export                       : out std_logic;  -- export
            pio_system_info_write_export                       : out std_logic;  -- export
            pio_system_info_writedata_export                   : out std_logic_vector(31 downto 0);  -- export
            pio_wdi_external_connection_export                 : out std_logic;  -- export
            ram_diag_bg_address_export                         : out std_logic_vector(6 downto 0);  -- export
            ram_diag_bg_clk_export                             : out std_logic;  -- export
            ram_diag_bg_read_export                            : out std_logic;  -- export
            ram_diag_bg_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_bg_reset_export                           : out std_logic;  -- export
            ram_diag_bg_write_export                           : out std_logic;  -- export
            ram_diag_bg_writedata_export                       : out std_logic_vector(31 downto 0);  -- export
            ram_scrap_address_export                           : out std_logic_vector(8 downto 0);  -- export
            ram_scrap_clk_export                               : out std_logic;  -- export
            ram_scrap_read_export                              : out std_logic;  -- export
            ram_scrap_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_scrap_reset_export                             : out std_logic;  -- export
            ram_scrap_write_export                             : out std_logic;  -- export
            ram_scrap_writedata_export                         : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_v2_ring_rx_address_export          : out std_logic_vector(9 downto 0);  -- export
            reg_bsn_monitor_v2_ring_rx_clk_export              : out std_logic;  -- export
            reg_bsn_monitor_v2_ring_rx_read_export             : out std_logic;  -- export
            reg_bsn_monitor_v2_ring_rx_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_v2_ring_rx_reset_export            : out std_logic;  -- export
            reg_bsn_monitor_v2_ring_rx_write_export            : out std_logic;  -- export
            reg_bsn_monitor_v2_ring_rx_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_v2_ring_tx_address_export          : out std_logic_vector(9 downto 0);  -- export
            reg_bsn_monitor_v2_ring_tx_clk_export              : out std_logic;  -- export
            reg_bsn_monitor_v2_ring_tx_read_export             : out std_logic;  -- export
            reg_bsn_monitor_v2_ring_tx_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_v2_ring_tx_reset_export            : out std_logic;  -- export
            reg_bsn_monitor_v2_ring_tx_write_export            : out std_logic;  -- export
            reg_bsn_monitor_v2_ring_tx_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_diag_bg_address_export                         : out std_logic_vector(2 downto 0);  -- export
            reg_diag_bg_clk_export                             : out std_logic;  -- export
            reg_diag_bg_read_export                            : out std_logic;  -- export
            reg_diag_bg_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_bg_reset_export                           : out std_logic;  -- export
            reg_diag_bg_write_export                           : out std_logic;  -- export
            reg_diag_bg_writedata_export                       : out std_logic_vector(31 downto 0);  -- export
            reg_dp_block_validate_bsn_at_sync_address_export   : out std_logic_vector(4 downto 0);  -- export
            reg_dp_block_validate_bsn_at_sync_clk_export       : out std_logic;  -- export
            reg_dp_block_validate_bsn_at_sync_read_export      : out std_logic;  -- export
            reg_dp_block_validate_bsn_at_sync_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_block_validate_bsn_at_sync_reset_export     : out std_logic;  -- export
            reg_dp_block_validate_bsn_at_sync_write_export     : out std_logic;  -- export
            reg_dp_block_validate_bsn_at_sync_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_dp_block_validate_err_address_export           : out std_logic_vector(6 downto 0);  -- export
            reg_dp_block_validate_err_clk_export               : out std_logic;  -- export
            reg_dp_block_validate_err_read_export              : out std_logic;  -- export
            reg_dp_block_validate_err_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_block_validate_err_reset_export             : out std_logic;  -- export
            reg_dp_block_validate_err_write_export             : out std_logic;  -- export
            reg_dp_block_validate_err_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_dp_xonoff_lane_address_export                  : out std_logic_vector(3 downto 0);  -- export
            reg_dp_xonoff_lane_clk_export                      : out std_logic;  -- export
            reg_dp_xonoff_lane_read_export                     : out std_logic;  -- export
            reg_dp_xonoff_lane_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_xonoff_lane_reset_export                    : out std_logic;  -- export
            reg_dp_xonoff_lane_write_export                    : out std_logic;  -- export
            reg_dp_xonoff_lane_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_dp_xonoff_local_address_export                 : out std_logic_vector(3 downto 0);  -- export
            reg_dp_xonoff_local_clk_export                     : out std_logic;  -- export
            reg_dp_xonoff_local_read_export                    : out std_logic;  -- export
            reg_dp_xonoff_local_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_xonoff_local_reset_export                   : out std_logic;  -- export
            reg_dp_xonoff_local_write_export                   : out std_logic;  -- export
            reg_dp_xonoff_local_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_ctrl_address_export                       : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_ctrl_clk_export                           : out std_logic;  -- export
            reg_dpmm_ctrl_read_export                          : out std_logic;  -- export
            reg_dpmm_ctrl_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_ctrl_reset_export                         : out std_logic;  -- export
            reg_dpmm_ctrl_write_export                         : out std_logic;  -- export
            reg_dpmm_ctrl_writedata_export                     : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_data_address_export                       : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_data_clk_export                           : out std_logic;  -- export
            reg_dpmm_data_read_export                          : out std_logic;  -- export
            reg_dpmm_data_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_data_reset_export                         : out std_logic;  -- export
            reg_dpmm_data_write_export                         : out std_logic;  -- export
            reg_dpmm_data_writedata_export                     : out std_logic_vector(31 downto 0);  -- export
            reg_epcs_address_export                            : out std_logic_vector(2 downto 0);  -- export
            reg_epcs_clk_export                                : out std_logic;  -- export
            reg_epcs_read_export                               : out std_logic;  -- export
            reg_epcs_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_epcs_reset_export                              : out std_logic;  -- export
            reg_epcs_write_export                              : out std_logic;  -- export
            reg_epcs_writedata_export                          : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_temp_sens_address_export                  : out std_logic_vector(2 downto 0);  -- export
            reg_fpga_temp_sens_clk_export                      : out std_logic;  -- export
            reg_fpga_temp_sens_read_export                     : out std_logic;  -- export
            reg_fpga_temp_sens_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_temp_sens_reset_export                    : out std_logic;  -- export
            reg_fpga_temp_sens_write_export                    : out std_logic;  -- export
            reg_fpga_temp_sens_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_voltage_sens_address_export               : out std_logic_vector(3 downto 0);  -- export
            reg_fpga_voltage_sens_clk_export                   : out std_logic;  -- export
            reg_fpga_voltage_sens_read_export                  : out std_logic;  -- export
            reg_fpga_voltage_sens_readdata_export              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_voltage_sens_reset_export                 : out std_logic;  -- export
            reg_fpga_voltage_sens_write_export                 : out std_logic;  -- export
            reg_fpga_voltage_sens_writedata_export             : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_ctrl_address_export                       : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_ctrl_clk_export                           : out std_logic;  -- export
            reg_mmdp_ctrl_read_export                          : out std_logic;  -- export
            reg_mmdp_ctrl_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_ctrl_reset_export                         : out std_logic;  -- export
            reg_mmdp_ctrl_write_export                         : out std_logic;  -- export
            reg_mmdp_ctrl_writedata_export                     : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_data_address_export                       : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_data_clk_export                           : out std_logic;  -- export
            reg_mmdp_data_read_export                          : out std_logic;  -- export
            reg_mmdp_data_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_data_reset_export                         : out std_logic;  -- export
            reg_mmdp_data_write_export                         : out std_logic;  -- export
            reg_mmdp_data_writedata_export                     : out std_logic_vector(31 downto 0);  -- export
            reg_remu_address_export                            : out std_logic_vector(2 downto 0);  -- export
            reg_remu_clk_export                                : out std_logic;  -- export
            reg_remu_read_export                               : out std_logic;  -- export
            reg_remu_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_remu_reset_export                              : out std_logic;  -- export
            reg_remu_write_export                              : out std_logic;  -- export
            reg_remu_writedata_export                          : out std_logic_vector(31 downto 0);  -- export
            reg_ring_info_address_export                       : out std_logic_vector(1 downto 0);  -- export
            reg_ring_info_clk_export                           : out std_logic;  -- export
            reg_ring_info_read_export                          : out std_logic;  -- export
            reg_ring_info_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_ring_info_reset_export                         : out std_logic;  -- export
            reg_ring_info_write_export                         : out std_logic;  -- export
            reg_ring_info_writedata_export                     : out std_logic_vector(31 downto 0);  -- export
            reg_ring_lane_info_address_export                  : out std_logic_vector(3 downto 0);  -- export
            reg_ring_lane_info_clk_export                      : out std_logic;  -- export
            reg_ring_lane_info_read_export                     : out std_logic;  -- export
            reg_ring_lane_info_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_ring_lane_info_reset_export                    : out std_logic;  -- export
            reg_ring_lane_info_write_export                    : out std_logic;  -- export
            reg_ring_lane_info_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_tr_10gbe_eth10g_address_export                 : out std_logic_vector(0 downto 0);  -- export
            reg_tr_10gbe_eth10g_clk_export                     : out std_logic;  -- export
            reg_tr_10gbe_eth10g_read_export                    : out std_logic;  -- export
            reg_tr_10gbe_eth10g_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_tr_10gbe_eth10g_reset_export                   : out std_logic;  -- export
            reg_tr_10gbe_eth10g_write_export                   : out std_logic;  -- export
            reg_tr_10gbe_eth10g_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_tr_10gbe_mac_address_export                    : out std_logic_vector(12 downto 0);  -- export
            reg_tr_10gbe_mac_clk_export                        : out std_logic;  -- export
            reg_tr_10gbe_mac_read_export                       : out std_logic;  -- export
            reg_tr_10gbe_mac_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_tr_10gbe_mac_reset_export                      : out std_logic;  -- export
            reg_tr_10gbe_mac_write_export                      : out std_logic;  -- export
            reg_tr_10gbe_mac_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            reg_wdi_address_export                             : out std_logic_vector(0 downto 0);  -- export
            reg_wdi_clk_export                                 : out std_logic;  -- export
            reg_wdi_read_export                                : out std_logic;  -- export
            reg_wdi_readdata_export                            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wdi_reset_export                               : out std_logic;  -- export
            reg_wdi_write_export                               : out std_logic;  -- export
            reg_wdi_writedata_export                           : out std_logic_vector(31 downto 0);  -- export
            reset_reset_n                                      : in  std_logic                     := 'X';  -- reset_n
            rom_system_info_address_export                     : out std_logic_vector(12 downto 0);  -- export
            rom_system_info_clk_export                         : out std_logic;  -- export
            rom_system_info_read_export                        : out std_logic;  -- export
            rom_system_info_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            rom_system_info_reset_export                       : out std_logic;  -- export
            rom_system_info_write_export                       : out std_logic;  -- export
            rom_system_info_writedata_export                   : out std_logic_vector(31 downto 0)  -- export
        );
    end component qsys_lofar2_unb2c_ring;
end qsys_lofar2_unb2c_ring_pkg;
