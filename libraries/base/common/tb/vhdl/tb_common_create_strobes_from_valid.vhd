-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Eric Kooistra
-- Purpose: Self checking and self-stopping tb for common_create_strobes_from_valid.vhd
-- Usage:
-- > as 3
-- > run -a

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_create_strobes_from_valid is
  generic (
    g_pipeline           : boolean := false;
    g_in_val_gaps        : boolean := true;
    g_nof_clk_per_sync   : natural := 10;
    g_nof_clk_per_block  : natural := 5
  );
end tb_common_create_strobes_from_valid;

architecture tb of tb_common_create_strobes_from_valid is
  constant clk_period     : time := 10 ns;

  constant c_nof_block_per_sync_max : natural := ceil_div(g_nof_clk_per_sync, g_nof_clk_per_block);
  constant c_nof_block_per_sync_min : natural := g_nof_clk_per_sync / g_nof_clk_per_block;
  constant c_fractional             : boolean := c_nof_block_per_sync_min /= c_nof_block_per_sync_max;

  constant c_nof_sync               : natural := sel_a_b(c_fractional, g_nof_clk_per_block, 1) * 3;

  signal tb_end      : std_logic := '0';
  signal rst         : std_logic := '1';
  signal clk         : std_logic := '0';
  signal in_val      : std_logic := '0';
  signal out_val     : std_logic;
  signal out_sop     : std_logic;
  signal out_eop     : std_logic;
  signal out_sync    : std_logic;
  signal out_val_cnt : natural := 0;
begin
  clk <= not clk or tb_end after clk_period / 2;

  p_in_stimuli : process
  begin
    rst <= '1';
    proc_common_wait_some_cycles(clk, 10);
    rst <= '0';
    proc_common_wait_some_cycles(clk, 10);
    for I in 0 to c_nof_sync - 1 loop
      for J in 0 to c_nof_block_per_sync_max - 1 loop
        for K in 0 to g_nof_clk_per_block - 1 loop
          if g_in_val_gaps and K = 0 then
            in_val <= '0';  -- insert a one cycle gap
            proc_common_wait_some_cycles(clk, 1);
          end if;
          in_val <= '1';
          proc_common_wait_some_cycles(clk, 1);
        end loop;
      end loop;
    end loop;
    proc_common_wait_some_cycles(clk, g_nof_clk_per_sync * 2);
    tb_end <= '1';
    wait;
  end process;

  out_val_cnt <= out_val_cnt + 1 when rising_edge(clk) and out_val = '1';

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if out_val = '1' then
        -- Verify out_eop
        if out_val_cnt mod g_nof_clk_per_block = g_nof_clk_per_block - 1 then
          assert out_eop = '1'
            report "Missing out_eop."
            severity ERROR;
        else
          assert out_eop = '0'
            report "Unexpected out_eop."
            severity ERROR;
        end if;

        -- Verify out_sop
        if out_val_cnt mod g_nof_clk_per_block = 0 then
          assert out_sop = '1'
            report "Missing out_sop."
            severity ERROR;
        else
          assert out_sop = '0'
            report "Unexpected out_sop."
            severity ERROR;
        end if;

        -- Verify out_sync
        if out_val_cnt mod g_nof_clk_per_block = 0 then
          if out_val_cnt mod g_nof_clk_per_sync <= g_nof_clk_per_block - 1 then
            assert out_sync = '1'
              report "Missing out_sync."
              severity ERROR;
          else
            assert out_sync = '0'
              report "Unexpected out_sync."
              severity ERROR;
          end if;
        else
          assert out_sync = '0'
            report "Unexpected out_sync."
            severity ERROR;
        end if;
      else
        -- Illegal strobe when out_val = '0'
        assert out_eop = '0'
          report "Illegal out_eop."
          severity ERROR;
        assert out_sop = '0'
          report "Illegal out_sop."
          severity ERROR;
        assert out_sync = '0'
          report "Illegal out_sync."
          severity ERROR;
      end if;
    end if;
  end process;

  u_in_sync : entity work.common_create_strobes_from_valid
  generic map (
    g_pipeline          => g_pipeline,
    g_nof_clk_per_sync  => g_nof_clk_per_sync,
    g_nof_clk_per_block => g_nof_clk_per_block
  )
  port map (
    rst      => rst,
    clk      => clk,
    in_val   => in_val,
    out_val  => out_val,
    out_sop  => out_sop,
    out_eop  => out_eop,
    out_sync => out_sync
  );
end tb;
