-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;

package unb1_bn_capture_pkg is
  -- Signal path input
  type t_c_bn_capture_sp is record
    sample_freq                : natural;  -- = 800;        -- The ADC sample rate is 800 Msps, so the LVDS rate is 800 Mbps per ADC BI data line,
    nof_samples_per_block      : natural;  -- = 1024;       -- = nof 8b ADC samples per block, e.g. 1024 for PFB down sample factor of 1024
    nof_samples_per_sync       : natural;  -- = 800*10**6;  -- = nof 8b ADC samples per sync interval
    monitor_buffer_nof_samples : natural;  -- = 1024;       -- = nof 8b ADC samples per monitor buffer
    monitor_buffer_use_sync    : boolean;  -- when TRUE start filling the buffer after the sync, else after the last word was read
  end record;

  constant c_bn_capture_sp : t_c_bn_capture_sp := (800, 1024, 800000 * 1024, 1024, true);  -- 800 MSps, block size 1024 samples, 800000 blocks per sync interval (= 1.024 s), monitor buffer 1024 samples using sync

  constant c_bn_capture_mm_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and dp_clk are the same, else use TRUE to cross the clock domain

  -- Aggregate BN capture constants record
  type t_c_bn_capture is record
    mm_clk_freq : natural;
    dp_clk_freq : natural;
    sp          : t_c_bn_capture_sp;
  end record;

  constant c_bn_capture : t_c_bn_capture := (c_unb1_board_mm_clk_freq_50M,  -- must match PLL setting in sopc_bn_capture
                                             c_unb1_board_ext_clk_freq_200M,
                                             c_bn_capture_sp);
end unb1_bn_capture_pkg;

package body unb1_bn_capture_pkg is
end unb1_bn_capture_pkg;
