-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Provide AXI-4-stream interfaces + standard avalon MM interfaces for
--          eth_tester.vhd such that it can be used to create a Vivado IP block.
-- Description:
-- . The rdma_generator_roce_tester_wrapper uses axi4_stream_dp_bridge to convert the dp
--   sosi/siso interfaces of the eth_tester into AXI4-Stream interfaces.
-- . In order for this component to be suitable as a Vivado IP, the ports are
--   exclusively STD_LOGIC(_VECTOR) where the widths are hard-coded as demanded
--   by the Vivado IP creator (only supports VHDL-93).
-- . * roce = RDMA Over Converged Ethernet
-- Remark
-- . Avalon is used for all MM interfaces, which can be bridged to AXI4-Lite in
--   vivado using the AXI AMM Bridge IP.

library IEEE, common_lib, dp_lib, eth_lib, rdma_icrc_external_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_components_pkg.all;
use eth_lib.eth_pkg.all;
use eth_lib.eth_tester_pkg.all;
use work.rdma_generator_pkg.all;

entity rdma_generator_roce_tester is
  port (
    -- Clocks and reset
    mm_clk             : in  std_logic;
    mm_rst             : in  std_logic;
    st_clk             : in  std_logic;
    st_rst             : in  std_logic;
    st_pps             : in  std_logic;

    tx_fifo_rd_emp_arr : out std_logic_vector(0 downto 0);

    rx_udp_sosi_arr                : in  t_dp_sosi_arr(0 downto 0) := (others => c_dp_sosi_rst);
    tx_udp_sosi_arr                : out t_dp_sosi_arr(0 downto 0) := (others => c_dp_sosi_rst);
    tx_udp_siso_arr                : in  t_dp_siso_arr(0 downto 0) := (others => c_dp_siso_rdy);

    reg_bg_ctrl_copi               : in  t_mem_copi := c_mem_copi_rst;
    reg_bg_ctrl_cipo               : out t_mem_cipo;
    reg_hdr_dat_copi               : in  t_mem_copi := c_mem_copi_rst;
    reg_hdr_dat_cipo               : out t_mem_cipo;
    reg_bsn_monitor_v2_tx_copi     : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_tx_cipo     : out t_mem_cipo;
    reg_strobe_total_count_tx_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_strobe_total_count_tx_cipo : out t_mem_cipo;
    reg_dp_split_copi              : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_split_cipo              : out t_mem_cipo;

    reg_bsn_monitor_v2_rx_copi     : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_rx_cipo     : out t_mem_cipo;
    reg_strobe_total_count_rx_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_strobe_total_count_rx_cipo : out t_mem_cipo;

    eth_src_mac        : in  std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    ip_src_addr        : in  std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    udp_src_port       : in  std_logic_vector(c_network_udp_port_w - 1 downto 0)

   );
end rdma_generator_roce_tester;

architecture str of rdma_generator_roce_tester is
  constant c_nof_byte                   : natural := c_rdma_generator_nof_octet_output_100gbe;
  constant c_reverse_byte_order         : boolean := false;
  signal eth_tester_tx_sosi_arr         : t_dp_sosi_arr(0 downto 0) := (others => c_dp_sosi_rst);
  signal eth_tester_tx_siso_arr         : t_dp_siso_arr(0 downto 0) := (others => c_dp_siso_rdy);

begin

  u_eth_tester : entity eth_lib.eth_tester
  generic map (
    g_nof_octet_generate => c_rdma_generator_nof_octet_generate_100gbe,
    g_nof_octet_output   => c_rdma_generator_nof_octet_output_100gbe,
    g_use_eth_header     => false,
    g_use_ip_udp_header  => true,
    g_use_dp_header      => false,
    g_hdr_calc_ip_crc    => true,
    g_hdr_field_arr      => c_rdma_generator_roce_hdr_field_arr,
    g_hdr_field_sel      => c_rdma_generator_roce_hdr_field_sel,
    -- Add icrc length here as g_hdr_app_len is used to calculate the total packet length.
    g_hdr_app_len        => c_rdma_generator_roce_hdr_len + c_rdma_generator_roce_icrc_len,
    g_remove_crc         => false
  )
  port map (
    -- Clocks and reset
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,
    st_rst             => st_rst,
    st_clk             => st_clk,
    st_pps             => st_pps,

    -- UDP transmit interface
    eth_src_mac        => eth_src_mac,
    ip_src_addr        => ip_src_addr,
    udp_src_port       => udp_src_port,

    tx_fifo_rd_emp_arr => tx_fifo_rd_emp_arr,

    tx_udp_sosi_arr    => eth_tester_tx_sosi_arr,
    tx_udp_siso_arr    => eth_tester_tx_siso_arr,

    -- UDP receive interface
    rx_udp_sosi_arr    => rx_udp_sosi_arr,

    -- Memory Mapped Slaves (one per stream)
    reg_bg_ctrl_copi               => reg_bg_ctrl_copi,
    reg_bg_ctrl_cipo               => reg_bg_ctrl_cipo,
    reg_hdr_dat_copi               => reg_hdr_dat_copi,
    reg_hdr_dat_cipo               => reg_hdr_dat_cipo,
    reg_bsn_monitor_v2_tx_copi     => reg_bsn_monitor_v2_tx_copi,
    reg_bsn_monitor_v2_tx_cipo     => reg_bsn_monitor_v2_tx_cipo,
    reg_strobe_total_count_tx_copi => reg_strobe_total_count_tx_copi,
    reg_strobe_total_count_tx_cipo => reg_strobe_total_count_tx_cipo,
    reg_dp_split_copi              => reg_dp_split_copi,
    reg_dp_split_cipo              => reg_dp_split_cipo,

    reg_bsn_monitor_v2_rx_copi     => reg_bsn_monitor_v2_rx_copi,
    reg_bsn_monitor_v2_rx_cipo     => reg_bsn_monitor_v2_rx_cipo,
    reg_strobe_total_count_rx_copi => reg_strobe_total_count_rx_copi,
    reg_strobe_total_count_rx_cipo => reg_strobe_total_count_rx_cipo
  );

  u_icrc_append : entity rdma_icrc_external_lib.append_crc_dp_wrapper
  port map (
    dp_clk => st_clk,
    dp_rst => st_rst,

    out_dp_sosi => tx_udp_sosi_arr(0),
    out_dp_siso => tx_udp_siso_arr(0),
    in_dp_sosi  => eth_tester_tx_sosi_arr(0),
    in_dp_siso  => eth_tester_tx_siso_arr(0)
  );
end str;
