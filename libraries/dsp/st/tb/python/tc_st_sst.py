#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for the st_sst entity.

   Description:

   This testcase is used in conjunction with the tb_mmf_ss_sst.vhd file.

   Generics for the VHDL testbench are passed to modelsim to configure the
   current simulation. This is done by the auto_sim script in $UNB/Firmware/sim/python
   To observe the VHDL signals in the wave window modelsim needs to be
   started manually and then this script must be started manually. Note that the 
   constants and generics also have to be set manually the. 

   Usage:

   > python tc_st_sst.py --unb 0 --bn 0 --sim 

"""

###############################################################################
# System imports
import test_case
import node_io
import unb_apertif as apr
import pi_diag_block_gen
import pi_diag_data_buffer
import pi_st_sst
import dsp_test 
import time
import sys
import pylab as pl
import numpy as np
import scipy as sp
import random
from tools import *
from common import *

###############################################################################

# Create a test case object
tc = test_case.Testcase('TB - ', '')

# Constants/Generics that are shared between VHDL and Python
# Constants            Defaults Description
# START_VHDL_GENERICS
c_nof_stat      = 8     #512    # The number of statistics, or the framesize of the input frames
c_xst_enable    = True   #TRUE   # Enable signal for cross correlation functionality. When enabled there is memory allocated for the imaginary part.
c_in_data_w     = 16     #18     # The data width of the input data
c_stat_data_w   = 56     #56     # The data width of accumulated statistics.
c_stat_data_sz  = 2      #2      # The number of registers that one accumulated statistic value spans. 
c_nof_instances = 4      #1      # The number of statistic modules that are placed in parallel
c_nof_frames    = 1      #1      # Number of frames to be captured
# END_VHDL_GENERICS

tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test bench for st_sst unit' )
tc.append_log(3, '>>>')
tc.append_log(3, '')
tc.set_result('PASSED')

#############################
# Python specific constants #
#############################

# Block Generator
c_bg_nof_streams     = c_nof_instances
c_bg_ram_size        = c_nof_frames*2**ceil_log2(c_nof_stat)

c_stimuli_length     = c_nof_frames*c_nof_stat
c_blocks_per_sync    = 64
c_treshold           = 3

# Create access object for nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

# Create block generator instance
bg = pi_diag_block_gen.PiDiagBlockGen(tc, io, c_bg_nof_streams, tc.nodeBnNrs, ramSizePerChannel=c_bg_ram_size)

# Create dsp_test instance for helpful methods
dsp_test = dsp_test.DspTest(c_stimuli_length, c_in_data_w, c_in_data_w)

# Create subband select reorder instance                                                                                   
st_sst = pi_st_sst.PiStSst(tc, io, nofInstances=c_nof_instances, nofStats=c_nof_stat, statDataWidth = c_stat_data_w, nofRegsPerStat=c_stat_data_sz, xstEnable=c_xst_enable, nodeNr=tc.nodeBnNrs)

###############################################################################
#
# Set the treshold register of the st_xst's
#
###############################################################################
for i in range(c_bg_nof_streams):
    st_sst.write_treshold([c_treshold], i)

###############################################################################
#
# Create and write the stimuli to the BG
#
###############################################################################
# Prepare stimuli for block generator
x_re_arr    = []
x_im_arr    = []  
bg_data_arr = []

for i in range(c_bg_nof_streams):
    x_re_stream = []
    x_im_stream = []
    for j in range(c_stimuli_length):
        x_re_stream.append(j*c_bg_nof_streams + i+1)
        x_im_stream.append(j*c_bg_nof_streams - i-15)
    x_re_arr.append(x_re_stream)
    x_im_arr.append(x_im_stream)
    bg_data_arr.append(dsp_test.concatenate_two_lists(x_re_stream, x_im_stream, c_in_data_w))  

# Write setting for the block generator:
bg.write_block_gen_settings(samplesPerPacket=c_nof_stat, blocksPerSync=c_blocks_per_sync, gapSize=0, memLowAddr=0, memHighAddr=c_stimuli_length-1, BSNInit=0)

# Write the stimuli to the block generator and enable the block generator
for i in range(c_bg_nof_streams):
    bg.write_waveform_ram(data=bg_data_arr[i], channelNr= i)

###############################################################################
#
# Enable the block generator
#
###############################################################################
bg.write_enable()

###############################################################################
#
# Calculate the reference values
#
###############################################################################
ref_stats_arr = []
for i in range(c_nof_instances):
    ref_stats = []
    for j in range(c_stimuli_length):
        if(j==0 or (j % (c_treshold+1) == 0) or c_treshold==0): 
            in_a = complex(x_re_arr[i][j], x_im_arr[i][j])
        in_b = complex(x_re_arr[i][j], x_im_arr[i][j])
        ref_stats.append(c_blocks_per_sync * (in_a * in_b.conjugate()))
    ref_stats_arr.append(ref_stats)

for i in ref_stats_arr:
    print i

time.sleep(5)

###############################################################################
#
# Read statistics form the st_xst's
#
###############################################################################
stats = []
for i in range(c_bg_nof_streams):
    stats.append(st_sst.read_and_verify_stats(ref_stats_arr[i], i))

###############################################################################
# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(3, '>>>')

sys.exit(tc.get_result())
