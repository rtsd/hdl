--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.dp_packet_pkg.all;

-- Purpose: Decode DP packet to DP sosi.
-- Description:
--
--   From an input DP packet:
--
--    ----------------------------------------------
--    |chan| bsn        | data ...             |err|
--    ----------------------------------------------
--      sop                                     eop
--      val  val   val    val ...               val
--                                              err, when g_use_phy_err = TRUE
--
--   extract the block of output SOSI data:
--
--                      -----------------------
--                      | data ...            |
--                      -----------------------
--                        sop             eop
--                        val   val  ...  val
--                        sync
--                        bsn
--                        channel
--                                        empty
--                                        err
--
--   The data length (eop) and empty do not show as fields in the packet
--   because they are implitely known per channel.
--
--   The dp_packet_enc does the reverse.
--
--   The g_channel_lo parameter allows using the LSbit channel fields to be
--   used for defining different groups of DP packet streams. This is e.g. used
--   to map these groups to corresponding 'tlen' types in case of transport
--   went by using UTH packets over a transceiver link.
--
-- Remark:
-- . This dp_packet_dec has a similar structure as uth_rx(rtl_hold) for the
--   flow control and as dp_packet_enc for the states. However dp_packet_dec
--   uses the sop and eop so it does not (need to) know the DP packet nof
--   data. Therefor it uses dp_shiftreg to adjust the output eop to the last
--   payload data instead of at the last err-field data.
-- . When the DP packet comes from a PHY link then it may have gotten corrupted
--   contents so then it is useful to set g_use_phy_err to TRUE. The PHY
--   error status is based on the received CRC and fits in 1 bit. The
--   g_phy_err_bi defines where the PHY error bit is put in src_out.err in
--   case g_use_phy_err = TRUE.
-- . The output SOSI sync is defined such that it can only be active at sop, so
--   not during entire frame or even until the next sop.

entity dp_packet_dec is
  generic (
    g_data_w        : natural := 32;
    g_channel_lo    : natural := 0;
    g_phy_err_bi    : natural := 0;  -- bit index in src_out.err for where to put the PHY snk_in.err bit when g_use_phy_err=TRUE, else not used
    g_use_phy_err   : boolean := false;  -- when FALSE pass on the DP packet err field, else merge the PHY snk_in.err field with the DP packet err field to get the output src_out.err.
    g_use_this_siso : boolean := true  -- default use TRUE for best throughput performance
  );
  port (
    rst       : in  std_logic;
    clk       : in  std_logic;
    -- ST sinks
    snk_out   : out t_dp_siso;
    snk_in    : in  t_dp_sosi;
    -- ST source
    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end dp_packet_dec;

architecture rtl of dp_packet_dec is
  constant c_output_reg     : boolean := true;  -- register dp_shiftreg output to ease timing closure

  constant c_channel_len    : natural := ceil_div(c_dp_packet_channel_w, g_data_w);
  constant c_bsn_len        : natural := ceil_div(c_dp_packet_bsn_w,     g_data_w);
  constant c_error_len      : natural := ceil_div(c_dp_packet_error_w,   g_data_w);
  constant c_len_max        : natural := c_bsn_len;  -- the BSN is the widest field

  constant c_channel_vec_w  : natural := c_channel_len * g_data_w;
  constant c_bsn_vec_w      : natural := c_bsn_len     * g_data_w;
  constant c_error_vec_w    : natural := c_error_len   * g_data_w;

  constant c_shiftreg_len   : natural := c_error_len + 1;

  type t_state is (s_sop, s_channel, s_bsn, s_data_sop, s_data_eop);

  -- declare data field slv signals to ease debugging in wave window
  signal in_data          : std_logic_vector(g_data_w - 1 downto 0);
  signal buf_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal blk_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal out_data         : std_logic_vector(g_data_w - 1 downto 0);

  signal state            : t_state;
  signal nxt_state        : t_state;

  signal cnt              : natural range 0 to c_len_max - 1;
  signal nxt_cnt          : natural;

  signal channel          : std_logic_vector(c_channel_vec_w - 1 downto 0);
  signal nxt_channel      : std_logic_vector(c_channel_vec_w - 1 downto 0);
  signal bsn              : std_logic_vector(c_bsn_vec_w - 1 downto 0);
  signal nxt_bsn          : std_logic_vector(c_bsn_vec_w - 1 downto 0);
  signal src_err          : std_logic_vector(c_error_vec_w - 1 downto 0);

  signal this_siso        : t_dp_siso;
  signal hold_src_in      : t_dp_siso;
  signal src_buf          : t_dp_sosi;
  signal nxt_src_buf      : t_dp_sosi;
  signal next_src_buf     : t_dp_sosi;

  signal blk_sosi         : t_dp_sosi;
  signal nxt_blk_sosi     : t_dp_sosi;
  signal cur_src_out_inputs : t_dp_sosi_arr(0 to c_shiftreg_len - 1);
  signal new_src_out_inputs : t_dp_sosi_arr(0 to c_shiftreg_len - 1);

  signal i_src_out        : t_dp_sosi;
begin
  src_out <= i_src_out;

  -- Map to slv to ease monitoring in wave window
  in_data     <= snk_in.data(g_data_w - 1 downto 0);
  buf_data    <= src_buf.data(g_data_w - 1 downto 0);
  blk_data    <= blk_sosi.data(g_data_w - 1 downto 0);
  out_data    <= i_src_out.data(g_data_w - 1 downto 0);

  -- Input flow control
  snk_out <= this_siso when g_use_this_siso = true else src_in;

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      channel        <= (others => '0');
      bsn            <= (others => '0');
      src_buf        <= c_dp_sosi_rst;
      blk_sosi       <= c_dp_sosi_rst;
      state          <= s_channel;
      cnt            <= c_channel_len - 1;
    elsif rising_edge(clk) then
      channel        <= nxt_channel;
      bsn            <= nxt_bsn;
      src_buf        <= nxt_src_buf;
      blk_sosi       <= nxt_blk_sosi;
      state          <= nxt_state;
      cnt            <= nxt_cnt;
    end if;
  end process;

  -- Hold input register
  nxt_src_buf <= next_src_buf;

  u_hold : entity work.dp_hold_input
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => OPEN,
    snk_in       => snk_in,
    -- ST source
    src_in       => hold_src_in,
    next_src_out => next_src_buf,
    pend_src_out => OPEN,
    src_out_reg  => src_buf
  );

  -- State machine
  p_state : process(state, src_in, channel, bsn, cnt, blk_sosi, next_src_buf)
    variable v_in_channel   : std_logic_vector(g_channel_lo - 1 downto 0);
    variable v_channel_data : std_logic_vector(g_data_w - 1 downto 0);
    variable v_bsn_data     : std_logic_vector(g_data_w - 1 downto 0);
  begin
    v_in_channel   := next_src_buf.channel(g_channel_lo - 1 downto 0);
    v_channel_data := RESIZE_UVEC(next_src_buf.data, g_data_w);
    v_bsn_data     := RESIZE_UVEC(next_src_buf.data, g_data_w);

    nxt_state          <= state;
    this_siso          <= src_in;
    hold_src_in        <= src_in;

    nxt_channel        <= channel;
    nxt_bsn            <= bsn;
    nxt_cnt            <= cnt;

    nxt_blk_sosi       <= blk_sosi;  -- blk_sosi outputs payload data and err field, the err field gets stripped later on
    nxt_blk_sosi.sync  <= '0';
    nxt_blk_sosi.valid <= '0';
    nxt_blk_sosi.sop   <= '0';
    nxt_blk_sosi.eop   <= '0';

    case state is
      when s_sop =>
        this_siso   <= c_dp_siso_rdy;
        hold_src_in <= c_dp_siso_rdy;
        if next_src_buf.sop = '1' then
          nxt_channel((cnt + 1) * g_data_w - 1 downto cnt * g_data_w) <= v_channel_data;
          if cnt = 0 then
            nxt_cnt <= c_bsn_len - 1;
            nxt_state <= s_bsn;
          else
            nxt_cnt <= cnt - 1;
            nxt_state <= s_channel;
          end if;
        end if;
      when s_channel =>
        this_siso   <= c_dp_siso_rdy;
        hold_src_in <= c_dp_siso_rdy;
        if next_src_buf.valid = '1' then
          nxt_channel((cnt + 1) * g_data_w - 1 downto cnt * g_data_w) <= v_channel_data;
          if cnt = 0 then
            nxt_cnt <= c_bsn_len - 1;
            nxt_state <= s_bsn;
          else
            nxt_cnt <= cnt - 1;
          end if;
        end if;
      when s_bsn =>
        this_siso   <= c_dp_siso_rdy;
        hold_src_in <= c_dp_siso_rdy;
        if next_src_buf.valid = '1' then
          nxt_bsn((cnt + 1) * g_data_w - 1 downto cnt * g_data_w) <= v_bsn_data;
          if cnt = 0 then
            this_siso <= src_in;  -- maintain active hold_src_in and now use the src_in for this_siso
            nxt_cnt <= c_channel_len - 1;  -- prepare cnt for next DP packet, the error field c_error_len for this DP packet gets handled by dp_shiftreg
            nxt_state <= s_data_sop;
          else
            nxt_cnt <= cnt - 1;
          end if;
        end if;
      when s_data_sop =>
        if next_src_buf.valid = '1' then  -- first payload data
          nxt_blk_sosi.data    <= next_src_buf.data;
          nxt_blk_sosi.valid   <= '1';
          nxt_blk_sosi.sop     <= '1';
          nxt_blk_sosi.channel <= RESIZE_DP_CHANNEL(channel & v_in_channel);
          nxt_blk_sosi.sync    <= bsn(bsn'high);  -- support SOSI sync via MSbit of BSN field (sync active only at sop),
          nxt_blk_sosi.bsn     <= RESIZE_DP_BSN(bsn(bsn'high - 1 downto 0));  -- the rest of the BSN field transports the SOSI BSN
          nxt_blk_sosi.err     <= TO_DP_ERROR(0);  -- initialy assume block has no error
          nxt_state <= s_data_eop;  -- no need to check for eop, because DP packet has at least 1 data word and an error field, so eop can not occur yet
        end if;
      when others =>  -- s_data_eop
        if next_src_buf.valid = '1' then  -- rest of payload data and the error field
          nxt_blk_sosi.data  <= next_src_buf.data;
          nxt_blk_sosi.valid <= '1';
          if next_src_buf.eop = '1' then
            nxt_blk_sosi.eop <= '1';
            nxt_blk_sosi.err <= next_src_buf.err;  -- the sosi error field contains the one bit PHY error status
            nxt_state <= s_sop;
          end if;
        end if;
    end case;

    -- Pass on frame level flow control
    this_siso.xon   <= src_in.xon;
    hold_src_in.xon <= src_in.xon;
  end process;

  -- Handle output error field and eop
  u_src_shift : entity work.dp_shiftreg
  generic map (
    g_output_reg     => c_output_reg,
    g_flush_eop      => true,
    g_modify_support => true,
    g_nof_words      => c_shiftreg_len
  )
  port map (
    rst                 => rst,
    clk                 => clk,
    -- ST sink
    snk_out             => OPEN,
    snk_in              => blk_sosi,
    -- Control shift register contents
    cur_shiftreg_inputs => cur_src_out_inputs,
    new_shiftreg_inputs => new_src_out_inputs,
    -- ST source
    src_in              => src_in,
    src_out             => i_src_out
  );

  p_src_err : process(cur_src_out_inputs)
  begin
    for I in 0 to c_error_len - 1 loop
      src_err((I + 1) * g_data_w - 1 downto I * g_data_w) <= RESIZE_UVEC(cur_src_out_inputs(I).data, g_data_w);
    end loop;
  end process;

  p_src_out : process(cur_src_out_inputs, src_err)
  begin
    -- default
    new_src_out_inputs <= cur_src_out_inputs;

    -- handle the SOSI err field
    if cur_src_out_inputs(0).eop = '1' then
      -- strip the error data words by clearing their control
      for I in 0 to c_error_len - 1 loop
        new_src_out_inputs(I).valid <= '0';
        new_src_out_inputs(I).sync <= '0';
      end loop;
      -- clear the eop at the last error data word
      new_src_out_inputs(0).eop <= '0';
      -- move the eop and move the error data word to the SOSI error field at last payload data
      new_src_out_inputs(c_shiftreg_len - 1).eop <= '1';
      new_src_out_inputs(c_shiftreg_len - 1).err <= RESIZE_DP_ERROR(src_err);  -- output transported DP packet err field
      if g_use_phy_err = true then
        new_src_out_inputs(c_shiftreg_len - 1).err(g_phy_err_bi) <= cur_src_out_inputs(0).err(0);  -- merge one bit PHY error status at bit index g_phy_err_bi
      end if;
    end if;
  end process;
end rtl;
