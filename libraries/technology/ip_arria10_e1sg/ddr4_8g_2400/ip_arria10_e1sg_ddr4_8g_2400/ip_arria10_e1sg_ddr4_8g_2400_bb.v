module ip_arria10_e1sg_ddr4_8g_2400 (
		output wire         amm_ready_0,         //     ctrl_amm_avalon_slave_0.waitrequest_n
		input  wire         amm_read_0,          //                            .read
		input  wire         amm_write_0,         //                            .write
		input  wire [26:0]  amm_address_0,       //                            .address
		output wire [575:0] amm_readdata_0,      //                            .readdata
		input  wire [575:0] amm_writedata_0,     //                            .writedata
		input  wire [6:0]   amm_burstcount_0,    //                            .burstcount
		input  wire [71:0]  amm_byteenable_0,    //                            .byteenable
		output wire         amm_readdatavalid_0, //                            .readdatavalid
		output wire         emif_usr_clk,        //   emif_usr_clk_clock_source.clk
		output wire         emif_usr_reset_n,    // emif_usr_reset_reset_source.reset_n
		input  wire         global_reset_n,      //     global_reset_reset_sink.reset_n
		output wire [1:0]   mem_ck,              //             mem_conduit_end.mem_ck
		output wire [1:0]   mem_ck_n,            //                            .mem_ck_n
		output wire [16:0]  mem_a,               //                            .mem_a
		output wire [0:0]   mem_act_n,           //                            .mem_act_n
		output wire [1:0]   mem_ba,              //                            .mem_ba
		output wire [1:0]   mem_bg,              //                            .mem_bg
		output wire [1:0]   mem_cke,             //                            .mem_cke
		output wire [1:0]   mem_cs_n,            //                            .mem_cs_n
		output wire [1:0]   mem_odt,             //                            .mem_odt
		output wire [0:0]   mem_reset_n,         //                            .mem_reset_n
		output wire [0:0]   mem_par,             //                            .mem_par
		input  wire [0:0]   mem_alert_n,         //                            .mem_alert_n
		inout  wire [8:0]   mem_dqs,             //                            .mem_dqs
		inout  wire [8:0]   mem_dqs_n,           //                            .mem_dqs_n
		inout  wire [71:0]  mem_dq,              //                            .mem_dq
		inout  wire [8:0]   mem_dbi_n,           //                            .mem_dbi_n
		input  wire         oct_rzqin,           //             oct_conduit_end.oct_rzqin
		input  wire         pll_ref_clk,         //      pll_ref_clk_clock_sink.clk
		output wire         local_cal_success,   //          status_conduit_end.local_cal_success
		output wire         local_cal_fail       //                            .local_cal_fail
	);
endmodule

