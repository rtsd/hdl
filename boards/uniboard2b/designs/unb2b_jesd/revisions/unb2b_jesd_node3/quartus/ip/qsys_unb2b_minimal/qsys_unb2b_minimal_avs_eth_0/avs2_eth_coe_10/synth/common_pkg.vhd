-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Eric Kooistra
-- Purpose:
-- . Collection of commonly used base funtions
-- Interface:
-- . [n/a]
-- Description:
-- . This is a package containing generic constants and functions.
-- . More information can be found in the comments near the code.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

package common_pkg is
  -- CONSTANT DECLARATIONS ----------------------------------------------------

  -- some integers
  constant c_0                    : natural := 0;
  constant c_zero                 : natural := 0;
  constant c_1                    : natural := 1;
  constant c_one                  : natural := 1;
  constant c_2                    : natural := 2;
  constant c_4                    : natural := 4;
  constant c_quad                 : natural := 4;
  constant c_8                    : natural := 8;
  constant c_16                   : natural := 16;
  constant c_32                   : natural := 32;
  constant c_64                   : natural := 64;
  constant c_128                  : natural := 128;
  constant c_256                  : natural := 256;

  -- widths and sizes
  constant c_halfword_sz          : natural := 2;
  constant c_word_sz              : natural := 4;
  constant c_longword_sz          : natural := 8;
  constant c_nibble_w             : natural := 4;
  constant c_byte_w               : natural := 8;
  constant c_octet_w              : natural := 8;
  constant c_halfword_w           : natural := c_byte_w * c_halfword_sz;
  constant c_word_w               : natural := c_byte_w * c_word_sz;
  constant c_integer_w            : natural := 32;  -- unfortunately VHDL integer type is limited to 32 bit values
  constant c_natural_w            : natural := c_integer_w - 1;  -- unfortunately VHDL natural type is limited to 31 bit values (0 and the positive subset of the VHDL integer type0
  constant c_longword_w           : natural := c_byte_w * c_longword_sz;

  -- logic
  constant c_sl0                  : std_logic := '0';
  constant c_sl1                  : std_logic := '1';
  constant c_unsigned_0           : unsigned(0 downto 0) := to_unsigned(0, 1);
  constant c_unsigned_1           : unsigned(0 downto 0) := to_unsigned(1, 1);
  constant c_signed_0             : signed(1 downto 0) := to_signed(0, 2);
  constant c_signed_1             : signed(1 downto 0) := to_signed(1, 2);
  constant c_slv0                 : std_logic_vector(255 downto 0) := (others => '0');
  constant c_slv1                 : std_logic_vector(255 downto 0) := (others => '1');
  constant c_word_01              : std_logic_vector(31 downto 0) := "01010101010101010101010101010101";
  constant c_word_10              : std_logic_vector(31 downto 0) := "10101010101010101010101010101010";
  constant c_slv01                : std_logic_vector(255 downto 0) := c_word_01 & c_word_01 & c_word_01 & c_word_01 & c_word_01 & c_word_01 & c_word_01 & c_word_01;
  constant c_slv10                : std_logic_vector(255 downto 0) := c_word_10 & c_word_10 & c_word_10 & c_word_10 & c_word_10 & c_word_10 & c_word_10 & c_word_10;

  -- math
  constant c_nof_complex          : natural := 2;  -- Real and imaginary part of complex number
  constant c_sign_w               : natural := 1;  -- Sign bit, can be used to skip one of the double sign bits of a product
  constant c_sum_of_prod_w        : natural := 1;  -- Bit growth for sum of 2 products, can be used in case complex multiply has normalized real and imag inputs instead of normalized amplitude inputs

  -- FF, block RAM, FIFO
  constant c_meta_delay_len       : natural := 3;  -- default nof flipflops (FF) in meta stability recovery delay line (e.g. for clock domain crossing)
  constant c_meta_fifo_depth      : natural := 16;  -- default use 16 word deep FIFO to cross clock domain, typically > 2*c_meta_delay_len or >~ 8 is enough

  constant c_bram_m9k_nof_bits    : natural := 1024 * 9;  -- size of 1 Altera M9K block RAM in bits
  constant c_bram_m9k_max_w       : natural := 36;  -- maximum width of 1 Altera M9K block RAM, so the size is then 256 words of 36 bits
  constant c_bram_m9k_fifo_depth  : natural := c_bram_m9k_nof_bits / c_bram_m9k_max_w;  -- using a smaller FIFO depth than this leaves part of the RAM unused

  constant c_fifo_afull_margin    : natural := 4;  -- default or minimal FIFO almost full margin

  -- DSP
  constant c_dsp_mult_w           : natural := 18;  -- Width of the embedded multipliers in Stratix IV

  -- TYPE DECLARATIONS --------------------------------------------------------
  type t_boolean_arr     is array (integer range <>) of boolean;  -- INTEGER left index starts default at -2**31
  type t_integer_arr     is array (integer range <>) of integer;  -- INTEGER left index starts default at -2**31
  type t_natural_arr     is array (integer range <>) of natural;  -- INTEGER left index starts default at -2**31
  type t_nat_boolean_arr is array (natural range <>) of boolean;  -- NATURAL left index starts default at 0
  type t_nat_integer_arr is array (natural range <>) of integer;  -- NATURAL left index starts default at 0
  type t_nat_natural_arr is array (natural range <>) of natural;  -- NATURAL left index starts default at 0
  type t_sl_arr          is array (integer range <>) of std_logic;
  type t_slv_1_arr       is array (integer range <>) of std_logic_vector(0 downto 0);
  type t_slv_2_arr       is array (integer range <>) of std_logic_vector(1 downto 0);
  type t_slv_4_arr       is array (integer range <>) of std_logic_vector(3 downto 0);
  type t_slv_8_arr       is array (integer range <>) of std_logic_vector(7 downto 0);
  type t_slv_12_arr      is array (integer range <>) of std_logic_vector(11 downto 0);
  type t_slv_16_arr      is array (integer range <>) of std_logic_vector(15 downto 0);
  type t_slv_18_arr      is array (integer range <>) of std_logic_vector(17 downto 0);
  type t_slv_24_arr      is array (integer range <>) of std_logic_vector(23 downto 0);
  type t_slv_32_arr      is array (integer range <>) of std_logic_vector(31 downto 0);
  type t_slv_44_arr      is array (integer range <>) of std_logic_vector(43 downto 0);
  type t_slv_48_arr      is array (integer range <>) of std_logic_vector(47 downto 0);
  type t_slv_64_arr      is array (integer range <>) of std_logic_vector(63 downto 0);
  type t_slv_128_arr     is array (integer range <>) of std_logic_vector(127 downto 0);
  type t_slv_256_arr     is array (integer range <>) of std_logic_vector(255 downto 0);
  type t_slv_512_arr     is array (integer range <>) of std_logic_vector(511 downto 0);
  type t_slv_1024_arr    is array (integer range <>) of std_logic_vector(1023 downto 0);

  constant c_boolean_arr     : t_boolean_arr     := (true, false);  -- array all possible values that can be iterated over
  constant c_nat_boolean_arr : t_nat_boolean_arr := (true, false);  -- array all possible values that can be iterated over

  type t_integer_matrix is array (integer range <>, integer range <>) of integer;
  type t_boolean_matrix is array (integer range <>, integer range <>) of boolean;
  type t_sl_matrix      is array (integer range <>, integer range <>) of std_logic;
  type t_slv_8_matrix   is array (integer range <>, integer range <>) of std_logic_vector(7 downto 0);
  type t_slv_16_matrix  is array (integer range <>, integer range <>) of std_logic_vector(15 downto 0);
  type t_slv_32_matrix  is array (integer range <>, integer range <>) of std_logic_vector(31 downto 0);
  type t_slv_64_matrix  is array (integer range <>, integer range <>) of std_logic_vector(63 downto 0);

  type t_natural_2arr_2 is array (integer range <>) of t_natural_arr(1 downto 0);

  -- STRUCTURE DECLARATIONS ---------------------------------------------------

  -- Clock and Reset
  --
  -- . rst   = Reset. Can be used asynchronously to take effect immediately
  --           when used before the clk'EVENT section. May also be used as
  --           synchronous reset using it as first condition in the clk'EVENT
  --           section. As synchronous reset it requires clock activity to take
  --           effect. A synchronous rst may or may not depend on clken,
  --           however typically rst should take priority over clken.
  -- . clk   = Clock. Used in clk'EVENT line via rising_edge(clk) or sometimes
  --           as falling_edge(clk).
  -- . clken = Clock Enable. Used for the whole clk'EVENT section.
  type t_sys_rce is record
    rst   : std_logic;
    clk   : std_logic;
    clken : std_logic;  -- := '1';
  end record;

  type t_sys_ce is record
    clk   : std_logic;
    clken : std_logic;  -- := '1';
  end record;

  -- FUNCTION DECLARATIONS ----------------------------------------------------

  -- All functions assume [high downto low] input ranges

  function pow2(n : natural) return natural;  -- = 2**n
  function ceil_pow2(n : integer) return natural;  -- = 2**n, returns 1 for n<0

  function true_log2(n : natural) return natural;  -- true_log2(n) = log2(n)
  function ceil_log2(n : natural) return natural;  -- ceil_log2(n) = log2(n), but force ceil_log2(1) = 1

  function floor_log10(n : natural) return natural;

  function is_pow2(n : natural) return boolean;  -- return TRUE when n is a power of 2, so 0, 1, 2, 4, 8, 16, ...
  function true_log_pow2(n : natural) return natural;  -- 2**true_log2(n), return power of 2 that is >= n

  function ratio( n, d : natural) return natural;  -- return n/d when n MOD d = 0 else return 0, so ratio * d = n only when integer ratio > 0
  function ratio2(n, m : natural) return natural;  -- return integer ratio of n/m or m/n, whichever is the largest

  function ceil_div(   n, d : natural)  return natural;  -- ceil_div    = n/d + (n MOD d)/=0
  function ceil_value( n, d : natural)  return natural;  -- ceil_value  = ceil_div(n, d) * d
  function floor_value(n, d : natural)  return natural;  -- floor_value = (n/d) * d
  function ceil_div(   n : unsigned; d: natural) return unsigned;
  function ceil_value( n : unsigned; d: natural) return unsigned;
  function floor_value(n : unsigned; d: natural) return unsigned;

  function slv(n: in std_logic)        return std_logic_vector;  -- standard logic to 1 element standard logic vector
  function sl( n: in std_logic_vector) return std_logic;  -- 1 element standard logic vector to standard logic

  function to_natural_arr(n : t_integer_arr; to_zero : boolean) return t_natural_arr;  -- if to_zero=TRUE then negative numbers are forced to zero, otherwise they will give a compile range error
  function to_natural_arr(n : t_nat_natural_arr)                return t_natural_arr;
  function to_integer_arr(n : t_natural_arr)                    return t_integer_arr;
  function to_integer_arr(n : t_nat_natural_arr)                return t_integer_arr;
  function to_slv_32_arr( n : t_integer_arr)                    return t_slv_32_arr;
  function to_slv_32_arr( n : t_natural_arr)                    return t_slv_32_arr;

  function vector_tree(slv : std_logic_vector; operation : string) return std_logic;  -- Core operation tree function for vector "AND", "OR", "XOR"
  function vector_and(slv : std_logic_vector) return std_logic;  -- '1' when all slv bits are '1' else '0'
  function vector_or( slv : std_logic_vector) return std_logic;  -- '0' when all slv bits are '0' else '1'
  function vector_xor(slv : std_logic_vector) return std_logic;  -- '1' when the slv has an odd number of '1' bits else '0'
  function vector_one_hot(slv : std_logic_vector) return std_logic_vector;  -- Returns slv when it contains one hot bit, else returns 0.

  function andv(slv : std_logic_vector) return std_logic;  -- alias of vector_and
  function orv( slv : std_logic_vector) return std_logic;  -- alias of vector_or
  function xorv(slv : std_logic_vector) return std_logic;  -- alias of vector_xor

  function matrix_and(mat : t_sl_matrix; wi, wj : natural) return std_logic;  -- '1' when all matrix bits are '1' else '0'
  function matrix_or( mat : t_sl_matrix; wi, wj : natural) return std_logic;  -- '0' when all matrix bits are '0' else '1'

  function smallest(n, m    : integer)       return integer;
  function smallest(n, m, l : integer)       return integer;
  function smallest(n       : t_natural_arr) return natural;

  function largest(n, m : integer)       return integer;
  function largest(n    : t_natural_arr) return natural;

  function func_sum(    n : t_natural_arr)     return natural;  -- sum     of all elements in array
  function func_sum(    n : t_nat_natural_arr) return natural;
  function func_product(n : t_natural_arr)     return natural;  -- product of all elements in array
  function func_product(n : t_nat_natural_arr) return natural;

  function "+" (L, R: t_natural_arr)               return t_natural_arr;  -- element wise sum
  function "+" (L   : t_natural_arr; R : integer)  return t_natural_arr;  -- element wise sum
  function "+" (L   : integer; R : t_natural_arr)  return t_natural_arr;  -- element wise sum

  function "-" (L, R: t_natural_arr)               return t_natural_arr;  -- element wise subtract
  function "-" (L, R: t_natural_arr)               return t_integer_arr;  -- element wise subtract, support negative result
  function "-" (L   : t_natural_arr; R : integer)  return t_natural_arr;  -- element wise subtract
  function "-" (L   : integer; R : t_natural_arr)  return t_natural_arr;  -- element wise subtract

  function "*" (L, R: t_natural_arr)               return t_natural_arr;  -- element wise product
  function "*" (L   : t_natural_arr; R : natural)  return t_natural_arr;  -- element wise product
  function "*" (L   : natural; R : t_natural_arr)  return t_natural_arr;  -- element wise product

  function "/" (L, R: t_natural_arr)               return t_natural_arr;  -- element wise division
  function "/" (L   : t_natural_arr; R : positive) return t_natural_arr;  -- element wise division
  function "/" (L   : natural; R : t_natural_arr)  return t_natural_arr;  -- element wise division

  function is_true(a : std_logic) return boolean;
  function is_true(a : std_logic) return natural;
  function is_true(a : boolean)   return std_logic;
  function is_true(a : boolean)   return natural;
  function is_true(a : integer)   return boolean;  -- also covers NATURAL because it is a subtype of INTEGER
  function is_true(a : integer)   return std_logic;  -- also covers NATURAL because it is a subtype of INTEGER

  function sel_a_b(sel,           a, b : boolean)           return boolean;
  function sel_a_b(sel,           a, b : integer)           return integer;
  function sel_a_b(sel : boolean; a, b : integer)           return integer;
  function sel_a_b(sel : boolean; a, b : real)              return real;
  function sel_a_b(sel : boolean; a, b : std_logic)         return std_logic;
  function sel_a_b(sel : integer; a, b : std_logic)         return std_logic;
  function sel_a_b(sel : integer; a, b : std_logic_vector)  return std_logic_vector;
  function sel_a_b(sel : boolean; a, b : std_logic_vector)  return std_logic_vector;
  function sel_a_b(sel : boolean; a, b : signed)            return signed;
  function sel_a_b(sel : boolean; a, b : unsigned)          return unsigned;
  function sel_a_b(sel : boolean; a, b : t_integer_arr)     return t_integer_arr;
  function sel_a_b(sel : boolean; a, b : t_natural_arr)     return t_natural_arr;
  function sel_a_b(sel : boolean; a, b : t_nat_integer_arr) return t_nat_integer_arr;
  function sel_a_b(sel : boolean; a, b : t_nat_natural_arr) return t_nat_natural_arr;
  function sel_a_b(sel : boolean; a, b : string)            return string;
  function sel_a_b(sel : integer; a, b : string)            return string;
  function sel_a_b(sel : boolean; a, b : time)              return time;
  function sel_a_b(sel : boolean; a, b : severity_level)    return severity_level;

  -- sel_n() index sel = 0, 1, 2, ... will return a, b, c, ...
  function sel_n(sel : natural; a, b, c                      : boolean) return boolean;  -- 3
  function sel_n(sel : natural; a, b, c, d                   : boolean) return boolean;  -- 4
  function sel_n(sel : natural; a, b, c, d, e                : boolean) return boolean;  -- 5
  function sel_n(sel : natural; a, b, c, d, e, f             : boolean) return boolean;  -- 6
  function sel_n(sel : natural; a, b, c, d, e, f, g          : boolean) return boolean;  -- 7
  function sel_n(sel : natural; a, b, c, d, e, f, g, h       : boolean) return boolean;  -- 8
  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i    : boolean) return boolean;  -- 9
  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i, j : boolean) return boolean;  -- 10

  function sel_n(sel : natural; a, b, c                      : integer) return integer;  -- 3
  function sel_n(sel : natural; a, b, c, d                   : integer) return integer;  -- 4
  function sel_n(sel : natural; a, b, c, d, e                : integer) return integer;  -- 5
  function sel_n(sel : natural; a, b, c, d, e, f             : integer) return integer;  -- 6
  function sel_n(sel : natural; a, b, c, d, e, f, g          : integer) return integer;  -- 7
  function sel_n(sel : natural; a, b, c, d, e, f, g, h       : integer) return integer;  -- 8
  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i    : integer) return integer;  -- 9
  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i, j : integer) return integer;  -- 10

  function sel_n(sel : natural; a, b                         : string) return string;  -- 2
  function sel_n(sel : natural; a, b, c                      : string) return string;  -- 3
  function sel_n(sel : natural; a, b, c, d                   : string) return string;  -- 4
  function sel_n(sel : natural; a, b, c, d, e                : string) return string;  -- 5
  function sel_n(sel : natural; a, b, c, d, e, f             : string) return string;  -- 6
  function sel_n(sel : natural; a, b, c, d, e, f, g          : string) return string;  -- 7
  function sel_n(sel : natural; a, b, c, d, e, f, g, h       : string) return string;  -- 8
  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i    : string) return string;  -- 9
  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i, j : string) return string;  -- 10

  function array_init(init : std_logic; nof              : natural) return std_logic_vector;  -- useful to init a unconstrained array of size 1
  function array_init(init,             nof              : natural) return t_natural_arr;  -- useful to init a unconstrained array of size 1
  function array_init(init,             nof              : natural) return t_nat_natural_arr;  -- useful to init a unconstrained array of size 1
  function array_init(init,             nof, incr        : natural) return t_natural_arr;  -- useful to init an array with incrementing numbers
  function array_init(init,             nof, incr        : natural) return t_nat_natural_arr;
  function array_init(init,             nof, incr        : integer) return t_slv_16_arr;
  function array_init(init,             nof, incr        : integer) return t_slv_32_arr;
  function array_init(init,             nof, width       : natural) return std_logic_vector;  -- useful to init an unconstrained std_logic_vector with repetitive content
  function array_init(init,             nof, width, incr : natural) return std_logic_vector;  -- useful to init an unconstrained std_logic_vector with incrementing content
  function array_sinit(init : integer;   nof, width       : natural) return std_logic_vector;  -- useful to init an unconstrained std_logic_vector with repetitive content

  function init_slv_64_matrix(nof_a, nof_b, k : integer) return t_slv_64_matrix;  -- initialize all elements in t_slv_64_matrix to value k

  -- Concatenate two or more STD_LOGIC_VECTORs into a single STD_LOGIC_VECTOR or extract one of them from a concatenated STD_LOGIC_VECTOR
  function func_slv_concat(  use_a, use_b, use_c, use_d, use_e, use_f, use_g : boolean; a, b, c, d, e, f, g : std_logic_vector) return std_logic_vector;
  function func_slv_concat(  use_a, use_b, use_c, use_d, use_e, use_f        : boolean; a, b, c, d, e, f    : std_logic_vector) return std_logic_vector;
  function func_slv_concat(  use_a, use_b, use_c, use_d, use_e               : boolean; a, b, c, d, e       : std_logic_vector) return std_logic_vector;
  function func_slv_concat(  use_a, use_b, use_c, use_d                      : boolean; a, b, c, d          : std_logic_vector) return std_logic_vector;
  function func_slv_concat(  use_a, use_b, use_c                             : boolean; a, b, c             : std_logic_vector) return std_logic_vector;
  function func_slv_concat(  use_a, use_b                                    : boolean; a, b                : std_logic_vector) return std_logic_vector;
  function func_slv_concat_w(use_a, use_b, use_c, use_d, use_e, use_f, use_g : boolean; a_w, b_w, c_w, d_w, e_w, f_w, g_w : natural) return natural;
  function func_slv_concat_w(use_a, use_b, use_c, use_d, use_e, use_f        : boolean; a_w, b_w, c_w, d_w, e_w, f_w      : natural) return natural;
  function func_slv_concat_w(use_a, use_b, use_c, use_d, use_e               : boolean; a_w, b_w, c_w, d_w, e_w           : natural) return natural;
  function func_slv_concat_w(use_a, use_b, use_c, use_d                      : boolean; a_w, b_w, c_w, d_w                : natural) return natural;
  function func_slv_concat_w(use_a, use_b, use_c                             : boolean; a_w, b_w, c_w                     : natural) return natural;
  function func_slv_concat_w(use_a, use_b                                    : boolean; a_w, b_w                          : natural) return natural;
  function func_slv_extract( use_a, use_b, use_c, use_d, use_e, use_f, use_g : boolean; a_w, b_w, c_w, d_w, e_w, f_w, g_w : natural; vec : std_logic_vector; sel : natural) return std_logic_vector;
  function func_slv_extract( use_a, use_b, use_c, use_d, use_e, use_f        : boolean; a_w, b_w, c_w, d_w, e_w, f_w      : natural; vec : std_logic_vector; sel : natural) return std_logic_vector;
  function func_slv_extract( use_a, use_b, use_c, use_d, use_e               : boolean; a_w, b_w, c_w, d_w, e_w           : natural; vec : std_logic_vector; sel : natural) return std_logic_vector;
  function func_slv_extract( use_a, use_b, use_c, use_d                      : boolean; a_w, b_w, c_w, d_w                : natural; vec : std_logic_vector; sel : natural) return std_logic_vector;
  function func_slv_extract( use_a, use_b, use_c                             : boolean; a_w, b_w, c_w                     : natural; vec : std_logic_vector; sel : natural) return std_logic_vector;
  function func_slv_extract( use_a, use_b                                    : boolean; a_w, b_w                          : natural; vec : std_logic_vector; sel : natural) return std_logic_vector;

  function TO_UINT(vec : std_logic_vector) return natural;  -- beware: NATURAL'HIGH = 2**31-1, not 2*32-1, use TO_SINT to avoid warning
  function TO_SINT(vec : std_logic_vector) return integer;

  function TO_UVEC(dec, w : natural) return std_logic_vector;
  function TO_SVEC(dec, w : integer) return std_logic_vector;

  function TO_SVEC_32(dec : integer) return std_logic_vector;  -- = TO_SVEC() with w=32 for t_slv_32_arr slv elements

-- The RESIZE for SIGNED in IEEE.NUMERIC_STD extends the sign bit or it keeps the sign bit and LS part. This
  -- behaviour of preserving the sign bit is less suitable for DSP and not necessary in general. A more
  -- appropriate approach is to ignore the MSbit sign and just keep the LS part. For too large values this
  -- means that the result gets wrapped, but that is fine for default behaviour, because that is also what
  -- happens for RESIZE of UNSIGNED. Therefor this is what the RESIZE_NUM for SIGNED and the RESIZE_SVEC do
  -- and better not use RESIZE for SIGNED anymore.
  function RESIZE_NUM( u   : unsigned;         w : natural) return unsigned;  -- left extend with '0' or keep LS part (same as RESIZE for UNSIGNED)
  function RESIZE_NUM( s   : signed;           w : natural) return signed;  -- extend sign bit or keep LS part
  function RESIZE_UVEC(sl  : std_logic;        w : natural) return std_logic_vector;  -- left extend with '0' into slv
  function RESIZE_UVEC(vec : std_logic_vector; w : natural) return std_logic_vector;  -- left extend with '0' or keep LS part
  function RESIZE_SVEC(vec : std_logic_vector; w : natural) return std_logic_vector;  -- extend sign bit or keep LS part
  function RESIZE_UINT(u   : integer;          w : natural) return integer;  -- left extend with '0' or keep LS part
  function RESIZE_SINT(s   : integer;          w : natural) return integer;  -- extend sign bit or keep LS part

  function RESIZE_UVEC_32(vec : std_logic_vector) return std_logic_vector;  -- = RESIZE_UVEC() with w=32 for t_slv_32_arr slv elements
  function RESIZE_SVEC_32(vec : std_logic_vector) return std_logic_vector;  -- = RESIZE_SVEC() with w=32 for t_slv_32_arr slv elements

  function INCR_UVEC(vec : std_logic_vector; dec : integer)  return std_logic_vector;
  function INCR_UVEC(vec : std_logic_vector; dec : unsigned) return std_logic_vector;
  function INCR_SVEC(vec : std_logic_vector; dec : integer)  return std_logic_vector;
  function INCR_SVEC(vec : std_logic_vector; dec : signed)   return std_logic_vector;
                                                                                                                   -- Used in common_add_sub.vhd
  function ADD_SVEC(l_vec : std_logic_vector; r_vec : std_logic_vector; res_w : natural) return std_logic_vector;  -- l_vec + r_vec, treat slv operands as signed,   slv output width is res_w
  function SUB_SVEC(l_vec : std_logic_vector; r_vec : std_logic_vector; res_w : natural) return std_logic_vector;  -- l_vec - r_vec, treat slv operands as signed,   slv output width is res_w
  function ADD_UVEC(l_vec : std_logic_vector; r_vec : std_logic_vector; res_w : natural) return std_logic_vector;  -- l_vec + r_vec, treat slv operands as unsigned, slv output width is res_w
  function SUB_UVEC(l_vec : std_logic_vector; r_vec : std_logic_vector; res_w : natural) return std_logic_vector;  -- l_vec - r_vec, treat slv operands as unsigned, slv output width is res_w

  function ADD_SVEC(l_vec : std_logic_vector; r_vec : std_logic_vector) return std_logic_vector;  -- l_vec + r_vec, treat slv operands as signed,   slv output width is l_vec'LENGTH
  function SUB_SVEC(l_vec : std_logic_vector; r_vec : std_logic_vector) return std_logic_vector;  -- l_vec - r_vec, treat slv operands as signed,   slv output width is l_vec'LENGTH
  function ADD_UVEC(l_vec : std_logic_vector; r_vec : std_logic_vector) return std_logic_vector;  -- l_vec + r_vec, treat slv operands as unsigned, slv output width is l_vec'LENGTH
  function SUB_UVEC(l_vec : std_logic_vector; r_vec : std_logic_vector) return std_logic_vector;  -- l_vec - r_vec, treat slv operands as unsigned, slv output width is l_vec'LENGTH

  function COMPLEX_MULT_REAL(a_re, a_im, b_re, b_im : integer) return integer;  -- Calculate real part of complex multiplication: a_re*b_re - a_im*b_im
  function COMPLEX_MULT_IMAG(a_re, a_im, b_re, b_im : integer) return integer;  -- Calculate imag part of complex multiplication: a_im*b_re + a_re*b_im

  function SHIFT_UVEC(vec : std_logic_vector; shift : integer) return std_logic_vector;  -- < 0 shift left, > 0 shift right
  function SHIFT_SVEC(vec : std_logic_vector; shift : integer) return std_logic_vector;  -- < 0 shift left, > 0 shift right

  function offset_binary(a : std_logic_vector) return std_logic_vector;

  function truncate(                vec : std_logic_vector; n              : natural) return std_logic_vector;  -- remove n LSBits from vec, so result has width vec'LENGTH-n
  function truncate_and_resize_uvec(vec : std_logic_vector; n,           w : natural) return std_logic_vector;  -- remove n LSBits from vec and then resize to width w
  function truncate_and_resize_svec(vec : std_logic_vector; n,           w : natural) return std_logic_vector;  -- idem for signed values
  function scale(                   vec : std_logic_vector; n:               natural) return std_logic_vector;  -- add n '0' LSBits to vec
  function scale_and_resize_uvec(   vec : std_logic_vector; n,           w : natural) return std_logic_vector;  -- add n '0' LSBits to vec and then resize to width w
  function scale_and_resize_svec(   vec : std_logic_vector; n,           w : natural) return std_logic_vector;  -- idem for signed values
  function truncate_or_resize_uvec( vec : std_logic_vector; b : boolean; w : natural) return std_logic_vector;  -- when b=TRUE then truncate to width w, else resize to width w
  function truncate_or_resize_svec( vec : std_logic_vector; b : boolean; w : natural) return std_logic_vector;  -- idem for signed values

  function s_round(   vec : std_logic_vector; n : natural; clip : boolean) return std_logic_vector;  -- remove n LSBits from vec by rounding away from 0, so result has width vec'LENGTH-n, and clip to avoid wrap
  function s_round(   vec : std_logic_vector; n : natural)                 return std_logic_vector;  -- remove n LSBits from vec by rounding away from 0, so result has width vec'LENGTH-n
  function s_round_up(vec : std_logic_vector; n : natural; clip : boolean) return std_logic_vector;  -- idem but round up to +infinity (s_round_up = u_round)
  function s_round_up(vec : std_logic_vector; n : natural)                 return std_logic_vector;  -- idem but round up to +infinity (s_round_up = u_round)
  function u_round(   vec : std_logic_vector; n : natural; clip : boolean) return std_logic_vector;  -- idem round up for unsigned values
  function u_round(   vec : std_logic_vector; n : natural)                 return std_logic_vector;  -- idem round up for unsigned values

  function u_to_s(u : natural; w : natural) return integer;  -- interpret w bit unsigned u as w bit   signed, and remove any MSbits
  function s_to_u(s : integer; w : natural) return natural;  -- interpret w bit   signed s as w bit unsigned, and remove any MSbits

  function u_wrap(u : natural; w : natural) return natural;  -- return u & 2**w-1 (bit wise and), so keep w LSbits of unsigned u, and remove MSbits
  function s_wrap(s : integer; w : natural) return integer;  -- return s & 2**w-1 (bit wise and), so keep w LSbits of   signed s, and remove MSbits

  function u_clip(u : natural; max : natural) return natural;  -- if s < max return s, else return n
  function s_clip(s : integer; max : natural; min : integer) return integer;  -- if s <=  min return  min, else if s >= max return max, else return s
  function s_clip(s : integer; max : natural               ) return integer;  -- if s <= -max return -max, else if s >= max return max, else return s

  function hton(a : std_logic_vector; w, sz : natural) return std_logic_vector;  -- convert endianity from host to network, sz in symbols of width w
  function hton(a : std_logic_vector;    sz : natural) return std_logic_vector;  -- convert endianity from host to network, sz in bytes
  function hton(a : std_logic_vector                 ) return std_logic_vector;  -- convert endianity from host to network, for all bytes in a
  function ntoh(a : std_logic_vector;    sz : natural) return std_logic_vector;  -- convert endianity from network to host, sz in bytes, ntoh() = hton()
  function ntoh(a : std_logic_vector                 ) return std_logic_vector;  -- convert endianity from network to host, for all bytes in a, ntoh() = hton()

  function flip(a : std_logic_vector)  return std_logic_vector;  -- bit flip a vector, map a[h:0] to [0:h]
  function flip(a, w : natural)        return natural;  -- bit flip a vector, map a[h:0] to [0:h], h = w-1
  function flip(a : t_slv_32_arr)      return t_slv_32_arr;
  function flip(a : t_integer_arr)     return t_integer_arr;
  function flip(a : t_natural_arr)     return t_natural_arr;
  function flip(a : t_nat_natural_arr) return t_nat_natural_arr;

  function transpose(a : std_logic_vector; row, col : natural) return std_logic_vector;  -- transpose a vector, map a[i*row+j] to output index [j*col+i]
  function transpose(a,                    row, col : natural) return natural;  -- transpose index a = [i*row+j] to output index [j*col+i]

  function split_w(input_w: natural; min_out_w: natural; max_out_w: natural) return natural;

  function pad(str: string; width: natural; pad_char: character) return string;

  function slice_up(str: string; width: natural; i: natural) return string;
  function slice_up(str: string; width: natural; i: natural; pad_char: character) return string;
  function slice_dn(str: string; width: natural; i: natural) return string;

  function nat_arr_to_concat_slv(nat_arr: t_natural_arr; nof_elements: natural) return std_logic_vector;

  ------------------------------------------------------------------------------
  -- Component specific functions
  ------------------------------------------------------------------------------

  -- common_fifo_*
  procedure proc_common_fifo_asserts (constant c_fifo_name   : in string;
                                      constant c_note_is_ful : in boolean;
                                      constant c_fail_rd_emp : in boolean;
                                      signal   wr_rst        : in std_logic;
                                      signal   wr_clk        : in std_logic;
                                      signal   wr_full       : in std_logic;
                                      signal   wr_en         : in std_logic;
                                      signal   rd_clk        : in std_logic;
                                      signal   rd_empty      : in std_logic;
                                      signal   rd_en         : in std_logic);

  -- common_fanout_tree
  function func_common_fanout_tree_pipelining(c_nof_stages, c_nof_output_per_cell, c_nof_output : natural;
                                              c_cell_pipeline_factor_arr, c_cell_pipeline_arr : t_natural_arr) return t_natural_arr;

  -- common_reorder_symbol
  function func_common_reorder2_is_there(I, J : natural) return boolean;
  function func_common_reorder2_is_active(I, J, N : natural) return boolean;
  function func_common_reorder2_get_select_index(I, J, N : natural) return integer;
  function func_common_reorder2_get_select(I, J, N : natural; select_arr : t_natural_arr) return natural;
  function func_common_reorder2_inverse_select(N : natural; select_arr : t_natural_arr) return t_natural_arr;

  -- Generate faster sample SCLK from digital DCLK for sim only
  procedure proc_common_dclk_generate_sclk(constant Pfactor : in    positive;
                                           signal   dclk    : in    std_logic;
                                           signal   sclk    : inout std_logic);
end common_pkg;

package body common_pkg is
  function pow2(n : natural) return natural is
  begin
    return 2**n;
  end;

  function ceil_pow2(n : integer) return natural is
  -- Also allows negative exponents and rounds up before returning the value
  begin
    return natural(integer(ceil(2**real(n))));
  end;

  function true_log2(n : natural) return natural is
  -- Purpose: For calculating extra vector width of existing vector
  -- Description: Return mathematical ceil(log2(n))
  --   n    log2()
  --   0 -> -oo  --> FAILURE
  --   1 ->  0
  --   2 ->  1
  --   3 ->  2
  --   4 ->  2
  --   5 ->  3
  --   6 ->  3
  --   7 ->  3
  --   8 ->  3
  --   9 ->  4
  --   etc, up to n = NATURAL'HIGH = 2**31-1
  begin
    return natural(integer(ceil(log2(real(n)))));
  end;

  function ceil_log2(n : natural) return natural is
  -- Purpose: For calculating vector width of new vector
  -- Description:
  --   Same as true_log2() except ceil_log2(1) = 1, which is needed to support
  --   the vector width width for 1 address, to avoid NULL array for single
  --   word register address.
  --   If n = 0, return 0 so we get a NULL array when using
  --   STD_LOGIC_VECTOR(ceil_log2(g_addr_w)-1 DOWNTO 0), instead of an error.
  begin
    if n = 0 then
      return 0;  -- Get NULL array
    elsif n = 1 then
      return 1;  -- avoid NULL array
    else
      return true_log2(n);
    end if;
  end;

  function floor_log10(n : natural) return natural is
  begin
    return natural(integer(floor(log10(real(n)))));
  end;

  function is_pow2(n : natural) return boolean is
  begin
    return n = 2**true_log2(n);
  end;

  function true_log_pow2(n : natural) return natural is
  begin
    return 2**true_log2(n);
  end;

  function ratio(n, d : natural) return natural is
  begin
    if n mod d = 0 then
      return n / d;
    else
      return 0;
    end if;
  end;

  function ratio2(n, m : natural) return natural is
  begin
    return largest(ratio(n, m), ratio(m, n));
  end;

  function ceil_div(n, d : natural) return natural is
  begin
    return n / d + sel_a_b(n mod d = 0, 0, 1);
  end;

  function ceil_value(n, d : natural) return natural is
  begin
    return ceil_div(n, d) * d;
  end;

  function floor_value(n, d : natural) return natural is
  begin
    return (n / d) * d;
  end;

  function ceil_div(n : unsigned; d: natural) return unsigned is
  begin
    return n / d + sel_a_b(n mod d = 0, 0, 1);  -- "/" returns same width as n
  end;

  function ceil_value(n : unsigned; d: natural) return unsigned is
    constant w : natural := n'length;
    variable p : unsigned(2 * w - 1 downto 0);
  begin
    p := ceil_div(n, d) * d;
    return p(w - 1 downto 0);  -- return same width as n
  end;

  function floor_value(n : unsigned; d: natural) return unsigned is
    constant w : natural := n'length;
    variable p : unsigned(2 * w - 1 downto 0);
  begin
    p := (n / d) * d;
    return p(w - 1 downto 0);  -- return same width as n
  end;

  function slv(n: in std_logic) return std_logic_vector is
    variable r : std_logic_vector(0 downto 0);
  begin
    r(0) := n;
    return r;
  end;

  function sl(n: in std_logic_vector) return std_logic is
    variable r : std_logic;
  begin
    r := n(n'low);
    return r;
  end;

  function to_natural_arr(n : t_integer_arr; to_zero : boolean) return t_natural_arr is
    variable vN : t_integer_arr(n'length - 1 downto 0);
    variable vR : t_natural_arr(n'length - 1 downto 0);
  begin
    vN := n;
    for I in vN'range loop
      if to_zero = false then
        vR(I) := vN(I);
      else
        vR(I) := 0;
        if vN(I) > 0 then
          vR(I) := vN(I);
        end if;
      end if;
    end loop;
    return vR;
  end;

  function to_natural_arr(n : t_nat_natural_arr) return t_natural_arr is
    variable vN : t_nat_natural_arr(n'length - 1 downto 0);
    variable vR : t_natural_arr(n'length - 1 downto 0);
  begin
    vN := n;
    for I in vN'range loop
      vR(I) := vN(I);
    end loop;
    return vR;
  end;

  function to_integer_arr(n : t_natural_arr) return t_integer_arr is
    variable vN : t_natural_arr(n'length - 1 downto 0);
    variable vR : t_integer_arr(n'length - 1 downto 0);
  begin
    vN := n;
    for I in vN'range loop
      vR(I) := vN(I);
    end loop;
    return vR;
  end;

  function to_integer_arr(n : t_nat_natural_arr) return t_integer_arr is
    variable vN : t_natural_arr(n'length - 1 downto 0);
  begin
    vN := to_natural_arr(n);
    return to_integer_arr(vN);
  end;

  function to_slv_32_arr(n : t_integer_arr) return t_slv_32_arr is
    variable vN : t_integer_arr(n'length - 1 downto 0);
    variable vR : t_slv_32_arr(n'length - 1 downto 0);
  begin
    vN := n;
    for I in vN'range loop
      vR(I) := TO_SVEC(vN(I), 32);
    end loop;
    return vR;
  end;

  function to_slv_32_arr(n : t_natural_arr) return t_slv_32_arr is
    variable vN : t_natural_arr(n'length - 1 downto 0);
    variable vR : t_slv_32_arr(n'length - 1 downto 0);
  begin
    vN := n;
    for I in vN'range loop
      vR(I) := TO_UVEC(vN(I), 32);
    end loop;
    return vR;
  end;

  function vector_tree(slv : std_logic_vector; operation : string) return std_logic is
    -- Linear loop to determine result takes combinatorial delay that is proportional to slv'LENGTH:
    --   FOR I IN slv'RANGE LOOP
    --     v_result := v_result OPERATION slv(I);
    --   END LOOP;
    --   RETURN v_result;
    -- Instead use binary tree to determine result with smallest combinatorial delay that depends on log2(slv'LENGTH)
    constant c_slv_w      : natural := slv'length;
    constant c_nof_stages : natural := ceil_log2(c_slv_w);
    constant c_w          : natural := 2**c_nof_stages;  -- extend the input slv to a vector with length power of 2 to ease using binary tree
    type t_stage_arr is array (-1 to c_nof_stages - 1) of std_logic_vector(c_w - 1 downto 0);
    variable v_stage_arr  : t_stage_arr;
    variable v_result     : std_logic := '0';
  begin
    -- default any unused, the stage results will be kept in the LSBits and the last result in bit 0
    if    operation = "AND" then v_stage_arr := (others => (others => '1'));
    elsif operation = "OR"  then v_stage_arr := (others => (others => '0'));
    elsif operation = "XOR" then v_stage_arr := (others => (others => '0'));
    else
      assert true
        report "common_pkg: Unsupported vector_tree operation"
        severity FAILURE;
    end if;
    v_stage_arr(-1)(c_slv_w - 1 downto 0) := slv;  -- any unused input c_w : c_slv_w bits have void default value
    for J in 0 to c_nof_stages - 1 loop
      for I in 0 to c_w / (2**(J + 1)) - 1 loop
        if    operation = "AND" then v_stage_arr(J)(I) := v_stage_arr(J - 1)(2 * I) and v_stage_arr(J - 1)(2 * I + 1);
        elsif operation = "OR"  then v_stage_arr(J)(I) := v_stage_arr(J - 1)(2 * I) or  v_stage_arr(J - 1)(2 * I + 1);
        elsif operation = "XOR" then v_stage_arr(J)(I) := v_stage_arr(J - 1)(2 * I) xor v_stage_arr(J - 1)(2 * I + 1);
        end if;
      end loop;
    end loop;
    return v_stage_arr(c_nof_stages - 1)(0);
  end;

  function vector_and(slv : std_logic_vector) return std_logic is
  begin
    return vector_tree(slv, "AND");
  end;

  function vector_or(slv : std_logic_vector) return std_logic is
  begin
    return vector_tree(slv, "OR");
  end;

  function vector_xor(slv : std_logic_vector) return std_logic is
  begin
    return vector_tree(slv, "XOR");
  end;

  function vector_one_hot(slv : std_logic_vector) return std_logic_vector is
    variable v_one_hot : boolean := false;
    variable v_zeros   : std_logic_vector(slv'range) := (others => '0');
  begin
    for i in slv'range loop
      if slv(i) = '1' then
        if not(v_one_hot) then
          -- No hot bits found so far
          v_one_hot := true;
        else
          -- This is the second hot bit found; return zeros.
          return v_zeros;
        end if;
      end if;
    end loop;
    -- No or a single hot bit found in slv; return slv.
    return slv;
  end;

  function andv(slv : std_logic_vector) return std_logic is
  begin
    return vector_tree(slv, "AND");
  end;

  function orv(slv : std_logic_vector) return std_logic is
  begin
    return vector_tree(slv, "OR");
  end;

  function xorv(slv : std_logic_vector) return std_logic is
  begin
    return vector_tree(slv, "XOR");
  end;

  function matrix_and(mat : t_sl_matrix; wi, wj : natural) return std_logic is
    variable v_mat    : t_sl_matrix(0 to wi - 1, 0 to wj - 1) := mat;  -- map to fixed range
    variable v_result : std_logic := '1';
  begin
    for I in 0 to wi - 1 loop
      for J in 0 to wj - 1 loop
        v_result := v_result and v_mat(I,J);
      end loop;
    end loop;
    return v_result;
  end;

  function matrix_or(mat : t_sl_matrix; wi, wj : natural) return std_logic is
    variable v_mat    : t_sl_matrix(0 to wi - 1, 0 to wj - 1) := mat;  -- map to fixed range
    variable v_result : std_logic := '0';
  begin
    for I in 0 to wi - 1 loop
      for J in 0 to wj - 1 loop
        v_result := v_result or v_mat(I,J);
      end loop;
    end loop;
    return v_result;
  end;

  function smallest(n, m : integer) return integer is
  begin
    if n < m then
      return n;
    else
      return m;
    end if;
  end;

  function smallest(n, m, l : integer) return integer is
    variable v : natural;
  begin
                  v := n;
    if v > m then v := m; end if;
    if v > l then v := l; end if;
    return v;
  end;

  function smallest(n : t_natural_arr) return natural is
    variable m : natural := 0;
  begin
    for I in n'range loop
      if n(I) < m then
        m := n(I);
      end if;
    end loop;
    return m;
  end;

  function largest(n, m : integer) return integer is
  begin
    if n > m then
      return n;
    else
      return m;
    end if;
  end;

  function largest(n : t_natural_arr) return natural is
    variable m : natural := 0;
  begin
    for I in n'range loop
      if n(I) > m then
        m := n(I);
      end if;
    end loop;
    return m;
  end;

  function func_sum(n : t_natural_arr) return natural is
    variable vS : natural;
  begin
    vS := 0;
    for I in n'range loop
      vS := vS + n(I);
    end loop;
    return vS;
  end;

  function func_sum(n : t_nat_natural_arr) return natural is
    variable vN : t_natural_arr(n'length - 1 downto 0);
  begin
    vN := to_natural_arr(n);
    return func_sum(vN);
  end;

  function func_product(n : t_natural_arr) return natural is
    variable vP : natural;
  begin
    vP := 1;
    for I in n'range loop
      vP := vP * n(I);
    end loop;
    return vP;
  end;

  function func_product(n : t_nat_natural_arr) return natural is
    variable vN : t_natural_arr(n'length - 1 downto 0);
  begin
    vN := to_natural_arr(n);
    return func_product(vN);
  end;

  function "+" (L, R: t_natural_arr) return t_natural_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vR : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vL := L;
    vR := R;
    for I in vL'range loop
      vP(I) := vL(I) + vR(I);
    end loop;
    return vP;
  end;

  function "+" (L: t_natural_arr; R : integer) return t_natural_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vL := L;
    for I in vL'range loop
      vP(I) := vL(I) + R;
    end loop;
    return vP;
  end;

  function "+" (L: integer; R : t_natural_arr) return t_natural_arr is
  begin
    return R + L;
  end;

  function "-" (L, R: t_natural_arr) return t_natural_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vR : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vL := L;
    vR := R;
    for I in vL'range loop
      vP(I) := vL(I) - vR(I);
    end loop;
    return vP;
  end;

  function "-" (L, R: t_natural_arr) return t_integer_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vR : t_natural_arr(w - 1 downto 0);
    variable vP : t_integer_arr(w - 1 downto 0);
  begin
    vL := L;
    vR := R;
    for I in vL'range loop
      vP(I) := vL(I) - vR(I);
    end loop;
    return vP;
  end;

  function "-" (L: t_natural_arr; R : integer) return t_natural_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vL := L;
    for I in vL'range loop
      vP(I) := vL(I) - R;
    end loop;
    return vP;
  end;

  function "-" (L: integer; R : t_natural_arr) return t_natural_arr is
    constant w  : natural := R'length;
    variable vR : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vR := R;
    for I in vR'range loop
      vP(I) := L - vR(I);
    end loop;
    return vP;
  end;

  function "*" (L, R: t_natural_arr) return t_natural_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vR : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vL := L;
    vR := R;
    for I in vL'range loop
      vP(I) := vL(I) * vR(I);
    end loop;
    return vP;
  end;

  function "*" (L: t_natural_arr; R : natural) return t_natural_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vL := L;
    for I in vL'range loop
      vP(I) := vL(I) * R;
    end loop;
    return vP;
  end;

  function "*" (L: natural; R : t_natural_arr) return t_natural_arr is
  begin
    return R * L;
  end;

  function "/" (L, R: t_natural_arr) return t_natural_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vR : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vL := L;
    vR := R;
    for I in vL'range loop
      vP(I) := vL(I) / vR(I);
    end loop;
    return vP;
  end;

  function "/" (L: t_natural_arr; R : positive) return t_natural_arr is
    constant w  : natural := L'length;
    variable vL : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vL := L;
    for I in vL'range loop
      vP(I) := vL(I) / R;
    end loop;
    return vP;
  end;

  function "/" (L: natural; R : t_natural_arr) return t_natural_arr is
    constant w  : natural := R'length;
    variable vR : t_natural_arr(w - 1 downto 0);
    variable vP : t_natural_arr(w - 1 downto 0);
  begin
    vR := R;
    for I in vR'range loop
      vP(I) := L / vR(I);
    end loop;
    return vP;
  end;

  function is_true(a : std_logic) return boolean   is begin if a = '1'  then return true; else return false; end if; end;
  function is_true(a : std_logic) return natural   is begin if a = '1'  then return 1;    else return 0;     end if; end;
  function is_true(a : boolean)   return std_logic is begin if a = true then return '1';  else return '0';   end if; end;
  function is_true(a : boolean)   return natural   is begin if a = true then return 1;    else return 0;     end if; end;
  function is_true(a : integer)   return boolean   is begin if a /= 0   then return true; else return false; end if; end;
  function is_true(a : integer)   return std_logic is begin if a /= 0   then return '1';  else return '0';   end if; end;

  function sel_a_b(sel, a, b : integer) return integer is
  begin
    if sel /= 0 then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel, a, b : boolean) return boolean is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : integer) return integer is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : real) return real is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : std_logic) return std_logic is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : integer; a, b : std_logic) return std_logic is
  begin
    if sel /= 0 then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : integer; a, b : std_logic_vector) return std_logic_vector is
  begin
    if sel /= 0 then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : std_logic_vector) return std_logic_vector is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : signed) return signed is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : unsigned) return unsigned is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : t_integer_arr) return t_integer_arr is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : t_natural_arr) return t_natural_arr is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : t_nat_integer_arr) return t_nat_integer_arr is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : t_nat_natural_arr) return t_nat_natural_arr is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : string) return string is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : integer; a, b : string) return string is
  begin
    if sel /= 0 then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : time) return time is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  function sel_a_b(sel : boolean; a, b : severity_level) return severity_level is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;

  -- sel_n : boolean
  function sel_n(sel : natural; a, b, c : boolean) return boolean is
    constant c_arr : t_nat_boolean_arr := (a, b, c);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d : boolean) return boolean is
    constant c_arr : t_nat_boolean_arr := (a, b, c, d);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e : boolean) return boolean is
    constant c_arr : t_nat_boolean_arr := (a, b, c, d, e);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f : boolean) return boolean is
    constant c_arr : t_nat_boolean_arr := (a, b, c, d, e, f);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f, g : boolean) return boolean is
    constant c_arr : t_nat_boolean_arr := (a, b, c, d, e, f, g);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f, g, h : boolean) return boolean is
    constant c_arr : t_nat_boolean_arr := (a, b, c, d, e, f, g, h);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i : boolean) return boolean is
    constant c_arr : t_nat_boolean_arr := (a, b, c, d, e, f, g, h, i);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i, j : boolean) return boolean is
    constant c_arr : t_nat_boolean_arr := (a, b, c, d, e, f, g, h, i, j);
  begin
    return c_arr(sel);
  end;

  -- sel_n : integer
  function sel_n(sel : natural; a, b, c : integer) return integer is
    constant c_arr : t_nat_integer_arr := (a, b, c);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d : integer) return integer is
    constant c_arr : t_nat_integer_arr := (a, b, c, d);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e : integer) return integer is
    constant c_arr : t_nat_integer_arr := (a, b, c, d, e);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f : integer) return integer is
    constant c_arr : t_nat_integer_arr := (a, b, c, d, e, f);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f, g : integer) return integer is
    constant c_arr : t_nat_integer_arr := (a, b, c, d, e, f, g);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f, g, h : integer) return integer is
    constant c_arr : t_nat_integer_arr := (a, b, c, d, e, f, g, h);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i : integer) return integer is
    constant c_arr : t_nat_integer_arr := (a, b, c, d, e, f, g, h, i);
  begin
    return c_arr(sel);
  end;

  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i, j : integer) return integer is
    constant c_arr : t_nat_integer_arr := (a, b, c, d, e, f, g, h, i, j);
  begin
    return c_arr(sel);
  end;

  -- sel_n : string
  function sel_n(sel : natural; a, b                         : string) return string is begin if sel = 0 then return            a;                          else return b; end if; end;
  function sel_n(sel : natural; a, b, c                      : string) return string is begin if sel < 2 then return sel_n(sel, a, b                     ); else return c; end if; end;
  function sel_n(sel : natural; a, b, c, d                   : string) return string is begin if sel < 3 then return sel_n(sel, a, b, c                  ); else return d; end if; end;
  function sel_n(sel : natural; a, b, c, d, e                : string) return string is begin if sel < 4 then return sel_n(sel, a, b, c, d               ); else return e; end if; end;
  function sel_n(sel : natural; a, b, c, d, e, f             : string) return string is begin if sel < 5 then return sel_n(sel, a, b, c, d, e            ); else return f; end if; end;
  function sel_n(sel : natural; a, b, c, d, e, f, g          : string) return string is begin if sel < 6 then return sel_n(sel, a, b, c, d, e, f         ); else return g; end if; end;
  function sel_n(sel : natural; a, b, c, d, e, f, g, h       : string) return string is begin if sel < 7 then return sel_n(sel, a, b, c, d, e, f, g      ); else return h; end if; end;
  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i    : string) return string is begin if sel < 8 then return sel_n(sel, a, b, c, d, e, f, g, h   ); else return i; end if; end;
  function sel_n(sel : natural; a, b, c, d, e, f, g, h, i, j : string) return string is begin if sel < 9 then return sel_n(sel, a, b, c, d, e, f, g, h, i); else return j; end if; end;

  function array_init(init : std_logic; nof : natural) return std_logic_vector is
    variable v_arr : std_logic_vector(0 to nof - 1);
  begin
    for I in v_arr'range loop
      v_arr(I) := init;
    end loop;
    return v_arr;
  end;

  function array_init(init, nof : natural) return t_natural_arr is
    variable v_arr : t_natural_arr(0 to nof - 1);
  begin
    for I in v_arr'range loop
      v_arr(I) := init;
    end loop;
    return v_arr;
  end;

  function array_init(init, nof : natural) return t_nat_natural_arr is
    variable v_arr : t_nat_natural_arr(0 to nof - 1);
  begin
    for I in v_arr'range loop
      v_arr(I) := init;
    end loop;
    return v_arr;
  end;

  function array_init(init, nof, incr : natural) return t_natural_arr is
    variable v_arr : t_natural_arr(0 to nof - 1);
    variable v_i   : natural;
  begin
    v_i := 0;
    for I in v_arr'range loop
      v_arr(I) := init + v_i * incr;
      v_i := v_i + 1;
    end loop;
    return v_arr;
  end;

  function array_init(init, nof, incr : natural) return t_nat_natural_arr is
    variable v_arr : t_nat_natural_arr(0 to nof - 1);
    variable v_i   : natural;
  begin
    v_i := 0;
    for I in v_arr'range loop
      v_arr(I) := init + v_i * incr;
      v_i := v_i + 1;
    end loop;
    return v_arr;
  end;

  function array_init(init, nof, incr : integer) return t_slv_16_arr is
    variable v_arr : t_slv_16_arr(0 to nof - 1);
    variable v_i   : natural;
  begin
    v_i := 0;
    for I in v_arr'range loop
      v_arr(I) := TO_SVEC(init + v_i * incr, 16);
      v_i := v_i + 1;
    end loop;
    return v_arr;
  end;

  function array_init(init, nof, incr : integer) return t_slv_32_arr is
    variable v_arr : t_slv_32_arr(0 to nof - 1);
    variable v_i   : natural;
  begin
    v_i := 0;
    for I in v_arr'range loop
      v_arr(I) := TO_SVEC(init + v_i * incr, 32);
      v_i := v_i + 1;
    end loop;
    return v_arr;
  end;

  function array_init(init, nof, width : natural) return std_logic_vector is
    variable v_arr : std_logic_vector(nof * width - 1 downto 0);
  begin
    for I in 0 to nof - 1 loop
      v_arr(width * (I + 1) - 1 downto width * I) := TO_UVEC(init, width);
    end loop;
    return v_arr;
  end;

  function array_init(init, nof, width, incr : natural) return std_logic_vector is
    variable v_arr : std_logic_vector(nof * width - 1 downto 0);
    variable v_i   : natural;
  begin
    v_i := 0;
    for I in 0 to nof - 1 loop
      v_arr(width * (I + 1) - 1 downto width * I) := TO_UVEC(init + v_i * incr, width);
      v_i := v_i + 1;
    end loop;
    return v_arr;
  end;

  function array_sinit(init :integer; nof, width : natural) return std_logic_vector is
    variable v_arr : std_logic_vector(nof * width - 1 downto 0);
  begin
    for I in 0 to nof - 1 loop
      v_arr(width * (I + 1) - 1 downto width * I) := TO_SVEC(init, width);
    end loop;
    return v_arr;
  end;

  function init_slv_64_matrix(nof_a, nof_b, k : integer) return t_slv_64_matrix is
    variable v_mat : t_slv_64_matrix(nof_a - 1 downto 0, nof_b - 1 downto 0);
  begin
    for I in 0 to nof_a - 1 loop
      for J in 0 to nof_b - 1 loop
        v_mat(I, J) := TO_SVEC(k, 64);
      end loop;
    end loop;
    return v_mat;
  end;

  -- Support concatenation of up to 7 slv into 1 slv
  function func_slv_concat(use_a, use_b, use_c, use_d, use_e, use_f, use_g : boolean; a, b, c, d, e, f, g : std_logic_vector) return std_logic_vector is
    constant c_max_w : natural := a'length + b'length + c'length + d'length + e'length + f'length + g'length;
    variable v_res   : std_logic_vector(c_max_w - 1 downto 0) := (others => '0');
    variable v_len   : natural := 0;
  begin
    if use_a = true then v_res(a'length - 1 + v_len downto v_len) := a; v_len := v_len + a'length; end if;
    if use_b = true then v_res(b'length - 1 + v_len downto v_len) := b; v_len := v_len + b'length; end if;
    if use_c = true then v_res(c'length - 1 + v_len downto v_len) := c; v_len := v_len + c'length; end if;
    if use_d = true then v_res(d'length - 1 + v_len downto v_len) := d; v_len := v_len + d'length; end if;
    if use_e = true then v_res(e'length - 1 + v_len downto v_len) := e; v_len := v_len + e'length; end if;
    if use_f = true then v_res(f'length - 1 + v_len downto v_len) := f; v_len := v_len + f'length; end if;
    if use_g = true then v_res(g'length - 1 + v_len downto v_len) := g; v_len := v_len + g'length; end if;
    return v_res(v_len - 1 downto 0);
  end func_slv_concat;

  function func_slv_concat(use_a, use_b, use_c, use_d, use_e, use_f : boolean; a, b, c, d, e, f : std_logic_vector) return std_logic_vector is
  begin
    return func_slv_concat(use_a, use_b, use_c, use_d, use_e, use_f, false, a, b, c, d, e, f, "0");
  end func_slv_concat;

  function func_slv_concat(use_a, use_b, use_c, use_d, use_e : boolean; a, b, c, d, e : std_logic_vector) return std_logic_vector is
  begin
    return func_slv_concat(use_a, use_b, use_c, use_d, use_e, false, false, a, b, c, d, e, "0", "0");
  end func_slv_concat;

  function func_slv_concat(use_a, use_b, use_c, use_d : boolean; a, b, c, d : std_logic_vector) return std_logic_vector is
  begin
    return func_slv_concat(use_a, use_b, use_c, use_d, false, false, false, a, b, c, d, "0", "0", "0");
  end func_slv_concat;

  function func_slv_concat(use_a, use_b, use_c : boolean; a, b, c : std_logic_vector) return std_logic_vector is
  begin
    return func_slv_concat(use_a, use_b, use_c, false, false, false, false, a, b, c, "0", "0", "0", "0");
  end func_slv_concat;

  function func_slv_concat(use_a, use_b : boolean; a, b : std_logic_vector) return std_logic_vector is
  begin
    return func_slv_concat(use_a, use_b, false, false, false, false, false, a, b, "0", "0", "0", "0", "0");
  end func_slv_concat;

  function func_slv_concat_w(use_a, use_b, use_c, use_d, use_e, use_f, use_g : boolean; a_w, b_w, c_w, d_w, e_w, f_w, g_w : natural) return natural is
    variable v_len : natural := 0;
  begin
    if use_a = true then v_len := v_len + a_w; end if;
    if use_b = true then v_len := v_len + b_w; end if;
    if use_c = true then v_len := v_len + c_w; end if;
    if use_d = true then v_len := v_len + d_w; end if;
    if use_e = true then v_len := v_len + e_w; end if;
    if use_f = true then v_len := v_len + f_w; end if;
    if use_g = true then v_len := v_len + g_w; end if;
    return v_len;
  end func_slv_concat_w;

  function func_slv_concat_w(use_a, use_b, use_c, use_d, use_e, use_f : boolean; a_w, b_w, c_w, d_w, e_w, f_w : natural) return natural is
  begin
    return func_slv_concat_w(use_a, use_b, use_c, use_d, use_e, use_f, false, a_w, b_w, c_w, d_w, e_w, f_w, 0);
  end func_slv_concat_w;

  function func_slv_concat_w(use_a, use_b, use_c, use_d, use_e : boolean; a_w, b_w, c_w, d_w, e_w : natural) return natural is
  begin
    return func_slv_concat_w(use_a, use_b, use_c, use_d, use_e, false, false, a_w, b_w, c_w, d_w, e_w, 0, 0);
  end func_slv_concat_w;

  function func_slv_concat_w(use_a, use_b, use_c, use_d : boolean; a_w, b_w, c_w, d_w : natural) return natural is
  begin
    return func_slv_concat_w(use_a, use_b, use_c, use_d, false, false, false, a_w, b_w, c_w, d_w, 0, 0, 0);
  end func_slv_concat_w;

  function func_slv_concat_w(use_a, use_b, use_c : boolean; a_w, b_w, c_w : natural) return natural is
  begin
    return func_slv_concat_w(use_a, use_b, use_c, false, false, false, false, a_w, b_w, c_w, 0, 0, 0, 0);
  end func_slv_concat_w;

  function func_slv_concat_w(use_a, use_b : boolean; a_w, b_w : natural) return natural is
  begin
    return func_slv_concat_w(use_a, use_b, false, false, false, false, false, a_w, b_w, 0, 0, 0, 0, 0);
  end func_slv_concat_w;

  -- extract slv
  function func_slv_extract(use_a, use_b, use_c, use_d, use_e, use_f, use_g : boolean; a_w, b_w, c_w, d_w, e_w, f_w, g_w : natural; vec : std_logic_vector; sel : natural) return std_logic_vector is
    variable v_w  : natural := 0;
    variable v_lo : natural := 0;
  begin
    -- if the selected slv is not used in vec, then return dummy, else return the selected slv from vec
    case sel is
      when 0 =>
        if use_a = true then v_w := a_w; else return c_slv0(a_w - 1 downto 0); end if;
      when 1 =>
        if use_b = true then v_w := b_w; else return c_slv0(b_w - 1 downto 0); end if;
        if use_a = true then v_lo := v_lo + a_w; end if;
      when 2 =>
        if use_c = true then v_w := c_w; else return c_slv0(c_w - 1 downto 0); end if;
        if use_a = true then v_lo := v_lo + a_w; end if;
        if use_b = true then v_lo := v_lo + b_w; end if;
      when 3 =>
        if use_d = true then v_w := d_w; else return c_slv0(d_w - 1 downto 0); end if;
        if use_a = true then v_lo := v_lo + a_w; end if;
        if use_b = true then v_lo := v_lo + b_w; end if;
        if use_c = true then v_lo := v_lo + c_w; end if;
      when 4 =>
        if use_e = true then v_w := e_w; else return c_slv0(e_w - 1 downto 0); end if;
        if use_a = true then v_lo := v_lo + a_w; end if;
        if use_b = true then v_lo := v_lo + b_w; end if;
        if use_c = true then v_lo := v_lo + c_w; end if;
        if use_d = true then v_lo := v_lo + d_w; end if;
      when 5 =>
        if use_f = true then v_w := f_w; else return c_slv0(f_w - 1 downto 0); end if;
        if use_a = true then v_lo := v_lo + a_w; end if;
        if use_b = true then v_lo := v_lo + b_w; end if;
        if use_c = true then v_lo := v_lo + c_w; end if;
        if use_d = true then v_lo := v_lo + d_w; end if;
        if use_e = true then v_lo := v_lo + e_w; end if;
      when 6 =>
        if use_g = true then v_w := g_w; else return c_slv0(g_w - 1 downto 0); end if;
        if use_a = true then v_lo := v_lo + a_w; end if;
        if use_b = true then v_lo := v_lo + b_w; end if;
        if use_c = true then v_lo := v_lo + c_w; end if;
        if use_d = true then v_lo := v_lo + d_w; end if;
        if use_e = true then v_lo := v_lo + e_w; end if;
        if use_f = true then v_lo := v_lo + f_w; end if;
      when others => report "Unknown common_pkg func_slv_extract argument" severity FAILURE;
    end case;
    return vec(v_w - 1 + v_lo downto v_lo);  -- extracted slv
  end func_slv_extract;

  function func_slv_extract(use_a, use_b, use_c, use_d, use_e, use_f : boolean; a_w, b_w, c_w, d_w, e_w, f_w : natural; vec : std_logic_vector; sel : natural) return std_logic_vector is
  begin
    return func_slv_extract(use_a, use_b, use_c, use_d, use_e, use_f, false, a_w, b_w, c_w, d_w, e_w, f_w, 0, vec, sel);
  end func_slv_extract;

  function func_slv_extract(use_a, use_b, use_c, use_d, use_e : boolean; a_w, b_w, c_w, d_w, e_w : natural; vec : std_logic_vector; sel : natural) return std_logic_vector is
  begin
    return func_slv_extract(use_a, use_b, use_c, use_d, use_e, false, false, a_w, b_w, c_w, d_w, e_w, 0, 0, vec, sel);
  end func_slv_extract;

  function func_slv_extract(use_a, use_b, use_c, use_d : boolean; a_w, b_w, c_w, d_w : natural; vec : std_logic_vector; sel : natural) return std_logic_vector is
  begin
    return func_slv_extract(use_a, use_b, use_c, use_d, false, false, false, a_w, b_w, c_w, d_w, 0, 0, 0, vec, sel);
  end func_slv_extract;

  function func_slv_extract(use_a, use_b, use_c : boolean; a_w, b_w, c_w : natural; vec : std_logic_vector; sel : natural) return std_logic_vector is
  begin
    return func_slv_extract(use_a, use_b, use_c, false, false, false, false, a_w, b_w, c_w, 0, 0, 0, 0, vec, sel);
  end func_slv_extract;

  function func_slv_extract(use_a, use_b : boolean; a_w, b_w : natural; vec : std_logic_vector; sel : natural) return std_logic_vector is
  begin
    return func_slv_extract(use_a, use_b, false, false, false, false, false, a_w, b_w, 0, 0, 0, 0, 0, vec, sel);
  end func_slv_extract;

  function TO_UINT(vec : std_logic_vector) return natural is
  begin
    return to_integer(unsigned(vec));
  end;

  function TO_SINT(vec : std_logic_vector) return integer is
  begin
    return to_integer(signed(vec));
  end;

  function TO_UVEC(dec, w : natural) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(dec, w));
  end;

  function TO_SVEC(dec, w : integer) return std_logic_vector is
  begin
    return std_logic_vector(to_signed(dec, w));
  end;

  function TO_SVEC_32(dec : integer) return std_logic_vector is
  begin
    return TO_SVEC(dec, 32);
  end;

  function RESIZE_NUM(u : unsigned; w : natural) return unsigned is
  begin
    -- left extend with '0' or keep LS part (same as RESIZE for UNSIGNED)
    return resize(u, w);
  end;

  function RESIZE_NUM(s : signed; w : natural) return signed is
  begin
    -- extend sign bit or keep LS part
    if w > s'length then
      return resize(s, w);  -- extend sign bit
    else
      return signed(resize(unsigned(s), w));  -- keep LSbits (= vec[w-1:0])
    end if;
  end;

  function RESIZE_UVEC(sl : std_logic; w : natural) return std_logic_vector is
    variable v_slv0 : std_logic_vector(w - 1 downto 1) := (others => '0');
  begin
    return v_slv0 & sl;
  end;

  function RESIZE_UVEC(vec : std_logic_vector; w : natural) return std_logic_vector is
  begin
    return std_logic_vector(RESIZE_NUM(unsigned(vec), w));
  end;

  function RESIZE_SVEC(vec : std_logic_vector; w : natural) return std_logic_vector is
  begin
    return std_logic_vector(RESIZE_NUM(signed(vec), w));
  end;

  function RESIZE_UINT(u : integer; w : natural) return integer is
    variable v : std_logic_vector(c_word_w - 1 downto 0);
  begin
    v := TO_UVEC(u, c_word_w);
    return TO_UINT(v(w - 1 downto 0));
  end;

  function RESIZE_SINT(s : integer; w : natural) return integer is
    variable v : std_logic_vector(c_word_w - 1 downto 0);
  begin
    v := TO_SVEC(s, c_word_w);
    return TO_SINT(v(w - 1 downto 0));
  end;

  function RESIZE_UVEC_32(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, 32);
  end;

  function RESIZE_SVEC_32(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_SVEC(vec, 32);
  end;

  function INCR_UVEC(vec : std_logic_vector; dec : integer) return std_logic_vector is
    variable v_dec : integer;
  begin
    if dec < 0 then
      v_dec := -dec;
      return std_logic_vector(unsigned(vec) - v_dec);  -- uses function "-" (L : UNSIGNED, R : NATURAL), there is no function + with R : INTEGER argument
    else
      v_dec := dec;
      return std_logic_vector(unsigned(vec) + v_dec);  -- uses function "+" (L : UNSIGNED, R : NATURAL)
    end if;
  end;

  function INCR_UVEC(vec : std_logic_vector; dec : unsigned) return std_logic_vector is
  begin
    return std_logic_vector(unsigned(vec) + dec);
  end;

  function INCR_SVEC(vec : std_logic_vector; dec : integer) return std_logic_vector is
    variable v_dec : integer;
  begin
    return std_logic_vector(signed(vec) + v_dec);  -- uses function "+" (L : SIGNED, R : INTEGER)
  end;

  function INCR_SVEC(vec : std_logic_vector; dec : signed) return std_logic_vector is
  begin
    return std_logic_vector(signed(vec) + dec);
  end;

  function ADD_SVEC(l_vec : std_logic_vector; r_vec : std_logic_vector; res_w : natural) return std_logic_vector is
  begin
    return std_logic_vector(RESIZE_NUM(signed(l_vec), res_w) + signed(r_vec));
  end;

  function SUB_SVEC(l_vec : std_logic_vector; r_vec : std_logic_vector; res_w : natural) return std_logic_vector is
  begin
    return std_logic_vector(RESIZE_NUM(signed(l_vec), res_w) - signed(r_vec));
  end;

  function ADD_UVEC(l_vec : std_logic_vector; r_vec : std_logic_vector; res_w : natural) return std_logic_vector is
  begin
    return std_logic_vector(RESIZE_NUM(unsigned(l_vec), res_w) + unsigned(r_vec));
  end;

  function SUB_UVEC(l_vec : std_logic_vector; r_vec : std_logic_vector; res_w : natural) return std_logic_vector is
  begin
    return std_logic_vector(RESIZE_NUM(unsigned(l_vec), res_w) - unsigned(r_vec));
  end;

  function ADD_SVEC(l_vec : std_logic_vector; r_vec : std_logic_vector) return std_logic_vector is
  begin
    return ADD_SVEC(l_vec, r_vec, l_vec'length);
  end;

  function SUB_SVEC(l_vec : std_logic_vector; r_vec : std_logic_vector) return std_logic_vector is
  begin
    return SUB_SVEC(l_vec, r_vec, l_vec'length);
  end;

  function ADD_UVEC(l_vec : std_logic_vector; r_vec : std_logic_vector) return std_logic_vector is
  begin
    return ADD_UVEC(l_vec, r_vec, l_vec'length);
  end;

  function SUB_UVEC(l_vec : std_logic_vector; r_vec : std_logic_vector) return std_logic_vector is
  begin
    return SUB_UVEC(l_vec, r_vec, l_vec'length);
  end;

  function COMPLEX_MULT_REAL(a_re, a_im, b_re, b_im : integer) return integer is
  begin
    return (a_re * b_re - a_im * b_im);
  end;

  function COMPLEX_MULT_IMAG(a_re, a_im, b_re, b_im : integer) return integer is
  begin
    return (a_im * b_re + a_re * b_im);
  end;

  function SHIFT_UVEC(vec : std_logic_vector; shift : integer) return std_logic_vector is
  begin
    if shift < 0 then
      return std_logic_vector(SHIFT_LEFT(unsigned(vec), -shift));  -- fill zeros from right
    else
      return std_logic_vector(SHIFT_RIGHT(unsigned(vec), shift));  -- fill zeros from left
    end if;
  end;

  function SHIFT_SVEC(vec : std_logic_vector; shift : integer) return std_logic_vector is
  begin
    if shift < 0 then
      return std_logic_vector(SHIFT_LEFT(signed(vec), -shift));  -- same as SHIFT_LEFT for UNSIGNED
    else
      return std_logic_vector(SHIFT_RIGHT(signed(vec), shift));  -- extend sign
    end if;
  end;

  --
  -- offset_binary() : maps offset binary to or from two-complement binary.
  --
  --   National ADC08DC1020     offset binary     two-complement binary
  --   + full scale =  127.5 :  11111111 = 255     127 = 01111111
  --     ...
  --   +            =   +0.5 :  10000000 = 128       0 = 00000000
  --   0
  --   -            =   -0.5 :  01111111 = 127      -1 = 11111111
  --     ...
  --   - full scale = -127.5 :  00000000 =   0    -128 = 10000000
  --
  -- To map between the offset binary and two complement binary involves
  -- adding 128 to the binary value or equivalently inverting the sign bit.
  -- The offset_binary() mapping can be done and undone both ways.
  -- The offset_binary() mapping to two-complement binary yields a DC offset
  -- of -0.5 Lsb.
  function offset_binary(a : std_logic_vector) return std_logic_vector is
    variable v_res : std_logic_vector(a'length - 1 downto 0) := a;
  begin
   v_res(v_res'high) := not v_res(v_res'high);  -- invert MSbit to get to from offset binary to two's complement, or vice versa
   return v_res;
  end;

  function truncate(vec : std_logic_vector; n : natural) return std_logic_vector is
    constant c_vec_w   : natural := vec'length;
    constant c_trunc_w : natural := c_vec_w - n;
    variable v_vec     : std_logic_vector(c_vec_w - 1 downto 0) := vec;
    variable v_res     : std_logic_vector(c_trunc_w - 1 downto 0);
  begin
   v_res := v_vec(c_vec_w - 1 downto n);  -- keep MS part
   return v_res;
  end;

  function truncate_and_resize_uvec(vec : std_logic_vector; n, w : natural) return std_logic_vector is
    constant c_vec_w   : natural := vec'length;
    constant c_trunc_w : natural := c_vec_w - n;
    variable v_trunc   : std_logic_vector(c_trunc_w - 1 downto 0);
    variable v_res     : std_logic_vector(w - 1 downto 0);
  begin
    v_trunc := truncate(vec, n);  -- first keep MS part
    v_res := RESIZE_UVEC(v_trunc, w);  -- then keep LS part or left extend with '0'
    return v_res;
  end;

  function truncate_and_resize_svec(vec : std_logic_vector; n, w : natural) return std_logic_vector is
    constant c_vec_w   : natural := vec'length;
    constant c_trunc_w : natural := c_vec_w - n;
    variable v_trunc   : std_logic_vector(c_trunc_w - 1 downto 0);
    variable v_res     : std_logic_vector(w - 1 downto 0);
  begin
    v_trunc := truncate(vec, n);  -- first keep MS part
    v_res := RESIZE_SVEC(v_trunc, w);  -- then keep sign bit and LS part or left extend sign bit
    return v_res;
  end;

  function scale(vec : std_logic_vector; n: natural) return std_logic_vector is
    constant c_vec_w   : natural := vec'length;
    constant c_scale_w : natural := c_vec_w + n;
    variable v_res     : std_logic_vector(c_scale_w - 1 downto 0) := (others => '0');
  begin
    v_res(c_scale_w - 1 downto n) := vec;  -- scale by adding n zero bits at the right
    return v_res;
  end;

  function scale_and_resize_uvec(vec : std_logic_vector; n, w : natural) return std_logic_vector is
    constant c_vec_w   : natural := vec'length;
    constant c_scale_w : natural := c_vec_w + n;
    variable v_scale   : std_logic_vector(c_scale_w - 1 downto 0) := (others => '0');
    variable v_res     : std_logic_vector(w - 1 downto 0);
  begin
    v_scale(c_scale_w - 1 downto n) := vec;  -- first scale by adding n zero bits at the right
    v_res := RESIZE_UVEC(v_scale, w);  -- then keep LS part or left extend with '0'
    return v_res;
  end;

  function scale_and_resize_svec(vec : std_logic_vector; n, w : natural) return std_logic_vector is
    constant c_vec_w   : natural := vec'length;
    constant c_scale_w : natural := c_vec_w + n;
    variable v_scale   : std_logic_vector(c_scale_w - 1 downto 0) := (others => '0');
    variable v_res     : std_logic_vector(w - 1 downto 0);
  begin
    v_scale(c_scale_w - 1 downto n) := vec;  -- first scale by adding n zero bits at the right
    v_res := RESIZE_SVEC(v_scale, w);  -- then keep LS part or left extend sign bit
    return v_res;
  end;

  function truncate_or_resize_uvec(vec : std_logic_vector; b : boolean; w : natural) return std_logic_vector is
    constant c_vec_w : natural := vec'length;
    variable c_n     : integer := c_vec_w - w;
    variable v_res   : std_logic_vector(w - 1 downto 0);
  begin
    if b = true and c_n > 0 then
      v_res := truncate_and_resize_uvec(vec, c_n, w);
    else
      v_res := RESIZE_UVEC(vec, w);
    end if;
    return v_res;
  end;

  function truncate_or_resize_svec(vec : std_logic_vector; b : boolean; w : natural) return std_logic_vector is
    constant c_vec_w : natural := vec'length;
    variable c_n     : integer := c_vec_w - w;
    variable v_res   : std_logic_vector(w - 1 downto 0);
  begin
    if b = true and c_n > 0 then
      v_res := truncate_and_resize_svec(vec, c_n, w);
    else
      v_res := RESIZE_SVEC(vec, w);
    end if;
    return v_res;
  end;

  -- Functions s_round, s_round_up and u_round:
  --
  -- . The returned output width is input width - n.
  -- . If n=0 then the return value is the same as the input value so only
  --   wires (NOP, no operation).
  -- . Both have the same implementation but different c_max and c_clip values.
  -- . Round up for unsigned so +2.5 becomes 3
  -- . Round away from zero for signed so round up for positive and round down for negative, so +2.5 becomes 3 and -2.5 becomes -3.
  -- . Round away from zero is also used by round() in Matlab, Python, TCL
  -- . Rounding up implies adding 0.5 and then truncation, use clip = TRUE to
  --   clip the potential overflow due to adding 0.5 to +max.
  -- . For negative values overflow due to rounding can not occur, because c_half-1 >= 0 for n>0
  -- . If the input comes from a product and is rounded to the input width then
  --   clip can safely be FALSE, because e.g. for unsigned 4b*4b=8b->4b the
  --   maximum product is 15*15=225 <= 255-8, and for signed 4b*4b=8b->4b the
  --   maximum product is -8*-8=+64 <= 127-8, so wrapping due to rounding
  --   overflow will never occur.

  function s_round(vec : std_logic_vector; n : natural; clip : boolean) return std_logic_vector is
    -- Use SIGNED to avoid NATURAL (32 bit range) overflow error
    constant c_in_w  : natural := vec'length;
    constant c_out_w : natural := vec'length - n;
    constant c_one   : signed(c_in_w - 1 downto 0) := to_signed(1, c_in_w);
    constant c_half  : signed(c_in_w - 1 downto 0) := SHIFT_LEFT(c_one, n - 1);  -- = 2**(n-1)
    constant c_max   : signed(c_in_w - 1 downto 0) := signed('0' & c_slv1(c_in_w - 2 downto 0)) - c_half;  -- = 2**(c_in_w-1)-1 - c_half
    constant c_clip  : signed(c_out_w - 1 downto 0) := signed('0' & c_slv1(c_out_w - 2 downto 0));  -- = 2**(c_out_w-1)-1
    variable v_in    : signed(c_in_w - 1 downto 0);
    variable v_out   : signed(c_out_w - 1 downto 0);
  begin
    v_in := signed(vec);
    if n > 0 then
      if clip = true and v_in > c_max then
        v_out := c_clip;  -- Round clip to maximum positive to avoid wrap to negative
      else
        if vec(vec'high) = '0' then
          v_out := RESIZE_NUM(SHIFT_RIGHT(v_in + c_half + 0, n), c_out_w);  -- Round up for positive
        else
          v_out := RESIZE_NUM(SHIFT_RIGHT(v_in + c_half - 1, n), c_out_w);  -- Round down for negative
        end if;
      end if;
    else
      v_out := RESIZE_NUM(v_in, c_out_w);  -- NOP
    end if;
    return std_logic_vector(v_out);
  end;

  function s_round(vec : std_logic_vector; n : natural) return std_logic_vector is
  begin
    return s_round(vec, n, false);  -- no round clip
  end;

  -- An alternative is to always round up, also for negative numbers (i.e. s_round_up = u_round).
  function s_round_up(vec : std_logic_vector; n : natural; clip : boolean) return std_logic_vector is
  begin
    return u_round(vec, n, clip);
  end;

  function s_round_up(vec : std_logic_vector; n : natural) return std_logic_vector is
  begin
    return u_round(vec, n, false);  -- no round clip
  end;

  -- Unsigned numbers are round up (almost same as s_round, but without the else on negative vec)
  function u_round(vec : std_logic_vector; n : natural; clip : boolean ) return std_logic_vector is
    -- Use UNSIGNED to avoid NATURAL (32 bit range) overflow error
    constant c_in_w  : natural := vec'length;
    constant c_out_w : natural := vec'length - n;
    constant c_one   : unsigned(c_in_w - 1 downto 0) := to_unsigned(1, c_in_w);
    constant c_half  : unsigned(c_in_w - 1 downto 0) := SHIFT_LEFT(c_one, n - 1);  -- = 2**(n-1)
    constant c_max   : unsigned(c_in_w - 1 downto 0) := unsigned(c_slv1(c_in_w - 1 downto 0)) - c_half;  -- = 2**c_in_w-1 - c_half
    constant c_clip  : unsigned(c_out_w - 1 downto 0) := unsigned(c_slv1(c_out_w - 1 downto 0));  -- = 2**c_out_w-1
    variable v_in    : unsigned(c_in_w - 1 downto 0);
    variable v_out   : unsigned(c_out_w - 1 downto 0);
  begin
    v_in := unsigned(vec);
    if n > 0 then
      if clip = true and v_in > c_max then
        v_out := c_clip;  -- Round clip to +max to avoid wrap to 0
      else
        v_out := RESIZE_NUM(SHIFT_RIGHT(v_in + c_half, n), c_out_w);  -- Round up
      end if;
    else
      v_out := RESIZE_NUM(v_in, c_out_w);  -- NOP
    end if;
    return std_logic_vector(v_out);
  end;

  function u_round(vec : std_logic_vector; n : natural) return std_logic_vector is
  begin
    return u_round(vec, n, false);  -- no round clip
  end;

  function u_to_s(u : natural; w : natural) return integer is
    variable v_u : std_logic_vector(31 downto 0) := TO_UVEC(u, 32);  -- via 32 bit word to avoid NUMERIC_STD.TO_SIGNED: vector truncated warming
  begin
    return TO_SINT(v_u(w - 1 downto 0));
  end;

  function s_to_u(s : integer; w : natural) return natural is
    variable v_s : std_logic_vector(31 downto 0) := TO_SVEC(s, 32);  -- via 32 bit word to avoid NUMERIC_STD.TO_SIGNED: vector truncated warming
  begin
    return TO_UINT(v_s(w - 1 downto 0));
  end;

  function u_wrap(u : natural; w : natural) return natural is
    variable v_u : std_logic_vector(31 downto 0) := TO_UVEC(u, 32);  -- via 32 bit word to avoid NUMERIC_STD.TO_SIGNED: vector truncated warming
  begin
    return TO_UINT(v_u(w - 1 downto 0));
  end;

  function s_wrap(s : integer; w : natural) return integer is
    variable v_s : std_logic_vector(31 downto 0) := TO_SVEC(s, 32);  -- via 32 bit word to avoid NUMERIC_STD.TO_SIGNED: vector truncated warming
  begin
    return TO_SINT(v_s(w - 1 downto 0));
  end;

  function u_clip(u : natural; max : natural) return natural is
  begin
    if u > max then
      return max;
    else
      return u;
    end if;
  end;

  function s_clip(s : integer; max : natural; min : integer) return integer is
  begin
    if s < min then
      return min;
    else
      if s > max then
        return max;
      else
        return s;
      end if;
    end if;
  end;

  function s_clip(s : integer; max : natural) return integer is
  begin
    return s_clip(s, max, -max);
  end;

  function hton(a : std_logic_vector; w, sz : natural) return std_logic_vector is
    variable v_a : std_logic_vector(a'length - 1 downto 0) := a;  -- map a to range [h:0]
    variable v_b : std_logic_vector(a'length - 1 downto 0) := a;  -- default b = a
    variable vL  : natural;
    variable vK  : natural;
  begin
    -- Note:
    -- . if sz = 1          then v_b = v_a
    -- . if a'LENGTH > sz*w then v_b(a'LENGTH:sz*w) = v_a(a'LENGTH:sz*w)
    for vL in 0 to sz - 1 loop
      vK := sz - 1 - vL;
      v_b((vL + 1) * w - 1 downto vL * w) := v_a((vK + 1) * w - 1 downto vK * w);
    end loop;
    return v_b;
  end function;

  function hton(a : std_logic_vector; sz : natural) return std_logic_vector is
  begin
    return hton(a, c_byte_w, sz);  -- symbol width w = c_byte_w = 8
  end function;

  function hton(a : std_logic_vector) return std_logic_vector is
    constant c_sz : natural := a'length / c_byte_w;
  begin
    return hton(a, c_byte_w, c_sz);  -- symbol width w = c_byte_w = 8
  end function;

  function ntoh(a : std_logic_vector; sz : natural) return std_logic_vector is
  begin
    return hton(a, sz);  -- i.e. ntoh() = hton()
  end function;

  function ntoh(a : std_logic_vector) return std_logic_vector is
  begin
    return hton(a);  -- i.e. ntoh() = hton()
  end function;

  function flip(a : std_logic_vector) return std_logic_vector is
    variable v_a : std_logic_vector(a'length - 1 downto 0) := a;
    variable v_b : std_logic_vector(a'length - 1 downto 0);
  begin
    for I in v_a'range loop
      v_b(a'length - 1 - I) := v_a(I);
    end loop;
    return v_b;
  end;

  function flip(a, w : natural) return natural is
  begin
    return TO_UINT(flip(TO_UVEC(a, w)));
  end;

  function flip(a : t_slv_32_arr) return t_slv_32_arr is
    variable v_a : t_slv_32_arr(a'length - 1 downto 0) := a;
    variable v_b : t_slv_32_arr(a'length - 1 downto 0);
  begin
    for I in v_a'range loop
      v_b(a'length - 1 - I) := v_a(I);
    end loop;
    return v_b;
  end;

  function flip(a : t_integer_arr) return t_integer_arr is
    variable v_a : t_integer_arr(a'length - 1 downto 0) := a;
    variable v_b : t_integer_arr(a'length - 1 downto 0);
  begin
    for I in v_a'range loop
      v_b(a'length - 1 - I) := v_a(I);
    end loop;
    return v_b;
  end;

  function flip(a : t_natural_arr) return t_natural_arr is
    variable v_a : t_natural_arr(a'length - 1 downto 0) := a;
    variable v_b : t_natural_arr(a'length - 1 downto 0);
  begin
    for I in v_a'range loop
      v_b(a'length - 1 - I) := v_a(I);
    end loop;
    return v_b;
  end;

  function flip(a : t_nat_natural_arr) return t_nat_natural_arr is
    variable v_a : t_nat_natural_arr(a'length - 1 downto 0) := a;
    variable v_b : t_nat_natural_arr(a'length - 1 downto 0);
  begin
    for I in v_a'range loop
      v_b(a'length - 1 - I) := v_a(I);
    end loop;
    return v_b;
  end;

  function transpose(a : std_logic_vector; row, col : natural) return std_logic_vector is
    variable vIn  : std_logic_vector(a'length - 1 downto 0);
    variable vOut : std_logic_vector(a'length - 1 downto 0);
  begin
    vIn  := a;  -- map input vector to h:0 range
    vOut := vIn;  -- default leave any unused MSbits the same
    for J in 0 to row - 1 loop
      for I in 0 to col - 1 loop
        vOut(J * col + I) := vIn(I * row + J);  -- transpose vector, map input index [i*row+j] to output index [j*col+i]
      end loop;
    end loop;
    return vOut;
  end function;

  function transpose(a, row, col : natural) return natural is  -- transpose index a = [i*row+j] to output index [j*col+i]
    variable vI  : natural;
    variable vJ  : natural;
  begin
    vI := a / row;
    vJ := a mod row;
    return vJ * col + vI;
  end;

  function split_w(input_w: natural; min_out_w: natural; max_out_w: natural) return natural is  -- Calculate input_w in multiples as close as possible to max_out_w
    -- Examples: split_w(256, 8, 32) = 32;  split_w(16, 8, 32) = 16; split_w(72, 8, 32) = 18;    -- Input_w must be multiple of 2.
    variable r: natural;
  begin
    r := input_w;
    for i in 1 to ceil_log2(input_w) loop  -- Useless to divide the number beyond this
      if r <= max_out_w and r >= min_out_w then
        return r;
      elsif i = ceil_log2(input_w) then  -- last iteration
        return 0;  -- Indicates wrong values were used
      end if;
      r := r / 2;
    end loop;
  end;

  function pad(str: string; width: natural; pad_char: character) return string is
    variable v_str : string(1 to width) := (others => pad_char);
  begin
    v_str(width - str'length + 1 to width) := str;
    return v_str;
  end;

  function slice_up(str: string; width: natural; i: natural) return string is
  begin
    return str(i * width + 1 to (i + 1) * width);
  end;

  -- If the input value is not a multiple of the desired width, the return value is padded with
  -- the passed pad value. E.g. if input='10' and desired width is 4, return value is '0010'.
  function slice_up(str: string; width: natural; i: natural; pad_char: character) return string is
    variable padded_str : string(1 to width) := (others => '0');
  begin
    padded_str := pad(str(i * width + 1 to (i + 1) * width), width, '0');
    return padded_str;
  end;

  function slice_dn(str: string; width: natural; i: natural) return string is
  begin
    return str((i + 1) * width - 1 downto i * width);
  end;

  function nat_arr_to_concat_slv(nat_arr: t_natural_arr; nof_elements: natural) return std_logic_vector is
    variable v_concat_slv : std_logic_vector(nof_elements * 32 - 1 downto 0) := (others => '0');
  begin
    for i in 0 to nof_elements - 1 loop
      v_concat_slv(i * 32 + 32 - 1 downto i * 32) :=  TO_UVEC(nat_arr(i), 32);
    end loop;
    return v_concat_slv;
  end;

  ------------------------------------------------------------------------------
  -- common_fifo_*
  ------------------------------------------------------------------------------

  procedure proc_common_fifo_asserts (constant c_fifo_name   : in string;
                                      constant c_note_is_ful : in boolean;
                                      constant c_fail_rd_emp : in boolean;
                                      signal   wr_rst        : in std_logic;
                                      signal   wr_clk        : in std_logic;
                                      signal   wr_full       : in std_logic;
                                      signal   wr_en         : in std_logic;
                                      signal   rd_clk        : in std_logic;
                                      signal   rd_empty      : in std_logic;
                                      signal   rd_en         : in std_logic) is
  begin
    -- c_fail_rd_emp : when TRUE report FAILURE when read from an empty FIFO, important when FIFO rd_val is not used
    -- c_note_is_ful : when TRUE report NOTE when FIFO goes full, to note that operation is on the limit
    -- FIFO overflow is always reported as FAILURE

    -- The FIFO wr_full goes high at reset to indicate that it can not be written and it goes low a few cycles after reset.
    -- Therefore only check on wr_full going high when wr_rst='0'.

    --synthesis translate_off
    assert not(c_fail_rd_emp = true and rising_edge(rd_clk) and rd_empty = '1' and rd_en = '1')
      report c_fifo_name & " : read from empty fifo occurred!"
      severity FAILURE;
    assert not(c_note_is_ful = true and rising_edge(wr_full) and wr_rst = '0')
      report c_fifo_name & " : fifo is full now"
      severity NOTE;
    assert not( rising_edge(wr_clk) and wr_full = '1' and wr_en = '1')
      report c_fifo_name & " : fifo overflow occurred!"
      severity FAILURE;
    --synthesis translate_on
  end procedure proc_common_fifo_asserts;

  ------------------------------------------------------------------------------
  -- common_fanout_tree
  ------------------------------------------------------------------------------

  function func_common_fanout_tree_pipelining(c_nof_stages, c_nof_output_per_cell, c_nof_output : natural;
                                              c_cell_pipeline_factor_arr, c_cell_pipeline_arr : t_natural_arr) return t_natural_arr is
    constant k_cell_pipeline_factor_arr : t_natural_arr(c_nof_stages - 1 downto 0) := c_cell_pipeline_factor_arr;
    constant k_cell_pipeline_arr        : t_natural_arr(c_nof_output_per_cell - 1 downto 0) := c_cell_pipeline_arr;
    variable v_stage_pipeline_arr       : t_natural_arr(c_nof_output - 1 downto 0) := (others => 0);
    variable v_prev_stage_pipeline_arr  : t_natural_arr(c_nof_output - 1 downto 0) := (others => 0);
  begin
    loop_stage : for j in 0 to c_nof_stages - 1 loop
      v_prev_stage_pipeline_arr := v_stage_pipeline_arr;
      loop_cell : for i in 0 to c_nof_output_per_cell**j - 1 loop
        v_stage_pipeline_arr((i + 1) * c_nof_output_per_cell - 1 downto i * c_nof_output_per_cell) := v_prev_stage_pipeline_arr(i) + (k_cell_pipeline_factor_arr(j) * k_cell_pipeline_arr);
      end loop;
    end loop;
    return v_stage_pipeline_arr;
  end function func_common_fanout_tree_pipelining;

  ------------------------------------------------------------------------------
  -- common_reorder_symbol
  ------------------------------------------------------------------------------

  -- Determine whether the stage I and row J index refer to any (active or redundant) 2-input reorder cell instantiation
  function func_common_reorder2_is_there(I, J : natural) return boolean is
    variable v_odd  : boolean;
    variable v_even : boolean;
  begin
    v_odd  := (I mod 2 = 1) and (J mod 2 = 1);  -- for odd  stage at each odd  row
    v_even := (I mod 2 = 0) and (J mod 2 = 0);  -- for even stage at each even row
    return v_odd or v_even;
  end func_common_reorder2_is_there;

  -- Determine whether the stage I and row J index refer to an active 2-input reorder cell instantiation in a reorder network with N stages
  function func_common_reorder2_is_active(I, J, N : natural) return boolean is
    variable v_inst : boolean;
    variable v_act  : boolean;
  begin
    v_inst := func_common_reorder2_is_there(I, J);
    v_act  := (I > 0) and (I <= N) and (J > 0) and (J < N);
    return v_inst and v_act;
  end func_common_reorder2_is_active;

  -- Get the index K in the select setting array for the reorder2 cell on stage I and row J in a reorder network with N stages
  function func_common_reorder2_get_select_index(I, J, N : natural) return integer is
    constant c_nof_reorder2_per_odd_stage  : natural := N / 2;
    constant c_nof_reorder2_per_even_stage : natural := (N - 1) / 2;
    variable v_nof_odd_stages  : natural;
    variable v_nof_even_stages : natural;
    variable v_offset          : natural;
    variable v_K               : integer;
  begin
    -- for I, J that do not refer to an reorder cell instance for -1 as dummy return value.
    -- for the redundant two port reorder cells at the border rows for -1 to indicate that the cell should pass on the input.
    v_K := -1;
    if func_common_reorder2_is_active(I, J, N) then
      -- for the active two port reorder cells use the setting at index v_K from the select setting array
      v_nof_odd_stages  :=  I / 2;
      v_nof_even_stages := (I - 1) / 2;
      v_offset          := (J - 1) / 2;  -- suits both odd stage and even stage
      v_K := v_nof_odd_stages * c_nof_reorder2_per_odd_stage + v_nof_even_stages * c_nof_reorder2_per_even_stage + v_offset;
    end if;
    return v_K;
  end func_common_reorder2_get_select_index;

  -- Get the select setting for the reorder2 cell on stage I and row J in a reorder network with N stages
  function func_common_reorder2_get_select(I, J, N : natural; select_arr : t_natural_arr) return natural is
    constant c_nof_select : natural := select_arr'length;
    constant c_select_arr : t_natural_arr(c_nof_select - 1 downto 0) := select_arr;  -- force range downto 0
    variable v_sel        : natural;
    variable v_K          : integer;
  begin
    v_sel := 0;
    v_K := func_common_reorder2_get_select_index(I, J, N);
    if v_K >= 0 then
      v_sel := c_select_arr(v_K);
    end if;
    return v_sel;
  end func_common_reorder2_get_select;

  -- Determine the inverse of a reorder network by using two reorder networks in series
  function func_common_reorder2_inverse_select(N : natural; select_arr : t_natural_arr) return t_natural_arr is
    constant c_nof_select      : natural := select_arr'length;
    constant c_select_arr      : t_natural_arr(c_nof_select - 1 downto 0) := select_arr;  -- force range downto 0
    variable v_sel             : natural;
    variable v_Ki              : integer;
    variable v_Ii              : natural;
    variable v_inverse_arr     : t_natural_arr(2 * c_nof_select - 1 downto 0) := (others => 0);  -- default set identity for the reorder2 cells in both reorder instances
  begin
    -- the inverse select consists of inverse_in reorder and inverse_out reorder in series
    if N mod 2 = 1 then
      -- N is odd so only need to fill in the inverse_in reorder, the inverse_out reorder remains at default pass on
      for I in 1 to N loop
        for J in 0 to N - 1 loop
          -- get the DUT setting
          v_sel := func_common_reorder2_get_select(I, J, N, c_select_arr);
          -- map DUT I to inverse v_Ii stage index and determine the index for the inverse setting
          v_Ii := 1 + N - I;
          v_Ki := func_common_reorder2_get_select_index(v_Ii, J, N);
          if v_Ki >= 0 then
            v_inverse_arr(v_Ki) := v_sel;
          end if;
        end loop;
      end loop;
    else
      -- N is even so only use stage 1 of the inverse_out reorder, the other stages remain at default pass on
      for K in 0 to N / 2 - 1 loop
         v_Ki := c_nof_select + K;  -- stage 1 of the inverse_out reorder
         v_inverse_arr(v_Ki) := c_select_arr(K);
      end loop;
      -- N is even so leave stage 1 of the inverse_in reorder at default pass on, and do inverse the other stages
      for I in 2 to N loop
        for J in 0 to N - 1 loop
          -- get the DUT setting
          v_sel := func_common_reorder2_get_select(I, J, N, c_select_arr);
          -- map DUT I to inverse v_Ii stage index and determine the index for the inverse setting
          v_Ii := 2 + N - I;
          v_Ki := func_common_reorder2_get_select_index(v_Ii, J, N);
          if v_Ki >= 0 then
            v_inverse_arr(v_Ki) := v_sel;
          end if;
        end loop;
      end loop;
    end if;
    return v_inverse_arr;
  end func_common_reorder2_inverse_select;

  ------------------------------------------------------------------------------
  -- PROCEDURE: Generate faster sample SCLK from digital DCLK for sim only
  -- Description:
  --   The SCLK kan be used to serialize Pfactor >= 1 symbols per word and then
  --   view them in a scope component that is use internally in the design.
  --   The scope component is only instantiated for simulation, to view the
  --   serialized symbols, typically with decimal radix and analogue format.
  --   The scope component will not be synthesized, because the SCLK can not
  --   be synthesized.
  --
  --   Pfactor = 4
  --            _______         _______         _______         _______
  --   DCLK ___|       |_______|       |_______|       |_______|       |_______
  --        ___________________   _   _   _   _   _   _   _   _   _   _   _   _
  --   SCLK                    |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_|
  --
  --   The rising edges of SCLK occur after the rising edge of DCLK, to ensure
  --   that they all apply to the same wide data word that was clocked by the
  --   rising edge of the DCLK.
  ------------------------------------------------------------------------------
  procedure proc_common_dclk_generate_sclk(constant Pfactor : in    positive;
                                           signal   dclk    : in    std_logic;
                                           signal   sclk    : inout std_logic) is
    variable v_dperiod : time;
    variable v_speriod : time;
  begin
    SCLK <= '1';
    -- Measure DCLK period
    wait until rising_edge(DCLK);
    v_dperiod := NOW;
    wait until rising_edge(DCLK);
    v_dperiod := NOW - v_dperiod;
    v_speriod := v_dperiod / Pfactor;
    -- Generate Pfactor SCLK periods per DCLK period
    while true loop
      -- Realign at every DCLK
      wait until rising_edge(DCLK);
      -- Create Pfactor SCLK periods within this DCLK period
      SCLK <= '0';
      if Pfactor > 1 then
        for I in 0 to 2 * Pfactor - 1 - 2 loop
          wait for v_speriod / 2;
          SCLK <= not SCLK;
        end loop;
      end if;
      wait for v_speriod / 2;
      SCLK <= '1';
      -- Wait for next DCLK
    end loop;
    wait;
  end proc_common_dclk_generate_sclk;

end common_pkg;
