-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

-- Purpose: Test bench for common_paged_ram_ww_rr
-- Description:
--   Verifies two DUTs ww_rr and w_rr using incrementing data.
-- Features:
-- . Page size can be any size g_page_sz >= 1, so not only powers of 2
-- . Use c_gap_sz = 0 to try writing and reading multiple page without idle
--   cycles
--
-- Usage:
-- > as 10
-- > run -all

entity tb_common_paged_ram_ww_rr is
  generic (
    g_pipeline_in     : natural := 0;  -- >= 0
    g_pipeline_out    : natural := 1;  -- >= 0
    g_page_sz         : natural := 10  -- >= 1
  );
end tb_common_paged_ram_ww_rr;

architecture tb of tb_common_paged_ram_ww_rr is
  constant clk_period        : time := 10 ns;

  constant c_nof_blocks      : natural := 4;

  constant c_ram_rd_latency  : natural := 1;
  constant c_rd_latency      : natural := g_pipeline_in + c_ram_rd_latency + g_pipeline_out;
  constant c_offset_a        : natural := 2 mod g_page_sz;  -- use c_offset_a and c_offset_b to have a read and b read from independent address
  constant c_offset_b        : natural := 5 mod g_page_sz;

  constant c_data_w          : natural := 8;
  constant c_gap_sz          : natural := 0;  -- >= 0
  constant c_addr_w          : natural := ceil_log2(g_page_sz);
  constant c_rl              : natural := 1;

  signal rst               : std_logic;
  signal clk               : std_logic := '1';
  signal tb_end            : std_logic := '0';

  -- DUT
  signal next_page         : std_logic;

  signal in_en             : std_logic;
  signal in_adr            : std_logic_vector(c_addr_w - 1 downto 0) := (others => '0');
  signal in_mod_adr        : natural;
  signal in_dat            : std_logic_vector(c_data_w - 1 downto 0) := (others => '0');

  signal wr_en_a           : std_logic;
  signal wr_adr_a          : std_logic_vector(c_addr_w - 1 downto 0) := (others => '0');
  signal wr_dat_a          : std_logic_vector(c_data_w - 1 downto 0) := (others => '0');

  signal wr_en_b           : std_logic;
  signal wr_adr_b          : std_logic_vector(c_addr_w - 1 downto 0) := (others => '0');
  signal wr_dat_b          : std_logic_vector(c_data_w - 1 downto 0) := (others => '0');

  signal rd_en_a           : std_logic;
  signal rd_adr_a          : std_logic_vector(c_addr_w - 1 downto 0);
  signal wrr_rd_dat_a      : std_logic_vector(c_data_w - 1 downto 0);
  signal wrr_rd_val_a      : std_logic;
  signal wwrr_rd_dat_a     : std_logic_vector(c_data_w - 1 downto 0);
  signal wwrr_rd_val_a     : std_logic;

  signal rd_en_b           : std_logic;
  signal rd_adr_b          : std_logic_vector(c_addr_w - 1 downto 0);
  signal wrr_rd_dat_b      : std_logic_vector(c_data_w - 1 downto 0);
  signal wrr_rd_val_b      : std_logic;
  signal wwrr_rd_dat_b     : std_logic_vector(c_data_w - 1 downto 0);
  signal wwrr_rd_val_b     : std_logic;

  -- Verify
  signal ready                  : std_logic := '1';
  signal verify_en              : std_logic;

  signal wrr_mod_dat_a          : natural;
  signal wrr_mod_dat_b          : natural;
  signal wrr_offset_dat_a       : natural;
  signal wrr_offset_dat_b       : natural;
  signal wrr_result_dat_a       : std_logic_vector(c_data_w - 1 downto 0);
  signal wrr_result_dat_b       : std_logic_vector(c_data_w - 1 downto 0);
  signal prev_wrr_result_dat_a  : std_logic_vector(c_data_w - 1 downto 0);
  signal prev_wrr_result_dat_b  : std_logic_vector(c_data_w - 1 downto 0);

  signal wwrr_mod_dat_a         : natural;
  signal wwrr_mod_dat_b         : natural;
  signal wwrr_offset_dat_a      : natural;
  signal wwrr_offset_dat_b      : natural;
  signal wwrr_result_dat_a      : std_logic_vector(c_data_w - 1 downto 0);
  signal wwrr_result_dat_b      : std_logic_vector(c_data_w - 1 downto 0);
  signal prev_wwrr_result_dat_a : std_logic_vector(c_data_w - 1 downto 0);
  signal prev_wwrr_result_dat_b : std_logic_vector(c_data_w - 1 downto 0);
begin
  clk <= not clk and not tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 2;

  -- Single write use in_en
  in_mod_adr <= TO_UINT(in_adr) mod g_page_sz;
  in_dat <= INCR_UVEC(in_dat, 1) when rising_edge(clk) and in_en = '1';
  in_adr <= TO_UVEC((TO_UINT(in_adr) + 1) mod g_page_sz, c_addr_w) when rising_edge(clk) and in_en = '1';

  -- Double write use in_en for halve of the g_page_sz time
  wr_en_a  <= in_en when in_mod_adr mod 2 = 0 else '0';  -- use port a to write the even addresses
  wr_dat_a <= in_dat;
  wr_adr_a <= in_adr;

  wr_en_b  <= '0'   when in_mod_adr = g_page_sz - 1 and g_page_sz mod 2 = 1 else  -- do not write at last address in case of odd g_page_sz
              in_en when in_mod_adr mod 2 = 0 else '0';  -- use port b to write the odd addresses

  wr_dat_b <= INCR_UVEC(in_dat, 1);
  wr_adr_b <= TO_UVEC((TO_UINT(in_adr) + 1) mod g_page_sz, c_addr_w);

  -- Double read use in_en
  rd_en_a  <= in_en;
  rd_adr_a <= TO_UVEC((TO_UINT(in_adr) + c_offset_a) mod g_page_sz, c_addr_w);  -- b read from other address than a
  rd_en_b  <= in_en;
  rd_adr_b <= TO_UVEC((TO_UINT(in_adr) + c_offset_b) mod g_page_sz, c_addr_w);  -- b read from other address than a

  p_stimuli : process
  begin
    next_page <= '0';
    in_en     <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 3);

    -- Access the pages several times
    for I in 0 to c_nof_blocks - 1 loop
      in_en <= '1';
      proc_common_wait_some_cycles(clk, g_page_sz - 1);
      next_page <= '1';
      proc_common_wait_some_cycles(clk, 1);
      next_page <= '0';
      in_en <= '0';
      proc_common_wait_some_cycles(clk, c_gap_sz);  -- optional gap between the pages
    end loop;

    in_en <= '0';
    proc_common_wait_some_cycles(clk, 1 + g_page_sz / 2);
    tb_end <= '1';
    wait;
  end process;

  p_verify : process
  begin
    verify_en <= '0';
    proc_common_wait_until_high(clk, next_page);
    proc_common_wait_some_cycles(clk, c_rd_latency);
    proc_common_wait_some_cycles(clk, 1);  -- use first read value as initial reference value
    verify_en <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DUTs
  ------------------------------------------------------------------------------

  -- Double write - double read
  u_dut_ww_rr : entity work.common_paged_ram_ww_rr
  generic map (
    g_pipeline_in  => g_pipeline_in,
    g_pipeline_out => g_pipeline_out,
    g_data_w       => c_data_w,
    g_page_sz      => g_page_sz
  )
  port map (
    rst         => rst,
    clk         => clk,
    clken       => '1',
    -- next page control
    next_page   => next_page,
    -- double write access to one page
    wr_adr_a    => wr_adr_a,
    wr_en_a     => wr_en_a,
    wr_dat_a    => wr_dat_a,
    wr_adr_b    => wr_adr_b,
    wr_en_b     => wr_en_b,
    wr_dat_b    => wr_dat_b,
    -- double read access from the other one page
    rd_adr_a    => rd_adr_a,
    rd_en_a     => rd_en_a,
    rd_adr_b    => rd_adr_b,
    rd_en_b     => rd_en_b,
    -- double read data from the other one page after c_rd_latency
    rd_dat_a    => wwrr_rd_dat_a,
    rd_val_a    => wwrr_rd_val_a,
    rd_dat_b    => wwrr_rd_dat_b,
    rd_val_b    => wwrr_rd_val_b
  );

  -- Single write - double read
  u_dut_w_rr : entity work.common_paged_ram_w_rr
  generic map (
    g_pipeline_in  => g_pipeline_in,
    g_pipeline_out => g_pipeline_out,
    g_data_w       => c_data_w,
    g_page_sz      => g_page_sz
  )
  port map (
    rst         => rst,
    clk         => clk,
    clken       => '1',
    -- next page control
    next_page   => next_page,
    -- double write access to one page
    wr_adr      => in_adr,
    wr_en       => in_en,
    wr_dat      => in_dat,
    -- double read access from the other one page
    rd_adr_a    => rd_adr_a,
    rd_en_a     => rd_en_a,
    rd_adr_b    => rd_adr_b,
    rd_en_b     => rd_en_b,
    -- double read data from the other one page after c_rd_latency
    rd_dat_a    => wrr_rd_dat_a,
    rd_val_a    => wrr_rd_val_a,
    rd_dat_b    => wrr_rd_dat_b,
    rd_val_b    => wrr_rd_val_b
  );

  ------------------------------------------------------------------------------
  -- Verify
  ------------------------------------------------------------------------------

  -- Undo the read offset address to restore incrementing data
  -- . Single write - double read:
  wrr_mod_dat_a    <= TO_UINT(wrr_rd_dat_a) mod g_page_sz;
  wrr_mod_dat_b    <= TO_UINT(wrr_rd_dat_b) mod g_page_sz;
  wrr_offset_dat_a <= 0 when wrr_mod_dat_a = c_offset_a else g_page_sz when wrr_mod_dat_a = 0;
  wrr_offset_dat_b <= 0 when wrr_mod_dat_b = c_offset_b else g_page_sz when wrr_mod_dat_b = 0;
  wrr_result_dat_a <= INCR_UVEC(wrr_rd_dat_a, wrr_offset_dat_a);
  wrr_result_dat_b <= INCR_UVEC(wrr_rd_dat_b, wrr_offset_dat_b);
  -- . Double write - double read:
  wwrr_mod_dat_a    <= TO_UINT(wwrr_rd_dat_a) mod g_page_sz;
  wwrr_mod_dat_b    <= TO_UINT(wwrr_rd_dat_b) mod g_page_sz;
  wwrr_offset_dat_a <= 0 when wwrr_mod_dat_a = c_offset_a else g_page_sz when wwrr_mod_dat_a = 0;
  wwrr_offset_dat_b <= 0 when wwrr_mod_dat_b = c_offset_b else g_page_sz when wwrr_mod_dat_b = 0;
  wwrr_result_dat_a <= INCR_UVEC(wwrr_rd_dat_a, wrr_offset_dat_a);
  wwrr_result_dat_b <= INCR_UVEC(wwrr_rd_dat_b, wrr_offset_dat_b);

  -- Verify that the read data is incrementing data
  -- . Single write - double read:
  proc_common_verify_data(c_rl, clk, verify_en, ready, wrr_rd_val_a, wrr_result_dat_a, prev_wrr_result_dat_a);
  proc_common_verify_data(c_rl, clk, verify_en, ready, wrr_rd_val_b, wrr_result_dat_b, prev_wrr_result_dat_b);
  -- Double write - double read:
  proc_common_verify_data(c_rl, clk, verify_en, ready, wwrr_rd_val_a, wwrr_result_dat_a, prev_wwrr_result_dat_a);
  proc_common_verify_data(c_rl, clk, verify_en, ready, wwrr_rd_val_b, wwrr_result_dat_b, prev_wwrr_result_dat_b);
end tb;
