-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Reuse from LOFAR rad_rdframe.vhd and rad_rdframe(rtl).vhd

library IEEE;
use IEEE.std_logic_1164.all;

-- Purpose:
--   Read or flush one frame from a FIFO.
-- Description:
--   The frame in the FIFO must be marked by sof and eof. If frm_req is high
--   then a new frame will be read form the FIFO and the frm_ack output gives
--   a pulse to indicate that frm_req can be released. A subsequent frm_req
--   will be acknowledged after the current frame has been read.
--   If frm_flush is high at frm_req then the frame will be read, but the output
--   val, sof, and eof remain low. When the frame has been read then the
--   frm_done output gives a pulse.
--   If the FIFO is empty then frm_ack output gives a pulse but then frm_busy
--   remains low.
--   With g_throttle_num / g_throttle_den the read output rate is set,
--   g_throttle_den=1 yields full speed.
--   A frame that does not start with a sof gets flushed too and is reported
--   via output frm_err. Typically output frm_err can be left OPEN.
-- Remarks:
--   . This sink ready latency = 1, applies to rd_req and rd_val
--   . This source ready latency is not used, because there is no out_req.
--     Hence this source assumes the downstream sink is always ready and pushes
--     a frame of throttled data out after a frm_req.
--   . To make use of frm_cnt connect frm_req to the eop of the FIFO input,
--     because then there is a frame pending in the FIFO. If frm_req='1' always
--     then frm_cnt is void.
--   . If the FIFO runs empty while reading a frame, then the throttling will
--     not output data when it can. To avoid this e.g. connect eof to frm_req.
--   . If the FIFO contains more than 2 frames then more frm_req requests are
--     needed to to read them all out. This is facilitated by frm_cnt>0.
--     If running the FIFO empty is no issue, then keeping frm_req high is also
--     suitable.
--   . Throttling after sof may not be needed if the sof data contains e.g. a
--     frame sequence number (then use g_throttle_eof=FALSE).
--   . The output throttle after the eof to avoid a new sof to follow the
--     previous eof too soon may not be needed (then use g_throttle_eof=FALSE).

entity dp_frame_rd is
  generic (
    g_dat_w          : positive := 16;
    g_empty_w        : natural := 1;
    g_channel_w      : natural := 1;
    g_error_w        : natural := 1;
    g_frm_cnt_max    : natural := 0;  -- maximum number of frames that may arrive while current frame is read, can be > 0 if subsequent frames are smaller or arrive in bursts
    g_throttle_num   : positive := 1;  -- numerator <= g_throttle_den
    g_throttle_den   : positive := 1;  -- denominator (use 1 for full speed, i.e no output throttling)
    g_throttle_sof   : boolean := false;  -- when false immediately do request next data after sof
    g_throttle_eof   : boolean := false  -- when false immediately wait for next frame request after eof
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    frm_req          : in  std_logic;  -- request a frame
    frm_flush        : in  std_logic;  -- request to flush the frame
    frm_ack          : out std_logic;  -- acknowledge the request
    frm_busy         : out std_logic;  -- acknowledge indicates sof (busy) or no frame (not busy)
    frm_err          : out std_logic;  -- the busy frame is erroneous
    frm_done         : out std_logic;  -- the busy frame done indicates the eof

    rd_req           : out std_logic;
    rd_dat           : in  std_logic_vector(g_dat_w - 1 downto 0);
    rd_empty         : in  std_logic_vector(g_empty_w - 1 downto 0) := (others => '0');
    rd_channel       : in  std_logic_vector(g_channel_w - 1 downto 0) := (others => '0');
    rd_err           : in  std_logic_vector(g_error_w - 1 downto 0) := (others => '0');
    rd_sync          : in  std_logic := '0';
    rd_val           : in  std_logic;
    rd_sof           : in  std_logic;  -- no default, because frame signaling must be used
    rd_eof           : in  std_logic;  -- no default, because frame signaling must be used

    out_dat          : out std_logic_vector(g_dat_w - 1 downto 0);
    out_empty        : out std_logic_vector(g_empty_w - 1 downto 0);
    out_channel      : out std_logic_vector(g_channel_w - 1 downto 0);
    out_err          : out std_logic_vector(g_error_w - 1 downto 0);
    out_sync         : out std_logic;
    out_val          : out std_logic;
    out_sof          : out std_logic;
    out_eof          : out std_logic
  );
end dp_frame_rd;

architecture rtl of dp_frame_rd is
  type t_state_enum is (s_idle, s_sof, s_eof, s_err);

  constant c_throttle_cnt_max : natural := g_throttle_den + g_throttle_num;

  signal state             : t_state_enum;
  signal nxt_state         : t_state_enum;

  signal out_en            : std_logic;
  signal nxt_out_en        : std_logic;

  signal nxt_frm_ack       : std_logic;
  signal i_frm_busy        : std_logic;
  signal nxt_frm_busy      : std_logic;
  signal i_frm_err         : std_logic;
  signal nxt_frm_err       : std_logic;
  signal nxt_frm_done      : std_logic;
  signal frm_cnt           : natural range 0 to g_frm_cnt_max;
  signal nxt_frm_cnt       : natural;

  signal out_throttle      : std_logic;
  signal nxt_out_throttle  : std_logic;
  signal next_out_throttle : std_logic;
  signal throttle_cnt_en   : std_logic;
  signal throttle_cnt      : natural range 0 to c_throttle_cnt_max - 1;
  signal nxt_throttle_cnt  : natural;

  signal nxt_out_dat       : std_logic_vector(out_dat'range);
  signal nxt_out_empty     : std_logic_vector(out_empty'range);
  signal nxt_out_channel   : std_logic_vector(out_channel'range);
  signal nxt_out_err       : std_logic_vector(out_err'range);
  signal nxt_out_sync      : std_logic;
  signal nxt_out_val       : std_logic;
  signal nxt_out_sof       : std_logic;
  signal nxt_out_eof       : std_logic;
begin
  frm_busy <= i_frm_busy;
  frm_err  <= i_frm_err;

  p_clk: process(clk, rst)
  begin
    if rst = '1' then
      state        <= s_idle;
      out_en       <= '0';
      frm_ack      <= '0';
      i_frm_busy   <= '0';
      i_frm_err    <= '0';
      frm_done     <= '0';
      frm_cnt      <= 0;
      throttle_cnt <= 0;
      out_throttle <= '0';
      out_dat      <= (others => '0');
      out_empty    <= (others => '0');
      out_channel  <= (others => '0');
      out_err      <= (others => '0');
      out_sync     <= '0';
      out_val      <= '0';
      out_sof      <= '0';
      out_eof      <= '0';
    elsif rising_edge(clk) then
      state        <= nxt_state;
      out_en       <= nxt_out_en;
      frm_ack      <= nxt_frm_ack;
      i_frm_busy   <= nxt_frm_busy;
      i_frm_err    <= nxt_frm_err;
      frm_done     <= nxt_frm_done;
      frm_cnt      <= nxt_frm_cnt;
      throttle_cnt <= nxt_throttle_cnt;
      out_throttle <= nxt_out_throttle;
      out_dat      <= nxt_out_dat;
      out_empty    <= nxt_out_empty;
      out_channel  <= nxt_out_channel;
      out_err      <= nxt_out_err;
      out_sync     <= nxt_out_sync;
      out_val      <= nxt_out_val;
      out_sof      <= nxt_out_sof;
      out_eof      <= nxt_out_eof;
    end if;
  end process;

  nxt_out_dat     <= rd_dat;
  nxt_out_empty   <= rd_empty;
  nxt_out_channel <= rd_channel;
  nxt_out_err     <= rd_err;

  -- Note for g_throttle_den=1 the throttle control collapses to:
  -- . throttle_cnt      = 0
  -- . next_out_throttle = '1'
  -- . out_throttle      = '1'
  -- and throttle_cnt_en is don't care.
  p_throttle_cnt : process(throttle_cnt_en, throttle_cnt, out_throttle)
  begin
    nxt_throttle_cnt <= 0;
    nxt_out_throttle <= '1';
    if throttle_cnt_en = '1' then
      nxt_throttle_cnt <= throttle_cnt;
      nxt_out_throttle <= out_throttle;
      if throttle_cnt < g_throttle_den - 1 then
        nxt_throttle_cnt <= throttle_cnt + g_throttle_num;
        nxt_out_throttle <= '0';
      else
        nxt_throttle_cnt <= throttle_cnt + g_throttle_num - g_throttle_den;
        nxt_out_throttle <= '1';
      end if;
    end if;
  end process;

  next_out_throttle <= nxt_out_throttle;  -- note next_out_throttle high in is same cycle as out_throttle when g_throttle_den=1
                                          -- else next_out_throttle is high one cycle before out_throttle when g_throttle_den>1

  p_frm_cnt : process(frm_cnt, frm_req, nxt_frm_done)
  begin
    nxt_frm_cnt <= frm_cnt;
    if not (frm_req = '1' and nxt_frm_done = '1') then
      -- check need to increment or decrement frm_cnt
      if frm_req = '1' and frm_cnt < g_frm_cnt_max then
        nxt_frm_cnt <= frm_cnt + 1;  -- clip at g_frm_cnt_max, application should set g_frm_cnt_max sufficiently large to avoid clipping or set g_frm_cnt_max=0 to effectively disable frame rd due to frm_cnt>0
      end if;
      if nxt_frm_done = '1' and frm_cnt > 0 then
        nxt_frm_cnt <= frm_cnt - 1;  -- to be sure do check that frm_cnt>0
      end if;
    end if;
  end process;

  p_output: process(state, frm_req, frm_cnt, frm_flush, i_frm_busy, i_frm_err, rd_sync, rd_val, rd_sof, rd_eof, out_en, next_out_throttle, out_throttle)
  begin
    rd_req           <= '0';
    nxt_frm_ack      <= '0';
    nxt_frm_busy     <= i_frm_busy;
    nxt_frm_err      <= i_frm_err;
    nxt_frm_done     <= '0';
    throttle_cnt_en  <= '0';
    nxt_out_en       <= out_en;
    nxt_out_sync     <= '0';
    nxt_out_val      <= '0';
    nxt_out_sof      <= '0';
    nxt_out_eof      <= '0';
    nxt_state        <= state;

    case state is
      when s_idle =>
        nxt_frm_busy    <= '0';  -- default no frame
        nxt_frm_err     <= '0';  -- default no error
        throttle_cnt_en <= '1';  -- keep on throttling after previous eof
        if out_throttle = '1' then
          throttle_cnt_en <= '0';  -- stop throttling until next sof
        end if;
        if next_out_throttle = '1' then
          if frm_req = '1' or frm_cnt > 0 then  -- check for new frame or any already pending frame(s)
            rd_req      <= '1';  -- begin reading the FIFO, e.g. if eof is connected to frm_req then there is a frame pending
            nxt_state   <= s_sof;
            nxt_out_en  <= not frm_flush;  -- output enable for read frame or output disable for flush frame
          end if;
        end if;
      when s_sof =>  -- out_throttle='1', no need to check
        nxt_frm_ack <= '1';  -- acknowledge frame request
        if rd_sof = '1' then  -- FIFO contains start of frame
          nxt_frm_busy    <= '1';
          throttle_cnt_en <= '1';  -- throttle during frame
          nxt_out_val     <= out_en;  -- take care in case last time the FIFO contained > 1 frames
          nxt_out_sof     <= out_en;  -- output pending rd_sof and valid rd_dat
          nxt_state       <= s_eof;
          if g_throttle_den = 1 or g_throttle_sof = false then
            rd_req <= '1';  -- keep on reading the FIFO after the sof
            throttle_cnt_en <= '0';
          end if;
        elsif rd_val = '0' then  -- FIFO is empty, it did not contain a frame which. This can happen if frm_req is kept '1' always.
          nxt_state       <= s_idle;  -- wait for next frm_req
        else  -- FIFO is out of frame sync
          nxt_frm_busy    <= '1';
          nxt_frm_err     <= '1';  -- report error unexpected valid data that is not sof
          rd_req          <= '1';  -- try to recover by flush reading the partial frame from FIFO, no need to throttle
          nxt_state       <= s_err;
        end if;
      when s_eof =>
        throttle_cnt_en <= '1';  -- throttle during frame
        if next_out_throttle = '1' then
          rd_req <= '1';  -- keep on reading the FIFO
        end if;
        if out_throttle = '1' then
          if rd_val = '1' then  -- for proper throttling there should be sufficient data in the FIFO
            nxt_out_val <= out_en;  -- output val
          end if;
          if rd_eof = '1' then
            nxt_out_eof  <= out_en;  -- output eof, stop reading the FIFO
            if rd_sync = '1' then
              nxt_out_sync <= out_en;  -- Pass the rd_sync at the eop to get the LOFAR style of sync before sop that fits dp_frame_fsn, instead of at sop as is used in DP for UniBoard
            end if;
            rd_req       <= '0';  -- stop reading the FIFO, necessary when g_throttle_den=1
            nxt_frm_done <= '1';
            nxt_state    <= s_idle;
            if g_throttle_eof = false then
              throttle_cnt_en <= '0';
            end if;
          end if;
        end if;
      when others =>  -- s_err             -- try to recover from FIFO lost frame sync, no need to throttle
        if rd_eof = '0' then
          rd_req       <= '1';  -- keep on flush reading the FIFO until there is an eof
        else
          nxt_frm_done <= '1';  -- wait for next frm_req
          nxt_state    <= s_idle;
        end if;
    end case;
  end process;
end rtl;
