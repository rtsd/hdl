--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;

package lofar2_unb2c_sdp_station_pkg is
 -----------------------------------------------------------------------------
  -- Revision control
  -----------------------------------------------------------------------------

  type t_lofar2_unb2c_sdp_station_config is record
    no_jesd                   : boolean;
    use_fsub                  : boolean;
    use_oversample            : boolean;
    use_bf                    : boolean;
    use_bdo_transpose         : boolean;
    nof_bdo_destinations_max  : natural;  -- <= c_sdp_bdo_mm_nof_destinations_max = 32 in sdp_bdo_pkg.vhd
    use_xsub                  : boolean;
    use_ring                  : boolean;
    P_sq                      : natural;
  end record;

  constant c_ait        : t_lofar2_unb2c_sdp_station_config := (false, false, false, false, false, 1, false, false, 0);
  constant c_fsub       : t_lofar2_unb2c_sdp_station_config := (false, true,  false, false, false, 1, false, false, 0);
  -- use c_bf on one node also to simulate bdo transpose
  -- use c_bf_ring with ring also to simulate bdo identity
  constant c_bf         : t_lofar2_unb2c_sdp_station_config := (false, true,  false, true,  true, 16, false, false, 0);
  constant c_bf_wg      : t_lofar2_unb2c_sdp_station_config := (true,  true,  false, true,  true, 16, false, false, 0);
  constant c_bf_ring    : t_lofar2_unb2c_sdp_station_config := (false, true,  false, true,  false, 1, false, true,  0);
  constant c_xsub_one   : t_lofar2_unb2c_sdp_station_config := (false, true,  false, false, false, 1, true,  false, 1);
  constant c_xsub_ring  : t_lofar2_unb2c_sdp_station_config := (false, true,  false, false, false, 1, true,  true,  9);
  constant c_full_wg    : t_lofar2_unb2c_sdp_station_config := (true,  true,  false, true,  true,  1, true,  true,  9);
  -- Use c_full for LOFAR2 Station SDP operations
  constant c_full       : t_lofar2_unb2c_sdp_station_config := (false, true,  false, true,  true, 16, true,  true,  9);
  constant c_full_wg_os : t_lofar2_unb2c_sdp_station_config := (true,  true,  true,  true,  true,  1, true,  true,  9);
  constant c_full_os    : t_lofar2_unb2c_sdp_station_config := (false, true,  true,  true,  true,  1, true,  true,  9);

  -- Function to select the revision configuration.
  function func_sel_revision_rec(g_design_name : string) return t_lofar2_unb2c_sdp_station_config;
end lofar2_unb2c_sdp_station_pkg;

package body lofar2_unb2c_sdp_station_pkg is
  function func_sel_revision_rec(g_design_name : string) return t_lofar2_unb2c_sdp_station_config is
  begin
    if    g_design_name = "lofar2_unb2c_sdp_station_adc"        then return c_ait;
    elsif g_design_name = "lofar2_unb2c_sdp_station_fsub"       then return c_fsub;
    elsif g_design_name = "lofar2_unb2c_sdp_station_bf"         then return c_bf;
    elsif g_design_name = "lofar2_unb2c_sdp_station_bf_wg"      then return c_bf_wg;
    elsif g_design_name = "lofar2_unb2c_sdp_station_bf_ring"    then return c_bf_ring;
    elsif g_design_name = "lofar2_unb2c_sdp_station_xsub_one"   then return c_xsub_one;
    elsif g_design_name = "lofar2_unb2c_sdp_station_xsub_ring"  then return c_xsub_ring;
    elsif g_design_name = "lofar2_unb2c_sdp_station_full_wg"    then return c_full_wg;
    elsif g_design_name = "lofar2_unb2c_sdp_station_full"       then return c_full;
    elsif g_design_name = "disturb2_unb2c_sdp_station_full_wg"  then return c_full_wg_os;
    elsif g_design_name = "disturb2_unb2c_sdp_station_full"     then return c_full_os;
    else  return c_full;
    end if;

  end;
end lofar2_unb2c_sdp_station_pkg;
