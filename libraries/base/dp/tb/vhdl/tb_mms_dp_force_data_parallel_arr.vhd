-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra, 10 Apr 2017
-- Purpose: Verify mms_dp_force_data_parallel_arr and mms_dp_force_data_parallel
-- Description:
-- Usage:
-- > as 10
-- > run -all
-- In wave window view sosi.data,re,im in radix decimal
-- The tb is self stopping and self checking.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_mms_dp_force_data_parallel_arr is
  generic (
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_random;  -- always active or random stimuli valid flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active or random verify  ready flow control
    g_nof_streams            : natural := 1;  -- >= 1
    g_dat_w                  : natural := 5;  -- must be <= 32 to fit INTEGER range
    g_force_stream           : integer := 0;  -- must be < g_nof_streams, force data on this stream
    g_force_data             : integer := -7;
    g_force_re               : integer := 3;
    g_force_im               : integer := -5;
    g_increment_data         : integer := 1;
    g_increment_re           : integer := 0;
    g_increment_im           : integer := 0;
    g_increment_on_sop       : boolean := false;  -- in this tb use same generic for data, re, im
    g_restart_on_sync        : boolean := false;  -- in this tb use same generic for data, re, im
    g_restart_on_sop         : boolean := false  -- in this tb use same generic for data, re, im
  );
end tb_mms_dp_force_data_parallel_arr;

architecture tb of tb_mms_dp_force_data_parallel_arr is
  constant c_mm_clk_period              : time := 20 ns;
  constant c_dp_clk_period              : time := 10 ns;
  constant c_cross_clock_domain_latency : natural := 20;

  constant c_init_data                  : integer := 0;
  constant c_init_channel               : integer := 11;  -- use recognizable value, will not change
  constant c_init_error                 : integer := 12;  -- use recognizable value, will not change
  constant c_nof_data_per_block         : integer := 17;
  constant c_nof_block_per_sync         : integer := 10;
  constant c_nof_blocks_per_test        : integer := c_nof_block_per_sync * 3;

  constant c_mm_reg_span                : natural := 4;  -- = 2**c_mm_reg.adr_w, span per stream
  constant c_force_en                   : natural := 1;
  constant c_force_dis                  : natural := 0;

  signal tb_end                : std_logic := '0';
  signal mm_clk                : std_logic := '1';
  signal mm_rst                : std_logic := '1';
  signal dp_clk                : std_logic := '1';
  signal dp_rst                : std_logic := '1';

  signal random_valid          : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_ready          : std_logic_vector(16 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal stimuli_en            : std_logic := '0';
  signal verify_en             : std_logic := '0';
  signal force_en              : std_logic := '0';

  signal snk_out               : t_dp_siso;
  signal snk_in                : t_dp_sosi;
  signal snk_in_re             : t_dp_sosi;
  signal snk_in_complex        : t_dp_sosi;
  signal snk_in_complex_dly    : t_dp_sosi;
  signal snk_out_arr           : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal snk_in_arr            : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal src_in_arr            : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal src_out_arr           : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal prev_src_out_arr      : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal src_in                : t_dp_siso;

  signal reg_force_data_mosi   : t_mem_mosi := c_mem_mosi_rst;
  signal reg_force_data_miso   : t_mem_miso;
begin
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  random_valid <= func_common_random(random_valid) when rising_edge(dp_clk);
  random_ready <= func_common_random(random_ready) when rising_edge(dp_clk);

  stimuli_en   <= random_valid(random_valid'high) when g_flow_control_stimuli = e_random else '1';
  src_in.ready <= random_ready(random_ready'high) when g_flow_control_verify = e_random else '1';
  src_in.xon   <= '1';

  src_in_arr <= (others => src_in);

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------
  p_stimuli_st : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    -- Initialisations
    v_sosi.bsn     := TO_DP_BSN(0);
    v_sosi.data    := TO_DP_SDATA(0);

    snk_in <= c_dp_sosi_rst;
    proc_common_wait_until_low(dp_clk, mm_rst);
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(dp_clk, 5);

    -- Generate packets
    while tb_end = '0' loop
      v_sosi.sync    := sel_a_b((unsigned(v_sosi.bsn) mod c_nof_block_per_sync) = 0, '1', '0');  -- insert sync starting at BSN=0 and with period g_sync_period
      v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_dat_w - 1 downto 0));  -- wrap when >= 2**g_dat_w
      -- Send block
      proc_dp_gen_block_data(g_dat_w, c_init_data, c_nof_data_per_block, c_init_channel, c_init_error, v_sosi.sync, v_sosi.bsn, dp_clk, stimuli_en, snk_out, snk_in);
      -- Prepare for next block
      v_sosi.bsn     := INCR_UVEC(v_sosi.bsn, 1);
      v_sosi.data    := INCR_UVEC(v_sosi.data, c_nof_data_per_block);
    end loop;

    wait;
  end process;

  snk_in_re      <= func_dp_stream_set_data(snk_in,    INCR_UVEC(snk_in.data, 1), "RE");  -- apply re = data+1
  snk_in_complex <= func_dp_stream_set_data(snk_in_re, INCR_UVEC(snk_in.data, 2), "IM");  -- apply im = data+2

  u_snk_in_complex_dly : entity work.dp_pipeline
  generic map (
    g_pipeline   => 1  -- latency of DUT
  )
  port map (
    rst      => dp_rst,
    clk      => dp_clk,
    snk_out  => OPEN,
    snk_in   => snk_in_complex,
    src_in   => src_in_arr(0),
    src_out  => snk_in_complex_dly
  );

  snk_out    <= snk_out_arr(0);  -- use stream 0 for flow control, all tb streams have same flow control
  snk_in_arr <= (others => snk_in_complex);  -- apply same default tb data to all streams

  p_stimuli_mm : process
    variable v_force_data : integer := g_force_data;
  begin
    proc_common_wait_until_low(dp_clk, mm_rst);
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(mm_clk, 5);

    -- Test default pass on the data unchanged
    verify_en <= '1';
    for I in 0 to c_nof_blocks_per_test - 1 loop
      proc_common_wait_until_hi_lo(dp_clk, snk_in.eop);
    end loop;
    verify_en <= '0';

    -- Test force data on stream g_force_stream
    force_en <= '1';
    proc_mem_mm_bus_wr(g_force_stream * c_mm_reg_span + 0, c_force_en,   mm_clk, reg_force_data_miso, reg_force_data_mosi);
    proc_mem_mm_bus_wr(g_force_stream * c_mm_reg_span + 1, g_force_data, mm_clk, reg_force_data_miso, reg_force_data_mosi);
    proc_mem_mm_bus_wr(g_force_stream * c_mm_reg_span + 2, g_force_re,   mm_clk, reg_force_data_miso, reg_force_data_mosi);
    proc_mem_mm_bus_wr(g_force_stream * c_mm_reg_span + 3, g_force_im,   mm_clk, reg_force_data_miso, reg_force_data_mosi);
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);

    proc_common_wait_until_hi_lo(dp_clk, snk_in.eop);
    verify_en <= '1';
    for I in 0 to c_nof_blocks_per_test - 1 loop
      proc_common_wait_until_hi_lo(dp_clk, snk_in.eop);
    end loop;
    verify_en <= '0';

    -- Test default pass on the data unchanged
    force_en <= '0';
    proc_mem_mm_bus_wr(g_force_stream * c_mm_reg_span + 0, c_force_dis,   mm_clk, reg_force_data_miso, reg_force_data_mosi);
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);
    proc_common_wait_some_cycles(dp_clk, c_cross_clock_domain_latency);

    verify_en <= '1';
    for I in 0 to c_nof_blocks_per_test - 1 loop
      proc_common_wait_until_hi_lo(dp_clk, snk_in.eop);
    end loop;
    verify_en <= '0';

    tb_end <= '1';
    wait;
  end process;

  -- capture previous valid src_out_arr to verify increments
  prev_src_out_arr <= src_out_arr when rising_edge(dp_clk) and snk_in_complex_dly.valid = '1';

  p_verify : process(dp_clk)
    variable v_exp_data : integer;
    variable v_exp_re   : integer;
    variable v_exp_im   : integer;
  begin
    if rising_edge(dp_clk) then
      if verify_en = '1' and snk_in_complex_dly.valid = '1' then
        for I in 0 to g_nof_streams - 1 loop
          -- verify unchanged sosi fields (= pipeline)
          assert src_out_arr(I).sync = snk_in_complex_dly.sync
            report "Unexpected sync"
            severity ERROR;
          assert src_out_arr(I).sop = snk_in_complex_dly.sop
            report "Unexpected sop"
            severity ERROR;
          assert src_out_arr(I).eop = snk_in_complex_dly.eop
            report "Unexpected eop"
            severity ERROR;
          assert src_out_arr(I).valid = snk_in_complex_dly.valid
            report "Unexpected valid"
            severity ERROR;
          assert signed(src_out_arr(I).bsn) = signed(snk_in_complex_dly.bsn)
            report "Unexpected bsn"
            severity ERROR;
          assert signed(src_out_arr(I).channel) = signed(snk_in_complex_dly.channel)
            report "Unexpected channel"
            severity ERROR;
          assert signed(src_out_arr(I).err) = signed(snk_in_complex_dly.err)
            report "Unexpected err"
            severity ERROR;

          if force_en = '0' or I /= g_force_stream then
            -- verify default data pass on (= pipeline)
            assert signed(src_out_arr(I).data) = signed(snk_in_complex_dly.data)
              report "Unexpected data"
              severity ERROR;
            assert signed(src_out_arr(I).re) = signed(snk_in_complex_dly.re)
              report "Unexpected real"
              severity ERROR;
            assert signed(src_out_arr(I).im) = signed(snk_in_complex_dly.im)
              report "Unexpected imag"
              severity ERROR;
          else
            -- verify default force data on stream I=g_force_stream
            v_exp_data := TO_SINT(INCR_DP_SDATA(   prev_src_out_arr(I).data, g_increment_data, g_dat_w));
            v_exp_re   := TO_SINT(INCR_DP_DSP_DATA(prev_src_out_arr(I).re,   g_increment_re,   g_dat_w));
            v_exp_im   := TO_SINT(INCR_DP_DSP_DATA(prev_src_out_arr(I).im,   g_increment_im,   g_dat_w));
            if g_increment_on_sop = true and snk_in_complex_dly.sop = '0' then
              v_exp_data := TO_SINT(prev_src_out_arr(I).data);
              v_exp_re   := TO_SINT(prev_src_out_arr(I).re);
              v_exp_im   := TO_SINT(prev_src_out_arr(I).im);
            end if;

            -- .data
            if g_increment_data = 0 then
              assert TO_SINT(src_out_arr(I).data) = g_force_data
                report "Unexpected force data"
                severity ERROR;
            else
              if g_restart_on_sync = true and snk_in_complex_dly.sync = '1' then
                assert TO_SINT(src_out_arr(I).data) = g_force_data
                  report "Unexpected restart force data at sync"
                  severity ERROR;
              elsif g_restart_on_sop = true and snk_in_complex_dly.sop = '1' then
                assert TO_SINT(src_out_arr(I).data) = g_force_data
                  report "Unexpected restart force data at sop"
                  severity ERROR;
              else
                assert TO_SINT(src_out_arr(I).data) = v_exp_data
                  report "Unexpected increment data"
                  severity ERROR;
              end if;
            end if;

            -- .re
            if g_increment_re = 0 then
              assert TO_SINT(src_out_arr(I).re) = g_force_re
                report "Unexpected force re"
                severity ERROR;
            else
              if g_restart_on_sync = true and snk_in_complex_dly.sync = '1' then
                assert TO_SINT(src_out_arr(I).re) = g_force_re
                  report "Unexpected restart force re at sync"
                  severity ERROR;
              elsif g_restart_on_sop = true and snk_in_complex_dly.sop = '1' then
                assert TO_SINT(src_out_arr(I).re) = g_force_re
                  report "Unexpected restart force re at sop"
                  severity ERROR;
              else
                assert TO_SINT(src_out_arr(I).re) = v_exp_re
                  report "Unexpected increment re"
                  severity ERROR;
              end if;
            end if;

            -- .im
            if g_increment_im = 0 then
              assert TO_SINT(src_out_arr(I).im) = g_force_im
                report "Unexpected force im"
                severity ERROR;
            else
              if g_restart_on_sync = true and snk_in_complex_dly.sync = '1' then
                assert TO_SINT(src_out_arr(I).im) = g_force_im
                  report "Unexpected restart force im at sync"
                  severity ERROR;
              elsif g_restart_on_sop = true and snk_in_complex_dly.sop = '1' then
                assert TO_SINT(src_out_arr(I).im) = g_force_im
                  report "Unexpected restart force im at sop"
                  severity ERROR;
              else
                assert TO_SINT(src_out_arr(I).im) = v_exp_im
                  report "Unexpected increment im"
                  severity ERROR;
              end if;
            end if;
          end if;
        end loop;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  gen_one : if g_nof_streams = 1 generate
    u_dut_one : entity work.mms_dp_force_data_parallel
    generic map (
      g_dat_w                 => g_dat_w,
      g_increment_data        => g_increment_data,
      g_increment_re          => g_increment_re,
      g_increment_im          => g_increment_im,
      g_increment_data_on_sop => g_increment_on_sop,
      g_increment_re_on_sop   => g_increment_on_sop,
      g_increment_im_on_sop   => g_increment_on_sop,
      g_restart_data_on_sync  => g_restart_on_sync,
      g_restart_re_on_sync    => g_restart_on_sync,
      g_restart_im_on_sync    => g_restart_on_sync,
      g_restart_data_on_sop   => g_restart_on_sop,
      g_restart_re_on_sop     => g_restart_on_sop,
      g_restart_im_on_sop     => g_restart_on_sop
    )
    port map (
      -- Clocks and reset
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,
      -- MM control
      reg_force_data_mosi => reg_force_data_mosi,
      reg_force_data_miso => reg_force_data_miso,
      -- ST sink
      snk_out             => snk_out_arr(0),
      snk_in              => snk_in_arr(0),
      -- ST source
      src_in              => src_in_arr(0),
      src_out             => src_out_arr(0)
    );
  end generate;

  gen_arr : if g_nof_streams > 1 generate
    u_dut_arr : entity work.mms_dp_force_data_parallel_arr
    generic map (
      g_nof_streams           => g_nof_streams,
      g_dat_w                 => g_dat_w,
      g_increment_data        => g_increment_data,
      g_increment_re          => g_increment_re,
      g_increment_im          => g_increment_im,
      g_increment_data_on_sop => g_increment_on_sop,
      g_increment_re_on_sop   => g_increment_on_sop,
      g_increment_im_on_sop   => g_increment_on_sop,
      g_restart_data_on_sync  => g_restart_on_sync,
      g_restart_re_on_sync    => g_restart_on_sync,
      g_restart_im_on_sync    => g_restart_on_sync,
      g_restart_data_on_sop   => g_restart_on_sop,
      g_restart_re_on_sop     => g_restart_on_sop,
      g_restart_im_on_sop     => g_restart_on_sop
    )
    port map (
      -- Clocks and reset
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,
      -- MM control
      reg_force_data_mosi => reg_force_data_mosi,
      reg_force_data_miso => reg_force_data_miso,
      -- ST sink
      snk_out_arr         => snk_out_arr,
      snk_in_arr          => snk_in_arr,
      -- ST source
      src_in_arr          => src_in_arr,
      src_out_arr         => src_out_arr
    );
  end generate;
end tb;
