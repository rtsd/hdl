// (C) 1992-2018 Intel Corporation.                            
// Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
// and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
// and/or other countries. Other marks and brands may be claimed as the property  
// of others. See Trademarks on intel.com for full list of Intel trademarks or    
// the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
// Your use of Intel Corporation's design tools, logic functions and other        
// software and tools, and its AMPP partner logic functions, and any output       
// files any of the foregoing (including device programming or simulation         
// files), and any associated documentation or information are expressly subject  
// to the terms and conditions of the Altera Program License Subscription         
// Agreement, Intel MegaCore Function License Agreement, or other applicable      
// license agreement, including, without limitation, that your use is for the     
// sole purpose of programming logic devices manufactured by Intel and sold by    
// Intel or its authorized distributors.  Please refer to the applicable          
// agreement for further details.                                                 

module freeze_wrapper(

  //input           freeze,

  //////// board ports //////////
  input           board_kernel_clk_clk,
  input           board_kernel_clk2x_clk,
  input           board_kernel_reset_reset_n,
  output [0:0]    board_kernel_irq_irq,
  //input [30:0]    board_acl_internal_snoop_data,
  //input           board_acl_internal_snoop_valid,
  //output          board_acl_internal_snoop_ready,
  output          board_kernel_cra_waitrequest,
  output [63:0]   board_kernel_cra_readdata,
  output          board_kernel_cra_readdatavalid,
  input [0:0]     board_kernel_cra_burstcount,
  input [63:0]    board_kernel_cra_writedata,
  input [29:0]    board_kernel_cra_address,
  input           board_kernel_cra_write,
  input           board_kernel_cra_read,
  input [7:0]     board_kernel_cra_byteenable,
  input           board_kernel_cra_debugaccess,


  input  wire [71:0]  board_kernel_stream_src_mm_io_data,
  input  wire         board_kernel_stream_src_mm_io_valid,
  output wire         board_kernel_stream_src_mm_io_ready,
  output wire [31:0]  board_kernel_stream_snk_mm_io_data,
  output wire         board_kernel_stream_snk_mm_io_valid,
  input  wire         board_kernel_stream_snk_mm_io_ready,


  input  wire [103:0] board_kernel_stream_src_10GbE_ring_0_data,
  input  wire         board_kernel_stream_src_10GbE_ring_0_valid,
  output wire         board_kernel_stream_src_10GbE_ring_0_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_ring_0_data,
  output wire         board_kernel_stream_snk_10GbE_ring_0_valid,
  input  wire         board_kernel_stream_snk_10GbE_ring_0_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_ring_1_data,
  input  wire         board_kernel_stream_src_10GbE_ring_1_valid,
  output wire         board_kernel_stream_src_10GbE_ring_1_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_ring_1_data,
  output wire         board_kernel_stream_snk_10GbE_ring_1_valid,
  input  wire         board_kernel_stream_snk_10GbE_ring_1_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_ring_2_data,
  input  wire         board_kernel_stream_src_10GbE_ring_2_valid,
  output wire         board_kernel_stream_src_10GbE_ring_2_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_ring_2_data,
  output wire         board_kernel_stream_snk_10GbE_ring_2_valid,
  input  wire         board_kernel_stream_snk_10GbE_ring_2_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_ring_3_data,
  input  wire         board_kernel_stream_src_10GbE_ring_3_valid,
  output wire         board_kernel_stream_src_10GbE_ring_3_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_ring_3_data,
  output wire         board_kernel_stream_snk_10GbE_ring_3_valid,
  input  wire         board_kernel_stream_snk_10GbE_ring_3_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_ring_4_data,
  input  wire         board_kernel_stream_src_10GbE_ring_4_valid,
  output wire         board_kernel_stream_src_10GbE_ring_4_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_ring_4_data,
  output wire         board_kernel_stream_snk_10GbE_ring_4_valid,
  input  wire         board_kernel_stream_snk_10GbE_ring_4_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_ring_5_data,
  input  wire         board_kernel_stream_src_10GbE_ring_5_valid,
  output wire         board_kernel_stream_src_10GbE_ring_5_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_ring_5_data,
  output wire         board_kernel_stream_snk_10GbE_ring_5_valid,
  input  wire         board_kernel_stream_snk_10GbE_ring_5_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_ring_6_data,
  input  wire         board_kernel_stream_src_10GbE_ring_6_valid,
  output wire         board_kernel_stream_src_10GbE_ring_6_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_ring_6_data,
  output wire         board_kernel_stream_snk_10GbE_ring_6_valid,
  input  wire         board_kernel_stream_snk_10GbE_ring_6_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_ring_7_data,
  input  wire         board_kernel_stream_src_10GbE_ring_7_valid,
  output wire         board_kernel_stream_src_10GbE_ring_7_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_ring_7_data,
  output wire         board_kernel_stream_snk_10GbE_ring_7_valid,
  input  wire         board_kernel_stream_snk_10GbE_ring_7_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_qsfp_0_data,
  input  wire         board_kernel_stream_src_10GbE_qsfp_0_valid,
  output wire         board_kernel_stream_src_10GbE_qsfp_0_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_qsfp_0_data,
  output wire         board_kernel_stream_snk_10GbE_qsfp_0_valid,
  input  wire         board_kernel_stream_snk_10GbE_qsfp_0_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_qsfp_1_data,
  input  wire         board_kernel_stream_src_10GbE_qsfp_1_valid,
  output wire         board_kernel_stream_src_10GbE_qsfp_1_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_qsfp_1_data,
  output wire         board_kernel_stream_snk_10GbE_qsfp_1_valid,
  input  wire         board_kernel_stream_snk_10GbE_qsfp_1_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_qsfp_2_data,
  input  wire         board_kernel_stream_src_10GbE_qsfp_2_valid,
  output wire         board_kernel_stream_src_10GbE_qsfp_2_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_qsfp_2_data,
  output wire         board_kernel_stream_snk_10GbE_qsfp_2_valid,
  input  wire         board_kernel_stream_snk_10GbE_qsfp_2_ready,
  
  input  wire [103:0] board_kernel_stream_src_10GbE_qsfp_3_data,
  input  wire         board_kernel_stream_src_10GbE_qsfp_3_valid,
  output wire         board_kernel_stream_src_10GbE_qsfp_3_ready,
  output wire [103:0] board_kernel_stream_snk_10GbE_qsfp_3_data,
  output wire         board_kernel_stream_snk_10GbE_qsfp_3_valid,
  input  wire         board_kernel_stream_snk_10GbE_qsfp_3_ready,
         
  input  wire [167:0] board_kernel_stream_src_lane_0_data,
  input  wire         board_kernel_stream_src_lane_0_valid,
  output wire         board_kernel_stream_src_lane_0_ready,
  output wire [167:0] board_kernel_stream_snk_lane_0_data,
  output wire         board_kernel_stream_snk_lane_0_valid,
  input  wire         board_kernel_stream_snk_lane_0_ready, 
 
  input  wire [167:0] board_kernel_stream_src_lane_1_data,
  input  wire         board_kernel_stream_src_lane_1_valid,
  output wire         board_kernel_stream_src_lane_1_ready,
  output wire [167:0] board_kernel_stream_snk_lane_1_data,
  output wire         board_kernel_stream_snk_lane_1_valid,
  input  wire         board_kernel_stream_snk_lane_1_ready, 
 
  input  wire [167:0] board_kernel_stream_src_lane_2_data,
  input  wire         board_kernel_stream_src_lane_2_valid,
  output wire         board_kernel_stream_src_lane_2_ready,
  output wire [167:0] board_kernel_stream_snk_lane_2_data,
  output wire         board_kernel_stream_snk_lane_2_valid,
  input  wire         board_kernel_stream_snk_lane_2_ready, 

  input  wire [167:0] board_kernel_stream_src_lane_3_data,
  input  wire         board_kernel_stream_src_lane_3_valid,
  output wire         board_kernel_stream_src_lane_3_ready,
  output wire [167:0] board_kernel_stream_snk_lane_3_data,
  output wire         board_kernel_stream_snk_lane_3_valid,
  input  wire         board_kernel_stream_snk_lane_3_ready, 

  input  wire [167:0] board_kernel_stream_src_lane_4_data,
  input  wire         board_kernel_stream_src_lane_4_valid,
  output wire         board_kernel_stream_src_lane_4_ready,
  output wire [167:0] board_kernel_stream_snk_lane_4_data,
  output wire         board_kernel_stream_snk_lane_4_valid,
  input  wire         board_kernel_stream_snk_lane_4_ready, 
 
  input  wire [167:0] board_kernel_stream_src_lane_5_data,
  input  wire         board_kernel_stream_src_lane_5_valid,
  output wire         board_kernel_stream_src_lane_5_ready,
  output wire [167:0] board_kernel_stream_snk_lane_5_data,
  output wire         board_kernel_stream_snk_lane_5_valid,
  input  wire         board_kernel_stream_snk_lane_5_ready, 
 
  input  wire [167:0] board_kernel_stream_src_lane_6_data,
  input  wire         board_kernel_stream_src_lane_6_valid,
  output wire         board_kernel_stream_src_lane_6_ready,
  output wire [167:0] board_kernel_stream_snk_lane_6_data,
  output wire         board_kernel_stream_snk_lane_6_valid,
  input  wire         board_kernel_stream_snk_lane_6_ready, 
 
  input  wire [167:0] board_kernel_stream_src_lane_7_data,
  input  wire         board_kernel_stream_src_lane_7_valid,
  output wire         board_kernel_stream_src_lane_7_ready,
  output wire [167:0] board_kernel_stream_snk_lane_7_data,
  output wire         board_kernel_stream_snk_lane_7_valid,
  input  wire         board_kernel_stream_snk_lane_7_ready,

  output wire [167:0] board_kernel_stream_snk_rx_monitor_0_data,
  output wire         board_kernel_stream_snk_rx_monitor_0_valid,
  input  wire         board_kernel_stream_snk_rx_monitor_0_ready,
  output wire [167:0] board_kernel_stream_snk_tx_monitor_0_data,
  output wire         board_kernel_stream_snk_tx_monitor_0_valid,
  input  wire         board_kernel_stream_snk_tx_monitor_0_ready,
  
  output wire [167:0] board_kernel_stream_snk_rx_monitor_1_data,
  output wire         board_kernel_stream_snk_rx_monitor_1_valid,
  input  wire         board_kernel_stream_snk_rx_monitor_1_ready,
  output wire [167:0] board_kernel_stream_snk_tx_monitor_1_data,
  output wire         board_kernel_stream_snk_tx_monitor_1_valid,
  input  wire         board_kernel_stream_snk_tx_monitor_1_ready,
  
  output wire [167:0] board_kernel_stream_snk_rx_monitor_2_data,
  output wire         board_kernel_stream_snk_rx_monitor_2_valid,
  input  wire         board_kernel_stream_snk_rx_monitor_2_ready,
  output wire [167:0] board_kernel_stream_snk_tx_monitor_2_data,
  output wire         board_kernel_stream_snk_tx_monitor_2_valid,
  input  wire         board_kernel_stream_snk_tx_monitor_2_ready,
  
  output wire [167:0] board_kernel_stream_snk_rx_monitor_3_data,
  output wire         board_kernel_stream_snk_rx_monitor_3_valid,
  input  wire         board_kernel_stream_snk_rx_monitor_3_ready,
  output wire [167:0] board_kernel_stream_snk_tx_monitor_3_data,
  output wire         board_kernel_stream_snk_tx_monitor_3_valid,
  input  wire         board_kernel_stream_snk_tx_monitor_3_ready,
  
  output wire [167:0] board_kernel_stream_snk_rx_monitor_4_data,
  output wire         board_kernel_stream_snk_rx_monitor_4_valid,
  input  wire         board_kernel_stream_snk_rx_monitor_4_ready,
  output wire [167:0] board_kernel_stream_snk_tx_monitor_4_data,
  output wire         board_kernel_stream_snk_tx_monitor_4_valid,
  input  wire         board_kernel_stream_snk_tx_monitor_4_ready,
  
  output wire [167:0] board_kernel_stream_snk_rx_monitor_5_data,
  output wire         board_kernel_stream_snk_rx_monitor_5_valid,
  input  wire         board_kernel_stream_snk_rx_monitor_5_ready,
  output wire [167:0] board_kernel_stream_snk_tx_monitor_5_data,
  output wire         board_kernel_stream_snk_tx_monitor_5_valid,
  input  wire         board_kernel_stream_snk_tx_monitor_5_ready,
  
  output wire [167:0] board_kernel_stream_snk_rx_monitor_6_data,
  output wire         board_kernel_stream_snk_rx_monitor_6_valid,
  input  wire         board_kernel_stream_snk_rx_monitor_6_ready,
  output wire [167:0] board_kernel_stream_snk_tx_monitor_6_data,
  output wire         board_kernel_stream_snk_tx_monitor_6_valid,
  input  wire         board_kernel_stream_snk_tx_monitor_6_ready,
  
  output wire [167:0] board_kernel_stream_snk_rx_monitor_7_data,
  output wire         board_kernel_stream_snk_rx_monitor_7_valid,
  input  wire         board_kernel_stream_snk_rx_monitor_7_ready,
  output wire [167:0] board_kernel_stream_snk_tx_monitor_7_data,
  output wire         board_kernel_stream_snk_tx_monitor_7_valid,
  input  wire         board_kernel_stream_snk_tx_monitor_7_ready,
  
  input  wire [103:0] board_kernel_stream_src_bs_data,
  input  wire         board_kernel_stream_src_bs_valid,
  output wire         board_kernel_stream_src_bs_ready,

  output [6:0]     board_kernel_register_mem_address,                
  output           board_kernel_register_mem_clken,      
  output           board_kernel_register_mem_chipselect, 
  output           board_kernel_register_mem_write,      
  input  [255:0]   board_kernel_register_mem_readdata,   
  output [255:0]   board_kernel_register_mem_writedata,  
  output [31:0]    board_kernel_register_mem_byteenable 
);
//=======================================================
//  pr_region instantiation
//=======================================================
pr_region pr_region_inst
(
  .clock_reset_clk(board_kernel_clk_clk),
  .clock_reset2x_clk(board_kernel_clk2x_clk),
  .clock_reset_reset_reset_n(board_kernel_reset_reset_n),
//  .clock_reset_reset_reset_n(kernel_system_clock_reset_reset_reset_n),
  .kernel_irq_irq(board_kernel_irq_irq),
//  .kernel_irq_irq(kernel_system_kernel_irq_irq),
//  .cc_snoop_clk_clk(board_kernel_clk_clk),
//  .cc_snoop_data(board_acl_internal_snoop_data),
//  .cc_snoop_valid(board_acl_internal_snoop_valid),
//  .cc_snoop_ready(kernel_system_cc_snoop_ready),
//  .kernel_cra_waitrequest(kernel_system_kernel_cra_waitrequest),
  .kernel_cra_waitrequest(board_kernel_cra_waitrequest),
  .kernel_cra_readdata(board_kernel_cra_readdata),
//  .kernel_cra_readdatavalid(kernel_system_kernel_cra_readdatavalid),
  .kernel_cra_readdatavalid(board_kernel_cra_readdatavalid),
  .kernel_cra_burstcount(board_kernel_cra_burstcount),
  .kernel_cra_writedata(board_kernel_cra_writedata),
  .kernel_cra_address(board_kernel_cra_address),
  .kernel_cra_write(board_kernel_cra_write),
  .kernel_cra_read(board_kernel_cra_read),
  .kernel_cra_byteenable(board_kernel_cra_byteenable),
  .kernel_cra_debugaccess(board_kernel_cra_debugaccess),


  .kernel_stream_src_10GbE_ring_0_data( board_kernel_stream_src_10GbE_ring_0_data),
  .kernel_stream_src_10GbE_ring_0_ready(board_kernel_stream_src_10GbE_ring_0_ready),
  .kernel_stream_src_10GbE_ring_0_valid(board_kernel_stream_src_10GbE_ring_0_valid),
  .kernel_stream_snk_10GbE_ring_0_data( board_kernel_stream_snk_10GbE_ring_0_data),
  .kernel_stream_snk_10GbE_ring_0_ready(board_kernel_stream_snk_10GbE_ring_0_ready),
  .kernel_stream_snk_10GbE_ring_0_valid(board_kernel_stream_snk_10GbE_ring_0_valid),

  .kernel_stream_src_10GbE_ring_1_data( board_kernel_stream_src_10GbE_ring_1_data),
  .kernel_stream_src_10GbE_ring_1_ready(board_kernel_stream_src_10GbE_ring_1_ready),
  .kernel_stream_src_10GbE_ring_1_valid(board_kernel_stream_src_10GbE_ring_1_valid),
  .kernel_stream_snk_10GbE_ring_1_data( board_kernel_stream_snk_10GbE_ring_1_data),
  .kernel_stream_snk_10GbE_ring_1_ready(board_kernel_stream_snk_10GbE_ring_1_ready),
  .kernel_stream_snk_10GbE_ring_1_valid(board_kernel_stream_snk_10GbE_ring_1_valid),

  .kernel_stream_src_10GbE_ring_2_data( board_kernel_stream_src_10GbE_ring_2_data),
  .kernel_stream_src_10GbE_ring_2_ready(board_kernel_stream_src_10GbE_ring_2_ready),
  .kernel_stream_src_10GbE_ring_2_valid(board_kernel_stream_src_10GbE_ring_2_valid),
  .kernel_stream_snk_10GbE_ring_2_data( board_kernel_stream_snk_10GbE_ring_2_data),
  .kernel_stream_snk_10GbE_ring_2_ready(board_kernel_stream_snk_10GbE_ring_2_ready),
  .kernel_stream_snk_10GbE_ring_2_valid(board_kernel_stream_snk_10GbE_ring_2_valid),

  .kernel_stream_src_10GbE_ring_3_data( board_kernel_stream_src_10GbE_ring_3_data),
  .kernel_stream_src_10GbE_ring_3_ready(board_kernel_stream_src_10GbE_ring_3_ready),
  .kernel_stream_src_10GbE_ring_3_valid(board_kernel_stream_src_10GbE_ring_3_valid),
  .kernel_stream_snk_10GbE_ring_3_data( board_kernel_stream_snk_10GbE_ring_3_data),
  .kernel_stream_snk_10GbE_ring_3_ready(board_kernel_stream_snk_10GbE_ring_3_ready),
  .kernel_stream_snk_10GbE_ring_3_valid(board_kernel_stream_snk_10GbE_ring_3_valid),

  .kernel_stream_src_10GbE_ring_4_data( board_kernel_stream_src_10GbE_ring_4_data),
  .kernel_stream_src_10GbE_ring_4_ready(board_kernel_stream_src_10GbE_ring_4_ready),
  .kernel_stream_src_10GbE_ring_4_valid(board_kernel_stream_src_10GbE_ring_4_valid),
  .kernel_stream_snk_10GbE_ring_4_data( board_kernel_stream_snk_10GbE_ring_4_data),
  .kernel_stream_snk_10GbE_ring_4_ready(board_kernel_stream_snk_10GbE_ring_4_ready),
  .kernel_stream_snk_10GbE_ring_4_valid(board_kernel_stream_snk_10GbE_ring_4_valid),

  .kernel_stream_src_10GbE_ring_5_data( board_kernel_stream_src_10GbE_ring_5_data),
  .kernel_stream_src_10GbE_ring_5_ready(board_kernel_stream_src_10GbE_ring_5_ready),
  .kernel_stream_src_10GbE_ring_5_valid(board_kernel_stream_src_10GbE_ring_5_valid),
  .kernel_stream_snk_10GbE_ring_5_data( board_kernel_stream_snk_10GbE_ring_5_data),
  .kernel_stream_snk_10GbE_ring_5_ready(board_kernel_stream_snk_10GbE_ring_5_ready),
  .kernel_stream_snk_10GbE_ring_5_valid(board_kernel_stream_snk_10GbE_ring_5_valid),

  .kernel_stream_src_10GbE_ring_6_data( board_kernel_stream_src_10GbE_ring_6_data),
  .kernel_stream_src_10GbE_ring_6_ready(board_kernel_stream_src_10GbE_ring_6_ready),
  .kernel_stream_src_10GbE_ring_6_valid(board_kernel_stream_src_10GbE_ring_6_valid),
  .kernel_stream_snk_10GbE_ring_6_data( board_kernel_stream_snk_10GbE_ring_6_data),
  .kernel_stream_snk_10GbE_ring_6_ready(board_kernel_stream_snk_10GbE_ring_6_ready),
  .kernel_stream_snk_10GbE_ring_6_valid(board_kernel_stream_snk_10GbE_ring_6_valid),

  .kernel_stream_src_10GbE_ring_7_data( board_kernel_stream_src_10GbE_ring_7_data),
  .kernel_stream_src_10GbE_ring_7_ready(board_kernel_stream_src_10GbE_ring_7_ready),
  .kernel_stream_src_10GbE_ring_7_valid(board_kernel_stream_src_10GbE_ring_7_valid),
  .kernel_stream_snk_10GbE_ring_7_data( board_kernel_stream_snk_10GbE_ring_7_data),
  .kernel_stream_snk_10GbE_ring_7_ready(board_kernel_stream_snk_10GbE_ring_7_ready),
  .kernel_stream_snk_10GbE_ring_7_valid(board_kernel_stream_snk_10GbE_ring_7_valid),

  .kernel_stream_src_10GbE_qsfp_0_data( board_kernel_stream_src_10GbE_qsfp_0_data),
  .kernel_stream_src_10GbE_qsfp_0_ready(board_kernel_stream_src_10GbE_qsfp_0_ready),
  .kernel_stream_src_10GbE_qsfp_0_valid(board_kernel_stream_src_10GbE_qsfp_0_valid),
  .kernel_stream_snk_10GbE_qsfp_0_data( board_kernel_stream_snk_10GbE_qsfp_0_data),
  .kernel_stream_snk_10GbE_qsfp_0_ready(board_kernel_stream_snk_10GbE_qsfp_0_ready),
  .kernel_stream_snk_10GbE_qsfp_0_valid(board_kernel_stream_snk_10GbE_qsfp_0_valid),

  .kernel_stream_src_10GbE_qsfp_1_data( board_kernel_stream_src_10GbE_qsfp_1_data),
  .kernel_stream_src_10GbE_qsfp_1_ready(board_kernel_stream_src_10GbE_qsfp_1_ready),
  .kernel_stream_src_10GbE_qsfp_1_valid(board_kernel_stream_src_10GbE_qsfp_1_valid),
  .kernel_stream_snk_10GbE_qsfp_1_data( board_kernel_stream_snk_10GbE_qsfp_1_data),
  .kernel_stream_snk_10GbE_qsfp_1_ready(board_kernel_stream_snk_10GbE_qsfp_1_ready),
  .kernel_stream_snk_10GbE_qsfp_1_valid(board_kernel_stream_snk_10GbE_qsfp_1_valid),

  .kernel_stream_src_10GbE_qsfp_2_data( board_kernel_stream_src_10GbE_qsfp_2_data),
  .kernel_stream_src_10GbE_qsfp_2_ready(board_kernel_stream_src_10GbE_qsfp_2_ready),
  .kernel_stream_src_10GbE_qsfp_2_valid(board_kernel_stream_src_10GbE_qsfp_2_valid),
  .kernel_stream_snk_10GbE_qsfp_2_data( board_kernel_stream_snk_10GbE_qsfp_2_data),
  .kernel_stream_snk_10GbE_qsfp_2_ready(board_kernel_stream_snk_10GbE_qsfp_2_ready),
  .kernel_stream_snk_10GbE_qsfp_2_valid(board_kernel_stream_snk_10GbE_qsfp_2_valid),

  .kernel_stream_src_10GbE_qsfp_3_data( board_kernel_stream_src_10GbE_qsfp_3_data),
  .kernel_stream_src_10GbE_qsfp_3_ready(board_kernel_stream_src_10GbE_qsfp_3_ready),
  .kernel_stream_src_10GbE_qsfp_3_valid(board_kernel_stream_src_10GbE_qsfp_3_valid),
  .kernel_stream_snk_10GbE_qsfp_3_data( board_kernel_stream_snk_10GbE_qsfp_3_data),
  .kernel_stream_snk_10GbE_qsfp_3_ready(board_kernel_stream_snk_10GbE_qsfp_3_ready),
  .kernel_stream_snk_10GbE_qsfp_3_valid(board_kernel_stream_snk_10GbE_qsfp_3_valid),

  .kernel_stream_src_lane_0_data( board_kernel_stream_src_lane_0_data),
  .kernel_stream_src_lane_0_ready(board_kernel_stream_src_lane_0_ready),
  .kernel_stream_src_lane_0_valid(board_kernel_stream_src_lane_0_valid),
  .kernel_stream_snk_lane_0_data( board_kernel_stream_snk_lane_0_data),
  .kernel_stream_snk_lane_0_ready(board_kernel_stream_snk_lane_0_ready),
  .kernel_stream_snk_lane_0_valid(board_kernel_stream_snk_lane_0_valid),

  .kernel_stream_src_lane_1_data( board_kernel_stream_src_lane_1_data),
  .kernel_stream_src_lane_1_ready(board_kernel_stream_src_lane_1_ready),
  .kernel_stream_src_lane_1_valid(board_kernel_stream_src_lane_1_valid),
  .kernel_stream_snk_lane_1_data( board_kernel_stream_snk_lane_1_data),
  .kernel_stream_snk_lane_1_ready(board_kernel_stream_snk_lane_1_ready),
  .kernel_stream_snk_lane_1_valid(board_kernel_stream_snk_lane_1_valid),

  .kernel_stream_src_lane_2_data( board_kernel_stream_src_lane_2_data),
  .kernel_stream_src_lane_2_ready(board_kernel_stream_src_lane_2_ready),
  .kernel_stream_src_lane_2_valid(board_kernel_stream_src_lane_2_valid),
  .kernel_stream_snk_lane_2_data( board_kernel_stream_snk_lane_2_data),
  .kernel_stream_snk_lane_2_ready(board_kernel_stream_snk_lane_2_ready),
  .kernel_stream_snk_lane_2_valid(board_kernel_stream_snk_lane_2_valid),

  .kernel_stream_src_lane_3_data( board_kernel_stream_src_lane_3_data),
  .kernel_stream_src_lane_3_ready(board_kernel_stream_src_lane_3_ready),
  .kernel_stream_src_lane_3_valid(board_kernel_stream_src_lane_3_valid),
  .kernel_stream_snk_lane_3_data( board_kernel_stream_snk_lane_3_data),
  .kernel_stream_snk_lane_3_ready(board_kernel_stream_snk_lane_3_ready),
  .kernel_stream_snk_lane_3_valid(board_kernel_stream_snk_lane_3_valid),

  .kernel_stream_src_lane_4_data( board_kernel_stream_src_lane_4_data),
  .kernel_stream_src_lane_4_ready(board_kernel_stream_src_lane_4_ready),
  .kernel_stream_src_lane_4_valid(board_kernel_stream_src_lane_4_valid),
  .kernel_stream_snk_lane_4_data( board_kernel_stream_snk_lane_4_data),
  .kernel_stream_snk_lane_4_ready(board_kernel_stream_snk_lane_4_ready),
  .kernel_stream_snk_lane_4_valid(board_kernel_stream_snk_lane_4_valid),

  .kernel_stream_src_lane_5_data( board_kernel_stream_src_lane_5_data),
  .kernel_stream_src_lane_5_ready(board_kernel_stream_src_lane_5_ready),
  .kernel_stream_src_lane_5_valid(board_kernel_stream_src_lane_5_valid),
  .kernel_stream_snk_lane_5_data( board_kernel_stream_snk_lane_5_data),
  .kernel_stream_snk_lane_5_ready(board_kernel_stream_snk_lane_5_ready),
  .kernel_stream_snk_lane_5_valid(board_kernel_stream_snk_lane_5_valid),

  .kernel_stream_src_lane_6_data( board_kernel_stream_src_lane_6_data),
  .kernel_stream_src_lane_6_ready(board_kernel_stream_src_lane_6_ready),
  .kernel_stream_src_lane_6_valid(board_kernel_stream_src_lane_6_valid),
  .kernel_stream_snk_lane_6_data( board_kernel_stream_snk_lane_6_data),
  .kernel_stream_snk_lane_6_ready(board_kernel_stream_snk_lane_6_ready),
  .kernel_stream_snk_lane_6_valid(board_kernel_stream_snk_lane_6_valid),

  .kernel_stream_src_lane_7_data( board_kernel_stream_src_lane_7_data),
  .kernel_stream_src_lane_7_ready(board_kernel_stream_src_lane_7_ready),
  .kernel_stream_src_lane_7_valid(board_kernel_stream_src_lane_7_valid),
  .kernel_stream_snk_lane_7_data( board_kernel_stream_snk_lane_7_data),
  .kernel_stream_snk_lane_7_ready(board_kernel_stream_snk_lane_7_ready),
  .kernel_stream_snk_lane_7_valid(board_kernel_stream_snk_lane_7_valid),

  .kernel_stream_snk_rx_monitor_0_data( board_kernel_stream_snk_rx_monitor_0_data),
  .kernel_stream_snk_rx_monitor_0_ready(board_kernel_stream_snk_rx_monitor_0_ready),
  .kernel_stream_snk_rx_monitor_0_valid(board_kernel_stream_snk_rx_monitor_0_valid),
  .kernel_stream_snk_tx_monitor_0_data( board_kernel_stream_snk_tx_monitor_0_data),
  .kernel_stream_snk_tx_monitor_0_ready(board_kernel_stream_snk_tx_monitor_0_ready),
  .kernel_stream_snk_tx_monitor_0_valid(board_kernel_stream_snk_tx_monitor_0_valid),

  .kernel_stream_snk_rx_monitor_1_data( board_kernel_stream_snk_rx_monitor_1_data),
  .kernel_stream_snk_rx_monitor_1_ready(board_kernel_stream_snk_rx_monitor_1_ready),
  .kernel_stream_snk_rx_monitor_1_valid(board_kernel_stream_snk_rx_monitor_1_valid),
  .kernel_stream_snk_tx_monitor_1_data( board_kernel_stream_snk_tx_monitor_1_data),
  .kernel_stream_snk_tx_monitor_1_ready(board_kernel_stream_snk_tx_monitor_1_ready),
  .kernel_stream_snk_tx_monitor_1_valid(board_kernel_stream_snk_tx_monitor_1_valid),

  .kernel_stream_snk_rx_monitor_2_data( board_kernel_stream_snk_rx_monitor_2_data),
  .kernel_stream_snk_rx_monitor_2_ready(board_kernel_stream_snk_rx_monitor_2_ready),
  .kernel_stream_snk_rx_monitor_2_valid(board_kernel_stream_snk_rx_monitor_2_valid),
  .kernel_stream_snk_tx_monitor_2_data( board_kernel_stream_snk_tx_monitor_2_data),
  .kernel_stream_snk_tx_monitor_2_ready(board_kernel_stream_snk_tx_monitor_2_ready),
  .kernel_stream_snk_tx_monitor_2_valid(board_kernel_stream_snk_tx_monitor_2_valid),

  .kernel_stream_snk_rx_monitor_3_data( board_kernel_stream_snk_rx_monitor_3_data),
  .kernel_stream_snk_rx_monitor_3_ready(board_kernel_stream_snk_rx_monitor_3_ready),
  .kernel_stream_snk_rx_monitor_3_valid(board_kernel_stream_snk_rx_monitor_3_valid),
  .kernel_stream_snk_tx_monitor_3_data( board_kernel_stream_snk_tx_monitor_3_data),
  .kernel_stream_snk_tx_monitor_3_ready(board_kernel_stream_snk_tx_monitor_3_ready),
  .kernel_stream_snk_tx_monitor_3_valid(board_kernel_stream_snk_tx_monitor_3_valid),

  .kernel_stream_snk_rx_monitor_4_data( board_kernel_stream_snk_rx_monitor_4_data),
  .kernel_stream_snk_rx_monitor_4_ready(board_kernel_stream_snk_rx_monitor_4_ready),
  .kernel_stream_snk_rx_monitor_4_valid(board_kernel_stream_snk_rx_monitor_4_valid),
  .kernel_stream_snk_tx_monitor_4_data( board_kernel_stream_snk_tx_monitor_4_data),
  .kernel_stream_snk_tx_monitor_4_ready(board_kernel_stream_snk_tx_monitor_4_ready),
  .kernel_stream_snk_tx_monitor_4_valid(board_kernel_stream_snk_tx_monitor_4_valid),

  .kernel_stream_snk_rx_monitor_5_data( board_kernel_stream_snk_rx_monitor_5_data),
  .kernel_stream_snk_rx_monitor_5_ready(board_kernel_stream_snk_rx_monitor_5_ready),
  .kernel_stream_snk_rx_monitor_5_valid(board_kernel_stream_snk_rx_monitor_5_valid),
  .kernel_stream_snk_tx_monitor_5_data( board_kernel_stream_snk_tx_monitor_5_data),
  .kernel_stream_snk_tx_monitor_5_ready(board_kernel_stream_snk_tx_monitor_5_ready),
  .kernel_stream_snk_tx_monitor_5_valid(board_kernel_stream_snk_tx_monitor_5_valid),

  .kernel_stream_snk_rx_monitor_6_data( board_kernel_stream_snk_rx_monitor_6_data),
  .kernel_stream_snk_rx_monitor_6_ready(board_kernel_stream_snk_rx_monitor_6_ready),
  .kernel_stream_snk_rx_monitor_6_valid(board_kernel_stream_snk_rx_monitor_6_valid),
  .kernel_stream_snk_tx_monitor_6_data( board_kernel_stream_snk_tx_monitor_6_data),
  .kernel_stream_snk_tx_monitor_6_ready(board_kernel_stream_snk_tx_monitor_6_ready),
  .kernel_stream_snk_tx_monitor_6_valid(board_kernel_stream_snk_tx_monitor_6_valid),

  .kernel_stream_snk_rx_monitor_7_data( board_kernel_stream_snk_rx_monitor_7_data),
  .kernel_stream_snk_rx_monitor_7_ready(board_kernel_stream_snk_rx_monitor_7_ready),
  .kernel_stream_snk_rx_monitor_7_valid(board_kernel_stream_snk_rx_monitor_7_valid),
  .kernel_stream_snk_tx_monitor_7_data( board_kernel_stream_snk_tx_monitor_7_data),
  .kernel_stream_snk_tx_monitor_7_ready(board_kernel_stream_snk_tx_monitor_7_ready),
  .kernel_stream_snk_tx_monitor_7_valid(board_kernel_stream_snk_tx_monitor_7_valid),



  .kernel_stream_src_bs_data( board_kernel_stream_src_bs_data),
  .kernel_stream_src_bs_ready(board_kernel_stream_src_bs_ready),
  .kernel_stream_src_bs_valid(board_kernel_stream_src_bs_valid),

  .kernel_stream_snk_mm_io_data(board_kernel_stream_snk_mm_io_data),
  .kernel_stream_snk_mm_io_ready(board_kernel_stream_snk_mm_io_ready),
  .kernel_stream_snk_mm_io_valid(board_kernel_stream_snk_mm_io_valid),
  .kernel_stream_src_mm_io_data(board_kernel_stream_src_mm_io_data),
  .kernel_stream_src_mm_io_ready(board_kernel_stream_src_mm_io_ready),
  .kernel_stream_src_mm_io_valid(board_kernel_stream_src_mm_io_valid),

  .kernel_register_mem_address(board_kernel_register_mem_address),  
  .kernel_register_mem_clken(board_kernel_register_mem_clken),  
  .kernel_register_mem_chipselect(board_kernel_register_mem_chipselect),  
  .kernel_register_mem_write(board_kernel_register_mem_write),  
  .kernel_register_mem_readdata(board_kernel_register_mem_readdata),  
  .kernel_register_mem_writedata(board_kernel_register_mem_writedata),  
  .kernel_register_mem_byteenable(board_kernel_register_mem_byteenable)  


);

endmodule
