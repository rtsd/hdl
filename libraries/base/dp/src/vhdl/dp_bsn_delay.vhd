-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose : Delay the input sync and BSN so they remain aligned to the data
--           path processing.
-- Description:
--   The out_release of the data path processing forms the release trigger for
--   the sync and the BSN. The delay is static (fixed) regarding the number of
--   blocks and can be 0 or more blocks and is set by g_nof_block_latency. The
--   delay is dynamic regarding the back pressure ready signal and can handle
--   any data processing logic latency >= 1 clock cycles.
-- Issues:
-- . The in_sync can occur during data invalid, so when in_eop = '0'. The
--   BSN timing must be robust for this.
-- . Connecting out_release to an early out_sop can cause that the out_sync
--   occurs during data invalid.
-- . Connecting out_release to out_eop ensures that the out_sync occurs during
--   valid but can cause the output sync and BSN to be 1 block early in case
--   the out_eop occurs before the in_eop (e.g. if the DP block outputs smaller
--   blocks than that it gets at its input).
-- ==> The dp_latency_adapter can now pass on a sync during data not valid. So
--     it is now possible to connect out_release to an early out_sop.
-- Remarks:
-- . The sync and BSN can not be delayed by a fixed latency, because the data
--   path processing may use dynamic back pressure using the ready signal.
--   Therefore this dp_bsn_delay uses out_release to release the sync and BSN.
-- . About dynamic block delay support.
--   - A data path function that can store and forward a dynamic number of
--     blocks can not use this dp_bsn_delay, because g_nof_block_latency is a
--     generic. Perhaps the block latency can be made dynamic by monitoring the
--     number of out_release that occured relative to the number of in_eop. The
--     g_nof_block_latency then sets the maximum that is supported.
--   - Alternatively, and probably better, the sync and BSN can be put through
--     a separate FIFO that is written by the in_eop and read by the
--     out_release. The FIFO can then have RL=0 so that the read sync becomes
--     out_sync directly and the read bsn then becomes out_bsn after a register
--     stage.
--     Putting the sync and BSN through the same FIFO as the data and sop, eop
--     consumes quite some RAM since the BSN is typically 48 bit wide and it
--     then needs to be pass through through the FIFO for every valid clock
--     cycle.

entity dp_bsn_delay is
  generic (
    g_nof_block_latency  : natural := 0;  -- >= 0
    g_bsn_w              : natural := 48  -- use g_bsn_w to allow leaving in_bsn NC
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    in_sync     : in  std_logic;
    in_bsn      : in  std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
    in_eop      : in  std_logic;
    out_release : in  std_logic;  -- input from the data path processing, used as output release pulse
    out_sync    : out std_logic;
    out_bsn     : out std_logic_vector(g_bsn_w - 1 downto 0)
  );
end dp_bsn_delay;

architecture rtl of dp_bsn_delay is
  type t_bsn_arr  is array (integer range <>) of std_logic_vector(g_bsn_w - 1 downto 0);

  signal hold_sync        : std_logic;
  signal sync_dly         : std_logic_vector(0 to g_nof_block_latency);  -- [0] is hold_sync, the combinatorial in_sync is not delayed, because the DP latency must be > 0
  signal nxt_sync_dly     : std_logic_vector(1 to g_nof_block_latency);

  signal bsn_dly          : t_bsn_arr(0 to g_nof_block_latency);  -- [0] is combinatorial in_bsn
  signal nxt_bsn_dly      : t_bsn_arr(1 to g_nof_block_latency);

  signal bsn_reg          : std_logic_vector(g_bsn_w - 1 downto 0);
  signal nxt_bsn_reg      : std_logic_vector(g_bsn_w - 1 downto 0);
begin
  p_clk : process(clk, rst)
  begin
    if rst = '1' then
      sync_dly(1 to g_nof_block_latency)  <= (others => '0');
      bsn_dly( 1 to g_nof_block_latency)  <= (others => (others => '0'));
      bsn_reg                             <= (others => '0');
    elsif rising_edge(clk) then
      sync_dly(1 to g_nof_block_latency)  <= nxt_sync_dly;
      bsn_dly( 1 to g_nof_block_latency)  <= nxt_bsn_dly;
      bsn_reg                             <= nxt_bsn_reg;
    end if;
  end process;

  -- Hold the in_sync during the block until the in_eop
  u_hold_sync : entity common_lib.common_switch
  generic map (
    g_priority_lo  => false,  -- in_sync has priority over in_eop, because they may occur simultaneously
    g_or_high      => false,  -- hold_sync goes high after in_sync
    g_and_low      => false  -- hold_sync goes low  after in_eop
  )
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => in_sync,
    switch_low  => in_eop,
    out_level   => hold_sync
  );

  -- Delay line for in_sync and in_bsn at block level
  sync_dly(0)  <= hold_sync;
  nxt_sync_dly <= sync_dly(0 to g_nof_block_latency - 1) when in_eop = '1' else sync_dly(1 to g_nof_block_latency);

  bsn_dly(0)   <= in_bsn;
  nxt_bsn_dly  <= bsn_dly( 0 to g_nof_block_latency - 1) when in_eop = '1' else bsn_dly( 1 to g_nof_block_latency);

  -- Output block alignment using the out_release
  out_sync    <= sync_dly(g_nof_block_latency) when out_release = '1' else '0';  -- combinatorial pulse
  nxt_bsn_reg <= bsn_dly(g_nof_block_latency)  when out_release = '1' else bsn_reg;  -- register the BSN to hold it during the output block
  out_bsn     <= bsn_reg;
end rtl;
