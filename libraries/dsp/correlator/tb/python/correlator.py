###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Purpose:
# . Automatic loading, running and verification of correlator test bench
# Description:
# . 
# Usage:
# . $ python correlator.py

import sys, os
sys.path.append(os.environ['UNB']+'/Firmware/sim/python')
from auto_sim import *
#import tc_correlator

INIT_DELAY_NS = None
LIBRARY_NAME = 'correlator'
TB_NAME      = 'tb_correlator'
GENERICS     = {'g_nof_inputs'         : 8,
                'g_nof_channels'       : 8,
                'g_nof_pre_mult_folds' : 1,
                'g_nof_input_folds'    : 1}
TARGET_NODES = ' --unb 0 --fn 0'        
COMMANDS     = ['tc_correlator.py' + TARGET_NODES + ' --gen ' +  '"' + str(GENERICS) + '"' ]  

# Generate .hex files before ModelSim starts
#tc_correlator.gen_filter_hex_files(GENERICS['g_nof_taps'], GENERICS['g_wb_factor'], GENERICS['g_nof_points'])
        
# Run the sim and return its result using sys.exit([return_value])                             
#sys.exit(auto_sim(os.environ['UNB'], LIBRARY_NAME, TB_NAME, COMMANDS, INIT_DELAY_NS, GENERICS))
auto_sim(os.environ['UNB'], LIBRARY_NAME, TB_NAME, COMMANDS, INIT_DELAY_NS, GENERICS)
