# (C) 1992-2018 Intel Corporation.                            
# Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
# and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
# and/or other countries. Other marks and brands may be claimed as the property  
# of others. See Trademarks on intel.com for full list of Intel trademarks or    
# the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
# Your use of Intel Corporation's design tools, logic functions and other        
# software and tools, and its AMPP partner logic functions, and any output       
# files any of the foregoing (including device programming or simulation         
# files), and any associated documentation or information are expressly subject  
# to the terms and conditions of the Altera Program License Subscription         
# Agreement, Intel MegaCore Function License Agreement, or other applicable      
# license agreement, including, without limitation, that your use is for the     
# sole purpose of programming logic devices manufactured by Intel and sold by    
# Intel or its authorized distributors.  Please refer to the applicable          
# agreement for further details.                                                 
# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Reinier vd Walle
# Purpose:
# . Quartus settings for OpenCL BSP
# Description:
# . " "
# ##########################################################################

# device.tcl contains settings unique to each device type/board variant (most importantly, the device string for the particular device type)
source device.tcl
#============================================================
# Files and basic settings
#============================================================

set_global_assignment -name SDC_FILE top.sdc

# opencl_bsp_ip.qsf contains all necessary Verilog and IP files including 
# top.v, ip/freeze_wrapper.v and kernel_mem IP that are used for all revision compiles.
# Flat and base revision compiles generate board.qsys and append the resulting .ip files 
# to opencl_bsp_ip.qsf, while top revision compiles imports a post-fit netlist of board.qsys 
# from the base revision compile and does not require the sources.
source opencl_bsp_ip.qsf
set_global_assignment -name TOP_LEVEL_ENTITY top

# Post IP SDC constraints
set_global_assignment -name SDC_FILE top_post.sdc

# Execute the pre/post CAD flow
set_global_assignment -name POST_FLOW_SCRIPT_FILE "quartus_cdb:scripts/post_flow_pr.tcl"

# Enable QHD 
set_global_assignment -name QHD_MODE ON

#============================================================
# Revision Specific Settings
#============================================================
set_global_assignment -name AUTO_GLOBAL_CLOCK OFF
set_global_assignment -name AUTO_GLOBAL_REGISTER_CONTROLS OFF

# Clocks
#set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to board_inst|config_clk~pad
#set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to board_inst|clk_clk~pad
#set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to board_inst|pll_ref_clk~pad
set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to board_inst|kernel_clk_gen|board_kernel_clk_gen|kernel_pll|altera_iopll_i|twentynm_pll|*
set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to *ALTERA_INSERTED_OSCILLATOR_FOR_IOPLL*
#set_instance_assignment -name GLOBAL_SIGNAL          OFF -to board_inst|mem|ddr4|ddr4a|arch|arch_inst|oct_inst|cal_oct.manual_oct_cal.r_clkdiv



#set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to u_ctrl_unb2b_board|\gen_eth:u_eth|u_tech_tse|\gen_ip_arria10_e1sg:u0|\u_LVDS_tse:u_tse|eth_tse_0|i_lvdsio_rx_0|core|arch_inst|internal_pll.pll_inst|outclock[2] 
set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to u_ctrl_unb2b_board|\gen_mm_clk_hardware:u_unb2_board_clk125_pll|\gen_fractional_pll:u_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|* 
set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to u_ctrl_unb2b_board|\gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|* 


# Resets
#set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to freeze_wrapper_inst|kernel_system_clock_reset_reset_reset_n
set_instance_assignment -name GLOBAL_SIGNAL GLOBAL_CLOCK -to freeze_wrapper_inst|board_kernel_reset_reset_n

# This setting indicates that the global signal will be frozen high during PR by user logic (implemented in the freeze_wrapper)
#set_instance_assignment -name PR_ALLOW_GLOBAL_LIMS ON -to freeze_wrapper_inst|kernel_system_clock_reset_reset_reset_n
#set_instance_assignment -name PR_ALLOW_GLOBAL_LIMS ON -to freeze_wrapper_inst|board_kernel_reset_reset_n


set_instance_assignment -name GLOBAL_SIGNAL OFF -to "*|reset_controller_sw|alt_rst_sync_uq1|altera_reset_synchronizer_int_chain_out"

set_instance_assignment -name GLOBAL_SIGNAL OFF -to "*sdi_ii_ed_loopback*|fifo_reset_sync_out"

#============================================================
# Synthesis and Fitter Fine-Tuning
#============================================================
set_global_assignment -name FITTER_EFFORT "STANDARD FIT"
set_global_assignment -name FIT_ONLY_ONE_ATTEMPT ON
set_global_assignment -name PLACEMENT_EFFORT_MULTIPLIER 4.0
set_global_assignment -name ROUTER_TIMING_OPTIMIZATION_LEVEL MAXIMUM
set_global_assignment -name OPTIMIZE_HOLD_TIMING "ALL PATHS"
set_global_assignment -name OPTIMIZATION_TECHNIQUE SPEED
set_global_assignment -name ALLOW_SHIFT_REGISTER_MERGING_ACROSS_HIERARCHIES OFF
set_global_assignment -name DISABLE_REGISTER_MERGING_ACROSS_HIERARCHIES ON
set_global_assignment -name LAST_QUARTUS_VERSION "19.2.0 Pro Edition"
#set_instance_assignment -name AUTO_SHIFT_REGISTER_RECOGNITION OFF -to board_inst|*pipe_stage_*
set_global_assignment -name ENABLE_PR_PINS OFF
set_global_assignment -name ENABLE_INIT_DONE_OUTPUT OFF

#============================================================
# End of original settings
#============================================================


#============================================================
# Board settings
#============================================================

#############################################################
# Misc
#############################################################
# Programming file generation
set_global_assignment -name ON_CHIP_BITSTREAM_DECOMPRESSION OFF

# Power model
set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"

# I/O Configuration
set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_100MHZ

#############################################################
# Pinouts
#############################################################
set_location_assignment PIN_K15 -to CLK
set_location_assignment PIN_J15 -to "CLK(n)"
set_location_assignment PIN_N12 -to ETH_CLK
set_location_assignment PIN_K14 -to PPS
set_location_assignment PIN_J14 -to "PPS(n)"

# enable 100 ohm termination:
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to CLK
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to PPS

#set_location_assignment PIN_AT33 -to CFG_DATA[0]
#set_location_assignment PIN_AT32 -to CFG_DATA[1]
#set_location_assignment PIN_BB33 -to CFG_DATA[2]
#set_location_assignment PIN_BA33 -to CFG_DATA[3]




# IO Standard Assignments from Gijs (excluding memory)
set_instance_assignment -name IO_STANDARD "1.8 V" -to ETH_CLK
set_instance_assignment -name GLOBAL_SIGNAL OFF -to ETH_CLK
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGIN[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGIN[1](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[0]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[0](n)"
set_instance_assignment -name IO_STANDARD LVDS -to ETH_SGOUT[1]
set_instance_assignment -name IO_STANDARD LVDS -to "ETH_SGOUT[1](n)"
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[6]
set_instance_assignment -name IO_STANDARD "1.8 V" -to ID[7]
set_instance_assignment -name IO_STANDARD "1.8 V" -to INTA
set_instance_assignment -name IO_STANDARD "1.8 V" -to INTB
set_instance_assignment -name IO_STANDARD "1.8 V" -to SENS_SC
set_instance_assignment -name IO_STANDARD "1.8 V" -to SENS_SD
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to TESTIO[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to VERSION[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to WDI

# locations changed 30 sept
set_location_assignment PIN_P16 -to ID[0]
set_location_assignment PIN_P15 -to ID[1]
set_location_assignment PIN_K13 -to ID[2]
set_location_assignment PIN_L13 -to ID[3]
set_location_assignment PIN_N16 -to ID[4]
set_location_assignment PIN_N14 -to ID[5]
set_location_assignment PIN_U13 -to ID[6]

set_location_assignment PIN_T13 -to ID[7]
set_location_assignment PIN_AU31 -to INTA
set_location_assignment PIN_AR30 -to INTB

set_location_assignment PIN_BC31 -to SENS_SC
set_location_assignment PIN_BB31 -to SENS_SD

set_location_assignment PIN_BA25 -to PMBUS_SC
set_location_assignment PIN_BD25 -to PMBUS_SD
set_location_assignment PIN_BD26 -to PMBUS_ALERT
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_SC
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_SD
set_instance_assignment -name IO_STANDARD "1.2 V" -to PMBUS_ALERT


set_location_assignment PIN_AN32 -to TESTIO[0]
set_location_assignment PIN_AP32 -to TESTIO[1]
set_location_assignment PIN_AT30 -to TESTIO[2]
set_location_assignment PIN_BD31 -to TESTIO[3]
set_location_assignment PIN_AU30 -to TESTIO[4]
set_location_assignment PIN_BD30 -to TESTIO[5]

set_location_assignment PIN_AB12 -to VERSION[0]
set_location_assignment PIN_AB13 -to VERSION[1]
set_location_assignment PIN_BB30 -to WDI

set_location_assignment PIN_K12 -to ETH_SGIN[0]
set_location_assignment PIN_J12 -to "ETH_SGIN[0](n)"
set_location_assignment PIN_AF33 -to ETH_SGIN[1]
set_location_assignment PIN_AE33 -to "ETH_SGIN[1](n)"
set_location_assignment PIN_H13 -to ETH_SGOUT[0]
set_location_assignment PIN_H12 -to "ETH_SGOUT[0](n)"
set_location_assignment PIN_AW31 -to ETH_SGOUT[1]
set_location_assignment PIN_AV31 -to "ETH_SGOUT[1](n)"

set_instance_assignment -name IO_STANDARD LVDS -to PPS
set_instance_assignment -name IO_STANDARD LVDS -to "PPS(n)"
set_instance_assignment -name IO_STANDARD LVDS -to CLK
set_instance_assignment -name IO_STANDARD LVDS -to "CLK(n)"

# Enable internal termination for LVDS inputs
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to PPS
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to CLK
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[0]
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to ETH_SGIN[1]

# Enable 100 ohm termination resistors
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to "ETH_SGIN[0](n)"
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to ETH_SGIN[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to ETH_SGIN[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to "ETH_SGIN[1](n)"

set_location_assignment PIN_AG31 -to altera_reserved_tms
set_location_assignment PIN_AJ31 -to altera_reserved_tck
set_location_assignment PIN_AK18 -to altera_reserved_tdi
set_location_assignment PIN_AH31 -to altera_reserved_ntrst
set_location_assignment PIN_AM29 -to altera_reserved_tdo
#set_location_assignment PIN_AV33 -to ~ALTERA_DATA0~


set_location_assignment PIN_BA33 -to QSFP_LED[0]
set_location_assignment PIN_BA30 -to QSFP_LED[1]
set_location_assignment PIN_BB33 -to QSFP_LED[2]
set_location_assignment PIN_AU33 -to QSFP_LED[3]
set_location_assignment PIN_AV32 -to QSFP_LED[4]
set_location_assignment PIN_AW30 -to QSFP_LED[5]
set_location_assignment PIN_AP31 -to QSFP_LED[6]
set_location_assignment PIN_AP30 -to QSFP_LED[7]
set_location_assignment PIN_AT33 -to QSFP_LED[8]
set_location_assignment PIN_AG32 -to QSFP_LED[9]
set_location_assignment PIN_AF32 -to QSFP_LED[10]
set_location_assignment PIN_AE32 -to QSFP_LED[11]



set_location_assignment PIN_Y36 -to SA_CLK
set_instance_assignment -name IO_STANDARD LVDS -to SA_CLK
# internal termination should be enabled.
set_instance_assignment -name XCVR_A10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to SA_CLK

set_location_assignment PIN_V9 -to BCK_REF_CLK
set_location_assignment PIN_V10 -to "BCK_REF_CLK(n)"
set_instance_assignment -name IO_STANDARD LVDS -to BCK_REF_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "BCK_REF_CLK(n)"

set_global_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON
### COMMENT OUT UNUSED LANES
### LANE 0, 1
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to  QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                QSFP_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to                QSFP_0_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[0]


set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[0]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[0]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[0]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[0]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[0]


### LANE 2, 3
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to  QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                QSFP_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to                QSFP_0_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[1]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[1]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[1]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[1]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[1]


### LANE 4, 5
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to  QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                QSFP_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to                QSFP_0_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[2]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[2]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[2]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[2]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[2]


### LANE 6,7
set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_8 -to  QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                QSFP_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_5 -to                QSFP_0_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  QSFP_0_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     QSFP_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to QSFP_0_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_0_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_0_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_1_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_1_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_1_TX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to                             RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to          RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to              RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_12 -to RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to                RING_1_RX[3]
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_3 -to                RING_1_RX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_1_RX[3]

set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to                   RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 30 -to                  RING_0_TX[3]
set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to                           RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to                    RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to     RING_0_TX[3]
set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to RING_0_TX[3]




## QSFP_0_RX
set_location_assignment PIN_AN38 -to QSFP_0_RX[0]
set_location_assignment PIN_AM40 -to QSFP_0_RX[1]
set_location_assignment PIN_AK40 -to QSFP_0_RX[2]
set_location_assignment PIN_AJ38 -to QSFP_0_RX[3]
#
## QSFP_0_TX
set_location_assignment PIN_AN42 -to QSFP_0_TX[0]
set_location_assignment PIN_AM44 -to QSFP_0_TX[1]
set_location_assignment PIN_AK44 -to QSFP_0_TX[2]
set_location_assignment PIN_AJ42 -to QSFP_0_TX[3]


## RING
set_location_assignment PIN_AP40 -to RING_0_RX[0]
set_location_assignment PIN_AR38 -to RING_0_RX[1]
set_location_assignment PIN_AT40 -to RING_0_RX[2]
set_location_assignment PIN_AU38 -to RING_0_RX[3]
set_location_assignment PIN_AP44 -to RING_0_TX[0]
set_location_assignment PIN_AR42 -to RING_0_TX[1]
set_location_assignment PIN_AT44 -to RING_0_TX[2]
set_location_assignment PIN_AU42 -to RING_0_TX[3]
set_location_assignment PIN_H40 -to RING_1_RX[0]
set_location_assignment PIN_J38 -to RING_1_RX[1]
set_location_assignment PIN_F40 -to RING_1_RX[2]
set_location_assignment PIN_G38 -to RING_1_RX[3]
set_location_assignment PIN_H44 -to RING_1_TX[0]
set_location_assignment PIN_J42 -to RING_1_TX[1]
set_location_assignment PIN_G42 -to RING_1_TX[2]
set_location_assignment PIN_F44 -to RING_1_TX[3]

