-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_latency_fifo is
end tb_tb_dp_latency_fifo;

architecture tb of tb_tb_dp_latency_fifo is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 2
  -- > run -all --> OK

  -- g_bypass                : BOOLEAN := FALSE;  -- for clarity rather use g_bypass=TRUE than equivalent g_fifo_size=0
  -- g_input_rl              : NATURAL := 1;      -- input ready latency
  -- g_output_rl             : NATURAL := 1;      -- output ready latency
  -- g_fifo_size             : NATURAL := 1;
  -- g_nof_symbols_per_block : NATURAL := 51;
  -- g_nof_symbols_per_data  : NATURAL := 4;
  -- g_in_en                 : t_dp_flow_control_enum := e_active, e_random or e_pulse flow control
  -- g_out_ready             : t_dp_flow_control_enum := e_active, e_random or e_pulse flow control

  u_fifo_bypass             : entity work.tb_dp_latency_fifo generic map (true,  1, 1,   1,   51, 4, e_active, e_active);
  u_fifo_0                  : entity work.tb_dp_latency_fifo generic map (false, 1, 1,   0,   51, 4, e_active, e_active);  -- try bypass via g_fifo_size=0

  u_fifo_1_act_act          : entity work.tb_dp_latency_fifo generic map (false, 1, 1,   1,   51, 4, e_active, e_active);
  u_fifo_1_rnd_act          : entity work.tb_dp_latency_fifo generic map (false, 1, 1,   1,   51, 4, e_random, e_active);
  u_fifo_1_rnd_rnd          : entity work.tb_dp_latency_fifo generic map (false, 1, 1,   1,   51, 4, e_random, e_random);
  u_fifo_1_pls_rnd          : entity work.tb_dp_latency_fifo generic map (false, 1, 1,   1,   51, 4, e_pulse,  e_random);

  u_fifo_1_act_act_rl_0_0   : entity work.tb_dp_latency_fifo generic map (false, 0, 0,   1,   51, 4, e_active, e_active);
  u_fifo_1_act_act_rl_0_1   : entity work.tb_dp_latency_fifo generic map (false, 0, 1,   2,   51, 4, e_active, e_active);
  u_fifo_1_act_act_rl_1_0   : entity work.tb_dp_latency_fifo generic map (false, 1, 0,   1,   51, 4, e_active, e_active);
  u_fifo_1_rnd_rnd_rl_0_0   : entity work.tb_dp_latency_fifo generic map (false, 0, 0,   1,   51, 4, e_random, e_random);
  u_fifo_1_rnd_rnd_rl_0_1   : entity work.tb_dp_latency_fifo generic map (false, 0, 1,   2,   51, 4, e_random, e_random);
  u_fifo_1_rnd_rnd_rl_1_0   : entity work.tb_dp_latency_fifo generic map (false, 1, 0,   1,   51, 4, e_random, e_random);

  u_fifo_2_rnd_rnd          : entity work.tb_dp_latency_fifo generic map (false, 1, 1,   2,   51, 4, e_random, e_random);
  u_fifo_2_pls_rnd          : entity work.tb_dp_latency_fifo generic map (false, 1, 1,   2,   51, 4, e_pulse,  e_random);
  u_fifo_3_pls_rnd          : entity work.tb_dp_latency_fifo generic map (false, 1, 1,   3,   51, 4, e_pulse,  e_random);
end tb;
