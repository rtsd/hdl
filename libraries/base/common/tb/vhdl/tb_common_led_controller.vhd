-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_led_controller
-- Description:
--   When ctrl_on='0' then toggle_ms drives the LED
--   When ctrl+on='1' then the LED is on and it pulses off when a ctrl_evt occurs
--                    too fast ctrl_evt get lost in a single pulse off.
--   The tb is self-stopping, but not self-checking. The verification needs to be
--   done manually in the wave window.
-- Usage:
--   > as 3
--   > run -a

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity tb_common_led_controller is
end tb_common_led_controller;

architecture tb of tb_common_led_controller is
  constant c_pulse_us      : natural := 10;
  constant c_1000          : natural := 10;  -- use eg 10 instead of 1000 to speed up simulation
  constant c_led_nof_ms    : natural := 3;
  constant c_on_nof_ms     : natural := 50;
  constant c_off_nof_ms    : natural := 100;
  constant c_nof_repeat    : natural := 2;

  constant clk_period   : time := 1000 ns / c_pulse_us;

  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '0';
  signal pulse_ms       : std_logic;
  signal toggle_ms      : std_logic;

  signal ctrl_on        : std_logic;
  signal ctrl_evt       : std_logic;
  signal dbg_evt        : natural;

  signal LED            : std_logic;
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  p_stimuli : process
    variable v_evt : positive;
  begin
    for R in 0 to c_nof_repeat - 1 loop
      -- ctrl_on='0' => ctrl_input = toggle_ms drives the LED
      ctrl_on  <= '0';
      ctrl_evt <= '0';
      for I in 0 to c_on_nof_ms - 1 loop
        wait until pulse_ms = '1';
      end loop;

      -- ctrl_on='1' => LED on and ctrl_evt pulses the LED off
      ctrl_on  <= '1';
      ctrl_evt <= '0';
      for I in 0 to c_on_nof_ms - 1 loop
        wait until pulse_ms = '1';
      end loop;

      -- ctrl_on='1' => LED on and ctrl_evt pulses the LED off
      v_evt := 1;
      for I in 0 to c_off_nof_ms - 1 loop
        if I = v_evt then
          ctrl_evt <= '1';
          wait until rising_edge(clk);
          ctrl_evt <= '0';
          v_evt := v_evt + I;
        end if;
        dbg_evt <= v_evt;
        wait until pulse_ms = '1';
      end loop;
    end loop;

    tb_end <= '1';
    wait for 1 us;
    wait;
  end process;

  u_common_pulser_us_ms_s : entity work.common_pulser_us_ms_s
  generic map (
    g_pulse_us   => c_pulse_us,  -- nof clk cycles to get us period
    g_pulse_ms   => c_1000,  -- nof pulse_us pulses to get ms period
    g_pulse_s    => c_1000  -- nof pulse_ms pulses to get s period
  )
  port map (
    rst          => rst,
    clk          => clk,
    pulse_ms     => pulse_ms  -- pulses after every g_pulse_us*g_pulse_ms clock cycles
  );

  u_common_toggle_ms : entity work.common_toggle
  port map (
    rst         => rst,
    clk         => clk,
    in_dat      => pulse_ms,
    out_dat     => toggle_ms
  );

  u_common_led_controller : entity work.common_led_controller
  generic map (
    g_nof_ms      => c_led_nof_ms
  )
  port map (
    rst               => rst,
    clk               => clk,
    pulse_ms          => pulse_ms,
    -- led control
    ctrl_on           => ctrl_on,
    ctrl_evt          => ctrl_evt,
    ctrl_input        => toggle_ms,
    -- led output
    led               => LED
  );
end tb;
