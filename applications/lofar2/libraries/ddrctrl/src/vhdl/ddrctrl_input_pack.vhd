-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: Make one data vector out of all the data from the t_dp_sosi_arr.
--
-- Description:
--  The data from the t_dp_sosi_arr gets put into one data vector.
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.dp_stream_pkg.all;

entity ddrctrl_input_pack is
  generic (
    g_nof_streams : positive := 12;  -- number of input streams
    g_data_w      : natural  := 14  -- data with of input data vectors
  );
  port (
    in_sosi_arr   : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);  -- input data
    out_sosi      : out t_dp_sosi       := c_dp_sosi_init  -- output data
  );
end ddrctrl_input_pack;

architecture rtl of ddrctrl_input_pack is
begin
  -- Putting all the data from the different streams into one data vector.
  gen_extract_and_pack_data : for I in 0 to g_nof_streams - 1 generate
    p_generate : process(in_sosi_arr) is
    begin
      out_sosi.data(g_data_w * (I + 1) - 1 downto g_data_w * I) <= in_sosi_arr(I).data(g_data_w - 1 downto 0);
    end process;
  end generate;

  -- check if the input data is valid bij doing a and operation on all of them
  out_sosi.valid  <= func_dp_stream_arr_and(in_sosi_arr, "VALID");
  out_sosi.sop    <= func_dp_stream_arr_and(in_sosi_arr, "SOP");
  out_sosi.eop    <= func_dp_stream_arr_and(in_sosi_arr, "EOP");
  out_sosi.sync   <= func_dp_stream_arr_and(in_sosi_arr, "SYNC");
  out_sosi.bsn    <= in_sosi_arr(0).bsn;
end rtl;
