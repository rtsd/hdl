-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Authors : J Hargreaves, L Hiemstra, R van der Walle
-- Purpose:
--   AIT - ADC (Jesd) receiver, input, timing and associated diagnostic blocks
-- Description:
--   Unb2b version for lab testing
--   Contains all the signal processing blocks to receive and time the ADC input data
--   See https://support.astron.nl/confluence/display/STAT/L5+SDPFW+DD%3A+ADC+data+input+and+timestamp

library IEEE, common_lib, diag_lib, aduh_lib, dp_lib, tech_jesd204b_lib, st_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.sdp_pkg.all;

entity node_sdp_adc_input_and_timing is
  generic (
    g_no_jesd                 : boolean := false;  -- when false use Rx JESD204B and WG, else use only WG
    -- When g_use_tech_jesd204b_v2 = false use tech_jesd204b with tech_jesd204b and FIFO in ait,
    -- else use tech_jesd204b_v2 with FIFO in tech_jesd204b_v2
    g_use_tech_jesd204b_v2    : boolean := false;
    g_no_st_histogram         : boolean := true;  -- when false use input histogram, else not to save block RAM
    g_buf_nof_data            : natural := c_sdp_V_si_db;
    g_bsn_nof_clk_per_sync    : natural := c_sdp_N_clk_per_sync;  -- Default 200M, overide for short simulation
    g_sim                     : boolean := false
  );
  port (
    -- clocks and resets
    mm_clk                         : in std_logic;
    mm_rst                         : in std_logic;
    dp_clk                         : in std_logic;
    dp_rst                         : in std_logic;
    dp_pps                         : in std_logic := '0';

    -- mm control buses
    -- JESD
    jesd204b_mosi                  : in  t_mem_mosi := c_mem_mosi_rst;
    jesd204b_miso                  : out t_mem_miso := c_mem_miso_rst;

    -- Shiftram (applies per-antenna delay)
    reg_dp_shiftram_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_shiftram_miso           : out t_mem_miso := c_mem_miso_rst;

    -- bsn source
    reg_bsn_source_v2_mosi         : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_source_v2_miso         : out t_mem_miso := c_mem_miso_rst;

    -- bsn scheduler
    reg_bsn_scheduler_wg_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_scheduler_wg_miso      : out t_mem_miso := c_mem_miso_rst;

    -- WG
    reg_wg_mosi                    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_wg_miso                    : out t_mem_miso := c_mem_miso_rst;
    ram_wg_mosi                    : in  t_mem_mosi := c_mem_mosi_rst;
    ram_wg_miso                    : out t_mem_miso := c_mem_miso_rst;

    -- BSN MONITOR
    reg_bsn_monitor_input_mosi     : in  t_mem_mosi;
    reg_bsn_monitor_input_miso     : out t_mem_miso;

    -- Data buffer for framed samples (variable depth)
    ram_diag_data_buf_bsn_mosi     : in  t_mem_mosi;
    ram_diag_data_buf_bsn_miso     : out t_mem_miso;
    reg_diag_data_buf_bsn_mosi     : in  t_mem_mosi;
    reg_diag_data_buf_bsn_miso     : out t_mem_miso;

    -- ST Histogram
    ram_st_histogram_mosi          : in  t_mem_mosi;
    ram_st_histogram_miso          : out t_mem_miso;

    -- Aduh (statistics) monitor
    reg_aduh_monitor_mosi          : in  t_mem_mosi;
    reg_aduh_monitor_miso          : out t_mem_miso;

    -- JESD control
    jesd_ctrl_mosi                 : in  t_mem_mosi;
    jesd_ctrl_miso                 : out t_mem_miso;

    -- JESD io signals
    jesd204b_serial_data           : in    std_logic_vector(c_sdp_S_pn - 1 downto 0) := (others => '0');
    jesd204b_refclk                : in    std_logic := '0';
    jesd204b_sysref                : in    std_logic := '0';
    jesd204b_sync_n                : out   std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0) := (others => '0');

    -- Streaming data output
    out_sosi_arr                   : out t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0);
    dp_bsn_source_restart          : out std_logic;  -- for dp_sync_recover in WPFB
    dp_bsn_source_new_interval     : out std_logic;  -- for SST and BST statistics offload, XST uses new_interval based on xst_processing_enable
    dp_bsn_source_nof_clk_per_sync : out std_logic_vector(c_word_w - 1 downto 0)  -- for RSN source in transient buffer
  );
end node_sdp_adc_input_and_timing;

architecture str of node_sdp_adc_input_and_timing is
  -- Waveform Generator
  constant c_wg_buf_directory       : string := "data/";
  constant c_wg_buf_dat_w           : natural := 18;  -- default value of WG that fits 14 bits of ADC data
  constant c_wg_buf_addr_w          : natural := 10;  -- default value of WG for 1024 samples;
  signal trigger_wg                 : std_logic;

  -- Frame parameters
  constant c_bs_bsn_w               : natural := 64;  -- > 51;
  constant c_bs_aux_w               : natural := 2;
  constant c_dp_fifo_dc_size        : natural := 64;

  signal dp_sysref                  : std_logic;

  -- JESD signals
  signal rx_clk                     : std_logic;  -- formerly jesd204b_frame_clk
  signal rx_rst                     : std_logic;
  signal rx_sysref                  : std_logic;

  signal mm_rst_jesd                : std_logic;
  signal mm_jesd_ctrl_reg_wr        : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_jesd_ctrl_reg_rd        : std_logic_vector(c_word_w - 1 downto 0);
  signal jesd204b_disable_arr       : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal jesd204b_reset_request     : std_logic := '0';

  -- AIT processing
  signal rx_sosi_arr                : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_shiftram_snk_in_arr     : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal ant_sosi_arr               : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal bs_sosi                    : t_dp_sosi := c_dp_sosi_rst;
  signal bs_sosi_valid              : std_logic;
  signal wg_sosi_arr                : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal mux_sosi_arr               : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal nxt_mux_sosi_arr           : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal st_sosi_arr                : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);

  signal rx_bsn_source_restart          : std_logic;
  signal rx_bsn_source_new_interval     : std_logic;
  signal rx_bsn_source_nof_clk_per_sync : std_logic_vector(c_word_w - 1 downto 0);
  signal rx_aux                         : std_logic_vector(c_bs_aux_w - 1 downto 0);
  signal dp_aux                         : std_logic_vector(c_bs_aux_w - 1 downto 0);
begin
  gen_no_jesd : if g_no_jesd = true generate
    dp_sysref <= dp_pps;
    rx_sosi_arr <= (others => c_dp_sosi_rst);

    jesd204b_miso <= c_mem_miso_rst;
    jesd_ctrl_miso <= c_mem_miso_rst;
  end generate;  -- gen_no_jesd

  gen_jesd : if g_no_jesd = false generate
    -----------------------------------------------------------------------------
    -- JESD Control register
    -----------------------------------------------------------------------------
    -- The node AIT is reset at power up by mm_rst and under software control by mm_rst_jesd.
    -- The mm_rst_jesd will cause a reset on the rx_rst by the reset sequencer in the u_jesd204b.
    -- The mm_rst_jesd is intended for node AIT resynchronisation tests of the u_jesd204b.
    -- The mm_rst_jesd should not be applied in an active SDP application, because this will cause
    -- a disturbance in the block timing of the out_sosi_arr(i).sync,bsn,sop,eop. The other logic
    -- in an SDP application assumes that the block timing of the out_sosi_arr(i) only contains
    -- complete blocks, so from sop to eop. Therefore, first mms_dp_bsn_source_v2 should be
    -- disabled to stop and flush the block processing, before applying mm_rst_jesd.

    -- Only accept JESD204B IP reset when the processing is disabled (indicated by bs_sosi_valid
    -- = '0'), to avoid corrupt bs_sosi blocks entering the subsequent processing due to that a
    -- JESD204B IP reset causes that the rx_clk stops.
    mm_rst_jesd <= mm_rst or jesd204b_reset_request;

    jesd204b_disable_arr <= mm_jesd_ctrl_reg_wr(c_sdp_S_pn - 1 downto 0);
    jesd204b_reset_request <= mm_jesd_ctrl_reg_wr(c_sdp_jesd_ctrl_reset_bi) and not bs_sosi_valid;

    p_mm_jesd_ctrl_reg_rd : process(mm_jesd_ctrl_reg_wr, jesd204b_reset_request)
    begin
      -- default readback what was written
      mm_jesd_ctrl_reg_rd <= mm_jesd_ctrl_reg_wr;
      -- report actual JESD204B reset status
      mm_jesd_ctrl_reg_rd(c_sdp_jesd_ctrl_reset_bi) <= jesd204b_reset_request;
    end process;

    u_mm_jesd_ctrl_reg : entity common_lib.common_reg_r_w
    generic map (
      g_reg       => c_sdp_mm_jesd_ctrl_reg,
      g_init_reg  => (others => '0')
    )
    port map (
      rst       => mm_rst,
      clk       => mm_clk,
      -- control side
      wr_en     => jesd_ctrl_mosi.wr,
      wr_adr    => jesd_ctrl_mosi.address(c_sdp_mm_jesd_ctrl_reg.adr_w - 1 downto 0),
      wr_dat    => jesd_ctrl_mosi.wrdata(c_sdp_mm_jesd_ctrl_reg.dat_w - 1 downto 0),
      rd_en     => jesd_ctrl_mosi.rd,
      rd_adr    => jesd_ctrl_mosi.address(c_sdp_mm_jesd_ctrl_reg.adr_w - 1 downto 0),
      rd_dat    => jesd_ctrl_miso.rddata(c_sdp_mm_jesd_ctrl_reg.dat_w - 1 downto 0),
      rd_val    => OPEN,
      -- data side
      out_reg   => mm_jesd_ctrl_reg_wr,
      in_reg    => mm_jesd_ctrl_reg_rd
    );

    -----------------------------------------------------------------------------
    -- JESD204B IP (ADC Handler)
    -----------------------------------------------------------------------------
    gen_jesd204b_v1 : if g_use_tech_jesd204b_v2 = false generate
      u_jesd204b_v1 : entity tech_jesd204b_lib.tech_jesd204b
      generic map(
        g_sim                => false,  -- do not use g_sim, because JESD204B IP does support mm_clk in sim
        g_nof_streams        => c_sdp_S_pn,
        g_nof_sync_n         => c_sdp_N_sync_jesd,
        g_jesd_freq          => c_sdp_jesd204b_freq
      )
      port map(
        jesd204b_refclk      => JESD204B_REFCLK,
        jesd204b_sysref      => JESD204B_SYSREF,
        jesd204b_sync_n_arr  => jesd204b_sync_n,

        rx_sosi_arr          => rx_sosi_arr,
        rx_clk               => rx_clk,
        rx_rst               => rx_rst,
        rx_sysref            => rx_sysref,

        jesd204b_disable_arr => jesd204b_disable_arr,

        -- MM
        mm_clk               => mm_clk,
        mm_rst               => mm_rst_jesd,

        jesd204b_mosi        => jesd204b_mosi,
        jesd204b_miso        => jesd204b_miso,

         -- Serial
        serial_tx_arr        => open,
        serial_rx_arr        => JESD204B_SERIAL_DATA(c_sdp_S_pn - 1 downto 0)
      );
    end generate;

    gen_jesd204b_v2 : if g_use_tech_jesd204b_v2 = true generate
      u_jesd204b_v2: entity tech_jesd204b_lib.tech_jesd204b_v2
      generic map(
        g_sim                => false,  -- do not use g_sim, because JESD204B IP does support mm_clk in sim
        g_nof_streams        => c_sdp_S_pn,
        g_nof_sync_n         => c_sdp_N_sync_jesd,
        g_jesd_freq          => c_sdp_jesd204b_freq
      )
      port map(
        jesd204b_refclk      => JESD204B_REFCLK,
        jesd204b_sysref      => JESD204B_SYSREF,
        jesd204b_sync_n_arr  => jesd204b_sync_n,

        dp_clk               => dp_clk,
        dp_rst               => dp_rst,
        dp_sysref            => dp_sysref,
        dp_sosi_arr          => rx_sosi_arr,

        jesd204b_disable_arr => jesd204b_disable_arr,

        -- MM
        mm_clk               => mm_clk,
        mm_rst               => mm_rst_jesd,

        jesd204b_mosi        => jesd204b_mosi,
        jesd204b_miso        => jesd204b_miso,

         -- Serial
        serial_tx_arr        => open,
        serial_rx_arr        => JESD204B_SERIAL_DATA(c_sdp_S_pn - 1 downto 0)
      );
    end generate;
  end generate;  -- gen_jesd

  -- Wire rx_clk from tech_jesd204b to AIT
  gen_rx_ait : if g_no_jesd = false and g_use_tech_jesd204b_v2 = false generate
    u_rx_ait : entity work.sdp_adc_input_and_timing
    generic map (
      g_no_st_histogram         => g_no_st_histogram,
      g_buf_nof_data            => g_buf_nof_data,
      g_bsn_nof_clk_per_sync    => g_bsn_nof_clk_per_sync,  -- Default 200M, overide for short simulation
      g_sim                     => g_sim
    )
    port map (
      -- clocks and resets
      mm_clk                         => mm_clk,
      mm_rst                         => mm_rst,
      rx_clk                         => rx_clk,
      rx_rst                         => rx_rst,
      rx_sysref                      => rx_sysref,

      -- input samples
      rx_sosi_arr                    => rx_sosi_arr,

      -- processing status
      bs_sosi_valid                  => bs_sosi_valid,
      -- MM control buses
      -- Shiftram (applies per-antenna delay)
      reg_dp_shiftram_mosi           => reg_dp_shiftram_mosi,
      reg_dp_shiftram_miso           => reg_dp_shiftram_miso,

      -- bsn source
      reg_bsn_source_v2_mosi         => reg_bsn_source_v2_mosi,
      reg_bsn_source_v2_miso         => reg_bsn_source_v2_miso,

      -- bsn scheduler
      reg_bsn_scheduler_wg_mosi      => reg_bsn_scheduler_wg_mosi,
      reg_bsn_scheduler_wg_miso      => reg_bsn_scheduler_wg_miso,

      -- WG
      reg_wg_mosi                    => reg_wg_mosi,
      reg_wg_miso                    => reg_wg_miso,
      ram_wg_mosi                    => ram_wg_mosi,
      ram_wg_miso                    => ram_wg_miso,

      -- BSN MONITOR
      reg_bsn_monitor_input_mosi     => reg_bsn_monitor_input_mosi,
      reg_bsn_monitor_input_miso     => reg_bsn_monitor_input_miso,

      -- Data buffer for framed samples (variable depth)
      ram_diag_data_buf_bsn_mosi     => ram_diag_data_buf_bsn_mosi,
      ram_diag_data_buf_bsn_miso     => ram_diag_data_buf_bsn_miso,
      reg_diag_data_buf_bsn_mosi     => reg_diag_data_buf_bsn_mosi,
      reg_diag_data_buf_bsn_miso     => reg_diag_data_buf_bsn_miso,

      -- ST Histogram
      ram_st_histogram_mosi          => ram_st_histogram_mosi,
      ram_st_histogram_miso          => ram_st_histogram_miso,

      -- Aduh (statistics) monitor
      reg_aduh_monitor_mosi          => reg_aduh_monitor_mosi,
      reg_aduh_monitor_miso          => reg_aduh_monitor_miso,

      -- Streaming data output
      out_sosi_arr                   => st_sosi_arr,
      rx_bsn_source_restart          => rx_bsn_source_restart,
      rx_bsn_source_new_interval     => rx_bsn_source_new_interval,
      rx_bsn_source_nof_clk_per_sync => rx_bsn_source_nof_clk_per_sync
    );

    -----------------------------------------------------------------------------
    -- Output rx_clk to dp_clk clock domain crossing
    -----------------------------------------------------------------------------
    -- rx_aux is synchronous with st_sosi_arr
    rx_aux(0) <= rx_bsn_source_restart;
    rx_aux(1) <= rx_bsn_source_new_interval;

    -- dp_aux is synchronous with out_sosi_arr
    dp_bsn_source_restart      <= dp_aux(0);
    dp_bsn_source_new_interval <= dp_aux(1);

    -- Thin dual clock fifo to cross from jesd frame clock (rx_clk) to dp_clk domain
    u_dp_fifo_dc_arr : entity dp_lib.dp_fifo_dc_arr
      generic map (
        g_nof_streams    => c_sdp_S_pn,
        g_data_w         => c_sdp_W_adc,
        g_data_signed    => true,
        g_bsn_w          => c_bs_bsn_w,
        g_aux_w          => c_bs_aux_w,
        g_use_empty      => false,
        g_use_ctrl       => true,
        g_use_sync       => true,
        g_use_bsn        => true,
        g_use_aux        => true,
        g_fifo_size      => c_dp_fifo_dc_size
      )
      port map (
        wr_rst           => rx_rst,
        wr_clk           => rx_clk,
        rd_rst           => dp_rst,
        rd_clk           => dp_clk,
        snk_in_arr       => st_sosi_arr,
        src_out_arr      => out_sosi_arr,
        in_aux           => rx_aux,
        out_aux          => dp_aux
      );

    -- MM write of rx_bsn_source_nof_clk_per_sync occurs with sufficient margin before it
    -- is used. Still use common_reg_cross_domain nonetheless to get from mm_clk to rx_clk
    -- in mms_dp_bsn_monitor, and from rx_clk to dp_clk here. No need to go via
    -- u_dp_fifo_dc_arr, use common_reg_cross_domain instead to save logic and/or RAM.
    u_dp_nof_block_per_sync : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => rx_rst,
      in_clk      => rx_clk,
      in_dat      => rx_bsn_source_nof_clk_per_sync,
      out_rst     => dp_rst,
      out_clk     => dp_clk,
      out_dat     => dp_bsn_source_nof_clk_per_sync
    );
  end generate;  -- gen_rx_ait

  -- Wire dp_clk to AIT when jesd IP tech_jesd204b_v2 is used, or when only the WG is used (so no jesd)
  gen_dp_ait : if (g_no_jesd = false and g_use_tech_jesd204b_v2 = true) or g_no_jesd = true generate
    u_dp_ait : entity work.sdp_adc_input_and_timing
    generic map (
      g_no_st_histogram         => g_no_st_histogram,
      g_buf_nof_data            => g_buf_nof_data,
      g_bsn_nof_clk_per_sync    => g_bsn_nof_clk_per_sync,  -- Default 200M, overide for short simulation
      g_sim                     => g_sim
    )
    port map (
      -- clocks and resets
      mm_clk                         => mm_clk,
      mm_rst                         => mm_rst,
      rx_clk                         => dp_clk,
      rx_rst                         => dp_rst,
      rx_sysref                      => dp_sysref,

      -- input samples
      rx_sosi_arr                    => rx_sosi_arr,

       -- processing status
      bs_sosi_valid                  => bs_sosi_valid,

      -- MM control buses
      -- Shiftram (applies per-antenna delay)
      reg_dp_shiftram_mosi           => reg_dp_shiftram_mosi,
      reg_dp_shiftram_miso           => reg_dp_shiftram_miso,

      -- bsn source
      reg_bsn_source_v2_mosi         => reg_bsn_source_v2_mosi,
      reg_bsn_source_v2_miso         => reg_bsn_source_v2_miso,

      -- bsn scheduler
      reg_bsn_scheduler_wg_mosi      => reg_bsn_scheduler_wg_mosi,
      reg_bsn_scheduler_wg_miso      => reg_bsn_scheduler_wg_miso,

      -- WG
      reg_wg_mosi                    => reg_wg_mosi,
      reg_wg_miso                    => reg_wg_miso,
      ram_wg_mosi                    => ram_wg_mosi,
      ram_wg_miso                    => ram_wg_miso,

      -- BSN MONITOR
      reg_bsn_monitor_input_mosi     => reg_bsn_monitor_input_mosi,
      reg_bsn_monitor_input_miso     => reg_bsn_monitor_input_miso,

      -- Data buffer for framed samples (variable depth)
      ram_diag_data_buf_bsn_mosi     => ram_diag_data_buf_bsn_mosi,
      ram_diag_data_buf_bsn_miso     => ram_diag_data_buf_bsn_miso,
      reg_diag_data_buf_bsn_mosi     => reg_diag_data_buf_bsn_mosi,
      reg_diag_data_buf_bsn_miso     => reg_diag_data_buf_bsn_miso,

      -- ST Histogram
      ram_st_histogram_mosi          => ram_st_histogram_mosi,
      ram_st_histogram_miso          => ram_st_histogram_miso,

      -- Aduh (statistics) monitor
      reg_aduh_monitor_mosi          => reg_aduh_monitor_mosi,
      reg_aduh_monitor_miso          => reg_aduh_monitor_miso,

      -- Streaming data output
      out_sosi_arr                   => out_sosi_arr,
      rx_bsn_source_restart          => dp_bsn_source_restart,
      rx_bsn_source_new_interval     => dp_bsn_source_new_interval,
      rx_bsn_source_nof_clk_per_sync => dp_bsn_source_nof_clk_per_sync
    );
  end generate;  -- gen_rx_ait
end str;
