-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: resize the data into a single sosi with a width of g_out_data_w.
--
-- Description:
--  The data gets collected into a vector(c_v) and from this vector the output
--  data gets read. When the reading passes the halfway point of c_v new data
--  is requested and the whole vector shifts g_in_data_w amount of bits to fit
--  the new data at the end.
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
--  The output vector must be larger than the input vector.

library IEEE, dp_lib, tech_ddr_lib, common_lib;
use IEEE.std_logic_1164.all;
use dp_lib.dp_stream_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use common_lib.common_pkg.all;

entity ddrctrl_output_unpack is
  generic (
    g_tech_ddr      : t_c_tech_ddr;
    g_in_data_w     : natural;
    g_out_data_w    : natural;
    g_block_size    : natural;
    g_bim           : natural
  );
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    in_sosi         : in  t_dp_sosi   := c_dp_sosi_init;
    in_bsn          : in  std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
    out_siso        : in  t_dp_siso;
    out_sosi        : out t_dp_sosi   := c_dp_sosi_init;
    out_ready       : out std_logic   := '0';
    state_off       : out std_logic   := '0'
  );
end ddrctrl_output_unpack;

architecture rtl of ddrctrl_output_unpack is
  constant c_v_w  : natural := g_in_data_w * 2;

  -- type for statemachine
  type t_state is ( READING, FIRST_READ, OVER_HALF, BSN, RESET, IDLE, OFF);

  -- record for readability
  type t_reg is record
  state           : t_state;
  a_of            : natural;
  op_data_cnt     : natural;
  delay_data      : std_logic_vector(g_in_data_w - 1 downto 0);
  dd_fresh        : std_logic;
  valid_data      : std_logic;
  c_v             : std_logic_vector(c_v_w - 1 downto 0);
  bsn_cnt         : natural;
  out_sosi        : t_dp_sosi;
  out_ready       : std_logic;
  state_off       : std_logic;
  end record;

  constant c_t_reg_init   : t_reg     := (RESET, 0, 0, (others => '0'), '0', '0', (others => '0'), 0, c_dp_sosi_init, '0', '0');

  -- signals for readability
  signal d_reg            : t_reg     := c_t_reg_init;
  signal q_reg            : t_reg     := c_t_reg_init;
begin
  q_reg <= d_reg when rising_edge(clk);

  -- put the input data into c_v and fill the output vector from c_v
  p_state : process(q_reg, rst, in_bsn, in_sosi, out_siso)

    variable v            : t_reg;
  begin
    v           := q_reg;
    if out_siso.ready = '1' or q_reg.state = OFF or q_reg.state = IDLE or q_reg.state = RESET or rst = '1' then

      case q_reg.state is
      when READING =>
        -- generating output from the data already present in c_v
        v.out_sosi.data(g_out_data_w - 1 downto 0) := q_reg.c_v((g_out_data_w * (q_reg.op_data_cnt + 1)) + q_reg.a_of - 1 downto (g_out_data_w * q_reg.op_data_cnt) + q_reg.a_of);
        v.out_sosi.valid  := '1';
        v.bsn_cnt         := q_reg.bsn_cnt + 1;
        v.op_data_cnt     := q_reg.op_data_cnt + 1;

        if q_reg.out_sosi.eop = '1' then
          v.out_sosi.bsn(c_dp_stream_bsn_w - 1 downto 0) := INCR_UVEC(q_reg.out_sosi.bsn, 1);
          v.out_sosi.eop  := '0';
          v.out_sosi.sop  := '1';
          v.bsn_cnt       := 0;
        elsif q_reg.out_sosi.sop = '1' then
          v.out_sosi.sop  := '0';
        end if;

      when OVER_HALF =>
        -- generating output data from c_v but past the halfway point of c_v so there needs to be new data added
        if q_reg.valid_data = '1' then
          -- generate output from the middle of c_v
          v.out_sosi.data(g_out_data_w - 1 downto 0) := q_reg.c_v((g_out_data_w * (q_reg.op_data_cnt + 1)) + q_reg.a_of - 1 downto (g_out_data_w * q_reg.op_data_cnt) + q_reg.a_of);
          v.out_sosi.valid  := '1';
          v.bsn_cnt         := q_reg.bsn_cnt + 1;
          -- put the second half of c_v into the first half of c_v
          v.c_v(g_in_data_w - 1 downto 0) := q_reg.c_v(c_v_w - 1 downto g_in_data_w);
          v.valid_data      := '0';
          v.a_of            := ((g_out_data_w * (q_reg.op_data_cnt + 1)) + q_reg.a_of) - g_in_data_w;
          v.op_data_cnt     := 0;
        elsif q_reg.valid_data = '0' then
          -- there is no data ready.
        end if;

        if q_reg.out_sosi.sop = '1' then
          v.out_sosi.sop  := '0';
        end if;

      when FIRST_READ =>
        -- put the second half of c_v into the first half of c_v
        v.c_v(g_in_data_w - 1 downto 0) := q_reg.c_v(c_v_w - 1 downto g_in_data_w);
        v.valid_data      := '0';

        -- fills the first half of c_v and generates output from it.
        v.c_v(g_in_data_w - 1 downto 0) := q_reg.c_v(c_v_w - 1 downto g_in_data_w);
        v.out_sosi.data(g_out_data_w - 1 downto 0) := v.c_v(g_out_data_w - 1 downto 0);
        v.out_sosi.valid  := '1';
        v.out_sosi.bsn(c_dp_stream_bsn_w - 1 downto 0) := in_bsn(c_dp_stream_bsn_w - 1 downto 0);
        v.out_sosi.sop    := '1';
        v.out_sosi.eop    := '0';
        v.bsn_cnt         := 0;
        v.op_data_cnt     := 1;

      when BSN =>
        -- generating output data from c_v but past the halfway point of c_v so there needs to be new data added also increases the bsn output
        v.out_sosi.valid := '0';
        if q_reg.valid_data = '1' then
          -- generate output from the middle of c_v
          v.out_sosi.data(g_out_data_w - 1 downto 0) := q_reg.c_v((g_out_data_w * (q_reg.op_data_cnt + 1)) + q_reg.a_of - 1 downto (g_out_data_w * q_reg.op_data_cnt) + q_reg.a_of);
          v.out_sosi.valid  := '1';
          v.bsn_cnt         := q_reg.bsn_cnt + 1;
          -- put the second half of c_v into the first half of c_v
          v.c_v(g_in_data_w - 1 downto 0) := q_reg.c_v(c_v_w - 1 downto g_in_data_w);
          v.valid_data      := '0';
          v.op_data_cnt     := 0;
        elsif (g_out_data_w * (v.op_data_cnt + 1)) + q_reg.a_of < g_in_data_w then
          -- generate output from the middle of c_v
          v.out_sosi.data(g_out_data_w - 1 downto 0) := q_reg.c_v((g_out_data_w * (q_reg.op_data_cnt + 1)) + q_reg.a_of - 1 downto (g_out_data_w * q_reg.op_data_cnt) + q_reg.a_of);
          v.out_sosi.valid  := '1';
          v.bsn_cnt         := q_reg.bsn_cnt + 1;
        end if;

        v.out_sosi.eop      := '1';
        v.a_of              := 0;
        v.bsn_cnt           := q_reg.bsn_cnt + 1;

      when RESET =>
        v := c_t_reg_init;

      when IDLE =>
        -- the statemachine goes to Idle when its finished or when its waiting on other components.
        v.out_sosi.valid  := '0';

      when OFF =>
        -- the stamachine has a state off so it knows when to go to first read, it can't go to first read from IDLE
        v.out_sosi  := c_dp_sosi_init;
        v.bsn_cnt   := 0;
        v.state_off := '1';
      end case;

    else
      -- IDLE
      v.out_sosi.valid  := '0';
    end if;

    -- compensating for fifo delay
    if q_reg.dd_fresh = '1' and q_reg.valid_data = '0' then
      -- put the delay data into the second half of c_v beceause these are now zeros
      v.c_v(c_v_w - 1 downto g_in_data_w) := q_reg.delay_data(g_in_data_w - 1 downto 0);
      v.dd_fresh      := '0';
      v.valid_data    := '1';
    end if;
    if in_sosi.valid = '1' then
      v.delay_data(g_in_data_w - 1 downto 0) := in_sosi.data(g_in_data_w - 1 downto 0);
      v.dd_fresh      := '1';
    end if;

    if rst = '1' then
      v.state     := RESET;
    elsif q_reg.state = RESET or (q_reg.valid_data = '0' and q_reg.state = OFF) or ((INCR_UVEC(INCR_UVEC(in_bsn, g_bim), -1)(c_dp_stream_bsn_w - 1 downto 0) = q_reg.out_sosi.bsn(c_dp_stream_bsn_w - 1 downto 0)) and v.out_sosi.eop = '1') then
      v.state     := OFF;
    elsif q_reg.state = OFF then
      v.state_off := '0';
      v.state     := FIRST_READ;
    elsif out_siso.ready = '0' then
      v.state     := IDLE;
    elsif q_reg.bsn_cnt = g_block_size-3 then
      v.state     := BSN;
    elsif q_reg.dd_fresh = '0' and q_reg.valid_data = '0' then
      v.state     := IDLE;
    elsif (g_out_data_w * (v.op_data_cnt + 1)) + q_reg.a_of >= g_in_data_w then
      v.state     := OVER_HALF;
    else
      v.state     := READING;
    end if;

    if q_reg.state = OFF and in_sosi.valid = '1' then
      v.out_ready := '0';
    else
      v.out_ready   := not v.valid_data;
    end if;
    d_reg <= v;
  end process;

  -- fill outputs
  out_sosi  <= q_reg.out_sosi;
  out_ready <= q_reg.out_ready;
  state_off <= q_reg.state_off;
end rtl;
