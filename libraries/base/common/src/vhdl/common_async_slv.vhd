-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Clock an asynchronous input slv into the clk domain
-- Description:
--   See common_async.
-- Remark:

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_async_slv is
  generic (
    g_rst_level : std_logic := '0';
    g_delay_len : positive := c_meta_delay_len  -- use common_pipeline if g_delay_len=0 for wires only is also needed
  );
  port (
    rst  : in  std_logic := '0';
    clk  : in  std_logic;
    din  : in  std_logic_vector;
    dout : out std_logic_vector
  );
end;

architecture str of common_async_slv is
begin
  gen_slv: for I in dout'range generate
    u_common_async : entity work.common_async
    generic map (
      g_rst_level => g_rst_level,
      g_delay_len => g_delay_len
    )
    port map (
      rst  => rst,
      clk  => clk,
      din  => din(I),
      dout => dout(I)
    );
  end generate;
end str;
