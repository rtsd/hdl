-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_pipeline_integer is
  generic (
    g_representation : string  := "SIGNED";  -- or "UNSIGNED"
    g_pipeline       : natural := 1;  -- 0 for wires, > 0 for registers,
    g_reset_value    : integer := 0;
    g_dat_w          : natural := 8
  );
  port (
    rst     : in  std_logic := '0';
    clk     : in  std_logic;
    clken   : in  std_logic := '1';
    in_clr  : in  std_logic := '0';
    in_en   : in  std_logic := '1';
    in_dat  : in  integer;
    out_dat : out integer
  );
end common_pipeline_integer;

architecture str of common_pipeline_integer is
  signal in_dat_slv   : std_logic_vector(g_dat_w - 1 downto 0);
  signal out_dat_slv  : std_logic_vector(g_dat_w - 1 downto 0);
begin
  in_dat_slv <= TO_SVEC(in_dat, g_dat_w) when g_representation = "SIGNED" else TO_UVEC(in_dat, g_dat_w);
  out_dat    <= TO_SINT(out_dat_slv)     when g_representation = "SIGNED" else TO_UINT(out_dat_slv);

  u_int : entity work.common_pipeline
  generic map (
    g_representation => g_representation,
    g_pipeline       => g_pipeline,
    g_reset_value    => g_reset_value,
    g_in_dat_w       => g_dat_w,
    g_out_dat_w      => g_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    in_clr  => in_clr,
    in_en   => in_en,
    in_dat  => in_dat_slv,
    out_dat => out_dat_slv
  );
end str;
