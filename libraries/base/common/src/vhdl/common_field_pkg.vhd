-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_str_pkg.all;

-- Purpose:
-- . Dynamically map record-like field structures onto SLVs.
-- Description:
-- . The MM register is defined by mm_fields.vhd.
-- . The MM register consists of "RO" = input fields (status) and "RW" = output fields (control) in
--   arbitrary order. The entire register is kept in a word_arr slv. The functions can extract the
--   "RO" fields into a slv_in and the "RW" fields into a slv_out. Hence the slv_in'LENGTH +
--   slv_out'LENGTH = word_arr'LENGTH.
--
-- . Advantages:
--   . Replaces non-generic (dedicated) records;
--   . Field widths are variable
-- Remarks:

package common_field_pkg is
  constant c_common_field_name_len    : natural := 64;
  constant c_common_field_default_len : natural := 256;

  type t_common_field is record
    name     : string(1 to c_common_field_name_len);
    mode     : string(1 to 2);
    size     : positive;
    default  : std_logic_vector(c_common_field_default_len - 1 downto 0);
  end record;

  type t_common_field_arr is array(integer range <>) of t_common_field;

  function field_name_pad(name: string) return string;

  function field_default(slv_in: std_logic_vector) return std_logic_vector;
  function field_default(nat_in: natural) return std_logic_vector;

  function field_map_defaults(field_arr : t_common_field_arr) return std_logic_vector;  -- returns slv_out

  function field_mode        (field_arr : t_common_field_arr; name: string     ) return string;
  function field_size        (field_arr : t_common_field_arr; name: string     ) return natural;
  function field_hi          (field_arr : t_common_field_arr; name: string     ) return integer;
  function field_hi          (field_arr : t_common_field_arr; index: natural   ) return natural;
  function field_lo          (field_arr : t_common_field_arr; name: string     ) return natural;
  function field_lo          (field_arr : t_common_field_arr; index: natural   ) return natural;
  function field_slv_len     (field_arr : t_common_field_arr                   ) return natural;
  function field_slv_in_len  (field_arr : t_common_field_arr                   ) return natural;
  function field_slv_out_len (field_arr : t_common_field_arr                   ) return natural;
  function field_nof_words   (field_arr : t_common_field_arr; word_w : natural ) return natural;
  function field_map_in      (field_arr : t_common_field_arr; slv        : std_logic_vector; word_w : natural; mode : string) return std_logic_vector;  -- returns word_arr
  function field_map_out     (field_arr : t_common_field_arr; word_arr   : std_logic_vector; word_w : natural                ) return std_logic_vector;  -- returns slv_out
  function field_map         (field_arr : t_common_field_arr; word_arr_in: std_logic_vector; word_arr_out: std_logic_vector; word_w : natural) return std_logic_vector;  -- returns word_arr

  function field_select_subset(subset_field_arr : t_common_field_arr; larger_field_arr : t_common_field_arr; larger_slv : std_logic_vector) return std_logic_vector;  -- returns subset slv

  function field_ovr_arr(field_arr : t_common_field_arr; ovr_init: std_logic_vector) return t_common_field_arr;

  function field_exists(field_arr : t_common_field_arr; name: string) return boolean;

  function field_arr_set_mode(field_arr : t_common_field_arr; mode : string) return t_common_field_arr;

  function sel_a_b(sel : boolean; a, b : t_common_field_arr ) return t_common_field_arr;
end common_field_pkg;

package body common_field_pkg is
  function field_name_pad(name: string) return string is
  begin
    return pad(name, c_common_field_name_len, ' ');
  end field_name_pad;

  function field_default(slv_in: std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(slv_in, c_common_field_default_len);
  end field_default;

  function field_default(nat_in: natural) return std_logic_vector is
  begin
    return TO_UVEC(nat_in, c_common_field_default_len);
  end field_default;

  function field_map_defaults(field_arr : t_common_field_arr) return std_logic_vector is
    variable v_slv_out : std_logic_vector(field_slv_out_len(field_arr) - 1 downto 0);
  begin
    for f in 0 to field_arr'high loop
      if field_arr(f).mode = "RW" then
        v_slv_out( field_hi(field_arr, field_arr(f).name) downto field_lo(field_arr, field_arr(f).name)) := field_arr(f).default(field_arr(f).size-1 downto 0);
      end if;
    end loop;
    return v_slv_out;
  end field_map_defaults;

  function field_mode(field_arr : t_common_field_arr; name: string) return string is
  -- Returns the mode string of the passed (via name) field
  begin
    if field_exists(field_arr, name) then
      for i in 0 to field_arr'high loop
        if field_arr(i).name = field_name_pad(name) then
          return field_arr(i).mode;
        end if;
      end loop;
    else
      return "-1";
    end if;
  end field_mode;

  function field_size(field_arr : t_common_field_arr; name: string) return natural is
  -- Returns the size of the passed (via name) field
  begin
    for i in 0 to field_arr'high loop
      if field_arr(i).name = field_name_pad(name) then
        return field_arr(i).size;
      end if;
    end loop;
  end field_size;

  function field_hi(field_arr : t_common_field_arr; name: string) return integer is
  -- Returns the high (=left) bit range index of the field within the field_arr interpreted as concatenated IN or OUT SLV
    variable v_acc_hi : natural := 0;
  begin
    if field_exists(field_arr, name) then
      for i in 0 to field_arr'high loop
        if field_arr(i).mode = field_mode(field_arr, name) then  -- increment index only for the "RO" = IN or the "RW" = OUT
          v_acc_hi := v_acc_hi + field_arr(i).size;
          if field_arr(i).name = field_name_pad(name) then
            return v_acc_hi - 1;
          end if;
        end if;
      end loop;
    else  -- field does not exist; return -1 which results in null array
      return - 1;
    end if;
  end field_hi;

  function field_hi(field_arr : t_common_field_arr; index : natural) return natural is
  -- Returns the high (=left) bit range index of the field within the field_arr interpreted as concatenated SLV
    variable v_acc_hi : natural := 0;
  begin
    for i in 0 to index loop
      v_acc_hi := v_acc_hi + field_arr(i).size;
      if i = index then
        return v_acc_hi - 1;
      end if;
    end loop;
  end field_hi;

  function field_lo(field_arr : t_common_field_arr; name: string) return natural is
  -- Returns the low (=right) bit range index of the field within the field_arr interpreted as concatenated IN or OUT SLV
    variable v_acc_hi : natural := 0;
  begin
    if field_exists(field_arr, name) then
      for i in 0 to field_arr'high loop
        if field_arr(i).mode = field_mode(field_arr, name) then  -- increment index only for the "RO" = IN or the "RW" = OUT
          v_acc_hi := v_acc_hi + field_arr(i).size;
          if field_arr(i).name = field_name_pad(name) then
            return v_acc_hi - field_arr(i).size;
          end if;
        end if;
      end loop;
    else
      return 0;
    end if;
  end field_lo;

  function field_lo(field_arr : t_common_field_arr; index : natural) return natural is
  -- Returns the low (=right) bit range index of the field within the field_arr interpreted as concatenated SLV
    variable v_acc_hi : natural := 0;
  begin
    for i in 0 to index loop
      v_acc_hi := v_acc_hi + field_arr(i).size;
      if i = index then
        return v_acc_hi - field_arr(i).size;
      end if;
    end loop;
  end field_lo;

  function field_slv_len(field_arr : t_common_field_arr) return natural is
  -- Return the total length of all fields in field_arr
    variable v_len : natural := 0;
  begin
    for i in 0 to field_arr'high loop
      v_len := v_len + field_arr(i).size;
    end loop;
    return v_len;
  end field_slv_len;

  function field_slv_in_len(field_arr : t_common_field_arr) return natural is
  -- Return the total length of the input fields in field_arr (= all "RO")
    variable v_len : natural := 0;
  begin
    for f in 0 to field_arr'high loop
      if field_arr(f).mode = "RO" then
        v_len := v_len + field_arr(f).size;
      end if;
    end loop;
    return v_len;
  end field_slv_in_len;

  function field_slv_out_len(field_arr : t_common_field_arr) return natural is
  -- Return the total length of the output fields in field_arr (= all "RW")
    variable v_len : natural := 0;
  begin
    for f in 0 to field_arr'high loop
      if field_arr(f).mode = "RW" then
        v_len := v_len + field_arr(f).size;
      end if;
    end loop;
    return v_len;
  end field_slv_out_len;

  function field_nof_words(field_arr : t_common_field_arr; word_w : natural) return natural is
  -- Return the number of words (of width word_w) required to hold field_arr
    variable v_word_cnt      : natural := 0;
    variable v_nof_reg_words : natural;
  begin
    for f in 0 to field_arr'high loop
      -- Get the number of register words this field spans
      v_nof_reg_words := ceil_div(field_arr(f).size, word_w);
      for w in 0 to v_nof_reg_words - 1 loop
        v_word_cnt := v_word_cnt + 1;
      end loop;
    end loop;
    return v_word_cnt;
  end field_nof_words;

  function field_map_in(field_arr : t_common_field_arr; slv: std_logic_vector; word_w : natural; mode : string) return std_logic_vector is
  -- Re-map a field SLV into a larger SLV, support mapping both the slv_in or the slv_out that dependents on mode; each field starting at a word boundary (word_w)
    variable v_word_arr : std_logic_vector(field_nof_words(field_arr, word_w) * word_w - 1 downto 0) := (others => '0');
    variable v_word_cnt : natural := 0;
  begin
    for f in 0 to field_arr'high loop
       -- Only extract the fields that are inputs
      if field_arr(f).mode = mode then  -- if mode="RO" then slv = slv_in, else if mode="RW" then slv = slv_out
        -- Extract the field
        v_word_arr( v_word_cnt * word_w + field_arr(f).size-1 downto v_word_cnt * word_w) := slv( field_hi(field_arr, field_arr(f).name) downto field_lo(field_arr, field_arr(f).name) );
      end if;
      -- Calculate the correct word offset for the next field
      v_word_cnt  := v_word_cnt + ceil_div(field_arr(f).size, word_w);
    end loop;
    return v_word_arr;
  end field_map_in;

  function field_map_out(field_arr : t_common_field_arr; word_arr: std_logic_vector; word_w : natural) return std_logic_vector is
  -- Reverse of field_map_in
    variable v_slv_out  : std_logic_vector(field_slv_out_len(field_arr) - 1 downto 0) := (others => '0');
    variable v_word_cnt : natural := 0;
  begin
    for f in 0 to field_arr'high loop
      -- Only extract the fields that are outputs
      if field_arr(f).mode = "RW" then
        -- Extract the field
        v_slv_out( field_hi(field_arr, field_arr(f).name) downto field_lo(field_arr, field_arr(f).name)) := word_arr( v_word_cnt * word_w + field_arr(f).size-1 downto v_word_cnt * word_w);
      end if;
      -- Calculate the correct word offset for the next field
      v_word_cnt  := v_word_cnt + ceil_div(field_arr(f).size, word_w);
    end loop;
    return v_slv_out;
  end field_map_out;

  function field_map(field_arr : t_common_field_arr; word_arr_in: std_logic_vector; word_arr_out: std_logic_vector; word_w : natural) return std_logic_vector is
  -- Create one SLV consisting of both read-only and output-readback fields, e.g. as input to an MM reg
    variable v_word_arr : std_logic_vector(field_nof_words(field_arr, word_w) * word_w - 1 downto 0);
    variable v_word_cnt : natural := 0;
  begin
    -- Wire the entire SLV to the input SLV by default
    v_word_arr := word_arr_in;
    -- Now re-assign the words that need to be read back from word_arr_out
    for f in 0 to field_arr'high loop
      if field_arr(f).mode = "RW" then
        v_word_arr( v_word_cnt * word_w + field_arr(f).size-1 downto v_word_cnt * word_w) := word_arr_out( v_word_cnt * word_w + field_arr(f).size-1 downto v_word_cnt * word_w);
      end if;
      -- Calculate the correct word offset for the next field
      v_word_cnt := v_word_cnt + ceil_div(field_arr(f).size, word_w);
    end loop;
    return v_word_arr;
  end field_map;

  function field_select_subset(subset_field_arr : t_common_field_arr; larger_field_arr : t_common_field_arr; larger_slv : std_logic_vector) return std_logic_vector is
  -- Return SLV mapped to "subset_field_arr" extracted from a larger SLV mapped to "larger_field_arr".
    variable v_hi_sub  : natural;
    variable v_lo_sub  : natural;
    variable v_hi_full : natural;
    variable v_lo_full : natural;
    variable out_slv   : std_logic_vector(larger_slv'range) := (others => '0');

  begin
    for j in subset_field_arr'range loop
      v_hi_sub  := field_hi(subset_field_arr, j);
      v_lo_sub  := field_lo(subset_field_arr, j);
      v_hi_full := field_hi(larger_field_arr, subset_field_arr(j).name);
      v_lo_full := field_lo(larger_field_arr, subset_field_arr(j).name);
      out_slv(v_hi_sub downto v_lo_sub) := larger_slv(v_hi_full downto v_lo_full);
    end loop;
    return out_slv;
  end field_select_subset;

  function field_ovr_arr(field_arr : t_common_field_arr; ovr_init: std_logic_vector) return t_common_field_arr is
  -- Copy field_arr but change widths to 1 to create a 1-bit override field for each field in field_arr.
    variable v_ovr_field_arr : t_common_field_arr(field_arr'range);
  begin
    v_ovr_field_arr := field_arr;
    for i in field_arr'range loop
      v_ovr_field_arr(i).size := 1;
      v_ovr_field_arr(i).default := field_default(slv(ovr_init(i)));
    end loop;
    return v_ovr_field_arr;
  end field_ovr_arr;

  function field_exists(field_arr : t_common_field_arr; name: string) return boolean is
  begin
    for i in field_arr'range loop
      if field_arr(i).name = field_name_pad(name) then
        return true;
      end if;
    end loop;
  return false;
  end field_exists;

  function field_arr_set_mode(field_arr : t_common_field_arr; mode : string) return t_common_field_arr is
    variable v_field_arr : t_common_field_arr(field_arr'range);
  begin
    v_field_arr := field_arr;
    for i in field_arr'range loop
      v_field_arr(i).mode := mode;
    end loop;
    return v_field_arr;
  end field_arr_set_mode;

  function sel_a_b(sel :boolean; a, b : t_common_field_arr) return t_common_field_arr is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;
  end;
end common_field_pkg;
