#!/bin/bash 

patchfile='sdc_util.sdc.patch'

echo -e "Applying patch: $patchfile\n"

cd generated/altera_lvds_core20_150/synth
patch  <../../../${patchfile}

echo "done."
