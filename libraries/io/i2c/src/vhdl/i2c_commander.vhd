-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: The I2C commander can activate one out of a list of protocols
-- Description:
--   The I2C commander can execute one out of g_i2c_cmdr.nof_protocols.
--   Each protocol contains one or more I2C accesses. The protocol_index
--   points to the protocol list that will become active.
--   The protocol lists are stored sequentially in u_protocol_ram. The
--   protocol_offset_arr contains the offsets to the start of each protocol
--   list.
--   The protocol result is kept u_result_ram, but it is also processed by the
--   I2C commander. For each protocol result byte the I2C commander uses the
--   active word from the result_expected_arr to know whether the result byte
--   should be 0 (as for protocol control and write SMBUS protocol identifiers)
--   or whether it contains read data (as read SMBUS protocol identifiers).
--   For the bits in the result_expected word that are '0' the result byte must
--   be 0 and for bits that are '1' the result byte gets stored in the
--   result_data_arr. If the result control byte is not 0 then the
--   result_error_cnt gets incremented.
--   The protocol_status reports when the protocol list has finished. The
--   protocol list gets activated by the write access to the corresponding
--   index address in the I2C commander register. Actual execution of the
--   protocol list may be post poned until an external sync (default '1')
--   occurs. This is useful if the I2C accesses must be synchronized among
--   multiple nodes.
--   Each protocol can have multiple I2C accesses. The maximum number of I2C
--   read data bytes per protocol list is determined by
--   g_i2c_cmdr.nof_result_data_max. In case a protocol list has no I2C read
--   accesses then the all expected result bytes are 0, so then the size of
--   the protocol list is only limited by the size of u_protocol_ram.
--
-- Remark:
-- . Connect protocol_mosi/miso to allow for dynamically programming of the
--   protocol lists. In this way it is possible to try many different protocols
--   e.g. with different write data bytes. For normal operation typically a
--   few fixed protocol lists are sufficient, so then protocol_mosi/miso can
--   be set left unconnected, which effictively makes u_protocol_ram a ROM.
-- . g_use_result_ram = FALSE is fine because most result status info and data
--   are accessible via the commander result_error_cnt and result_data_arr
--   registers. Using TRUE allows for detailed debugging in case something
--   goes wrong on the I2C bus.
-- . The result_expected_arr contains one mask word per protocol list. If
--   the protocol list is long then this word is reused periodically. Hence
--   if the protocol list contains multiple reads then these must occur within
--   one mask word or they have to occur periodically. This can be achieved
--   by inserting SMBUS_C_NOP in the protocol list.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.i2c_pkg.all;
use work.i2c_commander_pkg.all;
use technology_lib.technology_select_pkg.all;

entity i2c_commander is
  generic (
    g_sim                    : boolean := false;
    g_technology             : natural := c_tech_select_default;
    g_i2c_cmdr               : t_c_i2c_cmdr_commander;
    g_i2c_mm                 : t_c_i2c_mm;
    g_i2c_phy                : t_c_i2c_phy;
    g_protocol_ram_init_file : string := "UNUSED";
    g_use_result_ram         : boolean := false  -- Default FALSE, because the protocol result in u_result_ram is only useful debug purposes, typically the commander status registers are sufficient
  );
  port (
    rst                      : in  std_logic;
    clk                      : in  std_logic;
    sync                     : in  std_logic := '1';

    ---------------------------------------------------------------------------
    -- Memory Mapped slave interfaces
    ---------------------------------------------------------------------------
    commander_mosi           : in  t_mem_mosi;
    commander_miso           : out t_mem_miso;

    -- If the default protocol list in u_protocol_ram is fine, then the protocol slave port can be left no connected
    protocol_mosi            : in  t_mem_mosi := c_mem_mosi_rst;
    protocol_miso            : out t_mem_miso;

    -- Typically the commander status registers are sufficient, so then the results slave port can be left no connected
    result_mosi              : in  t_mem_mosi := c_mem_mosi_rst;
    result_miso              : out t_mem_miso;

    ---------------------------------------------------------------------------
    -- I2C interface
    ---------------------------------------------------------------------------
    scl                      : inout std_logic;  -- I2C Serial Clock Line
    sda                      : inout std_logic  -- I2C Serial Data Line
  );
end i2c_commander;

architecture str of i2c_commander is
  -- Use MM bus data width = SMBus data width = c_byte_w
  constant c_ram_rd_latency : natural := 1;  -- instead of c_mem_ram_rd_latency = 2

  constant c_protocol_ram : t_c_mem := (latency  => c_ram_rd_latency,
                                        adr_w    => g_i2c_mm.protocol_adr_w,
                                        dat_w    => c_byte_w,
                                        nof_dat  => g_i2c_mm.protocol_nof_dat,
                                        init_sl  => 'X');
  constant c_result_ram   : t_c_mem := (latency  => c_ram_rd_latency,
                                        adr_w    => g_i2c_mm.result_adr_w,
                                        dat_w    => c_byte_w,
                                        nof_dat  => g_i2c_mm.result_nof_dat,
                                        init_sl  => 'X');

  -- Commander control interface
  signal protocol_wr             : std_logic;
  signal protocol_index          : natural range 0 to g_i2c_cmdr.nof_protocols - 1;
  signal protocol_offset_arr     : t_natural_arr(0 to g_i2c_cmdr.nof_protocols - 1);
  signal result_expected_arr     : t_slv_32_arr(0 to g_i2c_cmdr.nof_protocols - 1);
  signal protocol_status_rd      : std_logic;
  signal protocol_status         : std_logic_vector(c_word_w - 1 downto 0);
  signal result_error_cnt        : std_logic_vector(c_word_w - 1 downto 0);
  signal result_data_arr         : t_slv_32_arr(0 to g_i2c_cmdr.nof_result_data_max - 1);

  signal protocol_offset         : natural;
  signal protocol_activate_pend  : std_logic;  -- pulse indicating new protocol list pending
  signal protocol_activate_evt   : std_logic;  -- pulse indicating new protocol list started
  signal result_ready_evt        : std_logic;  -- pulse indicating new result is available

  -- SMBus control interface
  signal protocol_rd_en          : std_logic;
  signal protocol_rd_adr         : std_logic_vector(g_i2c_mm.protocol_adr_w - 1 downto 0);
  signal protocol_rd_dat         : std_logic_vector(c_byte_w - 1 downto 0);
  signal protocol_rd_val         : std_logic;

  signal result_wr_en            : std_logic;
  signal result_wr_adr           : std_logic_vector(g_i2c_mm.result_adr_w - 1 downto 0);
  signal result_wr_dat           : std_logic_vector(c_byte_w - 1 downto 0);

  -- I2C ctrl list - SMBus
  signal smbus_in_dat            : std_logic_vector(c_byte_w - 1 downto 0);
  signal smbus_in_req            : std_logic;
  signal smbus_out_dat           : std_logic_vector(c_byte_w - 1 downto 0);
  signal smbus_out_val           : std_logic;
  signal smbus_out_err           : std_logic;
  signal smbus_out_ack           : std_logic;
  signal smbus_st_idle           : std_logic;
  signal smbus_st_end            : std_logic;
begin
  u_commander_reg : entity work.i2c_commander_reg
  generic map (
    g_i2c_cmdr  => g_i2c_cmdr,
    g_i2c_mm    => g_i2c_mm
  )
  port map (
    -- Clocks and reset
    rst                 => rst,
    clk                 => clk,

    -- Memory Mapped Slave
    sla_in              => commander_mosi,
    sla_out             => commander_miso,

    -- MM registers
    protocol_wr         => protocol_wr,
    protocol_index      => protocol_index,
    protocol_offset_arr => protocol_offset_arr,
    result_expected_arr => result_expected_arr,
    protocol_status_rd  => protocol_status_rd,
    protocol_status     => protocol_status,
    result_error_cnt    => result_error_cnt,
    result_data_arr     => result_data_arr
  );

  u_commander_ctrl : entity work.i2c_commander_ctrl
  generic map (
    g_i2c_cmdr  => g_i2c_cmdr,
    g_i2c_mm    => g_i2c_mm
  )
  port map (
    -- Clocks and reset
    rst                   => rst,
    clk                   => clk,

    -- MM registers
    protocol_wr           => protocol_wr,
    protocol_index        => protocol_index,
    protocol_offset_arr   => protocol_offset_arr,
    result_expected_arr   => result_expected_arr,
    protocol_status_rd    => protocol_status_rd,
    protocol_status       => protocol_status,
    result_error_cnt      => result_error_cnt,
    result_data_arr       => result_data_arr,

    -- I2C protocol control
    protocol_offset       => protocol_offset,
    protocol_activate     => protocol_activate_pend,
    protocol_activate_ack => protocol_activate_evt,
    result_wr_en          => result_wr_en,
    result_wr_adr         => result_wr_adr,
    result_wr_dat         => result_wr_dat,
    result_ready_evt      => result_ready_evt
  );

  -- Activate pending protocol at sync control
  u_protocol_activate : entity common_lib.common_request
  port map (
    rst         => rst,
    clk         => clk,
    sync        => sync,
    in_req      => protocol_activate_pend,
    out_req_evt => protocol_activate_evt
  );

  -- Result ready control
  result_ready_evt <= smbus_st_end;

  u_protocol_ctrl : entity work.i2c_list_ctrl
  generic map (
    g_protocol_adr_w => g_i2c_mm.protocol_adr_w,
    g_result_adr_w   => g_i2c_mm.result_adr_w
  )
  port map (
    rst              => rst,
    clk              => clk,
    activate         => protocol_activate_evt,
    busy             => OPEN,
    protocol_rd_en   => protocol_rd_en,
    protocol_rd_adr  => protocol_rd_adr,
    protocol_rd_ofs  => protocol_offset,
    protocol_rd_dat  => protocol_rd_dat,
    protocol_rd_val  => protocol_rd_val,
    result_wr_en     => result_wr_en,
    result_wr_adr    => result_wr_adr,
    result_wr_dat    => result_wr_dat,
    smbus_out_dat    => smbus_in_dat,
    smbus_out_req    => smbus_in_req,
    smbus_in_dat     => smbus_out_dat,
    smbus_in_val     => smbus_out_val,
    smbus_in_err     => smbus_out_err,
    smbus_in_ack     => smbus_out_ack,
    smbus_st_idle    => smbus_st_idle,
    smbus_st_end     => smbus_st_end
  );

  u_smbus : entity work.i2c_smbus
  generic map (
    g_i2c_phy        => g_i2c_phy
  )
  port map (
    gs_sim           => g_sim,
    rst              => rst,
    clk              => clk,
    in_dat           => smbus_in_dat,
    in_req           => smbus_in_req,
    out_dat          => smbus_out_dat,
    out_val          => smbus_out_val,
    out_err          => smbus_out_err,
    out_ack          => smbus_out_ack,
    st_idle          => smbus_st_idle,
    st_end           => smbus_st_end,
    scl              => scl,
    sda              => sda
  );

  -- I2C protocol list register
  u_protocol_ram : entity common_lib.common_ram_rw_rw
  generic map (
    g_technology => g_technology,
    g_ram       => c_protocol_ram,
    g_init_file => g_protocol_ram_init_file
  )
  port map (
    rst         => rst,
    clk         => clk,
    wr_en_a     => protocol_mosi.wr,
    wr_dat_a    => protocol_mosi.wrdata(c_protocol_ram.dat_w - 1 downto 0),
    adr_a       => protocol_mosi.address(c_protocol_ram.adr_w - 1 downto 0),
    rd_en_a     => protocol_mosi.rd,
    rd_dat_a    => protocol_miso.rddata(c_protocol_ram.dat_w - 1 downto 0),
    rd_val_a    => OPEN,
    wr_en_b     => '0',
    --wr_dat_b    => (OTHERS => '0'),
    adr_b       => protocol_rd_adr,
    rd_en_b     => protocol_rd_en,
    rd_dat_b    => protocol_rd_dat,
    rd_val_b    => protocol_rd_val
  );

  no_result_ram : if g_use_result_ram = false generate
    result_miso <= c_mem_miso_rst;
  end generate;

  gen_result_ram : if g_use_result_ram = true generate
    -- I2C result register
    u_result_ram : entity common_lib.common_ram_rw_rw
    generic map (
      g_technology => g_technology,
      g_ram       => c_result_ram
    )
    port map (
      rst         => rst,
      clk         => clk,
      wr_en_a     => result_mosi.wr,
      wr_dat_a    => result_mosi.wrdata(c_result_ram.dat_w - 1 downto 0),
      adr_a       => result_mosi.address(c_result_ram.adr_w - 1 downto 0),
      rd_en_a     => result_mosi.rd,
      rd_dat_a    => result_miso.rddata(c_result_ram.dat_w - 1 downto 0),
      rd_val_a    => OPEN,
      wr_en_b     => result_wr_en,
      wr_dat_b    => result_wr_dat,
      adr_b       => result_wr_adr,
      rd_en_b     => '0',
      rd_dat_b    => OPEN,
      rd_val_b    => open
    );
  end generate;
end str;
