-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author: D. F. Brouwer
-- Purpose:
--   RadioHDL wrapper / Instantiate RAM IP with generics
-- Description:
--   Copied component declaration and instance example from generated/ram_2port_2040/sim/ip_agi027_1e1v_ram_r_w_ram_2port_2040_gbkw2ny.vhd

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use technology_lib.technology_pkg.all;

library altera_lnsim;
use altera_lnsim.altera_lnsim_components.all;

entity ip_agi027_1e1v_ram_r_w is
  generic (
    g_inferred   : boolean := false;
    g_adr_w      : natural := 5;
    g_dat_w      : natural := 8;
    g_nof_words  : natural := 2**5;
    g_rd_latency : natural := 1;  -- choose 1 or 2
    g_init_file  : string  := "UNUSED"
  );
  port (
    clk         : in std_logic  := '1';
    data        : in std_logic_vector(g_dat_w - 1 downto 0) := (others => '0');
    rdaddress   : in std_logic_vector(g_adr_w - 1 downto 0) := (others => '0');
    wraddress   : in std_logic_vector(g_adr_w - 1 downto 0) := (others => '0');
    wren        : in std_logic  := '0';
    q           : out std_logic_vector(g_dat_w - 1 downto 0)
  );
end ip_agi027_1e1v_ram_r_w;

architecture SYN of ip_agi027_1e1v_ram_r_w is
  constant c_outdata_reg_b : string := tech_sel_a_b(g_rd_latency = 1, "UNREGISTERED", "CLOCK0");

  component altera_syncram
  generic (
          address_aclr_b  : string;
          address_reg_b  : string;
          clock_enable_input_a  : string;
          clock_enable_input_b  : string;
          clock_enable_output_b  : string;
          init_file  : string;
          -- enable_force_to_zero : string;
          intended_device_family  : string;
          lpm_type  : string;
          numwords_a  : integer;
          numwords_b  : integer;
          operation_mode  : string;
          outdata_aclr_b  : string;
          -- outdata_sclr_b  : string;
          outdata_reg_b  : string;
          power_up_uninitialized  : string;
          -- read_during_write_mode_mixed_ports : string;
          widthad_a  : integer;
          widthad_b  : integer;
          width_a  : integer;
          width_b  : integer;
          width_byteena_a  : integer
  );
  port (
      address_a : in std_logic_vector(g_adr_w - 1 downto 0);
      address_b : in std_logic_vector(g_adr_w - 1 downto 0);
      clock0 : in std_logic;
      data_a : in std_logic_vector(g_dat_w - 1 downto 0);
      wren_a : in std_logic;
      q_b : out std_logic_vector(g_dat_w - 1 downto 0)
  );
  end component;

  signal rdaddr : natural range 0 to g_nof_words - 1;
  signal wraddr : natural range 0 to g_nof_words - 1;

  signal out_q  : std_logic_vector(g_dat_w - 1 downto 0);
  signal reg_q  : std_logic_vector(g_dat_w - 1 downto 0);
begin
  assert g_rd_latency = 1 or g_rd_latency = 2
    report "ip_agi027_1e1v_ram_r_w : read latency must be 1 (default) or 2"
    severity FAILURE;

  gen_ip : if g_inferred = false generate
    u_altera_syncram : altera_syncram
    generic map (
            address_aclr_b  => "NONE",
            address_reg_b  => "CLOCK0",
            clock_enable_input_a  => "BYPASS",
            clock_enable_input_b  => "BYPASS",
            clock_enable_output_b  => "BYPASS",
            init_file  => g_init_file,
            -- enable_force_to_zero  => "FALSE",
            intended_device_family  => "Agilex7",
            lpm_type  => "altera_syncram",
            numwords_a  => g_nof_words,
            numwords_b  => g_nof_words,
            operation_mode  => "DUAL_PORT",
            outdata_aclr_b  => "NONE",
            -- outdata_sclr_b  => "NONE",
            outdata_reg_b  => c_outdata_reg_b,
            power_up_uninitialized  => "FALSE",
            -- read_during_write_mode_mixed_ports  => "DONT_CARE",
            widthad_a  => g_adr_w,
            widthad_b  => g_adr_w,
            width_a  => g_dat_w,
            width_b  => g_dat_w,
            width_byteena_a  => 1
    )
    port map (
        address_a => wraddress,
        address_b => rdaddress,
        clock0 => clk,
        data_a => data,
        wren_a => wren,
        q_b => q
    );
  end generate;

  gen_inferred : if g_inferred = true generate
    rdaddr <= to_integer(unsigned(rdaddress));
    wraddr <= to_integer(unsigned(wraddress));

    u_mem : entity work.ip_agi027_1e1v_simple_dual_port_ram_single_clock
    generic map (
      DATA_WIDTH => g_dat_w,
      ADDR_WIDTH => g_adr_w
    )
    port map (
      clk   => clk,
      raddr => rdaddr,
      waddr => wraddr,
      data  => data,
      we    => wren,
      q     => out_q
    );

    reg_q <= out_q when rising_edge(clk);

    q <= out_q when g_rd_latency = 1 else reg_q;
  end generate;
end SYN;
