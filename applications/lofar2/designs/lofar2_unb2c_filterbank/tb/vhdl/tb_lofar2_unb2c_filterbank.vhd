-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Self-checking testbench for simulating lofar2_unb2c_adc using WG data.
--
-- Description:
--   MM control actions:
--
--   1) Enable calc mode for WG via reg_diag_wg with:
--        freq = 19.921875MHz
--        ampl = 0.5 * 2**13
--
--   2) Read current BSN from reg_bsn_scheduler_wg and write reg_bsn_scheduler_wg
--      to trigger start of WG at BSN.
--
--   3) Read WG data via ram_aduh_mon into sp_sample and replay sp_sample for
--      analogue view in Wave window:
--
--   4) Read ADUH monitor power sum for via reg_aduh_mon and verify with
--      c_exp_wg_power_sp_0.
--      View sp_power_sum in Wave window
--   5) Read subband statistics (SST) via ram_st_sst and verify with
--      c_exp_subband_power_sp_0 at c_subband_sp_0.
--      View sp_subband_power_0  in Wave window
--
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   > run -a
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2c_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity tb_lofar2_unb2c_filterbank is
end tb_lofar2_unb2c_filterbank;

architecture tb of tb_lofar2_unb2c_filterbank is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_pps_period          : natural := 1000;

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C
  constant c_cable_delay         : time := 12 ns;

  constant c_nof_block_per_sync  : natural := 16;
  constant c_wpfb_sim            : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands, c_nof_block_per_sync);

  constant c_percentage          : real := 0.05;  -- percentage that actual value may differ from expected value
  constant c_lo_factor           : real := 1.0 - c_percentage;  -- lower boundary
  constant c_hi_factor           : real := 1.0 + c_percentage;  -- higher boundary

  -- WG
  constant c_full_scale_ampl      : real := real(2**(18 - 1) - 1);  -- = full scale of WG
  constant c_bsn_start_wg         : natural := 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  constant c_ampl_sp_0              : natural := 2**(c_sdp_W_adc - 1) / 2;  -- in number of lsb
  constant c_wg_subband_freq_unit : real := c_diag_wg_freq_unit / real(c_sdp_N_fft);  -- subband freq = Fs/1024 = 200 MSps/1024 = 195312.5 Hz sinus
  constant c_wg_freq_offset       : real := 0.0 / 11.0;  -- in freq_unit
  constant c_subband_sp_0           : real := 102.0;  -- Select subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
  constant c_wg_ampl_lsb          : real := c_diag_wg_ampl_unit / c_full_scale_ampl;  -- amplitude in number of LSbit resolution steps
  constant c_exp_wg_power_sp_0      : real := real(c_ampl_sp_0**2) / 2.0 * real(c_sdp_N_fft * c_nof_block_per_sync);

  -- WPFB
  constant c_nof_pfb                        : natural := 1;  -- Verifying 1 of c_sdp_P_pfb = 6 pfb to speed up simulation.
  constant c_wb_leakage_bin                 : natural := c_wpfb_sim.nof_points / c_wpfb_sim.wb_factor;  -- = 256, leakage will occur in this bin if FIR wb_factor is reversed
  constant c_exp_sp_subband_power_ratio     : real := 1.0 / 8.0;  -- depends on internal WPFB quantization and FIR coefficients
  constant c_exp_sp_subband_power_sum_ratio : real := c_exp_sp_subband_power_ratio;  -- because all sinus power is expected in one subband
  constant c_exp_subband_power_sp_0         : real := c_exp_wg_power_sp_0 * c_exp_sp_subband_power_ratio;

  type t_real_arr is array (integer range <>) of real;
  type t_slv_64_subbands_arr is array (integer range <>) of t_slv_64_arr(0 to c_sdp_N_sub);

  -- ADUH
  constant c_mon_buffer_nof_samples : natural := 512;  -- samples per stream
  constant c_mon_buffer_nof_words   : natural := c_mon_buffer_nof_samples;

  -- MM
  constant c_mm_file_reg_ppsh             : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "PIO_PPS";
  constant c_mm_file_reg_bsn_source       : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE";
  constant c_mm_file_reg_bsn_scheduler_wg : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_reg_aduh_mon         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_ADUH_MONITOR";
  constant c_mm_file_ram_aduh_mon         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_ADUH_MONITOR";
  constant c_mm_file_ram_st_sst           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_ST_SST";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0);

  -- WG
  signal dbg_c_exp_wg_power_sp_0 : real := c_exp_wg_power_sp_0;
  signal sp_samples              : t_integer_arr(0 to c_mon_buffer_nof_samples - 1) := (others => 0);
  signal sp_sample               : integer := 0;
  signal sp_power_sum            : std_logic_vector(63 downto 0);
  signal current_bsn_wg          : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- WPFB
  signal sp_subband_powers_arr2         : t_slv_64_subbands_arr(c_nof_pfb * c_nof_complex - 1 downto 0);  -- [sp][sub]
  signal sp_subband_power_0             : real;
  signal sp_subband_power_sum           : t_real_arr(c_nof_pfb * c_nof_complex - 1 downto 0) := (others => 0.0);
  signal sp_subband_power_sum_0         : real;
  signal sp_subband_power_ratio_0       : real;
  signal sp_subband_power_sum_ratio_0   : real;
  signal sp_subband_power_leakage_sum_0 : real;

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal ext_pps             : std_logic := '0';
  signal pps_rst             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal pmbus_scl           : std_logic;
  signal pmbus_sda           : std_logic;

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector((c_unb2c_board_tr_jesd204b.bus_w * c_unb2c_board_tr_jesd204b.nof_bus) - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref     : std_logic;
  signal jesd204b_sync_n     : std_logic_vector((c_unb2c_board_tr_jesd204b.nof_bus * c_unb2c_board_tr_jesd204b.bus_w) - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, ext_clk, pps);
  jesd204b_sysref <= pps;
  ext_pps <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2c_filterbank : entity work.lofar2_unb2c_filterbank
  generic map (
    g_design_name            => "lofar2_unb2c_filterbank_full",
    g_design_note            => "",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr,
    g_wpfb                   => c_wpfb_sim,
    g_scope_selected_subband => natural(c_subband_sp_0)
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- I2C Interface to Sensors
    SENS_SC      => sens_scl,
    SENS_SD      => sens_sda,

    PMBUS_SC     => pmbus_scl,
    PMBUS_SD     => pmbus_sda,
    PMBUS_ALERT  => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK      => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => jesd204b_sysref,
    JESD204B_SYNC_N => jesd204b_sync_n
  );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn                   : natural;
    variable v_sp_power_sum_0          : real;
    variable v_sp_subband_power      : real;
    variable v_W, v_T, v_U, v_S, v_B : natural;  -- array indicies
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    proc_common_wait_until_hi_lo(ext_clk, ext_pps);

    ----------------------------------------------------------------------------
    -- Enable BS
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source, 3,                    0, tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source, 2,                    0, tb_clk);  -- Init BSN = 0
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source, 1, c_nof_block_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source, 0,         16#00000003#, tb_clk);  -- Enable BS at PPS

    ----------------------------------------------------------------------------
    -- Enable WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 1, integer(  0.0 * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 2, integer((c_subband_sp_0 + c_wg_freq_offset) * c_wg_subband_freq_unit), tb_clk);  -- freq
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 3, integer(real(c_ampl_sp_0) * c_wg_ampl_lsb), tb_clk);  -- ampl

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;
    v_bsn := c_bsn_start_wg;
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, v_bsn, tb_clk);  -- first write low then high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,     0, tb_clk);  -- assume v_bsn < 2**31-1

    -- Wait for ADUH monitor to have filled with WG data
    wait for c_sdp_T_sub * c_sdp_N_taps;
    wait for c_sdp_T_sub * 2;

    ----------------------------------------------------------------------------
    -- WG data : read ADUH monitor buffer
    ----------------------------------------------------------------------------
    -- Wait for start of sync interval
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                            "UNSIGNED", rd_data, ">=", c_nof_block_per_sync * 2,  -- this is the wait until condition
                            c_sdp_T_sub, tb_clk);

    wait for c_sdp_T_sub;  -- ensure that one block of samples has filled the ADUH monitor buffer after the sync

    -- Read via MM
    for I in 0 to c_mon_buffer_nof_words - 1 loop
      mmf_mm_bus_rd(c_mm_file_ram_aduh_mon, I, rd_data, tb_clk);
      sp_samples(I) <= TO_SINT(rd_data(15 downto 0));
    end loop;

    -- Play to have waveform in time to allow viewing as analogue in the Wave Window
    for I in 0 to c_mon_buffer_nof_words - 1 loop
      proc_common_wait_some_cycles(ext_clk, 1);
      sp_sample <= sp_samples(I);
    end loop;

    wait for c_sdp_T_sub * 3;

    ---------------------------------------------------------------------------
    -- Read ADUH monitor power sum
    ---------------------------------------------------------------------------
    -- Wait for start of sync interval
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                            "UNSIGNED", rd_data, ">=", c_nof_block_per_sync * 3,  -- this is the wait until condition
                            c_sdp_T_sub, tb_clk);

    -- Read ADUH monitor power sum
    mmf_mm_bus_rd(c_mm_file_reg_aduh_mon, 2, rd_data, tb_clk);  -- read low part
    sp_power_sum(31 downto 0) <= rd_data;
    mmf_mm_bus_rd(c_mm_file_reg_aduh_mon, 3, rd_data, tb_clk);  -- read high part
    sp_power_sum(63 downto 32) <= rd_data;
    proc_common_wait_some_cycles(tb_clk, 1);

    ---------------------------------------------------------------------------
    -- Verify sp_power_sum
    ---------------------------------------------------------------------------

    -- Convert STD_LOGIC_VECTOR sp_power_sum to REAL
    v_sp_power_sum_0 := real(real(TO_UINT(sp_power_sum(61 downto 30))) * real(2**30) + real(TO_UINT(sp_power_sum(29 downto 0))));

    assert v_sp_power_sum_0 > c_lo_factor * c_exp_wg_power_sp_0
      report "Wrong SP power for SP 0"
      severity ERROR;
    assert v_sp_power_sum_0 < c_hi_factor * c_exp_wg_power_sp_0
      report "Wrong SP power for SP 0"
      severity ERROR;

    ---------------------------------------------------------------------------
    -- Read subband statistics
    ---------------------------------------------------------------------------
    -- . the subband statistics are c_wpfb_sim.stat_data_sz = 2 word power values.
    -- . there are c_sdp_N_sub = 512 subbands per signal path
    -- . one complex WPFB can process two real inputs A, B
    -- . the subbands are output alternately so A0 B0 A1 B1 ... A511 B511 for input A, B
    -- . the subband statistics multiple WPFB units appear in order in the ram_st_sst address map
    -- . the subband statistics are stored first lo word 0 then hi word 1

    for I in 0 to c_nof_pfb * c_nof_complex * c_sdp_N_sub * c_wpfb_sim.stat_data_sz - 1 loop
      v_W := I mod c_wpfb_sim.stat_data_sz;
      v_T := (I / c_wpfb_sim.stat_data_sz) mod c_nof_complex;
      v_U := I / (c_nof_complex * c_wpfb_sim.stat_data_sz * c_sdp_N_sub);
      v_S := v_T + v_U * c_nof_complex;
      v_B := (I / (c_nof_complex * c_wpfb_sim.stat_data_sz)) mod c_sdp_N_sub;
      if v_W = 0 then
        -- low part
        mmf_mm_bus_rd(c_mm_file_ram_st_sst, I, rd_data, tb_clk);
        sp_subband_powers_arr2(v_S)(v_B)(31 downto 0) <= rd_data;
      else
        -- high part
        mmf_mm_bus_rd(c_mm_file_ram_st_sst, I, rd_data, tb_clk);
        sp_subband_powers_arr2(v_S)(v_B)(63 downto 32) <= rd_data;

        -- Convert STD_LOGIC_VECTOR to REAL
        v_sp_subband_power := real(TO_UINT(rd_data(29 downto 0) &
            sp_subband_powers_arr2(v_S)(v_B)(31 downto 30))) * 2.0**30 +
            real(TO_UINT(sp_subband_powers_arr2(v_S)(v_B)(29 downto 0)));
        -- sum
        sp_subband_power_sum(v_S) <= sp_subband_power_sum(v_S) + v_sp_subband_power;
      end if;
    end loop;

    -- sp_subband_power_sum is the sum of all subband powers per SP, this value will be close to sp_subband_power
    -- because the input is a sinus, so most power will be in 1 subband. The sp_subband_power_leakage_sum shows
    -- how much power from the input sinus at a specific subband has leaked into the 511 other subbands.
    sp_subband_power_0 <= real(TO_UINT(sp_subband_powers_arr2(0)(integer(ROUND(c_subband_sp_0)))(61 downto 30))) * 2.0**30 +
        real(TO_UINT(sp_subband_powers_arr2(0)(integer(ROUND(c_subband_sp_0)))(29 downto 0)));

    sp_subband_power_sum_0 <= sp_subband_power_sum(0);

    proc_common_wait_some_cycles(tb_clk, 1);

    ---------------------------------------------------------------------------
    -- Verify subband statistics
    ---------------------------------------------------------------------------
    -- verify expected subband power based on WG power
    if v_sp_power_sum_0 > 0.0 then assert sp_subband_power_0 > c_lo_factor * c_exp_subband_power_sp_0 report "Wrong subband power for SP 0" severity ERROR; end if;
    if v_sp_power_sum_0 > 0.0 then assert sp_subband_power_0 < c_hi_factor * c_exp_subband_power_sp_0 report "Wrong subband power for SP 0" severity ERROR; end if;

    -- view c_exp_sp_subband_power_ratio in Wave window
    if v_sp_power_sum_0 > 0.0 then sp_subband_power_ratio_0 <= sp_subband_power_0 / v_sp_power_sum_0; end if;

    -- view c_exp_sp_subband_power_sum_ratio in Wave window
    -- The sp_subband_power_sum_ratio show similar information as sp_subband_power_leakage_sum, because when
    -- sp_subband_power_leakage_sum is small then sp_subband_power_sum_ratio ~= sp_subband_power_ratio.
    if v_sp_power_sum_0 > 0.0 then sp_subband_power_sum_ratio_0 <= sp_subband_power_sum_0 / v_sp_power_sum_0; end if;

    -- View sp_subband_power_leakage_sum in Wave window
    if v_sp_power_sum_0 > 0.0 then sp_subband_power_leakage_sum_0 <= sp_subband_power_sum_0 - sp_subband_power_0; end if;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;
end tb;
