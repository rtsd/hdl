-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : MMS for dp_bsn_source
-- Description: See dp_bsn_source.vhd

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_bsn_source is
  generic (
    g_cross_clock_domain     : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_block_size             : natural := 256;  -- 1024 samples @ 800M / 4 = 256 4 sample words @ 200 M
    g_nof_block_per_sync     : natural := 8;  -- 800M/1024 = 781250;
    g_bsn_w                  : natural := 48
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk            : in  std_logic;  -- memory-mapped bus clock
    dp_rst            : in  std_logic;  -- reset synchronous with st_clk
    dp_clk            : in  std_logic;  -- other clock domain clock
    dp_pps            : in  std_logic := '1';  -- external PPS in captured in dp_clk domain

    -- Memory-mapped clock domain
    reg_mosi          : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    reg_miso          : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- Streaming clock domain
    bs_sosi           : out t_dp_sosi
  );
end mms_dp_bsn_source;

architecture str of mms_dp_bsn_source is
  signal dp_on              : std_logic;
  signal dp_on_pps          : std_logic;
  signal nof_block_per_sync : std_logic_vector(c_word_w - 1 downto 0);
  signal init_bsn           : std_logic_vector(g_bsn_w - 1 downto 0);

  signal i_bs_sosi          : t_dp_sosi;

  signal dp_on_status       : std_logic;

  signal capture_bsn        : std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
begin
  bs_sosi <= i_bs_sosi;

  u_mm_reg : entity work.dp_bsn_source_reg
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_nof_block_per_sync => g_nof_block_per_sync
  )
  port map (
    -- Clocks and reset
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,
    st_rst                => dp_rst,
    st_clk                => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in                => reg_mosi,
    sla_out               => reg_miso,

    -- MM registers in st_clk domain
    st_on                 => dp_on,
    st_on_pps             => dp_on_pps,
    st_on_status          => dp_on_status,
    st_nof_block_per_sync => nof_block_per_sync,
    st_init_bsn           => init_bsn,
    st_current_bsn        => capture_bsn
  );

  u_bsn_source : entity work.dp_bsn_source
  generic map (
    g_block_size             => g_block_size,
    g_nof_block_per_sync     => g_nof_block_per_sync,
    g_bsn_w                  => g_bsn_w
  )
  port map (
    rst                => dp_rst,
    clk                => dp_clk,
    pps                => dp_pps,
    -- MM control
    dp_on              => dp_on,
    dp_on_pps          => dp_on_pps,
    dp_on_status       => dp_on_status,
    init_bsn           => init_bsn,
    nof_block_per_sync => nof_block_per_sync,
    -- Streaming
    src_out            => i_bs_sosi
  );

  --capture_bsn <= i_bs_sosi.bsn;                                                  -- capture current BSN
  --capture_bsn <= i_bs_sosi.bsn WHEN rising_edge(dp_clk) AND dp_pps='1';          -- capture BSN at external PPS
  capture_bsn <= i_bs_sosi.bsn when rising_edge(dp_clk) and i_bs_sosi.sync = '1';  -- capture BSN at internal sync
end str;
