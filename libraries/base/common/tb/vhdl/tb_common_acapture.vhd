-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_acapture
-- Usage:
--   > as 1
--   > run -all
--
-- Description:

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_acapture is
end tb_common_acapture;

architecture tb of tb_common_acapture is
  constant in_clk_period    : time := 10 ns;
  constant out_clk_period   : time := 7 ns;

  constant c_delay_len  : natural := 3;

  signal dbg_state                : natural;

  signal tb_end                   : std_logic := '0';
  signal in_rst                   : std_logic;
  signal in_clk                   : std_logic := '0';
  signal out_clk                  : std_logic := '0';

  signal in_dat                   : std_logic;
  signal out_cap                  : std_logic;
begin
  in_clk  <= not in_clk  or tb_end after in_clk_period / 2;
  out_clk <= not out_clk or tb_end after out_clk_period / 2;

  p_in_stimuli : process
  begin
    dbg_state <= 0;
    in_rst <= '1';
    in_dat <= '0';
    proc_common_wait_some_cycles(in_clk, 5);
    in_rst <= '0';
    for I in 0 to 4 loop
      proc_common_wait_some_cycles(in_clk, 9);
      in_dat <= '1';
      proc_common_wait_some_cycles(in_clk, 1);
      in_dat <= '0';
    end loop;

    dbg_state <= 9;
    proc_common_wait_some_cycles(in_clk, 10);
    tb_end <= '1';
    wait;
  end process;

  u_acapture : entity work.common_acapture
  generic map (
    g_rst_level => '0'
  )
  port map (
    in_rst  => in_rst,
    in_clk  => in_clk,
    in_dat  => in_dat,
    out_clk => out_clk,
    out_cap => out_cap
  );
end tb;
