-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Connect a single MM master interface to a list of MM slave
--          interfaces using a combinatorial muliplexer as bus.
-- Description:
-- * MM bus
--   The mm_bus_comb creates a memory mapped (MM) bus that connects read
--   and write accesses from the master interface to the addressed slave
--   interface. There is one master that controls the bus and there are
--   g_nof_slaves on the bus. Per slave the start address and address span
--   have to be specified via g_base_arr and g_width_arr.
--
-- * Slave allocation
--   The slaves have to be located on the bus such that the MSbits of the
--   global address can be used to select the slave and the LSbits of the
--   global address can directly be used to select the address within the
--   slave. Therefore:
--   . The width of a slave is the power of 2 that fits the address range of
--     the slave.
--   . The span of a slave is 2**width.
--   . The base address of a slave has to be a power of 2 multiple of the
--     slave span.
--
-- * The mm_clk is only used when there is a slave with read latency > 0, to
--   pipeline the slave_index_arr for the master_miso.rddata/rdval.
--   Typically a master will wait for the last rdval, before accessing
--   another slave port, so then it is not benecessary to pipeline the
--   slave_index_arr. However registering the slave_index_arr eases timing
--   closure on the miso part and will allow reading from different slave
--   ports without waiting, provided that both slaves have the same read
--   latency.
--
-- * Read latency
--   For read accesses a slave will typically have a read latency > 0, which
--   means that when the rd and address are active, then it takes read
--   latency number of clock cycles until the rddata becomes available. The
--   read latency can be specified per slave via g_rd_latency_arr.
--   The slave_index_arr is used to support that a new wr access or rd access
--   can already start, while a current rd access still has to finish with
--   a rdval. Without the slave_index_arr the master would have to wait with
--   a new rd or wr access to another slave until the read response from the
--   current slave has finished.
--                                          ________
--                                          | delay|
--   master_mosi.address[h:w] = index --+-->| line |--\
--                                      |   |______|  |
--                                      |             |
--                                      v             |
--    master_mosi --> slave_mosi_arr.wr[ ]----------------> slave_mosi_arr
--                                   rd               |
--                                                    v
--    master_miso <--------------------slave_miso_arr[ ]<-- slave_miso_arr
--
--
-- * No pipelining
--   The mm_bus_comb is combinatorial, so there is no pipelining between
--   the master interface and the slave interfaces. Use mm_bus_pipe to add
--   pipelining.
--
-- Usage:
--   See mm_bus.vhd.
--
-- Limitations:
-- * A limitation is that if one slave has a read latency of 2 and another
--   slave has a read latency of 1 then it is not possible to access them
--   without a gap of 1 mm_clk cycle, because the rdval will then be active
--   simultaneously from both slaves. Therefore the master can only use
--   random read access between slaves if all slaves have the same read
--   latency. For slaves that have larger read latency the master must
--   insert an gap, before it can read a slave that has less read latency.
--   An alternative workaround would be to use the same read latency for all
--   slaves on the bus, by pipelining the miso.rd, rddata for MM slaves that
--   have a smaller read latency.
--
-- Remarks:
-- . The mm_bus_comb resembles common_mem_mux, but the difference is that
--   with common_mem_mux all slaves have the same address range and are
--   spaced without address gaps. It is possible to use common_mem_mux in
--   series with mm_bus_comb to provide hierarchy by reprensenting an array
--   of slave ports via a single slave port on the MM bus.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity mm_bus_comb is
  generic (
    g_nof_slaves          : positive;  -- Number of MM slave interfaces on the bus
    g_base_arr            : t_nat_natural_arr;  -- Address base per slave
    g_width_arr           : t_nat_natural_arr;  -- Address width per slave
    g_rd_latency_arr      : t_nat_natural_arr  -- Read latency per slave
  );
  port (
    mm_clk         : in  std_logic := '0';
    master_mosi    : in  t_mem_mosi;
    master_miso    : out t_mem_miso;
    slave_mosi_arr : out t_mem_mosi_arr(0 to g_nof_slaves - 1);
    slave_miso_arr : in  t_mem_miso_arr(0 to g_nof_slaves - 1) := (others => c_mem_miso_rst)
  );
end mm_bus_comb;

architecture rtl of mm_bus_comb is
  -- Determine the address range of all slaves on the MM bus.
  function func_derive_mm_bus_addr_w(g_base_arr, g_width_arr : t_nat_natural_arr) return natural is
    variable v_base            : natural := 0;
    variable v_width           : natural;
    variable v_mm_bus_addr_max : natural;
  begin
    for I in g_base_arr'range loop
      if g_base_arr(I) > v_base then
        v_base  := g_base_arr(I);
        v_width := g_width_arr(I);
      end if;
    end loop;
    -- Largest base address + the width of the slave at this address - 1. The
    -- -1 is because the addresses count from 0 to N-1.
    v_mm_bus_addr_max := v_base + 2**v_width - 1;
    -- Return number of bits to represent the largest address that will be used
    -- on the MM bus
    return ceil_log2(v_mm_bus_addr_max);
  end;

  constant c_mm_bus_addr_w       : natural := func_derive_mm_bus_addr_w(g_base_arr, g_width_arr);
  constant c_rd_latency_max      : natural := largest(g_rd_latency_arr);

  signal slave_index_arr     : t_nat_natural_arr(0 to c_rd_latency_max) := (others => 0);
begin
  gen_single : if g_nof_slaves = 1 generate
    slave_mosi_arr(0) <= master_mosi;
    master_miso       <= slave_miso_arr(0);
  end generate;

  gen_multiple : if g_nof_slaves > 1 generate
    -- Detect which slave in the array is addressed
    p_index : process(master_mosi)
      variable v_base : natural;
    begin
      slave_index_arr(0) <= g_nof_slaves;  -- default index of none existing slave
      for I in 0 to g_nof_slaves - 1 loop
        v_base := TO_UINT(master_mosi.address(c_mm_bus_addr_w - 1 downto g_width_arr(I)));
        assert g_base_arr(I) mod 2**g_width_arr(I) = 0
          report "Slave base address must be a multiple of the slave width."
          severity FAILURE;
        if v_base = g_base_arr(I) / 2**g_width_arr(I) then
          slave_index_arr(0) <= I;  -- return index of addressed slave
          exit;  -- Found addressed slave, no need to loop further. EXIT is
                 -- not realy needed, because there can only be one
                 -- addressed slave so loop further will not change the index.
        end if;
      end loop;
    end process;

    slave_index_arr(1 to c_rd_latency_max) <= slave_index_arr(0 to c_rd_latency_max - 1) when rising_edge(mm_clk);

    -- Master access, can be write or read
    p_slave_mosi_arr : process(master_mosi, slave_index_arr)
    begin
      slave_mosi_arr <= (others => master_mosi);  -- default assign to all, to avoid latches
      for I in 0 to g_nof_slaves - 1 loop
        slave_mosi_arr(I).rd <= '0';
        slave_mosi_arr(I).wr <= '0';
        if I = slave_index_arr(0) then  -- check index for read or write access
          slave_mosi_arr(I).rd <= master_mosi.rd;
          slave_mosi_arr(I).wr <= master_mosi.wr;
        end if;
      end loop;
    end process;

    -- Slave response to read access after read latency mm_clk cycles
    p_master_miso : process(slave_miso_arr, slave_index_arr)
      variable v_rd_latency : natural;
    begin
      master_miso <= c_mem_miso_rst;  -- default clear, to avoid latches
      for I in 0 to g_nof_slaves - 1 loop
        v_rd_latency := g_rd_latency_arr(I);
        if I = slave_index_arr(v_rd_latency) then  -- check index for read response
          master_miso <= slave_miso_arr(I);
        end if;
      end loop;
      for I in 0 to g_nof_slaves - 1 loop
        if I = slave_index_arr(0) then  -- check index for waitrequest
          master_miso.waitrequest <= slave_miso_arr(I).waitrequest;
        end if;
      end loop;
    end process;

  end generate;
end rtl;
