-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- AUthor: E. Kooistra
-- Purpose: Test bench for eth_stream_udp using eth_tester
-- Description:
--   Similar as tb_eth_tester.vhd, but for only one stream and using streaming
--   interface loop back.
--
-- Usage:
-- > as 8
-- > run -a
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+ETH+tester+unit+for+1GbE

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use work.eth_pkg.all;
use work.eth_tester_pkg.all;
use work.tb_eth_tester_pkg.all;

entity tb_eth_stream_udp is
  generic (
    g_tb_index         : natural := 0;  -- use to incremental delay logging from tb instances in tb_tb
    g_nof_sync         : natural := 2;  -- number of BG sync intervals to set c_run_time
    g_udp_port_match   : boolean := true;

    -- t_diag_block_gen_integer =
    --   sl:  enable
    --   sl:  enable_sync
    --   nat: samples_per_packet
    --   nat: blocks_per_sync
    --   nat: gapsize
    --   nat: mem_low_adrs
    --   nat: mem_high_adrs
    --   nat: bsn_init
    g_bg_ctrl    : t_diag_block_gen_integer := ('1', '1', 50, 3, 200, 0, c_diag_bg_mem_max_adr, 0)  -- for first stream
  );
end tb_eth_stream_udp;

architecture tb of tb_eth_stream_udp is
  constant c_tb_str                : string := "tb-" & natural'image(g_tb_index) & " : ";  -- use to distinguish logging from tb instances in tb_tb
  constant mm_clk_period           : time := 10 ns;  -- 100 MHz
  constant c_nof_st_clk_per_s      : natural := 200 * 10**6;
  constant st_clk_period           : time :=  (10**9 / c_nof_st_clk_per_s) * 1 ns;  -- 5 ns, 200 MHz

  constant c_bg_block_len    : natural := g_bg_ctrl.samples_per_packet;
  constant c_bg_slot_len     : natural := c_bg_block_len + g_bg_ctrl.gapsize;
  constant c_eth_packet_len  : natural := func_eth_tester_eth_packet_length(c_bg_block_len);

  -- Use REAL to avoid NATURAL overflow in bps calculation
  constant c_bg_nof_bps      : real := real(c_bg_block_len * c_octet_w) * real(c_nof_st_clk_per_s) / real(c_bg_slot_len);
  constant c_bg_sync_period  : natural := c_bg_slot_len * g_bg_ctrl.blocks_per_sync;

  constant c_run_time        : natural := g_nof_sync * c_bg_sync_period;
  constant c_nof_sync        : natural := c_run_time / c_bg_sync_period;

  -- Destination UDP port
  constant c_rx_udp_port     : natural := TO_UINT(c_eth_tester_udp_dst_port);
  constant c_dst_udp_port    : natural := sel_a_b(g_udp_port_match, c_rx_udp_port, 17);

  -- Expected Tx --> Rx latency values obtained from a tb run
  constant c_tx_exp_latency          : natural := 0;
  constant c_rx_exp_latency_en       : boolean := c_bg_block_len >= 50;
  constant c_rx_exp_latency_st       : natural := sel_a_b(g_udp_port_match, 58, 0);

  constant c_nof_valid_per_packet    : natural := c_bg_block_len;

  constant c_total_count_nof_valid_per_sync  : natural := g_bg_ctrl.blocks_per_sync * c_nof_valid_per_packet;

  constant c_mon_nof_sop_tx    : natural := g_bg_ctrl.blocks_per_sync;
  constant c_mon_nof_sop_rx    : natural := sel_a_b(g_udp_port_match, c_mon_nof_sop_tx, 0);
  constant c_mon_nof_valid_tx  : natural := c_mon_nof_sop_tx * ceil_div(c_bg_block_len * c_octet_w, c_word_w);
  constant c_mon_nof_valid_rx  : natural := c_mon_nof_sop_rx * c_nof_valid_per_packet;

  -- Use sim default src MAC, IP, UDP port from eth_tester_pkg.vhd and based on c_gn_index
  constant c_gn_index           : natural := 17;  -- global node index
  constant c_gn_eth_src_mac     : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0) := c_eth_tester_eth_src_mac_47_16 & func_eth_tester_gn_index_to_mac_15_0(c_gn_index);
  constant c_gn_ip_src_addr     : std_logic_vector(c_network_ip_addr_w - 1 downto 0) := c_eth_tester_ip_src_addr_31_16 & func_eth_tester_gn_index_to_ip_15_0(c_gn_index);
  constant c_gn_udp_src_port    : std_logic_vector(c_network_udp_port_w - 1 downto 0) := c_eth_tester_udp_src_port_15_8 & TO_UVEC(c_gn_index, 8);

  -- Clocks and reset
  signal mm_rst              : std_logic := '1';
  signal mm_clk              : std_logic := '1';
  signal st_rst              : std_logic := '1';
  signal st_clk              : std_logic := '1';
  signal st_pps              : std_logic := '0';
  signal stimuli_end         : std_logic := '0';
  signal tb_end              : std_logic := '0';

  signal tx_fifo_rd_emp      : std_logic;

  -- ETH UDP data path interface
  signal tx_udp_sosi         : t_dp_sosi;
  signal tx_udp_siso         : t_dp_siso := c_dp_siso_rdy;
  signal rx_udp_sosi         : t_dp_sosi;

  -- MM interface
  -- . Tx
  signal reg_bg_ctrl_copi                : t_mem_copi := c_mem_copi_rst;
  signal reg_bg_ctrl_cipo                : t_mem_cipo;
  signal reg_hdr_dat_copi                : t_mem_copi := c_mem_copi_rst;
  signal reg_hdr_dat_cipo                : t_mem_cipo;
  signal reg_bsn_monitor_v2_tx_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_tx_cipo      : t_mem_cipo;
  signal reg_strobe_total_count_tx_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_strobe_total_count_tx_cipo  : t_mem_cipo;
  -- . Rx
  signal reg_bsn_monitor_v2_rx_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_rx_cipo      : t_mem_cipo;
  signal reg_strobe_total_count_rx_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_strobe_total_count_rx_cipo  : t_mem_cipo;

  -- . reg_strobe_total_count
  signal tx_total_count_nof_packet       : natural;
  signal rx_total_count_nof_packet       : natural;
  signal tx_exp_total_count_nof_packet   : natural;
  signal rx_exp_total_count_nof_packet   : natural;

  signal rx_total_count_nof_valid        : natural;
  signal rx_exp_total_count_nof_valid    : natural;

  -- . reg_bsn_monitor_v2
  signal tx_mon_nof_sop      : natural;
  signal tx_mon_nof_valid    : natural;
  signal tx_mon_latency      : natural;
  signal rx_mon_nof_sop      : natural;
  signal rx_mon_nof_valid    : natural;
  signal rx_mon_latency      : natural;

  -- ETH stream
  signal tse_tx_sosi         : t_dp_sosi;
  signal tse_tx_siso         : t_dp_siso;
  signal tse_rx_sosi         : t_dp_sosi;
  signal tse_rx_siso         : t_dp_siso;

  -- View in Wave window
  signal dbg_g_bg_ctrl            : t_diag_block_gen_integer := g_bg_ctrl;
  signal dbg_c_run_time           : natural := c_run_time;
  signal dbg_c_mon_nof_sop_tx     : natural := c_mon_nof_sop_tx;
  signal dbg_c_mon_nof_sop_rx     : natural := c_mon_nof_sop_rx;
  signal dbg_c_mon_nof_valid_tx   : natural := c_mon_nof_valid_tx;
  signal dbg_c_mon_nof_valid_rx   : natural := c_mon_nof_valid_rx;
begin
  mm_clk <= (not mm_clk) or tb_end after mm_clk_period / 2;
  st_clk <= (not st_clk) or tb_end after st_clk_period / 2;
  mm_rst <= '1', '0' after mm_clk_period * 5;
  st_rst <= '1', '0' after st_clk_period * 5;

  tx_exp_total_count_nof_packet <= c_nof_sync * g_bg_ctrl.blocks_per_sync;
  rx_exp_total_count_nof_packet <= sel_a_b(g_udp_port_match, tx_exp_total_count_nof_packet, 0);

  rx_exp_total_count_nof_valid <= sel_a_b(g_udp_port_match, c_nof_sync * c_total_count_nof_valid_per_sync, 0);

  -----------------------------------------------------------------------------
  -- MM control and monitoring
  -----------------------------------------------------------------------------
  p_mm : process
  begin
    tb_end <= '0';

    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    ---------------------------------------------------------------------------
    -- Rx UDP offload port
    ---------------------------------------------------------------------------
    -- Set destination MAC/IP/UDP port in tx header
    -- The MM addresses follow from byte address_offset // 4 in eth.peripheral.yaml
    proc_mem_mm_bus_wr(16#7#, c_dst_udp_port, mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    proc_mem_mm_bus_wr(16#10#, TO_SINT(c_eth_tester_ip_dst_addr), mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);  -- use signed to fit 32 b in INTEGER
    proc_mem_mm_bus_wr(16#18#, TO_SINT(c_eth_tester_eth_dst_mac(31 downto 0)), mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);  -- use signed to fit 32 b in INTEGER
    proc_mem_mm_bus_wr(16#19#, TO_UINT(c_eth_tester_eth_dst_mac(47 downto 32)), mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);

    ---------------------------------------------------------------------------
    -- Stimuli
    ---------------------------------------------------------------------------
    -- Prepare the BG
    proc_mem_mm_bus_wr(1, g_bg_ctrl.samples_per_packet, mm_clk, reg_bg_ctrl_copi);
    proc_mem_mm_bus_wr(2, g_bg_ctrl.blocks_per_sync,    mm_clk, reg_bg_ctrl_copi);
    proc_mem_mm_bus_wr(3, g_bg_ctrl.gapsize,            mm_clk, reg_bg_ctrl_copi);
    proc_mem_mm_bus_wr(4, g_bg_ctrl.mem_low_adrs,       mm_clk, reg_bg_ctrl_copi);
    proc_mem_mm_bus_wr(5, g_bg_ctrl.mem_high_adrs,      mm_clk, reg_bg_ctrl_copi);
    proc_mem_mm_bus_wr(6, g_bg_ctrl.bsn_init,           mm_clk, reg_bg_ctrl_copi);  -- low part
    proc_mem_mm_bus_wr(7, 0,                            mm_clk, reg_bg_ctrl_copi);  -- high part
    -- Enable the BG at st_pps pulse.
    proc_mem_mm_bus_wr(0, 3, mm_clk, reg_bg_ctrl_copi);
    proc_common_wait_some_cycles(mm_clk, 10);
    -- Issue an st_pps pulse to start the enabled BG
    proc_common_gen_pulse(st_clk, st_pps);

    -- Run test
    proc_common_wait_some_cycles(st_clk, c_run_time);

    -- Disable the BG
    proc_mem_mm_bus_wr(0, 0, mm_clk, reg_bg_ctrl_copi);

    -- Wait until Tx FIFO has emptied for the stream
    while tx_fifo_rd_emp /= '1' loop
      proc_common_wait_some_cycles(st_clk, 1);
    end loop;
    proc_common_wait_some_cycles(st_clk, c_bg_sync_period);
    stimuli_end <= '1';

    -- Delay logging between different tb instances
    proc_common_wait_some_cycles(st_clk, g_tb_index * 100);

    -- Print logging
    print_str("");  -- log empty line between tb results
    print_str(c_tb_str &
        "ETH bit rate :" &
        " c_bg_nof_bps = " & real'image(c_bg_nof_bps) & " bps");
    assert c_bg_nof_bps < 10.0**9
      report "Tx flow control will keep ETH bitrate < 1Gbps."
      severity NOTE;

    -------------------------------------------------------------------------
    -- Verification: Total counts
    -------------------------------------------------------------------------
    -- . read low part, ignore high part (= 0) of two word total counts
    -- Tx total nof packets
    proc_mem_mm_bus_rd(0, mm_clk, reg_strobe_total_count_tx_cipo, reg_strobe_total_count_tx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    tx_total_count_nof_packet <= TO_UINT(reg_strobe_total_count_tx_cipo.rddata(c_word_w - 1 downto 0));
    -- Rx total nof packets
    proc_mem_mm_bus_rd(0, mm_clk, reg_strobe_total_count_rx_cipo, reg_strobe_total_count_rx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rx_total_count_nof_packet <= TO_UINT(reg_strobe_total_count_rx_cipo.rddata(c_word_w - 1 downto 0));
    -- Rx total nof valids
    proc_mem_mm_bus_rd(2, mm_clk, reg_strobe_total_count_rx_cipo, reg_strobe_total_count_rx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rx_total_count_nof_valid <= TO_UINT(reg_strobe_total_count_rx_cipo.rddata(c_word_w - 1 downto 0));
    proc_common_wait_some_cycles(mm_clk, 1);

    -- Print logging
    print_str(c_tb_str &
        "Tx total counts monitor :" &
        " nof_packet = " & natural'image(tx_total_count_nof_packet));

    print_str(c_tb_str &
        "Rx total counts monitor :" &
        " nof_packet = " & natural'image(rx_total_count_nof_packet) &
        ", nof_valid  = " & natural'image(rx_total_count_nof_valid));

    -- Verify, only log when wrong
    if c_bg_nof_bps < 10.0**9 then
      assert tx_total_count_nof_packet = tx_exp_total_count_nof_packet
        report c_tb_str &
               "Wrong Tx total nof packets count, Tx count = " & natural'image(tx_total_count_nof_packet) &
               " /= " & natural'image(tx_exp_total_count_nof_packet) &
               " = Expected count"
        severity ERROR;

      assert rx_total_count_nof_packet = rx_exp_total_count_nof_packet
        report c_tb_str &
               "Wrong Rx total nof packets count, Rx count = " & natural'image(rx_total_count_nof_packet) &
               " /= " & natural'image(rx_exp_total_count_nof_packet) &
               " = Expected count"
        severity ERROR;

      assert rx_total_count_nof_valid = rx_exp_total_count_nof_valid
        report c_tb_str &
               "Wrong Rx total nof valids count, Rx count = " & natural'image(rx_total_count_nof_valid) &
               " /= " & natural'image(rx_exp_total_count_nof_valid) &
               " = Expected count"
        severity ERROR;
    end if;

    -------------------------------------------------------------------------
    -- Verification: BSN monitors (yield same values in every sync interval)
    -------------------------------------------------------------------------
    -- 3 = nof_sop
    -- 4 = nof_valid
    -- 6 = latency
    -- . Tx
    proc_mem_mm_bus_rd(3, mm_clk, reg_bsn_monitor_v2_tx_cipo, reg_bsn_monitor_v2_tx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    tx_mon_nof_sop <= TO_UINT(reg_bsn_monitor_v2_tx_cipo.rddata(c_word_w - 1 downto 0));
    proc_mem_mm_bus_rd(4, mm_clk, reg_bsn_monitor_v2_tx_cipo, reg_bsn_monitor_v2_tx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    tx_mon_nof_valid <= TO_UINT(reg_bsn_monitor_v2_tx_cipo.rddata(c_word_w - 1 downto 0));
    proc_mem_mm_bus_rd(6, mm_clk, reg_bsn_monitor_v2_tx_cipo, reg_bsn_monitor_v2_tx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    tx_mon_latency <= TO_UINT(reg_bsn_monitor_v2_tx_cipo.rddata(c_word_w - 1 downto 0));
    -- . Rx
    proc_mem_mm_bus_rd(3, mm_clk, reg_bsn_monitor_v2_rx_cipo, reg_bsn_monitor_v2_rx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rx_mon_nof_sop <= TO_UINT(reg_bsn_monitor_v2_rx_cipo.rddata(c_word_w - 1 downto 0));
    proc_mem_mm_bus_rd(4, mm_clk, reg_bsn_monitor_v2_rx_cipo, reg_bsn_monitor_v2_rx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rx_mon_nof_valid <= TO_UINT(reg_bsn_monitor_v2_rx_cipo.rddata(c_word_w - 1 downto 0));
    proc_mem_mm_bus_rd(6, mm_clk, reg_bsn_monitor_v2_rx_cipo, reg_bsn_monitor_v2_rx_copi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rx_mon_latency <= TO_UINT(reg_bsn_monitor_v2_rx_cipo.rddata(c_word_w - 1 downto 0));
    proc_common_wait_some_cycles(mm_clk, 1);

    -- Print logging
    print_str(c_tb_str &
        "Tx BSN monitor :" &
        " nof_sop = " & natural'image(tx_mon_nof_sop) &
        ", nof_valid = " & natural'image(tx_mon_nof_valid) &
        ", latency = " & natural'image(tx_mon_latency));

    print_str(c_tb_str &
        "Rx BSN monitor :" &
        " nof_sop = " & natural'image(rx_mon_nof_sop) &
        ", nof_valid = " & natural'image(rx_mon_nof_valid) &
        ", latency = " & natural'image(rx_mon_latency));

    if c_bg_nof_bps < 10.0**9 then
      -- Verify BSN monitors only when the BG sync interval is stable, so
      -- the ETH data rate < 1 Gbps and no BG block flow control.
      -- Verify, only log when wrong
      assert tx_mon_nof_sop = c_mon_nof_sop_tx
        report c_tb_str & "Wrong tx nof_sop"
        severity ERROR;
      assert rx_mon_nof_sop = c_mon_nof_sop_rx
        report c_tb_str & "Wrong rx nof_sop"
        severity ERROR;
      assert tx_mon_nof_valid = c_mon_nof_valid_tx
        report c_tb_str & "Wrong tx nof_valid"
        severity ERROR;
      assert rx_mon_nof_valid = c_mon_nof_valid_rx
        report c_tb_str & "Wrong rx nof_valid"
        severity ERROR;
      assert tx_mon_latency = c_tx_exp_latency
        report c_tb_str & "Wrong tx latency"
        severity ERROR;

      -- For short block lengths the Rx latency appears to become less, the
      -- exact Rx latency is therefore hard to predetermine. The actual
      -- latency is not critical, therefore it is sufficient to only very
      -- the latency when it is more or less fixed.
      if c_rx_exp_latency_en then
        assert almost_equal(rx_mon_latency, c_rx_exp_latency_st, 0)
          report c_tb_str & "Wrong rx latency using st interface"
          severity ERROR;
      end if;
    end if;

    -------------------------------------------------------------------------
    -- End of test
    -------------------------------------------------------------------------
    proc_common_wait_some_cycles(mm_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  u_eth_tester : entity work.eth_tester
  generic map (
    g_nof_streams      => 1,
    g_bg_sync_timeout  => c_eth_tester_sync_timeout,
    g_remove_crc       => false  -- no CRC with streaming loopback
  )
  port map (
    -- Clocks and reset
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,
    st_rst             => st_rst,
    st_clk             => st_clk,
    st_pps             => st_pps,

    -- UDP transmit interface
    eth_src_mac        => c_gn_eth_src_mac,
    ip_src_addr        => c_gn_ip_src_addr,
    udp_src_port       => c_gn_udp_src_port,

    sl(tx_fifo_rd_emp_arr) => tx_fifo_rd_emp,

    TO_DP_ONE(tx_udp_sosi_arr) => tx_udp_sosi,
    tx_udp_siso_arr    => TO_DP_ARR(tx_udp_siso),

    -- UDP receive interface
    rx_udp_sosi_arr    => TO_DP_ARR(rx_udp_sosi),

    -- Memory Mapped Slaves (one per stream)
    -- . Tx
    reg_bg_ctrl_copi               => reg_bg_ctrl_copi,
    reg_bg_ctrl_cipo               => reg_bg_ctrl_cipo,
    reg_hdr_dat_copi               => reg_hdr_dat_copi,
    reg_hdr_dat_cipo               => reg_hdr_dat_cipo,
    reg_bsn_monitor_v2_tx_copi     => reg_bsn_monitor_v2_tx_copi,
    reg_bsn_monitor_v2_tx_cipo     => reg_bsn_monitor_v2_tx_cipo,
    reg_strobe_total_count_tx_copi => reg_strobe_total_count_tx_copi,
    reg_strobe_total_count_tx_cipo => reg_strobe_total_count_tx_cipo,
    -- . Rx
    reg_bsn_monitor_v2_rx_copi     => reg_bsn_monitor_v2_rx_copi,
    reg_bsn_monitor_v2_rx_cipo     => reg_bsn_monitor_v2_rx_cipo,
    reg_strobe_total_count_rx_copi => reg_strobe_total_count_rx_copi,
    reg_strobe_total_count_rx_cipo => reg_strobe_total_count_rx_cipo
  );

  -- ETH stream
  u_dut : entity work.eth_stream_udp
  generic map (
    g_rx_udp_port => c_rx_udp_port
  )
  port map (
    -- Clocks and reset
    st_rst        => st_rst,
    st_clk        => st_clk,

    -- User UDP interface
    -- . Tx
    udp_tx_sosi   => tx_udp_sosi,
    udp_tx_siso   => tx_udp_siso,
    -- . Rx
    udp_rx_sosi   => rx_udp_sosi,
    udp_rx_siso   => c_dp_siso_rdy,

    -- PHY interface
    -- . Tx
    tse_tx_sosi   => tse_tx_sosi,
    tse_tx_siso   => tse_tx_siso,
    -- . Rx
    tse_rx_sosi   => tse_rx_sosi,
    tse_rx_siso   => tse_rx_siso
  );

  -- Loopback wire Tx to Rx, register to increasy ready latency from 1 to 2
  tse_rx_sosi <= tse_tx_sosi when rising_edge(st_clk);
  tse_tx_siso <= tse_rx_siso;
end tb;
