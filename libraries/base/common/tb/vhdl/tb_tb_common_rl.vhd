-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity tb_tb_common_rl is
end tb_tb_common_rl;

architecture tb of tb_tb_common_rl is
  constant c_nof_blocks  : natural := 1000;
  constant c_fifo_size   : natural := 64;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Usage:
  -- > as 2
  -- > run -all

  -- g_nof_blocks              : NATURAL := 1000;   -- nof blocks to simulate
  -- g_random_enable           : BOOLEAN := TRUE;   -- use TRUE for random input enable flow control
  -- g_random_ready            : BOOLEAN := TRUE;   -- use TRUE for random output ready flow control
  -- g_fifo_size               : NATURAL := 1024;
  -- g_rl_decrease_en          : BOOLEAN := FALSE;
  -- g_rl_increase_en          : BOOLEAN := FALSE
  -- g_rl_increase_hold_dat_en : BOOLEAN := TRUE

  u_random_fifo_sc_decr_0_incr_1_f  : entity work.tb_common_rl generic map (c_nof_blocks, true, true, c_fifo_size,  true,  true, false);
  u_random_fifo_sc_decr_0_incr_1_t  : entity work.tb_common_rl generic map (c_nof_blocks, true, true, c_fifo_size,  true,  true,  true);
  u_random_fifo_sc_decr_0           : entity work.tb_common_rl generic map (c_nof_blocks, true, true, c_fifo_size,  true, false, false);
  u_random_fifo_sc                  : entity work.tb_common_rl generic map (c_nof_blocks, true, true, c_fifo_size, false, false,  true);
end tb;
