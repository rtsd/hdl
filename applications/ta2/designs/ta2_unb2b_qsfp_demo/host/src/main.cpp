/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Reinier vd Walle
* Purpose:
* . Test the ta2_unb2b_qsfp_demo OpenCL application in emulator
* Description:
* . Run: -> make ta2_unb2b_qsfp_demo
* . Navigate to -> cd $HDL_WORK/unb2b/OpenCL/ta2_unb2b_qsfp_demo/bin
* . Execute -> CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 ./host
* *********************************************************************** */
#include <CL/cl_ext_intelfpga.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "common.h"

using namespace std;
int main(int argc, char **argv)
{
    if (argc > 2) {
        cerr << "usage: " << argv[0] << " [ta2_unb2b_qsfp_demo.aocx]" << endl;
        exit(1);
    }

    // Initialize OpenCL
    cl::Context context;
    vector<cl::Device> devices;
    init(context, devices);
    cl::Device &device = devices[0];

    // Get program
    string filename_bin = string(argc == 2 ? argv[1] : "ta2_unb2b_qsfp_demo.aocx");
    cl::Program program = get_program(context, device, filename_bin);


    // Setup command queues
    vector<cl::CommandQueue> queues(2);

    for (cl::CommandQueue &queue : queues) {
        queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);
    }

    cl::Event computeDoneA;
    cl::Event computeDoneB;

    // Setup FPGA kernels
    cl::Kernel writer40gbeKernel(program, "writer_40gbe");
    cl::Kernel writer10gbeKernel(program, "writer_10gbe");

    // Run FPGA kernels
    clog << ">>> Run fpga" << endl;
    try {
        queues[0].enqueueTask(writer40gbeKernel, nullptr, &computeDoneA);
        queues[1].enqueueTask(writer10gbeKernel, nullptr, &computeDoneB);


        computeDoneA.wait();
        cl_ulong start = computeDoneA.getProfilingInfo<CL_PROFILING_COMMAND_START>();
        cl_ulong stop  = computeDoneA.getProfilingInfo<CL_PROFILING_COMMAND_END>();
    
        double milliseconds = (stop - start) / 1e6;
        cout << "runtime 40GbE= " << milliseconds << " ms, " << endl;
    
        computeDoneB.wait();
        start = computeDoneB.getProfilingInfo<CL_PROFILING_COMMAND_START>();
        stop  = computeDoneB.getProfilingInfo<CL_PROFILING_COMMAND_END>();
    
        milliseconds = (stop - start) / 1e6;
        cout << "runtime 10GbE= " << milliseconds << " ms, " << endl;

    } catch (cl::Error &error) {
        cerr << "Error launching kernel: " << error.what() << endl;
        exit(EXIT_FAILURE);
    }


// process 40 GbE output, removing the flag data
    const string inputFileA = "kernel_output_40GbE";

   ifstream fileA(inputFileA);
   ostringstream ssA;
   ssA << fileA.rdbuf();
   const string& sA = ssA.str();
   vector<char> vecA(sA.begin(), sA.end());

   // remove flag data
    for (size_t i=32; i<vecA.size(); i+=32){
      vecA.erase(vecA.begin()+i);
    }

// process 10GbE output, removing the flag data
    const string inputFileB = "kernel_output_10GbE";

   ifstream fileB(inputFileB);
   ostringstream ssB;
   ssB << fileB.rdbuf();
   const string& sB = ssB.str();
   vector<char> vecB(sB.begin(), sB.end());

   // remove flag data
    for (size_t i=8; i<vecB.size(); i+=8){
      vecB.erase(vecB.begin()+i);
    }

// Verify that both outputs are the same
    if (vecA == vecB)
    {
      cout << "PASSED" << endl;   
    } else {
      cout << "FAILED: Data from QSFP outputs do not match!" << endl;
    }

// Write clean outputs to file
#if 0
    ofstream output_fileA("clean_kernel_output_40GbE.txt");
    ostream_iterator<char> output_iteratorA(output_fileA, "");
    copy(vecA.begin(), vecA.end(), output_iteratorA);

    ofstream output_fileB("clean_kernel_output_10GbE.txt");
    ostream_iterator<char> output_iteratorB(output_fileB, "");
    copy(vecB.begin(), vecB.end(), output_iteratorB);
#endif

    return EXIT_SUCCESS;
}
