-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, technology_lib, diag_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity unb2b_jesd is
  generic (
    g_design_name       : string  := "unb2b_jesd";
    g_design_note       : string  := "UNUSED";
    g_technology        : natural := c_tech_arria10_e1sg;
    g_sim               : boolean := false;  -- Overridden by TB
    g_sim_unb_nr        : natural := 0;
    g_sim_node_nr       : natural := 0;
    g_stamp_date        : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time        : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn         : natural := 0;  -- SVN revision    -- set by QSF
    g_factory_image     : boolean := false;
    g_protect_addr_range: boolean := false
  );
  port (
    -- GENERAL
--    CLK          : IN    STD_LOGIC; -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

    QSFP_LED     : out   std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0);

    -- JESD signals
    jesd204_rx_serial_data  : in std_logic;
    jesd204_sync_n_out      : out std_logic;
    jesd204_rx_sysref       : in std_logic;
    jesd204_device_clk      : in std_logic
  );
end unb2b_jesd;

architecture str of unb2b_jesd is
  -- Firmware version x.y
  constant c_fw_version             : t_unb2b_board_fw_version := (1, 1);
  constant c_mm_clk_freq            : natural := c_unb2b_board_mm_clk_freq_100M;

  signal CLK                   : std_logic;
  --signal PPS                   : STD_LOGIC;
  signal jesd204_rx_sysref_n   : std_logic;

  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic;

  signal st_rst                     : std_logic;
  signal st_clk                     : std_logic;
  signal st_pps                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- pm bus
  signal reg_unb_pmbus_mosi         : t_mem_mosi;
  signal reg_unb_pmbus_miso         : t_mem_miso;

  -- FPGA sensors
  signal reg_fpga_temp_sens_mosi     : t_mem_mosi;
  signal reg_fpga_temp_sens_miso     : t_mem_miso;
  signal reg_fpga_voltage_sens_mosi  : t_mem_mosi;
  signal reg_fpga_voltage_sens_miso  : t_mem_miso;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- EPCS read
  signal reg_dpmm_data_mosi         : t_mem_mosi;
  signal reg_dpmm_data_miso         : t_mem_miso;
  signal reg_dpmm_ctrl_mosi         : t_mem_mosi;
  signal reg_dpmm_ctrl_miso         : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi         : t_mem_mosi;
  signal reg_mmdp_data_miso         : t_mem_miso;
  signal reg_mmdp_ctrl_mosi         : t_mem_mosi;
  signal reg_mmdp_ctrl_miso         : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi              : t_mem_mosi;
  signal reg_epcs_miso              : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi              : t_mem_mosi;
  signal reg_remu_miso              : t_mem_miso;

  -- QSFP leds
  signal qsfp_green_led_arr         : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr           : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);

  -- JESD signals

  signal jesd204_rx_link_error : std_logic;
  signal jesd204_rx_link_data  : std_logic_vector(31 downto 0);
  signal jesd204_rx_link_valid : std_logic;
  signal jesd204_rx_link_ready : std_logic;

  signal ram_diag_data_buf_jesd_mosi    : t_mem_mosi;
  signal ram_diag_data_buf_jesd_miso    : t_mem_miso;
  signal reg_diag_data_buf_jesd_mosi    : t_mem_mosi;
  signal reg_diag_data_buf_jesd_miso    : t_mem_miso;
  signal diag_data_buf_snk_in_arr       : t_dp_sosi_arr(1 - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb2b_board_lib.ctrl_unb2b_board
  generic map (
    g_sim                => g_sim,
    g_technology         => g_technology,
    g_design_name        => g_design_name,
    g_design_note        => g_design_note,
    g_stamp_date         => g_stamp_date,
    g_stamp_time         => g_stamp_time,
    g_stamp_svn          => g_stamp_svn,
    g_fw_version         => c_fw_version,
    g_mm_clk_freq        => c_mm_clk_freq,
    g_eth_clk_freq       => c_unb2b_board_eth_clk_freq_125M,
    g_aux                => c_unb2b_board_aux,
    g_factory_image      => g_factory_image,
    g_protect_addr_range => g_protect_addr_range,
    g_dp_clk_use_pll     => false
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => st_rst,
    dp_clk                   => st_clk,
    dp_pps                   => st_pps,
    dp_rst_in                => st_rst,
    dp_clk_in                => st_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    SENS_SC                  => SENS_SC,
    SENS_SD                  => SENS_SD,
    -- PM bus
    PMBUS_SC                 => PMBUS_SC,
    PMBUS_SD                 => PMBUS_SD,
    PMBUS_ALERT              => PMBUS_ALERT,

    -- . 1GbE Control Interface
    ETH_clk                  => ETH_CLK,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_unb2b_jesd
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr
   )
  port map(
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,

    -- FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    -- PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- Remote Update
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    --
    ram_diag_data_buf_jesd_mosi => ram_diag_data_buf_jesd_mosi,
    ram_diag_data_buf_jesd_miso => ram_diag_data_buf_jesd_miso,
    reg_diag_data_buf_jesd_mosi => reg_diag_data_buf_jesd_mosi,
    reg_diag_data_buf_jesd_miso => reg_diag_data_buf_jesd_miso,

    jesd204_rx_serial_data   => jesd204_rx_serial_data,
    jesd204_sync_n_out       => jesd204_sync_n_out,
    jesd204_rx_link_error    => jesd204_rx_link_error,
    jesd204_rx_link_data     => jesd204_rx_link_data,
    jesd204_rx_link_valid    => jesd204_rx_link_valid,
    jesd204_rx_link_ready    => jesd204_rx_link_ready,
    jesd204_rx_sysref        => jesd204_rx_sysref_n,
    jesd204_device_clk       => st_clk
  );

  CLK                  <= jesd204_device_clk;
  --PPS                  <= jesd204_rx_sysref;
  jesd204_rx_sysref_n  <= not jesd204_rx_sysref;

  gen_jesd_mon_in : for i in 0 to 1 - 1 generate
    diag_data_buf_snk_in_arr(i).data(31 downto 0) <= jesd204_rx_link_data(31 downto 0);
    diag_data_buf_snk_in_arr(i).valid <= jesd204_rx_link_valid;
    diag_data_buf_snk_in_arr(i).sop   <= '0';
    diag_data_buf_snk_in_arr(i).eop   <= '0';
    diag_data_buf_snk_in_arr(i).err   <= (others => '0');
  end generate;

  u_diag_data_buffer : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_technology   => g_technology,
    g_nof_streams  => 1,
    g_data_w       => 32,
    g_buf_nof_data => 16384,  -- 8192,
    g_buf_use_sync => true,  -- when TRUE start filling the buffer at the in_sync, else after the last word was read
    g_use_rx_seq   => false
  )
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => mm_rst,
    dp_clk            => st_clk,

    ram_data_buf_mosi => ram_diag_data_buf_jesd_mosi,
    ram_data_buf_miso => ram_diag_data_buf_jesd_miso,
    reg_data_buf_mosi => reg_diag_data_buf_jesd_mosi,
    reg_data_buf_miso => reg_diag_data_buf_jesd_miso,

    in_sosi_arr       => diag_data_buf_snk_in_arr,
    in_sync           => st_pps
  );
end str;
