-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra, 28 Jan 2019
-- Purpose: Verify multiple variations of tb_dp_deinterleave_interleave_to_one
-- Description:
-- Usage:
-- > as 3
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_deinterleave_interleave_to_one is
end tb_tb_dp_deinterleave_interleave_to_one;

architecture tb of tb_tb_dp_deinterleave_interleave_to_one is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  constant c_rep_act   : natural :=   5;
  constant c_rep_rnd   : natural := 100;
begin
-- -- general
-- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;   -- always e_active, e_random or e_pulse flow control
-- -- specific
-- g_use_complex            : BOOLEAN := FALSE;
-- g_nof_repeat             : NATURAL := 5;
-- g_nof_streams            : NATURAL := 4;
-- g_pkt_len                : NATURAL := 12;  -- typcially multiple of g_nof_streams
-- g_pkt_gap                : NATURAL := 10

--      g_nof_streams
--      |  g_pkt_len
--      |  | g_pkt_gap
--      |  | |
  u_act_1_12_0         : entity work.tb_dp_deinterleave_interleave_to_one generic map(e_active, false, c_rep_act, 1, 12, 0);
  u_act_2_12_0         : entity work.tb_dp_deinterleave_interleave_to_one generic map(e_active, false, c_rep_act, 2, 12, 0);
  u_act_3_12_0         : entity work.tb_dp_deinterleave_interleave_to_one generic map(e_active, false, c_rep_act, 3, 12, 0);
  u_act_4_12_0         : entity work.tb_dp_deinterleave_interleave_to_one generic map(e_active, false, c_rep_act, 4, 12, 0);
  u_act_4_12_7         : entity work.tb_dp_deinterleave_interleave_to_one generic map(e_active, false, c_rep_act, 4, 12, 7);

  -- Fails if g_pkt_len is not a multiple of g_nof_streams
  --u_act_4_13_0         : ENTITY work.tb_dp_deinterleave_interleave_to_one GENERIC MAP(e_active, FALSE, c_rep_act, 4, 13, 0);
  --u_act_4_13_7         : ENTITY work.tb_dp_deinterleave_interleave_to_one GENERIC MAP(e_active, FALSE, c_rep_act, 4, 13, 7);

  u_rnd_1_12_0         : entity work.tb_dp_deinterleave_interleave_to_one generic map(e_random, false, c_rep_rnd, 1, 12, 0);

  -- Fails on random input valid
  --u_rnd_2_12_0         : ENTITY work.tb_dp_deinterleave_interleave_to_one GENERIC MAP(e_random, FALSE, c_rep_rnd, 2, 12, 0);
  --u_rnd_3_12_0         : ENTITY work.tb_dp_deinterleave_interleave_to_one GENERIC MAP(e_random, FALSE, c_rep_rnd, 3, 12, 0);
  --u_rnd_4_12_0         : ENTITY work.tb_dp_deinterleave_interleave_to_one GENERIC MAP(e_random, FALSE, c_rep_rnd, 4, 12, 0);
  --u_rnd_4_12_7         : ENTITY work.tb_dp_deinterleave_interleave_to_one GENERIC MAP(e_random, FALSE, c_rep_rnd, 4, 12, 7);
  -- Fails if g_pkt_len is not a multiple of g_nof_streams
  --u_rnd_4_13_0         : ENTITY work.tb_dp_deinterleave_interleave_to_one GENERIC MAP(e_random, FALSE, c_rep_rnd, 4, 13, 0);
  --u_rnd_4_13_7         : ENTITY work.tb_dp_deinterleave_interleave_to_one GENERIC MAP(e_random, FALSE, c_rep_rnd, 4, 13, 7);

  u_act_3_12_0_complex : entity work.tb_dp_deinterleave_interleave_to_one generic map(e_active,  true, c_rep_act, 3, 12, 0);
  u_pls_3_12_0         : entity work.tb_dp_deinterleave_interleave_to_one generic map(e_pulse,  false, c_rep_rnd, 3, 12, 0);
end tb;
