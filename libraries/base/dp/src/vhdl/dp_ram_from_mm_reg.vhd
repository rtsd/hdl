-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity dp_ram_from_mm_reg is
  generic (
    g_dp_on_at_init : std_logic := '0'  -- Enable data path at default
  );
  port (
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;

    st_rst            : in  std_logic;
    st_clk            : in  std_logic;

    sla_in            : in  t_mem_mosi;

    dp_on             : out std_logic
   );
end dp_ram_from_mm_reg;

architecture rtl of dp_ram_from_mm_reg is
  constant c_mm_reg  : t_c_mem := (latency  => 1,
                                   adr_w    => ceil_log2(1),
                                   dat_w    => c_word_w,
                                   nof_dat  => 1,
                                   init_sl  => '0');

  signal mm_dp_on : std_logic;
begin
  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      mm_dp_on <= g_dp_on_at_init;
    elsif rising_edge(mm_clk) then
      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Write Block Sync
          when 0 =>
            mm_dp_on <= sla_in.wrdata(0);
          when others => null;  -- unused MM addresses
        end case;
      end if;
    end if;
  end process;

  u_async_dp_on : entity common_lib.common_async
  generic map (
    g_rst_level => '0'
  )
  port map (
    rst  => st_rst,
    clk  => st_clk,
    din  => mm_dp_on,
    dout => dp_on
  );
end rtl;
