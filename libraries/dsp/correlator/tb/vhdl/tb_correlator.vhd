--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, diag_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.tb_common_pkg.all;
use diag_lib.diag_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use common_lib.common_mem_pkg.all;

-- Folding rules:
-- . g_nof_input_folds=g_nof_pre_mult_folds : no gaps are needed/desired in the input data.
-- . g_nof_input_folds<g_nof_pre_mult_folds : gaps are required in the input data
-- . g_nof_input_folds>g_nof_pre_mult_folds : is not allowed
--   . Produces unnecessary/undesired gaps in the multiplier output
entity tb_correlator is
  generic (
    g_nof_inputs         : natural := 8;
    g_nof_channels       : natural := 8;
    g_nof_pre_mult_folds : natural := 1;  -- Make sure c_nof_visibilities can be folded this many times
    g_nof_input_folds    : natural := 1  -- =< g_nof_pre_mult_folds. Use an even g_nof_inputs.
  );
end tb_correlator;

architecture tb of tb_correlator is
  constant c_complex_data_w     : natural := 8;

  constant c_nof_input_streams  : natural := g_nof_inputs / pow2(g_nof_input_folds);
  constant c_integration_period : natural := 18;
  constant c_nof_visibilities   : natural := (g_nof_inputs * (g_nof_inputs + 1)) / 2;

  constant c_input_gaps_needed  : boolean := g_nof_input_folds < g_nof_pre_mult_folds;
  constant c_block_period       : natural := sel_a_b(c_input_gaps_needed, pow2(g_nof_pre_mult_folds - g_nof_input_folds), 1);

  -- Block generator
  constant c_bg_block_size      : natural := g_nof_channels * pow2(g_nof_input_folds);
  constant c_bg_gapsize         : natural := c_bg_block_size * (c_block_period - 1);

  -- Indicate the integration period with the sync. In the correlator, the
  -- integration period is forced to a minimum of c_nof_visibilities to
  -- allow folding the output onto one stream.
  -- . The sync pulse is only there for the human eye (wave window) -
  --   it is not used by the correlator.
--  CONSTANT c_bg_blocks_per_sync : NATURAL := largest(c_integration_period, c_nof_visibilities);

  constant c_bg_blocks_per_sync : natural := c_integration_period;

  constant c_bg_ctrl            : t_diag_block_gen := ('1',  -- enable
                                                       '0',  -- enable_sync
                                                      TO_UVEC(     c_bg_block_size, c_diag_bg_samples_per_packet_w),
                                                      TO_UVEC(c_bg_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                                      TO_UVEC(        c_bg_gapsize, c_diag_bg_gapsize_w),
                                                      TO_UVEC(                   0, c_diag_bg_mem_low_adrs_w),
                                                      TO_UVEC(   c_bg_block_size-1, c_diag_bg_mem_high_adrs_w),
                                                      TO_UVEC(                   0, c_diag_bg_bsn_init_w));

  constant c_dp_clk_period      : time := 10 ns;
  constant c_mm_clk_period      : time := 10 ps;

  signal tb_end                 : std_logic := '0';
  signal dp_clk                 : std_logic := '1';
  signal dp_rst                 : std_logic;
  signal mm_clk                 : std_logic := '1';
  signal mm_rst                 : std_logic;

  signal block_gen_src_out_arr  : t_dp_sosi_arr(c_nof_input_streams - 1 downto 0);

  signal dp_fifo_sc_src_out_arr : t_dp_sosi_arr(c_nof_input_streams - 1 downto 0);
  signal dp_fifo_sc_src_in_arr  : t_dp_siso_arr(c_nof_input_streams - 1 downto 0);

  signal correlator_snk_in_arr  : t_dp_sosi_arr(c_nof_input_streams - 1 downto 0);
  signal correlator_src_out_arr : t_dp_sosi_arr(1 - 1 downto 0);

  signal ram_diag_data_buf_mosi : t_mem_mosi;
  signal ram_diag_data_buf_miso : t_mem_miso;
  signal reg_diag_data_buf_mosi : t_mem_mosi;
  signal reg_diag_data_buf_miso : t_mem_miso;
begin
  -----------------------------------------------------------------------------
  -- Clocks and reset
  -----------------------------------------------------------------------------
  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;

  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;

  -----------------------------------------------------------------------------
  -- Block generators
  -- . Each stream in block_gen_src_out_arr contains complex subband samples
  --   from one 'antenna'.
  -- . These complex subband samples are generated and converted to a HEX RAM
  --   initialization file using Python, see tb/python/gen_subband_hex_files.py
  -----------------------------------------------------------------------------
  u_mms_diag_block_gen : entity diag_lib.mms_diag_block_gen
  generic map (
    g_nof_streams        => c_nof_input_streams,
    g_buf_dat_w          => 2 * c_complex_data_w,
    g_buf_addr_w         => ceil_log2(TO_UINT(c_bg_ctrl.samples_per_packet)),
    g_file_name_prefix   => "hex/complex_subbands_" & natural'image(c_complex_data_w) & "b_" & "fold_" & natural'image(g_nof_input_folds),
    g_diag_block_gen_rst => c_bg_ctrl
  )
  port map (
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,

    dp_rst           => dp_rst,
    dp_clk           => dp_clk,

    out_sosi_arr     => block_gen_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Introduce gaps in the input stream
  -- . mms_diag_block_gen does not support gaps within blocks.
  -- . We'll use FIFO buffers and dp_gap to read out the FIFOs to introduce
  --   gaps.
  -----------------------------------------------------------------------------
  gen_dp_fifo_sc : for i in 0 to c_nof_input_streams - 1 generate
    u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
    generic map (
      g_data_w         => 2 * c_complex_data_w,
      g_use_sync       => true,
      g_use_ctrl       => false,
      g_use_complex    => true,
      g_fifo_size      => c_bg_block_size,
      g_fifo_af_margin => 0
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,

      wr_ful      => OPEN,
      usedw       => OPEN,
      rd_emp      => OPEN,

      snk_out     => OPEN,
      snk_in      => block_gen_src_out_arr(i),
      src_in      => dp_fifo_sc_src_in_arr(i),
      src_out     => dp_fifo_sc_src_out_arr(i)
    );
  end generate;

  gen_dp_src_out_timer : for i in 0 to c_nof_input_streams - 1 generate
    u_dp_src_out_timer : entity dp_lib.dp_src_out_timer
    generic map (
      g_block_period => c_block_period
    )
    port map (
      rst                  => dp_rst,
      clk                  => dp_clk,

      snk_in               => dp_fifo_sc_src_out_arr(i),
      snk_out              => dp_fifo_sc_src_in_arr(i)
    );
  end generate;

--  correlator_snk_in_arr <= dp_fifo_sc_src_out_arr;

--  -----------------------------------------------------------------------------
--  -- We want the correlator inputs to be properly tagged with the following
--  -- fields:
--  -- . Required:
--  --   . Sync pulse - this clears the accumulators
--  -- . Optional but recommended fields that are propagated through the correlator:
--  --   . BSN
--  --   . Channel index (part of which may indicate a channel group (beamlet index)
--  --   . SOP, EOP to tag channel blocks
--  -----------------------------------------------------------------------------
--  u_corr_output_framer : ENTITY work.corr_output_framer --FIXME rename to input_framer
--  GENERIC MAP (
--    g_nof_streams                 => c_nof_input_streams,
--    g_nof_folds                   => g_nof_input_folds, -- >0 also folds channel indices
--    g_nof_channels                => g_nof_channels,
--    g_nof_channel_frames_per_sync => c_integration_period,
--    g_generate_sync_and_bsn       => TRUE, -- Don't use input sync and BSN
--    g_fft_channel_index_reorder   => FALSE
--  )
--  PORT MAP (
--    rst         => dp_rst,
--    clk         => dp_clk,
--
--    snk_in_arr  => dp_fifo_sc_src_out_arr,
--
--    src_out_arr => correlator_snk_in_arr
--  );

  -----------------------------------------------------------------------------
  -- Device under test: correlator
  -----------------------------------------------------------------------------
  u_correlator : entity work.correlator
  generic map (
    g_nof_input_streams   => c_nof_input_streams,
    g_input_unfold_factor => g_nof_input_folds,
    g_nof_pre_mult_folds  => g_nof_pre_mult_folds,
    g_data_w              => c_complex_data_w,
    g_nof_channels        => g_nof_channels,
    g_integration_period  => c_integration_period
  )
  port map (
    clk         => dp_clk,
    rst         => dp_rst,

    snk_in_arr  => dp_fifo_sc_src_out_arr,
    src_out_arr => correlator_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Data buffers to be read out by Python
  -----------------------------------------------------------------------------
  u_diag_data_buffer : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => 1,
    g_data_w       => 64,
    g_data_type    => e_complex,
    g_buf_nof_data => c_nof_visibilities,
    g_buf_use_sync => true
  )
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    ram_data_buf_mosi => ram_diag_data_buf_mosi,
    ram_data_buf_miso => ram_diag_data_buf_miso,
    reg_data_buf_mosi => reg_diag_data_buf_mosi,
    reg_data_buf_miso => reg_diag_data_buf_miso,

    in_sync           => correlator_src_out_arr(0).sop,
    in_sosi_arr       => correlator_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Stream recorder to record the correlator output stream to a file
  -- . The data buffer can only take snapshots.
  -----------------------------------------------------------------------------
  u_dp_stream_rec_play : entity dp_lib.dp_stream_rec_play
  generic map (
    g_sim            => true,
    g_pass_through   => false,
    g_rec_not_play   => true,
    g_rec_play_file  => "$HDL_WORK/libraries/dsp/correlator/tb/rec/correlator_src_out_arr0.rec",
    g_record_invalid => false
  )
  port map (
    dp_clk  => dp_clk,
    snk_in  => correlator_src_out_arr(0),
    snk_out => OPEN,
    src_out => OPEN,
    src_in  => c_dp_siso_rdy
  );

  -----------------------------------------------------------------------------
  -- MM file I/O with Python
  -----------------------------------------------------------------------------
  u_mm_file_ram_diag_data_buffer : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "RAM_DIAG_DATA_BUFFER")
                                              port map(mm_rst, mm_clk, ram_diag_data_buf_mosi, ram_diag_data_buf_miso);

  u_mm_file_reg_diag_data_buffer : mm_file generic map(mmf_unb_file_prefix(0, 0, "FN") & "REG_DIAG_DATA_BUFFER")
                                              port map(mm_rst, mm_clk, reg_diag_data_buf_mosi, reg_diag_data_buf_miso);

  ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
end tb;
