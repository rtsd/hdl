--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_field_pkg.all;

package unb2_test_pkg is
  -- dp_offload_tx
  --CONSTANT c_nof_hdr_fields : NATURAL := 1+3+12+4+2; -- Total header bits = 384 = 6 64b words
  constant c_nof_hdr_fields : natural := 3 + 12 + 4 + 2;  -- Total header bits = 384 = 6 64b words
  constant c_hdr_field_arr  : t_common_field_arr(c_nof_hdr_fields - 1 downto 0) := (  -- ( field_name_pad("align"              ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("eth_dst_mac"        ), "  ", 48, field_default(0) ),
                                                                                   ( field_name_pad("eth_src_mac"        ), "  ", 48, field_default(0) ),
                                                                                   ( field_name_pad("eth_type"           ), "  ", 16, field_default(x"0800") ),
                                                                                   ( field_name_pad("ip_version"         ), "  ",  4, field_default(4) ),
                                                                                   ( field_name_pad("ip_header_length"   ), "  ",  4, field_default(5) ),
                                                                                   ( field_name_pad("ip_services"        ), "  ",  8, field_default(0) ),
                                                                                   ( field_name_pad("ip_total_length"    ), "  ", 16, field_default(0) ),  -- FIXME fill this in for non point-to-point use
                                                                                   ( field_name_pad("ip_identification"  ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_flags"           ), "  ",  3, field_default(2) ),
                                                                                   ( field_name_pad("ip_fragment_offset" ), "  ", 13, field_default(0) ),
                                                                                   ( field_name_pad("ip_time_to_live"    ), "  ",  8, field_default(127) ),
                                                                                   ( field_name_pad("ip_protocol"        ), "  ",  8, field_default(17) ),
                                                                                   ( field_name_pad("ip_header_checksum" ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_src_addr"        ), "  ", 32, field_default(0) ),
                                                                                   ( field_name_pad("ip_dst_addr"        ), "  ", 32, field_default(0) ),
                                                                                   ( field_name_pad("udp_src_port"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_dst_port"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_total_length"   ), "  ", 16, field_default(0) ),  -- FIXME fill this in for non point-to-point use
                                                                                   ( field_name_pad("udp_checksum"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("usr_sync"           ), "  ",  1, field_default(0) ),
                                                                                   ( field_name_pad("usr_bsn"            ), "  ", 47, field_default(0) ));

  --CONSTANT c_hdr_field_ovr_init         : STD_LOGIC_VECTOR(c_nof_hdr_fields-1 DOWNTO 0) := "1001"&"111111111100"&"0011"&"00";
  constant c_hdr_field_ovr_init         : std_logic_vector(c_nof_hdr_fields - 1 downto 0) := "001" & "111111111100" & "0011" & "00";
end unb2_test_pkg;
