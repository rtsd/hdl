--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Checks if the number of SOPs within a sync interval is as expected and
--          corrects if sync is too early or too late.

--
-- Description: The dp_sync_checker distinguishes three situations:
--
--              1. SYNC is ok
--              In case the sync interval is as expected the input data is simply
--              passed through to the output.
--
--              2. SYNC is too early
--              When the sync is too early the data is still passed to the output,
--              unitl the specief number of packets (g_nof_blk_per_sync) have been
--              send. The SYNC signal that was too early is suppressed and will not
--              be visible at the output. Once the g_nof_blk_per_sync packets have
--              been send to the output the dp_sync_checker will wait until a new
--              sync has arrived.
--
--              3. SYNC is too late
--              When an expected SYNC is not found at the input the dp_sync_checker
--              will stop passing data to the output and wait for the next SYNC on
--              the input.
--
-- Remarks: Parsing of backpressure is not yet implemented,  since it was not
--          required for the first purpose of this block (in reorder_transpose).
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mms_dp_sync_checker is
  generic(
    g_cross_clock_domain : boolean := true;
    g_nof_blk_per_sync   : natural := 16
  );
  port (
    mm_rst                   : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk                   : in  std_logic;  -- memory-mapped bus clock

    dp_clk                   : in  std_logic;
    dp_rst                   : in  std_logic;

    -- ST sinks
    snk_out                  : out t_dp_siso;
    snk_in                   : in  t_dp_sosi;
    -- ST source
    src_in                   : in  t_dp_siso := c_dp_siso_rdy;
    src_out                  : out t_dp_sosi;

    -- Memory Mapped
    reg_dp_sync_checker_mosi : in  t_mem_mosi;
    reg_dp_sync_checker_miso : out t_mem_miso

  );
end mms_dp_sync_checker;

architecture str of mms_dp_sync_checker is
  -- Define the actual size of the MM slave register
  constant c_nof_regs  : positive := 2;
  constant c_mm_reg    : t_c_mem := (latency  => 1,
                                     adr_w    => ceil_log2(c_nof_regs),
                                     dat_w    => c_word_w,
                                     nof_dat  => c_nof_regs,
                                     init_sl  => '0');

  signal read_register   : std_logic_vector(c_nof_regs * c_word_w - 1 downto 0);
  signal nof_early_syncs : std_logic_vector(c_word_w - 1 downto 0);
  signal nof_late_syncs  : std_logic_vector(c_word_w - 1 downto 0);
  signal reg_rd_arr      : std_logic_vector(c_nof_regs - 1 downto 0);
begin
  u_dp_sync_checker : entity work.dp_sync_checker
  generic map(
    g_nof_blk_per_sync => g_nof_blk_per_sync
  )
  port map(
    dp_clk                => dp_clk,
    dp_rst                => dp_rst,
    snk_out               => snk_out,
    snk_in                => snk_in,
    src_in                => src_in,
    src_out               => src_out,
    nof_early_syncs       => nof_early_syncs,
    nof_late_syncs        => nof_late_syncs,
    clear_nof_early_syncs => reg_rd_arr(0),
    clear_nof_late_syncs  => reg_rd_arr(1)
  );

  read_register <= nof_late_syncs & nof_early_syncs;

  u_reg : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_in_new_latency     => 1,
    g_readback           => true,
    g_reg                => c_mm_reg
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => dp_rst,
    st_clk      => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => reg_dp_sync_checker_mosi,
    sla_out     => reg_dp_sync_checker_miso,

    -- MM registers in st_clk domain
    reg_wr_arr  => OPEN,
    reg_rd_arr  => reg_rd_arr,
    in_new      => OPEN,
    in_reg      => read_register,
    out_reg     => OPEN,
    out_new     => open
  );
end str;
