-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Delay differential FPGA input

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity common_iobuf_in is
  generic(
    g_device_family : string := "Stratix IV";
    g_width         : natural := 8;
    g_delay_arr     : t_natural_arr := array_init(0, 8)  -- nof must match g_width
  );
  port (
    config_rst  : in  std_logic;
    config_clk  : in  std_logic;
    config_done : out std_logic;
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    out_dat     : out std_logic_vector(g_width - 1 downto 0)
  );
end common_iobuf_in;
