-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- Author: Eric Kooistra, 14 Dec 2018
-- Purpose: Verify multiple variations of tb_dp_block_select
-- Description:
-- Usage:
-- > as 3
-- > run -all

entity tb_tb_dp_block_select is
end tb_tb_dp_block_select;

architecture tb of tb_tb_dp_block_select is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  constant c_length   : natural := 5;
  constant c_end      : natural := c_length - 1;
begin
-- g_dut_pipeline         : NATURAL := 1;
-- g_nof_blocks_per_sync  : NATURAL := c_length;
-- g_index_lo             : NATURAL := 0;
-- g_index_hi             : NATURAL := c_length-1 = c_end

  u_0_end_comb  : entity work.tb_dp_block_select generic map(0, c_length, 0, c_end);  -- pass on unchanged without pipeline so combinatorially
  u_0_end       : entity work.tb_dp_block_select generic map(1, c_length, 0, c_end);  -- pass on unchanged with pipeline
  u_0_skip      : entity work.tb_dp_block_select generic map(1, c_length, 0, c_end - 1);  -- skip last block in sync interval
  u_skip_end    : entity work.tb_dp_block_select generic map(1, c_length, 1, c_end);  -- skip first block in sync interval
  u_skip_skip   : entity work.tb_dp_block_select generic map(1, c_length, 1, c_end - 1);  -- skip first and last block in sync interval
  u_0           : entity work.tb_dp_block_select generic map(1, c_length, 0, 0);  -- pass only block at first index
  u_2           : entity work.tb_dp_block_select generic map(1, c_length, 2, 2);  -- pass only block at hi index = lo index = 2
  u_end         : entity work.tb_dp_block_select generic map(1, c_length, c_end - 1, c_end - 1);  -- pass only block at last index
  u_none        : entity work.tb_dp_block_select generic map(1, c_length, 3, 2);  -- pass no block because hi index < lo index
  u_none_end    : entity work.tb_dp_block_select generic map(1, c_length, c_end + 1, c_end + 1);  -- pass no block because lo index >= c_length
end tb;
