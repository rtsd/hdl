-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;

package lofar2_unb2b_ring_pkg is
 -----------------------------------------------------------------------------
  -- Revision control
  -----------------------------------------------------------------------------

  type t_lofar2_unb2b_ring_config is record
    N_ring_lanes      : natural;
    N_error_counts    : natural;
  end record;

  constant c_one  : t_lofar2_unb2b_ring_config := (1, 8);
  constant c_full : t_lofar2_unb2b_ring_config := (8, 8);

  -- Function to select the revision configuration.
  function func_sel_revision_rec(g_design_name : string) return t_lofar2_unb2b_ring_config;
end lofar2_unb2b_ring_pkg;

package body lofar2_unb2b_ring_pkg is
  function func_sel_revision_rec(g_design_name : string) return t_lofar2_unb2b_ring_config is
  begin
    if    g_design_name = "lofar2_unb2b_ring_one"        then return c_one;
    else  return c_full;
    end if;

  end;
end lofar2_unb2b_ring_pkg;
