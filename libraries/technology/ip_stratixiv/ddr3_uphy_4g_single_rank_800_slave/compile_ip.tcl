#------------------------------------------------------------------------------
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on Megawizard-generated file msim_setup.tcl.

set IP_DIR "$env(HDL_BUILD_DIR)/unb1/qmegawiz/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_sim"

# Assume library work already exists

# Copy ROM/RAM files to simulation directory
file copy -force $IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_sequencer_mem.hex ./
file copy -force $IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_AC_ROM.hex ./
file copy -force $IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_inst_ROM.hex ./

# Compile the design files in correct order and map them all to library work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_mm_st_converter.v"                                        -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_addr_cmd.v"                                               -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_addr_cmd_wrap.v"                                          -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ddr2_odt_gen.v"                                           -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ddr3_odt_gen.v"                                           -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_lpddr2_addr_cmd.v"                                        -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_odt_gen.v"                                                -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_rdwr_data_tmg.v"                                          -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_arbiter.v"                                                -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_burst_gen.v"                                              -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_cmd_gen.v"                                                -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_csr.v"                                                    -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_buffer.v"                                                 -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_buffer_manager.v"                                         -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_burst_tracking.v"                                         -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_dataid_manager.v"                                         -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_fifo.v"                                                   -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_list.v"                                                   -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_rdata_path.v"                                             -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_wdata_path.v"                                             -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ecc_decoder.v"                                            -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ecc_decoder_32_syn.v"                                     -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ecc_decoder_64_syn.v"                                     -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ecc_encoder.v"                                            -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ecc_encoder_32_syn.v"                                     -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ecc_encoder_64_syn.v"                                     -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_ecc_encoder_decoder_wrapper.v"                            -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_axi_st_converter.v"                                       -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_input_if.v"                                               -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_rank_timer.v"                                             -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_sideband.v"                                               -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_tbp.v"                                                    -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_timing_param.v"                                           -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_controller.v"                                             -work work
vlog     +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_ddrx_controller_st_top.v"                                      -work work
vlog -sv +incdir+$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/alt_mem_if_nextgen_ddr3_controller_core.sv"                            -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_c0.v"                             -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0.v"                             -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_ram.v"                                                      -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_avalon_sc_fifo.v"                                               -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_merlin_master_translator.sv"                                    -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst_test_bench.v" -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_scc_sv_wrapper.sv"                                           -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_inst_ROM_no_ifdef_params.v"                                 -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_reset_synchronizer.v"                                           -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_write_decoder.v"                                            -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_bitcheck.v"                                                 -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_read_datapath.v"                                            -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_reset_controller.v"                                             -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_ddr3.v"                                                     -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_di_buffer_wrap.v"                                           -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_jumplogic.v"                                                -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_ac_ROM_reg.v"                                               -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_data_mgr.sv"                                                 -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_scc_siii_wrapper.sv"                                         -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_addr_router.sv"                -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_merlin_master_agent.sv"                                         -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_cmd_xbar_demux.sv"             -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_data_decoder.v"                                             -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_scc_siii_phase_decode.v"                                     -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_cmd_xbar_mux_003.sv"           -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_cmd_xbar_demux_001.sv"         -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_pattern_fifo.v"                                             -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_dm_decoder.v"                                               -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_merlin_slave_agent.sv"                                          -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_phy_mgr.sv"                                                  -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst.v"            -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_scc_sv_phase_decode.v"                                       -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_id_router.sv"                  -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_merlin_slave_translator.sv"                                     -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_inst_ROM_reg.v"                                             -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_scc_reg_file.v"                                              -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_lfsr12.v"                                                   -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_rsp_xbar_demux_003.sv"         -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_ac_ROM_no_ifdef_params.v"                                   -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_lfsr72.v"                                                   -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_rsp_xbar_mux.sv"               -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_datamux.v"                                                  -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_data_broadcast.v"                                           -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_scc_mgr.sv"                                                  -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_ram_csr.v"                                                  -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_lfsr36.v"                                                   -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_generic.sv"                                                 -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_addr_router_001.sv"            -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_core.sv"                                                    -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_merlin_burst_uncompressor.sv"                                   -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_merlin_arbitrator.sv"                                           -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_scc_acv_wrapper.sv"                                          -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_scc_acv_phase_decode.v"                                      -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/rw_manager_di_buffer.v"                                                -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altera_mem_if_sequencer_mem_no_ifdef_params.sv"                        -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_id_router_003.sv"              -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/sequencer_reg_file.sv"                                                 -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_s0_irq_mapper.sv"                 -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/afi_mux_ddr3_ddrx.v"                                                   -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_clock_pair_generator.v"        -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_read_valid_selector.v"         -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_addr_cmd_datapath.v"           -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_reset.v"                       -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_acv_ldc.v"                     -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_addr_cmd_pads.v"               -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_addr_cmd_ldc_pads.v"           -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_addr_cmd_ldc_pad.v"            -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/addr_cmd_non_ldc_pad.v"                                                -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_memphy.v"                      -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_reset_sync.v"                  -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_new_io_pads.v"                 -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_fr_cycle_shifter.v"            -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_read_datapath.v"               -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_write_datapath.v"              -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_hr_to_fr.v"                    -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_simple_ddio_out.v"             -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_sequencer_mux_bridge.sv"       -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_phy_csr.sv"                    -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_iss_probe.v"                   -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_flop_mem.v"                    -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0.sv"                            -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_p0_altdqdqs.v"                    -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altdq_dqs2_ddio_3reg_stratixiv.sv"                                     -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altdq_dqs2_abstract.sv"                                                -work work
vlog -sv                                                                   "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/altdq_dqs2_cal_delays.sv"                                              -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave_0002.v"                           -work work
vlog                                                                       "$IP_DIR/ip_stratixiv_ddr3_uphy_4g_single_rank_800_slave.v"
