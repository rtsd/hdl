--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Wrapper for corr_folder that allows the use of t_dp_sosi_2arr_2
-- Description:
-- . Our input is an array of                             g_nof_inputs*2 sosi buses
-- . Our 2 corr_folder instances below accept an array of 2*g_nof_inputs sosi buses
-- => We need to do a tranposed signal assignment.

entity corr_folder_2arr_2 is
  generic (
    g_nof_inputs : natural;
    g_nof_folds  : integer := -1
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_2arr_2  : in  t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);

    src_out_2arr_2 : out t_dp_sosi_2arr_2(sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1) - 1 downto 0)
  );
end corr_folder_2arr_2;

architecture str of corr_folder_2arr_2 is
  constant c_nof_outputs : natural := sel_a_b(g_nof_folds >= 0, ceil_div(g_nof_inputs, ceil_pow2(g_nof_folds)), 1);

  signal dp_pipeline_snk_in_2arr_2  : t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);
  signal dp_pipeline_src_out_2arr_2 : t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);

  type t_corr_folder_snk_in_2arr is array (2 - 1 downto 0) of t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  signal corr_folder_snk_in_2arr  : t_corr_folder_snk_in_2arr;

  type t_corr_folder_src_out_2arr is array (2 - 1 downto 0) of t_dp_sosi_arr(c_nof_outputs - 1 downto 0);
  signal corr_folder_src_out_2arr : t_corr_folder_src_out_2arr;
begin
  ------------------------------------------------------------------------------
  -- Preserve only the sync of input stream (0)(0)
  -- FIXME The above applies to a pulse sync which is always only present on
  -- (0)(0). Since the accumulator now uses a block sync, we need to preserve
  -- the sync of all inputs.
  ------------------------------------------------------------------------------
--  p_dp_pipeline_snk_in_2arr_2: PROCESS(snk_in_2arr_2)
--  BEGIN
--    dp_pipeline_snk_in_2arr_2 <= snk_in_2arr_2;
--    FOR i IN 0 TO g_nof_inputs-1 LOOP
--      FOR j IN 0 TO 2-1 LOOP
--        dp_pipeline_snk_in_2arr_2(i)(j).sync <= '0';
--      END LOOP;
--    END LOOP;
--  dp_pipeline_snk_in_2arr_2(0)(0).sync <= snk_in_2arr_2(0)(0).sync;
--  END PROCESS;
  dp_pipeline_snk_in_2arr_2 <= snk_in_2arr_2;

  -----------------------------------------------------------------------------
  -- . This pipeline stage provides the correct timing of valid data for the
  --   folder stage.
  --   . The delay required for each stream i is : (i rem 2^nof_folds)
  --     . Example for 4 streams, fold 2 times (into one output stream):
  --
  --       0) [0][1][2][3] -> pipeline -> [0]      [1]      [2]      [3]
  --       1) [0][1][2][3] -> pipeline ->   [0]      [1]      [2]      [3]
  --       2) [0][1][2][3] -> pipeline ->     [0]      [1]      [2]      [3]
  --       3) [0][1][2][3] -> pipeline ->       [0]      [1]      [2]      [3]
  -----------------------------------------------------------------------------
  gen_dp_pipeline_src_out_2arr2 : for i in 0 to g_nof_inputs - 1 generate
    gen_dp_pipeline : for j in 0 to 2 - 1 generate
      u_dp_pipeline : entity dp_lib.dp_pipeline
      generic map (
        g_pipeline => 0 + (i rem pow2(g_nof_folds))
      )
      port map (
        rst         => rst,
        clk         => clk,

        snk_in      => dp_pipeline_snk_in_2arr_2(i)(j),
        src_out     => dp_pipeline_src_out_2arr_2(i)(j)
      );
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- Folder input: tranposed pipeline output
  ------------------------------------------------------------------------------
  gen_input_i : for i in 0 to g_nof_inputs - 1 generate
    gen_input_j : for j in 0 to 2 - 1 generate
      corr_folder_snk_in_2arr(j)(i) <= dp_pipeline_src_out_2arr_2(i)(j);
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- Our two folders
  ------------------------------------------------------------------------------
  gen_corr_folder: for i in 0 to 2 - 1 generate
    u_corr_folder : entity work.corr_folder
    generic map (
      g_nof_inputs => g_nof_inputs,
      g_nof_folds  => g_nof_folds
    )
    port map (
      rst         => rst,
      clk         => clk,

      snk_in_arr  => corr_folder_snk_in_2arr(i),

      src_out_arr => corr_folder_src_out_2arr(i)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- Entity output: tranposed folder output
  ------------------------------------------------------------------------------
  gen_output_i : for i in 0 to c_nof_outputs - 1 generate
    gen_output_j : for j in 0 to 2 - 1 generate
      src_out_2arr_2(i)(j) <= corr_folder_src_out_2arr(j)(i);
    end generate;
  end generate;
end str;
