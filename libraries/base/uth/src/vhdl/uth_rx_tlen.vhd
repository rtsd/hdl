--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.uth_pkg.all;

-- Purpose: Determine UTH frame length and channel from Rx tlen field value
--
-- Description:
--   This uth_rx_tlen only lets the g_nof_ch different UTH frames through. The
--   frame 'tlen' should match the g_typ_arr. When it matches then the use
--   the corresponding length value from g_len_arr for passing the payload from
--   the UTH frame on to the corresponding channel. All other UTH frames that
--   may appear with 'tlen' that are not in g_typ_arr will get discarded.
--
-- * Example 1:
--   g_data_w  =  8
--   g_nof_ch  =  1
--   g_typ_arr = 30
--   g_len_arr = 30
--   channel   =  don't care
--
-- * Example 2:
--   g_data_w  =  8
--   g_nof_ch  =  5
--   g_typ_arr = 30, 128, 201, 200, 60
--   g_len_arr = 30,  25,  20,  20, 60
--   channel   =  0,   1,   2,   3,  4
--
--   Note that the g_typ_arr entries must all be unique. By using different
--   TLEN types it is possible to have different kind of packets with the same
--   length, e.g. like for TLEN type is 200 and 201 in example 2.

entity uth_rx_tlen is
  generic (
    g_data_w     : natural := c_uth_data_max_w;
    g_nof_ch     : natural := 4;  -- g_nof_ch, the channels are numbered from 0 TO g_nof_ch-1, each channel represents a type of UTH frame
    g_typ_arr    : t_natural_arr := array_init(0, 4);  -- g_nof_ch = g_typ_arr'LENGTH, use unconstraint type to allow generic g_nof_ch
    g_len_arr    : t_natural_arr := array_init(1, 4)  -- g_nof_ch = g_len_arr'LENGTH, use unconstraint type to allow generic g_nof_ch
  );
  port (
    tlen      : in  std_logic_vector;
    nof_data  : out std_logic_vector(g_data_w - 1 downto 0);
    channel   : out std_logic_vector
  );
end uth_rx_tlen;

architecture rtl of uth_rx_tlen is
  -- Map input t_natural_arr range to range 0 TO g_nof_ch-1
  constant c_typ_arr  : t_natural_arr(0 to g_nof_ch - 1) := g_typ_arr;
  constant c_len_arr  : t_natural_arr(0 to g_nof_ch - 1) := g_len_arr;
begin
  process(tlen)
    variable v_tlen : std_logic_vector(g_data_w - 1 downto 0);
  begin
    v_tlen := tlen(g_data_w - 1 downto 0);

    nof_data <= TO_UVEC(0, g_data_w);  -- default use zero payload length to flush UTH frames other than those supported by the g_nof_ch
    channel  <= TO_UVEC(0, channel'length);  -- default channel value

    for I in 0 to g_nof_ch - 1 loop
      if unsigned(v_tlen) = c_typ_arr(I) then  -- search tlen in g_typ_arr
        nof_data <= TO_UVEC(c_len_arr(I), g_data_w);  -- TLEN type match in g_typ_arr so use corresponding TLEN length from g_len_arr
        channel  <= TO_UVEC(I, channel'length);  -- and use the index I as channel number
        exit;  -- there will be only one match, the first match is the match
      end if;
    end loop;
  end process;
end rtl;
