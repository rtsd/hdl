-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_block_gen is
end tb_tb_dp_block_gen;

architecture tb of tb_tb_dp_block_gen is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 3
  -- > run -all                 --> OK

  -- g_use_src_in         : BOOLEAN := TRUE;
  -- g_nof_data_per_block : NATRUAL := 11;
  -- g_nof_blk_per_sync   : NATURAL := 8;
  -- g_enable             : t_dp_flow_control_enum := e_active;  -- always e_active or e_pulse block generator enable
  -- g_out_ready          : t_dp_flow_control_enum := e_random;  -- always e_active, e_random or e_pulse flow control
  -- g_nof_repeat         : NATURAL := 100

  u_src_in_ready_1 : entity work.tb_dp_block_gen generic map (true,  11, 1, e_active, e_active, 100);
  u_src_in_ready_8 : entity work.tb_dp_block_gen generic map (true,  11, 8, e_active, e_random, 100);
  u_src_in_ready_9 : entity work.tb_dp_block_gen generic map (true,  11, 9, e_pulse,  e_random, 100);
  u_snk_in_valid_1 : entity work.tb_dp_block_gen generic map (false, 11, 1, e_active, e_active, 100);
  u_snk_in_valid_8 : entity work.tb_dp_block_gen generic map (false, 11, 8, e_active, e_random, 100);
  u_snk_in_valid_9 : entity work.tb_dp_block_gen generic map (false, 11, 9, e_pulse,  e_random, 100);
end tb;
