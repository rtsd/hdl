-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.mdio_pkg.all;

entity tb_mdio_phy is
end tb_mdio_phy;

architecture tb of tb_mdio_phy is
  constant c_sim        : boolean := true;

  constant clk_period   : time := 10 ns;
  constant c_delay      : natural := 50;

  constant c_st         : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_st;
  constant c_op_addr    : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_addr;
  constant c_op_wr      : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_wr;
  constant c_op_rd      : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_rd;
  constant c_op_rd_incr : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_rd_incr;
  constant c_prtad      : std_logic_vector(4 downto 0) := "00000";  -- port address (example)
  constant c_devad      : std_logic_vector(4 downto 0) := "00001";  -- device address (example)
  constant c_ta         : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_ta;

  constant c_hdr_wraddr : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_addr & c_prtad & c_devad & c_ta;
  constant c_hdr_wrdata : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_wr   & c_prtad & c_devad & c_ta;
  constant c_hdr_rddata : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_rd   & c_prtad & c_devad & c_ta;

  constant c_phy_addr   : std_logic_vector(c_halfword_w - 1 downto 0) := std_logic_vector(to_unsigned(16#A001#, c_halfword_w));
  constant c_phy_data   : std_logic_vector(c_halfword_w - 1 downto 0) := std_logic_vector(to_unsigned(16#D001#, c_halfword_w));

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '1';
  signal rst            : std_logic;

  signal mdio_en_evt    : std_logic;
  signal mdio_done_evt  : std_logic;
  signal hdr            : std_logic_vector(c_halfword_w - 1 downto 0);
  signal tx_dat         : std_logic_vector(c_halfword_w - 1 downto 0);
  signal rx_dat         : std_logic_vector(c_halfword_w - 1 downto 0);

  signal mdc            : std_logic;
  signal mdat_in        : std_logic;
  signal mdat_oen       : std_logic;
  signal mdio           : std_logic;

  -- MDIO slave model
  constant c_slave_reg_addr : natural := 16#A001#;
  constant c_slave_reg_data : natural := 16#1234#;

  signal slave_addr     : std_logic_vector(c_halfword_w - 1 downto 0);  -- MDIO access address
  signal slave_reg_addr : std_logic_vector(c_halfword_w - 1 downto 0);  -- the one data register address
  signal slave_reg_dat  : std_logic_vector(c_halfword_w - 1 downto 0);  -- the one data register data value
begin
  -- run 50 us

  clk  <= not clk or tb_end after clk_period / 2;
  rst  <= '1', '0' after clk_period * 3;

  mdio <= 'H';  -- pull up

  -- run 1 us
  p_in_stimuli : process
  begin
    mdio_en_evt <= '0';
    wait until rst = '0';
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- write_MMD
    ----------------------------------------------------------------------------
    -- write PHY register address
    mdio_en_evt <= '1';
    hdr <= c_hdr_wraddr;
    tx_dat <= c_phy_addr;
    wait until rising_edge(clk);
    mdio_en_evt <= '0';
    while mdio_done_evt = '0' loop wait until rising_edge(clk); end loop;
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;
    -- write PHY register data
    mdio_en_evt <= '1';
    hdr <= c_hdr_wrdata;
    tx_dat <= c_phy_data;
    wait until rising_edge(clk);
    mdio_en_evt <= '0';
    while mdio_done_evt = '0' loop wait until rising_edge(clk); end loop;
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- read_MMD
    ----------------------------------------------------------------------------
    -- write PHY register address
    mdio_en_evt <= '1';
    hdr <= c_hdr_wraddr;
    tx_dat <= c_phy_addr;
    wait until rising_edge(clk);
    mdio_en_evt <= '0';
    while mdio_done_evt = '0' loop wait until rising_edge(clk); end loop;
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;
    -- read PHY register data
    mdio_en_evt <= '1';
    hdr <= c_hdr_rddata;
    wait until rising_edge(clk);
    mdio_en_evt <= '0';
    while mdio_done_evt = '0' loop wait until rising_edge(clk); end loop;
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Check read back value
    ----------------------------------------------------------------------------
    if rx_dat = c_phy_data then
      report "Write MDIO slave reg and readback went OK"
        severity NOTE;
    else
      report "Write MDIO slave reg and readback went wrong"
        severity ERROR;
    end if;

    ----------------------------------------------------------------------------
    -- read_MMD, ingore mdio_en_evt pulse while mdio_done_evt has not yet occured
    ----------------------------------------------------------------------------
    -- write PHY register address
    mdio_en_evt <= '1';
    hdr <= c_hdr_wraddr;
    tx_dat <= c_phy_addr;
    wait until rising_edge(clk);
    mdio_en_evt <= '0';
    for I in 0 to 5 loop wait until rising_edge(clk); end loop;
    mdio_en_evt <= '1';
    wait until rising_edge(clk);
    mdio_en_evt <= '0';
    while mdio_done_evt = '0' loop wait until rising_edge(clk); end loop;
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;
    -- read PHY register data
    mdio_en_evt <= '1';
    hdr <= c_hdr_rddata;
    wait until rising_edge(clk);
    mdio_en_evt <= '0';
    for I in 0 to 300 loop wait until rising_edge(clk); end loop;
    mdio_en_evt <= '1';
    wait until rising_edge(clk);
    mdio_en_evt <= '0';
    while mdio_done_evt = '0' loop wait until rising_edge(clk); end loop;
    for I in 0 to c_delay loop wait until rising_edge(clk); end loop;

    ----------------------------------------------------------------------------
    -- Check read back value
    ----------------------------------------------------------------------------
    if rx_dat = c_phy_data then
      report "Write MDIO slave reg and readback went OK"
        severity NOTE;
    else
      report "Write MDIO slave reg and readback went wrong"
        severity ERROR;
    end if;

    tb_end <= '1';
    wait;
  end process;

  -- MDIO master
  u_mdio_phy : entity work.mdio_phy
  port map (
    gs_sim        => c_sim,

    rst           => rst,
    clk           => clk,

    mdio_en_evt   => mdio_en_evt,
    mdio_done_evt => mdio_done_evt,

    hdr           => hdr,
    tx_dat        => tx_dat,
    rx_dat        => rx_dat,

    -- External clock and serial data.
    mdc           => mdc,
    mdat_in       => mdat_in,
    mdat_oen      => mdat_oen
  );

  u_iobuf : entity common_lib.common_inout
  port map (
    dat_inout        => mdio,
    dat_in_from_line => mdat_in,
    dat_out_to_line  => '0',  -- for output '1' rely on external pull up, only
    dat_out_en       => mdat_oen  -- pull low when mdat_oen (so implicitely mdat_out='0')
  );

  -- MDIO slave
  u_slave : entity work.mmd_slave
  generic map (
    g_st        => c_st,
    g_prtad     => c_prtad,
    g_devad     => c_devad,
    g_reg_addr  => c_slave_reg_addr,
    g_reg_dat   => c_slave_reg_data,
    g_reg_w     => c_halfword_w
  )
  port map (
    mdc         => mdc,
    mdio        => mdio,
    addr        => slave_addr,
    reg_dat     => slave_reg_dat
  );

  slave_reg_addr <= std_logic_vector(to_unsigned(c_slave_reg_addr, c_halfword_w));
end tb;
