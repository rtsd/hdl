-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Purpose: Combines an array of MM interfaces into a single MM interface.
-- Description:
--   The common_mem_mux unit combines an array of mosi's and miso's to one
--   single set of mosi and miso. Should be used to decrease the amount of
--   slave memory interfaces to the MM bus.
--
--                                  g_rd_latency
--                                 ______________
--        strip index:             |            |
--        mosi.address[h:w] ---+-->| delay line |--\
--                             |   |____________|  |
--                             |                   |
--                 selected    v                   |
--   mosi -------> mosi_arr.wr[ ]-----------------------------> mosi_arr
--                          rd                     |
--                                        selected v
--   miso <-------------------------------miso_arr[ ]<--------- miso_arr
--
--        . not selected mosi_arr get mosi but with wr='0', rd='0'
--        . not selected miso_arr are ignored
--
--   Use default g_broadcast=FALSE for multiplexed individual MM access to
--   each mosi_arr/miso_arr MM port. When g_broadcast=TRUE then a write
--   access to MM port [0] is passed on to all ports and a read access is
--   done from MM port [0]. The other ports cannot be read.
--
-- Remarks:
-- . In simulation selecting an unused element address will cause a simulation
--   failure. Therefore the element index is only accepted when it is in the
--   g_nof_mosi-1 DOWNTO 0 range.
-- . In case multiple common_mem_mux would be used in series, then only the
--   top one needs to account for g_rd_latency>0, the rest can use 0.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity common_mem_mux is
  generic (
    g_broadcast   : boolean := false;
    g_nof_mosi    : positive := 256;  -- Number of memory interfaces in the array.
    g_mult_addr_w : positive := 8;  -- Address width of each memory-interface element in the muliplexed array.
    g_rd_latency  : natural := 0
  );
  port (
    clk      : in  std_logic := '0';  -- only used when g_rd_latency > 0
    mosi     : in  t_mem_mosi;
    miso     : out t_mem_miso;
    mosi_arr : out t_mem_mosi_arr(g_nof_mosi - 1 downto 0);
    miso_arr : in  t_mem_miso_arr(g_nof_mosi - 1 downto 0) := (others => c_mem_miso_rst)
  );
end common_mem_mux;

architecture rtl of common_mem_mux is
  constant c_index_w        : natural := ceil_log2(g_nof_mosi);
  constant c_total_addr_w   : natural := c_index_w + g_mult_addr_w;

  signal index_arr : t_natural_arr(0 to g_rd_latency);
  signal index_rw  : natural;  -- read or write access
  signal index_rd  : natural;  -- read response
begin
  gen_single : if g_broadcast = false and g_nof_mosi = 1 generate
    mosi_arr(0) <= mosi;
    miso        <= miso_arr(0);
  end generate;

  gen_multiple : if g_broadcast = false and g_nof_mosi > 1 generate
    -- The activated element of the array is detected here
    index_arr(0) <= TO_UINT(mosi.address(c_total_addr_w - 1 downto g_mult_addr_w));

    -- Pipeline the index of the activated element to account for the read latency
    p_clk : process(clk)
    begin
      if rising_edge(clk) then
        index_arr(1 to g_rd_latency) <= index_arr(0 to g_rd_latency - 1);
      end if;
    end process;

    index_rw <= index_arr(0);
    index_rd <= index_arr(g_rd_latency);

    -- Master access, can be write or read
    p_mosi_arr : process(mosi, index_rw)
    begin
      for I in 0 to g_nof_mosi - 1 loop
        mosi_arr(I)    <= mosi;
        mosi_arr(I).rd <= '0';
        mosi_arr(I).wr <= '0';
        if I = index_rw then
          mosi_arr(I).rd <= mosi.rd;
          mosi_arr(I).wr <= mosi.wr;
        end if;
      end loop;
    end process;

    -- Slave response to read access after g_rd_latency clk cycles
    p_miso : process(miso_arr, index_rd)
    begin
      miso <= c_mem_miso_rst;
      for I in 0 to g_nof_mosi - 1 loop
        if I = index_rd then
          miso <= miso_arr(I);
        end if;
      end loop;
    end process;
  end generate;

  gen_broadcast : if g_broadcast = true generate
    mosi_arr <= (others => mosi);  -- broadcast write to all [g_nof_mosi-1:0] MM ports
    miso     <= miso_arr(0);  -- broadcast read only from MM port [0]
  end generate;
end rtl;
