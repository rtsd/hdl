# (C) 1992-2018 Intel Corporation.                            
# Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words    
# and logos are trademarks of Intel Corporation or its subsidiaries in the U.S.  
# and/or other countries. Other marks and brands may be claimed as the property  
# of others. See Trademarks on intel.com for full list of Intel trademarks or    
# the Trademarks & Brands Names Database (if Intel) or See www.Intel.com/legal (if Altera) 
# Your use of Intel Corporation's design tools, logic functions and other        
# software and tools, and its AMPP partner logic functions, and any output       
# files any of the foregoing (including device programming or simulation         
# files), and any associated documentation or information are expressly subject  
# to the terms and conditions of the Altera Program License Subscription         
# Agreement, Intel MegaCore Function License Agreement, or other applicable      
# license agreement, including, without limitation, that your use is for the     
# sole purpose of programming logic devices manufactured by Intel and sold by    
# Intel or its authorized distributors.  Please refer to the applicable          
# agreement for further details.                                                 



#**************************************************************
# Create Generated Clock
#**************************************************************
derive_pll_clocks

#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty

#**************************************************************
# Set Clock Groups
#**************************************************************
set_clock_groups -asynchronous -group {CLK}
set_clock_groups -asynchronous -group {BCK_REF_CLK}
set_clock_groups -asynchronous -group {CLK_USR}
set_clock_groups -asynchronous -group {CLKUSR}
set_clock_groups -asynchronous -group {SA_CLK}
set_clock_groups -asynchronous -group {SB_CLK}
# Do not put ETH_CLK in this list, otherwise the Triple Speed Ethernet does not work

set_clock_groups -asynchronous -group [get_clocks altera_ts_clk]

set_clock_groups -asynchronous -group [get_clocks altera_reserved_tck]


# IOPLL outputs (which have global names defined in the IP qsys settings)
set_clock_groups -asynchronous -group [get_clocks pll_clk20]
set_clock_groups -asynchronous -group [get_clocks pll_clk50]
set_clock_groups -asynchronous -group [get_clocks pll_clk100]
set_clock_groups -asynchronous -group [get_clocks pll_clk125]
set_clock_groups -asynchronous -group [get_clocks pll_clk200]
set_clock_groups -asynchronous -group [get_clocks pll_clk200p]
set_clock_groups -asynchronous -group [get_clocks pll_clk400]
# Isolate the 200MHz dp_clk
set_clock_groups -asynchronous -group [get_clocks {u_ctrl_unb2b_board|\gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|*}]

# Isolate the 125MHz mm_clk
set_clock_groups -asynchronous -group [get_clocks {u_ctrl_unb2b_board|\gen_mm_clk_hardware:u_unb2b_board_clk125_pll|\gen_fractional_pll:u_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|*}]

# Isolate Kernel clock
set_clock_groups -asynchronous -group [get_clocks {board_inst|kernel_clk_gen|board_kernel_clk_gen|kernel_pll|*}]
#**************************************************************
# Set False Path
#**************************************************************
set_false_path -from * -to {u_unb2b_board_qsfp_leds|green_on_arr[*]}
set_false_path -from * -to QSFP_LED[*]

set_false_path -from {u_ctrl_unb2b_board|u_mms_ppsh|u_ppsh|u_in|u_ddio_in|\gen_ip_arria10_e1sg:u0|\gen_w:0:u_ip_arria10_e1sg_ddio_in_1|ip_arria10_ddio_in_1|core|i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio~ddio_in_fr__nff} -to {u_ctrl_unb2b_board|u_mms_ppsh|u_ppsh|pps_ext_cap}

# Make the kernel reset multicycle
set_multicycle_path -to * -setup 4 -from {board_inst|kernel_interface|kernel_interface|reset_controller_sw|alt_rst_sync_uq1|altera_reset_synchronizer_int_chain_out}
set_multicycle_path -to * -hold 3 -from {board_inst|kernel_interface|kernel_interface|reset_controller_sw|alt_rst_sync_uq1|altera_reset_synchronizer_int_chain_out}
#set_multicycle_path -to * -setup 4 -from {freeze_wrapper_inst|kernel_system_clock_reset_reset_reset_n}
set_multicycle_path -to * -setup 4 -from {freeze_wrapper_inst|board_kernel_reset_reset_n}
#set_multicycle_path -to * -hold 3 -from {freeze_wrapper_inst|kernel_system_clock_reset_reset_reset_n}
set_multicycle_path -to * -hold 3 -from {freeze_wrapper_inst|board_kernel_reset_reset_n}

# Cut path to twoXclock_consumer (this instance is only there to keep 
# kernel interface consistent and prevents kernel_clk2x to be swept away by synthesis)
#set_false_path -from * -to freeze_wrapper_inst|pr_region_inst|*|twoXclock_consumer_NO_SHIFT_REG


