-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra/ P. Donker
-- Purpose:
--   1) Initial setup eth1g via the MM tse and MM r port
--   2) Loop control eth1g via MM r port and reg_interrupt to receive and
--      transmit packets via the MM ram port.
-- Description:
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, eth_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
--USE common_lib.tb_common_mem_pkg.ALL;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use eth_lib.eth_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.eth1g_mem_pkg.all;

entity eth1g_master is
  generic (
    g_sim         : boolean := false  -- when true speed up led toggling in simulation
  );
  port (
    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;

    tse_mosi      : out t_mem_mosi;
    tse_miso      : in  t_mem_miso;
    reg_interrupt : in  std_logic;
    reg_mosi      : out t_mem_mosi;
    reg_miso      : in  t_mem_miso;
    ram_mosi      : out t_mem_mosi;
    ram_miso      : in  t_mem_miso;

    src_mac       : in std_logic_vector(c_network_eth_mac_slv'range);
    src_ip        : in std_logic_vector(c_network_ip_addr_slv'range)
  );
end eth1g_master;

architecture rtl of eth1g_master is
  -- ETH control
  constant c_reply_payload  : boolean := false;  -- TRUE copy rx payload into response payload, else header only (e.g. for ARP)

  signal mm_init            : std_logic := '1';
  signal tse_psc_access     : std_logic := '0';  -- debug signal to view when PSC registers in TSE are accessed

  -- TSE constants
  constant c_promis_en      : boolean := false;  -- FALSE receive only frames for this src_mac and broadcast, TRUE receive all

  -- Test bench supported packet data types
  constant c_tb_tech_tse_data_type_symbols : natural := 0;
  constant c_tb_tech_tse_data_type_counter : natural := 1;
  constant c_tb_tech_tse_data_type_arp     : natural := 2;
  constant c_tb_tech_tse_data_type_ping    : natural := 3;  -- over IP/ICMP
  constant c_tb_tech_tse_data_type_udp     : natural := 4;  -- over IP

  -- ETH control
  constant c_control_rx_en  : natural := 2**c_eth_mm_reg_control_bi.rx_en;

  -- . UDP header
  constant c_udp_port_ctrl  : natural := 11;  -- ETH demux UDP for control
  constant c_udp_port_st0   : natural := 57;  -- ETH demux UDP port 0
  constant c_udp_port_st1   : natural := 58;  -- ETH demux UDP port 1
  constant c_udp_port_st2   : natural := 59;  -- ETH demux UDP port 2
  constant c_udp_port_en    : natural := 16#10000#;  -- ETH demux UDP port enable bit 16

  -- used in eth setup
  signal src_mac_hi         : std_logic_vector(c_16 - 1 downto 0);
  signal src_mac_lo         : std_logic_vector(c_32 - 1 downto 0);

  -- used in tse setup
  signal src_mac_0          : std_logic_vector(c_32 - 1 downto 0);
  signal src_mac_1          : std_logic_vector(c_16 - 1 downto 0);

  -- ETH MM registers interface
  signal eth_mm_reg_control : t_eth_mm_reg_control;
  signal eth_mm_reg_status  : t_eth_mm_reg_status;

  signal data_type   : natural := c_tech_tse_data_type_ping;

  -- State maschine
  signal ctrl_state  : natural := 0;
  signal mm_rd_state    : natural := 0;
  signal mm_wt_state    : natural := 0;

  -- mem mm bus request
  signal mm_rd_request   : std_logic := '0';
  signal mm_wr_request   : std_logic := '0';

  type t_state is (s_rst,
                   s_wr_demux_0, s_wr_demux_1, s_wr_demux_2, s_rd_demux_0, s_rd_demux_1, s_rd_demux_2,
                   s_wr_config_0, s_wr_config_1, s_wr_config_2, s_wr_config_3, s_wr_control_0,
                   s_rd_tse_rev, s_wr_tse_if_mode, s_rd_tse_control, s_rd_tse_status, s_wr_tse_control, s_wr_tse_promis_en, s_wr_tse_mac_0, s_wr_tse_mac_1, s_wr_tse_tx_ipg_len, s_wr_tse_frm_len,
                   s_wr_tse_rx_section_empty, s_wr_tse_rx_section_full, s_wr_tse_tx_section_empty, s_wr_tse_tx_section_full,
                   s_wr_tse_rx_almost_empty, s_wr_tse_rx_almost_full, s_wr_tse_tx_almost_empty, s_wr_tse_tx_almost_full,
                   s_wait_interrupt_1, s_wait_interrupt_0, s_rd_payload, s_wr_payload, s_wr_control, s_eth_continue);

  type t_reg is record
      -- outputs
      tse_mosi       : t_mem_mosi;
      reg_mosi       : t_mem_mosi;
      ram_mosi       : t_mem_mosi;
      --internals
      eth_init       : std_logic;
      tse_init       : std_logic;
      tse_psc_access : std_logic;  -- debug signal to view when PSC registers in TSE are accessed
      ram_offset     : natural;
      state          : t_state;
    end record t_reg;

  signal r     : t_reg;
  signal nxt_r : t_reg;

  constant lat_vec_size : natural := 8;

  signal lat_reg_rd     : std_logic;
  signal lat_reg_vec    : std_logic_vector(0 to lat_vec_size-1);
  signal reg_rd_valid   : std_logic;

  signal lat_ram_rd     : std_logic;
  signal lat_ram_vec    : std_logic_vector(0 to lat_vec_size-1);
  signal ram_rd_valid   : std_logic;

  -- Write data to the MM bus
  procedure proc_eth1g_mem_mm_bus_wr(constant wr_addr : in  natural;
                                     constant wr_data : in  integer;
                                     variable mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address := TO_MEM_ADDRESS(wr_addr);
    mm_mosi.wrdata  := TO_MEM_DATA(wr_data);
    mm_mosi.wr      := '1';
  end proc_eth1g_mem_mm_bus_wr;

  procedure proc_eth1g_mem_mm_bus_wr(constant wr_addr : in  natural;
                                     signal   wr_data : in  std_logic_vector;
                                     variable mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address := TO_MEM_ADDRESS(wr_addr);
    mm_mosi.wrdata  := RESIZE_MEM_DATA(wr_data);
    mm_mosi.wr      := '1';
  end proc_eth1g_mem_mm_bus_wr;

  procedure proc_eth1g_mem_mm_bus_rd(constant wr_addr : in  natural;
                                     variable mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address := TO_MEM_ADDRESS(wr_addr);
    mm_mosi.rd      := '1';
  end proc_eth1g_mem_mm_bus_rd;
begin
  tse_mosi   <= r.tse_mosi;
  reg_mosi   <= r.reg_mosi;
  ram_mosi   <= r.ram_mosi;

  src_mac_hi <= src_mac(c_48 - 1 downto c_32);
  src_mac_lo <= src_mac(c_32 - 1 downto 0);

  src_mac_0  <= hton(src_mac(c_48 - 1 downto c_16), 4);
  src_mac_1  <= hton(src_mac(c_16 - 1 downto  0), 2);

  --p_mm_init : PROCESS(mm_clk)
  --BEGIN
  --  IF rising_edge(mm_clk) THEN
  --    IF mm_rst='0' THEN
  --      mm_init <= '0';
  --    END IF;
  --  END IF;
  --END PROCESS;
  mm_init <= '0' when rising_edge(mm_clk) and mm_rst = '0';  -- concurrent statement is equivalent to commented p_mm_init

  p_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      r <= (c_mem_mosi_rst, c_mem_mosi_rst, c_mem_mosi_rst, '1', '1', '0', 0, s_rst);  -- reset all
    elsif rising_edge(mm_clk) then
      r <= nxt_r;
    end if;
  end process p_reg;

  p_rd_latency : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      lat_reg_vec <= (others => '0');
      lat_ram_vec <= (others => '0');
      lat_reg_rd <= '0';
      lat_ram_rd <= '0';
    elsif rising_edge(mm_clk) then
      if nxt_r.reg_mosi.rd = '1' then lat_reg_rd <= '1'; elsif reg_rd_valid = '1' then lat_reg_rd <= '0'; end if;
      if nxt_r.ram_mosi.rd = '1' then lat_ram_rd <= '1'; elsif ram_rd_valid = '1' then lat_ram_rd <= '0'; end if;

      if reg_rd_valid = '1' then lat_reg_vec <= (others => '0'); else lat_reg_vec <= lat_reg_rd & lat_reg_vec(0 to lat_vec_size-2); end if;
      if ram_rd_valid = '1' then lat_ram_vec <= (others => '0'); else lat_ram_vec <= lat_ram_rd & lat_ram_vec(0 to lat_vec_size-2); end if;
    end if;
  end process p_rd_latency;

  reg_rd_valid <= lat_reg_vec(c_mem_reg_rd_latency);
  ram_rd_valid <= lat_ram_vec(c_mem_ram_rd_latency);

  p_comb : process (r.state, reg_miso, ram_miso, tse_miso, reg_interrupt, mm_init, reg_rd_valid, ram_rd_valid)
    variable v : t_reg;
    variable v_eth_control_word : std_logic_vector(c_word_w - 1 downto 0);
  begin
    -- default assignment
    v := r;

    if tse_miso.waitrequest = '0' then
      v.tse_mosi.wr := '0';
      v.tse_mosi.rd := '0';
    end if;
    v.reg_mosi.wr := '0';
    v.reg_mosi.rd := '0';
    v.ram_mosi.wr := '0';
    v.ram_mosi.rd := '0';

    eth_mm_reg_status  <= c_eth_mm_reg_status_rst;
    eth_mm_reg_control <= c_eth_mm_reg_control_rst;

    case r.state is
      when s_rst =>
        v := (c_mem_mosi_rst, c_mem_mosi_rst, c_mem_mosi_rst, '1', '1', '0', 0, s_rst);  -- reset all
        if mm_init = '0' then v.state := s_wr_demux_0;
        end if;

      -- -- start eth setup -- --
      when s_wr_demux_0   => proc_eth1g_mem_mm_bus_wr(c_eth_reg_demux_wi + 0,   c_udp_port_en + c_udp_port_st0, v.reg_mosi); v.state := s_wr_demux_1;
      when s_wr_demux_1   => proc_eth1g_mem_mm_bus_wr(c_eth_reg_demux_wi + 1,   c_udp_port_en + c_udp_port_st1, v.reg_mosi); v.state := s_wr_demux_2;
      when s_wr_demux_2   => proc_eth1g_mem_mm_bus_wr(c_eth_reg_demux_wi + 2,   c_udp_port_en + c_udp_port_st2, v.reg_mosi); v.state := s_rd_demux_0;
      --WHEN s_wr_demux_2   => proc_eth1g_mem_mm_bus_wr(c_eth_reg_demux_wi+2,   c_udp_port_en+c_udp_port_st2, v.reg_mosi); v.state := s_wr_config_0;
      when s_rd_demux_0   =>  -- read back demux_0 settings
        if lat_reg_rd = '0' then proc_eth1g_mem_mm_bus_rd(c_eth_reg_demux_wi + 0, v.reg_mosi);
        elsif reg_rd_valid = '1' then v.state := s_rd_demux_1;
        end if;
      when s_rd_demux_1   =>  -- read back demux_1 settings
        if lat_reg_rd = '0' then proc_eth1g_mem_mm_bus_rd(c_eth_reg_demux_wi + 1, v.reg_mosi);
        elsif reg_rd_valid = '1' then v.state := s_rd_demux_2;
        end if;
      when s_rd_demux_2   =>  -- read back demux_2 settings
        if lat_reg_rd = '0' then proc_eth1g_mem_mm_bus_rd(c_eth_reg_demux_wi + 2, v.reg_mosi);
        elsif reg_rd_valid = '1' then v.state := s_wr_config_0;
        end if;
      when s_wr_config_0  => proc_eth1g_mem_mm_bus_wr(c_eth_reg_config_wi + 0,  src_mac_lo,      v.reg_mosi); v.state := s_wr_config_1;
      when s_wr_config_1  => proc_eth1g_mem_mm_bus_wr(c_eth_reg_config_wi + 1,  src_mac_hi,      v.reg_mosi); v.state := s_wr_config_2;
      when s_wr_config_2  => proc_eth1g_mem_mm_bus_wr(c_eth_reg_config_wi + 2,  src_ip,          v.reg_mosi); v.state := s_wr_config_3;
      when s_wr_config_3  => proc_eth1g_mem_mm_bus_wr(c_eth_reg_config_wi + 3,  c_udp_port_ctrl, v.reg_mosi); v.state := s_wr_control_0;
      when s_wr_control_0 => proc_eth1g_mem_mm_bus_wr(c_eth_reg_control_wi + 0, c_control_rx_en, v.reg_mosi); v.state := s_rd_tse_rev;
      --WHEN s_wr_control_0 => proc_eth1g_mem_mm_bus_wr(c_eth_reg_control_wi+0, c_control_rx_en, v.reg_mosi); v.state := s_wr_tse_if_mode;

      -- -- start tse setup -- --
      when s_rd_tse_rev =>

        v.eth_init := '0';
        v.tse_psc_access := '1';
        proc_eth1g_mem_mm_bus_rd(func_tech_tse_map_pcs_addr(16#22#), v.tse_mosi);  -- REV --> 0x0901
        v.state := s_wr_tse_if_mode;

      when s_wr_tse_if_mode          =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(func_tech_tse_map_pcs_addr(16#28#), 16#0008#, v.tse_mosi);
          v.state := s_rd_tse_control;
        end if;

      when s_rd_tse_control      =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_rd(func_tech_tse_map_pcs_addr(16#00#), v.tse_mosi);
          v.state := s_rd_tse_status;
        end if;

      when s_rd_tse_status      =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_rd(func_tech_tse_map_pcs_addr(16#02#), v.tse_mosi);
          v.state := s_wr_tse_control;
        end if;

      when s_wr_tse_control          =>
        if tse_miso.waitrequest = '0' then
          if g_sim = true then
            proc_eth1g_mem_mm_bus_wr(func_tech_tse_map_pcs_addr(16#00#), 16#0140#, v.tse_mosi);  -- PSC control, Auto negotiate disable
          else
            proc_eth1g_mem_mm_bus_wr(func_tech_tse_map_pcs_addr(16#00#), 16#0140#, v.tse_mosi);  -- PSC control, Auto negotiate disable
            --proc_eth1g_mem_mm_bus_wr(func_tech_tse_map_pcs_addr(16#00#), 16#1140#, v.tse_mosi);  -- PSC control, Auto negotiate enable
          end if;
          v.state := s_wr_tse_promis_en;
        end if;

      when s_wr_tse_promis_en        =>
        if tse_miso.waitrequest = '0' then
          v.tse_psc_access := '0';
          if c_promis_en = false then
            proc_eth1g_mem_mm_bus_wr(16#008#, 16#0100004B#, v.tse_mosi);  -- MAC control
          else
            proc_eth1g_mem_mm_bus_wr(16#008#, 16#0100005B#, v.tse_mosi);
          end if;
          v.state := s_wr_tse_mac_0;
        end if;

      when s_wr_tse_mac_0            =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#00C#, src_mac_0, v.tse_mosi);  -- MAC_0
          v.state := s_wr_tse_mac_1;
        end if;

      when s_wr_tse_mac_1            =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#010#, src_mac_1, v.tse_mosi);  -- MAC_1 <-- SRC_MAC
          v.state := s_wr_tse_tx_ipg_len;
        end if;

      when s_wr_tse_tx_ipg_len       =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#05C#, 16#0000000C#, v.tse_mosi);  -- TX_IPG_LENGTH <-- interpacket gap = 12
          v.state := s_wr_tse_frm_len;
        end if;

      when s_wr_tse_frm_len          =>
        if tse_miso.waitrequest = '0' then
          --proc_eth1g_mem_mm_bus_wr(16#014#, 16#000005EE#, v.tse_mosi);   -- FRM_LENGTH <-- receive max frame length = 1518
          proc_eth1g_mem_mm_bus_wr(16#014#, 16#0000233A#, v.tse_mosi);  -- FRM_LENGTH <-- receive max frame length = 9018
          v.state := s_wr_tse_rx_section_empty;
        end if;

      when s_wr_tse_rx_section_empty =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#01C#, c_tech_tse_rx_fifo_depth - 16, v.tse_mosi);  -- RX_SECTION_EMPTY <-- default FIFO depth - 16, >3
          v.state := s_wr_tse_rx_section_full;
        end if;

      when s_wr_tse_rx_section_full  =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#020#, 16, v.tse_mosi);  -- RX_SECTION_FULL  <-- default 16
          v.state := s_wr_tse_tx_section_empty;
        end if;

      when s_wr_tse_tx_section_empty =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#024#, c_tech_tse_tx_fifo_depth - 16, v.tse_mosi);  -- TX_SECTION_EMPTY <-- default FIFO depth - 16, >3
          v.state := s_wr_tse_tx_section_full;
        end if;

      when s_wr_tse_tx_section_full  =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#028#, 16, v.tse_mosi);  -- TX_SECTION_FULL  <-- default 16, >~ 8 otherwise no tx
          v.state := s_wr_tse_rx_almost_empty;
        end if;

      when s_wr_tse_rx_almost_empty  =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#02C#, 8, v.tse_mosi);  -- RX_ALMOST_EMPTY  <-- default 8
          v.state := s_wr_tse_rx_almost_full;
        end if;

      when s_wr_tse_rx_almost_full   =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#030#, 8, v.tse_mosi);  -- RX_ALMOST_FULL   <-- default 8
          v.state := s_wr_tse_tx_almost_empty;
        end if;

      when s_wr_tse_tx_almost_empty  =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#034#, 8, v.tse_mosi);  -- TX_ALMOST_EMPTY  <-- default 8
          v.state := s_wr_tse_tx_almost_full;
        end if;

      when s_wr_tse_tx_almost_full   =>
        if tse_miso.waitrequest = '0' then
          proc_eth1g_mem_mm_bus_wr(16#038#, c_tech_tse_tx_ready_latency + 3, v.tse_mosi);  -- TX_ALMOST_FULL   <-- default 3
          v.state := s_wait_interrupt_1;
          v.tse_init := '0';
        end if;

      -- -- start control loop -- --
      when s_wait_interrupt_1 =>
        if reg_interrupt = '1' then
          if lat_reg_rd = '0' then proc_eth1g_mem_mm_bus_rd(c_eth_reg_status_wi + 0, v.reg_mosi);  -- read status register to read the status
          elsif reg_rd_valid = '1' then
            eth_mm_reg_status <= func_eth_mm_reg_status(reg_miso.rddata);
            proc_eth1g_mem_mm_bus_wr(c_eth_reg_status_wi + 0, 0, v.reg_mosi);  -- write status register to acknowledge the interrupt
            v.state := s_wait_interrupt_0;
          end if;
        end if;

      when s_wait_interrupt_0 =>
        if reg_interrupt = '0' then
          -- prepare control register for response
          if c_reply_payload = true then
            eth_mm_reg_control.tx_nof_words <= INCR_UVEC(eth_mm_reg_status.rx_nof_words, -1);  -- -1 to skip the CRC word for the response
            eth_mm_reg_control.tx_empty     <= eth_mm_reg_status.rx_empty;
          else
            eth_mm_reg_control.tx_nof_words <= TO_UVEC(c_network_total_header_32b_nof_words, c_eth_max_frame_nof_words_w);
            eth_mm_reg_control.tx_empty     <= TO_UVEC(0, c_eth_empty_w);
          end if;
          eth_mm_reg_control.tx_en <= '1';
          eth_mm_reg_control.rx_en <= '1';

          -- TODO: check for data_type
          if c_reply_payload = true then
            v.ram_offset := func_tech_tse_header_size(data_type);
            v.state := s_rd_payload;
          else
            v.state := s_wr_control;
          end if;
        end if;

      when s_rd_payload   => proc_eth1g_mem_mm_bus_rd(c_eth_ram_rx_offset + v.ram_offset, v.ram_mosi);  v.state := s_wr_payload;
      when s_wr_payload =>
        if ram_rd_valid = '1' then
          proc_eth1g_mem_mm_bus_wr(c_eth_ram_tx_offset + v.ram_offset, TO_SINT(ram_miso.rddata(c_word_w - 1 downto 0)), v.ram_mosi);
          v.ram_offset := v.ram_offset + 1;
          if v.ram_offset = TO_UINT(eth_mm_reg_control.tx_nof_words) - 1 then v.state := s_wr_control; else v.state := s_rd_payload; end if;
        end if;

      when s_wr_control   =>
        v_eth_control_word := func_eth_mm_reg_control(eth_mm_reg_control);
        proc_eth1g_mem_mm_bus_wr(c_eth_reg_control_wi + 0, TO_UINT(v_eth_control_word), v.reg_mosi);  v.state := s_eth_continue;
      when s_eth_continue => proc_eth1g_mem_mm_bus_wr(c_eth_reg_continue_wi, 0, v.reg_mosi);  v.state := s_wait_interrupt_1;  -- write continue register to make the ETH module continue
      when others => null;
    end case;

    nxt_r <= v;
  end process p_comb;
end;

--ARCHITECTURE beh OF eth1g_master IS

--  -- ETH control
--  CONSTANT c_reply_payload  : BOOLEAN := TRUE;  -- TRUE copy rx payload into response payload, else header only (e.g. for ARP)

--  SIGNAL mm_init            : STD_LOGIC := '1';
--  SIGNAL eth_init           : STD_LOGIC := '1';  -- debug signal to view progress in Wave Window
--  SIGNAL tse_init           : STD_LOGIC := '1';  -- debug signal to view progress in Wave Window
--  SIGNAL tse_psc_access     : STD_LOGIC := '0';  -- debug signal to view when PSC registers in TSE are accessed

--  -- TSE constants
--  CONSTANT c_promis_en      : BOOLEAN := FALSE;   -- FALSE receive only frames for this src_mac and broadcast, TRUE receive all

--  -- Test bench supported packet data types
--  CONSTANT c_tb_tech_tse_data_type_symbols : NATURAL := 0;
--  CONSTANT c_tb_tech_tse_data_type_counter : NATURAL := 1;
--  CONSTANT c_tb_tech_tse_data_type_arp     : NATURAL := 2;
--  CONSTANT c_tb_tech_tse_data_type_ping    : NATURAL := 3;  -- over IP/ICMP
--  CONSTANT c_tb_tech_tse_data_type_udp     : NATURAL := 4;  -- over IP

--  -- ETH control
--  CONSTANT c_control_rx_en  : NATURAL := 2**c_eth_mm_reg_control_bi.rx_en;

--  -- . UDP header
--  CONSTANT c_udp_port_ctrl  : NATURAL := 11;                  -- ETH demux UDP for control
--  CONSTANT c_udp_port_st0   : NATURAL := 57;                  -- ETH demux UDP port 0
--  CONSTANT c_udp_port_st1   : NATURAL := 58;                  -- ETH demux UDP port 1
--  CONSTANT c_udp_port_st2   : NATURAL := 59;                  -- ETH demux UDP port 2
--  CONSTANT c_udp_port_en    : NATURAL := 16#10000#;           -- ETH demux UDP port enable bit 16

--  SIGNAL src_mac_hi         : STD_LOGIC_VECTOR(c_16-1 DOWNTO 0);
--  SIGNAL src_mac_lo         : STD_LOGIC_VECTOR(c_32-1 DOWNTO 0);

--  -- ETH MM registers interface
--  SIGNAL eth_mm_reg_control : t_eth_mm_reg_control;
--  SIGNAL eth_mm_reg_status  : t_eth_mm_reg_status;

--  SIGNAL data_type  : NATURAL := c_tb_tech_tse_data_type_ping;

--BEGIN

--  src_mac_hi <= src_mac(c_48-1 DOWNTO c_32);
--  src_mac_lo <= src_mac(c_32-1 DOWNTO 0);

--  --p_mm_init : PROCESS(mm_clk)
--  --BEGIN
--  --  IF rising_edge(mm_clk) THEN
--  --    IF mm_rst='0' THEN
--  --      mm_init <= '0';
--  --    END IF;
--  --  END IF;
--  --END PROCESS;
--  mm_init <= '0' WHEN rising_edge(mm_clk) AND mm_rst='0';   -- concurrent statement is equivalent to commented p_mm_init

--  p_eth_control : PROCESS
--    VARIABLE v_eth_control_word : STD_LOGIC_VECTOR(c_word_w-1 DOWNTO 0);
--  BEGIN
--    -- Reset only the control signals in the record, to reduce unnecessary logic usage
--    tse_mosi.wr      <= '0';
--    tse_mosi.rd      <= '0';
--    reg_mosi.wr      <= '0';
--    reg_mosi.rd      <= '0';
--    --ram_mosi.wr      <= '0';
--    --ram_mosi.rd      <= '0';
--    -- Reset entire record to avoid slv to integer conversion warnings on 'X'
--    --tse_mosi <= c_mem_mosi_rst;
--    --reg_mosi <= c_mem_mosi_rst;
--    ram_mosi <= c_mem_mosi_rst;

--    -- Wait for mm_rst release, use mm_init as synchronous equivalent of mm_rst
--    WAIT UNTIL mm_init='0';

--    ---------------------------------------------------------------------------
--    -- ETH setup
--    ---------------------------------------------------------------------------

--    -- Setup the DEMUX UDP
--    proc_mem_mm_bus_wr(c_eth_reg_demux_wi+0, c_udp_port_en+c_udp_port_st0, mm_clk, reg_miso, reg_mosi);  -- UDP port stream 0
--    proc_mem_mm_bus_wr(c_eth_reg_demux_wi+1, c_udp_port_en+c_udp_port_st1, mm_clk, reg_miso, reg_mosi);  -- UDP port stream 1
--    proc_mem_mm_bus_wr(c_eth_reg_demux_wi+2, c_udp_port_en+c_udp_port_st2, mm_clk, reg_miso, reg_mosi);  -- UDP port stream 2
--    proc_mem_mm_bus_rd(c_eth_reg_demux_wi+0,                               mm_clk, reg_miso, reg_mosi);
--    proc_mem_mm_bus_rd(c_eth_reg_demux_wi+1,                               mm_clk, reg_miso, reg_mosi);
--    proc_mem_mm_bus_rd(c_eth_reg_demux_wi+2,                               mm_clk, reg_miso, reg_mosi);

--    -- Setup the RX config
--    proc_mem_mm_bus_wr(c_eth_reg_config_wi+0, src_mac_lo,                  mm_clk, reg_miso, reg_mosi);  -- control MAC address lo word
--    proc_mem_mm_bus_wr(c_eth_reg_config_wi+1, src_mac_hi,                  mm_clk, reg_miso, reg_mosi);  -- control MAC address hi halfword
--    proc_mem_mm_bus_wr(c_eth_reg_config_wi+2, src_ip,                      mm_clk, reg_miso, reg_mosi);  -- control IP address
--    proc_mem_mm_bus_wr(c_eth_reg_config_wi+3, c_udp_port_ctrl,             mm_clk, reg_miso, reg_mosi);  -- control UDP port

--    -- Enable RX
--    proc_mem_mm_bus_wr(c_eth_reg_control_wi+0, c_control_rx_en,            mm_clk, reg_miso, reg_mosi);  -- control rx en
--    eth_init <= '0';

--    ---------------------------------------------------------------------------
--    -- TSE MAC setup
--    ---------------------------------------------------------------------------

--    --proc_tech_tse_setup(c_tech_select_default,
--    --                    c_promis_en, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tech_tse_tx_ready_latency,
--    --                    src_mac, tse_psc_access,
--    --                    mm_clk, tse_miso, tse_mosi);

--    proc_tech_tse_setup(c_promis_en, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tech_tse_tx_ready_latency,
--                        src_mac, tse_psc_access,
--                        mm_clk, tse_miso, tse_mosi);
--    tse_init <= '0';

--    ---------------------------------------------------------------------------
--    -- Ethernet Rx and Tx control
--    ---------------------------------------------------------------------------

--    WHILE TRUE LOOP
--      eth_mm_reg_status  <= c_eth_mm_reg_status_rst;
--      eth_mm_reg_control <= c_eth_mm_reg_control_rst;
--      -- wait for rx_avail interrupt
--      IF reg_interrupt='1' THEN
--        -- read status register to read the status
--        proc_mem_mm_bus_rd(c_eth_reg_status_wi+0, mm_clk, reg_miso, reg_mosi);  -- read result available in eth_mm_reg_status
--        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
--        eth_mm_reg_status <= func_eth_mm_reg_status(reg_miso.rddata);
--        WAIT UNTIL rising_edge(mm_clk);
--        -- write status register to acknowledge the interrupt
--        proc_mem_mm_bus_wr(c_eth_reg_status_wi+0, 0, mm_clk, reg_miso, reg_mosi);  -- void value
--        -- prepare control register for response
--        IF c_reply_payload=TRUE THEN
--          eth_mm_reg_control.tx_nof_words <= INCR_UVEC(eth_mm_reg_status.rx_nof_words, -1);  -- -1 to skip the CRC word for the response
--          eth_mm_reg_control.tx_empty     <= eth_mm_reg_status.rx_empty;
--        ELSE
--          eth_mm_reg_control.tx_nof_words <= TO_UVEC(c_network_total_header_32b_nof_words, c_eth_max_frame_nof_words_w);
--          eth_mm_reg_control.tx_empty     <= TO_UVEC(0, c_eth_empty_w);
--        END IF;
--        eth_mm_reg_control.tx_en <= '1';
--        eth_mm_reg_control.rx_en <= '1';
--        WAIT UNTIL rising_edge(mm_clk);
--        -- wait for interrupt removal due to status register read access
--        WHILE reg_interrupt='1' LOOP WAIT UNTIL rising_edge(mm_clk); END LOOP;
--        -- write control register to enable tx
--        IF c_reply_payload=TRUE THEN
--          -- . copy the received payload to the response payload (overwrite part of the default response header in case of raw ETH)
--          FOR I IN func_tech_tse_header_size(data_type) TO TO_UINT(eth_mm_reg_control.tx_nof_words)-1 LOOP
--            proc_mem_mm_bus_rd(c_eth_ram_rx_offset+I, mm_clk, ram_miso, ram_mosi);
--            proc_mem_mm_bus_rd_latency(c_mem_ram_rd_latency, mm_clk);
--            proc_mem_mm_bus_wr(c_eth_ram_tx_offset+I, TO_SINT(ram_miso.rddata(c_word_w-1 DOWNTO 0)), mm_clk, ram_miso, ram_mosi);
--          END LOOP;
--        --ELSE
--          -- . only reply header
--        END IF;
--        v_eth_control_word := func_eth_mm_reg_control(eth_mm_reg_control);
--        proc_mem_mm_bus_wr(c_eth_reg_control_wi+0, TO_UINT(v_eth_control_word),  mm_clk, reg_miso, reg_mosi);
--        -- write continue register to make the ETH module continue
--        proc_mem_mm_bus_wr(c_eth_reg_continue_wi, 0, mm_clk, reg_miso, reg_mosi);  -- void value
--      END IF;
--      WAIT UNTIL rising_edge(mm_clk);
--    END LOOP;

--    WAIT;
--  END PROCESS;
--END;
