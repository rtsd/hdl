--------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Author:
-- . Eric Kooistra
-- Purpose:
-- . Interleave g_nof_inputs parallel inputs into one serial output. This
--   functions is the inverse of dp_deinterleave_one_to_n.
-- Description:
-- . The src_out blocks size will be an integer multiple of g_nof_inputs, so
--   all snk_in_arr info and control fields have the same information.
--   Therefore the sosi info and control fields are take from snk_in_arr(0).
-- . The snk_in_arr data must arrive with >= g_nof_inputs-1 idle cycles
--   to allow the output to be serialized. The function does support siso
--   flow control.
-- . The snk_in_arr(0) can be output immediately, because if the input can
--   be valid (because ready was '1' in the previous clk cylce, RL=1) then
--   the output is allowed to be valid too (because the implementation is
--   combinatorial, any pipelining to ease timing closure is done separately
--   at the output). The timing diagram explains the flow control in relation
--   to snk_in_arr().valid and src_in.ready.
--
--                  clk  0  1  2  3  4  5  6  7  8
--         src_in.ready  1  1  0  1  1  1  1  1  1
--              r.ready     1  1  0  1  1  1  1  1
--              snk_in_arr [2]            [2]
--                         [1]            [1]
--              snk_in_arr [0]            [0]
--        snk_in_arr.valid  1  0  0  0  0  1  0  0
--             src_out cnt [0][1][2][2][0][0][1][2]
--           src_out.valid  1  1  0  1  0  1  1  1
--             shift_ready  0  0  0  1  1  0  0  1
--                                ^     ^
--           not ready in clk 2 --/     |
--     ready in clk 4 but no new data --/
--
-- Remark:
-- . The function needs r.ready because it also outputs when there is no valid
--   input. The r.ready is the previous src_in.ready, so this assumes RL = 1.
-- . The pair of dp_interleave_n_to_one.vhd and dp_deinterleave_one_to_n.vhd
--   is simpler than dp_interleave because it goes to and from 1. The dp_folder
--   only supports n that is power of 2.
--
entity dp_interleave_n_to_one is
  generic (
    g_pipeline      : natural := 1;  -- 0 for combinatorial, > 0 for registers
    g_nof_inputs    : natural
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_out_arr : out t_dp_siso_arr(g_nof_inputs - 1 downto 0);
    snk_in_arr  : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    src_in      : in  t_dp_siso := c_dp_siso_rdy;
    src_out     : out t_dp_sosi
  );
end dp_interleave_n_to_one;

architecture rtl of dp_interleave_n_to_one is
  -- local state registers
  type t_reg is record
    ready       : std_logic;
    cnt_en      : std_logic;
    cnt         : natural range 0 to g_nof_inputs - 1;
    input_arr   : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
  end record;

  signal s            : t_reg;  -- combinatoral state
  signal r            : t_reg;  -- register state

  signal shift_ready  : std_logic;
  signal snk_out      : t_dp_siso;
  signal out_siso     : t_dp_siso;
  signal out_sosi     : t_dp_sosi;
begin
  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      r.ready     <= '0';
      r.cnt_en    <= '0';
      r.cnt       <= 0;
      r.input_arr <= (others => c_dp_sosi_rst);
    elsif rising_edge(clk) then
      r <= s;
    end if;
  end process;

  -- Register external flow control to get previous src_in.ready for RL = 1
  s.ready <= out_siso.ready;

  -- Count every g_nof_inputs valid input samples in block
  p_cnt : process(snk_in_arr, r)
  begin
    s.cnt_en <= r.cnt_en;
    s.cnt <= r.cnt;
    if snk_in_arr(0).valid = '1' then
      s.cnt_en <= '1';  -- enable cnt for this valid g_nof_inputs
      s.cnt <= 0;  -- wrap or force restart at valid
    elsif r.ready = '1' then
      if r.cnt_en = '1' then
        if r.cnt < g_nof_inputs - 1 then
          s.cnt <= r.cnt + 1;
        else
          s.cnt_en <= '0';
          s.cnt <= 0;  -- wrap after every g_nof_inputs valid output samples
        end if;
      end if;
    end if;
  end process;

  -- Use input_arr to load and shift parallel input via input_arr(0)
  p_input_arr : process(snk_in_arr, s, r)
  begin
    s.input_arr <= r.input_arr;  -- default hold snk_in_arr in input_arr
    if snk_in_arr(0).valid = '1'THEN
      s.input_arr <= snk_in_arr;  -- load valid snk_in_arr
    elsif r.ready = '1' then
      if s.cnt > 0 then
        s.input_arr(g_nof_inputs - 2 downto 0) <= r.input_arr(g_nof_inputs - 1 downto 1);  -- Shift out in series via input_arr(0)
      end if;
    end if;
  end process;

  -- Use input_arr to load and shift parallel input via input_arr(0)
  p_out_sosi : process(snk_in_arr, s, r)
  begin
    -- Default pass on data
    out_sosi       <= s.input_arr(0);
    out_sosi.valid <= '0';
    out_sosi.sync  <= '0';
    out_sosi.sop   <= '0';
    out_sosi.eop   <= '0';

    if snk_in_arr(0).valid = '1'THEN
      -- Output first of parallel g_nof_inputs
      out_sosi <= s.input_arr(0);  -- first can output sync, sop
      if g_nof_inputs > 1 then
        out_sosi.eop <= '0';  -- output eop at last of g_nof_inputs
      end if;

    elsif r.ready = '1' then
      if s.cnt > 0 then
        -- Output remaining of parallel g_nof_inputs in series
        out_sosi <= s.input_arr(0);
        out_sosi.sync <= '0';  -- sync was output at first
        out_sosi.sop  <= '0';  -- sop was output at first
        if s.cnt < g_nof_inputs - 1 then
          out_sosi.eop <= '0';  -- output eop at last of g_nof_inputs
        end if;
      end if;
    end if;
  end process;

  -- Pass on external flow control together with local flow control
  -- . Local control of flow control
  shift_ready <= '0' when s.cnt_en = '1' and s.cnt < g_nof_inputs - 1 else '1';

  -- . Combine external flow control with local flow control
  snk_out.ready <= out_siso.ready and shift_ready;
  snk_out.xon   <= out_siso.xon;

  -- . all snk_out_arr have the same siso, so wire snk_out to all
  snk_out_arr <= (others => snk_out);

  -- Pipeline output to easy timing closure
  u_pipeline_output : entity work.dp_pipeline
  generic map (
    g_pipeline => g_pipeline  -- 0 for wires, > 0 for registers
  )
  port map (
    rst      => rst,
    clk      => clk,
    -- ST sink
    snk_out  => out_siso,
    snk_in   => out_sosi,
    -- ST source
    src_in   => src_in,
    src_out  => src_out
  );
end rtl;
