# ##########################################################################
# Copyright 2024
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################
# Author:
# . Reinier vd Walle
#
# Purpose:
# . Provide abstraction layer for using DP bus (sosi/siso) in cocotb.
#
# Description:
# . This file contains classes to create an abstraction layer for 
#   interacting with DP sosi/siso signals in cocotb testbenches.
# . The classes are copied from AvalonSTPkts in [1] and slightly adapted to 
#   use sosi/siso signals
#
# References:
# [1] - https://docs.cocotb.org/en/v1.5.1/_modules/cocotb_bus/drivers/avalon.html
# ##########################################################################
from typing import Iterable, Union, Optional

import cocotb
from cocotb.utils import hexdump
from cocotb.triggers import RisingEdge, ReadOnly
from cocotb.binary import BinaryValue
from cocotb.result import TestError
from cocotb_bus.drivers import ValidatedBusDriver
from cocotb_bus.monitors import BusMonitor

class SosiDriver(ValidatedBusDriver):
    _optional_signals = ['valid', 'sop', 'eop', 'sync', 'bsn', 'data', 're', 'im', 'empty', 'channel', 'err']
    _signals = []
    _default_config = {
        "dataBitsPerSymbol"             : 8,
        "firstSymbolInHighOrderBits"    : True,
        "maxChannel"                    : 1,
        "readyLatency"                  : 1
    }
    def __init__(self, dut, name, clk, data_w = None, bsn_init = 0):
        ValidatedBusDriver.__init__(self, dut, name, clk, bus_separator='.')    
        self.config = self._default_config.copy()
        self.use_empty = True
        self.config["useEmpty"] = self.use_empty
        if data_w == None:
            self.data_w = len(self.bus.data)
        else:
            self.data_w = data_w
        self.bsn = bsn_init

    async def _wait_ready(self):
        """Wait for a ready cycle on the bus before continuing.

            Can no longer drive values this cycle...

            FIXME assumes readyLatency of 0
        """
        await ReadOnly()
        while False: #not self.bus.ready.value:
            await RisingEdge(self.clock)
            await ReadOnly()

    async def _send_string(self, string: bytes, sync: bool = True, channel: Optional[int] = None) -> None:
        """Args:
            string: A string of bytes to send over the bus.
            channel: Channel to send the data on.
        """
        # Avoid spurious object creation by recycling
        clkedge = RisingEdge(self.clock)
        firstword = True

        # FIXME: buses that aren't an integer numbers of bytes
        bus_width = int(self.data_w / 8)
        
        word = BinaryValue(n_bits=len(self.bus.data),
                           bigEndian=self.config["firstSymbolInHighOrderBits"])

        single = BinaryValue(n_bits=1, bigEndian=False)
        if self.use_empty:
            empty = BinaryValue(n_bits=len(self.bus.empty), bigEndian=False)

        # Drive some defaults since we don't know what state we're in
        if self.use_empty:
            self.bus.empty.value = 0
        self.bus.sop.value = 0
        self.bus.eop.value = 0
        self.bus.valid.value = 0
        if hasattr(self.bus, 'err'):
            self.bus.err.value = 0

        if hasattr(self.bus, 'channel'):
            self.bus.channel.value = 0
        elif channel is not None:
            raise TestError("%s does not have a channel signal" % self.name)

        while string:
            if not firstword or (firstword and sync):
                await clkedge

            # Insert a gap where valid is low
            if not self.on:
                self.bus.valid.value = 0
                for _ in range(self.off):
                    await clkedge

                # Grab the next set of on/off values
                self._next_valids()

            # Consume a valid cycle
            if self.on is not True and self.on:
                self.on -= 1

            self.bus.valid.value = 1
            if hasattr(self.bus, 'channel'):
                if channel is None:
                    self.bus.channel.value = 0
                elif channel > self.config['maxChannel'] or channel < 0:
                    raise TestError("%s: Channel value %d is outside range 0-%d" %
                                    (self.name, channel, self.config['maxChannel']))
                else:
                    self.bus.channel.value = channel

            if firstword:
                self.bus.sop.value = 1
                self.bus.bsn.value = self.bsn
                self.bsn += 1
                firstword = False
            else:
                self.bus.sop.value = 0

            nbytes = min(len(string), bus_width)
            data = string[:nbytes]

            # set unused bits of dp_sosi.data field to 0
            data = bytes(word.n_bits // 8 - bus_width) + data        
            word.buff = data

            if len(string) <= bus_width:
                self.bus.eop.value = 1
                if self.use_empty:
                    self.bus.empty.value = bus_width - len(string)
                string = b""
            else:
                string = string[bus_width:]

            self.bus.data.value = word

            # If this is a bus with a ready signal, wait for this word to
            # be acknowledged
            if hasattr(self.bus, "ready"):
                await self._wait_ready()

        await clkedge
        self.bus.valid.value = 0
        self.bus.eop.value = 0
        word.binstr   = "x" * self.data_w
        single.binstr = "x"
        self.bus.data.value = word
        self.bus.sop.value = single
        self.bus.eop.value = single

        if self.use_empty:
            empty.binstr = "x" * len(self.bus.empty)
            self.bus.empty.value = empty
        if hasattr(self.bus, 'channel'):
            channel_value = BinaryValue(n_bits=len(self.bus.channel), bigEndian=False,
                                        value="x" * len(self.bus.channel))
            self.bus.channel.value = channel_value

    async def _send_iterable(self, pkt: Iterable, sync: bool = True) -> None:
        """Args:
            pkt: Will yield objects with attributes matching the
                signal names for each individual bus cycle.
        """
        clkedge = RisingEdge(self.clock)
        firstword = True

        for word in pkt:
            if not firstword or (firstword and sync):
                await clkedge

            firstword = False

            # Insert a gap where valid is low
            if not self.on:
                self.bus.valid.value = 0
                for _ in range(self.off):
                    await clkedge

                # Grab the next set of on/off values
                self._next_valids()

            # Consume a valid cycle
            if self.on is not True and self.on:
                self.on -= 1

            if not hasattr(word, "valid"):
                self.bus.valid.value = 1
            else:
                self.bus.value = word

            # Wait for valid words to be acknowledged
            if not hasattr(word, "valid") or word.valid:
                if hasattr(self.bus, "ready"):
                    await self._wait_ready()

        await clkedge
        self.bus.valid.value = 0

    async def _driver_send(self, pkt: Union[bytes, Iterable], sync: bool = True, channel: Optional[int] = None):
        """Send a packet over the bus.

        Args:
            pkt: Packet to drive onto the bus.
            channel: Channel attributed to the packet.

        If ``pkt`` is a string, we simply send it word by word

        If ``pkt`` is an iterable, it's assumed to yield objects with
        attributes matching the signal names.
        """

        # Avoid spurious object creation by recycling
        if isinstance(pkt, bytes):
            self.log.debug("Sending packet of length %d bytes", len(pkt))
            self.log.debug(hexdump(pkt))
            await self._send_string(pkt, sync=sync, channel=channel)
            self.log.debug("Successfully sent packet of length %d bytes", len(pkt))
        elif isinstance(pkt, str):
            raise TypeError("pkt must be a bytestring, not a unicode string")
        else:
            if channel is not None:
                self.log.warning("%s is ignoring channel=%d because pkt is an iterable", self.name, channel)
            await self._send_iterable(pkt, sync=sync)

class SisoDriver(ValidatedBusDriver):
    _optional_signals = ['ready', 'xon']
    _signals = []
    def __init__(self, dut, name, clk):
        ValidatedBusDriver.__init__(self, dut, name, clk, bus_separator='.')        

class SosiMonitor(BusMonitor):
    _optional_signals = ['valid', 'sop', 'eop', 'sync', 'bsn', 'data', 're', 'im', 'empty', 'channel', 'err']
    _signals = []

    _default_config = {
        "dataBitsPerSymbol"             : 8,
        "firstSymbolInHighOrderBits"    : True,
        "maxChannel"                    : 1,
        "readyLatency"                  : 1,
        "invalidTimeout"                : 0,
    }
    def __init__(self, dut, name, clk, data_w=None, reset=None, reset_n=None, report_channel=False):
        BusMonitor.__init__(self, dut, name, clk, reset=reset, reset_n=reset_n, bus_separator='.')
        self.config = self._default_config.copy()
        self.use_empty = True
        self.config["useEmpty"] = self.use_empty
        self.report_channel = report_channel
        self.data_w = data_w
    async def _monitor_recv(self):
        """Watch the pins and reconstruct transactions."""

        # Avoid spurious object creation by recycling
        clkedge = RisingEdge(self.clock)
        pkt = b""
        in_pkt = False
        invalid_cyclecount = 0
        channel = None

        def valid():
            if hasattr(self.bus, 'ready'):
                return self.bus.valid.value and self.bus.ready.value
            return self.bus.valid.value

        while True:
            await clkedge

            if self.in_reset:
                continue

            if valid():
                invalid_cyclecount = 0

                if self.bus.sop.value:
                    if pkt:
                        raise Exception("Duplicate start-of-packet received on %s" %
                                                  str(self.bus.sop))
                    pkt = b""
                    in_pkt = True

                if not in_pkt:
                    raise Exception("Data transfer outside of "
                                              "packet")

                # Handle empty and X's in empty / data
                vec = BinaryValue()
                if not self.bus.eop.value:
                    value = self.bus.data.value.get_binstr()[-1*self.data_w:]
                    vec.assign(value)
                else:
                    value = self.bus.data.value.get_binstr()[-1*self.data_w:]
                    if self.config["useEmpty"] and self.bus.empty.value.integer:
                        empty = self.bus.empty.value.integer * self.config["dataBitsPerSymbol"]
                        if self.config["firstSymbolInHighOrderBits"]:
                            value = value[:-empty]
                        else:
                            value = value[empty:]

                    vec.assign(value)
                    if not vec.is_resolvable:
                        raise Exception("After empty masking value is still bad?  "
                                                  "Had empty {:d}, got value {:s}".format(empty,
                                                                                          self.bus.data.value.get_binstr()))
                vec.big_endian = self.config['firstSymbolInHighOrderBits']
                pkt += vec.buff

                if hasattr(self.bus, 'channel'):
                    if channel is None:
                        channel = self.bus.channel.value.integer
                        if channel > self.config["maxChannel"]:
                            raise Exception("Channel value (%d) is greater than maxChannel (%d)" %
                                                      (channel, self.config["maxChannel"]))
                    elif self.bus.channel.value.integer != channel:
                        raise Exception("Channel value changed during packet")

                if self.bus.eop.value:
                    self.log.info("Received a packet of %d bytes", len(pkt))
                    self.log.debug(hexdump(pkt))
                    self.channel = channel
                    if self.report_channel:
                        self._recv({"data": pkt, "channel": channel})
                    else:
                        self._recv(pkt)
                    pkt = b""
                    in_pkt = False
                    channel = None
            else:
                if in_pkt:
                    invalid_cyclecount += 1
                    if self.config["invalidTimeout"]:
                        if invalid_cyclecount >= self.config["invalidTimeout"]:
                            raise Exception(
                                "In-Packet Timeout. Didn't receive any valid data for %d cycles!" %
                                invalid_cyclecount)

class SisoMonitor(BusMonitor):
    _optional_signals = ['ready', 'xon']
    _signals = []
    def __init__(self, dut, name, clk, reset=None, reset_n=None):
        BusMonitor.__init__(self, dut, name, clk, reset=reset, reset_n=reset_n, bus_separator='.')
        self.clk = clk

    async def _monitor_recv(self):
        """Watch the pins and reconstruct transactions."""

        # Avoid spurious object creation by recycling
        clkedge = RisingEdge(self.clock)

        # NB could await on valid here more efficiently?
        while True:
            await clkedge            
            self._recv({"ready": self.bus.ready.value, "xon": self.bus.xon.value})        

class DpStream:
    # at the time, cocotb_bus does not officially support VHDL record type bus. 
    # It does work in the following way:
    # 1. Only use _optional_signals, no _signals.
    # 2. Use bus_separator='.'
    def __init__(self, dut, sosi_name, siso_name, clk, rst, data_w = None, bsn_init = 0):
        self.sosi_drv = SosiDriver(dut, sosi_name, clk, data_w, bsn_init)
        self.siso_drv = SisoDriver(dut, siso_name, clk)
        self.sosi_mon = SosiMonitor(dut, sosi_name, clk, data_w, reset=rst)
        self.siso_mon = SisoMonitor(dut, siso_name, clk, reset=rst)
        cocotb.start_soon(self.siso_mon._monitor_recv())
