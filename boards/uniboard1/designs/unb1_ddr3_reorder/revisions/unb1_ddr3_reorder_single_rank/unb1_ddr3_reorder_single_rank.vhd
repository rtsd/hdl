-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, technology_lib, tech_ddr_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;

entity unb1_ddr3_reorder_single_rank is
  generic (
    g_design_name : string       := "unb1_ddr3_reorder_single_rank";  -- use revision name = entity name = design name
    g_design_note : string       := "Reference Reorder";
    g_sim         : boolean      := false;
    g_sim_unb_nr  : natural      := 0;
    g_sim_node_nr : natural      := 4;
    -- Stamps are passed via QIP at compile start if $UNB_COMPILE_STAMPS is set
    g_stamp_date  : natural      := 0;  -- Date (YYYYMMDD)
    g_stamp_time  : natural      := 0;  -- Time (HHMMSS)
    g_stamp_svn   : natural      := 0;  -- SVN revision
    g_use_MB_I    : natural      := 1;  -- 1: use MB_I  0: do not use
    g_tech_ddr    : t_c_tech_ddr := c_tech_ddr3_4g_single_rank_800m_master;
    g_aux         : t_c_unb1_board_aux := c_unb1_board_aux
  );
  port (
    -- GENERAL
    CLK           : in    std_logic;  -- System Clock
    PPS           : in    std_logic;  -- System Sync
    WDI           : out   std_logic;  -- Watchdog Clear
    INTA          : inout std_logic;  -- FPGA interconnect line
    INTB          : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION       : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID            : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO        : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc       : inout std_logic;
    sens_sd       : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk       : in    std_logic;
    ETH_SGIN      : in    std_logic;
    ETH_SGOUT     : out   std_logic;

    -- SO-DIMM Memory Bank I
    MB_I_IN       : in    t_tech_ddr3_phy_in;
    MB_I_IO       : inout t_tech_ddr3_phy_io;
    MB_I_OU       : out   t_tech_ddr3_phy_ou
  );
end unb1_ddr3_reorder_single_rank;

architecture str of unb1_ddr3_reorder_single_rank is
begin
  u_revision : entity work.unb1_ddr3_reorder
  generic map(
    g_design_name => g_design_name,
    g_design_note => g_design_note,
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr,
    g_stamp_date  => g_stamp_date,
    g_stamp_time  => g_stamp_time,
    g_stamp_svn   => g_stamp_svn,
    g_use_MB_I    => g_use_MB_I,
    g_tech_ddr    => g_tech_ddr,
    g_aux         => g_aux
  )
  port map(
    CLK       => CLK,
    PPS       => PPS,
    WDI       => WDI,
    INTA      => INTA,
    INTB      => INTB,
    VERSION   => VERSION,
    ID        => ID,
    TESTIO    => TESTIO,
    sens_sc   => sens_sc,
    sens_sd   => sens_sd,
    ETH_clk   => ETH_clk,
    ETH_SGIN  => ETH_SGIN,
    ETH_SGOUT => ETH_SGOUT,
    MB_I_IN   => MB_I_IN,
    MB_I_IO   => MB_I_IO,
    MB_I_OU   => MB_I_OU
  );
end str;
