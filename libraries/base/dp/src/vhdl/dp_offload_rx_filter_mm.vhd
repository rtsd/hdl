-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;

entity dp_offload_rx_filter_mm is
  generic (
    g_bypass              : boolean  := false;
    g_nof_streams         : positive := 1;
    g_data_w              : natural;
    g_hdr_field_arr       : t_common_field_arr
  );
  port (
    dp_rst            : in  std_logic;
    dp_clk            : in  std_logic;

    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;

    reg_dp_offload_rx_filter_hdr_fields_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_offload_rx_filter_hdr_fields_miso  : out t_mem_miso;

    snk_in_arr        : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    snk_out_arr       : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    src_out_arr       : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_in_arr        : in  t_dp_siso_arr(g_nof_streams - 1 downto 0);

    hdr_fields_to_check_arr: in  t_slv_1024_arr(g_nof_streams - 1 downto 0);

    hdr_fields_val    : in std_logic
  );
end dp_offload_rx_filter_mm;

architecture str of dp_offload_rx_filter_mm  is
  constant c_header_w : natural := field_slv_in_len(field_arr_set_mode(g_hdr_field_arr,  "RO"));
  constant c_nof_words: natural := c_header_w / g_data_w;

  constant c_head : natural := 1;
  constant c_tail : natural := 0;

  constant c_field_slv_out_w : natural := field_slv_out_len(field_arr_set_mode(g_hdr_field_arr,  "RW"));

  constant c_nof_ena         : natural := 4;
  constant c_reg_w           : natural := c_nof_ena * c_word_w;
  constant c_adr_w           : natural := ceil_log2(c_nof_ena);
  constant c_ena_reg         : t_c_mem := (1, c_adr_w, 32, c_nof_ena, '0');

  subtype eth_dst_mac_range     is natural range field_hi(g_hdr_field_arr, "eth_dst_mac"    ) downto field_lo(g_hdr_field_arr, "eth_dst_mac");
  subtype ip_total_length_range is natural range field_hi(g_hdr_field_arr, "ip_total_length") downto field_lo(g_hdr_field_arr, "ip_total_length");
  subtype ip_dst_addr_range     is natural range field_hi(g_hdr_field_arr, "ip_dst_addr"    ) downto field_lo(g_hdr_field_arr, "ip_dst_addr");
  subtype udp_dst_port_range    is natural range field_hi(g_hdr_field_arr, "udp_dst_port"   ) downto field_lo(g_hdr_field_arr, "udp_dst_port");

  type    t_field_slv_in_arr    is array (integer range <>) of std_logic_vector(field_slv_in_len(g_hdr_field_arr) - 1 downto 0);
  type    t_field_slv_out_arr   is array (integer range <>) of std_logic_vector(c_field_slv_out_w - 1 downto 0);

  signal  mm_fields_slv_in_arr    : t_field_slv_in_arr(g_nof_streams - 1 downto 0);
  signal  mm_fields_slv_out_arr   : t_field_slv_out_arr(g_nof_streams - 1 downto 0);

  signal  mult_streams_mosi_arr   : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal  mult_streams_miso_arr   : t_mem_miso_arr(g_nof_streams - 1 downto 0);

  signal  common_mosi_arr         : t_mem_mosi_arr(1 downto 0);
  signal  common_miso_arr         : t_mem_miso_arr(1 downto 0);

  type t_bool_arr    is array (integer range <>) of boolean;
  type t_reg_sig_arr is array (integer range <>) of std_logic_vector(c_reg_w - 1 downto 0);

  signal  reg_ena_sig             : t_reg_sig_arr(g_nof_streams - 1 downto 0);

  signal  eth_dst_mac_ena         : t_bool_arr(g_nof_streams - 1 downto 0);
  signal  ip_dst_addr_ena         : t_bool_arr(g_nof_streams - 1 downto 0);
  signal  ip_total_length_ena     : t_bool_arr(g_nof_streams - 1 downto 0);
  signal  udp_dst_port_ena        : t_bool_arr(g_nof_streams - 1 downto 0);

  type reg_type is record
    valid       : std_logic_vector(g_nof_streams - 1 downto 0);
    src_out_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  end record;

  signal r, rin : reg_type;
begin
  snk_out_arr <= src_in_arr;

  gen_bypass : if g_bypass = true generate
    src_out_arr <= snk_in_arr;
  end generate;

  no_bypass : if g_bypass = false generate
    p_comb : process(hdr_fields_to_check_arr, snk_in_arr, r, dp_rst)
      variable v : reg_type;
    begin
      v := r;

      v.src_out_arr := snk_in_arr;
      check : for i in 0 to g_nof_streams - 1 loop
        if snk_in_arr(i).sop = '1' then
          v.valid(i) := '1';

          if eth_dst_mac_ena(i)     = true and not(hdr_fields_to_check_arr(i)(eth_dst_mac_range)     = mm_fields_slv_out_arr(i)(eth_dst_mac_range)     ) then
            v.valid(i) := '0';
          end if;

          if ip_total_length_ena(i) = true and not(hdr_fields_to_check_arr(i)(ip_total_length_range) = mm_fields_slv_out_arr(i)(ip_total_length_range) ) then
            v.valid(i) := '0';
          end if;

          if ip_dst_addr_ena(i)     = true and not(hdr_fields_to_check_arr(i)(ip_dst_addr_range)     = mm_fields_slv_out_arr(i)(ip_dst_addr_range)     ) then
            v.valid(i) := '0';
          end if;

          if udp_dst_port_ena(i)    = true and not(hdr_fields_to_check_arr(i)(udp_dst_port_range)    = mm_fields_slv_out_arr(i)(udp_dst_port_range)    ) then
            v.valid(i) := '0';
          end if;
        end if;
      end loop check;

      if(dp_rst = '1') then
        v.src_out_arr := (others => c_dp_sosi_rst);
        v.valid       := (others => '0');
      end if;

      rin <= v;
    end process;

    process(r)
    begin
      src_out_arr   <= r.src_out_arr;
      set_val : for i in 0 to g_nof_streams - 1 loop
        src_out_arr(i).valid <= r.valid(i) and r.src_out_arr(i).valid;
        src_out_arr(i).sop   <= r.valid(i) and r.src_out_arr(i).sop;
        src_out_arr(i).eop   <= r.valid(i) and r.src_out_arr(i).eop;
      end loop set_val;
    end process;

    p_regs : process(dp_clk)
    begin
      if rising_edge(dp_clk) then
        r <= rin;
      end if;
    end process;

    -------------------------------------------
    -- mm_fields for MM access to each field --
    -------------------------------------------
    u_mult_mem_mux : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => g_nof_streams,
      g_mult_addr_w => ceil_log2(field_nof_words(g_hdr_field_arr, c_word_w)) + 1
    )
    port map (
      mosi     => reg_dp_offload_rx_filter_hdr_fields_mosi,
      miso     => reg_dp_offload_rx_filter_hdr_fields_miso,
      mosi_arr => mult_streams_mosi_arr,
      miso_arr => mult_streams_miso_arr
    );

    gen_mm_fields : for i in 0 to g_nof_streams - 1 generate
      u_common_mem_mux : entity common_lib.common_mem_mux
      generic map (
        g_nof_mosi    => 2,
        g_mult_addr_w => ceil_log2(field_nof_words(g_hdr_field_arr, c_word_w))
      )
      port map (
        mosi     => mult_streams_mosi_arr(i),
        miso     => mult_streams_miso_arr(i),
        mosi_arr => common_mosi_arr,
        miso_arr => common_miso_arr
      );

      eth_dst_mac_ena(i)     <= is_true(reg_ena_sig(i)(0));
      ip_dst_addr_ena(i)     <= is_true(reg_ena_sig(i)(32));
      ip_total_length_ena(i) <= is_true(reg_ena_sig(i)(64));
      udp_dst_port_ena(i)    <= is_true(reg_ena_sig(i)(96));

      cr : entity common_lib.common_reg_r_w_dc
      generic map(
        g_reg       => c_ena_reg
      )
      port map(
        -- Clocks and reset
        mm_rst      => mm_rst,  -- reset synchronous with mm_clk
        mm_clk      => mm_clk,  -- memory-mapped bus clock
        st_rst      => dp_rst,  -- reset synchronous with st_clk
        st_clk      => dp_clk,  -- other clock domain clock

        -- Memory Mapped Slave in mm_clk domain
        sla_in      => common_mosi_arr(1),  -- IN  t_mem_mosi;  -- actual ranges defined by g_reg
        sla_out     => common_miso_arr(1),  -- OUT t_mem_miso;  -- actual ranges defined by g_reg

        -- MM registers in st_clk domain
        reg_wr_arr  => OPEN,
        reg_rd_arr  => OPEN,
        in_new      => OPEN,
        in_reg      => reg_ena_sig(i),
        out_reg     => reg_ena_sig(i),
        out_new     => open
      );

      u_mm_fields_slv: entity mm_lib.mm_fields
      generic map(
        g_field_arr => field_arr_set_mode(g_hdr_field_arr,  "RW")
      )
      port map (
        mm_clk     => mm_clk,
        mm_rst     => mm_rst,

        mm_mosi    => common_mosi_arr(0),
        mm_miso    => common_miso_arr(0),

        slv_clk    => dp_clk,
        slv_rst    => dp_rst,

        slv_in     => mm_fields_slv_in_arr(i),
        slv_in_val => hdr_fields_val,
        slv_out    => mm_fields_slv_out_arr(i)
      );
    end generate;

  end generate;
end str;
