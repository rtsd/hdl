-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
-- =======
-- This component can be added to an uninterruptible source to provide the
-- stream with some flow control based on the ready signal of the
-- connected sink.

entity dp_ready is
  generic (
    g_ready_latency : natural := 1  -- >= 0
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso;
    src_out      : out t_dp_sosi
  );
end dp_ready;

architecture rtl of dp_ready is
  signal reg_ready : std_logic_vector(g_ready_latency downto 0);
  signal reg_val   : std_logic;
begin
  gen_latency : if g_ready_latency > 0 generate
    reg_ready(0) <= src_in.ready;  -- use reg_ready(0) to combinatorially store src_in.ready

    p_clk : process(rst, clk)
    begin
      if rst = '1' then
        reg_ready(g_ready_latency downto 1) <= (others => '0');
      elsif rising_edge(clk) then
        reg_ready(g_ready_latency downto 1) <= reg_ready(g_ready_latency - 1 downto 0);
      end if;
    end process;

    reg_val <= reg_ready(g_ready_latency);
  end generate gen_latency;

  no_latency : if g_ready_latency = 0 generate
    reg_val <= src_in.ready;
  end generate;

  p_src_out : process(snk_in, reg_val)
  begin
    src_out       <= snk_in;
    src_out.sync  <= snk_in.sync  and reg_val;
    src_out.valid <= snk_in.valid and reg_val;
    src_out.sop   <= snk_in.sop   and reg_val;
    src_out.eop   <= snk_in.eop   and reg_val;
  end process;
end rtl;
