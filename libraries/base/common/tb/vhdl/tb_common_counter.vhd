-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_counter is
end tb_common_counter;

architecture tb of tb_common_counter is
  constant clk_period   : time := 10 ns;

  constant c_cnt_init   : natural := 3;
  constant c_cnt_w      : natural := 5;

  signal rst      : std_logic;
  signal clk      : std_logic := '0';

  signal cnt_clr  : std_logic := '0';  -- synchronous cnt_clr is only interpreted when clken is active
  signal cnt_ld   : std_logic := '0';  -- cnt_ld loads the output count with the input load value, independent of cnt_en
  signal cnt_en   : std_logic := '1';
  signal load     : std_logic_vector(c_cnt_w - 1 downto 0) := TO_UVEC(c_cnt_init, c_cnt_w);
  signal count    : std_logic_vector(c_cnt_w - 1 downto 0);
  signal cnt_max  : std_logic_vector(c_cnt_w - 1 downto 0);
begin
  clk <= not clk after clk_period / 2;
  rst <= '1', '0' after clk_period * 3;

  -- run 1 us
  p_in_stimuli : process
  begin
    cnt_clr <= '0';
    cnt_ld  <= '0';
    cnt_en  <= '0';
    cnt_max <= (others => '0');
    wait until rst = '0';
    wait until rising_edge(clk);

    -- Start counting
    cnt_en  <= '1';
    for I in 0 to 9 loop
      wait until rising_edge(clk);
    end loop;

    -- Reload counter
    cnt_ld  <= '1';
    wait until rising_edge(clk);
    cnt_ld  <= '0';
    for I in 0 to 9 loop
      wait until rising_edge(clk);
    end loop;

    -- briefly stop counting
    cnt_en  <= '0';
    wait until rising_edge(clk);
    -- countine counting
    cnt_en  <= '1';
    for I in 0 to 9 loop
      wait until rising_edge(clk);
    end loop;

    -- set the cnt_max
    cnt_max <= TO_UVEC(2**(c_cnt_w - 1), c_cnt_w);

    wait;
  end process;

  -- device under test
  u_dut : entity work.common_counter
  generic map (
    g_init      => c_cnt_init,
    g_width     => c_cnt_w,
    g_step_size => 1
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => cnt_clr,
    cnt_ld  => cnt_ld,
    cnt_en  => cnt_en,
    cnt_max => cnt_max,
    load    => load,
    count   => count
  );
end tb;
