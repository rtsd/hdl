%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Run pfir_coeff
% Description :
%   Plot FIR filter coefficients and transfer function for different
%   applications:
%   . 'lofar_subband';
%   . 'apertif_subband';
%   . 'apertif_channel';
%   . 'arts_channel';
%   . 'bypass_channel';
%   . 'bypass_subband';
%   . 'incrementing_channel';
%
%   - For reference the design can also plot the LOFAR station subband
%     FIR coefficients from file.
%   - The design can use fir1 or fircls1 to calculate the FIR filter 
%     coefficients. The fir1 has a flat passband, but the fircls1 
%     achieves a steeper pass-stop transition and in the combination a
%     more flat gain response between channels. Therefore fircls1 is
%     preferred when r_pass = 0.0001 and for r_pass = 0.001 it is better
%     to choose fir1.
%   - The half power channel bandwidth of BWchan can be set via
%     config.hp_factor. Default config.hp_factor = 1 and then the channel
%     bandwidth is equal to the channel rate, so Wchan = fchan.
%     The config.hp_factor to have flat power gain between channels is
%     determined automatically when config.hp_adjust = true.
%   - Default the DC response of the FIR filter per polyphase section of
%     L taps will not be the same for all N polyphase sections. Especially
%     for large N > 64 the outer polyphase sections will deviate
%     significantly compared to the polyphase sections in the middle.
%     When config.dc_adjust = true then the DC response of all polyphase
%     sections will be made equal.
%   - For fircls1 the passband ripple and stopband ripple can be specified.
%     For fir1 the passband is flat and the stopband attenuation increases
%     more and more for more distant frequencies. The fir1 can achieve
%     suffcient r_stop at the next channel. The passband and stopband
%     characteristics are only ideal when coeff_w=0 (doubles). The stopband
%     attenuation r_stop ~= coeff_w * -6 dB. The passband ripple also
%     depends on coeff_w r_pass ~= 1 + 0.5/2^coeff_w.
%
%   See pfir_coeff_adjust.m for more information.
%
% FIR file management:
%   Use svn status -q data/ to see whether rerun of this script recreates
%   the same FIR file as stored in SVN data/
%
%   Run diff compare scripts to verify saved FIR files with stored FIR
%   files:
%     >. $RADIOHDL/libraries/dsp/filter/src/python/diff_lofar_coefs
%     >. $RADIOHDL/libraries/dsp/filter/src/python/diff_pfir_coefs
%
%   Run recreate scripts to (re)create FIR stored mif files from dat files:
%     >. $RADIOHDL/libraries/dsp/filter/src/python/recreate_4wb_mifs
%     >. $RADIOHDL/libraries/dsp/filter/src/python/recreate_pfir_mifs

clear all;
close all;
fig=0;

%% Select application
application = 'lofar_subband';      % load LOFAR subband filterbank coefficients from file
%application = 'apertif_subband';    % calculate Apertif subband filterbank coefficients using fir1 or fircls1
%application = 'apertif_channel';    % calculate Apertif channel filterbank coefficients using fir1 or fircls1
%application = 'arts_channel';       % calculate Arts channel filterbank coefficients using fir1 or fircls1
%application = 'bypass_channel';     % create bypass pfir channel coefficients with 1 in first tap and 0 in remaining taps
%application = 'bypass_subband';     % create bypass pfir subband coefficients with 1 in first tap and 0 in remaining taps
%application = 'incrementing_channel';  % create incrementing pfir channel coefficients 1:L*N for PFIR implementation verification purposes


%% - Application specific settings
% Default settings (will get overruled dependend on application)
config.hp_factor = 1;
config.hp_adjust = false;
config.dc_adjust = false;
config.design = 'none';
if strcmp(application, 'bypass_channel')
    N = 64;
    L = 8;
    coeff_w =  9;                                       % default to coeff RAM data width for channel
    q_full_scale = 2^(coeff_w-1);
    q_max = q_full_scale-1;
    coeff = q_max*[ones(N,1); zeros((L-1)*N,1)];        % row
    config.design = 'bypass';
    BWchan = config.hp_factor/N;
elseif strcmp(application, 'bypass_subband')
    N = 1024;
    L = 16;
    coeff_w = 16;                                       % default to coeff RAM data width for subband
    q_full_scale = 2^(coeff_w-1);
    q_max = q_full_scale-1;
    coeff = q_max*[ones(N,1); zeros((L-1)*N,1)];        % row
    config.design = 'bypass';
    BWchan = config.hp_factor/N;
elseif strcmp(application, 'incrementing_channel')
    N = 64;
    L = 8;
    coeff_w = 9;                                        % default to coeff RAM data width for channel
    q_full_scale = 2^(coeff_w-1);
    coeff = [1:L*N]';                                   % row
    %coeff = coeff / q_full_scale;                       % or use coeff = coeff / (max(coeff)+1);
    config.design = 'incrementing';
    BWchan = config.hp_factor/N;
elseif strcmp(application, 'lofar_subband')
    % Load the quantized FIR coefficients of the subband filterbank in a LOFAR station
    N = 1024;
    L = 16;
    coeff_w = 16;
    q_full_scale = 2^(coeff_w-1);
    %coeffLoad = load('data/Coefficient_16KKaiser.gold');    % column, created with pfs_coeff_final.m, DC gain = 0.995714
    coeffLoad = load('data/Coeffs16384Kaiser-quant.dat');   % column, used in LOFAR1, DC gain = 0.994817
    coeffLoad = coeffLoad';                                 % row with integer coefficients from file
    config.dc_adjust = false;
    if config.dc_adjust==false
        coeff = coeffLoad;
    else
        coeff = pfir_coeff_dc_adjust(coeffLoad, N, L, coeff_w);
    end
    coeffNoDC = coeff;   % keep DC adjusted integer coefficients
    coeff = coeff / q_full_scale;
    config.design = 'lofar_subband';
    BWchan = config.hp_factor/N;
else
    if strcmp(application, 'apertif_subband')
        N = 1024;                    % N = nof channels for complex input and N = 2 * nof subbands for real input data
        L = 16;                      % Number of taps
        coeff_w = 16;                % bits/coefficient = 1 sign + (coeff_w-1) mantissa, 0 for floating point
        config.hp_adjust = false;
        config.dc_adjust = true;
        config.design = 'fircls1';   % FIR filter design method ('fircls1' or 'fir1')
    elseif strcmp(application, 'apertif_channel')
        N = 64;                      % N = nof channels for complex input and N = 2 * nof subbands for real input data
        L = 8;                       % Number of taps, even 4 could be sufficient (erko)
        coeff_w = 9;                 % bits/coefficient = 1 sign + (coeff_w-1) mantissa, 0 for floating point
        config.hp_adjust = true;
        config.dc_adjust = true;
        config.design = 'fircls1';   % FIR filter design method ('fircls1' or 'fir1')
    elseif strcmp(application, 'arts_channel')
        N = 32;                      % Arts required is N=4, use more to reduce the relative number of channels that get aliased dur to critically sampled subband filterbank
        L = 8;
        coeff_w = 9;
        config.hp_adjust = true;
        config.dc_adjust = true;
        config.design = 'fircls1';   % FIR filter design method ('fircls1' or 'fir1')
    end
    
    % Enable or disable adjustment of the PFIR coefficients. Uncomment to
    % overrule application default for test purposes:
    %config.hp_adjust = true;
    %config.hp_adjust = false;
    
    %config.dc_adjust = true;
    %config.dc_adjust = false;
    
    % Select FIR filter design method (fircls1 yields better results than
    % fir1 regarding stop band suppression). Uncomment to overrule
    % application default for test purposes:
    %config.design = 'fircls1';
    %config.design = 'fir1';
    
    if strcmp(config.design, 'fir1')
        r_pass = 0;  % not used for fir1
        r_stop = 0;  % not used for fir1
    else  % fircls1
        % Pass and stop band deviation for fircls1
        r_pass = 1e-4; % choose 0.0001 to have more flat reponse between channels
                       % for 0.001 it is better to choose fir1
        r_stop = 1e-4; % choose 0.0001 to avoid artefact at ends of impulse response
                       % that occur for 0.001
                       
        % Config parameters that are specific for fircls1
        %config.design_flag = 'trace';
        config.design_flag = '';
        %config.interpolate = 'resample';
        %config.interpolate = 'fourier';
        config.interpolate = 'interpft';
    end
    
    % Calculate the FIR coefficients with optional FIR filter bandwidth adjustment
    BWchan = config.hp_factor/N;
    coeff_q_max = 2^(coeff_w-1)-1;
    coeff = pfir_coeff_adjust(N, L, BWchan, r_pass, r_stop, coeff_w, config, coeff_q_max);
end
NL = N*L;                % Total number of FIR filter coefficients (taps)

% Try effect of wrong wideband factor P order of the FIR coefficients.
% It is noticable in the FIR transfer function as about factor 2 in dB
% less attenuation outside the passband for bins that are multiples of
% about N/P. For P = 4 the attenuation spikes occur at:
% . bin 255.6
% . bin 256.4
% . bin 511.6
% In a time domain simulation a CW at those bins will then also show in
% the DC bin.
fir_wb_reverse = true;
fir_wb_reverse = false;   % default
if fir_wb_reverse
    P = 4;
    coeff = reshape(coeff, P, NL/P);
    coeff = flipud(coeff);
    coeff = reshape(coeff, 1, NL);
end

% FIR filter analysis
ZP = 1;
ZP = 16;                 % use zero padding factor ZP >> 1 to increase frequency resolution of hf_abs
coeff_zero_padded = [coeff / sum(coeff), zeros(1, (ZP-1)*NL)];
hf_abs = abs(fftshift(fft(coeff_zero_padded, NL*ZP))); 

pp = 1:N;                % polyphases index
hi = 1:NL;               % coefficients index
hx = hi / N;             % coefficients axis in tap units

fi = 1:NL*ZP;               % frequency index
fx = (fi - NL*ZP/2-1) / L;  % frequency axis in channel units after fft_shift()

fi_0 = NL*ZP/2+1;           % frequency index of center of channel so fx = 0
fi_n = fi_0 - round(L*ZP/2);       % frequency index of -f_chan/2
fi_p = fi_0 + round(L*ZP/2);       % frequency index of +f_chan/2

hf_abs_0 = db(hf_abs(fi_0));  % power gain at center of channel
hf_abs_n = db(hf_abs(fi_n));  % power gain at -f_chan/2
hf_abs_p = db(hf_abs(fi_p));  % power gain at +f_chan/2

% Find half power frequency edges of the passband
hpower_margin = 1/ZP;  % start with some margin,
hpower_range = 4;      % find pair of frequencies at lower edge and pair of frequencies at higher edge
hf_abs_power = hf_abs.^2;

while true
    hpower_indices = find(hf_abs_power>0.5-hpower_margin & hf_abs_power<0.5+hpower_margin);
    hpower_len = length(hpower_indices);
    if hpower_len>hpower_range
        hpower_margin = hpower_margin / hpower_range;
    elseif hpower_len<hpower_range
        hpower_margin = hpower_margin * 1.1;
    else
        break;
    end
    %hpower_margin, hpower_len
end
if length(hpower_indices)==hpower_range
    % Linear interpolate for the lower pair and for the higher pair of frequencies
    hf_pair_lo = hf_abs_power(hpower_indices([1, 2]));
    hf_pair_hi = hf_abs_power(hpower_indices([3, 4]));
    fx_pair_lo = [fx(hpower_indices(1))/ZP, fx(hpower_indices(2))/ZP];
    fx_pair_hi = [fx(hpower_indices(3))/ZP, fx(hpower_indices(4))/ZP];
    % y = a * x + b
    a_lo = (hf_pair_lo(2) - hf_pair_lo(1))/(fx_pair_lo(2) - fx_pair_lo(1));  % a = delta y / delta x
    b_lo = hf_pair_lo(1) - a_lo*fx_pair_lo(1);                               % b = y - a * x
    fx_lo = (0.5 - b_lo) / a_lo;                                             % x = (y - b) / a
    a_hi = (hf_pair_hi(2) - hf_pair_hi(1))/(fx_pair_hi(2) - fx_pair_hi(1));  % a = delta y / delta x
    b_hi = hf_pair_hi(1) - a_hi*fx_pair_hi(1);                               % b = y - a * x
    fx_hi = (0.5 - b_hi) / a_hi;                                             % x = (y - b) / a
    hpower_bandwidth = (fx_hi - fx_lo);
else
    disp(sprintf('Error could not find half power bandwidth'));
end
    

%% Print FIR-filter parameters 
disp(sprintf([ ...
    'Filter design                   : %s\n', ...
    'Number of polyphases (FFT size) : N                = %d\n', ...
    'Number of FIR taps              : L                = %d\n', ...
    'Number of FIR coefficients      : N*L              = %d\n', ...
    'Channel half power factor       : hp_factor        = %8.6f\n', ...
    'Channel bandwidth               : BWchan*N         = %8.6f [f_chan]\n' , ...
    'Zero padding factor             : ZP               = %d\n', ...
    'Channel half power bandwidth    : hpower_bandwidth = %5.3f [f_chan]\n', ...
    'Channel power gain at center    : hf_abs_0         = %8.5f [dB]\n', ...
    'Channel power gain at -f_chan/2 : hf_abs_n         = %8.5f [dB]\n', ...
    'Channel power gain at +f_chan/2 : hf_abs_p         = %8.5f [dB]\n', ...
    'For reference 10*log10(0.25)    : quarter power, so 0.5000 amplitude = %8.4f [dB]\n', ...
    'For reference 10*log10(0.5)     : half power,    so 0.7071 amplitude = %8.4f [dB]\n'] , ...
    config.design, N, L, NL, config.hp_factor, BWchan*N, ZP, hpower_bandwidth, hf_abs_0, hf_abs_n, hf_abs_p, db(sqrt(0.25)), db(sqrt(0.5))));
if strcmp(config.design, 'fircls1')
    disp(sprintf([ ...
        'Pass band deviation             : r_pass           = %9.7f = %10.6f dB ripple\n' , ...
        'Stop band deviation             : r_stop           = %9.7f = %10.6f dB attenuation\n'] , ...
        r_pass, 20*log10(1+r_pass), r_stop, 20*log10(r_stop)));
end


%% Create common file_name_prefix
file_name_prefix = 'run_pfir_coeff_m_';
if config.hp_adjust
    file_name_prefix = [file_name_prefix, 'flat_hp_'];
end
if config.dc_adjust
    file_name_prefix = [file_name_prefix, 'no_dc_'];
end
file_name_prefix = [file_name_prefix, config.design];
file_name_prefix = sprintf([file_name_prefix, '_%dtaps_%dpoints_%db'], L, N, coeff_w)

%% Save quantized integer FIR filter coefficients 
% N.B. - read '.txt'-file with 'wordpad' or use \r\n
if coeff_w>0
    if strcmp(config.design, 'bypass')
        int_coeff = coeff;
    elseif strcmp(config.design, 'incrementing')
        int_coeff = coeff;
    else
        q_full_scale = 2^(coeff_w-1);
        q_max = q_full_scale-1;
        int_coeff = round(q_full_scale * coeff);  % int_coeff - q_full_scale * coeff, should all be 0, because the quantization was done based on q_full_scale
        coeff_dc_gain = sum(coeff) / N;
        disp(sprintf([ ...
            'Max normalized quantized coeff = %12.10f (<= 1)\n' , ...
            'Max scaled quantized coeff     = %12.6f (<= %d, q_margin = %d)\n', ...
            'DC gain per polyphase          = %f\n'] , ...
            max(coeff), max(int_coeff), q_max, q_max-max(int_coeff), coeff_dc_gain));
    end
    file_name = ['data/', file_name_prefix, '.dat'];
    fid = fopen(file_name , 'w');
    fprintf(fid ,'%d\n', int_coeff);
    fclose(fid);
    
    if strcmp(application, 'lofar_subband')
        if config.dc_adjust==false
            % diff Coeffs16384Kaiser-quant.dat
            % Coeffs16384Kaiser-quant-withdc.dat --> are identical
            fid = fopen('data/Coeffs16384Kaiser-quant-withdc.dat', 'w');
            fprintf(fid ,'%d\n', int_coeff);
            fclose(fid);
        else
            % diff
            % run_pfir_coeff_m_no_dc_lofar_subband_16taps_1024points_16b.dat
            % Coeffs16384Kaiser-quant-nodc.dat  --> are identical
            fid = fopen('data/Coeffs16384Kaiser-quant-nodc.dat', 'w');
            fprintf(fid ,'%d\n', int_coeff);
            fclose(fid);
        end
    end
end


%% Plot FIR-filter coefficients
strNL = [' (', num2str(N), '-points, ', num2str(L), '-taps', ')'];
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
if coeff_w == 0
    plot(hx, coeff);
    title(['FIR filter coefficients for ', config.design, ' (floating point)', strNL]);
else
    plot(hx, int_coeff);
    title(['FIR filter coefficients for ', config.design, ' (', num2str(coeff_w), ' bits)', strNL]);
end
grid on;
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
if coeff_w == 0
    mesh(reshape(coeff, N, L));
else
    mesh(reshape(int_coeff, N, L));
end
title(['FIR filter coefficients for ', config.design, strNL]);
xlabel(['Taps 1:', num2str(L)]);
ylabel(['Channels 1:', num2str(N)]);
file_name = ['plots/', file_name_prefix, '_coefficients.jpg'];
print(file_name, '-djpeg')

%% Plot FIR-filter DC response per polyphase
if coeff_w>0
    dc_polyphases        = sum(reshape(int_coeff, N, L).');
    dc_polyphases_mean   = mean(dc_polyphases);
    dc_polyphases_median = median(dc_polyphases);
    dc_polyphases_min    = min(dc_polyphases);
    dc_polyphases_max    = max(dc_polyphases);
    disp(sprintf(['dc_polyphases_max    : %d\n', ...
                  'dc_polyphases_mean   : %f\n', ...
                  'dc_polyphases_median : %f\n', ...
                  'dc_polyphases_min    : %d\n', ...
                  'dc_polyphases_span   : %d\n'], ...
                   dc_polyphases_max, dc_polyphases_mean, dc_polyphases_median, dc_polyphases_min, dc_polyphases_max-dc_polyphases_min))
    strNL = [' (', num2str(N), '-points, ', num2str(L), '-taps', ')'];
    fig=fig+1;
    figure('position', [300+fig*20 200-fig*20 1000 800]);
    figure(fig);
    plot(pp, dc_polyphases - dc_polyphases_median);
    title(['FIR filter DC response per polyphase coefficients for ', config.design, ' (', num2str(coeff_w), ' bits)', strNL]);
    xlabel(['Polyphase 1:', num2str(N)]);
    ylabel(['Sum of taps (with median at zero is ', num2str(dc_polyphases_median), ')']);
    grid on;
    file_name = ['plots/', file_name_prefix, '_dc_response_per_polyphase.jpg'];
    print(file_name, '-djpeg')
end

% Plot DC adjustment for LOFAR subband FIR-filter coefficients
if strcmp(application, 'lofar_subband') && config.dc_adjust==true
    strNL = [' (', num2str(N), '-points, ', num2str(L), '-taps', ')'];
    fig=fig+1;
    figure('position', [300+fig*20 200-fig*20 1000 800]);
    figure(fig);
    plot(hi, coeffNoDC - coeffLoad);
    title(['FIR filter coefficient DC adjustments for ', config.design, strNL]);
    xlabel(['Coefficients']);
    ylabel(['Adjustment']);
    grid on;
end

% Plot FIR-filter amplitude characteristic using fft()
% . full plot in [dB]
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(fx/ZP, db(hf_abs));  % db() = 20*log10 for voltage
xlo = fx(1)/ZP;
xhi = fx(NL*ZP)/ZP;
xlim([xlo xhi]);
ylo = -120;
yhi = 10;
ylim([ylo yhi]);
grid on; 
title(['Full FIR filter transfer function for ', config.design, strNL]);
xlabel('Frequency [channels]');
ylabel('Power [dB]');
file_name = ['plots/', file_name_prefix, '_transfer_function_full.jpg'];
print(file_name, '-djpeg')

% . zoomed plot in power
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(fx/ZP, hf_abs_power);
xlo = -0.7;
xhi = 0.7;
xlim([xlo xhi]);
grid on; 
title(['Zoomed FIR filter transfer function for ', config.design, strNL]);
xlabel('Frequency [channels]');
ylabel('Power');
file_name = ['plots/', file_name_prefix, '_transfer_function_zoom.jpg'];
print(file_name, '-djpeg')

% Plot FIR-filter amplitude and phase characteristic using freqz()
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
Nres = N * 128;
freqz(coeff, 1, Nres);   % draws 2 subplots, one for magnitude and one for phase
subplot(2,1,1);
xlim([0 3/N])
title(['FIR filter transfer function for ', config.design, strNL]);
subplot(2,1,2);
xlim([0 3/N])


% Plot FIR-filter response between channels
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
subplot(2,1,1)
hf_abs1 = circshift(hf_abs, L*ZP, 2);   % shift to next channel (in dimension 2 because hf_abs is a row vector)
plot(fx/ZP, db(hf_abs), 'k', fx/ZP, db(hf_abs1), 'r');  % db() = 20 *  log10 for voltage
xlo = -1;
xhi = 2;
xlim([xlo xhi]);
grid on; 
title(['FIR filter transfer function between channels for ', config.design, strNL]);
xlabel('Frequency [channels]');
ylabel('Power [dB]');

subplot(2,1,2)
plot(fx/ZP, hf_abs.^2, 'r.', ...
     fx/ZP, hf_abs1.^2, 'b.', ...
     fx/ZP, hf_abs.^2 + hf_abs1.^2, 'k');
xlo = -1;
xhi = 2;
xlim([xlo xhi]);
%ylim([0.8 1.2]);
grid on; 
title(['FIR filter sum of power responses between channels for ', config.design, strNL]);
xlabel('Frequency [channels]');
ylabel('Power');

file_name = ['plots/', file_name_prefix, '_transfer_function_two_channels.jpg'];
print(file_name, '-djpeg')

