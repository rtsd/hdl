--------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Eric Kooistra, 26 Apr 2018
-- Purpose:
-- . Reshape the block size by redefining the sop and eop, and reshape the sync
--   interval via number of blocks per sync
-- Description:
--   This dp_block_reshape_sync is similar to dp_block_reshape.vhd, however it
--   uses an extra level in dp_counter to also create the new sync interval.
--   The first snk_in.valid yields the first src_out.sync and src_out.sop. The
--   g_reshape_nof_data_per_blk then defines the src_out.eop. The
--   g_reshape_nof_blk_per_sync defines when the subsequent sync occurs. The
--   snk_in.sync, snk_in.sop and snk_in.eop are ignored.
--
--   Dependend on g_reshape_bsn the local BSN can be filled in:
--   * If g_reshape_bsn = FALSE then the BSN field is passed on unchanged.
--   * If g_reshape_bsn = TRUE then the snk_in.bsn at the snk_in.sync is
--     captured and replicated at the src_out.sync. For intermediate
--     src_out.sop the src_out.bsn starts at 1 and increments.
--
--   The other sosi fields channel @ sop and empty, error @ eop are passed on
--   unchanged.
--
--   The advantage of passing the sop sosi fields and eop sosi fields unchanged
--   is that a next dp_block_reshape_sync instance that reshapes back to the
--   original snk_in block size recovers them.
--
--  Sync index:
--    The src_index_arr[2:0] with sync index, sop index and valid index are
--    also output. The sync index defaults to 0 and only counts if
--    c_nof_output_sync_per_input_sync > 1, which depends on
--    g_input_nof_data_per_sync.
--    If g_input_nof_data_per_sync <= c_reshape_nof_data_per_sync, then the
--    sync index defaults to 0 and then the value of g_input_nof_data_per_sync
--    is don't care. Therefore use g_input_nof_data_per_sync = 0 or 1 to
--    effectively disable the use of sync_index.
--    The sync index counts using dp_counter level 2, so with c_nof_counters
--    = 3. If the sync index does not need to count, then c_nof_counters = 2,
--    because dp_counter cannot support range(0,1,1) = [0].
--
-- Remarks:
-- . The dp_block_reshape_sync supports pipelining and flow control, because it
--   for that it relies on dp_counter.
-- . With g_reshape_bsn = TRUE this dp_block_reshape_sync is equivalent to
--   dp_sync_insert.vhd.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_block_reshape_sync is
  generic (
    g_input_nof_data_per_sync  : natural :=  0;  -- nof data per input sync interval, used only for sync_index
    g_reshape_nof_data_per_blk : natural :=  8;  -- nof data per output block
    g_reshape_nof_blk_per_sync : natural := 12;  -- nof blocks per output sync interval
    g_reshape_bsn              : boolean := false;  -- when TRUE reshape BSN else when FALSE pass on BSN
    g_pipeline_src_out         : natural :=  1;  -- Pipeline source outputs (data,valid,sop,eop etc)
    g_pipeline_src_in          : natural :=  0  -- Pipeline source inputs (ready,xon). This will also pipeline src_out.
  );
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;

    snk_in        : in  t_dp_sosi;
    snk_out       : out t_dp_siso;

    src_out       : out t_dp_sosi;
    src_in        : in  t_dp_siso := c_dp_siso_rdy;
    src_index_arr : out t_natural_arr(2 downto 0)  -- [2] = sync index, [1] sop index, [0] valid index
  );
end dp_block_reshape_sync;

architecture str of dp_block_reshape_sync is
  constant c_reshape_nof_data_per_sync       : natural := g_reshape_nof_data_per_blk * g_reshape_nof_blk_per_sync;

  -- If g_input_nof_data_per_sync <= c_reshape_nof_data_per_sync then c_nof_output_sync_per_input_sync = 1, else the
  -- assumption is that g_input_nof_data_per_sync is an integer multiple of c_reshape_nof_data_per_sync.
  constant c_nof_output_sync_per_input_sync  : natural := sel_a_b(g_input_nof_data_per_sync > c_reshape_nof_data_per_sync,
                                                                  g_input_nof_data_per_sync / c_reshape_nof_data_per_sync, 1);

  -- counter [0] is use for block reshape,
  -- counter [1] is used for sync reshape,
  -- counter [2] is only used for sync_index if c_nof_output_sync_per_input_sync > 1
  constant c_nof_counters  : natural := sel_a_b(c_nof_output_sync_per_input_sync > 1, 3, 2);
  constant c_range_start   : t_nat_natural_arr(c_nof_counters - 1 downto 0) := (others => 0);
  constant c_range_stop    : t_nat_natural_arr(c_nof_counters - 1 downto 0) := sel_a_b(c_nof_output_sync_per_input_sync > 1,
                                    (c_nof_output_sync_per_input_sync, g_reshape_nof_blk_per_sync, g_reshape_nof_data_per_blk),
                                                                      (g_reshape_nof_blk_per_sync, g_reshape_nof_data_per_blk));
  constant c_range_step    : t_nat_natural_arr(c_nof_counters - 1 downto 0) := (others => 1);

  signal cnt_sosi_arr      : t_dp_sosi_arr(c_nof_counters - 1 downto 0);
  signal input_sosi        : t_dp_sosi;
  signal input_siso        : t_dp_siso;
  signal reshape_sosi      : t_dp_sosi;
  signal reshape_siso      : t_dp_siso;

  signal sync_index        : natural := 0;
  signal sop_index         : natural;
  signal valid_index       : natural;
  signal output_sync       : std_logic;
  signal output_sop        : std_logic;
  signal output_eop        : std_logic;

  -- Reshaped BSN state
  signal output_bsn        : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal global_bsn        : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal local_bsn         : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal reg_output_bsn    : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal reg_global_bsn    : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
begin
  u_dp_counter : entity work.dp_counter
  generic map (
    g_nof_counters     => c_nof_counters,
    g_range_start      => c_range_start,
    g_range_stop       => c_range_stop,
    g_range_step       => c_range_step,
    g_pipeline_src_out => g_pipeline_src_out,
    g_pipeline_src_in  => g_pipeline_src_in
  )
  port map (
    clk               => clk,
    rst               => rst,

    snk_in            => snk_in,
    snk_out           => snk_out,

    src_out           => input_sosi,
    src_in            => input_siso,

    count_src_out_arr => cnt_sosi_arr
  );

  gen_sync_index : if c_nof_output_sync_per_input_sync > 1 generate
    sync_index <= TO_UINT(cnt_sosi_arr(2).data);
  end generate;

  sop_index   <= TO_UINT(cnt_sosi_arr(1).data);
  valid_index <= TO_UINT(cnt_sosi_arr(0).data);

  src_index_arr(2) <= sync_index;  -- default 0, counting if c_nof_output_sync_per_input_sync > 1
  src_index_arr(1) <= sop_index;
  src_index_arr(0) <= valid_index;

  output_sync <= cnt_sosi_arr(1).sop;
  output_sop  <= cnt_sosi_arr(0).sop;
  output_eop  <= cnt_sosi_arr(0).eop;

  no_reshape_bsn : if g_reshape_bsn = false generate
    output_bsn <= input_sosi.bsn;
  end generate;

  gen_reshape_bsn : if g_reshape_bsn = true generate
    reg_global_bsn <= global_bsn when rising_edge(clk);
    reg_output_bsn <= output_bsn when rising_edge(clk);

    p_global_bsn : process(reg_global_bsn, input_sosi)
    begin
      -- Keep global BSN at input sync for sync in reshaped sync intervals
      global_bsn <= reg_global_bsn;
      if input_sosi.sync = '1' then
        global_bsn <= input_sosi.bsn;
      end if;
    end process;

    local_bsn <= TO_DP_BSN(sop_index);

    p_output_bsn : process(reg_output_bsn, output_sync, global_bsn, output_sop, local_bsn)
    begin
      -- Output reshaped BSN with global BSN at output sync and local BSN at output sop
      output_bsn <= reg_output_bsn;
      if output_sync = '1' then
        output_bsn <= global_bsn;
      elsif output_sop = '1' then
        output_bsn <= local_bsn;
      end if;
    end process;
  end generate;

  input_siso <= reshape_siso;

  p_wire : process(input_sosi, output_bsn, output_sync, output_sop, output_eop)
  begin
    reshape_sosi      <= input_sosi;
    reshape_sosi.bsn  <= output_bsn;
    reshape_sosi.sync <= output_sync;
    reshape_sosi.sop  <= output_sop;
    reshape_sosi.eop  <= output_eop;
  end process;

  -- No need to pipeline output for gen_reshape_bsn, because does not have so much combinatorial logic
  src_out      <= reshape_sosi;
  reshape_siso <= src_in;
end str;
