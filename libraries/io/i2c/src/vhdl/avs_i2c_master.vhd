-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.i2c_pkg.all;

-- Need to wrap i2c_master with this avs_i2c_master, because the SOPC Component
-- Editor does not support the user defined types from i2c_pkg.

-- Name signals for automatic type and interface recognition:
--
--  <interface type>_<interface name>_<signal type>[_n]
--
-- * Avalon <interface type>:
--   . csi_ = Clock input
--   . avs_ = Avalon-MM slave
--   . coe_ = Conduit
-- * User defined <interface name>:
--   . _system_
--   . _control_
--   . etc.
-- * Avalon <signal type>:
--   . _reset
--   . _clk
--   . _address
--   . etc.

entity avs_i2c_master is
  generic (
    -- g_i2c_mm
    g_control_adr_w          : natural := c_i2c_control_adr_w;  -- =1, fixed
    g_protocol_adr_w         : natural := c_i2c_protocol_adr_w;
    g_result_adr_w           : natural := c_i2c_result_adr_w;
    -- g_i2c_phy
    g_clk_cnt                : natural := 399;
    g_comma_w                : natural := c_i2c_comma_w_dis
  );
  port (
    ---------------------------------------------------------------------------
    -- Avalon Conduit interfaces: coe_*_export
    ---------------------------------------------------------------------------
    -- GENERIC Signal
    coe_gs_sim_export        : in  std_logic := '0';

    -- System
    coe_sync_export          : in  std_logic := '1';

    -- I2C interface
    coe_i2c_scl_export       : inout std_logic;  -- I2C Serial Clock Line
    coe_i2c_sda_export       : inout std_logic;  -- I2C Serial Data Line

    ---------------------------------------------------------------------------
    -- Avalon Clock Input interface: csi_*
    ---------------------------------------------------------------------------
    csi_system_reset         : in  std_logic;
    csi_system_clk           : in  std_logic;

    ---------------------------------------------------------------------------
    -- Avalon Memory Mapped Slave interfaces: avs_*
    ---------------------------------------------------------------------------
    -- MM slave I2C control register
    avs_control_address      : in  std_logic;  -- g_control_adr_w=1
    avs_control_write        : in  std_logic;
    avs_control_read         : in  std_logic;
    avs_control_writedata    : in  std_logic_vector(c_word_w - 1 downto 0);  -- use default MM bus width for control
    avs_control_readdata     : out std_logic_vector(c_word_w - 1 downto 0);
    -- MM slave I2C protocol register
    avs_protocol_address     : in  std_logic_vector(g_protocol_adr_w - 1 downto 0);
    avs_protocol_write       : in  std_logic;
    avs_protocol_read        : in  std_logic;
    avs_protocol_writedata   : in  std_logic_vector(c_byte_w - 1 downto 0);  -- define MM bus data has same width as SMBus data
    avs_protocol_readdata    : out std_logic_vector(c_byte_w - 1 downto 0);
    -- MM slave I2C result register
    avs_result_address       : in  std_logic_vector(g_result_adr_w - 1 downto 0);
    avs_result_write         : in  std_logic;
    avs_result_read          : in  std_logic;
    avs_result_writedata     : in  std_logic_vector(c_byte_w - 1 downto 0);  -- define MM bus data has same width as SMBus data
    avs_result_readdata      : out std_logic_vector(c_byte_w - 1 downto 0);

    ---------------------------------------------------------------------------
    -- Avalon Interrupt Sender interface: ins_*
    ---------------------------------------------------------------------------
    ins_interrupt_irq        : out std_logic
  );
end avs_i2c_master;

architecture wrap of avs_i2c_master is
  constant c_avs_i2c_mm  : t_c_i2c_mm  := (g_control_adr_w, g_protocol_adr_w, 2**g_protocol_adr_w, g_result_adr_w, 2**g_result_adr_w);
  constant c_avs_i2c_phy : t_c_i2c_phy := (g_clk_cnt, g_comma_w);

  signal cs_sim                : boolean;
  signal i_avs_control_address : std_logic_vector(0 downto 0);
begin
  -- SOPC Builder does not use BOOLEAN, so use STD_LOGIC and is_true()
  cs_sim <= is_true(coe_gs_sim_export);

  -- SOPC Builder writes STD_LOGIC_VECTOR(0 DOWNTO 0) as STD_LOGIC so take care of mapping
  i_avs_control_address(0) <= avs_control_address;

  u_i2c_master : entity work.i2c_master
  generic map (
    g_i2c_mm                 => c_avs_i2c_mm,
    g_i2c_phy                => c_avs_i2c_phy
  )
  port map (
    -- GENERIC Signal
    gs_sim                   => cs_sim,

    rst                      => csi_system_reset,
    clk                      => csi_system_clk,
    sync                     => coe_sync_export,

    ---------------------------------------------------------------------------
    -- Memory Mapped Slave interface with Interrupt
    ---------------------------------------------------------------------------
    -- MM slave I2C control register
    mms_control_address      => i_avs_control_address,
    mms_control_write        => avs_control_write,
    mms_control_read         => avs_control_read,
    mms_control_writedata    => avs_control_writedata,
    mms_control_readdata     => avs_control_readdata,
    -- MM slave I2C protocol register
    mms_protocol_address     => avs_protocol_address,
    mms_protocol_write       => avs_protocol_write,
    mms_protocol_read        => avs_protocol_read,
    mms_protocol_writedata   => avs_protocol_writedata,
    mms_protocol_readdata    => avs_protocol_readdata,
    -- MM slave I2C result register
    mms_result_address       => avs_result_address,
    mms_result_write         => avs_result_write,
    mms_result_read          => avs_result_read,
    mms_result_writedata     => avs_result_writedata,
    mms_result_readdata      => avs_result_readdata,
    -- Interrupt
    ins_result_rdy           => ins_interrupt_irq,

    ---------------------------------------------------------------------------
    -- I2C interface
    ---------------------------------------------------------------------------
    scl                      => coe_i2c_scl_export,
    sda                      => coe_i2c_sda_export
  );
end wrap;
