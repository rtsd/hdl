-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- Author : E. Kooistra, 29 mar 2017
-- Purpose : Set gain per stream via MM
-- Description:
--   There is one MM programmable gain per stream and therefore the same gain
--   applies to all serial data on that stream. There are g_nof_streams in
--   parallel.
--   If the gain is complex then there is a separate MM reg for the imaginary
--   gain. If the data is complex then the in_sosi.re/im are used, else the
--   in_sosi.data is used. The following combinations are possible:
--
--   g_complex_data   g_complex_gain   Description
--     True             True           for amplitude and phase adjustment
--     True             False          for amplitude only adjustment
--     False            False          for real input gain adjustment
--     False            True           for making real input complex
--
-- Remarks:
-- . The in_sosi_arr().valid is propagated to out_sosi_arr().valid.
-- . Typically choose g_out_dat_w = g_gain_w + g_in_dat_w - 1. This skips the
--   double sign bit of the product and assumes that full scale amplitude of
--   the complex gain = full scale re = full scale im to not cause overflow.
-- . Requantization needs to be done outside this component, e.g. using
--   dp_requantize.
-- . There is one gain per parallel stream. If all streams can have the same
--   gain, then program the same gain for each stream. This avoids potential
--   fanout timing issues if one gain programmable would be used for all
--   streams.
-- . If the gain needs to vary in time per stream, because the stream carries
--   time multiplexed data, then a new component is necessary. The gains per
--   stream may then be stored in a MM RAM, because they are used sequentially.
-- . see also reqant_selection_compensation.m
-- . A dp_pipeline is used to pass through the dp_control_fields.

library IEEE, common_lib, common_mult_lib, technology_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_dp_gain_arr is
  generic (
    g_technology                    : natural := c_tech_select_default;
    g_nof_streams                   : natural := 3;  -- for g_nof_streams=1 there is mms_dp_gain.vhd
    g_complex_data                  : boolean := true;
    g_complex_gain                  : boolean := false;
    g_gain_init_re                  : integer := 1;
    g_gain_init_im                  : integer := 0;
    g_gain_w                        : natural := 16;
    g_in_dat_w                      : natural := 9;
    g_out_dat_w                     : natural := 24;  -- 16 + 9 - 1 = 24 (-1 to skip double sign bit in product)

    -- pipelining (typically use defaults)
    -- . real multiplier
    g_pipeline_real_mult_input      : natural := 1;  -- 0 or 1
    g_pipeline_real_mult_product    : natural := 1;  -- 0 or 1
    g_pipeline_real_mult_output     : natural := 1;  -- >= 0
    -- . complex multiplier
    g_pipeline_complex_mult_input   : natural := 1;  -- 0 or 1
    g_pipeline_complex_mult_product : natural := 0;  -- 0 or 1
    g_pipeline_complex_mult_adder   : natural := 1;  -- 0 or 1
    g_pipeline_complex_mult_output  : natural := 1  -- >= 0
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;
    dp_rst            : in  std_logic;
    dp_clk            : in  std_logic;

    -- MM access to gain
    reg_gain_re_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_gain_re_miso  : out t_mem_miso;
    reg_gain_im_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_gain_im_miso  : out t_mem_miso;

    reg_gain_re       : out std_logic_vector(g_nof_streams * g_gain_w - 1 downto 0);
    reg_gain_im       : out std_logic_vector(g_nof_streams * g_gain_w - 1 downto 0);

    -- ST
    in_sosi_arr       : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    out_sosi_arr      : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_gain_arr;

architecture str of mms_dp_gain_arr is
  constant c_real_multiply  : boolean := g_complex_data = false and g_complex_gain = false;

  constant c_real_multiply_latency    : natural := g_pipeline_real_mult_input + g_pipeline_real_mult_product + g_pipeline_real_mult_output;
  constant c_complex_multiply_latency : natural := g_pipeline_complex_mult_input + g_pipeline_complex_mult_product + g_pipeline_complex_mult_adder + g_pipeline_complex_mult_output;

  -- TYPE t_c_mem IS RECORD
  --   latency   : NATURAL;    -- read latency
  --   adr_w     : NATURAL;
  --   dat_w     : NATURAL;
  --   nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
  --   init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
  constant c_mm_reg         : t_c_mem := (1, ceil_log2(g_nof_streams), g_gain_w, g_nof_streams, 'X');

  constant c_init_re        : std_logic_vector(g_nof_streams * g_gain_w - 1 downto 0) := array_sinit(g_gain_init_re, g_nof_streams, g_gain_w);
  constant c_init_im        : std_logic_vector(g_nof_streams * g_gain_w - 1 downto 0) := array_sinit(g_gain_init_im, g_nof_streams, g_gain_w);

  constant c_mm_reg_init_re : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := RESIZE_UVEC(c_init_re, c_mem_reg_init_w);
  constant c_mm_reg_init_im : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := RESIZE_UVEC(c_init_im, c_mem_reg_init_w);

  signal i_reg_gain_re        : std_logic_vector(g_nof_streams * g_gain_w - 1 downto 0) := c_init_re;
  signal i_reg_gain_im        : std_logic_vector(g_nof_streams * g_gain_w - 1 downto 0) := c_init_im;

  signal mult_sosi_arr         : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal pipelined_in_sosi_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
begin
  reg_gain_re <= i_reg_gain_re;
  reg_gain_im <= i_reg_gain_im;

  u_common_reg_r_w_dc_re : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => true,
    g_readback           => false,
    g_reg                => c_mm_reg,
    g_init_reg           => c_mm_reg_init_re
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in         => reg_gain_re_mosi,
    sla_out        => reg_gain_re_miso,

    -- MM registers in st_clk domain
    reg_wr_arr     => OPEN,
    reg_rd_arr     => OPEN,
    in_reg         => i_reg_gain_re,
    out_reg        => i_reg_gain_re  -- readback via ST clock domain
  );

  gen_real_multiply : if c_real_multiply = true generate
    gen_nof_streams : for I in 0 to g_nof_streams - 1 generate
      u_common_mult : entity common_mult_lib.common_mult
      generic map (
        g_technology       => g_technology,
        g_variant          => "IP",
        g_in_a_w           => g_gain_w,
        g_in_b_w           => g_in_dat_w,
        g_out_p_w          => g_out_dat_w,  -- g_out_dat_w = g_gain_w+g_in_dat_w, use smaller g_out_dat_w to truncate MSbits, or larger g_out_dat_w to extend MSbits
        g_nof_mult         => 1,
        g_pipeline_input   => g_pipeline_real_mult_input,
        g_pipeline_product => g_pipeline_real_mult_product,
        g_pipeline_output  => g_pipeline_real_mult_output,
        g_representation   => "SIGNED"  -- or "UNSIGNED"
      )
      port map (
        rst        => dp_rst,
        clk        => dp_clk,
        in_a       => i_reg_gain_re((I + 1) * g_gain_w - 1 downto I * g_gain_w),
        in_b       => in_sosi_arr(I).data(g_in_dat_w - 1 downto 0),
        in_val     => in_sosi_arr(I).valid,
        out_p      => mult_sosi_arr(I).data(g_out_dat_w - 1 downto 0),
        out_val    => mult_sosi_arr(I).valid
      );

      u_pipeline : entity work.dp_pipeline
      generic map (
        g_pipeline => c_real_multiply_latency
      )
      port map (
        rst     => dp_rst,
        clk     => dp_clk,
        snk_in  => in_sosi_arr(I),
        src_out => pipelined_in_sosi_arr(I)
      );

      p_out_sosi_arr : process(mult_sosi_arr, pipelined_in_sosi_arr)
      begin
        out_sosi_arr(I)       <= pipelined_in_sosi_arr(I);
        out_sosi_arr(I).valid <= mult_sosi_arr(I).valid;
        out_sosi_arr(I).data  <= RESIZE_DP_SDATA(mult_sosi_arr(I).data(g_out_dat_w - 1 downto 0));  -- sign extend
      end process;

    end generate gen_nof_streams;
  end generate gen_real_multiply;

  gen_complex_multiply : if c_real_multiply = false generate
    gen_complex_gain : if g_complex_gain = true generate
      u_common_reg_r_w_dc_im : entity common_lib.common_reg_r_w_dc
      generic map (
        g_cross_clock_domain => true,
        g_readback           => false,
        g_reg                => c_mm_reg,
        g_init_reg           => c_mm_reg_init_im
      )
      port map (
        -- Clocks and reset
        mm_rst         => mm_rst,
        mm_clk         => mm_clk,
        st_rst         => dp_rst,
        st_clk         => dp_clk,

        -- Memory Mapped Slave in mm_clk domain
        sla_in         => reg_gain_im_mosi,
        sla_out        => reg_gain_im_miso,

        -- MM registers in st_clk domain
        reg_wr_arr     => OPEN,
        reg_rd_arr     => OPEN,
        in_reg         => i_reg_gain_im,
        out_reg        => i_reg_gain_im  -- readback via ST clock domain
      );
    end generate gen_complex_gain;

    -- ELSE: if g_complex_gain=FALSE then use default i_reg_gain_im, which is then typically g_gain_init_im=0 for all streams.

    gen_nof_streams : for I in 0 to g_nof_streams - 1 generate
      u_common_complex_mult : entity common_mult_lib.common_complex_mult
      generic map (
        g_technology       => g_technology,
        g_variant          => "IP",
        g_in_a_w           => g_gain_w,
        g_in_b_w           => g_in_dat_w,
        g_out_p_w          => g_out_dat_w,  -- g_out_dat_w = g_gain_w+g_in_dat_w, use smaller g_out_dat_w to truncate MSbits, or larger g_out_dat_w to extend MSbits
        g_conjugate_b      => false,
        g_pipeline_input   => g_pipeline_complex_mult_input,
        g_pipeline_product => g_pipeline_complex_mult_product,
        g_pipeline_adder   => g_pipeline_complex_mult_adder,
        g_pipeline_output  => g_pipeline_complex_mult_output
      )
      port map (
        rst        => dp_rst,
        clk        => dp_clk,
        in_ar      => i_reg_gain_re((I + 1) * g_gain_w - 1 downto I * g_gain_w),
        in_ai      => i_reg_gain_im((I + 1) * g_gain_w - 1 downto I * g_gain_w),
        in_br      => in_sosi_arr(I).re(g_in_dat_w - 1 downto 0),
        in_bi      => in_sosi_arr(I).im(g_in_dat_w - 1 downto 0),
        in_val     => in_sosi_arr(I).valid,  -- only propagate valid, not used internally
        out_pr     => mult_sosi_arr(I).re(g_out_dat_w - 1 downto 0),
        out_pi     => mult_sosi_arr(I).im(g_out_dat_w - 1 downto 0),
        out_val    => mult_sosi_arr(I).valid
      );

      u_pipeline : entity work.dp_pipeline
      generic map (
        g_pipeline => c_complex_multiply_latency
      )
      port map (
        rst     => dp_rst,
        clk     => dp_clk,
        snk_in  => in_sosi_arr(I),
        src_out => pipelined_in_sosi_arr(I)
      );

      p_out_sosi_arr : process(mult_sosi_arr, pipelined_in_sosi_arr)
      begin
        out_sosi_arr(I)       <= pipelined_in_sosi_arr(I);
        out_sosi_arr(I).valid <= mult_sosi_arr(I).valid;
        out_sosi_arr(I).re    <= RESIZE_DP_DSP_DATA(mult_sosi_arr(I).re(g_out_dat_w - 1 downto 0));  -- sign extend
        out_sosi_arr(I).im    <= RESIZE_DP_DSP_DATA(mult_sosi_arr(I).im(g_out_dat_w - 1 downto 0));  -- sign extend
      end process;

    end generate gen_nof_streams;
  end generate gen_complex_multiply;
end str;
