// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
Z8x4yUUyiGzD2Xt+331dXYQHw0P59otDqtQMtv76mPXMjq+OPzv77a47GewcwxA4
m6A8vrjOV0ss8A6MYdUGUtF2nzEwwnz5tD/YhmWndUfvDSxllAVTuG2UuVD1v3Ki
e93haUr97T4ed/gTZ2C/GU6XKTgoEA4J10TzIHdg1gc=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 2752 )
`pragma protect data_block
+PPcdTzG3Hw3j93V5h79tpEnXvdq2FekmwEVbGvt3UIW/UFWn3zwfnv0sET+Ey08
1dX2vwqgIQEsrWzirOfzUjBJ/5ffKS16+PcuYOwjTvfWIzkOfOpzOzCTAsSvZYeQ
2fISUfNH12BpP9vIa9n9qmXVgpwtVSbcGgAZZX67iZfrD5/dgrDrBMJ2EVZvgyfe
OmzQh4ZDoYiVSvSz/kcGCQ2u8Y8fQ3aIM+54gBOwf+x6quaYi/V7VFZOgzzcFIw2
NVVJKK7aW7XuwfiKoow4Pmgn3PVgP9NLdAAwMerYk701ZNQpHoNyVUFnVTHSV9JH
ZAQYIIdTWCt8RaA3cPaZqygq5yP3Llk+qzJyE6WJ6d1gIPBO2Og5fNvAwu4bSFc2
yLhZ9W8Gbxh86cuzlxWAe/SJln2tj74ZZEIkl9xsUqJrLQq7L4B0BXRIFfBf89E9
MszmqXqG6dLuTX6so4cOycir01MGozfcxeJsYxgtZMP1aaiQgwIiKkajTeeO/8Qn
VHI3TWKFH6G9crMqQdzWPo6lH5uXe7LU6ATc+iszbYpUheRnx++HpGydXqjeIiZt
QSSMKzDtt3yXNp8loWhSa6tYqSo0p0+Etayn2w7/YIzWebdQpAoBnHVfdgzkZXYx
ES5291ixSXIwH6wutoUBcbwatkYOo+H59PbgmiigceUjxR7yoHAV8NHZLWjX0NL6
mmpN9/ufdrHz/emcj2np90R7APHW0eCG00i386k8iaPbxpB+Bv3N2LXmCmhd65Oa
zhtsTI/4u/rSZ/zxz1VS1g5QUPISx8H38OtCVYjx+ve+Vhe3X5fGtwpxdXFO8WYa
h4VFIJyao3sHb2OIA8RADQBJVA9lfKAL/zdFdtxRE2Be5looJr3GonFgqmnIj3mm
sXspnMvy4sjg16fY2GaRb1e0FHoT0iieJoaW0exhPZnfykt+wjXvT5ME+NxFTDG+
SextY1KGosrZUJMKECPHNyQEtWBOSaP9o9+D4E3Wq9tGW2+h0Hh/PVEhGnpPJvmm
2J3dTwAXyYEbAEFgkmcK1IS33spBS433w+0YupkmVOcrxiQgXzuZ+i5HADUEZS8+
t99L5sdrZJPZhqW0+qnKMISaA5f2BbUpi4rCR5L2Bao5ykbj68676WOISr9o9iZF
hj+Sgh3hz0LxWisH5HqLm92XnPWLg87nmyaeDgkJzLITAdfF+BgDmbGGw9jyDrrd
N4mYiqFz9/MZeaqmpfdry4Oh84YfuAg3sbuHhmqGszfFsU2szWL9t12hJKSRzIHe
liIJ4XX4xYEhzA40ZRL5/c3OApr9Ywoic3UD8indoKHSvzzUxrJtN/F8VEzZ867/
N99gWJZ9tpskt3zseah4T4xCTrq4L/BKUCPHPOGeUSEhZ96PWnyFas8L/mBB+W25
sMNTQSqxbihlgImS/12bEhHKBptCqd7NqWYrdI+ojk/d/q9vmRpWz0C4xPj8KbeW
iH5qMrA3CjEAe5pYpCtWzUiX28v3u6ozahZltTbuKSpeU214vjeyOTO4+DieRQ4a
X3dj2xmW7RNtTp0Uwi3OuClknWElaqT+Jne/XAzkPfCQeVCVm/YXNm7BXGhfHZK6
BS2yDRF34UrjyGcK0G48qSFeG5bovg/7VhcYa2ZkbXxI+IwJSRW+z1OJFgXKiQ2P
iJ+x5isyUGj1BugLcE5DGvNtlZVK80+d3eA8lDYmj5GyVSBfhhYPOqBEqNblHhpf
ms4r3Rp7J6SYDd+w9W5JURh54nw+ZyRUmqiCGIaBaoDSEbUupZdyoEqUjZaMOARV
INgdlQi3SU9PSUXqKy4weIq9rf9MV15Gd560A0xbmvHkUbg3tmsNV6AsYZOcf5RC
W2j8lBLeIcGZtyzHoQUC8ILwSy8N9RYGblMmO1yh2+nxTnUwfZLFcPdLmv9E9KgP
xkM1nvsPPMTS28ixytL6RMdrQBHCOsPxq3ZxNizyMJC7Lbkgia1pPE0YoiuMKegU
sHmYqmc6yy2CIzKLcCmLAzGjIyp2gqbpjVkddoMyqjM7SUCb62Ii4rZS+C9dy75B
gaRdtMHiY5SuwEM5MY1xfIFBdKXNzBmJy3dRxFQoKbuzNS+xt0ed3TtPa2ELMDUS
35CyhjpCTLKf5jvlR3lVeQM5GyifvbHFhxT2U46O/LerS58igwO11S+GuPjNwX7W
bt57tcs0DL/Q4jCC571RCizhxGu2HdKXLAVFE78ta3dHPudgBV0Qhtnwb91ktDCK
hzJdhy0yaeQdxu9kQnuhZFJk5zBa9CQEwAa6ZyQPvJl9RhvZrCFL1/6nWVDA0clB
4H55sJcPV+UkcXsWqutmco3SEmBLlaovAamzAcOVJTf+JugxePYggUDT7Dk+NXDf
qJK6Sas15eNvKGc7U7DZ/mIMRHZUDar/X/QrtXiyz8zT+8vSWO2jV1VDI21dxGF4
vw792YhUkLXA2+EqNhf9yvZPKKd9+w97liNZpcZ4UEIZ1uiMO81VZi2T7XWj1xqe
GL8X3eDthWajzsOkyBDjSE7/ZK1CdVmjxaqf76W0TJBBIe36cGdLx/BAhVQaYGRD
prcGZNF3W+aVVFBfKtmMbHY0e+7dvAClKrM+/UrmInFMjzdqBGNL/lBzNo6yKH31
6gbL3bkDHf3ZVw34UJE7Ne493rusvwADEK364JvYlF7WdCiSsQ6+w5cWoQ7h4DvN
r371pm7Y0jOLdb9ZDZqdNdag2WXqSImrB6q7SQ10SiKHSh5twvnf9FcL4Cpk2Asz
FNe5ZhGAPU2xcIAJhA2ayQBmkCSgEo4i8zMtPR4DoOqJrvfyI0Z6sMy2aEFzalob
P1bo4iJuuJuLe7FdJ5ghTrxYpv3mPXJdR5Y5OLJBTe8sYvrSmmKE3VFbQmwkm53N
yxktHJWab4KIcUk3HB5Wm2Ig9lEwk5EbbKTVts9vTciqRW0Ypv/s/Lz3hmUyu9ju
/k+azB7OFctqte3Pr2c/D2DdYRA5SF+IPFeQI04N/q52xXYLt4cS5z4z9b5CYHqE
QlHFQehuk9b1tDr5D+9JAQNCtggimoTOrNeVIWeiI/V8Er+bxSC7FRF7ATQcBoxm
9rIe47OfYSz5lBu7Mp90OxiaJ29FYtjqAN+2V+KH8kapBpEUYm6Y+fELMhOVUebp
aHN+lJ7T17T8yxmn/kaUr4G4pTO/fv8E3e9afTBHCM2epMmbBvIRrmHOdU1zhBBl
N/d7MI8SNpJaSiNAfRMs7dOfJtSkJzbDxnRGP8r/cs8zp1HzDdw+SqaFWLEADwFn
IBHYqR9EZmLjMA2SVOqPpc83AZOfHZMzkWcg+GHhKKTBUhZ1Nvj3SPMIhya5zhHV
eyGsW3xbkXp5SnifPmlVztiZoOkPHeZePW8/Mq70WRaD+h/PQlxAr5DjDEVHIwx+
GLyPBnlDE+Kb5gcnvnohhW6w4jZqtVFaiwZzFdcOA5jl+jYu5VEIrY1jgr+KpKH0
NkMRX7GFcE7Rwj1TLmQNK0Nxb5bKLIpMuc17kRpisalDHK3RT9JjZA9mLO4y06q6
0znIV6Uwcl8j+tA917s/gyOB/mwQdjJU5nBV9SqTmqDN3K4DuqwPBlJ78lkpafWS
LMbbUrXxt67FFjpxwpdmBW1Q19aDwdwqY8z3RCBIAsJDa2ruws6NrVRe3cIi1CkU
VMkIi7KtJBBhYqE73cWaDg==

`pragma protect end_protected
