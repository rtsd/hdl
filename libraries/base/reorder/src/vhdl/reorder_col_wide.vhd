-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Select and/or reorder data on multiple streams.
--
-- Description:
--   Reorder-row unit that handles a stream that consists of
--   multiple (g_wb_factor) input streams.
--   It assumes that the g_nof_ch_in input channels are equally
--   distributed over the g_wb_factor input streams.
--
-- Remarks:
--

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity reorder_col_wide is
  generic (
    g_wb_factor          : natural := 4;
    g_dsp_data_w         : natural := 18;  -- complex data width, = c_data_w / 2
    g_nof_ch_in          : natural := 256;
    g_nof_ch_sel         : natural := 192;  -- g_nof_ch_sel < g_nof_ch_in
    g_select_file_prefix : string  := "UNUSED";
    g_use_complex        : boolean := true
  );
  port (
    mm_rst              : in  std_logic;
    mm_clk              : in  std_logic;
    dp_rst              : in  std_logic;
    dp_clk              : in  std_logic;

    -- Memory Mapped
    ram_ss_ss_wide_mosi : in  t_mem_mosi;  -- channel select control
    ram_ss_ss_wide_miso : out t_mem_miso;

    -- Streaming
    input_sosi_arr      : in  t_dp_sosi_arr(g_wb_factor - 1 downto 0);
    input_siso_arr      : out t_dp_siso_arr(g_wb_factor - 1 downto 0) := (others => c_dp_siso_rdy);

    output_sosi_arr     : out t_dp_sosi_arr(g_wb_factor - 1 downto 0);  -- selected output
    output_siso_arr     : in  t_dp_siso_arr(g_wb_factor - 1 downto 0) := (others => c_dp_siso_rdy)
  );
end reorder_col_wide;

architecture str of reorder_col_wide is
  constant c_mem_addr_w           : natural := ceil_log2(g_nof_ch_sel);
  constant c_nof_ch_in            : natural := g_nof_ch_in;
  constant c_nof_ch_sel           : natural := g_nof_ch_sel;

  signal ram_ss_ss_wide_mosi_arr  : t_mem_mosi_arr(g_wb_factor - 1 downto 0);
  signal ram_ss_ss_wide_miso_arr  : t_mem_miso_arr(g_wb_factor - 1 downto 0) := (others => c_mem_miso_rst);
begin
  ---------------------------------------------------------------
  -- COMBINE MEMORY MAPPED INTERFACES
  ---------------------------------------------------------------
  -- Combine the internal array of mm interfaces for the selection
  -- memory to one array that is connected to the port of the ss_wide wunit
  u_mem_mux_select : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_wb_factor,
    g_mult_addr_w => c_mem_addr_w
  )
  port map (
    mosi     => ram_ss_ss_wide_mosi,
    miso     => ram_ss_ss_wide_miso,
    mosi_arr => ram_ss_ss_wide_mosi_arr,
    miso_arr => ram_ss_ss_wide_miso_arr
  );

  ---------------------------------------------------------------
  -- INSTANTIATE MULTIPLE SINGLE CHANNEL SUBBAND SELECT UNITS
  ---------------------------------------------------------------
  gen_reorder_col_arr : for I in 0 to g_wb_factor - 1 generate
    u_reorder_col : entity work.reorder_col
    generic map (
      g_dsp_data_w         => g_dsp_data_w,
      g_nof_ch_in          => c_nof_ch_in,
      g_nof_ch_sel         => c_nof_ch_sel,
      g_select_file_name   => sel_a_b(g_select_file_prefix = "UNUSED", "UNUSED", g_select_file_prefix & "_" & natural'image(I) & ".hex"),
      g_use_complex        => g_use_complex
    )
    port map (
      mm_rst         => mm_rst,
      mm_clk         => mm_clk,
      dp_rst         => dp_rst,
      dp_clk         => dp_clk,

      -- Memory Mapped
      ram_ss_ss_mosi => ram_ss_ss_wide_mosi_arr(I),
      ram_ss_ss_miso => ram_ss_ss_wide_miso_arr(I),

      -- Streaming
      input_sosi     => input_sosi_arr(I),
      input_siso     => input_siso_arr(I),

      output_sosi    => output_sosi_arr(I),
      output_siso    => output_siso_arr(I)
    );
  end generate;
end str;
