-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose : Monitor signal path statistics (array version)
-- Description :
--   Array wrapper to allow insatntiation of g_nof_streams channel ADUH
-- Remarks:

entity mms_aduh_monitor_arr is
  generic (
    g_cross_clock_domain   : boolean := true;  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
    g_nof_streams          : positive := 1;
    g_symbol_w             : natural := 8;
    g_nof_symbols_per_data : natural := 4;  -- big endian in_data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
    g_nof_accumulations    : natural := 800 * 10**6;  -- integration time in symbols, defines internal accumulator widths
    g_buffer_nof_symbols   : natural := 1024;
    g_buffer_use_sync      : boolean := false  -- when TRUE start filling the buffer after the in_sync, else after the last word was read
  );
  port (
    -- Memory-mapped clock domain
    mm_rst     : in  std_logic;
    mm_clk     : in  std_logic;

    reg_mosi   : in  t_mem_mosi;  -- read only access to the mean_sum and power_sum
    reg_miso   : out t_mem_miso;
    buf_mosi   : in  t_mem_mosi;  -- read and overwrite access to the data buffer
    buf_miso   : out t_mem_miso;

    -- Streaming clock domain
    st_rst     : in  std_logic;
    st_clk     : in  std_logic;

    in_sosi_arr: in t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_aduh_monitor_arr;

architecture str of mms_aduh_monitor_arr is
  constant c_reg_adr_w    : natural := 2;
  constant c_buf_adr_w    : natural := ceil_log2(g_buffer_nof_symbols);

  signal reg_mosi_arr     : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr     : t_mem_miso_arr(g_nof_streams - 1 downto 0);
  signal buf_mosi_arr     : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal buf_miso_arr     : t_mem_miso_arr(g_nof_streams - 1 downto 0);
begin
  u_common_mem_mux_reg : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_reg_adr_w
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  u_common_mem_mux_buf : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_buf_adr_w
  )
  port map (
    mosi     => buf_mosi,
    miso     => buf_miso,
    mosi_arr => buf_mosi_arr,
    miso_arr => buf_miso_arr
  );

  gen_aduh_monitor : for I in 0 to g_nof_streams - 1 generate
    u_mms_aduh_monitor : entity work.mms_aduh_monitor
    generic map (
      g_cross_clock_domain   => g_cross_clock_domain,
      g_symbol_w             => g_symbol_w,
      g_nof_symbols_per_data => g_nof_symbols_per_data,
      g_nof_accumulations    => g_nof_accumulations,
      g_buffer_nof_symbols   => g_buffer_nof_symbols,
      g_buffer_use_sync      => g_buffer_use_sync
   )
    port map (
      -- Clocks and reset
      mm_rst                 => mm_rst,
      mm_clk                 => mm_clk,
      st_rst                 => st_rst,
      st_clk                 => st_clk,

      -- Memory Mapped Slave in mm_clk domain
      reg_mosi               => reg_mosi_arr(I),
      reg_miso               => reg_miso_arr(I),
      buf_mosi               => buf_mosi_arr(I),
      buf_miso               => buf_miso_arr(I),

      -- Streaming inputs
      in_sosi                => in_sosi_arr(I)
    );
  end generate;
end str;
