-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: R. vd Walle
-- Purpose: This package contains rdma_generator specific constants and/or functions
-- Description:
--
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;

package rdma_generator_pkg is
  constant c_rdma_generator_nof_octet_generate_100gbe : natural := 64;
  constant c_rdma_generator_nof_octet_output_100gbe   : natural := 64;

  -- hdr_field_sel bit selects where the hdr_field value is set:
  -- . 0 = data path controlled, value is set in data path, so field_default()
  --       is not used.
  -- . 1 = MM controlled, value is set via MM or by the field_default(), so any
  --       data path setting in eth_tester.vhd is not used.
  -- Remarks:
  -- . For constant values it is convenient to use MM controlled, because then
  --   the field_default() is used that can be set here in
  --   c_rdma_generator_hdr_field_arr.
  -- . For reserved values it is convenient to use MM controlled, because then
  --   in future they could still be changed via MM without having to recompile
  --   the FW.
  -- . Typically only use data path controlled if the value has to be set
  --   dynamically, so dependent on the state of the FW.
  -- . If a data path controlled field is not set in the FW, then it defaults
  --   to 0 by declaring hdr_fields_in_arr with all 0. Hence e.g. udp_checksum
  --   = 0 can be achieve via data path and default hdr_fields_in_arr = 0 or
  --   via MM controlled and field_default(0).
  constant c_rdma_generator_dp_nof_hdr_fields : natural := 4;
  constant c_rdma_generator_dp_hdr_field_sel  : std_logic_vector(c_rdma_generator_dp_nof_hdr_fields - 1 downto 0) := "0100";

  constant c_rdma_generator_dp_hdr_field_arr : t_common_field_arr(c_rdma_generator_dp_nof_hdr_fields - 1 downto 0) := (
      ( field_name_pad("dp_length"  ), "RW", 16, field_default(0) ),
      ( field_name_pad("dp_reserved"), "RW", 15, field_default(0) ),
      ( field_name_pad("dp_sync"    ), "RW",  1, field_default(0) ),
      ( field_name_pad("dp_bsn"     ), "RW", 64, field_default(0) )
  );
  constant c_rdma_generator_dp_reg_hdr_dat_addr_w    : natural := ceil_log2(field_nof_words(c_rdma_generator_dp_hdr_field_arr, c_word_w));
  constant c_rdma_generator_dp_reg_hdr_dat_addr_span : natural := 2**c_rdma_generator_dp_reg_hdr_dat_addr_w;

  constant c_rdma_generator_dp_app_hdr_len  : natural :=  12;  -- octets

  -- RoCEv2 header for RDMA write operation (excluding ETH, IP, UDP)
  -- Base Transport Header (BTH) + RDMA Extended Transport Header (RETH) + Immediate Data
  constant c_rdma_generator_roce_nof_hdr_fields : natural := 12 + 4 + 13 + 3 + 1;
  constant c_rdma_generator_roce_hdr_field_sel  : std_logic_vector(c_rdma_generator_roce_nof_hdr_fields - 1 downto 0) :=  "111011111001" & "0100" & "1111111111111" & "111" & "1";

  constant c_rdma_generator_roce_hdr_field_arr : t_common_field_arr(c_rdma_generator_roce_nof_hdr_fields - 1 downto 0) := (
      ( field_name_pad("ip_version"          ), "RW",  4, field_default(4) ),
      ( field_name_pad("ip_header_length"    ), "RW",  4, field_default(5) ),
      ( field_name_pad("ip_services"         ), "RW",  8, field_default(0) ),
      ( field_name_pad("ip_total_length"     ), "RW", 16, field_default(0) ),  -- depends on BG block size, so set by data path
      ( field_name_pad("ip_identification"   ), "RW", 16, field_default(0) ),
      ( field_name_pad("ip_flags"            ), "RW",  3, field_default(2) ),
      ( field_name_pad("ip_fragment_offset"  ), "RW", 13, field_default(0) ),
      ( field_name_pad("ip_time_to_live"     ), "RW",  8, field_default(127) ),
      ( field_name_pad("ip_protocol"         ), "RW",  8, field_default(17) ),
      ( field_name_pad("ip_header_checksum"  ), "RW", 16, field_default(0) ),
      ( field_name_pad("ip_src_addr"         ), "RW", 32, field_default(0) ),
      ( field_name_pad("ip_dst_addr"         ), "RW", 32, field_default(0) ),  -- c_eth_tester_ip_dst_addr

      ( field_name_pad("udp_src_port"        ), "RW", 16, field_default(0) ),
      ( field_name_pad("udp_dst_port"        ), "RW", 16, field_default(0) ),  -- c_eth_tester_udp_dst_port
      ( field_name_pad("udp_total_length"    ), "RW", 16, field_default(0) ),  -- depends on BG block size, so set by data path
      ( field_name_pad("udp_checksum"        ), "RW", 16, field_default(0) ),

      ( field_name_pad("bth_opcode"          ), "RW",  8, field_default(0) ),
      ( field_name_pad("bth_se"              ), "RW",  1, field_default(0) ),
      ( field_name_pad("bth_m"               ), "RW",  1, field_default(0) ),
      ( field_name_pad("bth_pad"             ), "RW",  2, field_default(0) ),
      ( field_name_pad("bth_tver"            ), "RW",  4, field_default(0) ),
      ( field_name_pad("bth_partition_key"   ), "RW", 16, field_default(0) ),
      ( field_name_pad("bth_fres"            ), "RW",  1, field_default(0) ),
      ( field_name_pad("bth_bres"            ), "RW",  1, field_default(0) ),
      ( field_name_pad("bth_reserved_a"      ), "RW",  6, field_default(0) ),
      ( field_name_pad("bth_dest_qp"         ), "RW", 16, field_default(0) ),
      ( field_name_pad("bth_ack_req"         ), "RW",  1, field_default(0) ),
      ( field_name_pad("bth_reserved_b"      ), "RW",  7, field_default(0) ),
      ( field_name_pad("bth_psn"             ), "RW", 32, field_default(0) ),

      ( field_name_pad("reth_virtual_address"), "RW", 64, field_default(0) ),
      ( field_name_pad("reth_r_key"          ), "RW", 32, field_default(0) ),
      ( field_name_pad("reth_dma_length"     ), "RW", 32, field_default(0) ),

      ( field_name_pad("immediate_data"      ), "RW", 32, field_default(0) )
  );
  constant c_rdma_generator_roce_reg_hdr_dat_addr_w    : natural := ceil_log2(field_nof_words(c_rdma_generator_roce_hdr_field_arr, c_word_w));
  constant c_rdma_generator_roce_reg_hdr_dat_addr_span : natural := 2**c_rdma_generator_roce_reg_hdr_dat_addr_w;

  constant c_rdma_generator_roce_hdr_len  : natural :=  32;  -- octets
  constant c_rdma_generator_roce_icrc_len : natural :=  4;   -- octets
end rdma_generator_pkg;

package body rdma_generator_pkg is
end rdma_generator_pkg;
