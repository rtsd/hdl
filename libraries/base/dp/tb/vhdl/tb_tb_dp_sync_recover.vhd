-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Verify multiple variations of tb_dp_sync_recover
-- Description:
-- Usage:
-- > as 3
-- > run -all
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_sync_recover is
end tb_tb_dp_sync_recover;

architecture tb of tb_tb_dp_sync_recover is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--    g_nof_data_per_block     : NATURAL := 16;
--    g_nof_blk_per_sync       : NATURAL := 8;
--    g_gap_size_during_block  : NATURAL := 0;
--    g_gap_size_between_block : NATURAL := 0;
--    g_init_bsn               : NATURAL := 23;
--    g_bsn_at_restart         : NATURAL := 40; -- the bsn index at which to restart the dut.
--    g_dut_latency            : NATURAL := 25;
--    g_nof_repeat             : NATURAL := 14

  u_no_gaps            : entity work.tb_dp_sync_recover generic map(16, 8, 0, 0, 3, 50,     10,  14);
  u_gap                : entity work.tb_dp_sync_recover generic map(16, 8, 1, 3, 3, 50,     10,  14);
  u_restart_at_sync    : entity work.tb_dp_sync_recover generic map(16, 8, 1, 3, 3, 2 * 8 + 3,  10,  14);
  u_restart_at_sync_hi : entity work.tb_dp_sync_recover generic map(16, 8, 1, 3, 3, 2 * 8 + 3,  100, 14);
  u_restart_at_sync_lo : entity work.tb_dp_sync_recover generic map(16, 8, 1, 3, 3, 2 * 8 + 3,  1,   14);
  u_high_latency       : entity work.tb_dp_sync_recover generic map(16, 8, 1, 3, 3, 50,     100, 14);
  u_low_latency        : entity work.tb_dp_sync_recover generic map(16, 8, 1, 3, 3, 50,     1,   14);
end tb;
