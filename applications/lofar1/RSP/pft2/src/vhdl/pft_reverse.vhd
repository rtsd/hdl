-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: pipelined FFT
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pft_reverse is
  generic (
    g_fft_sz          : natural;
    g_fft_sz_w        : natural;
    g_data_w          : natural
  );
  port (
    rddata_re         : in  std_logic_vector(g_data_w - 1 downto 0);
    rddata_im         : in  std_logic_vector(g_data_w - 1 downto 0);
    rdaddr            : out std_logic_vector(g_fft_sz_w - 1 downto 0);
    rden              : out std_logic;
    rdval             : in  std_logic;
    rdsync            : in  std_logic;
    out_dat_re        : out std_logic_vector(g_data_w - 1 downto 0);
    out_dat_im        : out std_logic_vector(g_data_w - 1 downto 0);
    out_val           : out std_logic;
    out_sync          : out std_logic;
    page_rdy          : in  std_logic;
    page_done         : out std_logic;
    clk               : in  std_logic;
    rst               : in  std_logic
  );
end pft_reverse;

architecture rtl of pft_reverse is
  signal i_rdaddr   : std_logic_vector(rdaddr'range);
  signal nxt_rdaddr : std_logic_vector(rdaddr'range);
  signal i_rden     : std_logic;
  signal nxt_rden   : std_logic;
  signal rdrdy      : std_logic;
begin
  rdaddr    <= i_rdaddr;
  rden      <= i_rden;
  page_done <= rdrdy;

  registers : process (rst, clk)
  begin
    if rst = '1' then
      -- Output signals.
      i_rdaddr <= (others => '0');
      i_rden   <= '0';
      -- Internal signals.
    elsif rising_edge(clk) then
      -- Output signals.
      i_rdaddr <= nxt_rdaddr;
      i_rden   <= nxt_rden;
      -- Internal signals.
    end if;
  end process;

  read_enable_control : process (i_rden, page_rdy, rdrdy)
  begin
    nxt_rden <= i_rden;
    if page_rdy = '1' then
      nxt_rden <= '1';
    elsif rdrdy = '1' then
      nxt_rden <= '0';
    end if;
  end process;

  read_addr_control : process (i_rdaddr, i_rden)
  begin
    rdrdy      <= '0';
    nxt_rdaddr <= i_rdaddr;
    if unsigned(i_rdaddr) >= g_fft_sz - 1 then
      nxt_rdaddr <= (others => '0');
      rdrdy      <= '1';
    elsif i_rden = '1' then
      nxt_rdaddr <= std_logic_vector(unsigned(i_rdaddr) + 1);
    end if;
  end process;

  out_dat_re <= rddata_re when rdval = '1' else (others => '0');
  out_dat_im <= rddata_im when rdval = '1' else (others => '0');
  out_val    <= '1' when rdval = '1' else '0';
  out_sync   <= '1' when rdsync = '1' else '0';
end rtl;
