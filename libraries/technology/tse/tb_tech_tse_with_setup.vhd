-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for tech_tse for the Tripple Speed Ethernet IP technology
--          wrapper, with setup.
-- Description:
--   Same tb as tb_tech_tse.vhd, but instead:
--   . fixed use TSE IP (c_sim_level = 0)
--   . using TSE setup in DUT
--   . verify external MM access to TSE after setup in p_mm_setup.
--   . use c_jumbo_en = FALSE for maximum 1500 packet size as with unb_osy, a
--     9000 octet packet is received properly, but has rx_src_out.err = 3
--     indicating invalid length. Use c_jumbo_en = TRUE to avoid invalid
--     length.
-- Usage:
--   > as 10
--   > run -all

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use WORK.tech_tse_pkg.all;
use WORK.tb_tech_tse_pkg.all;

entity tb_tech_tse_with_setup is
  -- Test bench control parameters
  generic (
    g_technology : natural := c_tech_select_default;
    --   g_data_type = c_tb_tech_tse_data_type_symbols  = 0
    --   g_data_type = c_tb_tech_tse_data_type_counter  = 1
    g_data_type  : natural := c_tb_tech_tse_data_type_symbols;
    g_tb_end     : boolean := true  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
  );
  port (
    tb_end : out std_logic
  );
end tb_tech_tse_with_setup;

architecture tb of tb_tech_tse_with_setup is
  constant c_sim                : boolean := true;
  constant c_sim_level          : natural := 0;  -- 0 = use IP; 1 = use fast serdes model;
  constant c_jumbo_en           : boolean := true;

  constant sys_clk_period       : time := 10 ns;  -- 100 MHz
  constant eth_clk_period       : time :=  8 ns;  -- 125 MHz
  constant cable_delay          : time := sel_a_b(c_sim_level = 0, 12 ns, 0 ns);

  constant c_promis_en          : boolean := false;
  constant c_tx_ready_latency   : natural := c_tech_tse_tx_ready_latency;  -- 0, 1 are supported, must match TSE MAC c_tech_tse_tx_ready_latency
  constant c_nof_tx_not_valid   : natural := 0;  -- when > 0 then pull tx valid low for c_nof_tx_not_valid beats during tx

  --CONSTANT c_pkt_length_arr     : t_nat_natural_arr := (0, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 1472, 1473, 9000);
  constant c_pkt_length_arr     : t_nat_natural_arr := array_init(0, 80, 1) & array_init(1499, 2, 1) & 9000;
  constant c_nof_pkt            : natural := c_pkt_length_arr'length;

  constant c_dst_mac            : std_logic_vector(c_network_eth_mac_slv'range) := X"10FA01020300";
  constant c_src_mac            : std_logic_vector(c_network_eth_mac_slv'range) := X"123456789ABC";  -- = 12-34-56-78-9A-BC
  constant c_ethertype          : std_logic_vector(c_network_eth_type_slv'range) := X"10FA";
  constant c_etherlen           : std_logic_vector(c_network_eth_type_slv'range) := "0000000000010000";

  -- Packet headers
  constant c_eth_header_loopback : t_network_eth_header := (c_src_mac, c_src_mac, c_ethertype);
  constant c_eth_header_etherlen : t_network_eth_header := (c_src_mac, c_src_mac, c_etherlen);

  signal total_header_loopback   : t_network_total_header;
  signal total_header_etherlen   : t_network_total_header;

  -- Clocks and reset
  signal rx_end            : std_logic := '0';
  signal eth_clk           : std_logic := '0';  -- tse reference clock
  signal sys_clk           : std_logic := '0';  -- system clock
  signal st_clk            : std_logic;  -- stream clock
  signal mm_clk            : std_logic;  -- memory-mapped bus clock
  signal mm_rst            : std_logic;  -- reset synchronous with mm_clk

  -- TSE MAC control interface
  signal tse_setup_done    : std_logic;

  signal mm_init           : std_logic := '1';
  signal mm_copi           : t_mem_copi;
  signal mm_cipo           : t_mem_cipo;
  signal mm_rddata         : std_logic_vector(c_word_w - 1 downto 0);  -- for view in Wave window

  -- TSE MAC transmit interface
  -- . The tb is the ST source
  signal tx_en             : std_logic := '1';
  signal tx_siso           : t_dp_siso;
  signal tx_sosi           : t_dp_sosi;
  -- . MAC specific
  signal tx_mac_in         : t_tech_tse_tx_mac;
  signal tx_mac_out        : t_tech_tse_tx_mac;

  -- TSE MAC receive interface
  -- . The tb is the ST sink
  signal rx_sosi           : t_dp_sosi;
  signal rx_siso           : t_dp_siso;
  signal rx_data           : std_logic_vector(c_word_w - 1 downto 0);  -- for view in Wave window
  -- . MAC specific
  signal rx_mac_out        : t_tech_tse_rx_mac;

  -- TSE PHY interface
  signal eth_txp           : std_logic;
  signal eth_rxp           : std_logic;

  signal tse_led           : t_tech_tse_led;

  -- Verification
  signal tx_pkt_cnt     : natural := 0;
  signal rx_pkt_cnt     : natural := 0;
begin
  eth_clk <= not eth_clk after eth_clk_period / 2;  -- TSE reference clock
  sys_clk <= not sys_clk after sys_clk_period / 2;  -- System clock

  mm_clk  <= sys_clk;
  st_clk  <= sys_clk;

  -- Use signal to leave unused fields 'X'
  total_header_loopback.eth <= c_eth_header_loopback;
  total_header_etherlen.eth <= c_eth_header_etherlen;

  mm_rddata <= mm_cipo.rddata(c_word_w - 1 downto 0);
  rx_data <= rx_sosi.data(c_word_w - 1 downto 0);

  p_mm_setup : process
    variable v_version : natural;
  begin
    mm_init <= '1';
    mm_copi.wr <= '0';
    mm_copi.rd <= '0';

    -- reset release
    mm_rst <= '1';
    for I in 0 to 9 loop wait until rising_edge(mm_clk); end loop;
    mm_rst <= '0';
    for I in 0 to 9 loop wait until rising_edge(mm_clk); end loop;

    -- Wait for tech_tse_with_setup to finish MM access to TSE
    proc_common_wait_until_high(mm_clk, tse_setup_done);
    proc_common_wait_some_cycles(mm_clk, 10);

    -- Verify external MM access to TSE
    proc_mem_mm_bus_rd(16#000#, mm_clk, mm_cipo, mm_copi);  -- REV --> CUST_VERSION & 0x0901, 0x1200, 0x1304
    case c_tech_select_default is
      when c_tech_stratixiv    => v_version := 16#0901#;  -- unb1
      when c_tech_arria10_e1sg => v_version := 16#1200#;  -- unb2b
      when c_tech_arria10_e2sg => v_version := 16#1304#;  -- unb2c
      when others              => v_version := 0;  -- default
    end case;
    assert unsigned(mm_rddata(c_16 - 1 downto 0)) = v_version
      report "Wrong external MM read access result."
      severity ERROR;

    -- Wait for link synchronisation
    proc_common_wait_until_high(mm_clk, tse_led.link);
    proc_common_wait_some_cycles(mm_clk, 10);

    mm_init <= '0';
    wait;
  end process;

  p_ff_transmitter : process
  begin
    -- . Avalon ST
    tx_sosi.data  <= (others => '0');
    tx_sosi.valid <= '0';
    tx_sosi.sop   <= '0';
    tx_sosi.eop   <= '0';
    tx_sosi.empty <= (others => '0');
    tx_sosi.err   <= (others => '0');
    -- . MAC specific
    tx_mac_in.crc_fwd <= '0';  -- when '0' then TSE MAC generates the TX CRC field

    while mm_init /= '0' loop
      wait until rising_edge(st_clk);
    end loop;
    for I in 0 to 9 loop wait until rising_edge(st_clk); end loop;

    -- Loopback txp->rxp so DST_MAC = c_src_mac to send to itself

    -- TX frame:
    -- . I=0 is empty payload, so only 4 words of the ETH header with 46 padding zeros, so empty = 2
    -- . For I=1 to 46 the payload length remains 46 with padding zeros, so empty = 2
    -- . For I>46 the payload length is I and empty = 4 - (I mod 4)

    for I in 0 to c_nof_pkt - 1 loop
      proc_tech_tse_tx_packet(total_header_loopback, c_pkt_length_arr(I), g_data_type, c_tx_ready_latency, c_nof_tx_not_valid, st_clk, tx_en, tx_siso, tx_sosi);
    end loop;

    for I in 0 to 1500 * 2 loop wait until rising_edge(st_clk); end loop;
    rx_end <= '1';
    wait;
  end process;

  p_ff_receiver : process
  begin
    -- . Avalon ST
    rx_siso.ready <= '0';

    while mm_init /= '0' loop
      wait until rising_edge(st_clk);
    end loop;

    -- Receive forever
    while true loop
      proc_tech_tse_rx_packet(total_header_loopback, g_data_type, st_clk, rx_sosi, rx_siso);
    end loop;

    wait;
  end process;

  dut : entity work.tech_tse_with_setup
  generic map (
    g_technology => g_technology,
    g_ETH_PHY    => "LVDS",  -- "LVDS" (default): uses LVDS IOs for ctrl_unb_common, "XCVR": uses tranceiver PHY
    g_jumbo_en   => c_jumbo_en,
    g_sim        => c_sim,
    g_sim_level  => c_sim_level  -- 0 = use IP; 1 = use fast serdes model;
  )
  port map (
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    eth_clk        => eth_clk,
    tx_snk_clk     => st_clk,
    rx_src_clk     => st_clk,

    -- TSE setup
    src_mac        => c_src_mac,
    setup_done     => tse_setup_done,

    -- Memory Mapped Slave
    mm_ctlr_copi   => mm_copi,
    mm_ctlr_cipo   => mm_cipo,

    -- MAC transmit interface
    -- . ST sink
    tx_snk_in      => tx_sosi,
    tx_snk_out     => tx_siso,

    -- MAC receive interface
    -- . ST Source
    rx_src_in      => rx_siso,
    rx_src_out     => rx_sosi,

    -- PHY interface
    eth_txp        => eth_txp,
    eth_rxp        => eth_rxp,

    tse_led        => tse_led
  );

  -- Loopback
  eth_rxp <= transport eth_txp after cable_delay;

  -- Verification
  tx_pkt_cnt <= tx_pkt_cnt + 1 when tx_sosi.sop = '1' and rising_edge(st_clk);
  rx_pkt_cnt <= rx_pkt_cnt + 1 when rx_sosi.eop = '1' and rising_edge(st_clk);

  p_verify : process
  begin
    tb_end <= '0';
    wait until rx_end = '1';
    -- Verify that all transmitted packets have been received
    if tx_pkt_cnt = 0 then
      report "No packets were transmitted."
        severity ERROR;
    elsif rx_pkt_cnt = 0 then
      report "No packets were received."
        severity ERROR;
    elsif tx_pkt_cnt /= rx_pkt_cnt then
      report "Not all transmitted packets were received."
        severity ERROR;
    end if;
    tb_end <= '1';

    wait for 1 ns;
    if g_tb_end = false then
      report "Tb simulation finished."
        severity NOTE;
    else
      report "Tb simulation finished."
        severity FAILURE;
    end if;
    wait;
  end process;
end tb;
