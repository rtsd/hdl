-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Self-checking testbench for simulating lofar2_unb2c_sdp_station_bf capturing BST UDP offload packets.
--
-- Description:
--   MM control actions:
--
--   1) Enable BSN source and enable BST offload
--
--   2) Verify ethernet statistics using eth_statistics, it checks the number of
--      received packets and the total number of valid data. The content of the packets is not verified.
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   > run -a
--   Takes about 20 m
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2c_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, lofar2_unb2c_sdp_station_lib, eth_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use lofar2_sdp_lib.tb_sdp_pkg.all;

entity tb_lofar2_unb2c_sdp_station_bf_bst_offload is
end tb_lofar2_unb2c_sdp_station_bf_bst_offload;

architecture tb of tb_lofar2_unb2c_sdp_station_bf_bst_offload is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_gn_nr           : natural := c_unb_nr * 4 + c_node_nr;
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_gn_nr, 8);
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_mm_clk_period       : time := 10 ns;  -- 100 MHz internal mm_clk
  constant c_bck_ref_clk_period  : time := 5 ns;

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C

  constant c_nof_block_per_sync  : natural := 16;  -- long enough to stream out udp data
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_pps_period          : natural := c_nof_clk_per_sync;
  constant c_wpfb_sim            : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands, c_nof_block_per_sync);
  constant c_nof_sync            : natural := 1;

  -- header fields
  constant c_exp_beamlet_index   : natural := 0;  -- depends on beamset bset * c_sdp_S_sub_bf

  constant c_exp_sdp_info        : t_sdp_info := (
                                     TO_UVEC(3, 6),  -- antenna_field_index
                                     TO_UVEC(601, 10),  -- station_id
                                     '0',  -- antenna_band_index
                                     x"7FFFFFFF",  -- observation_id, use > 0 to avoid Warning: (vsim-151) NUMERIC_STD.TO_INTEGER: Value -2 is not in bounds of subtype NATURAL.
                                     b"01",  -- nyquist_zone_index, 0 = first, 1 = second, 2 = third
                                     '1',  -- f_adc, 0 = 160 MHz, 1 = 200 MHz
                                     '0',  -- fsub_type, 0 = critically sampled, 1 = oversampled
                                     '0',  -- beam_repositioning_flag
                                     x"1400"  -- block_period = 5120
                                   );

  -- MM
  constant c_mm_file_reg_sdp_info         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_SDP_INFO";
  constant c_mm_file_reg_bsn_source_v2    : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_stat_enable_bst  : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_STAT_ENABLE_BST";
  constant c_mm_file_reg_stat_hdr_dat_bst : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_STAT_HDR_DAT_BST";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0) := (others => '0');
  signal eth_done            : std_logic := '0';

  -- . 1GbE output
  constant c_eth_check_nof_packets        : natural := c_nof_sync * 1;  -- 1 received packet per sync interval
  constant c_eth_header_size              : natural := 19;  -- words
  constant c_eth_crc_size                 : natural := 1;  -- word
  constant c_eth_packet_size              : natural := c_eth_header_size + c_eth_crc_size + (c_sdp_W_statistic / c_word_w) * c_sdp_S_sub_bf * c_sdp_N_pol;  -- 20 + 2 * 488 * 2 = 1972
  constant c_eth_check_nof_valid          : natural := c_eth_check_nof_packets * c_eth_packet_size;

  -- eth statistics should be done after c_nof_sync + 1 intervals (+1 because first new_interval is skipped)
  constant c_eth_runtime_timeout          : time := (c_nof_sync + 2) * c_nof_clk_per_sync * c_ext_clk_period;

  constant c_exp_ip_header_checksum       : natural := 16#C76C#;  -- value obtained from rx_sdp_stat_header.ip.header_checksum in wave window

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal ext_pps             : std_logic := '0';
  signal pps_rst             : std_logic := '1';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_txp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_rxp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');

  -- Rx packets
  signal eth_rx_sosi         : t_dp_sosi;
  signal eth_rx_data         : std_logic_vector(c_32 - 1 downto 0);

  -- Decode packets
  signal rx_offload_sosi     : t_dp_sosi;
  signal rx_hdr_fields_out   : std_logic_vector(1023 downto 0);
  signal rx_hdr_fields_raw   : std_logic_vector(1023 downto 0) := (others => '0');
  signal rx_sdp_stat_header  : t_sdp_stat_header;
  signal exp_sdp_stat_header : t_sdp_stat_header;

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref     : std_logic;
  signal jesd204b_sync_n     : std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk(0) <= not eth_clk(0) after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(5, c_pps_period, '1', pps_rst, ext_clk, pps);
  jesd204b_sysref <= pps;
  ext_pps <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2c_sdp_station_bf : entity lofar2_unb2c_sdp_station_lib.lofar2_unb2c_sdp_station
  generic map (
    g_design_name            => "lofar2_unb2c_sdp_station_bf",
    g_design_note            => "",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr,
    g_wpfb                   => c_wpfb_sim,
    g_bsn_nof_clk_per_sync   => c_nof_clk_per_sync
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK      => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => jesd204b_sysref,
    JESD204B_SYNC_N => jesd204b_sync_n
  );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;
    pps_rst <= '0';

    ----------------------------------------------------------------------------
    -- Offload destination MAC/IP/UDP
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_stat_hdr_dat_bst, 41, TO_UINT(c_sdp_stat_eth_dst_mac(47 downto 32)), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_stat_hdr_dat_bst, 40, TO_SINT(c_sdp_stat_eth_dst_mac(31 downto 0)), tb_clk);  -- use signed to fit 32 b in INTEGER
    mmf_mm_bus_wr(c_mm_file_reg_stat_hdr_dat_bst, 25, TO_SINT(c_sdp_stat_ip_dst_addr), tb_clk);  -- use signed to fit 32 b in INTEGER
    mmf_mm_bus_wr(c_mm_file_reg_stat_hdr_dat_bst, 23, TO_UINT(c_sdp_stat_udp_dst_port), tb_clk);
    proc_common_wait_cross_clock_domain_latency(c_mm_clk_period, c_ext_clk_period);

    ----------------------------------------------------------------------------
    -- Set SDP info
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  8, TO_UINT(c_exp_sdp_info.antenna_field_index), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  7, TO_UINT(c_exp_sdp_info.station_id), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  6, TO_UINT(slv(c_exp_sdp_info.antenna_band_index)), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  5, TO_UINT(c_exp_sdp_info.observation_id), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  4, TO_UINT(c_exp_sdp_info.nyquist_zone_index), tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_sdp_info,  1, TO_UINT(slv(c_exp_sdp_info.beam_repositioning_flag)), tb_clk);

    ----------------------------------------------------------------------------
    -- Enable BSN
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 3,                    0, tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 2,                    0, tb_clk);  -- Init BSN = 0
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 1,   c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0,         16#00000001#, tb_clk);  -- Enable BSN immediately

    ----------------------------------------------------------------------------
    -- Offload enable
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_stat_enable_bst, 0, 1, tb_clk);  -- only beamset 0

    -- wait for udp offload is done
    proc_common_wait_until_high(ext_clk, eth_done);

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;

  -------------------------------------------------------------------------
  -- Verify proper DUT 1GbE offload output using Ethernet packet statistics
  -------------------------------------------------------------------------
  u_eth_statistics : entity eth_lib.eth_statistics
    generic map (
      g_runtime_nof_packets => c_eth_check_nof_packets,
      g_runtime_timeout     => c_eth_runtime_timeout,
      g_check_nof_valid     => true,
      g_check_nof_valid_ref => c_eth_check_nof_valid
   )
  port map (
    eth_serial_in => eth_txp(0),
    eth_src_out   => eth_rx_sosi,
    tb_end        => eth_done
  );

  eth_rx_data <= eth_rx_sosi.data(c_32 - 1 downto 0);

  -- . Verify XST packet header
  u_dp_offload_rx : entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams         => 1,
    g_data_w              => c_word_w,
    g_hdr_field_arr       => c_sdp_stat_hdr_field_arr,
    g_remove_crc          => true,
    g_crc_nof_words       => 1
  )
  port map (
    mm_rst                => pps_rst,
    mm_clk                => tb_clk,

    dp_rst                => pps_rst,
    dp_clk                => eth_clk(0),

    snk_in_arr(0)         => eth_rx_sosi,

    src_out_arr(0)        => rx_offload_sosi,

    hdr_fields_out_arr(0) => rx_hdr_fields_out,
    hdr_fields_raw_arr(0) => rx_hdr_fields_raw
  );

  rx_sdp_stat_header <= func_sdp_map_stat_header(rx_hdr_fields_raw);

  exp_sdp_stat_header <= func_sdp_compose_stat_header(c_exp_ip_header_checksum,  -- calculated by IO eth
                                                      c_exp_sdp_info,
                                                      "BST",
                                                      '1',  -- fixed '1' for BST and XST, weighted_subbands_flag
                                                      c_gn_nr,
                                                      c_nof_block_per_sync,
                                                      0,  -- not used for BST, sst_signal_input
                                                      c_exp_beamlet_index,
                                                      0,  -- not used for BST, subband_index
                                                      0,  -- not used for BST, xst_signal_input_A
                                                      0,  -- not used for BST, xst_signal_input_B
                                                      0);  -- dp_bsn

  p_verify_header : process(rx_offload_sosi)
    variable v_bool : boolean;
  begin
    -- Prepare exp_sdp_stat_header before rx_offload_sosi.eop, so that it can be verified at rx_offload_sosi.eop
    if rx_offload_sosi.eop = '1' then
      v_bool := func_sdp_verify_stat_header("BST", rx_sdp_stat_header, exp_sdp_stat_header);
    end if;
  end process;
end tb;
