-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra, 13 Apr 2017
-- Purpose:
-- . Multi test bench
-- Description:
-- Usage:
-- > as 5
-- > run -all
--

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_throttle_xon is
end tb_tb_dp_throttle_xon;

architecture tb of tb_tb_dp_throttle_xon is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 3
  -- > run -all                 --> OK

-- -- general
-- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active or random flow control
-- g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active or random flow control
-- -- specific
-- g_restart_at_sync    : BOOLEAN := FALSE;  -- if true restart xon control at each snk_in_sync, else and start at first valid and never restart
-- g_block_size         : NATURAL := 10;    -- number of valid data per block marked by sop and eop
-- g_nof_block_on       : NATURAL := 13;     -- number of blocks that snk_out.xon is active
-- g_nof_block_per_sync : NATURAL := 10;
-- g_nof_clk_off        : NATURAL := 37     -- must be > g_block_size, number of clock cycles that snk_out.xon is kept inactive

  u_act_xon_shorter_than_sync_interval : entity work.tb_dp_throttle_xon generic map (e_active, e_active, false, 20,  7, 10, 37);
  u_act_xon_equal_to_sync_interval     : entity work.tb_dp_throttle_xon generic map (e_active, e_active, false, 20, 10, 10, 37);
  u_act_xon_longer_than_sync_interval  : entity work.tb_dp_throttle_xon generic map (e_active, e_active, false, 20, 13, 10, 37);
  u_act_no_xoff                        : entity work.tb_dp_throttle_xon generic map (e_active, e_active, false, 20, 13, 10, 0);
  u_act_block_size_is_1                : entity work.tb_dp_throttle_xon generic map (e_active, e_active, false,  1, 10, 10, 37);
  u_act_block_size_is_2                : entity work.tb_dp_throttle_xon generic map (e_active, e_active, false,  2, 10, 10, 37);
  u_act_block_size_is_3                : entity work.tb_dp_throttle_xon generic map (e_active, e_active, false,  3, 10, 10, 37);
  u_rnd_xon_equal_to_sync_interval     : entity work.tb_dp_throttle_xon generic map (e_random, e_random, false, 20, 10, 10, 37);
end tb;
