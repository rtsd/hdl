-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_toggle.vhd
-- Usage:
-- > as 10
-- > run -all
-- Observe the out_toggle during the different stimuli indicated by tb_state.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_toggle is
end tb_common_toggle;

architecture tb of tb_common_toggle is
  constant clk_period    : time := 10 ns;
  constant c_nof_toggles : natural := 5;

  signal tb_end     : std_logic := '0';
  signal rst        : std_logic;
  signal clk        : std_logic := '0';
  signal tb_state   : natural := 0;
  signal in_dat     : std_logic;
  signal in_val     : std_logic;
  signal out_toggle : std_logic;
begin
  clk  <= not clk or tb_end after clk_period / 2;
  rst  <= '1', '0' after 7 * clk_period;

  p_in_stimuli : process
  begin
    tb_state <= 0;
    in_dat <= '0';
    in_val <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    -- 1) Toggle in_at while in_val is active
    tb_state <= 1;
    in_val <= '1';
    in_dat <= '0';
    for I in 0 to c_nof_toggles - 1 loop
      proc_common_wait_some_cycles(clk, 10);
      in_dat <= '1';
      proc_common_wait_some_cycles(clk, 10);
      in_dat <= '0';
    end loop;

    -- 2) Toggle in_at while in_val is inactive should be ingored
    tb_state <= 2;
    in_val <= '0';
    in_dat <= '0';
    for I in 0 to c_nof_toggles - 1 loop
      proc_common_wait_some_cycles(clk, 10);
      in_dat <= '1';
      proc_common_wait_some_cycles(clk, 10);
      in_dat <= '0';
    end loop;

    -- Toggle in_at while in_val is active
    tb_state <= 3;
    in_val <= '1';
    in_dat <= '0';
    for I in 0 to c_nof_toggles - 1 loop
      proc_common_wait_some_cycles(clk, 10);
      in_dat <= '1';
      proc_common_wait_some_cycles(clk, 10);
      in_dat <= '0';
    end loop;

    tb_state <= 9;
    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  u_toggle : entity work.common_toggle
  generic map (
    g_evt_type   => "RISING",
    g_rst_level  => '0'
  )
  port map (
    rst         => rst,
    clk         => clk,
    clken       => '1',
    in_dat      => in_dat,
    in_val      => in_val,
    out_dat     => out_toggle
  );
end tb;
