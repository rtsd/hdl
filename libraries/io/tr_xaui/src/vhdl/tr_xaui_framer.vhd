-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tr_xaui_framer is
  generic (
    g_dat_len : natural := 1000000;  -- Max number of cycles carrying user data
    g_gap_len : natural := 5  -- Gap length, including 2 cycles for the START and TERMINATE words
    );
  port (
    tx_clk     : in std_logic;
    tx_rst     : in std_logic;
    -- ST sink
    snk_out    : out t_dp_siso;
    snk_in     : in  t_dp_sosi;

    xgmii_tx_d : out std_logic_vector(c_xgmii_data_w - 1 downto 0);
    xgmii_tx_c : out std_logic_vector(c_xgmii_nof_lanes - 1 downto 0)

  );
end tr_xaui_framer;

architecture rtl of tr_xaui_framer is
  type t_state_enum is (s_init, s_gap, s_data);

  signal prev_state     : t_state_enum;
  signal state          : t_state_enum;
  signal nxt_state      : t_state_enum;

  signal i_xgmii_tx_d   : std_logic_vector(c_xgmii_data_w - 1 downto 0);
  signal nxt_xgmii_tx_d : std_logic_vector(c_xgmii_data_w - 1 downto 0);

  signal i_xgmii_tx_c   : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0);
  signal nxt_xgmii_tx_c : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0);

  signal gap_siso       : t_dp_siso;
  signal gap_sosi       : t_dp_sosi;

  signal prev_gap_sosi  : t_dp_sosi;
begin
  xgmii_tx_d <= i_xgmii_tx_d;
  xgmii_tx_c <= i_xgmii_tx_c;

  -- We're using a dp_gap instance to make sure gaps occur on a regular basis. This is
  -- required as idle patterns (that are converted to lane alignment and other control
  -- chars by the xaui phy IP) are inserted during these gaps. Dp_gap also extends any
  -- gap on its snk_in to the minimum gap_len by deasserting snk_out.ready.
  u_dp_gap : entity dp_lib.dp_gap
  generic map (
    g_dat_len    => g_dat_len,
    g_gap_len    => g_gap_len,
    g_gap_extend => true
  )
  port map (
    rst       => tx_rst,
    clk       => tx_clk,

    snk_out   => snk_out,
    snk_in    => snk_in,

    src_in    => gap_siso,
    src_out   => gap_sosi
  );

  gap_siso <= c_dp_siso_rdy;  -- no flow control

  -- As the dp_gap takes care of the frame length and the gap length, all our FSM needs
  -- to do is to take care of xgmii data and control words:
  --   * idle->data should be separated by a START (0xFB=Frame Begin) control char (inserted during 1 invalid cycle)
  --   * data: encoded as data
  --   * data->idle should be separated by a TERMINATE (0xFD=Frame Delimiter) control char (inserted during 1 invalid cycle)
  --   * idle: encoded as control
  -- ..so we need only 2 states (gap & data) and a data buffer:
  --
  --      gap_sosi: G  G  D0 D1 D2 D3 G  G G G G D4 D5
  -- prev_gap_sosi: G  G  G  D0 D1 D2 D3 G G G G G  D4  D5
  -- nxt tx data  : I  I  S  D0 D1 D2 D3 T I I I S  D4  D5
  -- State        : G  G  G  D  D  D  D  G G G G G  D   D
  p_clk : process(tx_clk, tx_rst)
  begin
    if tx_rst = '1' then
      state               <= s_init;
      i_xgmii_tx_d        <= c_xgmii_d_idle;
      i_xgmii_tx_c        <= c_xgmii_c_idle;
    elsif rising_edge(tx_clk) then
      state               <= nxt_state;
      prev_state          <= state;
      prev_gap_sosi       <= gap_sosi;
      i_xgmii_tx_d        <= nxt_xgmii_tx_d;
      i_xgmii_tx_c        <= nxt_xgmii_tx_c;
    end if;
  end process;

  p_state : process(state, prev_state, gap_sosi)
  begin
    nxt_state  <= state;

  case state is

    when s_gap =>
      -- Insert idle words during gaps
      nxt_xgmii_tx_d <= c_xgmii_d_idle;
      nxt_xgmii_tx_c <= c_xgmii_c_idle;
      if prev_state = s_data then
        -- Insert the TERM word during transition from data to idle
        nxt_xgmii_tx_d <= c_xgmii_d_term;
        nxt_xgmii_tx_c <= c_xgmii_c_term;
      end if;
      if gap_sosi.valid = '1' then
        -- Insert the START word during transition from idle to data
        nxt_state      <= s_data;
        nxt_xgmii_tx_d <= c_xgmii_d_start;
        nxt_xgmii_tx_c <= c_xgmii_c_start;
      end if;

    when s_data =>  -- Forward the data stored in prev_gap_sosi
      nxt_xgmii_tx_d <= prev_gap_sosi.data(c_xgmii_data_w - 1 downto 0);
      nxt_xgmii_tx_c <= c_xgmii_c_data;
      if gap_sosi.valid = '0' then
        nxt_state      <= s_gap;
      end if;

    when others =>  -- s_init
      nxt_state      <= s_gap;
      nxt_xgmii_tx_d <= c_xgmii_d_idle;
      nxt_xgmii_tx_c <= c_xgmii_c_idle;
    end case;
  end process;
end rtl;
