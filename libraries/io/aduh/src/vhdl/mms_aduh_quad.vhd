-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Capture input from four ADC [A,B,C,D] and provide MM slave read
--          only register for aduh_quad

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.aduh_dd_pkg.all;

entity mms_aduh_quad is
  generic (
    -- General
    g_sim                : boolean := false;
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and dp_clk are the same, else use TRUE to cross the clock domain
    -- ADC Interface
    g_nof_dp_phs_clk     : natural := 1;  -- nof dp_phs_clk that can be used to detect the word phase
    g_ai                 : t_c_aduh_dd_ai := c_aduh_dd_ai
  );
  port (
    -- ADC Interface
    -- . ADU_AB
    ADC_BI_A          : in  std_logic_vector(g_ai.port_w - 1 downto 0) := (others => '0');
    ADC_BI_B          : in  std_logic_vector(g_ai.port_w - 1 downto 0) := (others => '0');
    ADC_BI_A_CLK      : in  std_logic := '0';
    ADC_BI_A_CLK_RST  : out std_logic;

    -- . ADU_CD
    ADC_BI_C          : in  std_logic_vector(g_ai.port_w - 1 downto 0) := (others => '0');
    ADC_BI_D          : in  std_logic_vector(g_ai.port_w - 1 downto 0) := (others => '0');
    ADC_BI_D_CLK      : in  std_logic := '0';
    ADC_BI_D_CLK_RST  : out std_logic;

    -- MM clock domain
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;

    reg_mosi          : in  t_mem_mosi;
    reg_miso          : out t_mem_miso;

    -- Streaming clock domain
    dp_rst            : in  std_logic;
    dp_clk            : in  std_logic;
    dp_phs_clk_vec    : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);

    -- . data
    aduh_sosi_arr     : out t_dp_sosi_arr(0 to g_ai.nof_sp - 1)  -- = [0:3] = ADC_BI ports [A,B,C,D]
  );
end mms_aduh_quad;

architecture str of mms_aduh_quad is
  signal aduh_ab_status         : std_logic_vector(c_word_w - 1 downto 0);
  signal aduh_ab_locked         : std_logic;
  signal aduh_ab_stable         : std_logic;
  signal aduh_ab_stable_ack     : std_logic;
  signal aduh_ab_control        : std_logic_vector(c_word_w - 1 downto 0);

  signal aduh_cd_status         : std_logic_vector(c_word_w - 1 downto 0);
  signal aduh_cd_locked         : std_logic;
  signal aduh_cd_stable         : std_logic;
  signal aduh_cd_stable_ack     : std_logic;
  signal aduh_cd_control        : std_logic_vector(c_word_w - 1 downto 0);

  signal aduh_verify_res        : t_slv_32_arr(0 to g_ai.nof_sp - 1);  -- [8,7:0]
  signal aduh_verify_res_val    : std_logic_vector(0 to g_ai.nof_sp - 1);
  signal aduh_verify_res_ack    : std_logic_vector(0 to g_ai.nof_sp - 1);
begin
  -----------------------------------------------------------------------------
  -- MM control register for the data input modules
  -----------------------------------------------------------------------------

  u_mm_reg : entity work.aduh_quad_reg
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_nof_dp_phs_clk     => g_nof_dp_phs_clk
  )
  port map (
    -- Clocks and reset
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,
    st_rst                   => dp_rst,
    st_clk                   => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in                   => reg_mosi,
    sla_out                  => reg_miso,

    -- MM registers in st_clk domain
    -- . ADU status
    st_aduh_ab_status        => aduh_ab_status,
    st_aduh_ab_locked        => aduh_ab_locked,
    st_aduh_ab_stable        => aduh_ab_stable,
    st_aduh_ab_stable_ack    => aduh_ab_stable_ack,
    st_aduh_ab_control       => aduh_ab_control,

    st_aduh_cd_status        => aduh_cd_status,
    st_aduh_cd_locked        => aduh_cd_locked,
    st_aduh_cd_stable        => aduh_cd_stable,
    st_aduh_cd_stable_ack    => aduh_cd_stable_ack,
    st_aduh_cd_control       => aduh_cd_control,

    -- . ADU pattern verify
    st_aduh_a_verify_res     => aduh_verify_res(0),
    st_aduh_a_verify_res_val => aduh_verify_res_val(0),
    st_aduh_a_verify_res_ack => aduh_verify_res_ack(0),

    st_aduh_b_verify_res     => aduh_verify_res(1),
    st_aduh_b_verify_res_val => aduh_verify_res_val(1),
    st_aduh_b_verify_res_ack => aduh_verify_res_ack(1),

    st_aduh_c_verify_res     => aduh_verify_res(2),
    st_aduh_c_verify_res_val => aduh_verify_res_val(2),
    st_aduh_c_verify_res_ack => aduh_verify_res_ack(2),

    st_aduh_d_verify_res     => aduh_verify_res(3),
    st_aduh_d_verify_res_val => aduh_verify_res_val(3),
    st_aduh_d_verify_res_ack => aduh_verify_res_ack(3)
  );

  u_aduh_quad : entity work.aduh_quad
  generic map (
    -- ADC Interface
    g_sim            => g_sim,
    g_nof_dp_phs_clk => g_nof_dp_phs_clk,
    g_ai             => g_ai
  )
  port map (
    -- ADC Interface
    -- . ADU_AB
    ADC_BI_A               => ADC_BI_A,
    ADC_BI_B               => ADC_BI_B,
    ADC_BI_A_CLK           => ADC_BI_A_CLK,
    ADC_BI_A_CLK_RST       => ADC_BI_A_CLK_RST,

    -- . ADU_CD
    ADC_BI_C               => ADC_BI_C,
    ADC_BI_D               => ADC_BI_D,
    ADC_BI_D_CLK           => ADC_BI_D_CLK,
    ADC_BI_D_CLK_RST       => ADC_BI_D_CLK_RST,

    -- Streaming clock domain
    dp_rst                 => dp_rst,
    dp_clk                 => dp_clk,
    dp_phs_clk_vec         => dp_phs_clk_vec,

    -- . data
    aduh_sosi_arr          => aduh_sosi_arr,

    -- . status
    aduh_ab_status         => aduh_ab_status,
    aduh_ab_locked         => aduh_ab_locked,
    aduh_ab_stable         => aduh_ab_stable,
    aduh_ab_stable_ack     => aduh_ab_stable_ack,
    aduh_ab_control        => aduh_ab_control,

    aduh_cd_status         => aduh_cd_status,
    aduh_cd_locked         => aduh_cd_locked,
    aduh_cd_stable         => aduh_cd_stable,
    aduh_cd_stable_ack     => aduh_cd_stable_ack,
    aduh_cd_control        => aduh_cd_control,

    aduh_verify_res        => aduh_verify_res,
    aduh_verify_res_val    => aduh_verify_res_val,
    aduh_verify_res_ack    => aduh_verify_res_ack
  );
end str;
