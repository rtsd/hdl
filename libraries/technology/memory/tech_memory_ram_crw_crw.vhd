-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer
-- Remark:
-- . The Agilex 7 (agi027_1e1v) doesn't support this IP as used for previous
--   FPGA technology identifiers (device types). Instead, the rw_rw IP should
--   be used. For previous technology identifiers, it is constructed using
--   this crw_crw IP by providing the same clock twice. For more details
--   please refer the README.txt in the ip_agi027_1e1v/ram/ folder.
-- . For Agilex 7 (agi027_1e1v) is also the ip_agi027_1e1v_ram_rw_rw added
--   to this package, but it is only supporting clock_b.

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_memory_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_ram_lib;
library ip_arria10_ram_lib;
library ip_arria10_e3sge3_ram_lib;
library ip_arria10_e1sg_ram_lib;
library ip_arria10_e2sg_ram_lib;
library ip_ultrascale_ram_lib;
library ip_agi027_1e1v_ram_lib;

entity tech_memory_ram_crw_crw is
  generic (
    g_technology : natural := c_tech_select_default;
    g_adr_w      : natural := 5;
    g_dat_w      : natural := 8;
    g_nof_words  : natural := 2**5;
    g_rd_latency : natural := 2;  -- choose 1 or 2
    g_init_file  : string  := "UNUSED"
  );
  port
  (
    address_a : in std_logic_vector(g_adr_w - 1 downto 0);
    address_b : in std_logic_vector(g_adr_w - 1 downto 0);
    clock_a   : in std_logic  := '1';
    clock_b   : in std_logic;
    data_a    : in std_logic_vector(g_dat_w - 1 downto 0);
    data_b    : in std_logic_vector(g_dat_w - 1 downto 0);
    enable_a  : in std_logic  := '1';
    enable_b  : in std_logic  := '1';
    rden_a    : in std_logic  := '1';
    rden_b    : in std_logic  := '1';
    wren_a    : in std_logic  := '0';
    wren_b    : in std_logic  := '0';
    q_a       : out std_logic_vector(g_dat_w - 1 downto 0);
    q_b       : out std_logic_vector(g_dat_w - 1 downto 0)
  );
end tech_memory_ram_crw_crw;

architecture str of tech_memory_ram_crw_crw is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_ram_crw_crw
    generic map (g_adr_w, g_dat_w, g_nof_words, g_rd_latency, g_init_file)
    port map (address_a, address_b, clock_a, clock_b, data_a, data_b, enable_a, enable_b, rden_a, rden_b, wren_a, wren_b, q_a, q_b);
  end generate;

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_ram_crw_crw
    generic map (false, g_adr_w, g_dat_w, g_nof_words, g_rd_latency, g_init_file)
    port map (address_a, address_b, clock_a, clock_b, data_a, data_b, wren_a, wren_b, q_a, q_b);
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_ram_crw_crw
    generic map (false, g_adr_w, g_dat_w, g_nof_words, g_rd_latency, g_init_file)
    port map (address_a, address_b, clock_a, clock_b, data_a, data_b, wren_a, wren_b, q_a, q_b);
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_ram_crw_crw
    generic map (false, g_adr_w, g_dat_w, g_nof_words, g_rd_latency, g_init_file)
    port map (address_a, address_b, clock_a, clock_b, data_a, data_b, wren_a, wren_b, q_a, q_b);
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_ram_crw_crw
    generic map (false, g_adr_w, g_dat_w, g_nof_words, g_rd_latency, g_init_file)
    port map (address_a, address_b, clock_a, clock_b, data_a, data_b, wren_a, wren_b, q_a, q_b);
  end generate;

  gen_ip_ultrascale : if g_technology = c_tech_ultrascale generate
    u0 : ip_ultrascale_ram_crw_crw
    generic map (false, g_adr_w, g_dat_w, g_nof_words, g_rd_latency, g_init_file)
    port map (address_a, address_b, clock_a, clock_b, data_a, data_b, wren_a, wren_b, q_a, q_b);
  end generate;

  gen_ip_agi027_1e1v : if g_technology = c_tech_agi027_1e1v generate
    u0 : ip_agi027_1e1v_ram_rw_rw
    generic map (false, g_adr_w, g_dat_w, g_nof_words, g_rd_latency, g_init_file)
    port map (address_a, address_b, clock_b, data_a, data_b, wren_a, wren_b, q_a, q_b);
  end generate;
end architecture;

