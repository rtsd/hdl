
# (C) 2001-2014 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# Files created by the megawizard will be compiled intop the work library
# and other libraries with this script.    
#
# Be sure you run "unb_mgw ddr3" first. 
#
# Currently it handles: 
#                       -UPHY_4G_800_MASTER
#                       -UPHY_4G_800_SLAVE
#                       -UPHY_4G_1066_MASTER
#                       -UPHY_4G_1066_SLAVE
#                       -Memory model

######################
# UPHY_4G_800_MASTER #
######################

set IP_DIR "$env(UNB)/Firmware/modules/ddr3/src/ip/megawizard/uphy_4g_800_master_sim/" 

# Create compilation libraries
proc ensure_lib { lib } { if ![file isdirectory $lib] { vlib $lib } }

ensure_lib      ./work/
vmap       work ./work/
ensure_lib                                       ./work/         
vmap       uphy_4g_800_master_a0                 ./work/         
ensure_lib                                       ./work/        
vmap       uphy_4g_800_master_ng0                ./work/        
ensure_lib                                       ./work/       
vmap       uphy_4g_800_master_dll0               ./work/       
ensure_lib                                       ./work/       
vmap       uphy_4g_800_master_oct0               ./work/       
ensure_lib                                       ./work/         
vmap       uphy_4g_800_master_c0                 ./work/         
ensure_lib                                       ./work/         
vmap       uphy_4g_800_master_s0                 ./work/         
ensure_lib                                       ./work/         
vmap       uphy_4g_800_master_m0                 ./work/         
ensure_lib                                       ./work/         
vmap       uphy_4g_800_master_p0                 ./work/         
ensure_lib                                       ./work/       
vmap       uphy_4g_800_master_pll0               ./work/       
ensure_lib                                       ./work/
vmap       uphy_4g_800_master_uphy_4g_800_master ./work/

# ----------------------------------------
# Copy ROM/RAM files to simulation directory
file copy -force $IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_sequencer_mem.hex ./
file copy -force $IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_AC_ROM.hex ./
file copy -force $IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_inst_ROM.hex ./

# ----------------------------------------
# Compile the design files in correct order
vlog                                         "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_mm_st_converter.v"                                        -work uphy_4g_800_master_a0         
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_addr_cmd.v"                                               -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_addr_cmd_wrap.v"                                          -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ddr2_odt_gen.v"                                           -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ddr3_odt_gen.v"                                           -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_lpddr2_addr_cmd.v"                                        -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_odt_gen.v"                                                -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_rdwr_data_tmg.v"                                          -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_arbiter.v"                                                -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_burst_gen.v"                                              -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_cmd_gen.v"                                                -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_csr.v"                                                    -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_buffer.v"                                                 -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_buffer_manager.v"                                         -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_burst_tracking.v"                                         -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_dataid_manager.v"                                         -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_fifo.v"                                                   -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_list.v"                                                   -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_rdata_path.v"                                             -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_wdata_path.v"                                             -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ecc_decoder.v"                                            -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ecc_decoder_32_syn.v"                                     -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ecc_decoder_64_syn.v"                                     -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ecc_encoder.v"                                            -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ecc_encoder_32_syn.v"                                     -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ecc_encoder_64_syn.v"                                     -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_ecc_encoder_decoder_wrapper.v"                            -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_axi_st_converter.v"                                       -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_input_if.v"                                               -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_rank_timer.v"                                             -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_sideband.v"                                               -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_tbp.v"                                                    -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_timing_param.v"                                           -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_controller.v"                                             -work uphy_4g_800_master_ng0        
vlog     +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_ddrx_controller_st_top.v"                                      -work uphy_4g_800_master_ng0        
vlog -sv +incdir+$IP_DIR/uphy_4g_800_master/ "$IP_DIR/uphy_4g_800_master/alt_mem_if_nextgen_ddr3_controller_core.sv"                            -work uphy_4g_800_master_ng0        
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_mem_if_dll_stratixiv.sv"                                        -work uphy_4g_800_master_dll0       
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_mem_if_oct_stratixiv.sv"                                        -work uphy_4g_800_master_oct0       
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_c0.v"                                               -work uphy_4g_800_master_c0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0.v"                                               -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_inst_ROM_reg.v"                                             -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_id_router.sv"                                    -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_data_decoder.v"                                             -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_ram_csr.v"                                                  -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_datamux.v"                                                  -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_inst_ROM_no_ifdef_params.v"                                 -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_ac_ROM_reg.v"                                               -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_cmd_xbar_demux.sv"                               -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_addr_router_001.sv"                              -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_di_buffer.v"                                                -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_data_broadcast.v"                                           -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_bitcheck.v"                                                 -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_ac_ROM_no_ifdef_params.v"                                   -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_cmd_xbar_demux_001.sv"                           -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/sequencer_phy_mgr.sv"                                                  -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_irq_mapper.sv"                                   -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/altera_reset_controller.v"                                             -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/sequencer_scc_sv_wrapper.sv"                                           -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_rsp_xbar_demux_003.sv"                           -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_addr_router.sv"                                  -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/altera_avalon_sc_fifo.v"                                               -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_ddr3.v"                                                     -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/sequencer_data_mgr.sv"                                                 -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_lfsr72.v"                                                   -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_merlin_master_agent.sv"                                         -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_ram.v"                                                      -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst_test_bench.v" -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/sequencer_scc_acv_wrapper.sv"                                          -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/sequencer_scc_siii_phase_decode.v"                                     -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/sequencer_scc_sv_phase_decode.v"                                       -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_mem_if_sequencer_mem_no_ifdef_params.sv"                        -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_id_router_003.sv"                                -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_lfsr36.v"                                                   -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/rw_manager_generic.sv"                                                 -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/sequencer_scc_siii_wrapper.sv"                                         -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_cmd_xbar_mux_003.sv"                             -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/sequencer_scc_mgr.sv"                                                  -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_lfsr12.v"                                                   -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_merlin_master_translator.sv"                                    -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/sequencer_scc_reg_file.v"                                              -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/sequencer_reg_file.sv"                                                 -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_pattern_fifo.v"                                             -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_read_datapath.v"                                            -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/rw_manager_core.sv"                                                    -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_merlin_arbitrator.sv"                                           -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/altera_reset_synchronizer.v"                                           -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_jumplogic.v"                                                -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_merlin_slave_translator.sv"                                     -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_dm_decoder.v"                                               -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/sequencer_scc_acv_phase_decode.v"                                      -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_write_decoder.v"                                            -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/rw_manager_di_buffer_wrap.v"                                           -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_merlin_burst_uncompressor.sv"                                   -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_s0_rsp_xbar_mux.sv"                                 -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst.v"            -work uphy_4g_800_master_s0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altera_merlin_slave_agent.sv"                                          -work uphy_4g_800_master_s0         
vlog                                         "$IP_DIR/uphy_4g_800_master/afi_mux_ddr3_ddrx.v"                                                   -work uphy_4g_800_master_m0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_clock_pair_generator.v"                          -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_read_valid_selector.v"                           -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_addr_cmd_datapath.v"                             -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_reset.v"                                         -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_acv_ldc.v"                                       -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_addr_cmd_pads.v"                                 -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_addr_cmd_ldc_pads.v"                             -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_addr_cmd_ldc_pad.v"                              -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/addr_cmd_non_ldc_pad.v"                                                -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_memphy.v"                                        -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_reset_sync.v"                                    -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_new_io_pads.v"                                   -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_fr_cycle_shifter.v"                              -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_read_datapath.v"                                 -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_write_datapath.v"                                -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_hr_to_fr.v"                                      -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_simple_ddio_out.v"                               -work uphy_4g_800_master_p0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_sequencer_mux_bridge.sv"                         -work uphy_4g_800_master_p0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_phy_csr.sv"                                      -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_iss_probe.v"                                     -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_flop_mem.v"                                      -work uphy_4g_800_master_p0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0.sv"                                              -work uphy_4g_800_master_p0         
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_p0_altdqdqs.v"                                      -work uphy_4g_800_master_p0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altdq_dqs2_ddio_3reg_stratixiv.sv"                                     -work uphy_4g_800_master_p0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altdq_dqs2_abstract.sv"                                                -work uphy_4g_800_master_p0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/altdq_dqs2_cal_delays.sv"                                              -work uphy_4g_800_master_p0         
vlog -sv                                     "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_pll0.sv"                                            -work uphy_4g_800_master_pll0       
vlog                                         "$IP_DIR/uphy_4g_800_master/uphy_4g_800_master_0002.v"                                             -work uphy_4g_800_master_uphy_4g_800_master
vlog                                         "$IP_DIR/uphy_4g_800_master.v"                                                                                                  

########################################################################################
# Get the memory model for the uphy_4g_* from the uphy_4g_master design example:       #
########################################################################################

set IP_DIR "$env(UNB)/Firmware/modules/ddr3/src/ip/megawizard/uphy_4g_800_master_example_design/simulation/vhdl/submodules/"

vlog -sv         "$IP_DIR/alt_mem_if_ddr3_mem_model_top_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en.sv"  - work work 
vlog -sv         "$IP_DIR/alt_mem_if_common_ddr_mem_model_ddr3_mem_if_dm_pins_en_mem_if_dqsn_en.sv"  - work work 

######################
# UPHY_4G_800_SLAVE  #
######################

set IP_DIR "$env(UNB)/Firmware/modules/ddr3/src/ip/megawizard/uphy_4g_800_slave_sim/" 

ensure_lib                                       ./work/         
vmap       uphy_4g_800_slave_a0                  ./work/         
ensure_lib                                       ./work/        
vmap       uphy_4g_800_slave_ng0                 ./work/        
ensure_lib                                       ./work/       
vmap       uphy_4g_800_slave_dll0                ./work/       
ensure_lib                                       ./work/       
vmap       uphy_4g_800_slave_oct0                ./work/       
ensure_lib                                       ./work/         
vmap       uphy_4g_800_slave_c0                  ./work/         
ensure_lib                                       ./work/         
vmap       uphy_4g_800_slave_s0                  ./work/         
ensure_lib                                       ./work/         
vmap       uphy_4g_800_slave_m0                  ./work/         
ensure_lib                                       ./work/         
vmap       uphy_4g_800_slave_p0                  ./work/         
ensure_lib                                       ./work/       
vmap       uphy_4g_800_slave_pll0                ./work/       
ensure_lib                                       ./work/
vmap       uphy_4g_800_slave_uphy_4g_800_slave   ./work/

# ----------------------------------------
# Copy ROM/RAM files to simulation directory
file copy -force $IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_sequencer_mem.hex ./
file copy -force $IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_AC_ROM.hex ./
file copy -force $IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_inst_ROM.hex ./

# ----------------------------------------
# Compile the design files in correct order
vlog                                        "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_mm_st_converter.v"                                        -work uphy_4g_800_slave_a0               
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_addr_cmd.v"                                               -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_addr_cmd_wrap.v"                                          -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ddr2_odt_gen.v"                                           -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ddr3_odt_gen.v"                                           -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_lpddr2_addr_cmd.v"                                        -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_odt_gen.v"                                                -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_rdwr_data_tmg.v"                                          -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_arbiter.v"                                                -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_burst_gen.v"                                              -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_cmd_gen.v"                                                -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_csr.v"                                                    -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_buffer.v"                                                 -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_buffer_manager.v"                                         -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_burst_tracking.v"                                         -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_dataid_manager.v"                                         -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_fifo.v"                                                   -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_list.v"                                                   -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_rdata_path.v"                                             -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_wdata_path.v"                                             -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ecc_decoder.v"                                            -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ecc_decoder_32_syn.v"                                     -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ecc_decoder_64_syn.v"                                     -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ecc_encoder.v"                                            -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ecc_encoder_32_syn.v"                                     -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ecc_encoder_64_syn.v"                                     -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_ecc_encoder_decoder_wrapper.v"                            -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_axi_st_converter.v"                                       -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_input_if.v"                                               -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_rank_timer.v"                                             -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_sideband.v"                                               -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_tbp.v"                                                    -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_timing_param.v"                                           -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_controller.v"                                             -work uphy_4g_800_slave_ng0              
vlog     +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_ddrx_controller_st_top.v"                                      -work uphy_4g_800_slave_ng0              
vlog -sv +incdir+$IP_DIR/uphy_4g_800_slave/ "$IP_DIR/uphy_4g_800_slave/alt_mem_if_nextgen_ddr3_controller_core.sv"                            -work uphy_4g_800_slave_ng0              
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altera_mem_if_dll_stratixiv.sv"                                        -work uphy_4g_800_slave_dll0             
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_c0.v"                                                -work uphy_4g_800_slave_c0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0.v"                                                -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_inst_ROM_reg.v"                                             -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_data_decoder.v"                                             -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_ram_csr.v"                                                  -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_datamux.v"                                                  -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_rsp_xbar_demux_003.sv"                            -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_inst_ROM_no_ifdef_params.v"                                 -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_ac_ROM_reg.v"                                               -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_addr_router_001.sv"                               -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_di_buffer.v"                                                -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_data_broadcast.v"                                           -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_bitcheck.v"                                                 -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_ac_ROM_no_ifdef_params.v"                                   -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/sequencer_phy_mgr.sv"                                                  -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_addr_router.sv"                                   -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/altera_reset_controller.v"                                             -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/sequencer_scc_sv_wrapper.sv"                                           -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/altera_avalon_sc_fifo.v"                                               -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_ddr3.v"                                                     -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/sequencer_data_mgr.sv"                                                 -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_lfsr72.v"                                                   -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altera_merlin_master_agent.sv"                                         -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_cmd_xbar_demux.sv"                                -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_ram.v"                                                      -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst_test_bench.v" -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_cmd_xbar_mux_003.sv"                              -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/sequencer_scc_acv_wrapper.sv"                                          -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_cmd_xbar_demux_001.sv"                            -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/sequencer_scc_siii_phase_decode.v"                                     -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/sequencer_scc_sv_phase_decode.v"                                       -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altera_mem_if_sequencer_mem_no_ifdef_params.sv"                        -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_lfsr36.v"                                                   -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/rw_manager_generic.sv"                                                 -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/sequencer_scc_siii_wrapper.sv"                                         -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_irq_mapper.sv"                                    -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/sequencer_scc_mgr.sv"                                                  -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_lfsr12.v"                                                   -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altera_merlin_master_translator.sv"                                    -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/sequencer_scc_reg_file.v"                                              -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/sequencer_reg_file.sv"                                                 -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_pattern_fifo.v"                                             -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_read_datapath.v"                                            -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/rw_manager_core.sv"                                                    -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altera_merlin_arbitrator.sv"                                           -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/altera_reset_synchronizer.v"                                           -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_id_router_003.sv"                                 -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_jumplogic.v"                                                -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altera_merlin_slave_translator.sv"                                     -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_id_router.sv"                                     -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_dm_decoder.v"                                               -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/sequencer_scc_acv_phase_decode.v"                                      -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_write_decoder.v"                                            -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/rw_manager_di_buffer_wrap.v"                                           -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altera_merlin_burst_uncompressor.sv"                                   -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_s0_rsp_xbar_mux.sv"                                  -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst.v"            -work uphy_4g_800_slave_s0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altera_merlin_slave_agent.sv"                                          -work uphy_4g_800_slave_s0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/afi_mux_ddr3_ddrx.v"                                                   -work uphy_4g_800_slave_m0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_clock_pair_generator.v"                           -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_read_valid_selector.v"                            -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_addr_cmd_datapath.v"                              -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_reset.v"                                          -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_acv_ldc.v"                                        -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_addr_cmd_pads.v"                                  -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_addr_cmd_ldc_pads.v"                              -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_addr_cmd_ldc_pad.v"                               -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/addr_cmd_non_ldc_pad.v"                                                -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_memphy.v"                                         -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_reset_sync.v"                                     -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_new_io_pads.v"                                    -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_fr_cycle_shifter.v"                               -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_read_datapath.v"                                  -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_write_datapath.v"                                 -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_hr_to_fr.v"                                       -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_simple_ddio_out.v"                                -work uphy_4g_800_slave_p0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_sequencer_mux_bridge.sv"                          -work uphy_4g_800_slave_p0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_phy_csr.sv"                                       -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_iss_probe.v"                                      -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_flop_mem.v"                                       -work uphy_4g_800_slave_p0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0.sv"                                               -work uphy_4g_800_slave_p0               
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_p0_altdqdqs.v"                                       -work uphy_4g_800_slave_p0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altdq_dqs2_ddio_3reg_stratixiv.sv"                                     -work uphy_4g_800_slave_p0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altdq_dqs2_abstract.sv"                                                -work uphy_4g_800_slave_p0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/altdq_dqs2_cal_delays.sv"                                              -work uphy_4g_800_slave_p0               
vlog -sv                                    "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_pll0.sv"                                             -work uphy_4g_800_slave_pll0             
vlog                                        "$IP_DIR/uphy_4g_800_slave/uphy_4g_800_slave_0002.v"                                              -work uphy_4g_800_slave_uphy_4g_800_slave
vlog                                        "$IP_DIR/uphy_4g_800_slave.v"                                                                                                              

########################
# UPHY_4G_1066_MASTER  #
########################

set IP_DIR "$env(UNB)/Firmware/modules/ddr3/src/ip/megawizard/uphy_4g_1066_master_sim/" 

# Copy ROM/RAM files to simulation directory                                                                                 
file copy -force $IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_sequencer_mem.hex ./
file copy -force $IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_AC_ROM.hex ./       
file copy -force $IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_inst_ROM.hex ./     

ensure_lib                                         ./work/                 
vmap       uphy_4g_1066_master_a0                  ./work/                 
ensure_lib                                         ./work/                
vmap       uphy_4g_1066_master_ng0                 ./work/                
ensure_lib                                         ./work/               
vmap       uphy_4g_1066_master_dll0                ./work/               
ensure_lib                                         ./work/               
vmap       uphy_4g_1066_master_oct0                ./work/               
ensure_lib                                         ./work/                 
vmap       uphy_4g_1066_master_c0                  ./work/                 
ensure_lib                                         ./work/                 
vmap       uphy_4g_1066_master_s0                  ./work/                 
ensure_lib                                         ./work/                 
vmap       uphy_4g_1066_master_m0                  ./work/                 
ensure_lib                                         ./work/                 
vmap       uphy_4g_1066_master_p0                  ./work/                 
ensure_lib                                         ./work/               
vmap       uphy_4g_1066_master_pll0                ./work/               
ensure_lib                                         ./work/
vmap       uphy_4g_1066_master_uphy_4g_1066_master ./work/

vlog                                          "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_mm_st_converter.v"                                        -work uphy_4g_1066_master_a0                 
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_addr_cmd.v"                                               -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_addr_cmd_wrap.v"                                          -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ddr2_odt_gen.v"                                           -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ddr3_odt_gen.v"                                           -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_lpddr2_addr_cmd.v"                                        -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_odt_gen.v"                                                -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_rdwr_data_tmg.v"                                          -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_arbiter.v"                                                -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_burst_gen.v"                                              -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_cmd_gen.v"                                                -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_csr.v"                                                    -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_buffer.v"                                                 -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_buffer_manager.v"                                         -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_burst_tracking.v"                                         -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_dataid_manager.v"                                         -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_fifo.v"                                                   -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_list.v"                                                   -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_rdata_path.v"                                             -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_wdata_path.v"                                             -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ecc_decoder.v"                                            -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ecc_decoder_32_syn.v"                                     -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ecc_decoder_64_syn.v"                                     -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ecc_encoder.v"                                            -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ecc_encoder_32_syn.v"                                     -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ecc_encoder_64_syn.v"                                     -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_ecc_encoder_decoder_wrapper.v"                            -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_axi_st_converter.v"                                       -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_input_if.v"                                               -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_rank_timer.v"                                             -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_sideband.v"                                               -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_tbp.v"                                                    -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_timing_param.v"                                           -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_controller.v"                                             -work uphy_4g_1066_master_ng0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_ddrx_controller_st_top.v"                                      -work uphy_4g_1066_master_ng0                
vlog -sv +incdir+$IP_DIR/uphy_4g_1066_master/ "$IP_DIR/uphy_4g_1066_master/alt_mem_if_nextgen_ddr3_controller_core.sv"                            -work uphy_4g_1066_master_ng0                
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_mem_if_dll_stratixiv.sv"                                        -work uphy_4g_1066_master_dll0               
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_mem_if_oct_stratixiv.sv"                                        -work uphy_4g_1066_master_oct0               
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_c0.v"                                              -work uphy_4g_1066_master_c0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0.v"                                              -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_rsp_xbar_mux.sv"                                -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_inst_ROM_reg.v"                                             -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_data_decoder.v"                                             -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_ram_csr.v"                                                  -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_datamux.v"                                                  -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_inst_ROM_no_ifdef_params.v"                                 -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_ac_ROM_reg.v"                                               -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_di_buffer.v"                                                -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_data_broadcast.v"                                           -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_bitcheck.v"                                                 -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_ac_ROM_no_ifdef_params.v"                                   -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/sequencer_phy_mgr.sv"                                                  -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/altera_reset_controller.v"                                             -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/sequencer_scc_sv_wrapper.sv"                                           -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/altera_avalon_sc_fifo.v"                                               -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_ddr3.v"                                                     -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/sequencer_data_mgr.sv"                                                 -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_lfsr72.v"                                                   -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_merlin_master_agent.sv"                                         -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_cmd_xbar_demux_001.sv"                          -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_addr_router.sv"                                 -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_ram.v"                                                      -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_cmd_xbar_demux.sv"                              -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst_test_bench.v" -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/sequencer_scc_acv_wrapper.sv"                                          -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_irq_mapper.sv"                                  -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/sequencer_scc_siii_phase_decode.v"                                     -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/sequencer_scc_sv_phase_decode.v"                                       -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_mem_if_sequencer_mem_no_ifdef_params.sv"                        -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_lfsr36.v"                                                   -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_id_router.sv"                                   -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/rw_manager_generic.sv"                                                 -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/sequencer_scc_siii_wrapper.sv"                                         -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/sequencer_scc_mgr.sv"                                                  -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_lfsr12.v"                                                   -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_merlin_master_translator.sv"                                    -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/sequencer_scc_reg_file.v"                                              -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/sequencer_reg_file.sv"                                                 -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_pattern_fifo.v"                                             -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_read_datapath.v"                                            -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/rw_manager_core.sv"                                                    -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_merlin_arbitrator.sv"                                           -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/altera_reset_synchronizer.v"                                           -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_cmd_xbar_mux_003.sv"                            -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_id_router_003.sv"                               -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_jumplogic.v"                                                -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_merlin_slave_translator.sv"                                     -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_rsp_xbar_demux_003.sv"                          -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_dm_decoder.v"                                               -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/sequencer_scc_acv_phase_decode.v"                                      -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_write_decoder.v"                                            -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/rw_manager_di_buffer_wrap.v"                                           -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_merlin_burst_uncompressor.sv"                                   -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_s0_addr_router_001.sv"                             -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst.v"            -work uphy_4g_1066_master_s0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altera_merlin_slave_agent.sv"                                          -work uphy_4g_1066_master_s0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/afi_mux_ddr3_ddrx.v"                                                   -work uphy_4g_1066_master_m0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_clock_pair_generator.v"                         -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_read_valid_selector.v"                          -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_addr_cmd_datapath.v"                            -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_reset.v"                                        -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_acv_ldc.v"                                      -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_addr_cmd_pads.v"                                -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_addr_cmd_ldc_pads.v"                            -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_addr_cmd_ldc_pad.v"                             -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/addr_cmd_non_ldc_pad.v"                                                -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_memphy.v"                                       -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_reset_sync.v"                                   -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_new_io_pads.v"                                  -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_fr_cycle_shifter.v"                             -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_read_datapath.v"                                -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_write_datapath.v"                               -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_hr_to_fr.v"                                     -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_simple_ddio_out.v"                              -work uphy_4g_1066_master_p0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_sequencer_mux_bridge.sv"                        -work uphy_4g_1066_master_p0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_phy_csr.sv"                                     -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_iss_probe.v"                                    -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_flop_mem.v"                                     -work uphy_4g_1066_master_p0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0.sv"                                             -work uphy_4g_1066_master_p0                 
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_p0_altdqdqs.v"                                     -work uphy_4g_1066_master_p0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altdq_dqs2_ddio_3reg_stratixiv.sv"                                     -work uphy_4g_1066_master_p0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altdq_dqs2_abstract.sv"                                                -work uphy_4g_1066_master_p0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/altdq_dqs2_cal_delays.sv"                                              -work uphy_4g_1066_master_p0                 
vlog -sv                                      "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_pll0.sv"                                           -work uphy_4g_1066_master_pll0               
vlog                                          "$IP_DIR/uphy_4g_1066_master/uphy_4g_1066_master_0002.v"                                            -work uphy_4g_1066_master_uphy_4g_1066_master
vlog                                          "$IP_DIR/uphy_4g_1066_master.v"                                                                                                                  

########################
# UPHY_4G_1066_SLAVE   #
########################

set IP_DIR "$env(UNB)/Firmware/modules/ddr3/src/ip/megawizard/uphy_4g_1066_slave_sim/" 

# ----------------------------------------
# Copy ROM/RAM files to simulation directory
file copy -force $IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_sequencer_mem.hex ./
file copy -force $IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_AC_ROM.hex ./
file copy -force $IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_inst_ROM.hex ./

ensure_lib                                       ./work/                
vmap       uphy_4g_1066_slave_a0                 ./work/                
ensure_lib                                       ./work/               
vmap       uphy_4g_1066_slave_ng0                ./work/               
ensure_lib                                       ./work/              
vmap       uphy_4g_1066_slave_dll0               ./work/              
ensure_lib                                       ./work/                
vmap       uphy_4g_1066_slave_c0                 ./work/                
ensure_lib                                       ./work/                
vmap       uphy_4g_1066_slave_s0                 ./work/                
ensure_lib                                       ./work/                
vmap       uphy_4g_1066_slave_m0                 ./work/                
ensure_lib                                       ./work/                
vmap       uphy_4g_1066_slave_p0                 ./work/                
ensure_lib                                       ./work/              
vmap       uphy_4g_1066_slave_pll0               ./work/              
ensure_lib                                       ./work/
vmap       uphy_4g_1066_slave_uphy_4g_1066_slave ./work/

# ----------------------------------------
# Compile the design files in correct order
vlog                                         "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_mm_st_converter.v"                                        -work uphy_4g_1066_slave_a0                
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_addr_cmd.v"                                               -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_addr_cmd_wrap.v"                                          -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ddr2_odt_gen.v"                                           -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ddr3_odt_gen.v"                                           -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_lpddr2_addr_cmd.v"                                        -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_odt_gen.v"                                                -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_rdwr_data_tmg.v"                                          -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_arbiter.v"                                                -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_burst_gen.v"                                              -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_cmd_gen.v"                                                -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_csr.v"                                                    -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_buffer.v"                                                 -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_buffer_manager.v"                                         -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_burst_tracking.v"                                         -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_dataid_manager.v"                                         -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_fifo.v"                                                   -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_list.v"                                                   -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_rdata_path.v"                                             -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_wdata_path.v"                                             -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ecc_decoder.v"                                            -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ecc_decoder_32_syn.v"                                     -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ecc_decoder_64_syn.v"                                     -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ecc_encoder.v"                                            -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ecc_encoder_32_syn.v"                                     -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ecc_encoder_64_syn.v"                                     -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_ecc_encoder_decoder_wrapper.v"                            -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_axi_st_converter.v"                                       -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_input_if.v"                                               -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_rank_timer.v"                                             -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_sideband.v"                                               -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_tbp.v"                                                    -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_timing_param.v"                                           -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_controller.v"                                             -work uphy_4g_1066_slave_ng0               
vlog     +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_ddrx_controller_st_top.v"                                      -work uphy_4g_1066_slave_ng0               
vlog -sv +incdir+$IP_DIR/uphy_4g_1066_slave/ "$IP_DIR/uphy_4g_1066_slave/alt_mem_if_nextgen_ddr3_controller_core.sv"                            -work uphy_4g_1066_slave_ng0               
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altera_mem_if_dll_stratixiv.sv"                                        -work uphy_4g_1066_slave_dll0              
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_c0.v"                                               -work uphy_4g_1066_slave_c0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0.v"                                               -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_inst_ROM_reg.v"                                             -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_data_decoder.v"                                             -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_ram_csr.v"                                                  -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_datamux.v"                                                  -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_cmd_xbar_demux.sv"                               -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_inst_ROM_no_ifdef_params.v"                                 -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_ac_ROM_reg.v"                                               -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_di_buffer.v"                                                -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_data_broadcast.v"                                           -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_bitcheck.v"                                                 -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_ac_ROM_no_ifdef_params.v"                                   -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/sequencer_phy_mgr.sv"                                                  -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/altera_reset_controller.v"                                             -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/sequencer_scc_sv_wrapper.sv"                                           -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/altera_avalon_sc_fifo.v"                                               -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_ddr3.v"                                                     -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/sequencer_data_mgr.sv"                                                 -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_lfsr72.v"                                                   -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altera_merlin_master_agent.sv"                                         -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_addr_router_001.sv"                              -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_ram.v"                                                      -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_rsp_xbar_demux_003.sv"                           -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_id_router.sv"                                    -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst_test_bench.v" -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_cmd_xbar_mux_003.sv"                             -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/sequencer_scc_acv_wrapper.sv"                                          -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_addr_router.sv"                                  -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/sequencer_scc_siii_phase_decode.v"                                     -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/sequencer_scc_sv_phase_decode.v"                                       -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altera_mem_if_sequencer_mem_no_ifdef_params.sv"                        -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_lfsr36.v"                                                   -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/rw_manager_generic.sv"                                                 -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_cmd_xbar_demux_001.sv"                           -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/sequencer_scc_siii_wrapper.sv"                                         -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/sequencer_scc_mgr.sv"                                                  -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_lfsr12.v"                                                   -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altera_merlin_master_translator.sv"                                    -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/sequencer_scc_reg_file.v"                                              -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/sequencer_reg_file.sv"                                                 -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_pattern_fifo.v"                                             -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_read_datapath.v"                                            -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/rw_manager_core.sv"                                                    -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altera_merlin_arbitrator.sv"                                           -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_irq_mapper.sv"                                   -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/altera_reset_synchronizer.v"                                           -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_id_router_003.sv"                                -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_jumplogic.v"                                                -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altera_merlin_slave_translator.sv"                                     -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_dm_decoder.v"                                               -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_s0_rsp_xbar_mux.sv"                                 -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/sequencer_scc_acv_phase_decode.v"                                      -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_write_decoder.v"                                            -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/rw_manager_di_buffer_wrap.v"                                           -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altera_merlin_burst_uncompressor.sv"                                   -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/altera_mem_if_sequencer_cpu_no_ifdef_params_sim_cpu_inst.v"            -work uphy_4g_1066_slave_s0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altera_merlin_slave_agent.sv"                                          -work uphy_4g_1066_slave_s0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/afi_mux_ddr3_ddrx.v"                                                   -work uphy_4g_1066_slave_m0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_clock_pair_generator.v"                          -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_read_valid_selector.v"                           -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_addr_cmd_datapath.v"                             -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_reset.v"                                         -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_acv_ldc.v"                                       -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_addr_cmd_pads.v"                                 -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_addr_cmd_ldc_pads.v"                             -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_addr_cmd_ldc_pad.v"                              -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/addr_cmd_non_ldc_pad.v"                                                -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_memphy.v"                                        -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_reset_sync.v"                                    -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_new_io_pads.v"                                   -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_fr_cycle_shifter.v"                              -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_read_datapath.v"                                 -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_write_datapath.v"                                -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_hr_to_fr.v"                                      -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_simple_ddio_out.v"                               -work uphy_4g_1066_slave_p0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_sequencer_mux_bridge.sv"                         -work uphy_4g_1066_slave_p0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_phy_csr.sv"                                      -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_iss_probe.v"                                     -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_flop_mem.v"                                      -work uphy_4g_1066_slave_p0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0.sv"                                              -work uphy_4g_1066_slave_p0                
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_p0_altdqdqs.v"                                      -work uphy_4g_1066_slave_p0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altdq_dqs2_ddio_3reg_stratixiv.sv"                                     -work uphy_4g_1066_slave_p0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altdq_dqs2_abstract.sv"                                                -work uphy_4g_1066_slave_p0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/altdq_dqs2_cal_delays.sv"                                              -work uphy_4g_1066_slave_p0                
vlog -sv                                     "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_pll0.sv"                                            -work uphy_4g_1066_slave_pll0              
vlog                                         "$IP_DIR/uphy_4g_1066_slave/uphy_4g_1066_slave_0002.v"                                             -work uphy_4g_1066_slave_uphy_4g_1066_slave
vlog                                         "$IP_DIR/uphy_4g_1066_slave.v"                                                                                                                

