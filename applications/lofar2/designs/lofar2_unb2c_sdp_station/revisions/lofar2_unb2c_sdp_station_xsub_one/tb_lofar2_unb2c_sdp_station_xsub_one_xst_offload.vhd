-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Self-checking testbench for simulating lofar2_unb2c_sdp_station_xsub_one capturing BST UDP offload packets.
--
-- Description:
--   MM control actions:
--
--   1) Enable BSN source and enable BST offload
--
--   2) Verify ethernet statistics using eth_statistics, it checks the number of
--      received packets and the total number of valid data. The content of the packets is not verified.
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   And add more missing sosi_arr signals or constants via sim tab, entity instance in hierarchy and
--   then select from objects window, e.g. for:
--   > add wave -position insertpoint  \
--     sim:/tb_lofar2_unb2c_sdp_station_xsub_one_xst_offload/u_lofar_unb2c_sdp_station_xsub_one/u_sdp_station/ait_sosi_arr \
--     sim:/tb_lofar2_unb2c_sdp_station_xsub_one_xst_offload/u_lofar_unb2c_sdp_station_xsub_one/u_sdp_station/pfb_sosi_arr \
--     sim:/tb_lofar2_unb2c_sdp_station_xsub_one_xst_offload/u_lofar_unb2c_sdp_station_xsub_one/u_sdp_station/fsub_sosi_arr \
--     sim:/tb_lofar2_unb2c_sdp_station_xsub_one_xst_offload/u_lofar_unb2c_sdp_station_xsub_one/u_sdp_station/bs_sosi
--   > add wave -position insertpoint  \
--     sim:/tb_lofar2_unb2c_sdp_station_xsub_one_xst_offload/c_exp_subband_xst
--   > run -a
--   Takes about 60 m
-- View e.g.:
--   * rx_sdp_stat_re/im in radix decimal and in format literal or analogue
--   * rx_sdp_stat_header.app
--   * new_interval in node_sdp_correlator.vhd
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2c_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, lofar2_unb2c_sdp_station_lib, eth_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity tb_lofar2_unb2c_sdp_station_xsub_one_xst_offload is
  generic (
    -- Use g_try_xst_restart = TRUE to check hdr_input.integration_interval in sdp_statistics_offload.vhd,
    -- default use FALSE for shorter simulation
    g_try_xst_restart : boolean := false
  );
end tb_lofar2_unb2c_sdp_station_xsub_one_xst_offload;

architecture tb of tb_lofar2_unb2c_sdp_station_xsub_one_xst_offload is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := sel_a_b(g_try_xst_restart, 0, 2);
  constant c_node_nr         : natural := sel_a_b(g_try_xst_restart, 0, 1);  -- use node > 0 to check nof_cycles_dly in sdp_statistics_offload.vhd
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_unb_nr * 4 + c_node_nr, 8);  -- c_unb2c_board_nof_node = 4, c_unb2c_board_aux.id_w = 8
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2c_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C

  constant c_nof_block_per_sync  : natural := 16;  -- long enough to stream out udp data
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_pps_period          : natural := c_nof_clk_per_sync;
  constant c_wpfb_sim            : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands, c_nof_block_per_sync);
  constant c_ctrl_interval_size  : natural := c_nof_clk_per_sync;
  constant c_nof_crosslets       : natural := 3;  -- not too large, so that offload still fits in c_nof_block_per_sync
  constant c_subband_select_arr  : t_natural_arr(0 to c_sdp_N_crosslets_max - 1) := (10, 11, 12, 13, 14, 15, 16);
  constant c_subband_step        : natural := 3;  -- e.g. 0 or c_nof_crosslets
  constant c_nof_sync            : natural := 3;

  constant c_max_ratio           : real := 0.001;  -- ratio that actual value may differ from expected value

  -- WG
  constant c_bsn_start_wg         : natural := 2;
  constant c_sp_ampl              : real := 0.25;  -- WG normalized amplitude, 1.0 = FS (full scale), use <= 0.25 to avoid XST overflow
  constant c_wg_ampl              : natural := natural(c_sp_ampl * real(c_sdp_FS_adc));  -- in number of lsb
  constant c_wg_subband           : natural := 12;

  -- WPFB
  constant c_exp_subband_ampl     : real := real(c_wg_ampl) * c_sdp_wpfb_subband_sp_ampl_ratio;  -- = c_wg_ampl * 0.994817 * 8
  constant c_exp_subband_power    : real := c_exp_subband_ampl**2.0;  -- complex signal ampl, so power is A**2 (not A**2 / 2 as for real)
  constant c_exp_subband_sst      : real := c_exp_subband_power * real(c_nof_block_per_sync);
  constant c_exp_subband_xst      : real := c_exp_subband_sst;  -- all signal inputs use same WG, and auto correlation XST = SST

  -- MM
  constant c_mm_file_reg_bsn_source_v2           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_bsn_scheduler_wg        : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg                 : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_reg_crosslets_info          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_CROSSLETS_INFO";
  constant c_mm_file_reg_nof_crosslets           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_NOF_CROSSLETS";
  constant c_mm_file_reg_bsn_sync_scheduler_xsub : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SYNC_SCHEDULER_XSUB";
  constant c_mm_file_reg_stat_enable_xst         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_STAT_ENABLE_XST";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0) := (others => '0');
  signal eth_done            : std_logic := '0';

  -- . 1GbE output
  constant c_eth_check_nof_packets        : natural := c_nof_sync * c_nof_crosslets;
  constant c_eth_header_size              : natural := 19;  -- words
  constant c_eth_crc_size                 : natural := 1;  -- word
  constant c_eth_packet_size              : natural := c_eth_header_size + c_eth_crc_size + (c_sdp_W_statistic / c_word_w) * c_sdp_S_pn * c_sdp_S_pn * c_nof_complex;  -- 20 + 2 * 12 * 12 * 2 = 596
  constant c_eth_check_nof_valid          : natural := c_eth_check_nof_packets * c_eth_packet_size;
  constant c_eth_runtime_timeout          : time := (c_nof_sync + 3) * c_nof_clk_per_sync * c_ext_clk_period;  -- eth statistics should be done after c_nof_sync

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal ext_pps             : std_logic := '0';
  signal pps_rst             : std_logic := '1';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_txp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_rxp             : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');

  -- WG
  signal current_bsn_wg      : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- Rx packets
  signal eth_rx_sosi         : t_dp_sosi;
  signal eth_rx_data         : std_logic_vector(c_32 - 1 downto 0);

  -- Decode packets
  signal rx_offload_sosi     : t_dp_sosi;

  -- . view payload statistics data
  signal rx_word_cnt         : natural := 0;
  signal rx_sdp_stat_data    : std_logic_vector(c_32 - 1 downto 0);
  signal rx_sdp_stat_re      : std_logic_vector(c_64 - 1 downto 0);
  signal rx_sdp_stat_im      : std_logic_vector(c_64 - 1 downto 0);
  signal rx_sdp_stat_re_val  : std_logic := '0';
  signal rx_sdp_stat_im_val  : std_logic := '0';
  signal rx_sdp_stat_index   : natural := 0;
  signal rx_a_sp             : natural := 0;
  signal rx_b_sp             : natural := 0;

  -- . view header
  signal rx_hdr_fields_out   : std_logic_vector(1023 downto 0);
  signal rx_hdr_fields_raw   : std_logic_vector(1023 downto 0) := (others => '0');
  signal rx_sdp_stat_header  : t_sdp_stat_header;

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref     : std_logic;
  signal jesd204b_sync_n     : std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk(0) <= not eth_clk(0) after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(5, c_pps_period, '1', pps_rst, ext_clk, pps);
  jesd204b_sysref <= pps;
  ext_pps <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2c_sdp_station_xsub_one : entity lofar2_unb2c_sdp_station_lib.lofar2_unb2c_sdp_station
  generic map (
    g_design_name            => "lofar2_unb2c_sdp_station_xsub_one",
    g_design_note            => "",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr,
    g_wpfb                   => c_wpfb_sim,
    g_bsn_nof_clk_per_sync   => c_nof_clk_per_sync
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK      => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => jesd204b_sysref,
    JESD204B_SYNC_N => jesd204b_sync_n
  );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn  : natural;
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    ----------------------------------------------------------------------------
    -- Enable BSN
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 3,                    0, tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 2,                    0, tb_clk);  -- Init BSN = 0
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 1,   c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0,         16#00000001#, tb_clk);  -- Enable BSN immediately

    -- Release PPS pulser, to get first PPS now and to start BSN source
    wait for 1 us;
    pps_rst <= '0';

    ----------------------------------------------------------------------------
    -- Enable and start WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    for I in 0 to c_sdp_S_pn - 1 loop
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, I * 4 + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, I * 4 + 1, integer(0.0 * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, I * 4 + 2, integer(real(c_wg_subband) * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
      mmf_mm_bus_wr(c_mm_file_reg_diag_wg, I * 4 + 3, integer(real(c_wg_ampl) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl
    end loop;

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, c_bsn_start_wg, tb_clk);  -- first write low then high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,              0, tb_clk);  -- assume v_bsn < 2**31-1

    ----------------------------------------------------------------------------
    -- Setup and enable xsub
    ----------------------------------------------------------------------------

    -- Crosslets info
    for I in 0 to c_sdp_N_crosslets_max - 1 loop
      mmf_mm_bus_wr(c_mm_file_reg_crosslets_info, I, c_subband_select_arr(I), tb_clk);  -- offsets
    end loop;
    mmf_mm_bus_wr(c_mm_file_reg_crosslets_info, 15, c_subband_step, tb_clk);  -- step size

    -- Number of crosslets
    mmf_mm_bus_wr(c_mm_file_reg_nof_crosslets, 0, c_nof_crosslets, tb_clk);

    -- Integration interval and enable
    mmf_mm_bus_wr(c_mm_file_reg_bsn_sync_scheduler_xsub, 1, c_ctrl_interval_size, tb_clk);  -- Interval size
    mmf_mm_bus_wr(c_mm_file_reg_bsn_sync_scheduler_xsub, 2, c_nof_block_per_sync, tb_clk);  -- first write bsn low then bsn high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_sync_scheduler_xsub, 3,                    0, tb_clk);  -- bsn high, assume v_bsn < 2**31-1
    mmf_mm_bus_wr(c_mm_file_reg_bsn_sync_scheduler_xsub, 0,                    1, tb_clk);  -- enable

    ----------------------------------------------------------------------------
    -- XST offload enable
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_stat_enable_xst, 0, 1, tb_clk);

    -- wait for udp offload is done
    proc_common_wait_until_high(ext_clk, eth_done);

    ----------------------------------------------------------------------------
    -- XST processing re-enable
    ----------------------------------------------------------------------------
    if g_try_xst_restart then
      -- XST processing disable
      mmf_mm_bus_wr(c_mm_file_reg_bsn_sync_scheduler_xsub, 0, 0, tb_clk);
      proc_common_wait_some_cycles(ext_clk, c_nof_clk_per_sync / 2);

      -- Read current BSN
      mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
      mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
      proc_common_wait_some_cycles(tb_clk, 1);

      -- XST processing enable
      v_bsn := TO_UINT(current_bsn_wg) + 7;
      mmf_mm_bus_wr(c_mm_file_reg_bsn_sync_scheduler_xsub, 2, v_bsn, tb_clk);  -- first write bsn low then bsn high part
      mmf_mm_bus_wr(c_mm_file_reg_bsn_sync_scheduler_xsub, 3, 0, tb_clk);  -- bsn high, assume v_bsn < 2**31-1
      mmf_mm_bus_wr(c_mm_file_reg_bsn_sync_scheduler_xsub, 0, 1, tb_clk);
      proc_common_wait_some_cycles(ext_clk, c_nof_clk_per_sync * 4);
    end if;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;

  -------------------------------------------------------------------------
  -- Verify proper DUT 1GbE offload output
  -------------------------------------------------------------------------
  -- . Verify Ethernet packet statistics
  u_eth_statistics : entity eth_lib.eth_statistics
    generic map (
      g_runtime_nof_packets => c_eth_check_nof_packets,
      g_runtime_timeout     => c_eth_runtime_timeout,
      g_check_nof_valid     => true,
      g_check_nof_valid_ref => c_eth_check_nof_valid
   )
  port map (
    eth_serial_in => eth_txp(0),
    eth_src_out   => eth_rx_sosi,
    tb_end        => eth_done
  );

  eth_rx_data <= eth_rx_sosi.data(c_32 - 1 downto 0);

  -- . View / verify XST packet header
  u_rx_statistics : entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams         => 1,
    g_data_w              => c_word_w,
    g_hdr_field_arr       => c_sdp_stat_hdr_field_arr,
    g_remove_crc          => true,
    g_crc_nof_words       => 1
  )
  port map (
    mm_rst                => pps_rst,
    mm_clk                => tb_clk,

    dp_rst                => pps_rst,
    dp_clk                => eth_clk(0),

    snk_in_arr(0)         => eth_rx_sosi,

    src_out_arr(0)        => rx_offload_sosi,

    hdr_fields_out_arr(0) => rx_hdr_fields_out,
    hdr_fields_raw_arr(0) => rx_hdr_fields_raw
  );

  rx_sdp_stat_header <= func_sdp_map_stat_header(rx_hdr_fields_raw);

  -- . View / verify XST packet payload
  p_rx_sdp_view_stat_data : process(eth_clk(0))
  begin
    if rising_edge(eth_clk(0)) then
      rx_sdp_stat_re_val <= '0';
      rx_sdp_stat_im_val <= '0';
      if rx_offload_sosi.valid = '1' then
        -- Count words 0, 1, 2, 3
        rx_word_cnt <= 0;
        if rx_word_cnt < c_sdp_N_pol * c_nof_complex - 1 then
          rx_word_cnt <= rx_word_cnt + 1;
        end if;

        -- Count words for the complex statistics with two 32bits words per 64 bit statistic,
        -- high word part and real data received first.
        case rx_word_cnt is
          when 0 => rx_sdp_stat_data <= rx_offload_sosi.data(c_32 - 1 downto 0);
          when 1 => rx_sdp_stat_re   <= rx_sdp_stat_data & rx_offload_sosi.data(c_32 - 1 downto 0);
                    rx_sdp_stat_re_val <= '1';
          when 2 => rx_sdp_stat_data <= rx_offload_sosi.data(c_32 - 1 downto 0);
          when 3 => rx_sdp_stat_im   <= rx_sdp_stat_data & rx_offload_sosi.data(c_32 - 1 downto 0);
                    rx_sdp_stat_im_val <= '1';
          when others => null;
        end case;
      end if;

      -- Count the complex statistics
      if rx_sdp_stat_im_val = '1' then
        rx_sdp_stat_index <= rx_sdp_stat_index + 1;
      end if;

      if rx_offload_sosi.sop = '1' then
        rx_sdp_stat_index <= 0;  -- restart per Rx packet
      end if;
    end if;
  end process;

  p_rx_sdp_verify_stat_data : process(eth_clk(0))
    variable v_subband_ix : natural;  -- _ix = index
  begin
    if rising_edge(eth_clk(0)) then
      v_subband_ix := TO_UINT(rx_sdp_stat_header.app.sdp_data_id_xst_subband_index);
      -- real part
      if rx_sdp_stat_re_val = '1' then
        if v_subband_ix = c_wg_subband then
          -- Expect the strong XST value at the WG subband
          assert almost_equal(TO_SREAL(rx_sdp_stat_re) / c_exp_subband_xst, 1.0, c_max_ratio)
            report "Wrong XST real value at subband = " & int_to_str(v_subband_ix)
            severity ERROR;
        else
          -- WG is only in one subband, so expect almost zero in the other subbands
          assert almost_zero(TO_SREAL(rx_sdp_stat_re) / c_exp_subband_xst, c_max_ratio)
            report "Too large XST real value at subband = " & int_to_str(v_subband_ix)
            severity ERROR;
        end if;
      end if;
      if rx_sdp_stat_im_val = '1' then
        -- All WG have same phase, so expect zero imaginary part.
        -- . The imag part is exactly zero for signal inputs a and b that are both connected to the corresponding input of the WPFB.
        -- . The imag part is  almost zero for signal inputs a and b that are both connected to the opposite inputs of the WPFB, due
        --   to crosstalk rounding difference from input a to b or from input b to a of the complex FFT.
        if (rx_a_sp mod c_sdp_Q_fft) = (rx_b_sp mod c_sdp_Q_fft) then
          assert signed(rx_sdp_stat_im) = 0
            report "Non zero XST imaginary value at subband = " & int_to_str(v_subband_ix)
            severity ERROR;
        else
          assert almost_zero(TO_SREAL(rx_sdp_stat_im) / c_exp_subband_xst, c_max_ratio)
            report "Too large XST imaginary value at subband = " & int_to_str(v_subband_ix)
            severity ERROR;
        end if;
      end if;
    end if;
  end process;

  -- rx_sdp_stat_index counts the S_pn * S_pn = X_sq = 12 * 12 = 144 complex statistics
  rx_a_sp <= rx_sdp_stat_index / c_sdp_S_pn;  -- signal input A
  rx_b_sp <= rx_sdp_stat_index mod c_sdp_S_pn;  -- signal input B
end tb;
