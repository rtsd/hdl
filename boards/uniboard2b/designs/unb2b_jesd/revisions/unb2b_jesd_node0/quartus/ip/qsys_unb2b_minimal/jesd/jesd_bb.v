module jesd (
		input  wire        alldev_lane_aligned,        //       alldev_lane_aligned.export
		output wire [4:0]  csr_cf,                     //                    csr_cf.export
		output wire [1:0]  csr_cs,                     //                    csr_cs.export
		output wire [7:0]  csr_f,                      //                     csr_f.export
		output wire        csr_hd,                     //                    csr_hd.export
		output wire [4:0]  csr_k,                      //                     csr_k.export
		output wire [4:0]  csr_l,                      //                     csr_l.export
		output wire [0:0]  csr_lane_powerdown,         //        csr_lane_powerdown.export
		output wire [7:0]  csr_m,                      //                     csr_m.export
		output wire [4:0]  csr_n,                      //                     csr_n.export
		output wire [4:0]  csr_np,                     //                    csr_np.export
		output wire [3:0]  csr_rx_testmode,            //           csr_rx_testmode.export
		output wire [4:0]  csr_s,                      //                     csr_s.export
		output wire        dev_lane_aligned,           //          dev_lane_aligned.export
		output wire        dev_sync_n,                 //                dev_sync_n.export
		input  wire        jesd204_rx_avs_chipselect,  //            jesd204_rx_avs.chipselect
		input  wire [7:0]  jesd204_rx_avs_address,     //                          .address
		input  wire        jesd204_rx_avs_read,        //                          .read
		output wire [31:0] jesd204_rx_avs_readdata,    //                          .readdata
		output wire        jesd204_rx_avs_waitrequest, //                          .waitrequest
		input  wire        jesd204_rx_avs_write,       //                          .write
		input  wire [31:0] jesd204_rx_avs_writedata,   //                          .writedata
		input  wire        jesd204_rx_avs_clk,         //        jesd204_rx_avs_clk.clk
		input  wire        jesd204_rx_avs_rst_n,       //      jesd204_rx_avs_rst_n.reset_n
		input  wire [31:0] jesd204_rx_dlb_data,        //       jesd204_rx_dlb_data.export
		input  wire [0:0]  jesd204_rx_dlb_data_valid,  // jesd204_rx_dlb_data_valid.export
		input  wire [3:0]  jesd204_rx_dlb_disperr,     //    jesd204_rx_dlb_disperr.export
		input  wire [3:0]  jesd204_rx_dlb_errdetect,   //  jesd204_rx_dlb_errdetect.export
		input  wire [3:0]  jesd204_rx_dlb_kchar_data,  // jesd204_rx_dlb_kchar_data.export
		input  wire        jesd204_rx_frame_error,     //    jesd204_rx_frame_error.export
		output wire        jesd204_rx_int,             //            jesd204_rx_int.irq
		output wire [31:0] jesd204_rx_link_data,       //           jesd204_rx_link.data
		output wire        jesd204_rx_link_valid,      //                          .valid
		input  wire        jesd204_rx_link_ready,      //                          .ready
		input  wire        pll_ref_clk,                //               pll_ref_clk.clk
		input  wire [0:0]  rx_analogreset,             //            rx_analogreset.rx_analogreset
		output wire [0:0]  rx_cal_busy,                //               rx_cal_busy.rx_cal_busy
		input  wire [0:0]  rx_digitalreset,            //           rx_digitalreset.rx_digitalreset
		output wire [0:0]  rx_islockedtodata,          //         rx_islockedtodata.rx_is_lockedtodata
		input  wire [0:0]  rx_serial_data,             //            rx_serial_data.rx_serial_data
		input  wire        rxlink_clk,                 //                rxlink_clk.clk
		input  wire        rxlink_rst_n_reset_n,       //              rxlink_rst_n.reset_n
		output wire [0:0]  rxphy_clk,                  //                 rxphy_clk.export
		output wire [3:0]  sof,                        //                       sof.export
		output wire [3:0]  somf,                       //                      somf.export
		input  wire        sysref                      //                    sysref.export
	);
endmodule

