-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author:
--   Eric Kooistra, 14 feb 2023
-- Purpose:
-- . Test bench for dp_reverse_n_data_fc.
-- Description:
--   Block diagram:
--
--                reverse               reverse back
--    stimuli --> dp_reverse_n_data_fc --> dp_reverse_n_data_fc --> verify
--
-- Remark:
-- . Stimuli and verification copied from tb_dp_deinterleave_one_to_n_to_one.
--
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_reverse_n_data_fc is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    -- specific
    g_pipeline               : natural := 1;  -- 0 for combinatorial, > 0 for registers
    g_nof_repeat             : natural := 5;
    g_reverse_len            : natural := 7;
    g_pkt_gap                : natural := 10
  );
end tb_dp_reverse_n_data_fc;

architecture tb of tb_dp_reverse_n_data_fc is
  constant c_pkt_len                  : natural := g_reverse_len * 5;

  -- dp_stream_stimuli
  constant c_stimuli_pulse_active     : natural := 3;
  constant c_stimuli_pulse_period     : natural := 7;

  constant c_data_w                   : natural := 16;
  constant c_data_init                : natural := 0;
  constant c_re_init                  : natural := 7;
  constant c_im_init                  : natural := 17;
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_err_init                 : natural := 247;
  constant c_channel_init             : natural := 5;  -- fixed

  -- dp_stream_verify
  constant c_verify_pulse_active      : natural := 1;
  constant c_verify_pulse_period      : natural := 5;

  constant c_flow_control_latency_pls : natural := g_nof_repeat * c_pkt_len * (c_verify_pulse_period * c_stimuli_pulse_period) / (c_stimuli_pulse_active * c_verify_pulse_active);
  constant c_flow_control_latency_rnd : natural := g_nof_repeat * c_pkt_len;
  constant c_flow_control_latency     : natural := sel_a_b(g_flow_control_stimuli = e_pulse or g_flow_control_verify = e_pulse,
                                                           c_flow_control_latency_pls,
                                                           c_flow_control_latency_rnd);  -- worst case value

  constant c_data_max                 : unsigned(c_data_w - 1 downto 0) := (others => '1');
  constant c_dsp_max                  : unsigned(c_data_w - 1 downto 0) := (others => '1');

  --CONSTANT c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_rst;  -- default 0 is no wrap
  constant c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := TO_DP_SOSI_UNSIGNED('0', '0', '0', '0', c_data_max, c_dsp_max, c_dsp_max, c_unsigned_0, c_unsigned_0, c_unsigned_0, c_unsigned_0);
  constant c_verify_snk_in_cnt_gap    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_ones;  -- default only accept increment +1

  -- both
  constant c_sync_period              : natural := 10;
  constant c_sync_offset              : natural := 7;

  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';
  signal tb_end                     : std_logic := '0';

  signal stimuli_src_in             : t_dp_siso := c_dp_siso_rdy;
  signal stimuli_src_out            : t_dp_sosi;
  signal stimuli_src_out_data       : std_logic_vector(c_data_w - 1 downto 0);

  signal reverse_src_in             : t_dp_siso;
  signal reverse_src_out            : t_dp_sosi;
  signal reverse_src_out_data       : std_logic_vector(c_data_w - 1 downto 0);

  signal verify_snk_in_enable       : t_dp_sosi_sl := c_dp_sosi_sl_rst;
  signal last_snk_in                : t_dp_sosi;
  signal last_snk_in_evt            : std_logic;
  signal verify_last_snk_in_evt     : t_dp_sosi_sl := c_dp_sosi_sl_rst;

  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal verify_snk_in_data         : std_logic_vector(c_data_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  u_dp_stream_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 15,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_stimuli_pulse_active,
    g_pulse_period   => c_stimuli_pulse_period,
    g_flow_control   => g_flow_control_stimuli,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_use_complex    => false,
    g_data_init      => c_data_init,
    g_re_init        => c_re_init,
    g_im_init        => c_im_init,
    g_bsn_init       => c_bsn_init,
    g_err_init       => c_err_init,
    g_channel_init   => c_channel_init,
    -- specific
    g_in_dat_w       => c_data_w,
    g_nof_repeat     => g_nof_repeat,
    g_pkt_len        => c_pkt_len,
    g_pkt_gap        => g_pkt_gap,
    g_wait_last_evt  => c_flow_control_latency  -- number of clk cycles to wait with last_snk_in_evt after finishing the stimuli
  )
  port map (
    rst                 => rst,
    clk                 => clk,

    -- Generate stimuli
    src_in              => stimuli_src_in,
    src_out             => stimuli_src_out,

    -- End of stimuli
    last_snk_in         => last_snk_in,  -- expected verify_snk_in after end of stimuli
    last_snk_in_evt     => last_snk_in_evt,  -- trigger verify to verify the last_snk_in
    tb_end              => tb_end  -- signal end of tb as far as this dp_stream_stimuli is concerned
  );

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Select fields that need to be verified
  -- . during the test
  verify_snk_in_enable.sync    <= '1';  -- or '0'
  verify_snk_in_enable.bsn     <= '1';
  verify_snk_in_enable.data    <= '1';
  verify_snk_in_enable.valid   <= '1';
  verify_snk_in_enable.sop     <= '1';
  verify_snk_in_enable.eop     <= '1';
  verify_snk_in_enable.empty   <= '0';
  verify_snk_in_enable.channel <= '1';
  verify_snk_in_enable.err     <= '1';

  -- . after the test
  verify_last_snk_in_evt.sync    <= last_snk_in_evt;
  verify_last_snk_in_evt.bsn     <= last_snk_in_evt;
  verify_last_snk_in_evt.data    <= last_snk_in_evt;
  verify_last_snk_in_evt.valid   <= last_snk_in_evt;
  verify_last_snk_in_evt.sop     <= last_snk_in_evt;
  verify_last_snk_in_evt.eop     <= last_snk_in_evt;
  verify_last_snk_in_evt.empty   <= '0';
  verify_last_snk_in_evt.channel <= last_snk_in_evt;
  verify_last_snk_in_evt.err     <= last_snk_in_evt;

  u_dp_stream_verify : entity work.dp_stream_verify
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 14,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_verify_pulse_active,
    g_pulse_period   => c_verify_pulse_period,
    g_flow_control   => g_flow_control_verify,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_snk_in_cnt_max => c_verify_snk_in_cnt_max,
    g_snk_in_cnt_gap => c_verify_snk_in_cnt_gap,
    -- specific
    g_in_dat_w       => c_data_w,
    g_pkt_len        => c_pkt_len
  )
  port map (
    rst                        => rst,
    clk                        => clk,

    -- Verify data
    snk_out                    => verify_snk_out,
    snk_in                     => verify_snk_in,

    -- During stimuli
    verify_snk_in_enable       => verify_snk_in_enable,  -- enable verify to verify that the verify_snk_in fields are incrementing

    -- End of stimuli
    expected_snk_in            => last_snk_in,  -- expected verify_snk_in after end of stimuli
    verify_expected_snk_in_evt => verify_last_snk_in_evt  -- trigger verify to verify the last_snk_in
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  -- Reverse
  u_reverse_fc : entity work.dp_reverse_n_data_fc
  generic map (
    g_pipeline_in  => g_pipeline,
    g_pipeline_out => g_pipeline,
    g_reverse_len  => g_reverse_len
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out     => stimuli_src_in,
    snk_in      => stimuli_src_out,
    src_in      => reverse_src_in,
    src_out     => reverse_src_out
  );

  -- Reverse again to unreverse
  u_unreverse_fc : entity work.dp_reverse_n_data_fc
  generic map (
    g_pipeline_in  => g_pipeline,
    g_pipeline_out => g_pipeline,
    g_reverse_len  => g_reverse_len
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_out     => reverse_src_in,
    snk_in      => reverse_src_out,
    src_in      => verify_snk_out,
    src_out     => verify_snk_in
  );

  ------------------------------------------------------------------------------
  -- Auxiliary
  ------------------------------------------------------------------------------

  -- Map to slv to ease monitoring in wave window
  stimuli_src_out_data <= stimuli_src_out.data(c_data_w - 1 downto 0);
  reverse_src_out_data <= reverse_src_out.data(c_data_w - 1 downto 0);
  verify_snk_in_data   <= verify_snk_in.data(c_data_w - 1 downto 0);
end tb;
