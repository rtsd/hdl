-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Array wrapper for Spectral inversion.
-- Description:
-- . Adds array support and Memory Mapped enable interface to si.vhd.
-- Remark:
-- . See si.vhd for more detail.
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity si_arr is
  generic (
    g_nof_streams : natural := 1;
    g_pipeline    : natural := 1;  -- 0 for wires, 1 for output pipeline
    g_dat_w       : natural := 18
  );
  port (
    in_sosi_arr  : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    out_sosi_arr : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    reg_si_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_si_miso  : out t_mem_miso;

    mm_rst       : in  std_logic;
    mm_clk       : in  std_logic;
    dp_clk       : in  std_logic;
    dp_rst       : in  std_logic
  );
end si_arr;

architecture str of si_arr is
  --TYPE t_c_mem IS RECORD
  --  latency   : NATURAL;    -- read latency
  --  adr_w     : NATURAL;
  --  dat_w     : NATURAL;
  --  nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
  --  init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
  --  --init_file : STRING;     -- "UNUSED", unconstrained length can not be in record
  --END RECORD;

  -- Use one MM word to fit the si_en bits, so this suits g_nof_streams
  -- <= c_word_w = 32.
  constant c_si_mem_reg : t_c_mem := (c_mem_reg_rd_latency, 1, g_nof_streams, 1, '0');

  signal reg_si_en : std_logic_vector(g_nof_streams - 1 downto 0);
begin
  u_mms_common_reg : entity common_lib.mms_common_reg
  generic map (
    g_mm_reg       => c_si_mem_reg
  )
  port map (
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    reg_mosi       => reg_si_mosi,
    reg_miso       => reg_si_miso,

    in_reg         => reg_si_en,
    out_reg        => reg_si_en
  );

  gen_nof_streams : for I in 0 to g_nof_streams - 1 generate
    u_si : entity work.si
    generic map (
      g_pipeline => g_pipeline,
      g_dat_w    => g_dat_w
    )
    port map (
      in_sosi   => in_sosi_arr(I),
      out_sosi  => out_sosi_arr(I),
      si_en     => reg_si_en(I),
      clk       => dp_clk,
      rst       => dp_rst
    );
  end generate;
end str;
