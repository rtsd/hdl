--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, mdio_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use mdio_lib.mdio_pkg.all;
use mdio_lib.mdio_vitesse_vsc8486_pkg.all;

entity tr_xaui_mdio is
  generic (
    g_sim                   : boolean := false;
    g_nof_xaui              : natural := 1;  -- Up to 3 (hard XAUI only) supported
    g_mdio_epcs_dis         : boolean := false  -- TRUE disables EPCS on init; e.g. to target a 10GbE card in PC that does not support it
  );
  port (
    -- Transceiver PLL reference clock
    tr_clk                  : in  std_logic;
    tr_rst                  : in  std_logic;

    -- MM clock for register of optional MDIO master
    mm_clk                  : in  std_logic := '0';
    mm_rst                  : in  std_logic := '0';

    -- MDIO master = mm slave
    mdio_mosi_arr           : in  t_mem_mosi_arr(g_nof_xaui - 1 downto 0) := (others => c_mem_mosi_rst);
    mdio_miso_arr           : out t_mem_miso_arr(g_nof_xaui - 1 downto 0);

    -- MDIO External clock and serial data.
    mdio_rst                : out std_logic;
    mdio_mdc_arr            : out std_logic_vector(g_nof_xaui - 1 downto 0);
    mdio_mdat_in_arr        : in  std_logic_vector(g_nof_xaui - 1 downto 0) := (others => '0');
    mdio_mdat_oen_arr       : out std_logic_vector(g_nof_xaui - 1 downto 0)
  );
end tr_xaui_mdio;

architecture str of tr_xaui_mdio is
  constant c_mdio_xaui_phy_mdc_period     : natural := 256;  -- must be a power of 2
  constant c_mdio_xaui_phy_hold_time      : natural := 10;
  constant c_mdio_xaui_phy_setup_time     : natural := 2;

  constant c_mdio_xaui_phy                : t_c_mdio_phy := (c_mdio_xaui_phy_mdc_period, c_mdio_xaui_phy_hold_time, c_mdio_xaui_phy_setup_time);

  signal mdio_hdr                  : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal mdio_tx_dat               : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal mdio_rx_dat               : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal mdio_en_evt               : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal mdio_done                 : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal mdio_done_ack_evt         : std_logic_vector(g_nof_xaui - 1 downto 0);

  signal reg_mdio_hdr              : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal reg_mdio_tx_dat           : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal reg_mdio_rx_dat           : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal reg_mdio_en_evt           : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal reg_mdio_done             : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal reg_mdio_done_ack_evt     : std_logic_vector(g_nof_xaui - 1 downto 0);

  signal ctlr_mdio_rst             : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal ctlr_mdio_hdr             : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal ctlr_mdio_tx_dat          : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal ctlr_mdio_rx_dat          : t_slv_16_arr(g_nof_xaui - 1 downto 0);
  signal ctlr_mdio_en_evt          : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal ctlr_mdio_done            : std_logic_vector(g_nof_xaui - 1 downto 0);
  signal ctlr_mdio_done_ack_evt    : std_logic_vector(g_nof_xaui - 1 downto 0);

  signal ctlr_exec_complete        : std_logic_vector(g_nof_xaui - 1 downto 0);
begin
  -- UniBoard FN resets 4 Vitesse chips using one output.
  mdio_rst <= ctlr_mdio_rst(0);

  gen_nof_xaui : for i in g_nof_xaui - 1 downto 0 generate
    -- PHY core
    u_mdio_phy : entity mdio_lib.mdio_phy
    generic map(
      g_mdio_phy      => c_mdio_xaui_phy,
      g_add_mdc_cycle => true
    )
    port map (
      gs_sim            => g_sim,

      rst               => tr_rst,
      clk               => tr_clk,

      mdio_en_evt       => mdio_en_evt(i),
      mdio_done         => mdio_done(i),
      mdio_done_ack_evt => mdio_done_ack_evt(i),

      hdr               => mdio_hdr(i),
      tx_dat            => mdio_tx_dat(i),
      rx_dat            => mdio_rx_dat(i),

      -- External clock and serial data
      mdc               => mdio_mdc_arr(i),
      mdat_in           => mdio_mdat_in_arr(i),
      mdat_oen          => mdio_mdat_oen_arr(i)
    );

    -- MM port
    u_mdio_phy_reg : entity mdio_lib.mdio_phy_reg
    port map (
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,

      mdio_rst          => tr_rst,
      mdio_clk          => tr_clk,

      sla_in            => mdio_mosi_arr(i),
      sla_out           => mdio_miso_arr(i),

      mdio_en_evt       => reg_mdio_en_evt(i),
      mdio_done_ack_evt => reg_mdio_done_ack_evt(i),
      mdio_done         => reg_mdio_done(i),

      mdio_hdr          => reg_mdio_hdr(i),
      mdio_tx_dat       => reg_mdio_tx_dat(i),
      mdio_rx_dat       => reg_mdio_rx_dat(i)
    );

    -- MDIO controller auto-executes MDIO sequence on startup
    gen_mdio_epcs : if g_mdio_epcs_dis = false generate
      u_mdio_ctlr_epcs : entity mdio_lib.mdio_ctlr
      generic map (
         g_mdio_prtad           => c_mdio_vsc8486_prtad,
         g_mdio_cmd_arr         => c_mdio_vsc8486_init_cmd_arr,
         g_mdio_rst_level       => '0',
         g_mdio_rst_cycles      => sel_a_b(g_sim, 10, 250000),
         g_mdio_post_rst_cycles => sel_a_b(g_sim, 10, 250000)
        )
      port map (
        rst               => tr_rst,
        clk               => tr_clk,

        mdio_rst          => ctlr_mdio_rst(i),
        mdio_en_evt       => ctlr_mdio_en_evt(i),
        mdio_done         => ctlr_mdio_done(i),

        mdio_done_ack_evt => ctlr_mdio_done_ack_evt(i),

        hdr               => ctlr_mdio_hdr(i),
        tx_dat            => ctlr_mdio_tx_dat(i),

        exec_complete     => ctlr_exec_complete(i)
      );
    end generate;

    -- MDIO controller auto-executes MDIO sequence on startup
    gen_mdio_no_epcs : if g_mdio_epcs_dis = true generate
      u_mdio_ctlr_no_epcs : entity mdio_lib.mdio_ctlr
      generic map (
         g_mdio_prtad           => c_mdio_vsc8486_prtad,
         g_mdio_cmd_arr         => c_mdio_vsc8486_init_epcs_dis_cmd_arr,
         g_mdio_rst_level       => '0',
         g_mdio_rst_cycles      => sel_a_b(g_sim, 10, 250000),
         g_mdio_post_rst_cycles => sel_a_b(g_sim, 10, 250000)
        )
      port map (
        rst               => tr_rst,
        clk               => tr_clk,

        mdio_rst          => ctlr_mdio_rst(i),
        mdio_en_evt       => ctlr_mdio_en_evt(i),
        mdio_done         => ctlr_mdio_done(i),

        mdio_done_ack_evt => ctlr_mdio_done_ack_evt(i),

        hdr               => ctlr_mdio_hdr(i),
        tx_dat            => ctlr_mdio_tx_dat(i),

        exec_complete     => ctlr_exec_complete(i)
      );
    end generate;

    -- Connect the mdio_ctlr to the mdio_phy initially, when it's done connect the MM controller to allow user control/monitoring.
    mdio_en_evt(i)       <= ctlr_mdio_en_evt(i)       when ctlr_exec_complete(i) = '0' else reg_mdio_en_evt(i);
    mdio_done_ack_evt(i) <= ctlr_mdio_done_ack_evt(i) when ctlr_exec_complete(i) = '0' else reg_mdio_done_ack_evt(i);
    mdio_hdr(i)          <= ctlr_mdio_hdr(i)          when ctlr_exec_complete(i) = '0' else reg_mdio_hdr(i);
    mdio_tx_dat(i)       <= ctlr_mdio_tx_dat(i)       when ctlr_exec_complete(i) = '0' else reg_mdio_tx_dat(i);

    ctlr_mdio_done(i)    <= mdio_done(i)   when ctlr_exec_complete(i) = '0' else '0';
    reg_mdio_done(i)     <= mdio_done(i)   when ctlr_exec_complete(i) = '1' else '0';
    reg_mdio_rx_dat(i)   <= mdio_rx_dat(i) when ctlr_exec_complete(i) = '1' else (others => '0');
  end generate;
end str;
