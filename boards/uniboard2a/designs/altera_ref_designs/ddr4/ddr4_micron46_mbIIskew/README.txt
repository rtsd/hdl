This is an Altera reference design for DDR4

It works on Uniboard^2 MB_II

The self-generated 'qii' design is now archived in the Altera Archive file:
emif_0_example_design/ed_synth_ddr4_micron46_mbIIskew.qar

In Quartus this projects can be extracted and recompiled.
After programming the compiled .SOF file in the Uniboard^2 FPGA(s) use the
External Memory Interface Toolkit in Quartus->Tools->System Debugging Tools->External Memory Interface Toolkit
