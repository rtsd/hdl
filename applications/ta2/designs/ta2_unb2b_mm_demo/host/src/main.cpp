/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Reinier vd Walle
* Purpose:
* . Test the ta2_unb2b_mm_demo OpenCL application in emulator
* Description:
* . Run: -> make ta2_unb2b_mm_demo
* . Navigate to -> cd $HDL_WORK/unb2b/OpenCL/ta2_unb2b_mm_demo/bin
* . Execute -> CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 ./host
* *********************************************************************** */
#include <CL/cl_ext_intelfpga.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "common.h"
#include <unistd.h>

using namespace std;
int main(int argc, char **argv)
{
    if (argc > 2) {
        cerr << "usage: " << argv[0] << " [ta2_unb2b_mm_demo.aocx]" << endl;
        exit(1);
    }

    // Initialize OpenCL
    cl::Context context;
    vector<cl::Device> devices;
    init(context, devices);
    cl::Device &device = devices[0];

    // Get program
    string filename_bin = string(argc == 2 ? argv[1] : "ta2_unb2b_mm_demo.aocx");
    cl::Program program = get_program(context, device, filename_bin);


    // Setup command queues
    vector<cl::CommandQueue> queues(4);

    for (cl::CommandQueue &queue : queues) {
        queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);
    }

    cl::Event computeDone[4];

    // Setup FPGA kernels
    cl::Kernel mmInController(program, "mm_in_controller");
    cl::Kernel mmOutController(program, "mm_out_controller");
    cl::Kernel processA(program, "process_a");
    cl::Kernel processB(program, "process_b");


    // Run FPGA kernels
    clog << ">>> Run fpga" << endl;
    try {
        queues[0].enqueueTask(processA, nullptr, &computeDone[0]);
        queues[1].enqueueTask(processB, nullptr, &computeDone[1]);
        queues[2].enqueueTask(mmOutController, nullptr, &computeDone[2]);
        queues[3].enqueueTask(mmInController, nullptr, &computeDone[3]);

    } catch (cl::Error &error) {
        cerr << "Error launching kernel: " << error.what() << endl;
        exit(EXIT_FAILURE);
    }

    // Write IO channel file
    vector<char> cmdVecs[] = {{'A', 'B', 'C', 'D', 0x33, 0x00, 0x00, 0x00, 0x01}, //write on undefined address
                     {'E', 'F', 'G', 'H', 0x00, 0x00, 0x00, 0x00, 0x01},          // write on addr 0
                     {'I', 'J', 'K', 'L', 0x01, 0x00, 0x00, 0x00, 0x01},          // write on addr 1
                     {'M', 'N', 'O', 'P', 0x02, 0x00, 0x00, 0x00, 0x01},          // write on addr 2
                     {0x00, 0x00, 0x00, 0x00, 0x33, 0x00, 0x00, 0x00, 0x00},      // read on undefined address
                     {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},      // read on addr 0
                     {0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00},      // read on addr 1
                     {0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00}};     // read on addr 2

    ofstream output_fileA("kernel_input_mm");
    ostream_iterator<char> output_iteratorA(output_fileA, "");
    for (int i = 0; i < 8; i++)
      copy(cmdVecs[i].begin(), cmdVecs[i].end(), output_iteratorA);

    output_fileA.close(); 
    clog << ">>> Written IO file" << endl;

    // wait for mm_out_controller to be finished
    computeDone[2].wait();

    // print output IO channel file
    const string inputFileB = "kernel_output_mm";
    ifstream fileB(inputFileB);
    clog << fileB.rdbuf() << endl;

    return EXIT_SUCCESS;
}
