-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Provide MM access to the ST input of a FIFO.
-- Description:
--   An MM master can directly write the FIFO, provided that the MM data width
--   (g_mm_word_w) fits within the ST data width.
--   The MM master can read the mm_usedw[] value to know how many valid data
--   words are in the FIFO.
--   The MM master can read the mm_availw[] value to know how many valid data
--   it can write to the FIFO in a loop, until it needs to check mm_availw[]
--   again. The mm_availw[] is defined as g_fifo_size - g_fifo_af_margin -
--   mm_usedw[].
--   When mm_availw[] > 0 then the MM master can be sure that the mm_wrdata
--   will fit in the FIFO. Therefore it is not needed to use src_out.ready and
--   in this way the MM master does not need to check whether the FIFO has free
--   space left for every write FIFO data access.
-- Remark:
-- . This dp_fifo_from_mm connects to the MM clock domain side of the FIFO,
--   hence is operates entirely in the MM clock domain.
-- . The ST sop, eop, empty, channel and error fields are not supported.

entity dp_fifo_from_mm is
  generic (
    g_fifo_size      : natural := 512;
    g_fifo_af_margin : natural := 1;  -- >=1, so > 0 to avoid FIFO full (which causes usedw=0, because value g_fifo_size then wraps to 0)
    g_mm_word_w      : natural := 32
  );
  port (
    rst           : in  std_logic;  -- MM clock domain
    clk           : in  std_logic;
    -- ST soure connected to FIFO input
    src_out       : out t_dp_sosi;
    usedw         : in  std_logic_vector(ceil_log2(g_fifo_size) - 1 downto 0);
    -- Control for FIFO read access
    mm_wr         : in  std_logic;  -- MM write pulse to write the mm_wrdata to src_out.data
    mm_wrdata     : in  std_logic_vector(g_mm_word_w - 1 downto 0);
    mm_usedw      : out std_logic_vector(g_mm_word_w - 1 downto 0);
    mm_availw     : out std_logic_vector(g_mm_word_w - 1 downto 0)
  );
end dp_fifo_from_mm;

architecture str of dp_fifo_from_mm is
  constant c_fifo_almost_full : natural := g_fifo_size - g_fifo_af_margin;  -- FIFO almost full level, usedw will always be <= c_fifo_almost_full
  constant c_usedw_w          : natural := ceil_log2(g_fifo_size);

  signal availw        : std_logic_vector(c_usedw_w - 1 downto 0);  -- no need to use ceil_log2(g_fifo_size+1), because availw = g_fifo_size - g_fifo_af_margin < g_fifo_size when the FIFO is empty
begin
  assert g_fifo_af_margin /= 0
    report "dp_fifo_from_mm.vhd : the g_fifo_af_margin must be > 0"
    severity FAILURE;

  -- Combinatorial logic
  availw <= std_logic_vector(to_unsigned(c_fifo_almost_full, c_usedw_w) - unsigned(usedw));  -- No danger for underflow (< 0) because usedw will always be <= c_fifo_almost_full

  -- Wires
  mm_usedw  <= RESIZE_UVEC(usedw,  g_mm_word_w);
  mm_availw <= RESIZE_UVEC(availw, g_mm_word_w);

  src_out.valid <= mm_wr;
  src_out.data  <= RESIZE_DP_DATA(mm_wrdata);  -- Unsigned resize to ST data width
end str;
