-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   D.F. Brouwer
-- Purpose:
--   Simulation model for DDIO in
-- Description:
--   The double data rate datain samples that arrive at time series t0, t1, t2,
--   ... get output with samples t0, t2, ... in dataout_l and samples t1, t3,
--   ... in dataout_h. Hence dataout = dataout_h & dataout_l contains the
--   time series samples in little endian format with the first sample in the
--   LSpart as shown in the timing diagram:
--               _   _   _   _
--     ck       | |_| |_| |_| |_
--     datain    0 1 2 3 4 5 6 7
--     in_dat_r      1   3   5
--     in_dat_f    0   2   4
--     dataout_h     1   3   5
--     dataout_l     0   2   4
-- Reference:
--   Copied from ip_arria10_e2sg/ddio/sim/ip_arria10_e2sg_ddio_in_1.vhd

library IEEE;
use IEEE.std_logic_1164.all;

entity ip_agi027_1e1v_ddio_in_1 is
  port (
    datain    : in  std_logic_vector(0 downto 0) := (others => '0');
    ck        : in  std_logic                    := '0';
    aclr      : in  std_logic                    := '0';
    dataout_h : out std_logic_vector(0 downto 0);
    dataout_l : out std_logic_vector(0 downto 0)
  );
end ip_agi027_1e1v_ddio_in_1;

architecture beh of ip_agi027_1e1v_ddio_in_1 is
  signal in_dat_r   : std_logic;
  signal in_dat_f   : std_logic;
begin
  in_dat_r <= datain(0) when rising_edge(ck);
  in_dat_f <= datain(0) when falling_edge(ck);

  dataout_h <= (others => in_dat_r);
  dataout_l <= (others => in_dat_f) when rising_edge(ck);
end beh;
