--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------
-- Author: Eric Kooistra
-- Purpose:
--   The dp_repack_data_out_shift stage for dp_repack_data, using data shift in.
-- Description:
--   See dp_repack_data.
--
library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity dp_repack_data_out_shift is
  generic (
    g_bypass           : boolean := false;
    g_in_buf_dat_w     : natural;
    g_out_dat_w        : natural;
    g_out_nof_words    : natural;
    g_out_symbol_w     : natural := 1  -- default 1 for snk_in.empty in nof bits, else use power of 2
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;

    snk_out          : out t_dp_siso;
    snk_in           : in  t_dp_sosi;
    snk_in_data      : in  std_logic_vector(g_in_buf_dat_w - 1 downto 0);

    src_in           : in  t_dp_siso;
    src_out          : out t_dp_sosi
  );
end dp_repack_data_out_shift;

architecture rtl of dp_repack_data_out_shift is
  constant c_out_buf_dat_w     : natural := g_out_dat_w * g_out_nof_words;
  constant c_out_buf_dat_lo    : natural := sel_a_b(c_out_buf_dat_w > g_in_buf_dat_w,
                            c_out_buf_dat_w -  g_in_buf_dat_w, 0);  -- pack into subsection with 0 or more padding bits
  constant c_snk_in_dat_lo     : natural := sel_a_b(c_out_buf_dat_w < g_in_buf_dat_w,
                       g_in_buf_dat_w - c_out_buf_dat_w, 0);  -- unpack from subsection that has 0 or more padding bits
  constant c_bit_cnt_max       : natural := c_out_buf_dat_w;
  constant c_out_empty_lo      : natural := true_log2(g_out_symbol_w);

  type t_dat_arr is array (integer range <>) of std_logic_vector(g_out_dat_w - 1 downto 0);

  type t_reg is record
    dat_arr       : t_dat_arr(g_out_nof_words - 1 downto 0);
    src_out       : t_dp_sosi;
    hold_out      : t_dp_sosi;  -- hold src_out valid and sync/sop/eop until src_in.ready
    shift         : std_logic;  -- shift out the dat_arr
    dat_bit_cnt   : natural range 0 to c_bit_cnt_max;  -- actual nof bits in subsection
    pack_bit_cnt  : natural range 0 to c_bit_cnt_max;  -- count nof bits in subsection
    empty_bit_cnt : natural range 0 to c_bit_cnt_max;  -- empty nof bits in subsection
    eos           : std_logic;  -- end of subsection
  end record;

  signal data_vec  : std_logic_vector(c_out_buf_dat_w - 1 downto 0) := (others => '0');

  signal r_snk_out : t_dp_siso := c_dp_siso_rdy;
  signal r         : t_reg;
  signal nxt_r     : t_reg;

  -- Debug signals
  signal i_src_out          : t_dp_sosi;
  signal src_out_data       : std_logic_vector(g_out_dat_w - 1 downto 0);

  signal dbg_g_in_buf_dat_w : natural := g_in_buf_dat_w;
  signal dbg_g_out_dat_w    : natural := g_out_dat_w;
  signal dbg_out_nof_words  : natural := g_out_nof_words;
  signal dbg_out_symbol_w   : natural := g_out_symbol_w;
  signal dbc_out_buf_dat_w  : natural := c_out_buf_dat_w;
  signal dbc_out_buf_dat_lo : natural := c_out_buf_dat_lo;
  signal dbc_snk_in_dat_lo  : natural := c_snk_in_dat_lo;
begin
  src_out      <= i_src_out;
  src_out_data <= i_src_out.data(g_out_dat_w - 1 downto 0);

  gen_bypass : if g_bypass = true generate
    snk_out <= src_in;

    p_src_out : process(snk_in)
    begin
      i_src_out <= snk_in;
      if c_snk_in_dat_lo > 0 then
        i_src_out.data  <= SHIFT_UVEC(RESIZE_DP_DATA(snk_in_data), c_snk_in_dat_lo);
        i_src_out.empty <= INCR_UVEC( snk_in.empty, -c_snk_in_dat_lo);
      end if;
      if c_out_buf_dat_lo > 0 then
        i_src_out.data  <= SHIFT_UVEC(RESIZE_DP_DATA(snk_in_data), -c_out_buf_dat_lo);
        i_src_out.empty <= INCR_UVEC( snk_in.empty, c_out_buf_dat_lo);
      end if;
    end process;
  end generate;

  no_bypass : if g_bypass = false generate
    p_comb : process(rst, snk_in, r, data_vec, src_in)
      variable v : t_reg;
    begin
      ------------------------------------------------------------------------
      -- Default
      v := r;
      v.src_out.sync  := '0';
      v.src_out.valid := '0';
      v.src_out.sop   := '0';
      v.src_out.eop   := '0';

      ------------------------------------------------------------------------
      -- Function
      if r.hold_out.valid = '0' then

        -- Clear hold_out for new output valid
        if r.src_out.sop = '1' then
          v.hold_out.sync := '0';
          v.hold_out.sop  := '0';
        end if;
        if r.src_out.eop = '1' then
          v.hold_out.eop := '0';
        end if;

        -- Capture the snk_in block info that is valid at sop and eop
        if snk_in.sop = '1' then
          v.hold_out.sop    := '1';
          v.hold_out.sync   := snk_in.sync;
          v.src_out.bsn     := snk_in.bsn;
          v.src_out.channel := snk_in.channel;
        end if;
        if snk_in.eop = '1' then
          v.hold_out.eop    := '1';  -- local function will calculate src_out.empty based on snk_in.empty
          v.src_out.err     := snk_in.err;
        end if;

        if r.shift = '1' then
          -- shift out rest of subsection
          v.hold_out.valid := '1';

          -- shift up from low to high and shift out at high index
          v.dat_arr(g_out_nof_words - 1 downto 1) := r.dat_arr(g_out_nof_words - 2 downto 0);
          v.dat_arr(0) := (others => '0');  -- shift in data=0

          v.pack_bit_cnt := r.pack_bit_cnt - g_out_dat_w;

          -- end of pack subsection
          if v.pack_bit_cnt <= r.empty_bit_cnt then
            v.eos   := '1';  -- end of subsection, so ready for new snk_in
            v.shift := '0';  -- stop shifting
          end if;

        elsif snk_in.valid = '1' then
          -- start of pack subsection
          v.hold_out.valid := '1';

          for I in 0 to g_out_nof_words - 1 loop
            v.dat_arr(I) := data_vec((I + 1) * g_out_dat_w - 1 downto I * g_out_dat_w);
          end loop;

          v.dat_bit_cnt := g_in_buf_dat_w - c_snk_in_dat_lo;  -- default dat_bit_cnt per subsection
          if snk_in.eop = '1' then
            -- pass on last subsection dat_bit_cnt info via DP empty field
            v.dat_bit_cnt := g_in_buf_dat_w - TO_UINT(snk_in.empty);
          end if;

          v.pack_bit_cnt  := c_out_buf_dat_w - g_out_dat_w;
          v.empty_bit_cnt := c_out_buf_dat_w - v.dat_bit_cnt;
          v.eos           := '0';
          v.shift         := '1';

          -- end of pack subsection
          if v.pack_bit_cnt <= v.empty_bit_cnt then
            v.eos   := '1';  -- end of subsection, so ready for new snk_in
            v.shift := '0';
          end if;
        end if;

        -- fill in local empty if this is the last subsection of a block
        if v.eos = '1' then
          if v.hold_out.eop = '1' then
            v.src_out.empty := TO_DP_EMPTY(v.empty_bit_cnt - v.pack_bit_cnt);  -- in nof bits
            v.src_out.empty := SHIFT_UVEC(v.src_out.empty, c_out_empty_lo);  -- in nof symbols
          end if;
        end if;

        -- pass on the v.dat_arr as data vector
        v.src_out.data  := RESIZE_DP_DATA(v.dat_arr(g_out_nof_words - 1));

        -- output valid data when ready, else hold_out.valid to signal pending output
        if v.hold_out.valid = '1' then
          if src_in.ready = '1' then
            v.src_out.valid  := '1';
            v.src_out.sync   := v.hold_out.sync;
            v.src_out.sop    := v.hold_out.sop;
            v.src_out.eop    := v.hold_out.eop and v.eos;  -- output eop at end of subsection
            v.hold_out.valid := '0';
          end if;
        end if;

      else
        -- pending output
        if src_in.ready = '1' then
          v.src_out.valid := '1';
          v.src_out.sync  := r.hold_out.sync;
          v.src_out.sop   := r.hold_out.sop;
          v.src_out.eop   := r.hold_out.eop and r.eos;  -- output eop at end of subsection
          v.hold_out.valid := '0';
        end if;
      end if;

      ------------------------------------------------------------------------
      -- Reset and nxt_r
      if rst = '1' then
        v.src_out       := c_dp_sosi_rst;
        v.hold_out      := c_dp_sosi_rst;
        v.shift         := '0';
        v.dat_bit_cnt   := 0;
        v.pack_bit_cnt  := 0;
        v.empty_bit_cnt := 0;
        v.eos           := '0';
      end if;

      nxt_r <= v;
    end process;

    --------------------------------------------------------------------------
    -- p_reg
    r <= nxt_r when rising_edge(clk);

    --------------------------------------------------------------------------
    -- Wires
    data_vec(c_out_buf_dat_w - 1 downto c_out_buf_dat_lo) <= snk_in_data(g_in_buf_dat_w - 1 downto c_snk_in_dat_lo);

    --------------------------------------------------------------------------
    -- Wired output
    i_src_out <= r.src_out;

    --------------------------------------------------------------------------
    -- Flow control

    -- local function flow control
    p_flow : process(nxt_r)
    begin
      r_snk_out <= c_dp_siso_rdy;
      if nxt_r.shift = '1' and nxt_r.eos = '0' then
        r_snk_out.ready <= '0';  -- output shift out stage function is only ready when it is not shifting or at the
                                 -- end of the subsection
      end if;
    end process;

    -- combined local and remote src_in flow control
    snk_out.ready <= r_snk_out.ready when nxt_r.hold_out.valid = '0' else src_in.ready;  -- if there is pending output
                                                                  -- then the src_in ready determines the flow control
    snk_out.xon   <= src_in.xon;  -- just pass on the xon/off frame flow control
  end generate;
end rtl;
