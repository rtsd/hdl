----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/27/2023 10:50:24 AM
-- Design Name: 
-- Module Name: append_crc_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.crc_pkg.all;
use work.bit_width_config_pkg.all;
use work.rdma_pkg.all;

entity append_crc_wrapper is
    Port ( 
      i_clk              : in std_logic;
      i_clk_reset        : in std_logic;
      
      -- o_stream
      -- Source In and Sink Out
      o_stream_tready    : in std_logic;
  
      -- Source Out and Sink In
      o_stream_tvalid    : out std_logic;
      o_stream_tdata     : out std_logic_vector(512 - 1 downto 0);
      o_stream_tkeep     : out std_logic_vector(512 / 8 - 1 downto 0);
      o_stream_tlast     : out std_logic;
  
      -- i_stream
      -- Source In and Sink Out
      i_stream_tready    : out std_logic;
  
      -- Source Out and Sink In
      i_stream_tvalid    : in std_logic;
      i_stream_tdata     : in std_logic_vector(512 - 1 downto 0);
      i_stream_tkeep     : in std_logic_vector(512 / 8 - 1 downto 0);
      i_stream_tlast     : in std_logic
      );
end append_crc_wrapper;

architecture Behavioral of append_crc_wrapper is

  signal i_stream_stop : std_logic;
  signal o_stream_stop : std_logic;
  
  signal o_stream      : AXI4_Streaming_RDMA_t;
  signal i_stream      : AXI4_Streaming_RDMA_t;

begin 
  -- Wire Records to IN/OUT ports.
  -- o_stream
  i_stream_stop   <= not o_stream_tready;

  o_stream_tvalid <= o_stream.valid;
  o_stream_tlast  <= o_stream.last;
  o_stream_tdata  <= o_stream.data;
  o_stream_tkeep  <= o_stream.keep;
  
  -- i_stream
  i_stream_tready <= not o_stream_stop;

  i_stream.valid <= i_stream_tvalid;  
  i_stream.last  <= i_stream_tlast;
  i_stream.data  <= i_stream_tdata;
  i_stream.keep  <= i_stream_tkeep;
  
u_append_crc : entity work.append_crc
  generic map(
    g_AREA_SPEED_TRADEOFF_FACTOR     => 32,
    g_INIT_CRC                       => f_ICRC_INIT,
    g_BIT_REVERSE_IN                 => true,
    g_BIT_REVERSE_OUT                => true,
    g_XOR_CRC_OUT                    => (31 downto 0 => '1'),
    g_OR_MASK                        => to_slv(c_RoCEv2_HEADER_INVARIANT_MASK),
    g_INVALID_CYCLES_BETWEEN_PACKETS => 1    
  )
  port map(
    i_clk         => i_clk,
    i_clk_reset   => i_clk_reset,
    i_stream      => i_stream,
    o_stream_stop => o_stream_stop,
    o_stream      => o_stream,
    i_stream_stop => i_stream_stop
  );
    
end Behavioral;
