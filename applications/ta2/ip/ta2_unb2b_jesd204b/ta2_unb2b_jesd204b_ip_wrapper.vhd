-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Instantiates ta2_unb2b_10GbE component
library IEEE;
use IEEE.std_logic_1164.all;

entity ta2_unb2b_jesd204b_ip_wrapper is
  port (
    config_clk       : in  std_logic;  -- 100MHz clk for reconfig block and status interface
    config_reset     : in  std_logic;

    -- MM Control
    jesd204b_mosi_address     : in  std_logic_vector(7 downto 0);
    jesd204b_mosi_wrdata      : in  std_logic_vector(31 downto 0);
    jesd204b_mosi_wr          : in  std_logic;
    jesd204b_mosi_rd          : in  std_logic;
    jesd204b_miso_rddata      : out std_logic_vector(31 downto 0);
    jesd204b_miso_waitrequest : out std_logic;

    -- JESD204B external signals
    jesd204b_refclk       : in std_logic := '0';  -- Reference clock. For AD9683 use 200MHz direct from clock reference pin
    jesd204b_sysref       : in std_logic := '0';  -- SYSREF should drive ADC and FPGA with correct phase wrt jesd204b_device_clk
    jesd204b_sync_n_arr   : out std_logic_vector(0 downto 0);  -- output to control ADC initialization/syncronization phase

    serial_rx_arr         : in  std_logic_vector(0 downto 0);

    kernel_clk       : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
    kernel_reset     : in  std_logic;

    kernel_src_data  : out std_logic_vector(15 downto 0);  -- RX Data to kernel
    kernel_src_valid : out std_logic;  -- RX data valid signal to kernel
    kernel_src_ready : in  std_logic  -- Flow control from kernel

  );
end ta2_unb2b_jesd204b_ip_wrapper;

architecture str of ta2_unb2b_jesd204b_ip_wrapper is
  ----------------------------------------------------------------------------
  -- ta2_unb2b_ Component
  ----------------------------------------------------------------------------
  component ta2_unb2b_jesd204b is
    port (
      config_clk       : in  std_logic;  -- 100MHz clk for reconfig block and status interface
      config_reset     : in  std_logic;

      -- MM Control
      jesd204b_mosi_address     : in  std_logic_vector(7 downto 0);
      jesd204b_mosi_wrdata      : in  std_logic_vector(31 downto 0);
      jesd204b_mosi_wr          : in  std_logic;
      jesd204b_mosi_rd          : in  std_logic;
      jesd204b_miso_rddata      : out std_logic_vector(31 downto 0);
      jesd204b_miso_waitrequest : out std_logic;

      -- JESD204B external signals
      jesd204b_refclk       : in std_logic := '0';  -- Reference clock. For AD9683 use 200MHz direct from clock reference pin
      jesd204b_sysref       : in std_logic := '0';  -- SYSREF should drive ADC and FPGA with correct phase wrt jesd204b_device_clk
      jesd204b_sync_n_arr   : out std_logic_vector(0 downto 0);  -- output to control ADC initialization/syncronization phase

      serial_rx_arr         : in  std_logic_vector(0 downto 0);

      kernel_clk       : in  std_logic;  -- Kernel clock (runs the kernel_* I/O below)
      kernel_reset     : in  std_logic;

      kernel_src_data  : out std_logic_vector(15 downto 0);  -- RX Data to kernel
      kernel_src_valid : out std_logic;  -- RX data valid signal to kernel
      kernel_src_ready : in  std_logic  -- Flow control from kernel

    );
  end component ta2_unb2b_jesd204b;
begin
  u_ta2_unb2b_jesd204b : ta2_unb2b_jesd204b
    port map (
      config_clk                => config_clk,
      config_reset              => config_reset,
      jesd204b_mosi_address     => jesd204b_mosi_address,
      jesd204b_mosi_wrdata      => jesd204b_mosi_wrdata,
      jesd204b_mosi_wr          => jesd204b_mosi_wr,
      jesd204b_mosi_rd          => jesd204b_mosi_rd,
      jesd204b_miso_rddata      => jesd204b_miso_rddata,
      jesd204b_miso_waitrequest => jesd204b_miso_waitrequest,
      jesd204b_refclk           => jesd204b_refclk,
      jesd204b_sysref           => jesd204b_sysref,
      jesd204b_sync_n_arr       => jesd204b_sync_n_arr,
      serial_rx_arr             => serial_rx_arr,
      kernel_clk                => kernel_clk,
      kernel_reset              => kernel_reset,
      kernel_src_data           => kernel_src_data,
      kernel_src_valid          => kernel_src_valid,
      kernel_src_ready          => kernel_src_ready
    );
end str;
