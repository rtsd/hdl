-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common_pkg.all;

entity common_round is
  --
  -- ISE XST results for rounding 36b --> 18b:
  --    int      clip  -->  slices  FFs  LUTs
  -- 1) signed   TRUE       63      54   80       -- increases with input widths > 18b
  -- 2) signed   FALSE      59      54   73       -- increases with input widths > 18b
  -- 3) unsigned TRUE       34      37   43       -- same for all input widths > 18b
  -- 4) unsigned FALSE      21      37   19       -- same for all input widths > 18b
  --
  -- If the input comes from a product and is rounded to the input width then g_round_clip can safely be FALSE, because e.g. for unsigned
  -- 4b*4b=8b->4b the maximum product is 15*15=225 <= 255-8, so wrapping will never occur.
  -- When g_round = FALSE then truncate (= remove) the LSbits and then g_round_clip and g_round_even are dont care.
  --
  generic (
    g_representation  : string  := "SIGNED";  -- SIGNED (round +-0.5 away from zero to +- infinity) or UNSIGNED rounding (round 0.5 up to + inifinity)
    g_round           : boolean := true;  -- when TRUE round the input, else truncate the input
    g_round_clip      : boolean := false;  -- when TRUE clip rounded input >= +max to avoid wrapping to output -min (signed) or 0 (unsigned)
    g_round_even      : boolean := false;  -- when TRUE round to even, else round away from zero
    g_pipeline_input  : natural := 0;  -- >= 0
    g_pipeline_output : natural := 1;  -- >= 0, use g_pipeline_input=0 and g_pipeline_output=0 for combinatorial output
    g_in_dat_w        : natural := 36;
    g_out_dat_w       : natural := 18
  );
  port (
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_dat     : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    out_dat    : out std_logic_vector(g_out_dat_w - 1 downto 0)
  );
end;

architecture rtl of common_round is
  constant c_remove_w     : integer := g_in_dat_w - g_out_dat_w;

  signal reg_dat       : std_logic_vector(in_dat'range);
  signal res_dat       : std_logic_vector(out_dat'range);
begin
  u_input_pipe : entity work.common_pipeline
  generic map (
    g_representation => g_representation,
    g_pipeline       => g_pipeline_input,
    g_in_dat_w       => g_in_dat_w,
    g_out_dat_w      => g_in_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => in_dat,
    out_dat => reg_dat
  );

  -- Increase to out_dat width
  no_s : if c_remove_w <= 0 and g_representation = "SIGNED" generate
    res_dat <= RESIZE_SVEC(reg_dat, g_out_dat_w);
  end generate;

  no_u : if c_remove_w <= 0 and g_representation = "UNSIGNED" generate
    res_dat <= RESIZE_UVEC(reg_dat, g_out_dat_w);
  end generate;

  -- Decrease to out_dat width by c_remove_w number of LSbits
  -- . rounding
  gen_s : if c_remove_w > 0 and g_round = true and g_representation = "SIGNED" generate
    res_dat <= s_round(reg_dat, c_remove_w, g_round_clip, g_round_even);
  end generate;

  gen_u : if c_remove_w > 0 and g_round = true and g_representation = "UNSIGNED" generate
    res_dat <= u_round(reg_dat, c_remove_w, g_round_clip, g_round_even);
  end generate;

  -- . truncating
  gen_t : if c_remove_w > 0 and g_round = false generate
    res_dat <= truncate(reg_dat, c_remove_w);
  end generate;

  u_output_pipe : entity work.common_pipeline
  generic map (
    g_representation => g_representation,
    g_pipeline       => g_pipeline_output,
    g_in_dat_w       => g_out_dat_w,
    g_out_dat_w      => g_out_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => res_dat,
    out_dat => out_dat
  );
end rtl;
