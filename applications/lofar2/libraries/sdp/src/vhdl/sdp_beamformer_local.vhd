-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- . Implements the functionality of beamformer_local in node_sdp_beamformer.
-- Description:
-- The local BF function weights the subbands from the S_pn signal inputs and
-- adds them to form the local beamlet sum.
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use work.sdp_pkg.all;

entity sdp_beamformer_local is
  generic (
    g_bf_weights_file_name : string := "UNUSED";
    -- Use no default raw width, to force instance to set it
    g_raw_dat_w            : natural;  -- default: c_sdp_W_subband;
    g_raw_fraction_w       : natural  -- default: 0
  );
  port (
    dp_clk      : in  std_logic;
    dp_rst      : in  std_logic;

    in_sosi_arr : in  t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
    out_sosi    : out t_dp_sosi;

    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;

    ram_bf_weights_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_bf_weights_miso : out t_mem_miso
  );
end sdp_beamformer_local;

architecture str of sdp_beamformer_local is
  constant c_complex_adder_latency : natural := ceil_log2(c_sdp_S_pn);
  constant c_bf_weights_latency    : natural := 5;  -- 3 for complex multiplier + 2 RAM latency
  constant c_total_latency         : natural := 3 + c_bf_weights_latency + c_complex_adder_latency;

  -- Product width, do -1 to skip double sign bit in product
  constant c_product_w             : natural := c_sdp_W_bf_weight + g_raw_dat_w - 1;

  -- With BF adder wrapping a c_complex_adder_sum_w = c_sdp_W_beamlet_sum is
  -- in fact enough, because the MSbit will be simply removed. However,
  -- it is fine to use wider BF adder, because the extra MSbits will likely
  -- be optimized away by synthesis. If instead BF adder clipping was used
  -- then it would be necessary to keep the extra bits, to avoid wrapping of
  -- the sum.
  constant c_complex_adder_sum_w   : natural := c_product_w + ceil_log2(c_sdp_S_pn);

  signal sub_sosi_arr             : t_dp_sosi_arr(c_sdp_N_pol_bf * c_sdp_P_pfb - 1 downto 0) :=
                                        (others => c_dp_sosi_rst);

  signal bf_weights_out_sosi_arr  : t_dp_sosi_arr(c_sdp_N_pol_bf * c_sdp_P_pfb - 1 downto 0) :=
                                        (others => c_dp_sosi_rst);

  signal bf_weights_x_sosi_arr    : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal bf_weights_y_sosi_arr    : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal deinterleaved_x_sosi_arr : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal deinterleaved_y_sosi_arr : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal interleave_out_sosi_arr  : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)  := (others => c_dp_sosi_rst);

  signal complex_add_out_sosi     : t_dp_sosi := c_dp_sosi_rst;
  signal pipelined_in_sosi        : t_dp_sosi := c_dp_sosi_rst;
  signal dp_requantize_in_sosi    : t_dp_sosi := c_dp_sosi_rst;
begin
  ---------------------------------------------------------------
  -- COPY INPUT STREAMS FOR X AND Y POLARIZATION PATHS
  --   0: 5 = S_pn signal inputs (time multiplexed by Q_fft) for BF X pol
  --   6:11 = S_pn signal inputs (time multiplexed by Q_fft) for BF Y pol
  ---------------------------------------------------------------
  -- Use short index variables PB (= Polarization Beamlet), I (= Instance)
  -- names, to ease recognizing them as loop indices.
  gen_pol : for PB in 0 to c_sdp_N_pol_bf - 1 generate
    gen_pfb : for I in 0 to c_sdp_P_pfb - 1 generate
      sub_sosi_arr(PB * c_sdp_P_pfb + I) <= in_sosi_arr(I);
    end generate;
  end generate;

  ---------------------------------------------------------------
  -- BEAMFORMER WEIGHTS
  ---------------------------------------------------------------
  u_sdp_bf_weights : entity work.sdp_bf_weights
    generic map (
      g_gains_file_name => g_bf_weights_file_name,
      g_raw_dat_w       => g_raw_dat_w
    )
    port map(
      dp_clk         => dp_clk,
      dp_rst         => dp_rst,

      in_sosi_arr    => sub_sosi_arr,
      out_sosi_arr   => bf_weights_out_sosi_arr,

      mm_rst         => mm_rst,
      mm_clk         => mm_clk,

      ram_gains_mosi => ram_bf_weights_mosi,
      ram_gains_miso => ram_bf_weights_miso
    );

  -- X pol is lower half of bf_weights_out
  bf_weights_x_sosi_arr <= bf_weights_out_sosi_arr(c_sdp_P_pfb - 1 downto 0);

  -- Y pol is upper half of bf_weights_out
  bf_weights_y_sosi_arr <= bf_weights_out_sosi_arr(2 * c_sdp_P_pfb - 1 downto c_sdp_P_pfb);

  ---------------------------------------------------------------
  -- DEINTERLEAVE X PATH
  ---------------------------------------------------------------
  gen_deinterleave_x_pol : for I in 0 to c_sdp_P_pfb - 1 generate
    u_dp_deinterleave_x_pol : entity dp_lib.dp_deinterleave_one_to_n
    generic map(
      g_nof_outputs => c_sdp_Q_fft
    )
    port map(
      rst   => dp_rst,
      clk   => dp_clk,

      snk_in => bf_weights_x_sosi_arr(I),
      src_out_arr(0) => deinterleaved_x_sosi_arr(c_sdp_Q_fft * I),
      src_out_arr(1) => deinterleaved_x_sosi_arr(c_sdp_Q_fft * I + 1)
    );
  end generate;

  ---------------------------------------------------------------
  -- DEINTERLEAVE Y PATH
  ---------------------------------------------------------------
  gen_deinterleave_y_pol : for I in 0 to c_sdp_P_pfb - 1 generate
    u_dp_deinterleave_y_pol : entity dp_lib.dp_deinterleave_one_to_n
    generic map(
      g_nof_outputs => c_sdp_Q_fft
    )
    port map(
      rst   => dp_rst,
      clk   => dp_clk,

      snk_in => bf_weights_y_sosi_arr(I),
      src_out_arr(0) => deinterleaved_y_sosi_arr(c_sdp_Q_fft * I),
      src_out_arr(1) => deinterleaved_y_sosi_arr(c_sdp_Q_fft * I + 1)
    );
  end generate;

  ---------------------------------------------------------------
  -- INTERLEAVE X AND Y POLARIZATIONS
  ---------------------------------------------------------------
  gen_interleave : for I in 0 to c_sdp_S_pn - 1 generate
    u_dp_interleave : entity dp_lib.dp_interleave_n_to_one
    generic map(
      g_nof_inputs => c_sdp_N_pol_bf
    )
    port map(
      rst   => dp_rst,
      clk   => dp_clk,

      snk_in_arr(0) => deinterleaved_x_sosi_arr(I),
      snk_in_arr(1) => deinterleaved_y_sosi_arr(I),
      src_out => interleave_out_sosi_arr(I)
    );
  end generate;

  ---------------------------------------------------------------
  -- ADD
  ---------------------------------------------------------------
  u_dp_complex_add : entity dp_lib.dp_complex_add
  generic map(
    g_nof_inputs => c_sdp_S_pn,
    g_data_w => c_product_w
  )
  port map(
    rst   => dp_rst,
    clk   => dp_clk,

    snk_in_arr => interleave_out_sosi_arr,
    src_out => complex_add_out_sosi
  );

  ---------------------------------------------------------------
  -- DP PIPELINE IN_SOSI FIELDS
  ---------------------------------------------------------------
  u_pipeline : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline    => c_total_latency
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,
    snk_in  => in_sosi_arr(0),
    src_out => pipelined_in_sosi
  );

  ---------------------------------------------------------------
  -- COMBINE OUTPUT WITH PIPELINED IN_SOSI
  ---------------------------------------------------------------
  p_sosi : process(pipelined_in_sosi, complex_add_out_sosi)
  begin
    dp_requantize_in_sosi <= pipelined_in_sosi;
    dp_requantize_in_sosi.re <= complex_add_out_sosi.re;
    dp_requantize_in_sosi.im <= complex_add_out_sosi.im;
  end process;

  ---------------------------------------------------------------
  -- REQUANTIZE
  ---------------------------------------------------------------
  u_dp_requantize : entity dp_lib.dp_requantize
  generic map (
    g_complex             => true,
    g_representation      => "SIGNED",
    g_lsb_w               => c_sdp_W_bf_weight_fraction + g_raw_fraction_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_msb_clip            => false,  -- wrap beamlet overflow
    g_msb_clip_symmetric  => false,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,  -- no msb clipping, so no need for pipeline
    g_in_dat_w            => c_complex_adder_sum_w,
    g_out_dat_w           => c_sdp_W_beamlet_sum
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => dp_requantize_in_sosi,
    -- ST source
    src_out      => out_sosi
  );
end str;
