-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--     Jonathan Hargreaves, 16 Nov 2016
--
-- Purpose
--     Calculate Stokes products I, Q, U and V for a single complex input stream
-- Description
--     Input data are time multiplexed [time ie BLOCK0,BLOCK1 ...][subband fbin0,fbin1, ...][Polatization X,Y]
--     Use the valid signal to synchronize. Breaks are allowed at any time.
--     Nint (g_nof_int) succsessive values to be integrated
--     Outputs four parallel data streams containing the integrated I,Q,U,V products
-- Remarks
--     The outputs are placed in the data field of a dp_sosi record
--     I is scaled up by one bit because it is always a positive real
--     Q and U are real
--     V is imaginary

library IEEE, common_lib, common_mult_lib, technology_lib, dp_lib, iquv_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity iquv is
  generic (
    g_sim           : boolean := false;
    g_technology    : natural := c_tech_select_default;
    g_variant       : string  := "IP";  -- overide with "RTL" for sc3
    g_use_accum     : boolean := true;
    g_nof_int       : natural := 8;  -- number of successive subbands to integrate
    g_in_data_w     : natural := 12;  -- width of the incoming complex data
    g_out_data_w    : natural := 18;  -- width to round the output to
    g_lsb_adj_w     : natural := 0  -- number of bit to shift the output down
  );
  port (
    dp_rst          : in  std_logic;
    dp_clk          : in  std_logic;

    -- Streaming
    in_complex      : in  t_dp_sosi;  -- Complex input data
    i_out           : out t_dp_sosi;  -- Complex output data I
    q_out           : out t_dp_sosi;  -- Complex output data Q
    u_out           : out t_dp_sosi;  -- Complex output data U
    v_out           : out t_dp_sosi  -- Complex output data V

  );
end iquv;

architecture str of iquv is
  constant c_multprod_w           : natural := g_in_data_w * 2;
  constant c_iquv_w               : natural := c_multprod_w + 1;
  constant c_nof_int              : natural := sel_a_b(g_use_accum, g_nof_int, 1);  -- force nof_int to 1 if accum is not used
  constant c_accumcount_w         : natural := true_log2(c_nof_int);
  constant c_iquv_accum_w         : natural := c_iquv_w + c_accumcount_w;
  constant c_lsb_w                : natural := c_iquv_accum_w - g_out_data_w - g_lsb_adj_w;
  --CONSTANT c_lsb_w                : NATURAL := 30; --25; -- based on EK & JH analysis 30-08-18

  signal ctrl_count               : std_logic_vector(1 downto 0);
  signal enable_strobe            : std_logic;
  signal in_complex_re_d1         : std_logic_vector(g_in_data_w - 1 downto 0) := (others => '0');
  signal in_complex_im_d1         : std_logic_vector(g_in_data_w - 1 downto 0) := (others => '0');
  signal in_complex_valid         : std_logic_vector(0 downto 0) := "0";
  signal in_complex_valid_d1      : std_logic_vector(0 downto 0) := "0";
  signal product_xx               : std_logic_vector(c_multprod_w - 1 downto 0);
  signal product_xy_re            : std_logic_vector(c_multprod_w - 1 downto 0);
  signal product_xy_im            : std_logic_vector(c_multprod_w - 1 downto 0);
  signal product_xy_im_neg        : std_logic_vector(c_multprod_w - 1 downto 0);
  signal product_yy               : std_logic_vector(c_multprod_w - 1 downto 0);
  signal products_valid           : std_logic;
  signal products_valid_vec       : std_logic_vector(0 downto 0) := "0";
  signal products_valid_vec_dly   : std_logic_vector(0 downto 0) := "0";
  signal product_xy_re_dly        : std_logic_vector(c_multprod_w - 1 downto 0);
  signal product_xy_im_dly        : std_logic_vector(c_multprod_w - 1 downto 0);
  signal i_result                 : std_logic_vector(c_iquv_w - 1 downto 0);
  signal q_result                 : std_logic_vector(c_iquv_w - 1 downto 0);
  signal u_result                 : std_logic_vector(c_iquv_w - 1 downto 0);
  signal v_result                 : std_logic_vector(c_iquv_w - 1 downto 0);
  signal i_out_raw_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal q_out_raw_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal u_out_raw_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal v_out_raw_sosi           : t_dp_sosi := c_dp_sosi_rst;
begin
  ------------------------------------------------------------------------------
  -- control counter to demultiplex the X and Y polarizations
  ------------------------------------------------------------------------------
  u_ctrl_count : entity common_lib.common_counter
  generic map (
    g_latency   => 1,
    g_init      => 0,
    g_width     => 2,
    g_step_size => 1
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,
    cnt_en      => in_complex.valid,
    count       => ctrl_count
  );

  enable_strobe  <= ctrl_count(0) when in_complex.valid = '1' else '0';

  ------------------------------------------------------------------------------
  -- delay the input one clock
  ------------------------------------------------------------------------------
  u_delay_input_real : entity common_lib.common_pipeline
  generic map (
    g_in_dat_w    => g_in_data_w,
    g_out_dat_w   => g_in_data_w,
    g_reset_value => 0,
    g_pipeline    => 1
  )
  port map (
    clk           => dp_clk,
    rst           => dp_rst,
    in_dat        => in_complex.re(g_in_data_w - 1 downto 0),
    out_dat       => in_complex_re_d1
  );

  u_delay_input_imag : entity common_lib.common_pipeline
  generic map (
    g_in_dat_w    => g_in_data_w,
    g_out_dat_w   => g_in_data_w,
    g_reset_value => 0,
    g_pipeline    => 1
  )
  port map (
    clk           => dp_clk,
    rst           => dp_rst,
    in_dat        => in_complex.im(g_in_data_w - 1 downto 0),
    out_dat       => in_complex_im_d1
  );

  in_complex_valid(0) <= in_complex.valid;

  u_delay_input_valid : entity common_lib.common_pipeline
  generic map (
    g_in_dat_w    => 1,
    g_out_dat_w   => 1,
    g_reset_value => 0,
    g_pipeline    => 1
  )
  port map (
    clk           => dp_clk,
    rst           => dp_rst,
    in_dat        => in_complex_valid,
    out_dat       => in_complex_valid_d1
  );

  ------------------------------------------------------------------------------
  -- Two complex multipliers (XX'. XY' and YY')
  ------------------------------------------------------------------------------
--  u_CmplxMul_xx : ENTITY common_mult_lib.common_complex_mult
--  GENERIC MAP (
--    g_technology       => g_technology,
--    g_in_a_w           => g_in_data_w,
--    g_in_b_w           => g_in_data_w,
--    g_out_p_w          => c_multprod_w,
--    g_conjugate_b      => true
--  )
--  port map (
--    rst       => dp_rst,
--    clk       => dp_clk,
--    in_ar     => in_complex_re_d1,
--    in_ai     => in_complex_im_d1,
--    in_br     => in_complex_re_d1,
--    in_bi     => in_complex_im_d1,
--    in_val    => enable_strobe,
--    out_pr    => product_xx,
--    out_pi    => open,
--    out_val   => products_valid
--  );

  u_CmplxMul_xy : entity common_mult_lib.common_complex_mult
  generic map (
    g_technology       => g_technology,
    g_variant          => g_variant,
    g_in_a_w           => g_in_data_w,
    g_in_b_w           => g_in_data_w,
    g_out_p_w          => c_multprod_w,
    g_conjugate_b      => true
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,
    in_ar     => in_complex_re_d1,
    in_ai     => in_complex_im_d1,
    in_br     => in_complex.re(g_in_data_w - 1 downto 0),
    in_bi     => in_complex.im(g_in_data_w - 1 downto 0),
    in_val    => enable_strobe,
    out_pr    => product_xy_re,
    out_pi    => product_xy_im,
    out_val   => open
  );

  u_CmplxMul_xxyy : entity common_mult_lib.common_complex_mult
  generic map (
    g_technology       => g_technology,
    g_variant          => g_variant,
    g_in_a_w           => g_in_data_w,
    g_in_b_w           => g_in_data_w,
    g_out_p_w          => c_multprod_w,
    g_conjugate_b      => true
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,
    in_ar     => in_complex.re(g_in_data_w - 1 downto 0),
    in_ai     => in_complex.im(g_in_data_w - 1 downto 0),
    in_br     => in_complex.re(g_in_data_w - 1 downto 0),
    in_bi     => in_complex.im(g_in_data_w - 1 downto 0),
    in_val    => enable_strobe,
    out_pr    => product_yy,
    out_pi    => open,
    out_val   => products_valid
  );

  ------------------------------------------------------------------------------
  -- Delay the YY product one clock to get the XX product
  ------------------------------------------------------------------------------
  u_delay_yy : entity common_lib.common_pipeline
  generic map (
    g_in_dat_w    => c_multprod_w,
    g_out_dat_w   => c_multprod_w,
    g_reset_value => 0,
    g_pipeline    => 1
  )
  port map (
    clk           => dp_clk,
    rst           => dp_rst,
    in_dat        => product_yy,
    out_dat       => product_xx
  );

  ------------------------------------------------------------------------------
  -- I = XX' + YY'
  ------------------------------------------------------------------------------
  u_adder_i : entity common_lib.common_add_sub
  generic map (
    g_representation   => "UNSIGNED",
    g_direction        => "ADD",
    g_in_dat_w         => c_multprod_w,
    g_out_dat_w        => c_iquv_w
  )
  port map (
    clk       => dp_clk,
    clken     => '1',
    in_a      => product_xx,
    in_b      => product_yy,
    result    => i_result
  );

  ------------------------------------------------------------------------------
  -- Q = XX' - YY'
  ------------------------------------------------------------------------------
  u_adder_q : entity common_lib.common_add_sub
  generic map (
    g_representation   => "SIGNED",
    g_direction        => "SUB",
    g_in_dat_w         => c_multprod_w,
    g_out_dat_w        => c_iquv_w
  )
  port map (
    clk       => dp_clk,
    clken     => '1',
    in_a      => product_xx,
    in_b      => product_yy,
    result    => q_result
  );

  ------------------------------------------------------------------------------
  -- Delay XY' to match adders, then bitshift to calculate
  -- U = 2 * Re(XY')
  -- V = -2 * Im(XY')
  -- note the Im part is negated before the delay
  -- Delay the multiplier products valid output as well
  ------------------------------------------------------------------------------
  u_delay_xy_real : entity common_lib.common_pipeline
  generic map (
    g_in_dat_w  => c_multprod_w,
    g_out_dat_w => c_multprod_w,
    g_reset_value => 0,
    g_pipeline  => 1
  )
  port map (
    rst           => dp_rst,
    clk           => dp_clk,
    in_dat        => product_xy_re,
    out_dat       => product_xy_re_dly
  );

  product_xy_im_neg <= TO_SVEC(-TO_SINT(product_xy_im), c_multprod_w);

  u_delay_xy_imag : entity common_lib.common_pipeline
  generic map (
    g_in_dat_w    => c_multprod_w,
    g_out_dat_w   => c_multprod_w,
    g_reset_value => 0,
    g_pipeline    => 1
  )
  port map (
    rst           => dp_rst,
    clk           => dp_clk,
    in_dat        => product_xy_im_neg,
    out_dat       => product_xy_im_dly
  );

  u_result <= product_xy_re_dly(c_multprod_w - 1 downto 0) & '0';
  v_result <= product_xy_im_dly(c_multprod_w - 1 downto 0) & '0';

  products_valid_vec(0) <= products_valid;

  u_delay_products_valid : entity common_lib.common_pipeline
  generic map (
    g_in_dat_w    => 1,
    g_out_dat_w   => 1,
    g_reset_value => 0,
    g_pipeline    => 1
  )
  port map (
    rst           => dp_rst,
    clk           => dp_clk,
    in_dat        => products_valid_vec,
    out_dat       => products_valid_vec_dly
  );

  ------------------------------------------------------------------------------
  -- Accumulators to integrate the Stokes products
  ------------------------------------------------------------------------------
  gen_iquv_accum : if g_use_accum = true generate
    u_iquv_accum : entity work.iquv_accum
    generic map (
      g_accumcount_w  => c_accumcount_w,
      g_in_data_w     => c_iquv_w
    )
    port map (
      dp_rst          => dp_rst,
      dp_clk          => dp_clk,
      i_in            => i_result,
      q_in            => q_result,
      u_in            => u_result,
      v_in            => v_result,
      in_valid        => products_valid_vec_dly(0),
      i_out_sosi      => i_out_raw_sosi,
      q_out_sosi      => q_out_raw_sosi,
      u_out_sosi      => u_out_raw_sosi,
      v_out_sosi      => v_out_raw_sosi
    );
  end generate;

  gen_noiquv_accum : if g_use_accum = false generate
    i_out_raw_sosi.data(c_iquv_w - 1 downto 0)   <= i_result;
    q_out_raw_sosi.data(c_iquv_w - 1 downto 0)   <= q_result;
    u_out_raw_sosi.data(c_iquv_w - 1 downto 0)   <= u_result;
    v_out_raw_sosi.data(c_iquv_w - 1 downto 0)   <= v_result;
    i_out_raw_sosi.valid    <= products_valid_vec_dly(0);
    q_out_raw_sosi.valid    <= products_valid_vec_dly(0);
    u_out_raw_sosi.valid    <= products_valid_vec_dly(0);
    v_out_raw_sosi.valid    <= products_valid_vec_dly(0);
  end generate;

  ------------------------------------------------------------------------------
  -- Requantize the outputs to the desired bit width
  ------------------------------------------------------------------------------
  u_dp_requantize_i : entity dp_lib.dp_requantize
  generic map (
    g_complex             => false,
    g_representation      => "UNSIGNED",
    g_lsb_w               => c_lsb_w - 1,
    g_lsb_round           => true,
    g_lsb_round_clip      => true,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => true,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_iquv_accum_w,
    g_out_dat_w           => g_out_data_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => i_out_raw_sosi,
    src_out    => i_out,
    out_ovr    => open
  );

  u_dp_requantize_q : entity dp_lib.dp_requantize
  generic map (
    g_complex             => false,
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => true,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => true,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_iquv_accum_w,
    g_out_dat_w           => g_out_data_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => q_out_raw_sosi,
    src_out    => q_out,
    out_ovr    => open
  );

  u_dp_requantize_u : entity dp_lib.dp_requantize
  generic map (
    g_complex             => false,
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => true,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => true,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_iquv_accum_w,
    g_out_dat_w           => g_out_data_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => u_out_raw_sosi,
    src_out    => u_out,
    out_ovr    => open
  );

  u_dp_requantize_v : entity dp_lib.dp_requantize
  generic map (
    g_complex             => false,
    g_representation      => "SIGNED",
    g_lsb_w               => c_lsb_w,
    g_lsb_round           => true,
    g_lsb_round_clip      => true,
    g_msb_clip            => true,
    g_msb_clip_symmetric  => true,
    g_gain_w              => 0,
    g_pipeline_remove_lsb => 1,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => c_iquv_accum_w,
    g_out_dat_w           => g_out_data_w
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    snk_in     => v_out_raw_sosi,
    src_out    => v_out,
    out_ovr    => open
  );
end str;
