--IP Functional Simulation Model
--VERSION_BEGIN 11.1SP2 cbx_mgl 2012:01:25:21:26:09:SJ cbx_simgen 2012:01:25:21:25:27:SJ  VERSION_END


-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

 LIBRARY sgate;
 USE sgate.sgate_pack.all;

--synthesis_resources = lut 7 mux21 10 oper_add 2 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  altera_merlin_traffic_limiter_0001 IS 
	 PORT 
	 ( 
		 clk	:	IN  STD_LOGIC;
		 cmd_sink_channel	:	IN  STD_LOGIC_VECTOR (1 DOWNTO 0);
		 cmd_sink_data	:	IN  STD_LOGIC_VECTOR (65 DOWNTO 0);
		 cmd_sink_endofpacket	:	IN  STD_LOGIC;
		 cmd_sink_ready	:	OUT  STD_LOGIC;
		 cmd_sink_startofpacket	:	IN  STD_LOGIC;
		 cmd_sink_valid	:	IN  STD_LOGIC;
		 cmd_src_channel	:	OUT  STD_LOGIC_VECTOR (1 DOWNTO 0);
		 cmd_src_data	:	OUT  STD_LOGIC_VECTOR (65 DOWNTO 0);
		 cmd_src_endofpacket	:	OUT  STD_LOGIC;
		 cmd_src_ready	:	IN  STD_LOGIC;
		 cmd_src_startofpacket	:	OUT  STD_LOGIC;
		 cmd_src_valid	:	OUT  STD_LOGIC_VECTOR (1 DOWNTO 0);
		 reset	:	IN  STD_LOGIC;
		 rsp_sink_channel	:	IN  STD_LOGIC_VECTOR (1 DOWNTO 0);
		 rsp_sink_data	:	IN  STD_LOGIC_VECTOR (65 DOWNTO 0);
		 rsp_sink_endofpacket	:	IN  STD_LOGIC;
		 rsp_sink_ready	:	OUT  STD_LOGIC;
		 rsp_sink_startofpacket	:	IN  STD_LOGIC;
		 rsp_sink_valid	:	IN  STD_LOGIC;
		 rsp_src_channel	:	OUT  STD_LOGIC_VECTOR (1 DOWNTO 0);
		 rsp_src_data	:	OUT  STD_LOGIC_VECTOR (65 DOWNTO 0);
		 rsp_src_endofpacket	:	OUT  STD_LOGIC;
		 rsp_src_ready	:	IN  STD_LOGIC;
		 rsp_src_startofpacket	:	OUT  STD_LOGIC;
		 rsp_src_valid	:	OUT  STD_LOGIC
	 ); 
 END altera_merlin_traffic_limiter_0001;

 ARCHITECTURE RTL OF altera_merlin_traffic_limiter_0001 IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
	 SIGNAL	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_0_343q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_1_342q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_2_341q	:	STD_LOGIC := '0';
	 SIGNAL  wire_ni_w_lg_w_lg_w413w415w423w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w_lg_w_lg_w413w415w416w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w_lg_w413w415w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w422w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w414w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_ni_w413w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_channel_0_337q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_channel_1_305q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_dest_id_0_304q	:	STD_LOGIC := '0';
	 SIGNAL	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_has_pending_responses_293q	:	STD_LOGIC := '0';
	 SIGNAL  wire_nO_w420w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_sink_ready_346m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_0_353m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_1_352m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_internal_valid_347m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_314m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_315m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_316m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_318m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_319m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_320m_dataout	:	STD_LOGIC;
	 SIGNAL  wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_a	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_b	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_gnd	:	STD_LOGIC;
	 SIGNAL  wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_o	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_a	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_b	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
	 SIGNAL  wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_o	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
	 SIGNAL  wire_w_lg_w412w418w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_lg_w_cmd_sink_data_range157w428w432w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_cmd_sink_valid430w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w424w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w417w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_lg_w412w418w419w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_reset427w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w470w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w412w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w421w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w429w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  wire_w_lg_w_cmd_sink_data_range157w428w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_always1_321_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_348_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_349_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_nonposted_cmd_accepted_310_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_response_accepted_312_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_save_dest_id_298_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_344_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_345_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_vcc :	STD_LOGIC;
	 SIGNAL  wire_w_cmd_sink_data_range157w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
 BEGIN

	wire_gnd <= '0';
	wire_w_lg_w412w418w(0) <= wire_w412w(0) AND wire_w417w(0);
	wire_w_lg_w_lg_w_cmd_sink_data_range157w428w432w(0) <= wire_w_lg_w_cmd_sink_data_range157w428w(0) AND wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_internal_valid_347m_dataout;
	wire_w_lg_cmd_sink_valid430w(0) <= cmd_sink_valid AND wire_w429w(0);
	wire_w424w(0) <= s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_nonposted_cmd_accepted_310_dataout AND wire_ni_w_lg_w_lg_w413w415w423w(0);
	wire_w417w(0) <= s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_response_accepted_312_dataout AND wire_ni_w_lg_w_lg_w413w415w416w(0);
	wire_w_lg_w_lg_w412w418w419w(0) <= NOT wire_w_lg_w412w418w(0);
	wire_w_lg_reset427w(0) <= NOT reset;
	wire_w470w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_always1_321_dataout;
	wire_w412w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_nonposted_cmd_accepted_310_dataout;
	wire_w421w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_response_accepted_312_dataout;
	wire_w429w(0) <= NOT s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_345_dataout;
	wire_w_lg_w_cmd_sink_data_range157w428w(0) <= NOT wire_w_cmd_sink_data_range157w(0);
	cmd_sink_ready <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_sink_ready_346m_dataout;
	cmd_src_channel <= ( cmd_sink_channel(1 DOWNTO 0));
	cmd_src_data <= ( cmd_sink_data(65 DOWNTO 0));
	cmd_src_endofpacket <= cmd_sink_endofpacket;
	cmd_src_startofpacket <= cmd_sink_startofpacket;
	cmd_src_valid <= ( wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_1_352m_dataout & wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_0_353m_dataout);
	rsp_sink_ready <= rsp_src_ready;
	rsp_src_channel <= ( rsp_sink_channel(1 DOWNTO 0));
	rsp_src_data <= ( rsp_sink_data(65 DOWNTO 0));
	rsp_src_endofpacket <= rsp_sink_endofpacket;
	rsp_src_startofpacket <= rsp_sink_startofpacket;
	rsp_src_valid <= rsp_sink_valid;
	s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_always1_321_dataout <= (s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_nonposted_cmd_accepted_310_dataout AND s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_response_accepted_312_dataout);
	s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_348_dataout <= (cmd_sink_valid AND cmd_sink_channel(0));
	s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_349_dataout <= (cmd_sink_valid AND cmd_sink_channel(1));
	s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_nonposted_cmd_accepted_310_dataout <= (wire_w_lg_w_lg_w_cmd_sink_data_range157w428w432w(0) AND (cmd_sink_endofpacket AND cmd_src_ready));
	s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_response_accepted_312_dataout <= (rsp_sink_endofpacket AND (rsp_sink_valid AND rsp_src_ready));
	s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_save_dest_id_298_dataout <= (wire_w_lg_w_cmd_sink_data_range157w428w(0) AND wire_w_lg_cmd_sink_valid430w(0));
	s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_344_dataout <= (wire_w_lg_w_cmd_sink_data_range157w428w(0) AND altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_has_pending_responses_293q);
	s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_345_dataout <= ((cmd_sink_data(64) XOR altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_dest_id_0_304q) AND s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_344_dataout);
	s_wire_vcc <= '1';
	wire_w_cmd_sink_data_range157w(0) <= cmd_sink_data(52);
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_0_343q <= '0';
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_1_342q <= '0';
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_2_341q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
			IF (s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_always1_321_dataout = '0') THEN
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_0_343q <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_320m_dataout;
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_1_342q <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_319m_dataout;
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_2_341q <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_318m_dataout;
			END IF;
		END IF;
	END PROCESS;
	wire_ni_w_lg_w_lg_w413w415w423w(0) <= wire_ni_w_lg_w413w415w(0) AND wire_ni_w422w(0);
	wire_ni_w_lg_w_lg_w413w415w416w(0) <= wire_ni_w_lg_w413w415w(0) AND altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_0_343q;
	wire_ni_w_lg_w413w415w(0) <= wire_ni_w413w(0) AND wire_ni_w414w(0);
	wire_ni_w422w(0) <= NOT altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_0_343q;
	wire_ni_w414w(0) <= NOT altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_1_342q;
	wire_ni_w413w(0) <= NOT altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_2_341q;
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_channel_0_337q <= '0';
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_channel_1_305q <= '0';
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_dest_id_0_304q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
			IF (s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_save_dest_id_298_dataout = '1') THEN
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_channel_0_337q <= cmd_sink_channel(0);
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_channel_1_305q <= cmd_sink_channel(1);
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_dest_id_0_304q <= cmd_sink_data(64);
			END IF;
		END IF;
	END PROCESS;
	PROCESS (clk, reset)
	BEGIN
		IF (reset = '1') THEN
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_has_pending_responses_293q <= '0';
		ELSIF (clk = '1' AND clk'event) THEN
				altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_has_pending_responses_293q <= (wire_nO_w420w(0) OR (wire_w421w(0) AND wire_w424w(0)));
		END IF;
	END PROCESS;
	wire_nO_w420w(0) <= altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_has_pending_responses_293q AND wire_w_lg_w_lg_w412w418w419w(0);
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_sink_ready_346m_dataout <= cmd_src_ready AND NOT(s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_345_dataout);
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_0_353m_dataout <= (altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_channel_0_337q AND s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_348_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_344_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_348_dataout;
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_1_352m_dataout <= (altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_last_channel_1_305q AND s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_349_dataout) WHEN s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_344_dataout = '1'  ELSE s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_cmd_src_valid_349_dataout;
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_internal_valid_347m_dataout <= cmd_sink_valid AND NOT(s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_suppress_345_dataout);
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_314m_dataout <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_o(2) WHEN s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_nonposted_cmd_accepted_310_dataout = '1'  ELSE altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_2_341q;
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_315m_dataout <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_o(1) WHEN s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_nonposted_cmd_accepted_310_dataout = '1'  ELSE altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_1_342q;
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_316m_dataout <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_o(0) WHEN s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_nonposted_cmd_accepted_310_dataout = '1'  ELSE altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_0_343q;
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_318m_dataout <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_o(3) WHEN s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_response_accepted_312_dataout = '1'  ELSE wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_314m_dataout;
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_319m_dataout <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_o(2) WHEN s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_response_accepted_312_dataout = '1'  ELSE wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_315m_dataout;
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_320m_dataout <= wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_o(1) WHEN s_wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_response_accepted_312_dataout = '1'  ELSE wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_next_pending_response_count_316m_dataout;
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_a <= ( altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_2_341q & altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_1_342q & altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_0_343q);
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_b <= ( "0" & "0" & "1");
	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313 :  oper_add
	  GENERIC MAP (
		sgate_representation => 0,
		width_a => 3,
		width_b => 3,
		width_o => 3
	  )
	  PORT MAP ( 
		a => wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_a,
		b => wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_b,
		cin => wire_gnd,
		o => wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add0_313_o
	  );
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_a <= ( altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_2_341q & altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_1_342q & altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_pending_response_count_0_343q & "1");
	wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_b <= ( "1" & "1" & "0" & "1");
	altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317 :  oper_add
	  GENERIC MAP (
		sgate_representation => 0,
		width_a => 4,
		width_b => 4,
		width_o => 4
	  )
	  PORT MAP ( 
		a => wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_a,
		b => wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_b,
		cin => wire_gnd,
		o => wire_altera_merlin_traffic_limiter_0001_altera_merlin_traffic_limiter_limiter_add1_317_o
	  );

 END RTL; --altera_merlin_traffic_limiter_0001
--synopsys translate_on
--VALID FILE
