-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_mux is
  generic (
    -- Try MUX settings
    g_dut_use_empty       : boolean := false;
    g_dut_use_in_channel  : boolean := false;
    g_dut_use_sync        : boolean := true;
    g_mode                : natural := 3;
    g_dut_nof_input       : natural := 2;  -- >= 1, when 1 then use array_init() to assign the unconstrained t_natural_arr generic arrays
    g_dut_use_fifo        : boolean := false;
    g_dut_fifo_size       : t_natural_arr := array_init(1024, 2);  -- must match g_dut_nof_input
    g_dut_fifo_fill       : t_natural_arr := array_init(   0, 2)  -- must match g_dut_nof_input
  );
end tb_dp_mux;

architecture tb of tb_dp_mux is
  -- See tb_dp_pkg.vhd for explanation and run time

  -- DUT
  constant c_dut_in_latency      : natural := 1;  -- fixed for dp_mux
  constant c_dut_out_latency     : natural := 1;  -- fixed for dp_mux

  -- The mux input streams can have a channel field (c_dp_channel_user_w) and the mux will add some LSBits to represent the
  -- input port numbers (c_dut_nof_input_w) in the mux output stream channel field. The total mux output stream channel
  -- must fit in c_dp_stream_channel_w
  constant c_dut_in_channel_w    : natural := c_dp_channel_user_w;
  constant c_dut_nof_input_w     : natural := true_log2(g_dut_nof_input);
  constant c_dut_out_channel_w   : natural := sel_a_b(g_dut_use_in_channel, c_dp_channel_user_w + c_dut_nof_input_w, c_dut_nof_input_w);
  constant c_dut_use_out_channel : boolean := true;  -- fixed for tb

  -- Stimuli
  constant c_tx_latency     : natural := c_dut_in_latency;  -- TX ready latency of TB
  constant c_tx_void        : natural := sel_a_b(c_tx_latency, 1, 0);  -- used to avoid empty range VHDL warnings when c_tx_latency=0
  constant c_tx_offset_sop  : natural := 3;
  constant c_tx_period_sop  : natural := 7;  -- sop in data valid cycle 3,  10,   17, ...
  constant c_tx_offset_eop  : natural := 9;  -- eop in data valid cycle   9,   16,   23, ...
  constant c_tx_period_eop  : natural := c_tx_period_sop;
  constant c_tx_offset_sync : natural := c_tx_offset_sop;  -- sync in data valid at every n-th sop
  constant c_tx_period_sync : natural := c_tx_period_sop * 3;
  constant c_rx_latency     : natural := c_dut_out_latency;  -- RX ready latency from DUT
  constant c_verify_en_wait : natural := 10 * g_dut_nof_input + largest(g_dut_fifo_fill);  -- wait some cycles before asserting verify enable

  constant c_empty_offset   : natural := 1;  -- dummy number to distinghuis the empty   field from the data field value
  constant c_channel_offset : natural := 2;  -- dummy number to distinghuis the channel field from the data field value

  constant c_random_w       : natural := 19;

  signal tb_end_vec     : std_logic_vector(0 to g_dut_nof_input - 1) := (others => '0');
  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '0';
  signal rst            : std_logic;
  signal sync           : std_logic;
  signal sync_dly       : std_logic_vector(0 to g_dut_nof_input - 1);
  signal lfsr1          : std_logic_vector(c_random_w - 1 downto 0) := (others => '0');
  signal lfsr2          : std_logic_vector(c_random_w   downto 0) := (others => '0');

  signal cnt_dat        : t_dp_data_arr(0 to g_dut_nof_input - 1);
  signal cnt_val        : std_logic_vector(0 to g_dut_nof_input - 1);
  signal cnt_en         : std_logic_vector(0 to g_dut_nof_input - 1);

  type t_dp_state_enum_arr is array (natural range <>) of t_dp_state_enum;

  type t_tx_data_arr_arr  is array (natural range <>) of t_dp_data_arr(   0 to c_tx_latency + c_tx_void);
  type t_tx_val_arr_arr   is array (natural range <>) of std_logic_vector(0 to c_tx_latency + c_tx_void);

  type t_prev_out_ready_arr_arr is array (natural range <>) of std_logic_vector(0 to c_rx_latency);

  signal tx_data        : t_tx_data_arr_arr(0 to g_dut_nof_input - 1) := (others => (others => (others => '0')));
  signal tx_val         : t_tx_val_arr_arr( 0 to g_dut_nof_input - 1) :=          (others => (others => '0'));

  signal sel_ctrl       : natural range 0 to g_dut_nof_input - 1 := 0;  -- used by g_mode = 2, 3

  signal in_ready       : std_logic_vector(0 to g_dut_nof_input - 1);
  signal in_data        : t_dp_data_arr(0 to g_dut_nof_input - 1) := (others => (others => '0'));
  signal in_empty       : t_dp_data_arr(0 to g_dut_nof_input - 1) := (others => (others => '0'));
  signal in_channel     : t_dp_data_arr(0 to g_dut_nof_input - 1) := (others => (others => '0'));
  signal in_sync        : std_logic_vector(0 to g_dut_nof_input - 1);
  signal in_val         : std_logic_vector(0 to g_dut_nof_input - 1);
  signal in_sop         : std_logic_vector(0 to g_dut_nof_input - 1);
  signal in_eop         : std_logic_vector(0 to g_dut_nof_input - 1);

  signal in_data_vec    : std_logic_vector(g_dut_nof_input * c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_empty_vec   : std_logic_vector(g_dut_nof_input * c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_channel_vec : std_logic_vector(g_dut_nof_input * c_dp_data_w - 1 downto 0) := (others => '0');

  signal in_siso        : t_dp_siso_arr(0 to g_dut_nof_input - 1);
  signal in_sosi        : t_dp_sosi_arr(0 to g_dut_nof_input - 1) := (others => c_dp_sosi_rst);
  signal mux_siso       : t_dp_siso := c_dp_siso_rdy;
  signal mux_sosi       : t_dp_sosi;

  signal mux_data       : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal mux_empty      : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal mux_channel    : std_logic_vector(c_dut_out_channel_w - 1 downto 0);
  signal mux_sync       : std_logic;
  signal mux_val        : std_logic;
  signal mux_sop        : std_logic;
  signal mux_eop        : std_logic;
  signal mux_ready      : std_logic;

  signal out_ready      : std_logic_vector(        0 to g_dut_nof_input - 1);
  signal prev_out_ready : t_prev_out_ready_arr_arr(0 to g_dut_nof_input - 1);

  signal out_data       : t_dp_data_arr(0 to g_dut_nof_input - 1) := (others => (others => '0'));
  signal out_empty      : t_dp_data_arr(0 to g_dut_nof_input - 1) := (others => (others => '0'));
  signal out_channel    : t_dp_data_arr(0 to g_dut_nof_input - 1) := (others => (others => '0'));
  signal out_sync       : std_logic_vector(0 to g_dut_nof_input - 1);
  signal out_val        : std_logic_vector(0 to g_dut_nof_input - 1);
  signal out_sop        : std_logic_vector(0 to g_dut_nof_input - 1);
  signal out_eop        : std_logic_vector(0 to g_dut_nof_input - 1);
  signal hold_out_sop   : std_logic_vector(0 to g_dut_nof_input - 1);
  signal prev_out_data  : t_dp_data_arr(0 to g_dut_nof_input - 1) := (others => (others => '0'));

  signal state          : t_dp_state_enum_arr(0 to g_dut_nof_input - 1);

  signal verify_en      : std_logic;
  signal verify_done    : std_logic_vector(0 to g_dut_nof_input - 1);

  signal exp_data       : std_logic_vector(c_dp_data_w - 1 downto 0) := TO_UVEC(1000, c_dp_data_w);
begin
  tb_end <= vector_and(tb_end_vec);

  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  -- Sync interval
  proc_dp_sync_interval(clk, sync);

  io : for I in 0 to g_dut_nof_input - 1 generate
    sync_dly(I) <= transport sync after clk_period * I;

    -- Input data
    cnt_val <= in_ready and cnt_en;

    proc_dp_cnt_dat(rst, clk, cnt_val(I), cnt_dat(I));
    proc_dp_tx_data(c_tx_latency, rst, clk, cnt_val(I), cnt_dat(I), tx_data(I), tx_val(I), in_data(I), in_val(I));
    proc_dp_tx_ctrl(c_tx_offset_sync, c_tx_period_sync, in_data(I), in_val(I), in_sync(I));
    proc_dp_tx_ctrl(c_tx_offset_sop, c_tx_period_sop, in_data(I), in_val(I), in_sop(I));
    proc_dp_tx_ctrl(c_tx_offset_eop, c_tx_period_eop, in_data(I), in_val(I), in_eop(I));

    in_empty(I)   <= INCR_UVEC(in_data(I), c_empty_offset);
    in_channel(I) <= INCR_UVEC(in_data(I), c_channel_offset);

    -- Input wires
    in_data_vec(   (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w) <= in_data(I);
    in_empty_vec(  (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w) <= in_empty(I);
    in_channel_vec((I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w) <= in_channel(I);

    -- Stimuli control
    proc_dp_count_en(rst, clk, sync, lfsr1, state(I), verify_done(I), tb_end_vec(I), cnt_en(I));  -- all cnt_en behave the same
    proc_dp_out_ready(rst, clk, sync, lfsr2, out_ready(I));  -- all out_ready behave the same
    --proc_dp_count_en(rst, clk, sync_dly(I), lfsr1, state(I), verify_done(I), tb_end_vec(I), cnt_en(I));  -- all cnt_en are relatively delayed
    --proc_dp_out_ready(rst, clk, sync_dly(I), lfsr2, out_ready(I));                                       -- all out_ready are relatively delayed

    -- Output demux
    out_data(I)    <= mux_data;
    out_empty(I)   <= mux_empty;
    out_val(I)     <= mux_val when c_dut_nof_input_w = 0 else  -- avoid Warning: NUMERIC_STD.TO_INTEGER: null detected, returning 0
                      mux_val when TO_UINT(mux_channel(c_dut_nof_input_w - 1 downto 0)) = I else '0';
    out_sync(I)    <= mux_sync when c_dut_nof_input_w = 0 else
                      mux_sync when TO_UINT(mux_channel(c_dut_nof_input_w - 1 downto 0)) = I else '0';
    out_sop(I)     <= mux_sop when c_dut_nof_input_w = 0 else
                      mux_sop when TO_UINT(mux_channel(c_dut_nof_input_w - 1 downto 0)) = I else '0';
    out_eop(I)     <= mux_eop when c_dut_nof_input_w = 0 else
                      mux_eop when TO_UINT(mux_channel(c_dut_nof_input_w - 1 downto 0)) = I else '0';

    -- Output verify
    proc_dp_verify_en(c_verify_en_wait, rst, clk, sync, verify_en);
    proc_dp_verify_data("mux_data", c_rx_latency, clk, verify_en, out_ready(I), out_val(I), out_data(I), prev_out_data(I));
    proc_dp_verify_valid(c_rx_latency, clk, verify_en, out_ready(I), prev_out_ready(I), out_val(I));
    proc_dp_verify_ctrl(c_tx_offset_sop, c_tx_period_sop, "sop", clk, verify_en, out_data(I), out_val(I), out_sop(I));
    proc_dp_verify_ctrl(c_tx_offset_eop, c_tx_period_eop, "eop", clk, verify_en, out_data(I), out_val(I), out_eop(I));
    proc_dp_verify_sop_and_eop(clk, out_val(I), out_sop(I), out_eop(I), hold_out_sop(I));  -- Verify that sop and eop come in pairs

    gen_verify_sync : if g_dut_use_sync = true generate
      proc_dp_verify_ctrl(c_tx_offset_sync, c_tx_period_sync, "sync", clk, verify_en, out_data(I), out_val(I), out_sync(I));
    end generate;

    gen_verify_empty : if g_dut_use_empty = true generate
      proc_dp_verify_other_sosi("empty", INCR_UVEC(out_data(I), c_empty_offset), clk, verify_en, out_empty(I));
    end generate;

    gen_verify_channel : if g_dut_use_in_channel = true generate
      out_channel(I)(c_dut_in_channel_w - 1 downto 0) <= mux_channel(c_dut_out_channel_w - 1 downto c_dut_nof_input_w);
      proc_dp_verify_other_sosi("channel", INCR_UVEC(out_data(I), c_channel_offset), clk, verify_en, out_channel(I));
    end generate;

    -- Check that the test has ran at all
    proc_dp_verify_value(e_at_least, clk, verify_done(I), exp_data, out_data(I));
  end generate;

  mux_ready <= vector_and(out_ready);

  ------------------------------------------------------------------------------
  -- DUT dp_mux
  ------------------------------------------------------------------------------

  -- map sl, slv to record
  p_in : process(in_siso, in_data_vec, in_empty_vec, in_channel_vec, in_val, in_sop, in_eop)
  begin
    for I in 0 to g_dut_nof_input - 1 loop
      in_ready(I) <= in_siso(I).ready;  -- SISO
      in_sosi(I).data(c_dp_data_w - 1 downto 0) <= in_data_vec(                (I + 1) * c_dp_data_w - 1 downto I * c_dp_data_w);  -- SOSI
      in_sosi(I).empty                        <= in_empty_vec(  c_dp_empty_w   + I * c_dp_data_w - 1 downto I * c_dp_data_w);
      in_sosi(I).channel                      <= in_channel_vec(c_dp_channel_w + I * c_dp_data_w - 1 downto I * c_dp_data_w);
      in_sosi(I).sync                         <= in_sync(I);
      in_sosi(I).valid                        <= in_val(I);
      in_sosi(I).sop                          <= in_sop(I);
      in_sosi(I).eop                          <= in_eop(I);
    end loop;
  end process;

  mux_siso.ready <= mux_ready;  -- SISO
  mux_data                           <= mux_sosi.data(c_dp_data_w - 1 downto 0);  -- SOSI
  mux_empty(c_dp_empty_w - 1 downto 0) <= mux_sosi.empty;
  mux_channel                        <= mux_sosi.channel(c_dut_out_channel_w - 1 downto 0);
  mux_sync                           <= mux_sosi.sync;
  mux_val                            <= mux_sosi.valid;
  mux_sop                            <= mux_sosi.sop;
  mux_eop                            <= mux_sosi.eop;

  p_sel_ctrl : process
  begin
    proc_common_wait_some_cycles(clk, c_dp_sync_interval / 5);
    if sel_ctrl = g_dut_nof_input - 1 then
      sel_ctrl <= 0;
    else
      sel_ctrl <= sel_ctrl + 1;
    end if;
  end process;

  dut : entity work.dp_mux
  generic map (
    g_data_w          => c_dp_data_w,
    g_empty_w         => c_dp_empty_w,
    g_in_channel_w    => c_dut_in_channel_w,
    g_error_w         => 1,
    g_use_empty       => g_dut_use_empty,
    g_use_in_channel  => g_dut_use_in_channel,
    g_use_error       => false,
    g_use_sync        => g_dut_use_sync,
    g_mode            => g_mode,
    g_nof_input       => g_dut_nof_input,
    g_use_fifo        => g_dut_use_fifo,
    g_fifo_size       => g_dut_fifo_size,
    g_fifo_fill       => g_dut_fifo_fill
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- Control
    sel_ctrl    => sel_ctrl,
    -- ST sinks
    snk_out_arr => in_siso,  -- OUT = request to upstream ST source
    snk_in_arr  => in_sosi,
    -- ST source
    src_in      => mux_siso,  -- IN  = request from downstream ST sink
    src_out     => mux_sosi
  );
end tb;
