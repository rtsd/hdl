-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: HJ Pepping              , Original cos/sin LUT in fringe_stopping_pkg.
--         E. Kooistra, 10 Apr 2017, Moved to this more common common_math_pkg.
--
-- Purpose: Useful math look up table functions
-- Description:
-- Remarks:
-- * Use MATH_2_PI instead of 2.0*MATH_PI because it is available.
-- * For SIN() and COS() first use MOD to get the input angle in range [0:2pi>,
--   to avoid XLOCAL <= 0.0 error in SIN() or COS(). This error can occur due to
--   floating point accuracy limitations when an input angle < 0 or > MATH_2_PI
--   is reduced to within the [0:2pi> range inside SIN() or COS().
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.math_real.all;
use work.common_pkg.all;
use work.common_lfsr_sequences_pkg.all;

package common_math_pkg is
  -- Function to create the cos/sin lookup table with amplitude, frequency and phase
  -- . N is number of samples in the lookup table, [0:N-1] = [0:N> = [0:2pi*FREQ>
  -- . +round(ampl) is the maximum integer value and -round(ampl) the minimum integer value
  -- . freq is the number of periods in N samples
  -- . phi is phase offset in radials
  -- . use common_math_create_look_up_table_phasor() to create a complex phasor look up table
  function common_math_create_look_up_table_cos(N : positive; AMPL, FREQ, PHI : real) return t_nat_integer_arr;
  function common_math_create_look_up_table_sin(N : positive; AMPL, FREQ, PHI : real) return t_nat_integer_arr;

  -- Function to create the cos/sin lookup table with one period and maximum amplitude
  -- . N is number of samples in the lookup table, [0:N-1] = [0:N> = [0:2pi>
  -- . ampl = 2**(w-1)-1, +ampl is maximum and -ampl is minimim integer value
  -- . freq = 1, fixed: one period in lookup table
  -- . phi  = 0, fixed: no phase offset
  function common_math_create_look_up_table_cos(N : positive; W : positive) return t_nat_integer_arr;
  function common_math_create_look_up_table_sin(N : positive; W : positive) return t_nat_integer_arr;
  -- . idem but with option to invert the waveform (phi = pi rad) or not (phi = 0 rad)
  function common_math_create_look_up_table_cos(N : positive; W : positive; INVERT : boolean) return t_nat_integer_arr;
  function common_math_create_look_up_table_sin(N : positive; W : positive; INVERT : boolean) return t_nat_integer_arr;

  -- Function to sum all values in the lookup table, to determine the DC value
  function common_math_sum_look_up_table(table : t_nat_integer_arr) return integer;

  -- Function to concat Im & Re values in the lookup table, output = (hi << W) + lo
  function common_math_concat_look_up_table(table_hi, table_lo : t_nat_integer_arr; W : positive) return t_nat_integer_arr;

  -- Function to concat Im & Re values of phasor in the lookup table, output = (imag << W) + real
  -- . N is number of samples in the lookup table, [0:N-1] = [0:N> = [0:2pi*FREQ>
  -- . +round(ampl) is the maximum integer value and -round(ampl) the minimum integer value
  --   full scale = 2**(W-1)
  --   max = full_scale - 1 = 2**(W-1)-1
  --   use AMPL <= max to avoid wrapping
  -- . freq is the number of periods in N samples
  -- . phi is phase offset in radials
  -- Phasor: exp(j*angle) = cos(angle) + j*sin(angle)
  -- A complex FFT of N points has N bins or channels: ch = -N/2:0:N/2-1.
  -- To create an FFT input phasor with frequency in the middle of a channel use FREQ = ch.
  function common_math_create_look_up_table_phasor(N, W : positive; AMPL, FREQ, PHI : real) return t_nat_integer_arr;
  function common_math_create_look_up_table_phasor(N, W : positive; AMPL, FREQ, PHI : real) return t_slv_32_arr;  -- range 0 TO N-1

  function common_math_create_random_arr(N, W : positive; seed : natural) return t_integer_arr;
end common_math_pkg;

package body common_math_pkg is
  function common_math_create_look_up_table_cos(N : positive; AMPL, FREQ, PHI : real) return t_nat_integer_arr is
    variable v_table : t_nat_integer_arr(N - 1 downto 0);
    variable v_angle : real;
  begin
    for I in 0 to N - 1 loop
      -- first use MOD to get angle in range [0:2pi>, to avoid XLOCAL <= 0.0 error in SIN() or COS()
      v_angle := (MATH_2_PI * FREQ * real(I) / real(N) + PHI) mod MATH_2_PI;
      v_table(I) := integer(ROUND(AMPL * COS(v_angle)));
    end loop;

    return v_table;
  end;

  function common_math_create_look_up_table_cos(N : positive; W : positive) return t_nat_integer_arr is
  begin
    return common_math_create_look_up_table_cos(N, real(2**(W - 1) - 1), 1.0, 0.0);
  end;

  function common_math_create_look_up_table_cos(N : positive; W : positive; INVERT : boolean) return t_nat_integer_arr is
  begin
    if INVERT = false then
      return common_math_create_look_up_table_cos(N, real(2**(W - 1) - 1), 1.0, 0.0);
    else
      return common_math_create_look_up_table_cos(N, real(2**(W - 1) - 1), 1.0, MATH_PI);
    end if;
  end;

  function common_math_create_look_up_table_sin(N : positive; AMPL, FREQ, PHI : real) return t_nat_integer_arr is
    variable v_table : t_nat_integer_arr(N - 1 downto 0);
    variable v_angle : real;
  begin
    for I in 0 to N - 1 loop
      -- first use MOD to get angle in range [0:2pi>, to avoid XLOCAL <= 0.0 error in SIN() or COS()
      v_angle := (MATH_2_PI * FREQ * real(I) / real(N) + PHI) mod MATH_2_PI;
      v_table(I) := integer(ROUND(AMPL * SIN(v_angle)));
    end loop;
    return v_table;
  end;

  function common_math_create_look_up_table_sin(N : positive; W : positive) return t_nat_integer_arr is
    variable v_table : t_nat_integer_arr(N - 1 downto 0);
  begin
    return common_math_create_look_up_table_sin(N, real(2**(W - 1) - 1), 1.0, 0.0);
  end;

  function common_math_create_look_up_table_sin(N : positive; W : positive; INVERT : boolean) return t_nat_integer_arr is
  begin
    if INVERT = false then
      return common_math_create_look_up_table_sin(N, real(2**(W - 1) - 1), 1.0, 0.0);
    else
      return common_math_create_look_up_table_sin(N, real(2**(W - 1) - 1), 1.0, MATH_PI);
    end if;
  end;

  function common_math_sum_look_up_table(table : t_nat_integer_arr) return integer is
    variable v_dc : integer := 0;
  begin
    for I in table'range loop
      v_dc := v_dc + table(I);
    end loop;
    return v_dc;
  end;

  function common_math_concat_look_up_table(table_hi, table_lo : t_nat_integer_arr; W : positive) return t_nat_integer_arr is
    variable v_table : t_nat_integer_arr(table_hi'range);
    variable v_hi    : std_logic_vector(W - 1 downto 0);
    variable v_lo    : std_logic_vector(W - 1 downto 0);
  begin
    assert W <= 16
      report "common_math_concat_look_up_table: width W*2 does not fit in 32 bit VHDL integer."
      severity FAILURE;
    for I in table_hi'range loop
      v_hi := TO_SVEC(table_hi(I), W);
      v_lo := TO_SVEC(table_lo(I), W);
      v_table(I) := TO_SINT(v_hi & v_lo);
    end loop;
    return v_table;
  end;

  function common_math_create_look_up_table_phasor(N, W : positive; AMPL, FREQ, PHI : real) return t_nat_integer_arr is
    constant c_cos_arr : t_nat_integer_arr := common_math_create_look_up_table_cos(N, AMPL, FREQ, PHI);
    constant c_sin_arr : t_nat_integer_arr := common_math_create_look_up_table_sin(N, AMPL, FREQ, PHI);
    constant c_exp_arr : t_nat_integer_arr := common_math_concat_look_up_table(c_sin_arr, c_cos_arr, W);
  begin
    return c_exp_arr;  -- Concatenated W bit sin imag part & W bit cos real part
  end;

  function common_math_create_look_up_table_phasor(N, W : positive; AMPL, FREQ, PHI : real) return t_slv_32_arr is
    constant c_exp_arr : t_nat_integer_arr := common_math_create_look_up_table_phasor(N, W, AMPL, FREQ, PHI);
    variable v_exp_arr : t_slv_32_arr(0 to N - 1);
  begin
    for I in 0 to N - 1 loop
      v_exp_arr(I) := TO_SVEC(c_exp_arr(I), 32);
    end loop;
    return v_exp_arr;
  end;

  function common_math_create_random_arr(N, W : positive; seed : natural) return t_integer_arr is
    variable v_rand_arr : t_integer_arr(0 to N - 1);
    variable v_random : std_logic_vector(W - 1 downto 0) := TO_UVEC(seed, W);
  begin
    for I in 0 to N - 1 loop
      v_random := func_common_random(v_random);
      v_rand_arr(I) := TO_SINT(v_random);
    end loop;
    return v_rand_arr;
  end;
end common_math_pkg;
