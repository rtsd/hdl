

Simulation
----------
-> Read ../../doc/README first until step 3
Modelsim instructions:

    # in Modelsim do:
    lp unb2a_test_1GbE
    mk all
    # now double click on testbench file
    as 10
    run 500us


    # while the simulation runs... in another terminal/bash session do:
    cd unb2a_test/tb/python

    # To read out the design_name, ppsh and sensors; do:
    python tc_unb2a_test.py --sim --unb 0 --fn 3 --seq INFO,PPSH,SENSORS 
    # (sensor results only show up after 1000us of simulation runtime)

    # To test the 1GbE offload; do:
    python tc_unb2a_test.py --sim --unb 0 --fn 3 --seq BGDB -s 1GbE -r 0


    # to end simulation in Modelsim do:
    quit -sim



Testing on hardware
-------------------
-> Read ../../doc/README first until step 5

# (assume that the Uniboard is --unb 1)

# To read out the design_name, ppsh and sensors; do:

python tc_unb2a_test.py --unb 1 --fn 0:3 --seq REGMAP,INFO,PPSH,SENSORS -v5




#python tc_unb2a_test.py --unb 1 --fn 3 --seq REGMAP,BGDB -v5 -s 10GBE -r 0:15 -v 3 --rep -1
#python tc_unb2a_test_ddr.py --unb 1 --fn 1 -v 5 -s I,II --rep 1 -n 10000000


