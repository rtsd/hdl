-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose : Schedule an output trigger based on the Data Path BSN[]
-- Description:
--   When the snk_in.bsn matches the scheduled_bsn then trigger_out will pulse
--   at the block sync of that block.
-- Remark:
-- . The block sync is defined by the snk_in.eop. The trigger_out is in phase
--   with this block sync.
-- . If the scheduled_bsn is in the past then no trigger_out pulse will occur.
-- . A scheduled_bsn that has not yet occured can be canceled simply by applying
--   a new scheduled_bsn value.
-- . The bsn scheduler can only handle one scheduled_bsn at a time.

entity dp_bsn_scheduler is
  generic (
    g_bsn_w       : natural := 48
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    -- MM control
    scheduled_bsn : in  std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
    -- Streaming
    snk_in        : in  t_dp_sosi;  -- only uses eop (= block sync), bsn[]
    trigger_out   : out std_logic
  );
end dp_bsn_scheduler;

architecture rtl of dp_bsn_scheduler is
  signal trigger_en     : std_logic;
  signal nxt_trigger_en : std_logic;
begin
  -- Use the block sync combinatorially, to avoid one cycle latency and have block synchronisation for the trigger_out
  trigger_out <= snk_in.eop and trigger_en;

  nxt_trigger_en <= '1' when unsigned(scheduled_bsn) = unsigned(snk_in.bsn(scheduled_bsn'range)) + 1 else '0';

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      trigger_en <= '0';
    elsif rising_edge(clk) then
      trigger_en <= nxt_trigger_en;
    end if;
  end process;
end rtl;
