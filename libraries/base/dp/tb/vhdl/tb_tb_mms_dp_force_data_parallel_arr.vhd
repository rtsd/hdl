--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author: E. Kooistra, 10 Apr 2017
-- Purpose: Multi-testbench for mms_dp_force_data_parallel_arr
-- Description:
--   Verify mms_dp_force_data_parallel_arr
-- Usage:
--   > as 4
--   > run -all

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_mms_dp_force_data_parallel_arr is
end tb_tb_mms_dp_force_data_parallel_arr;

architecture tb of tb_tb_mms_dp_force_data_parallel_arr is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
-- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;   -- always active or random stimuli valid flow control
-- g_flow_control_verify    : t_dp_flow_control_enum := e_active;   -- always active or random verify  ready flow control
-- g_nof_streams            : NATURAL := 1;     -- >= 1
-- g_dat_w                  : NATURAL := 32;    -- must be <= 32 to fit INTEGER range
-- g_force_stream           : INTEGER := 0;     -- must be < g_nof_streams, force data on this stream
-- g_force_data             : INTEGER := -1;
-- g_force_re               : INTEGER := 2;
-- g_force_im               : INTEGER := -3;
-- g_increment_data         : INTEGER := 0;
-- g_increment_re           : INTEGER := 0;
-- g_increment_im           : INTEGER := 0;
-- g_increment_on_sop       : BOOLEAN := FALSE;  -- in this tb use same generic for data, re, im
-- g_restart_on_sync        : BOOLEAN := FALSE;  -- in this tb use same generic for data, re, im
-- g_restart_on_sop         : BOOLEAN := FALSE   -- in this tb use same generic for data, re, im

  u_act_force_data_one_stream        : entity work.tb_mms_dp_force_data_parallel_arr generic map (e_active, e_active, 1, 16, 0,  -1, 2, -3,  0, 0, 0,  false, false, false);
  u_act_force_data_incr_stream       : entity work.tb_mms_dp_force_data_parallel_arr generic map (e_active, e_active, 1, 16, 0,  -1, 2, -3,  1, 1, 1,  false, false, false);
  u_act_force_data_incr_sop_stream   : entity work.tb_mms_dp_force_data_parallel_arr generic map (e_active, e_active, 1, 16, 0,  -1, 2, -3,  1, 1, 1,   true, false, false);
  u_rnd_force_data_stream0           : entity work.tb_mms_dp_force_data_parallel_arr generic map (e_random, e_random, 3, 16, 0,  -1, 2, -3,  0, 0, 0,  false, false, false);
  u_rnd_force_data_incr_stream1      : entity work.tb_mms_dp_force_data_parallel_arr generic map (e_random, e_random, 3, 16, 1,  -1, 2, -3,  1, 1, 1,  false, false, false);
  u_rnd_force_data_incr_sync_stream2 : entity work.tb_mms_dp_force_data_parallel_arr generic map (e_random, e_random, 3, 16, 2,  -1, 2, -3,  1, 1, 1,  false,  true, false);
  u_rnd_force_data_incr_sop_stream1  : entity work.tb_mms_dp_force_data_parallel_arr generic map (e_random, e_random, 3, 16, 1,  -1, 2, -3,  1, 1, 1,  false, false,  true);
end tb;
