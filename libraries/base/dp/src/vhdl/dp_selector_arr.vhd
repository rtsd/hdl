-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Selects between two input arrays using MM interface.
-- Description:
--  Output is set to pipe_sosi_arr when register is set to '1', output is set
--  to ref_sosi_arr when register is set to '0'. pipe_sosi_arr input can be
--  pipelined, this is configured with the generic "g_pipeline".
-- Remark:
--  The select register is synchronised with the sync signal in ref_sosi_arr.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library common_lib, common_mult_lib;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_selector_arr is
  generic (
    g_nof_arr     : natural := 1;
    g_pipeline    : natural := 1
  );
  port (
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;
    dp_rst                  : in  std_logic := '0';
    dp_clk                  : in  std_logic;

    -- MM interface
    reg_selector_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    reg_selector_miso       : out t_mem_miso;

    pipe_sosi_arr           : in  t_dp_sosi_arr(g_nof_arr - 1 downto 0);
    ref_sosi_arr            : in  t_dp_sosi_arr(g_nof_arr - 1 downto 0);
    out_sosi_arr            : out t_dp_sosi_arr(g_nof_arr - 1 downto 0);

    selector_en             : out std_logic
  );
end dp_selector_arr;

architecture str of dp_selector_arr is
  constant c_selector_mem_reg : t_c_mem := (c_mem_reg_rd_latency, 1, 1, 1, '0');

  signal reg_selector_en : std_logic_vector(c_selector_mem_reg.dat_w * c_selector_mem_reg.nof_dat - 1 downto 0);
  signal n_en            : std_logic;
  signal switch_select   : std_logic;

  signal pipelined_pipe_sosi_arr  : t_dp_sosi_arr(g_nof_arr - 1 downto 0);
  signal select_sosi_arr          : t_dp_sosi_arr(g_nof_arr - 1 downto 0);
begin
  selector_en <= reg_selector_en(0);

  u_mms_common_reg : entity common_lib.mms_common_reg
  generic map (
    g_mm_reg       => c_selector_mem_reg
  )
  port map (
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    reg_mosi       => reg_selector_mosi,
    reg_miso       => reg_selector_miso,

    in_reg         => reg_selector_en,
    out_reg        => reg_selector_en
  );

  n_en <= not reg_selector_en(0);

  u_common_switch : entity common_lib.common_switch
    port map (
      rst         => dp_rst,
      clk         => dp_clk,
      clken       => ref_sosi_arr(0).sync,
      switch_high => reg_selector_en(0),
      switch_low  => n_en,
      out_level   => switch_select
  );

  u_pipeline_arr : entity work.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_arr,
    g_pipeline    => g_pipeline
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    snk_in_arr   => pipe_sosi_arr,
    src_out_arr  => pipelined_pipe_sosi_arr
  );

  select_sosi_arr <= pipelined_pipe_sosi_arr when switch_select = '1' else ref_sosi_arr;

  u_pipeline_arr_out : entity work.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_arr,
    g_pipeline    => 1
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    snk_in_arr   => select_sosi_arr,
    src_out_arr  => out_sosi_arr
  );
end str;
