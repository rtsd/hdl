-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench running two tech_eth_10g with ppm offset.
-- Description:
--   The two instances of tb_ip_stratixiv_phy_xaui have a 100 ppm offset in their
--   tr_ref_clk_644.
--   The tb is self checking based on that tb_ip_stratixiv_phy_xaui is self checking
--   and both tb_ip_stratixiv_phy_xaui instances send the same and expect the same.
-- Remarks:
--   . The test fails when g_nof_10ppm /= 0 (erko, 4 dec 2014).
--   . It also appears that the serial data still has 320 ps period even when
--     g_nof_10ppm /=0 so probably the IP simulation model is not accurate enough
--     to verify ppm offsets.
-- Usage:
--   > do wave_tb_ip_stratixiv_phy_xaui_ppm.do
--   > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity tb_ip_stratixiv_phy_xaui_ppm is
  generic (
    g_sim_time   : time := 20 us;
    g_nof_10ppm  : integer := 2  -- use /= 0 to verify XO ppm offset between two devices
  );
end tb_ip_stratixiv_phy_xaui_ppm;

architecture tb of tb_ip_stratixiv_phy_xaui_ppm is
  constant tr_clk_156_period : time := 6.4 ns;
  constant tr_clk_156_10ppm  : time := 64 fs;

  signal xaui_tx_serial_0 : std_logic_vector(c_nof_xaui_lanes - 1 downto 0);
  signal xaui_tx_serial_1 : std_logic_vector(c_nof_xaui_lanes - 1 downto 0);
begin
  u_tb_ip_stratixiv_phy_xaui_0 : entity work.tb_ip_stratixiv_phy_xaui
  generic map (
    g_sim_time                => g_sim_time,
    g_tr_clk_156_period       => tr_clk_156_period,
    g_verify_link_recovery    => false,
    g_use_xaui_rx_serial_in   => true
  )
  port map (
    xaui_tx_serial_out  => xaui_tx_serial_0,
    xaui_rx_serial_in   => xaui_tx_serial_1
  );

  u_tb_ip_stratixiv_phy_xaui_1 : entity work.tb_ip_stratixiv_phy_xaui
  generic map (
    g_sim_time                => g_sim_time,
    g_tr_clk_156_period       => tr_clk_156_period + tr_clk_156_10ppm * g_nof_10ppm,
    g_verify_link_recovery    => false,
    g_use_xaui_rx_serial_in   => true
  )
  port map (
    xaui_tx_serial_out  => xaui_tx_serial_1,
    xaui_rx_serial_in   => xaui_tx_serial_0
  );
end tb;
