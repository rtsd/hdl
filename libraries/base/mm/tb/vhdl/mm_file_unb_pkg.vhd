-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use work.mm_file_pkg.all;

package mm_file_unb_pkg is
  type t_c_mmf_unb_sys is record
    nof_unb : natural;  -- Nof used UniBoard in our system [0..nof_unb-1]
    nof_fn  : natural;  -- Nof used FNs [0..nof_fn-1] per UniBoard
    nof_bn  : natural;  -- Nof used BNs [0..nof_fn-1] per UniBoard
  end record;

  constant c_mmf_unb_nof_fn          : natural := 4;
  constant c_mmf_unb_nof_bn          : natural := 4;
  constant c_mmf_unb_nof_pn          : natural := c_mmf_unb_nof_fn + c_mmf_unb_nof_bn;  -- = 8

  -- use fixed central directory to ease use of Python test case with Modelsim
  constant c_mmf_unb_file_path       : string := "$HDL_IOFILE_SIM_DIR/";

  -- create mmf file prefix that is unique per slave
  function mmf_unb_file_prefix(sys: t_c_mmf_unb_sys; node: natural) return string;
  function mmf_unb_file_prefix(             unb, node: natural; node_type: string) return string;  -- unb 0,1,..., node = 0:3 for FN or BN
  function mmf_unb_file_prefix(             unb, node: natural) return string;  -- unb 0,1,..., node = 0:7, with 0:3 for FN and 4:7 for BN
  function mmf_unb_file_prefix(tb,          unb, node: natural) return string;  -- idem, with extra index tb = 0,1,... for use with multi testbench
  function mmf_unb_file_prefix(tb, subrack, unb, node: natural) return string;  -- idem, with extra index subrack =  0,1,... to support same local unb range per subrack
end mm_file_unb_pkg;

package body mm_file_unb_pkg is
  function mmf_unb_file_prefix(sys: t_c_mmf_unb_sys; node: natural) return string is
    -- This function is used to create files for node function instances that (can) run on
    -- an FN or a BN. One generate loop can be used for all node instances, no need to
    -- use a separate FOR loop for the back nodes and the front nodes as this function
    -- determines the UniBoard index for you.
    variable v_nodes_per_board : natural := sys.nof_fn + sys.nof_bn;
    variable v_board_index     : natural := node / v_nodes_per_board;
    variable v_node_nr         : natural := node rem v_nodes_per_board;
    variable v_node_type       : string(1 to 2) := sel_a_b(v_node_nr >= sys.nof_fn, "BN", "FN");
    variable v_node_index      : natural := sel_a_b(v_node_nr >= sys.nof_fn, v_node_nr - sys.nof_fn, v_node_nr);
  begin
    return mmf_slave_prefix(c_mmf_unb_file_path, "UNB", v_board_index, v_node_type, v_node_index);
  end;

  function mmf_unb_file_prefix(unb, node: natural; node_type: string) return string is
    -- Use this function and pass the UNB and node type BN 0:3 or node type FN 0:3 index.
  begin
    return mmf_slave_prefix(c_mmf_unb_file_path, "UNB", unb, node_type, node);
  end;

  function mmf_unb_file_prefix(unb, node: natural) return string is
    -- Use this function and pass the UNB and node 0:7 index.
    constant c_node_type       : string(1 to 2) := sel_a_b(node >= c_mmf_unb_nof_fn, "BN", "FN");
    constant c_node_nr         : natural := node mod c_mmf_unb_nof_fn;  -- PN 0:3 --> FN 0:3, PN 4:7 --> BN 0:3
  begin
    return mmf_slave_prefix(c_mmf_unb_file_path, "UNB", unb, c_node_type, c_node_nr);
  end;

  function mmf_unb_file_prefix(tb, unb, node: natural) return string is
    -- Use this function and pass the UNB and node 0:7 index and a test bench index to allow file IO with multi tb.
    constant c_node_type       : string(1 to 2) := sel_a_b(node >= c_mmf_unb_nof_fn, "BN", "FN");
    constant c_node_nr         : natural := node mod c_mmf_unb_nof_fn;  -- PN 0:3 --> FN 0:3, PN 4:7 --> BN 0:3
  begin
    return mmf_slave_prefix(c_mmf_unb_file_path, "TB", tb, "UNB", unb, c_node_type, c_node_nr);
  end;

  function mmf_unb_file_prefix(tb, subrack, unb, node: natural) return string is
    -- Use this function and pass the UNB and node 0:7 index and a test bench index to allow file IO with multi subrack and multi tb.
    constant c_node_type       : string(1 to 2) := sel_a_b(node >= c_mmf_unb_nof_fn, "BN", "FN");
    constant c_node_nr         : natural := node mod c_mmf_unb_nof_fn;  -- PN 0:3 --> FN 0:3, PN 4:7 --> BN 0:3
  begin
    return mmf_slave_prefix(c_mmf_unb_file_path, "TB", tb, "SUBRACK", subrack, "UNB", unb, c_node_type, c_node_nr);
  end;
end mm_file_unb_pkg;
