#------------------------------------------------------------------------------
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

set IP_DIR   "$env(HDL_BUILD_DIR)/sim"

#vlib ./work/         ;# Assume library work already exists

vmap ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151 ./work/

vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/twentynm_xcvr_avmm.sv"                 -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/mentor/twentynm_xcvr_avmm.sv"          -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/alt_xcvr_resync.sv"                    -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/mentor/alt_xcvr_resync.sv"             -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/altera_xcvr_fpll_a10.sv"               -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/mentor/altera_xcvr_fpll_a10.sv"        -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/a10_avmm_h.sv"                         -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/alt_xcvr_native_avmm_nf.sv"            -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/alt_xcvr_pll_embedded_debug.sv"        -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/alt_xcvr_pll_avmm_csr.sv"              -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/mentor/alt_xcvr_pll_embedded_debug.sv" -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vlog -sv "$IP_DIR/../altera_xcvr_fpll_a10_151/sim/mentor/alt_xcvr_pll_avmm_csr.sv"       -work ip_arria10_e3sge3_fractional_pll_clk125_altera_xcvr_fpll_a10_151
vcom     "$IP_DIR/ip_arria10_e3sge3_fractional_pll_clk125.vhd"

