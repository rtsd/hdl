-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: Polyphase FIR filter
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pfs_ctrl is
  generic (
    g_nof_bands_w     : natural;
    g_nof_taps        : natural;
    g_nof_taps_w      : natural;
    g_taps_w          : natural
  );
  port (
    clk               : in std_logic;
    rst               : in std_logic;
    restart           : in std_logic;
    in_x              : in std_logic_vector(g_taps_w - 1 downto 0);
    in_y              : in std_logic_vector(g_taps_w - 1 downto 0);
    in_val            : in std_logic;
    in_sync           : in std_logic;
    taps_rdaddr       : out std_logic_vector(g_nof_bands_w - 1 downto 0);
    taps_wraddr       : out std_logic_vector(g_nof_bands_w - 1 downto 0);
    taps_wren         : out std_logic;
    taps_in_x         : in  std_logic_vector(g_nof_taps * g_taps_w - 1 downto 0);
    taps_in_y         : in  std_logic_vector(g_nof_taps * g_taps_w - 1 downto 0);
    taps_out_x        : out std_logic_vector(g_nof_taps * g_taps_w - 1 downto 0);
    taps_out_y        : out std_logic_vector(g_nof_taps * g_taps_w - 1 downto 0);
    out_val           : out std_logic;
    out_sync          : out std_logic
  );
end pfs_ctrl;

architecture rtl of pfs_ctrl is
  -- The number of cycles that should be waited until the result that comes out
  -- of the MAC block is the valid result. The ctrl block will generate a valid
  -- pulse.
  constant c_mem_delay   : integer := 2;
  constant c_fir_delay   : integer := 8;

  type delay_reg is array (0 to c_mem_delay) of std_logic_vector(g_taps_w - 1 downto 0);

  signal in_val_reg      : std_logic;
  signal in_x_reg        : delay_reg;
  signal in_y_reg        : delay_reg;
  signal i_taps_rdaddr   : std_logic_vector(taps_rdaddr'range);
  signal nxt_taps_rdaddr : std_logic_vector(taps_rdaddr'range);
  signal rdval           : std_logic_vector(c_fir_delay - 1 downto 0);
  signal sync_reg        : std_logic_vector(c_fir_delay downto 0);
  signal nxt_rdval       : std_logic_vector(rdval'range);
  signal i_taps_wraddr   : std_logic_vector(taps_wraddr'range);
  signal nxt_taps_wraddr : std_logic_vector(taps_wraddr'range);
begin
  -- Output signals.
  taps_rdaddr <= i_taps_rdaddr;
  taps_wraddr <= i_taps_wraddr;
  out_val <= rdval(c_fir_delay - 1);
  out_sync <= sync_reg(c_fir_delay);

  registers_proc : process (clk, rst)
  begin
    if rst = '1' then
      -- Input registers.
      in_val_reg    <= '0';
      in_x_reg      <= (others => (others => '0'));
      in_y_reg      <= (others => (others => '0'));
      -- Output registers.
      -- Internal registers.
      rdval         <= (others => '0');
      sync_reg      <= (others => '0');
      i_taps_rdaddr <= (others => '0');
      i_taps_wraddr <= (others => '0');
    elsif rising_edge(clk) then
      -- Input registers.
      in_val_reg    <= in_val;
      in_x_reg      <= in_x & in_x_reg(0 to in_x_reg'high - 1);
      in_y_reg      <= in_y & in_y_reg(0 to in_y_reg'high - 1);
      -- Output registers.
      -- Internal registers.
      rdval         <= nxt_rdval;
      sync_reg      <= sync_reg(sync_reg'high - 1 downto 0) & in_sync;
      i_taps_rdaddr <= nxt_taps_rdaddr;
      i_taps_wraddr <= nxt_taps_wraddr;
    end if;
  end process;

  read_address_gen : process (restart, i_taps_rdaddr, in_val_reg, rdval)
  begin
    nxt_taps_rdaddr <= std_logic_vector(unsigned(i_taps_rdaddr) + 1);
    if restart = '1' then
      nxt_taps_rdaddr <= (others => '0');
    elsif in_val_reg = '0' then
      nxt_taps_rdaddr <= i_taps_rdaddr;
    end if;

    nxt_rdval <= rdval(rdval'high - 1 downto 0) & '0';
    if in_val_reg = '1' then
      nxt_rdval <= rdval(rdval'high - 1 downto 0) & '1';
    end if;
  end process;

  write_control : process (restart, i_taps_wraddr, taps_in_x, taps_in_y, in_x_reg, rdval,
      in_y_reg)
  begin
    nxt_taps_wraddr <= std_logic_vector(unsigned(i_taps_wraddr) + 1);
    if restart = '1' then
      nxt_taps_wraddr <= (others => '0');
    elsif rdval(c_mem_delay - 1) = '0' then
      nxt_taps_wraddr <= i_taps_wraddr;
    end if;

    taps_out_x <= (others => '0');
    taps_out_y <= (others => '0');
    taps_wren <= '0';
    if rdval(c_mem_delay - 1) = '1' then
      taps_out_x <= taps_in_x(taps_in_x'high - g_taps_w downto 0) & in_x_reg(c_mem_delay);
      taps_out_y <= taps_in_y(taps_in_y'high - g_taps_w downto 0) & in_y_reg(c_mem_delay);
      taps_wren <= '1';
    end if;
  end process;
end rtl;
