-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Usage:
-- > as 10
-- > run -all
-- > p_verify_res should report no errors

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.aduh_dd_pkg.all;

entity tb_aduh_verify is
end tb_aduh_verify;

architecture tb of tb_aduh_verify is
  --TYPE t_c_aduh_dd_ai IS RECORD
  --  nof_sp      : NATURAL;          -- = 4
  --  nof_adu     : NATURAL;          -- = 2
  --  nof_ports   : NATURAL;          -- = 2
  --  port_w      : NATURAL;          -- = 8
  --  dd_factor   : NATURAL;          -- = 2
  --  rx_factor   : NATURAL;          -- = 2
  --  deskew      : t_c_aduh_delays;  -- = (0, 0, (OTHERS=>0), (OTHERS=>0), (OTHERS=>0), (OTHERS=>0))  -- clock: a, b, data: a, b, c, d
  --END RECORD;
  constant c_ai                  : t_c_aduh_dd_ai := c_aduh_dd_ai;  -- use defaults

  constant c_dp_factor           : natural := c_ai.rx_factor * c_ai.dd_factor;  -- = 4 = 2 * 2

  constant c_sample_period       : time := 1250 ps;  -- 800 MTps
  constant c_dp_clk_period       : time := c_sample_period * c_dp_factor;  -- 200 MHz

  constant c_dp_phs_clk_step     : time := c_dp_clk_period / 32;  -- the PLL can output clocks with phase shifts of 360/32 = 11.25 degrees
  constant c_dp_phs_clk_period   : natural := 32;  -- number of dp_clk periods per dp_phs_clk period
  constant c_nof_dp_phs_clk      : natural := 1;

  signal tb_end              : std_logic := '0';

  -- Analogue
  signal SCLK                : std_logic := '1';  -- central sample clock = 800 MHz
  signal DCLK                : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DIG_A               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_B               : std_logic_vector(c_ai.port_w - 1 downto 0);

  -- Digital streaming
  signal dp_clk              : std_logic := '1';  -- digital data path clock = 200 MHz (deser factor 4);
  signal dp_rst              : std_logic;
  signal dp_phs_cnt          : natural := 0;
  signal dp_phs_clk          : std_logic := '1';
  signal dp_phs_clk_vec      : std_logic_vector(c_nof_dp_phs_clk - 1 downto 0);

  signal aduh_sosi_arr       : t_dp_sosi_arr(0 to c_ai.nof_sp - 1);
  signal dp_val              : std_logic;
  signal dp_sosi_arr         : t_dp_sosi_arr(0 to c_ai.nof_sp - 1);

  -- MM Interface
  signal ab_locked           : std_logic;
  signal ab_stable           : std_logic;
  signal ab_stable_ack       : std_logic := '0';

  signal test_pattern_en     : std_logic;
  signal verify_ok           : std_logic;  -- verify that verify_res indicates OK when expected
  signal verify_wrong        : std_logic;  -- verify that verify_res indicates ERROR when expected

  signal a_pattern_sel       : natural range 0 to 1 := 0;  -- 0 = DI, 1 = DQ
  signal a_verify_res        : std_logic_vector(c_ai.port_w downto 0);
  signal a_verify_res_val    : std_logic;
  signal a_verify_res_ack    : std_logic;

  signal b_pattern_sel       : natural range 0 to 1 := 1;  -- 0 = DI, 1 = DQ
  signal b_verify_res        : std_logic_vector(c_ai.port_w downto 0);
  signal b_verify_res_val    : std_logic;
  signal b_verify_res_ack    : std_logic;
begin
  -----------------------------------------------------------------------------
  -- ADUH_VERIFY port A and B
  -----------------------------------------------------------------------------

  -- Stimuli:
  -- . tb_end test bench duration control
  -- . test_pattern_en
  -- . BIST restarts due to verify_res_ack
  -- . dp_val to check that verify reports error when input valid goes low
  test_pattern_en  <= '1', '0' after 5 us,   '1' after 6 us;
  dp_val           <= '1',                                     '0' after 15 us,   '1' after 16 us;
  verify_wrong     <= '0', '1' after 5.5 us, '0' after 6.5 us, '1' after 15.5 us, '0' after 16.5 us;
  verify_ok        <= '0', '1' after 1 us,   '0' after 4 us, '1' after 8 us,                   '0' after  9 us, '1' after 11 us,                   '0' after 14 us, '1' after 18 us;
  a_verify_res_ack <= '0',                   '1' after 7 us, '0' after 7 us + c_dp_clk_period, '1' after 10 us, '0' after 10 us + c_dp_clk_period, '1' after 17 us, '0' after 17 us + c_dp_clk_period;
  b_verify_res_ack <= '0',                   '1' after 7 us, '0' after 7 us + c_dp_clk_period, '1' after 10 us, '0' after 10 us + c_dp_clk_period, '1' after 17 us, '0' after 17 us + c_dp_clk_period;
  tb_end           <= '0', '1' after 20 us;

  dp_sosi_arr      <= aduh_sosi_arr when dp_val = '1' else (others => c_dp_sosi_rst);

  u_verify_a : entity work.aduh_verify
  generic map (
    g_symbol_w             => c_ai.port_w,  -- = 8, fixed
    g_nof_symbols_per_data => c_dp_factor  -- = 4, fixed, big endian in_sosi.data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
  )
  port map (
    rst            => dp_rst,
    clk            => dp_clk,

    -- ST input
    in_sosi        => dp_sosi_arr(0),

    -- Static control input (connect via MM or leave open to use default)
    pattern_sel    => a_pattern_sel,
    verify_res     => a_verify_res,
    verify_res_val => a_verify_res_val,
    verify_res_ack => a_verify_res_ack
  );

  u_verify_b : entity work.aduh_verify
  generic map (
    g_symbol_w             => c_ai.port_w,  -- = 8, fixed
    g_nof_symbols_per_data => c_dp_factor  -- = 4, fixed, big endian in_sosi.data, t0 in MSSymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
  )
  port map (
    rst            => dp_rst,
    clk            => dp_clk,

    -- ST input
    in_sosi        => dp_sosi_arr(1),

    -- Static control input (connect via MM or leave open to use default)
    pattern_sel    => b_pattern_sel,
    verify_res     => b_verify_res,
    verify_res_val => b_verify_res_val,
    verify_res_ack => b_verify_res_ack
  );

  -- Use verify_ok to check that the verify_res is valid and indicates OK when expected
  p_verify_res_ok : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if verify_ok = '1' then
        if a_verify_res_val = '0' or unsigned(a_verify_res) /= 0 then
          report "ADUH A: test pattern I error."
            severity ERROR;
        end if;
        if b_verify_res_val = '0' or unsigned(b_verify_res) /= 0 then
          report "ADUH B: test pattern Q error."
            severity ERROR;
        end if;
      end if;
    end if;
  end process;

  -- Use verify_wrong to check that the verify_res is valid and indicates ERROR when expected
  p_verify_res_wrong : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if verify_wrong = '1' then
        if a_verify_res_val = '0' or unsigned(a_verify_res) = 0 then
          report "ADUH A: test pattern I undetected error."
            severity ERROR;
        end if;
        if b_verify_res_val = '0' or unsigned(b_verify_res) = 0 then
          report "ADUH B: test pattern Q undetected error."
            severity ERROR;
        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- ADU ADC port A,B both in test pattern mode
  -----------------------------------------------------------------------------

  -- Same sample clock for all ADC
  SCLK <= (not SCLK) or tb_end after c_sample_period / 2;

  -- National ADC
  u_adc : entity work.adu_half
  port map (
    CLK             => SCLK,
    DCLK            => DCLK,
    DCLK_RST        => '0',
    DI              => DIG_A,
    DQ              => DIG_B,

    test_pattern_en => test_pattern_en
  );

  -----------------------------------------------------------------------------
  -- ADUH_DD using only port A,B
  -----------------------------------------------------------------------------

  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;

  -- Create DP phase reference clock
  p_dp_phs_clk : process(dp_clk)
  begin
    if c_dp_phs_clk_period = 1 then
      dp_phs_clk <= dp_clk;
    elsif rising_edge(dp_clk) then
      if dp_phs_cnt mod (c_dp_phs_clk_period / 2) = 0 then
        dp_phs_clk <= not dp_phs_clk;
      end if;
      if dp_phs_cnt = c_dp_phs_clk_period - 1 then
        dp_phs_cnt <= 0;
      else
        dp_phs_cnt <= dp_phs_cnt + 1;
      end if;
    end if;
  end process;

  gen_dp_phs_clk_vec : for I in c_nof_dp_phs_clk - 1 downto 0 generate
    dp_phs_clk_vec(I) <= transport dp_phs_clk after I * c_dp_phs_clk_step;
  end generate;

  u_aduh_dd : entity work.aduh_dd
  generic map (
    g_ai => c_ai
  )
  port map (
    -- LVDS Interface
    -- . g_ai.nof_sp = 4, fixed support 4 signal paths A,B,C,D, but only use and connect A,B here
    ADC_BI_A         => DIG_A,
    ADC_BI_B         => DIG_B,

    ADC_BI_A_CLK     => DCLK,

    -- MM Interface
    ab_locked        => ab_locked,
    ab_stable        => ab_stable,
    ab_stable_ack    => ab_stable_ack,

    -- DP Interface
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,
    dp_phs_clk_vec   => dp_phs_clk_vec,

    -- . Streaming
    src_out_arr      => aduh_sosi_arr
  );
end tb;
