module qsys_unb2b_minimal_reg_unb_sens #(
		parameter g_adr_w = 6,
		parameter g_dat_w = 32
	) (
		output wire [g_adr_w-1:0] coe_address_export,   //      address.export
		output wire               coe_clk_export,       //          clk.export
		input  wire [g_adr_w-1:0] avs_mem_address,      //          mem.address
		input  wire               avs_mem_write,        //             .write
		input  wire [g_dat_w-1:0] avs_mem_writedata,    //             .writedata
		input  wire               avs_mem_read,         //             .read
		output wire [g_dat_w-1:0] avs_mem_readdata,     //             .readdata
		output wire               coe_read_export,      //         read.export
		input  wire [g_dat_w-1:0] coe_readdata_export,  //     readdata.export
		output wire               coe_reset_export,     //        reset.export
		input  wire               csi_system_clk,       //       system.clk
		input  wire               csi_system_reset,     // system_reset.reset
		output wire               coe_write_export,     //        write.export
		output wire [g_dat_w-1:0] coe_writedata_export  //    writedata.export
	);
endmodule

