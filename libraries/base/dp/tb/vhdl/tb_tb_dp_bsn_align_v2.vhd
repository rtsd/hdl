-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 15 sept 2021
-- Purpose: Regression multi tb for dp_bsn_align_v2
-- Description:
-- Usage:
-- > as 3
-- > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_bsn_align_v2 is
end tb_tb_dp_bsn_align_v2;

architecture tb of tb_tb_dp_bsn_align_v2 is
  constant c_block                : natural := 11;
  constant c_period               : natural := 20;
  constant c_nof_blk              : natural := 20;  -- choose > circular buffer size

  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- DUT
  -- g_nof_streams                : NATURAL := 2;      -- number of input and output streams
  -- g_bsn_latency_max            : NATURAL := 1;      -- Maximum travel latency of a remote block in number of block periods T_blk
  -- g_bsn_latency_first_node     : natural := 1;
  -- g_nof_aligners_max           : NATURAL := 1;      -- 1 when only align at last node, > 1 when align at every intermediate node
  -- g_block_size                 : NATURAL := 11;     -- > 1, g_block_size=1 is not supported
  -- g_block_period               : NATURAL := 20;     -- >= g_block_size, = g_block_size + c_gap_size
  -- g_bsn_w                      : NATURAL := c_dp_stream_bsn_w;  -- number of bits in sosi BSN
  -- g_data_w                     : NATURAL := 16;     -- number of bits in sosi data
  -- g_data_replacement_value     : INTEGER := 0;      -- output sosi data replacement value for missing input blocks
  -- g_disable_stream_id          : NATURAL := 0;      -- default 0 to enable all streams, > 0 selects stream that will be disabled
  -- g_lost_stream_id             : NATURAL := 0;      -- default 0 to have all streams, > 0 selects stream that will be lost
  -- g_lost_bsn_id                : NATURAL := 10;     -- for stream 1 the block with bsn = g_lost_bsn_id will be lost
  -- g_bsn_init                   : NATURAL := 3;      -- use > 0 to have no lost data for g_lost_bsn_id = 0
  -- g_use_mm_output              : BOOLEAN := FALSE;  -- output via MM or via streaming DP
  -- g_pipeline_input             : NATURAL := 0;      -- >= 0, choose 0 for wires, choose 1 to ease timing closure of in_sosi_arr
  -- g_pipeline_output            : NATURAL := 0;      -- >= 0, choose 0 for wires, choose 1 to ease timing closure of out_sop_arr
  -- g_rd_latency                 : NATURAL := 2;      -- 1 or 2, choose 2 to ease timing closure
  --
  -- -- TB
  -- g_tb_diff_delay        : INTEGER := 0;       -- 0 = aligned inputs, -1 = max input delay for no loss,
  --                                              -- >~ g_bsn_latency_max * g_block_period will give loss
  -- g_tb_nof_restart       : NATURAL := 1;       -- number of times to restart the input stimuli
  -- g_tb_nof_blocks        : NATURAL := 10       -- number of input blocks per restart

  u_mm_output               : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    true,  0, 0, 1,  0, 2, c_nof_blk);
  u_mm_output_single        : entity work.tb_dp_bsn_align_v2 generic map (1, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    true,  0, 0, 1,  0, 2, c_nof_blk);
  u_output                  : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);
  u_output_single           : entity work.tb_dp_bsn_align_v2 generic map (1, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);
  u_output_pipe1            : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 1, 1, 1,  0, 2, c_nof_blk);
  u_pipe1_rdlat2            : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 1, 0, 2,  0, 2, c_nof_blk);
  u_zero_gap                : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1, c_block,  c_block, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);
  u_zero_gap_pipe1_rdlat2   : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1, c_block,  c_block, 32, 16, 17, 0, 0,  0, 3,    false, 1, 1, 2,  0, 2, c_nof_blk);
  u_stream_disable          : entity work.tb_dp_bsn_align_v2 generic map (3, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 2, 0,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);
  u_stream_lost             : entity work.tb_dp_bsn_align_v2 generic map (3, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 2,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);
  u_stream_disable_lost     : entity work.tb_dp_bsn_align_v2 generic map (4, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 1, 2,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);
  u_bsn_lost                : entity work.tb_dp_bsn_align_v2 generic map (3, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0, 10, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);
  u_diff_delay              : entity work.tb_dp_bsn_align_v2 generic map (3, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1, -1, 2, c_nof_blk);

  -- g_block_size = 2**4 = 16
  u_mm_block_pow2           : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1,      16, c_period, 32, 16, 17, 0, 0,  0, 3,    true,  0, 0, 1,  0, 2, c_nof_blk);
  u_block_pow2              : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1,      16, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);

  -- test where bsn * g_block_size > 2^10 to test address resizing
  u_mm_large_bsn            : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3000, true,  0, 0, 1,  0, 2, c_nof_blk);
  u_large_bsn               : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3000, false, 0, 0, 1,  0, 2, c_nof_blk);

  -- BSN latency
  u_bsn_lat_max_2           : entity work.tb_dp_bsn_align_v2 generic map (2,   2,   2,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);
  u_bsn_lat_max_3           : entity work.tb_dp_bsn_align_v2 generic map (2,   3,   3,  1, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1,  0, 2, c_nof_blk);

  -- chain of aligners
  u_nof_aligners_16         : entity work.tb_dp_bsn_align_v2 generic map (2,   2, c_1, 16, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1,  0, 2,       100);
  u_nof_aligners_8          : entity work.tb_dp_bsn_align_v2 generic map (2, c_1, c_1,  8, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1,  0, 2,        50);
  u_nof_aligners_diff_delay : entity work.tb_dp_bsn_align_v2 generic map (4, c_1, c_1,  3, c_block, c_period, 32, 16, 17, 0, 0,  0, 3,    false, 0, 0, 1, -1, 2,        50);
end tb;
