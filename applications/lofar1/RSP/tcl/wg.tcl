###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# Procedures for generating test cases
#
# - wg_i2bb
# - wg_i2bbbb
#
# - wg_wait_for_interrupt
#
# - wg_overwrite_rsr
# - wg_read_rsr
# - wg_write_rsr_timestamp
# - wg_read_rsr_timestamp
# - wg_write_rsr_beam_mode_settings
# - wg_read_rsr_beam_mode_settings
# - wg_write_rsr_sdo_mode_settings
# - wg_read_rsr_sdo_mode_settings
#
# - wg_write_cr_softclear
# - wg_write_cr_softsync
# - wg_write_cr_sync_on
# - wg_write_cr_sync_off
# - wg_read_cr_sync_off
# - wg_write_cr_sync_delay
# - wg_read_cr_sync_delay
#
# - wg_write_serdes_rx_delay
# - wg_write_serdes_tx_delay
# - wg_read_serdes_rx_delay
# - wg_read_serdes_tx_delay
#
# - wg_write_rsu_altsync
#
# - wg_write_rd_smbh_protocol_list
# - wg_overwrite_rd_smbh_protocol_results
#
# - wg_write_rcuh_settings
# - wg_read_rcuh_settings
# - wg_write_rcuh_settings_test
# - wg_read_rcuh_settings_test
# - wg_write_rcuh_clock_control
# - wg_read_rcuh_clock_control
# - wg_write_rcuh_control_list
# - wg_read_rcuh_control_result
#
# - wg_write_tbbi_settings
# - wg_read_tbbi_settings
# - wg_write_tbbi_bandsel
# - wg_read_tbbi_bandsel
# - wg_write_tbbi_test
# - wg_read_tbbi_test
#
# - wg_write_diag_bypass
# - wg_read_diag_bypass
# - wg_setup_diag_waveform_generator
# - wg_read_diag_waveform_generator
# - wg_write_diag_waveform
# - wg_read_diag_waveform
# - wg_read_diag_result_buffer
#
# - wg_write_bs_nof_samples_psync
# - wg_read_bs_nof_samples_psync
#
# - wg_write_ss
# - wg_read_ss
# - wg_write_sdo_ss
# - wg_read_sdo_ss
#
# - wg_write_bf
# - wg_read_bf
#
# - wg_overwrite_subband_power_statistics
# - wg_read_subband_power_statistics
# - wg_read_beamlet_power_statistics
# - wg_read_crosslet_power_statistics
#
# - wg_write_rad_settings
# - wg_read_rad_settings
# - wg_read_rad_latency
#
# - wg_write_cdo_settings
# - wg_read_cdo_settings
# - wg_write_cdo_transport_header
# - wg_read_cdo_transport_header
#
# - wg_adc
# - wg_calculate_analogue_waveform
# - wg_calculate_next_sequence_value
#
# - wg_sdo_ss_generate_map
#
# - wg_write_file
# - wg_read_file
#
# - wg_flip
# - wg_transpose
#
# - wg_clip
# - wg_wrap
# - wg_round
# - wg_truncate
#
# - wg_wait
#
###############################################################################


###############################################################################
#
# WG_I2BB : Convert list of integers into list of byte-bytes
#
# Input:
#
# - s   = List of integers
#
# Return:
#
# - ret = List of two bytes, LSByte first per pair
#
proc wg_i2bb {s} {
  set ret {}
  foreach i $s {
    lappend ret [expr $i%256] [expr ($i/256)%256]
  }
  return $ret
}


###############################################################################
#
# WG_I2BBBB : Convert list of integers into list of byte-bytes-byte-bytes
#
# Input:
#
# - s   = List of integers
#
# Return:
#
# - ret = List of four bytes, LSByte first per four
#
proc wg_i2bbbb {s} {
  set ret {}
  foreach i $s {
    lappend ret [expr $i%256] [expr ($i/256)%256] [expr ($i/(256*256))%256] [expr ($i/(256*256*256))%256]
  }
  return $ret
}


###############################################################################
#
# WG_WAIT_FOR_INTERRUPT : Wait for MEP interrupt message from board
#
# Input:
# - repeat = nof MEP timeout periods to wait for MEP interrupt
# - applev = Append log level
#
# Return: 1 when interrupt message was received, else 0.
#
#   tc appendlog messages are reported.
#
proc wg_wait_for_interrupt {{repeat 10} {applev 21}} {

  set mep_interrupt 0
  set rep           1
  while {$mep_interrupt==0} {
    puts                 "Rep-$rep: Wait for MEP interrupt"      ;# useful for simulation
    tc appendLog $applev "Rep-$rep: Wait for MEP interrupt"
    set mep_interrupt [expr ![rsp interruptMsg [msg interruptMepMsg]]]
    if {$rep >= $repeat} {
      tc appendLog 21 "Timeout for MEP interrupt, break to continue."
      tc setResult FAILED
      break
    }
    incr rep
    #after 10
  }
    
  return $mep_interrupt
}


###############################################################################
#
# WG_OVERWRITE_RSR : Overwrite the selected process fields of the RSP status register
#
# Input:
#
# - procid   = Process ID : 'rsp', 'eth', 'mep', 'diag', 'bs', 'rcuh', 'rsu', 'ado', 'rad', 'rcuh_test' or 'all'
# - value    = Byte value to use for overwrite
# - rspId    = RSP ID: 'rsp#'
#
# Return:
#
# - ret      = void
#
proc wg_overwrite_rsr {{procid "all"} {value 255} {rspId rsp0}} {

  global env
  source "$env(RSP)/rsp/tb/tc/5. Datapath/constants.tcl"

  switch $procid {
    "rsp"        {set offset $c_ei_status_rsp_offset;       set size                      $c_ei_status_rsp_size}
    "eth"        {set offset $c_ei_status_eth_offset;       set size                      $c_ei_status_eth_size}
    "mep"        {set offset $c_ei_status_mep_offset;       set size                      $c_ei_status_mep_size}
    "diag"       {set offset $c_ei_status_diag_offset;      set size                      $c_ei_status_diag_size}
    "bs"         {set offset $c_ei_status_bs_offset;        set size [expr $c_rsp_nof_blp*$c_ei_status_bs_size]}
    "rcuh"       {set offset $c_ei_status_rcuh_offset;      set size [expr $c_rsp_nof_blp*$c_ei_status_rcuh_size]}
    "rsu"        {set offset $c_ei_status_rsu_offset;       set size                      $c_ei_status_rsu_size}
    "ado"        {set offset $c_ei_status_ado_offset;       set size [expr $c_rsp_nof_blp*$c_ei_status_ado_size]}
    "rad"        {set offset $c_ei_status_rad_offset;       set size [expr $c_rad_nof_rx_status *$c_ei_status_rad_size]}
    "rcuh_test"  {set offset $c_ei_status_rcuh_test_offset; set size [expr $c_rsp_nof_blp*$c_ei_status_rcuh_test_size]}
    default      {set offset $c_ei_status_rsr_offset;       set size                      $c_ei_status_rsr_size}
  }

  set status {}
  for {set i 0} {$i < $size} {incr i} {
    lappend status [expr $value]
  }

  foreach ri $rspId {
    if {[rsp writeMsg [msg writeMepMsg "rsr status rsp \"offset $offset\"" $status] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_RSR : Read the selected process fields from the RSP status register
#
# Input:
#
# - procid   = Process ID : 'rsp', 'eth', 'mep', 'diag', 'bs', 'rcuh', 'rsu', 'ado', 'rad', 'rcuh_test' or 'all'
# - rspId    = RSP ID: 'rsp#'
# - applev   = Append log level
#
# Return:
#
# - ret      = List of read process status fields, list structure: {{first RSP} {last RSP}},
#              where the data structure per RSP depends on the procid.
# - retb     = Boolean telling wither the status is OK or ERROR
#
proc wg_read_rsr {{procid "all"} {rspId rsp0} {applev 11}} {

  global env
  source "$env(RSP)/rsp/tb/tc/5. Datapath/constants.tcl"

  set ret_rsp {}      ;# Declare return result

  # Read all RSR fields
  foreach ri $rspId {
    set ret {}
    set retb {}
    if {[rsp readMsg [msg readMepMsg "rsr status rsp \"offset $c_ei_status_rsr_offset\"" $c_ei_status_rsr_size] $ri] == 0} {
      msg setOffset 0
      # RSP status
      set rsp_volt_1v2         [msg readUnsigned 1]
      set rsp_volt_2v5         [msg readUnsigned 1]
      set rsp_volt_3v3         [msg readUnsigned 1]
      set rsp_pcb_temp         [msg readSigned   1]
      set rsp_bp_temp          [msg readSigned   1]
      set rsp_ap0_temp         [msg readSigned   1]
      set rsp_ap1_temp         [msg readSigned   1]
      set rsp_ap2_temp         [msg readSigned   1]
      set rsp_ap3_temp         [msg readSigned   1]
      set rsp_clk              [msg readUnsigned 1]
      set rsp_rsvd             [msg readUnsigned 2]
      # ETH status
      set eth_nof_frames       [msg readUnsigned 4]
      set eth_nof_errors       [msg readUnsigned 4]
      set eth_last_error       [msg readUnsigned 1]
      set eth_rsvd             [msg readUnsigned 3]
      # MEP status
      set mep_seq_nr           [msg readUnsigned 2]
      set mep_prev_error       [msg readUnsigned 1]
      set mep_rsvd             [msg readUnsigned 1]
      # DIAG status
      set diag_interface       [msg readUnsigned 1]
      set diag_mode            [msg readUnsigned 1]
      set diag_ri_errors(rsp)  [msg readUnsigned 2]
      set diag_rcux_errors     [msg readUnsigned 2]
      set diag_rcuy_errors     [msg readUnsigned 2]
      set diag_eth_errors(LCU) [msg readUnsigned 2]
      set diag_eth_errors(CEP) [msg readUnsigned 2]
      set diag_serdes_errors   [msg readUnsigned 2]
      set diag_ri_errors(0)    [msg readUnsigned 2]
      set diag_ri_errors(1)    [msg readUnsigned 2]
      set diag_ri_errors(2)    [msg readUnsigned 2]
      set diag_ri_errors(3)    [msg readUnsigned 2]
      set diag_rsvd            [msg readUnsigned 2]
      # BS status
      for {set bi 0} {$bi < $c_rsp_nof_blp} {incr bi} {
        set bs_ext_cnt($bi)    [msg readUnsigned 4]
        set bs_sync_cnt($bi)   [msg readUnsigned 4]
        set bs_sample_cnt($bi) [msg readUnsigned 4]
        set bs_slice_cnt($bi)  [msg readUnsigned 4]
      }
      # RCU status
      for {set bi 0} {$bi < $c_rsp_nof_blp} {incr bi} {
        set rcu_status($bi)    [msg readUnsigned 4]
        set rcu_nof_ovr_x($bi) [msg readUnsigned 4]
        set rcu_nof_ovr_y($bi) [msg readUnsigned 4]
      }
      # RSU status
      set rsu_cp_status  [msg readUnsigned 4]
      # ADO status
      for {set bi 0} {$bi < $c_rsp_nof_blp} {incr bi} {
        set ado_x($bi)         [msg readSigned 4]
        set ado_y($bi)         [msg readSigned 4]
      }
      # RAD BP status - RI, XB[0:3], S[0:3]
      set rad_ri               [msg readUnsigned 4]
      for {set la 0} {$la < $c_rsp_nof_lanes} {incr la} {
        set rad_lane($la,crosslets)  [msg readUnsigned 4]
        set rad_lane($la,beamlets)   [msg readUnsigned 4]
      }
      for {set la 0} {$la < $c_rsp_nof_lanes} {incr la} {
        set rad_lane($la,subbands)  [msg readUnsigned 4]
      }
      # RCU test status
      for {set bi 0} {$bi < $c_rsp_nof_blp} {incr bi} {
        set rcu_test_x($bi)    [msg readUnsigned 2]
        set rcu_test_y($bi)    [msg readUnsigned 2]
      }

      # Report RSR fields indicated by procid
      tc appendLog $applev ""
      tc appendLog $applev ">>> RSR read outs for RSP-$ri."
      tc appendLog $applev ""

      if {$procid == "rsp" || $procid == "all"} {
        tc appendLog $applev "RSP board status:"
        tc appendLog $applev ""
        tc appendLog $applev "  supply 1.2V = [format "%6.3f" [expr $rsp_volt_1v2 * 2.5/192]] V     (= [format "%3d * 2.5/192" $rsp_volt_1v2])  (unit [format "%5.3f" [expr 2.5/192]] V)"
        tc appendLog $applev "  supply 2.5V = [format "%6.3f" [expr $rsp_volt_2v5 * 3.3/192]] V     (= [format "%3d * 3.3/192" $rsp_volt_2v5])  (unit [format "%5.3f" [expr 3.3/192]] V)"
        tc appendLog $applev "  supply 3.3V = [format "%6.3f" [expr $rsp_volt_3v3 * 5.0/192]] V     (= [format "%3d * 5.0/192" $rsp_volt_3v3])  (unit [format "%5.3f" [expr 5.0/192]] V)"
        tc appendLog $applev ""
        tc appendLog $applev "  temp PCB = [format "%4d" $rsp_pcb_temp] degrees C"
        tc appendLog $applev ""
        tc appendLog $applev "  temp BP  = [format "%4d" $rsp_bp_temp] degrees C"
        tc appendLog $applev "  temp AP0 = [format "%4d" $rsp_ap0_temp] degrees C"
        tc appendLog $applev "  temp AP1 = [format "%4d" $rsp_ap1_temp] degrees C"
        tc appendLog $applev "  temp AP2 = [format "%4d" $rsp_ap2_temp] degrees C"
        tc appendLog $applev "  temp AP3 = [format "%4d" $rsp_ap3_temp] degrees C"
        tc appendLog $applev ""
        tc appendLog $applev "  clk rate = [format "%4d" $rsp_clk] MHz"
        tc appendLog $applev ""
        lappend ret "$rsp_volt_1v2 $rsp_volt_2v5 $rsp_volt_3v3 $rsp_pcb_temp $rsp_bp_temp $rsp_ap0_temp $rsp_ap1_temp $rsp_ap2_temp $rsp_ap3_temp $rsp_clk"
      }
      if {$procid == "eth" || $procid == "all"} {
        tc appendLog $applev "Ethernet status:"
        tc appendLog $applev ""
        tc appendLog $applev "  Number of frames = $eth_nof_frames"
        tc appendLog $applev "  Number of errors = $eth_nof_errors"
        if       {$eth_last_error == $c_ei_eth_err_noerr          } {tc appendLog $applev "  Last error       = OK"
        } elseif {$eth_last_error == $c_ei_eth_err_preamblevalue  } {tc appendLog $applev "  Last error       = Preamble value error"
        } elseif {$eth_last_error == $c_ei_eth_err_framedelimiter } {tc appendLog $applev "  Last error       = Frame delimiter error"
        } elseif {$eth_last_error == $c_ei_eth_err_preamblelength } {tc appendLog $applev "  Last error       = Preamble length error"
        } elseif {$eth_last_error == $c_ei_eth_err_headerlength   } {tc appendLog $applev "  Last error       = Frame header lenght error"
        } elseif {$eth_last_error == $c_ei_eth_err_crc            } {tc appendLog $applev "  Last error       = CRC error"
        } elseif {$eth_last_error == $c_ei_eth_err_oddnibblelength} {tc appendLog $applev "  Last error       = Odd nof nibbles error"
        } elseif {$eth_last_error == $c_ei_eth_err_framelength    } {tc appendLog $applev "  Last error       = Frame size error"
        } else                                                      {tc appendLog $applev "  Last error       = Illegal error code: $eth_last_error"}
        tc appendLog $applev ""
        lappend ret "$eth_nof_frames $eth_nof_errors $eth_last_error"
      }
      if {$procid == "mep" || $procid == "all"} {
        tc appendLog $applev "MEP status:"
        tc appendLog $applev ""
        tc appendLog $applev "  Sequence number           = $mep_seq_nr"
        if       {$mep_prev_error == $c_ei_mep_err_noerr   } {tc appendLog $applev "  Error status previous msg = OK"
        } elseif {$mep_prev_error == $c_ei_mep_err_type    } {tc appendLog $applev "  Error status previous msg = Unknown message type"
        } elseif {$mep_prev_error == $c_ei_mep_err_addr_blp} {tc appendLog $applev "  Error status previous msg = Illegal BLP address"
        } elseif {$mep_prev_error == $c_ei_mep_err_pid     } {tc appendLog $applev "  Error status previous msg = Invalid PID"
        } elseif {$mep_prev_error == $c_ei_mep_err_regid   } {tc appendLog $applev "  Error status previous msg = Register does not exist"
        } elseif {$mep_prev_error == $c_ei_mep_err_offset  } {tc appendLog $applev "  Error status previous msg = Offset too large"
        } elseif {$mep_prev_error == $c_ei_mep_err_size    } {tc appendLog $applev "  Error status previous msg = Message too large"
        } elseif {$mep_prev_error == $c_ei_mep_err_ringcrc } {tc appendLog $applev "  Error status previous msg = Ring CRC error"
        } elseif {$mep_prev_error == $c_ei_mep_err_timeout } {tc appendLog $applev "  Error status previous msg = Timeout"
        } else                                               {tc appendLog $applev "  Error status previous msg = Illegal error code: $mep_prev_error"}
        tc appendLog $applev ""
        lappend ret "$mep_seq_nr $mep_prev_error"
      }
      if {$procid == "diag" || $procid == "all"} {
        tc appendLog $applev "DIAG status:"
        tc appendLog $applev ""
        tc appendLog $applev "  Interface                 = $diag_interface"
        tc appendLog $applev "  Mode                      = $diag_mode"
        # - BP RI
        if {$diag_ri_errors(rsp) == $c_diag_res_ok} {
          tc appendLog $applev "  Test result RI-BP         = OK"
        } elseif {$diag_ri_errors(rsp) == $c_diag_res_none} {
          tc appendLog $applev "  Test result RI-BP         = Nothing happened"
        } elseif {$diag_ri_errors(rsp) == $c_diag_res_sync_timeout} {
          tc appendLog $applev "  Test result RI-BP         = Sync timeout"
        } elseif {$diag_ri_errors(rsp) == $c_diag_res_data_timeout} {
          tc appendLog $applev "  Test result RI-BP         = Data timeout"
        } elseif {$diag_ri_errors(rsp) == $c_diag_res_word_err} {
          tc appendLog $applev "  Test result RI-BP         = Data errors occured"
        } else {
          tc appendLog $applev "  Test result RI-BP         = Unknown status $diag_ri_errors(rsp)"
        }
        tc appendLog $applev "  Test result RCU-X         = $diag_rcux_errors"
        tc appendLog $applev "  Test result RCU-Y         = $diag_rcuy_errors"
        # - ETH
        set eth_interfaces {LCU CEP}
        foreach ei $eth_interfaces {
          if {$diag_eth_errors($ei) == $c_diag_res_ok} {
            tc appendLog $applev "  Test result $ei           = OK"
          } elseif {$diag_eth_errors($ei) == $c_diag_res_none} {
            tc appendLog $applev "  Test result $ei           = Nothing happened"
          } elseif {$diag_eth_errors($ei) == $c_diag_res_data_timeout} {
            tc appendLog $applev "  Test result $ei           = Data timeout (= frame lost)"
          } elseif {$diag_eth_errors($ei) == $c_diag_res_word_err} {
            tc appendLog $applev "  Test result $ei           = Word errors occured"
          } else {
            tc appendLog $applev "  Test result $ei           = Unknown status $diag_eth_errors($ei)"
          }
        }
        # - SERDES lanes
        set lane_mask  [expr round(pow(2,$c_rsp_nof_lanes)-1)]
        set lane_ref   [expr round(pow(2,$c_rsp_nof_lanes))]
        for {set i 0} {$i < $c_rsp_nof_lanes} {incr i} {
          set lane_errors [expr ($diag_serdes_errors >> ($i*$c_rsp_nof_lanes)) & $lane_mask]
          if {$lane_errors == $c_diag_res_ok} {
            tc appendLog $applev "  Test result SERDES lane $i = OK"
          } elseif {$lane_errors == $c_diag_res_none} {
            tc appendLog $applev "  Test result SERDES lane $i = nothing happened"
          } elseif {$lane_errors == $c_diag_res_sync_timeout} {
            tc appendLog $applev "  Test result SERDES lane $i = sync timeout"
          } elseif {$lane_errors == $c_diag_res_data_timeout} {
            tc appendLog $applev "  Test result SERDES lane $i = data timeout"
          } elseif {$lane_errors == $c_diag_res_word_err} {
            tc appendLog $applev "  Test result SERDES lane $i = word errors occured"
          } elseif {$lane_errors == $c_diag_res_illegal} {
            tc appendLog $applev "  Test result SERDES lane $i = illegal status $lane_errors"
          } else {
            tc appendLog $applev "  Test result SERDES lane $i = unknown status $lane_errors"
          }
        }
        # - APs RI
        for {set ai 0} {$ai<4} {incr ai} {
          if {$diag_ri_errors($ai) == $c_diag_res_ok} {
            tc appendLog $applev "  Test result RI-AP$ai        = OK"
          } elseif {$diag_ri_errors($ai) == $c_diag_res_none} {
            tc appendLog $applev "  Test result RI-AP$ai        = Nothing happened."
          } elseif {$diag_ri_errors($ai) == $c_diag_res_sync_timeout} {
            tc appendLog $applev "  Test result RI-AP$ai        = Sync timeout."
          } elseif {$diag_ri_errors($ai) == $c_diag_res_data_timeout} {
            tc appendLog $applev "  Test result RI-AP$ai        = Data timeout."
          } elseif {$diag_ri_errors($ai) == $c_diag_res_word_err} {
            tc appendLog $applev "  Test result RI-AP$ai        = Data errors occured."
          } else {
            tc appendLog $applev "  Test result RI-AP$ai        = Unknown status $diag_ri_errors($ai)."
          }
        }
        tc appendLog $applev ""
        lappend ret "$diag_interface $diag_mode $diag_ri_errors(rsp) $diag_rcux_errors $diag_rcuy_errors $diag_eth_errors(LCU) $diag_eth_errors(CEP) $diag_serdes_errors $diag_ri_errors(0) $diag_ri_errors(1) $diag_ri_errors(2) $diag_ri_errors(3)"
      }
      if {$procid == "bs" || $procid == "all"} {
        tc appendLog $applev "BS status:"
        tc appendLog $applev ""
        for {set bi 0} {$bi < $c_rsp_nof_blp} {incr bi} {
          set     str "BLP-$bi: "
          lappend str [format "Ext_cnt = %u, " $bs_ext_cnt($bi)]
          lappend str [format "Sync_cnt = %u, " $bs_sync_cnt($bi)]
          lappend str [format "Sample_cnt = %u, " $bs_sample_cnt($bi)]
          lappend str [format "Slice_cnt = %u." $bs_slice_cnt($bi)]
          set     str [join $str]
          tc appendLog $applev "  $str"
          lappend ret "$bs_ext_cnt($bi) $bs_sync_cnt($bi) $bs_sample_cnt($bi) $bs_slice_cnt($bi)"
        }
        tc appendLog $applev ""
      }
      if {$procid == "rcuh" || $procid == "all"} {
        tc appendLog $applev "RCU status:"
        tc appendLog $applev ""
        for {set bi 0} {$bi < $c_rsp_nof_blp} {incr bi} {
          set     str "BLP-$bi: "
          lappend str [format "status = %u, " $rcu_status($bi)]
          lappend str [format "nof_ovr_x = %u, " $rcu_nof_ovr_x($bi)]
          lappend str [format "nof_ovr_y = %u." $rcu_nof_ovr_y($bi)]
          set     str [join $str]
          tc appendLog $applev "  $str"
          lappend ret "$rcu_status($bi) $rcu_nof_ovr_x($bi) $rcu_nof_ovr_y($bi)"
        }
        tc appendLog $applev ""
      }
      if {$procid == "rsu" || $procid == "all"} {
        tc appendLog $applev "RSU status:"
        tc appendLog $applev ""
        tc appendLog $applev "  CP status = $rsu_cp_status"
        tc appendLog $applev ""
        if {(($rsu_cp_status >> $c_cp_statusRdy) & 0x1) == 1} {
          tc appendLog $applev "    \[$c_cp_statusRdy\] statusRdy       = 1 : CP is done"
        } else {
          tc appendLog $applev "    \[$c_cp_statusRdy\] statusRdy       = 0 : CP is in some intermediate state"
        }
        if {(($rsu_cp_status >> $c_cp_statusFpgaType) & 0x1) == $c_cp_bp} {
          tc appendLog $applev "    \[$c_cp_statusFpgaType\] statusFpgaType  = $c_cp_bp : Image was loaded via JTAG"
        } else {
          tc appendLog $applev "    \[$c_cp_statusFpgaType\] statusFpgaType  = [expr !$c_cp_bp] : Image was loaded from flash"
        }
        if {(($rsu_cp_status >> $c_cp_statusImageType) & 0x1) == 0} {
          tc appendLog $applev "    \[$c_cp_statusImageType\] statusImageType = 0 : Factory image is running"
        } else {
          tc appendLog $applev "    \[$c_cp_statusImageType\] statusImageType = 1 : User image is running"
        }
        if       {(($rsu_cp_status >> $c_cp_statusTrigLo) & $c_cp_trig_mask) == $c_cp_trig_ButtonRst} {
          tc appendLog $applev "  \[$c_cp_statusTrigHi:$c_cp_statusTrigLo\] statusTrig      = $c_cp_trig_ButtonRst : Reconfiguration due to button reset"
        } elseif {(($rsu_cp_status >> $c_cp_statusTrigLo) & $c_cp_trig_mask) == $c_cp_trig_TempRst} {
          tc appendLog $applev "  \[$c_cp_statusTrigHi:$c_cp_statusTrigLo\] statusTrig      = $c_cp_trig_TempRst : Reconfiguration due to over temperature"
        } elseif {(($rsu_cp_status >> $c_cp_statusTrigLo) & $c_cp_trig_mask) == $c_cp_trig_UserRst} {
          tc appendLog $applev "  \[$c_cp_statusTrigHi:$c_cp_statusTrigLo\] statusTrig      = $c_cp_trig_UserRst : Reconfiguration due to user reset"
        } elseif {(($rsu_cp_status >> $c_cp_statusTrigLo) & $c_cp_trig_mask) == $c_cp_trig_WdRst} {
          tc appendLog $applev "  \[$c_cp_statusTrigHi:$c_cp_statusTrigLo\] statusTrig      = $c_cp_trig_WdRst : Reconfiguration due to watchdog reset"
        } else {
          tc appendLog $applev "  \[$c_cp_statusTrigHi:$c_cp_statusTrigLo\] statusTrig      = [expr ($rsu_cp_status >> $c_cp_statusTrigLo) & $c_cp_trig_mask] : Unknown reconfiguration trigger"
        }
          set cp_version [expr ((($rsu_cp_status >> $c_cp_statusVersion1) & 0x1) << 1) + (($rsu_cp_status >> $c_cp_statusVersion0) & 0x1)]
          tc appendLog $applev "  \[$c_cp_statusVersion1,$c_cp_statusVersion0\] statusVersion   = $cp_version : CP version number"
        tc appendLog $applev ""
        lappend ret "$rsu_cp_status"
      }
      if {$procid == "ado" || $procid == "all"} {
        tc appendLog $applev "ADC offset:"
        tc appendLog $applev ""
        set slice_size [expr $c_cpx * $c_rsp_nof_subbands]
        for {set bi 0} {$bi < $c_rsp_nof_blp} {incr bi} {
          set nof_samples_psync [expr $bs_slice_cnt($bi) * $slice_size]
          set str "BLP-$bi, RCU-X ADC offset = "
          if {$nof_samples_psync != 0} {
            lappend str [format "%11.7f lsb    " [expr 1.0 * $ado_x($bi) * $c_ado_scale / $nof_samples_psync]]
          } else {
            lappend str "-----------"
          }
          lappend str [format "(%10d * %u / %11.0f)" $ado_x($bi) $c_ado_scale $nof_samples_psync]
          set str [join $str]
          tc appendLog $applev "  $str"

          set str "BLP-$bi, RCU-Y ADC offset = "
          if {$nof_samples_psync != 0} {
            lappend str [format "%11.7f lsb    " [expr 1.0 * $ado_y($bi) * $c_ado_scale / $nof_samples_psync]]
          } else {
            lappend str "-----------"
          }
          lappend str [format "(%10d * %u / %11.0f)" $ado_y($bi) $c_ado_scale $nof_samples_psync]
          set str [join $str]
          tc appendLog $applev "  $str"

          lappend ret "$ado_x($bi) $ado_y($bi)"
        }
        tc appendLog $applev ""
      }
      if {$procid == "rad" || $procid == "all"} {
        tc appendLog $applev "RAD BP frame rx status:"
        tc appendLog $applev ""
        tc appendLog $applev "                     Align  Sync   CRC    Frame cnt"
        set retb $c_rsr_undefined              ;# default undefined
        set str "RI               : "
        set cnt [expr $rad_ri & ((1<<18)-1)]
        if {$cnt==0} {
          append str "-      -      -      "
        } else {
          set retb $c_rsr_ok                   ;# when rad is busy there is always input from RI, from lane only in case of multiple RSP
                                       append str "-      "                                                      ;# not applicable for RI
          if {($rad_ri & (1<<19))!=0} {append str "OK     "} else {append str "Error  "; set retb $c_rsr_error}  ;# sync error(s)
          if {($rad_ri & (1<<18))==0} {append str "OK     "} else {append str "Error  "; set retb $c_rsr_error}  ;# CRC error(s)
        }
        append str [format "%u" $cnt]
        tc appendLog $applev "  $str"
        lappend ret "$rad_ri"
        foreach let {crosslets beamlets subbands} {
          for {set la 0} {$la < $c_rsp_nof_lanes} {incr la} {
            set str [format "Lane-$la, %-9s: " $let]
            set cnt [expr $rad_lane($la,$let) & ((1<<18)-1)]
            if {$cnt==0} {
              append str "-      -      -      "
            } else {
                                              ;# input from lane can keep retb as set by input from RI, or cause retb to indicate error
              if {($rad_lane($la,$let) & (1<<20))==0} {append str "OK     "} else {append str "Error  "; set retb $c_rsr_error}  ;# frame(s) discarded
              if {($rad_lane($la,$let) & (1<<19))!=0} {append str "OK     "} else {append str "Error  "; set retb $c_rsr_error}  ;# sync error(s)
              if {($rad_lane($la,$let) & (1<<18))==0} {append str "OK     "} else {append str "Error  "; set retb $c_rsr_error}  ;# CRC error(s)
            }
            append str [format "%u" $cnt]
            tc appendLog $applev "  $str"
            lappend ret "$rad_lane($la,$let)"
          }
        }
        tc appendLog $applev ""
        set ret "$retb $ret"
      }
      if {$procid == "rcuh_test" || $procid == "all"} {
        tc appendLog $applev "RCU test status:"
        tc appendLog $applev ""
        for {set bi 0} {$bi < $c_rsp_nof_blp} {incr bi} {
          set     str "BLP-$bi: "
          lappend str [format "rcu_test_x = 0x%x, " $rcu_test_x($bi)]
          lappend str [format "rcu_test_y = 0x%x"   $rcu_test_y($bi)]
          set     str [join $str]
          tc appendLog $applev "  $str"
          lappend ret "$rcu_test_x($bi) $rcu_test_y($bi)"
        }
        tc appendLog $applev ""
      }
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
    lappend ret_rsp "$ret"
  }

  return $ret_rsp
}


###############################################################################
#
# WG_WRITE_RSR_TIMESTAMP : Write RSR timestamp
#
# Input:
#
# - timestamp          = Timestamp 32 bit >= 0
# - timestamp_mode     = Timestamp update at sync:
#                        0 = no change
#                        1 = reset to 0xFFFFFFFF = -1
#                        2 = auto increment
# - blpId              = BP, AP ID: 'rsp blp#'
# - rspId              = RSP ID: 'rsp#'
# - applev             = Append log level
#
# Return: void
#
proc wg_write_rsr_timestamp {timestamp {timestamp_mode 1} {blpId blp0} {rspId rsp0} {applev 41}} {
  if {$timestamp_mode==1} {
    tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, write RSR timestamp: $timestamp (mode: reset to 0xFFFFFFFF at sync)"
  } elseif {$timestamp_mode==2} {
    tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, write RSR timestamp: $timestamp (mode: auto increment at sync)"
  } else {
    tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, write RSR timestamp: $timestamp (mode: no change at sync)"
  }
  foreach ri $rspId {
    # Make use of MEP broadcast to BP and all BLP on an RSP board
    if {[rsp writeMsg [msg writeMepMsg "rsr timestamp $blpId" "[wg_i2bbbb $timestamp] $timestamp_mode"] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_RSR_TIMESTAMP : RSR read timestamp
#
# Input:
#
# - blpId    = BP, AP ID: 'rsp blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - timestamp = RSR timestamp value
#
#   tc appendlog messages are reported.
#
proc wg_read_rsr_timestamp {{blpId blp0} {rspId rsp0} {applev 21}} {
  set timestamp {}
  if {[rsp readMsg [msg readMepMsg "rsr timestamp $blpId" 5] $rspId] == 0} {
    msg setOffset 0
    set timestamp [msg readUnsigned 4]
    set timestamp_mode [msg readUnsigned 1]
    if {$timestamp_mode==1} {
      tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, read RSR timestamp: $timestamp (mode: reset to 0xFFFFFFFF at sync)"
    } elseif {$timestamp_mode==2} {
      tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, read RSR timestamp: $timestamp (mode: auto increment at sync)"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, read RSR timestamp: $timestamp (mode: no change at sync)"
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $timestamp
}


###############################################################################
#
# WG_WRITE_RSR_BEAM_MODE_SETTINGS : Write RSR beam mode settings
#
# Input:
#
# - bm_select          = Select beam mode 0, 1 or 2
# - blpId              = BP, AP ID: 'rsp blp#'
# - rspId              = RSP ID: 'rsp#'
# - applev             = Append log level
#
# Return: void
#
proc wg_write_rsr_beam_mode_settings {bm_select {blpId blp0} {rspId rsp0} {applev 41}} {
  tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, write RSR beam mode settings:"
  tc appendLog $applev "      Beam mode select  = $bm_select"
  foreach ri $rspId {
    # Make use of MEP broadcast to BP and all BLP on an RSP board
    if {[rsp writeMsg [msg writeMepMsg "rsr beammode $blpId \"offset 1\"" $bm_select] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_RSR_BEAM_MODE_SETTINGS : Read RSR beam mode settings
#
# Input:
#
# - blpId    = BP, AP ID: 'rsp blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - bm_max      = Maximum beam mode 0, 1 or 2 that is supported in the hardware
# - bm_select   = Select beam mode 0, 1 or 2
#
#   tc appendlog messages are reported.
#
proc wg_read_rsr_beam_mode_settings {{blpId blp0} {rspId rsp0} {applev 21}} {
  set settings {}
  if {[rsp readMsg [msg readMepMsg "rsr beammode $blpId" 2] $rspId] == 0} {
    msg setOffset 0
    set bm_max    [msg readUnsigned 1]
    set bm_select [msg readUnsigned 1]
    tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, read RSR beam mode settings:"
    tc appendLog $applev "      Beam mode max (read only) = $bm_max"
    tc appendLog $applev "      Beam mode select          = $bm_select"
    lappend settings $bm_max
    lappend settings $bm_select
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $settings
}


###############################################################################
#
# WG_WRITE_RSR_SDO_MODE_SETTINGS : Write RSR Subband Data Output mode settings
#
# Input:
#
# - sdo_mode           = Select SDO mode 0, 1, 2 or 3
# - blpId              = BP, AP ID: 'rsp blp#'
# - rspId              = RSP ID: 'rsp#'
# - applev             = Append log level
#
# Return: void
#
proc wg_write_rsr_sdo_mode_settings {sdo_mode {blpId blp0} {rspId rsp0} {applev 41}} {
  tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, write RSR sdo mode settings:"
  tc appendLog $applev "      SDO mode select  = $sdo_mode"
  foreach ri $rspId {
    # Make use of MEP broadcast to BP and all BLP on an RSP board
    if {[rsp writeMsg [msg writeMepMsg "rsr sdomode $blpId \"offset 1\"" $sdo_mode] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_RSR_SDO_MODE_SETTINGS : Read RSR Subband Data Output mode settings
#
# Input:
#
# - blpId    = BP, AP ID: 'rsp blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - sdo_max    = Maximum SDO mode 0, 1, 2 or 3 that is supported in the hardware
# - sdo_mode   = Select SDO mode 0, 1, 2 or 3
#
#   tc appendlog messages are reported.
#
proc wg_read_rsr_sdo_mode_settings {{blpId blp0} {rspId rsp0} {applev 21}} {
  set settings {}
  if {[rsp readMsg [msg readMepMsg "rsr sdomode $blpId" 2] $rspId] == 0} {
    msg setOffset 0
    set sdo_max  [msg readUnsigned 1]
    set sdo_mode [msg readUnsigned 1]
    tc appendLog $applev ">>> RSP-$rspId, FPGA-$blpId, read RSR sdo mode settings:"
    tc appendLog $applev "      SDO mode max (read only) = $sdo_max"
    tc appendLog $applev "      SDO mode select          = $sdo_mode"
    lappend settings $sdo_max
    lappend settings $sdo_mode
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $settings
}

###############################################################################
#
# WG_WRITE_CR_SOFTCLEAR : CR apply softclear
#
# Input:
#
# - blpId    = BP, AP ID: 'rsp blp#'
# - rspId    = RSP ID: 'rsp#'
#
# Return: void
#
# - The data value is don't care, the write event itself causes the softclear.
#
proc wg_write_cr_softclear {{blpId blp0} {rspId rsp0}} {
  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    set bi $blpId
#     foreach bi $blpId {
      if {[rsp writeMsg [msg writeMepMsg "cr softrst $bi" 1] $ri] == 0} {
        tc setResult PASSED
      } else {
        tc setResult FAILED
      }
#     }
  }
}


###############################################################################
#
# WG_WRITE_CR_SOFTSYNC : CR apply softsync
#
# Input:
#
# - blpId    = BP, AP ID: 'rsp blp#'
# - rspId    = RSP ID: 'rsp#'
#
# Return: void
#
# - The data value is don't care, the write event itself causes the softsync.
#
proc wg_write_cr_softsync {{blpId blp0} {rspId rsp0}} {
  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    set bi $blpId
#     foreach bi $blpId {
      if {[rsp writeMsg [msg writeMepMsg "cr softsync $bi" 0] $ri] == 0} {
        tc setResult PASSED
      } else {
        tc setResult FAILED
      }
#     }
  }
}


###############################################################################
#
# WG_WRITE_CR_SYNC_OFF : CR disable external sync
# WG_WRITE_CR_SYNC_ON  : CR enable external sync
#
# Input:
#
# - blpId    = BP, AP ID: 'rsp blp#'
# - rspId    = RSP ID: 'rsp#'
#
# Return: void
#
proc wg_write_cr_sync_off {{blpId blp0} {rspId rsp0}} {
  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    set bi $blpId
#     foreach bi $blpId {
      if {[rsp writeMsg [msg writeMepMsg "cr syncoff $bi" 1] $ri] == 0} {
        tc setResult PASSED
      } else {
        tc setResult FAILED
      }
#     }
  }
}

proc wg_write_cr_sync_on {{blpId blp0} {rspId rsp0}} {
  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    set bi $blpId
#     foreach bi $blpId {
      if {[rsp writeMsg [msg writeMepMsg "cr syncoff $bi" 0] $ri] == 0} {
        tc setResult PASSED
      } else {
        tc setResult FAILED
      }
#     }
  }
}


###############################################################################
#
# WG_READ_CR_SYNC_OFF : CR read sync off bit
#
# Input:
#
# - blpId    = BP, AP ID: 'rsp blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - syncoff  = External sync disable bit
#
#   tc appendlog messages are reported.
#
proc wg_read_cr_sync_off {{blpId blp0} {rspId rsp0} {applev 21}} {
  set syncoff -1
  if {[rsp readMsg [msg readMepMsg "cr syncoff $blpId" 1] $rspId] == 0} {
    msg setOffset 0
    set syncoff [msg readUnsigned 1]
    if {$syncoff==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, read CR syncoff : $syncoff (= enabled)"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, read CR syncoff : $syncoff (= disabled)"
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $syncoff
}


###############################################################################
#
# WG_WRITE_CR_SYNC_DELAY : CR write external sync input delay
#
# Input:
#
# - syncdelay = 0 is reset input delay to hardware default, > 0 increment input delay one time
# - syncedge  = 0 is capture ext_sync on rising edge, != 0 is on falling edge
# - blpId     = BP, AP ID: 'rsp blp#'
# - rspId     = RSP ID: 'rsp#'
#
# Return: void
#
proc wg_write_cr_sync_delay {{syncdelay 0} {syncedge 0} {blpId blp0} {rspId rsp0}} {
  set bit0 [expr $syncdelay > 0]
  set bit1 [expr $syncedge  > 0]
  set syncdata [expr ($bit1 << 1) + $bit0]
  foreach ri $rspId {
    # Make use of MEP broadcast to BP and all BLP on an RSP board
    set bi $blpId
#     foreach bi $blpId {
      if {[rsp writeMsg [msg writeMepMsg "cr syncdelay $bi" $syncdata] $ri] == 0} {
        tc setResult PASSED
      } else {
        tc setResult FAILED
      }
#     }
  }
}


###############################################################################
#
# WG_READ_CR_SYNC_DELAY : CR read sync delay bit
#
# Input:
#
# - blpId    = BP, AP ID: 'rsp blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - syncdata  = External sync data
#               . bit 0 : '0' is default input delay, '1' is incremented delay
#               . bit 1 : '0' capture external sync on rising edge, '1' on falling edge
#
#   tc appendlog messages are reported.
#
proc wg_read_cr_sync_delay {{blpId blp0} {rspId rsp0} {applev 21}} {
  set syncdata -1
  if {[rsp readMsg [msg readMepMsg "cr syncdelay $blpId" 1] $rspId] == 0} {
    msg setOffset 0
    set syncdata [msg readUnsigned 1]
    set bit0 [expr  $syncdata & 1      ]
    set bit1 [expr ($syncdata & 2) >> 1]
    if {$bit0==0 && $bit1==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, read CR sync: default input delay,     default capture on rising edge"
    } elseif {$bit0==0 && $bit1!=0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, read CR sync: default input delay,     capture on falling edge"
    } elseif {$bit0!=0 && $bit1==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, read CR sync: incremented input delay, default capture on rising edge"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, read CR sync: incremented input delay, capture on falling edge"
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $syncdata
}


###############################################################################
#
# WG_WRITE_SERDES_RX_DELAY : SERDES write RX_CLK output delay
#
# Input:
#
# - clk_delay  = Rx clock input delay
#               . bit 0 : '0' is reset to default delay, '1' is increment delay
# - rspId     = RSP ID: 'rsp#'
#
# Return: void
#
proc wg_write_serdes_rx_delay {{clk_delay 0} {rspId rsp0}} {
  set bit0 [expr $clk_delay & 1]
  foreach ri $rspId {
    if {[rsp writeMsg [msg writeMepMsg "serdes rxdelay rsp" $bit0] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_WRITE_SERDES_TX_DELAY : SERDES write TX_CLK output delay
#
# Input:
#
# - clk_delay  = Tx clock output delay
#               . bit 0 : '0' is reset to default delay, '1' is increment delay
# - rspId     = RSP ID: 'rsp#'
#
# Return: void
#
proc wg_write_serdes_tx_delay {{clk_delay 0} {rspId rsp0}} {
  set bit0 [expr $clk_delay & 1]
  foreach ri $rspId {
    if {[rsp writeMsg [msg writeMepMsg "serdes txdelay rsp" $bit0] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_SERDES_RX_DELAY : SERDES read RX_CLK input delay
#
# Input:
#
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - clk_delay  = Rx clock input delay
#                . bit 0 : '0' is default input delay, '1' is incremented delay
#
#   tc appendlog messages are reported.
#
proc wg_read_serdes_rx_delay {{rspId rsp0} {applev 21}} {
  set clk_delay -1
  if {[rsp readMsg [msg readMepMsg "serdes rxdelay rsp" 1] $rspId] == 0} {
    msg setOffset 0
    set clk_delay [msg readUnsigned 1]
    set bit0 [expr $clk_delay & 1]
    if {$bit0==0} {
      tc appendLog $applev ">>> RSP-$rspId, read SERDES rx_clk : default input delay"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, read SERDES rx_clk : incremented input delay"
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $clk_delay
}


###############################################################################
#
# WG_READ_SERDES_TX_DELAY : SERDES read TX_CLK output delay
#
# Input:
#
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - clk_delay  = Tx clock output delay
#                . bit 0 : '0' is default delay, '1' is incremented delay
#
#   tc appendlog messages are reported.
#
proc wg_read_serdes_tx_delay {{rspId rsp0} {applev 21}} {
  set clk_delay -1
  if {[rsp readMsg [msg readMepMsg "serdes txdelay rsp" 1] $rspId] == 0} {
    msg setOffset 0
    set clk_delay [msg readUnsigned 1]
    set bit0 [expr $clk_delay & 1]
    if {$bit0==0} {
      tc appendLog $applev ">>> RSP-$rspId, read SERDES tx_clk : default output delay"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, read SERDES tx_clk : incremented output delay"
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $clk_delay
}


###############################################################################
#
# WG_WRITE_RSU_ALTSYNC : RSU apply altsync
#
# Input:
#
# - rspId = RSP ID: 'rsp#'
#
# Return: void
#
proc wg_write_rsu_altsync {{rspId rsp0}} {
  foreach ri $rspId {
    if {[rsp writeMsg [msg writeMepMsg "rsu sysctrl rsp" 1] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_WRITE_RD_SMBH_PROTOCOL_LIST : Write and readback protocol list to SMBus handler in RSP. The list will take effect at next sync
#
# Input:
#
# - smbh          = SMBus handler: 'smbh' or 'rcuh'
# - protocol_list = Protocol list
# - polId         = Polarization ID: x, y or "x y"
# - blpId         = BLP ID: 'blp#'
# - rspId         = RSP ID: 'rsp#'
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_write_rd_smbh_protocol_list {smbh protocol_list {polId {}} {blpId blp0} {rspId rsp0}} {
  if {$smbh == "tdsh"} {
    # Force blpId and polId to single element
    set polId z     ;# dummy
    set blpId rsp   ;# rsp = bp
  }

  # - Write the protocol list to the SMBH
  smb_write_protocol_list $smbh $protocol_list $polId $blpId $rspId

  # - Read back the protocol list to verify that this is possible
  foreach ri $rspId {
    foreach bi $blpId {
      foreach pi $polId {
        set rb_protocol_list [smb_readback_protocol_list $smbh [llength $protocol_list] $pi $bi $ri]
        if {[string equal $protocol_list $rb_protocol_list] == 1} {
          if {$smbh == "rcuh"} {
            tc appendLog 21 ">>> RSP-$ri, BLP-$bi, $smbh-$pi: The protocol list READBACK went OK"
          } else {
            tc appendLog 21 ">>> RSP-$ri, BLP-$bi, $smbh  : The protocol list READBACK went OK"
          }
          tc setResult PASSED
        } else {
          if {$smbh == "rcuh"} {
            tc appendLog 11 ">>> RSP-$ri, BLP-$bi, $smbh-$pi: The protocol list READBACK went wrong:"
          } else {
            tc appendLog 11 ">>> RSP-$ri, BLP-$bi, $smbh  : The protocol list READBACK went wrong:"
          }
          tc appendLog 11 "Expected protocol list: $protocol_list"
          tc appendLog 11 "Readback protocol list: $rb_protocol_list"
          tc setResult FAILED
        }
      }
    }
  }
}


###############################################################################
#
# WG_OVERWRITE_RD_SMBH_PROTOCOL_RESULTS : Overwrite and read protocol result of SMBus handler in RSP.
#
# Input:
#
# - smbh          = SMBus handler: 'smbh' or 'rcuh'
# - polId         = Polarization ID: x, y or "x y"
# - blpId         = BLP ID: 'blp#'
# - rspId         = RSP ID: 'rsp#'
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_overwrite_rd_smbh_protocol_results {smbh {polId {}} {blpId blp0} {rspId rsp0}} {
  global env
  source "$env(RSP)/rsp/tb/tc/5. Datapath/constants.tcl"
  
  set nof_result_bytes [expr round(pow(2,$c_rcuh_result_adr_w))]
  if {$smbh == "tdsh"} {
    set nof_result_bytes [expr round(pow(2,$c_tdsh_result_adr_w))]
    # Force blpId and polId to single element
    set polId z     ;# dummy
    set blpId rsp   ;# rsp = bp
  }

  # - Overwrite first entries of protocol result register to be sure that the results will be fresh
  set wr_result {}
  for {set i 1} {$i <= $nof_result_bytes} {incr i} {
    lappend wr_result 17      ;# Just some number not equal to 0, 1, 255 and < 256
  }

  # - Overwrite
  smb_overwrite_results $smbh $wr_result $polId $blpId $rspId

  # - Readback to verify overwrite
  foreach ri $rspId {
    foreach bi $blpId {
      foreach pi $polId {
        # Read the protocol result from the I2C handler
        set rd_result [smb_read_results $smbh $nof_result_bytes $pi $bi $ri]
        if {[string equal $wr_result $rd_result] == 1} {
          if {$smbh == "rcuh"} {
            tc appendLog 21 ">>> RSP-$ri, BLP-$bi, $smbh-$pi: The protocol results OVERWRITE and read went OK"
          } else {
            tc appendLog 21 ">>> RSP-$ri, BLP-$bi, $smbh  : The protocol results OVERWRITE and read went OK"
          }
          tc setResult PASSED
        } else {
          if {$smbh == "rcuh"} {
            tc appendLog 11 ">>> RSP-$ri, BLP-$bi, $smbh-$pi: The protocol results OVERWRITE and read went wrong:"
          } else {
            tc appendLog 11 ">>> RSP-$ri, BLP-$bi, $smbh  : The protocol results OVERWRITE and read went wrong:"
          }
          tc appendLog 11 "Expected protocol result: $wr_result"
          tc appendLog 11 "Readback protocol result: $rd_result"
          tc setResult FAILED
        }
      }
    }
  }
}


###############################################################################
#
# WG_WRITE_RCUH_SETTINGS : Write RCUH settings to register in RSP. The settings will take effect immediately
#
# Input:
#
# - input_en_x     = Input enable for x, 1 is RCU input enable, 0 is RCU input disable (powerup default)
# - input_en_y     = Input enable for y, 1 is RCU input enable, 0 is RCU input disable (powerup default)
# - input_delay_x  = Input sample delay depth for x
# - input_delay_y  = Input sample delay depth for y
# - blpId          = BLP ID: 'blp#'
# - rspId          = RSP ID: 'rsp#'
# - applev         = Append log level
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_write_rcuh_settings {input_en_x input_en_y input_delay_x input_delay_y {blpId blp0} {rspId rsp0} {applev 41}} {
  # Assemble settings bytes from input arguments
  set inp_x [expr $input_delay_x & 0x7F]
  set inp_x [expr $inp_x | (($input_en_x != 0)<<7)]
  set inp_y [expr $input_delay_y & 0x7F]
  set inp_y [expr $inp_y | (($input_en_y != 0)<<7)]
  
  set test_data 0
  
  set inp_x_hi [expr ($input_delay_x & 0x7F80) >> 7]
  set inp_y_hi [expr ($input_delay_y & 0x7F80) >> 7]
  
  tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-x write: input_en = [expr ($inp_x & 0x80)>0], input_delay = [expr ($inp_x_hi << 7) + ($inp_x & 0x7F)]"
  tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-y write: input_en = [expr ($inp_y & 0x80)>0], input_delay = [expr ($inp_y_hi << 7) + ($inp_y & 0x7F)]"

  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    if {[rsp writeMsg [msg writeMepMsg "rcuh settings $blpId" "$inp_x $inp_y $test_data $inp_x_hi $inp_y_hi"] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_RCUH_SETTINGS : Read RCUH settings from register in RSP.
#
# Input:
#
# - blpId  = BLP ID: 'blp#'   -- only one
# - rspId  = RSP ID: 'rsp#'   -- only one
# - applev   = Append log level
#
# Report:
#
#   tc appendlog messages are reported.
#   . input_en_x     = 1 is RCU X input enable, 0 is RCU input disable (powerup default)
#   . input_delay_x  = Input sample delay depth for RCU X
#   . input_en_y     = idem RCU Y
#   . input_delay_y  = idem RCU Y
#
#   tc setResult is set
#
proc wg_read_rcuh_settings {{blpId blp0} {rspId rsp0} {applev 21} } {
  set settings {-1 -1 -1 -1}
  if {[rsp readMsg [msg readMepMsg "rcuh settings $blpId" 5] $rspId] == 0} {
    msg setOffset 0
    set inp_x     [msg readUnsigned 1]
    set inp_y     [msg readUnsigned 1]
    set test_data [msg readUnsigned 1]
    set inp_x_hi  [msg readUnsigned 1]
    set inp_y_hi  [msg readUnsigned 1]
    set input_en_x    [expr ($inp_x & 0x80)>0]
    set input_en_y    [expr ($inp_y & 0x80)>0]
    set input_delay_x [expr ($inp_x_hi << 7) + ($inp_x & 0x7F)]
    set input_delay_y [expr ($inp_y_hi << 7) + ($inp_y & 0x7F)]
    set settings "$input_en_x $input_delay_x $input_en_y $input_delay_y"
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-x read: input_en = $input_en_x, input_delay = $input_delay_x"
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-y read: input_en = $input_en_y, input_delay = $input_delay_y"
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $settings
}


###############################################################################
#
# WG_WRITE_RCUH_SETTINGS_TEST : Write RCUH test data settings to register in RSP.
#
# Input:
#
# - sel_x    = Test data for x, 1 is COUNTER data, 0 is PRSG data (powerup default)
# - sel_y    = Test data for y, 1 is COUNTER data, 0 is PRSG data (powerup default)
# - en_x     = '0' disable x reference test data (powerup default), '1' enable x reference test data
# - en_y     = '0' disable y reference test data (powerup default), '1' enable y reference test data
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - applev   = Append log level
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_write_rcuh_settings_test {en_x en_y sel_x sel_y {blpId blp0} {rspId rsp0} {applev 41}} {
  # Assemble settings word from input arguments
  set en_x  [expr $en_x & 0x1]
  set en_y  [expr $en_y & 0x1]
  set sel_x [expr $sel_x & 0x1]
  set sel_y [expr $sel_y & 0x1]
  
  set testdata [expr $sel_x + ($sel_y<<1) + ($en_x<<2) + ($en_y<<3)]

  if {$en_x==0} {
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-x disable reference test data"
  } else {
    if {$sel_x==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-x enable PRSG reference test data"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-x enable COUNTER reference test data"
    }
  }
  if {$en_y==0} {
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-y disable reference test data"
  } else {
    if {$sel_y==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-y enable PRSG reference test data"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-y enable COUNTER reference test data"
    }
  }

  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    if {[rsp writeMsg [msg writeMepMsg "rcuh settings $blpId \"offset 2\"" $testdata] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_RCUH_SETTINGS_TEST : Read RCUH test data settings from register in RSP.
#
# Input:
#
# - blpId  = BLP ID: 'blp#'   -- only one
# - rspId  = RSP ID: 'rsp#'   -- only one
# - applev = Append log level
#
# Report:
#
#   tc appendlog messages are reported.
#   . sel_x = Test data for x, 1 is COUNTER data, 0 is PRSG data (powerup default)
#   . sel_y = Test data for y, 1 is COUNTER data, 0 is PRSG data (powerup default)
#   . en_x  = '0' disabled x reference test data (powerup default), '1' enabled x reference test data
#   . en_y  = '0' disabled y reference test data (powerup default), '1' enabled y reference test data
#
#   tc setResult is set
#
proc wg_read_rcuh_settings_test {{blpId blp0} {rspId rsp0} {applev 21} } {
  set testdata -1
  if {[rsp readMsg [msg readMepMsg "rcuh settings $blpId \"offset 2\"" 1] $rspId] == 0} {
    msg setOffset 0
    set testdata [msg readUnsigned 1]
    set sel_x [expr $testdata & 0x1]
    set sel_y [expr $testdata & 0x2]
    set en_x  [expr $testdata & 0x4]
    set en_y  [expr $testdata & 0x8]
    if {$en_x==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-x reference test data read: Disabled"
    } else {
      if {$sel_x==0} {
        tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-x reference test data read: PRSG"
      } else {
        tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-x reference test data read: COUNTER"
      }
    }
    if {$en_y==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-y reference test data read: Disabled"
    } else {
      if {$sel_y==0} {
        tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-y reference test data read: PRSG"
      } else {
        tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCUH-y reference test data read: COUNTER"
      }
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $testdata
}


###############################################################################
#
# WG_WRITE_RCUH_CLOCK_CONTROL : RCUH write RCU clock input delay
#
# Input:
#
# - clk_delay = 0 is reset RCU clock input delay to hardware default, > 0 increment input delay one time
# - clk_edge  = 0 is capture RCU data on rising edge, != 0 is on falling edge
# - polId     = Polarization ID: x, y or "x y"
# - blpId     = BP, AP ID: 'rsp blp#'
# - rspId     = RSP ID: 'rsp#'
#
# Return: void
#
proc wg_write_rcuh_clock_control {{clk_delay 0} {clk_edge 0} {polId x} {blpId blp0} {rspId rsp0}} {
  set bit0 [expr $clk_delay > 0]  ;# RCU clock idelay control bit           
  set bit1 [expr $clk_edge  > 0]  ;# RCU clock edge data capture control bit
  set clk_control [expr ($bit1 << 1) + $bit0]
  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    set bi $blpId
#     foreach bi $blpId {
      foreach pi $polId {
        if {[rsp writeMsg [msg writeMepMsg "rcuh clock$pi $bi" $clk_control] $ri] == 0} {
          tc setResult PASSED
        } else {
          tc setResult FAILED
        }
      }
#     }
  }
}


###############################################################################
#
# WG_READ_RCUH_CLOCK_CONTROL : RCUH read RCU clock input delay bit and clock edge bit
#
# Input:
#
# - polId    = Polarization ID: x, y or "x y" - only one
# - blpId    = BP, AP ID: 'rsp blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - clk_control = RCU clock control
#             . bit 0 : '0' is default RCU clock delay, '1' is incremented delay
#             . bit 1 : '0' capture RCU data on rising edge, '1' on falling edge
#
#   tc appendlog messages are reported.
#
proc wg_read_rcuh_clock_control {{polId x} {blpId blp0} {rspId rsp0} {applev 21}} {
  set clk_control -1
  if {[rsp readMsg [msg readMepMsg "rcuh clock$polId $blpId" 1] $rspId] == 0} {
    msg setOffset 0
    set clk_control [msg readUnsigned 1]
    set bit0 [expr  $clk_control & 1      ]  ;# RCU clock idelay control bit
    set bit1 [expr ($clk_control & 2) >> 1]  ;# RCU clock edge data capture control bit
    if {$bit0==0 && $bit1==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-$polId read clock control: default input delay,     default capture data on rising edge"
    } elseif {$bit0==0 && $bit1!=0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-$polId read clock control: default input delay,     capture data on falling edge"
    } elseif {$bit0!=0 && $bit1==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-$polId read clock control: incremented input delay, default capture data on rising edge"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-$polId read clock control: incremented input delay, capture data on falling edge"
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $clk_control
}


###############################################################################
#
# WG_WRITE_RCUH_CONTROL_LIST : Write RCU protocol list to register in RSP. The list will take effect at next sync
#
# Input:
#
# - ctrl     = RCU control, three decimal byte values
# - polId    = Polarization ID: x, y or "x y"
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_write_rcuh_control_list {ctrl {polId {x y}} {blpId blp0} {rspId rsp0}} {
  # - Prepare the protocol list for RCU control register
  set c_nof_ctrl_bytes  3
  set ctrl [join $ctrl]
  if {([llength $ctrl] % $c_nof_ctrl_bytes) == 0} {
    # Treat as one or more seperate accesses each accessing the whole ctrl word of c_nof_ctrl_bytes
    set cn 0
    set cw {}
    set addr              1
    set protocol_list {}
    foreach ci $ctrl {
      lappend cw $ci
      incr cn
      if {$cn == $c_nof_ctrl_bytes} {
        lappend protocol_list [smb_protocol PROTOCOL_C_SEND_BLOCK    $c_nof_ctrl_bytes $addr $cw]
        lappend protocol_list [smb_protocol PROTOCOL_C_RECEIVE_BLOCK $c_nof_ctrl_bytes $addr]
        set cn 0
        set cw {}
      }
    }
  } else {
    # Treat as single access, this results in a wrapped and/or partial RCU ctrl word access 
    lappend protocol_list [smb_protocol PROTOCOL_C_SEND_BLOCK    [llength $ctrl]   $addr $ctrl]
    lappend protocol_list [smb_protocol PROTOCOL_C_RECEIVE_BLOCK $c_nof_ctrl_bytes $addr]
  }
  lappend protocol_list [smb_protocol PROTOCOL_C_END]
  set protocol_list [join $protocol_list]

  # - Write (and readback) the protocol list to the RCUH, use same for all RCU
  wg_write_rd_smbh_protocol_list rcuh $protocol_list $polId $blpId $rspId

  # - Overwrite (and readback) first entry of protocol result register to be sure that the results will be fresh
  wg_overwrite_rd_smbh_protocol_results rcuh $polId $blpId $rspId
}


###############################################################################
#
# WG_READ_RCUH_CONTROL_RESULT : Read RCU protocol results from register in RSP
#
# Input:
#
# - ctrl     = Expected RCU control bytes, three decimal byte values
# - polId    = Polarization: x, y or "x y"
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_read_rcuh_control_result {ctrl {polId {x y}} {blpId blp0} {rspId rsp0}} {
  global env
  source "$env(RSP)/rsp/tb/tc/5. Datapath/constants.tcl"

  # - Prepare expected read result
  set c_nof_ctrl_bytes  3
  if {([llength $ctrl] % $c_nof_ctrl_bytes) == 0} {
    # Treat as one or more seperate accesses each accessing the whole ctrl word of c_nof_ctrl_bytes
    set ctrl [join $ctrl]
    set cn 0
    set cw {}
    set exp_result ""
    foreach ci $ctrl {
      if {$cn == 0} {
        # correct expected read for RCU version number in ctrlreg[23:20]
        lappend cw [expr (($c_rcu_version & 0xF) << 4) + ($ci & 0xF)]
      } else {
        lappend cw $ci
      }
      incr cn
      if {$cn == $c_nof_ctrl_bytes} {
        append exp_result "0 $cw 0 "
        set cn 0
        set cw {}
      }
    }
    append exp_result "0"
  } else {
    # Treat as single access, this results in a wrapped and/or partial RCU ctrl word access 
    tc appendLog 11 "Can not determine expected results for wrapped and/or partial RCU ctrl word access"
    crash
  }

  # - Read the protocol results from the RCUH
  foreach ri $rspId {
    foreach bi $blpId {
      foreach pi $polId {
        set rd_result [smb_read_results rcuh [llength $exp_result] $pi $bi $ri]

        # Equal?
        if {[string equal $rd_result $exp_result] == 1} {
          tc appendLog 21 ">>> RSP-$ri, BLP-$bi, RCU-$pi I2C access result buffer contents is OK"
          tc setResult PASSED
        } else {
          tc appendLog 11 ">>> RSP-$ri, BLP-$bi, RCU-$pi I2C access result buffer contents is wrong:"
          tc appendLog 11 "Expected protocol result: $exp_result"
          tc appendLog 11 "Read     protocol result: $rd_result"
          tc setResult FAILED
        }
      }
    }
  }
}


###############################################################################
#
# WG_WRITE_TBBI_SETTINGS : Write TBBI settings to register in RSP. The settings will take effect at sync.
#
# Input:
#
# - station_id     = Station identifier number, 1 byte.
# - rsp_id         = RSP board identifier number, 1 byte.
# - rcu_id         = RCU board identifier number, 1 byte.
# - sample_freq    = 160 or 200, to represent the sample frequency of the RCU in MHz, 1 byte
# - nof_samples    = Number of samples, 2 bytes
# - nof_bands      = Number of bands, 2 bytes
# - polId          = Polarization ID: x, y or "x y"
# - blpId          = BLP ID: 'blp#'
# - rspId          = RSP ID: 'rsp#'
# - applev         = Append log level
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_write_tbbi_settings {station_id rsp_id rcu_id sample_freq nof_samples nof_bands {polId {x y}} {blpId blp0} {rspId rsp0} {applev 41}} {
  tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-$polId write TBBI settings:"
  tc appendLog $applev "      station_id     = $station_id"
  tc appendLog $applev "      rsp_id         = $rsp_id"
  tc appendLog $applev "      rcu_id         = $rcu_id"
  tc appendLog $applev "      sample_freq    = $sample_freq"
  tc appendLog $applev "      nof_samples    = $nof_samples"
  tc appendLog $applev "      nof_bands      = $nof_bands"

  foreach ri $rspId {
    foreach pi $polId {
      # Make use of MEP broadcast to all BLP on an RSP board
      if {[rsp writeMsg [msg writeMepMsg "tbbi settings$pi $blpId" "$station_id
                                                                    $rsp_id
                                                                    $rcu_id
                                                                    $sample_freq
                                                                    [wg_i2bb $nof_samples]
                                                                    [wg_i2bb $nof_bands]" ] $ri] == 0} {
        tc setResult PASSED
      } else {
        tc setResult FAILED
      }
    }
  }
}


###############################################################################
#
# WG_READ_TBBI_SETTINGS : Read TBBI settings register in RSP
#
# Input:
#
# - polId    = Polarization ID: x, y or "x y" - only one
# - blpId    = BLP ID: 'rsp blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - settings = TBBI settings
#
#   tc appendlog messages are reported.
#
proc wg_read_tbbi_settings {{polId x} {blpId blp0} {rspId rsp0} {applev 21}} {
  set settings {}
  if {[rsp readMsg [msg readMepMsg "tbbi settings$polId $blpId" 8] $rspId] == 0} {
    msg setOffset 0
    set station_id  [msg readUnsigned 1]
    set rsp_id      [msg readUnsigned 1]
    set rcu_id      [msg readUnsigned 1]
    set sample_freq [msg readUnsigned 1]
    set nof_samples [msg readUnsigned 2]
    set nof_bands   [msg readUnsigned 2]
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-$polId read TBBI settings:"
    tc appendLog $applev "      station_id  = $station_id"
    tc appendLog $applev "      rsp_id      = $rsp_id"
    tc appendLog $applev "      rcu_id      = $rcu_id"
    tc appendLog $applev "      sample_freq = $sample_freq"
    tc appendLog $applev "      nof_samples = $nof_samples"
    tc appendLog $applev "      nof_bands   = $nof_bands"
    lappend settings $station_id
    lappend settings $rsp_id
    lappend settings $rcu_id
    lappend settings $sample_freq
    lappend settings $nof_samples
    lappend settings $nof_bands
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $settings
}


###############################################################################
#
# WG_WRITE_TBBI_BANDSEL : Write TBBI band selections to register in RSP. The settings will take effect at sync.
#
# Input:
#
# - nof_bands      = When 0 then use bandsel array, else select first nof_bands
# - bandsel        = Array of band select bytes ('dec' or 0x'hex'), will be padded with 0 bytes to total nof 64 bytes if necessary
# - polId          = Polarization ID: x, y or "x y"
# - blpId          = BLP ID: 'blp#'
# - rspId          = RSP ID: 'rsp#'
# - applev         = Append log level
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_write_tbbi_bandsel {nof_bands bandsel {polId {x y}} {blpId blp0} {rspId rsp0} {applev 41}} {
  set bytesel {}
  set bytenr 0
  if {$nof_bands==0} {
    foreach bs $bandsel {
      lappend bytesel [expr $bs]
      incr bytenr
    }
  } else {
    set nof_bytes [expr $nof_bands/8]
    set nof_bits  [expr $nof_bands - 8*$nof_bytes]
  tc appendLog $applev "      nof_bytes = $nof_bytes"
  tc appendLog $applev "      nof_bits = $nof_bits"
    
    for {set bi 0} {$bi < $nof_bytes} {incr bi} {
      lappend bytesel [expr 0xFF]
    }
    set bytenr $nof_bytes
    if {$nof_bits > 0} {
      lappend bytesel [expr (1 << $nof_bits) - 1]
      incr bytenr
    }
  }
  for {set bi $bytenr} {$bi < 64} {incr bi} {
    lappend bytesel 0
  }
  
  tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-$polId write TBBI bandsel:"
  tc appendLog $applev "      nof_bands = $nof_bands"
  set bytenr 0
  for {set br 0} {$br < 8} {incr br} {
    set bytelo       $bytenr
    set bytehi [expr $bytenr + 8-1]
    set str ""
    for {set bc 0} {$bc < 8} {incr bc} {
      append str [format " %2x" [lindex $bytesel $bytenr]]
      incr bytenr
    }
    tc appendLog $applev "      bandsel\[$bytelo:$bytehi\] = $str"
  }
  
  foreach ri $rspId {
    foreach pi $polId {
      # Make use of MEP broadcast to all BLP on an RSP board
      if {[rsp writeMsg [msg writeMepMsg "tbbi selmem$pi $blpId" $bytesel] $ri] == 0} {
        tc setResult PASSED
      } else {
        tc setResult FAILED
      }
    }
  }
}


###############################################################################
#
# WG_READ_TBBI_BANDSEL : Read TBBI band selections from register in RSP.
#
# Input:
#
# - polId          = Polarization ID: x, y or "x y" - only one
# - blpId          = BLP ID: 'blp#' - only one
# - rspId          = RSP ID: 'rsp#' - only one
# - applev         = Append log level
#
# Report:
#
#   bandsel        = Array of band select bytes ('dec' or 0x'hex'), will be padded with 0 bytes to total nof 64 bytes if necessary
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_read_tbbi_bandsel {{polId x} {blpId blp0} {rspId rsp0} {applev 41}} {
  set bandsel {}
  if {[rsp readMsg [msg readMepMsg "tbbi selmem$polId $blpId" 64] $rspId] == 0} {
    msg setOffset 0
    for {set bi 0} {$bi < 64} {incr bi} {
      lappend bandsel [msg readUnsigned 1]
    }
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, RCU-$polId read TBBI bandsel:"
    set bytenr 0
    for {set br 0} {$br < 8} {incr br} {
      set bytelo       $bytenr
      set bytehi [expr $bytenr + 8-1]
      set str ""
      for {set bc 0} {$bc < 8} {incr bc} {
        append str [format " %2x" [lindex $bandsel $bytenr]]
        incr bytenr
      }
      tc appendLog $applev "      bandsel\[$bytelo:$bytehi\] = $str"
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $bandsel
}


###############################################################################
#
# WG_WRITE_TBBI_TEST : Write TBBI tx test data settings to register in RSP.
#
# Input:
#
# - sel_x    = Test data for x, 1 is COUNTER data, 0 is PRSG data (powerup default)
# - sel_y    = Test data for y, 1 is COUNTER data, 0 is PRSG data (powerup default)
# - en_x     = '0' disable x tx test data (powerup default), '1' enable x tx test data
# - en_y     = '0' disable y tx test data (powerup default), '1' enable y tx test data
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - applev   = Append log level
#
# Report:
#
#   tc appendlog messages are reported.
#   tc setResult is set
#
proc wg_write_tbbi_test {en_x en_y sel_x sel_y {blpId blp0} {rspId rsp0} {applev 41}} {
  # Assemble settings word from input arguments
  set en_x  [expr $en_x & 0x1]
  set en_y  [expr $en_y & 0x1]
  set sel_x [expr $sel_x & 0x1]
  set sel_y [expr $sel_y & 0x1]
  
  set testdata [expr $sel_x + ($sel_y<<1) + ($en_x<<2) + ($en_y<<3)]

  if {$en_x==0} {
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-x disable tx test data"
  } else {
    if {$sel_x==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-x enable PRSG tx test data"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-x enable COUNTER tx test data"
    }
  }
  if {$en_y==0} {
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-y disable tx test data"
  } else {
    if {$sel_y==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-y enable PRSG tx test data"
    } else {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-y enable COUNTER tx test data"
    }
  }

  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    if {[rsp writeMsg [msg writeMepMsg "tbbi test $blpId" $testdata] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_TBBI_TEST : Read TBBI tx test data settings from register in RSP.
#
# Input:
#
# - blpId  = BLP ID: 'blp#'   -- only one
# - rspId  = RSP ID: 'rsp#'   -- only one
# - applev = Append log level
#
# Report:
#
#   tc appendlog messages are reported.
#   . sel_x = Test data for x, 1 is COUNTER data, 0 is PRSG data (powerup default)
#   . sel_y = Test data for y, 1 is COUNTER data, 0 is PRSG data (powerup default)
#   . en_x  = '0' disabled x tx test data (powerup default), '1' enabled x tx test data
#   . en_y  = '0' disabled y tx test data (powerup default), '1' enabled y tx test data
#
#   tc setResult is set
#
proc wg_read_tbbi_test {{blpId blp0} {rspId rsp0} {applev 21} } {
  set testdata -1
  if {[rsp readMsg [msg readMepMsg "tbbi test $blpId" 1] $rspId] == 0} {
    msg setOffset 0
    set testdata [msg readUnsigned 1]
    set sel_x [expr $testdata & 0x1]
    set sel_y [expr $testdata & 0x2]
    set en_x  [expr $testdata & 0x4]
    set en_y  [expr $testdata & 0x8]
    if {$en_x==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-x tx test data read: Disabled"
    } else {
      if {$sel_x==0} {
        tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-x tx test data read: PRSG"
      } else {
        tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-x tx test data read: COUNTER"
      }
    }
    if {$en_y==0} {
      tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-y tx test data read: Disabled"
    } else {
      if {$sel_y==0} {
        tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-y tx test data read: PRSG"
      } else {
        tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, TBBI-y tx test data read: COUNTER"
      }
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $testdata
}


###############################################################################
#
# WG_WRITE_DIAG_BYPASS : DIAG write bypass
#
# Input:
#
# - bypass   = Bypass enable bits
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - applev   = Append log level
#
# Return: void
#
#   tc appendlog messages are reported.
#
proc wg_write_diag_bypass {bypass {blpId blp0} {rspId rsp0} {applev 41}} {
  tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, write DIAG bypass:"
  tc appendLog $applev "      bit(0) : Bypass DC                      = [expr ($bypass &  1    ) > 0]"
  tc appendLog $applev "      bit(1) : Bypass PFS                     = [expr ($bypass & (1<<1)) > 0]"
  tc appendLog $applev "      bit(2) : Bypass PFT                     = [expr ($bypass & (1<<2)) > 0]"
  tc appendLog $applev "      bit(3) : Bypass BF                      = [expr ($bypass & (1<<3)) > 0]"
  tc appendLog $applev "      bit(4) : SI enable X                    = [expr ($bypass & (1<<4)) > 0]"
  tc appendLog $applev "      bit(5) : SI enable Y                    = [expr ($bypass & (1<<5)) > 0]"
  tc appendLog $applev "      bit(6) : DIAG result buffer use sync    = [expr ($bypass & (1<<6)) > 0]"
  tc appendLog $applev "      bit(7) : DIAG result buffer use resync  = [expr ($bypass & (1<<7)) > 0]"
  tc appendLog $applev "      bit(8) : PFT switching disable          = [expr ($bypass & (1<<8)) > 0]"
  tc appendLog $applev "   bit(10:9) : DIAG result buffer for BM bank = [expr ($bypass & (3<<9)) > 0]"
  tc appendLog $applev "     bit(11) : DIAG result buffer for BP      = [expr ($bypass & (1<<11)) > 0]"
  tc appendLog $applev "     bit(12) : Page swap on system sync       = [expr ($bypass & (1<<12)) > 0]"
  tc appendLog $applev "     bit(13) : RAD tx beamlet disable         = [expr ($bypass & (1<<13)) > 0]"
  tc appendLog $applev "     bit(14) : RAD tx crosslet disable        = [expr ($bypass & (1<<14)) > 0]"
  tc appendLog $applev "     bit(15) : RAD tx subband disable         = [expr ($bypass & (1<<15)) > 0]"

  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    if {[rsp writeMsg [msg writeMepMsg "diag bypass $blpId" [wg_i2bb $bypass]] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_DIAG_BYPASS : DIAG read bypass
#
# Input:
#
# - blpId    = BLP ID: 'blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - bypass   = Bypass enable bits
#
#   tc appendlog messages are reported.
#
proc wg_read_diag_bypass {{blpId blp0} {rspId rsp0} {applev 21}} {
  set bypass -1
  if {[rsp readMsg [msg readMepMsg "diag bypass $blpId" 2] $rspId] == 0} {
    msg setOffset 0
    set bypass [msg readUnsigned 2]
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, read DIAG bypass:"
    tc appendLog $applev "      bit(0) : Bypass DC                      = [expr ($bypass &  1    ) > 0]"
    tc appendLog $applev "      bit(1) : Bypass PFS                     = [expr ($bypass & (1<<1)) > 0]"
    tc appendLog $applev "      bit(2) : Bypass PFT                     = [expr ($bypass & (1<<2)) > 0]"
    tc appendLog $applev "      bit(3) : Bypass BF                      = [expr ($bypass & (1<<3)) > 0]"
    tc appendLog $applev "      bit(4) : SI enable X                    = [expr ($bypass & (1<<4)) > 0]"
    tc appendLog $applev "      bit(5) : SI enable Y                    = [expr ($bypass & (1<<5)) > 0]"
    tc appendLog $applev "      bit(6) : DIAG result buffer use sync    = [expr ($bypass & (1<<6)) > 0]"
    tc appendLog $applev "      bit(7) : DIAG result buffer use resync  = [expr ($bypass & (1<<7)) > 0]"
    tc appendLog $applev "      bit(8) : PFT switching disable          = [expr ($bypass & (1<<8)) > 0]"
    tc appendLog $applev "   bit(10:9) : DIAG result buffer for BM bank = [expr ($bypass & (3<<9)) > 0]"
    tc appendLog $applev "     bit(11) : DIAG result buffer for BP      = [expr ($bypass & (1<<11)) > 0]"
    tc appendLog $applev "     bit(12) : Page swap on system sync       = [expr ($bypass & (1<<12)) > 0]"
    tc appendLog $applev "     bit(13) : RAD tx beamlet disable         = [expr ($bypass & (1<<13)) > 0]"
    tc appendLog $applev "     bit(14) : RAD tx crosslet disable        = [expr ($bypass & (1<<14)) > 0]"
    tc appendLog $applev "     bit(15) : RAD tx subband disable         = [expr ($bypass & (1<<15)) > 0]"
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $bypass
}


###############################################################################
#
# WG_SETUP_DIAG_WAVEFORM_GENERATOR : Setup the waveform generator
#
# Input:
#
# - mode     = Mode: 'idle', 'single', 'repeat', 'calc'
# - phase    = - Calc phase: 8 bit INTEGER
# - freq     = - Calc freq: 32 bit INTEGER
# - ampl     = - Calc ampl: 32 bit INTEGER
# - polId    = Polarization ID: x, y or "x y"
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - bc       = When != 0 use MEP broadcast
#
# Return: void
#
proc wg_setup_diag_waveform_generator {mode nof_samples {phase 0} {freq 0} {ampl 0} {polId {x y}} {blpId blp0} {rspId rsp0} {bc 0}} {
  # Format bytes (LSByte first):  mode 1, phase 1, nof_samples 2, freq 4, ampl 4.
  set m 0
  switch $mode {
    idle    {set m 0}
    calc    {set m 1}
    single  {set m 3}
    repeat  {set m 5}
    default {
      puts "Unknown WG mode $mode."
      exit
    }
  }
  foreach ri $rspId {
    foreach pi $polId {
      if {$bc == 0} {
        foreach bi $blpId {
          if {[rsp writeMsg [msg writeMepMsg "diag set$pi $bi" "$m
                                                                [expr      $phase%256]
                                                                [wg_i2bb   $nof_samples]
                                                                [wg_i2bbbb $freq]
                                                                [wg_i2bbbb $ampl]" ] $ri] == 0} {
            tc setResult PASSED
          } else {
            tc setResult FAILED
          }
        }
      } else {
        # Make use of MEP broadcast to all BLP on an RSP board
        if {[rsp writeMsg [msg writeMepMsg "diag set$pi $blpId" "$m
                                                                 [expr      $phase%256]
                                                                 [wg_i2bb   $nof_samples]
                                                                 [wg_i2bbbb $freq]
                                                                 [wg_i2bbbb $ampl]" ] $ri] == 0} {
          tc setResult PASSED
        } else {
          tc setResult FAILED
        }
      }
    }
  }
}


###############################################################################
#
# WG_READ_DIAG_WAVEFORM_GENERATOR : Read the DIAG waveform generator settings
#
# Input:
#
# - polId    = Polarization: x | y   - only one
# - blpId    = BLP ID: 'blp#'        - only one
# - rspId    = RSP ID: 'rsp#'        - only one
#
# Return:
#
# - settings = List of WG settings fields
#
proc wg_read_diag_waveform_generator {{polId x} {blpId blp0} {rspId rsp0}} {
  set settings {}
  if {[rsp readMsg [msg readMepMsg "diag set$polId $blpId" 12] $rspId] == 0} {
    msg setOffset 0
    switch [msg readUnsigned 1] {
      0  {set m idle}
      1  {set m calc}
      3  {set m single}
      5  {set m repeat}
      default {
        set m "unknown"
      }
    }
    lappend settings $m                       ;# mode
    lappend settings [msg readUnsigned 1]     ;# phase
    lappend settings [msg readUnsigned 2]     ;# nof samples
    lappend settings [msg readUnsigned 4]     ;# freq
    lappend settings [msg readUnsigned 4]     ;# ampl
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $settings
}


###############################################################################
#
# WG_WRITE_DIAG_WAVEFORM : Write waveform to the waveform generator
#
# Input:
#
# - wavep    = Polarization: 'wavex' or 'wavey'
# - samples  = Waveform sample values, INTEGERs
# - polId    = Polarization ID: x, y or "x y"
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - bc       = When != 0 use MEP broadcast
#
# Return:
#
# - ret      = 1 if write PASSED else 0
#
proc wg_write_diag_waveform {samples {polId {x y}} {blpId blp0} {rspId rsp0} {bc 0}} {
  set c_eth_block 1400

  set data    [wg_i2bbbb $samples]
  set len     [llength $data]
  set ret_all 0

  foreach ri $rspId {
    foreach pi $polId {
      set i 0
      while {$i < $len} {
        if {$bc == 0} {
          foreach bi $blpId {
            if {[rsp writeMsg [msg writeMepMsg "diag wave$pi $bi \"offset $i\"" [lrange $data $i [expr $i +  $c_eth_block - 1]]] $ri] == 0} {
              tc setResult PASSED
            } else {
              tc setResult FAILED
              set ret_all 1
            }
          }
        } else {
          # Make use of MEP broadcast to all BLP on an RSP board
          if {[rsp writeMsg [msg writeMepMsg "diag wave$pi $blpId \"offset $i\"" [lrange $data $i [expr $i +  $c_eth_block - 1]]] $ri] == 0} {
            tc setResult PASSED
          } else {
            tc setResult FAILED
            set ret_all 1
          }
        }
        incr i $c_eth_block
      }
    }
  }

  return $ret_all
}


###############################################################################
#
# WG_READ_DIAG_WAVEFORM : Readback DIAG waveform
#
# Input:
#
# - len_w    = Number of words to read from the waveform buffer, INTEGER
# - polId    = Polarization ID: 'x' or 'y' - only one
# - blpId    = BLP ID: 'blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
#
# Return:
#
# - waveform = Read message result (0 when readMsg went PASSED else 1) and the read waveform samples
#
proc wg_read_diag_waveform {len_w {polId x} {blpId blp0} {rspId rsp0}} {
  set c_diag_reg_wave_dat_w  18
  set c_sign                 [expr round(pow(2,$c_diag_reg_wave_dat_w-1))]

  set c_eth_block   1400
  set i             0
  set n             $c_eth_block
  set width         4
  set len           [expr $len_w * $width]
  set ret           0
  set waveform      {}

  while {$i < $len && $ret == 0} {
    if {$len - $i < $n} {set n [expr $len - $i]}
    if {[rsp readMsg [msg readMepMsg "diag wave$polId $blpId \"offset $i\"" $n] $rspId] == 0} {
      msg setOffset 0
      for {set k 0} {$k < $n/$width} {incr k} {
        set sample [msg readUnsigned $width]
        if {$sample >= $c_sign} {
          lappend waveform [expr $sample - 2*$c_sign]
        } else {
          lappend waveform $sample
        }
      }
      tc setResult PASSED
    } else {
      tc setResult FAILED
      set ret 1
    }
    incr i $c_eth_block
  }
  lappend ret $waveform

  return $ret
}


###############################################################################
#
# WG_READ_DIAG_RESULT_BUFFER : Read DIAG result buffer
#
# Input:
#
# - len      = Number of bytes to read from the result buffer, INTEGER
# - width    = Word width of result buffer samples, INTEGER 1, 2 or 4
# - blpId    = BLP ID: 'blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
#
# Return:
#
# - res_buffer = Result buffer samples
#
proc wg_read_diag_result_buffer {len width {blpId blp0} {rspId rsp0}} {
  set c_eth_block   1400
  set i             0
  set n             $c_eth_block
  set ret           0
  set res_buffer    {}

  while {$i < $len && $ret == 0} {
    if {$len - $i < $n} {set n [expr $len - $i]}
    if {[rsp readMsg [msg readMepMsg "diag result $blpId \"offset $i\"" $n] $rspId] == 0} {
      msg setOffset 0
      for {set k 0} {$k < $n/$width} {incr k} {
        lappend res_buffer [msg readSigned $width]
      }
      tc setResult PASSED
    } else {
      tc setResult FAILED
      set ret 1
    }
    incr i $c_eth_block
  }

  return $res_buffer
}


###############################################################################
#
# WG_WRITE_BS_NOF_SAMPLES_PSYNC : Write BS number of samples per sync interval
#
# Input:
#
# - nof_samples_psync  = BS nof samples per sync interval
# - blpId              = BLP ID: 'blp#'
# - rspId              = RSP ID: 'rsp#'
# - applev             = Append log level
#
# Return: void
#
proc wg_write_bs_nof_samples_psync {nof_samples_psync {blpId blp0} {rspId rsp0} {applev 41}} {
  tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, write BS nof samples per sync: $nof_samples_psync"
  foreach ri $rspId {
    # Make use of MEP broadcast to all BLP on an RSP board
    if {[rsp writeMsg [msg writeMepMsg "bs psync $blpId" [wg_i2bbbb $nof_samples_psync]] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_BS_NOF_SAMPLES_PSYNC : BS read nof samples per sync interval
#
# Input:
#
# - blpId    = BLP ID: 'blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - nof_samples_psync = BS nof samples per sync value
#
#   tc appendlog messages are reported.
#
proc wg_read_bs_nof_samples_psync {{blpId blp0} {rspId rsp0} {applev 21}} {
  set nof_samples_psync -1
  if {[rsp readMsg [msg readMepMsg "bs psync $blpId" 4] $rspId] == 0} {
    msg setOffset 0
    set nof_samples_psync [msg readUnsigned 4]
    tc appendLog $applev ">>> RSP-$rspId, BLP-$blpId, read BS nof samples per sync : $nof_samples_psync"
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $nof_samples_psync
}


###############################################################################
#
# WG_WRITE_SS : Write subband to beamlet map to SS
#
# Input:
#
# - ss_map   = Subband to beamlet map
# - bank     = SS bank number
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - bc       = When != 0 use MEP broadcast
# - str      = SS instance name
#
# Return: void
#
proc wg_write_ss {ss_map {bank 0} {blpId blp0} {rspId rsp0} {bc 0} {str ss}} {
  set ss_map_bb [wg_i2bb $ss_map]
  foreach ri $rspId {
    if {$bc == 0} {
      foreach bi $blpId {
        if {[rsp writeMsg [msg writeMepMsg "$str settings$bank $bi" $ss_map_bb] $ri] == 0} {
          tc setResult PASSED
        } else {
          tc setResult FAILED
        }
      }
    } else {
      # Make use of MEP broadcast to all BLP on an RSP board
      if {[rsp writeMsg [msg writeMepMsg "$str settings$bank $blpId" $ss_map_bb] $ri] == 0} {
        tc setResult PASSED
      } else {
        tc setResult FAILED
      }
    }
  }
}


###############################################################################
#
# WG_READ_SS : Read back SS register
#
# Input:
#
# - len_w    = Number of words to read from the SS register, INTEGER
# - bank     = SS bank number
# - blpId    = BLP ID: 'blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
# - str      = SS instance name
#
# Return:
#
# - ss_map    = SS mapping
#
proc wg_read_ss {len_w {bank 0} {blpId blp0} {rspId rsp0} {str ss}} {
  set c_eth_block   1400
  set i             0
  set n             $c_eth_block
  set ret           0
  set width         2
  set len           [expr $len_w * $width]
  set ss_map        {}

  while {$i < $len && $ret == 0} {
    if {$len - $i < $n} {set n [expr $len - $i]}
    if {[rsp readMsg [msg readMepMsg "$str settings$bank $blpId \"offset $i\"" $n] $rspId] == 0} {
      msg setOffset 0
      for {set k 0} {$k < $n/$width} {incr k} {
        lappend ss_map [msg readSigned $width]
      }
      tc setResult PASSED
    } else {
      tc setResult FAILED
      set ret 1
    }
    incr i $c_eth_block
  }

  return $ss_map
}


###############################################################################
#
# WG_WRITE_SDO_SS : Write subband data output map to SDO-SS
#
# Input:
#
# - ss_map   = Subband data output map
# - bank     = SDO SS bank number
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - bc       = When != 0 use MEP broadcast
#
# Return: void
#
proc wg_write_sdo_ss {ss_map {bank 0} {blpId blp0} {rspId rsp0} {bc 0}} {
  return [wg_write_ss $ss_map $bank $blpId $rspId $bc sdo_ss]
}


###############################################################################
#
# WG_READ_SDO_SS : Read back subband data output map from SDO-SS register
#
# Input:
#
# - len_w    = Number of words to read from the SDO-SS register, INTEGER
# - bank     = SDO SS bank number
# - blpId    = BLP ID: 'blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
#
# Return:
#
# - ss_map   = SDO-SS mapping
#
proc wg_read_sdo_ss {len_w {bank 0} {blpId blp0} {rspId rsp0}} {
  return [wg_read_ss $len_w $bank $blpId $rspId sdo_ss]
}


###############################################################################
#
# WG_WRITE_BF : Write beamformer coefficients
#
# Input:
#
# - coefs    = Row of coefficients for xr, xi, yr or yi output
# - row      = Coefficent matrix row number: 'coefxr', 'coefxi', 'coefyr' or 'coefyi'
# - bank     = Beam mode bank number
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - bc       = When != 0 use MEP broadcast
#
# Return:
#
# - ret      = 1 if write PASSED else 0
#
proc wg_write_bf {coefs row {bank 0} {blpId blp0} {rspId rsp0} {bc 0}} {
  set c_eth_block 1400
  set coefs_bb [wg_i2bb $coefs]
  set len [llength $coefs_bb]
  set ret 0

  foreach ri $rspId {
    set i 0
    while {$i < $len} {
      if {$bc == 0} {
        foreach bi $blpId {
          if {[rsp writeMsg [msg writeMepMsg "bf $row$bank $bi \"offset $i\"" [lrange $coefs_bb $i [expr $i +  $c_eth_block - 1]]] $ri] == 0} {
            tc setResult PASSED
          } else {
            tc setResult FAILED
            set ret 1
          }
        }
      } else {
        # Make use of MEP broadcast to all BLP on an RSP board
        if {[rsp writeMsg [msg writeMepMsg "bf $row$bank $blpId \"offset $i\"" [lrange $coefs_bb $i [expr $i +  $c_eth_block - 1]]] $ri] == 0} {
          tc setResult PASSED
        } else {
          tc setResult FAILED
          set ret 1
        }
      }
      incr i $c_eth_block
    }
  }

  return $ret
}


###############################################################################
#
# WG_READ_BF : Read back BF register
#
# Input:
#
# - row      = Coefficent matrix row number: 'coefxr', 'coefxi', 'coefyr' or 'coefyi'
# - len_w    = Number of words to read from the coefficients register, INTEGER
# - bank     = Beam mode bank number
# - blpId    = BLP ID: 'blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
#
# Return:
#
# - coefs    = Coefficients
#
proc wg_read_bf {row len_w {bank 0} {blpId blp0} {rspId rsp0}} {
  set c_eth_block   1400
  set i             0
  set n             $c_eth_block
  set ret           0
  set width         2
  set len           [expr $len_w * $width]
  set coefs         {}

  while {$i < $len && $ret == 0} {
    if {$len - $i < $n} {set n [expr $len - $i]}
    if {[rsp readMsg [msg readMepMsg "bf $row$bank $blpId \"offset $i\"" $n] $rspId] == 0} {
      msg setOffset 0
      for {set k 0} {$k < $n/$width} {incr k} {
        lappend coefs [msg readSigned $width]
      }
      tc setResult PASSED
    } else {
      tc setResult FAILED
      set ret 1
    }
    incr i $c_eth_block
  }

  return $coefs
}


###############################################################################
#
# WG_OVERWRITE_SUBBAND_POWER_STATISTICS : Overwrite the subband statistics buffer
#
# Input:
#
# - samples  = SST buffer sample values, INTEGERs
# - blpId    = BLP ID: 'blp#'
# - rspId    = RSP ID: 'rsp#'
# - bc       = When != 0 use MEP broadcast
#
# Return:
#
# - ret      = 1 if write PASSED else 0
#
# Warning: SST buffer does not support write access (yet), so this function can not be used
#
proc wg_overwrite_subband_power_statistics {samples {blpId blp0} {rspId rsp0} {bc 0}} {
  set c_eth_block 1400

  set data    [wg_i2bbbb $samples]
  set len     [llength $data]
  set ret_all 0

  foreach ri $rspId {
    set i 0
    while {$i < $len} {
      if {$bc == 0} {
        foreach bi $blpId {
          if {[rsp writeMsg [msg writeMepMsg "sst power $bi \"offset $i\"" [lrange $data $i [expr $i +  $c_eth_block - 1]]] $ri] == 0} {
            tc setResult PASSED
          } else {
            tc setResult FAILED
            set ret_all 1
          }
        }
      } else {
        # Make use of MEP broadcast to all BLP on an RSP board
        if {[rsp writeMsg [msg writeMepMsg "sst power $blpId \"offset $i\"" [lrange $data $i [expr $i +  $c_eth_block - 1]]] $ri] == 0} {
          tc setResult PASSED
        } else {
          tc setResult FAILED
          set ret_all 1
        }
      }
      incr i $c_eth_block
    }
  }

  return $ret_all
}


###############################################################################
#
# WG_READ_SUBBAND_POWER_STATISTICS : Read subband statistics buffer
#
# Input:
#
# - len_w    = Number of words to read from the statistics buffer, INTEGER
# - blpId    = BLP ID: 'blp#' - only one
# - rspId    = RSP ID: 'rsp#' - only one
#
# Return:
#
# - sst_buffer = Subband power statistics
#
proc wg_read_subband_power_statistics {len_w {blpId blp0} {rspId rsp0}} {
  set width         4
  set len           [expr $len_w * $width]
  set c_eth_block   1400
  set i             0
  set n             $c_eth_block
  set ret           0
  set sst_buffer    {}

  while {$i < $len && $ret == 0} {
    if {$len - $i < $n} {set n [expr $len - $i]}
    if {[rsp readMsg [msg readMepMsg "sst power $blpId \"offset $i\"" $n] $rspId] == 0} {
      msg setOffset 0
      for {set k 0} {$k < $n/$width} {incr k} {
        lappend sst_buffer [msg readUnsigned $width]
      }
      tc setResult PASSED
    } else {
      tc setResult FAILED
      set ret 1
    }
    incr i $c_eth_block
  }

  return $sst_buffer
}


###############################################################################
#
# WG_READ_BEAMLET_POWER_STATISTICS : Read beamlet statistics buffer
#
# Input:
#
# - len_w    = Number of words to read from the statistics register, INTEGER
# - regId    = Statistics register to read '#', # = 0..3 - select one
# - rspId    = RSP ID: 'rsp#' - only one
#
# Return:
#
# - bst_lane = Beamlet power statistics for lane $regId
#
proc wg_read_beamlet_power_statistics {len_w {regId 0} {rspId rsp0}} {
  set width         4
  set len           [expr $len_w * $width]
  set c_eth_block   1400
  set i             0
  set n             $c_eth_block
  set ret           0
  set bst_lane    {}

  while {$i < $len && $ret == 0} {
    if {$len - $i < $n} {set n [expr $len - $i]}
    if {[rsp readMsg [msg readMepMsg "bst power$regId rsp \"offset $i\"" $n] $rspId] == 0} {
      msg setOffset 0
      for {set k 0} {$k < $n/$width} {incr k} {
        lappend bst_lane [msg readUnsigned $width]
      }
      tc setResult PASSED
    } else {
      tc setResult FAILED
      set ret 1
    }
    incr i $c_eth_block
  }

  return $bst_lane
}


###############################################################################
#
# WG_READ_CROSSLET_POWER_STATISTICS : Read crosslet statistics buffer
#
# Input:
#
# - len_w    = Number of words to read from the statistics register, INTEGER
# - regId    = Statistics register to read 'r#', # = 0..31 - select one
# - rspId    = RSP ID: 'rsp#' - only one
#
# Return:
#
# - xst_buffer = Crossler power statistics from one register
#
proc wg_read_crosslet_power_statistics {len_w {regId r0} {rspId rsp0}} {
  set width         4
  set len           [expr $len_w * $width]
  set c_eth_block   1400
  set i             0
  set n             $c_eth_block
  set ret           0
  set xst_buffer    {}

  while {$i < $len && $ret == 0} {
    if {$len - $i < $n} {set n [expr $len - $i]}
    if {[rsp readMsg [msg readMepMsg "xst $regId rsp \"offset $i\"" $n] $rspId] == 0} {
      msg setOffset 0
      for {set k 0} {$k < $n/$width} {incr k} {
        lappend xst_buffer [msg readUnsigned $width]
      }
      tc setResult PASSED
    } else {
      tc setResult FAILED
      set ret 1
    }
    incr i $c_eth_block
  }

  return $xst_buffer
}


###############################################################################
#
# WG_WRITE_RAD_SETTINGS : RAD_BP write settings
#
# Input:
#
# - settings = Lane settings for beamlets and crosslets
# - rspId    = RSP ID: 'rsp#'
# - applev   = Append log level
#
# Return: void
#
#   tc appendlog messages are reported (see wg_read_rad_settings for lane mode definition).
#
proc wg_write_rad_settings {settings {rspId rsp0} {applev 21}} {
  set nof_lanes 4
  tc appendLog $applev ">>> RSP-$rspId write RAD settings:"
  # beamlet lane modes
  for {set i 0} {$i < $nof_lanes} {incr i} {
    set lane_mode [expr (($settings >> (8*$i)) & 0x3)]
    switch $lane_mode {
      0       {tc appendLog $applev "      lane($i): beamlet  mode local"}
      1       {tc appendLog $applev "      lane($i): beamlet  mode disable"}
      2       {tc appendLog $applev "      lane($i): beamlet  mode combine"}
      default {tc appendLog $applev "      lane($i): beamlet  mode remote"}
    }
  }
  # crosslet lane modes
  for {set i 0} {$i < $nof_lanes} {incr i} {
    set lane_mode [expr (($settings >> (8*$i + 2)) & 0x3)]
    switch $lane_mode {
      0       {tc appendLog $applev "      lane($i): crosslet mode local"}
      1       {tc appendLog $applev "      lane($i): crosslet mode disable"}
      2       {tc appendLog $applev "      lane($i): crosslet mode combine"}
      default {tc appendLog $applev "      lane($i): crosslet mode remote"}
    }
  }
  foreach ri $rspId {
    if {[rsp writeMsg [msg writeMepMsg "rad settings rsp" [wg_i2bbbb $settings]] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_RAD_SETTINGS : RAD_BP read settings
#
# Input:
#
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - settings = Lane settings for beamlets and crosslets
#
#     lane mode: one byte for each lane
#     format: XXXXAABB
#   
#     where XX = don't care
#           AA = xlet mode
#           BB = blet mode
#   
#      mode 00 = ignore remote data (only local)  DEFAULT
#      mode 01 = disable
#      mode 10 = combine local and remote data
#      mode 11 = ignore local data (only remote)
#
#   tc appendlog messages are reported.
#
proc wg_read_rad_settings {{rspId rsp0} {applev 21}} {
  set nof_lanes 4
  set settings {}
  if {[rsp readMsg [msg readMepMsg "rad settings rsp" 4] $rspId] == 0} {
    msg setOffset 0
    set settings [msg readUnsigned 4]
    tc appendLog $applev ">>> RSP-$rspId read RAD settings:"
    # beamlet lane modes
    for {set i 0} {$i < $nof_lanes} {incr i} {
      set lane_mode [expr (($settings >> (8*$i)) & 0x3)]
      switch $lane_mode {
        0       {tc appendLog $applev "      lane($i): beamlet  mode local"}
        1       {tc appendLog $applev "      lane($i): beamlet  mode disable"}
        2       {tc appendLog $applev "      lane($i): beamlet  mode combine"}
        default {tc appendLog $applev "      lane($i): beamlet  mode remote"}
      }
    }
    # crosslet lane modes
    for {set i 0} {$i < $nof_lanes} {incr i} {
      set lane_mode [expr (($settings >> (8*$i + 2)) & 0x3)]
      switch $lane_mode {
        0       {tc appendLog $applev "      lane($i): crosslet mode local"}
        1       {tc appendLog $applev "      lane($i): crosslet mode disable"}
        2       {tc appendLog $applev "      lane($i): crosslet mode combine"}
        default {tc appendLog $applev "      lane($i): crosslet mode remote"}
      }
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $settings
}


###############################################################################
#
# WG_READ_RAD_LATENCY : RAD_BP read latency
#
# Input:
#
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - latency = RI and lane latencies crosslets and beamlets
#
#   tc appendlog messages are reported.
#
proc wg_read_rad_latency {{rspId rsp0} {applev 21}} {
  global env
  source "$env(RSP)/rsp/tb/tc/5. Datapath/constants.tcl"
  
  set nof_lanes $c_rsp_nof_lanes
  set nof_rd [expr 2*$c_rad_nof_rx_latency]
  set latency {}
  if {[rsp readMsg [msg readMepMsg "rad latency rsp" $nof_rd] $rspId] == 0} {
    msg setOffset 0
    for {set i 0} {$i < $nof_lanes} {incr i} {
      set lane($i,b) [msg readUnsigned 2]
      set lane($i,x) [msg readUnsigned 2]
    }
    set ring [msg readUnsigned 2]

    tc appendLog $applev ">>> RSP-$rspId read RAD latency:"
    tc appendLog $applev "      Ring     : $ring"
    lappend latency $ring
    for {set i [expr $nof_lanes-1]} {$i >= 0 } {incr i -1} {
      tc appendLog $applev "      Lane($i,x): $lane($i,x)"
      tc appendLog $applev "      Lane($i,b): $lane($i,b)"
      lappend latency $lane($i,x)
      lappend latency $lane($i,b)
    }
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $latency
}


###############################################################################
#
# WG_WRITE_CDO_SETTINGS : Write the CDO setting register
#
# Input:
#
# - station_id    = Station identifier (16 bit INTEGER)
# - config_id     = Configuration identifier (16 bit INTEGER)
# - ffi           = For Future Implementqation (16 bit INTEGER)
# - ctrl          = CDO control (16 bit INTEGER)
# - nof_blocks    = Nof blocks per CEP frame (8 bit INTEGER)
# - nof_beamlets  = Nof beamlets per block (8 bit INTEGER)
# - dst_mac       = Destination MAC (6 bytes)
# - src_mac       = Source MAC (6 bytes)
# - dst_ip        = Destination IP address = ARP-TPA (4 bytes)
# - src_ip        = Source IP address = ARP-SPA (4 bytes)
# - rspId         = RSP ID: 'rsp#'
# - applev        = Append log level
#
# Return: void
#
proc wg_write_cdo_settings {{station_id 0} {config_id 0} {ffi 0} {ctrl 0} {nof_blocks 16} {nof_beamlets 54} {dst_mac "0 0 0 0 0 0"} {src_mac "0 0 0 0 0 0"} {dst_ip "0 0 0 0"} {src_ip "0 0 0 0"} {rspId rsp0} {applev 21}} {
  # Format bytes (LSByte first, postfix _le = little endian):
  set dst_mac_le {}
  set src_mac_le {}
  if {[llength $dst_mac] != 6} {
    puts "Incorrect dst_mac length ($dst_mac has length [llength $dst_mac])."
    exit
  } elseif {[llength $src_mac] != 6} {
    puts "Incorrect src_mac length ($src_mac has length [llength $src_mac])."
    exit
  } else {
    for {set i 5} {$i >= 0} {incr i -1} {
      lappend dst_mac_le [lindex $dst_mac $i]
      lappend src_mac_le [lindex $src_mac $i]
    }
  }
  set dst_ip_le {}
  set src_ip_le {}
  if {[llength $dst_ip] != 4} {
    puts "Incorrect dst_ip length ($dst_ip has length [llength $dst_ip])."
    exit
  } elseif {[llength $src_ip] != 4} {
    puts "Incorrect src_ip length ($src_ip has length [llength $src_ip])."
    exit
  } else {
    for {set i 3} {$i >= 0} {incr i -1} {
      lappend dst_ip_le [lindex $dst_ip $i]
      lappend src_ip_le [lindex $src_ip $i]
    }
  }

  tc appendLog $applev ">>> RSP-$rspId write CDO settings:"
  tc appendLog $applev "      Station ID               = $station_id"
  tc appendLog $applev "      Configuration ID         = $config_id"
  tc appendLog $applev "      FFI                      = $ffi"
  tc appendLog $applev "      CDO control              = $ctrl"
  tc appendLog $applev "        cdo_en                 = [expr  $ctrl       & 0x1]"
  tc appendLog $applev "        cdo_lane_sel           = [expr ($ctrl >> 1) & 0x3]"
  tc appendLog $applev "        cdo_fiber_balance_en   = [expr ($ctrl >> 3) & 0x1]"
  tc appendLog $applev "        cdo_arp_en             = [expr ($ctrl >> 4) & 0x1]"
  tc appendLog $applev "      Nof blocks per CEP frame = $nof_blocks"
  tc appendLog $applev "      Nof beamlets per block   = $nof_beamlets"
  tc appendLog $applev "      Destination MAC          = $dst_mac"
  tc appendLog $applev "      Source MAC               = $src_mac"
  tc appendLog $applev "      Destination IP address   = $dst_ip"
  tc appendLog $applev "      Source IP address        = $src_ip"

  foreach ri $rspId {
    if {[rsp writeMsg [msg writeMepMsg "cdo settings rsp" "[wg_i2bb $station_id]
                                                           [wg_i2bb $config_id]
                                                           [wg_i2bb $ffi]
                                                           [wg_i2bb $ctrl]
                                                           $nof_blocks
                                                           $nof_beamlets
                                                           $dst_mac_le
                                                           $src_mac_le
                                                           $dst_ip_le
                                                           $src_ip_le" ] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_CDO_SETTINGS : Read CDO settings register
#
# Input:
#
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - settings = CDO settings
#
#   tc appendlog messages are reported.
#
proc wg_read_cdo_settings {{rspId rsp0} {applev 21}} {
  set settings {}
  if {[rsp readMsg [msg readMepMsg "cdo settings rsp" 30] $rspId] == 0} {
    msg setOffset 0
    set station_id   [msg readUnsigned 2]
    set config_id    [msg readUnsigned 2]
    set ffi          [msg readUnsigned 2]
    set ctrl         [msg readUnsigned 2]
    set nof_blocks   [msg readUnsigned 1]
    set nof_beamlets [msg readUnsigned 1]
    set dst_mac      {}
    set dst_mac      [linsert $dst_mac 0 [msg readUnsigned 1]]
    set dst_mac      [linsert $dst_mac 0 [msg readUnsigned 1]]
    set dst_mac      [linsert $dst_mac 0 [msg readUnsigned 1]]
    set dst_mac      [linsert $dst_mac 0 [msg readUnsigned 1]]
    set dst_mac      [linsert $dst_mac 0 [msg readUnsigned 1]]
    set dst_mac      [linsert $dst_mac 0 [msg readUnsigned 1]]
    set src_mac      {}
    set src_mac      [linsert $src_mac 0 [msg readUnsigned 1]]
    set src_mac      [linsert $src_mac 0 [msg readUnsigned 1]]
    set src_mac      [linsert $src_mac 0 [msg readUnsigned 1]]
    set src_mac      [linsert $src_mac 0 [msg readUnsigned 1]]
    set src_mac      [linsert $src_mac 0 [msg readUnsigned 1]]
    set src_mac      [linsert $src_mac 0 [msg readUnsigned 1]]
    set dst_ip       {}
    set dst_ip       [linsert $dst_ip  0 [msg readUnsigned 1]]
    set dst_ip       [linsert $dst_ip  0 [msg readUnsigned 1]]
    set dst_ip       [linsert $dst_ip  0 [msg readUnsigned 1]]
    set dst_ip       [linsert $dst_ip  0 [msg readUnsigned 1]]
    set src_ip       {}
    set src_ip       [linsert $src_ip  0 [msg readUnsigned 1]]
    set src_ip       [linsert $src_ip  0 [msg readUnsigned 1]]
    set src_ip       [linsert $src_ip  0 [msg readUnsigned 1]]
    set src_ip       [linsert $src_ip  0 [msg readUnsigned 1]]
    tc appendLog $applev ">>> RSP-$rspId read CDO settings:"
    tc appendLog $applev "      Station ID               = $station_id"
    tc appendLog $applev "      Configuration ID         = $config_id"
    tc appendLog $applev "      FFI                      = $ffi"
    tc appendLog $applev "      CDO control              = $ctrl"
    tc appendLog $applev "        cdo_en                 = [expr  $ctrl       & 0x1]"
    tc appendLog $applev "        cdo_lane_sel           = [expr ($ctrl >> 1) & 0x3]"
    tc appendLog $applev "        cdo_fiber_balance_en   = [expr ($ctrl >> 3) & 0x1]"
    tc appendLog $applev "        cdo_arp_en             = [expr ($ctrl >> 4) & 0x1]"
    tc appendLog $applev "      Nof blocks per CEP frame = $nof_blocks"
    tc appendLog $applev "      Nof beamlets per block   = $nof_beamlets"
    tc appendLog $applev "      Destination MAC          = $dst_mac"
    tc appendLog $applev "      Source MAC               = $src_mac"
    tc appendLog $applev "      Destination IP address   = $dst_ip"
    tc appendLog $applev "      Source IP address        = $src_ip"
    lappend settings $station_id
    lappend settings $config_id
    lappend settings $ffi
    lappend settings $ctrl
    lappend settings $nof_blocks
    lappend settings $nof_beamlets
    lappend settings $dst_mac
    lappend settings $src_mac
    lappend settings $dst_ip
    lappend settings $src_ip
    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $settings
}


###############################################################################
#
# WG_WRITE_CDO_TRANSPORT_HEADER : Write the CDO transport header register
#
# Input:
#
# - transport  = Transport header: 32 bytes
# - rspId      = RSP ID: 'rsp#'
# - applev     = Append log level
#
# Return: void
#
proc wg_write_cdo_transport_header {transport {rspId rsp0} {applev 21}} {
  if {[llength $transport] != 32} {
    puts "Incorrect transport header lenght ($transport has lenght [llength $transport])."
    exit
  }

  foreach ri $rspId {
    if {[rsp writeMsg [msg writeMepMsg "cdo iphdr rsp" $transport ] $ri] == 0} {
      tc setResult PASSED
    } else {
      tc setResult FAILED
    }
  }
}


###############################################################################
#
# WG_READ_CDO_TRANSPORT_HEADER : Read CDO transport header register
#
# Input:
#
# - rspId    = RSP ID: 'rsp#' - only one
# - applev   = Append log level
#
# Return:
#
# - transport = CDO transport header
#
#   tc appendlog messages are reported.
#
proc wg_read_cdo_transport_header {{rspId rsp0} {applev 21}} {
  set transport {}
  if {[rsp readMsg [msg readMepMsg "cdo iphdr rsp" 32] $rspId] == 0} {
    msg setOffset 0
    for {set i 0} {$i < 32} {incr i} {
      lappend transport [msg readUnsigned 1]
    }
    set th_version         [expr ([lindex $transport  0] & 0xF0) >> 4]
    set th_header_length   [expr  [lindex $transport  0] & 0xF]
    set th_type_of_service        [lindex $transport  1]
    set th_total_length    [expr  [lindex $transport  2] * 256 + [lindex $transport 3]]
    set th_identification  [expr  [lindex $transport  4] * 256 + [lindex $transport 5]]
    set th_flags           [expr ([lindex $transport  6] & 0xE0) >> 5]
    set th_fragment_offset [expr ([lindex $transport  6] & 0x1F) * 256 + [lindex $transport 7]]
    set th_ttl                    [lindex $transport  8]
    set th_protocol               [lindex $transport  9]
    set th_header_crc      [expr  [lindex $transport 10] * 256 + [lindex $transport 11]]
    set th_source_ip              [lrange $transport 12 15]
    set th_dest_ip                [lrange $transport 16 19]
    set th_udp_source_port [expr  [lindex $transport 20] * 256 + [lindex $transport 21]]
    set th_udp_dest_port   [expr  [lindex $transport 22] * 256 + [lindex $transport 23]]
    set th_udp_length      [expr  [lindex $transport 24] * 256 + [lindex $transport 25]]
    set th_udp_checksum    [expr  [lindex $transport 26] * 256 + [lindex $transport 27]]
    set th_user_defined           [lrange $transport 28 31]

    tc appendLog $applev ">>> RSP-$rspId write CDO transport header:"
    tc appendLog $applev "      Raw header           = $transport "
    tc appendLog $applev "      Version              = $th_version"
    tc appendLog $applev "      Header length        = $th_header_length"
    tc appendLog $applev "      Type of service      = $th_type_of_service"
    tc appendLog $applev "      Total lenght         = $th_total_length"
    tc appendLog $applev "      Identification       = $th_identification"
    tc appendLog $applev "      Flags                = $th_flags"
    tc appendLog $applev "      Fragment offset      = $th_fragment_offset"
    tc appendLog $applev "      TTL                  = $th_ttl"
    tc appendLog $applev "      Protocol             = $th_protocol"
    tc appendLog $applev "      Header CRC           = $th_header_crc"
    tc appendLog $applev "      Source IP            = $th_source_ip"
    tc appendLog $applev "      Destination IP       = $th_dest_ip"
    tc appendLog $applev "      UDP source port      = $th_udp_source_port"
    tc appendLog $applev "      UDP destination port = $th_udp_dest_port"
    tc appendLog $applev "      UDP length           = $th_udp_length"
    tc appendLog $applev "      Checksum             = $th_udp_checksum"
    tc appendLog $applev "      User defined         = $th_user_defined"

    tc setResult PASSED
  } else {
    tc setResult FAILED
  }
  return $transport
}


###############################################################################
#
# WG_ADC : Analogue to Digital converter.
#
# Input:
#
# - sample      = List of one or more sample values, REAL
# - nof_bits    = number of bits of the ADC, INTEGER
#
# Return:
#
# - ret         = Quantized sample value(s)
#
proc wg_adc {sample nof_bits} {
  set adc_max [expr round(pow(2,$nof_bits - 1)) - 1]
  set adc_min [expr -$adc_max - 1]

  set ret {}
  foreach a $sample {
    set q [expr round($a)]
    if {$q > $adc_max} {set q $adc_max}
    if {$q < $adc_min} {set q $adc_min}
    lappend ret $q
  }

  return $ret
}


###############################################################################
#
# WG_CALCULATE_ANALOGUE_WAVEFORM : Analogue waveform generator.
#
# Input:
#
# - type        = type of signal, STRING :
#                 - sinus
#                 - slope          (includes: sawtooth, triangle, incr, decr)
#                 - block          (includes: DC, impulse, square)
#                 - uniform noise
# - ampl        = amplitude, in units of ADC LSbit, REAL
# - offset      = DC offset, in units of ADC LSbit, REAL
# - phase       = phase, in units of a sample period, REAL typically between +-period,
#                 used as seed for random signal
# - period      = period, in units of a sample period, REAL
# - duty        = duty cycle for rising slope or block high, not used for sinus,
#                 in units of a sample period, REAL between 0 and period
# - nof_samples = number of samples of the sinus, INTEGER
#
# Return:
#
# - Real sample values of the requested waveform
#
proc wg_calculate_analogue_waveform {{type sinus} {ampl 1000} {offset 0} {phase 0} {period 512} {duty 1} {nof_samples 1024}} {

  set c_pi [expr 4*atan(1.0)]

  set amp [expr double($ampl)]
  set ofs [expr double($offset)]
  set phs [expr double($phase)]
  set per [expr double($period)]
  set dty [expr double($duty)]

  set waveform {}

  set p [expr $phs - int($phs/$per)*$per]
  if {$p < 0} {set p [expr $p + $per]}

  switch $type {
    sinus {
      for {set i 0} {$i < $nof_samples} {incr i} {
        lappend waveform [expr $ofs + $amp * sin(2*$c_pi * ($i + $phs) / $per)]
      }
    }
    cosin {
      for {set i 0} {$i < $nof_samples} {incr i} {
        lappend waveform [expr $ofs + $amp * cos(2*$c_pi * ($i + $phs) / $per)]
      }
    }
    slope {
      set a_up {}
      set b_up {}
      set a_down {}
      set b_down {}
      if {$dty != 0} {
        set a_up   [expr $amp/$dty]
        set b_up    0
      }
      if {$dty != $per} {
        set a_down [expr -$amp/($per - $dty)]
        set b_down  $amp
      }
      for {set i 0} {$i < $nof_samples} {incr i} {
        if {$p >= $per} {set p [expr $p - $per]}
        if {$p < $dty} {
          lappend waveform [expr $ofs + $b_up   + $a_up   * $p]
        } else {
          lappend waveform [expr $ofs + $b_down + $a_down * $p]
        }
        set p [expr $p + 1]
      }
    }
    block {
      set q_high [expr $ofs + $amp]
      set q_low  [expr $ofs]
      for {set i 0} {$i < $nof_samples} {incr i} {
        if {$p >= $per} {set p [expr $p - $per]}
        if {$p < $dty} {
          lappend waveform $q_high
        } else {
          lappend waveform $q_low
        }
        set p [expr $p + 1]
      }
    }
    uniform {
      expr srand(int($phs))
      for {set i 0} {$i < $nof_samples} {incr i} {
        lappend waveform [expr $ofs + $amp - 2*$amp*rand()]
      }
    }
    default {}
  }
  return $waveform
}


###############################################################################
#
# WG_CALCULATE_NEXT_SEQUENCE_VALUE : Calculate next sequence value
#
# Input:
#
# - in_word   = shift register seed
# - seq       = PSRG    : use PSRG sequence as in RCU (width = 12)
#               others  : use COUNTER sequence
#
# Return:
#
# - out_word  = shift register contents after one cycle
#
proc wg_calculate_next_sequence_value {in_word {seq PSRG} {width 12}} {
  if {[string equal $seq PSRG] == 1} {
    # ----- RCU PSRG sequence -----

    # Polynoom
    set w [expr int(pow(2,$width))-1]
    set taps  {0 3 5 11}

    # Feedback shift register
    set p 0
    foreach t $taps {
      set b  [expr int(pow(2,$t))]
      set b  [expr !!($in_word & $b)]
      set p  [expr $p ^ $b]
    }
    set out_word [expr (($in_word << 1) & $w) + !$p]
  } elseif {[string equal $seq COUNTER] == 1} {
    # ----- RCU COUNTER sequence -----

    set w [expr int(pow(2,$width))-1]
    set out_word [expr ($in_word+1) & $w]
  }
  return $out_word
}


###############################################################################
#
# WG_SDO_GENERATE_MAP : Generate the SDO-SS map for an AP frame.
#
# Description:
# . Support various different subband data in the 4 lane frames (LF). The
#   subband data  in the lane frames depends on the data_mode. The data modes
#   assume that the default incrementing counter data [0:1023] is in WG
#   waveform X and Y and that the DSP data path blocks that preceed the SDO-SS
#   are bypassed.
# . This function is equivaluent to func_sdo_ss_generate_map() in
#   tb_sdo_pkg.vhd.
#
# Input:
#
# - ap_id     = AP index number 0,1,2,3
# - value     = data value in case mode = "CONSTANT"
# - data_mode = "RCU"        : use RCU index 0:7 as LF[0:3] data
#             = "SUBBAND"    : use subband index 0:35 as LF[0:3] data
#             = "LANE"       : use lane index 0:3 as LF[0:3] data
#             = "SS_MAP"     : use this SS map index as LF[0:3] data
#             = "COUNT_UP"   : count up as LF[0:3] data
#             = "COUNT_DOWN" : count down as LF[0:3] data
#             = "CONSTANT"   : constant value as LF[0:3] data
#
# Return:
#
# - ss_map = SDO-SS mapping
#
proc wg_sdo_ss_generate_map {ap_id value {data_mode CONSTANT}} {
  global env
  source "$env(RSP)/rsp/tb/tc/5. Datapath/constants.tcl"

  set ss_map {}
  for {set i 0} {$i < $c_sdo_ss_nof_subbands_ap} {incr i} {
    lappend ss_map 0
  } ;# lrepeat yields invalid command name error
    
  set vKmax $c_sdo_ss_nof_subbands_rsp
  
  for {set vLane 0} {$vLane < $c_rsp_nof_lanes} {incr vLane} {
    for {set vSub 0} {$vSub < $c_rsp_sdo_nof_subbands_per_lane} {incr vSub} {
      for {set vAp 0} {$vAp < $c_rsp_nof_ap} {incr vAp} {
        for {set vPol 0} {$vPol < $c_pol} {incr vPol} {
          # logical index
          set vRcu     [expr $vAp*$c_pol + $vPol]
          set vSubband [expr $vLane*$c_rsp_sdo_nof_subbands_per_lane + $vSub]
          set vK       [expr ($vSubband*$c_rsp_nof_ap + $vAp)*$c_pol + $vPol]
            
          # location in ss_map
          set vLP_band [expr $vSub*$c_pol + $vPol]
          set vAP_band [expr $vLane*$c_rsp_sdo_nof_subbands_per_lane*$c_pol + $vLP_band]
          
          set vB $vAP_band
          if {$vAp==$ap_id} {
            if {$data_mode=="rcu"       } {set ss_map [lreplace $ss_map $vB $vB $vRcu              ]}
            if {$data_mode=="subband"   } {set ss_map [lreplace $ss_map $vB $vB $vSubband          ]}
            if {$data_mode=="lane"      } {set ss_map [lreplace $ss_map $vB $vB $vLane             ]}
            if {$data_mode=="ss_map"    } {set ss_map [lreplace $ss_map $vB $vB $vAP_band          ]}
            if {$data_mode=="count_up"  } {set ss_map [lreplace $ss_map $vB $vB $vK                ]}
            if {$data_mode=="count_down"} {set ss_map [lreplace $ss_map $vB $vB [expr $vKmax - $vK]]}
            if {$data_mode=="constant"  } {set ss_map [lreplace $ss_map $vB $vB $value             ]}
          }
          set vK [expr $vK + $c_rsp_nof_rcu]
        }
      }
    }
  }
  return $ss_map
}

###############################################################################
#
# WG_WRITE_FILE : Write signal to a file
#
# Input:
#
# - fname      = Signal name, STRING
# - sig        = Signal values, INTEGERs
#
# Return: void
#
#
proc wg_write_file {fname sig} {
  set chan [open "$fname.sig" w]
  foreach i $sig {puts $chan $i}
  close $chan
}


###############################################################################
#
# WG_READ_FILE : Read signal from a file
#
# Input:
#
# - fname      = Signal name, STRING
#
# Return:
#
# - sig        = Signal values
#
proc wg_read_file {fname} {
  set sig {}
  set chan [open "$fname" r]
  while {[gets $chan line] >= 0} {
    lappend sig $line
  }
  close $chan
  return $sig
}


###############################################################################
#
# WG_FLIP : Flip a list
#
# Input:
#
# - inp      = Input list inp[0:n-1]
#
# Return:
#
# - outp     = inp[n-1:0]
#
proc wg_flip {inp} {
  set outp $inp
  set n    [llength $inp]
  set i    0
  foreach e $inp {
    lset outp [expr $n-1-$i] $e
    incr i
  }
  return $outp
}


###############################################################################
#
# WG_TRANSPOSE : Transpose a list that with m x n elements to list with n x m elements
#
# Input:
#
# - m        = Number of rows
# - n        = Number of colums
# - inp      = Input list inp[(0,0:m-1):(n-1,0:m-1)
#
# Return:
#
# - outp     = inp[(0:n-1,0):(m-1:n-1,0)]
#
proc wg_transpose {inp m n} {
  set outp $inp
  set j    0    ;# row index
  set i    0    ;# column index
  foreach e $inp {
    lset outp [expr $i+$j*$n] $e
    if {$j < $m-1} {
      incr j
    } else {
      set  j 0
      incr i
    }
  }
  return $outp
}


###############################################################################
#
# WG_CLIP : Clip an integer value to w bits
#
# Input:
#
# - inp      = Integer value
# - w        = Output width in number of bits
#
# Description: Output range -2**(w-1) to +2**(w-1)-1
#
# Return:
#
# - outp     = Clipped value
#
proc wg_clip {inp w} {
  set outp 0
  if {$w>0} {
    set clip_p [expr  int(pow(2,($w-1)))-1]
    set clip_n [expr -int(pow(2,($w-1)))  ]
    if {$inp > $clip_p} {
      set outp $clip_p
    } elseif {$inp < $clip_n} {
      set outp $clip_n
    } else {
      set outp $inp
    }
  }
  return $outp
}


###############################################################################
#
# WG_WRAP : Wrap an integer value to w bits
#
# Input:
#
# - inp      = Integer value
# - w        = Output width in number of bits
#
# Description: Remove MSbits, output range -2**(w-1) to +2**(w-1)-1
#
# Return:
#
# - outp     = Wrapped value
#
proc wg_wrap {inp w} {
  set outp 0
  if {$w>0} {
    set wrap_mask [expr int(pow(2,($w-1)))-1]
    set wrap_sign [expr int(pow(2,($w-1)))]
    if {($inp & $wrap_sign) == 0} {
      set outp [expr  $inp & $wrap_mask]
    } else {
      set outp [expr ($inp & $wrap_mask) - $wrap_sign]
    }
  }
  return $outp
}


###############################################################################
#
# WG_ROUND : Round the w LSbits of an integer value
#
# Input:
#
# - inp       = Integer value
# - w         = Number of LSbits to round
# - direction = "HALF_AWAY", "HALF_UP"
#
# Description:
#   direction = "HALF_AWAY" --> Round half away from zero so +0.5 --> 1, -0.5 --> -1.
#   direction = "HALF_UP"   --> Round half to +infinity   so +0.5 --> 1, -0.5 --> 0.
# Return:
#
# - outp     = Rounded value
#
proc wg_round {inp w {direction "HALF_AWAY"}} {
  set outp $inp
  if {$w>0} {
    set round_factor [expr int(pow(2,$w))]
    set round_p [expr int(pow(2,($w-1)))  ]
    set round_n [expr int(pow(2,($w-1)))-1]
    if {$direction == "HALF_UP"} {
      set outp [expr ($inp+$round_p)/$round_factor]
    }
    if {$direction == "HALF_AWAY"} {
      if {$inp >= 0} {
        set outp [expr ($inp+$round_p)/$round_factor]
      } else {
        set outp [expr ($inp+$round_n)/$round_factor]
      }
    }
  }
  return $outp
}


###############################################################################
#
# WG_TRUNCATE : Truncate the w LSbits of an integer value
#
# Input:
#
# - inp      = Integer value
# - w        = Number of LSbits to truncate
#
# Description: Remove LSBits.
# Return:
#
# - outp     = Truncated value
#
proc wg_truncate {inp w} {
  set outp $inp
  if {$w>0} {
    if {$inp >= 0} {
      set outp [expr $inp>>$w]
    } else {
      set outp [expr -((-$inp)>>$w)]
    }
  }
  return $outp
}


###############################################################################
#
# WG_WAIT : Wait some time
#
# Input:
#
# - t  =  wait time in msec
#
# Return: void
#
proc wg_wait {t} {
#   set c [clock clicks -milliseconds]
#   while {[clock clicks -milliseconds] < $c + $t} {}
  after [expr int(ceil($t))]
}
