-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Capture internal STD_LOGIC signal at double data rate
-- Description: See common_ddreg.vhd
-- Remark:

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_ddreg_slv is
  generic (
    g_in_delay_len  : positive := c_meta_delay_len;
    g_out_delay_len : positive := c_meta_delay_len
  );
  port (
    in_clk      : in  std_logic;
    in_dat      : in  std_logic_vector;
    rst         : in  std_logic := '0';
    out_clk     : in  std_logic;
    out_dat_hi  : out std_logic_vector;
    out_dat_lo  : out std_logic_vector
  );
end common_ddreg_slv;

architecture str of common_ddreg_slv is
begin
  gen_slv: for I in in_dat'range generate
    u_ddreg : entity work.common_ddreg
    generic map (
      g_in_delay_len  => g_in_delay_len,
      g_out_delay_len => g_out_delay_len
    )
    port map (
      in_clk      => in_clk,
      in_dat      => in_dat(I),
      rst         => rst,
      out_clk     => out_clk,
      out_dat_hi  => out_dat_hi(I),
      out_dat_lo  => out_dat_lo(I)
    );
  end generate;
end str;
