-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Capture ADC samples from ADU on a BN

library IEEE, common_lib, unb1_board_lib, dp_lib, ppsh_lib, eth_lib, tech_tse_lib, aduh_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use eth_lib.eth_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use aduh_lib.aduh_dd_pkg.all;
use work.unb1_bn_capture_pkg.all;

entity unb1_bn_capture is
  generic (
    -- General
    g_sim         : boolean := false;
    g_design_name : string  := "unb1_bn_capture";
    -- Stamps are passed via QIP at compile start if $UNB_COMPILE_STAMPS is set
    g_stamp_date  : natural := 0;  -- Date (YYYYMMDD)
    g_stamp_time  : natural := 0;  -- Time (HHMMSS)
    g_stamp_svn   : natural := 0;  -- SVN revision
    -- Development note    := "<---------max 32 chars--------->"
    g_design_note : string := "ADUH 6 phs clocks; dp_shiftram"
  );
  port (
    -- GENERAL
    CLK                    : in    std_logic;  -- System Clock
    PPS                    : in    std_logic;  -- System Sync
    WDI                    : out   std_logic;  -- Watchdog Clear
    INTA                   : inout std_logic;  -- FPGA interconnect line
    INTB                   : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION                : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID                     : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO                 : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc                : inout std_logic;
    sens_sd                : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk                : in    std_logic;
    ETH_SGIN               : in    std_logic;
    ETH_SGOUT              : out   std_logic;

    -- ADC Interface
    ADC_BI_A               : in    std_logic_vector(c_aduh_dd_ai.port_w - 1 downto 0);
    ADC_BI_B               : in    std_logic_vector(c_aduh_dd_ai.port_w - 1 downto 0);
    ADC_BI_A_CLK           : in    std_logic;  -- use ADC_BI_A_CLK for port A and B, the ADC_BI_C_CLK is NC
    ADC_BI_A_CLK_RST       : out   std_logic;  -- differential: via _P = ADC_SDL[1], _N = ADC_SCL[1] gets inferred
    ADC_BI_C               : in    std_logic_vector(c_aduh_dd_ai.port_w - 1 downto 0);
    ADC_BI_D               : in    std_logic_vector(c_aduh_dd_ai.port_w - 1 downto 0);
    ADC_BI_D_CLK           : in    std_logic;  -- use ADC_BI_D_CLK for port C and D, the ADC_BI_B_CLK is NC
    ADC_BI_D_CLK_RST       : out   std_logic;  -- differential: via _P = ADC_SDL[2], _N = ADC_SCL[2] gets inferred
    ADC_AB_SCL             : inout std_logic;  -- = ADC_SCL[0]
    ADC_AB_SDA             : inout std_logic;  -- = ADC_SDA[0]
    ADC_CD_SCL             : inout std_logic;  -- = ADC_SCL[3]
    ADC_CD_SDA             : inout std_logic  -- = ADC_SDA[3]
  );
end unb1_bn_capture;

architecture str of unb1_bn_capture is
  constant c_fw_version             : t_unb1_board_fw_version := (1, 12);  -- firmware version x.y
  constant c_use_phy                : t_c_unb1_board_use_phy := (1, 0, 0, 0, 0, 0, 1, 1);
  constant c_dp_clk_use_pll         : boolean := true;
  constant c_nof_dp_phs_clk         : natural := 6;  -- nof dp_phs_clk that can be used to detect the word phase, must be <= 6
  constant c_bn_capture             : t_c_bn_capture := c_bn_capture;
  constant c_ai                     : t_c_aduh_dd_ai := c_aduh_dd_ai;
  constant c_reg_dp_shiftram_adr_w  : natural := 3;

  signal this_chip_id               : std_logic_vector(c_unb1_board_nof_chip_w - 1 downto 0);  -- [2:0], so range 0-3 for FN and range 4-7 BN

  -- System
  signal cs_sim                     : std_logic;
  signal xo_clk                     : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_locked                  : std_logic;
  signal mm_rst                     : std_logic;

  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;
  signal dp_phs_clk_vec             : std_logic_vector(c_nof_dp_phs_clk - 1 downto 0);
  signal dp_pps                     : std_logic;

  -- PIOs
  signal pout_debug_wave            : std_logic_vector(c_word_w - 1 downto 0);
  signal pout_wdi                   : std_logic;
  signal pin_intab                  : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
  signal pout_intab                 : std_logic_vector(c_word_w - 1 downto 0);
  signal pout_phy_disable           : std_logic_vector(c_word_w - 1 downto 0);

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;  -- mms_unb_sens registers
  signal reg_unb_sens_miso          : t_mem_miso;

  -- eth1g
  signal eth1g_tse_clk              : std_logic;
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- MM bsn source
  signal reg_bsn_source_mosi        : t_mem_mosi;  -- Start a BSN timestamp
  signal reg_bsn_source_miso        : t_mem_miso;

  -- MM bsn scheduler for WG
  signal reg_bsn_scheduler_wg_mosi  : t_mem_mosi;  -- Schedule WG restart at a BSN, read current BSN
  signal reg_bsn_scheduler_wg_miso  : t_mem_miso;

  -- MM aduh quad
  signal reg_adc_quad_mosi          : t_mem_mosi;  -- ADUH locked status and pattern verify for two half ADUs so 4 ADC inputs A, B, C and D
  signal reg_adc_quad_miso          : t_mem_miso;

  -- MM wideband waveform generator registers [0,1,2,3] for signal paths [A,B,C,D]
  signal reg_wg_mosi_arr            : t_mem_mosi_arr(0 to c_ai.nof_sp - 1);  -- = [0:3] = Waveform Generator control ports [A,B,C,D]
  signal reg_wg_miso_arr            : t_mem_miso_arr(0 to c_ai.nof_sp - 1);
  signal ram_wg_mosi_arr            : t_mem_mosi_arr(0 to c_ai.nof_sp - 1);  -- = [0:3] = Waveform Generator waveform buffer ports [A,B,C,D]
  signal ram_wg_miso_arr            : t_mem_miso_arr(0 to c_ai.nof_sp - 1);

  -- MM DP shiftram
  signal reg_dp_shiftram_mosi       : t_mem_mosi;
  signal reg_dp_shiftram_miso       : t_mem_miso;

  -- MM signal path monitors for [A, B, C, D]
  signal reg_mon_mosi_arr           : t_mem_mosi_arr(0 to c_ai.nof_sp - 1);  -- = [0:3] = Read only access to the data mean sums and power sums [A,B,C,D]
  signal reg_mon_miso_arr           : t_mem_miso_arr(0 to c_ai.nof_sp - 1);
  signal ram_mon_mosi_arr           : t_mem_mosi_arr(0 to c_ai.nof_sp - 1);  -- = [0:3] = Read only access to the data monitor buffers [A,B,C,D]
  signal ram_mon_miso_arr           : t_mem_miso_arr(0 to c_ai.nof_sp - 1);

  -- MM registers [0,1] for I2C access with ADU AB and with ADU CD
  signal reg_commander_mosi_arr     : t_mem_mosi_arr(0 to c_ai.nof_adu - 1);
  signal reg_commander_miso_arr     : t_mem_miso_arr(0 to c_ai.nof_adu - 1);
  signal ram_protocol_mosi_arr      : t_mem_mosi_arr(0 to c_ai.nof_adu - 1);
  signal ram_protocol_miso_arr      : t_mem_miso_arr(0 to c_ai.nof_adu - 1);
  signal ram_result_mosi_arr        : t_mem_mosi_arr(0 to c_ai.nof_adu - 1);
  signal ram_result_miso_arr        : t_mem_miso_arr(0 to c_ai.nof_adu - 1);

  signal reg_bsn_scheduler_sp_on_mosi  : t_mem_mosi;
  signal reg_bsn_scheduler_sp_on_miso  : t_mem_miso;
  signal reg_bsn_scheduler_sp_off_mosi  : t_mem_mosi;
  signal reg_bsn_scheduler_sp_off_miso  : t_mem_miso;
begin
  -----------------------------------------------------------------------------
  -- SOPC system
  -----------------------------------------------------------------------------

  u_sopc : entity work.sopc_unb1_bn_capture
  port map (
    -- 1) global signals:
    clk_0                                         => xo_clk,  -- PLL reference = 25 MHz from ETH_clk pin
    reset_n                                       => xo_rst_n,
    mm_clk                                        => mm_clk,  -- PLL clk[0] = 50 MHz system clock that the NIOS2 and the MM bus run on
    cal_clk                                       => OPEN,  -- PLL clk[1] = 40 MHz calibration clock for the IO reconfiguration
    tse_clk                                       => eth1g_tse_clk,  -- PLL clk[2] = 125 MHz calibration clock for the TSE

    -- the_altpll_0
    locked_from_the_altpll_0                      => mm_locked,
    phasedone_from_the_altpll_0                   => OPEN,
    areset_to_the_altpll_0                        => xo_rst,

    -- the_avs_eth_0
    coe_clk_export_from_the_avs_eth_0             => OPEN,
    coe_reset_export_from_the_avs_eth_0           => eth1g_mm_rst,
    coe_tse_address_export_from_the_avs_eth_0     => eth1g_tse_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
    coe_tse_write_export_from_the_avs_eth_0       => eth1g_tse_mosi.wr,
    coe_tse_writedata_export_from_the_avs_eth_0   => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
    coe_tse_read_export_from_the_avs_eth_0        => eth1g_tse_mosi.rd,
    coe_tse_readdata_export_to_the_avs_eth_0      => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
    coe_tse_waitrequest_export_to_the_avs_eth_0   => eth1g_tse_miso.waitrequest,
    coe_reg_address_export_from_the_avs_eth_0     => eth1g_reg_mosi.address(c_eth_reg_addr_w - 1 downto 0),
    coe_reg_write_export_from_the_avs_eth_0       => eth1g_reg_mosi.wr,
    coe_reg_writedata_export_from_the_avs_eth_0   => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
    coe_reg_read_export_from_the_avs_eth_0        => eth1g_reg_mosi.rd,
    coe_reg_readdata_export_to_the_avs_eth_0      => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
    coe_irq_export_to_the_avs_eth_0               => eth1g_reg_interrupt,
    coe_ram_address_export_from_the_avs_eth_0     => eth1g_ram_mosi.address(c_eth_ram_addr_w - 1 downto 0),
    coe_ram_write_export_from_the_avs_eth_0       => eth1g_ram_mosi.wr,
    coe_ram_writedata_export_from_the_avs_eth_0   => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
    coe_ram_read_export_from_the_avs_eth_0        => eth1g_ram_mosi.rd,
    coe_ram_readdata_export_to_the_avs_eth_0      => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),

    -- the_reg_unb_sens
    coe_clk_export_from_the_reg_unb_sens                => OPEN,
    coe_reset_export_from_the_reg_unb_sens              => OPEN,
    coe_address_export_from_the_reg_unb_sens            => reg_unb_sens_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_unb_sens               => reg_unb_sens_mosi.rd,
    coe_readdata_export_to_the_reg_unb_sens             => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_unb_sens              => reg_unb_sens_mosi.wr,
    coe_writedata_export_from_the_reg_unb_sens          => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_bsn_source
    coe_clk_export_from_the_reg_bsn_source              => OPEN,
    coe_reset_export_from_the_reg_bsn_source            => OPEN,
    coe_address_export_from_the_reg_bsn_source          => reg_bsn_source_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_bsn_source_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_bsn_source             => reg_bsn_source_mosi.rd,
    coe_readdata_export_to_the_reg_bsn_source           => reg_bsn_source_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_bsn_source            => reg_bsn_source_mosi.wr,
    coe_writedata_export_from_the_reg_bsn_source        => reg_bsn_source_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_bsn_scheduler_wg
    coe_clk_export_from_the_reg_bsn_scheduler_wg        => OPEN,
    coe_reset_export_from_the_reg_bsn_scheduler_wg      => OPEN,
    coe_address_export_from_the_reg_bsn_scheduler_wg    => reg_bsn_scheduler_wg_mosi.address(0),  -- reg_bsn_scheduler_adr_w = 1
    coe_read_export_from_the_reg_bsn_scheduler_wg       => reg_bsn_scheduler_wg_mosi.rd,
    coe_readdata_export_to_the_reg_bsn_scheduler_wg     => reg_bsn_scheduler_wg_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_bsn_scheduler_wg      => reg_bsn_scheduler_wg_mosi.wr,
    coe_writedata_export_from_the_reg_bsn_scheduler_wg  => reg_bsn_scheduler_wg_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_adc_quad
    coe_clk_export_from_the_reg_adc_quad                => OPEN,
    coe_reset_export_from_the_reg_adc_quad              => OPEN,
    coe_address_export_from_the_reg_adc_quad            => reg_adc_quad_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_adc_quad_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_adc_quad               => reg_adc_quad_mosi.rd,
    coe_readdata_export_to_the_reg_adc_quad             => reg_adc_quad_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_adc_quad              => reg_adc_quad_mosi.wr,
    coe_writedata_export_from_the_reg_adc_quad          => reg_adc_quad_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_diag_wg_0
    coe_clk_export_from_the_reg_diag_wg_0               => OPEN,
    coe_reset_export_from_the_reg_diag_wg_0             => OPEN,
    coe_address_export_from_the_reg_diag_wg_0           => reg_wg_mosi_arr(0).address(c_unb1_board_peripherals_mm_reg_default.reg_diag_wg_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_diag_wg_0              => reg_wg_mosi_arr(0).rd,
    coe_readdata_export_to_the_reg_diag_wg_0            => reg_wg_miso_arr(0).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_diag_wg_0             => reg_wg_mosi_arr(0).wr,
    coe_writedata_export_from_the_reg_diag_wg_0         => reg_wg_mosi_arr(0).wrdata(c_word_w - 1 downto 0),

    -- the_reg_diag_wg_1
    coe_clk_export_from_the_reg_diag_wg_1               => OPEN,
    coe_reset_export_from_the_reg_diag_wg_1             => OPEN,
    coe_address_export_from_the_reg_diag_wg_1           => reg_wg_mosi_arr(1).address(c_unb1_board_peripherals_mm_reg_default.reg_diag_wg_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_diag_wg_1              => reg_wg_mosi_arr(1).rd,
    coe_readdata_export_to_the_reg_diag_wg_1            => reg_wg_miso_arr(1).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_diag_wg_1             => reg_wg_mosi_arr(1).wr,
    coe_writedata_export_from_the_reg_diag_wg_1         => reg_wg_mosi_arr(1).wrdata(c_word_w - 1 downto 0),

    -- the_reg_diag_wg_2
    coe_clk_export_from_the_reg_diag_wg_2               => OPEN,
    coe_reset_export_from_the_reg_diag_wg_2             => OPEN,
    coe_address_export_from_the_reg_diag_wg_2           => reg_wg_mosi_arr(2).address(c_unb1_board_peripherals_mm_reg_default.reg_diag_wg_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_diag_wg_2              => reg_wg_mosi_arr(2).rd,
    coe_readdata_export_to_the_reg_diag_wg_2            => reg_wg_miso_arr(2).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_diag_wg_2             => reg_wg_mosi_arr(2).wr,
    coe_writedata_export_from_the_reg_diag_wg_2         => reg_wg_mosi_arr(2).wrdata(c_word_w - 1 downto 0),

    -- the_reg_diag_wg_3
    coe_clk_export_from_the_reg_diag_wg_3               => OPEN,
    coe_reset_export_from_the_reg_diag_wg_3             => OPEN,
    coe_address_export_from_the_reg_diag_wg_3           => reg_wg_mosi_arr(3).address(c_unb1_board_peripherals_mm_reg_default.reg_diag_wg_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_diag_wg_3              => reg_wg_mosi_arr(3).rd,
    coe_readdata_export_to_the_reg_diag_wg_3            => reg_wg_miso_arr(3).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_diag_wg_3             => reg_wg_mosi_arr(3).wr,
    coe_writedata_export_from_the_reg_diag_wg_3         => reg_wg_mosi_arr(3).wrdata(c_word_w - 1 downto 0),

    -- the_ram_diag_wg_0
    coe_clk_export_from_the_ram_diag_wg_0               => OPEN,
    coe_reset_export_from_the_ram_diag_wg_0             => OPEN,
    coe_address_export_from_the_ram_diag_wg_0           => ram_wg_mosi_arr(0).address(c_unb1_board_peripherals_mm_reg_default.ram_diag_wg_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_diag_wg_0              => ram_wg_mosi_arr(0).rd,
    coe_readdata_export_to_the_ram_diag_wg_0            => ram_wg_miso_arr(0).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_diag_wg_0             => ram_wg_mosi_arr(0).wr,
    coe_writedata_export_from_the_ram_diag_wg_0         => ram_wg_mosi_arr(0).wrdata(c_word_w - 1 downto 0),

    -- the_ram_diag_wg_1
    coe_clk_export_from_the_ram_diag_wg_1               => OPEN,
    coe_reset_export_from_the_ram_diag_wg_1             => OPEN,
    coe_address_export_from_the_ram_diag_wg_1           => ram_wg_mosi_arr(1).address(c_unb1_board_peripherals_mm_reg_default.ram_diag_wg_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_diag_wg_1              => ram_wg_mosi_arr(1).rd,
    coe_readdata_export_to_the_ram_diag_wg_1            => ram_wg_miso_arr(1).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_diag_wg_1             => ram_wg_mosi_arr(1).wr,
    coe_writedata_export_from_the_ram_diag_wg_1         => ram_wg_mosi_arr(1).wrdata(c_word_w - 1 downto 0),

    -- the_ram_diag_wg_2
    coe_clk_export_from_the_ram_diag_wg_2               => OPEN,
    coe_reset_export_from_the_ram_diag_wg_2             => OPEN,
    coe_address_export_from_the_ram_diag_wg_2           => ram_wg_mosi_arr(2).address(c_unb1_board_peripherals_mm_reg_default.ram_diag_wg_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_diag_wg_2              => ram_wg_mosi_arr(2).rd,
    coe_readdata_export_to_the_ram_diag_wg_2            => ram_wg_miso_arr(2).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_diag_wg_2             => ram_wg_mosi_arr(2).wr,
    coe_writedata_export_from_the_ram_diag_wg_2         => ram_wg_mosi_arr(2).wrdata(c_word_w - 1 downto 0),

    -- the_ram_diag_wg_3
    coe_clk_export_from_the_ram_diag_wg_3               => OPEN,
    coe_reset_export_from_the_ram_diag_wg_3             => OPEN,
    coe_address_export_from_the_ram_diag_wg_3           => ram_wg_mosi_arr(3).address(c_unb1_board_peripherals_mm_reg_default.ram_diag_wg_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_diag_wg_3              => ram_wg_mosi_arr(3).rd,
    coe_readdata_export_to_the_ram_diag_wg_3            => ram_wg_miso_arr(3).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_diag_wg_3             => ram_wg_mosi_arr(3).wr,
    coe_writedata_export_from_the_ram_diag_wg_3         => ram_wg_mosi_arr(3).wrdata(c_word_w - 1 downto 0),

    -- the_reg_dp_shiftram
    coe_clk_export_from_the_reg_dp_shiftram             => OPEN,
    coe_reset_export_from_the_reg_dp_shiftram           => OPEN,
    coe_address_export_from_the_reg_dp_shiftram         => reg_dp_shiftram_mosi.address(c_reg_dp_shiftram_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_dp_shiftram            => reg_dp_shiftram_mosi.rd,
    coe_readdata_export_to_the_reg_dp_shiftram          => reg_dp_shiftram_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_dp_shiftram           => reg_dp_shiftram_mosi.wr,
    coe_writedata_export_from_the_reg_dp_shiftram       => reg_dp_shiftram_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_aduh_mon_0
    coe_clk_export_from_the_reg_aduh_mon_0              => OPEN,
    coe_reset_export_from_the_reg_aduh_mon_0            => OPEN,
    coe_address_export_from_the_reg_aduh_mon_0          => reg_mon_mosi_arr(0).address(c_unb1_board_peripherals_mm_reg_default.reg_aduh_mon_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_aduh_mon_0             => reg_mon_mosi_arr(0).rd,
    coe_readdata_export_to_the_reg_aduh_mon_0           => reg_mon_miso_arr(0).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_aduh_mon_0            => reg_mon_mosi_arr(0).wr,
    coe_writedata_export_from_the_reg_aduh_mon_0        => reg_mon_mosi_arr(0).wrdata(c_word_w - 1 downto 0),

    -- the_reg_aduh_mon_1
    coe_clk_export_from_the_reg_aduh_mon_1              => OPEN,
    coe_reset_export_from_the_reg_aduh_mon_1            => OPEN,
    coe_address_export_from_the_reg_aduh_mon_1          => reg_mon_mosi_arr(1).address(c_unb1_board_peripherals_mm_reg_default.reg_aduh_mon_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_aduh_mon_1             => reg_mon_mosi_arr(1).rd,
    coe_readdata_export_to_the_reg_aduh_mon_1           => reg_mon_miso_arr(1).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_aduh_mon_1            => reg_mon_mosi_arr(1).wr,
    coe_writedata_export_from_the_reg_aduh_mon_1        => reg_mon_mosi_arr(1).wrdata(c_word_w - 1 downto 0),

    -- the_reg_aduh_mon_2
    coe_clk_export_from_the_reg_aduh_mon_2              => OPEN,
    coe_reset_export_from_the_reg_aduh_mon_2            => OPEN,
    coe_address_export_from_the_reg_aduh_mon_2          => reg_mon_mosi_arr(2).address(c_unb1_board_peripherals_mm_reg_default.reg_aduh_mon_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_aduh_mon_2             => reg_mon_mosi_arr(2).rd,
    coe_readdata_export_to_the_reg_aduh_mon_2           => reg_mon_miso_arr(2).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_aduh_mon_2            => reg_mon_mosi_arr(2).wr,
    coe_writedata_export_from_the_reg_aduh_mon_2        => reg_mon_mosi_arr(2).wrdata(c_word_w - 1 downto 0),

    -- the_reg_aduh_mon_3
    coe_clk_export_from_the_reg_aduh_mon_3              => OPEN,
    coe_reset_export_from_the_reg_aduh_mon_3            => OPEN,
    coe_address_export_from_the_reg_aduh_mon_3          => reg_mon_mosi_arr(3).address(c_unb1_board_peripherals_mm_reg_default.reg_aduh_mon_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_aduh_mon_3             => reg_mon_mosi_arr(3).rd,
    coe_readdata_export_to_the_reg_aduh_mon_3           => reg_mon_miso_arr(3).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_aduh_mon_3            => reg_mon_mosi_arr(3).wr,
    coe_writedata_export_from_the_reg_aduh_mon_3        => reg_mon_mosi_arr(3).wrdata(c_word_w - 1 downto 0),

    -- the_ram_aduh_mon_0
    coe_clk_export_from_the_ram_aduh_mon_0              => OPEN,
    coe_reset_export_from_the_ram_aduh_mon_0            => OPEN,
    coe_address_export_from_the_ram_aduh_mon_0          => ram_mon_mosi_arr(0).address(c_unb1_board_peripherals_mm_reg_default.ram_aduh_mon_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_aduh_mon_0             => ram_mon_mosi_arr(0).rd,
    coe_readdata_export_to_the_ram_aduh_mon_0           => ram_mon_miso_arr(0).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_aduh_mon_0            => ram_mon_mosi_arr(0).wr,
    coe_writedata_export_from_the_ram_aduh_mon_0        => ram_mon_mosi_arr(0).wrdata(c_word_w - 1 downto 0),

    -- the_ram_aduh_mon_1
    coe_clk_export_from_the_ram_aduh_mon_1              => OPEN,
    coe_reset_export_from_the_ram_aduh_mon_1            => OPEN,
    coe_address_export_from_the_ram_aduh_mon_1          => ram_mon_mosi_arr(1).address(c_unb1_board_peripherals_mm_reg_default.ram_aduh_mon_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_aduh_mon_1             => ram_mon_mosi_arr(1).rd,
    coe_readdata_export_to_the_ram_aduh_mon_1           => ram_mon_miso_arr(1).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_aduh_mon_1            => ram_mon_mosi_arr(1).wr,
    coe_writedata_export_from_the_ram_aduh_mon_1        => ram_mon_mosi_arr(1).wrdata(c_word_w - 1 downto 0),

    -- the_ram_aduh_mon_2
    coe_clk_export_from_the_ram_aduh_mon_2              => OPEN,
    coe_reset_export_from_the_ram_aduh_mon_2            => OPEN,
    coe_address_export_from_the_ram_aduh_mon_2          => ram_mon_mosi_arr(2).address(c_unb1_board_peripherals_mm_reg_default.ram_aduh_mon_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_aduh_mon_2             => ram_mon_mosi_arr(2).rd,
    coe_readdata_export_to_the_ram_aduh_mon_2           => ram_mon_miso_arr(2).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_aduh_mon_2            => ram_mon_mosi_arr(2).wr,
    coe_writedata_export_from_the_ram_aduh_mon_2        => ram_mon_mosi_arr(2).wrdata(c_word_w - 1 downto 0),

    -- the_ram_aduh_mon_3
    coe_clk_export_from_the_ram_aduh_mon_3              => OPEN,
    coe_reset_export_from_the_ram_aduh_mon_3            => OPEN,
    coe_address_export_from_the_ram_aduh_mon_3          => ram_mon_mosi_arr(3).address(c_unb1_board_peripherals_mm_reg_default.ram_aduh_mon_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_aduh_mon_3             => ram_mon_mosi_arr(3).rd,
    coe_readdata_export_to_the_ram_aduh_mon_3           => ram_mon_miso_arr(3).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_aduh_mon_3            => ram_mon_mosi_arr(3).wr,
    coe_writedata_export_from_the_ram_aduh_mon_3        => ram_mon_mosi_arr(3).wrdata(c_word_w - 1 downto 0),

    -- the_reg_adu_i2c_commander_ab
    coe_clk_export_from_the_reg_adu_i2c_commander_ab        => OPEN,
    coe_reset_export_from_the_reg_adu_i2c_commander_ab      => OPEN,
    coe_address_export_from_the_reg_adu_i2c_commander_ab    => reg_commander_mosi_arr(0).address(c_unb1_board_peripherals_mm_reg_default.reg_i2c_commander_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_adu_i2c_commander_ab       => reg_commander_mosi_arr(0).rd,
    coe_readdata_export_to_the_reg_adu_i2c_commander_ab     => reg_commander_miso_arr(0).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_adu_i2c_commander_ab      => reg_commander_mosi_arr(0).wr,
    coe_writedata_export_from_the_reg_adu_i2c_commander_ab  => reg_commander_mosi_arr(0).wrdata(c_word_w - 1 downto 0),

    -- the_reg_adu_i2c_commander_cd
    coe_clk_export_from_the_reg_adu_i2c_commander_cd        => OPEN,
    coe_reset_export_from_the_reg_adu_i2c_commander_cd      => OPEN,
    coe_address_export_from_the_reg_adu_i2c_commander_cd    => reg_commander_mosi_arr(1).address(c_unb1_board_peripherals_mm_reg_default.reg_i2c_commander_adr_w - 1 downto 0),
    coe_read_export_from_the_reg_adu_i2c_commander_cd       => reg_commander_mosi_arr(1).rd,
    coe_readdata_export_to_the_reg_adu_i2c_commander_cd     => reg_commander_miso_arr(1).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_adu_i2c_commander_cd      => reg_commander_mosi_arr(1).wr,
    coe_writedata_export_from_the_reg_adu_i2c_commander_cd  => reg_commander_mosi_arr(1).wrdata(c_word_w - 1 downto 0),

    -- the_ram_adu_i2c_protocol_ab
    coe_clk_export_from_the_ram_adu_i2c_protocol_ab         => OPEN,
    coe_reset_export_from_the_ram_adu_i2c_protocol_ab       => OPEN,
    coe_address_export_from_the_ram_adu_i2c_protocol_ab     => ram_protocol_mosi_arr(0).address(c_unb1_board_peripherals_mm_reg_default.ram_i2c_protocol_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_adu_i2c_protocol_ab        => ram_protocol_mosi_arr(0).rd,
    coe_readdata_export_to_the_ram_adu_i2c_protocol_ab      => ram_protocol_miso_arr(0).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_adu_i2c_protocol_ab       => ram_protocol_mosi_arr(0).wr,
    coe_writedata_export_from_the_ram_adu_i2c_protocol_ab   => ram_protocol_mosi_arr(0).wrdata(c_word_w - 1 downto 0),

    -- the_ram_adu_i2c_protocol_cd
    coe_clk_export_from_the_ram_adu_i2c_protocol_cd         => OPEN,
    coe_reset_export_from_the_ram_adu_i2c_protocol_cd       => OPEN,
    coe_address_export_from_the_ram_adu_i2c_protocol_cd     => ram_protocol_mosi_arr(1).address(c_unb1_board_peripherals_mm_reg_default.ram_i2c_protocol_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_adu_i2c_protocol_cd        => ram_protocol_mosi_arr(1).rd,
    coe_readdata_export_to_the_ram_adu_i2c_protocol_cd      => ram_protocol_miso_arr(1).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_adu_i2c_protocol_cd       => ram_protocol_mosi_arr(1).wr,
    coe_writedata_export_from_the_ram_adu_i2c_protocol_cd   => ram_protocol_mosi_arr(1).wrdata(c_word_w - 1 downto 0),

    -- the_ram_adu_i2c_result_ab
    coe_clk_export_from_the_ram_adu_i2c_result_ab           => OPEN,
    coe_reset_export_from_the_ram_adu_i2c_result_ab         => OPEN,
    coe_address_export_from_the_ram_adu_i2c_result_ab       => ram_result_mosi_arr(0).address(c_unb1_board_peripherals_mm_reg_default.ram_i2c_result_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_adu_i2c_result_ab          => ram_result_mosi_arr(0).rd,
    coe_readdata_export_to_the_ram_adu_i2c_result_ab        => ram_result_miso_arr(0).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_adu_i2c_result_ab         => ram_result_mosi_arr(0).wr,
    coe_writedata_export_from_the_ram_adu_i2c_result_ab     => ram_result_mosi_arr(0).wrdata(c_word_w - 1 downto 0),

    -- the_ram_adu_i2c_result_cd
    coe_clk_export_from_the_ram_adu_i2c_result_cd           => OPEN,
    coe_reset_export_from_the_ram_adu_i2c_result_cd         => OPEN,
    coe_address_export_from_the_ram_adu_i2c_result_cd       => ram_result_mosi_arr(1).address(c_unb1_board_peripherals_mm_reg_default.ram_i2c_result_adr_w - 1 downto 0),
    coe_read_export_from_the_ram_adu_i2c_result_cd          => ram_result_mosi_arr(1).rd,
    coe_readdata_export_to_the_ram_adu_i2c_result_cd        => ram_result_miso_arr(1).rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_ram_adu_i2c_result_cd         => ram_result_mosi_arr(1).wr,
    coe_writedata_export_from_the_ram_adu_i2c_result_cd     => ram_result_mosi_arr(1).wrdata(c_word_w - 1 downto 0),

    -- the_reg_bsn_scheduler_sp_on
    coe_clk_export_from_the_reg_bsn_scheduler_sp_on         => OPEN,
    coe_reset_export_from_the_reg_bsn_scheduler_sp_on       => OPEN,
    coe_address_export_from_the_reg_bsn_scheduler_sp_on     => reg_bsn_scheduler_sp_on_mosi.address(0),
    coe_read_export_from_the_reg_bsn_scheduler_sp_on        => reg_bsn_scheduler_sp_on_mosi.rd,
    coe_readdata_export_to_the_reg_bsn_scheduler_sp_on      => reg_bsn_scheduler_sp_on_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_bsn_scheduler_sp_on       => reg_bsn_scheduler_sp_on_mosi.wr,
    coe_writedata_export_from_the_reg_bsn_scheduler_sp_on   => reg_bsn_scheduler_sp_on_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_reg_bsn_scheduler_sp_off
    coe_clk_export_from_the_reg_bsn_scheduler_sp_off         => OPEN,
    coe_reset_export_from_the_reg_bsn_scheduler_sp_off       => OPEN,
    coe_address_export_from_the_reg_bsn_scheduler_sp_off     => reg_bsn_scheduler_sp_off_mosi.address(0),
    coe_read_export_from_the_reg_bsn_scheduler_sp_off        => reg_bsn_scheduler_sp_off_mosi.rd,
    coe_readdata_export_to_the_reg_bsn_scheduler_sp_off      => reg_bsn_scheduler_sp_off_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_bsn_scheduler_sp_off       => reg_bsn_scheduler_sp_off_mosi.wr,
    coe_writedata_export_from_the_reg_bsn_scheduler_sp_off   => reg_bsn_scheduler_sp_off_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_pio_debug_wave
    out_port_from_the_pio_debug_wave                    => pout_debug_wave,

    -- the_pio_pps
    coe_clk_export_from_the_pio_pps               => OPEN,
    coe_reset_export_from_the_pio_pps             => OPEN,
    coe_address_export_from_the_pio_pps           => reg_ppsh_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1),  -- 1 bit address width so must use (0) instead of (0 DOWNTO 0)
    coe_read_export_from_the_pio_pps              => reg_ppsh_mosi.rd,
    coe_readdata_export_to_the_pio_pps            => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_pio_pps             => reg_ppsh_mosi.wr,
    coe_writedata_export_from_the_pio_pps         => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_pio_system_info: actually a avs_common_mm instance
    coe_clk_export_from_the_pio_system_info             => OPEN,
    coe_reset_export_from_the_pio_system_info           => OPEN,
    coe_address_export_from_the_pio_system_info         => reg_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
    coe_read_export_from_the_pio_system_info            => reg_unb_system_info_mosi.rd,
    coe_readdata_export_to_the_pio_system_info          => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_pio_system_info           => reg_unb_system_info_mosi.wr,
    coe_writedata_export_from_the_pio_system_info       => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_rom_system_info
    coe_clk_export_from_the_rom_system_info             => OPEN,
    coe_reset_export_from_the_rom_system_info           => OPEN,
    coe_address_export_from_the_rom_system_info         => rom_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
    coe_read_export_from_the_rom_system_info            => rom_unb_system_info_mosi.rd,
    coe_readdata_export_to_the_rom_system_info          => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_rom_system_info           => rom_unb_system_info_mosi.wr,
    coe_writedata_export_from_the_rom_system_info       => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

    -- the_pio_wdi
    out_port_from_the_pio_wdi                           => pout_wdi,

    -- the_reg_wdi
    coe_clk_export_from_the_reg_wdi                 => OPEN,
    coe_reset_export_from_the_reg_wdi               => OPEN,
    coe_address_export_from_the_reg_wdi             => reg_wdi_mosi.address(0),
    coe_read_export_from_the_reg_wdi                => reg_wdi_mosi.rd,
    coe_readdata_export_to_the_reg_wdi              => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
    coe_write_export_from_the_reg_wdi               => reg_wdi_mosi.wr,
    coe_writedata_export_from_the_reg_wdi           => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0)
  );

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------

--  u_ctrl : ENTITY unb_common_lib.ctrl_unb_common
--  GENERIC MAP (
--    -- General
--    g_sim              => g_sim,
--    g_design_name      => g_design_name,
--    g_fw_version       => g_fw_version,
--    g_stamp_date       => g_stamp_date,
--    g_stamp_time       => g_stamp_time,
--    g_stamp_svn        => g_stamp_svn,
--    g_design_note      => g_design_note,
--    g_mm_clk_freq      => g_bn_capture.mm_clk_freq,   -- must match PLL setting in sopc_bn_capture
--    g_dp_clk_freq      => g_bn_capture.dp_clk_freq,
--    g_dp_phs_clk_vec_w => g_nof_dp_phs_clk,
--    -- Use PHY Interface
--    g_use_phy          => g_use_phy,
--    -- Auxiliary Interface
--    g_aux              => g_aux
--  )

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim                     => g_sim,
    g_design_name             => g_design_name,
    g_design_note             => g_design_note,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_stamp_svn               => g_stamp_svn,
    g_fw_version              => c_fw_version,
    g_mm_clk_freq             => c_unb1_board_mm_clk_freq_50M,
    g_dp_phs_clk_vec_w        => c_nof_dp_phs_clk,
    g_dp_clk_use_pll          => c_dp_clk_use_pll,
    g_use_phy                 => c_use_phy,
    g_aux                     => c_unb1_board_aux
  )
  port map (
    -- System
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,
    dp_phs_clk_vec           => dp_phs_clk_vec,  -- divided and phase shifted dp_clk

    this_chip_id             => this_chip_id,

     -- PIOs
    pout_debug_wave          => pout_debug_wave,
    pout_wdi                 => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- ppsh
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,  -- 125 MHz from xo_clk PLL in SOPC system
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    sens_sc                  => sens_sc,
    sens_sd                  => sens_sd,
    -- . 1GbE Control Interface
    ETH_clk                  => ETH_clk,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- Specific node function
  -----------------------------------------------------------------------------
  u_node : entity work.node_unb1_bn_capture
  generic map (
    -- General
    g_sim            => g_sim,
    -- BN capture specific
    g_bn_capture     => c_bn_capture,
    -- Use PHY Interface
    g_use_phy        => c_use_phy,
    -- Auxiliary Interface
    g_aux            => c_unb1_board_aux,
    -- ADC Interface
    g_nof_dp_phs_clk => c_nof_dp_phs_clk,
    g_ai             => c_ai
  )
  port map (
    --
    -- >>> SOPC system with conduit peripheral MM bus
    --
    -- System
    mm_rst                    => mm_rst,
    mm_clk                    => mm_clk,

    dp_rst                    => dp_rst,
    dp_clk                    => dp_clk,
    dp_phs_clk_vec            => dp_phs_clk_vec,
    dp_pps                    => dp_pps,

    ext_clk                   => CLK,

    -- MM bsn source
    reg_bsn_source_mosi       => reg_bsn_source_mosi,
    reg_bsn_source_miso       => reg_bsn_source_miso,

    -- MM bsn schedule WG
    reg_bsn_scheduler_wg_mosi => reg_bsn_scheduler_wg_mosi,
    reg_bsn_scheduler_wg_miso => reg_bsn_scheduler_wg_miso,

    -- MM aduh quad
    reg_adc_quad_mosi         => reg_adc_quad_mosi,
    reg_adc_quad_miso         => reg_adc_quad_miso,

    -- MM wideband waveform generator registers [0,1,2,3] for signal paths [A,B,C,D]
    reg_wg_mosi_arr           => reg_wg_mosi_arr,
    reg_wg_miso_arr           => reg_wg_miso_arr,
    ram_wg_mosi_arr           => ram_wg_mosi_arr,
    ram_wg_miso_arr           => ram_wg_miso_arr,

    -- MM DP shiftram
    reg_dp_shiftram_mosi      => reg_dp_shiftram_mosi,
    reg_dp_shiftram_miso      => reg_dp_shiftram_miso,

    -- MM signal path monitors for [A, B, C, D]
    reg_mon_mosi_arr          => reg_mon_mosi_arr,
    reg_mon_miso_arr          => reg_mon_miso_arr,
    ram_mon_mosi_arr          => ram_mon_mosi_arr,
    ram_mon_miso_arr          => ram_mon_miso_arr,

    -- MM registers [0,1] for I2C access with ADUs [AB,CD]
    reg_commander_mosi_arr    => reg_commander_mosi_arr,
    reg_commander_miso_arr    => reg_commander_miso_arr,
    ram_protocol_mosi_arr     => ram_protocol_mosi_arr,
    ram_protocol_miso_arr     => ram_protocol_miso_arr,
    ram_result_mosi_arr       => ram_result_mosi_arr,
    ram_result_miso_arr       => ram_result_miso_arr,

    -- MM registers to enable and disable signal path
    reg_bsn_scheduler_sp_on_mosi  => reg_bsn_scheduler_sp_on_mosi,
    reg_bsn_scheduler_sp_on_miso  => reg_bsn_scheduler_sp_on_miso,
    reg_bsn_scheduler_sp_off_mosi => reg_bsn_scheduler_sp_off_mosi,
    reg_bsn_scheduler_sp_off_miso => reg_bsn_scheduler_sp_off_miso,
    --
    -- >>> Node FPGA pins
    --
    -- ADC Interface
    ADC_BI_A               => ADC_BI_A,
    ADC_BI_B               => ADC_BI_B,
    ADC_BI_A_CLK           => ADC_BI_A_CLK,
    ADC_BI_A_CLK_RST       => ADC_BI_A_CLK_RST,
    ADC_BI_C               => ADC_BI_C,
    ADC_BI_D               => ADC_BI_D,
    ADC_BI_D_CLK           => ADC_BI_D_CLK,
    ADC_BI_D_CLK_RST       => ADC_BI_D_CLK_RST,
    ADC_AB_SCL             => ADC_AB_SCL,  -- I2C AB
    ADC_AB_SDA             => ADC_AB_SDA,
    ADC_CD_SCL             => ADC_CD_SCL,  -- I2C CD
    ADC_CD_SDA             => ADC_CD_SDA
  );
end str;
