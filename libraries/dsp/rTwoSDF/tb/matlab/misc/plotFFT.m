k=13;

N=2.^[1:k];

dft_mult=    N.^2;
fft_mult=    (N/2).*(log2(N));
r2mdc_mult=  log2(N)-2;
r2sdf_mult=  log2(N)-2;
r22sdf_mult= log2(log2(N))-1;


figure;grid on; hold on;
%plot(dft_mult); set(h,'Color',[rand,rand,rand])
%plot(fft_mult); set(h,'Color',[rand,rand,rand])
h=plot(r2mdc_mult);set(h,'Color',[rand,rand,rand])
h=plot(r2sdf_mult);set(h,'Color',[rand,rand,rand])
h=plot(r22sdf_mult);set(h,'Color',[rand,rand,rand])

