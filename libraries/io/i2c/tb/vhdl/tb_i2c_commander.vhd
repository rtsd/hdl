-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify the i2c_commander and create a u_protocol_ram init file
--
-- Description:
--   The protocols for the I2C slaves on UNB are stored in the dev_unb_pkg.
--   The protocols for the I2C slaves on ADU are stored in the dev_adu_pkg.
--   This tb can model both via g_board = "unb" or "adu". The protocols for the
--   board are concatenated into c_protocol_ram_init.
--   The p_mm_stimuli writes the c_protocol_ram_init bytes to u_protocol_ram
--   and in parallel they get written to a file by u_protocol_ram_init_file:
--
--     data/adu_protocol_ram_init.txt
--   or
--     data/unb_protocol_ram_init.txt
--
--   See data/how_to_create_memory_init_hex_file.txt for how to
--   create a memory initialization hex file from this.
--
--   The p_mm_stimuli continues with executing all protocol lists and checking
--   the expected results and ASSERTs ERROR if an access went not OK and in
--   case of read data if the read data value was not OK.
--
-- Remark:
-- . Use c_protocol_ram_init_file="UNUSED" to initialize the u_protocol_ram
--   with the c_protocol_ram_init sequence,
--   else use the actual protocol_ram_init.hex file and verify that it contains
--   the same as the c_protocol_ram_init sequence.
--
-- Usage:
--   > do wave_i2c_commander.do
--   > run -all
--   In the Wave Window view the signal u_commander/protocol_index to observe
--   the progress

entity tb_i2c_commander is
  generic (
    g_board      : string := "adu"  -- else default to "unb"
    --g_board      : STRING := "unb"
  );
end tb_i2c_commander;

library IEEE, common_lib, tst_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.i2c_smbus_pkg.all;
use work.i2c_dev_max1617_pkg.all;
use work.i2c_dev_adu_pkg.all;
use work.i2c_dev_unb_pkg.all;
use work.i2c_pkg.all;
use work.i2c_commander_pkg.all;
use work.i2c_commander_aduh_pkg.all;
use work.i2c_commander_unbh_pkg.all;

architecture tb of tb_i2c_commander is
  constant c_protocol_ram_init_file : string := sel_a_b(g_board = "adu", "data/adu_protocol_ram_init.hex", "data/unb_protocol_ram_init.hex");
  --CONSTANT c_protocol_ram_init_file : STRING := "UNUSED";

  constant c_use_result_ram       : boolean := true;

  constant c_sim                  : boolean := true;  -- FALSE
  --CONSTANT c_sim                  : BOOLEAN := FALSE;
  constant c_clk_freq_in_MHz      : natural := 100;  -- 100 MHz
  constant c_clk_period           : time    := (10**3 / c_clk_freq_in_MHz) * 1 ns;
  constant c_rst_period           : time    := 4 * c_clk_period;

  constant c_phy_i2c              : t_c_i2c_phy := func_i2c_sel_a_b(c_sim, c_i2c_phy_sim, func_i2c_calculate_phy(c_clk_freq_in_MHz));

  -- Model I2C sensor slaves on the bus
  constant ADR_MAX6652            : natural := sel_a_b(g_board = "adu",                   0, I2C_UNB_MAX6652_ADR);
  constant ADR_MAX1617            : natural := sel_a_b(g_board = "adu", I2C_ADU_MAX1617_ADR, I2C_UNB_MAX1617_ADR);
  constant ADR_PCA9555            : natural := sel_a_b(g_board = "adu", I2C_ADU_PCA9555_ADR, 0);

  constant c_sens_volt_address    : std_logic_vector := TO_UVEC(ADR_MAX6652, 7);  -- MAX6652 address GND
  constant c_max6652_volt_1v2     : natural := 92;  -- 92 *  2.5/192 = 1.2
  constant c_max6652_volt_2v5     : natural := 147;  -- 147 *  3.3/192 = 2.5
  constant c_max6652_volt_nc      : natural := 13;  -- 13 * 12  /192 = 0.1
  constant c_max6652_volt_3v3     : natural := 127;  -- 127 *  5.0/192 = 3.3
  constant c_max6652_temp         : natural := 40;

  --CONSTANT c_sens_temp_address    : STD_LOGIC_VECTOR := TO_UVEC(MAX1617_ADR_LOW_MID, 7);  -- use other slave address to force I2C errors
  constant c_sens_temp_address    : std_logic_vector := TO_UVEC(ADR_MAX1617, 7);  -- MAX1618 address MID  MID
  constant c_max1618_temp         : integer := 60;

  constant c_io_expander_address  : std_logic_vector := TO_UVEC(ADR_PCA9555, 7);  -- ADR_PCA9555 address

  -- Select the expected read data arrays for the result data (the read data values are tb dependent, so therefore they are not obtained from a package)
  constant c_max1618_expected_data_read_temp_arr   : t_i2c_cmdr_natural_arr := (c_max1618_temp,        c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x,
                                                                                c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x);
  constant c_max6652_expected_data_read_config_arr : t_i2c_cmdr_natural_arr := (c_max6652_volt_1v2,    c_max6652_volt_2v5,    c_max6652_volt_3v3,    c_max6652_temp,
                                                                                c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x, c_i2c_cmdr_expected_x);

  constant c_expected_data_0_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_max1618_expected_data_read_temp_arr, c_max1618_expected_data_read_temp_arr);
  constant c_expected_data_1_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_max6652_expected_data_read_config_arr);
  constant c_expected_data_2_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_3_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_4_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_5_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_6_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_7_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_8_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_9_arr  : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_10_arr : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_11_arr : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_12_arr : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_13_arr : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_14_arr : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);
  constant c_expected_data_15_arr : t_i2c_cmdr_natural_arr := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_expected_data_none_arr,     c_i2c_cmdr_expected_data_none_arr);

  constant c_expected_data_mat : t_i2c_cmdr_natural_mat(0 to c_i2c_cmdr_max_nof_protocols - 1) := (c_expected_data_0_arr,
                                                                                                 c_expected_data_1_arr,
                                                                                                 c_expected_data_2_arr,
                                                                                                 c_expected_data_3_arr,
                                                                                                 c_expected_data_4_arr,
                                                                                                 c_expected_data_5_arr,
                                                                                                 c_expected_data_6_arr,
                                                                                                 c_expected_data_7_arr,
                                                                                                 c_expected_data_8_arr,
                                                                                                 c_expected_data_9_arr,
                                                                                                 c_expected_data_10_arr,
                                                                                                 c_expected_data_11_arr,
                                                                                                 c_expected_data_12_arr,
                                                                                                 c_expected_data_13_arr,
                                                                                                 c_expected_data_14_arr,
                                                                                                 c_expected_data_15_arr);

  -- RAM sizes
  constant c_mem_i2c              : t_c_i2c_mm := func_i2c_sel_a_b(g_board = "adu", c_i2c_cmdr_aduh_i2c_mm, c_i2c_cmdr_unbh_i2c_mm);

  -- Commander parameters
  constant c_protocol_ram_init    : t_nat_natural_arr := sel_a_b(g_board = "adu", c_i2c_cmdr_aduh_protocol_ram_init, c_i2c_cmdr_unbh_protocol_ram_init);
  constant c_nof_result_data_arr  : t_nat_natural_arr := sel_a_b(g_board = "adu", c_i2c_cmdr_aduh_nof_result_data_arr, c_i2c_cmdr_unbh_nof_result_data_arr);

  constant c_protocol_commander   : t_c_i2c_cmdr_commander := func_i2c_cmdr_sel_a_b(g_board = "adu", c_i2c_cmdr_aduh_protocol_commander, c_i2c_cmdr_unbh_protocol_commander);

  -- Commander MM register word indexes
  constant c_protocol_status_wi   : natural := 3 * c_protocol_commander.nof_protocols;
  constant c_result_error_cnt_wi  : natural := 3 * c_protocol_commander.nof_protocols + 1;
  constant c_result_data_wi       : natural := 3 * c_protocol_commander.nof_protocols + 2;

  -- Test bench PHY
  signal tb_end            : std_logic := '0';
  signal clk               : std_logic := '0';
  signal rst               : std_logic := '1';
  signal sync              : std_logic := '1';

  signal scl_stretch       : std_logic := 'Z';
  signal scl               : std_logic;
  signal sda               : std_logic;

  -- IO expander
  signal iobank0           : std_logic_vector(7 downto 0);
  signal iobank1           : std_logic_vector(7 downto 0);

  -- IO expander ADU
  signal adu_sclk              : std_logic;
  signal adu_sdata             : std_logic;
  signal adu_led_scs_n         : std_logic;
  signal adu_cal_adc           : std_logic;
  signal adu_pwr_ctrl          : std_logic_vector(1 downto 0);
  signal adu_pwr_ctrl_1_dig    : std_logic;
  signal adu_pwr_ctrl_0_ana    : std_logic;
  signal adu_ctrl              : std_logic_vector(3 downto 0);
  signal adu_ctrl_0_atten_le   : std_logic;
  signal adu_ctrl_1_modem_on   : std_logic;
  signal adu_ctrl_2_atten_le   : std_logic;
  signal adu_ctrl_3_modem_on   : std_logic;

  signal adu_atten_ctrl        : std_logic_vector(5 downto 0);

  -- File interface
  signal file_miso         : t_mem_miso;
  signal file_mosi         : t_mem_mosi;

  -- MM registers interface
  signal commander_miso    : t_mem_miso;
  signal commander_mosi    : t_mem_mosi;
  signal protocol_miso     : t_mem_miso;
  signal protocol_mosi     : t_mem_mosi;
  signal result_miso       : t_mem_miso;
  signal result_mosi       : t_mem_mosi;

  -- Commander results
  signal protocol_data     : natural;
  signal protocol_status   : natural;
  signal result_data       : natural;
  signal result_error_cnt  : natural;
begin
  -- run -all

  rst <= '0' after 4 * c_clk_period;
  clk <= (not clk) or tb_end after c_clk_period / 2;

  -- I2C bus
  scl <= 'H';  -- model I2C pull up
  sda <= 'H';  -- model I2C pull up, use '0' and '1' to verify SDA forced low or high error

  scl <= scl_stretch;

  sens_clk_stretch : process (scl)
  begin
    if falling_edge(scl) then
      scl_stretch <= '0', 'Z' after 50 ns;  -- < 10 ns to effectively disable stretching, >= 50 ns to enable it
    end if;
  end process;

  u_protocol_ram_init_file : entity tst_lib.tst_output
  generic map (
    g_file_name   => "data/" & g_board & "_protocol_ram_init.txt",
    g_nof_data    => 1,
    g_data_width  => c_byte_w,
    g_data_type   => "UNSIGNED"
  )
  port map (
    clk      => clk,
    rst      => rst,
    in_dat   => file_mosi.wrdata(c_byte_w - 1 downto 0),
    in_val   => file_mosi.wr
  );

  p_mm_stimuli : process
  begin
    -- Wait for reset release
    commander_mosi <= c_mem_mosi_rst;
    file_mosi      <= c_mem_mosi_rst;
    protocol_mosi  <= c_mem_mosi_rst;
    result_mosi    <= c_mem_mosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    ----------------------------------------------------------------------------
    -- Initialize the u_protocol_ram or verify its default contents
    ----------------------------------------------------------------------------

    if c_protocol_ram_init_file = "UNUSED" then
      -- Write
      for I in 0 to c_protocol_ram_init'length - 1 loop
        proc_mem_mm_bus_wr(I, c_protocol_ram_init(I), clk, protocol_miso, protocol_mosi);  -- fill u_protocol_ram
      end loop;
      for I in c_protocol_ram_init'length to c_mem_i2c.protocol_nof_dat - 1 loop
        proc_mem_mm_bus_wr(I, SMBUS_C_END, clk, protocol_miso, protocol_mosi);  -- fill remainder of u_protocol_ram with default
      end loop;

    else
      -- Read and verify
      for I in 0 to c_protocol_ram_init'length - 1 loop
        proc_mem_mm_bus_rd(I, clk, protocol_miso, protocol_mosi);
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, clk);
        protocol_data <= TO_UINT(protocol_miso.rddata(c_byte_w - 1 downto 0));
        proc_common_wait_some_cycles(clk, 1);
        assert c_protocol_ram_init(I) = protocol_data
          report "Unexpected protocol data"
          severity ERROR;
      end loop;
      for I in c_protocol_ram_init'length to c_mem_i2c.protocol_nof_dat - 1 loop
        proc_mem_mm_bus_rd(I, clk, protocol_miso, protocol_mosi);
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, clk);
        protocol_data <= TO_UINT(protocol_miso.rddata(c_byte_w - 1 downto 0));
        proc_common_wait_some_cycles(clk, 1);
        assert SMBUS_C_END = protocol_data
          report "Unexpected protocol end data"
          severity ERROR;
      end loop;
    end if;

    proc_common_wait_some_cycles(clk, 10);

    ----------------------------------------------------------------------------
    -- Create the u_protocol_ram_init_file
    ----------------------------------------------------------------------------

    for I in 0 to c_protocol_ram_init'length - 1 loop
      proc_mem_mm_bus_wr(I, c_protocol_ram_init(I), clk, file_miso, file_mosi);  -- fill u_protocol_ram_init_file
    end loop;
    for I in c_protocol_ram_init'length to c_mem_i2c.protocol_nof_dat - 1 loop
      proc_mem_mm_bus_wr(I, SMBUS_C_END, clk, file_miso, file_mosi);  -- fill remainder of u_protocol_ram_init_file with default
    end loop;

    proc_common_wait_some_cycles(clk, 10);

    ----------------------------------------------------------------------------
    -- Try and verify all commander protocols
    ----------------------------------------------------------------------------

    for P in 0 to c_protocol_commander.nof_protocols - 1 loop

      -- Issue protocol list P
      proc_mem_mm_bus_wr(P, 1, clk, commander_miso, commander_mosi);

      -- Wait for protocol done
      while protocol_status /= c_i2c_cmdr_state_done loop
        -- read commander protocol status register
        proc_mem_mm_bus_rd(c_protocol_status_wi, clk, commander_miso, commander_mosi);
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, clk);
        protocol_status <= TO_UINT(commander_miso.rddata);
        proc_common_wait_some_cycles(clk, 1);
      end loop;

      -- Read commander result error count
      proc_mem_mm_bus_rd(c_result_error_cnt_wi, clk, commander_miso, commander_mosi);
      proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, clk);
      result_error_cnt <= TO_UINT(commander_miso.rddata);
      proc_common_wait_some_cycles(clk, 1);
      assert result_error_cnt = 0
        report "The result error count is not 0"
        severity ERROR;

      -- Read commander result data
      for I in 0 to c_nof_result_data_arr(P) - 1 loop
        proc_mem_mm_bus_rd(c_result_data_wi + I, clk, commander_miso, commander_mosi);
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, clk);
        result_data <= TO_UINT(commander_miso.rddata);
        proc_common_wait_some_cycles(clk, 1);
        assert c_expected_data_mat(P)(I) = result_data
          report "Unexpected result data"
          severity ERROR;
      end loop;

      -- Wait for protocol idle
      while protocol_status /= c_i2c_cmdr_state_idle loop
        -- read commander protocol status register
        proc_mem_mm_bus_rd(c_protocol_status_wi, clk, commander_miso, commander_mosi);
        proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, clk);
        protocol_status <= TO_UINT(commander_miso.rddata);
        proc_common_wait_some_cycles(clk, 1);
      end loop;

      -- Wait some time between the protocols
      proc_common_wait_some_cycles(clk, 100);
    end loop;

    ----------------------------------------------------------------------------
    -- The end
    ----------------------------------------------------------------------------

    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  -- I2C commander
  u_commander : entity work.i2c_commander
  generic map (
    g_sim                    => c_sim,
    g_i2c_cmdr               => c_protocol_commander,
    g_i2c_mm                 => c_mem_i2c,
    g_i2c_phy                => c_phy_i2c,
    g_protocol_ram_init_file => c_protocol_ram_init_file,
    g_use_result_ram         => c_use_result_ram
  )
  port map (
    rst               => rst,
    clk               => clk,
    sync              => sync,

    ---------------------------------------------------------------------------
    -- Memory Mapped slave interfaces
    ---------------------------------------------------------------------------
    commander_mosi    => commander_mosi,
    commander_miso    => commander_miso,

    -- If the default protocol list in u_protocol_ram is fine, then the protocol slave port can be left no connected
    protocol_mosi     => protocol_mosi,
    protocol_miso     => protocol_miso,

    -- Typically the commander status registers are sufficient, so then the results slave port can be left no connected
    result_mosi       => result_mosi,
    result_miso       => result_miso,

    ---------------------------------------------------------------------------
    -- I2C interface
    ---------------------------------------------------------------------------
    scl               => scl,
    sda               => sda
  );

  -- I2C slaves
  u_sens_volt : entity work.dev_max6652  -- only on "unb"
  generic map (
    g_address => c_sens_volt_address
  )
  port map (
    scl       => scl,
    sda       => sda,
    volt_2v5  => c_max6652_volt_1v2,
    volt_3v3  => c_max6652_volt_2v5,
    volt_12v  => c_max6652_volt_nc,
    volt_vcc  => c_max6652_volt_3v3,
    temp      => c_max6652_temp
  );

  u_sens_temp : entity work.dev_max1618  -- both on "unb" and on "adu"
  generic map (
    g_address => c_sens_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_max1618_temp
  );

  u_io_expander : entity work.dev_pca9555  -- only on "adu"
  generic map (
    g_address => c_io_expander_address
  )
  port map (
    scl       => scl,
    sda       => sda,
    iobank0   => iobank0,
    iobank1   => iobank1
  );

  -- ADU interpretation of the IO expander outputs
  adu_sclk            <= iobank0(4);
  adu_sdata           <= iobank0(5);
  adu_led_scs_n       <= iobank0(7);
  adu_cal_adc         <= iobank1(7);
  adu_pwr_ctrl        <= iobank0(6) & iobank1(6);
  adu_pwr_ctrl_1_dig  <= adu_pwr_ctrl(1);
  adu_pwr_ctrl_0_ana  <= adu_pwr_ctrl(0);

  adu_ctrl            <= iobank0(3 downto 0);
  adu_ctrl_0_atten_le <= adu_ctrl(0);
  adu_ctrl_1_modem_on <= adu_ctrl(1);
  adu_ctrl_2_atten_le <= adu_ctrl(2);
  adu_ctrl_3_modem_on <= adu_ctrl(3);

  adu_atten_ctrl      <= iobank1(5 downto 0);
end tb;
