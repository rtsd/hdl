-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.common_mem_pkg.all;

-- Purpose: Get in_dat from in_clk to out_clk domain when in_new is asserted.
-- Remarks:
-- . If in_new is a pulse, then new in_dat is available after g_in_new_latency.
-- . It is also allowed to hold in_new high, then out_new will pulse once for
--   every 24 out_clk cycles.
-- . Use in_done to be sure that in_dat due to in_new has crossed the clock
--   domain, in case of multiple in_new pulses in a row the in_done will only
--   pulse when this state remains s_idle, so after the last in_new.
-- . If the in_dat remains unchanged during the crossing of in_new to out_en
--   then g_input_buf=FALSE may be used to save some flipflops

entity common_reg_cross_domain is
  generic (
    g_input_buf      : boolean := true;
    g_in_new_latency : natural := 0;  -- >= 0
    g_out_dat_init   : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := (others => '0')
  );
  port (
    in_rst      : in  std_logic;
    in_clk      : in  std_logic;
    in_new      : in  std_logic := '1';  -- when '1' then new in_dat is available after g_in_new_latency
    in_dat      : in  std_logic_vector;
    in_done     : out std_logic;  -- pulses when no more pending in_new
    out_rst     : in  std_logic;
    out_clk     : in  std_logic;
    out_dat     : out std_logic_vector;
    out_new     : out std_logic  -- when '1' then the out_dat was updated with in_dat due to in_new
  );
end common_reg_cross_domain;

architecture rtl of common_reg_cross_domain is
  constant c_dat : std_logic_vector(in_dat'range) := g_out_dat_init(in_dat'range);

  signal in_rst_p         : std_logic := '1';
  signal out_rst_p        : std_logic := '1';

  ------------------------------------------------------------------------------
  -- in_clk domain
  ------------------------------------------------------------------------------
  signal reg_new          : std_logic_vector(0 to g_in_new_latency) := (others => '0');
  signal nxt_reg_new      : std_logic_vector(reg_new'range);

  signal in_buf           : std_logic_vector(c_dat'range) := c_dat;
  signal in_buf_reg       : std_logic_vector(c_dat'range) := c_dat;
  signal nxt_in_buf_reg   : std_logic_vector(c_dat'range);

  -- Register access clock domain crossing
  type t_state_enum is (s_idle, s_busy);

  signal cross_req        : std_logic;
  signal cross_busy       : std_logic;
  signal nxt_in_done      : std_logic;
  signal state            : t_state_enum;
  signal nxt_state        : t_state_enum;
  signal prev_state       : t_state_enum;
  signal in_new_hold      : std_logic;
  signal nxt_in_new_hold  : std_logic;

  ------------------------------------------------------------------------------
  -- out_clk domain
  ------------------------------------------------------------------------------
  signal out_en           : std_logic;
  signal i_out_dat        : std_logic_vector(c_dat'range) := c_dat;  -- register init without physical reset
  signal nxt_out_dat      : std_logic_vector(c_dat'range);
begin
  out_dat <= i_out_dat;

  -- Pipeline synchronous input rst to ease timing closure
  in_rst_p <= in_rst when rising_edge(in_clk);
  out_rst_p <= out_rst when rising_edge(out_clk);

  ------------------------------------------------------------------------------
  -- in_clk domain
  ------------------------------------------------------------------------------

  reg_new(0) <= in_new;

  gen_latency : if g_in_new_latency > 0 generate
    p_reg_new : process(in_rst_p, in_clk)
    begin
      if in_rst_p = '1' then
        reg_new(1 to g_in_new_latency) <= (others => '0');
      elsif rising_edge(in_clk) then
        reg_new(1 to g_in_new_latency) <= nxt_reg_new(1 to g_in_new_latency);
      end if;
    end process;

    nxt_reg_new(1 to g_in_new_latency) <= reg_new(0 to g_in_new_latency - 1);
  end generate;

  p_in_clk : process(in_rst_p, in_clk)
  begin
    if in_rst_p = '1' then
      in_new_hold <= '0';
      in_done     <= '0';
      state       <= s_idle;
      prev_state  <= s_idle;
    elsif rising_edge(in_clk) then
      in_buf_reg  <= nxt_in_buf_reg;
      in_new_hold <= nxt_in_new_hold;
      in_done     <= nxt_in_done;
      state       <= nxt_state;
      prev_state  <= state;
    end if;
  end process;

  -- capture the new register data
  no_in_buf : if g_input_buf = false generate
    in_buf <= in_dat;  -- assumes that in_dat remains unchanged during the crossing of in_new to out_en
  end generate;

  gen_in_buf : if g_input_buf = true generate
    nxt_in_buf_reg <= in_dat when cross_req = '1' else in_buf_reg;
    in_buf         <= in_buf_reg;
  end generate;

  -- handshake control of the clock domain crossing by u_cross_req
  -- hold any subsequent in_new during cross domain busy to ensure that the out_dat will get the latest value of in_dat
  p_state : process(state, prev_state, reg_new, in_new_hold, cross_busy)
  begin
    cross_req <= '0';
    nxt_in_done <= '0';
    nxt_in_new_hold <= in_new_hold;
    nxt_state <= state;
    case state is
      when s_idle =>
        nxt_in_new_hold <= '0';
        if reg_new(g_in_new_latency) = '1' or in_new_hold = '1' then
          cross_req <= '1';
          nxt_state <= s_busy;
        elsif unsigned(reg_new) = 0 and prev_state = s_busy then
          nxt_in_done <= '1';  -- no pending in_new at input or in shift register and just left s_busy, so signal in_done
        end if;
      when others =>  -- s_busy
        if reg_new(g_in_new_latency) = '1' then
          nxt_in_new_hold <= '1';
        end if;
        if cross_busy = '0' then
          nxt_state <= s_idle;
        end if;
    end case;
  end process;

  ------------------------------------------------------------------------------
  -- cross clock domain
  ------------------------------------------------------------------------------
  u_cross_req : entity common_lib.common_spulse
  port map (
    in_rst     => in_rst_p,
    in_clk     => in_clk,
    in_pulse   => cross_req,
    in_busy    => cross_busy,
    out_rst    => out_rst_p,
    out_clk    => out_clk,
    out_pulse  => out_en
  );

  ------------------------------------------------------------------------------
  -- out_clk domain
  ------------------------------------------------------------------------------
  p_out_clk : process(out_rst_p, out_clk)
  begin
    if out_rst_p = '1' then
      out_new   <= '0';
    elsif rising_edge(out_clk) then
      i_out_dat <= nxt_out_dat;
      out_new   <= out_en;
    end if;
  end process;

  -- some clock cycles after the cross_req the in_buf data is stable for sure
  nxt_out_dat <= in_buf when out_en = '1' else i_out_dat;
end rtl;
