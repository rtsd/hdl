-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, diag_lib, dp_lib, eth_lib, tech_tse_lib, tr_10GbE_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use eth_lib.eth_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;

-- Purpose:

entity unb1_tr_10GbE is
  generic (
    g_design_name             : string  := "unb1_tr_10GbE";
    g_sim                     : boolean := false;  -- Overridden by TB
    g_stamp_date              : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time              : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn               : natural := 0  -- SVN revision    -- set by QSF
  );
  port (
    -- GENERAL
    CLK           : in    std_logic;  -- System Clock
    PPS           : in    std_logic;  -- System Sync
    WDI           : out   std_logic;  -- Watchdog Clear
    INTA          : inout std_logic;  -- FPGA interconnect line
    INTB          : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION       : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID            : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO        : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc       : inout std_logic;
    sens_sd       : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk       : in    std_logic;
    ETH_SGIN      : in    std_logic;
    ETH_SGOUT     : out   std_logic;

    -- Transceiver clocks
    SA_CLK        : in  std_logic;  -- SerDes Clock BN-BI / SI_FN

    -- Serial I/O: 10GbE receivers
    SI_FN_0_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_0_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_1_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_1_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_2_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_2_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_3_TX    : out   std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
    SI_FN_3_RX    : in    std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);

    SI_FN_0_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_1_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_2_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_3_CNTRL : inout std_logic_vector(c_unb1_board_ci.tr.cntrl_w - 1 downto 0);
    SI_FN_RSTN    : out   std_logic := '1'  -- ResetN is pulled up in the Vitesse chip, but pulled down again by external 1k resistor.
                                           -- So we need to assign a '1' to it.
  );
end unb1_tr_10GbE;

architecture str of unb1_tr_10GbE is
  constant c_use_phy                : t_c_unb1_board_use_phy  := (1, 1, 0, 0, 0, 0, 0, 1);
  constant c_nof_10GbE_streams      : natural := 3;  -- The number of 10G input streams

  -- Block generator
  constant c_bg_block_size          : natural := 176;
  constant c_bg_gapsize             : natural := 256 - 176;
  constant c_bg_blocks_per_sync     : natural := 100;
  constant c_bg_ctrl                : t_diag_block_gen := (sel_a_b(g_sim, '1', '0'),  -- enable: On by default in simulation; MM enable required on hardware.
                                                           '0',  -- enable_sync
                                                          TO_UVEC(     c_bg_block_size, c_diag_bg_samples_per_packet_w),
                                                          TO_UVEC(c_bg_blocks_per_sync, c_diag_bg_blocks_per_sync_w),
                                                          TO_UVEC(        c_bg_gapsize, c_diag_bg_gapsize_w),
                                                          TO_UVEC(                   0, c_diag_bg_mem_low_adrs_w),
                                                          TO_UVEC(   c_bg_block_size-1, c_diag_bg_mem_high_adrs_w),
                                                          TO_UVEC(                   0, c_diag_bg_bsn_init_w));
  -- System
  signal cs_sim                                      : std_logic;
  signal xo_clk                                      : std_logic;
  signal xo_rst                                      : std_logic;
  signal xo_rst_n                                    : std_logic;
  signal mm_clk                                      : std_logic;
  signal mm_locked                                   : std_logic;
  signal mm_rst                                      : std_logic;
  signal cal_clk                                     : std_logic;
  signal epcs_clk                                    : std_logic;

  signal dp_rst                                      : std_logic;
  signal dp_clk                                      : std_logic;
  signal dp_pps                                      : std_logic;
  signal sa_rst                                      : std_logic;

  signal eth1g_tse_clk                               : std_logic;
  signal eth1g_mm_rst                                : std_logic;
  signal eth1g_reg_interrupt                         : std_logic;  -- Interrupt

  -- PIOs
  signal pout_wdi                                    : std_logic;

  -- MM Register interfaces
  signal reg_wdi_mosi                                : t_mem_mosi;
  signal reg_wdi_miso                                : t_mem_miso;
  signal reg_unb_system_info_mosi                    : t_mem_mosi;
  signal reg_unb_system_info_miso                    : t_mem_miso;
  signal rom_unb_system_info_mosi                    : t_mem_mosi;
  signal rom_unb_system_info_miso                    : t_mem_miso;
  signal reg_unb_sens_mosi                           : t_mem_mosi;
  signal reg_unb_sens_miso                           : t_mem_miso;
  signal reg_ppsh_mosi                               : t_mem_mosi;
  signal reg_ppsh_miso                               : t_mem_miso;
  signal eth1g_ram_mosi                              : t_mem_mosi;
  signal eth1g_ram_miso                              : t_mem_miso;
  signal eth1g_reg_mosi                              : t_mem_mosi;
  signal eth1g_reg_miso                              : t_mem_miso;
  signal eth1g_tse_mosi                              : t_mem_mosi;
  signal eth1g_tse_miso                              : t_mem_miso;
  signal reg_diag_bg_mosi                      : t_mem_mosi;
  signal reg_diag_bg_miso                      : t_mem_miso;
  signal reg_mdio_0_mosi                             : t_mem_mosi;
  signal reg_mdio_0_miso                             : t_mem_miso;
  signal reg_mdio_1_mosi                             : t_mem_mosi;
  signal reg_mdio_1_miso                             : t_mem_miso;
  signal reg_mdio_2_mosi                             : t_mem_mosi;
  signal reg_mdio_2_miso                             : t_mem_miso;
  signal reg_dp_offload_rx_hdr_dat_mosi              : t_mem_mosi;
  signal reg_dp_offload_rx_hdr_dat_miso              : t_mem_miso;
  signal reg_dp_offload_tx_hdr_dat_mosi              : t_mem_mosi;
  signal reg_dp_offload_tx_hdr_dat_miso              : t_mem_miso;
  signal reg_tr_10gbe_mosi                           : t_mem_mosi;
  signal reg_tr_10gbe_miso                           : t_mem_miso;
  signal reg_tr_xaui_mosi                            : t_mem_mosi;
  signal reg_tr_xaui_miso                            : t_mem_miso;
  signal reg_bsn_monitor_mosi                        : t_mem_mosi;
  signal reg_bsn_monitor_miso                        : t_mem_miso;
  signal reg_mdio_mosi_arr                           : t_mem_mosi_arr(c_unb1_board_nof_mdio - 1 downto 0);
  signal reg_mdio_miso_arr                           : t_mem_miso_arr(c_unb1_board_nof_mdio - 1 downto 0);

  signal mms_diag_block_gen_src_out_arr              : t_dp_sosi_arr(c_nof_10GbE_streams - 1 downto 0);

  -- Interface: 10GbE
  signal xaui_tx_arr                                 : t_xaui_arr(c_nof_10GbE_streams - 1 downto 0);
  signal xaui_rx_arr                                 : t_xaui_arr(c_nof_10GbE_streams - 1 downto 0);
  signal unb_xaui_tx_arr                             : t_unb1_board_xaui_sl_2arr(c_nof_10GbE_streams - 1 downto 0);
  signal unb_xaui_rx_arr                             : t_unb1_board_xaui_sl_2arr(c_nof_10GbE_streams - 1 downto 0);
  signal mdio_mdc_arr                                : std_logic_vector(c_nof_10GbE_streams - 1 downto 0);
  signal mdio_mdat_in_arr                            : std_logic_vector(c_nof_10GbE_streams - 1 downto 0);
  signal mdio_mdat_oen_arr                           : std_logic_vector(c_nof_10GbE_streams - 1 downto 0);

  -- DP offload RX
  signal dp_offload_rx_snk_in_arr                    : t_dp_sosi_arr(c_nof_10GbE_streams - 1 downto 0);
  signal dp_offload_rx_snk_out_arr                   : t_dp_siso_arr(c_nof_10GbE_streams - 1 downto 0);
  signal dp_offload_rx_src_out_arr                   : t_dp_sosi_arr(c_nof_10GbE_streams - 1 downto 0);
  signal dp_offload_rx_src_in_arr                    : t_dp_siso_arr(c_nof_10GbE_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal dp_offload_rx_restored_src_out_arr          : t_dp_sosi_arr(c_nof_10GbE_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal hdr_fields_out_arr                          : t_slv_1024_arr(c_nof_10GbE_streams - 1 downto 0);

  -- BSN monitors
  signal dp_bsn_monitor_in_siso_arr                  : t_dp_siso_arr(c_nof_10GbE_streams + 1 + 1 - 1 downto 0);
  signal dp_bsn_monitor_in_sosi_arr                  : t_dp_sosi_arr(c_nof_10GbE_streams + 1 + 1 - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- TX: 3 Block generators
  -----------------------------------------------------------------------------
  u_mms_diag_block_gen : entity diag_lib.mms_diag_block_gen
  generic map (
    g_nof_streams        => c_nof_10GbE_streams,
    g_buf_dat_w          => 64,
    g_buf_addr_w         => ceil_log2(TO_UINT(c_bg_ctrl.samples_per_packet)),
    g_file_name_prefix   => "hex/composite_signals",
    g_diag_block_gen_rst => c_bg_ctrl
  )
  port map (
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,

    dp_rst           => dp_rst,
    dp_clk           => dp_clk,

    reg_bg_ctrl_mosi => reg_diag_bg_mosi,
    reg_bg_ctrl_miso => reg_diag_bg_miso,

    out_sosi_arr     => mms_diag_block_gen_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- 10GbE TX/RX
  -----------------------------------------------------------------------------
  -- Wire together different types
  gen_wires: for i in 0 to c_nof_10GbE_streams - 1 generate
    unb_xaui_tx_arr(i) <= xaui_tx_arr(i);
    xaui_rx_arr(i)     <= unb_xaui_rx_arr(i);
  end generate;

  u_front_io : entity unb1_board_lib.unb1_board_front_io
  generic map (
    g_nof_xaui => c_nof_10GbE_streams
  )
  port map (
    xaui_tx_arr       => unb_xaui_tx_arr,
    xaui_rx_arr       => unb_xaui_rx_arr,

    mdio_mdc_arr      => mdio_mdc_arr,
    mdio_mdat_in_arr  => mdio_mdat_in_arr,
    mdio_mdat_oen_arr => mdio_mdat_oen_arr,

    -- Serial I/O
    SI_FN_0_TX        => SI_FN_0_TX,
    SI_FN_0_RX        => SI_FN_0_RX,
    SI_FN_1_TX        => SI_FN_1_TX,
    SI_FN_1_RX        => SI_FN_1_RX,
    SI_FN_2_TX        => SI_FN_2_TX,
    SI_FN_2_RX        => SI_FN_2_RX,

    SI_FN_0_CNTRL     => SI_FN_0_CNTRL,
    SI_FN_1_CNTRL     => SI_FN_1_CNTRL,
    SI_FN_2_CNTRL     => SI_FN_2_CNTRL,
    SI_FN_3_CNTRL     => SI_FN_3_CNTRL
  );

  u_areset_sa_rst : entity common_lib.common_areset
  generic map(
    g_rst_level => '1',
    g_delay_len => 4
  )
  port map(
    clk     => SA_CLK,
    in_rst  => '0',
    out_rst => sa_rst
  );

  u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
  generic map(
    g_sim             => g_sim,
    g_sim_level       => 1,
    g_nof_macs        => c_nof_10GbE_streams,
    g_use_mdio        => true
  )
  port map (
    -- Transceiver PLL reference clock
    tr_ref_clk_156    => SA_CLK,
    tr_ref_rst_156    => sa_rst,

    -- Calibration & reconfig clock
    cal_rec_clk       => mm_clk,

    -- MM interface
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,

    reg_mac_mosi      => reg_tr_10GbE_mosi,
    reg_mac_miso      => reg_tr_10GbE_miso,

    xaui_mosi         => reg_tr_xaui_mosi,
    xaui_miso         => reg_tr_xaui_miso,

    mdio_mosi_arr     => reg_mdio_mosi_arr(c_nof_10GbE_streams - 1 downto 0),
    mdio_miso_arr     => reg_mdio_miso_arr(c_nof_10GbE_streams - 1 downto 0),

    -- DP interface
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    src_out_arr       => dp_offload_rx_snk_in_arr,
    src_in_arr        => dp_offload_rx_snk_out_arr,

    -- Serial XAUI IO
    xaui_tx_arr       => xaui_tx_arr,
    xaui_rx_arr       => xaui_rx_arr,

    -- MDIO interface
    mdio_rst          => SI_FN_RSTN,
    mdio_mdc_arr      => mdio_mdc_arr,
    mdio_mdat_in_arr  => mdio_mdat_in_arr,
    mdio_mdat_oen_arr => mdio_mdat_oen_arr
  );

  -----------------------------------------------------------------------------
  -- RX: dp_offload_rx
  -----------------------------------------------------------------------------
--  u_dp_offload_rx : ENTITY dp_lib.dp_offload_rx
--  GENERIC MAP (
--    g_nof_streams         => c_nof_10GbE_streams,
--    g_data_w              => c_xgmii_data_w,
--    g_hdr_field_arr       => c_apertif_udp_offload_hdr_field_arr,
--    g_remove_crc          => FALSE,
--    g_crc_nof_words       => 0
--   )
--  PORT MAP (
--    mm_rst                => mm_rst,
--    mm_clk                => mm_clk,
--
--    dp_rst                => dp_rst,
--    dp_clk                => dp_clk,
--
--    reg_hdr_dat_mosi      => reg_dp_offload_rx_hdr_dat_mosi,
--    reg_hdr_dat_miso      => reg_dp_offload_rx_hdr_dat_miso,
--
--    snk_in_arr            => dp_offload_rx_snk_in_arr,
--    snk_out_arr           => dp_offload_rx_snk_out_arr,
--
--    src_out_arr           => dp_offload_rx_src_out_arr,
--    src_in_arr            => (OTHERS=>c_dp_siso_rdy), --dp_offload_rx_src_in_arr,
--
--    hdr_fields_out_arr    => hdr_fields_out_arr
--    );
--
--  gen_restore_bf_out_i : FOR i IN 0 TO c_nof_10GbE_streams-1 GENERATE
--    dp_offload_rx_restored_src_out_arr(i).sync <=          sl(hdr_fields_out_arr(i)(field_hi(c_apertif_udp_offload_hdr_field_arr, "dp_sync") DOWNTO field_lo(c_apertif_udp_offload_hdr_field_arr, "dp_sync" )));
--    dp_offload_rx_restored_src_out_arr(i).bsn  <= RESIZE_UVEC(hdr_fields_out_arr(i)(field_hi(c_apertif_udp_offload_hdr_field_arr, "dp_bsn" ) DOWNTO field_lo(c_apertif_udp_offload_hdr_field_arr, "dp_bsn"  )), c_dp_stream_bsn_w);
--
--    dp_offload_rx_restored_src_out_arr(i).data  <= dp_offload_rx_src_out_arr(i).data;
--    dp_offload_rx_restored_src_out_arr(i).valid <= dp_offload_rx_src_out_arr(i).valid;
--    dp_offload_rx_restored_src_out_arr(i).sop   <= dp_offload_rx_src_out_arr(i).sop;
--    dp_offload_rx_restored_src_out_arr(i).eop   <= dp_offload_rx_src_out_arr(i).eop;
--    dp_offload_rx_restored_src_out_arr(i).err   <= dp_offload_rx_src_out_arr(i).err;
--  END GENERATE;

  -----------------------------------------------------------------------------
  -- RX: BSN monitors at several stages in the stream
  -----------------------------------------------------------------------------
  u_dp_bsn_monitor : entity dp_lib.mms_dp_bsn_monitor
  generic map (
    g_nof_streams        => 5,
    g_sync_timeout       => 200000000,
    g_log_first_bsn      => true
  )
  port map (
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    reg_mosi    => reg_bsn_monitor_mosi,
    reg_miso    => reg_bsn_monitor_miso,

    dp_rst      => dp_rst,
    dp_clk      => dp_clk,
    in_siso_arr => dp_bsn_monitor_in_siso_arr,
    in_sosi_arr => dp_bsn_monitor_in_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim                     => g_sim,
    g_sim_flash_model         => false,
    g_design_name             => g_design_name,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_stamp_svn               => g_stamp_svn,
    g_mm_clk_freq             => c_unb1_board_mm_clk_freq_50M,
    g_use_phy                 => c_use_phy,
    g_aux                     => c_unb1_board_aux,
    g_dp_clk_use_pll          => true,
    g_xo_clk_use_pll          => true
  )
  port map (
    -- Clock and reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk_out               => mm_clk,
    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    mm_locked                => mm_locked,
    mm_locked_out            => mm_locked,

    epcs_clk                 => epcs_clk,
    epcs_clk_out             => epcs_clk,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,
    dp_pps                   => dp_pps,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    cal_rec_clk              => cal_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_tse_clk_out        => eth1g_tse_clk,
    eth1g_tse_clk            => eth1g_tse_clk,
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    sens_sc                  => sens_sc,
    sens_sd                  => sens_sd,
    -- . 1GbE Control Interface
    ETH_clk                  => ETH_clk,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm_unb1_tr_10GbE : entity work.mmm_unb1_tr_10GbE
  generic map(
    g_sim         => g_sim
  )
  port map(
    mm_clk                         =>  mm_clk,
    mm_rst                         =>  mm_rst,
    pout_wdi                       =>  pout_wdi,
    reg_wdi_mosi                   =>  reg_wdi_mosi,
    reg_wdi_miso                   =>  reg_wdi_miso,
    reg_unb_system_info_mosi       =>  reg_unb_system_info_mosi,
    reg_unb_system_info_miso       =>  reg_unb_system_info_miso,
    rom_unb_system_info_mosi       =>  rom_unb_system_info_mosi,
    rom_unb_system_info_miso       =>  rom_unb_system_info_miso,
    reg_unb_sens_mosi              =>  reg_unb_sens_mosi,
    reg_unb_sens_miso              =>  reg_unb_sens_miso,
    reg_ppsh_mosi                  =>  reg_ppsh_mosi,
    reg_ppsh_miso                  =>  reg_ppsh_miso,
    eth1g_mm_rst                   =>  eth1g_mm_rst,
    eth1g_reg_interrupt            =>  eth1g_reg_interrupt,
    eth1g_ram_mosi                 =>  eth1g_ram_mosi,
    eth1g_ram_miso                 =>  eth1g_ram_miso,
    eth1g_reg_mosi                 =>  eth1g_reg_mosi,
    eth1g_reg_miso                 =>  eth1g_reg_miso,
    eth1g_tse_mosi                 =>  eth1g_tse_mosi,
    eth1g_tse_miso                 =>  eth1g_tse_miso,
    reg_diag_bg_mosi         =>  reg_diag_bg_mosi,
    reg_diag_bg_miso         =>  reg_diag_bg_miso,
    reg_mdio_0_mosi                =>  reg_mdio_0_mosi,
    reg_mdio_0_miso                =>  reg_mdio_0_miso,
    reg_mdio_1_mosi                =>  reg_mdio_1_mosi,
    reg_mdio_1_miso                =>  reg_mdio_1_miso,
    reg_mdio_2_mosi                =>  reg_mdio_2_mosi,
    reg_mdio_2_miso                =>  reg_mdio_2_miso,
    reg_dp_offload_rx_hdr_dat_mosi =>  reg_dp_offload_rx_hdr_dat_mosi,
    reg_dp_offload_rx_hdr_dat_miso =>  reg_dp_offload_rx_hdr_dat_miso,
    reg_dp_offload_tx_hdr_dat_mosi =>  reg_dp_offload_tx_hdr_dat_mosi,
    reg_dp_offload_tx_hdr_dat_miso =>  reg_dp_offload_tx_hdr_dat_miso,
    reg_tr_10gbe_mosi              =>  reg_tr_10gbe_mosi,
    reg_tr_10gbe_miso              =>  reg_tr_10gbe_miso,
    reg_tr_xaui_mosi               =>  reg_tr_xaui_mosi,
    reg_tr_xaui_miso               =>  reg_tr_xaui_miso,
    reg_bsn_monitor_mosi     =>  reg_bsn_monitor_mosi,
    reg_bsn_monitor_miso     =>  reg_bsn_monitor_miso
  );

  reg_mdio_mosi_arr(0) <= reg_mdio_0_mosi;
  reg_mdio_mosi_arr(1) <= reg_mdio_1_mosi;
  reg_mdio_mosi_arr(2) <= reg_mdio_2_mosi;

  reg_mdio_0_miso <= reg_mdio_miso_arr(0);
  reg_mdio_1_miso <= reg_mdio_miso_arr(1);
  reg_mdio_2_miso <= reg_mdio_miso_arr(2);
end str;
