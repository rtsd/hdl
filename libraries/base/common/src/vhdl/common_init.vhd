-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;

-- Purpose:
--
-- 1) The hold can be used as a synchronous reset for the rest of the logic.
--    The difference with common_areset is that common_init uses a counter
--    whereas common_areset uses a shift register to set the hold time for the
--    hold. Hence with common_init it is possible to define very long reset
--    hold times with minimal logic.
-- 2) The init can be used to ensure that all subsequent logic starts
--    at the same clock cycle. Due to no constraint on rst timing, the rst
--    signal could be released 1 or even 2 cycles earlier for one register in
--    the FPGA than for another register. The init pulse can be used to start
--    the init action, when rst has been released for all registers in the
--    whole FPGA for sure.
--
-- Remarks:
-- .  In LOFAR only init was used

entity common_init is
  generic (
    g_latency_w : natural := 4  -- >= 1
  );
  port (
    rst    : in  std_logic;
    clk    : in  std_logic;
    hold   : out std_logic;
    init   : out std_logic
  );
end;

architecture rtl of common_init is
  signal cnt           : std_logic_vector(g_latency_w downto 0);  -- use cnt(g_latency_w) to stop the counter
  signal cnt_en        : std_logic;
  signal prev_cnt_en   : std_logic;

  -- add extra output register stage to ease timing, because these signals will typically have a large fan out
  signal hold_reg      : std_logic;
  signal init_reg      : std_logic;
  signal nxt_init_reg  : std_logic;
begin
  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      prev_cnt_en   <= '1';
      hold          <= '1';
      hold_reg      <= '1';
      init          <= '0';
      init_reg      <= '0';
    elsif rising_edge(clk) then
      prev_cnt_en   <= cnt_en;
      hold_reg      <= cnt_en;
      hold          <= hold_reg;
      init_reg      <= nxt_init_reg;
      init          <= init_reg;
    end if;
  end process;

  cnt_en <= not cnt(cnt'high);

  nxt_init_reg <= '1' when cnt_en = '0' and prev_cnt_en = '1' else '0';

  u_counter : entity common_lib.common_counter
  generic map (
    g_width     => g_latency_w + 1
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => '0',
    cnt_en  => cnt_en,
    count   => cnt
  );
end rtl;
