-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- . Implements the functionality of beamformer_remote in node_sdp_beamformer.
-- Description:
-- The remote BF function adds the local and remote sums.
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use work.sdp_pkg.all;

entity sdp_beamformer_remote is
  generic (
    g_nof_aligners_max  : natural := c_sdp_N_pn_max
  );
  port (
    dp_clk        : in  std_logic;
    dp_rst        : in  std_logic;

    rn_index      : in  natural range 0 to c_sdp_N_pn_max - 1 := 0;

    local_bf_sosi : in  t_dp_sosi;
    from_ri_sosi  : in  t_dp_sosi;
    to_ri_sosi    : out t_dp_sosi;
    bf_sum_sosi   : out t_dp_sosi;

    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;

    reg_bsn_align_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_align_cipo : out t_mem_cipo;

    reg_bsn_monitor_v2_bsn_align_input_copi  : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_bsn_align_input_cipo  : out t_mem_cipo;

    reg_bsn_monitor_v2_bsn_align_output_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_bsn_align_output_cipo : out t_mem_cipo
  );
end sdp_beamformer_remote;

architecture str of sdp_beamformer_remote is
  constant c_data_w                : natural := c_nof_complex * c_sdp_W_beamlet_sum;
  constant c_block_size            : natural := c_sdp_S_sub_bf * c_sdp_N_pol_bf;
  constant c_fifo_size             : natural := 2**ceil_log2((c_block_size * 9) / 16);  -- 9/16 = 36/64, 1 block of
                                                -- 64 bit words rounded to the next power of 2 = 1024.

  -- Max 2 blocks latency per node in chain. Use c_bsn_latency_first_node = 1
  -- for first node is possible, because it does not have to align with remote
  -- input. By using c_bsn_latency_first_node = 1 the circular buffer size
  -- becomes true_log_pow2(1 + g_nof_aligners_max * c_bsn_latency_max +
  -- c_bsn_latency_first_node) = true_log_pow2(1 + (16 - 1) * 2 + 1) = 32
  -- blocks, instead of true_log_pow2(1 + 16 * 2) = 64 blocks.
  constant c_bsn_latency_max        : natural := 2;
  constant c_bsn_latency_first_node : natural := 1;

  signal chain_node_index        : natural range 0 to c_sdp_N_pn_max - 1 := 0;

  -- c_sdp_P_sum = 2 streams, 1 for local, 1 for remote
  signal dispatch_sosi_arr       : t_dp_sosi_arr(c_sdp_P_sum - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal dp_fifo_sosi            : t_dp_sosi := c_dp_sosi_rst;
  signal dp_fifo_siso            : t_dp_siso := c_dp_siso_rdy;
  signal beamlets_data_sosi_arr  : t_dp_sosi_arr(c_sdp_P_sum - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal beamlets_sosi_arr       : t_dp_sosi_arr(c_sdp_P_sum - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal i_bf_sum_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal bf_sum_data_sosi        : t_dp_sosi := c_dp_sosi_rst;
begin
  -- repacking beamlets re/im to data field.
  p_wire_local_bf_sosi : process(local_bf_sosi)
  begin
    dispatch_sosi_arr(0) <= local_bf_sosi;
    dispatch_sosi_arr(0).data(c_sdp_W_beamlet_sum - 1 downto 0) <=
                              local_bf_sosi.re(c_sdp_W_beamlet_sum - 1 downto 0);
    dispatch_sosi_arr(0).data(c_data_w - 1 downto c_sdp_W_beamlet_sum) <=
                              local_bf_sosi.im(c_sdp_W_beamlet_sum - 1 downto 0);
  end process;

  ---------------------------------------------------------------
  -- FIFO
  ---------------------------------------------------------------
  u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
  generic map (
    g_data_w     => c_longword_w,
    g_bsn_w      => c_dp_stream_bsn_w,
    g_use_bsn    => true,
    g_use_sync   => true,
    g_fifo_size  => c_fifo_size
  )
  port map (
    rst     => dp_rst,
    clk     => dp_clk,

    snk_in  => from_ri_sosi,
    src_in  => dp_fifo_siso,
    src_out => dp_fifo_sosi
  );

  ---------------------------------------------------------------
  -- Repack 64b to 36b
  ---------------------------------------------------------------
  u_dp_repack_data_rx : entity dp_lib.dp_repack_data
  generic map (
    g_in_dat_w       => c_longword_w,
    g_in_nof_words   => 9,  -- 9/16 = 36/64
    g_out_dat_w      => c_data_w,
    g_out_nof_words  => 16,  -- 9/16 = 36/64
    g_pipeline_ready => true
  )
  port map (
    rst => dp_rst,
    clk => dp_clk,

    snk_in  => dp_fifo_sosi,
    snk_out => dp_fifo_siso,
    src_out => dispatch_sosi_arr(1)
  );

  ---------------------------------------------------------------
  -- dp_bsn_aligner_v2
  ---------------------------------------------------------------

  -- The SDP beamformer starts at ring node 0 and outputs at the last ring
  -- node, therefore the chain_node_index = the rn_index. The chain_node_index
  -- does not wrap, because it starts at ring node 0. Therefore a design with
  -- an SDP beamformer that is defined for g_nof_aligners_max = c_sdp_N_pn_max
  -- = 16 will also work in a ring with less nodes.
  chain_node_index <= rn_index;

  u_mmp_dp_bsn_align_v2 : entity dp_lib.mmp_dp_bsn_align_v2
  generic map(
    -- for dp_bsn_align_v2
    g_nof_streams               => c_sdp_P_sum,
    g_bsn_latency_max           => c_bsn_latency_max,
    g_bsn_latency_first_node    => c_bsn_latency_first_node,
    g_nof_aligners_max          => g_nof_aligners_max,
    g_block_size                => c_block_size,
    g_data_w                    => c_data_w,
    g_use_mm_output             => false,
    g_rd_latency                => 1,
    -- for mms_dp_bsn_monitor_v2
    g_nof_clk_per_sync          => c_sdp_N_clk_sync_timeout,  -- Using c_sdp_N_clk_sync_timeout as g_nof_clk_per_sync
                                                              -- is used for BSN monitor timeout.
    g_nof_input_bsn_monitors    => c_sdp_P_sum,
    g_use_bsn_output_monitor    => true
    )
  port map (
    -- Memory-mapped clock domain
    mm_rst                  => mm_rst,
    mm_clk                  => mm_clk,

    reg_bsn_align_copi      => reg_bsn_align_copi,
    reg_bsn_align_cipo      => reg_bsn_align_cipo,

    reg_input_monitor_copi  => reg_bsn_monitor_v2_bsn_align_input_copi,
    reg_input_monitor_cipo  => reg_bsn_monitor_v2_bsn_align_input_cipo,

    reg_output_monitor_copi => reg_bsn_monitor_v2_bsn_align_output_copi,
    reg_output_monitor_cipo => reg_bsn_monitor_v2_bsn_align_output_cipo,

    -- Streaming clock domain
    dp_rst     => dp_rst,
    dp_clk     => dp_clk,

    chain_node_index => chain_node_index,

    -- Streaming input
    in_sosi_arr  => dispatch_sosi_arr,
    out_sosi_arr => beamlets_data_sosi_arr
  );

  -- repacking beamlets data to re/im field.
  p_wire_beamlets_sosi : process(beamlets_data_sosi_arr)
  begin
    beamlets_sosi_arr(0) <= beamlets_data_sosi_arr(0);
    beamlets_sosi_arr(1) <= beamlets_data_sosi_arr(1);
    beamlets_sosi_arr(0).re <=
        RESIZE_DP_DSP_DATA(beamlets_data_sosi_arr(0).data(c_sdp_W_beamlet_sum - 1 downto 0));
    beamlets_sosi_arr(0).im <=
        RESIZE_DP_DSP_DATA(beamlets_data_sosi_arr(0).data(           c_data_w - 1 downto c_sdp_W_beamlet_sum));
    beamlets_sosi_arr(1).re <=
        RESIZE_DP_DSP_DATA(beamlets_data_sosi_arr(1).data(c_sdp_W_beamlet_sum - 1 downto 0));
    beamlets_sosi_arr(1).im <=
        RESIZE_DP_DSP_DATA(beamlets_data_sosi_arr(1).data(           c_data_w - 1 downto c_sdp_W_beamlet_sum));
  end process;

  ---------------------------------------------------------------
  -- ADD local + remote
  ---------------------------------------------------------------
  u_dp_complex_add : entity dp_lib.dp_complex_add
  generic map(
    g_nof_inputs => c_sdp_P_sum,
    g_data_w => c_sdp_W_beamlet_sum
  )
  port map(
    rst   => dp_rst,
    clk   => dp_clk,

    snk_in_arr => beamlets_sosi_arr,
    src_out    => i_bf_sum_sosi
  );

  ---------------------------------------------------------------
  -- Local output
  ---------------------------------------------------------------
  bf_sum_sosi <= i_bf_sum_sosi;

  ---------------------------------------------------------------
  -- Ring output: Repack 36b to 64b
  ---------------------------------------------------------------

  -- repacking bf_sum re/im to data field
  p_wire_bf_sum_sosi : process(i_bf_sum_sosi)
  begin
    bf_sum_data_sosi <= i_bf_sum_sosi;
    bf_sum_data_sosi.data(c_sdp_W_beamlet_sum - 1 downto 0) <=
         i_bf_sum_sosi.re(c_sdp_W_beamlet_sum - 1 downto 0);
    bf_sum_data_sosi.data(           c_data_w - 1 downto c_sdp_W_beamlet_sum) <=
         i_bf_sum_sosi.im(c_sdp_W_beamlet_sum - 1 downto 0);
  end process;

  u_dp_repack_data_sum : entity dp_lib.dp_repack_data
  generic map (
    g_in_dat_w       => c_data_w,
    g_in_nof_words   => 16,  -- 16/9 = 64/36
    g_out_dat_w      => c_longword_w,
    g_out_nof_words  => 9,  -- 16/9 = 64/36
    g_pipeline_ready => true
  )
  port map (
    rst => dp_rst,
    clk => dp_clk,

    snk_in  => bf_sum_data_sosi,
    src_out => to_ri_sosi
  );
end str;
