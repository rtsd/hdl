###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
import sys
import os
import time
from common import *
from common_dsp import *
from file_io import *
from mem_init_file import list_to_hex

# Purpose:
# . Generate the NOF_INPUTS complex subband list containing the correlator
#   input streams.
# Description:
# . The function in this module generates correlator_snk_in_arr list used to:
#   . generate HEX files for the block generators (gen_correlator_snk_in_hex.py)
#   . feed to the correlator Python model ver_correlator_src_out.py)
# . Each incremental input receives an additional 1 degree of phase shift:
#   . Input  0 -  0 degrees
#   . Input  1 -  1 degrees
#   . ..
#   . Input 23 - 23 degrees

NOF_INPUTS = 8 
NOF_CHANNELS = 8
COMPLEX_WIDTH = 8 
NOF_PRE_MULT_FOLDS = 1 
NOF_INPUT_FOLDS = 1 # =< NOF_PRE_MULT_FOLDS


USE_NEW_REC_FILE = True #True = don't use outdated .rec file; wait for new one.
PLOT_CORR_OUT_PHASES = True # Plot the correlator output phases of channel 0 of both the VHDL and the Model

NOF_VISIBILITIES = (NOF_INPUTS*(NOF_INPUTS+1))/2
NOF_LINES_IN_REC_FILE_TO_VERIFY = NOF_CHANNELS*NOF_VISIBILITIES
NOF_INPUT_STREAMS = NOF_INPUTS/pow(2, NOF_INPUT_FOLDS)

NOF_MULTS = ceil_div(NOF_VISIBILITIES, pow(2, NOF_PRE_MULT_FOLDS))

INTEGRATION_PERIOD = NOF_MULTS # We always use the minimum integration period needed to be able to interleave the accumulator outputs onto one stream
CORRELATOR_MULT_OUTPUT_COMPLEX_WIDTH = 2* COMPLEX_WIDTH
CORRELATOR_OUTPUT_COMPLEX_WIDTH = CORRELATOR_MULT_OUTPUT_COMPLEX_WIDTH + ceil_log2(INTEGRATION_PERIOD)
NOF_WORDS_PER_BLOCK = NOF_CHANNELS*pow(2, NOF_INPUT_FOLDS)
MEM_WIDTH  = COMPLEX_WIDTH*2
MEM_DEPTH  = NOF_WORDS_PER_BLOCK
PATH = "../../src/hex"
FILENAME = "complex_subbands"

REC_FILE = os.environ['RADIOHDL']+'/libraries/dsp/correlator/tb/rec/correlator_src_out_arr0.rec'


def gen_correlator_snk_in_arr_hex(input_lists):
    """
    bla
    """
    concat_input_lists = concat_complex_2arr(input_lists, NOF_INPUTS, NOF_CHANNELS, COMPLEX_WIDTH)

    # ==========================================================
    # Interleave the lists if user wants folded correlator input
    # . Note: no support for more than 1 fold yet.
    # ==========================================================
    if NOF_INPUT_FOLDS==1:
        input_stream_lists = []
        for input_stream_nr in range(NOF_INPUT_STREAMS):
            input_stream_lists.append( interleave([concat_input_lists[2*input_stream_nr], concat_input_lists[2*input_stream_nr+1]] ) )
    elif NOF_INPUT_FOLDS==0:
        input_stream_lists = concat_input_lists
    
    # ====================
    # Write the HEX files
    # ====================
    for input_stream_nr in range(NOF_INPUT_STREAMS):
        list_to_hex( input_stream_lists[input_stream_nr], PATH+"/"+FILENAME+"_"+str(COMPLEX_WIDTH)+'b_fold_'+str(NOF_INPUT_FOLDS)+'_'+str(input_stream_nr)+".hex", MEM_WIDTH, MEM_DEPTH)

def compare_correlator_src_out_arr(vhdl_out, model_out):
    """
    Compare the VHDL correlator's output to the Python model output.
    """
    channel_nr = 0
    for vhdl_channel,model_channel in zip(vhdl_out, model_out):
        vis_nr = 0
        for vhdl_visibility,model_visibility in zip(vhdl_channel, model_channel):
            if not(vhdl_visibility==model_visibility):
                print 'compare_correlator_src_out_arr: FAILED'
                print '. channel', channel_nr, 'visibility', vis_nr
                print '  . model:', model_visibility
                print '  . VHDL :', vhdl_visibility
                sys.exit()
            vis_nr+=1
        channel_nr+=1
    print 'Checked %d channels, %d visibilities each: PASSED' %(channel_nr, vis_nr)




def run_tb():
    """
    
    """
    import subprocess
    import node_io
    import test_case


    # Create a .do file for ModelSim
    c_do_file_name  = os.environ['HOME']+"/.ms_do.do"
    library_name = 'correlator'
    tb_name = 'tb_correlator'
    vhdl_generics = {}
    vhdl_generics['g_nof_inputs'] = 8
    other=None
    tc = test_case.Testcase(tb_name.upper()+' - ', '')
    io = node_io.NodeIO(tc.nodeImages, 'sim')
    io.simIO.make_do_file(os.environ['RADIOHDL'], c_do_file_name, library_name, tb_name, vhdl_generics, other)



    # First setup the ModelSim environment for unb1
#    cmd = '/home/schuur/SVN/RadioHDL/trunk/tools/modelsim/set_modelsim'
#    args = 'unb1'
#    subprocess.call([cmd, args])

    # Start modelsim in a separate terminal
#    ms_cmd = ["konsole", "-e", "/home/software/Mentor/6.6c/modeltech/linux_x86_64/vsim", "-c", "-do", c_do_file_name]
#    ms = subprocess.Popen(ms_cmd, shell=False, close_fds=True, preexec_fn=os.setsid)
 
    ms_cmd = ["konsole", "-e", "run_modelsim", "unb1", "-do", c_do_file_name]









if __name__ == '__main__':

    # Generate input data
    correlator_snk_in_arr_complex = gen_complex_inputs(NOF_INPUTS, NOF_CHANNELS, COMPLEX_WIDTH)
    
    # Convert to int and generate HEX files from input data
    correlator_snk_in_arr_int = complex_to_int(correlator_snk_in_arr_complex, NOF_INPUTS, NOF_CHANNELS)
    
    gen_correlator_snk_in_arr_hex(correlator_snk_in_arr_int)
    
    # Calculate correlator output from input lists
    correlator_src_out_arr_ref = correlate(correlator_snk_in_arr_int, INTEGRATION_PERIOD)
    
    
    # (run TB)
    #run_tb()
    
    if USE_NEW_REC_FILE==True:
        # Wait for the correlator output
        print 'Waiting for %s to be modified...' %REC_FILE
        now = time.time()
        do_until_gt(file_mod_time, now, s_timeout=900, filename=REC_FILE)
        
        print '%s modified. Waiting for %d lines to be written...' %(REC_FILE, NOF_LINES_IN_REC_FILE_TO_VERIFY)
        do_until_gt(file_linecount, NOF_LINES_IN_REC_FILE_TO_VERIFY, s_timeout=900, filename=REC_FILE)
    else:
        print 'Using old .rec file'
    
    
    
    
    # Read test bench output file written by dp_stream_rec_play.vhd
    correlator_src_out_arr = rec_file_to_complex(REC_FILE, complex_width=CORRELATOR_OUTPUT_COMPLEX_WIDTH)
    
    # Compare modeloutput to VHDL output
    compare_correlator_src_out_arr(correlator_src_out_arr, correlator_src_out_arr_ref)
    
    if PLOT_CORR_OUT_PHASES==True:  
      plot_phase_shifts(correlator_src_out_arr, NOF_VISIBILITIES)
      plot_phase_shifts(correlator_src_out_arr_ref, NOF_VISIBILITIES)
