 -------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM access via slave register to diag_rx_seq
-- Description:
--
--   Each DP stream has its own diag_rx_seq and its own MM control register.
--   The MM control registers are accessible via a single MM port thanks to
--   the common_mem_mux. Each single MM control register is defined as:
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |                                         diag_sel = [1], diag_en = [0] |  0  RW
--  |-----------------------------------------------------------------------|
--  |                                       res_val_n = [1], res_ok_n = [0] |  1  RO
--  |-----------------------------------------------------------------------|
--  |                                      rx_cnt[31:0]                     |  2  RO
--  |-----------------------------------------------------------------------|
--  |                              rx_sample[g_seq_dat_w-1:0]               |  3  RO
--  |-----------------------------------------------------------------------|
--  |                      diag_steps_arr[0][g_seq_dat_w-1:0]               |  4  RW
--  |-----------------------------------------------------------------------|
--  |                      diag_steps_arr[1][g_seq_dat_w-1:0]               |  5  RW
--  |-----------------------------------------------------------------------|
--  |                      diag_steps_arr[2][g_seq_dat_w-1:0]               |  6  RW
--  |-----------------------------------------------------------------------|
--  |                      diag_steps_arr[3][g_seq_dat_w-1:0]               |  7  RW
--  |-----------------------------------------------------------------------|
--
-- . g_nof_streams
--   The MM control register for stream I in 0:g_nof_streams-1 starts at word
--   index wi = I * 2**c_mm_reg.adr_w.
--
-- . diag_en
--     '0' = stop and reset input sequence verification
--     '1' = enable input sequence verification
--
-- . diag_sel
--     '0' = verify PSRG data
--     '1' = verify CNTR data
--
-- . Results
--   When res_val_n = '1' then no valid data is being received. When
--   res_val_n = '0' then at least two valid data have been received so the
--   diag_rx_seq can detect whether the subsequent data is ok. When res_ok_n
--   = '0' then indeed all data that has been received so far is correct.
--   When res_ok_n = '1' then at least 1 data word was received with errors.
--   Once res_ok_n goes high it remains high.
--
-- . g_data_w and g_seq_dat_w
--   The DP streaming data field is c_dp_stream_data_w bits wide but only
--   g_data_w bits are used. The g_seq_dat_w must be >= 1 and <= g_data_w.
--   If g_seq_dat_w < g_data_w then the data carries replicated copies of
--   the g_seq_dat_w. The maximum g_seq_dat_w depends on the pseudo random
--   data width of the LFSR sequeces in common_lfsr_sequences_pkg and on
--   whether timing closure can still be achieved for wider g_seq_dat_w.
--   Thanks to the replication a smaller g_seq_dat_w can be used to provide
--   CNTR or LFSR data for the DP data. If the higher bits do notmatch the
--   sequence in the lower bits, then the rx data is forced to -1, and that
--   will then be detected and reported by u_diag_rx_seq as a sequence error.
--
-- . rx_cnt
--   Counts the number of valid input data that was received since diag_en
--   went active. An incrementing rx_cnt shows that data is being received.
--
-- . rx_sample
--   The rx_sample keeps the last valid in_dat value. When diag_en='0' it is
--   reset to 0. Reading rx_sample via MM gives an impression of the valid
--   in_dat activity.
--
-- . g_use_steps
--   When g_use_steps=FALSE then diag_sel selects whether PSRG or COUNTER
--   data with increment +1 is used to verify the input data.
--   When g_use_steps=TRUE then the g_nof_steps =
--   c_diag_seq_rx_reg_nof_steps = 4 MM step registers define the allowed
--   COUNTER increment values.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.diag_pkg.all;

entity mms_diag_rx_seq is
  generic (
    g_nof_streams : natural := 1;
    g_use_steps   : boolean := false;
    g_nof_steps   : natural := c_diag_seq_rx_reg_nof_steps;
    g_seq_dat_w   : natural := c_word_w;  -- >= 1, test sequence data width
    g_data_w      : natural := c_word_w  -- >= g_seq_dat_w, user data width
  );
  port (
    -- Clocks and reset
    mm_rst         : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk         : in  std_logic;  -- MM bus clock
    dp_rst         : in  std_logic;  -- reset synchronous with dp_clk
    dp_clk         : in  std_logic;  -- DP streaming bus clock

    -- Memory Mapped Slave
    reg_mosi       : in  t_mem_mosi;  -- multiplexed port for g_nof_streams MM control/status registers
    reg_miso       : out t_mem_miso;

    -- Streaming interface
    rx_snk_in_arr  : in t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_diag_rx_seq;

architecture str of mms_diag_rx_seq is
  -- Define MM slave register size
  constant c_mm_reg      : t_c_mem  := (latency  => 1,
                                        adr_w    => c_diag_seq_rx_reg_adr_w,
                                        dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                        nof_dat  => c_diag_seq_rx_reg_nof_dat,
                                        init_sl  => '0');

  -- Define MM slave register fields for Python peripheral using pi_common.py (specify MM register access per word, not per individual bit because mm_fields assumes 1 field per MM word)
  constant c_mm_reg_field_arr : t_common_field_arr(c_mm_reg.nof_dat - 1 downto 0) := ( ( field_name_pad("step_3"),    "RW", c_word_w, field_default(0) ),  -- [7] = diag_steps_arr[3], c_diag_seq_rx_reg_nof_steps = 4
                                                                                     ( field_name_pad("step_2"),    "RW", c_word_w, field_default(0) ),  -- [6] = diag_steps_arr[2]
                                                                                     ( field_name_pad("step_1"),    "RW", c_word_w, field_default(0) ),  -- [5] = diag_steps_arr[1]
                                                                                     ( field_name_pad("step_0"),    "RW", c_word_w, field_default(0) ),  -- [4] = diag_steps_arr[0]
                                                                                     ( field_name_pad("rx_sample"), "RO", c_word_w, field_default(0) ),  -- [3]
                                                                                     ( field_name_pad("rx_cnt"),    "RO", c_word_w, field_default(0) ),  -- [2]
                                                                                     ( field_name_pad("result"),    "RO",        2, field_default(0) ),  -- [1] = result[1:0]  = res_val_n & res_ok_n
                                                                                     ( field_name_pad("control"),   "RW",        2, field_default(0) ));  -- [0] = control[1:0] = diag_sel & diag_en

  constant c_reg_slv_w   : natural := c_mm_reg.nof_dat * c_mm_reg.dat_w;
  constant c_reg_dat_w   : natural := smallest(c_word_w, g_seq_dat_w);

  constant c_nof_steps_wi     : natural := c_diag_seq_rx_reg_nof_steps_wi;

  type t_reg_slv_arr is array (integer range <>) of std_logic_vector(c_reg_slv_w - 1 downto 0);
  type t_seq_dat_arr is array (integer range <>) of std_logic_vector(g_seq_dat_w - 1 downto 0);
  type t_data_arr    is array (integer range <>) of std_logic_vector(g_data_w - 1 downto 0);
  type t_steps_2arr  is array (integer range <>) of t_integer_arr(g_nof_steps - 1 downto 0);

  signal reg_mosi_arr        : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr        : t_mem_miso_arr(g_nof_streams - 1 downto 0);

  -- Registers in dp_clk domain
  signal ctrl_reg_arr        : t_reg_slv_arr(g_nof_streams - 1 downto 0) := (others => (others => '0'));
  signal stat_reg_arr        : t_reg_slv_arr(g_nof_streams - 1 downto 0) := (others => (others => '0'));

  signal diag_en_arr         : std_logic_vector(g_nof_streams - 1 downto 0);
  signal diag_sel_arr        : std_logic_vector(g_nof_streams - 1 downto 0);
  signal diag_steps_2arr     : t_steps_2arr(g_nof_streams - 1 downto 0);

  signal rx_cnt_arr          : t_slv_32_arr(g_nof_streams - 1 downto 0);  -- can use t_slv_32_arr because c_mm_reg.dat_w = c_word_w = 32 fixed
  signal rx_sample_arr       : t_seq_dat_arr(g_nof_streams - 1 downto 0);
  signal rx_sample_diff_arr  : t_seq_dat_arr(g_nof_streams - 1 downto 0);
  signal rx_sample_val_arr   : std_logic_vector(g_nof_streams - 1 downto 0);
  signal rx_seq_arr          : t_seq_dat_arr(g_nof_streams - 1 downto 0);
  signal rx_seq_val_arr      : std_logic_vector(g_nof_streams - 1 downto 0);
  signal rx_data_arr         : t_data_arr(g_nof_streams - 1 downto 0);
  signal rx_data_val_arr     : std_logic_vector(g_nof_streams - 1 downto 0);

  signal diag_res_arr        : t_seq_dat_arr(g_nof_streams - 1 downto 0);
  signal diag_res_val_arr    : std_logic_vector(g_nof_streams - 1 downto 0);

  signal stat_res_ok_n_arr   : std_logic_vector(g_nof_streams - 1 downto 0);
  signal stat_res_val_n_arr  : std_logic_vector(g_nof_streams - 1 downto 0);
begin
  assert g_data_w >= g_seq_dat_w
    report "mms_diag_rx_seq: g_data_w < g_seq_dat_w is not allowed."
    severity FAILURE;

  gen_nof_streams: for I in 0 to g_nof_streams - 1 generate
    -- no unreplicate needed
    gen_one : if g_data_w = g_seq_dat_w generate
      rx_seq_arr(I)     <= rx_snk_in_arr(i).data(g_seq_dat_w - 1 downto 0);
      rx_seq_val_arr(I) <= rx_snk_in_arr(i).valid;
    end generate;

    -- unreplicate needed
    gen_unreplicate : if g_data_w > g_seq_dat_w generate
      -- keep sequence in low bits and set high bits to '1' if they mismatch the corresponding bit in the sequence
      rx_data_arr(I)     <= UNREPLICATE_DP_DATA(rx_snk_in_arr(i).data(g_data_w - 1 downto 0), g_seq_dat_w);
      rx_data_val_arr(I) <=                     rx_snk_in_arr(i).valid;

      -- keep sequence in low bits if the high bits match otherwise force low bits value to -1 to indicate the mismatch
      p_rx_seq : process(dp_clk)
      begin
        if rising_edge(dp_clk) then  -- register to ease timing closure
          if unsigned(rx_data_arr(I)(g_data_w - 1 downto g_seq_dat_w)) = 0 then
            rx_seq_arr(I) <= rx_data_arr(I)(g_seq_dat_w - 1 downto 0);
          else
            rx_seq_arr(I) <= TO_SVEC(-1, g_seq_dat_w);
          end if;
          rx_seq_val_arr(I) <= rx_data_val_arr(I);
        end if;
      end process;
    end generate;

    -- detect rx sequence errors
    u_diag_rx_seq: entity WORK.diag_rx_seq
    generic map (
      g_use_steps       => g_use_steps,
      g_nof_steps       => g_nof_steps,
      g_cnt_w           => c_word_w,
      g_dat_w           => g_seq_dat_w,
      g_diag_res_w      => g_seq_dat_w  -- do not use g_seq_dat_w+1 to include NOT diag_res_val in MSbit, instead use diag_res_val output
    )
    port map (
      rst               => dp_rst,
      clk               => dp_clk,

      -- Write and read back registers:
      diag_en           => diag_en_arr(I),
      diag_sel          => diag_sel_arr(I),
      diag_steps_arr    => diag_steps_2arr(I),

      -- Read only registers:
      diag_res          => diag_res_arr(I),
      diag_res_val      => diag_res_val_arr(I),
      diag_sample       => rx_sample_arr(I),
      diag_sample_diff  => rx_sample_diff_arr(I),
      diag_sample_val   => rx_sample_val_arr(I),

      -- Streaming
      in_cnt            => rx_cnt_arr(I),
      in_dat            => rx_seq_arr(I),
      in_val            => rx_seq_val_arr(I)
    );

    -- Map diag_res to single bit and register it to ease timing closure
    stat_res_ok_n_arr(I)  <= orv(diag_res_arr(I))    when rising_edge(dp_clk);
    stat_res_val_n_arr(I) <= not diag_res_val_arr(I) when rising_edge(dp_clk);

    -- Register mapping
    -- . write ctrl_reg_arr
    diag_en_arr(I)   <= ctrl_reg_arr(I)(0);  -- address 0, data bit [0]
    diag_sel_arr(I)  <= ctrl_reg_arr(I)(1);  -- address 0, data bit [1]

    gen_diag_steps_2arr : for J in 0 to g_nof_steps - 1 generate
      diag_steps_2arr(I)(J) <= TO_SINT(ctrl_reg_arr(I)(c_reg_dat_w - 1 + (c_nof_steps_wi + J) * c_word_w downto (c_nof_steps_wi + J) * c_word_w));  -- address 4, 5, 6, 7
    end generate;

    -- . read stat_reg_arr
    p_stat_reg_arr : process(ctrl_reg_arr, stat_res_ok_n_arr, stat_res_val_n_arr, rx_cnt_arr, rx_sample_arr)
    begin
      -- Default write / readback:
      stat_reg_arr(I) <= ctrl_reg_arr(I);  -- default control read back
      -- Status read only:
      stat_reg_arr(I)(                  0 + 1 * c_word_w) <= stat_res_ok_n_arr(I);  -- address 1, data bit [0]
      stat_reg_arr(I)(                  1 + 1 * c_word_w) <= stat_res_val_n_arr(I);  -- address 1, data bit [1]
      stat_reg_arr(I)(3 * c_word_w - 1 downto 2 * c_word_w) <= rx_cnt_arr(I);  -- address 2: read rx_cnt per stream
      stat_reg_arr(I)(4 * c_word_w - 1 downto 3 * c_word_w) <= RESIZE_UVEC(rx_sample_arr(I), c_word_w);  -- address 3: read valid sample per stream
    end process;

    u_reg : entity common_lib.common_reg_r_w_dc
    generic map (
      g_cross_clock_domain => true,
      g_readback           => false,  -- must use FALSE for write/read or read only register when g_cross_clock_domain=TRUE
      g_reg                => c_mm_reg
    )
    port map (
      -- Clocks and reset
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,
      st_rst      => dp_rst,
      st_clk      => dp_clk,

      -- Memory Mapped Slave in mm_clk domain
      sla_in      => reg_mosi_arr(I),
      sla_out     => reg_miso_arr(I),

      -- MM registers in dp_clk domain
      in_reg      => stat_reg_arr(I),
      out_reg     => ctrl_reg_arr(I)
    );
  end generate;

  -- Combine the internal array of mm interfaces for the bg_data to one array that is connected to the port of the MM bus
  u_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_mm_reg.adr_w
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );
end str;
