-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.mdio_pkg.all;

entity mdio_phy_reg is
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;
    mm_clk            : in  std_logic;
    mdio_rst          : in  std_logic;
    mdio_clk          : in  std_logic;

    -- Memory Mapped Slave in mm_clk domain
    sla_in            : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out           : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in mdio_clk domain
    mdio_en_evt       : out std_logic;
    mdio_done_ack_evt : out std_logic;
    mdio_done         : in  std_logic;

    mdio_hdr          : out std_logic_vector(c_halfword_w - 1 downto 0);
    mdio_tx_dat       : out std_logic_vector(c_halfword_w - 1 downto 0);
    mdio_rx_dat       : in  std_logic_vector(c_halfword_w - 1 downto 0)
   );
end mdio_phy_reg;

architecture rtl of mdio_phy_reg is
  constant c_mm_reg        : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(5),
                                         dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                         nof_dat  => 5,
                                         init_sl  => '0');
  -- Registers in mm_clk domain
  signal mm_en_evt       : std_logic;
  signal mm_done_ack_evt : std_logic;
  signal mm_done         : std_logic;

  signal mm_hdr          : std_logic_vector(c_halfword_w - 1 downto 0);
  signal mm_tx_dat       : std_logic_vector(c_halfword_w - 1 downto 0);
  signal mm_rx_dat       : std_logic_vector(c_halfword_w - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out   <= c_mem_miso_rst;
      -- Write access, register values
      mm_done_ack_evt <= '0';
      mm_en_evt       <= '0';
      mm_tx_dat       <= (others => '0');
      mm_hdr          <= (others => '0');
    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write event defaults
      mm_done_ack_evt <= '0';
      mm_en_evt       <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Write Block Sync
          when 0 =>
            mm_hdr          <= sla_in.wrdata(c_halfword_w - 1 downto 0);
            mm_en_evt       <= '1';
          when 1 =>
            mm_tx_dat       <= sla_in.wrdata(c_halfword_w - 1 downto 0);
          when 4 =>
            mm_done_ack_evt <= sla_in.wrdata(0);
          when others => null;  -- unused MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Read Block Sync
          when 2 =>
            sla_out.rddata(c_halfword_w - 1 downto 0) <= mm_rx_dat;
          when 3 =>
            sla_out.rddata(0) <= mm_done;
          when others => null;  -- unused MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and mdio_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the safe side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  mdio_tx_dat <= mm_tx_dat;
  mm_rx_dat   <= mdio_rx_dat;

  u_cross_domain_hdr : entity common_lib.common_reg_cross_domain
  port map (
    in_rst      => mm_rst,
    in_clk      => mm_clk,
    in_new      => mm_en_evt,
    in_dat      => mm_hdr,
    in_done     => OPEN,
    out_rst     => mdio_rst,
    out_clk     => mdio_clk,
    out_dat     => mdio_hdr,
    out_new     => mdio_en_evt
  );

  u_spulse_done_evt : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_done_ack_evt,
    in_busy   => OPEN,
    out_rst   => mdio_rst,
    out_clk   => mdio_clk,
    out_pulse => mdio_done_ack_evt
  );

  u_async_done : entity common_lib.common_async
  generic map (
    g_rst_level => '0'
  )
  port map (
    rst  => mm_rst,
    clk  => mm_clk,
    din  => mdio_done,
    dout => mm_done
  );
end rtl;
