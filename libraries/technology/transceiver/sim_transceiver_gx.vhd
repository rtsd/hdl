--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author: Eric Kooistra / Daniel van der Schuur
-- Purpose:
--   Drop-in simulation model for tech_transceiver_gx.vhd.
-- Description:
--   A fast serdes for simulation within tr_nonbonded.
--
--   Internally the GX model divides the c_tr_clk_period up into g_data_w * 10/8
--   smaller periods that transport the data and the overhead. The model uses
--   some artifical 10b/8b scheme to encode the overhead. The GX model is
--   inspired on 10/8 encoding, however it will also suit 66/64 encoding,
--   because the GX model does not model the encoding, it only ensures that the
--   data can be transported as a bit stream.
--
--   The c_tr_clk_period is set sufficiently short to fit within any actual
--   tr_clk period. This does not slow down the simulation, because
--   after the last data has been processed the GX model waits for the next
--   tr_clk rising_edge, so the amount of toggling is independent of
--   c_tr_clk_period. To be sure the model measures the actual tr_clk period,
--   which is assumed to be related to the user data rate clock. The actual
--   tr_clk_period must be >= c_tr_clk_period, because otherwise the GX model
--   will miss data. After every data word the GX serializer and deserializer
--   models realign with the actual tr_clk. Therefore it is possible to have
--   tr_clk_period > c_tr_clk_period.
--
--   The sim model can be stopped by external tb_end input or by stopping the
--   tr_clk, which is detected via internal tr_end. Using tr_end is useful
--   when tb_end cannot be provided via the tb, e.g. because tb_end is not
--   available as IN port on top level FPGA entities. Using tb_end IN is kept
--   to be remain compatible with tb that use it.
--
-- Remarks:
--   None

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity sim_transceiver_gx is
  generic(
    g_data_w     : natural;
    g_nof_gx     : natural;
    g_mbps       : natural;  -- line rate in Mbps so including 10/8 encoding
    g_tx         : boolean;
    g_rx         : boolean
  );
  port(
    tb_end       : in  std_logic := '0';  -- in simulation stop internal clocks when tb_end='1' to support 'run -all'

    tr_clk       : in  std_logic;

    rx_clk       : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_rst       : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_sosi_arr  : out t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    rx_siso_arr  : in  t_dp_siso_arr(g_nof_gx - 1 downto 0);

    tx_clk       : out std_logic_vector(g_nof_gx - 1 downto 0);
    tx_rst       : out std_logic_vector(g_nof_gx - 1 downto 0);
    tx_sosi_arr  : in  t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    tx_siso_arr  : out t_dp_siso_arr(g_nof_gx - 1 downto 0);

    --Serial I/O
    rx_datain    : in  std_logic_vector(g_nof_gx - 1 downto 0);
    tx_dataout   : out std_logic_vector(g_nof_gx - 1 downto 0)
  );
end sim_transceiver_gx;

architecture str of sim_transceiver_gx is
  constant c_data_clk_period  : time := 1 us * g_data_w * 10 / 8 / g_mbps;
  constant c_tr_clk_period    : time := 1 ns;  -- run the GX model somewhat faster to support shorter tr_clk_period

  type t_ctrl_2arr is array(g_nof_gx - 1 downto 0) of std_logic_vector(g_data_w / c_byte_w - 1 downto 0);

  signal tr_rst      : std_logic;

  signal tx_ready    : std_logic;

  signal tx_in_ctrl  : t_ctrl_2arr;
  signal tx_in_sop   : t_ctrl_2arr;
  signal tx_in_eop   : t_ctrl_2arr;
  signal rx_out_ctrl : t_ctrl_2arr;
  signal rx_out_sop  : t_ctrl_2arr;
  signal rx_out_eop  : t_ctrl_2arr;

  signal dbg_c_data_clk_period : time := c_data_clk_period;  -- view constant in Wave window
  signal dbg_c_tr_clk_period   : time := c_tr_clk_period;  -- view constant in Wave window
  signal tr_clk_period         : time;

  signal tr_end      : std_logic := '0';  -- local detect whether the tr_clk has stopped by continuosly checking the toggling
  signal sim_end     : std_logic := '0';  -- in simulation stop internal clocks when tb_end='1' or tr_end='1' to support 'run -all'
begin
  u_areset_tr_rst : entity common_lib.common_areset
  generic map(
    g_rst_level => '1'
  )
  port map(
    clk     => tr_clk,
    in_rst  => '0',
    out_rst => tr_rst
  );

  p_check_tr_clk_period : process
    variable v_period : time;
  begin
    -- wait until tr_rst released, so tr_clk is running
    wait until tr_rst = '0';

    -- measure tr_clk period
    wait until rising_edge(tr_clk);
    v_period := NOW;
    wait until rising_edge(tr_clk);
    v_period := NOW - v_period;
    wait until rising_edge(tr_clk);
    assert v_period >= c_tr_clk_period
      report "Actual tr_clk period = " & time_to_str(v_period) & " should be >= c_tr_clk_period = " & time_to_str(c_tr_clk_period)
      severity ERROR;

    -- use signal to show tr_clk period in Wave window
    tr_clk_period <= v_period;

    -- detect whether tr_clk has stopped by continuously checking the toggling (cannot use WAIT UNTIL
    -- rising_edge(tr_clk) because tr_clk is the item that is checked).
    wait until rising_edge(tr_clk);
    wait for c_tr_clk_period / 4;  -- to be sure that tr_clk='1' before starting the while TRUE loop
    while true loop
      if tr_clk /= '1' then  -- level of tr_clk after rising_edge()
        exit;
      end if;
      wait for v_period / 2;
      if tr_clk /= '0' then  -- level of tr_clk after half period
        exit;
      end if;
      wait for v_period / 2;
    end loop;

    tr_end <= '1';
    wait;
  end process;

  sim_end <= tb_end or tr_end;  -- combine external and internal stop sim conditions

  tx_clk <= (others => tr_clk);
  tx_rst <= (others => tr_rst);
  rx_clk <= (others => tr_clk);
  rx_rst <= (others => tr_rst);

  p_tx_rdy : process
    variable v_tx_clk_cnt : natural := 0;
  begin
    tx_ready <= '0';
    wait for 1084.8 ns;  -- Time until tx_rst (in tr_clk domain) is deasserted: tx init done
    while v_tx_clk_cnt < 1000 loop  -- 1000 tx clk cycles until alignment is completed
      wait until rising_edge(tr_clk);
      v_tx_clk_cnt := v_tx_clk_cnt + 1;
      tx_ready <= '1';
    end loop;
    wait;
  end process;

  gen_sim: for i in 0 to g_nof_gx - 1 generate
    gen_tx : if g_tx = true generate
      tx_siso_arr(i).ready <= tx_ready;
      tx_siso_arr(i).xon   <= tx_ready;

      tx_in_ctrl(i) <= (others => tx_sosi_arr(i).valid);
      tx_in_sop(i)  <= (others => tx_sosi_arr(i).sop);
      tx_in_eop(i)  <= (others => tx_sosi_arr(i).eop);

      u_ser: entity work.sim_transceiver_serializer
      generic map (
        g_data_w         => g_data_w,
        g_tr_clk_period  => c_tr_clk_period
      )
      port map (
        tb_end        => sim_end,

        tr_clk        => tr_clk,
        tr_rst        => tr_rst,

        tx_in_data    => tx_sosi_arr(i).data(g_data_w - 1 downto 0),
        tx_in_ctrl    => tx_in_ctrl(i),
        tx_in_sop     => tx_in_sop(i),
        tx_in_eop     => tx_in_eop(i),

        tx_serial_out => tx_dataout(i)
      );
    end generate;

    gen_rx : if g_rx = true generate
      u_des: entity work.sim_transceiver_deserializer
      generic map (
        g_data_w         => g_data_w,
        g_tr_clk_period  => c_tr_clk_period
      )
      port map (
        tb_end        => sim_end,

        tr_clk        => tr_clk,
        tr_rst        => tr_rst,

        rx_out_data   => rx_sosi_arr(i).data(g_data_w - 1 downto 0),
        rx_out_ctrl   => rx_out_ctrl(i),
        rx_out_sop    => rx_out_sop(i),
        rx_out_eop    => rx_out_eop(i),

        rx_serial_in  => rx_datain(i)
      );
    end generate;

    rx_sosi_arr(i).valid <= andv(rx_out_ctrl(i));
    rx_sosi_arr(i).sop   <= andv(rx_out_sop(i));
    rx_sosi_arr(i).eop   <= andv(rx_out_eop(i));
  end generate;
end str;
