-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: -
-- Purpose:
--   1) Initial setup eth1g via the MM tse and MM reg port
--   2) Loop control eth1g via MM reg port and reg_interrupt to receive and
--      transmit packets via the MM ram port.
-- Description:
--
-------------------------------------------------------------------------------

library IEEE, common_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;

package eth1g_mem_pkg is
  ------------------------------------------------------------------------------
  -- MM bus access functions
  ------------------------------------------------------------------------------

  -- The mm_miso input needs to be declared as signal, because otherwise the
  -- procedure does not notice a change (also not when the mm_clk is declared
  -- as signal).

  -- Write data to the MM bus
  procedure proc_mem_mm_bus_wr(constant wr_addr : in  natural;  -- [31:0]
                               constant wr_data : in  integer;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;  -- used for waitrequest
                               signal   mm_mosi : out t_mem_mosi);

  procedure proc_mem_mm_bus_wr(constant wr_addr : in  integer;  -- [31:0]
                               signal   wr_data : in  std_logic_vector;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;  -- used for waitrequest
                               signal   mm_mosi : out t_mem_mosi);

  -- Read data request to the MM bus
  procedure proc_mem_mm_bus_rd(constant rd_addr : in  natural;  -- [31:0]
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;  -- used for waitrequest
                               signal   mm_mosi : out t_mem_mosi);

  -- Wait for read data valid after read latency mm_clk cycles
  procedure proc_mem_mm_bus_rd_latency(constant c_rd_latency : in natural;
                                       signal   mm_clk       : in std_logic);

-- supported packet data types
  constant c_tech_tse_data_type_symbols : natural := 0;
  constant c_tech_tse_data_type_counter : natural := 1;
  constant c_tech_tse_data_type_arp     : natural := 2;
  constant c_tech_tse_data_type_ping    : natural := 3;  -- over IP/ICMP
  constant c_tech_tse_data_type_udp     : natural := 4;  -- over IP

  function func_tech_tse_header_size(data_type : natural) return natural;  -- raw ethernet: 4 header words, protocol ethernet: 11 header words

  -- Configure the TSE MAC
  procedure proc_tech_tse_setup(constant c_promis_en         : in  boolean;
                                constant c_tse_tx_fifo_depth : in  natural;
                                constant c_tse_rx_fifo_depth : in  natural;
                                constant c_tx_ready_latency  : in  natural;
                                constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                signal   psc_access          : out std_logic;
                                signal   mm_clk              : in  std_logic;
                                signal   mm_miso             : in  t_mem_miso;
                                signal   mm_mosi             : out t_mem_mosi);
end eth1g_mem_pkg;

package body eth1g_mem_pkg is
  ------------------------------------------------------------------------------
  -- Private functions
  ------------------------------------------------------------------------------

  -- Issues a rd or a wr MM access
  procedure proc_mm_access(signal mm_clk    : in  std_logic;
                           signal mm_access : out std_logic) is
  begin
    mm_access <= '1';
    if rising_edge(mm_clk) then
      mm_access <= '0';
    end if;

  end proc_mm_access;

  -- Issues a rd or a wr MM access and wait for it to have finished
  procedure proc_mm_access(signal mm_clk     : in  std_logic;
                           signal mm_waitreq : in  std_logic;
                           signal mm_access  : out std_logic) is
  begin
    mm_access <= '1';
    wait until rising_edge(mm_clk);
    while mm_waitreq = '1' loop
      wait until rising_edge(mm_clk);
    end loop;
    mm_access <= '0';

  end proc_mm_access;

  function func_map_pcs_addr(pcs_addr : natural) return natural is
  begin
    return pcs_addr * 2 + c_tech_tse_byte_addr_pcs_offset;
  end func_map_pcs_addr;

  ------------------------------------------------------------------------------
  -- Public functions
  ------------------------------------------------------------------------------

  -- Write data to the MM bus
  procedure proc_mem_mm_bus_wr(constant wr_addr : in  natural;
                               constant wr_data : in  integer;
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;
                               signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_mosi.wrdata  <= TO_MEM_DATA(wr_data);
    proc_mm_access(mm_clk, mm_miso.waitrequest, mm_mosi.wr);

  end proc_mem_mm_bus_wr;

  procedure proc_mem_mm_bus_wr(constant wr_addr : in  integer;
                                 signal   wr_data : in  std_logic_vector;
                                 signal   mm_clk  : in  std_logic;
                                 signal   mm_miso : in  t_mem_miso;
                                 signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_mosi.wrdata  <= RESIZE_MEM_DATA(wr_data);
    proc_mm_access(mm_clk, mm_miso.waitrequest, mm_mosi.wr);
  end proc_mem_mm_bus_wr;

  -- Read data request to the MM bus
  -- Use proc_mem_mm_bus_rd_latency() to wait for the MM MISO rd_data signal
  -- to show the data after some read latency
  procedure proc_mem_mm_bus_rd(constant rd_addr : in  natural;
                               signal   mm_clk  : in  std_logic;
                               signal   mm_miso : in  t_mem_miso;
                               signal   mm_mosi : out t_mem_mosi) is
  begin
    mm_mosi.address <= TO_MEM_ADDRESS(rd_addr);
    mm_mosi.rd <= '1';
    proc_mm_access(mm_clk, mm_miso.waitrequest, mm_mosi.rd);
    mm_mosi.rd <= '0';
  end proc_mem_mm_bus_rd;

  -- Wait for read data valid after read latency mm_clk cycles
  -- Directly assign mm_miso.rddata to capture the read data
  procedure proc_mem_mm_bus_rd_latency(constant c_rd_latency : in natural;
                                       signal   mm_clk       : in std_logic) is
  begin
    for I in 0 to c_rd_latency - 1 loop wait until rising_edge(mm_clk); end loop;
  end proc_mem_mm_bus_rd_latency;

  function func_tech_tse_header_size(data_type : natural) return natural is
  begin
    case data_type is
      when c_tech_tse_data_type_symbols => return c_network_total_header_32b_eth_nof_words;
      when c_tech_tse_data_type_counter => return c_network_total_header_32b_eth_nof_words;
      when others => null;
    end case;
    return c_network_total_header_32b_nof_words;
  end func_tech_tse_header_size;

  -- . The src_mac[47:0] = 0x123456789ABC for MAC address 12-34-56-78-9A-BC
  procedure proc_tech_tse_setup(constant c_promis_en         : in  boolean;
                                constant c_tse_tx_fifo_depth : in  natural;
                                constant c_tse_rx_fifo_depth : in  natural;
                                constant c_tx_ready_latency  : in  natural;
                                constant src_mac             : in  std_logic_vector(c_network_eth_mac_slv'range);
                                signal   psc_access          : out std_logic;
                                signal   mm_clk              : in  std_logic;
                                signal   mm_miso             : in  t_mem_miso;
                                signal   mm_mosi             : out t_mem_mosi) is
    constant c_mac0       : integer := TO_SINT(hton(src_mac(47 downto 16), 4));
    constant c_mac1       : integer := TO_SINT(hton(src_mac(15 downto  0), 2));
  begin
    -- PSC control
    psc_access <= '1';
    proc_mem_mm_bus_rd(func_map_pcs_addr(16#22#),           mm_clk, mm_miso, mm_mosi);  -- REV --> 0x0901
    proc_mem_mm_bus_wr(func_map_pcs_addr(16#28#), 16#0008#, mm_clk, mm_miso, mm_mosi);  -- IF_MODE <-- Force 1GbE, no autonegatiation
    proc_mem_mm_bus_rd(func_map_pcs_addr(16#00#),           mm_clk, mm_miso, mm_mosi);  -- CONTROL --> 0x1140
    proc_mem_mm_bus_rd(func_map_pcs_addr(16#02#),           mm_clk, mm_miso, mm_mosi);  -- STATUS --> 0x000D
    proc_mem_mm_bus_wr(func_map_pcs_addr(16#00#), 16#0140#, mm_clk, mm_miso, mm_mosi);  -- CONTROL <-- Auto negotiate disable
    --proc_mem_mm_bus_wr(func_map_pcs_addr(16#00#), 16#1140#, mm_clk, mm_miso, mm_mosi);  -- CONTROL <-- Auto negotiate enable
    psc_access <= '0';

    -- MAC control
    proc_mem_mm_bus_rd(16#000#, mm_clk, mm_miso, mm_mosi);  -- REV --> CUST_VERSION & 0x0901
    if c_promis_en = false then
      proc_mem_mm_bus_wr(16#008#, 16#0100004B#, mm_clk, mm_miso, mm_mosi);
    else
      proc_mem_mm_bus_wr(16#008#, 16#0100005B#, mm_clk, mm_miso, mm_mosi);
    end if;
      -- COMMAND_CONFIG <--
      -- Only the bits relevant to UniBoard are explained here, others are 0
      -- [    0] = TX_ENA             = 1, enable tx datapath
      -- [    1] = RX_ENA             = 1, enable rx datapath
      -- [    2] = XON_GEN            = 0
      -- [    3] = ETH_SPEED          = 1, enable 1GbE operation
      -- [    4] = PROMIS_EN          = 0, when 1 then receive all frames
      -- [    5] = PAD_EN             = 0, when 1 enable receive padding removal (requires ethertype=payload length)
      -- [    6] = CRC_FWD            = 1, enable receive CRC forward
      -- [    7] = PAUSE_FWD          = 0
      -- [    8] = PAUSE_IGNORE       = 0
      -- [    9] = TX_ADDR_INS        = 0, when 1 then MAX overwrites tx SRC MAC with mac_0,1 or one of the supplemental mac
      -- [   10] = HD_ENA             = 0
      -- [   11] = EXCESS_COL         = 0
      -- [   12] = LATE_COL           = 0
      -- [   13] = SW_RESET           = 0, when 1 MAC disables tx and rx, clear statistics and flushes receive FIFO
      -- [   14] = MHAS_SEL           = 0, select multicast address resolutions hash-code mode
      -- [   15] = LOOP_ENA           = 0
      -- [18-16] = TX_ADDR_SEL[2:0]   = 000, TX_ADDR_INS insert mac_0,1 or one of the supplemental mac
      -- [   19] = MAGIC_EN           = 0
      -- [   20] = SLEEP              = 0
      -- [   21] = WAKEUP             = 0
      -- [   22] = XOFF_GEN           = 0
      -- [   23] = CNT_FRM_ENA        = 0
      -- [   24] = NO_LGTH_CHECK      = 1, when 0 then check payload length of received frames (requires ethertype=payload length)
      -- [   25] = ENA_10             = 0
      -- [   26] = RX_ERR_DISC        = 0, when 1 then discard erroneous frames (requires store and forward mode, so rx_section_full=0)
      --                                   when 0 then pass on with rx_err[0]=1
      -- [   27] = DISABLE_RD_TIMEOUT = 0
      -- [30-28] = RSVD               = 000
      -- [   31] = CNT_RESET          = 0, when 1 clear statistics
    proc_mem_mm_bus_wr(16#00C#,       c_mac0, mm_clk, mm_miso, mm_mosi);  -- MAC_0
    proc_mem_mm_bus_wr(16#010#,       c_mac1, mm_clk, mm_miso, mm_mosi);  -- MAC_1 <-- SRC_MAC = 12-34-56-78-9A-BC
    proc_mem_mm_bus_wr(16#05C#, 16#0000000C#, mm_clk, mm_miso, mm_mosi);  -- TX_IPG_LENGTH <-- interpacket gap = 12
    --proc_mem_mm_bus_wr(16#014#, 16#000005EE#, mm_clk, mm_miso, mm_mosi);  -- FRM_LENGTH <-- receive max frame length = 1518
    proc_mem_mm_bus_wr(16#014#, 16#0000233A#, mm_clk, mm_miso, mm_mosi);  -- FRM_LENGTH <-- receive max frame length = 9018

    -- FIFO legenda:
    -- . Tx section full  = There is enough data in the FIFO to start reading it, when 0 then store and forward.
    -- . Rx section full  = There is enough data in the FIFO to start reading it, when 0 then store and forward.
    -- . Tx section empty = There is not much empty space anymore in the FIFO, warn user via ff_tx_septy
    -- . Rx section empty = There is not much empty space anymore in the FIFO, inform remote device via XOFF flow control
    -- . Tx almost full   = Assert ff_tx_a_full and deassert ff_tx_rdy. Furthermore TX_ALMOST_FULL = c_tx_ready_latency+3,
    --                      so choose 3 for zero tx ready latency
    -- . Rx almost full   = Assert ff_rx_a_full and if the user is not ready ff_rx_rdy then:
    --                      --> break off the reception with an error to avoid FIFO overflow
    -- . Tx almost empty  = Assert ff_tx_a_empty and if the FIFO does not contain a eop yet then:
    --                      --> break off the transmission with an error to avoid FIFO underflow
    -- . Rx almost empty  = Assert ff_rx_a_empty
    -- Typical FIFO values:
    -- . TX_SECTION_FULL  = 16   > 8   = TX_ALMOST_EMPTY
    -- . RX_SECTION_FULL  = 16   > 8   = RX_ALMOST_EMPTY
    -- . TX_SECTION_EMPTY = D-16 < D-3 = Tx FIFO depth - TX_ALMOST_FULL
    -- . RX_SECTION_EMPTY = D-16 < D-8 = Rx FIFO depth - RX_ALMOST_FULL
    -- . c_tse_tx_fifo_depth = 1 M9K = 256*32b = 1k * 8b is sufficient when the Tx user respects ff_tx_rdy, to store a complete
    --                         ETH packet would require 1518 byte, so 2 M9K = 2k * 8b
    -- . c_tse_rx_fifo_depth = 1 M9K = 256*32b = 1k * 8b is sufficient when the Rx user ff_rx_rdy is sufficiently active
    proc_mem_mm_bus_wr(16#01C#, c_tse_rx_fifo_depth - 16, mm_clk, mm_miso, mm_mosi);  -- RX_SECTION_EMPTY <-- default FIFO depth - 16, >3
    proc_mem_mm_bus_wr(16#020#,                     16, mm_clk, mm_miso, mm_mosi);  -- RX_SECTION_FULL  <-- default 16
    proc_mem_mm_bus_wr(16#024#, c_tse_tx_fifo_depth - 16, mm_clk, mm_miso, mm_mosi);  -- TX_SECTION_EMPTY <-- default FIFO depth - 16, >3
    proc_mem_mm_bus_wr(16#028#,                     16, mm_clk, mm_miso, mm_mosi);  -- TX_SECTION_FULL  <-- default 16, >~ 8 otherwise no tx
    proc_mem_mm_bus_wr(16#02C#,                      8, mm_clk, mm_miso, mm_mosi);  -- RX_ALMOST_EMPTY  <-- default 8
    proc_mem_mm_bus_wr(16#030#,                      8, mm_clk, mm_miso, mm_mosi);  -- RX_ALMOST_FULL   <-- default 8
    proc_mem_mm_bus_wr(16#034#,                      8, mm_clk, mm_miso, mm_mosi);  -- TX_ALMOST_EMPTY  <-- default 8
    proc_mem_mm_bus_wr(16#038#,   c_tx_ready_latency + 3, mm_clk, mm_miso, mm_mosi);  -- TX_ALMOST_FULL   <-- default 3

    proc_mem_mm_bus_rd(16#0E8#, mm_clk, mm_miso, mm_mosi);  -- TX_CMD_STAT --> 0x00040000 : [18]=1 TX_SHIFT16, [17]=0 OMIT_CRC
    proc_mem_mm_bus_rd(16#0EC#, mm_clk, mm_miso, mm_mosi);  -- RX_CMD_STAT --> 0x02000000 : [25]=1 RX_SHIFT16

    wait until rising_edge(mm_clk);
  end proc_tech_tse_setup;
end eth1g_mem_pkg;
