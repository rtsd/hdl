-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify dp_sync_insert
-- Description:
-- Usage:
-- > as 8
-- > run -all
--
-- * The tb is self stopping because tb_end will stop the simulation by
--   stopping the clk and thus all toggling.
-- * The tb is self checking regarding the global and local BSN using
--   proc_dp_verify_bsn().

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_sync_insert is
  generic (
    g_block_size_input       : natural := 16;
    g_nof_blk_per_sync_input : natural := 32;
    g_gap_size_during_block  : natural := 0;
    g_gap_size_between_block : natural := 0;
    g_nof_data_per_block     : natural := 8;
    g_nof_blk_per_sync       : natural := 4;
    g_nof_repeat             : natural := 14
  );
end tb_dp_sync_insert;

architecture tb of tb_dp_sync_insert is
  constant c_dut_latency         : natural := 2;
  constant c_nof_replicated_sync : natural := g_block_size_input / g_nof_data_per_block * g_nof_blk_per_sync_input / g_nof_blk_per_sync;

  signal tb_end                    : std_logic := '0';
  signal clk                       : std_logic := '1';
  signal rst                       : std_logic := '1';

  -- DUT
  signal ref_sosi                  : t_dp_sosi := c_dp_sosi_rst;
  signal out_sosi                  : t_dp_sosi;

  -- Verification
  signal dly_valid_arr             : std_logic_vector(0 to c_dut_latency) := (others => '0');
  signal out_hold_sop              : std_logic := '0';
  signal exp_size                  : natural := g_nof_data_per_block;
  signal cnt_size                  : natural;
  signal verify_bsn_en             : std_logic := '0';
  signal cnt_replicated_global_bsn : natural;
  signal prev_out_sosi_global      : t_dp_sosi := c_dp_sosi_rst;
  signal prev_out_sosi_local       : t_dp_sosi := c_dp_sosi_rst;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- STIMULI
  ------------------------------------------------------------------------------

  p_stimuli : process
  begin
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    for I in 0 to g_nof_repeat - 1 loop
      -- Generate first block with sync
      ref_sosi.sync  <= '1';
      ref_sosi.sop   <= '1';
      ref_sosi.valid <= '1';
      ref_sosi.bsn   <= TO_DP_BSN(23 + I);
      proc_common_wait_some_cycles(clk, 1);
      ref_sosi.sync  <= '0';
      ref_sosi.sop   <= '0';

      -- Optionally apply valid='0' during block of data
      ref_sosi.valid <= '0';
      proc_common_wait_some_cycles(clk, g_gap_size_during_block);
      ref_sosi.valid <= '1';

      proc_common_wait_some_cycles(clk, g_block_size_input - 2);
      ref_sosi.eop   <= '1';
      proc_common_wait_some_cycles(clk, 1);
      ref_sosi.eop   <= '0';
      ref_sosi.valid <= '0';

      -- Optionally apply valid='0' between block of data
      proc_common_wait_some_cycles(clk, g_gap_size_between_block);

      -- Generate next blocks after sync
      for I in 0 to g_nof_blk_per_sync_input - 2 loop
        ref_sosi.sop   <= '1';
        ref_sosi.valid <= '1';
        proc_common_wait_some_cycles(clk, 1);
        ref_sosi.sync  <= '0';
        ref_sosi.sop   <= '0';

        -- Optionally apply valid='0' during block of data
        ref_sosi.valid <= '0';
        proc_common_wait_some_cycles(clk, g_gap_size_during_block);
        ref_sosi.valid <= '1';

        proc_common_wait_some_cycles(clk, g_block_size_input - 2);
        ref_sosi.eop   <= '1';
        proc_common_wait_some_cycles(clk, 1);
        ref_sosi.eop   <= '0';
        ref_sosi.valid <= '0';

        -- Optionally apply valid='0' between block of data
        proc_common_wait_some_cycles(clk, g_gap_size_between_block);
      end loop;
    end loop;

    -- End of stimuli
    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  ref_sosi.data  <= INCR_UVEC(ref_sosi.data, 1) when rising_edge(clk);
  ref_sosi.re    <= INCR_UVEC(ref_sosi.re,   2) when rising_edge(clk);
  ref_sosi.im    <= INCR_UVEC(ref_sosi.im,   3) when rising_edge(clk);

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dut: entity work.dp_sync_insert
  generic map (
    g_nof_data_per_blk   => g_nof_data_per_block,
    g_nof_blk_per_sync   => g_nof_blk_per_sync
  )
  port map (
    rst        => rst,
    clk        => clk,
    -- Streaming sink
    snk_in     => ref_sosi,
    -- Streaming source
    src_out    => out_sosi
  );

  ------------------------------------------------------------------------------
  -- Verification
  -- . use some DUT specific verification
  -- . use some general Verification means from tb_dp_pkg.vhd, dp_stream_verify.vhd
  ------------------------------------------------------------------------------
  dly_valid_arr(0)                  <= ref_sosi.valid;
  dly_valid_arr(1 to c_dut_latency) <= dly_valid_arr(0 to c_dut_latency - 1) when rising_edge(clk);

  p_verify_valid : process(clk)
  begin
    if rising_edge(clk) then
      assert out_sosi.valid = dly_valid_arr(c_dut_latency)
        report "Wrong out_sosi.valid"
        severity ERROR;
    end if;
  end process;

  p_verify_bsn_en : process
  begin
    wait for clk_period * 20;
    assert verify_bsn_en = '1'
      report "DP : BSN verification did not start in time."
      severity ERROR;
    wait;
  end process;

  -- Verify output global and local bsn
  proc_dp_verify_bsn(true, 1, c_nof_replicated_sync, g_nof_blk_per_sync,
                     clk, out_sosi.sync, out_sosi.sop, out_sosi.bsn,
                     verify_bsn_en, cnt_replicated_global_bsn, prev_out_sosi_global.bsn, prev_out_sosi_local.bsn);

  -- Verify output packet ctrl
  proc_dp_verify_sop_and_eop(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, out_hold_sop);

  -- Verify output packet block size
  proc_dp_verify_block_size(exp_size, clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, cnt_size);
end tb;
