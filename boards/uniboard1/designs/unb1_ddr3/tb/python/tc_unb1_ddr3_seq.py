#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for unb1_ddr3

Description:
  Tx seq --> DDR3 --> Rx seq
  
Usage:
  python tc_unb1_ddr3_seq.py --sim --unb 0 --fn 3 --rep 3 -n   0 -v 5
  python tc_unb1_ddr3_seq.py --sim --unb 0 --fn 3 --rep 3 -n 100 -v 3
  
"""

###############################################################################
# System imports
import test_case
import node_io
import pi_diag_tx_seq
import pi_diag_rx_seq
import pi_io_ddr

from tools import *
from common import *
from pi_common import *


##################################################################################################################
# Main
#
# Create a test case object
tc = test_case.Testcase('TB - ', '')
tc.set_result('PASSED')
tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test case for the unb1_ddr3 design on %s' % tc.unb_nodes_string())
tc.append_log(3, '>>>')
tc.append_log(3, '')
    
# Create access object for all nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)
    
# Create instances for the periperals
c_nof_streams = 1

tx_seq = pi_diag_tx_seq.PiDiagTxSeq(tc, io, nof_inst=c_nof_streams)
rx_seq = pi_diag_rx_seq.PiDiagRxSeq(tc, io, nof_inst=c_nof_streams)

# Create object for DDR register map
ddr = pi_io_ddr.PiIoDdr(tc, io, nof_inst = 1)

##################################################################################################################
# Test

# Wait for power up (reset release)
io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))

# Control defaults
nof_mon = 2
start_address = 0
ctlr_word_w = 256
st_dat_w = 64
if tc.number>0:
    nof_words = tc.number
else:
    nof_words = sel_a_b(tc.sim, 3001, 134217720)        # try e.g: 1 is smallest, 100, 3001 is a prime number, 134217728 = 4GByte / (256b/8b)

for rep in range(tc.repeat):
    tc.append_log(5, '')
    tc.append_log(3, '>>> Rep-%d' % rep)

    # Initialization
    tx_seq.write_disable()
    rx_seq.write_disable()
    
    # Wait for the DDR memory to become available    
    do_until_eq(ddr.read_init_done, ms_retry=3000, val=1, s_timeout=3600)        
    
    # Flush Tx FIFO
    ddr.write_flush_pulse()
    io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
    
    # Set DDR controller in write mode and start writing
    ddr.write_set_address(data=start_address)
    ddr.write_access_size(data=nof_words)
    ddr.write_mode_write()
    ddr.write_begin_access()
    
    # Tx sequence start
    tx_seq.write_enable_cntr()
    
    # Tx sequence monitor
    for mon in range(nof_mon):
        io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
        tx_seq.read_cnt()
    
    # Wait until controller write access is done
    do_until_eq(ddr.read_done, ms_retry=3000, val=1, s_timeout=3600)        
    tx_seq.read_cnt()
    
    # Rx sequence start
    rx_seq.write_enable_cntr()
    
    # Set DDR3 controller in read mode and start reading
    ddr.write_mode_read()
    ddr.write_begin_access()
    
    # Rx sequence monitor
    for mon in range(nof_mon):
        io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
        rx_seq.read_cnt()
    
    # Wait until controller read access is done
    do_until_eq(ddr.read_done, ms_retry=3000, val=1, s_timeout=3600)

    # Wait until the last word in the read access from DDR3 has been read on all nodes
    read_cnt = rx_seq.read_cnt(vLevel=7)
    do_until_eq(rx_seq.read_cnt, ms_retry=3000, val=read_cnt[0], s_timeout=3600)
    io.wait_for_time(hw_time=0.1, sim_time=(1, 'us'))
    
    # Read rx_cnt from all nodes
    read_cnt = rx_seq.read_cnt()
    
    # Verify expected rx_cnt
    for nodeNr, rx_cnt in zip(tc.nodeNrs, read_cnt):
        niStr = tc.to_node_string(nodeNr)
        if rx_cnt*st_dat_w==nof_words*ctlr_word_w:
            tc.append_log(3, niStr + 'Rx cnt OK')
        else:
            tc.append_log(2, niStr + 'Rx cnt error (expected %d != %d)' % (nof_words*ctlr_word_w, rx_cnt*st_dat_w))
            tc.set_result('FAILED')
      
    # Verify rx result (will tc.set_result('FAILED') if diag_rx_seq has detected a sequence error)
    rx_seq.read_result()
    

# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(0, '>>> Runtime=%f seconds (%f hours)' % (tc.get_run_time(),tc.get_run_time()/3600))
tc.append_log(3, '>>>')

