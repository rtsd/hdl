library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture rtl of pfs_fir_ctrl is
  -- The number of cycles that should be waited until the result that comes out
  -- of the MAC block is the valid result. The ctrl block will generate a valid
  -- pulse.
  constant c_mac_res_delay  : integer := 7;
  -- The number of cycles before the MAC block has to be reset before it can
  -- start with the next FIR calculation.
  constant c_mac_clr_delay  : integer := 4;

  type fir_state_enum is (
    idle,
    calc
  );

  signal fir_state              : fir_state_enum;
  signal nxt_fir_state          : fir_state_enum;
  signal taps_cnt               : std_logic_vector(g_nof_taps_w - 1 downto 0);
  signal nxt_taps_cnt           : std_logic_vector(taps_cnt'range);
  signal prefilter_cnt          : std_logic_vector(g_nof_prefilter_w - 1 downto 0);
  signal nxt_prefilter_cnt      : std_logic_vector(prefilter_cnt'range);
  signal taps_addr_base         : std_logic_vector(taps_cnt'range);
  signal nxt_taps_addr_base     : std_logic_vector(taps_addr_base'range);
  signal nxt_sample_wren        : std_logic;
  signal taps_addr_offset       : std_logic_vector(taps_cnt'range);
  signal i_taps_addr            : std_logic_vector(taps_addr'range);
  signal nxt_taps_addr          : std_logic_vector(taps_addr'range);
  signal nxt_taps_rden          : std_logic;
  signal nxt_coefs_addr         : std_logic_vector(coefs_addr'range);
  signal nxt_coefs_rden         : std_logic;
  signal i_sample_data_hor      : std_logic_vector(sample_data_hor'range);
  signal nxt_sample_data_hor    : std_logic_vector(sample_data_hor'range);
  signal i_sample_data_ver      : std_logic_vector(sample_data_ver'range);
  signal nxt_sample_data_ver    : std_logic_vector(sample_data_ver'range);
  signal i_sample_addr          : std_logic_vector(sample_addr'range);
  signal nxt_sample_addr        : std_logic_vector(sample_addr'range);
  signal mac_res_delay          : std_logic_vector(c_mac_res_delay - 1 downto 0);
  signal nxt_mac_res_delay      : std_logic_vector(mac_res_delay'range);
  signal sync_delay             : std_logic_vector(c_mac_res_delay + g_nof_taps * 2 - 1 downto 0);
  signal nxt_sync_delay         : std_logic_vector(sync_delay'range);
  signal last_tap               : std_logic;
begin
  -- Output signals.
  result_val       <= mac_res_delay(mac_res_delay'high);
  result_sync      <= sync_delay(sync_delay'high);
--  res_clr          <= mac_res_delay(c_mac_clr_delay - 1);
  taps_addr        <= i_taps_addr;
  sample_data_hor  <= i_sample_data_hor;
  sample_data_ver  <= i_sample_data_ver;
  sample_addr      <= i_sample_addr;

  registers_proc : process (clk, rst)
  begin
    if rst = '1' then
      -- Input registers.
      -- Output registers.
      sample_wren        <= '0';
      coefs_addr         <= (others => '0');
      coefs_rden         <= '0';
      i_taps_addr        <= (others => '0');
      taps_rden          <= '0';
      i_sample_data_hor  <= (others => '0');
      i_sample_data_ver  <= (others => '0');
      i_sample_addr      <= (others => '0');
      -- Internal registers.
      mac_res_delay      <= (others => '0');
      sync_delay         <= (others => '0');
      fir_state          <= idle;
      taps_cnt           <= (others => '0');
      prefilter_cnt      <= (others => '0');
      taps_addr_base     <= (others => '0');
    elsif rising_edge(clk) then
      -- Input registers.
      -- Output registers.
      sample_wren        <= nxt_sample_wren;
      coefs_addr         <= nxt_coefs_addr;
      coefs_rden         <= nxt_coefs_rden;
      i_taps_addr        <= nxt_taps_addr;
      taps_rden          <= nxt_taps_rden;
      i_sample_data_hor  <= nxt_sample_data_hor;
      i_sample_data_ver  <= nxt_sample_data_ver;
      i_sample_addr      <= nxt_sample_addr;
      -- Internal registers.
      mac_res_delay      <= nxt_mac_res_delay;
      sync_delay         <= nxt_sync_delay;
      fir_state          <= nxt_fir_state;
      taps_cnt           <= nxt_taps_cnt;
      prefilter_cnt      <= nxt_prefilter_cnt;
      taps_addr_base     <= nxt_taps_addr_base;
    end if;
  end process;

  -- The state machine will stay in idle until it receives an input sample. At
  -- that time it will start the FIR filter operation. It will return to idle
  -- unless another sample is received at the time the calculation is ready.
  state_machine_proc : process (fir_state, input_val, last_tap)
  begin
    nxt_fir_state <= fir_state;

    case fir_state is
      when idle =>
        if input_val = '1' then
          nxt_fir_state <= calc;
        end if;
      when calc =>
        if input_val = '0' and last_tap = '1' then
          nxt_fir_state <= idle;
        end if;
      when others =>
        nxt_fir_state <= idle;
    end case;
  end process;

  -- Pulse when the tap counter reaches it maximum.
  last_tap <= '1' when unsigned(taps_cnt) = (g_nof_taps - 1) else '0';

  res_clr <= '1'  when unsigned(taps_cnt) = c_mac_clr_delay - 1 else '0';

  -- Counter that will be used for addressing various RAMs, and to indicate
  -- to the state machine when a FIR operation is finished.
  tap_counter_proc : process (taps_cnt, input_val, fir_state, last_tap)
  begin
    nxt_taps_cnt <= taps_cnt;
    if input_val = '1' or last_tap = '1' then
      nxt_taps_cnt <= (others => '0');
    elsif fir_state = calc then
      nxt_taps_cnt <= std_logic_vector(unsigned(taps_cnt) + 1);
    end if;
  end process;

  -- Keep track of the current logical FIR operation. This counter will be used
  -- for addressing of the memories.
  prefilter_counter_proc : process (prefilter_cnt, last_tap)
  begin
    nxt_prefilter_cnt <= prefilter_cnt;
    if last_tap = '1' then
      if unsigned(prefilter_cnt) >= (g_nof_prefilter - 1) then
        nxt_prefilter_cnt <= (others => '0');
      else
        nxt_prefilter_cnt <= std_logic_vector(unsigned(prefilter_cnt) + 1);
      end if;
    end if;
  end process;

  -- Generate address and read enable for the coefficient memory.
  coefficient_control_proc : process (prefilter_cnt, taps_cnt, fir_state)
  begin
    nxt_coefs_addr <= (others => '0');
    nxt_coefs_rden <= '0';

    if fir_state = calc then
      nxt_coefs_rden <= '1';
      nxt_coefs_addr <= prefilter_cnt & taps_cnt;
    end if;
  end process;

  -- Generate write signals for the taps memories. The received samples will
  -- be written into this memory at the end of the FIR calculation cycle.
  sample_ram_control_proc : process (taps_cnt, input_val, i_sample_data_hor,
    input_hor, prefilter_cnt, taps_addr_base, i_sample_addr, i_sample_data_ver,
    input_ver)
  begin
    nxt_sample_wren      <= '0';
    nxt_sample_data_hor  <= i_sample_data_hor;
    nxt_sample_data_ver  <= i_sample_data_ver;
    nxt_sample_addr      <= i_sample_addr;

    if unsigned(taps_cnt) = (g_nof_taps - 2) then
      nxt_sample_wren <= '1';
      nxt_sample_addr <= prefilter_cnt & taps_addr_base;
    end if;

    if input_val = '1' then
      nxt_sample_data_hor <= std_logic_vector(to_signed(
          to_integer(signed(input_hor)), i_sample_data_hor'length));
      nxt_sample_data_ver <= std_logic_vector(to_signed(
          to_integer(signed(input_ver)), i_sample_data_ver'length));
    end if;
  end process;

  -- Generate read signals for the taps memories. Also, the base address of the
  -- current logical FIR operation will be generated here. The base address will
  -- change in time to emulate a shift register using memories.
  tap_ram_control_proc : process (taps_addr_base, fir_state, prefilter_cnt,
    taps_addr_offset, last_tap)
  begin
    nxt_taps_addr_base <= taps_addr_base;
    nxt_taps_rden <= '0';
    nxt_taps_addr <= (others => '0');

    if fir_state = calc then
      nxt_taps_rden <= '1';
      nxt_taps_addr <= prefilter_cnt & taps_addr_offset;
    end if;

    if unsigned(prefilter_cnt) = (g_nof_prefilter - 1) and last_tap = '1' then
      nxt_taps_addr_base <=
          std_logic_vector(unsigned(taps_addr_base) + 1);
    end if;
  end process;

  taps_addr_offset <= std_logic_vector(unsigned(taps_addr_base)
      + unsigned(taps_cnt));

  -- The MAC delay register is used to generate a valid pulse for the MAC output
  -- when the FIR calculation is done, and to generate a reset pulse to set the
  -- intermediate result of the MAC to 0.
  mac_controller_proc : process (taps_cnt, mac_res_delay, sync_delay, input_sync)
  begin
    nxt_mac_res_delay <= mac_res_delay(mac_res_delay'high - 1 downto 0) & '0';
    nxt_sync_delay <= sync_delay(sync_delay'high - 1 downto 0) & '0';

    if unsigned(taps_cnt) = g_nof_taps - 1 then
      nxt_mac_res_delay(0) <= '1';
      nxt_sync_delay(0) <= input_sync;
    end if;
  end process;
end rtl;
