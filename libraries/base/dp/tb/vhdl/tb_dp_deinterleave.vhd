-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use work.tb_dp_pkg.all;

-- Purpose: Test bench to check deinterleave function on DP level
-- Usage:
--   > as 6
--   > run -all
-- Remark:
--   This TB is only used to visually inspect the wave window to see
--   if all functions well on DP level. The actual deinterleaving itself
--   is verified more thoroughly in tb_common_reinterleave.

entity tb_dp_deinterleave is
  generic (
    g_dat_w       : natural := 16;
    g_nof_out     : natural := 2;
    g_block_size  : natural := 3;
    g_use_complex : boolean := true
 );
end;

architecture tb of tb_dp_deinterleave is
  type t_dsp_data_arr is array (integer range <>) of std_logic_vector(g_dat_w / 2 - 1 downto 0);
  type t_data_arr     is array (integer range <>) of std_logic_vector(g_dat_w - 1 downto 0);
  type t_val_arr      is array (integer range <>) of std_logic;

  -----------------------------------------------------------------------------
  -- Standard TB clocking, RST and control
  -----------------------------------------------------------------------------
  constant c_clk_period : time := 10 ns;

  signal clk            : std_logic := '1';
  signal rst            : std_logic;
  signal tb_end         : std_logic := '0';

  signal cnt_ena        : std_logic;

  -----------------------------------------------------------------------------
  --I/O streams
  -----------------------------------------------------------------------------
  constant c_input_val   : natural := 192;
  constant c_input_inval : natural := 64;

  signal snk_in  : t_dp_sosi;
  signal snk_out : t_dp_siso;

  signal src_out_arr : t_dp_sosi_arr(g_nof_out - 1 downto 0);

  -----------------------------------------------------------------------------
  --Monitor signals for wave window
  -----------------------------------------------------------------------------
  signal in_data      :  std_logic_vector(g_dat_w - 1 downto 0);
  signal in_re        :  std_logic_vector(g_dat_w / 2 - 1 downto 0);
  signal in_im        :  std_logic_vector(g_dat_w / 2 - 1 downto 0);
  signal in_val       :  std_logic;
  signal out_data_arr :  t_data_arr(g_nof_out - 1 downto 0);
  signal out_re_arr   :  t_dsp_data_arr(g_nof_out - 1 downto 0);
  signal out_im_arr   :  t_dsp_data_arr(g_nof_out - 1 downto 0);
  signal out_val_arr  :  t_val_arr(g_nof_out - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Standard TB clocking, RST and control
  -----------------------------------------------------------------------------
  clk    <= not clk or tb_end after c_clk_period / 2;
  rst    <= '1', '0' after 3 * c_clk_period;

  tb_end <= '0', '1' after 2 us;

  -----------------------------------------------------------------------------
  -- Generate test data stream with counter data
  -----------------------------------------------------------------------------
  cnt_ena <= '0', '1' after 20 * c_clk_period;

  snk_out.ready <= '1';
  snk_out.xon <= '1';

  p_stimuli : process
    variable v_bsn : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := (others => '0');
  begin
    while true loop
      wait until rising_edge(clk);
      proc_dp_gen_block_data(1, sel_a_b(g_use_complex, false, true), g_dat_w, g_dat_w, 0, 0, 0, c_input_val, 0, 0, '1', v_bsn, clk, cnt_ena, snk_out, snk_in);
      proc_common_wait_some_cycles(clk, c_input_inval);
      v_bsn := INCR_UVEC(v_bsn, 1);
    end loop;
   end process;

  -----------------------------------------------------------------------------
  -- DUT
  -----------------------------------------------------------------------------
  u_deinterleave : entity work.dp_deinterleave
  generic map (
    g_nof_out           => g_nof_out,
    g_block_size_int    => g_block_size,
    g_block_size_output => g_block_size,
    g_dat_w             => g_dat_w,
    g_use_complex       => g_use_complex
  )
  port map (
    rst        => rst,
    clk        => clk,

    snk_in      => snk_in,
    src_out_arr => src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Wave window monitor
  -----------------------------------------------------------------------------
  in_data <= snk_in.data(g_dat_w - 1 downto 0);
  in_re   <= snk_in.re(g_dat_w / 2 - 1 downto 0);
  in_im   <= snk_in.im(g_dat_w / 2 - 1 downto 0);
  in_val  <= snk_in.valid;

  gen_out : for I in g_nof_out - 1 downto 0 generate
    out_data_arr(I) <= src_out_arr(I).data(g_dat_w - 1 downto 0);
    out_re_arr(I)  <= src_out_arr(I).re(g_dat_w / 2 - 1 downto 0);
    out_im_arr(I)  <= src_out_arr(I).im(g_dat_w / 2 - 1 downto 0);
    out_val_arr(I) <= src_out_arr(I).valid;
  end generate;
end tb;
