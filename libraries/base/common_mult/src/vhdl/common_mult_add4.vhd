-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, technology_lib, tech_mult_lib, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Function: vector low part product + or - vector high part product
--   Call:
--     a = a(3) & a(2) & a(1) & a(0)
--     b = b(3) & b(2) & b(1) & b(0)
--   then:                                                                first sum   first sum   second sum
--                                                                        g_add_sub0  g_add_sub1  g_add_sub
--     result = (a(0)*b(0) + a(1)*b(1)) +  ((a(2)*b(2) + a(3)*b(3))) when   "ADD"       "ADD"       "ADD"
--     result = (a(0)*b(0) - a(1)*b(1)) +  ((a(2)*b(2) + a(3)*b(3))) when   "SUB"       "ADD"       "ADD"
--     result = (a(0)*b(0) + a(1)*b(1)) +  ((a(2)*b(2) - a(3)*b(3))) when   "ADD"       "SUB"       "ADD"
--     result = (a(0)*b(0) + a(1)*b(1)) -  ((a(2)*b(2) + a(3)*b(3))) when   "ADD"       "SUB"       "SUB"
--   and:
--     res = RESIZE(result)
--
-- Architectures:
-- . rtl      : uses RTL to have all registers in one clocked process
--

entity common_mult_add4 is
  generic (
    g_technology       : natural  := c_tech_select_default;
    g_variant          : string   := "RTL";
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_res_w            : positive;  -- g_in_a_w + g_in_b_w + log2(4)
    g_force_dsp        : boolean := true;  -- when TRUE resize input width to >= 18
    g_add_sub0         : string := "ADD";  -- or "SUB"
    g_add_sub1         : string := "ADD";  -- or "SUB"
    g_add_sub          : string := "ADD";  -- or "SUB" only available with rtl architecture
    g_nof_mult         : integer := 4;  -- fixed
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1, first sum
    g_pipeline_output  : natural := 1  -- >= 0,   second sum and optional rounding
  );
  port (
    rst        : in  std_logic := '0';
    clk        : in  std_logic;
    clken      : in  std_logic := '1';
    in_a       : in  std_logic_vector(g_nof_mult * g_in_a_w - 1 downto 0);
    in_b       : in  std_logic_vector(g_nof_mult * g_in_b_w - 1 downto 0);
    res        : out std_logic_vector(g_res_w - 1 downto 0)
  );
end common_mult_add4;

architecture str of common_mult_add4 is
  -- Extra output pipelining is only needed when g_pipeline_output > 1
  constant c_pipeline_output : natural := sel_a_b(g_pipeline_output > 0, g_pipeline_output - 1, 0);
  signal   result            : std_logic_vector(res'range);
begin
  u_mult_add4 : entity tech_mult_lib.tech_mult_add4
  generic map(
    g_technology       => g_technology,
    g_variant          => g_variant,
    g_in_a_w           => g_in_a_w,
    g_in_b_w           => g_in_b_w,
    g_res_w            => g_res_w,
    g_force_dsp        => g_force_dsp,
    g_add_sub0         => g_add_sub0,
    g_add_sub1         => g_add_sub1,
    g_add_sub          => g_add_sub,
    g_nof_mult         => g_nof_mult,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_adder   => g_pipeline_adder,
    g_pipeline_output  => g_pipeline_output
  )
  port map(
    rst   => rst,
    clk   => clk,
    clken => clken,
    in_a  => in_a,
    in_b  => in_b,
    res   => result
  );

  ------------------------------------------------------------------------------
  -- Extra output pipelining
  ------------------------------------------------------------------------------

  u_output_pipe : entity common_lib.common_pipeline  -- pipeline output
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline_output,
    g_in_dat_w       => res'LENGTH,
    g_out_dat_w      => res'length
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => result,
    out_dat => res
  );
end str;
