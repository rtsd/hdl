-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;

-- Derived from LOFAR cfg_single_reg

-- Purpose: Provide a MM interface to a register vector
--
-- Description:
--   The register has g_reg.nof_dat words and each word is g_reg.dat_w bits
--   wide. At the control side the register is accessed per word using the
--   address input wr_adr or rd_adr as index. At the data side the whole
--   register of g_reg.dat_w*g_reg.nof_dat bits is available at once. This is
--   the key difference with using a RAM.
--   E.g. for g_reg.nof_dat = 3 and g_reg.dat_w = 32 the addressing accesses
--   the register bits as follows:
--     wr_adr[1:0], rd_adr[1:0] = 0 --> reg[31:0]
--     wr_adr[1:0], rd_adr[1:0] = 1 --> reg[63:32]
--     wr_adr[1:0], rd_adr[1:0] = 2 --> reg[95:64]
--   E.g. for wr_adr = 0 and wr_en = '1': out_reg[31:0] = wr_dat[31:0]
--   E.g. for rd_adr = 0 and rd_en = '1':  rd_dat[31:0] = in_reg[31:0]
--
--   The word in the register that got accessed is reported via reg_wr_arr
--   or via reg_rd_arr depended on whether it was a write access or an read
--   access.
--
-- Usage:
-- 1) Connect out_reg to in_reg for write and readback register.
-- 2) Do not connect out_reg to in_reg for seperate write only register and
--    read only register at the same address.
-- 3) Leave out_reg OPEN for read only register.
-- 4) Connect wr_adr and rd_adr to have a shared address bus register.

entity common_reg_r_w is
  generic (
    g_reg       : t_c_mem := c_mem_reg;
    g_init_reg  : std_logic_vector(c_mem_reg_init_w - 1 downto 0) := (others => '0')
  );
  port (
    rst         : in  std_logic := '0';
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    -- control side
    wr_en       : in  std_logic;
    wr_adr      : in  std_logic_vector(g_reg.adr_w - 1 downto 0);
    wr_dat      : in  std_logic_vector(g_reg.dat_w - 1 downto 0);
    rd_en       : in  std_logic;
    rd_adr      : in  std_logic_vector(g_reg.adr_w - 1 downto 0);
    rd_dat      : out std_logic_vector(g_reg.dat_w - 1 downto 0);
    rd_val      : out std_logic;
    -- data side
    reg_wr_arr  : out std_logic_vector(            g_reg.nof_dat - 1 downto 0);
    reg_rd_arr  : out std_logic_vector(            g_reg.nof_dat - 1 downto 0);
    out_reg     : out std_logic_vector(g_reg.dat_w * g_reg.nof_dat - 1 downto 0);
    in_reg      : in  std_logic_vector(g_reg.dat_w * g_reg.nof_dat - 1 downto 0)
  );
end common_reg_r_w;

architecture rtl of common_reg_r_w is
  constant c_rd_latency  : natural := 1;
  constant c_pipeline    : natural := g_reg.latency - c_rd_latency;
  constant c_pipe_dat_w  : natural := 1 + g_reg.dat_w;  -- pipeline rd_val & rd_dat together

  type t_reg_arr is array (integer range <>) of std_logic_vector(g_reg.dat_w - 1 downto 0);

  signal in_reg_arr      : t_reg_arr(g_reg.nof_dat - 1 downto 0);
  signal out_reg_arr     : t_reg_arr(g_reg.nof_dat - 1 downto 0);

  signal pipe_dat_in     : std_logic_vector(c_pipe_dat_w - 1 downto 0);
  signal pipe_dat_out    : std_logic_vector(c_pipe_dat_w - 1 downto 0);

  signal nxt_reg_wr_arr  : std_logic_vector(reg_wr_arr'range);
  signal nxt_reg_rd_arr  : std_logic_vector(reg_rd_arr'range);

  signal i_out_reg       : std_logic_vector(out_reg'range);
  signal nxt_out_reg     : std_logic_vector(out_reg'range);

  signal int_rd_dat      : std_logic_vector(rd_dat'range);
  signal int_rd_val      : std_logic;
  signal nxt_rd_dat      : std_logic_vector(rd_dat'range);
  signal nxt_rd_val      : std_logic;
begin
  out_reg <= i_out_reg;

  -- View as reg_arr
  gen_reg_arr : for I in 0 to g_reg.nof_dat - 1 generate
    in_reg_arr(I) <= in_reg((i + 1) * g_reg.dat_w - 1 downto i * g_reg.dat_w);
    out_reg_arr(I) <= i_out_reg((i + 1) * g_reg.dat_w - 1 downto i * g_reg.dat_w);
  end generate;

  -- Pipeline to support read data latency > 1
  u_pipe_rd : entity work.common_pipeline
  generic map (
    g_pipeline   => c_pipeline,
    g_in_dat_w   => c_pipe_dat_w,
    g_out_dat_w  => c_pipe_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => pipe_dat_in,
    out_dat => pipe_dat_out
  );

  pipe_dat_in <= int_rd_val & int_rd_dat;

  rd_dat <= pipe_dat_out(pipe_dat_out'high - 1 downto 0);
  rd_val <= pipe_dat_out(pipe_dat_out'high);

  p_reg : process (rst, clk)
  begin
    if rst = '1' then
      -- Output signals.
      reg_wr_arr <= (others => '0');
      reg_rd_arr <= (others => '0');
      int_rd_val <= '0';
      int_rd_dat <= (others => '0');
      -- Internal signals.
      i_out_reg <= g_init_reg(out_reg'range);
    elsif rising_edge(clk) then
      -- Output signals.
      reg_wr_arr <= nxt_reg_wr_arr;
      reg_rd_arr <= nxt_reg_rd_arr;
      int_rd_val <= nxt_rd_val;
      int_rd_dat <= nxt_rd_dat;
      -- Internal signals.
      i_out_reg <= nxt_out_reg;
    end if;
  end process;

  p_control : process (rd_en, int_rd_dat, rd_adr, in_reg, i_out_reg, wr_adr, wr_en, wr_dat)
  begin
    nxt_rd_val <= rd_en;  -- rd_val depends only on rd_en, so for an out of range address the old rd_dat is output

    nxt_reg_rd_arr <= (others => '0');
    nxt_rd_dat <= int_rd_dat;
    if rd_en = '1' then
      for I in 0 to g_reg.nof_dat - 1 loop
        if unsigned(rd_adr) = I then
          nxt_reg_rd_arr(I) <= '1';
          nxt_rd_dat <= in_reg((I + 1) * g_reg.dat_w - 1 downto I * g_reg.dat_w);
        end if;
      end loop;
    end if;

    nxt_reg_wr_arr <= (others => '0');
    nxt_out_reg <= i_out_reg;
    if wr_en = '1' then
      for i in 0 to g_reg.nof_dat - 1 loop
        if unsigned(wr_adr) = I then
          nxt_reg_wr_arr(I) <= '1';
          nxt_out_reg((I + 1) * g_reg.dat_w - 1 downto I * g_reg.dat_w) <= wr_dat;
        end if;
      end loop;
    end if;
  end process;
end rtl;
