post_message "Running ta2_unb2b_ddr script"
set radiohdl_build $::env(HDL_BUILD_DIR)
#============================================================
# Files and basic settings
#============================================================

# Local HDL files
set_global_assignment -name VHDL_FILE ip/ta2_unb2b_ddr/ta2_unb2b_ddr.vhd

# IP files
set_global_assignment -name IP_FILE ip/ta2_unb2b_ddr/ta2_unb2b_ddr_pipe_stage.ip
set_global_assignment -name IP_FILE ip/ta2_unb2b_ddr/ta2_unb2b_ddr_clock_cross.ip

# All used HDL library *_lib.qip files in order, copied from ta2_unb2b_ddr.qsf in RadioHDL build directory. 
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/technology/technology_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_ram/ip_arria10_e1sg_ram_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_memory/tech_memory_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_fifo/ip_arria10_e1sg_fifo_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_fifo/tech_fifo_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_ddio/ip_arria10_e1sg_ddio_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_iobuf/tech_iobuf_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tst/tst_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/common/common_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ip_arria10_e1sg_ddr4_8g_1600/ip_arria10_e1sg_ddr4_8g_1600_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/tech_ddr/tech_ddr_lib.qip"
set_global_assignment -name QIP_FILE "$radiohdl_build/unb2b/quartus/ta2_unb2b_ddr/ta2_unb2b_ddr_lib.qip"



