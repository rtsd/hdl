#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Purpose: 
# . Test dp_offload_tx and dp_offload_rx, connected via 1GbE (loopback in sim)
# Description:
# . Usage: 
#   . Sim: Target only one node *after 2us*:
#     $ python tc_unb1_dp_offload.py --unb 0 --fn 0 --sim
#   . Synth: Target any TWO nodes:
#     $ python tc_unb1_dp_offload.py --unb 0 --fn 0,1

from common import *

import test_case
import node_io
import pi_common
import time
import sys
import os

import pi_system_info
import pi_diag_block_gen
import pi_dp_offload_tx
import pi_dp_offload_tx_hdr_dat_unb_dp_offload
import pi_dp_offload_tx_hdr_ovr_unb_dp_offload
import pi2_bsn_monitor
import pi_eth

###############################################################################
# Setup
###############################################################################
tc = test_case.Testcase('TB - ', '')
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

BASE_MAC = 0x2286080000
NOF_STREAMS = 1 

# Set parameters hardwired into unb1_dp_offload.vhd
if tc.sim==True:
    NOF_BLOCKS_PER_SYNC = 5 # Simulation: block gen outputs 10 blocks/sync. dp_offload_tx packs 2 blocks/packet so we receive 10/2 blocks/sync.
else:
    NOF_BLOCKS_PER_SYNC = 100000 # Hardware: block gen outputs 200000 blocks/sync. dp_offload_tx packs 2 blocks/packet so we receive 200000/2 blocks/sync.

NOF_WORDS_PER_BLOCK   = 11
NOF_BLOCKS_PER_PACKET = 2
BLOCK_LEN = NOF_WORDS_PER_BLOCK * NOF_BLOCKS_PER_PACKET

tc.set_result('PASSED')
tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test case for design unb_dp_offload. Targets: %s' % tc.unb_nodes_string())
tc.append_log(3, '>>>')
tc.append_log(3, '')

bg            = pi_diag_block_gen.PiDiagBlockGen(tc, io, nofChannels=NOF_STREAMS, ramSizePerChannel=1024)
dpotx_hdr_dat = pi_dp_offload_tx_hdr_dat_unb_dp_offload.PiDpOffloadTxHdrDatUnbDpOffload(tc, io, nof_inst=NOF_STREAMS)

info  = pi_system_info.PiSystemInfo(tc, io)
mon   = pi2_bsn_monitor.PiBsnMonitor(tc, io, nof_inst=NOF_STREAMS)
eth   = pi_eth.PiEth(tc, io) 

###############################################################################
# Task
###############################################################################

# Get design name to determine revision
name = info.read_design_name()

tc.append_log(1, '>>> reading REGMAPs')
info.make_register_info()
tc.append_log(1, '>>> reload NodeIO class')
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)


# for each stream, enable the corresponding UDP port on the 1GbE RX side
for stream in tc.gpNumbers:
    print 'UDP port enable', stream
    eth.write_udp_port_en(stream)

if tc.sim==False:
    # In sim the (one) node is looped back to itself (eth tx -> eth rx), but on the board there's a switch so we
    # need to send data from node[0] to node[1] and vice versa.
    
    # Set dst MAC of node[0] to MAC of node[1]
    eth_dst_mac = CommonBytes(BASE_MAC, 6)
    eth_dst_mac[1] = tc.unbNrs[0]
    eth_dst_mac[0] = tc.nodeNrs[1]%8
    dpotx_hdr_dat.write(node_nrs=tc.nodeNrs[0], inst_nrs=tc.gpNumbers, registers=[('eth_dst_mac', eth_dst_mac.data)], regmap=dpotx_hdr_dat.regmap)
    # Set dst MAC of node[1] to MAC of node[0]
    eth_dst_mac = CommonBytes(BASE_MAC, 6)
    eth_dst_mac[1] = tc.unbNrs[0]
    eth_dst_mac[0] = tc.nodeNrs[0]%8
    dpotx_hdr_dat.write(node_nrs=tc.nodeNrs[1], inst_nrs=tc.gpNumbers, registers=[('eth_dst_mac', eth_dst_mac.data)], regmap=dpotx_hdr_dat.regmap)

bg.write_enable()

# Declare our reference values
ref = { 'xon_stable'   : 1,
        'ready_stable' : 1,
        'sync_timeout' : 0,
#        'bsn_at_sync'  : 0 ,# increments every second on HW
        'nof_sop'      : NOF_BLOCKS_PER_SYNC, 
        'nof_valid'    : NOF_BLOCKS_PER_SYNC*BLOCK_LEN,
        'nof_err'      : 0,
        'block_len'    : BLOCK_LEN}
#        'nof_sync'     : 0 } # increments every second on HW

# Wiat for one sync interval so out BSN monitor values are valid
do_until_eq( method=mon.read, val=NOF_BLOCKS_PER_SYNC, ms_retry=1000, s_timeout=20, inst_nrs=tc.gpNumbers, registers=['nof_sop'], regmap=mon.regmap, flatten_result=True)

# Read BSN moitors and compare to reference
pi_common.ref_compare(tc, mon.read(inst_nrs=tc.gpNumbers, regmap=mon.regmap),  ref)

###############################################################################
# end
###############################################################################
tc.set_section_id('')
tc.append_log(0, '>>> Test Case result: %s' % tc.get_result())

sys.exit(tc.get_result())
