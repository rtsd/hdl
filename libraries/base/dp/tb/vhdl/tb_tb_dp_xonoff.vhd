-------------------------------------------------------------------------------
--
-- Copyright (C) 2019
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra, 24 Jan 2019
-- Purpose:
-- . Multi test bench
-- Description:
-- Usage:
-- > as 5
-- > run -all
--

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_xonoff is
end tb_tb_dp_xonoff;

architecture tb of tb_tb_dp_xonoff is
  constant c_rep : natural := 1000;

  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 3
  -- > run -all                 --> OK

-- -- general
-- g_flow_control_stimuli   : t_dp_flow_control_enum := e_random;   -- always active, random or pulse flow control
-- g_flow_control_verify    : t_dp_flow_control_enum := e_random;  -- always active, random or pulse flow control
-- -- specific
-- g_in_dat_w               : NATURAL := 32;
-- g_in_nof_words           : NATURAL := 1;
-- g_nof_repeat             : NATURAL := 1000;
-- g_nof_dut                : NATURAL := 10;
-- g_pkt_len                : NATURAL := 5;  -- must be a multiple of g_in_nof_words
-- g_pkt_gap                : NATURAL := 4

  u_act_act         : entity work.tb_dp_xonoff generic map (e_active, e_active, 32, 1, c_rep, 10, 5, 1);
  u_rnd_rnd_1       : entity work.tb_dp_xonoff generic map (e_random, e_random, 32, 1, c_rep,  1, 5, 3);
  u_rnd_rnd_10      : entity work.tb_dp_xonoff generic map (e_random, e_random, 32, 1, c_rep, 10, 5, 4);
  u_act_act_gap0    : entity work.tb_dp_xonoff generic map (e_active, e_active, 32, 1, c_rep, 10, 5, 0);
  u_rnd_rnd_gap0    : entity work.tb_dp_xonoff generic map (e_random, e_random, 32, 1, c_rep, 10, 5, 0);
  u_pls_pls         : entity work.tb_dp_xonoff generic map (e_pulse,  e_pulse,  32, 7, c_rep, 10, 5, 4);
end tb;
