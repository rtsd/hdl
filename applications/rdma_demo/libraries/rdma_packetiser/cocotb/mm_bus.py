# ##########################################################################
# Copyright 2024
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################
# Author:
# . Reinier vd Walle
#
# Purpose:
# . Provide abstraction layer for using MM bus (copi/cipo) in cocotb.
#
# Description:
# . This file contains classes to create an abstraction layer for 
#   interacting with MM copi/cipo signals in cocotb testbenches.
# . The classes are copied from AvalonMM in [1] and slightly adapted to 
#   use copi/cipo signals
#
# References:
# [1] - https://docs.cocotb.org/en/v1.5.1/_modules/cocotb_bus/drivers/avalon.html
# ##########################################################################
from cocotb.log import SimLog
from cocotb.triggers import RisingEdge, ReadOnly
from cocotb.binary import BinaryValue
from cocotb.result import TestError
from cocotb_bus.drivers import BusDriver
from cocotb.decorators import coroutine

class CopiDriver(BusDriver):
    _signals = []
    _optional_signals = ["rd", "wr", "wrdata", "address"]

    def __init__(self, entity, name, clock, **kwargs):
        BusDriver.__init__(self, entity, name, clock, **kwargs)
        self._can_read = False
        self._can_write = False

        # Drive some sensible defaults (setimmediatevalue to avoid x asserts)
        if hasattr(self.bus, "rd"):
            self.bus.rd.setimmediatevalue(0)
            self._can_read = True

        if hasattr(self.bus, "wr"):
            self.bus.wr.setimmediatevalue(0)
            v = self.bus.wrdata.value
            v.binstr = "x" * len(self.bus.wrdata)
            self.bus.wrdata.value = v
            self._can_write = True

        if hasattr(self.bus, "address"):
            v = self.bus.address.value
            v.binstr = "x" * len(self.bus.address)
            self.bus.address.setimmediatevalue(v)

    def read(self, address):
        pass

    def write(self, address, value):
        pass
        

class CipoDriver(BusDriver):
    _signals = []
    _optional_signals = ["rddata", "waitrequest","rdval"]

    def __init__(self, entity, name, clock, **kwargs):
        BusDriver.__init__(self, entity, name, clock, **kwargs)
        self._can_read = True
        self._can_write = False

        # Drive some sensible defaults (setimmediatevalue to avoid x asserts)
        if hasattr(self.bus, "rdval"):
            self.bus.rdval.setimmediatevalue(0)            

        if hasattr(self.bus, "rddata"):            
            v = self.bus.rddata.value
            v.binstr = "x" * len(self.bus.rddata)
            self.bus.rddata.value = v            

        if hasattr(self.bus, "waitrequest"):
            self.bus.waitrequest.setimmediatevalue(0)

    def read(self, address):
        pass

    def write(self, address, value):
        pass

class MMController():
    """Memory Mapped Interface (MM) Controller."""

    def __init__(self, entity, copi_name, cipo_name, clock):        
        self._log = SimLog("cocotb.%s.%s" % (entity._name, copi_name))
        self._log.debug("Memory Mapped Controller created")
        self.copi = CopiDriver(entity, copi_name, clock, bus_separator='.')
        self.cipo = CipoDriver(entity, cipo_name, clock, bus_separator='.')
        self.clock = clock

    def __len__(self):
        return 2**len(self.copi.bus.address)

    @coroutine
    async def read(self, address: int, sync: bool = True) -> BinaryValue:
        """Issue a request to the bus and block until this comes back.

        Simulation time still progresses
        but syntactically it blocks.

        Args:
            address: The address to read from.
            sync: Wait for rising edge on clock initially.
                Defaults to True.

        Returns:
            The read data value.

        Raises:
            :any:`TestError`: If controller is write-only.
        """
        if not self.copi._can_read:
            self._log.error("Cannot read - have no read signal")
            raise TestError("Attempt to read on a write-only MM Controller")

        await self.copi._acquire_lock()

        # Apply values for next clock edge
        if sync:
            await RisingEdge(self.clock)
        self.copi.bus.address.value = address
        self.copi.bus.rd.value = 1


        # Wait for waitrequest to be low
        if hasattr(self.cipo.bus, "waitrequest"):
            await self.cipo._wait_for_nsignal(self.cipo.bus.waitrequest)
        await RisingEdge(self.clock)

        # Deassert read
        self.copi.bus.rd.value = 0                
        #v = self.copi.bus.address.value
        #v.binstr = "x" * len(self.copi.bus.address)
        #self.copi.bus.address.value = v

        if hasattr(self.cipo.bus, "rdval"):
            while True:
                await ReadOnly()
                if int(self.cipo.bus.rdval):
                    break
                await RisingEdge(self.clock)
        else:
            # Assume readLatency = 1 if no readdatavalid
            # FIXME need to configure this,
            # should take a dictionary of Avalon properties.
            await ReadOnly()

        # Get the data
        data = self.cipo.bus.rddata.value
        
        self.copi._release_lock()
        return data

    @coroutine
    async def write(self, address: int, value: int) -> None:
        """Issue a write to the given address with the specified
        value.

        Args:
            address: The address to write to.
            value: The data value to write.

        Raises:
            :any:`TestError`: If controller is read-only.
        """
        if not self.copi._can_write:
            self._log.error("Cannot write - have no write signal")
            raise TestError("Attempt to write on a read-only MM Controller")

        await self.copi._acquire_lock()

        # Apply values to bus
        await RisingEdge(self.clock)
        self.copi.bus.address.value = address
        self.copi.bus.wrdata.value = value
        self.copi.bus.wr.value = 1
        
        # Wait for waitrequest to be low
        if hasattr(self.cipo.bus, "waitrequest"):
            await self.cipo._wait_for_nsignal(self.cipo.bus.waitrequest)

        # Deassert write
        await RisingEdge(self.clock)
        self.copi.bus.wr.value = 0        
        #v = self.copi.bus.address.value
        #v.binstr = "x" * len(self.copi.bus.address)
        #self.copi.bus.address.value = v

        #v = self.copi.bus.wrdata.value
        #v.binstr = "x" * len(self.copi.bus.wrdata)
        #self.copi.bus.wrdata.value = v
        self.copi._release_lock()
