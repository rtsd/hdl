-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use diag_lib.diag_pkg.all;

entity tr_nonbonded_reg is
  generic (
     g_nof_gx             : natural
 );
  port (
    -- Clocks and reset
    mm_rst               : in  std_logic;
    mm_clk               : in  std_logic;

    tx_rst               : in  std_logic_vector(g_nof_gx - 1 downto 0);
    tx_clk               : in  std_logic_vector(g_nof_gx - 1 downto 0);

    rx_rst               : in  std_logic_vector(g_nof_gx - 1 downto 0);
    rx_clk               : in  std_logic_vector(g_nof_gx - 1 downto 0);

    -- Memory Mapped Slave in mm_clk domain
    sla_in               : in  t_mem_mosi;
    sla_out              : out t_mem_miso;

    tx_state             : in  std_logic_vector(2 * g_nof_gx - 1 downto 0);
    tx_align_en          : out std_logic_vector(g_nof_gx - 1 downto 0);

    rx_state             : in  std_logic_vector(2 * g_nof_gx - 1 downto 0);
    rx_align_en          : out std_logic_vector(g_nof_gx - 1 downto 0);

    rx_dataout           : in t_slv_32_arr(g_nof_gx - 1 downto 0)
   );
end tr_nonbonded_reg;

architecture rtl of tr_nonbonded_reg is
  constant c_nof_addr         : natural := 16;
  constant c_max_nof_gx       : natural := 12;

  constant c_mm_reg           : t_c_mem := (latency  => 1,
                                            adr_w    => ceil_log2(c_nof_addr),
                                            dat_w    => c_word_w,
                                            nof_dat  => c_nof_addr,
                                            init_sl  => '0');

  signal mm_tx_state              : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_tx_align_en           : std_logic_vector(c_word_w - 1 downto 0);

  signal mm_rx_state              : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_rx_align_en           : std_logic_vector(c_word_w - 1 downto 0);

  signal mm_rx_dataout             : t_slv_32_arr(c_max_nof_gx - 1 downto 0);
begin
  gen_wires : for i in 0 to g_nof_gx - 1 generate
    mm_rx_dataout(i) <= rx_dataout(i);
  end generate;

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out   <= c_mem_miso_rst;
      -- Write access, register values
      mm_tx_align_en(g_nof_gx - 1 downto 0) <= (others => '0');
      mm_rx_align_en(g_nof_gx - 1 downto 0) <= (others => '0');

    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write event defaults

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 0 =>
            mm_tx_align_en(g_nof_gx - 1 downto 0) <= sla_in.wrdata(g_nof_gx - 1 downto 0);
          when 2 =>
            mm_rx_align_en(g_nof_gx - 1 downto 0) <= sla_in.wrdata(g_nof_gx - 1 downto 0);

          when others => null;  -- not used MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Read Block Sync
          when 1 =>
            sla_out.rddata(2 * g_nof_gx - 1 downto 0) <= mm_tx_state(2 * g_nof_gx - 1 downto 0);
          when 3 =>
            sla_out.rddata(2 * g_nof_gx - 1 downto 0) <= mm_rx_state(2 * g_nof_gx - 1 downto 0);

          when 4 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(0);
          when 5 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(1);
          when 6 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(2);
          when 7 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(3);
          when 8 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(4);
          when 9 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(5);
          when 10 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(6);
          when 11 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(7);
          when 12 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(8);
          when 13 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(9);
          when 14 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(10);
          when 15 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_rx_dataout(11);

          when others => null;
        end case;
      end if;
    end if;
  end process;

  mm_tx_state(2 * g_nof_gx - 1 downto 0) <= tx_state;
  mm_rx_state(2 * g_nof_gx - 1 downto 0) <= rx_state;

  gen_asyncs: for i in 0 to g_nof_gx - 1 generate
    u_async_tx_align_en: entity common_lib.common_async
    generic map(
      g_rst_level => '0'
    )
    port map(
      rst  => tx_rst(i),
      clk  => tx_clk(i),
      din  => mm_tx_align_en(i),
      dout => tx_align_en(i)
    );

    u_async_rx_align_en: entity common_lib.common_async
    generic map(
      g_rst_level => '0'
    )
    port map(
      rst  => rx_rst(i),
      clk  => rx_clk(i),
      din  => mm_rx_align_en(i),
      dout => rx_align_en(i)
    );
  end generate;
end rtl;
