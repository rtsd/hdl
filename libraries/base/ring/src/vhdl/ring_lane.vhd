-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
--
-- Purpose: Implement the function of a complete ring lane by combining ring_lane/tx.
-- Description:
-- . See "L5 SDPFW Design Document: Ring" at https://support.astron.nl/confluence/x/jyu7Ag
-- Remark:

library IEEE, common_lib, mm_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.ring_pkg.all;

entity ring_lane is
  generic (
    g_lane_direction            : natural := 1;
    g_lane_data_w               : natural := 64;
    g_lane_packet_length        : natural := 1024;
    g_lane_total_nof_packets_w  : natural := c_longword_w;  -- <= c_longword_w = 64
    g_use_dp_layer              : boolean := true;
    g_nof_rx_monitors           : natural := 0;
    g_nof_tx_monitors           : natural := 1;
    g_err_bi                    : natural := 0;  -- ring_rx bit index in sosi.err field to set for wrongly sized packets
    g_nof_err_counts            : natural := 1;  -- nof counters to count the set err bits in range sosi.err(g_nof_err_counts-1 DOWNTO 0)
    g_bsn_at_sync_check_channel : natural := 1;  -- on which channel should the bsn be checked
    g_validate_channel          : boolean := true;
    g_validate_channel_mode     : string := ">";
    g_sync_timeout              : natural := 220 * 10**6  -- 10% margin
  );
  port (
    -- Clocks and reset
    mm_rst : in  std_logic;
    mm_clk : in  std_logic;

    dp_clk : in  std_logic;
    dp_rst : in  std_logic;

    from_lane_sosi     : out t_dp_sosi;
    to_lane_sosi       : in  t_dp_sosi;
    lane_rx_cable_sosi : in  t_dp_sosi;
    lane_rx_board_sosi : in  t_dp_sosi;
    lane_tx_cable_sosi : out t_dp_sosi;
    lane_tx_board_sosi : out t_dp_sosi;
    bs_sosi            : in  t_dp_sosi;

    reg_ring_lane_info_copi                : in  t_mem_copi;
    reg_ring_lane_info_cipo                : out t_mem_cipo;
    reg_bsn_monitor_v2_ring_rx_copi        : in  t_mem_copi;
    reg_bsn_monitor_v2_ring_rx_cipo        : out t_mem_cipo;
    reg_bsn_monitor_v2_ring_tx_copi        : in  t_mem_copi;
    reg_bsn_monitor_v2_ring_tx_cipo        : out t_mem_cipo;
    reg_dp_block_validate_err_copi         : in  t_mem_copi;
    reg_dp_block_validate_err_cipo         : out t_mem_cipo;
    reg_dp_block_validate_bsn_at_sync_copi : in  t_mem_copi;
    reg_dp_block_validate_bsn_at_sync_cipo : out t_mem_cipo;

    this_rn   : in std_logic_vector(c_byte_w - 1 downto 0);
    N_rn      : in std_logic_vector(c_byte_w - 1 downto 0);
    rx_select : in std_logic := '0';
    tx_select : in std_logic := '0'
  );
end ring_lane;

architecture str of ring_lane is
  constant c_lane_direction : std_logic := sel_a_b(g_lane_direction, '1', '0');
  signal lane_info : t_ring_lane_info;
begin
  u_ring_lane_info : entity work.ring_lane_info
  port map (
    dp_rst => dp_rst,
    dp_clk => dp_clk,

    mm_rst => mm_rst,
    mm_clk => mm_clk,

    reg_mosi => reg_ring_lane_info_copi,
    reg_miso => reg_ring_lane_info_cipo,

    lane_direction => c_lane_direction,
    lane_info => lane_info
  );

  u_ring_rx : entity work.ring_rx
  generic map (
    g_use_dp_layer        => g_use_dp_layer,
    g_lane_direction      => g_lane_direction,
    g_total_nof_packets_w => g_lane_total_nof_packets_w,
    g_data_w              => g_lane_data_w,
    g_nof_rx_monitors     => g_nof_rx_monitors,
    g_err_bi              => g_err_bi,
    g_block_size          => g_lane_packet_length,
    g_nof_err_counts      => g_nof_err_counts,
    g_check_channel       => g_bsn_at_sync_check_channel,
    g_sync_timeout        => g_sync_timeout
  )
  port map (
    mm_rst => mm_rst,
    mm_clk => mm_clk,
    dp_clk => dp_clk,
    dp_rst => dp_rst,

    from_lane_sosi     => from_lane_sosi,
    lane_rx_cable_sosi => lane_rx_cable_sosi,
    lane_rx_board_sosi => lane_rx_board_sosi,
    bs_sosi            => bs_sosi,

    reg_bsn_monitor_v2_copi                => reg_bsn_monitor_v2_ring_rx_copi,
    reg_bsn_monitor_v2_cipo                => reg_bsn_monitor_v2_ring_rx_cipo,
    reg_dp_block_validate_err_copi         => reg_dp_block_validate_err_copi,
    reg_dp_block_validate_err_cipo         => reg_dp_block_validate_err_cipo,
    reg_dp_block_validate_bsn_at_sync_copi => reg_dp_block_validate_bsn_at_sync_copi,
    reg_dp_block_validate_bsn_at_sync_cipo => reg_dp_block_validate_bsn_at_sync_cipo,

    ref_sync  => bs_sosi.sync,
    rx_select => rx_select,
    this_rn   => this_rn,
    N_rn      => N_rn
  );

  u_ring_tx : entity work.ring_tx
  generic map (
    g_use_dp_layer    => g_use_dp_layer,
    g_lane_direction  => g_lane_direction,
    g_data_w          => g_lane_data_w,
    g_nof_tx_monitors => g_nof_tx_monitors,
    g_validate_channel => g_validate_channel,
    g_mode            => g_validate_channel_mode,
    g_sync_timeout    => g_sync_timeout
  )
  port map (
    mm_rst =>  mm_rst,
    mm_clk =>  mm_clk,
    dp_clk =>  dp_clk,
    dp_rst =>  dp_rst,

    to_lane_sosi       => to_lane_sosi,
    lane_tx_cable_sosi => lane_tx_cable_sosi,
    lane_tx_board_sosi => lane_tx_board_sosi,

    reg_bsn_monitor_v2_copi => reg_bsn_monitor_v2_ring_tx_copi,
    reg_bsn_monitor_v2_cipo => reg_bsn_monitor_v2_ring_tx_cipo,

    ref_sync       => bs_sosi.sync,
    tx_select      => tx_select,
    remove_channel => lane_info.transport_nof_hops,
    this_rn        => this_rn,
    N_rn           => N_rn
  );
end str;
