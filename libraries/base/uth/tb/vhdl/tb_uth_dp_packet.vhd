-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify DP packets as payload in UTH frames over link
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-- . The verify procedures check the correct output
-- . Observe out_* signals in Wave Window

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_packet_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_uth_dp_packet is
  generic (
    g_phy_link_valid_support : boolean := true;  -- when TRUE then the PHY link can pass on valid control, else the valid not transported and implicitely always active
    g_data_w                 : natural := 16;
    g_in_en                  : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_out_ready              : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_nof_repeat             : natural := 10  -- for constant active stream control using 1 is sufficient, use > 1 to verify longer with random stimuli
  );
end tb_uth_dp_packet;

architecture tb of tb_uth_dp_packet is
  constant c_rl                       : natural := 1;
  constant c_typ_ofs                  : natural := sel_a_b(g_data_w < 16, 2**7, 2**15);  -- avoid out of 32 bit INTEGER range for g_data_w > 30
  constant c_max_len                  : natural := c_typ_ofs - 1;
  constant c_data_init                : integer := 0;
  constant c_data_w                   : integer := sel_a_b(g_data_w < 30, g_data_w, 30);  -- used to avoid INTEGER range error for 2**31 and 2**32
  constant c_data_mod                 : integer := 2**c_data_w;  -- used to avoid TO_UVEC warning for smaller g_data_w : "NUMERIC_STD.TO_UNSIGNED: vector truncated"
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0877665544332211";
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  constant c_pkt_overhead_len         : natural := func_dp_packet_overhead_len(g_data_w);

  constant c_nof_tlen_max             : natural := 5;
  constant c_nof_tlen                 : natural := 5;
  constant c_nof_tlen_w               : natural := ceil_log2(c_nof_tlen);
  constant c_nof_input                : natural := 3;
  constant c_nof_streams              : natural := c_nof_tlen * c_nof_input;
  constant c_typ_arr                  : t_natural_arr(0 to c_nof_tlen_max - 1) := (c_typ_ofs,
                                                                                 c_typ_ofs + 1,
                                                                                 c_typ_ofs + 2,
                                                                                 c_typ_ofs + 3,
                                                                                 c_typ_ofs + 4);
  constant c_len_arr                  : t_natural_arr(0 to c_nof_tlen_max - 1) := (c_pkt_overhead_len + 3,
                                                                                 c_pkt_overhead_len + 1,
                                                                                 c_pkt_overhead_len + 1,
                                                                                 c_pkt_overhead_len + 70,
                                                                                 c_pkt_overhead_len + 17);
  --CONSTANT c_repeat_arr               : t_natural_arr(0 TO c_nof_streams-1) := array_init(g_nof_repeat, c_nof_streams, 1);  -- use g_nof_repeat+cK to have different expected_out_data(I,J) per input, to verify that the output demultiplexing works correct
  constant c_repeat_arr               : t_natural_arr(0 to c_nof_streams - 1) := array_init(g_nof_repeat, c_nof_streams);  -- use g_nof_repeat for all inputs

  constant c_bsn_w                    : natural := 48;
  constant c_channel_w                : natural := ceil_log2(c_nof_tlen) + ceil_log2(c_nof_input);
  constant c_err_w                    : natural := 1;

  --- Default connect enc-uth_tx to uth_rx-dec via UTH over PHY link
  constant c_loopback_packet          : boolean := false;  -- when TRUE loop back enc-dec, to verify throughput without UTH
  constant c_loopback_mux             : boolean := false;  -- when TRUE loop back mux-demux, to verify throughput at SISO/SOSI single multiplexed stream level, without DP PACKET and without UTH

  type t_siso_2arr is array (integer range <>) of t_dp_siso_arr(0 to c_nof_input - 1);
  type t_sosi_2arr is array (integer range <>) of t_dp_sosi_arr(0 to c_nof_input - 1);

  type t_data_2arr   is array (0 to c_nof_tlen - 1, 0 to c_nof_input - 1) of std_logic_vector(g_data_w - 1 downto 0);
  type t_rl_vec_2arr is array (0 to c_nof_tlen - 1, 0 to c_nof_input - 1) of std_logic_vector(0 to c_rl);

  signal tb_end_vec        : std_logic_vector(c_nof_streams - 1 downto 0) := (others => '0');
  signal tb_end            : std_logic;
  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';

  signal random_0          : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1          : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0           : std_logic;
  signal pulse_1           : std_logic;
  signal pulse_en          : std_logic := '1';

  signal in_en             : std_logic := '1';
  signal in_siso_2arr      : t_siso_2arr(0 to c_nof_tlen - 1);
  signal in_sosi_2arr      : t_sosi_2arr(0 to c_nof_tlen - 1);
  signal in_ready          : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1);
  signal in_data           : t_data_2arr;
  signal in_val            : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1);
  signal in_sop            : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1);
  signal in_eop            : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1);

  signal mux_siso_arr      : t_dp_siso_arr(0 to c_nof_tlen - 1);
  signal mux_sosi_arr      : t_dp_sosi_arr(0 to c_nof_tlen - 1);
  signal mux_siso          : t_dp_siso;
  signal mux_sosi          : t_dp_sosi;

  signal tx_siso           : t_dp_siso;
  signal tx_sosi           : t_dp_sosi;
  signal tx_data           : std_logic_vector(g_data_w - 1 downto 0);
  signal tx_val            : std_logic;
  signal tx_sop            : std_logic;
  signal tx_eop            : std_logic;
  signal tx_channel        : std_logic_vector(c_channel_w - 1 downto 0);
  signal tx_err            : std_logic_vector(c_err_w - 1 downto 0);

  signal pkt_enc_siso      : t_dp_siso;
  signal pkt_enc_sosi      : t_dp_sosi;
  signal pkt_tx_siso       : t_dp_siso := c_dp_siso_rdy;
  signal pkt_tx_sosi       : t_dp_sosi;
  signal pkt_tx_data       : std_logic_vector(g_data_w - 1 downto 0);
  signal pkt_tx_val        : std_logic;
  signal pkt_tx_sop        : std_logic;
  signal pkt_tx_eop        : std_logic;
  signal pkt_tx_channel    : std_logic_vector(c_channel_w - 1 downto 0);
  signal pkt_tx_err        : std_logic_vector(c_err_w - 1 downto 0);

  signal uth_tx_siso       : t_dp_siso := c_dp_siso_rdy;
  signal uth_tx_sosi       : t_dp_sosi;
  signal uth_tx_data       : std_logic_vector(g_data_w - 1 downto 0);
  signal uth_tx_val        : std_logic;
  signal uth_tx_sop        : std_logic;
  signal uth_tx_eop        : std_logic;

  signal phy_link_err      : std_logic;

  signal uth_rx_siso       : t_dp_siso := c_dp_siso_rdy;
  signal uth_rx_sosi       : t_dp_sosi;
  signal uth_rx_data       : std_logic_vector(g_data_w - 1 downto 0);
  signal uth_rx_val        : std_logic;
  signal uth_rx_sop        : std_logic;
  signal uth_rx_eop        : std_logic;

  signal pkt_rx_siso       : t_dp_siso := c_dp_siso_rdy;
  signal pkt_rx_sosi       : t_dp_sosi;
  signal pkt_rx_data       : std_logic_vector(g_data_w - 1 downto 0);
  signal pkt_rx_val        : std_logic;
  signal pkt_rx_sop        : std_logic;
  signal pkt_rx_eop        : std_logic;
  signal pkt_rx_channel    : std_logic_vector(c_channel_w - 1 downto 0);
  signal pkt_rx_err        : std_logic_vector(c_err_w - 1 downto 0);

  signal pkt_dec_siso      : t_dp_siso;
  signal pkt_dec_sosi      : t_dp_sosi;
  signal rx_siso           : t_dp_siso := c_dp_siso_rdy;
  signal rx_sosi           : t_dp_sosi;
  signal rx_data           : std_logic_vector(g_data_w - 1 downto 0);
  signal rx_val            : std_logic;
  signal rx_sop            : std_logic;
  signal rx_eop            : std_logic;
  signal rx_channel        : std_logic_vector(c_channel_w - 1 downto 0);
  signal rx_err            : std_logic_vector(c_err_w - 1 downto 0);
  signal rx_sync           : std_logic;
  signal rx_bsn            : std_logic_vector(c_bsn_w - 1 downto 0);

  signal demux_siso_arr    : t_dp_siso_arr(0 to c_nof_tlen - 1);
  signal demux_sosi_arr    : t_dp_sosi_arr(0 to c_nof_tlen - 1);
  signal demux_siso        : t_dp_siso;
  signal demux_sosi        : t_dp_sosi;

  signal out_siso_2arr     : t_siso_2arr(0 to c_nof_tlen - 1);
  signal out_sosi_2arr     : t_sosi_2arr(0 to c_nof_tlen - 1);

  signal out_siso          : t_dp_siso := c_dp_siso_rdy;

  signal verify_en         : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1) := (others => (others => '0'));
  signal verify_done       : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1) := (others => (others => '0'));
  signal count_eop         : t_integer_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1) := (others => (others => 0));
  signal prev_count_eop    : t_integer_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1) := (others => (others => 0));

  signal prev_out_ready    : t_rl_vec_2arr;
  signal prev_out_data     : t_data_2arr := (others => (others => TO_SVEC(c_data_init - 1, g_data_w)));
  signal out_data          : t_data_2arr;
  signal out_val           : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1);
  signal out_sop           : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1);
  signal out_eop           : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1);
  signal hold_out_sop      : t_sl_matrix(0 to c_nof_tlen - 1, 0 to c_nof_input - 1);
  signal expected_out_data : t_data_2arr;

  -- Auxiliary
  signal in_ready_or       : std_logic;
  signal in_val_or         : std_logic;
  signal out_val_or        : std_logic;
begin
  tb_end <= '0', vector_and(tb_end_vec) after clk_period * g_nof_repeat * 100;

  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- LEVEL 1: STREAM CONTROL
  ------------------------------------------------------------------------------

  assert g_phy_link_valid_support = true
    report "TB_UTH_DP_PACKET : No valid support on the link, this requires using always active streaming control"
    severity NOTE;

  in_en         <= '1'                     when g_phy_link_valid_support = false else
                   '1'                     when g_in_en = e_active               else
                   random_0(random_0'high) when g_in_en = e_random               else
                   pulse_0                 when g_in_en = e_pulse;

  out_siso.ready <= '1'                     when g_phy_link_valid_support = false else
                    '1'                     when g_out_ready = e_active           else
                    random_1(random_1'high) when g_out_ready = e_random           else
                    pulse_1                 when g_out_ready = e_pulse;

  out_siso_2arr <= (others => (others => out_siso));

  ------------------------------------------------------------------------------
  -- LEVEL 1: USER DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  gen_tlen : for I in 0 to c_nof_tlen - 1 generate
    gen_input : for J in 0 to c_nof_input - 1 generate
      p_stimuli : process
        constant c_pkt_data_len : natural := c_len_arr(I) - c_pkt_overhead_len;
        constant cK             : natural := I * c_nof_input + J;
        variable v_data         : natural;
      begin
        v_data := c_data_init + cK;
        in_sosi_2arr(I)(J) <= c_dp_sosi_rst;
        proc_common_wait_until_low(clk, rst);
        proc_common_wait_some_cycles(clk, 5);

        -- Begin of stimuli
        for R in 0 to c_repeat_arr(cK) - 1 loop
          proc_dp_gen_block_data(c_rl, true, g_data_w, g_data_w, v_data, 0, 0, c_pkt_data_len, 0, 0, c_sl0, c_bsn_init, clk, in_en, in_siso_2arr(I)(J), in_sosi_2arr(I)(J));
          --proc_common_wait_some_cycles(clk, c_nof_streams*50-cK);
          v_data := v_data + c_pkt_data_len;   if v_data >= c_data_mod then v_data := v_data - c_data_mod; end if;
        end loop;

        -- End of stimuli
        expected_out_data(I, J) <= TO_UVEC(v_data - 1, g_data_w);
        wait;
      end process;

      in_data(I, J) <= in_sosi_2arr(I)(J).data(g_data_w - 1 downto 0);
      in_val( I, J) <= in_sosi_2arr(I)(J).valid;
      in_sop( I, J) <= in_sosi_2arr(I)(J).sop;
      in_eop( I, J) <= in_sosi_2arr(I)(J).eop;
    end generate;
  end generate;

  in_val_or <= matrix_or(in_val, c_nof_tlen, c_nof_input);

  p_in_ready : process(in_siso_2arr)
  begin
    for I in 0 to c_nof_tlen - 1 loop
      for J in 0 to c_nof_input - 1 loop
        in_ready(I, J) <= in_siso_2arr(I)(J).ready;
      end loop;
    end loop;
  end process;

  in_ready_or <= matrix_or(in_ready, c_nof_tlen, c_nof_input);

  ------------------------------------------------------------------------------
  -- LEVEL 1: USER DATA VERIFICATION
  ------------------------------------------------------------------------------

  prev_count_eop <= count_eop when rising_edge(clk);

  gen_verify : for I in 0 to c_nof_tlen - 1 generate
    gen_output : for J in 0 to c_nof_input - 1 generate
      -- Verification logistics
      verify_en(I, J) <= '1'              when rising_edge(clk) and out_sosi_2arr(I)(J).sop = '1';  -- verify enable after first output sop
      count_eop(I, J) <= count_eop(I, J) + 1 when rising_edge(clk) and out_sosi_2arr(I)(J).eop = '1';  -- count number of output eop
      verify_done(I, J) <= '1'            when rising_edge(clk) and count_eop(I, J) = c_repeat_arr(I * c_nof_input + J) and count_eop(I, J) /= prev_count_eop(I, J) else '0';
      tb_end_vec(I * c_nof_input + J) <= '1' when rising_edge(clk) and count_eop(I, J) = c_repeat_arr(I * c_nof_input + J);

      -- Actual verification of the output streams
      proc_dp_verify_data("out_sosi_2arr.data", c_rl, clk, verify_en(I, J), out_siso_2arr(I)(J).ready, out_sosi_2arr(I)(J).valid, out_data(I, J), prev_out_data(I, J));  -- Verify that the output is incrementing data, like the input stimuli
      proc_dp_verify_valid(c_rl, clk, verify_en(I, J), out_siso_2arr(I)(J).ready, prev_out_ready(I, J), out_sosi_2arr(I)(J).valid);  -- Verify that the output valid fits with the output ready latency
      proc_dp_verify_sop_and_eop(clk, out_sosi_2arr(I)(J).valid, out_sosi_2arr(I)(J).sop, out_sosi_2arr(I)(J).eop, hold_out_sop(I, J));  -- Verify that sop and eop come in pairs
      proc_dp_verify_value(e_equal, clk, verify_done(I, J), expected_out_data(I, J), prev_out_data(I, J));  -- Verify that the stimuli have been applied at all
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- LEVEL 2: MULTIPLEXER
  ------------------------------------------------------------------------------

  -- Input level multiplexing
  gen_mux : for I in 0 to c_nof_tlen - 1 generate
    u_input : entity dp_lib.dp_mux
    generic map (
      g_nof_input       => c_nof_input,
      g_use_fifo        => false,
      g_fifo_size       => array_init(1024, c_nof_input),  -- must match g_nof_input, even when g_use_fifo=FALSE
      g_fifo_fill       => array_init(   0, c_nof_input)  -- must match g_nof_input, even when g_use_fifo=FALSE
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- ST sinks
      snk_out_arr => in_siso_2arr(I),
      snk_in_arr  => in_sosi_2arr(I),
      -- ST source
      src_in      => mux_siso_arr(I),
      src_out     => mux_sosi_arr(I)
    );
  end generate;

  -- Type level multiplexing
  u_tlen_mux : entity dp_lib.dp_mux
  generic map (
    g_nof_input       => c_nof_tlen,
    g_use_fifo        => false,
    g_fifo_size       => array_init(1024, c_nof_tlen),  -- must match g_nof_input, even when g_use_fifo=FALSE
    g_fifo_fill       => array_init(   0, c_nof_tlen)  -- must match g_nof_input, even when g_use_fifo=FALSE
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => mux_siso_arr,
    snk_in_arr  => mux_sosi_arr,
    -- ST source
    src_in      => mux_siso,
    src_out     => mux_sosi
  );

  -- Map to slv to ease monitoring in wave window
  tx_data    <= tx_sosi.data(g_data_w - 1 downto 0);
  tx_val     <= tx_sosi.valid;
  tx_sop     <= tx_sosi.sop;
  tx_eop     <= tx_sosi.eop;
  tx_channel <= tx_sosi.channel(c_channel_w - 1 downto 0);
  tx_err     <= tx_sosi.err(c_err_w - 1 downto 0);

  ------------------------------------------------------------------------------
  -- LEVEL 2: MUX-DEMUX LOOPBACK
  ------------------------------------------------------------------------------

  no_loopback_mux : if c_loopback_mux = false generate
    mux_siso   <= tx_siso;
    rx_siso    <= demux_siso;
    tx_sosi    <= mux_sosi;
    demux_sosi <= rx_sosi;
  end generate;

  gen_loopback_mux : if c_loopback_mux = true generate
    mux_siso   <= demux_siso;
    demux_sosi <= mux_sosi;
  end generate;

  ------------------------------------------------------------------------------
  -- LEVEL 2: DE-MULTIPLEXER
  ------------------------------------------------------------------------------

  -- Type level multiplexing
  u_tlen_demux : entity dp_lib.dp_demux
  generic map (
    g_nof_output  => c_nof_tlen,
    g_combined    => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out     => demux_siso,
    snk_in      => demux_sosi,
    -- ST source
    src_in_arr  => demux_siso_arr,
    src_out_arr => demux_sosi_arr
  );

  gen_demux : for I in 0 to c_nof_tlen - 1 generate
    u_output : entity dp_lib.dp_demux
    generic map (
      g_nof_output  => c_nof_input,
      g_combined    => false
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- ST sinks
      snk_out     => demux_siso_arr(I),
      snk_in      => demux_sosi_arr(I),
      -- ST source
      src_in_arr  => out_siso_2arr(I),
      src_out_arr => out_sosi_2arr(I)
    );

    gen_output : for J in 0 to c_nof_input - 1 generate
      out_data(I, J) <= out_sosi_2arr(I)(J).data(g_data_w - 1 downto 0);
      out_val( I, J) <= out_sosi_2arr(I)(J).valid;
      out_sop( I, J) <= out_sosi_2arr(I)(J).sop;
      out_eop( I, J) <= out_sosi_2arr(I)(J).eop;
    end generate;
  end generate;

  out_val_or <= matrix_or(out_val, c_nof_tlen, c_nof_input);

  ------------------------------------------------------------------------------
  -- LEVEL 3: TRANSMITTER DP SOSI ==> DP PACKET
  ------------------------------------------------------------------------------

  -- Encode the data block to a DP packet
  u_dp_packet_enc : entity dp_lib.dp_packet_enc
  generic map (
    g_data_w     => g_data_w,
    g_channel_lo => c_nof_tlen_w
  )
  port map (
    rst       => rst,
    clk       => clk,
    -- ST sinks
    snk_out   => tx_siso,
    snk_in    => tx_sosi,
    -- ST source
    src_in    => pkt_enc_siso,
    src_out   => pkt_enc_sosi
  );

  -- Map to slv to ease monitoring in wave window
  pkt_tx_data    <= pkt_tx_sosi.data(g_data_w - 1 downto 0);
  pkt_tx_val     <= pkt_tx_sosi.valid;
  pkt_tx_sop     <= pkt_tx_sosi.sop;
  pkt_tx_eop     <= pkt_tx_sosi.eop;
  pkt_tx_channel <= pkt_tx_sosi.channel(c_channel_w - 1 downto 0);
  pkt_tx_err     <= pkt_tx_sosi.err(c_err_w - 1 downto 0);

  ------------------------------------------------------------------------------
  -- LEVEL 3: ENC-DEC LOOPBACK
  ------------------------------------------------------------------------------

  no_loopback_packet : if c_loopback_packet = false generate
    pkt_enc_siso <= pkt_tx_siso;
    pkt_rx_siso  <= pkt_dec_siso;
    pkt_tx_sosi  <= pkt_enc_sosi;
    pkt_dec_sosi <= pkt_rx_sosi;
  end generate;

  gen_loopback_packet : if c_loopback_packet = true generate
    pkt_enc_siso <= pkt_dec_siso;
    pkt_dec_sosi <= pkt_enc_sosi;
  end generate;

  ------------------------------------------------------------------------------
  -- LEVEL 3: RECEIVER DP PACKET ==> DP SOSI
  ------------------------------------------------------------------------------

  -- Decode the DP packet into a data block
  u_dp_packet_dec : entity dp_lib.dp_packet_dec
  generic map (
    g_data_w     => g_data_w,
    g_channel_lo => c_nof_tlen_w
  )
  port map (
    rst       => rst,
    clk       => clk,
    -- ST sinks
    snk_out   => pkt_dec_siso,
    snk_in    => pkt_dec_sosi,
    -- ST source
    src_in    => rx_siso,
    src_out   => rx_sosi
  );

  -- Map to slv to ease monitoring in wave window
  rx_data    <= rx_sosi.data(g_data_w - 1 downto 0);
  rx_val     <= rx_sosi.valid;
  rx_sop     <= rx_sosi.sop;
  rx_eop     <= rx_sosi.eop;
  rx_err     <= rx_sosi.err(c_err_w - 1 downto 0);
  rx_channel <= rx_sosi.channel(c_channel_w - 1 downto 0);
  rx_sync    <= rx_sosi.sync;
  rx_bsn     <= rx_sosi.bsn(c_bsn_w - 1 downto 0);

  ------------------------------------------------------------------------------
  -- LEVEL 4: TRANSMITTER DP PACKET ==> UTH PACKET
  ------------------------------------------------------------------------------

  -- Transmit the data block as a UTH frame
  u_uth_tx : entity work.uth_tx(rtl_hold)
  generic map (
    g_data_w      => g_data_w,
    g_nof_ch      => c_nof_tlen,
    g_typ_arr     => c_typ_arr
  )
  port map (
    rst       => rst,
    clk       => clk,
    -- ST sinks
    snk_out   => pkt_tx_siso,
    snk_in    => pkt_tx_sosi,
    -- ST source
    src_in    => uth_tx_siso,
    src_out   => uth_tx_sosi
  );

  -- Map to slv to ease monitoring in wave window
  uth_tx_data <= uth_tx_sosi.data(g_data_w - 1 downto 0);
  uth_tx_val  <= uth_tx_sosi.valid;
  uth_tx_sop  <= uth_tx_sosi.sop;
  uth_tx_eop  <= uth_tx_sosi.eop;

  ------------------------------------------------------------------------------
  -- LEVEL 4: RECEIVER UTH PACKET ==> DP PACKET
  ------------------------------------------------------------------------------

  -- Receive UTH frame and extract the payload data block
  u_uth_rx : entity work.uth_rx(rtl_hold)
  generic map (
    g_data_w      => g_data_w,
    g_len_max     => c_max_len,
    g_nof_ch      => c_nof_tlen,
    g_typ_arr     => c_typ_arr,
    g_len_arr     => c_len_arr
  )
  port map (
    rst       => rst,
    clk       => clk,
    -- ST sinks
    snk_out   => uth_rx_siso,
    snk_in    => uth_rx_sosi,
    -- ST source
    src_in    => pkt_rx_siso,
    src_out   => pkt_rx_sosi
  );

  -- Map to slv to ease monitoring in wave window
  pkt_rx_data    <= pkt_rx_sosi.data(g_data_w - 1 downto 0);
  pkt_rx_val     <= pkt_rx_sosi.valid;
  pkt_rx_sop     <= pkt_rx_sosi.sop;
  pkt_rx_eop     <= pkt_rx_sosi.eop;
  pkt_rx_channel <= pkt_rx_sosi.channel(c_channel_w - 1 downto 0);
  pkt_rx_err     <= pkt_rx_sosi.err(c_err_w - 1 downto 0);

  ------------------------------------------------------------------------------
  -- LEVEL 5: PHY LINK INTERFACE
  ------------------------------------------------------------------------------

  -- Model the transceiver link
  -- . No sosi delay using dp_phy_link, to allow siso backpressure
  -- . Pass on backpressure from Rx to Tx for verification purposes, true backpressure at PHY link level needs to be done at packet level
  -- . Model link error somewhere during a UTH payload to check the Rx CRC

  p_phy_link : process(uth_tx_sosi, phy_link_err)
  begin
    uth_rx_sosi      <= uth_tx_sosi;
    uth_rx_sosi.data <= RESIZE_DP_DATA(uth_tx_sosi.data(g_data_w - 1 downto 0));  -- limit PHY data width to g_data_w
    if g_phy_link_valid_support = false then
      uth_rx_sosi.valid <= '1';
    end if;
    if phy_link_err = '1' then
      uth_rx_sosi.data <= TO_DP_SDATA(-1);
    end if;
  end process;

  uth_tx_siso <= uth_rx_siso;

  phy_link_err <= '0';
  --phy_link_err <= '0', '1' AFTER 42.1*clk_period, '0' AFTER 43.1*clk_period;  -- determine appropriate error pulse instant from the Wave window in a trial simualtion

  -- Map to slv to ease monitoring in wave window
  uth_rx_data    <= uth_rx_sosi.data(g_data_w - 1 downto 0);
  uth_rx_val     <= uth_rx_sosi.valid;
  uth_rx_sop     <= uth_rx_sosi.sop;
  uth_rx_eop     <= uth_rx_sosi.eop;
end tb;
