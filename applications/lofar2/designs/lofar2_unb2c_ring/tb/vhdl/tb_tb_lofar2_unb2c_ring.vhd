-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Verify multiple variations of tb_lofar2_unb2c_ring
-- Description:
-- Usage:
-- > as 3
-- > run -all
--   . tb takes about 1h4m
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_pkg.all;

entity tb_tb_lofar2_unb2c_ring is
end tb_tb_lofar2_unb2c_ring;

architecture tb of tb_tb_lofar2_unb2c_ring is
  constant c_nof_rn   : natural := 3;
  constant c_nof_tb   : natural := 5;
  signal   tb_end_vec : std_logic_vector(c_nof_tb - 1 downto 0) := (others => '0');
  signal   tb_end     : std_logic;  -- declare tb_end as STD_LOGIC to avoid 'No objects found' error on 'when -label tb_end' in *.do file
begin
--    g_multi_tb            : BOOLEAN              := FALSE;
--    g_unb_nr              : NATURAL              := 4;
--    g_design_name         : STRING               := "lofar2_unb2c_ring_one";
--    g_nof_rn              : NATURAL              := 16;
--    g_nof_block_per_sync  : NATURAL              := 32;
--    g_access_scheme       : INTEGER RANGE 1 TO 3 := 2

-- using different g_unb_nr to avoid MM file clashing.
  u_one_1    : entity work.tb_lofar2_unb2c_ring generic map(true, 0, "lofar2_unb2c_ring_one",  c_nof_rn,  3, 1) port map(tb_end_vec(0));  -- access scheme 1.
  u_one_2_3  : entity work.tb_lofar2_unb2c_ring generic map(true, 1, "lofar2_unb2c_ring_one",  c_nof_rn,  3, 2) port map(tb_end_vec(1));  -- access scheme 2/3. Tb for access scheme 2 is same tb for 3
  u_full_1   : entity work.tb_lofar2_unb2c_ring generic map(true, 2, "lofar2_unb2c_ring_full", c_nof_rn,  3, 1) port map(tb_end_vec(2));  -- access scheme 1.
  u_full_2_3 : entity work.tb_lofar2_unb2c_ring generic map(true, 3, "lofar2_unb2c_ring_full", c_nof_rn, 32, 2) port map(tb_end_vec(3));  -- access scheme 2/3. Tb for access scheme 2 is same tb for 3
  -- u_16_rn -> Using 16 ring nodes. Using the _one revision as it only uses 1 lane to limit MM readout time. Also using
  -- g_nof_block_per_sync = 32 as all bsn monitors have to be read during one sync period which takes more time with 16 nodes.
  u_16_rn    : entity work.tb_lofar2_unb2c_ring generic map(true, 4, "lofar2_unb2c_ring_one",        16, 32, 2) port map(tb_end_vec(4));

  tb_end <= vector_and(tb_end_vec);
  proc_common_stop_simulation(tb_end);
end tb;
