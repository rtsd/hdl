// Copyright (C) Altera Corporation. All rights reserved. 
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 11.1sp2 linux32 Build 259 01/26/2012
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6c"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
kLZr13QLguIANDar9SulmA7hFf2aqzrtRVOIklmFZVPadAAhrxt4F23s/2iYEhJH
+o8fhXosOcAZiHnzyKq07f1YCEkRAItSnQrsYPF8j0hJhn1DM/alSlcavHIXmH2P
FdI/HIJng41AFhkct+wjpUFz+wmpC/3EdHZZzPUP+nQ=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1920)
eXr5AcWAly4w3dXx5sROdvKFBwCeLZmjp4KmZgUCmiAASwzYZpa+CpxNy5KRl246
UArn5xaf7nwWYuvttlWnNL/lJJXPiJSKywVKCBXil6gc1JilEHik44jiLB2Ftnmr
FlKDz3KmHWNUf6Z7as8hpZHi5N84HGKAeNBOdECxkB1ve7nEve86gFpRqlAhZ/eN
0gWxXWmgGVPjYDR5yuIeplE5DOOozIGWXEfYSzPB79HwEgtJxjekQei3KF28YuiY
WK9S/2zjaIBT0ypOiZ/yC2z/aL3JDyDye0m1K/+CSnP94u1kXkNS7RKO2db+pM0p
fQ7UartWkwk2zitV2IuY+HCEs/T8/y8yor7erWjN6mRCWRQdVpKVKwI7t35rwSbT
zl02Kzat6XDv5E5rQF4nUkx70qnBHSgxvM7ZdJqxbguZaBjdjvjOGs5Z9o7jAHmp
DNWihVqTEpoQ7r6WEzLDyIVCmIb1X5MBbi8R5DfxFCeh6B+0jxMnCUB3I9zrX1Qn
CZ7Q/0DlwRlLZDK05cM1p5kYt7wh+cKXOi+TcEO35EHInjlE/PP7fJfECirl80iZ
gjeHqqk+zWm4R4l67RJI8hgyfNt9H2BlCc+xBk7DRaw5Q+Gp4+Gd6TyPzCfabbDV
CQqi+dvPvDNzhknWtiYJ0do2Oxb1FBQWFpM4fcArnJbu3FtRCXN5gNtV6pxMq/cC
BghGxhD3Ya/7ORuRCHD3rjSSQtJo8TcqHlg0qXpNM4b9f+z2xNSbC9QdI24zGFTB
qblV7XhcafpJ5tUZ1pPKnWL97FLvaJxZB8T0eo6bilAjXOrExEGfA2rTxDsEmzDm
vcUNl8AX0olp4CywtwG6qFcJ3JbPbFBW+JgqJEOFN5uFaCX2jCZouDovihKUwJ81
jnlGr21/HnMxAWUK4+rGmiRQ9zHgYBFF8CsriNVqsicc6SaPaoH8Dwh6Lax1nrO9
SK04g5xL+YE53jVQDB1z1V+b1F3Y59buUAeqaoXMYVvgtDXgxf2VeLqeaTrbD773
PevRcqOJdD6u50wp54Fuir6BqeHX69Dr3hLkl48SaNGw6fnMYTi7ztkV/TJ1nKi7
Mt4PogePZNTxoUOoqYppY6i6OOMnEqJ3dwD0kTSEF1UYEg7ebVB3ISIKRgO8SlV6
lJcbP0K2B9RBWgnWpM0U9kwohgRnpbyXlSa3rSxLs+WaMisDnACzz96VgoHqfX9f
loSgmqVVgqKGuToMt48nEqC2AgQZJjeKa8eEqu3hJtLiOU4pUKCFFa1DPaYswdTz
7C2sKWrQrZOBZ3eAWJ8+j6a2yHk42NAkKCEWf8f7c31YyatYGnm6PnXsQOCkLeHy
a1flbxwGmKDlGvOpr99x+JdqYiDQQatcbJGQXhKHZqHK8JKRh48ZAhDKQnSFlQUL
61N2ykiEs5JCgqcslrUKmG3y4x4J51hPUYdfvK1Em9PIbsxbXDhGeMqN83e43QJQ
guDaOLs1ICisP3ml32/EYFkePgNgXulO2vPteR7jFnuElyOryANeeTzK8yZ1ANnl
63NoXohuldSFdiUUxtqnpuqjLx6igNk2Fur4IbYTDoh1eONPsBylZwxZrFWjmd5X
EvXoKET0SDLMO6DDqToujZxcaOU4yXyEMWULBlnkcXgomL7wKc+6DhvN8NCUA0hn
p6pptB+MHGmXRvHFGVdCb3ZMagvT1jcyG2lW+w8b2gtou2iNCOPO5OWXioRPEoFi
Byc/ybepK+gxaYNYDOdj5RNoC1Lt6X4tBC0uIebHtkeP0QA5CPiBzV/oovzQJ2KE
j7aZPLLPjAfs/7F3EaaJuxMDioky9k9XGcisg2PMn2DzDP9SfQvtD3qendZ77VQA
GqVNpa/uwAnjIhm3Kop1mWlzq4Q10B9gjuEsTdYgtx5SRNO7ZGvCmC/SubeAmwaG
J8iuvppYyUlEVjjQPFYXWlzGDbeM9Lo083BCVL5ySvXVAwnkD4RsvhoT7M6JTQGt
nKuHRv4qbUnSmrZv6ferDPQfxN1AxZD9ZVTGyw2Yy9Rhtykd50KkZNCug4xDl4km
qdPGl2F8EcgJ2XargNlhQ/iwTWQABrR3mnPa+tDr1fi8HMCUUHh/sr3ygKYm6yZF
nd3BqEEBwF8qNfFFJvseWKR1CRHxrWDY+YSqy/uKzl1gH4Np/Q0yFQioDRPP/OH1
899qyXiX6Pa4WBo2v+IG+Gix0Ys8hbbfFBRMWRsH7Zad/67ygyMdlYds3YkJE/ez
FJ+bC9rkl1C6a1uz+DSyvVcCk1JuoYvz3XOHJBEZQD4WnVImWjyBxvz/1C9PRYMn
O5xtRurcQy4F7J/upFQv3BaUuSDCGlNS1YLFRNj5ypHTAgnxWxgso6J3v3+vk3G+
OdRcBmv1/eshwVb1MEFgLLYTC2S9bH93ExFZQAmI/49d4zLx8nt+rqAjFtioFop3
wMD5J7p5iM/a7F8ebpH/8e7758WBtzW/1x1nfEGFH0QHG7ZcKEoxlM/PVwhOr5nZ
GiZqz2Spxk9oi0TZXSKPEan+aOW6Cics2Wvjm12oVHFgcvH/elqkI3BmcnSEC7Ia
`pragma protect end_protected
