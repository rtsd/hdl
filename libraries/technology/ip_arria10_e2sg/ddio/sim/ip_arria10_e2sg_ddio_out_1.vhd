-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Simulation model for DDIO out
-- Description:
--   This function is the inverse of DDIO in as described in ip_arria10_e2sg_ddio_in_1.
--   The timing diagram:
--                 _   _   _   _   _
--     outclock   | |_| |_| |_| |_| |_
--     datain_h        1   3   5
--     datain_l        0   2   4
--     dataout @ r         1   3   5
--     dataout @ f       0   2   4
--     dataout           0 1 2 3 4 5 6 7
--

library IEEE;
use IEEE.std_logic_1164.all;

entity ip_arria10_e2sg_ddio_out_1 is
  port (
    dataout  : out std_logic_vector(0 downto 0);
    outclock : in  std_logic                    := '0';
    aclr     : in  std_logic                    := '0';
    datain_h : in  std_logic_vector(0 downto 0) := (others => '0');
    datain_l : in  std_logic_vector(0 downto 0) := (others => '0')
  );
end ip_arria10_e2sg_ddio_out_1;

architecture beh of ip_arria10_e2sg_ddio_out_1 is
  signal out_dat_r   : std_logic;
  signal out_dat_f   : std_logic;
begin
  dataout <= datain_l when falling_edge(outclock) else
             datain_h when rising_edge(outclock);
end beh;
