-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_concat is
end tb_tb_dp_concat;

architecture tb of tb_tb_dp_concat is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 10
  -- > run 300 us --> OK

  -- Try tb_dp_concat settings : GENERIC MAP (g_data_w, g_symbol_w, g_random_control)

  u_nof_1     : entity work.tb_dp_concat generic map (1 * 16, 16, false);
  u_nof_2     : entity work.tb_dp_concat generic map (2 * 16, 16, false);
  u_nof_3     : entity work.tb_dp_concat generic map (3 * 16, 16, false);
  u_nof_4     : entity work.tb_dp_concat generic map (4 * 16, 16, false);

  u_nof_1_rnd : entity work.tb_dp_concat generic map (1 * 16, 16, true);
  u_nof_2_rnd : entity work.tb_dp_concat generic map (2 * 16, 16, true);
  u_nof_3_rnd : entity work.tb_dp_concat generic map (3 * 16, 16, true);
  u_nof_4_rnd : entity work.tb_dp_concat generic map (4 * 16, 16, true);
end tb;
