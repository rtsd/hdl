-------------------------------------------------------------------------------
--
-- Copyright (C) 2010-2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

-- Purpose:
--   Extend the input WDI that is controlled in SW (as it should be) to avoid
--   that the watchdog reset will occur when new SW is loaded, while keeping
--   the HDL image. This component extends the last input WDI by toggling the
--   output WDI for about 2**(g_extend_w-1) ms more.

entity unb2c_board_wdi_extend is
  generic (
    g_extend_w : natural := 14
  );
  port (
    rst              : in  std_logic;
    clk              : in  std_logic;
    pulse_ms         : in  std_logic;  -- pulses every 1 ms
    wdi_in           : in  std_logic;
    wdi_out          : out std_logic
  );
end unb2c_board_wdi_extend;

architecture str of unb2c_board_wdi_extend is
  signal wdi_evt     : std_logic;

  signal wdi_cnt     : std_logic_vector(g_extend_w - 1 downto 0);
  signal wdi_cnt_en  : std_logic;

  signal i_wdi_out   : std_logic;
  signal nxt_wdi_out : std_logic;
begin
  wdi_out <= i_wdi_out;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      i_wdi_out <= '0';
    elsif rising_edge(clk) then
      i_wdi_out <= nxt_wdi_out;
    end if;
  end process;

  wdi_cnt_en <= '1' when pulse_ms = '1' and wdi_cnt(wdi_cnt'high) = '0' else '0';

  nxt_wdi_out <= not i_wdi_out when wdi_cnt_en = '1' else i_wdi_out;

  u_common_evt : entity common_lib.common_evt
  generic map (
    g_evt_type => "BOTH",
    g_out_reg  => true
  )
  port map (
    rst      => rst,
    clk      => clk,
    in_sig   => wdi_in,
    out_evt  => wdi_evt
  );

  u_common_counter : entity common_lib.common_counter
  generic map (
    g_width   => g_extend_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => wdi_evt,
    cnt_en  => wdi_cnt_en,
    count   => wdi_cnt
  );
end str;
