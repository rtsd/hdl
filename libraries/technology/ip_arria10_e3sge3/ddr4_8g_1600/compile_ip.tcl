#------------------------------------------------------------------------------
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file generated/sim/mentor/msim_setup.tcl
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR

set IP_DIR   "$env(HDL_BUILD_DIR)/sim"

#vlib ./work/         ;# Assume library work already exists

vmap ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151 ./work/
vmap ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_151         ./work/

  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151_4thorvi.sv" -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_top.sv"                                 -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_bufs.sv"                                -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_buf_udir_se_i.sv"                       -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_buf_udir_se_o.sv"                       -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_buf_udir_df_i.sv"                       -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_buf_udir_df_o.sv"                       -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_buf_udir_cp_i.sv"                       -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_buf_bdir_df.sv"                         -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_buf_bdir_se.sv"                         -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_buf_unused.sv"                          -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_pll.sv"                                 -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_pll_fast_sim.sv"                        -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_pll_extra_clks.sv"                      -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_oct.sv"                                 -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_core_clks_rsts.sv"                      -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_hps_clks_rsts.sv"                       -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_io_aux.sv"                              -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_io_tiles.sv"                            -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_hmc_avl_if.sv"                          -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_hmc_sideband_if.sv"                     -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_hmc_mmr_if.sv"                          -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_hmc_amm_data_if.sv"                     -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_hmc_ast_data_if.sv"                     -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_afi_if.sv"                              -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_seq_if.sv"                              -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_emif_arch_nf_regs.sv"                                -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_oct.sv"                                              -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog -sv "$IP_DIR/../altera_emif_arch_nf_151/sim/altera_oct_um_fsm.sv"                                       -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_arch_nf_151
  vlog     "$IP_DIR/../altera_emif_151/sim/ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_151_nnn2r2a.v"                  -work ip_arria10_e3sge3_ddr4_8g_1600_altera_emif_151        
  vcom     "$IP_DIR/ip_arria10_e3sge3_ddr4_8g_1600.vhd"                                                                                                                    
