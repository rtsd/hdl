--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
-- . A RAM of which the output can be shifted in time relative to the input
-- Description:
-- . Valid data is continuously written from adddres 0..g_nof_words-1.
-- . The shift is introduced by manipulating the read address.
-- . The maximum shift value is g_nof_words-2 instead of g_nof_words-1, this
--   is due to the one word/cycle that is required between a write and a read
--   on the same address. Higher shift values than the maximum will be
--   interpreted as the maximum value.
-- . data_out_shift will always indicate the shift as applied to the
--   corresponding data_out.
-- . The common_shiftram has a latency of c_shiftram_latency = 3, so the
--   actual shift is c_shiftram_latency + data_in_shift.
--
--
--    Cycle 0              Cycle 1             Cycle 2           Cycle 3
--    =======      _       =======     _       =======           =======
-- data_in      ->| |-->ram_wr_data-->| |
-- data_in_val  ->| |-->ram_wr_addr-->| |
-- data_in_shift->| |-->ram_wr_en---->|_|
--                |_|-->ram_wr_shift   R
--                r0                   _                   _
--                      ram_wr_addr-->| |-->ram_rd_en---->| |-->data_out
--                      ram_wr_en---->| |-->ram_rd_addr-->|_|-->data_out_val
--                      ram_wr_shift->| |                  R
--                                    | |                  _
--                                    |_|-->ram_rd_shift->|_|-->data_out_shift
--                                    r1                  r2
--
-- R      = RAM I/O
-- r0..r2 = register stages.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_shiftram is
  generic (
    g_technology  : natural := c_tech_select_default;
    g_data_w      : natural;
    g_nof_words   : natural;  -- Depth of RAM. Must be a power of two.
    g_output_invalid_during_shift_incr : boolean := false;
    g_fixed_shift : boolean := false  -- If data_in_shift is constant, set to TRUE
  );  -- for better timing results
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    data_in        : in  std_logic_vector(g_data_w - 1 downto 0);
    data_in_val    : in  std_logic;
    data_in_shift  : in  std_logic_vector(ceil_log2(g_nof_words - 1) - 1 downto 0);

    data_out       : out std_logic_vector(g_data_w - 1 downto 0);
    data_out_val   : out std_logic;
    data_out_shift : out std_logic_vector(ceil_log2(g_nof_words - 1) - 1 downto 0)
  );
end common_shiftram;

architecture rtl of common_shiftram is
  -- RAM constants
  constant c_ram_rl      : natural := 1;
  constant c_ram_addr_w  : natural := ceil_log2(g_nof_words);
  constant c_ram_data_w  : natural := g_data_w;
  constant c_ram_nof_dat : natural := g_nof_words;
  constant c_ram_init_sl : std_logic := '0';
  constant c_ram         : t_c_mem := (c_ram_rl, c_ram_addr_w, c_ram_data_w,
                                       c_ram_nof_dat, c_ram_init_sl);

  signal ram_data_out     : std_logic_vector(g_data_w - 1 downto 0);
  signal ram_data_out_val : std_logic;

  -- Register stage 0
  type t_reg_0 is record
    ram_wr_data     : std_logic_vector(c_ram_data_w - 1 downto 0);
    ram_wr_addr     : std_logic_vector(c_ram_addr_w - 1 downto 0);
    ram_wr_en       : std_logic;
    ram_wr_shift    : std_logic_vector(c_ram_addr_w - 1 downto 0);
    ram_wr_shift_incr : boolean;
  end record;

  constant c_reg_0_defaults : t_reg_0 := ( (others => '0'),
                                           (others => '0'),
                                           '0',
                                           (others => '0'),
                                           false);

  signal r0, nxt_r0 : t_reg_0 := c_reg_0_defaults;

  -- Register stage 1
  type t_reg_1 is record
    ram_rd_addr     : std_logic_vector(c_ram_addr_w - 1 downto 0);
    ram_rd_en       : std_logic;
    ram_rd_shift    : std_logic_vector(c_ram_addr_w - 1 downto 0);
  end record;

  constant c_reg_1_defaults : t_reg_1 := ( (others => '0'),
                                           '0',
                                           (others => '0'));

  signal r1, nxt_r1 : t_reg_1 := c_reg_1_defaults;

  -- Register stage 2
  type t_reg_2 is record
    data_out_shift    : std_logic_vector(c_ram_addr_w - 1 downto 0);
  end record;

  constant c_reg_2_defaults : t_reg_2 :=  (others => (others => '0'));

  signal r2, nxt_r2 : t_reg_2 := c_reg_2_defaults;

  -- Register stage 3 (optional)
  type t_reg_3 is record
    data_out_shift    : std_logic_vector(c_ram_addr_w - 1 downto 0);
    data_out          : std_logic_vector(g_data_w - 1 downto 0);
    data_out_val      : std_logic;
  end record;

  constant c_reg_3_defaults : t_reg_3 :=  ( (others => '0'),
                                            (others => '0'),
                                            '0');

  signal r3, nxt_r3 : t_reg_3 := c_reg_3_defaults;
begin
  -----------------------------------------------------------------------------
  -- Register stage 0
  -----------------------------------------------------------------------------
  r0 <= nxt_r0 when rising_edge(clk);

  p_comb_0 : process(rst, r0, data_in, data_in_val, data_in_shift)
    variable v : t_reg_0;
    variable v_data_in_shift : std_logic_vector(c_ram_addr_w - 1 downto 0);
  begin
    v := r0;

    v.ram_wr_data  := data_in;
    v.ram_wr_en    := data_in_val;

    if data_in_val = '1' then
      -- Limit max shift to g_nof_words-2
        v_data_in_shift := data_in_shift;

      if v_data_in_shift = TO_UVEC(g_nof_words - 1, c_ram_addr_w) then
        v_data_in_shift := TO_UVEC(g_nof_words - 2, c_ram_addr_w);
      end if;

      if r0.ram_wr_en = '1' then
        v.ram_wr_addr := INCR_UVEC(r0.ram_wr_addr, 1);
      end if;

      if r0.ram_wr_shift_incr = true then
        -- New shift value is more than one higher than previous shift
        v.ram_wr_shift := INCR_UVEC(r0.ram_wr_shift, 1);
      end if;
      if TO_UINT(v_data_in_shift) < TO_UINT(v.ram_wr_shift) then
        -- User wants to lower the shift. Forward this request immediately
        v.ram_wr_shift := v_data_in_shift;
      end if;

      -- Smooth out the shift input by the user so it does not increment by
      -- more than one at a time
      if TO_UINT(v_data_in_shift) > TO_UINT(v.ram_wr_shift) then
        v.ram_wr_shift_incr := true;
      else
        v.ram_wr_shift_incr := false;
      end if;
    end if;

    if rst = '1' then
      v := c_reg_0_defaults;
    end if;

    nxt_r0 <= v;
  end process;

  -----------------------------------------------------------------------------
  -- Register stage 1
  -----------------------------------------------------------------------------
  r1 <= nxt_r1 when rising_edge(clk);

  p_comb_1 : process(rst, r1, r0)
    variable v : t_reg_1;
    variable v_shift_diff : integer := 0;
  begin
    v := r1;

    v.ram_rd_en    := r0.ram_wr_en;
    v.ram_rd_shift := r0.ram_wr_shift;

    if r1.ram_rd_en = '1' then
      -- Read next address
      v.ram_rd_addr  := INCR_UVEC(r1.ram_rd_addr, 1);

      if TO_UINT(r0.ram_wr_shift) > TO_UINT(r1.ram_rd_shift) then
        -- We need to shift by reading the same address again
        v.ram_rd_addr := r1.ram_rd_addr;
      end if;

      if g_fixed_shift = false and (TO_UINT(r0.ram_wr_shift) < TO_UINT(r1.ram_rd_shift)) then
        -- Apply shift decrease at instantaniously
        v_shift_diff := TO_UINT(r1.ram_rd_shift) - TO_UINT(r0.ram_wr_shift) + 1;
        v.ram_rd_addr := INCR_UVEC(r1.ram_rd_addr, v_shift_diff);
      end if;
    end if;

    if rst = '1' then
      v := c_reg_1_defaults;
    end if;

    nxt_r1 <= v;
  end process;

  -----------------------------------------------------------------------------
  -- Register stage 2
  -----------------------------------------------------------------------------
  r2 <= nxt_r2 when rising_edge(clk);

  p_comb_2 : process(rst, r2, r1)
    variable v : t_reg_2;
  begin
    v := r2;

    v.data_out_shift := r1.ram_rd_shift;

    if rst = '1' then
      v := c_reg_2_defaults;
    end if;

    nxt_r2 <= v;
  end process;

--  data_out_shift <= r2.data_out_shift;

  -----------------------------------------------------------------------------
  -- RAM
  -----------------------------------------------------------------------------
  u_common_ram_r_w: entity common_lib.common_ram_r_w
  generic map (
    g_technology => g_technology,
    g_ram       => c_ram,
    g_init_file => "UNUSED",
    g_true_dual_port => false
  )
  port map (
    rst       => rst,
    clk       => clk,
    clken     => '1',
    wr_en     => r0.ram_wr_en,
    wr_adr    => r0.ram_wr_addr,
    wr_dat    => r0.ram_wr_data,
    rd_en     => r1.ram_rd_en,
    rd_adr    => r1.ram_rd_addr,
    rd_dat    => ram_data_out,
    rd_val    => ram_data_out_val
  );

  gen_outputs: if g_output_invalid_during_shift_incr = false generate
    data_out_shift <= r2.data_out_shift;
    data_out       <= ram_data_out;
    data_out_val   <= ram_data_out_val;
  end generate;

  -----------------------------------------------------------------------------
  -- Register stage 3 (optional)
  -----------------------------------------------------------------------------
  gen_output_invalid: if g_output_invalid_during_shift_incr = true generate
    r3 <= nxt_r3 when rising_edge(clk);

    p_comb_2 : process(rst, r2, r3, data_in_shift, ram_data_out, ram_data_out_val)
      variable v : t_reg_3;
    begin
      v := r3;

      v.data_out_shift := r2.data_out_shift;

      if r2.data_out_shift = data_in_shift then
        v.data_out     := ram_data_out;
        v.data_out_val := ram_data_out_val;
      else
        v.data_out     := (others => '0');
        v.data_out_val := '0';
      end if;

      if rst = '1' then
        v := c_reg_3_defaults;
      end if;

      nxt_r3 <= v;
    end process;

    data_out_shift <= r3.data_out_shift;
    data_out       <= r3.data_out;
    data_out_val   <= r3.data_out_val;
  end generate;
end rtl;
