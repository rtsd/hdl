-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Subband Select Reordering.
--
-- Description: For every clock cycle within a frame a different output
--              configuration can be created, based on the available inputs.
--
--              The in_select input defines the mapping of the inputs to the outputs
--              for a single clock cylce.
--
-- Remarks:
-- in_select always has to be defined on the same clock cycle as the in_sosi data.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity reorder_row_select is
  generic (
    g_dsp_data_w    : natural := 16;  -- complex data width, = c_data_w / 2
    g_nof_inputs    : natural := 8;
    g_nof_outputs   : natural := 16;
    g_use_complex   : boolean := true;
    g_pipeline_in   : natural := 1;  -- pipeline in_data
    g_pipeline_in_m : natural := 1;  -- pipeline in_data for M-fold fan out
    g_pipeline_out  : natural := 1  -- pipeline out_data
  );
  port (
    dp_rst              : in  std_logic;
    dp_clk              : in  std_logic;

    -- Streaming
    input_sosi_arr      : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    output_sosi_arr     : out t_dp_sosi_arr(g_nof_outputs - 1 downto 0);
    in_select           : in  std_logic_vector(g_nof_outputs * ceil_log2(g_nof_inputs) - 1 downto 0)
  );
end reorder_row_select;

architecture str of reorder_row_select is
  constant c_tot_pipeline     : natural := g_pipeline_in + g_pipeline_in_m + g_pipeline_out;
  constant c_data_w           : natural := g_dsp_data_w * c_nof_complex;

  type t_dp_sosi_2arr is array (integer range <>) of t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  type reg_type is record
    pipe_sosi_2arr  : t_dp_sosi_2arr(c_tot_pipeline-1 downto 0);
    output_sosi_arr : t_dp_sosi_arr(g_nof_outputs - 1 downto 0);
  end record;

  signal r, rin             : reg_type;

  signal reorder_in_dat     : std_logic_vector(g_nof_inputs * c_data_w - 1 downto 0);
  signal reorder_out_dat    : std_logic_vector(g_nof_outputs * c_data_w - 1 downto 0);
begin
  ---------------------------------------------------------------
  -- PREPARE THE INPUT DATA.
  --
  -- Use a delayed version of the input data to correct for the
  -- delay that is introduced by the read latency of the
  -- selection buffer.
  ---------------------------------------------------------------
  gen_input : for I in g_nof_inputs - 1 downto 0 generate
    use_complex : if g_use_complex generate
      reorder_in_dat((I + 1) * c_data_w - 1 downto I * c_data_w) <=
        input_sosi_arr(I).im(g_dsp_data_w - 1 downto 0) &
        input_sosi_arr(I).re(g_dsp_data_w - 1 downto 0);
    end generate;

    use_data : if not g_use_complex generate
      reorder_in_dat((I + 1) * c_data_w - 1 downto I * c_data_w) <=
        input_sosi_arr(I).data(c_data_w - 1 downto 0);
    end generate;
  end generate;

  ---------------------------------------------------------------
  -- EXECUTE SELECTION
  --
  -- Selection is performed based on the setting of the
  -- in_select signal.
  ---------------------------------------------------------------
  u_reorder : entity common_lib.common_select_m_symbols
  generic map (
    g_nof_input     => g_nof_inputs,
    g_nof_output    => g_nof_outputs,
    g_symbol_w      => c_data_w,
    g_pipeline_in   => g_pipeline_in,
    g_pipeline_in_m => g_pipeline_in_m,
    g_pipeline_out  => g_pipeline_out
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    in_data    => reorder_in_dat,
    in_select  => in_select,
    out_data   => reorder_out_dat
  );

  ---------------------------------------------------------------
  -- REGISTERING AND PIPELINING
  --
  -- This process takes care of registering the incoming SOSI
  -- array and the pipelining for all SOSI control fields.
  -- Also the data-output of the select_m_symbols block is merged
  -- here with the rest of the pipelined SOSI signals.
  ---------------------------------------------------------------
  comb : process(r, input_sosi_arr, reorder_out_dat)
    variable v : reg_type;
    -- Use intermediate variables to avoid too long code lines
    variable v_re : std_logic_vector(g_dsp_data_w - 1 downto 0);
    variable v_im : std_logic_vector(g_dsp_data_w - 1 downto 0);
  begin
    v                      := r;
    v.pipe_sosi_2arr(0)    := input_sosi_arr;
    v.pipe_sosi_2arr(c_tot_pipeline-1 downto 1) := r.pipe_sosi_2arr(c_tot_pipeline-2 downto 0);

    -- Merge data output to the outgoing SOSI record.
    -- Assigning re,im is don't care when g_use_complex is false.
    for I in g_nof_outputs - 1 downto 0 loop
      v_im := reorder_out_dat((I + 1) * c_data_w - 1                downto I * c_data_w + g_dsp_data_w);
      v_re := reorder_out_dat((I + 1) * c_data_w - g_dsp_data_w - 1 downto I * c_data_w);
      v.output_sosi_arr(I)    := r.pipe_sosi_2arr(c_tot_pipeline-1)(0);
      v.output_sosi_arr(I).im := RESIZE_DP_DSP_DATA(v_im);
      v.output_sosi_arr(I).re := RESIZE_DP_DSP_DATA(v_re);
    end loop;

    rin <= v;
  end process comb;

  regs : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      r <= rin;
    end if;
  end process;

  output_sosi_arr <= r.output_sosi_arr;
end str;
