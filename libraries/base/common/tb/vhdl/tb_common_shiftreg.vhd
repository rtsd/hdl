-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, tst_lib, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;

-- Purpose: Test bench for common_shiftreg.vhd
-- Usage:
-- > as 3
-- > run -all
--   proc_common_verify_data self-checks the incrementing output

entity tb_common_shiftreg is
  generic (
    g_pipeline  : natural := 0;
    g_flush_en  : boolean := true;  -- use true to flush shift register when full else only shift at active in_val
    g_nof_dat   : natural := 3;  -- nof dat in the shift register, including in_dat
    g_dat_w     : natural := 8
  );
end tb_common_shiftreg;

architecture tb of tb_common_shiftreg is
  constant clk_period   : time := 10 ns;

  constant c_cnt_w      : natural := ceil_log2(g_nof_dat);
  constant c_repeat     : natural := 5;
  constant c_gap_len    : natural := 50;

  -- Stimuli
  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '1';

  signal in_dat         : std_logic_vector(g_dat_w - 1 downto 0) := (others => '0');
  signal nxt_in_dat     : std_logic_vector(g_dat_w - 1 downto 0);
  signal in_val         : std_logic;
  signal nxt_in_val     : std_logic;
  signal in_sop         : std_logic;
  signal nxt_in_sop     : std_logic;
  signal in_eop         : std_logic;
  signal nxt_in_eop     : std_logic;

  -- DUT output
  signal out_data_vec   : std_logic_vector(g_nof_dat * g_dat_w - 1 downto 0);
  signal out_val_vec    : std_logic_vector(g_nof_dat - 1 downto 0);
  signal out_sop_vec    : std_logic_vector(g_nof_dat - 1 downto 0);
  signal out_eop_vec    : std_logic_vector(g_nof_dat - 1 downto 0);
  signal out_cnt        : std_logic_vector(c_cnt_w - 1 downto 0);

  signal out_dat        : std_logic_vector(g_dat_w - 1 downto 0) := (others => '0');
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_eop        : std_logic;

  -- Verify
  signal verify_en      : std_logic := '0';
  signal ready          : std_logic := '1';
  signal prev_out_dat   : std_logic_vector(g_dat_w - 1 downto 0) := (others => '1');
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      in_dat <= (others => '0');
      in_val <= '0';
      in_sop <= '0';
      in_eop <= '0';
    elsif rising_edge(clk) then
      in_dat <= nxt_in_dat;
      in_val <= nxt_in_val;
      in_sop <= nxt_in_sop;
      in_eop <= nxt_in_eop;
    end if;
  end process;

  nxt_in_dat <= std_logic_vector(signed(in_dat) + 1) when in_val = '1' else in_dat;

  p_stimuli : process
  begin
    nxt_in_val <= '0';
    nxt_in_sop <= '0';
    nxt_in_eop <= '0';
    proc_common_wait_some_cycles(clk, 10);
    verify_en <= '1';

    -- Verify flush
    proc_common_sop(clk, nxt_in_val, nxt_in_sop);
    proc_common_wait_some_cycles(clk, g_nof_dat * 5);
    proc_common_eop_flush(g_nof_dat, clk, nxt_in_val, nxt_in_eop);
    proc_common_wait_some_cycles(clk, c_gap_len);

    -- Verify pulsed valid without flush
    for I in 1 to c_repeat loop
      proc_common_sop(clk, nxt_in_val, nxt_in_sop);
      for J in 0 to 5 * g_nof_dat + I - 1 loop
        proc_common_val_duty(I, I, clk, nxt_in_val);
      end loop;
      proc_common_eop_flush(0, clk, nxt_in_val, nxt_in_eop);
    end loop;
    proc_common_wait_some_cycles(clk, c_gap_len);

    -- Verify pulsed valid with flush
    for I in 1 to c_repeat loop
      proc_common_sop(clk, nxt_in_val, nxt_in_sop);
      for J in 0 to 5 * g_nof_dat + I - 1 loop
        proc_common_val_duty(I, I, clk, nxt_in_val);
      end loop;
      proc_common_eop_flush(g_nof_dat, clk, nxt_in_val, nxt_in_eop);
    end loop;
    proc_common_wait_some_cycles(clk, c_gap_len);

    -- Verify chirped valid with flush
    for I in g_nof_dat - 1 to 2 * g_nof_dat loop
      proc_common_sop(clk, nxt_in_val, nxt_in_sop);
      for J in g_nof_dat - 1 to 2 * g_nof_dat loop
        proc_common_val_duty(I, I, clk, nxt_in_val);
      end loop;
      proc_common_eop_flush(g_nof_dat, clk, nxt_in_val, nxt_in_eop);
    end loop;
    proc_common_wait_some_cycles(clk, c_gap_len);

    -- Verify continue valid with flush
    proc_common_sop(clk, nxt_in_val, nxt_in_sop);
    proc_common_wait_some_cycles(clk, 25);
    proc_common_eop_flush(g_nof_dat, clk, nxt_in_val, nxt_in_eop);

    proc_common_wait_some_cycles(clk, c_gap_len);
    tb_end <= '1';
    wait;
  end process;

  -- DUT
  u_shiftreg : entity work.common_shiftreg
  generic map (
    g_pipeline  => g_pipeline,
    g_flush_en  => g_flush_en,
    g_nof_dat   => g_nof_dat,
    g_dat_w     => g_dat_w
  )
  port map (
    rst          => rst,
    clk          => clk,

    in_dat       => in_dat,
    in_val       => in_val,
    in_sop       => in_sop,
    in_eop       => in_eop,

    out_data_vec => out_data_vec,
    out_val_vec  => out_val_vec,
    out_sop_vec  => out_sop_vec,
    out_eop_vec  => out_eop_vec,
    out_cnt      => out_cnt,

    out_dat      => out_dat,
    out_val      => out_val,
    out_sop      => out_sop,
    out_eop      => out_eop
  );

  -- Verification
  proc_common_verify_data(1, clk, verify_en, ready, out_val, out_dat, prev_out_dat);
end tb;
