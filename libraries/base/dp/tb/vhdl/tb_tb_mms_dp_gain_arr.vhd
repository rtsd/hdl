--------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author: E. Kooistra, 29 mar 2017
-- Purpose: Multi-testbench for mms_dp_gain_arr
-- Description:
--   Verify mms_dp_gain_arr
-- Usage:
--   > as 4
--   > run -all

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;
use technology_lib.technology_select_pkg.all;

entity tb_tb_mms_dp_gain_arr is
end tb_tb_mms_dp_gain_arr;

architecture tb of tb_tb_mms_dp_gain_arr is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_technology             : NATURAL := c_tech_select_default;
  -- g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;   -- always active or random stimuli valid flow control
  -- g_nof_streams            : NATURAL := 1;      -- >= 1
  -- g_complex_data           : BOOLEAN := TRUE;
  -- g_complex_gain           : BOOLEAN := FALSE

  u_rnd_complex_data_complex_gain_3 : entity work.tb_mms_dp_gain_arr generic map (c_tech_select_default, e_random, 3,  true,  true);
  u_rnd_complex_data_complex_gain_1 : entity work.tb_mms_dp_gain_arr generic map (c_tech_select_default, e_random, 1,  true,  true);
  u_rnd_complex_data_real_gain_3    : entity work.tb_mms_dp_gain_arr generic map (c_tech_select_default, e_random, 3,  true,  true);
  u_rnd_complex_data_real_gain_1    : entity work.tb_mms_dp_gain_arr generic map (c_tech_select_default, e_random, 1,  true,  true);
  u_rnd_real_data_real_gain_3       : entity work.tb_mms_dp_gain_arr generic map (c_tech_select_default, e_random, 3, false, false);
  u_rnd_real_data_real_gain_1       : entity work.tb_mms_dp_gain_arr generic map (c_tech_select_default, e_random, 1, false, false);
end tb;
