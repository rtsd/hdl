-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use std.textio.all;
use IEEE.std_logic_textio.all;
use common_lib.common_str_pkg.all;

-- Purpose:
-- . Like dp_sosi_recorder.vhd, but records an array to a file.

entity dp_sosi_arr_recorder is
  generic (
    g_nof_streams    : natural;
    g_record_file    : string := "dp_sosi_arr_recorder.rec";
    g_record_invalid : boolean := true;  -- FALSE = record valid cycles only
    g_wait_for_valid : boolean := true;  -- TRUE  = Don't record anything until valid is high
    g_record_sync    : boolean := true;  -- TRUE  = Write snk_in.sync                    to file, else write "0" to file
    g_record_bsn     : boolean := true;  -- TRUE  = Write snk_in.bsn(g_bsn_w-1 DOWNTO 0) to file, else write "0" to file
    g_record_data    : boolean := true;  -- etc.
    g_record_re      : boolean := true;
    g_record_im      : boolean := true;
    g_record_sop     : boolean := true;
    g_record_eop     : boolean := true;
    g_record_empty   : boolean := true;
    g_record_channel : boolean := true;
    g_record_err     : boolean := true;
    g_bsn_w          : natural := c_dp_stream_bsn_w;
    g_data_w         : natural := c_dp_stream_data_w;
    g_re_w           : natural := c_dp_stream_dsp_data_w;
    g_im_w           : natural := c_dp_stream_dsp_data_w;
    g_empty_w        : natural := c_dp_stream_empty_w;
    g_channel_w      : natural := c_dp_stream_channel_w;
    g_err_w          : natural := c_dp_stream_error_w
  );
  port (
    dp_clk     : in std_logic;
    snk_in_arr : in t_dp_sosi_arr(g_nof_stream - 1 downto 0)
  );
end dp_sosi_arr_recorder;

architecture beh of dp_sosi_arr_recorder is
  constant c_nof_bsn_chars     : natural := sel_a_b(g_record_bsn,     ceil_div(g_bsn_w,     4), 1);
  constant c_nof_data_chars    : natural := sel_a_b(g_record_data,    ceil_div(g_data_w,    4), 1);
  constant c_nof_re_chars      : natural := sel_a_b(g_record_re,      ceil_div(g_re_w,      4), 1);
  constant c_nof_im_chars      : natural := sel_a_b(g_record_im,      ceil_div(g_im_w,      4), 1);
  constant c_nof_empty_chars   : natural := sel_a_b(g_record_empty,   ceil_div(g_empty_w,   4), 1);
  constant c_nof_channel_chars : natural := sel_a_b(g_record_channel, ceil_div(g_channel_w, 4), 1);
  constant c_nof_err_chars     : natural := sel_a_b(g_record_err,     ceil_div(g_err_w,     4), 1);

  signal record_start : boolean := false;
  file   record_file  : TEXT;
begin
  gen_record_start : if g_wait_for_valid = true generate  -- Start recording after valid=1
    p_record_start : process(snk_in.valid)
      variable v_open_status : file_open_status;
    begin
      if snk_in.valid = '1' and record_start = false then
        -- Open the recording file
        file_open(v_open_status, record_file, g_record_file, write_mode);
        -- Start recording
        record_start <= true;
      end if;
    end process;
  end generate;

  no_record_start : if g_wait_for_valid = false generate  -- Start recording immediately
    record_start <= true;
  end generate;

  p_record : process(dp_clk, snk_in)
    variable v_line    : LINE;
    variable v_sync    : string(1 to 1)                   := (others => '?');
    variable v_bsn     : string(1 to c_nof_bsn_chars)     := (others => '?');
    variable v_data    : string(1 to c_nof_data_chars)    := (others => '?');
    variable v_re      : string(1 to c_nof_re_chars)      := (others => '?');
    variable v_im      : string(1 to c_nof_im_chars)      := (others => '?');
    variable v_valid   : string(1 to 1)                   := (others => '?');
    variable v_sop     : string(1 to 1)                   := (others => '?');
    variable v_eop     : string(1 to 1)                   := (others => '?');
    variable v_empty   : string(1 to c_nof_empty_chars)   := (others => '?');
    variable v_channel : string(1 to c_nof_channel_chars) := (others => '?');
    variable v_err     : string(1 to c_nof_err_chars)     := (others => '?');
  begin
    if record_start = true then
      if rising_edge(dp_clk) then

        if snk_in.valid = '1' then
          v_valid := "v";
        else
          v_valid := "i";
        end if;

        if g_record_invalid = true or (g_record_invalid = false and snk_in.valid = '1') then
          if g_record_sync = true then
            if snk_in.sync = '1' then
              v_sync := "S";
            else
              v_sync := ".";
            end if;
          end if;
          if g_record_bsn = true then
            v_bsn := str_to_hex(slv_to_str(snk_in.bsn(g_bsn_w - 1 downto 0)));
          end if;
          if g_record_data = true then
            v_data := str_to_hex(slv_to_str(snk_in.data(g_data_w - 1 downto 0)));
          end if;
          if g_record_re = true then
            v_re := str_to_hex(slv_to_str(snk_in.re(g_re_w - 1 downto 0)));
          end if;
          if g_record_im = true then
            v_im := str_to_hex(slv_to_str(snk_in.im(g_im_w - 1 downto 0)));
          end if;
          if g_record_sop = true then
            if snk_in.sop = '1' then
              v_sop := "s";
            else
              v_sop := ".";
            end if;
          end if;
          if g_record_eop = true then
            if snk_in.eop = '1' then
              v_eop := "e";
            else
              v_eop := ".";
            end if;
          end if;
          if g_record_empty = true then
            v_empty := str_to_hex(slv_to_str(snk_in.empty(g_empty_w - 1 downto 0)));
          end if;
          if g_record_channel = true then
            v_channel := str_to_hex(slv_to_str(snk_in.channel(g_channel_w - 1 downto 0)));
          end if;
          if g_record_err = true then
            v_err := str_to_hex(slv_to_str(snk_in.err(g_err_w - 1 downto 0)));
          end if;

          -- Write all DP stream record fields of this cycle to line
          write(v_line, v_valid & " " & v_sync & " " & v_bsn & " " & v_data & " " & v_re & " " & v_im & " " & v_sop & " " & v_eop & " " & v_empty & " " & v_channel & " " & v_err);

        else
          -- Write only the invalid 'i' of this cycle to line
          write(v_line, v_valid);
        end if;
        -- Write the line to the file
        writeline(record_file, v_line);
      end if;
    end if;
  end process;
end beh;
