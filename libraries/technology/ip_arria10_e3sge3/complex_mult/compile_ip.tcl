#------------------------------------------------------------------------------
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file generated/sim/mentor/msim_setup.tcl
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR

set IP_DIR   "$env(HDL_BUILD_DIR)/sim"

#vlib ./work/         ;# Assume library work already exists

vmap ip_arria10_e3sge3_complex_mult_altmult_complex_151 ./work/

vlog "$IP_DIR/../altmult_complex_151/sim/ip_arria10_e3sge3_complex_mult_altmult_complex_151_s7jvoaq.v" -work ip_arria10_e3sge3_complex_mult_altmult_complex_151
vcom "$IP_DIR/ip_arria10_e3sge3_complex_mult.vhd"                                                                                                       
