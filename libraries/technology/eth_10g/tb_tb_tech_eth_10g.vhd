-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi testbench for tech_eth_10g the 10G Ethernet IP technology wrapper.
-- Description:
-- Usage:
--   > as 5
--   > run -all

library IEEE, technology_lib, tech_pll_lib, tech_mac_10g_lib, common_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_mac_10g_lib.tb_tech_mac_10g_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_tb_tech_eth_10g is
end tb_tb_tech_eth_10g;

architecture tb of tb_tb_tech_eth_10g is
  constant c_644       : time := tech_pll_clk_644_period;
  constant c_156       : time := 6.4 ns;
  constant c_data_type : natural := c_tb_tech_mac_10g_data_type_symbols;

  constant c_tb_end_vec : std_logic_vector(7 downto 0) := (others => '1');
  signal   tb_end_vec   : std_logic_vector(7 downto 0) := c_tb_end_vec;  -- sufficiently long to fit all tb instances
  signal   tb_end       : std_logic := '0';
begin
-- g_technology              : NATURAL := c_tech_select_default;
-- g_tb_end                  : BOOLEAN := TRUE;   -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
-- g_no_dut                  : BOOLEAN := FALSE;  -- default FALSE to verify the DUT, else use TRUE to verify the tb itself without the DUT
-- g_sim_level               : NATURAL := 0;      -- 0 = use IP; 1 = use fast serdes model
-- g_ref_clk_644_period      : TIME := tech_pll_clk_644_period;  -- for 10GBASE-R
-- g_ref_clk_156_period      : TIME := 6.4 ns;                   -- for XAUI
-- g_data_type               : NATURAL := c_tb_tech_mac_10g_data_type_symbols;
-- g_verify_link_recovery    : BOOLEAN := TRUE;
-- g_use_serial_rx_in        : BOOLEAN := FALSE   -- default FALSE when this tb is ran standalone, else use TRUE to simulate a link between two instances of this tb

  u_no_dut       : entity work.tb_tech_eth_10g generic map (c_tech_select_default, false,  true, 0, c_644, c_156, c_data_type, true, false) port map (tb_end_vec(0));
  u_tech_eth_10g : entity work.tb_tech_eth_10g generic map (c_tech_select_default, false, false, 0, c_644, c_156, c_data_type, true, false) port map (tb_end_vec(1));
  u_sim_eth_10g  : entity work.tb_tech_eth_10g generic map (c_tech_select_default, false, false, 1, c_644, c_156, c_data_type, true, false) port map (tb_end_vec(2));

  -- Simulation end control
  tb_end <= '1' when tb_end_vec = c_tb_end_vec else '0';

  proc_common_stop_simulation(tb_end);
  proc_common_timeout_failure(1 ms, tb_end);
end tb;
