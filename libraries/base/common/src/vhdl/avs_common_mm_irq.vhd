-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: AVS wrapper to make a MM slave port with IRQ available as conduit
-- Description:
-- Remark:
-- . Same function as avs_common_mm.vhd, but with the IRQ.
-- . The avs_common_mm_irq_hw.tcl determines the read latency, which is 1.

library IEEE;
use IEEE.std_logic_1164.all;

entity avs_common_mm_irq is
  generic (
    g_adr_w     : natural := 5;
    g_dat_w     : natural := 32
  );
  port (
    -- MM side
    csi_system_reset       : in  std_logic;
    csi_system_clk         : in  std_logic;

    avs_mem_address        : in  std_logic_vector(g_adr_w - 1 downto 0);
    avs_mem_write          : in  std_logic;
    avs_mem_writedata      : in  std_logic_vector(g_dat_w - 1 downto 0);
    avs_mem_read           : in  std_logic;
    avs_mem_readdata       : out std_logic_vector(g_dat_w - 1 downto 0);

    ins_interrupt_irq      : out std_logic;

    -- User side
    coe_reset_export       : out std_logic;
    coe_clk_export         : out std_logic;

    coe_address_export     : out std_logic_vector(g_adr_w - 1 downto 0);
    coe_write_export       : out std_logic;
    coe_writedata_export   : out std_logic_vector(g_dat_w - 1 downto 0);
    coe_read_export        : out std_logic;
    coe_readdata_export    : in  std_logic_vector(g_dat_w - 1 downto 0);

    coe_irq_export         : in  std_logic
  );
end avs_common_mm_irq;

architecture wrap of avs_common_mm_irq is
begin
  -- wires
  coe_reset_export     <= csi_system_reset;
  coe_clk_export       <= csi_system_clk;

  coe_address_export   <= avs_mem_address;
  coe_write_export     <= avs_mem_write;
  coe_writedata_export <= avs_mem_writedata;
  coe_read_export      <= avs_mem_read;
  avs_mem_readdata     <= coe_readdata_export;

  ins_interrupt_irq    <= coe_irq_export;  -- can not use coe_interrupt_export as name, because *_interrupt_* is already the MM side name
end wrap;
