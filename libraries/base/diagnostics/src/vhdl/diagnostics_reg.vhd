-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, diag_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use diag_lib.diag_pkg.all;

entity diagnostics_reg is
  generic (
    g_nof_streams        : natural;
    g_separate_clk       : boolean := false  -- Use separate SRC and SNK clock domains
 );
  port (
    -- Clocks and reset
    mm_rst               : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk               : in  std_logic;  -- memory-mapped bus clock

    st_rst               : in  std_logic := '0';  -- reset synchronous with st_clk
    st_clk               : in  std_logic := '0';  -- other clock domain clock

    src_clk              : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
    src_rst              : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');

    snk_clk              : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
    snk_rst              : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');

    -- Memory Mapped Slave in mm_clk domain
    sla_in               : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out              : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    st_src_en            : out std_logic_vector(g_nof_streams - 1 downto 0);
    st_src_md            : out std_logic_vector(g_nof_streams - 1 downto 0);
    st_src_cnt           : in  t_slv_32_arr(g_nof_streams - 1 downto 0);
    st_src_cnt_clr_evt   : out std_logic_vector(g_nof_streams - 1 downto 0);

    st_snk_en            : out std_logic_vector(g_nof_streams - 1 downto 0);
    st_snk_md            : out std_logic_vector(g_nof_streams - 1 downto 0);
    st_snk_cnt           : in  t_slv_32_arr(g_nof_streams - 1 downto 0);
    st_snk_cnt_clr_evt   : out std_logic_vector(g_nof_streams - 1 downto 0);
    st_snk_diag_val      : in  std_logic_vector(g_nof_streams - 1 downto 0);
    st_snk_diag_res      : in  std_logic_vector(g_nof_streams - 1 downto 0)
   );
end diagnostics_reg;

architecture rtl of diagnostics_reg is
  constant c_nof_registers       : natural := 40;  -- src_cnt and snk_cnt registers should be variable in size....but the CASE process makes that difficult.

  constant c_mm_reg              : t_c_mem := (latency  => 1,
                                            adr_w    => ceil_log2(c_nof_registers),
                                            dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                            nof_dat  => c_nof_registers,
                                            init_sl  => '0');

  -- Registers in mm_clk domain
  signal mm_src_en            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mm_src_md            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mm_src_cnt           : t_slv_32_arr(16 - 1 downto 0);  -- g_nof_streams not used as we must support 16 (fixed number of regs)
  signal mm_src_cnt_clr_evt   : std_logic_vector(g_nof_streams - 1 downto 0);

  signal mm_snk_en            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mm_snk_md            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mm_snk_cnt           : t_slv_32_arr(16 - 1 downto 0);  -- g_nof_streams not used as we must support 16 (fixed number of regs)

  signal mm_snk_cnt_clr_evt   : std_logic_vector(g_nof_streams - 1 downto 0);

  signal mm_snk_diag_val      : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mm_snk_diag_res      : std_logic_vector(g_nof_streams - 1 downto 0);

  signal i_src_clk            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal i_src_rst            : std_logic_vector(g_nof_streams - 1 downto 0);

  signal i_snk_clk            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal i_snk_rst            : std_logic_vector(g_nof_streams - 1 downto 0);
begin
  gen_sep_clk : if g_separate_clk = true generate
    i_src_clk <= src_clk;
    i_src_rst <= src_rst;

    i_snk_clk <= snk_clk;
    i_snk_rst <= snk_rst;
  end generate;

  gen_one_clk : if g_separate_clk = false generate
    gen_nof_streams : for i in 0 to g_nof_streams - 1 generate
      i_src_clk(i) <= st_clk;
      i_src_rst(i) <= st_rst;

      i_snk_clk(i) <= st_clk;
      i_snk_rst(i) <= st_rst;
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out   <= c_mem_miso_rst;
      -- Write access, register values
      mm_src_en <= (others => '0');
      mm_src_md <= (others => '0');
      mm_snk_en <= (others => '0');
      mm_snk_md <= (others => '0');
    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write event defaults
      mm_src_cnt_clr_evt <= (others => '0');
      mm_snk_cnt_clr_evt <= (others => '0');

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Write Block Sync
          when 0 =>
            mm_src_en                  <= sla_in.wrdata(g_nof_streams - 1 downto 0);
          when 1 =>
            mm_src_md                  <= sla_in.wrdata(g_nof_streams - 1 downto 0);
          when 2 =>
            mm_src_cnt_clr_evt         <= sla_in.wrdata(g_nof_streams - 1 downto 0);
          when 19 =>
            mm_snk_en                  <= sla_in.wrdata(g_nof_streams - 1 downto 0);
          when 20 =>
            mm_snk_md                  <= sla_in.wrdata(g_nof_streams - 1 downto 0);
          when 21 =>
            mm_snk_cnt_clr_evt         <= sla_in.wrdata(g_nof_streams - 1 downto 0);
          when others => null;  -- not used MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 3 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(0);  -- 16 src/snk counts supported for now.
          when 4 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(1);
          when 5 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(2);
          when 6 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(3);
          when 7 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(4);
          when 8 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(5);
          when 9 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(6);
          when 10 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(7);
          when 11 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(8);
          when 12 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(9);
          when 13 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(10);
          when 14 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(11);
          when 15 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(12);
          when 16 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(13);
          when 17 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(14);
          when 18 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_src_cnt(15);

          when 22 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(0);
          when 23 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(1);
          when 24 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(2);
          when 25 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(3);
          when 26 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(4);
          when 27 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(5);
          when 28 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(6);
          when 29 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(7);
          when 30 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(8);
          when 31 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(9);
          when 32 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(10);
          when 33 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(11);
          when 34 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(12);
          when 35 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(13);
          when 36 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(14);
          when 37 =>
            sla_out.rddata(c_word_w - 1 downto 0) <= mm_snk_cnt(15);

          when 38 =>
            sla_out.rddata(g_nof_streams - 1 downto 0) <= mm_snk_diag_val;
          when 39 =>
            sla_out.rddata(g_nof_streams - 1 downto 0) <= mm_snk_diag_res;

          when others => null;  -- not used MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and st_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the safe side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  gen_g_nof_streams_asyncs : for i in 0 to g_nof_streams - 1 generate
    u_async_src_en : entity common_lib.common_async
      generic map (
        g_rst_level => '0'
      )
      port map (
        rst  => i_src_rst(i),
        clk  => i_src_clk(i),
        din  => mm_src_en(i),
        dout => st_src_en(i)
      );

    u_async_snk_en : entity common_lib.common_async
      generic map (
        g_rst_level => '0'
      )
      port map (
        rst  => i_snk_rst(i),
        clk  => i_snk_clk(i),
        din  => mm_snk_en(i),
        dout => st_snk_en(i)
      );

    u_async_src_md : entity common_lib.common_async
      generic map (
        g_rst_level => '0'
      )
      port map (
        rst  => i_src_rst(i),
        clk  => i_src_clk(i),
        din  => mm_src_md(i),
        dout => st_src_md(i)
      );

    u_async_snk_md : entity common_lib.common_async
      generic map (
        g_rst_level => '0'
      )
      port map (
        rst  => i_snk_rst(i),
        clk  => i_snk_clk(i),
        din  => mm_snk_md(i),
        dout => st_snk_md(i)
      );

    u_cross_domain_src_cnt : entity common_lib.common_reg_cross_domain
      port map (
        in_rst      => i_src_rst(i),
        in_clk      => i_src_clk(i),
        in_dat      => st_src_cnt(i),
        in_done     => OPEN,
        out_rst     => mm_rst,
        out_clk     => mm_clk,
        out_dat     => mm_src_cnt(i),
        out_new     => open
      );

    u_spulse_src_cnt_clr_evt : entity common_lib.common_spulse
      port map (
        in_rst    => mm_rst,
        in_clk    => mm_clk,
        in_pulse  => mm_src_cnt_clr_evt(i),
        in_busy   => OPEN,
        out_rst   => i_src_rst(i),
        out_clk   => i_src_clk(i),
        out_pulse => st_src_cnt_clr_evt(i)
      );

    u_cross_domain_snk_cnt : entity common_lib.common_reg_cross_domain
      port map (
        in_rst      => i_snk_rst(i),
        in_clk      => i_snk_clk(i),
        in_dat      => st_snk_cnt(i),
        in_done     => OPEN,
        out_rst     => mm_rst,
        out_clk     => mm_clk,
        out_dat     => mm_snk_cnt(i),
        out_new     => open
      );

    u_spulse_snk_cnt_clr_evt : entity common_lib.common_spulse
      port map (
        in_rst    => mm_rst,
        in_clk    => mm_clk,
        in_pulse  => mm_snk_cnt_clr_evt(i),
        in_busy   => OPEN,
        out_rst   => i_snk_rst(i),
        out_clk   => i_snk_clk(i),
        out_pulse => st_snk_cnt_clr_evt(i)
      );

    -- Diag_res and related signals are polled after a certain nof words has been
    -- received - data will be stable.

--    mm_snk_diag_val(i)     <= st_snk_diag_val(i);
--    mm_snk_diag_res(i)     <= st_snk_diag_res(i);
  end generate;

  u_cross_domain_snk_diag_val : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => i_src_rst(0),
      in_clk      => i_src_clk(0),
      in_dat      => st_snk_diag_val,
      in_done     => OPEN,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_snk_diag_val,
      out_new     => open
    );

  u_cross_domain_snk_diag_res : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => i_src_rst(0),
      in_clk      => i_src_clk(0),
      in_dat      => st_snk_diag_res,
      in_done     => OPEN,
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_snk_diag_res,
      out_new     => open
    );
end rtl;
