-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author : R. vd Walle
-- Purpose : Array wrapper for mmp_dp_bsn_sync_scheduler.vhd
-- Description: This component is wrapper that uses mmp_dp_bsn_sync_scheduler.vhd
-- with input 0 to determine the streaming control (sync, sop, eop, valid). So
-- it is assumed that all inputs in in_sosi_arr have identical control signals.
--
-- Remarks: See mmp_dp_bsn_sync_scheduler.vhd

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity mmp_dp_bsn_sync_scheduler_arr is
  generic (
    g_nof_streams            : positive := 1;
    g_bsn_w                  : natural  := c_dp_stream_bsn_w;
    g_block_size             : natural  := 256;  -- = number of data valid per BSN block, must be >= 2
    g_ctrl_interval_size_min : natural  := 1  -- Minimum interval size to use if MM write interval size is set too small.
  );
  port (
    -- Clocks and reset
    mm_rst          : in  std_logic;
    mm_clk          : in  std_logic;
    dp_rst          : in  std_logic;
    dp_clk          : in  std_logic;

    -- MM control
    reg_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_miso        : out t_mem_miso;
    reg_ctrl_interval_size : out natural;

    -- Streaming
    in_sosi_arr        : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    out_sosi_arr       : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    out_start          : out std_logic;
    out_start_interval : out std_logic;
    out_enable         : out std_logic
  );
end mmp_dp_bsn_sync_scheduler_arr;

architecture str of mmp_dp_bsn_sync_scheduler_arr is
  constant c_pipeline : natural := 1;

  signal single_src_out : t_dp_sosi;

  signal in_sosi_arr_piped : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
begin
  -- dp_bsn_sync_scheduler
  u_mmp_dp_bsn_sync_scheduler : entity work.mmp_dp_bsn_sync_scheduler
  generic map (
    g_bsn_w                  => g_bsn_w,
    g_block_size             => g_block_size,
    g_ctrl_interval_size_min => g_ctrl_interval_size_min
  )
  port map (
    dp_rst   => dp_rst,
    dp_clk   => dp_clk,
    mm_rst   => mm_rst,
    mm_clk   => mm_clk,

    reg_mosi => reg_mosi,
    reg_miso => reg_miso,
    reg_ctrl_interval_size => reg_ctrl_interval_size,

    in_sosi  => in_sosi_arr(0),
    out_sosi => single_src_out,

    out_start          => out_start,
    out_start_interval => out_start_interval,
    out_enable         => out_enable
  );

  -- Pipeline in_sosi_arr to compensate for the latency in mmp_dp_bsn_sync_scheduler
  u_dp_pipeline_arr : entity work.dp_pipeline_arr
  generic map (
    g_nof_streams => g_nof_streams,
    g_pipeline    => c_pipeline
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,
    -- ST sink
    snk_in_arr  => in_sosi_arr,
    -- ST source
    src_out_arr => in_sosi_arr_piped
  );

  p_streams : process(in_sosi_arr_piped, single_src_out)
  begin
    out_sosi_arr <= in_sosi_arr_piped;
    for I in 0 to g_nof_streams - 1 loop
      out_sosi_arr(I).sop   <= single_src_out.sop;
      out_sosi_arr(I).eop   <= single_src_out.eop;
      out_sosi_arr(I).valid <= single_src_out.valid;
      out_sosi_arr(I).sync  <= single_src_out.sync;
    end loop;
  end process;
end str;
