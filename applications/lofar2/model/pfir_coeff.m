%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: W. Lubberhuizen, 2005
%         E. Kooistra, 2016
%
% pfir_coeff - compute polyphase FIR filter coefficients
%   Computes the coefficients of the prototype FIR filter for a polyphase
%   filterbank with N fft channels, and subfilter length L and channel
%   bandwidth BWchan. Typically BWchan = 1/N or somewhat more have -3dB
%   reponse between channels.
%
%   The FIR coefficients can be caluculated with Matlab 'fir1' or with
%   Matlab 'fircls1' dependend on config.design.
%
%   For Matlab fircls1() r_pass and r_stop are the maximum passband and
%   stop band deviations, respectively.
%   For Matlab fircls1() and Q>1, the filter coefficients are computed by
%   interpolating a shorter filter with an upsampling factor Q. This
%   reduces computation time substantially. N, L and Q must be powers of 2.
%
%   The FIR coefficients are normalized to fit in range +-1. Given that
%   the impulse response has a sync pulse shape this implies that the
%   largest coefficient is h_fir_max = 1. Most energy is passed on via
%   the center tap and therefore the filter DC gain per poly phase
%   h_fir_dc_gain is close to 1. The quantization maps h_fir_max to
%   optional input argument coeff_q_max which defaults to 2^(nof_bits-1)-1.
%
%   For nof_bits>1, the FIR filter coefficients are quantized with
%   nof_bits bits.
%
%   The returned value is a coefficients vector of length NL, which also
%   represents the impulse response of prototype FIR filter.
%
%   The run_pfir_coeff.m shows how this pfir_coeff.m can be used.
function coeff = pfir_coeff(N, L, BWchan, r_pass, r_stop, nof_bits, config, coeff_q_max)

% Default options
if ~exist('coeff_q_max', 'var'); coeff_q_max = 2^(nof_bits-1)-1; end;  % the quantization maps h_fir_max to coeff_q_max

% requested filter length
M2=N*L;
if strcmp(config.design, 'fir1')
    Q = 1;
elseif M2<=1024
    Q = 1;
else
    Q = L;  % use interpolation to speed up calculation
    %Q = 1;  % no interpolation
end
% initial filter length
M1=N*L/Q;

% pass bandwidth, w_pb, is the normalized cutoff frequency in the range between 0 and 1 (where 1 corresponds to the
% Nyquist frequency (as defined in fircls1)
w_pb = Q * BWchan;

% compute initial filter
if strcmp(config.design, 'fir1')
    h_comp = fir1(M1-1, w_pb);
    disp(sprintf('pfir_coeff : FIR coefficients calculated using Matlab fir1.\n'))
else
    if strcmp(config.design_flag, '')
        h_comp = fircls1(M1-1, w_pb, r_pass, r_stop);
    else
        h_comp = fircls1(M1-1, w_pb, r_pass, r_stop, config.design_flag);
    end
    disp(sprintf(['pfir_coeff : FIR coefficients calculated using Matlab fircls1.\n', ...
                  'pfir_coeff : . pass band deviation = %e\n', ...
                  'pfir_coeff : . stop band deviation = %e\n'], ...
                  r_pass, r_stop))
end

if Q<=1
    h_fir = h_comp;
    disp(sprintf('pfir_coeff : FIR coefficients calculated directly with no interpolation.\n'))
else
    % 1a) Use Matlab fourier interpolation method
    h_interpft = interpft(h_comp,length(h_comp)*Q);

    % 1b) Use DIY fourier interpolation method
    f1=fft(h_comp);
    f2=zeros(1, M2);
    % copy the lower frequency half.
    n=0:M1/2;
    f2(1+n)=f1(1+n);
    % to make the impulse response symmetric in time,
    % we need to apply a small phase correction,
    % corresponding to a shift in the time domain.
    f2(1+n)=f2(1+n).*exp(-sqrt(-1)*(Q-1)*pi*n/M2);
    % recreate the upper part of the spectrum from the lower part
    f2(M2-n)=conj(f2(2+n));
    % back to time domain
    h_fourier = real(ifft(f2));

    % 2) Use resample interpolation method
    h_resample = resample(h_comp, Q, 1);

    % select FIR coefficients from either interpolation method (all are good, but resample causes peek gratings in stopband when N>64)
    if strcmp(config.interpolate, 'resample')
        h_fir = h_resample;
        disp(sprintf('pfir_coeff : FIR coefficients calculated with resample interpolation.\n'));
    elseif strcmp(config.interpolate, 'fourier')
        h_fir = h_fourier;
        disp(sprintf('pfir_coeff : FIR coefficients calculated with DIY Fourier interpolation.\n'));
    else
        h_fir = h_interpft;
        disp(sprintf('pfir_coeff : FIR coefficients calculated with Matlab Fourier interpolation.\n'));
    end
end;

% normalize the coefficients to fit in range +-1
disp(sprintf('pfir_coeff : Normalizing the PFIR coefficients to fit in range +-1.'));
h_fir = h_fir / max(h_fir);
h_fir_max = max(h_fir);  % note: is 1 due to the normalization
h_fir_min = min(h_fir);
h_fir_dc_gain = sum(h_fir) / N;
disp(sprintf('             . Maximum coefficient value : %f', h_fir_max));
disp(sprintf('             . Minimum coefficient value : %f', h_fir_min));
disp(sprintf('             . DC gain per polyphase     : %f\n', h_fir_dc_gain));

% quantize the coeffients
if nof_bits>0
    disp(sprintf('pfir_coeff : Quantizing FIR coefficients nof_bits = %d\n', nof_bits));
    h_fir = quantize(h_fir, h_fir_max, nof_bits, 'half_away', 'clip', coeff_q_max);  % note: h_fir_max is 1 due to the normalization
end;

% output FIR coefficient
coeff = h_fir;
