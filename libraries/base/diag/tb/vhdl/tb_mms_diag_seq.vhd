--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Test bench for mms_diag_tx_seq --> mms_diag_rx_seq
--   The tb is self-stopping and self-checking.
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.diag_pkg.all;
use work.tb_diag_pkg.all;

entity tb_mms_diag_seq is
  generic (
    -- general
    g_flow_control_verify  : t_dp_flow_control_enum := e_random;  -- always active or random flow control
    -- specific
    g_nof_streams          : natural := 1;
    g_use_steps            : boolean := false;  -- when TRUE this tb can only verify +1 increment (ie. using c_rx_steps_arr_ones)
    g_mm_broadcast_tx      : boolean := false;
    g_data_w               : natural := 40;  -- >= g_seq_dat_w
    g_seq_dat_w            : natural := 16
  );
end entity tb_mms_diag_seq;

architecture str of tb_mms_diag_seq is
  constant mm_clk_period          : time := 8 ns;  -- 125 MHz
  constant dp_clk_period          : time := 5 ns;  -- 200 MHz
  constant c_random_w             : natural := 16;
  constant c_rx_steps_arr_ones    : t_integer_arr(c_diag_seq_rx_reg_nof_steps - 1 downto 0) := (1, 1, 1, 1);
  constant c_rx_steps_arr_access  : t_integer_arr(c_diag_seq_rx_reg_nof_steps - 1 downto 0) := (1, -2, -3, 4);

  signal random                   : std_logic_vector(c_random_w - 1 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal ready                    : std_logic;

  signal tb_end                   : std_logic := '0';
  signal tb_mode                  : t_tb_diag_seq_mode_enum := s_off;
  signal tb_mode_multi            : t_tb_diag_seq_mode_enum := s_off;
  signal tb_verify                : std_logic := '0';
  signal mm_rst                   : std_logic;
  signal mm_clk                   : std_logic := '0';
  signal dp_rst                   : std_logic := '1';
  signal dp_clk                   : std_logic := '0';

  signal reg_tx_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal reg_tx_miso              : t_mem_miso;

  signal reg_rx_mosi              : t_mem_mosi := c_mem_mosi_rst;
  signal reg_rx_miso              : t_mem_miso;

  signal rd_reg_arr               : t_diag_seq_mm_reg_arr(g_nof_streams downto 0);  -- use +1 to support g_nof_streams=1, 2 in p_stimuli_and_verify

  signal tx_src_out_arr           : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal tx_src_in_arr            : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  signal stream_index             : natural;
  signal force_low_error          : std_logic;
  signal force_replicate_error    : std_logic;

  signal rx_snk_in_arr            : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
begin
  mm_rst <= '1', '0' after mm_clk_period * 5;
  dp_rst <= mm_rst when rising_edge(dp_clk);

  mm_clk <= not mm_clk or tb_end after mm_clk_period / 2;
  dp_clk <= not dp_clk or tb_end after dp_clk_period / 2;

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  random <= func_common_random(random) when rising_edge(dp_clk);

  ready <= '1'                 when g_flow_control_verify = e_active  else
           random(random'high) when g_flow_control_verify = e_random  else
           '1';

  tx_src_in_arr <= func_dp_stream_arr_set(tx_src_in_arr, ready, "READY");

  ------------------------------------------------------------------------------
  -- Stimuli
  ------------------------------------------------------------------------------

  p_stimuli_and_verify : process
    -- Cannot use non static rd_reg_arr(I) variable index in procedure argument, therefor use constant index rd_reg_arr(c_st_0) or rd_reg_arr(c_st_1).
    constant c_st_0 : natural := 0;
    constant c_st_1 : natural := 1;
  begin
    stream_index <= 0;  -- default verify stream 0
    force_low_error       <= '0';
    force_replicate_error <= '0';
    tb_mode <= s_off;
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    -------------------------------------------------------------------------
    -- Verify MM access for write and readback rx_steps
    -------------------------------------------------------------------------
    -- . write some arbitrary integer values
    proc_diag_seq_rx_write_steps(c_st_0, c_rx_steps_arr_access, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
    for I in 0 to c_diag_seq_rx_reg_nof_steps - 1 loop
      assert rd_reg_arr(c_st_0).rx_steps(I) = c_rx_steps_arr_access(I)
        report "Wrong diag rx_steps read back."
        severity ERROR;
    end loop;

    -- . write default increment +1 to have same behaviour as with g_use_steps=FALSE and diag_sel = "CNTR"
    proc_diag_seq_rx_write_steps(c_st_0, c_rx_steps_arr_ones, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
    for I in 0 to c_diag_seq_rx_reg_nof_steps - 1 loop
      assert rd_reg_arr(c_st_0).rx_steps(I) = c_rx_steps_arr_ones(I)
        report "Wrong diag rx_steps read back."
        severity ERROR;
    end loop;

    -------------------------------------------------------------------------
    -- Verify s_off
    -------------------------------------------------------------------------
    proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));
    if g_nof_streams > 1 then
      stream_index <= 1;  -- try stream 1
      proc_diag_seq_verify(c_st_1, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode_multi, tb_verify, rd_reg_arr(c_st_1));
      stream_index <= 0;  -- back to stream 0
    end if;

    -------------------------------------------------------------------------
    -- Verify Tx and Rx on and both with the same pattern
    -------------------------------------------------------------------------
    tb_mode <= s_expect_ok;
    proc_diag_seq_tx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
    proc_diag_seq_rx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
    proc_diag_seq_tx_enable(c_st_0, "CNTR", 17, 0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
    proc_diag_seq_rx_enable(c_st_0, "CNTR", mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));

    -- Run test and read and verify Rx status
    proc_common_wait_some_cycles(mm_clk, 200);
    proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));

    -- Check that there is valid data
    if g_flow_control_verify = e_active then
      assert rx_snk_in_arr(0).valid = '1'
        report "Wrong diag result: no valid data at rx_snk_in_arr(0)."
        severity ERROR;
    end if;

    -------------------------------------------------------------------------
    -- Verify Tx and Rx on but with different pattern
    -------------------------------------------------------------------------
    tb_mode <= s_expect_error;
    proc_diag_seq_tx_enable(c_st_0, "PSRG", 17, 0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));

    -- Run test and read and verify Rx status
    proc_common_wait_some_cycles(mm_clk, 200);
    proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));

    -------------------------------------------------------------------------
    -- Verify Rx off
    -------------------------------------------------------------------------
    tb_mode <= s_expect_no_result;
    proc_diag_seq_rx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));

    -- Run test and read and verify Rx status
    proc_common_wait_some_cycles(mm_clk, 200);
    proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));

    if g_mm_broadcast_tx = false then
      -------------------------------------------------------------------------
      -- Verify Tx and Rx on with error in sequence low part
      -------------------------------------------------------------------------
      tb_mode <= s_expect_ok;
      proc_diag_seq_tx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
      proc_diag_seq_rx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
      proc_diag_seq_tx_enable(c_st_0, "CNTR", 17, 0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
      proc_diag_seq_rx_enable(c_st_0, "CNTR", mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));

      -- Run test and read and verify Rx status
      proc_common_wait_some_cycles(mm_clk, 200);
      proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));

      tb_mode <= s_expect_error;
      proc_common_wait_some_cycles(dp_clk, 1);
      force_low_error <= '1';
      proc_common_wait_some_cycles(dp_clk, c_random_w);  -- sufficently long to affect a valid data in case g_flow_control_verify=e_random
      force_low_error <= '0';
      proc_common_wait_some_cycles(mm_clk, 200);
      proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));
    end if;

    if g_mm_broadcast_tx = false and g_data_w > g_seq_dat_w then
      -------------------------------------------------------------------------
      -- Verify Tx and Rx on with error in sequence replicate part
      -------------------------------------------------------------------------
      tb_mode <= s_expect_ok;
      proc_diag_seq_tx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
      proc_diag_seq_rx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
      proc_diag_seq_tx_enable(c_st_0, "CNTR", 17, 0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
      proc_diag_seq_rx_enable(c_st_0, "CNTR", mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));

      -- Run test and read and verify Rx status
      proc_common_wait_some_cycles(mm_clk, 200);
      proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));

      tb_mode <= s_expect_error;
      proc_common_wait_some_cycles(dp_clk, 1);
      force_replicate_error <= '1';
      proc_common_wait_some_cycles(dp_clk, c_random_w);  -- sufficently long to affect a valid data in case g_flow_control_verify=e_random
      force_replicate_error <= '0';
      proc_common_wait_some_cycles(mm_clk, 200);
      proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));
    end if;

    if g_mm_broadcast_tx = false and g_nof_streams > 1 then
      stream_index <= 1;  -- try stream 1
      -------------------------------------------------------------------------
      -- Verify Tx and Rx on and both with the same pattern for stream 1
      -------------------------------------------------------------------------
      proc_diag_seq_tx_disable(c_st_1, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_1));
      proc_diag_seq_rx_disable(c_st_1, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_1));
      proc_diag_seq_tx_enable(c_st_1, "CNTR", 17, 0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_1));
      proc_diag_seq_rx_enable(c_st_1, "CNTR", mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_1));

      -- Run test and read and verify Rx status
      proc_common_wait_some_cycles(mm_clk, 200);
      tb_mode_multi <= s_expect_ok;
      proc_diag_seq_verify(c_st_1, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode_multi, tb_verify, rd_reg_arr(c_st_1));

      -- Check that there is valid data
      if g_flow_control_verify = e_active then
        assert rx_snk_in_arr(1).valid = '1'
          report "Wrong diag result: no valid data at rx_snk_in_arr(1)."
          severity ERROR;
      end if;
      stream_index <= 0;  -- back to stream 0
    end if;

    -------------------------------------------------------------------------
    -- Both off
    -------------------------------------------------------------------------
    tb_mode <= s_off;
    proc_diag_seq_tx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
    proc_diag_seq_rx_disable(c_st_0, mm_clk, dp_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, rd_reg_arr(c_st_0));
    proc_common_wait_some_cycles(mm_clk, 100);
    proc_diag_seq_verify(c_st_0, mm_clk, reg_tx_miso, reg_tx_mosi, reg_rx_miso, reg_rx_mosi, tb_mode, tb_verify, rd_reg_arr(c_st_0));
    proc_common_wait_some_cycles(mm_clk, 10);
    tb_end <= '1';
    wait;
  end process;

  p_verify_mm_broadcast : process(rx_snk_in_arr)
    variable v_snk_in : t_dp_sosi;
  begin
    if g_mm_broadcast_tx = true and g_nof_streams > 1 then
      v_snk_in := rx_snk_in_arr(0);
      for I in 1 to g_nof_streams - 1 loop
        if v_snk_in /= rx_snk_in_arr(I) then
          report "Wrong diag result: for g_mm_broadcast_tx=TRUE all streams should carry the same data." severity ERROR;
          exit;
        end if;
      end loop;
    end if;
  end process;

  u_mms_diag_tx_seq: entity WORK.mms_diag_tx_seq
  generic map(
    g_mm_broadcast => g_mm_broadcast_tx,
    g_nof_streams  => g_nof_streams,
    g_seq_dat_w    => g_seq_dat_w
  )
  port map(
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    dp_rst         => dp_rst,
    dp_clk         => dp_clk,

    -- MM interface
    reg_mosi       => reg_tx_mosi,
    reg_miso       => reg_tx_miso,

    -- DP streaming interface
    tx_src_out_arr => tx_src_out_arr,
    tx_src_in_arr  => tx_src_in_arr
  );

  u_mms_diag_rx_seq: entity WORK.mms_diag_rx_seq
  generic map(
    g_nof_streams => g_nof_streams,
    g_use_steps   => g_use_steps,
    g_seq_dat_w   => g_seq_dat_w,
    g_data_w      => g_data_w
  )
  port map(
    -- Clocks and reset
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    dp_rst         => dp_rst,
    dp_clk         => dp_clk,

    -- MM interface
    reg_mosi       => reg_rx_mosi,
    reg_miso       => reg_rx_miso,

    -- DP streaming interface
    rx_snk_in_arr  => rx_snk_in_arr
  );

  p_connect : process(tx_src_out_arr, force_low_error, force_replicate_error)
  begin
    -- Default lopoback all streams
    rx_snk_in_arr <= tx_src_out_arr;
    -- Optionally apply errors on stream 0
    if force_low_error = '1' then
      rx_snk_in_arr(0).data(0) <= not tx_src_out_arr(0).data(0);
    end if;
    if force_replicate_error = '1' then
      rx_snk_in_arr(0).data(g_seq_dat_w) <= not tx_src_out_arr(0).data(g_seq_dat_w);
    end if;
  end process;
end architecture str;
