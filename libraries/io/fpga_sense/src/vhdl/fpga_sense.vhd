-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- Description:
--  For temperature calculation see:
--  https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug_alttemp_sense.pdf
--  Which states Temperature = ( (A * C) / 1024 ) - B
--  Where A = 693, B = 265, C = decimal value of tempout[9..0] (unsigned)

library IEEE, common_lib, technology_lib, tech_fpga_temp_sens_lib, tech_fpga_voltage_sens_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;
use technology_lib.technology_pkg.all;
--USE tech_temp_sense_lib.tech_temp_sense_component_pkg.ALL;

entity fpga_sense is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_sim            : boolean;
    g_temp_high      : natural := 85
  );
  port (
    -- MM interface
    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;

    start_sense : in  std_logic;
    temp_alarm  : out std_logic;

    reg_temp_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    reg_temp_miso          : out t_mem_miso;

    reg_voltage_store_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_voltage_store_miso : out t_mem_miso
  );
end fpga_sense;

architecture str of fpga_sense is
  -- constants for the temperature sensor
  constant c_mem_reg_temp_adr_w     : natural := 1;
  constant c_mem_reg_temp_dat_w     : natural := 32;
  constant c_mem_reg_temp_nof_data  : natural := 1;
  constant c_mem_reg_temp_data : t_c_mem := (c_mem_reg_rd_latency, c_mem_reg_temp_adr_w,  c_mem_reg_temp_dat_w,  c_mem_reg_temp_nof_data, 'X');

  -- temp = (693 * adc)/1024 - 265 => adc = (temp + 265)*1024/693
  constant c_temp_high_raw : std_logic_vector(9 downto 0) := TO_UVEC(((g_temp_high + 265) * 1024) / 693, 10);

  -- constants for the voltage sensor
  constant c_mem_reg_voltage_adr_w     : natural := 1;
  constant c_mem_reg_voltage_dat_w     : natural := 32;
  constant c_mem_reg_voltage_nof_data  : natural := 1;
  constant c_mem_reg_voltage_data : t_c_mem := (c_mem_reg_rd_latency, c_mem_reg_voltage_adr_w,  c_mem_reg_voltage_dat_w,  c_mem_reg_voltage_nof_data, 'X');

  signal mm_reg_temp_data : std_logic_vector(c_mem_reg_temp_dat_w - 1 downto 0);
  signal temp_data        : std_logic_vector(9 downto 0);
  signal eoc              : std_logic;
  signal start_sense_mm : std_logic := '0';
  signal start_sense_mm_d1 : std_logic := '0';
  signal start_sense_mm_d2 : std_logic := '0';
  signal controller_csr_write : std_logic := '0';
  signal controller_csr_writedata : std_logic_vector(31 downto 0) := X"00000001";
                                        -- bits 9:8 = "00" select channels 2-7
                                        -- bits 2:1 = "00" select single conversion
                                        -- bit 0 = '1' set the self-clearing run bit
begin
  -- temperature sensor
  temp_alarm <= '1' when (unsigned(temp_data) > unsigned(c_temp_high_raw)) else '0';

  gen_tech_fpga_temp_sens: if g_sim = false generate
    u_tech_fpga_temp_sens : entity tech_fpga_temp_sens_lib.tech_fpga_temp_sens
    generic map (
      g_technology => g_technology
    )
    port map (
      corectl => start_sense,
      eoc     => eoc,  -- : OUT STD_LOGIC;
      reset   => mm_rst,
      tempout => temp_data  -- : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
    );

--  The eoc signal goes high for one clock cycle of the 1-MHz internal oscillator clock,
--  indicating end of conversion. You can latch the data on tempout at the falling edge of eoc.
    process(eoc, mm_rst)
    begin
      if mm_rst = '1' then
        mm_reg_temp_data <= (others => '0');
      elsif falling_edge(eoc) then
        mm_reg_temp_data <= RESIZE_UVEC(temp_data, c_mem_reg_temp_dat_w);
      end if;
    end process;

  end generate;

  no_tech_fpga_temp_sens: if g_sim = true generate
    -- temp = (693 * adc)/1024 - 265 => adc = (temp + 265)*1024/693
    temp_data <= TO_UVEC(458, temp_data'length);  -- choose temp = 45 degrees so adc temp_data = 458
    mm_reg_temp_data <= RESIZE_UVEC(temp_data, c_mem_reg_temp_dat_w);
  end generate;

  u_reg_map : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => false,
    g_in_new_latency     => 0,
    g_readback           => false,
    g_reg                => c_mem_reg_temp_data,
    g_init_reg           => (others => '0')
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => mm_rst,
    st_clk      => mm_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => reg_temp_mosi,
    sla_out     => reg_temp_miso,

    -- MM registers in st_clk domain
    reg_wr_arr  => OPEN,
    reg_rd_arr  => OPEN,
    in_new      => '1',
    in_reg      => mm_reg_temp_data,
    out_reg     => open
  );

  -- voltage sensor
  no_tech_fpga_voltage_sens: if g_sim = true generate
    -- Model the voltage sensor by returning the MM register address of the voltage field
    reg_voltage_store_miso.rddata <= RESIZE_MEM_UDATA(reg_voltage_store_mosi.address(3 downto 0));
  end generate;

  gen_tech_fpga_voltage_sens: if g_sim = false generate
    u_tech_fpga_voltage_sens : entity tech_fpga_voltage_sens_lib.tech_fpga_voltage_sens
      generic map (
        g_technology => g_technology
      )
      port map (
        clock_clk                  => mm_clk,
        reset_sink_reset           => mm_rst,
        controller_csr_address     => '1',
        controller_csr_read        => '0',
        controller_csr_write       => controller_csr_write,
        controller_csr_writedata   => controller_csr_writedata,
        controller_csr_readdata    => open,
        sample_store_csr_address   => reg_voltage_store_mosi.address(3 downto 0),
        sample_store_csr_read      => reg_voltage_store_mosi.rd,
        sample_store_csr_write     => reg_voltage_store_mosi.wr,
        sample_store_csr_writedata => reg_voltage_store_mosi.wrdata(31 downto 0),
        sample_store_csr_readdata  => reg_voltage_store_miso.rddata(31 downto 0),
        sample_store_irq_irq       => open
    );

    process(mm_clk, mm_rst)
      begin
        if mm_rst = '1' then
          controller_csr_write <= '0';
          start_sense_mm       <= '0';
          start_sense_mm_d1    <= '0';
          start_sense_mm_d2    <= '0';
        elsif rising_edge(mm_clk) then
          start_sense_mm <= start_sense;
          start_sense_mm_d1 <= start_sense_mm;
          start_sense_mm_d2 <= start_sense_mm_d1;
          if start_sense_mm_d1 = '1' and start_sense_mm_d2 = '0' then
            controller_csr_write <= '1';
          else
            controller_csr_write <= '0';
          end if;
        end if;
    end process;

  end generate;
end str;
