-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 26 okt 2021
-- Purpose: Multi tb for common_resize.vhd and RESIZE_NUM() in common_pkg.vhd

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_resize is
end tb_tb_resize;

architecture tb of tb_tb_resize is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- Supported for RESIZE_NUM() and common_resize.vhd
  -- g_in_dat_w        : NATURAL := 5;
  -- g_out_dat_w       : NATURAL := 3;
  -- -- Only supported for common_resize.vhd
  -- g_clip            : BOOLEAN := TRUE;
  -- g_clip_symmetric  : BOOLEAN := TRUE;

  u_wrap_extend_w       : entity work.tb_resize generic map (3, 5, false, false);
  u_wrap_keep_w         : entity work.tb_resize generic map (5, 5, false, false);
  u_wrap_reduce_w       : entity work.tb_resize generic map (5, 3, false, false);

  u_clip_extend_w       : entity work.tb_resize generic map (3, 5, true, false);
  u_clip_keep_w         : entity work.tb_resize generic map (5, 5, true, false);
  u_clip_reduce_w       : entity work.tb_resize generic map (5, 3, true, false);

  u_clip_sym_extend_w   : entity work.tb_resize generic map (3, 5, true, true);
  u_clip_sym_keep_w     : entity work.tb_resize generic map (5, 5, true, true);
  u_clip_sym_reduce_w   : entity work.tb_resize generic map (5, 3, true, true);
end tb;
