-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

-- run 50 us

entity tb_dp_fifo_dc_mixed_widths is
  generic (
    g_random_flow   : boolean := false;
    g_narrow_period : time    := 10 ns;
    g_wide_period   : time    := 10 ns;
    g_narrow_w      : natural := 16;
    g_nof_narrow    : natural := 4;
    g_use_ctrl      : boolean := true;
    g_read_rl       : natural := 0
  );
end tb_dp_fifo_dc_mixed_widths;

architecture tb of tb_dp_fifo_dc_mixed_widths is
  constant c_rl                : natural := 1;
  constant c_wr_fifo_size      : natural := 128;  -- use same nof words for both n2w FIFO and w2n FIFO, hence the w2n FIFO is bigger
  constant c_long_frame_len    : natural := c_wr_fifo_size * 5;
  constant c_wide_w            : natural := g_nof_narrow * g_narrow_w;

  constant c_verify_mode       : boolean := sel_a_b(g_use_ctrl, false, true);  -- use mode FALSE to verify per frame

  signal tb_end             : std_logic := '0';
  signal narrow_rst         : std_logic;
  signal narrow_clk         : std_logic := '0';
  signal wide_rst           : std_logic;
  signal wide_clk           : std_logic := '0';
  signal arst               : std_logic;

  signal random_0           : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1           : std_logic_vector(15 downto 0) := (others => '0');

  signal in_en              : std_logic := '1';
  signal in_siso            : t_dp_siso;
  signal in_sosi            : t_dp_sosi := c_dp_sosi_rst;

  signal fifo_n2w_wr_usedw  : std_logic_vector(ceil_log2(c_wr_fifo_size             ) - 1 downto 0);
  signal fifo_n2w_rd_usedw  : std_logic_vector(ceil_log2(c_wr_fifo_size / g_nof_narrow) - 1 downto 0);
  signal fifo_n2w_rd_emp    : std_logic;

  signal wide_siso          : t_dp_siso;
  signal wide_sosi          : t_dp_sosi;

  signal wide_siso_rl1      : t_dp_siso;
  signal wide_sosi_rl1      : t_dp_sosi;

  signal fifo_w2n_wr_usedw  : std_logic_vector(ceil_log2(c_wr_fifo_size             ) - 1 downto 0);
  signal fifo_w2n_rd_usedw  : std_logic_vector(ceil_log2(c_wr_fifo_size * g_nof_narrow) - 1 downto 0);
  signal fifo_w2n_rd_emp    : std_logic;

  signal out_siso           : t_dp_siso := c_dp_siso_rdy;
  signal out_sosi           : t_dp_sosi;

  signal out_en             : std_logic := '1';
  signal test_fifo_afull     : std_logic := '0';

  signal verify_wide_en     : std_logic := '0';
  signal verify_out_en      : std_logic := '0';
  signal verify_done        : std_logic;

  signal prev_wide_ready    : std_logic_vector(0 to c_rl);
  signal wide_data          : std_logic_vector(c_wide_w - 1 downto 0);
  signal prev_wide_data     : std_logic_vector(c_wide_w - 1 downto 0);
  signal wide_gap           : std_logic;

  signal prev_out_ready     : std_logic_vector(0 to c_rl);
  signal out_data           : std_logic_vector(g_narrow_w - 1 downto 0);
  signal prev_out_data      : std_logic_vector(g_narrow_w - 1 downto 0) := TO_SVEC(-1, g_narrow_w);
  signal out_gap            : std_logic;

  signal expected_about     : std_logic_vector(g_narrow_w - 1 downto 0);
  signal expected_exact     : std_logic_vector(g_narrow_w - 1 downto 0);
begin
  narrow_rst  <= '1', '0' after g_narrow_period * 7;
  narrow_clk  <=  not narrow_clk or tb_end after g_narrow_period / 2;

  wide_rst  <= '1', '0' after g_wide_period * 7;
  wide_clk  <=  not wide_clk or tb_end after g_wide_period / 2;

  arst <= narrow_rst or wide_rst;

  random_0 <= func_common_random(random_0) when rising_edge(narrow_clk);
  random_1 <= func_common_random(random_1) when rising_edge(narrow_clk);

  no_random_flow : if g_random_flow = false generate
    in_en          <= '1';
    out_siso.ready <= out_en;
  end generate;

  gen_random_flow : if g_random_flow = true generate
    in_en          <= random_0(random_0'high);
    out_siso.ready <= random_1(random_1'high) and out_en;
  end generate;

  p_test_fifo_afull : process
  begin
    out_en <= '1';
    wait until test_fifo_afull = '1';  -- default enable output ready until the fifo almost full test has to start
    out_en <= '0';  -- disable the output to start the fifo almost full test
    proc_common_wait_some_cycles(narrow_clk, 10);  -- wait some more cycles
    wait until in_siso.ready = '0';  -- when the n2w FIFO is almost full then it will pull its ready low
    proc_common_wait_some_cycles(narrow_clk, 10);  -- wait some more cycles
    out_en <= '1';  -- enable the output again to leave this dead lock state
  end process;

  p_stimuli : process
    variable v_init : natural;
    variable v_len  : natural;
  begin
    test_fifo_afull <= '0';
    verify_done <= '0';
    wait until arst = '0';
    -- ensure that n2w and w2n FIFOs are out of internal reset, and align to narrow_clk
    proc_common_wait_some_cycles(wide_clk, c_tree_delay_len + 10);
    proc_common_wait_some_cycles(narrow_clk, c_tree_delay_len + 10);

    -- Frame data with incrementing data over all frames, so the data can also be used as unframed stimuli
    v_init := 0; v_len := 0;

    -- . try different frame lengths and with different gaps in between
    for I in 0 to 3 loop
      -- . verify per frame can only verify frames that are longer than 1 data word, therefore use v_len >= g_nof_narrow+1
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 1;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 1;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len := 2 * g_nof_narrow;    proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len := 2 * g_nof_narrow;    proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 1;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 2;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 3;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 4;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 5;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 6;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 7;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 8;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 9;  proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      v_init := v_init + v_len;  v_len :=   g_nof_narrow + 10; proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);  proc_common_wait_some_cycles(narrow_clk, I);
      proc_common_wait_some_cycles(narrow_clk, 20);
    end loop;

    -- . try to fill the FIFO (via out_siso.ready control via out_en in process p_test_fifo_afull)
    test_fifo_afull <= '1';
    proc_common_wait_some_cycles(narrow_clk, 20);
    v_init := v_init + v_len;
    v_len := g_nof_narrow + c_long_frame_len;
    proc_dp_gen_frame(c_rl, g_narrow_w, g_narrow_w, v_init, v_len, 0, '0', narrow_clk, in_en, in_siso, in_sosi);
    proc_common_wait_some_cycles(narrow_clk, 20);
    test_fifo_afull <= '0';

    v_init := v_init + v_len;
    expected_exact <= TO_UVEC(v_init - 1, g_narrow_w);  -- used for framed data
    expected_about <= TO_UVEC(v_init - g_nof_narrow, g_narrow_w);  -- for unframed data there may still be some symbols pending in the n2w FIFO, waiting for a wide word to have filled

    wait until fifo_w2n_rd_emp = '1';  -- ensure that FIFOs have been read empty
    proc_common_wait_some_cycles(narrow_clk, 20);
    verify_done <= '1';
    proc_common_wait_some_cycles(narrow_clk, 1);
    verify_done <= '0';

    -- . tb end
    proc_common_wait_some_cycles(narrow_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  -- Narrow to wide FIFO
  u_dut_n2w : entity work.dp_fifo_dc_mixed_widths
  generic map (
    g_wr_data_w    => g_narrow_w,
    g_rd_data_w    => c_wide_w,
    g_use_ctrl     => g_use_ctrl,
    g_wr_fifo_size => c_wr_fifo_size,
    g_rd_fifo_rl   => g_read_rl
  )
  port map (
    wr_rst         => narrow_rst,
    wr_clk         => narrow_clk,
    rd_rst         => wide_rst,
    rd_clk         => wide_clk,
    -- ST sink
    snk_out        => in_siso,
    snk_in         => in_sosi,
    -- Monitor FIFO filling
    wr_usedw       => fifo_n2w_wr_usedw,
    rd_usedw       => fifo_n2w_rd_usedw,
    rd_emp         => fifo_n2w_rd_emp,
    -- ST source
    src_in         => wide_siso,
    src_out        => wide_sosi
  );

  -- Adapt for wide to narrow FIFO input RL=1 in case g_read_rl=0
  u_rl : entity work.dp_latency_adapter
  generic map (
    g_in_latency  => g_read_rl,
    g_out_latency => c_rl
  )
  port map (
    rst       => wide_rst,
    clk       => wide_clk,
    -- ST sink
    snk_out   => wide_siso,
    snk_in    => wide_sosi,
    -- ST source
    src_in    => wide_siso_rl1,
    src_out   => wide_sosi_rl1
  );

  -- Wide to narrow FIFO
  u_dut_w2n : entity work.dp_fifo_dc_mixed_widths
  generic map (
    g_wr_data_w    => c_wide_w,
    g_rd_data_w    => g_narrow_w,
    g_use_ctrl     => g_use_ctrl,
    g_wr_fifo_size => c_wr_fifo_size,
    g_rd_fifo_rl   => g_read_rl
  )
  port map (
    wr_rst         => wide_rst,
    wr_clk         => wide_clk,
    rd_rst         => narrow_rst,
    rd_clk         => narrow_clk,
    -- ST sink
    snk_out        => wide_siso_rl1,
    snk_in         => wide_sosi_rl1,
    -- Monitor FIFO filling
    wr_usedw       => fifo_w2n_wr_usedw,
    rd_usedw       => fifo_w2n_rd_usedw,
    rd_emp         => fifo_w2n_rd_emp,
    -- ST source
    src_in         => out_siso,
    src_out        => out_sosi
  );

  -- 1) Verify intermediate wide data

  -- . Note that verify per frame can only verify frames that are longer than 1 data word
  proc_dp_verify_en(c_verify_mode, wide_clk, wide_sosi.valid, wide_sosi.sop, wide_sosi.eop, verify_wide_en);

  -- . Using proc_dp_verify_symbols() suits c_nof_symbols_per_data >= 1
  wide_data <= wide_sosi.data(c_wide_w - 1 downto 0);
  proc_dp_verify_symbols(g_read_rl, c_wide_w, g_narrow_w, wide_clk, verify_wide_en, wide_siso.ready, wide_sosi.valid, wide_sosi.eop, wide_data, wide_sosi.empty, prev_wide_data);
  proc_dp_verify_valid(g_read_rl, wide_clk, verify_wide_en, wide_siso.ready, prev_wide_ready, wide_sosi.valid);
  proc_dp_verify_gap_invalid(wide_clk, wide_sosi.valid, wide_sosi.sop, wide_sosi.eop, wide_gap);

  -- 2) Verify final narrow data

  -- . Note that verify per frame can only verify frames that are longer than 1 data word
  proc_dp_verify_en(c_verify_mode, narrow_clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, verify_out_en);

  -- . Using proc_dp_verify_data() only suits c_nof_symbols_per_data = 1
  out_data <= out_sosi.data(g_narrow_w - 1 downto 0);
  proc_dp_verify_data("out_sosi.data", g_read_rl, narrow_clk, verify_out_en, out_siso.ready, out_sosi.valid, out_data, prev_out_data);
  proc_dp_verify_valid(g_read_rl, narrow_clk, verify_out_en, out_siso.ready, prev_out_ready, out_sosi.valid);
  proc_dp_verify_gap_invalid(narrow_clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, out_gap);

  -- . Check that the test has ran at all
  proc_dp_verify_value(e_at_least, narrow_clk, verify_done, expected_about, prev_out_data);  -- for unframed data we know at least what to expect

  gen_exact : if g_use_ctrl = true generate
    proc_dp_verify_value(e_equal, narrow_clk, verify_done, expected_exact, prev_out_data);  -- for framed data we know exactly what to expect
  end generate;
end tb;
