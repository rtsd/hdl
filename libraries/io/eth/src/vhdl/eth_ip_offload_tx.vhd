-------------------------------------------------------------------------------
--
-- Copyright 2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose:
-- . concatenate header from hdr_fields_in_arr to incoming sosi stream and
--   calculate + insert ip header checksum (crc).
-- Description:
-- . dp_offload_tx_v3 is used to concatenate the header to the dp sosi stream.
-- . eth_ip_header_checksum is used to calculate and insert the ip header
--   checksum.
library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;

entity eth_ip_offload_tx is
  generic (
    g_nof_streams    : natural;
    g_data_w         : natural;
    g_symbol_w       : natural;
    g_hdr_field_arr  : t_common_field_arr;  -- User defined header fields
    g_hdr_field_sel  : std_logic_vector;  -- For each header field, select the source: 0=data path, 1=MM controlled
    g_pipeline_ready : boolean := false
  );
  port (
    mm_rst               : in  std_logic := '0';
    mm_clk               : in  std_logic := '0';

    dp_rst               : in  std_logic;
    dp_clk               : in  std_logic;

    reg_hdr_dat_copi     : in  t_mem_copi := c_mem_copi_rst;
    reg_hdr_dat_cipo     : out t_mem_cipo;

    snk_in_arr           : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    snk_out_arr          : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    src_out_arr          : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_in_arr           : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    hdr_fields_in_arr    : in  t_slv_1024_arr(g_nof_streams - 1 downto 0);  -- hdr_fields_in_arr(i) is considered valid @ snk_in_arr(i).sop
    hdr_fields_out_arr   : out t_slv_1024_arr(g_nof_streams - 1 downto 0)
  );
end eth_ip_offload_tx;

architecture str of eth_ip_offload_tx is
  signal dp_offload_tx_src_out_arr        : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_offload_tx_src_in_arr         : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal dp_offload_tx_hdr_fields_out_arr : t_slv_1024_arr(g_nof_streams - 1 downto 0);

begin

  u_dp_offload_tx_v3 : entity dp_lib.dp_offload_tx_v3
  generic map (
    g_nof_streams    => g_nof_streams,
    g_data_w         => g_data_w,
    g_symbol_w       => g_symbol_w,
    g_hdr_field_arr  => g_hdr_field_arr,
    g_hdr_field_sel  => g_hdr_field_sel,
    g_pipeline_ready => g_pipeline_ready
  )
  port map (
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,
    reg_hdr_dat_mosi   => reg_hdr_dat_copi,
    reg_hdr_dat_miso   => reg_hdr_dat_cipo,
    snk_in_arr         => snk_in_arr,
    snk_out_arr        => snk_out_arr,
    src_out_arr        => dp_offload_tx_src_out_arr,
    src_in_arr         => dp_offload_tx_src_in_arr,
    hdr_fields_in_arr  => hdr_fields_in_arr,
    hdr_fields_out_arr => dp_offload_tx_hdr_fields_out_arr
  );

  hdr_fields_out_arr <= dp_offload_tx_hdr_fields_out_arr;

  gen_crc : for i in 0 to g_nof_streams - 1 generate
    u_eth_ip_header_checksum : entity work.eth_ip_header_checksum
    generic map (
      g_data_w        => g_data_w,
      g_hdr_field_arr => g_hdr_field_arr
    )
    port map (
      rst               => dp_rst,
      clk               => dp_clk,

      snk_in            => dp_offload_tx_src_out_arr(i),
      snk_out           => dp_offload_tx_src_in_arr(i),

      src_out           => src_out_arr(i),
      src_in            => src_in_arr(i),

      hdr_fields_slv_in => dp_offload_tx_hdr_fields_out_arr(i)
    );
  end generate;
 end str;
