-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra 2009, updated in 2021
-- Purpose: Test bench for common_resize.vhd and for RESIZE_NUM() from
--          common_pkg.vhd
-- Usage:
-- > do wave_resize.do
--   or use:
-- > as 5
--   and manually set the signed data output signals to radix decimal
-- > run -a
-- . Verify clipping using g_clip=TRUE and g_clip_symmetric=TRUE or FALSE
-- . Verify wrapping using g_clip=FALSE
-- . Observe reg_dat with respect to out_sdat, out_sovr for signed
-- . Observe reg_dat with respect to out_udat, out_uovr for unsigned

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_resize is
  generic (
    -- Supported for RESIZE_NUM() and common_resize.vhd
    g_in_dat_w        : natural := 5;
    g_out_dat_w       : natural := 3;
    -- Only supported for common_resize.vhd
    g_clip            : boolean := true;
    g_clip_symmetric  : boolean := true
  );
end tb_resize;

architecture tb of tb_resize is
  constant clk_period   : time    := 10 ns;

  constant c_pipeline_input  : natural := 0;
  constant c_pipeline_output : natural := 1;
  constant c_pipeline        : natural := c_pipeline_input + c_pipeline_output;

  -- Test data
  signal in_val          : std_logic;
  signal in_dat          : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal reg_val         : std_logic;
  signal reg_dat         : std_logic_vector(g_in_dat_w - 1 downto 0);

  -- Signed output data, view as radix decimal in Wave window
  signal reg_sdat        : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal lowrange_sdat   : std_logic_vector(g_out_dat_w - 1 downto 0);  -- keep LSbits or sign extend
  signal resize_num_sdat : std_logic_vector(g_out_dat_w - 1 downto 0);  -- using RESIZE_NUM() from common_pkg.vhd
  signal resize_sdat     : std_logic_vector(g_out_dat_w - 1 downto 0);  -- using RESIZE() from IEEE.NUMERIC_STD
  signal out_sdat        : std_logic_vector(g_out_dat_w - 1 downto 0);  -- using common_resize.vhd
  signal out_sovr        : std_logic;  -- overflow for out_sdat

  -- Unsigned output data, view as radix unsigned in Wave window
  signal reg_udat        : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal lowrange_udat   : std_logic_vector(g_out_dat_w - 1 downto 0);  -- keep LSbits
  signal resize_num_udat : std_logic_vector(g_out_dat_w - 1 downto 0);  -- using RESIZE_NUM() from common_pkg.vhd
  signal resize_udat     : std_logic_vector(g_out_dat_w - 1 downto 0);  -- using RESIZE() from IEEE.NUMERIC_STD
  signal out_udat        : std_logic_vector(g_out_dat_w - 1 downto 0);  -- using common_resize.vhd
  signal out_uovr        : std_logic;  -- overflow for out_udat

  signal tb_end          : std_logic := '0';
  signal clk             : std_logic := '1';
  signal rst             : std_logic;

  constant c_init        : std_logic_vector(in_dat'range) := (others => '0');
  constant g_clip_umax   : natural := 2**g_out_dat_w - 1;
  constant g_clip_smax   : natural := 2**(g_out_dat_w - 1) - 1;
  constant g_clip_smin   : integer := -2**(g_out_dat_w - 1);
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  -- Testbench end
  p_tb_end : process
    variable v_dat : std_logic_vector(in_dat'range);
  begin
    tb_end <= '0';
    wait until in_val = '1';
    wait until rising_edge(clk);
    v_dat := in_dat;  -- keep first in_dat
    wait until rising_edge(clk);
    wait until v_dat = in_dat;  -- wait until all incrementing in_dat values have been applied at least once
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    tb_end <= '1';
    wait;
  end process;

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      in_val      <= '0';
      in_dat      <= c_init;
    elsif rising_edge(clk) then
      in_val      <= '1';
      in_dat      <= std_logic_vector(signed(in_dat) + 1);
    end if;
  end process;

  -- Delay input as much as DUT output, assume c_pipeline = 1
  reg_val <= in_val when rising_edge(clk);
  reg_dat <= in_dat when rising_edge(clk);
  reg_sdat <= reg_dat;
  reg_udat <= reg_dat;

  gen_extend_lowrange : if g_out_dat_w >= g_in_dat_w generate
    p_extend_lowrange : process(clk)
    begin
      if rising_edge(clk) then
        -- Extend MSbits for "SIGNED" and "UNSIGNED"
        lowrange_sdat <= (others => in_dat(g_in_dat_w - 1));  -- Extend MSbit for "SIGNED"
        lowrange_udat <= (others => '0');  -- Extend '0' for "UNSIGNED"
        lowrange_sdat(g_in_dat_w - 1 downto 0) <= in_dat;
        lowrange_udat(g_in_dat_w - 1 downto 0) <= in_dat;
      end if;
    end process;
  end generate;

  gen_reduce_lowrange : if g_out_dat_w < g_in_dat_w generate
    p_reduce_lowrange : process(clk)
    begin
      if rising_edge(clk) then
        -- Remove MSbits for "SIGNED" and "UNSIGNED"
        lowrange_sdat <= in_dat(g_out_dat_w - 1 downto 0);
        lowrange_udat <= in_dat(g_out_dat_w - 1 downto 0);
      end if;
    end process;
  end generate;

  -- IEEE RESIZE() for "SIGNED" and "UNSIGNED"
  resize_sdat <= std_logic_vector(resize(  signed(in_dat), g_out_dat_w)) when rising_edge(clk);
  resize_udat <= std_logic_vector(resize(unsigned(in_dat), g_out_dat_w)) when rising_edge(clk);

  -- RESIZE_NUM() from common_pkg.vhd for "SIGNED" and "UNSIGNED"
  resize_num_sdat <= std_logic_vector(RESIZE_NUM(  signed(in_dat), g_out_dat_w)) when rising_edge(clk);
  resize_num_udat <= std_logic_vector(RESIZE_NUM(unsigned(in_dat), g_out_dat_w)) when rising_edge(clk);

  -- DUT for "SIGNED"
  u_s_resize : entity work.common_resize
  generic map (
    g_representation  => "SIGNED",
    g_clip            => g_clip,
    g_clip_symmetric  => g_clip_symmetric,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_sdat,
    out_ovr        => out_sovr
  );

  -- DUT for "UNSIGNED"
  u_u_resize : entity work.common_resize
  generic map (
    g_representation  => "UNSIGNED",
    g_clip            => g_clip,
    g_clip_symmetric  => g_clip_symmetric,
    g_pipeline_input  => c_pipeline_input,
    g_pipeline_output => c_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk            => clk,
    in_dat         => in_dat,
    out_dat        => out_udat,
    out_ovr        => out_uovr
  );

  -- Verification
  p_verify : process
  begin
    wait until rising_edge(clk);
    if reg_val = '1' then
      if g_in_dat_w <= g_out_dat_w then
        -- For extended width expected value is same as input value
        assert signed( lowrange_sdat) = signed(reg_dat)
          report "Wrong extended lowrange_sdat"
          severity ERROR;
        assert unsigned( lowrange_udat) = unsigned(reg_dat)
          report "Wrong extended lowrange_udat"
          severity ERROR;
        assert signed( resize_sdat) = signed(reg_dat)
          report "Wrong extended resize_sdat"
          severity ERROR;
        assert unsigned( resize_udat) = unsigned(reg_dat)
          report "Wrong extended resize_udat"
          severity ERROR;
        assert signed(resize_num_sdat) = signed(reg_dat)
          report "Wrong extended resize_num_sdat"
          severity ERROR;
        assert unsigned(resize_num_udat) = unsigned(reg_dat)
          report "Wrong extended resize_num_udat"
          severity ERROR;
        assert signed( out_sdat) = signed(reg_dat)
          report "Wrong extended out_sdat"
          severity ERROR;
        assert unsigned( out_udat) = unsigned(reg_dat)
          report "Wrong extended out_udat"
          severity ERROR;
      else
        -- For reduced width compare unsigned with lowrange
        assert unsigned( resize_udat) = unsigned(lowrange_udat)
          report "Wrong wrapped resize_udat"
          severity ERROR;
        assert unsigned(resize_num_udat) = unsigned(lowrange_udat)
          report "Wrong wrapped resize_num_udat"
          severity ERROR;
        assert unsigned(       out_udat) = unsigned(lowrange_udat) or
               unsigned( out_udat) = g_clip_umax
          report "Wrong clipped out_udat"
          severity ERROR;

        -- For reduced width compare signed with lowrange
        -- . no need to verify RESIZE(), because it is part of IEEE.NUMERIC_STD
        -- . verify RESIZE_NUM() below for all g_out_dat_w
        -- . verify common_resize here
        if g_clip then
          if g_clip_symmetric then
            assert (signed(out_sdat) = signed(lowrange_sdat) or signed(out_sdat) = -g_clip_smax or signed(out_sdat) = g_clip_smax) and
                   signed(out_sdat) /=  g_clip_smin and
                   signed(out_sdat) /= -g_clip_smin
              report "Wrong clipped symmetrical out_sdat"
              severity ERROR;
          else
            assert (signed(out_sdat) = signed(lowrange_sdat) or signed(out_sdat) = g_clip_smin or signed(out_sdat) = g_clip_smax)
              report "Wrong clipped out_sdat"
              severity ERROR;
          end if;
        else
          assert signed(out_sdat) = signed(lowrange_sdat)
            report "Wrong wrapped out_sdat"
            severity ERROR;
        end if;
      end if;

      -- RESIZE_NUM() in common_pkg.vhd is always equivalent to lowrange
      assert signed(resize_num_sdat) = signed(lowrange_sdat)
        report "Wrong resize_num_sdat /= lowrange_sdat"
        severity ERROR;
      assert unsigned(resize_num_udat) = unsigned(lowrange_udat)
        report "Wrong resize_num_udat /= lowrange_udat"
        severity ERROR;
    end if;
  end process;
end tb;
