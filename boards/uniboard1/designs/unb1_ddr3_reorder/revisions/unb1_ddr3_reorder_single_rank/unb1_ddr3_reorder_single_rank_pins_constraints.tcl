set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[8] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[8] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[8] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[9] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[9] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[9] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[10] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[10] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[10] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[11] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[11] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[11] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[12] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[12] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[12] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[13] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[13] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[13] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[14] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[14] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[14] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[15] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[15] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[15] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[16] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[16] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[16] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[17] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[17] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[17] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[18] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[18] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[18] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[19] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[19] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[19] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[20] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[20] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[20] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[21] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[21] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[21] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[22] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[22] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[22] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[23] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[23] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[23] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[24] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[24] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[24] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[25] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[25] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[25] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[26] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[26] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[26] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[27] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[27] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[27] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[28] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[28] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[28] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[29] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[29] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[29] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[30] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[30] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[30] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[31] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[31] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[31] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[32] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[32] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[32] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[33] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[33] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[33] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[34] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[34] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[34] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[35] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[35] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[35] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[36] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[36] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[36] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[37] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[37] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[37] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[38] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[38] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[38] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[39] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[39] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[39] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[40] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[40] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[40] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[41] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[41] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[41] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[42] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[42] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[42] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[43] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[43] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[43] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[44] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[44] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[44] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[45] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[45] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[45] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[46] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[46] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[46] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[47] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[47] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[47] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[48] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[48] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[48] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[49] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[49] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[49] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[50] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[50] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[50] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[51] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[51] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[51] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[52] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[52] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[52] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[53] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[53] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[53] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[54] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[54] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[54] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[55] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[55] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[55] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[56] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[56] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[56] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[57] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[57] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[57] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[58] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[58] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[58] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[59] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[59] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[59] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[60] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[60] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[60] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[61] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[61] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[61] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[62] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[62] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[62] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_IO.dq[63] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[63] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dq[63] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs_n[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs_n[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs_n[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs_n[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs_n[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs_n[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs_n[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_IO.dqs_n[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name INPUT_TERMINATION "PARALLEL 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_IO.dqs_n[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_OU.ck[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITHOUT CALIBRATION" -to MB_I_OU.ck[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_OU.ck[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITHOUT CALIBRATION" -to MB_I_OU.ck[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_OU.ck_n[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITHOUT CALIBRATION" -to MB_I_OU.ck_n[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I" -to MB_I_OU.ck_n[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITHOUT CALIBRATION" -to MB_I_OU.ck_n[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[10] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[10] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[11] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[11] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[12] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[12] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[13] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[13] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[14] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[14] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[8] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[8] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.a[9] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.a[9] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.ba[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.ba[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.ba[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.ba[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.ba[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.ba[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.cs_n[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.cs_n[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.cs_n[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.cs_n[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.we_n -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.we_n -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.ras_n -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.ras_n -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.cas_n -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.cas_n -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.cke[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.cke[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.cke[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.cke[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.odt[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.odt[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.odt[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.odt[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD 1.5V -to MB_I_OU.reset_n -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to MB_I_OU.reset_n -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.dm[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_OU.dm[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.dm[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_OU.dm[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.dm[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_OU.dm[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.dm[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_OU.dm[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.dm[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_OU.dm[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.dm[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_OU.dm[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.dm[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_OU.dm[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to MB_I_OU.dm[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name OUTPUT_TERMINATION "SERIES 50 OHM WITH CALIBRATION" -to MB_I_OU.dm[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_IO.dq[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_IO.dq[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_IO.dq[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_IO.dq[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_IO.dq[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_IO.dq[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_IO.dq[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_IO.dq[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_IO.dq[8] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_IO.dq[9] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_IO.dq[10] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_IO.dq[11] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_IO.dq[12] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_IO.dq[13] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_IO.dq[14] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_IO.dq[15] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_IO.dq[16] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_IO.dq[17] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_IO.dq[18] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_IO.dq[19] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_IO.dq[20] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_IO.dq[21] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_IO.dq[22] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_IO.dq[23] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_IO.dq[24] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_IO.dq[25] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_IO.dq[26] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_IO.dq[27] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_IO.dq[28] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_IO.dq[29] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_IO.dq[30] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_IO.dq[31] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_IO.dq[32] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_IO.dq[33] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_IO.dq[34] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_IO.dq[35] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_IO.dq[36] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_IO.dq[37] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_IO.dq[38] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_IO.dq[39] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_IO.dq[40] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_IO.dq[41] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_IO.dq[42] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_IO.dq[43] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_IO.dq[44] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_IO.dq[45] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_IO.dq[46] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_IO.dq[47] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_IO.dq[48] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_IO.dq[49] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_IO.dq[50] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_IO.dq[51] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_IO.dq[52] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_IO.dq[53] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_IO.dq[54] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_IO.dq[55] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_IO.dq[56] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_IO.dq[57] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_IO.dq[58] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_IO.dq[59] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_IO.dq[60] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_IO.dq[61] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_IO.dq[62] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_IO.dq[63] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[0] -to MB_I_OU.dm[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[1] -to MB_I_OU.dm[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[2] -to MB_I_OU.dm[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[3] -to MB_I_OU.dm[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[4] -to MB_I_OU.dm[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[5] -to MB_I_OU.dm[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[6] -to MB_I_OU.dm[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name DQ_GROUP 9 -from MB_I_IO.dqs[7] -to MB_I_OU.dm[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[8] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[9] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[10] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[11] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[12] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[13] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[14] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[15] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[16] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[17] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[18] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[19] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[20] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[21] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[22] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[23] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[24] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[25] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[26] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[27] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[28] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[29] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[30] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[31] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[32] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[33] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[34] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[35] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[36] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[37] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[38] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[39] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[40] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[41] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[42] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[43] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[44] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[45] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[46] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[47] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[48] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[49] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[50] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[51] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[52] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[53] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[54] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[55] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[56] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[57] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[58] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[59] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[60] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[61] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[62] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dq[63] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_OU.dm[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_OU.dm[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_OU.dm[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_OU.dm[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_OU.dm[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_OU.dm[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_OU.dm[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_OU.dm[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs_n[0] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs_n[1] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs_n[2] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs_n[3] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs_n[4] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs_n[5] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs_n[6] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name MEM_INTERFACE_DELAY_CHAIN_CONFIG FLEXIBLE_TIMING -to MB_I_IO.dqs_n[7] -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL "GLOBAL CLOCK" -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|pll0|upll_memphy|auto_generated|clk[1]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL "GLOBAL CLOCK" -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|pll0|upll_memphy|auto_generated|clk[2]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL "DUAL-REGIONAL CLOCK" -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|pll0|upll_memphy|auto_generated|clk[3]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL "DUAL-REGIONAL CLOCK" -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|pll0|upll_memphy|auto_generated|clk[5]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL "DUAL-REGIONAL CLOCK" -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|pll0|upll_memphy|auto_generated|clk[6]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|ureset|phy_reset_mem_stable_n" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|ureset|phy_reset_n" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|s0|sequencer_rw_mgr_inst|rw_mgr_inst|rw_mgr_core_inst|rw_soft_reset_n" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_write_side[0]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_wraddress[0]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_write_side[1]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_wraddress[1]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_write_side[2]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_wraddress[2]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_write_side[3]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_wraddress[3]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_write_side[4]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_wraddress[4]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_write_side[5]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_wraddress[5]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_write_side[6]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_wraddress[6]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_write_side[7]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|reset_n_fifo_wraddress[7]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|read_capture_clk_div2[0]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|read_capture_clk_div2[1]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|read_capture_clk_div2[2]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|read_capture_clk_div2[3]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|read_capture_clk_div2[4]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|read_capture_clk_div2[5]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|read_capture_clk_div2[6]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name GLOBAL_SIGNAL OFF -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|p0|umemphy|uread_datapath|read_capture_clk_div2[7]" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name ENABLE_BENEFICIAL_SKEW_OPTIMIZATION_FOR_NON_GLOBAL_CLOCKS ON -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_instance_assignment -name PLL_ENFORCE_USER_PHASE_SHIFT ON -to "u_revision|u_node|u_ddr_mem_ctrl|u_tech_ddr|\\gen_ip_stratixiv:u0|\\gen_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master:u_ip_stratixiv_ddr3_uphy_4g_single_rank_800_master|ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_inst|pll0|upll_memphy|auto_generated|pll1" -tag __ip_stratixiv_ddr3_uphy_4g_single_rank_800_master_p0
set_global_assignment -name UNIPHY_SEQUENCER_DQS_CONFIG_ENABLE ON
set_global_assignment -name OPTIMIZE_MULTI_CORNER_TIMING ON
set_global_assignment -name UNIPHY_TEMP_VER_CODE 206275612