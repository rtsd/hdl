-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra
-- Purpose: Multi-testbench for diag_block_gen
-- Description:
--   Verify diag_block_gen
-- Usage:
--   > as 4
--   > run -all

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

entity tb_tb_diag_block_gen is
end tb_tb_diag_block_gen;

architecture tb of tb_tb_diag_block_gen is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- -- general
  -- g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
  -- -- specific
  -- g_nof_samples_per_packet : NATURAL := 96;
  -- g_nof_blocks_per_sync    : NATURAL := 10;
  -- g_gapsize                : NATURAL := 32;  -- >= 0
  -- g_buf_adr_w      : NATURAL := 7;   -- Waveform buffer address width (requires corresponding c_buf_file)
  -- g_buf_dat_w      : NATURAL := 32   -- Waveform buffer stored data width (requires corresponding c_buf_file)
  -- g_try_phasor     : BOOLEAN := FALSE  -- use TRUE to see BG phasor in wave window with out_sosi.re/im in radix
                                          -- decimal and analogue format, no self test

  u_bg               : entity work.tb_diag_block_gen generic map (e_active, 96, 10, 32, 7, 32, false);
  u_bg_ready         : entity work.tb_diag_block_gen generic map (e_random, 96, 10, 32, 7, 32, false);
  u_bg_minimal_gap_0 : entity work.tb_diag_block_gen generic map (e_active,  2,  1,  0, 7, 32, false);
  u_bg_minimal_gap_1 : entity work.tb_diag_block_gen generic map (e_active,  2,  1,  1, 7, 32, false);
end tb;
