# Assignments for unb2c arria10 10GbE pins when the jesd204b interface is used
# Use this in place of unb2c_10GbE_pins.tcl


# Pins needed for the 12 channel JESD204B interface to the ADCs

set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC[0]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC[1]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC[3]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC[4]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC[5]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC[6]
set_instance_assignment -name IO_STANDARD "1.8 V" -to JESD204B_SYNC[7]

set_location_assignment PIN_AD12 -to JESD204B_SYNC[0]
set_location_assignment PIN_AC13 -to JESD204B_SYNC[1]
set_location_assignment PIN_AA13 -to JESD204B_SYNC[2]
set_location_assignment PIN_AA12 -to JESD204B_SYNC[3]
set_location_assignment PIN_V14 -to JESD204B_SYNC[4]
set_location_assignment PIN_V12 -to JESD204B_SYNC[5]
set_location_assignment PIN_U14 -to JESD204B_SYNC[6]
set_location_assignment PIN_R13 -to JESD204B_SYNC[7]

set_location_assignment PIN_Y13 -to JESD204B_SYSREF
set_location_assignment PIN_Y12 -to "JESD204B_SYSREF(n)"
set_instance_assignment -name IO_STANDARD LVDS -to JESD204B_SYSREF
set_instance_assignment -name IO_STANDARD LVDS -to "JESD204B_SYSREF(n)"




# The following is copied from unb2c_10GbE_pins.tcl. 
# Settings for BCK_RX[0..11] are modified to suit the JESD204B protocol

set_location_assignment PIN_AL32 -to CLKUSR

set_instance_assignment -name IO_STANDARD LVDS -to BCK_REF_CLK
set_instance_assignment -name IO_STANDARD LVDS -to "BCK_REF_CLK(n)"
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to BCK_REF_CLK
set_location_assignment PIN_V9 -to BCK_REF_CLK
set_location_assignment PIN_V10 -to "BCK_REF_CLK(n)"


set_location_assignment PIN_AR7 -to BCK_RX[11]
set_location_assignment PIN_AR8 -to "BCK_RX[11](n)"
set_location_assignment PIN_AP5 -to BCK_RX[10]
set_location_assignment PIN_AP6 -to "BCK_RX[10](n)"
set_location_assignment PIN_AU7 -to BCK_RX[9]
set_location_assignment PIN_AU8 -to "BCK_RX[9](n)"
set_location_assignment PIN_AT5 -to BCK_RX[8]
set_location_assignment PIN_AT6 -to "BCK_RX[8](n)"
set_location_assignment PIN_AW7 -to BCK_RX[7]
set_location_assignment PIN_AW8 -to "BCK_RX[7](n)"
set_location_assignment PIN_AV5 -to BCK_RX[6]
set_location_assignment PIN_AV6 -to "BCK_RX[6](n)"
set_location_assignment PIN_BA7 -to BCK_RX[5]
set_location_assignment PIN_BA8 -to "BCK_RX[5](n)"
set_location_assignment PIN_AY5 -to BCK_RX[4]
set_location_assignment PIN_AY6 -to "BCK_RX[4](n)"
set_location_assignment PIN_BC7 -to BCK_RX[3]
set_location_assignment PIN_BC8 -to "BCK_RX[3](n)"
set_location_assignment PIN_BB5 -to BCK_RX[2]
set_location_assignment PIN_BB6 -to "BCK_RX[2](n)"
set_location_assignment PIN_AY9 -to BCK_RX[1]
set_location_assignment PIN_AY10 -to "BCK_RX[1](n)"
set_location_assignment PIN_BB9 -to BCK_RX[0]
set_location_assignment PIN_BB10 -to "BCK_RX[0](n)"

set_global_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON

