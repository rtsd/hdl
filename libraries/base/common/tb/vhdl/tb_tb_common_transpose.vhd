-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;

-- Purpose: Multi-test bench for common_transpose.vhd
-- Usage:
-- > as 3
-- > run -all

entity tb_tb_common_transpose is
end tb_tb_common_transpose;

architecture tb of tb_tb_common_transpose is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--                                                            g_pipeline_shiftreg  : NATURAL := 0;
--                                                            |  g_pipeline_transpose : NATURAL := 0;
--                                                            |  |  g_pipeline_hold      : NATURAL := 0;
--                                                            |  |  |  g_pipeline_select    : NATURAL := 1;
--                                                            |  |  |  |   g_nof_data           : NATURAL := 3;
--                                                            |  |  |  |   |   g_data_w             : NATURAL := 12
--                                                            |  |  |  |   |   |
  u_4_4   : entity common_lib.tb_common_transpose generic map(0, 0, 0, 1,  4,  4);

  u_4_8   : entity common_lib.tb_common_transpose generic map(0, 0, 0, 1,  4,  8);

  u_3_12  : entity common_lib.tb_common_transpose generic map(0, 0, 0, 1,  3, 12);
  u_3_12p : entity common_lib.tb_common_transpose generic map(1, 3, 1, 0,  3, 12);

  u_4_16  : entity common_lib.tb_common_transpose generic map(0, 0, 0, 1,  4, 16);
  u_4_16p : entity common_lib.tb_common_transpose generic map(1, 2, 3, 1,  4, 16);
end tb;
