-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_unb1_test_pkg is
    -----------------------------------------------------------------------------
    -- this component declaration is copy-pasted from Quartus v11.1 QSYS builder
    -----------------------------------------------------------------------------
    component qsys_unb1_test is
        port (
            coe_ram_write_export_from_the_avs_eth_0          : out std_logic;  -- export
            coe_reg_read_export_from_the_avs_eth_0           : out std_logic;  -- export
            coe_readdata_export_to_the_reg_mmdp_ctrl         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_address_export_from_the_pio_system_info      : out std_logic_vector(4 downto 0);  -- export
            coe_address_export_from_the_pio_pps              : out std_logic_vector(0 downto 0);  -- export
            coe_waitrequest_export_to_the_reg_tr_10GbE       : in  std_logic                     := 'X';  -- export
            coe_reset_export_from_the_pio_pps                : out std_logic;  -- export
            coe_readdata_export_to_the_reg_epcs              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_readdata_export_to_the_pio_pps               : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_writedata_export_from_the_pio_system_info    : out std_logic_vector(31 downto 0);  -- export
            coe_write_export_from_the_reg_tr_xaui            : out std_logic;  -- export
            coe_reset_export_from_the_reg_unb_sens           : out std_logic;  -- export
            coe_tse_write_export_from_the_avs_eth_0          : out std_logic;  -- export
            coe_writedata_export_from_the_reg_tr_xaui        : out std_logic_vector(31 downto 0);  -- export
            coe_reset_export_from_the_reg_wdi                : out std_logic;  -- export
            coe_readdata_export_to_the_reg_dpmm_data         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_writedata_export_from_the_reg_mmdp_ctrl      : out std_logic_vector(31 downto 0);  -- export
            coe_address_export_from_the_reg_dpmm_ctrl        : out std_logic_vector(0 downto 0);  -- export
            coe_clk_export_from_the_rom_system_info          : out std_logic;  -- export
            coe_reset_export_from_the_reg_remu               : out std_logic;  -- export
            coe_read_export_from_the_reg_unb_sens            : out std_logic;  -- export
            coe_write_export_from_the_reg_unb_sens           : out std_logic;  -- export
            coe_clk_export_from_the_reg_dpmm_data            : out std_logic;  -- export
            coe_clk_export_from_the_reg_unb_sens             : out std_logic;  -- export
            coe_reg_writedata_export_from_the_avs_eth_0      : out std_logic_vector(31 downto 0);  -- export
            coe_address_export_from_the_reg_tr_xaui          : out std_logic_vector(10 downto 0);  -- export
            coe_readdata_export_to_the_reg_dpmm_ctrl         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_reset_export_from_the_reg_mmdp_data          : out std_logic;  -- export
            coe_read_export_from_the_reg_wdi                 : out std_logic;  -- export
            coe_reg_write_export_from_the_avs_eth_0          : out std_logic;  -- export
            coe_writedata_export_from_the_reg_mmdp_data      : out std_logic_vector(31 downto 0);  -- export
            coe_read_export_from_the_reg_epcs                : out std_logic;  -- export
            coe_readdata_export_to_the_reg_remu              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_readdata_export_to_the_reg_unb_sens          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_ram_address_export_from_the_avs_eth_0        : out std_logic_vector(9 downto 0);  -- export
            coe_waitrequest_export_to_the_reg_tr_xaui        : in  std_logic                     := 'X';  -- export
            coe_clk_export_from_the_pio_pps                  : out std_logic;  -- export
            coe_readdata_export_to_the_pio_system_info       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_write_export_from_the_reg_tr_10GbE           : out std_logic;  -- export
            coe_reset_export_from_the_reg_tr_xaui            : out std_logic;  -- export
            coe_writedata_export_from_the_rom_system_info    : out std_logic_vector(31 downto 0);  -- export
            coe_address_export_from_the_reg_dpmm_data        : out std_logic_vector(0 downto 0);  -- export
            coe_address_export_from_the_reg_tr_10GbE         : out std_logic_vector(14 downto 0);  -- export
            coe_write_export_from_the_reg_mmdp_ctrl          : out std_logic;  -- export
            coe_reset_export_from_the_avs_eth_0              : out std_logic;  -- export
            coe_address_export_from_the_reg_wdi              : out std_logic_vector(0 downto 0);  -- export
            coe_write_export_from_the_pio_system_info        : out std_logic;  -- export
            coe_tse_address_export_from_the_avs_eth_0        : out std_logic_vector(9 downto 0);  -- export
            coe_write_export_from_the_pio_pps                : out std_logic;  -- export
            coe_write_export_from_the_rom_system_info        : out std_logic;  -- export
            coe_irq_export_to_the_avs_eth_0                  : in  std_logic                     := 'X';  -- export
            coe_read_export_from_the_rom_system_info         : out std_logic;  -- export
            coe_reset_export_from_the_reg_epcs               : out std_logic;  -- export
            reset_n                                          : in  std_logic                     := 'X';  -- reset_n
            coe_tse_writedata_export_from_the_avs_eth_0      : out std_logic_vector(31 downto 0);  -- export
            coe_clk_export_from_the_reg_mmdp_ctrl            : out std_logic;  -- export
            coe_ram_read_export_from_the_avs_eth_0           : out std_logic;  -- export
            coe_tse_readdata_export_to_the_avs_eth_0         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            clk_0                                            : in  std_logic                     := 'X';  -- clk
            coe_read_export_from_the_reg_dpmm_ctrl           : out std_logic;  -- export
            coe_readdata_export_to_the_reg_tr_xaui           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_writedata_export_from_the_reg_remu           : out std_logic_vector(31 downto 0);  -- export
            coe_write_export_from_the_reg_dpmm_data          : out std_logic;  -- export
            coe_writedata_export_from_the_reg_unb_sens       : out std_logic_vector(31 downto 0);  -- export
            coe_writedata_export_from_the_reg_tr_10GbE       : out std_logic_vector(31 downto 0);  -- export
            coe_reg_readdata_export_to_the_avs_eth_0         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_read_export_from_the_reg_tr_10GbE            : out std_logic;  -- export
            coe_clk_export_from_the_reg_tr_10GbE             : out std_logic;  -- export
            coe_reset_export_from_the_reg_dpmm_ctrl          : out std_logic;  -- export
            coe_tse_read_export_from_the_avs_eth_0           : out std_logic;  -- export
            coe_writedata_export_from_the_reg_dpmm_data      : out std_logic_vector(31 downto 0);  -- export
            coe_read_export_from_the_reg_tr_xaui             : out std_logic;  -- export
            coe_writedata_export_from_the_reg_wdi            : out std_logic_vector(31 downto 0);  -- export
            coe_reset_export_from_the_pio_system_info        : out std_logic;  -- export
            coe_read_export_from_the_pio_system_info         : out std_logic;  -- export
            coe_clk_export_from_the_reg_mmdp_data            : out std_logic;  -- export
            coe_clk_export_from_the_reg_wdi                  : out std_logic;  -- export
            coe_clk_export_from_the_reg_epcs                 : out std_logic;  -- export
            coe_write_export_from_the_reg_remu               : out std_logic;  -- export
            coe_ram_readdata_export_to_the_avs_eth_0         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_read_export_from_the_reg_mmdp_data           : out std_logic;  -- export
            coe_writedata_export_from_the_reg_epcs           : out std_logic_vector(31 downto 0);  -- export
            out_port_from_the_pio_wdi                        : out std_logic;  -- export
            coe_reset_export_from_the_reg_dpmm_data          : out std_logic;  -- export
            coe_clk_export_from_the_reg_remu                 : out std_logic;  -- export
            coe_read_export_from_the_reg_mmdp_ctrl           : out std_logic;  -- export
            coe_clk_export_from_the_avs_eth_0                : out std_logic;  -- export
            coe_address_export_from_the_reg_mmdp_ctrl        : out std_logic_vector(0 downto 0);  -- export
            coe_write_export_from_the_reg_epcs               : out std_logic;  -- export
            coe_readdata_export_to_the_rom_system_info       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_reset_export_from_the_reg_mmdp_ctrl          : out std_logic;  -- export
            coe_readdata_export_to_the_reg_mmdp_data         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_write_export_from_the_reg_wdi                : out std_logic;  -- export
            coe_clk_export_from_the_reg_tr_xaui              : out std_logic;  -- export
            coe_readdata_export_to_the_reg_wdi               : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_read_export_from_the_pio_pps                 : out std_logic;  -- export
            coe_clk_export_from_the_pio_system_info          : out std_logic;  -- export
            coe_writedata_export_from_the_pio_pps            : out std_logic_vector(31 downto 0);  -- export
            coe_address_export_from_the_reg_epcs             : out std_logic_vector(2 downto 0);  -- export
            coe_read_export_from_the_reg_dpmm_data           : out std_logic;  -- export
            coe_reset_export_from_the_rom_system_info        : out std_logic;  -- export
            coe_writedata_export_from_the_reg_dpmm_ctrl      : out std_logic_vector(31 downto 0);  -- export
            coe_tse_waitrequest_export_to_the_avs_eth_0      : in  std_logic                     := 'X';  -- export
            coe_address_export_from_the_reg_mmdp_data        : out std_logic_vector(0 downto 0);  -- export
            coe_address_export_from_the_reg_unb_sens         : out std_logic_vector(2 downto 0);  -- export
            coe_address_export_from_the_rom_system_info      : out std_logic_vector(9 downto 0);  -- export
            coe_clk_export_from_the_reg_dpmm_ctrl            : out std_logic;  -- export
            coe_reg_address_export_from_the_avs_eth_0        : out std_logic_vector(3 downto 0);  -- export
            coe_write_export_from_the_reg_mmdp_data          : out std_logic;  -- export
            coe_address_export_from_the_reg_remu             : out std_logic_vector(2 downto 0);  -- export
            coe_readdata_export_to_the_reg_tr_10GbE          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            coe_write_export_from_the_reg_dpmm_ctrl          : out std_logic;  -- export
            coe_reset_export_from_the_reg_tr_10GbE           : out std_logic;  -- export
            coe_ram_writedata_export_from_the_avs_eth_0      : out std_logic_vector(31 downto 0);  -- export
            coe_read_export_from_the_reg_remu                : out std_logic;  -- export
            reg_bsn_monitor_1gbe_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_1gbe_read_export                 : out std_logic;  -- export
            reg_bsn_monitor_1gbe_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_1gbe_write_export                : out std_logic;  -- export
            reg_bsn_monitor_1gbe_address_export              : out std_logic_vector(3 downto 0);  -- export
            reg_bsn_monitor_1gbe_clk_export                  : out std_logic;  -- export
            reg_bsn_monitor_1gbe_reset_export                : out std_logic;  -- export
            reg_diag_data_buffer_10gbe_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_10gbe_read_export           : out std_logic;  -- export
            reg_diag_data_buffer_10gbe_writedata_export      : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_10gbe_write_export          : out std_logic;  -- export
            reg_diag_data_buffer_10gbe_address_export        : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buffer_10gbe_clk_export            : out std_logic;  -- export
            reg_diag_data_buffer_10gbe_reset_export          : out std_logic;  -- export
            ram_diag_bg_10gbe_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_bg_10gbe_read_export                    : out std_logic;  -- export
            ram_diag_bg_10gbe_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            ram_diag_bg_10gbe_write_export                   : out std_logic;  -- export
            ram_diag_bg_10gbe_address_export                 : out std_logic_vector(11 downto 0);  -- export
            ram_diag_bg_10gbe_clk_export                     : out std_logic;  -- export
            ram_diag_bg_10gbe_reset_export                   : out std_logic;  -- export
            reg_diag_bg_10gbe_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_bg_10gbe_read_export                    : out std_logic;  -- export
            reg_diag_bg_10gbe_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_diag_bg_10gbe_write_export                   : out std_logic;  -- export
            reg_diag_bg_10gbe_address_export                 : out std_logic_vector(2 downto 0);  -- export
            reg_diag_bg_10gbe_clk_export                     : out std_logic;  -- export
            reg_diag_bg_10gbe_reset_export                   : out std_logic;  -- export
            reg_dp_offload_rx_10gbe_hdr_dat_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_offload_rx_10gbe_hdr_dat_read_export      : out std_logic;  -- export
            reg_dp_offload_rx_10gbe_hdr_dat_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_dp_offload_rx_10gbe_hdr_dat_write_export     : out std_logic;  -- export
            reg_dp_offload_rx_10gbe_hdr_dat_address_export   : out std_logic_vector(7 downto 0);  -- export
            reg_dp_offload_rx_10gbe_hdr_dat_clk_export       : out std_logic;  -- export
            reg_dp_offload_rx_10gbe_hdr_dat_reset_export     : out std_logic;  -- export
            reg_dp_offload_tx_10gbe_hdr_dat_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_offload_tx_10gbe_hdr_dat_read_export      : out std_logic;  -- export
            reg_dp_offload_tx_10gbe_hdr_dat_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_dp_offload_tx_10gbe_hdr_dat_write_export     : out std_logic;  -- export
            reg_dp_offload_tx_10gbe_hdr_dat_address_export   : out std_logic_vector(7 downto 0);  -- export
            reg_dp_offload_tx_10gbe_hdr_dat_clk_export       : out std_logic;  -- export
            reg_dp_offload_tx_10gbe_hdr_dat_reset_export     : out std_logic;  -- export
            reg_dp_offload_tx_10gbe_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_offload_tx_10gbe_read_export              : out std_logic;  -- export
            reg_dp_offload_tx_10gbe_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_dp_offload_tx_10gbe_write_export             : out std_logic;  -- export
            reg_dp_offload_tx_10gbe_address_export           : out std_logic_vector(2 downto 0);  -- export
            reg_dp_offload_tx_10gbe_clk_export               : out std_logic;  -- export
            reg_dp_offload_tx_10gbe_reset_export             : out std_logic;  -- export
            reg_bsn_monitor_10gbe_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_10gbe_read_export                : out std_logic;  -- export
            reg_bsn_monitor_10gbe_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_10gbe_write_export               : out std_logic;  -- export
            reg_bsn_monitor_10gbe_address_export             : out std_logic_vector(5 downto 0);  -- export
            reg_bsn_monitor_10gbe_clk_export                 : out std_logic;  -- export
            reg_bsn_monitor_10gbe_reset_export               : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_reset_export              : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_clk_export                : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_address_export            : out std_logic_vector(0 downto 0);  -- export
            reg_dp_offload_tx_1gbe_write_export              : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_dp_offload_tx_1gbe_read_export               : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_offload_tx_1gbe_hdr_dat_reset_export      : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_hdr_dat_clk_export        : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_hdr_dat_address_export    : out std_logic_vector(5 downto 0);  -- export
            reg_dp_offload_tx_1gbe_hdr_dat_write_export      : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_hdr_dat_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            reg_dp_offload_tx_1gbe_hdr_dat_read_export       : out std_logic;  -- export
            reg_dp_offload_tx_1gbe_hdr_dat_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dp_offload_rx_1gbe_hdr_dat_reset_export      : out std_logic;  -- export
            reg_dp_offload_rx_1gbe_hdr_dat_clk_export        : out std_logic;  -- export
            reg_dp_offload_rx_1gbe_hdr_dat_address_export    : out std_logic_vector(5 downto 0);  -- export
            reg_dp_offload_rx_1gbe_hdr_dat_write_export      : out std_logic;  -- export
            reg_dp_offload_rx_1gbe_hdr_dat_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            reg_dp_offload_rx_1gbe_hdr_dat_read_export       : out std_logic;  -- export
            reg_dp_offload_rx_1gbe_hdr_dat_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_bg_1gbe_reset_export                    : out std_logic;  -- export
            reg_diag_bg_1gbe_clk_export                      : out std_logic;  -- export
            reg_diag_bg_1gbe_address_export                  : out std_logic_vector(2 downto 0);  -- export
            reg_diag_bg_1gbe_write_export                    : out std_logic;  -- export
            reg_diag_bg_1gbe_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_diag_bg_1gbe_read_export                     : out std_logic;  -- export
            reg_diag_bg_1gbe_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_bg_1gbe_reset_export                    : out std_logic;  -- export
            ram_diag_bg_1gbe_clk_export                      : out std_logic;  -- export
            ram_diag_bg_1gbe_address_export                  : out std_logic_vector(9 downto 0);  -- export
            ram_diag_bg_1gbe_write_export                    : out std_logic;  -- export
            ram_diag_bg_1gbe_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            ram_diag_bg_1gbe_read_export                     : out std_logic;  -- export
            ram_diag_bg_1gbe_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_1gbe_reset_export           : out std_logic;  -- export
            reg_diag_data_buffer_1gbe_clk_export             : out std_logic;  -- export
            reg_diag_data_buffer_1gbe_address_export         : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buffer_1gbe_write_export           : out std_logic;  -- export
            reg_diag_data_buffer_1gbe_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_1gbe_read_export            : out std_logic;  -- export
            reg_diag_data_buffer_1gbe_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_1gbe_reset_export           : out std_logic;  -- export
            ram_diag_data_buffer_1gbe_clk_export             : out std_logic;  -- export
            ram_diag_data_buffer_1gbe_address_export         : out std_logic_vector(13 downto 0);  -- export
            ram_diag_data_buffer_1gbe_write_export           : out std_logic;  -- export
            ram_diag_data_buffer_1gbe_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_1gbe_read_export            : out std_logic;  -- export
            ram_diag_data_buffer_1gbe_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_10gbe_reset_export          : out std_logic;  -- export
            ram_diag_data_buffer_10gbe_clk_export            : out std_logic;  -- export
            ram_diag_data_buffer_10gbe_address_export        : out std_logic_vector(13 downto 0);  -- export
            ram_diag_data_buffer_10gbe_write_export          : out std_logic;  -- export
            ram_diag_data_buffer_10gbe_writedata_export      : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_10gbe_read_export           : out std_logic;  -- export
            ram_diag_data_buffer_10gbe_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_1gbe_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_1gbe_read_export                 : out std_logic;  -- export
            reg_diag_rx_seq_1gbe_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_diag_rx_seq_1gbe_write_export                : out std_logic;  -- export
            reg_diag_rx_seq_1gbe_address_export              : out std_logic_vector(2 downto 0);  -- export
            reg_diag_rx_seq_1gbe_clk_export                  : out std_logic;  -- export
            reg_diag_rx_seq_1gbe_reset_export                : out std_logic;  -- export
            reg_diag_tx_seq_1gbe_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_tx_seq_1gbe_read_export                 : out std_logic;  -- export
            reg_diag_tx_seq_1gbe_writedata_export            : out std_logic_vector(31 downto 0);  -- export
            reg_diag_tx_seq_1gbe_write_export                : out std_logic;  -- export
            reg_diag_tx_seq_1gbe_address_export              : out std_logic_vector(1 downto 0);  -- export
            reg_diag_tx_seq_1gbe_clk_export                  : out std_logic;  -- export
            reg_diag_tx_seq_1gbe_reset_export                : out std_logic;  -- export
            reg_diag_rx_seq_10gbe_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_10gbe_read_export                : out std_logic;  -- export
            reg_diag_rx_seq_10gbe_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_diag_rx_seq_10gbe_write_export               : out std_logic;  -- export
            reg_diag_rx_seq_10gbe_address_export             : out std_logic_vector(4 downto 0);  -- export
            reg_diag_rx_seq_10gbe_clk_export                 : out std_logic;  -- export
            reg_diag_rx_seq_10gbe_reset_export               : out std_logic;  -- export
            reg_diag_tx_seq_10gbe_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_tx_seq_10gbe_read_export                : out std_logic;  -- export
            reg_diag_tx_seq_10gbe_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_diag_tx_seq_10gbe_write_export               : out std_logic;  -- export
            reg_diag_tx_seq_10gbe_address_export             : out std_logic_vector(3 downto 0);  -- export
            reg_diag_tx_seq_10gbe_clk_export                 : out std_logic;  -- export
            reg_diag_tx_seq_10gbe_reset_export               : out std_logic;  -- export
            reg_io_ddr_mb_i_reset_export                     : out std_logic;  -- export
            reg_io_ddr_mb_i_clk_export                       : out std_logic;  -- export
            reg_io_ddr_mb_i_address_export                   : out std_logic_vector(15 downto 0);  -- export
            reg_io_ddr_mb_i_write_export                     : out std_logic;  -- export
            reg_io_ddr_mb_i_writedata_export                 : out std_logic_vector(31 downto 0);  -- export
            reg_io_ddr_mb_i_read_export                      : out std_logic;  -- export
            reg_io_ddr_mb_i_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_io_ddr_mb_ii_reset_export                    : out std_logic;  -- export
            reg_io_ddr_mb_ii_clk_export                      : out std_logic;  -- export
            reg_io_ddr_mb_ii_address_export                  : out std_logic_vector(15 downto 0);  -- export
            reg_io_ddr_mb_ii_write_export                    : out std_logic;  -- export
            reg_io_ddr_mb_ii_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_io_ddr_mb_ii_read_export                     : out std_logic;  -- export
            reg_io_ddr_mb_ii_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_tx_seq_ddr_mb_i_reset_export            : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_i_clk_export              : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_i_address_export          : out std_logic_vector(1 downto 0);  -- export
            reg_diag_tx_seq_ddr_mb_i_write_export            : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_i_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_diag_tx_seq_ddr_mb_i_read_export             : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_i_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_tx_seq_ddr_mb_ii_reset_export           : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_ii_clk_export             : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_ii_address_export         : out std_logic_vector(1 downto 0);  -- export
            reg_diag_tx_seq_ddr_mb_ii_write_export           : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_ii_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            reg_diag_tx_seq_ddr_mb_ii_read_export            : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_ii_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_ddr_mb_i_reset_export            : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_i_clk_export              : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_i_address_export          : out std_logic_vector(2 downto 0);  -- export
            reg_diag_rx_seq_ddr_mb_i_write_export            : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_i_writedata_export        : out std_logic_vector(31 downto 0);  -- export
            reg_diag_rx_seq_ddr_mb_i_read_export             : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_i_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_ddr_mb_ii_reset_export           : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_ii_clk_export             : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_ii_address_export         : out std_logic_vector(2 downto 0);  -- export
            reg_diag_rx_seq_ddr_mb_ii_write_export           : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_ii_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            reg_diag_rx_seq_ddr_mb_ii_read_export            : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_ii_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_ddr_mb_i_reset_export       : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_i_clk_export         : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_i_address_export     : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buffer_ddr_mb_i_write_export       : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_i_writedata_export   : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_ddr_mb_i_read_export        : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_i_readdata_export    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_ddr_mb_ii_reset_export      : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_ii_clk_export        : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_ii_address_export    : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buffer_ddr_mb_ii_write_export      : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_ii_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_ddr_mb_ii_read_export       : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_ii_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_ddr_mb_i_reset_export       : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_i_clk_export         : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_i_address_export     : out std_logic_vector(10 downto 0);  -- export
            ram_diag_data_buffer_ddr_mb_i_write_export       : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_i_writedata_export   : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_ddr_mb_i_read_export        : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_i_readdata_export    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_ddr_mb_ii_reset_export      : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_ii_clk_export        : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_ii_address_export    : out std_logic_vector(10 downto 0);  -- export
            ram_diag_data_buffer_ddr_mb_ii_write_export      : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_ii_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_ddr_mb_ii_read_export       : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_ii_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X')  -- export
        );
    end component qsys_unb1_test;
end qsys_unb1_test_pkg;
