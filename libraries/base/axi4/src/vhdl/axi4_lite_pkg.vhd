-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:
--   Package containing usefull definitions for working with AXI4-Lite
-- Description:
--   Ported from:
--   https://git.astron.nl/rtsd/gemini/-/blob/master/libraries/base/axi4/src/vhdl/axi4_lite_pkg.vhd
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use std.textio.all;
use IEEE.std_logic_textio.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

package axi4_lite_pkg is
  ------------------------------------------------------------------------------
  -- Simple AXI4 lite memory access (for MM control interface)
  ------------------------------------------------------------------------------
  constant c_axi4_lite_address_w  : natural := 32;
  constant c_axi4_lite_data_w     : natural := 32;
  constant c_axi4_lite_prot_w     : natural := 3;
  constant c_axi4_lite_resp_w     : natural := 2;

  type t_axi4_lite_copi is record  -- Controller Out Peripheral In
    -- write address channel
    awaddr  : std_logic_vector(c_axi4_lite_address_w - 1 downto 0);  -- write address
    awprot  : std_logic_vector(c_axi4_lite_prot_w - 1 downto 0);  -- access permission for write
    awvalid : std_logic;  -- write address valid
    -- write data channel
    wdata   : std_logic_vector(c_axi4_lite_data_w - 1 downto 0);  -- write data
    wstrb   : std_logic_vector((c_axi4_lite_data_w / c_byte_w) - 1 downto 0);  -- write strobes
    wvalid  : std_logic;  -- write valid
    -- write response channel
    bready  : std_logic;  -- response ready
    -- read address channel
    araddr  : std_logic_vector(c_axi4_lite_address_w - 1 downto 0);  -- read address
    arprot  : std_logic_vector(c_axi4_lite_prot_w - 1 downto 0);  -- access permission for read
    arvalid : std_logic;  -- read address valid
    -- read data channel
    rready  : std_logic;  -- read ready
  end record;

  type t_axi4_lite_cipo is record  -- Controller In Peripheral Out
    -- write_address channel
    awready : std_logic;  -- write address ready
    -- write data channel
    wready  : std_logic;  -- write ready
    -- write response channel
    bresp   : std_logic_vector(c_axi4_lite_resp_w - 1 downto 0);  -- write response
    bvalid  : std_logic;  -- write response valid
    -- read address channel
    arready : std_logic;  -- read address ready
    -- read data channel
    rdata   : std_logic_vector(c_axi4_lite_data_w - 1 downto 0);  -- read data
    rresp   : std_logic_vector(c_axi4_lite_resp_w - 1 downto 0);  -- read response
    rvalid  : std_logic;  -- read valid
  end record;

  constant c_axi4_lite_copi_rst : t_axi4_lite_copi := ((others => '0'), (others => '0'), '0', (others => '0'), (others => '0'), '0', '0', (others => '0'), (others => '0'), '0', '0');
  constant c_axi4_lite_cipo_rst : t_axi4_lite_cipo := ('0', '0', (others => '0'), '0', '0', (others => '0'), (others => '0'), '0');

  -- Multi port array for MM records
  type t_axi4_lite_cipo_arr is array (integer range <>) of t_axi4_lite_cipo;
  type t_axi4_lite_copi_arr is array (integer range <>) of t_axi4_lite_copi;

  constant c_axi4_lite_resp_okay   : std_logic_vector(c_axi4_lite_resp_w - 1 downto 0) := "00";  -- normal access success
  constant c_axi4_lite_resp_exokay : std_logic_vector(c_axi4_lite_resp_w - 1 downto 0) := "01";  -- exclusive access okay
  constant c_axi4_lite_resp_slverr : std_logic_vector(c_axi4_lite_resp_w - 1 downto 0) := "10";  -- peripheral error
  constant c_axi4_lite_resp_decerr : std_logic_vector(c_axi4_lite_resp_w - 1 downto 0) := "11";  -- decode error

  -- Functions to convert axi4-lite to MM.
  function func_axi4_lite_to_mm_copi(axi4_copi : t_axi4_lite_copi) return t_mem_copi;
  function func_axi4_lite_to_mm_cipo(axi4_cipo : t_axi4_lite_cipo) return t_mem_cipo;

  -- Functions to convert MM to axi4-lite.
  function func_axi4_lite_from_mm_copi(mm_copi : t_mem_copi) return t_axi4_lite_copi;
  function func_axi4_lite_from_mm_cipo(mm_cipo : t_mem_cipo; bvalid : std_logic) return t_axi4_lite_cipo;
end axi4_lite_pkg;

package body axi4_lite_pkg is
  function func_axi4_lite_to_mm_copi(axi4_copi : t_axi4_lite_copi) return t_mem_copi is
    variable v_mm_copi : t_mem_copi := c_mem_copi_rst;
  begin
    if axi4_copi.awvalid = '1' then
      v_mm_copi.address := "00" & axi4_copi.awaddr(c_axi4_lite_address_w - 1 downto 2);  -- convert byte addressed to word addressed.
    else
      v_mm_copi.address := "00" & axi4_copi.araddr(c_axi4_lite_address_w - 1 downto 2);  -- convert byte addressed to word addressed.
    end if;

    v_mm_copi.wrdata(c_axi4_lite_data_w - 1 downto 0) := axi4_copi.wdata;
    v_mm_copi.wr                                    := axi4_copi.awvalid;
    v_mm_copi.rd                                    := axi4_copi.arvalid;
    return v_mm_copi;
  end;

  function func_axi4_lite_to_mm_cipo(axi4_cipo : t_axi4_lite_cipo) return t_mem_cipo is
    variable v_mm_cipo : t_mem_cipo := c_mem_cipo_rst;
  begin
    v_mm_cipo.rddata(c_axi4_lite_data_w - 1 downto 0) := axi4_cipo.rdata;
    v_mm_cipo.rdval                                 := axi4_cipo.rvalid;
    v_mm_cipo.waitrequest                           := not (axi4_cipo.awready and axi4_cipo.wready and axi4_cipo.arready);
    return v_mm_cipo;
  end;

  function func_axi4_lite_from_mm_copi(mm_copi : t_mem_copi) return t_axi4_lite_copi is
    variable v_axi4_copi : t_axi4_lite_copi := c_axi4_lite_copi_rst;
  begin
    v_axi4_copi.awaddr  := mm_copi.address(c_axi4_lite_address_w - 3 downto 0) & "00";  -- convert word addressed to byte addressed.
    v_axi4_copi.awprot  := (others => '0');
    v_axi4_copi.awvalid := mm_copi.wr;
    v_axi4_copi.wdata   := mm_copi.wrdata(c_axi4_lite_data_w - 1 downto 0);
    v_axi4_copi.wstrb   := (others => '1');  -- Either ignored or all bytes selected.
    v_axi4_copi.wvalid  := mm_copi.wr;
    v_axi4_copi.bready  := '1';  -- Unsupported by MM, assuming always ready.
    v_axi4_copi.araddr  := mm_copi.address(c_axi4_lite_address_w - 3 downto 0) & "00";  -- convert word addressed to byte addressed.
    v_axi4_copi.arprot  := (others => '0');
    v_axi4_copi.arvalid := mm_copi.rd;
    v_axi4_copi.rready  := '1';  -- Unsupported by MM, assuming always ready.
    return v_axi4_copi;
  end;

  function func_axi4_lite_from_mm_cipo(mm_cipo : t_mem_cipo; bvalid : std_logic) return t_axi4_lite_cipo is
    variable v_axi4_cipo : t_axi4_lite_cipo := c_axi4_lite_cipo_rst;
  begin
    v_axi4_cipo.awready := not mm_cipo.waitrequest;
    v_axi4_cipo.wready  := not mm_cipo.waitrequest;
    v_axi4_cipo.bresp   := c_axi4_lite_resp_okay;
    v_axi4_cipo.bvalid  := bvalid;
    v_axi4_cipo.arready := not mm_cipo.waitrequest;
    v_axi4_cipo.rdata   := mm_cipo.rddata(c_axi4_lite_data_w - 1 downto 0);
    v_axi4_cipo.rresp   := c_axi4_lite_resp_okay;
    v_axi4_cipo.rvalid  := mm_cipo.rdval;
    return v_axi4_cipo;
  end;
end axi4_lite_pkg;
