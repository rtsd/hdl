-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

-- > as 4
-- > run -all --> OK

entity tb_tb_dp_bsn_source_v2 is
end tb_tb_dp_bsn_source_v2;

architecture tb of tb_tb_dp_bsn_source_v2 is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- from tb_dp_bsn_source_v2.vhd
  --
  -- g_pps_interval : NATURAL := 240
  -- g_block_size   : NATURAL := 32

  -- test integer case
  u_20_10  : entity work.tb_dp_bsn_source_v2 generic map (20, 10);  -- 20 // 10 = 2, 20 MOD 10 = 0, 20/10 = 2 block/sync
  u_22_11  : entity work.tb_dp_bsn_source_v2 generic map (22, 11);  -- 22 // 11 = 2, 22 MOD 11 = 0, 22/11 = 2 block/sync
  u_39_13  : entity work.tb_dp_bsn_source_v2 generic map (39, 13);  -- 39 // 13 = 3, 39 MOD 13 = 0, 39/13 = 3 block/sync

  -- test smallest nof block per sync
  u_10_10  : entity work.tb_dp_bsn_source_v2 generic map (10, 10);  -- 1 block/sync
  u_5_5    : entity work.tb_dp_bsn_source_v2 generic map (5, 5);  -- 1 block/sync

  -- test smallest g_block_size case
  u_3_3    : entity work.tb_dp_bsn_source_v2 generic map (3, 3);  -- 3 // 3 = 1, 3 MOD 3 = 0, 3/3 = 1 block/sync
  u_6_3    : entity work.tb_dp_bsn_source_v2 generic map (6, 3);  -- 6 // 3 = 2, 6 MOD 3 = 0, 6/3 = 2 block/sync
  u_7_3    : entity work.tb_dp_bsn_source_v2 generic map (7, 3);  -- 7 // 3 = 2, 7 MOD 3 = 1, 7/3 = 2.33 block/sync

  -- test lofar case with 0.5 fraction in average nof block/sync
  u_20_8   : entity work.tb_dp_bsn_source_v2 generic map (20, 8);  -- 20 // 8 = 2, 20 MOD 8 = 4, 20/8 = 2.5 block/sync

  -- test fractional (corner) cases
  u_18_9   : entity work.tb_dp_bsn_source_v2 generic map (18, 9);  -- 18 MOD 9 = 0
  u_17_9   : entity work.tb_dp_bsn_source_v2 generic map (17, 9);  -- 17 MOD 9 = 8 = g_block_size - 1
  u_19_9   : entity work.tb_dp_bsn_source_v2 generic map (19, 9);  -- 19 MOD 9 = 1
  u_20_9   : entity work.tb_dp_bsn_source_v2 generic map (20, 9);  -- 20 MOD 9 = 2
  u_25_9   : entity work.tb_dp_bsn_source_v2 generic map (25, 9);  -- 25 MOD 9 = 7
  u_26_9   : entity work.tb_dp_bsn_source_v2 generic map (26, 9);  -- 26 MOD 9 = 8 = g_block_size - 1
  u_27_9   : entity work.tb_dp_bsn_source_v2 generic map (27, 9);  -- 27 MOD 9 = 0

  -- test some prime values
  u_17_3   : entity work.tb_dp_bsn_source_v2 generic map (17, 3);  -- 17 // 3 = 5, 17 MOD 3 = 2, 17/3 = 5.66 block/sync
  u_101_17 : entity work.tb_dp_bsn_source_v2 generic map (101, 17);  -- 101 // 17 = 5, 101 MOD 17 = 16, 101/17 = 5.9411 block/sync
end tb;
