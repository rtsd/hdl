// Copyright (C) Altera Corporation. All rights reserved. 
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 11.1sp2 linux32 Build 259 01/26/2012
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6c"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Y242AJE3S/1ANzr6+KU25/a5awpzNB4pDaGchfOik9m+2Fi6iQRkOk0U4OMWz58Z
VuFIEHsr0UoAYyVmDGj4lzKFFaufIORdvc7Z7L38shyvft5HoR0Ci1b/bIKFOAMT
RhZ6QTU6+yR1Ty9wbcjuC2Or7ypWbgdiK3nljlSyFBI=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2960)
zCl7u6ofds3NLjRQlfllshVMmAyW+b3G/cnbOoCMAwu3eHOpxZyRVZsOlErdYi8P
gzgXZjKNY1HvyiORtvFC8kg5pqOwJ1Dg515IDNFX99fOY8+0yR0M210/sigMvriZ
0YEcOXFt2Wg4uZjB9tu8HTvib24WqbplI8hpheI/PaNOAfwPRUU7rC2WGTqoQhNQ
6G4ZfYhcEzg+iemHBgxsJzXNbaqRUkOol+PQQ557TQRUBEMmQE51jCutCc6tOmo9
NPfgLWYdSKB+UKqnInZ7ByA+u9WeoaYeb7CJVRKnSH8IzyIjQnINAuW9CLOrN48j
I4R4Trt5hMSr4gW7FjLeaKWFNmokaH75U3WzIJK+y7FtVCsqU6j1NQw+JIKR51e5
y/FArMWJ1Ba1Nb9wmcG/WTr9PpFIQE4cEBGk6PfhfGxsMTrwC80qK/CXq4Lg7cog
PjHss9ptEEoY5Ds6YwdRwum51kHRwG+IvSegjZkk7TI39FfGCVVb6cKgKqTJyD+C
SHJtwVDPvMfzQKLLaKwwe+azQntqz3DHFiSm0wt0bacGvbtyo/HWhp8UzmLDLbh1
pR6PIrOugz+6HFVI0BFo6n2QDtViu7v3Ca3o94aKczavS8W6ZVCxApk2z/bjsjMF
LNqLnvHPSaXtRdCHNq/L/0yJe0jrEPiA9J93pn1hPiFt52gr0xwRlg1PUfrg1fEG
1+yqsFMqVg7kKUbdp2MeH2TebJ1qoVMTQo+xFzbZb2l7NzK1KXmWUPtfw+hYqlFx
nwcZPMaiAfIMmM365tQqUNnDACBozJ7VffN0M8gFRl0h+mpWORffNNxkBv1O9Qr6
FsmgCClbC8LL6e5zXC/TWJF4Jac+++txPqF3sNGlNsseAN1akwlqZCGVpbrnT6YC
trGsi5uj2Pxu3QoiWqV4O+CX7ERVF8iFfITUAWJhYhyRRx6TcpbrflZCsT2r+Q6w
TEM1YsLAIEHAPrHCg7xR9PzRxW2nCBcyhG38xYuMuxwOycylENwmQaT2N+QccYMO
rLwg73eiwhBLDqsgbR/f8fZIfwI5/JT8TsCePLo4m4i4+WSZpgH3ds2o3sQWqli2
s4BGkn7tl2PuaqJ+OG6JONV341Q3ZJGbNWAkp2iD3L0/rutlMWlYpnUveFRQcaoE
B/tsJbmvY0Ojr7tjBAxQ2Fz9FEUK0e4rqU84nQFg9OUcGPVddKv3+8z+VB6PerEP
gIIII5hOKGTAyK9KxDMUDOVEyle0XOcu/i1ZjSIeuVIYZO6PfB5c7kcnLGhBJtU4
xYceMOAbtG9DqJHgrr0OWluKZCvwSz4dgfgClfag8E5hv1kKs3N8aaDqHCIZPFg9
8Jbg+nwBmbezNGD7ILD6GBt5ssq5ybaNqbd/h/kDo8p08VXSH90qp8sACPQmygkn
tVvS7MvYLff5XZW8ecWTHXKDRt9PqwxThB+EKqnCl/csNcrbKUcFV8STTYgJjdzO
cVegRwbpRbZPCEgj/E9ZAQ7Tv8VcsthJzSm7HFFgODPw5iMRRriWGUllsFyrZfUJ
N1wrZuzBL/rFOT8IOLi7NZ8375m9xVA3EwKBI39M57H5n83aZeX5od6jgQygbyRm
xOQrHwn7Q9p0EuvnYZMHJUTpY7tnNzuNd5DepDLT0VkK2UCFS+2umjqN7F1+Ca36
wac6XA4TSKEG7nnR1e3rpP7H0LfQd9YyVkvIMmrT5htS7350sWife4xCRcZ5FlqP
+uwQqOzVsGfo+DM0QNNTDtRav5EH1zNO++TYFBU/kwJQyb44o8CmS5h8x3Gxccl9
n1Lry2V/iKvG548/AVwfOCqFOf3qeuu5EfD9pRGHD3/aGLmt2qUHqxglEcYJi/Ra
a+GaB1/1RX9lCQUKRlouqhrsVFLZ6yVBXQAyYm/pbtk7+fOhnCDyOfX8wIjC+kAw
LuBOHKRbnmAtbh5GJrdtSTb+daVHnGNKCXgiEWY3IzRDFdJlMvgNxNjqvHYH/Xrv
cl2fNHyut0lQeEdophJyumpvyajIravGi0RLSTSU4+feoWbK7uSMUCFv0J32394l
s1f5se60oINZiKiY8CzvcYSFiE0Y5tTvcnohXN+RNdd51a1NBxm5sN5K5khIaq9k
87B7ClenzRbUfs9xXf2j2s1rSjNXCOtooHPMDiXfIvpE3H+8aJ/KD9evtADaobpe
g9hcxQlGBErVetj71mhHZMA/DQOiQ8yrBWKWahlO9nXaD/9nCnoklTRxhJKx1ERo
AWGVmo0wW3Dx0tr2WeLheeb291IlS0iL2Trzs4cDd/k4FmT6BUSKlBoOkUBrExZc
hsj4HOu6urtdNSEdWSVCotpQ5hv35D4DRJZ/tOFOdhdn8nW2SXEvjC7EU21j9CBL
fFbq2tlIWpb8IxdbzlJkAPHIgQEvAJybUXSOf89aWdx4tixzJdak3NuBUYQ/s5w8
F6F25g9gnSS3n3tOSmvggMWXERWnr53KdHgx1y3xH2AM1EAmXiWhEsgknBDpf1gk
UsuggkHmAO9bA3ae3gIsLjU98yx/K+COiIrrpntJZu+l/jZwMWR3/K6CfPDdwo5n
N7dI5fjTORqWIXzx9Wx/tVfr9JH4XhS23CNcB2WIAlAWt2qjroSTGgfkLXRIM31n
fLSVmWMAm5BAAKC3DbUjnfhc+1w5rOUWP7V41c2PRn+FEUpBrdpSoxnFqw1ODPNw
XKLmMzr30JsDvRM6RwiqGjTwvNnCCs6DZ6eZafc4UNBbTy36yZ+5slGXzWYeCGt8
BKdOYy0VDdcWkuQ9RVQ4hZETNOmctdDhUU/9QdOpNjdPaF+FmPyqZYOJsABrRwwe
K8XXrJ4lra0Idx3scWo0b7vFd9K2/OovAT91WIVHhGAJoZUZKSl1t2CY2eumEa1t
2MsssJIW5ZyvR5LWoomi2Fmu/jzn1+oqo+KAuINDwSPWgcRgYkmcTepgKib+KOpO
9iK7h+xzZ6AkeTWpXhUyfVFC807So/0biDWkhp2csLJhkJ+aUcx7m6s7pySwCO5L
T6IuYq8bwSWLAbIHsrtZgpJcuNPg8W4ckfyt/2BleHrK3AriSjyiVHPHOfKgpqs5
4Kxe2i7uQ2h+CPseVNjF8r48OUiX+1XNNBIf4t7dXUySOk9vVuPhrtYW2eWZz4Wv
SN/uyfP3sbfA6/1N5tjZzlDqTgiTp5pFT74GBhQTTcSAtgUULHN7A+JG8gfYDrfK
RBB3k44Ee3mSP3JHg3rLpp72GWATSJAe+a5hW0hGkPzNyNd66le/4LpKPLQSwluv
luPZLDPnzSbmOmjm30RzV8gXsIAIRHnrE0qo50Cev7f7o3MoeGSfutT7Joz3QXI1
hoVB4uvPIWikI+W4+L6HSdiPjuE69HIZrId4RX/MEMHB8ZUrBnLwd41Qv3GW7+sC
mbzW6pwG02AlSIXCMQ+YMe27q4Hz9JxpkEdYIY7S3t3qSvRKnZUf2QgaDUPOtbmF
vb2BT41J99c3eYfvvz02gJeEy4B/F0EzL5eidO8RiIjd3DcqKCSXxNyFPGdtXYFz
Ij3tHpL71pnQLkvftU/Yq4iNtUJe+xpcnwhiwSZQ7DcmXSIs3zwpOdJbiyX4VS50
8LIBWeIb/ehNx6abzTGoOzpOo0zqBG718p4t6OYMeICTGfhnudBdDMHWAZB5BQG8
QnbwDbaPtpodkgJSOMTW/QgvYgX/Xe48x0GPr6f6VxG4eRPYetN2LfKEy6nrn+y3
WzyouO72rAwJ7Avu5L1DrEASPiH9BVLtgkjVMYkLIU3uTLsNfmLUe/hn8KpfWOqL
7HZWhw3LDyLf6XKW7/9AaM+NH8MkyMAMffFFLEhUYOAuuy1TP/u2YluxUJ1nw6No
QPnzU3YjaHM7B5WVb9OJxWR/i9qn2ygJVB6sTCgy+nKrxInJL9k9/ty7Q0LiuTZC
TZLSTxJH7oFhTcYgOqYkR67qmI/nZfq7foaVjI1H/Ns=
`pragma protect end_protected
