--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Prepend one word with empty symbols at the head of a frame
-- Description:
--   This function is typically used to change the word boundary in a frame by
--   inserting g_nof_padding symbols at the head of the frame.
--   E.g. the total ethernet header is 42 octets and then followed by the
--   payload. This dp_pad_insert can insert 2 or 6 octets in front of the
--   header to make it 48 octets and hence make the payload start at a 32 or
--   64 bit word boundary respectively.
-- Remark:
-- . Ready latency (RL) = 1
-- . The padding preserves the sync and BSN from the snk_in.
-- . Reverse operation is dp_pad_remove, which is merely dp_split.
-- . This dp_pad_insert now can only insert 1 word at the head, this could be
--   extended to allow also g_nof_padding > g_data_w/g_symbol_w

entity dp_pad_insert is
  generic (
    g_data_w          : natural := 64;
    g_symbol_w        : natural := 8;  -- g_data_w/g_symbol_w must be integer
    g_nof_padding     : natural := 6;  -- must be > 0 and  <= g_data_w/g_symbol_w
    g_internal_bypass : boolean := false  -- can be used to eliminate external GENERATE-statements/wiring
  );
  port (
    rst     : in  std_logic;
    clk     : in  std_logic;
    -- ST sinks
    snk_out : out t_dp_siso;
    snk_in  : in  t_dp_sosi;
    -- ST source
    src_in  : in  t_dp_siso;
    src_out : out t_dp_sosi
  );
end dp_pad_insert;

architecture str of dp_pad_insert is
  constant c_symbols_per_beat     : natural := g_data_w / g_symbol_w;
  constant c_pad_empty            : natural := c_symbols_per_beat - g_nof_padding;

  -- dp padding is one word with g_nof_padding octets and c_pad_empty empty
  constant c_pad_sosi    : t_dp_sosi := ('0', (others => '0'), (others => '0'), (others => '0'), (others => '0'), '1', '1', '1', TO_DP_EMPTY(c_pad_empty), (others => '0'), (others => '0'));

  signal pad_siso        : t_dp_siso;
  signal pad_sosi        : t_dp_sosi;
  signal nxt_pad_sosi    : t_dp_sosi;

  signal in_siso         : t_dp_siso;
  signal next_in_sosi    : t_dp_sosi;
  signal pend_in_sosi    : t_dp_sosi;
  signal in_sosi         : t_dp_sosi;

  -- element (0) becomes the head, element (1) becomes the tail
  signal concat_siso_arr : t_dp_siso_arr(0 to 1);
  signal concat_sosi_arr : t_dp_sosi_arr(0 to 1);
begin
  no_bypass : if g_internal_bypass = false generate
    -- Create the padding input word, while preserving the sync and BSN from the snk_in
    p_clk : process(rst, clk)
    begin
      if rst = '1' then
        pad_sosi <= c_dp_sosi_rst;
        in_sosi  <= c_dp_sosi_rst;
      elsif rising_edge(clk) then
        pad_sosi <= nxt_pad_sosi;
        in_sosi  <= next_in_sosi;
      end if;
    end process;

    p_pad_sosi : process(pad_siso, pend_in_sosi)
    begin
      nxt_pad_sosi <= c_dp_sosi_rst;
      -- Provide the padding word when the output is ready and the input is pending, use the sync, bsn and channel from the input
      if pad_siso.ready = '1' and pend_in_sosi.sop = '1' then
        nxt_pad_sosi         <= c_pad_sosi;
        nxt_pad_sosi.sync    <= pend_in_sosi.sync;
        nxt_pad_sosi.bsn     <= pend_in_sosi.bsn;
        nxt_pad_sosi.channel <= pend_in_sosi.channel;
      end if;
    end process;

    u_hold_snk_in : entity work.dp_hold_input
    port map (
      rst              => rst,
      clk              => clk,
      -- ST sink
      snk_out          => snk_out,
      snk_in           => snk_in,
      -- ST source
      src_in           => in_siso,
      next_src_out     => next_in_sosi,
      pend_src_out     => pend_in_sosi,
      src_out_reg      => in_sosi
    );

    -- Prepend the padding octets to snk_in
    pad_siso           <= concat_siso_arr(0);
    in_siso            <= concat_siso_arr(1);
    concat_sosi_arr(0) <= pad_sosi;  -- = head frame with in_sosi sop info thanks to p_pad_sosi
    concat_sosi_arr(1) <= in_sosi;  -- = tail frame with in_sosi eop info

    u_concat : entity work.dp_concat  -- RL = 1
    generic map (
      g_data_w    => g_data_w,
      g_symbol_w  => g_symbol_w
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- ST sinks
      snk_out_arr => concat_siso_arr,
      snk_in_arr  => concat_sosi_arr,
      -- ST source
      src_in      => src_in,
      src_out     => src_out
    );
  end generate;

  gen_bypass : if g_internal_bypass = true generate
    src_out <= snk_in;
    snk_out <= src_in;
  end generate;
end str;
