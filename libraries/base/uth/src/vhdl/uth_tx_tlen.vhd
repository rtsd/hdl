--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.uth_pkg.all;

-- Purpose: Determine UTH frame Tx tlen field value per channel
--
-- Description:
--   This uth_tx_tlen combinatorially determines the tlen field value for each
--   channel. The assumption is that each channel has a fixed nof data. These
--   block length are define for each channel via g_len_arr.
--   If g_typ_arr for that channel has a value > g_len_max then the g_typ_arr
--   value is passed on via the tlen field (so then 'tlen' is used as UTH frame
--   type), else g_typ_arr must be set to 0 to indicate that the g_len_arr
--   value is passed on via the tlen field (so then 'tlen' is used as UTH frame
--   nof data).
--
-- * Example 1:
--   g_data_w  =  8
--   g_len_max =  2**7-1, so tlen < 128 indicates length and >= 128 indicates type
--   g_nof_ch  =  1
--   g_typ_arr =  0
--   g_len_arr = 30
--   channel   =  don't care
--
-- * Example 2:
--   g_data_w  =  8
--   g_len_max =  2**7-1, so tlen < 128 indicates length and >= 128 indicates type
--   g_nof_ch  =  4
--   g_typ_arr =  0, 128, 200,  0
--   g_len_arr = 30,  25,  20, 60
--   channel   =  0,   1,   2,  3

entity uth_tx_tlen is
  generic (
    g_data_w     : natural := c_uth_data_max_w;
    g_len_max    : natural := 2**15 - 1;  -- g_len_max must be < 2**(g_data_w-1), because Msbit of tlen indicates type when '1' and length when '0'
    g_nof_ch     : natural := 4;  -- g_nof_ch, the channels are numbered from 0 TO g_nof_ch-1, each channel represents a type of UTH frame
    g_typ_arr    : t_natural_arr := array_init(0, 4);  -- g_nof_ch = g_typ_arr'LENGTH, use unconstraint type to allow generic g_nof_ch
    g_len_arr    : t_natural_arr := array_init(1, 4)  -- g_nof_ch = g_len_arr'LENGTH, use unconstraint type to allow generic g_nof_ch
  );
  port (
    channel   : in  std_logic_vector;
    tlen      : out std_logic_vector
  );
end uth_tx_tlen;

architecture rtl of uth_tx_tlen is
  constant c_typ_ofs  : natural := g_len_max + 1;

  -- Map input t_natural_arr range to range 0 TO g_nof_ch-1
  constant c_typ_arr  : t_natural_arr(0 to g_nof_ch - 1) := g_typ_arr;
  constant c_len_arr  : t_natural_arr(0 to g_nof_ch - 1) := g_len_arr;
begin
  assert ceil_log2(g_len_max) < g_data_w
    report "uth_tx_tlen: g_len_max is too large to use tlen field as length"
    severity FAILURE;

  process(channel)
    variable v_ch   : natural;
    variable v_tlen : std_logic_vector(g_data_w - 1 downto 0);
  begin
    v_ch := 0;  -- If g_nof_ch=1 then it is not necessary to use the channel input
    if g_nof_ch > 1 then
      v_ch := TO_UINT(channel);
    end if;

    v_tlen := TO_UVEC(c_len_arr(v_ch), g_data_w);  -- values in c_len_arr must be <= g_len_max
    if c_typ_arr(v_ch) > g_len_max then
      v_tlen := TO_UVEC(c_typ_arr(v_ch), g_data_w);
    end if;

    tlen <= RESIZE_SVEC(v_tlen, tlen'length);
  end process;
end rtl;
