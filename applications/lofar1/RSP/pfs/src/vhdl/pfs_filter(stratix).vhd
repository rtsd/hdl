library IEEE, lpm, altera_mf, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

architecture stratix of pfs_filter is
  component altmult_add
  generic (
    input_register_b2                     : string  := "CLOCK0";
    input_register_a1                     : string  := "CLOCK0";
    multiplier_register0                  : string  := "CLOCK0";
    signed_pipeline_aclr_b                : string  := "ACLR3";
    input_register_b3                     : string  := "CLOCK0";
    input_register_a2                     : string  := "CLOCK0";
    multiplier_register1                  : string  := "CLOCK0";
    addnsub_multiplier_pipeline_aclr1     : string  := "ACLR3";
    input_register_a3                     : string  := "CLOCK0";
    multiplier_register2                  : string  := "CLOCK0";
    signed_aclr_a                         : string  := "ACLR3";
    signed_register_a                     : string  := "CLOCK0";
    number_of_multipliers                 : natural := 4;
    multiplier_register3                  : string  := "CLOCK0";
    multiplier_aclr0                      : string  := "ACLR3";
    addnsub_multiplier_pipeline_aclr3     : string  := "ACLR3";
    signed_aclr_b                         : string  := "ACLR3";
    signed_register_b                     : string  := "CLOCK0";
    lpm_type                              : string  := "altmult_add";
    multiplier_aclr1                      : string  := "ACLR3";
    input_aclr_b0                         : string  := "ACLR3";
    output_register                       : string  := "CLOCK0";
    width_result                          : natural := g_taps_w + g_coef_w + 2;
    representation_a                      : string  := "SIGNED";
    signed_pipeline_register_a            : string  := "CLOCK0";
    input_source_b0                       : string  := "DATAB";
    multiplier_aclr2                      : string  := "ACLR3";
    input_aclr_b1                         : string  := "ACLR3";
    input_aclr_a0                         : string  := "ACLR3";
    multiplier3_direction                 : string  := "ADD";
    addnsub_multiplier_register1          : string  := "CLOCK0";
    representation_b                      : string  := "SIGNED";
    signed_pipeline_register_b            : string  := "CLOCK0";
    input_source_b1                       : string  := "DATAB";
    input_source_a0                       : string  := "DATAA";
    multiplier_aclr3                      : string  := "ACLR3";
    input_aclr_b2                         : string  := "ACLR3";
    input_aclr_a1                         : string  := "ACLR3";
    dedicated_multiplier_circuitry        : string  := "YES";
    input_source_b2                       : string  := "DATAB";
    input_source_a1                       : string  := "DATAA";
    input_aclr_b3                         : string  := "ACLR3";
    input_aclr_a2                         : string  := "ACLR3";
    addnsub_multiplier_register3          : string  := "CLOCK0";
    addnsub_multiplier_aclr1              : string  := "ACLR3";
    output_aclr                           : string  := "ACLR3";
    input_source_b3                       : string  := "DATAB";
    input_source_a2                       : string  := "DATAA";
    input_aclr_a3                         : string  := "ACLR3";
    input_source_a3                       : string  := "DATAA";
    addnsub_multiplier_aclr3              : string  := "ACLR3";
    intended_device_family                : string  := "Stratix II";
    addnsub_multiplier_pipeline_register1 : string  := "CLOCK0";
    width_a                               : natural := g_taps_w;
    input_register_b0                     : string  := "CLOCK0";
    width_b                               : natural := g_coef_w;
    input_register_b1                     : string  := "CLOCK0";
    input_register_a0                     : string  := "CLOCK0";
    addnsub_multiplier_pipeline_register3 : string  := "CLOCK0";
    multiplier1_direction                 : string  := "ADD";
    signed_pipeline_aclr_a                : string  := "ACLR3"
  );
  port (
    dataa                                 : in std_logic_vector(g_taps_w * 4 - 1 downto 0);
    datab                                 : in std_logic_vector(g_coef_w * 4 - 1 downto 0);
    clock0                                : in std_logic;
    aclr3                                 : in std_logic;
    result                                : out std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0)
  );
  end component;

  signal res_0         : std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0);
  signal res_1         : std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0);
  signal res_2         : std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0);
  signal res_3         : std_logic_vector(g_coef_w + g_taps_w + 2 - 1 downto 0);
  signal add_a         : std_logic_vector(g_coef_w + g_taps_w + 3 - 1 downto 0);
  signal add_b         : std_logic_vector(g_coef_w + g_taps_w + 3 - 1 downto 0);
  signal add_c         : std_logic_vector(g_coef_w + g_taps_w + 4 - 1 downto 0);
begin
  registers : process (clk, rst)
  begin
    if rst = '1' then
      result <= (others => '0');
      add_a <= (others => '0');
      add_b <= (others => '0');
    elsif rising_edge(clk) then
      result <= add_c(add_c'high downto add_c'length  - result'length);
      add_a <= std_logic_vector(resize(signed(res_0), add_a'length) + signed(res_1));
      add_b <= std_logic_vector(resize(signed(res_2), add_b'length) + signed(res_3));
    end if;
  end process;

  add_c <= std_logic_vector(SHIFT_LEFT((resize(signed(add_a), add_c'length) + signed(add_b)), 4));
--  nxt_result <= STD_LOGIC_VECTOR(RESIZE(SIGNED(add_c),

  ALTMULT_ADD_0 : altmult_add
  port map (
    dataa  => taps(47 downto 0),
    datab  => coefs(63 downto 0),
    clock0 => clk,
    aclr3  => rst,
    result => res_0
  );

  ALTMULT_ADD_1 : altmult_add
  port map (
    dataa  => taps(95 downto 48),
    datab  => coefs(127 downto 64),
    clock0 => clk,
    aclr3  => rst,
    result => res_1
  );

  ALTMULT_ADD_2 : altmult_add
  port map (
    dataa  => taps(143 downto 96),
    datab  => coefs(191 downto 128),
    clock0 => clk,
    aclr3  => rst,
    result => res_2
  );

  ALTMULT_ADD_3 : altmult_add
  port map (
    dataa  => taps(191 downto 144),
    datab  => coefs(255 downto 192),
    clock0 => clk,
    aclr3  => rst,
    result => res_3
  );
end stratix;
