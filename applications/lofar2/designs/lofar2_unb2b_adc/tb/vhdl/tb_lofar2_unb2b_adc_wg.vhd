-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Self-checking testbench for simulating lofar2_unb2b_adc using WG data.
--
-- Description:
--   MM control actions:
--
--   1) Enable calc mode for WG via reg_diag_wg with:
--        freq = 20MHz
--        ampl = 0.5 * 2**13
--
--   2) Read current BSN from reg_bsn_scheduler_wg and write reg_bsn_scheduler_wg
--      to trigger start of WG at BSN.
--
--   3) Read WG data via ram_aduh_mon into sp_sample and replay sp_sample for
--      analogue view in Wave window:
--
--   4) Read ADUH monitor power sum for via reg_aduh_mon and verify with
--      c_exp_wg_power_sp.
--      View sp_power_sum in Wave window
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   > run -a
--
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2b_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;

entity tb_lofar2_unb2b_adc_wg is
end tb_lofar2_unb2b_adc_wg;

architecture tb of tb_lofar2_unb2b_adc_wg is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2b_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_pps_period          : natural := 1000;

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C
  constant c_cable_delay         : time := 12 ns;

  constant c_sample_freq         : natural := c_unb2b_board_ext_clk_freq_200M / 10**6;  -- 200 MSps
  constant c_sample_period       : time := (10**6 / c_sample_freq) * 1 ps;

  constant c_nof_sync            : natural := 5;
  constant c_nof_block_per_sync  : natural := 16;

  constant c_percentage          : real := 0.05;  -- percentage that actual value may differ from expected value
  constant c_lo_factor           : real := 1.0 - c_percentage;  -- lower boundary
  constant c_hi_factor           : real := 1.0 + c_percentage;  -- higher boundary

  constant c_nof_points          : natural := 1024;
  constant c_nof_taps            : natural := 16;

  constant c_subband_period      : time := c_nof_points * c_sample_period;

  -- WG
  constant c_full_scale_ampl      : real := real(2**(18 - 1) - 1);  -- = full scale of WG
  constant c_bsn_start_wg         : natural := 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  constant c_ampl_sp              : natural := 2**(14 - 1) / 2;  -- in number of lsb
  constant c_subband_sp           : real := 51.2;  -- Select subband at index 512/10 = 51.2 = 20 MHz
  constant c_wg_subband_freq_unit : real := c_diag_wg_freq_unit / 512.0;  -- subband freq = Fs/512 = 200 MSps/512 = 390625 Hz sinus
  constant c_wg_ampl_lsb          : real := c_diag_wg_ampl_unit / c_full_scale_ampl;  -- amplitude in number of LSbit resolution steps
  constant c_exp_wg_power_sp      : real := real(c_ampl_sp**2) / 2.0 * real(c_nof_points * c_nof_block_per_sync);

  -- ADUH
  constant c_mon_buffer_nof_samples  : natural := 1024;  -- samples per stream
  constant c_mon_buffer_nof_words    : natural := c_mon_buffer_nof_samples;

  -- MM
  constant c_mm_file_reg_ppsh               : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "PIO_PPS";
  constant c_mm_file_reg_bsn_source         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE";
  constant c_mm_file_reg_bsn_scheduler_wg   : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg            : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_reg_aduh_mon           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_ADUH_MONITOR";
  constant c_mm_file_ram_aduh_mon           : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "RAM_ADUH_MONITOR";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0);

  -- WG
  signal dbg_c_exp_wg_power_sp : real := c_exp_wg_power_sp;
  signal sp_samples            : t_integer_arr(0 to c_mon_buffer_nof_samples - 1) := (others => 0);
  signal sp_sample             : integer := 0;
  signal sp_power_sum          : unsigned(63 downto 0);
  signal current_bsn_wg        : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal ext_pps             : std_logic := '0';
  signal pps_rst             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal pmbus_scl           : std_logic;
  signal pmbus_sda           : std_logic;

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector((c_unb2b_board_tr_jesd204b.bus_w * c_unb2b_board_tr_jesd204b.nof_bus) - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref     : std_logic;
  signal jesd204b_sync_n     : std_logic_vector((c_unb2b_board_tr_jesd204b.nof_bus * c_unb2b_board_tr_jesd204b.bus_w) - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  JESD204B_REFCLK <= not JESD204B_REFCLK after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, ext_clk, pps);
  jesd204b_sysref <= pps;
  ext_pps <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2b_adc : entity work.lofar2_unb2b_adc
  generic map (
    g_design_name => "lofar2_unb2b_adc_one_node",
    g_design_note => "Lofar2 adc with one node",
    g_sim         => c_sim,
    g_sim_unb_nr  => c_unb_nr,
    g_sim_node_nr => c_node_nr
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- I2C Interface to Sensors
    SENS_SC      => sens_scl,
    SENS_SD      => sens_sda,

    PMBUS_SC     => pmbus_scl,
    PMBUS_SD     => pmbus_sda,
    PMBUS_ALERT  => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    JESD204B_SERIAL_DATA  => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK       => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => jesd204b_sysref,
    JESD204B_SYNC_N => jesd204b_sync_n
  );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn                   : natural;
    variable v_sp_power_sum          : real;
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    proc_common_wait_until_hi_lo(ext_clk, ext_pps);

    ----------------------------------------------------------------------------
    -- Enable BS
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source, 3,                    0, tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source, 2,                    0, tb_clk);  -- Init BSN = 0
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source, 1, c_nof_block_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source, 0,         16#00000003#, tb_clk);  -- Enable BS at PPS

    ----------------------------------------------------------------------------
    -- Enable WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 1, integer(  0.0 * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 2, integer(c_subband_sp * c_wg_subband_freq_unit), tb_clk);  -- freq
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 3, integer(real(c_ampl_sp) * c_wg_ampl_lsb), tb_clk);  -- ampl

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;
    v_bsn := c_bsn_start_wg;
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, v_bsn, tb_clk);  -- first write low then high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,     0, tb_clk);  -- assume v_bsn < 2**31-1

    -- Wait for ADUH monitor to have filled with WG data
    wait for c_subband_period * c_nof_taps;
    wait for c_subband_period * 2;

    ----------------------------------------------------------------------------
    -- WG data : read ADUH monitor buffer
    ----------------------------------------------------------------------------
    -- Wait for start of sync interval
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                            "UNSIGNED", rd_data, ">=", c_nof_block_per_sync * 2,  -- this is the wait until condition
                            c_subband_period, tb_clk);

    wait for c_subband_period;  -- ensure that one block of samples has filled the ADUH monitor buffer after the sync

    -- Read via MM
    for I in 0 to c_mon_buffer_nof_words - 1 loop
      mmf_mm_bus_rd(c_mm_file_ram_aduh_mon, I, rd_data, tb_clk);
      sp_samples(I) <= TO_SINT(rd_data(15 downto 0));
    end loop;

    -- Play to have waveform in time to allow viewing as analogue in the Wave Window
    for I in 0 to c_mon_buffer_nof_words - 1 loop
      proc_common_wait_some_cycles(ext_clk, 1);
      sp_sample <= sp_samples(I);
    end loop;

    wait for c_subband_period * 3;

    ---------------------------------------------------------------------------
    -- Read ADUH monitor power sum
    ---------------------------------------------------------------------------
    -- Wait for start of sync interval
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                            "UNSIGNED", rd_data, ">=", c_nof_block_per_sync * 3,  -- this is the wait until condition
                            c_subband_period, tb_clk);

    -- Read ADUH monitor power sum
    mmf_mm_bus_rd(c_mm_file_reg_aduh_mon, 2, rd_data, tb_clk);  -- read low part
    sp_power_sum(31 downto 0) <= unsigned(rd_data);
    mmf_mm_bus_rd(c_mm_file_reg_aduh_mon, 3, rd_data, tb_clk);  -- read high part
    sp_power_sum(63 downto 32) <= unsigned(rd_data);
    proc_common_wait_some_cycles(tb_clk, 1);

    ---------------------------------------------------------------------------
    -- Verification
    ---------------------------------------------------------------------------
    -- Convert UNSIGNED sp_power_sum to REAL
    v_sp_power_sum := real(real(to_integer(sp_power_sum(61 downto 30))) * real(2**30) + real(to_integer(sp_power_sum(29 downto 0))));

    assert v_sp_power_sum > c_lo_factor * c_exp_wg_power_sp
      report "Wrong SP power for SP 0"
      severity ERROR;
    assert v_sp_power_sum < c_hi_factor * c_exp_wg_power_sp
      report "Wrong SP power for SP 0"
      severity ERROR;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;
end tb;
