-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Dual page memory with single wr in one page and dual rd in other page
-- Description:
--   When next_page pulses then the next access will occur in the other page.
-- Remarks:
--   Each page uses one or more RAM blocks.

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_paged_ram_w_rr is
  generic (
    g_technology      : natural := c_tech_select_default;
    g_pipeline_in     : natural := 0;  -- >= 0
    g_pipeline_out    : natural := 0;  -- >= 0
    g_data_w          : natural;
    g_page_sz         : natural;
    g_ram_rd_latency  : natural := 1  -- >= 1
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    -- next page control
    next_page   : in  std_logic;
    -- single write access to one page
    wr_adr      : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    wr_en       : in  std_logic := '0';
    wr_dat      : in  std_logic_vector(g_data_w - 1 downto 0) := (others => '0');
    -- double read access from the other one page
    rd_adr_a    : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    rd_en_a     : in  std_logic := '1';
    rd_adr_b    : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    rd_en_b     : in  std_logic := '1';
    -- double read data from the other one page after c_rd_latency
    rd_dat_a    : out std_logic_vector(g_data_w - 1 downto 0);
    rd_val_a    : out std_logic;
    rd_dat_b    : out std_logic_vector(g_data_w - 1 downto 0);
    rd_val_b    : out std_logic
  );
end common_paged_ram_w_rr;

architecture str of common_paged_ram_w_rr is
begin
  u_ww_rr : entity work.common_paged_ram_ww_rr
  generic map (
    g_technology     => g_technology,
    g_pipeline_in    => g_pipeline_in,
    g_pipeline_out   => g_pipeline_out,
    g_data_w         => g_data_w,
    g_page_sz        => g_page_sz,
    g_ram_rd_latency => g_ram_rd_latency
  )
  port map (
    rst         => rst,
    clk         => clk,
    clken       => clken,
    -- next page control
    next_page   => next_page,
    -- double write access to one page  --> use only page a
    wr_adr_a    => wr_adr,
    wr_en_a     => wr_en,
    wr_dat_a    => wr_dat,
    -- double read access from the other one page
    rd_adr_a    => rd_adr_a,
    rd_en_a     => rd_en_a,
    rd_adr_b    => rd_adr_b,
    rd_en_b     => rd_en_b,
    -- double read data from the other one page after c_rd_latency
    rd_dat_a    => rd_dat_a,
    rd_val_a    => rd_val_a,
    rd_dat_b    => rd_dat_b,
    rd_val_b    => rd_val_b
  );
end str;
