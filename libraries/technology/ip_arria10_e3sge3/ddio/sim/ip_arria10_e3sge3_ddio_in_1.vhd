-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Simulation model for DDIO in
-- Description:
--   The double data rate datain samples that arrive at time series t0, t1, t2,
--   ... get output with samples t0, t2, ... in dataout_l and samples t1, t3,
--   ... in dataout_h. Hence dataout = dataout_h & dataout_l contains the
--   time series samples in little endian format with the first sample in the
--   LSpart as shown in the timing diagram:
--               _   _   _   _
--     inclock  | |_| |_| |_| |_
--     datain    0 1 2 3 4 5 6 7
--     in_dat_r      1   3   5
--     in_dat_f    0   2   4
--     dataout_h     1   3   5
--     dataout_l     0   2   4
--

library IEEE;
use IEEE.std_logic_1164.all;

entity ip_arria10_e3sge3_ddio_in_1 is
  port (
    datain    : in  std_logic_vector(0 downto 0) := (others => '0');
    inclock   : in  std_logic                    := '0';
    aclr      : in  std_logic                    := '0';
    dataout_h : out std_logic_vector(0 downto 0);
    dataout_l : out std_logic_vector(0 downto 0)
  );
end ip_arria10_e3sge3_ddio_in_1;

architecture beh of ip_arria10_e3sge3_ddio_in_1 is
  signal in_dat_r   : std_logic;
  signal in_dat_f   : std_logic;
begin
  in_dat_r <= datain(0) when rising_edge(inclock);
  in_dat_f <= datain(0) when falling_edge(inclock);

  dataout_h <= (others => in_dat_r);
  dataout_l <= (others => in_dat_f) when rising_edge(inclock);
end beh;
