--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

package dp_stream_pkg is
  ------------------------------------------------------------------------------
  -- General DP stream record defintion
  ------------------------------------------------------------------------------

  -- Remarks:
  -- * Choose smallest maximum SOSI slv lengths that fit all use cases, because unconstrained record fields slv is not allowed
  -- * The large SOSI data field width of 256b has some disadvantages:
  --   . about 10% extra simulation time and PC memory usage compared to 72b (measured using tb_unb_tse_board)
  --   . a 256b number has 64 hex digits in the Wave window which is awkward because of the leading zeros when typically
  --     only 32b are used, fortunately integer representation still works OK (except 0 which is shown as blank).
  --   However the alternatives are not attractive, because they affect the implementation of the streaming
  --   components that use the SOSI record. Alternatives are e.g.:
  --   . define an extra long SOSI data field ldata[255:0] in addition to the existing data[71:0] field
  --   . use the array of SOSI records to contain wider data, all with the same SOSI control field values
  --   . define another similar SOSI record with data[255:0].
  --   Therefore define data width as 256b, because the disadvantages are acceptable and the benefit is great, because all
  --   streaming components can remain as they are.
  -- * Added sync and bsn to SOSI to have timestamp information with the data
  -- * Added re and im to SOSI to support complex data for DSP
  -- * The sosi fields can be labeled in diffent groups: ctrl, info and data as shown in comment at the t_dp_sosi definition.
  --   This grouping is useful for functions that operate on a t_dp_sosi signal.
  -- * The info fields are valid at the sop or at the eop, but typically they hold their last active value to avoid unnessary
  --   toggling and to ease viewing in the wave window.
  constant c_dp_stream_bsn_w      : natural :=  64;  -- 64 is sufficient to count blocks of data for years
  constant c_dp_stream_data_w     : natural := 768;  -- 72 is sufficient for max word 8 * 9-bit. 576 supports half rate DDR4 bus data width. The current 768 is enough for wide single clock SLVs (e.g. headers)
  constant c_dp_stream_dsp_data_w : natural :=  64;  -- 64 is sufficient for DSP data, including complex power accumulates
  constant c_dp_stream_empty_w    : natural :=  16;  -- 8 is sufficient for max 256 symbols per data word, still use 16 bit to be able to count c_dp_stream_data_w in bits
  constant c_dp_stream_channel_w  : natural :=  32;  -- 32 is sufficient for several levels of hierarchy in mapping types of streams on to channels
  constant c_dp_stream_error_w    : natural :=  32;  -- 32 is sufficient for several levels of hierarchy in mapping error numbers, e.g. 32 different one-hot encoded errors, bit [0] = 0 = OK

  constant c_dp_stream_ok         : natural := 0;  -- SOSI err field OK value
  constant c_dp_stream_err        : natural := 1;  -- SOSI err field error value /= OK

  constant c_dp_stream_rl         : natural := 1;  -- SISO default data path stream ready latency RL = 1

  type t_dp_siso is record  -- Source In or Sink Out
    ready    : std_logic;  -- fine cycle based flow control using ready latency RL >= 0
    xon      : std_logic;  -- coarse typically block based flow control using xon/xoff
  end record;

  type t_dp_sosi is record  -- Source Out or Sink In
    sync     : std_logic;  -- ctrl
    bsn      : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);  -- info at sop      (block sequence number)
    data     : std_logic_vector(c_dp_stream_data_w - 1 downto 0);  -- data
    re       : std_logic_vector(c_dp_stream_dsp_data_w - 1 downto 0);  -- data
    im       : std_logic_vector(c_dp_stream_dsp_data_w - 1 downto 0);  -- data
    valid    : std_logic;  -- ctrl
    sop      : std_logic;  -- ctrl
    eop      : std_logic;  -- ctrl
    empty    : std_logic_vector(c_dp_stream_empty_w - 1 downto 0);  -- info at eop
    channel  : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);  -- info at sop
    err      : std_logic_vector(c_dp_stream_error_w - 1 downto 0);  -- info at eop (name field 'err' to avoid the 'error' keyword)
  end record;

  -- Initialise signal declarations with c_dp_stream_rst/rdy to ease the interpretation of slv fields with unused bits
  constant c_dp_siso_rst   : t_dp_siso := ('0', '0');
  constant c_dp_siso_x     : t_dp_siso := ('X', 'X');
  constant c_dp_siso_hold  : t_dp_siso := ('0', '1');
  constant c_dp_siso_rdy   : t_dp_siso := ('1', '1');
  constant c_dp_siso_flush : t_dp_siso := ('1', '0');
  constant c_dp_sosi_init  : t_dp_sosi := ('0', (others => '0'), (others => '0'), (others => '0'), (others => '0'), '0', '0', '0', (others => '0'), (others => '0'), (others => '0'));
  constant c_dp_sosi_rst   : t_dp_sosi := ('0', (others => '0'), (others => '0'), (others => '0'), (others => '0'), '0', '0', '0', (others => '0'), (others => '0'), (others => '0'));
  constant c_dp_sosi_x     : t_dp_sosi := ('X', (others => 'X'), (others => 'X'), (others => 'X'), (others => 'X'), 'X', 'X', 'X', (others => 'X'), (others => 'X'), (others => 'X'));

  -- Use integers instead of slv for monitoring purposes (integer range limited to 31 bit plus sign bit)
  type t_dp_sosi_integer is record
    sync     : std_logic;
    bsn      : natural;
    data     : integer;
    re       : integer;
    im       : integer;
    valid    : std_logic;
    sop      : std_logic;
    eop      : std_logic;
    empty    : natural;
    channel  : natural;
    err      : natural;
  end record;

  -- Use unsigned instead of slv for monitoring purposes beyond the integer range of t_dp_sosi_integer
  type t_dp_sosi_unsigned is record
    sync     : std_logic;
    bsn      : unsigned(c_dp_stream_bsn_w - 1 downto 0);
    data     : unsigned(c_dp_stream_data_w - 1 downto 0);
    re       : unsigned(c_dp_stream_dsp_data_w - 1 downto 0);
    im       : unsigned(c_dp_stream_dsp_data_w - 1 downto 0);
    valid    : std_logic;
    sop      : std_logic;
    eop      : std_logic;
    empty    : unsigned(c_dp_stream_empty_w - 1 downto 0);
    channel  : unsigned(c_dp_stream_channel_w - 1 downto 0);
    err      : unsigned(c_dp_stream_error_w - 1 downto 0);
  end record;

  constant c_dp_sosi_unsigned_rst  : t_dp_sosi_unsigned := ('0', (others => '0'), (others => '0'), (others => '0'), (others => '0'), '0', '0', '0', (others => '0'), (others => '0'), (others => '0'));
  constant c_dp_sosi_unsigned_ones : t_dp_sosi_unsigned := ('1',
                                                            to_unsigned(1, c_dp_stream_bsn_w),
                                                            to_unsigned(1, c_dp_stream_data_w),
                                                            to_unsigned(1, c_dp_stream_dsp_data_w),
                                                            to_unsigned(1, c_dp_stream_dsp_data_w),
                                                            '1', '1', '1',
                                                            to_unsigned(1, c_dp_stream_empty_w),
                                                            to_unsigned(1, c_dp_stream_channel_w),
                                                            to_unsigned(1, c_dp_stream_error_w));

  -- Use boolean to define whether a t_dp_siso, t_dp_sosi field is used ('1') or not ('0')
  type t_dp_siso_sl is record
    ready    : std_logic;
    xon      : std_logic;
  end record;

  type t_dp_sosi_sl is record
    sync     : std_logic;
    bsn      : std_logic;
    data     : std_logic;
    re       : std_logic;
    im       : std_logic;
    valid    : std_logic;
    sop      : std_logic;
    eop      : std_logic;
    empty    : std_logic;
    channel  : std_logic;
    err      : std_logic;
  end record;

  constant c_dp_siso_sl_rst  : t_dp_siso_sl := ('0', '0');
  constant c_dp_siso_sl_ones : t_dp_siso_sl := ('1', '1');
  constant c_dp_sosi_sl_rst  : t_dp_sosi_sl := ('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
  constant c_dp_sosi_sl_ones : t_dp_sosi_sl := ('1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

  -- Multi port or multi register array for DP stream records
  type t_dp_siso_arr is array (integer range <>) of t_dp_siso;
  type t_dp_sosi_arr is array (integer range <>) of t_dp_sosi;

  type t_dp_sosi_integer_arr  is array (integer range <>) of t_dp_sosi_integer;
  type t_dp_sosi_unsigned_arr is array (integer range <>) of t_dp_sosi_unsigned;

  type t_dp_siso_sl_arr is array (integer range <>) of t_dp_siso_sl;
  type t_dp_sosi_sl_arr is array (integer range <>) of t_dp_sosi_sl;

  -- Multi port or multi register slv arrays for DP stream records fields
  type t_dp_bsn_slv_arr      is array (integer range <>) of std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  type t_dp_data_slv_arr     is array (integer range <>) of std_logic_vector(c_dp_stream_data_w - 1 downto 0);
  type t_dp_dsp_data_slv_arr is array (integer range <>) of std_logic_vector(c_dp_stream_dsp_data_w - 1 downto 0);
  type t_dp_empty_slv_arr    is array (integer range <>) of std_logic_vector(c_dp_stream_empty_w - 1 downto 0);
  type t_dp_channel_slv_arr  is array (integer range <>) of std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  type t_dp_error_slv_arr    is array (integer range <>) of std_logic_vector(c_dp_stream_error_w - 1 downto 0);

  -- Multi-dimemsion array types with fixed LS-dimension
  type t_dp_siso_2arr_1 is array (integer range <>) of t_dp_siso_arr(0 downto 0);
  type t_dp_sosi_2arr_1 is array (integer range <>) of t_dp_sosi_arr(0 downto 0);

  -- . 2 dimensional array with 2 fixed LS sosi/siso interfaces (dp_split, dp_concat)
  type t_dp_siso_2arr_2 is array (integer range <>) of t_dp_siso_arr(1 downto 0);
  type t_dp_sosi_2arr_2 is array (integer range <>) of t_dp_sosi_arr(1 downto 0);

  type t_dp_siso_2arr_3 is array (integer range <>) of t_dp_siso_arr(2 downto 0);
  type t_dp_sosi_2arr_3 is array (integer range <>) of t_dp_sosi_arr(2 downto 0);

  type t_dp_siso_2arr_4 is array (integer range <>) of t_dp_siso_arr(3 downto 0);
  type t_dp_sosi_2arr_4 is array (integer range <>) of t_dp_sosi_arr(3 downto 0);

  type t_dp_siso_2arr_8 is array (integer range <>) of t_dp_siso_arr(7 downto 0);
  type t_dp_sosi_2arr_8 is array (integer range <>) of t_dp_sosi_arr(7 downto 0);

  type t_dp_siso_2arr_9 is array (integer range <>) of t_dp_siso_arr(8 downto 0);
  type t_dp_sosi_2arr_9 is array (integer range <>) of t_dp_sosi_arr(8 downto 0);

  type t_dp_siso_2arr_12 is array (integer range <>) of t_dp_siso_arr(11 downto 0);
  type t_dp_sosi_2arr_12 is array (integer range <>) of t_dp_sosi_arr(11 downto 0);

  type t_dp_siso_3arr_4_2 is array (integer range <>) of t_dp_siso_2arr_2(3 downto 0);
  type t_dp_sosi_3arr_4_2 is array (integer range <>) of t_dp_sosi_2arr_2(3 downto 0);

  -- 2-dimensional streaming array type:
  -- Note:
  --   This t_*_mat is less useful then a t_*_2arr array of arrays, because assignments can only be done per element (i.e. not per row). However for t_*_2arr
  --   the arrays dimension must be fixed, so these t_*_2arr types are application dependent and need to be defined where used.
  type t_dp_siso_mat is array (integer range <>, integer range <>) of t_dp_siso;
  type t_dp_sosi_mat is array (integer range <>, integer range <>) of t_dp_sosi;

  -- Check sosi.valid against siso.ready
  procedure proc_dp_siso_alert(constant c_ready_latency : in    natural;
                               signal   clk             : in    std_logic;
                               signal   sosi            : in    t_dp_sosi;
                               signal   siso            : in    t_dp_siso;
                               signal   ready_reg       : inout std_logic_vector);

  -- Default RL=1
  procedure proc_dp_siso_alert(signal   clk             : in    std_logic;
                               signal   sosi            : in    t_dp_sosi;
                               signal   siso            : in    t_dp_siso;
                               signal   ready_reg       : inout std_logic_vector);

  -- SOSI/SISO array version
  procedure proc_dp_siso_alert(constant c_ready_latency : in    natural;
                               signal   clk             : in    std_logic;
                               signal   sosi_arr        : in    t_dp_sosi_arr;
                               signal   siso_arr        : in    t_dp_siso_arr;
                               signal   ready_reg       : inout std_logic_vector);

  -- SOSI/SISO array version with RL=1
  procedure proc_dp_siso_alert(signal   clk             : in    std_logic;
                               signal   sosi_arr        : in    t_dp_sosi_arr;
                               signal   siso_arr        : in    t_dp_siso_arr;
                               signal   ready_reg       : inout std_logic_vector);

  -- Resize functions to fit an integer or an SLV in the corresponding t_dp_sosi field width
  -- . Use these functions to assign sosi data TO a record field
  -- . Use the range selection [n-1 DOWNTO 0] to assign sosi data FROM a record field to an slv
  -- . The unused sosi data field bits could remain undefined 'X', because the unused bits in the fields are not used at all.
  --   Typically the sosi data are treated as unsigned in the record field, so extended with '0'. However for interpretating
  --   signed data in the simulation wave window it is easier to use sign extension in the record field. Therefore TO_DP_SDATA
  --   and RESIZE_DP_SDATA are defined as well.
  function TO_DP_BSN(     n : natural) return std_logic_vector;
  function TO_DP_DATA(    n : integer) return std_logic_vector;  -- use integer to support 32 bit range, so -1 = 0xFFFFFFFF = +2**32-1
  function TO_DP_SDATA(   n : integer) return std_logic_vector;  -- use integer to support 32 bit range and signed
  function TO_DP_UDATA(   n : integer) return std_logic_vector;  -- alias of TO_DP_DATA()
  function TO_DP_DSP_DATA(n : integer) return std_logic_vector;  -- for re and im fields, signed data
  function TO_DP_DSP_UDATA(n: integer) return std_logic_vector;  -- for re and im fields, unsigned data (useful to carry indices)
  function TO_DP_EMPTY(   n : natural) return std_logic_vector;
  function TO_DP_CHANNEL( n : natural) return std_logic_vector;
  function TO_DP_ERROR(   n : natural) return std_logic_vector;
  function RESIZE_DP_BSN(     vec : std_logic_vector) return std_logic_vector;
  function RESIZE_DP_DATA(    vec : std_logic_vector) return std_logic_vector;  -- set unused MSBits to '0'
  function RESIZE_DP_SDATA(   vec : std_logic_vector) return std_logic_vector;  -- sign extend unused MSBits
  function RESIZE_DP_XDATA(   vec : std_logic_vector) return std_logic_vector;  -- set unused MSBits to 'X'
  function RESIZE_DP_DSP_DATA(vec : std_logic_vector) return std_logic_vector;  -- sign extend unused MSBits of re and im fields
  function RESIZE_DP_EMPTY(   vec : std_logic_vector) return std_logic_vector;
  function RESIZE_DP_CHANNEL( vec : std_logic_vector) return std_logic_vector;
  function RESIZE_DP_ERROR(   vec : std_logic_vector) return std_logic_vector;

  function INCR_DP_DATA(    vec : std_logic_vector; dec : integer; w : natural) return std_logic_vector;  -- unsigned vec(w-1:0) + dec
  function INCR_DP_SDATA(   vec : std_logic_vector; dec : integer; w : natural) return std_logic_vector;  -- signed vec(w-1:0) + dec
  function INCR_DP_DSP_DATA(vec : std_logic_vector; dec : integer; w : natural) return std_logic_vector;  -- signed vec(w-1:0) + dec

  function REPLICATE_DP_DATA(  seq  : std_logic_vector                 ) return std_logic_vector;  -- replicate seq as often as fits in c_dp_stream_data_w
  function UNREPLICATE_DP_DATA(data : std_logic_vector; seq_w : natural) return std_logic_vector;  -- unreplicate data to width seq_w, return low seq_w bits and set mismatch MSbits bits to '1'

  function TO_DP_SOSI_UNSIGNED(sync, valid, sop, eop : std_logic; bsn, data, re, im, empty, channel, err : unsigned) return t_dp_sosi_unsigned;

  -- Keep part of head data and combine part of tail data, use the other sosi from head_sosi
  function func_dp_data_shift_first(head_sosi, tail_sosi : t_dp_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_tail              : natural) return t_dp_sosi;
  -- Shift and combine part of previous data and this data, use the other sosi from prev_sosi
  function func_dp_data_shift(      prev_sosi, this_sosi : t_dp_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_this              : natural) return t_dp_sosi;
  -- Shift part of tail data and account for input empty
  function func_dp_data_shift_last(            tail_sosi : t_dp_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_tail, input_empty : natural) return t_dp_sosi;

  -- Determine resulting empty if two streams are concatenated or split
  function func_dp_empty_concat(head_empty, tail_empty : std_logic_vector; nof_symbols_per_data : natural) return std_logic_vector;
  function func_dp_empty_split(input_empty, head_empty : std_logic_vector; nof_symbols_per_data : natural) return std_logic_vector;

  -- Multiplex the t_dp_sosi_arr based on the valid, assuming that at most one input is active valid.
  function func_dp_sosi_arr_mux(dp : t_dp_sosi_arr) return t_dp_sosi;

  -- Determine the combined logical value of corresponding STD_LOGIC fields in t_dp_*_arr (for all elements or only for the mask[]='1' elements)
  function func_dp_stream_arr_and(dp : t_dp_siso_arr; mask : std_logic_vector; str : string) return std_logic;
  function func_dp_stream_arr_and(dp : t_dp_sosi_arr; mask : std_logic_vector; str : string) return std_logic;
  function func_dp_stream_arr_and(dp : t_dp_siso_arr;                          str : string) return std_logic;
  function func_dp_stream_arr_and(dp : t_dp_sosi_arr;                          str : string) return std_logic;
  function func_dp_stream_arr_or( dp : t_dp_siso_arr; mask : std_logic_vector; str : string) return std_logic;
  function func_dp_stream_arr_or( dp : t_dp_sosi_arr; mask : std_logic_vector; str : string) return std_logic;
  function func_dp_stream_arr_or( dp : t_dp_siso_arr;                          str : string) return std_logic;
  function func_dp_stream_arr_or( dp : t_dp_sosi_arr;                          str : string) return std_logic;

  -- Functions to set or get a STD_LOGIC field as a STD_LOGIC_VECTOR to or from an siso or an sosi array
  function func_dp_stream_arr_set(dp : t_dp_siso_arr; slv : std_logic_vector; str : string) return t_dp_siso_arr;
  function func_dp_stream_arr_set(dp : t_dp_sosi_arr; slv : std_logic_vector; str : string) return t_dp_sosi_arr;
  function func_dp_stream_arr_set(dp : t_dp_siso_arr; sl  : std_logic;        str : string) return t_dp_siso_arr;
  function func_dp_stream_arr_set(dp : t_dp_sosi_arr; sl  : std_logic;        str : string) return t_dp_sosi_arr;
  function func_dp_stream_arr_get(dp : t_dp_siso_arr;                         str : string) return std_logic_vector;
  function func_dp_stream_arr_get(dp : t_dp_sosi_arr;                         str : string) return std_logic_vector;

  -- Functions to select elements from two siso or two sosi arrays (sel[] = '1' selects a, sel[] = '0' selects b)
  function func_dp_stream_arr_select(sel : std_logic_vector; a,                 b : t_dp_siso)     return t_dp_siso_arr;
  function func_dp_stream_arr_select(sel : std_logic_vector; a,                 b : t_dp_sosi)     return t_dp_sosi_arr;
  function func_dp_stream_arr_select(sel : std_logic_vector; a : t_dp_siso_arr; b : t_dp_siso)     return t_dp_siso_arr;
  function func_dp_stream_arr_select(sel : std_logic_vector; a : t_dp_sosi_arr; b : t_dp_sosi)     return t_dp_sosi_arr;
  function func_dp_stream_arr_select(sel : std_logic_vector; a : t_dp_siso;     b : t_dp_siso_arr) return t_dp_siso_arr;
  function func_dp_stream_arr_select(sel : std_logic_vector; a : t_dp_sosi;     b : t_dp_sosi_arr) return t_dp_sosi_arr;
  function func_dp_stream_arr_select(sel : std_logic_vector; a,                 b : t_dp_siso_arr) return t_dp_siso_arr;
  function func_dp_stream_arr_select(sel : std_logic_vector; a,                 b : t_dp_sosi_arr) return t_dp_sosi_arr;

  -- Fix reversed buses due to connecting TO to DOWNTO range arrays.
  function func_dp_stream_arr_reverse_range(in_arr : t_dp_sosi_arr) return t_dp_sosi_arr;
  function func_dp_stream_arr_reverse_range(in_arr : t_dp_siso_arr) return t_dp_siso_arr;

  -- Functions to combinatorially hold the data fields and to set or reset the control fields in an sosi array
  function func_dp_stream_arr_combine_data_info_ctrl(dp : t_dp_sosi_arr; info, ctrl : t_dp_sosi) return t_dp_sosi_arr;
  function func_dp_stream_arr_set_info(              dp : t_dp_sosi_arr; info       : t_dp_sosi) return t_dp_sosi_arr;
  function func_dp_stream_arr_set_control(           dp : t_dp_sosi_arr;       ctrl : t_dp_sosi) return t_dp_sosi_arr;
  function func_dp_stream_arr_reset_control(         dp : t_dp_sosi_arr                        ) return t_dp_sosi_arr;

  -- Reset sosi ctrl and preserve the sosi data (to avoid unnecessary data toggling and to ease data view in Wave window)
  function func_dp_stream_reset_control(dp : t_dp_sosi) return t_dp_sosi;

  -- Functions to combinatorially determine the maximum and minimum sosi bsn[w-1:0] value in the sosi array (for all elements or only for the mask[]='1' elements)
  function func_dp_stream_arr_bsn_max(dp : t_dp_sosi_arr; mask : std_logic_vector; w : natural) return std_logic_vector;
  function func_dp_stream_arr_bsn_max(dp : t_dp_sosi_arr;                          w : natural) return std_logic_vector;
  function func_dp_stream_arr_bsn_min(dp : t_dp_sosi_arr; mask : std_logic_vector; w : natural) return std_logic_vector;
  function func_dp_stream_arr_bsn_min(dp : t_dp_sosi_arr;                          w : natural) return std_logic_vector;

  -- Function to copy the BSN of one valid stream to all output streams.
  function func_dp_stream_arr_copy_valid_bsn(dp : t_dp_sosi_arr; mask : std_logic_vector) return t_dp_sosi_arr;

  -- Functions to combinatorially handle channels
  -- Note that the *_select and *_remove function are equivalent to dp_demux with g_combined=TRUE
  function func_dp_stream_channel_set   (st_sosi : t_dp_sosi; ch : natural) return t_dp_sosi;  -- select channel nr, add the channel field
  function func_dp_stream_channel_select(st_sosi : t_dp_sosi; ch : natural) return t_dp_sosi;  -- select channel nr, skip the channel field
  function func_dp_stream_channel_remove(st_sosi : t_dp_sosi; ch : natural) return t_dp_sosi;  -- skip channel nr

  -- Functions to combinatorially handle the error field
  function func_dp_stream_error_set(st_sosi : t_dp_sosi; n : natural) return t_dp_sosi;  -- force err = 0, is OK

  -- Functions to combinatorially handle the BSN field
  function func_dp_stream_bsn_set(st_sosi : t_dp_sosi; bsn : std_logic_vector) return t_dp_sosi;

  -- Functions to combine sosi fields
  function func_dp_stream_combine_info_and_data(info, data : t_dp_sosi) return t_dp_sosi;

  -- Functions to convert sosi fields
  function func_dp_stream_slv_to_integer(slv_sosi : t_dp_sosi; w : natural) return t_dp_sosi_integer;

  -- Functions to set the DATA, RE and IM field in a stream.
  function func_dp_stream_set_data(dp : t_dp_sosi;     slv : std_logic_vector; str : string                         ) return t_dp_sosi;
  function func_dp_stream_set_data(dp : t_dp_sosi_arr; slv : std_logic_vector; str : string                         ) return t_dp_sosi_arr;
  function func_dp_stream_set_data(dp : t_dp_sosi_arr; slv : std_logic_vector; str : string; mask : std_logic_vector) return t_dp_sosi_arr;

   -- Functions to rewire between concatenated sosi.data and concatenated sosi.re,im
   -- . data_order_im_re defines the concatenation order data = im&re or re&im
   -- . nof_data defines the number of concatenated streams that are concatenated in the sosi.data or sosi.re,im
   -- . rewire nof_data streams from data  to re,im and force data = X  to show that sosi data    is used
   -- . rewire nof_data streams from re,im to data  and force re,im = X to show that sosi complex is used
  function func_dp_stream_complex_to_data(dp : t_dp_sosi; data_w : natural; nof_data : natural; data_order_im_re : boolean) return t_dp_sosi;
  function func_dp_stream_complex_to_data(dp : t_dp_sosi; data_w : natural; nof_data : natural                            ) return t_dp_sosi;  -- data_order_im_re = TRUE
  function func_dp_stream_complex_to_data(dp : t_dp_sosi; data_w : natural                                                ) return t_dp_sosi;  -- data_order_im_re = TRUE, nof_data = 1
  function func_dp_stream_data_to_complex(dp : t_dp_sosi; data_w : natural; nof_data : natural; data_order_im_re : boolean) return t_dp_sosi;
  function func_dp_stream_data_to_complex(dp : t_dp_sosi; data_w : natural; nof_data : natural                            ) return t_dp_sosi;  -- data_order_im_re = TRUE
  function func_dp_stream_data_to_complex(dp : t_dp_sosi; data_w : natural                                                ) return t_dp_sosi;  -- data_order_im_re = TRUE, nof_data = 1

  function func_dp_stream_complex_to_data(dp_arr : t_dp_sosi_arr; data_w : natural; nof_data : natural; data_order_im_re : boolean) return t_dp_sosi_arr;
  function func_dp_stream_complex_to_data(dp_arr : t_dp_sosi_arr; data_w : natural; nof_data : natural                            ) return t_dp_sosi_arr;
  function func_dp_stream_complex_to_data(dp_arr : t_dp_sosi_arr; data_w : natural                                                ) return t_dp_sosi_arr;
  function func_dp_stream_data_to_complex(dp_arr : t_dp_sosi_arr; data_w : natural; nof_data : natural; data_order_im_re : boolean) return t_dp_sosi_arr;
  function func_dp_stream_data_to_complex(dp_arr : t_dp_sosi_arr; data_w : natural; nof_data : natural                            ) return t_dp_sosi_arr;
  function func_dp_stream_data_to_complex(dp_arr : t_dp_sosi_arr; data_w : natural                                                ) return t_dp_sosi_arr;

  -- Concatenate the data and complex re,im fields from a SOSI array into a single SOSI stream (assumes streams are in sync)
  function func_dp_stream_concat(snk_in_arr : t_dp_sosi_arr; data_w : natural) return t_dp_sosi;  -- Concat SOSI_ARR data into single SOSI
  function func_dp_stream_concat(src_in     : t_dp_siso; nof_streams : natural) return t_dp_siso_arr;  -- Wire single SISO to SISO_ARR

  -- Reconcatenate the data and complex re,im fields from a SOSI array from nof_data*in_w to nof_data*out_w
  -- . data_representation = "SIGNED"   treat sosi.data field as signed
  --                         "UNSIGNED" treat sosi.data field as unsigned
  --                         "COMPLEX"  treat sosi.data field as complex concatenated
  -- . data_order_im_re = TRUE  then "COMPLEX" data = im&re
  --                      FALSE then "COMPLEX" data = re&im
  --                      ignore when data_representation /= "COMPLEX"
  function func_dp_stream_reconcat(snk_in     : t_dp_sosi;     in_w, out_w, nof_data : natural; data_representation : string; data_order_im_re : boolean) return t_dp_sosi;
  function func_dp_stream_reconcat(snk_in     : t_dp_sosi;     in_w, out_w, nof_data : natural; data_representation : string                            ) return t_dp_sosi;
  function func_dp_stream_reconcat(snk_in_arr : t_dp_sosi_arr; in_w, out_w, nof_data : natural; data_representation : string; data_order_im_re : boolean) return t_dp_sosi_arr;
  function func_dp_stream_reconcat(snk_in_arr : t_dp_sosi_arr; in_w, out_w, nof_data : natural; data_representation : string                            ) return t_dp_sosi_arr;

  -- Deconcatenate data and complex re,im fields from SOSI into SOSI array
  function func_dp_stream_deconcat(snk_in      : t_dp_sosi; nof_streams, data_w : natural) return t_dp_sosi_arr;  -- Deconcat SOSI data
  function func_dp_stream_deconcat(src_out_arr : t_dp_siso_arr) return t_dp_siso;  -- Wire SISO_ARR(0) to single SISO
end dp_stream_pkg;

package body dp_stream_pkg is
  -- Check sosi.valid against siso.ready
  procedure proc_dp_siso_alert(constant c_ready_latency : in    natural;
                               signal   clk             : in    std_logic;
                               signal   sosi            : in    t_dp_sosi;
                               signal   siso            : in    t_dp_siso;
                               signal   ready_reg       : inout std_logic_vector) is
  begin
    ready_reg(0) <= siso.ready;
    -- Register siso.ready in c_ready_latency registers
    if rising_edge(clk) then
      -- Check DP sink
      if sosi.valid = '1' and ready_reg(c_ready_latency) = '0' then
        report "RL ERROR"
          severity FAILURE;
      end if;

      ready_reg( 1 to c_ready_latency) <= ready_reg( 0 to c_ready_latency - 1);
    end if;
  end proc_dp_siso_alert;

  -- Default RL=1
  procedure proc_dp_siso_alert(signal   clk             : in    std_logic;
                               signal   sosi            : in    t_dp_sosi;
                               signal   siso            : in    t_dp_siso;
                               signal   ready_reg       : inout std_logic_vector) is
  begin
    proc_dp_siso_alert(1, clk, sosi, siso, ready_reg);
  end proc_dp_siso_alert;

  -- SOSI/SISO array version
  procedure proc_dp_siso_alert(constant c_ready_latency : in    natural;
                               signal   clk             : in    std_logic;
                               signal   sosi_arr        : in    t_dp_sosi_arr;
                               signal   siso_arr        : in    t_dp_siso_arr;
                               signal   ready_reg       : inout std_logic_vector) is
  begin
    for i in 0 to sosi_arr'length - 1 loop
      ready_reg(i * (c_ready_latency + 1)) <= siso_arr(i).ready;  -- SLV is used as an array: nof_streams*(0..c_ready_latency)
    end loop;
    -- Register siso.ready in c_ready_latency registers
    if rising_edge(clk) then
      for i in 0 to sosi_arr'length - 1 loop
        -- Check DP sink
        if sosi_arr(i).valid = '1' and ready_reg(i * (c_ready_latency + 1) + 1) = '0' then
          report "RL ERROR"
            severity FAILURE;
        end if;
        ready_reg(i * (c_ready_latency + 1) + 1 to i * (c_ready_latency + 1) + c_ready_latency) <=  ready_reg(i * (c_ready_latency + 1) to i * (c_ready_latency + 1) + c_ready_latency - 1);
      end loop;
    end if;
  end proc_dp_siso_alert;

  -- SOSI/SISO array version with RL=1
  procedure proc_dp_siso_alert(signal   clk             : in    std_logic;
                               signal   sosi_arr        : in    t_dp_sosi_arr;
                               signal   siso_arr        : in    t_dp_siso_arr;
                               signal   ready_reg       : inout std_logic_vector) is
  begin
    proc_dp_siso_alert(1, clk, sosi_arr, siso_arr, ready_reg);
  end proc_dp_siso_alert;

  -- Resize functions to fit an integer or an SLV in the corresponding t_dp_sosi field width
  function TO_DP_BSN(n : natural) return std_logic_vector is
  begin
    return RESIZE_UVEC(TO_SVEC(n, 32), c_dp_stream_bsn_w);
  end TO_DP_BSN;

  function TO_DP_DATA(n : integer) return std_logic_vector is
  begin
    return RESIZE_UVEC(TO_SVEC(n, 32), c_dp_stream_data_w);
  end TO_DP_DATA;

  function TO_DP_SDATA(n : integer) return std_logic_vector is
  begin
    return RESIZE_SVEC(TO_SVEC(n, 32), c_dp_stream_data_w);
  end TO_DP_SDATA;

  function TO_DP_UDATA(n : integer) return std_logic_vector is
  begin
    return TO_DP_DATA(n);
  end TO_DP_UDATA;

  function TO_DP_DSP_DATA(n : integer) return std_logic_vector is
  begin
    return RESIZE_SVEC(TO_SVEC(n, 32), c_dp_stream_dsp_data_w);
  end TO_DP_DSP_DATA;

  function TO_DP_DSP_UDATA(n : integer) return std_logic_vector is
  begin
    return RESIZE_UVEC(TO_SVEC(n, 32), c_dp_stream_dsp_data_w);
  end TO_DP_DSP_UDATA;

  function TO_DP_EMPTY(n : natural) return std_logic_vector is
  begin
    return TO_UVEC(n, c_dp_stream_empty_w);
  end TO_DP_EMPTY;

  function TO_DP_CHANNEL(n : natural) return std_logic_vector is
  begin
    return TO_UVEC(n, c_dp_stream_channel_w);
  end TO_DP_CHANNEL;

  function TO_DP_ERROR(n : natural) return std_logic_vector is
  begin
    return TO_UVEC(n, c_dp_stream_error_w);
  end TO_DP_ERROR;

  function RESIZE_DP_BSN(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_dp_stream_bsn_w);
  end RESIZE_DP_BSN;

  function RESIZE_DP_DATA(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_dp_stream_data_w);
  end RESIZE_DP_DATA;

  function RESIZE_DP_SDATA(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_SVEC(vec, c_dp_stream_data_w);
  end RESIZE_DP_SDATA;

  function RESIZE_DP_XDATA(vec : std_logic_vector) return std_logic_vector is
    variable v_vec : std_logic_vector(c_dp_stream_data_w - 1 downto 0) := (others => 'X');
  begin
    v_vec(vec'length - 1 downto 0) := vec;
    return v_vec;
  end RESIZE_DP_XDATA;

  function RESIZE_DP_DSP_DATA(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_SVEC(vec, c_dp_stream_dsp_data_w);
  end RESIZE_DP_DSP_DATA;

  function RESIZE_DP_EMPTY(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_dp_stream_empty_w);
  end RESIZE_DP_EMPTY;

  function RESIZE_DP_CHANNEL(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_dp_stream_channel_w);
  end RESIZE_DP_CHANNEL;

  function RESIZE_DP_ERROR(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_dp_stream_error_w);
  end RESIZE_DP_ERROR;

  function INCR_DP_DATA(vec : std_logic_vector; dec : integer; w : natural) return std_logic_vector is
  begin
    return RESIZE_DP_DATA(std_logic_vector(unsigned(vec(w - 1 downto 0)) + dec));
  end INCR_DP_DATA;

  function INCR_DP_SDATA(vec : std_logic_vector; dec : integer; w : natural) return std_logic_vector is
  begin
    return RESIZE_DP_SDATA(std_logic_vector(signed(vec(w - 1 downto 0)) + dec));
  end INCR_DP_SDATA;

  function INCR_DP_DSP_DATA(vec : std_logic_vector; dec : integer; w : natural) return std_logic_vector is
  begin
    return RESIZE_DP_DSP_DATA(std_logic_vector(signed(vec(w - 1 downto 0)) + dec));
  end INCR_DP_DSP_DATA;

  function REPLICATE_DP_DATA(seq : std_logic_vector) return std_logic_vector is
    constant c_seq_w            : natural := seq'length;
    constant c_nof_replications : natural := ceil_div(c_dp_stream_data_w, c_seq_w);
    constant c_vec_w            : natural := ceil_value(c_dp_stream_data_w, c_seq_w);
    variable v_vec              : std_logic_vector(c_vec_w - 1 downto 0);
  begin
    for I in 0 to c_nof_replications - 1 loop
      v_vec((I + 1) * c_seq_w - 1 downto I * c_seq_w) := seq;
    end loop;
    return v_vec(c_dp_stream_data_w - 1 downto 0);
  end REPLICATE_DP_DATA;

  function UNREPLICATE_DP_DATA(data : std_logic_vector; seq_w :natural) return std_logic_vector is
    constant c_data_w           : natural := data'length;
    constant c_nof_replications : natural := ceil_div(c_data_w, seq_w);
    constant c_vec_w            : natural := ceil_value(c_data_w, seq_w);
    variable v_seq              : std_logic_vector(seq_w - 1 downto 0);
    variable v_data             : std_logic_vector(c_vec_w - 1 downto 0);
    variable v_vec              : std_logic_vector(c_vec_w - 1 downto 0);
  begin
    v_data := RESIZE_UVEC(data, c_vec_w);
    v_seq := v_data(seq_w - 1 downto 0);  -- low data part is the v_seq
    v_vec(seq_w - 1 downto 0) := v_seq;  -- keep v_seq at low part of return value
    if c_nof_replications > 1 then
      for I in 1 to c_nof_replications - 1 loop
        v_vec((I + 1) * seq_w - 1 downto I * seq_w) := v_data((I + 1) * seq_w - 1 downto I * seq_w) xor v_seq;  -- set return bit to '1' for high part data bits that do not match low part v_seq
      end loop;
    end if;
    return v_vec(c_data_w - 1 downto 0);
  end UNREPLICATE_DP_DATA;

  function TO_DP_SOSI_UNSIGNED(sync, valid, sop, eop : std_logic; bsn, data, re, im, empty, channel, err : unsigned) return t_dp_sosi_unsigned is
    variable v_sosi_unsigned : t_dp_sosi_unsigned;
  begin
    v_sosi_unsigned.sync    := sync;
    v_sosi_unsigned.valid   := valid;
    v_sosi_unsigned.sop     := sop;
    v_sosi_unsigned.eop     := eop;
    v_sosi_unsigned.bsn     := resize(bsn,     c_dp_stream_bsn_w);
    v_sosi_unsigned.data    := resize(data,    c_dp_stream_data_w);
    v_sosi_unsigned.re      := resize(re,      c_dp_stream_dsp_data_w);
    v_sosi_unsigned.im      := resize(im,      c_dp_stream_dsp_data_w);
    v_sosi_unsigned.empty   := resize(empty,   c_dp_stream_empty_w);
    v_sosi_unsigned.channel := resize(channel, c_dp_stream_channel_w);
    v_sosi_unsigned.err     := resize(err,     c_dp_stream_error_w);
    return v_sosi_unsigned;
  end TO_DP_SOSI_UNSIGNED;

  -- Keep part of head data and combine part of tail data
  function func_dp_data_shift_first(head_sosi, tail_sosi : t_dp_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_tail : natural) return t_dp_sosi is
    variable vN     : natural := nof_symbols_per_data;
    variable v_sosi : t_dp_sosi;
  begin
    assert nof_symbols_from_tail < vN
      report "func_dp_data_shift_first : no symbols from head"
      severity FAILURE;
    -- use the other sosi from head_sosi
    v_sosi := head_sosi;  -- I = nof_symbols_from_tail = 0
    for I in 1 to vN - 1 loop  -- I > 0
      if nof_symbols_from_tail = I then
        v_sosi.data(I * symbol_w - 1 downto 0) := tail_sosi.data(vN * symbol_w - 1 downto (vN - I) * symbol_w);
      end if;
    end loop;
    return v_sosi;
  end func_dp_data_shift_first;

  -- Shift and combine part of previous data and this data,
  function func_dp_data_shift(prev_sosi, this_sosi : t_dp_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_this : natural) return t_dp_sosi is
    variable vK     : natural := nof_symbols_from_this;
    variable vN     : natural := nof_symbols_per_data;
    variable v_sosi : t_dp_sosi;
  begin
    -- use the other sosi from this_sosi if nof_symbols_from_this > 0 else use other sosi from prev_sosi
    if vK > 0 then
      v_sosi := this_sosi;
    else
      v_sosi := prev_sosi;
    end if;

    -- use sosi data from both if 0 < nof_symbols_from_this < nof_symbols_per_data (i.e. 0 < I < vN)
    if vK < nof_symbols_per_data then  -- I = vK = nof_symbols_from_this < vN
      -- Implementation using variable vK directly instead of via I in a LOOP
      -- IF vK > 0 THEN
      --   v_sosi.data(vN*symbol_w-1 DOWNTO vK*symbol_w)            := prev_sosi.data((vN-vK)*symbol_w-1 DOWNTO                0);
      --   v_sosi.data(                     vK*symbol_w-1 DOWNTO 0) := this_sosi.data( vN    *symbol_w-1 DOWNTO (vN-vK)*symbol_w);
      -- END IF;
      -- Implementaion using LOOP vK rather than VARIABLE vK directly as index to help synthesis and avoid potential multiplier
      v_sosi.data := prev_sosi.data;  -- I = vK = nof_symbols_from_this = 0
      for I in 1 to vN - 1 loop  -- I = vK = nof_symbols_from_this > 0
        if vK = I then
          v_sosi.data(vN * symbol_w - 1 downto I * symbol_w)            := prev_sosi.data((vN - I) * symbol_w - 1 downto               0);
          v_sosi.data(                     I * symbol_w - 1 downto 0) := this_sosi.data( vN   * symbol_w - 1 downto (vN - I) * symbol_w);
        end if;
      end loop;
    end if;
    return v_sosi;
  end func_dp_data_shift;

  -- Shift part of tail data and account for input empty
  function func_dp_data_shift_last(tail_sosi : t_dp_sosi; symbol_w, nof_symbols_per_data, nof_symbols_from_tail, input_empty : natural) return t_dp_sosi is
    variable vK     : natural := nof_symbols_from_tail;
    variable vL     : natural := input_empty;
    variable vN     : natural := nof_symbols_per_data;
    variable v_sosi : t_dp_sosi;
  begin
    assert vK > 0
      report "func_dp_data_shift_last : no symbols from tail"
      severity FAILURE;
    assert vK + vL <= vN
      report "func_dp_data_shift_last : impossible shift"
      severity FAILURE;
    v_sosi := tail_sosi;
    -- Implementation using variable vK directly instead of via I in a LOOP
    -- IF vK > 0 THEN
    --   v_sosi.data(vN*symbol_w-1 DOWNTO (vN-vK)*symbol_w) <= tail_sosi.data((vK+vL)*symbol_w-1 DOWNTO vL*symbol_w);
    -- END IF;
    -- Implementation using LOOP vK rather than VARIABLE vK directly as index to help synthesis and avoid potential multiplier
    -- Implementation using LOOP vL rather than VARIABLE vL directly as index to help synthesis and avoid potential multiplier
    for I in 1 to vN - 1 loop
      if vK = I then
        for J in 0 to vN - 1 loop
          if vL = J then
            v_sosi.data(vN * symbol_w - 1 downto (vN - I) * symbol_w) := tail_sosi.data((I + J) * symbol_w - 1 downto J * symbol_w);
          end if;
        end loop;
      end if;
    end loop;
    return v_sosi;
  end func_dp_data_shift_last;

  -- Determine resulting empty if two streams are concatenated
  -- . both empty must use the same nof symbols per data
  function func_dp_empty_concat(head_empty, tail_empty : std_logic_vector; nof_symbols_per_data : natural) return std_logic_vector is
    variable v_a, v_b, v_empty : natural;
  begin
    v_a := TO_UINT(head_empty);
    v_b := TO_UINT(tail_empty);
    v_empty := v_a + v_b;
    if v_empty >= nof_symbols_per_data then
      v_empty := v_empty - nof_symbols_per_data;
    end if;
    return TO_UVEC(v_empty, head_empty'length);
  end func_dp_empty_concat;

  function func_dp_empty_split(input_empty, head_empty : std_logic_vector; nof_symbols_per_data : natural) return std_logic_vector is
    variable v_a, v_b, v_empty : natural;
  begin
    v_a   := TO_UINT(input_empty);
    v_b   := TO_UINT(head_empty);
    if v_a >= v_b then
      v_empty := v_a - v_b;
    else
      v_empty := (nof_symbols_per_data + v_a) - v_b;
    end if;
    return TO_UVEC(v_empty, head_empty'length);
  end func_dp_empty_split;

  -- Multiplex the t_dp_sosi_arr based on the valid, assuming that at most one input is active valid.
  function func_dp_sosi_arr_mux(dp : t_dp_sosi_arr) return t_dp_sosi is
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    for I in dp'range loop
      if dp(I).valid = '1' then
        v_sosi := dp(I);
        exit;
      end if;
    end loop;
    return v_sosi;
  end func_dp_sosi_arr_mux;

  -- Determine the combined logical value of corresponding STD_LOGIC fields in t_dp_*_arr (for all elements or only for the mask[]='1' elements)
  function func_dp_stream_arr_and(dp : t_dp_siso_arr; mask : std_logic_vector; str : string) return std_logic is
    variable v_vec : std_logic_vector(dp'range) := (others => '1');  -- set default v_vec such that unmasked input have no influence on operation result
    variable v_any : std_logic := '0';
  begin
    -- map siso field to v_vec
    for I in dp'range loop
      if mask(I) = '1' then
        v_any := '1';
        if    str = "READY" then v_vec(I) := dp(I).ready;
        elsif str = "XON"   then v_vec(I) := dp(I).xon;
        else  report "Error in func_dp_stream_arr_and for t_dp_siso_arr";
        end if;
      end if;
    end loop;
    -- do operation on the selected record field
    if v_any = '1' then
      return vector_and(v_vec);  -- return AND of the masked input fields
    else
      return '0';  -- return '0' if no input was masked
    end if;
  end func_dp_stream_arr_and;

  function func_dp_stream_arr_and(dp : t_dp_sosi_arr; mask : std_logic_vector; str : string) return std_logic is
    variable v_vec : std_logic_vector(dp'range) := (others => '1');  -- set default v_vec such that unmasked input have no influence on operation result
    variable v_any : std_logic := '0';
  begin
    -- map siso field to v_vec
    for I in dp'range loop
      if mask(I) = '1' then
        v_any := '1';
        if    str = "VALID" then v_vec(I) := dp(I).valid;
        elsif str = "SOP"   then v_vec(I) := dp(I).sop;
        elsif str = "EOP"   then v_vec(I) := dp(I).eop;
        elsif str = "SYNC"  then v_vec(I) := dp(I).sync;
        else  report "Error in func_dp_stream_arr_and for t_dp_sosi_arr";
        end if;
      end if;
    end loop;
    -- do operation on the selected record field
    if v_any = '1' then
      return vector_and(v_vec);  -- return AND of the masked input fields
    else
      return '0';  -- return '0' if no input was masked
    end if;
  end func_dp_stream_arr_and;

  function func_dp_stream_arr_and(dp : t_dp_siso_arr; str : string) return std_logic is
    constant c_mask : std_logic_vector(dp'range) := (others => '1');
  begin
    return func_dp_stream_arr_and(dp, c_mask, str);
  end func_dp_stream_arr_and;

  function func_dp_stream_arr_and(dp : t_dp_sosi_arr; str : string) return std_logic is
    constant c_mask : std_logic_vector(dp'range) := (others => '1');
  begin
    return func_dp_stream_arr_and(dp, c_mask, str);
  end func_dp_stream_arr_and;

  function func_dp_stream_arr_or(dp : t_dp_siso_arr; mask : std_logic_vector; str : string) return std_logic is
    variable v_vec : std_logic_vector(dp'range) := (others => '0');  -- set default v_vec such that unmasked input have no influence on operation result
    variable v_any : std_logic := '0';
  begin
    -- map siso field to v_vec
    for I in dp'range loop
      if mask(I) = '1' then
        v_any := '1';
        if    str = "READY" then v_vec(I) := dp(I).ready;
        elsif str = "XON"   then v_vec(I) := dp(I).xon;
        else  report "Error in func_dp_stream_arr_or for t_dp_siso_arr";
        end if;
      end if;
    end loop;
    -- do operation on the selected record field
    if v_any = '1' then
      return vector_or(v_vec);  -- return OR of the masked input fields
    else
      return '0';  -- return '0' if no input was masked
    end if;
  end func_dp_stream_arr_or;

  function func_dp_stream_arr_or(dp : t_dp_sosi_arr; mask : std_logic_vector; str : string) return std_logic is
    variable v_vec : std_logic_vector(dp'range) := (others => '0');  -- set default v_vec such that unmasked input have no influence on operation result
    variable v_any : std_logic := '0';
  begin
    -- map siso field to v_vec
    for I in dp'range loop
      if mask(I) = '1' then
        v_any := '1';
        if    str = "VALID" then v_vec(I) := dp(I).valid;
        elsif str = "SOP"   then v_vec(I) := dp(I).sop;
        elsif str = "EOP"   then v_vec(I) := dp(I).eop;
        elsif str = "SYNC"  then v_vec(I) := dp(I).sync;
        else  report "Error in func_dp_stream_arr_or for t_dp_sosi_arr";
        end if;
      end if;
    end loop;
    -- do operation on the selected record field
    if v_any = '1' then
      return vector_or(v_vec);  -- return OR of the masked input fields
    else
      return '0';  -- return '0' if no input was masked
    end if;
  end func_dp_stream_arr_or;

  function func_dp_stream_arr_or(dp : t_dp_siso_arr; str : string) return std_logic is
    constant c_mask : std_logic_vector(dp'range) := (others => '1');
  begin
    return func_dp_stream_arr_or(dp, c_mask, str);
  end func_dp_stream_arr_or;

  function func_dp_stream_arr_or(dp : t_dp_sosi_arr; str : string) return std_logic is
    constant c_mask : std_logic_vector(dp'range) := (others => '1');
  begin
    return func_dp_stream_arr_or(dp, c_mask, str);
  end func_dp_stream_arr_or;

  -- Functions to set or get a STD_LOGIC field as a STD_LOGIC_VECTOR to or from an siso or an sosi array
  function func_dp_stream_arr_set(dp : t_dp_siso_arr; slv : std_logic_vector; str : string) return t_dp_siso_arr is
    variable v_dp  : t_dp_siso_arr(dp'range)    := dp;  -- default
    variable v_slv : std_logic_vector(dp'range) := slv;  -- map to ensure same range as for dp
  begin
    for I in dp'range loop
      if    str = "READY" then v_dp(I).ready := v_slv(I);
      elsif str = "XON"   then v_dp(I).xon   := v_slv(I);
      else  report "Error in func_dp_stream_arr_set for t_dp_siso_arr";
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_set;

  function func_dp_stream_arr_set(dp : t_dp_sosi_arr; slv : std_logic_vector; str : string) return t_dp_sosi_arr is
    variable v_dp  : t_dp_sosi_arr(dp'range)    := dp;  -- default
    variable v_slv : std_logic_vector(dp'range) := slv;  -- map to ensure same range as for dp
  begin
    for I in dp'range loop
      if    str = "VALID" then v_dp(I).valid := v_slv(I);
      elsif str = "SOP"   then v_dp(I).sop   := v_slv(I);
      elsif str = "EOP"   then v_dp(I).eop   := v_slv(I);
      elsif str = "SYNC"  then v_dp(I).sync  := v_slv(I);
      else  report "Error in func_dp_stream_arr_set for t_dp_sosi_arr";
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_set;

  function func_dp_stream_arr_set(dp : t_dp_siso_arr; sl : std_logic; str : string) return t_dp_siso_arr is
    variable v_slv : std_logic_vector(dp'range) := (others => sl);
  begin
    return func_dp_stream_arr_set(dp, v_slv, str);
  end func_dp_stream_arr_set;

  function func_dp_stream_arr_set(dp : t_dp_sosi_arr; sl : std_logic; str : string) return t_dp_sosi_arr is
    variable v_slv : std_logic_vector(dp'range) := (others => sl);
  begin
    return func_dp_stream_arr_set(dp, v_slv, str);
  end func_dp_stream_arr_set;

  function func_dp_stream_arr_get(dp : t_dp_siso_arr; str : string) return std_logic_vector is
    variable v_ctrl : std_logic_vector(dp'range);
  begin
    for I in dp'range loop
      if    str = "READY" then v_ctrl(I) := dp(I).ready;
      elsif str = "XON"   then v_ctrl(I) := dp(I).xon;
      else  report "Error in func_dp_stream_arr_get for t_dp_siso_arr";
      end if;
    end loop;
    return v_ctrl;
  end func_dp_stream_arr_get;

  function func_dp_stream_arr_get(dp : t_dp_sosi_arr; str : string) return std_logic_vector is
    variable v_ctrl : std_logic_vector(dp'range);
  begin
    for I in dp'range loop
      if    str = "VALID" then v_ctrl(I) := dp(I).valid;
      elsif str = "SOP"   then v_ctrl(I) := dp(I).sop;
      elsif str = "EOP"   then v_ctrl(I) := dp(I).eop;
      elsif str = "SYNC"  then v_ctrl(I) := dp(I).sync;
      else  report "Error in func_dp_stream_arr_get for t_dp_sosi_arr";
      end if;
    end loop;
    return v_ctrl;
  end func_dp_stream_arr_get;

  -- Functions to select elements from two siso or two sosi arrays (sel[] = '1' selects a, sel[] = '0' selects b)
  function func_dp_stream_arr_select(sel : std_logic_vector; a, b : t_dp_siso) return t_dp_siso_arr is
    variable v_dp : t_dp_siso_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_dp(I) := a;
      else
        v_dp(I) := b;
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_select;

  function func_dp_stream_arr_select(sel : std_logic_vector; a : t_dp_siso_arr; b : t_dp_siso) return t_dp_siso_arr is
    variable v_dp : t_dp_siso_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_dp(I) := a(I);
      else
        v_dp(I) := b;
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_select;

  function func_dp_stream_arr_select(sel : std_logic_vector; a : t_dp_siso; b : t_dp_siso_arr) return t_dp_siso_arr is
    variable v_dp : t_dp_siso_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_dp(I) := a;
      else
        v_dp(I) := b(I);
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_select;

  function func_dp_stream_arr_select(sel : std_logic_vector; a, b : t_dp_siso_arr) return t_dp_siso_arr is
    variable v_dp : t_dp_siso_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_dp(I) := a(I);
      else
        v_dp(I) := b(I);
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_select;

  function func_dp_stream_arr_select(sel : std_logic_vector; a, b : t_dp_sosi) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_dp(I) := a;
      else
        v_dp(I) := b;
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_select;

  function func_dp_stream_arr_select(sel : std_logic_vector; a : t_dp_sosi_arr; b : t_dp_sosi) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_dp(I) := a(I);
      else
        v_dp(I) := b;
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_select;

  function func_dp_stream_arr_select(sel : std_logic_vector; a : t_dp_sosi; b : t_dp_sosi_arr) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_dp(I) := a;
      else
        v_dp(I) := b(I);
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_select;

  function func_dp_stream_arr_select(sel : std_logic_vector; a, b : t_dp_sosi_arr) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(sel'range);
  begin
    for I in sel'range loop
      if sel(I) = '1' then
        v_dp(I) := a(I);
      else
        v_dp(I) := b(I);
      end if;
    end loop;
    return v_dp;
  end func_dp_stream_arr_select;

  function func_dp_stream_arr_reverse_range(in_arr : t_dp_siso_arr) return t_dp_siso_arr is
    variable v_to_range : t_dp_siso_arr(0 to in_arr'high);
    variable v_downto_range : t_dp_siso_arr(in_arr'high downto 0);
  begin
    for i in in_arr'range loop
      v_to_range(i)     := in_arr(in_arr'high - i);
      v_downto_range(i) := in_arr(in_arr'high - i);
    end loop;
    if in_arr'left > in_arr'right then
      return v_downto_range;
    elsif in_arr'left < in_arr'right then
      return v_to_range;
    else
      return in_arr;
    end if;
  end func_dp_stream_arr_reverse_range;

  function func_dp_stream_arr_reverse_range(in_arr : t_dp_sosi_arr) return t_dp_sosi_arr is
    variable v_to_range : t_dp_sosi_arr(0 to in_arr'high);
    variable v_downto_range : t_dp_sosi_arr(in_arr'high downto 0);
  begin
    for i in in_arr'range loop
      v_to_range(i)     := in_arr(in_arr'high - i);
      v_downto_range(i) := in_arr(in_arr'high - i);
    end loop;
    if in_arr'left > in_arr'right then
      return v_downto_range;
    elsif in_arr'left < in_arr'right then
      return v_to_range;
    else
      return in_arr;
    end if;
  end func_dp_stream_arr_reverse_range;

  -- Functions to combinatorially hold the data fields and to set or reset the info and control fields in an sosi array
  function func_dp_stream_arr_combine_data_info_ctrl(dp : t_dp_sosi_arr; info, ctrl : t_dp_sosi) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(dp'range) := dp;  -- hold sosi data
  begin
    v_dp := func_dp_stream_arr_set_info(   v_dp, info);  -- set sosi info
    v_dp := func_dp_stream_arr_set_control(v_dp, ctrl);  -- set sosi ctrl
    return v_dp;
  end func_dp_stream_arr_combine_data_info_ctrl;

  function func_dp_stream_arr_set_info(dp : t_dp_sosi_arr; info : t_dp_sosi) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(dp'range) := dp;  -- hold sosi data
  begin
    for I in dp'range loop  -- set sosi info
      v_dp(I).bsn     := info.bsn;  -- sop
      v_dp(I).channel := info.channel;  -- sop
      v_dp(I).empty   := info.empty;  -- eop
      v_dp(I).err     := info.err;  -- eop
    end loop;
    return v_dp;
  end func_dp_stream_arr_set_info;

  function func_dp_stream_arr_set_control(dp : t_dp_sosi_arr; ctrl : t_dp_sosi) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(dp'range) := dp;  -- hold sosi data
  begin
    for I in dp'range loop  -- set sosi control
      v_dp(I).valid := ctrl.valid;
      v_dp(I).sop   := ctrl.sop;
      v_dp(I).eop   := ctrl.eop;
      v_dp(I).sync  := ctrl.sync;
    end loop;
    return v_dp;
  end func_dp_stream_arr_set_control;

  function func_dp_stream_arr_reset_control(dp : t_dp_sosi_arr) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(dp'range) := dp;  -- hold sosi data
  begin
    for I in dp'range loop  -- reset sosi control
      v_dp(I).valid := '0';
      v_dp(I).sop   := '0';
      v_dp(I).eop   := '0';
      v_dp(I).sync  := '0';
    end loop;
    return v_dp;
  end func_dp_stream_arr_reset_control;

  function func_dp_stream_reset_control(dp : t_dp_sosi) return t_dp_sosi is
    variable v_dp : t_dp_sosi := dp;  -- hold sosi data
  begin
    -- reset sosi control
    v_dp.valid := '0';
    v_dp.sop   := '0';
    v_dp.eop   := '0';
    v_dp.sync  := '0';
    return v_dp;
  end func_dp_stream_reset_control;

  -- Functions to combinatorially determine the maximum and minimum sosi bsn[w-1:0] value in the sosi array (for all elements or only for the mask[]='1' elements)
  function func_dp_stream_arr_bsn_max(dp : t_dp_sosi_arr; mask : std_logic_vector; w : natural) return std_logic_vector is
    variable v_bsn : std_logic_vector(w - 1 downto 0) := (others => '0');  -- init max v_bsn with minimum value
  begin
    for I in dp'range loop
      if mask(I) = '1' then
        if unsigned(v_bsn) < unsigned(dp(I).bsn(w - 1 downto 0)) then
          v_bsn := dp(I).bsn(w - 1 downto 0);
        end if;
      end if;
    end loop;
    return v_bsn;
  end func_dp_stream_arr_bsn_max;

  function func_dp_stream_arr_bsn_max(dp : t_dp_sosi_arr; w : natural) return std_logic_vector is
    constant c_mask : std_logic_vector(dp'range) := (others => '1');
  begin
    return func_dp_stream_arr_bsn_max(dp, c_mask, w);
  end func_dp_stream_arr_bsn_max;

  function func_dp_stream_arr_bsn_min(dp : t_dp_sosi_arr; mask : std_logic_vector; w : natural) return std_logic_vector is
    variable v_bsn : std_logic_vector(w - 1 downto 0) := (others => '1');  -- init min v_bsn with maximum value
  begin
    for I in dp'range loop
      if mask(I) = '1' then
        if unsigned(v_bsn) > unsigned(dp(I).bsn(w - 1 downto 0)) then
          v_bsn := dp(I).bsn(w - 1 downto 0);
        end if;
      end if;
    end loop;
    return v_bsn;
  end func_dp_stream_arr_bsn_min;

  function func_dp_stream_arr_bsn_min(dp : t_dp_sosi_arr; w : natural) return std_logic_vector is
    constant c_mask : std_logic_vector(dp'range) := (others => '1');
  begin
    return func_dp_stream_arr_bsn_min(dp, c_mask, w);
  end func_dp_stream_arr_bsn_min;

  -- Function to copy the BSN number of one valid stream to all other streams.
  function func_dp_stream_arr_copy_valid_bsn(dp : t_dp_sosi_arr; mask : std_logic_vector) return t_dp_sosi_arr is
    variable v_bsn : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := (others => '0');
    variable v_dp  : t_dp_sosi_arr(dp'range) := dp;  -- hold sosi data
  begin
    for I in dp'range loop
      if mask(I) = '1' then
        v_bsn := dp(I).bsn;
      end if;
    end loop;
    for I in dp'range loop
      v_dp(I).bsn := v_bsn;
    end loop;
    return v_dp;
  end func_dp_stream_arr_copy_valid_bsn;

  -- Functions to combinatorially handle channels
  function func_dp_stream_channel_set(st_sosi : t_dp_sosi; ch : natural) return t_dp_sosi is
    variable v_rec : t_dp_sosi := st_sosi;
  begin
    v_rec.channel := TO_UVEC(ch, c_dp_stream_channel_w);
    return v_rec;
  end func_dp_stream_channel_set;

  function func_dp_stream_channel_select(st_sosi : t_dp_sosi; ch : natural) return t_dp_sosi is
    variable v_rec : t_dp_sosi := st_sosi;
  begin
    if unsigned(st_sosi.channel) /= ch then
      v_rec.valid := '0';
      v_rec.sop   := '0';
      v_rec.eop   := '0';
    end if;
    return v_rec;
  end func_dp_stream_channel_select;

  function func_dp_stream_channel_remove(st_sosi : t_dp_sosi; ch : natural) return t_dp_sosi is
    variable v_rec : t_dp_sosi := st_sosi;
  begin
    if unsigned(st_sosi.channel) = ch then
      v_rec.valid := '0';
      v_rec.sop   := '0';
      v_rec.eop   := '0';
    end if;
    return v_rec;
  end func_dp_stream_channel_remove;

  function func_dp_stream_error_set(st_sosi : t_dp_sosi; n : natural) return t_dp_sosi is
    variable v_rec : t_dp_sosi := st_sosi;
  begin
    v_rec.err := TO_UVEC(n, c_dp_stream_error_w);
    return v_rec;
  end func_dp_stream_error_set;

  function func_dp_stream_bsn_set(st_sosi : t_dp_sosi; bsn : std_logic_vector) return t_dp_sosi is
    variable v_rec : t_dp_sosi := st_sosi;
  begin
    v_rec.bsn := RESIZE_DP_BSN(bsn);
    return v_rec;
  end func_dp_stream_bsn_set;

  function func_dp_stream_combine_info_and_data(info, data : t_dp_sosi) return t_dp_sosi is
    variable v_rec : t_dp_sosi := data;  -- Sosi data fields
  begin
    -- Combine sosi data with the sosi info fields
    v_rec.sync    := info.sync and data.sop;  -- force sync only active at data.sop
    v_rec.bsn     := info.bsn;
    v_rec.channel := info.channel;
    v_rec.empty   := info.empty;
    v_rec.err     := info.err;
    return v_rec;
  end func_dp_stream_combine_info_and_data;

  function func_dp_stream_slv_to_integer(slv_sosi : t_dp_sosi; w : natural) return t_dp_sosi_integer is
    variable v_rec : t_dp_sosi_integer;
  begin
    v_rec.sync     := slv_sosi.sync;
    v_rec.bsn      := TO_UINT(slv_sosi.bsn(30 downto 0));  -- NATURAL'width = 31 bit
    v_rec.data     := TO_SINT(slv_sosi.data(w - 1 downto 0));
    v_rec.re       := TO_SINT(slv_sosi.re(w - 1 downto 0));
    v_rec.im       := TO_SINT(slv_sosi.im(w - 1 downto 0));
    v_rec.valid    := slv_sosi.valid;
    v_rec.sop      := slv_sosi.sop;
    v_rec.eop      := slv_sosi.eop;
    v_rec.empty    := TO_UINT(slv_sosi.empty);
    v_rec.channel  := TO_UINT(slv_sosi.channel);
    v_rec.err      := TO_UINT(slv_sosi.err);
    return v_rec;
  end func_dp_stream_slv_to_integer;

  function func_dp_stream_set_data(dp : t_dp_sosi; slv : std_logic_vector; str : string) return t_dp_sosi is
    variable v_dp : t_dp_sosi := dp;
  begin
      if    str = "DATA" then v_dp.data := RESIZE_DP_DATA(slv);
      elsif str = "DSP"  then v_dp.re   := RESIZE_DP_DSP_DATA(slv);
                            v_dp.im   := RESIZE_DP_DSP_DATA(slv);
      elsif str = "RE"  then  v_dp.re   := RESIZE_DP_DSP_DATA(slv);
      elsif str = "IM"  then  v_dp.im   := RESIZE_DP_DSP_DATA(slv);
      elsif str = "ALL" then  v_dp.data := RESIZE_DP_DATA(slv);
                            v_dp.re   := RESIZE_DP_DSP_DATA(slv);
                            v_dp.im   := RESIZE_DP_DSP_DATA(slv);
      else  report "Error in func_dp_stream_set_data for t_dp_sosi";
      end if;
    return v_dp;
  end;

  function func_dp_stream_set_data(dp : t_dp_sosi_arr; slv : std_logic_vector; str : string) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(dp'range) := dp;
  begin
    for I in dp'range loop
      v_dp(I) := func_dp_stream_set_data(dp(I), slv, str);
    end loop;
    return v_dp;
  end;

  function func_dp_stream_set_data(dp : t_dp_sosi_arr; slv : std_logic_vector; str : string; mask : std_logic_vector) return t_dp_sosi_arr is
    variable v_dp : t_dp_sosi_arr(dp'range) := dp;
  begin
    for I in dp'range loop
      if mask(I) = '0' then
        v_dp(I) := func_dp_stream_set_data(dp(I), slv, str);
      end if;
    end loop;
    return v_dp;
  end;

   -- Functions to rewire between concatenated sosi.data and concatenated sosi.re,im
  function func_dp_stream_complex_to_data(dp : t_dp_sosi; data_w : natural; nof_data : natural; data_order_im_re : boolean) return t_dp_sosi is
    constant c_compl_data_w : natural := data_w / 2;
    variable v_dp           : t_dp_sosi := dp;
    variable v_re           : std_logic_vector(c_compl_data_w - 1 downto 0);
    variable v_im           : std_logic_vector(c_compl_data_w - 1 downto 0);
  begin
    v_dp.data := (others => '0');
    v_dp.re := (others => 'X');
    v_dp.im := (others => 'X');
    for I in 0 to nof_data - 1 loop
      v_re := dp.re(c_compl_data_w - 1 + I * c_compl_data_w downto I * c_compl_data_w);
      v_im := dp.im(c_compl_data_w - 1 + I * c_compl_data_w downto I * c_compl_data_w);
      if data_order_im_re = true then
        v_dp.data((I + 1) * data_w - 1 downto I * data_w) := v_im & v_re;
      else
        v_dp.data((I + 1) * data_w - 1 downto I * data_w) := v_re & v_im;
      end if;
    end loop;
    return v_dp;
  end;

  function func_dp_stream_complex_to_data(dp : t_dp_sosi; data_w : natural; nof_data : natural) return t_dp_sosi is
  begin
    return func_dp_stream_complex_to_data(dp, data_w, nof_data, true);
  end;

  function func_dp_stream_complex_to_data(dp : t_dp_sosi; data_w : natural) return t_dp_sosi is
  begin
    return func_dp_stream_complex_to_data(dp, data_w, 1, true);
  end;

  function func_dp_stream_data_to_complex(dp : t_dp_sosi; data_w : natural; nof_data : natural; data_order_im_re : boolean) return t_dp_sosi is
    constant c_compl_data_w : natural := data_w / 2;
    variable v_dp           : t_dp_sosi := dp;
    variable v_hi           : std_logic_vector(c_compl_data_w - 1 downto 0);
    variable v_lo           : std_logic_vector(c_compl_data_w - 1 downto 0);
  begin
    v_dp.data := (others => 'X');
    v_dp.re := (others => '0');
    v_dp.im := (others => '0');
    for I in 0 to nof_data - 1 loop
      v_hi := dp.data(        data_w - 1 + I * data_w downto c_compl_data_w + I * data_w);
      v_lo := dp.data(c_compl_data_w - 1 + I * data_w downto              0 + I * data_w);
      if data_order_im_re = true then
        v_dp.im((I + 1) * c_compl_data_w - 1 downto I * c_compl_data_w) := v_hi;
        v_dp.re((I + 1) * c_compl_data_w - 1 downto I * c_compl_data_w) := v_lo;
      else
        v_dp.re((I + 1) * c_compl_data_w - 1 downto I * c_compl_data_w) := v_hi;
        v_dp.im((I + 1) * c_compl_data_w - 1 downto I * c_compl_data_w) := v_lo;
      end if;
    end loop;
    return v_dp;
  end;

  function func_dp_stream_data_to_complex(dp : t_dp_sosi; data_w : natural; nof_data : natural) return t_dp_sosi is
  begin
    return func_dp_stream_data_to_complex(dp, data_w, nof_data, true);
  end;

  function func_dp_stream_data_to_complex(dp : t_dp_sosi; data_w : natural) return t_dp_sosi is
  begin
    return func_dp_stream_data_to_complex(dp, data_w, 1, true);
  end;

  function func_dp_stream_complex_to_data(dp_arr : t_dp_sosi_arr; data_w : natural; nof_data : natural; data_order_im_re : boolean) return t_dp_sosi_arr is
    variable v_dp_arr : t_dp_sosi_arr(dp_arr'range);
  begin
    for i in dp_arr'range loop
      v_dp_arr(i) := func_dp_stream_complex_to_data(dp_arr(i), data_w, nof_data, data_order_im_re);  -- nof_data per stream is 1
    end loop;
    return v_dp_arr;
  end;

  function func_dp_stream_complex_to_data(dp_arr : t_dp_sosi_arr; data_w : natural; nof_data : natural) return t_dp_sosi_arr is
  begin
    return func_dp_stream_complex_to_data(dp_arr, data_w, nof_data, true);
  end;

  function func_dp_stream_complex_to_data(dp_arr : t_dp_sosi_arr; data_w : natural) return t_dp_sosi_arr is
  begin
    return func_dp_stream_complex_to_data(dp_arr, data_w, 1, true);
  end;

  function func_dp_stream_data_to_complex(dp_arr : t_dp_sosi_arr; data_w : natural; nof_data : natural; data_order_im_re : boolean) return t_dp_sosi_arr is
    variable v_dp_arr : t_dp_sosi_arr(dp_arr'range);
  begin
    for i in dp_arr'range loop
      v_dp_arr(i) := func_dp_stream_data_to_complex(dp_arr(i), data_w, nof_data, data_order_im_re);  -- nof_data per stream is 1
    end loop;
    return v_dp_arr;
  end;

  function func_dp_stream_data_to_complex(dp_arr : t_dp_sosi_arr; data_w : natural; nof_data : natural) return t_dp_sosi_arr is
  begin
    return func_dp_stream_data_to_complex(dp_arr, data_w, nof_data, true);
  end;

  function func_dp_stream_data_to_complex(dp_arr : t_dp_sosi_arr; data_w : natural) return t_dp_sosi_arr is
  begin
    return func_dp_stream_data_to_complex(dp_arr, data_w, 1, true);
  end;

  -- Concatenate the data (and complex fields) from a SOSI array into a single SOSI stream (assumes streams are in sync)
  function func_dp_stream_concat(snk_in_arr : t_dp_sosi_arr; data_w : natural) return t_dp_sosi is
    constant c_compl_data_w : natural   := data_w / 2;
    variable v_src_out      : t_dp_sosi := snk_in_arr(0);
  begin
    v_src_out.data := (others => '0');
    v_src_out.re   := (others => '0');
    v_src_out.im   := (others => '0');
    for i in snk_in_arr'range loop
      v_src_out.data((i + 1) *        data_w - 1 downto i *        data_w) := snk_in_arr(i).data(      data_w - 1 downto 0);
      v_src_out.re(  (i + 1) * c_compl_data_w - 1 downto i * c_compl_data_w) := snk_in_arr(i).re(c_compl_data_w - 1 downto 0);
      v_src_out.im(  (i + 1) * c_compl_data_w - 1 downto i * c_compl_data_w) := snk_in_arr(i).im(c_compl_data_w - 1 downto 0);
    end loop;
    return v_src_out;
  end;

  function func_dp_stream_concat(src_in : t_dp_siso; nof_streams : natural) return t_dp_siso_arr is  -- Wire single SISO to SISO_ARR
    variable v_snk_out_arr : t_dp_siso_arr(nof_streams - 1 downto 0);
  begin
    for i in v_snk_out_arr'range loop
      v_snk_out_arr(i) := src_in;
    end loop;
    return v_snk_out_arr;
  end;

  -- Reconcatenate the data and complex re,im fields from a SOSI array from nof_data*in_w to nof_data*out_w
  function func_dp_stream_reconcat(snk_in : t_dp_sosi; in_w, out_w, nof_data : natural; data_representation : string; data_order_im_re : boolean) return t_dp_sosi is
    constant c_compl_in_w  : natural   := in_w / 2;
    constant c_compl_out_w : natural   := out_w / 2;
    variable v_src_out     : t_dp_sosi := snk_in;
    variable v_in_data     : std_logic_vector(in_w - 1 downto 0);
    variable v_out_data    : std_logic_vector(out_w - 1 downto 0) := (others => '0');  -- default set sosi.data to 0
  begin
    v_src_out := snk_in;
    v_src_out.data := (others => '0');
    v_src_out.re   := (others => '0');
    v_src_out.im   := (others => '0');
    for i in 0 to nof_data - 1 loop
      v_in_data := snk_in.data((i + 1) * in_w - 1 downto i * in_w);
      if data_representation = "UNSIGNED" then  -- treat data as unsigned
        v_out_data := RESIZE_UVEC(v_in_data, out_w);
      else
        if data_representation = "SIGNED" then  -- treat data as signed
          v_out_data := RESIZE_SVEC(v_in_data, out_w);
        else
          -- treat data as complex
          if data_order_im_re = true then
            -- data = im&re
            v_out_data := RESIZE_SVEC(v_in_data(2 * c_compl_in_w - 1 downto c_compl_in_w), c_compl_out_w) &
                          RESIZE_SVEC(v_in_data(  c_compl_in_w - 1 downto            0), c_compl_out_w);
          else
            -- data = re&im
            v_out_data := RESIZE_SVEC(v_in_data(  c_compl_in_w - 1 downto            0), c_compl_out_w) &
                          RESIZE_SVEC(v_in_data(2 * c_compl_in_w - 1 downto c_compl_in_w), c_compl_out_w);
          end if;
        end if;
      end if;
      v_src_out.data((i + 1) *        out_w - 1 downto i *        out_w) := v_out_data;
      v_src_out.re(  (i + 1) * c_compl_out_w - 1 downto i * c_compl_out_w) := RESIZE_SVEC(snk_in.re((i + 1) * c_compl_in_w - 1 downto i * c_compl_in_w), c_compl_out_w);
      v_src_out.im(  (i + 1) * c_compl_out_w - 1 downto i * c_compl_out_w) := RESIZE_SVEC(snk_in.im((i + 1) * c_compl_in_w - 1 downto i * c_compl_in_w), c_compl_out_w);
    end loop;
    return v_src_out;
  end;

  function func_dp_stream_reconcat(snk_in : t_dp_sosi; in_w, out_w, nof_data : natural; data_representation : string) return t_dp_sosi is
  begin
    return func_dp_stream_reconcat(snk_in, in_w, out_w, nof_data, data_representation, true);
  end;

  function func_dp_stream_reconcat(snk_in_arr : t_dp_sosi_arr; in_w, out_w, nof_data : natural; data_representation : string; data_order_im_re : boolean) return t_dp_sosi_arr is
    variable v_src_out_arr : t_dp_sosi_arr(snk_in_arr'range) := snk_in_arr;
  begin
    for i in v_src_out_arr'range loop
      v_src_out_arr(i) := func_dp_stream_reconcat(snk_in_arr(i), in_w, out_w, nof_data, data_representation, data_order_im_re);
    end loop;
    return v_src_out_arr;
  end;

  function func_dp_stream_reconcat(snk_in_arr : t_dp_sosi_arr; in_w, out_w, nof_data : natural; data_representation : string) return t_dp_sosi_arr is
  begin
    return func_dp_stream_reconcat(snk_in_arr, in_w, out_w, nof_data, data_representation, true);
  end;

  -- Deconcatenate data from SOSI into SOSI array
  function func_dp_stream_deconcat(snk_in : t_dp_sosi; nof_streams, data_w : natural) return t_dp_sosi_arr is
    constant c_compl_data_w : natural := data_w / 2;
    variable v_src_out_arr  : t_dp_sosi_arr(nof_streams - 1 downto 0);
  begin
    for i in v_src_out_arr'range loop
      v_src_out_arr(i) := snk_in;
      v_src_out_arr(i).data := (others => '0');
      v_src_out_arr(i).re   := (others => '0');
      v_src_out_arr(i).im   := (others => '0');
      v_src_out_arr(i).data := RESIZE_DP_DATA(    snk_in.data((i + 1) *        data_w - 1 downto i *        data_w));
      v_src_out_arr(i).re   := RESIZE_DP_DSP_DATA(snk_in.re  ((i + 1) * c_compl_data_w - 1 downto i * c_compl_data_w));
      v_src_out_arr(i).im   := RESIZE_DP_DSP_DATA(snk_in.im  ((i + 1) * c_compl_data_w - 1 downto i * c_compl_data_w));
    end loop;
    return v_src_out_arr;
  end;

  function func_dp_stream_deconcat(src_out_arr : t_dp_siso_arr) return t_dp_siso is  -- Wire SISO_ARR(0) to single SISO
  begin
    return src_out_arr(0);
  end;
end dp_stream_pkg;
