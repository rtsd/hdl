-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- Select subbands from incoming blocks
-- Description:
-- * The Crosslet subband select selects N_crosslets from each incoming block.
--   Per crosslet there are S_pn = 12 subbands, one from each signal input of
--   the PN.
-- * The cur_crosslets_info is valid at the out_sosi.sync and for the entire
--   sync interval. The cur_crosslets_info identifies the crosslets that are
--   being calculated during this out_sosi.sync interval.
--   The prev_crosslets_info identifies the crosslets that were calculated
--   during the previous out_sosi.sync interval, so the XST for those crosslets
--   are then pending to be offloaded.
-- * The new_interval is active during the first in_sosi_arr.sync interval. The
--   out_sosi.sync is active one sop period after the in_sosi_arr.sync. Hence
--   the new_interval is active about one block before the first out_sosi.sync
--   and inactive about one block before the next out_sosi.sync, so
--   new_interval can be used to know when a new sequence of out_sosi.sync
--   intervals starts.
-- Remark:
-- . See L5 SDPFW Design Document: Subband Correlator
--   Link: https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L5+SDPFW+Design+Document%3A+Subband+Correlator
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, reorder_lib, st_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use work.sdp_pkg.all;

entity sdp_crosslets_subband_select is
  generic (
    g_N_crosslets : natural := c_sdp_N_crosslets_max;
    g_ctrl_interval_size_min : natural := c_sdp_N_clk_per_sync_min
  );
  port (
    dp_clk         : in  std_logic;
    dp_rst         : in  std_logic;

    in_sosi_arr    : in  t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
    out_sosi       : out t_dp_sosi;

    new_interval       : out std_logic;
    ctrl_interval_size : out natural;

    mm_rst         : in  std_logic;
    mm_clk         : in  std_logic;

    reg_crosslets_info_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_crosslets_info_miso : out t_mem_miso := c_mem_miso_rst;

    reg_bsn_sync_scheduler_xsub_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_sync_scheduler_xsub_miso : out t_mem_miso := c_mem_miso_rst;

    cur_crosslets_info_rec  : out t_sdp_crosslets_info;
    prev_crosslets_info_rec : out t_sdp_crosslets_info
  );
end sdp_crosslets_subband_select;

architecture str of sdp_crosslets_subband_select is
  constant c_crosslets_info_dly  : natural := 1;
  constant c_col_select_addr_w   : natural := ceil_log2(c_sdp_Q_fft * c_sdp_N_sub);
  constant c_row_select_slv_w    : natural := ceil_log2(c_sdp_P_pfb);
  constant c_row_select_pipeline : natural := 1;
  constant c_out_sosi_pipeline   : natural := 1;

  type t_crosslets_control_reg is record  -- local registers
    offset_index     : natural;
    row_index        : natural;
    col_index        : natural;
    step             : natural range 0 to c_sdp_N_sub - 1;
    offsets          : t_natural_arr(g_N_crosslets - 1 downto 0);
    started          : std_logic;
    row_select_slv   : std_logic_vector(c_row_select_slv_w - 1 downto 0);
    col_select_mosi  : t_mem_mosi;
    sync_detected    : std_logic;
  end record;

  constant c_reg_rst : t_crosslets_control_reg :=
                           ( 0, 0, 0, 0, (others => 0), '0', (others => '0'), c_mem_mosi_rst, '0');

  -- Define the local registers in t_crosslets_control_reg record
  signal r     : t_crosslets_control_reg;
  signal nxt_r : t_crosslets_control_reg;

  signal start_trigger   : std_logic := '0';

  signal col_select_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal col_select_miso : t_mem_miso := c_mem_miso_rst;
  signal row_select_slv  : std_logic_vector(c_row_select_slv_w - 1 downto 0);

  signal dp_bsn_sync_scheduler_src_out_arr : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal col_sosi_arr :  t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
  signal row_sosi     :  t_dp_sosi;

  signal crosslets_info_reg    : std_logic_vector(c_sdp_crosslets_info_reg_w - 1 downto 0) := (others => '0');
  signal crosslets_info_reg_in : std_logic_vector(c_sdp_crosslets_info_reg_w - 1 downto 0) := (others => '0');
  signal active_crosslets_info : std_logic_vector(c_sdp_crosslets_info_reg_w - 1 downto 0) := (others => '0');
  signal cur_crosslets_info    : std_logic_vector(c_sdp_crosslets_info_reg_w - 1 downto 0) := (others => '0');
  signal prev_crosslets_info   : std_logic_vector(c_sdp_crosslets_info_reg_w - 1 downto 0) := (others => '0');

  -- Map crosslets_info slv to record for easier view in Wave window
  signal crosslets_info_rec        : t_sdp_crosslets_info;
  signal crosslets_info_rec_inout  : t_sdp_crosslets_info;
  signal active_crosslets_info_rec : t_sdp_crosslets_info;
begin
  ---------------------------------------------------------------
  -- BSN sync scheduler
  ---------------------------------------------------------------
  u_mmp_dp_bsn_sync_scheduler_arr : entity dp_lib.mmp_dp_bsn_sync_scheduler_arr
  generic map (
    g_nof_streams            => c_sdp_P_pfb,
    g_block_size             => c_sdp_N_fft,
    g_ctrl_interval_size_min => g_ctrl_interval_size_min
  )
  port map (
    dp_rst   => dp_rst,
    dp_clk   => dp_clk,
    mm_rst   => mm_rst,
    mm_clk   => mm_clk,

    reg_mosi => reg_bsn_sync_scheduler_xsub_mosi,
    reg_miso => reg_bsn_sync_scheduler_xsub_miso,
    reg_ctrl_interval_size => ctrl_interval_size,

    in_sosi_arr  => in_sosi_arr,
    out_sosi_arr => dp_bsn_sync_scheduler_src_out_arr,

    out_start => start_trigger,
    out_start_interval => new_interval
  );

  ---------------------------------------------------------------
  -- Crosslets info
  ---------------------------------------------------------------
  u_crosslets_info : entity common_lib.mms_common_reg
  generic map(
    g_mm_reg => c_sdp_mm_reg_crosslets_info
  )
  port map(
    -- Clocks and reset
    mm_rst => mm_rst,
    mm_clk => mm_clk,
    st_rst => dp_rst,
    st_clk => dp_clk,

    -- MM bus access in memory-mapped clock domain
    reg_mosi => reg_crosslets_info_mosi,
    reg_miso => reg_crosslets_info_miso,

    in_reg   => crosslets_info_reg_in,
    out_reg  => crosslets_info_reg
  );

  p_set_unused_crosslets : process(cur_crosslets_info)
  begin
    -- MM readback the currently active crosslets info, instead of the initial MM written crosslets_info_reg
    crosslets_info_reg_in <= cur_crosslets_info;  -- Always use crosslets info 6:0 + step(@ index 15)
    -- Set crosslets 14:7 to -1
    for I in g_N_crosslets to c_sdp_mm_reg_crosslets_info.nof_dat - 2 loop
      crosslets_info_reg_in((I + 1) * c_sdp_crosslets_index_w - 1 downto I * c_sdp_crosslets_index_w ) <=
                                                                                   TO_SVEC(-1, c_sdp_crosslets_index_w);
    end loop;
  end process;

  ---------------------------------------------------------------
  -- Crosslets control process
  ---------------------------------------------------------------
  p_regs_crosslets_control : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(dp_clk) then
      r <= nxt_r;
    end if;
  end process;

  p_comb_crosslets_control : process(r, start_trigger, crosslets_info_reg,
                                     dp_bsn_sync_scheduler_src_out_arr, col_select_miso)
    variable v : t_crosslets_control_reg;
    -- Use extra variable to simplify col_select_mosi address selection.
    -- Also using v_offsets instead of v.offsets to clearly indicate we do not only use this variable on the left side
    -- but also on the right side of assignments.
    variable v_offsets : t_natural_arr(g_N_crosslets - 1 downto 0);
  begin
    v := r;
    v.col_select_mosi := c_mem_mosi_rst;
    v_offsets := r.offsets;

    -- start/restart
    if start_trigger = '1' then
      v.started       := '1';  -- Once started r.started remains active. This is to prevent read/write actions before
                               -- the initial start_trigger.
      v.offset_index  := 0;
      v.row_index     := 0;
      v.col_index     := 0;
      v.sync_detected := '0';  -- set sync_detected to 0 in the case that a sync has been detected before the initial
                               -- start_trigger.

      -- start_trigger is active on the sync so we can immediatly reset the offsets/step such that they are used in
      -- the next packet. It is up to the user to schedule the start trigger on a BSN that coincides with a sync
      -- interval if that is desired.
      v.step := TO_UINT(crosslets_info_reg(c_sdp_crosslets_info_reg_w - 1 downto
                                           c_sdp_crosslets_info_reg_w - c_sdp_crosslets_index_w));
      for I in 0 to g_N_crosslets - 1 loop
        v_offsets(I) := TO_UINT(crosslets_info_reg((I + 1) * c_sdp_crosslets_index_w - 1 downto
                                                   I * c_sdp_crosslets_index_w));
      end loop;
    end if;

    -- Do not set sync_detected if start_trigger = 1 because the first sync interval after (re)start
    -- already has set the indices for the first interval and we do not want to increase them with the step.
    if dp_bsn_sync_scheduler_src_out_arr(0).sync = '1' and start_trigger = '0' then
      v.sync_detected := '1';
    end if;

    if r.started = '1' then  -- Once started r.started remains active.
      -- add step to offsets
      -- . using r.sync_detected to change offsets 1 packet after the sync due to the buffered packet in
      --   reorder_col_wide_select
      if dp_bsn_sync_scheduler_src_out_arr(0).eop = '1' and r.sync_detected = '1' then
        v.sync_detected := '0';
        for I in 0 to g_N_crosslets - 1 loop
          v_offsets(I) := r.offsets(I) + r.step;
        end loop;
      end if;

      -- Make col/row selection
      if col_select_miso.waitrequest = '0' then
        if r.col_index >= c_sdp_Q_fft - 1 then
          v.col_index := 0;
          if r.row_index >= c_sdp_P_pfb - 1 then
            v.row_index := 0;
            if r.offset_index >= g_N_crosslets - 1 then
              v.offset_index := 0;
            else
              v.offset_index := r.offset_index + 1;
            end if;
          else
            v.row_index := r.row_index + 1;
          end if;
        else
          v.col_index := r.col_index + 1;
        end if;

        v.col_select_mosi.rd := '1';
        v.col_select_mosi.address(c_col_select_addr_w - 1 downto 0) :=
                                   TO_UVEC(c_sdp_Q_fft * v_offsets(r.offset_index) + r.col_index, c_col_select_addr_w);
        v.row_select_slv := TO_UVEC(r.row_index, c_row_select_slv_w);
      end if;
    end if;
    v.offsets := v_offsets;
    nxt_r <= v;
  end process;

  col_select_mosi <= r.col_select_mosi;
  -- pipeline to time row select
  u_pipe_row_select : entity common_lib.common_pipeline
  generic map(
    g_pipeline => c_row_select_pipeline,
    g_in_dat_w => c_row_select_slv_w,
    g_out_dat_w => c_row_select_slv_w
  )
  port map(
    rst => dp_rst,
    clk => dp_clk,
    in_dat => r.row_select_slv,
    out_dat => row_select_slv
  );

  ---------------------------------------------------------------
  -- Crosslet Select
  ---------------------------------------------------------------
  u_reorder_col_wide_select : entity reorder_lib.reorder_col_wide_select
  generic map (
    g_nof_inputs         => c_sdp_P_pfb,
    g_dsp_data_w         => c_sdp_W_crosslet,
    g_nof_ch_in          => c_sdp_N_sub * c_sdp_Q_fft,
    g_nof_ch_sel         => g_N_crosslets * c_sdp_S_pn
  )
  port map (
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,

    -- Memory Mapped
    col_select_mosi    => col_select_mosi,
    col_select_miso    => col_select_miso,

    -- Streaming
    input_sosi_arr     => dp_bsn_sync_scheduler_src_out_arr,

    output_sosi_arr    => col_sosi_arr
  );

  u_reorder_row_select : entity reorder_lib.reorder_row_select
  generic map (
    g_dsp_data_w         => c_sdp_W_crosslet,
    g_nof_inputs         => c_sdp_P_pfb,
    g_nof_outputs        => 1,
    g_pipeline_in        => 0,
    g_pipeline_in_m      => 1,
    g_pipeline_out       => 1
  )
  port map (
    dp_rst         => dp_rst,
    dp_clk         => dp_clk,

    in_select      => row_select_slv,

    -- Streaming
    input_sosi_arr     => col_sosi_arr,

    output_sosi_arr(0) => row_sosi
  );

  ---------------------------------------------------------------
  -- Out Crosslet info pipeline
  ---------------------------------------------------------------
  active_crosslets_info(c_sdp_crosslets_info_reg_w - 1 downto c_sdp_crosslets_info_reg_w - c_sdp_crosslets_index_w) <=
                                                                               TO_UVEC(r.step, c_sdp_crosslets_index_w);

  gen_crosslets_info : for I in 0 to g_N_crosslets - 1 generate
    active_crosslets_info((I + 1) * c_sdp_crosslets_index_w - 1 downto I * c_sdp_crosslets_index_w) <=
                                                                         TO_UVEC(r.offsets(I), c_sdp_crosslets_index_w);
  end generate;

  -- pipeline for alignment with sync
  u_common_pipeline_cur : entity common_lib.common_pipeline
  generic map(
    g_pipeline  => c_crosslets_info_dly,
    g_in_dat_w  => c_sdp_crosslets_info_reg_w,
    g_out_dat_w => c_sdp_crosslets_info_reg_w
  )
  port map(
    rst => dp_rst,
    clk => dp_clk,
    in_en => row_sosi.sync,
    in_dat => active_crosslets_info,
    out_dat => cur_crosslets_info
  );

  u_common_pipeline_prev : entity common_lib.common_pipeline
  generic map(
    g_pipeline  => c_crosslets_info_dly,
    g_in_dat_w  => c_sdp_crosslets_info_reg_w,
    g_out_dat_w => c_sdp_crosslets_info_reg_w
  )
  port map(
    rst => dp_rst,
    clk => dp_clk,
    in_en => row_sosi.sync,
    in_dat => cur_crosslets_info,
    out_dat => prev_crosslets_info
  );

  ---------------------------------------------------------------
  -- Out sosi pipeline
  ---------------------------------------------------------------
  u_dp_pipeline : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline => c_out_sosi_pipeline
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => row_sosi,
    -- ST source
    src_out      => out_sosi
  );

  -- Map crosslets_info slv to record for easier view in Wave window
  crosslets_info_rec        <= func_sdp_map_crosslets_info(crosslets_info_reg);
  crosslets_info_rec_inout  <= func_sdp_map_crosslets_info(crosslets_info_reg_in);
  active_crosslets_info_rec <= func_sdp_map_crosslets_info(active_crosslets_info);

  cur_crosslets_info_rec    <= func_sdp_map_crosslets_info(cur_crosslets_info);
  prev_crosslets_info_rec   <= func_sdp_map_crosslets_info(prev_crosslets_info);
end str;
