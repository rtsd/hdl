-------------------------------------------------------------------------------
--
-- Copyright (C) 2010-2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

-- Purpose: Provide the basic node clock control (resets, pulses, WDI)
-- Description:
--   . Create mm_rst for mm_clk:
--   . Extend WDI to avoid watchdog reset during software reload
--   . Pulse every 1 us, 1 ms and 1 s

entity unb2b_board_node_ctrl is
  generic (
    g_pulse_us     : natural := 125;  -- nof system clock cycles to get us period, equal to system clock frequency / 10**6
    g_pulse_ms     : natural := 1000;  -- nof pulse_us pulses to get ms period (fixed, use less to speed up simulation)
    g_pulse_s      : natural := 1000;  -- nof pulse_ms pulses to get  s period (fixed, use less to speed up simulation)
    g_wdi_extend_w : natural := 14  -- extend the mm_wdi_in by toggling the mm_wdi_out for about 2**(14-1)= 8 s more
  );
  port (
    -- MM clock domain reset
    mm_clk          : in  std_logic;  -- MM clock
    mm_locked       : in  std_logic := '1';  -- MM clock PLL locked (or use default '1')
    mm_rst          : out std_logic;  -- MM reset released after MM clock PLL has locked

    -- WDI extend
    mm_wdi_in       : in  std_logic;  -- from software running on the NIOS2 in the SOPC design
    mm_wdi_out      : out std_logic;  -- to FPGA pin

    -- Pulses
    mm_pulse_us     : out std_logic;  -- pulses every us
    mm_pulse_ms     : out std_logic;  -- pulses every ms
    mm_pulse_s      : out std_logic  -- pulses every s
  );
end unb2b_board_node_ctrl;

architecture str of unb2b_board_node_ctrl is
  constant c_reset_len   : natural := 4;  -- >= c_meta_delay_len from common_pkg

  signal mm_locked_n     : std_logic;
  signal i_mm_rst        : std_logic;
  signal i_mm_pulse_ms   : std_logic;
begin
  -- Create mm_rst reset in mm_clk domain based on mm_locked
  mm_rst <= i_mm_rst;

  mm_locked_n <= not mm_locked;

  u_common_areset_mm : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => mm_locked_n,  -- release reset after some clock cycles when the PLL has locked
    clk       => mm_clk,
    out_rst   => i_mm_rst
  );

  -- Create 1 pulse per us, per ms and per s
  mm_pulse_ms <= i_mm_pulse_ms;

  u_common_pulser_us_ms_s : entity common_lib.common_pulser_us_ms_s
  generic map (
    g_pulse_us  => g_pulse_us,
    g_pulse_ms  => g_pulse_ms,
    g_pulse_s   => g_pulse_s
  )
  port map (
    rst         => i_mm_rst,
    clk         => mm_clk,
    pulse_us    => mm_pulse_us,
    pulse_ms    => i_mm_pulse_ms,
    pulse_s     => mm_pulse_s
  );

  -- Toggle the WDI every 1 ms
  u_unb2b_board_wdi_extend : entity work.unb2b_board_wdi_extend
  generic map (
    g_extend_w => g_wdi_extend_w
  )
  port map (
    rst        => i_mm_rst,
    clk        => mm_clk,
    pulse_ms   => i_mm_pulse_ms,
    wdi_in     => mm_wdi_in,
    wdi_out    => mm_wdi_out
  );
end str;
