-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_lvdsh_dd_phs4 is
end tb_tb_lvdsh_dd_phs4;

architecture tb of tb_tb_lvdsh_dd_phs4 is
  constant c_dp_phs_clk_period : natural := 32;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 3
  -- > run -all

  -- g_dclk_drift         : TIME := 0 ps;       -- 0 ps, use -2 ps or 2 ps to model the range of sample phase uncertainty by letting the sclk and dclk drift with respect to the dp_clk,
  -- g_dclk_offon         : BOOLEAN := TRUE;    -- when TRUE switch the dclk off-on periodically, to model ADU not present or ADU restart or ADU replaced
  -- g_dp_phs_clk_period  : NATURAL := 32;      -- number of dp_clk periods per dp_phs_clk period
  -- g_nof_dp_phs_clk     : NATURAL := 1;       -- nof dp_phs_clk that can be used to detect the lock
  -- g_dp_phs_clk_en_mask : NATURAL := 16#FF#;  -- bit mask to indiviually enable or disable a dp_phs_clk in range [g_nof_dp_phs_clk-1:0]
  -- g_dp_phs_clk_en_vary : BOOLEAN := FALSE;   -- use FALSE to use g_dp_phs_clk_en_mask, else use TRUE to vary g_dp_phs_clk_en_vec (assuming g_nof_dp_phs_clk>1)
  -- g_in_phase           : NATURAL := 1        -- 0:3

  u_phase_0    : entity work.tb_lvdsh_dd_phs4 generic map (0 ps, false, c_dp_phs_clk_period, 1, 16#FF#, false, 0);
  u_phase_1    : entity work.tb_lvdsh_dd_phs4 generic map (0 ps, false, c_dp_phs_clk_period, 1, 16#FF#, false, 1);
  u_phase_2    : entity work.tb_lvdsh_dd_phs4 generic map (0 ps, false, c_dp_phs_clk_period, 1, 16#FF#, false, 2);
  u_phase_3    : entity work.tb_lvdsh_dd_phs4 generic map (0 ps, false, c_dp_phs_clk_period, 1, 16#FF#, false, 3);
end tb;
