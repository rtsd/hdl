--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
-- Description:
--  . Block diagram:
--                                                                                  <-- tx_clk_312
--                                                                                  <-- tx_clk_156
--                                                                                  <-- rx_clk_312
--                                                                                  <-- rx_clk_156
--                         g_pre_header_padding |
--                                              | tx_remove_snk      tx_mac_snk
--              __________                      |  .  ______________  .  __________________
--              |        |                      |  .  |            |  .  |                |
--              | dp_pad |                      |  .  | dp_latency |  .  |                |
--    tx_snk -->| remove |----------------------|---->| adapter    |---->|                |
--              |        |                      |     | RL=1 --> 0 |     |                |
--              |________|                      |     |____________|     |                |
--                                              |                        |tech_mac_10g    |
--              __________     ______________   |     ______________     |ip_<device_name>|---XGMII
--              |        |     |            |   |     |            |     |                |
--              | dp_pad |     | dp_latency |   |     | dp_latency |     |                |
--    rx_src <--| insert |<----| fifo       |<--|-----| adapter    |<----|                |
--              |        |  .  |            |   |  .  | RL=1 --> 0 |  .  |                |
--              |________|  .  |____________|   |  .  |____________|  .  |________________|
--                          .                   |  .                  .           |
--                          x_fifo_src          | rx_mac_src_rl1     rx_mac_src   |
--                                              |                                 |
--                                              |                                csr
--
-- . Clocks:
--   The 312.5 MHz and 156.25 MHz clocks are inputs to the MAC-10G.
--   - For 10GBASE-R the Tx and Rx clocks come from the same local reference PLL.
--   - For XAUI the 312.5 MHz is not used. The Tx clock comes from the local
--     reference PLL. The Rx clock comes from the Rx PHY, so the Rx clock is
--     then a different clock domain than the Tx clock and the Rx clock may then
--     have some ppm drift with respect to the Tx clock.
--
-- . dp_latency_adapter:
--   The dp_latency_adapters are needed to adjust to RL=1 for the DP interface.
--
-- . g_pre_header_padding:
--   If g_pre_header_padding is TRUE then remove pre header padding from ST tx
--   and insert it in ST rx, because the 10G MAC IP does not use pre header
--   padding. When g_pre_header_padding is FALSE then the ST frame is directly
--   passed on via the dp_latency_adapters. The dp_latency_adapters are needed
--   because the ST interface uses RL=1 whereas e.g. the Altera mac_10g IP uses
--   RL=0.
--   The dp_latency_fifo is necessary because the rx_mac_src_in.ready must remain
--   active during a reception, otherwise the rx frame gets truncated due to
--   back pressure by dp_pad_insert.
--

library IEEE, common_lib, dp_lib, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.tech_mac_10g_component_pkg.all;

entity tech_mac_10g is
  generic (
    g_technology          : natural := c_tech_select_default;
    g_pre_header_padding  : boolean := false
  );
  port (
    -- MM
    mm_clk            : in  std_logic;
    mm_rst            : in  std_logic;
    csr_mosi          : in  t_mem_mosi;  -- CSR = control status register
    csr_miso          : out t_mem_miso;

    -- ST
    tx_clk_312        : in  std_logic := '0';  -- 312.5 MHz from local reference or not used, dependent on g_technology
    tx_clk_156        : in  std_logic;  -- 156.25 MHz from local reference
    tx_rst            : in  std_logic;  -- for tx_clk_156 clock domain
    tx_snk_in         : in  t_dp_sosi;  -- 64 bit data
    tx_snk_out        : out t_dp_siso;

    rx_clk_312        : in  std_logic := '0';  -- 312.5 MHz from local reference or not used, dependent on g_technology
    rx_clk_156        : in  std_logic;  -- 156.25 MHz from local reference or from rx phy, dependent on g_technology
    rx_rst            : in  std_logic;  -- for rx_clk_156 clock domain
    rx_src_out        : out t_dp_sosi;  -- 64 bit data
    rx_src_in         : in  t_dp_siso;

    -- XGMII
    xgmii_link_status : out std_logic_vector(c_tech_mac_10g_link_status_w - 1 downto 0);  -- 2 bit
    xgmii_tx_data     : out std_logic_vector(c_xgmii_w - 1 downto 0);  -- 72 bit
    xgmii_rx_data     : in  std_logic_vector(c_xgmii_w - 1 downto 0)  -- 72 bit
  );
end tech_mac_10g;

architecture str of tech_mac_10g is
  -- Adapt ST ready latency 1 to IP ready latency
  constant c_ip_tx_ready_latency : natural := sel_a_b(g_technology = c_tech_stratixiv or g_technology = c_tech_arria10_proto or g_technology = c_tech_arria10_e3sge3 or g_technology = c_tech_arria10_e1sg or g_technology = c_tech_arria10_e2sg, 0, 1);
  constant c_ip_rx_ready_latency : natural := sel_a_b(g_technology = c_tech_stratixiv or g_technology = c_tech_arria10_proto or g_technology = c_tech_arria10_e3sge3 or g_technology = c_tech_arria10_e1sg or g_technology = c_tech_arria10_e2sg, 0, 1);

  signal tx_mac_snk_in_data  : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);  -- 64 bit
  signal tx_mac_snk_in       : t_dp_sosi;
  signal tx_mac_snk_out      : t_dp_siso;
  signal tx_remove_snk_in    : t_dp_sosi;
  signal tx_remove_snk_out   : t_dp_siso;

  signal rx_mac_src_out_data : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);  -- 64 bit
  signal rx_mac_src_out      : t_dp_sosi := c_dp_sosi_rst;  -- force unused bits to '0' to ease monitoring in wave window
  signal rx_mac_src_in       : t_dp_siso;
  signal rx_mac_src_out_rl1  : t_dp_sosi;
  signal rx_mac_src_in_rl1   : t_dp_siso;
  signal rx_fifo_src_out     : t_dp_sosi;
  signal rx_fifo_src_in      : t_dp_siso;
  signal rx_insert_src_out   : t_dp_sosi;
  signal rx_insert_src_in    : t_dp_siso;

  constant c_fifo_size       : natural := 2;  -- latency to insert pre header padding, can take ready low for 2 cycles

  signal fifo_usedw          : std_logic_vector(ceil_log2(3 + c_fifo_size) - 1 downto 0);  -- +3 to match dp_latency_fifo usedw width
  signal fifo_ful            : std_logic;
  signal fifo_emp            : std_logic;
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : entity work.tech_mac_10g_stratixiv
    port map (mm_clk, mm_rst, csr_mosi, csr_miso,
              tx_clk_156, tx_rst, tx_mac_snk_in, tx_mac_snk_out,
              rx_clk_156, rx_rst, rx_mac_src_out, rx_mac_src_in,
              xgmii_link_status, xgmii_tx_data, xgmii_rx_data);
  end generate;

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : entity work.tech_mac_10g_arria10
    port map (mm_clk, mm_rst, csr_mosi, csr_miso,
              tx_clk_312, tx_clk_156, tx_rst, tx_mac_snk_in, tx_mac_snk_out,
              rx_clk_312, rx_clk_156, rx_rst, rx_mac_src_out, rx_mac_src_in,
              xgmii_link_status, xgmii_tx_data, xgmii_rx_data);
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : entity work.tech_mac_10g_arria10_e3sge3
    port map (mm_clk, mm_rst, csr_mosi, csr_miso,
              tx_clk_312, tx_clk_156, tx_rst, tx_mac_snk_in, tx_mac_snk_out,
              rx_clk_312, rx_clk_156, rx_rst, rx_mac_src_out, rx_mac_src_in,
              xgmii_link_status, xgmii_tx_data, xgmii_rx_data);
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : entity work.tech_mac_10g_arria10_e1sg
    port map (mm_clk, mm_rst, csr_mosi, csr_miso,
              tx_clk_312, tx_clk_156, tx_rst, tx_mac_snk_in, tx_mac_snk_out,
              rx_clk_312, rx_clk_156, rx_rst, rx_mac_src_out, rx_mac_src_in,
              xgmii_link_status, xgmii_tx_data, xgmii_rx_data);
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : entity work.tech_mac_10g_arria10_e2sg
    port map (mm_clk, mm_rst, csr_mosi, csr_miso,
              tx_clk_312, tx_clk_156, tx_rst, tx_mac_snk_in, tx_mac_snk_out,
              rx_clk_312, rx_clk_156, rx_rst, rx_mac_src_out, rx_mac_src_in,
              xgmii_link_status, xgmii_tx_data, xgmii_rx_data);
  end generate;

  -----------------------------------------------------------------------------
  -- Debug signals to ease monitoring in wave window
  -----------------------------------------------------------------------------

  tx_mac_snk_in_data  <= tx_mac_snk_in.data(c_tech_mac_10g_data_w - 1 downto 0);
  rx_mac_src_out_data <= rx_mac_src_out.data(c_tech_mac_10g_data_w - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Tx
  -----------------------------------------------------------------------------

  -- If g_pre_header_padding is TRUE then tx remove pre header padding else bypass
  gen_tx_remove : if g_pre_header_padding = true generate
    u_tx_dp_pad_remove : entity dp_lib.dp_pad_remove
    generic map (
      g_data_w          => c_tech_mac_10g_data_w,
      g_symbol_w        => c_tech_mac_10g_symbol_w,
      g_nof_padding     => c_network_total_header_64b_align_len
    )
    port map (
      rst     => tx_rst,
      clk     => tx_clk_156,
      snk_out => tx_snk_out,
      snk_in  => tx_snk_in,
      src_in  => tx_remove_snk_out,
      src_out => tx_remove_snk_in
    );
  end generate;

  no_tx_remove : if g_pre_header_padding = false generate
    tx_snk_out       <= tx_remove_snk_out;
    tx_remove_snk_in <= tx_snk_in;
  end generate;

  u_tx_dp_latency_adapter : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency  => 1,
    g_out_latency => c_ip_tx_ready_latency
  )
  port map (
    rst       => tx_rst,
    clk       => tx_clk_156,
    snk_out   => tx_remove_snk_out,
    snk_in    => tx_remove_snk_in,
    src_in    => tx_mac_snk_out,
    src_out   => tx_mac_snk_in
  );

  -----------------------------------------------------------------------------
  -- Rx
  -----------------------------------------------------------------------------

  u_rx_dp_latency_adapter : entity dp_lib.dp_latency_adapter
  generic map (
    g_in_latency  => c_ip_rx_ready_latency,
    g_out_latency => 1
  )
  port map (
    rst       => rx_rst,
    clk       => rx_clk_156,
    snk_out   => rx_mac_src_in,
    snk_in    => rx_mac_src_out,
    src_in    => rx_mac_src_in_rl1,
    src_out   => rx_mac_src_out_rl1
  );

  -- If g_pre_header_padding is TRUE then rx insert pre header padding to data word align the payload else bypass
  gen_rx_insert : if g_pre_header_padding = true generate
    -- Need FIFO to avoid that with dp_pad_insert flow control stops the rx MAC
    u_rx_dp_latency_fifo : entity dp_lib.dp_latency_fifo
    generic map (
      g_fifo_size  => c_fifo_size
    )
    port map (
      rst       => rx_rst,
      clk       => rx_clk_156,
      usedw     => fifo_usedw,
      wr_ful    => fifo_ful,
      rd_emp    => fifo_emp,
      snk_out   => rx_mac_src_in_rl1,
      snk_in    => rx_mac_src_out_rl1,
      src_in    => rx_fifo_src_in,
      src_out   => rx_fifo_src_out
    );

    u_rx_dp_pad_insert : entity dp_lib.dp_pad_insert
    generic map (
      g_data_w          => c_tech_mac_10g_data_w,
      g_symbol_w        => c_tech_mac_10g_symbol_w,
      g_nof_padding     => c_network_total_header_64b_align_len
    )
    port map (
      rst     => rx_rst,
      clk     => rx_clk_156,
      snk_out => rx_fifo_src_in,
      snk_in  => rx_fifo_src_out,
      src_in  => rx_insert_src_in,
      src_out => rx_insert_src_out
    );

    rx_insert_src_in <= rx_src_in;
    rx_src_out       <= rx_insert_src_out;
  end generate;

  no_rx_insert : if g_pre_header_padding = false generate
    rx_mac_src_in_rl1 <= rx_src_in;
    rx_src_out        <= rx_mac_src_out_rl1;
  end generate;
end str;
