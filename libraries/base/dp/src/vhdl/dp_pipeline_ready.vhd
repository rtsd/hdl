-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Pipeline the source input
-- Description:
--   This dp_pipeline_ready provides a single clock cycle delay of the source
--   input (i.e. siso). It does this by first going from RL = g_in_latency -->
--   0 and then to RL = g_out_latency.
-- Data flow:
--   . out RL >  in RL                : incr(out RL - in RL)
--   . out RL <= in RL AND out RL = 0 : incr(1) --> adapt(out RL)
--   . out RL <= in RL AND out RL > 0 : adapt(0) --> incr(out RL)
-- Remark:
-- . The g_in_latency may be 0, but for g_in_latency=0 the sosi.ready acts
--   as an acknowledge and that could simply also be registered by the user.

entity dp_pipeline_ready is
  generic (
    g_in_latency   : natural := 1;  -- >= 0
    g_out_latency  : natural := 1  -- >= 0
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;
    -- ST source
    src_in       : in  t_dp_siso;
    src_out      : out t_dp_sosi
  );
end dp_pipeline_ready;

architecture str of dp_pipeline_ready is
  signal internal_siso  : t_dp_siso;
  signal internal_sosi  : t_dp_sosi;
begin
  gen_out_incr_rl : if g_out_latency > g_in_latency generate
    -- Register siso by incrementing the input RL first
    u_incr : entity work.dp_latency_increase
    generic map (
      g_in_latency   => g_in_latency,
      g_incr_latency => g_out_latency - g_in_latency
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => snk_out,
      snk_in       => snk_in,
      -- ST source
      src_in       => src_in,
      src_out      => src_out
    );
  end generate;

  gen_out_rl_0 : if g_out_latency <= g_in_latency and g_out_latency = 0 generate
    -- Register siso by incrementing the input RL first
    u_incr : entity work.dp_latency_increase
    generic map (
      g_in_latency   => g_in_latency,
      g_incr_latency => 1
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => snk_out,
      snk_in       => snk_in,
      -- ST source
      src_in       => internal_siso,
      src_out      => internal_sosi
    );

    -- Input RL --> 0
    u_adapt : entity work.dp_latency_adapter
    generic map (
      g_in_latency   => g_in_latency + 1,
      g_out_latency  => g_out_latency
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => internal_siso,
      snk_in       => internal_sosi,
      -- ST source
      src_in       => src_in,
      src_out      => src_out
    );
  end generate;

  gen_out_rl : if g_out_latency <= g_in_latency and g_out_latency > 0 generate
    -- First adapt the input RL --> 0
    u_adapt : entity work.dp_latency_adapter
    generic map (
      g_in_latency   => g_in_latency,
      g_out_latency  => 0
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => snk_out,
      snk_in       => snk_in,
      -- ST source
      src_in       => internal_siso,
      src_out      => internal_sosi
    );

    -- Register siso by incrementing the internal RL = 0 --> the output RL
    u_incr : entity work.dp_latency_increase
    generic map (
      g_in_latency   => 0,
      g_incr_latency => g_out_latency
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => internal_siso,
      snk_in       => internal_sosi,
      -- ST source
      src_in       => src_in,
      src_out      => src_out
    );
  end generate;
end str;
