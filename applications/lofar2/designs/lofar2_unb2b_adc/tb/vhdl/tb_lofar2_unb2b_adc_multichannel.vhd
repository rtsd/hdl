-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: Jonathan Hargreaves
-- Purpose: Tb to show that lofar2_unb2b_adc can simulate
-- Description:
--   Must use c_sim = TRUE to speed up simulation
--   This is a compile-only test bench
-- Usage:
--   Load sim    # check that design can load in vsim
--   > as 10     # check that the hierarchy for g_design_name is complete
--   > run -a    # check that design can simulate some us without error

library IEEE, common_lib, unb2b_board_lib, i2c_lib, ip_arria10_e1sg_jesd204b_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use common_lib.tb_common_pkg.all;
use ip_arria10_e1sg_jesd204b_lib.ip_arria10_e1sg_jesd204b_component_pkg.all;

entity tb_lofar2_unb2b_adc_multichannel is
end tb_lofar2_unb2b_adc_multichannel;

architecture tb of tb_lofar2_unb2b_adc_multichannel is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;  -- Back node 3
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2b_board_fw_version := (1, 0);

  constant c_eth_clk_period  : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period  : time := 5 ns;
  constant c_jesd204b_sampclk_period  : time := 5 ns;
  constant c_pps_period      : natural := 1000;
  constant c_bondingclk_period  : time := 10 ns;
  constant c_sysref_period   : natural := 10000;  -- number of sample clocks between sysref pulses

  -- Transport delays
  type t_time_arr            is array (0 to 11) of time;
  constant c_nof_jesd204b_tx    : natural := 3;  -- number of jesd204b input sources to instantiate
  constant c_delay_data_arr     : t_time_arr := (4000 ps,
                                                 5000 ps,
                                                 6000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps,
                                                 5000 ps);  -- transport delays tx to rx data
  constant c_delay_sysreftoadc_arr : t_time_arr := (4000 ps,
                                                 5000 ps,
                                                 6000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps,
                                                 1000 ps);  -- transport delays clock source to adc(tx)
  constant c_delay_sysreftofpga : time := 10200 ps;

  -- clocks and resets for the jesd204b tx
  signal txlink_clk          : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal dev_sync_n          : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal txphy_clk           : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal mm_rst              : std_logic;
  signal avs_rst_n           : std_logic;
  signal txlink_rst_n        : std_logic;
  signal tx_analogreset      : std_logic_vector(0 downto 0);
  signal tx_digitalreset     : std_logic_vector(0 downto 0);
  signal tx_bonding_clocks   : std_logic_vector(5 downto 0) := (others => '0');
  signal bonding_clock_0     : std_logic := '0';
  signal bonding_clock_1     : std_logic := '0';
  signal bonding_clock_2     : std_logic := '0';
  signal bonding_clock_3     : std_logic := '0';
  signal bonding_clock_4     : std_logic := '0';
  signal bonding_clock_5     : std_logic := '0';
  signal pll_locked          : std_logic_vector(0 downto 0);

  constant c_mm_clk_period   : time := 20 ns;
  signal mm_clk              : std_logic := '0';

  -- Tb
  signal tb_end                      : std_logic := '0';
  signal sim_done                    : std_logic := '0';

  -- DUT
  signal ext_clk                     : std_logic := '0';
  signal pps                         : std_logic := '0';
  signal pps_rst                     : std_logic := '0';

  signal WDI                         : std_logic;
  signal INTA                        : std_logic;
  signal INTB                        : std_logic;

  signal eth_clk                     : std_logic := '0';
  signal eth_txp                     : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
  signal eth_rxp                     : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

  signal sens_scl                    : std_logic;
  signal sens_sda                    : std_logic;
  signal pmbus_scl                   : std_logic;
  signal pmbus_sda                   : std_logic;

  -- serial transceivers
  signal serial_tx                   : std_logic_vector(c_nof_jesd204b_tx - 1 downto 0);
  signal bck_rx                      : std_logic_vector((c_unb2b_board_tr_jesd204b.bus_w * c_unb2b_board_tr_jesd204b.nof_bus) - 1 downto 0) := (others => '0');

  -- jesd204b syncronization signals and delayed copies
  signal jesd204b_sysref             : std_logic;
  signal jesd204b_sampclk            : std_logic := '0';

  signal jesd204b_sampclk_fpga       : std_logic := '1';
  signal jesd204b_sampclk_adc        : std_logic_vector(11 downto 0);
  signal jesd204b_sysref_fpga        : std_logic;
  signal jesd204b_sysref_adc         : std_logic_vector(11 downto 0);
  signal jesd204b_sysref_adc_1       : std_logic_vector(11 downto 0);
  signal jesd204b_sysref_adc_2       : std_logic_vector(11 downto 0);
  signal jesd204b_sync_n_adc         : std_logic_vector(11 downto 0);
  signal jesd204b_sync_n_fpga        : std_logic_vector(11 downto 0);

  -- Test bench data
  signal jesd204b_tx_link_data_arr   : t_slv_32_arr(11 downto 0);
  signal jesd204b_tx_link_valid      : std_logic_vector(11 downto 0);
  signal jesd204b_tx_link_ready      : std_logic_vector(11 downto 0);
  signal jesd204b_tx_frame_ready     : std_logic_vector(11 downto 0);

  -- Diagnostic signals
  signal avs_chipselect              : std_logic_vector(11 downto 0);
  signal avs_read                    : std_logic_vector(11 downto 0);
  signal avs_readdata                : t_slv_32_arr(11 downto 0);
  signal avs_address                 : t_slv_8_arr(11 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, ext_clk, pps);
  --jesd204b_sysref <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2b_adc : entity work.lofar2_unb2b_adc
  generic map (
    g_design_name => "lofar2_unb2b_adc_one_node",
    g_design_note => "Lofar2 adc with one node",
    g_sim         => c_sim,
    g_sim_unb_nr  => c_unb_nr,
    g_sim_node_nr => c_node_nr
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- I2C Interface to Sensors
    SENS_SC      => sens_scl,
    SENS_SD      => sens_sda,

    PMBUS_SC     => pmbus_scl,
    PMBUS_SD     => pmbus_sda,
    PMBUS_ALERT  => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    JESD204B_SERIAL_DATA    => bck_rx,
    JESD204B_REFCLK         => jesd204b_sampclk_fpga,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => jesd204b_sysref_fpga,
    JESD204B_SYNC_N   => jesd204b_sync_n_fpga
  );

  -----------------------------------------------------------------------------
  -- Transport
  -----------------------------------------------------------------------------

  gen_transport : for i in 0 to c_nof_jesd204b_tx - 1 generate
    jesd204b_sampclk_adc(i) <= transport jesd204b_sampclk after c_delay_sysreftoadc_arr(i);
    jesd204b_sysref_adc(i)  <= transport jesd204b_sysref after c_delay_sysreftoadc_arr(i);
--    txlink_clk(i) <= jesd204b_sampclk_div2 after c_delay_sysreftoadc_arr(i);
    bck_rx(i) <= transport serial_tx(i) after c_delay_data_arr(i);
    jesd204b_sync_n_adc(i) <= transport jesd204b_sync_n_fpga(i) after c_delay_data_arr(i);
  end generate;

  jesd204b_sampclk_fpga <= transport jesd204b_sampclk after c_delay_sysreftofpga;
  jesd204b_sysref_fpga <= transport jesd204b_sysref after c_delay_sysreftofpga;

  -----------------------------------------------------------------------------
  -- Use a jesd204b instance in TX-ONLY modeTransmit Only.
  -----------------------------------------------------------------------------

  gen_jesd204b_tx : for i in 0 to c_nof_jesd204b_tx - 1 generate
    u_ip_arria10_e1sg_jesd204b_tx : ip_arria10_e1sg_jesd204b_tx
      port map
      (
        csr_cf                     => OPEN,
        csr_cs                     => OPEN,
        csr_f                      => OPEN,
        csr_hd                     => OPEN,
        csr_k                      => OPEN,
        csr_l                      => OPEN,
        csr_lane_powerdown         => open,  -- out
        csr_m                      => OPEN,
        csr_n                      => OPEN,
        csr_np                     => OPEN,
        csr_tx_testmode            => OPEN,
        csr_tx_testpattern_a       => OPEN,
        csr_tx_testpattern_b       => OPEN,
        csr_tx_testpattern_c       => OPEN,
        csr_tx_testpattern_d       => OPEN,
        csr_s                      => OPEN,
        dev_sync_n                 => dev_sync_n(i),  -- out
        jesd204_tx_avs_chipselect  => avs_chipselect(i),  -- jesd204b_mosi_arr(i).chipselect,
        jesd204_tx_avs_address     => avs_address(i),
        jesd204_tx_avs_read        => avs_read(i),
        jesd204_tx_avs_readdata    => avs_readdata(i),
        jesd204_tx_avs_waitrequest => open,
        jesd204_tx_avs_write       => '0',
        jesd204_tx_avs_writedata   => (others => '0'),
        jesd204_tx_avs_clk         => mm_clk,
        jesd204_tx_avs_rst_n       => avs_rst_n,
        jesd204_tx_dlb_data        => open,  -- debug/loopback testing
        jesd204_tx_dlb_kchar_data  => open,  -- debug/loopback testing
        jesd204_tx_frame_ready     => jesd204b_tx_frame_ready(i),
        jesd204_tx_frame_error     => '0',
        jesd204_tx_int             => OPEN,  -- Connected to status IO in example design
        jesd204_tx_link_data       => jesd204b_tx_link_data_arr(i),  -- in
        jesd204_tx_link_valid      => jesd204b_tx_link_valid(i),  -- in
        jesd204_tx_link_ready      => jesd204b_tx_link_ready(i),  -- out
        mdev_sync_n                => dev_sync_n(i),  -- in
        pll_locked                 => pll_locked,  -- in
        sync_n                     => jesd204b_sync_n_adc(i),  -- in
        tx_analogreset             => tx_analogreset,
        tx_bonding_clocks          => tx_bonding_clocks,  -- : in  std_logic_vector(5 downto 0)  := (others => 'X'); -- clk
        tx_cal_busy                => open,
        tx_digitalreset            => tx_digitalreset,
        tx_serial_data             => serial_tx(i downto i),
        txlink_clk                 => txlink_clk(i),
        txlink_rst_n_reset_n       => txlink_rst_n,
        txphy_clk                  => txphy_clk(i downto i),
        somf                       => OPEN,
        sysref                     => jesd204b_sysref_adc(i)
      );

    -- Generate test pattern at each ADC

    p_data : process (jesd204b_sampclk_adc(i), mm_rst)
      variable data  : integer := 0;
      variable even_sample : boolean := true;
    begin
      if mm_rst = '1' then
         jesd204b_tx_link_data_arr(i) <= (others => '0');
         jesd204b_tx_link_valid(i) <= '0';
         txlink_clk(i) <= '0';
         data := 0;
         even_sample := true;
       else
         if rising_edge(jesd204b_sampclk_adc(i)) then
           txlink_clk(i) <= not txlink_clk(i);
           jesd204b_sysref_adc_1(i) <= jesd204b_sysref_adc(i);
           jesd204b_sysref_adc_2(i) <= jesd204b_sysref_adc_1(i);
           if (jesd204b_sysref_adc(i) = '1' and jesd204b_sysref_adc_1(i) = '0') then
             data := 1000;
           elsif (jesd204b_sysref_adc_1(i) = '1' and jesd204b_sysref_adc_2(i) = '0') then
             data := -1000;
           else
             data := 0;
           end if;

           -- Frame the data to 32 bits at half the rate
           if(jesd204b_tx_link_ready(i) = '0') then
             even_sample := true;
           else
             even_sample := not even_sample;
           end if;
           if (even_sample = true) then
             jesd204b_tx_link_data_arr(i)(15 downto 0) <= TO_SVEC(data, 16);
             jesd204b_tx_link_valid(i) <= '0';
           else
             jesd204b_tx_link_data_arr(i)(31 downto 16) <= TO_SVEC(data, 16);
             jesd204b_tx_link_valid(i) <= '1';
           end if;
         end if;
       end if;
    end process;
  end generate;

  -----------------------------------------------------------------------------
  -- Stimulii
  -----------------------------------------------------------------------------

  -- Clocks and resets
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after 800 ns;
  avs_rst_n <= '0', '1'  after 23500 ns;
  tx_analogreset(0) <= '1', '0' after 18500 ns;
  tx_digitalreset(0) <= '1', '0' after 23000 ns;
  txlink_rst_n <= '0', '1' after 25500 ns;
  pll_locked(0) <= '0', '1' after 1000 ns;

  bonding_clock_5 <= not bonding_clock_5 after 250 ps;
  bonding_clock_4 <= not bonding_clock_4 after 250 ps;
  bonding_clock_3 <= not bonding_clock_3 after 500 ps;
  bonding_clock_2 <= not bonding_clock_2 after 500 ps;
  bonding_clock_0 <= not bonding_clock_0 after 2500 ps;

  p_bonding_clock_1 : process
  begin
    bonding_clock_1 <= '0';
    wait for 4000 ps;
    bonding_clock_1 <= '1';
    wait for 1000 ps;
  end process;

  tx_bonding_clocks(5) <= transport bonding_clock_5 after 4890 ps;
  tx_bonding_clocks(4) <= transport bonding_clock_4 after 4640 ps;
  tx_bonding_clocks(3) <= transport bonding_clock_3 after 4920 ps;
  tx_bonding_clocks(2) <= transport bonding_clock_2 after 4930 ps;
  tx_bonding_clocks(1) <= transport bonding_clock_1 after 7490 ps;
  tx_bonding_clocks(0) <= transport bonding_clock_0 after 4000 ps;

  -- Sample Clock
  jesd204b_sampclk <= not jesd204b_sampclk after c_jesd204b_sampclk_period / 2;  -- JESD sample clock (200MHz)

  -- clock source process

  p_sysref : process (jesd204b_sampclk, mm_rst)
    variable count  : natural := 0;
  begin
    if mm_rst = '1' then
       jesd204b_sysref <= '0';
       count := 0;
     else
       if rising_edge(jesd204b_sampclk) then
        if (count = c_sysref_period - 1) then
           count := 0;
         else
           count := count + 1;
         end if;

         if count > c_sysref_period - 8 then
           jesd204b_sysref <= '1';
         else
           jesd204b_sysref <= '0';
         end if;
       end if;
     end if;
  end process;

  ------------------------------------------------------------------------------
  -- Diagnostics
  ------------------------------------------------------------------------------
  p_read_avs_regs : process
  begin
    wait for 100 ns;
    avs_address(0) <= (others => '0');
    avs_chipselect(0) <= '0';
    avs_read(0) <= '0';
    wait until avs_rst_n = '1';
    while true loop
      wait until rising_edge(mm_clk);
      avs_address(0) <= X"14";  -- dll control
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);
      avs_address(0) <= X"15";  -- syncn_sysref control
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);

      avs_address(0) <= X"18";  -- syncn_sysref control
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);
      avs_address(0) <= X"19";  -- syncn_sysref control
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);

      avs_address(0) <= X"20";  -- tx control0
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
      wait until rising_edge(mm_clk);
      avs_address(0) <= X"26";  -- tx control0
      avs_chipselect(0) <= '1';
      avs_read(0) <= '1';
      wait for c_mm_clk_period * 1;
      wait until rising_edge(mm_clk);
      avs_address(0) <= (others => '0');
      avs_chipselect(0) <= '0';
      avs_read(0) <= '0';
      wait for c_mm_clk_period * 32;
    end loop;
  end process;

  ------------------------------------------------------------------------------
  -- Simulation end
  ------------------------------------------------------------------------------
  --sim_done <= '0', '1' AFTER 1 us;
  sim_done <= '0';

  proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
end tb;
