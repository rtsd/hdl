-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_lofar2_unb2b_sdp_station_pkg is
  -----------------------------------------------------------------------------
  -- this component declaration is copy-pasted from Quartus platform designer:
  -----------------------------------------------------------------------------
    component qsys_lofar2_unb2b_sdp_station is
        port (
            avs_eth_0_clk_export                                   : out std_logic;                                        -- export
            avs_eth_0_irq_export                                   : in  std_logic                     := 'X';             -- export
            avs_eth_0_ram_address_export                           : out std_logic_vector(9 downto 0);                     -- export
            avs_eth_0_ram_read_export                              : out std_logic;                                        -- export
            avs_eth_0_ram_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            avs_eth_0_ram_write_export                             : out std_logic;                                        -- export
            avs_eth_0_ram_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            avs_eth_0_reg_address_export                           : out std_logic_vector(3 downto 0);                     -- export
            avs_eth_0_reg_read_export                              : out std_logic;                                        -- export
            avs_eth_0_reg_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            avs_eth_0_reg_write_export                             : out std_logic;                                        -- export
            avs_eth_0_reg_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            avs_eth_0_reset_export                                 : out std_logic;                                        -- export
            avs_eth_0_tse_address_export                           : out std_logic_vector(9 downto 0);                     -- export
            avs_eth_0_tse_read_export                              : out std_logic;                                        -- export
            avs_eth_0_tse_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            avs_eth_0_tse_waitrequest_export                       : in  std_logic                     := 'X';             -- export
            avs_eth_0_tse_write_export                             : out std_logic;                                        -- export
            avs_eth_0_tse_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            clk_clk                                                : in  std_logic                     := 'X';             -- clk
            jesd204b_address_export                                : out std_logic_vector(11 downto 0);                    -- export
            jesd204b_clk_export                                    : out std_logic;                                        -- export
            jesd204b_read_export                                   : out std_logic;                                        -- export
            jesd204b_readdata_export                               : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            jesd204b_reset_export                                  : out std_logic;                                        -- export
            jesd204b_write_export                                  : out std_logic;                                        -- export
            jesd204b_writedata_export                              : out std_logic_vector(31 downto 0);                    -- export
            pio_jesd_ctrl_address_export                           : out std_logic_vector(0 downto 0);                     -- export
            pio_jesd_ctrl_clk_export                               : out std_logic;                                        -- export
            pio_jesd_ctrl_read_export                              : out std_logic;                                        -- export
            pio_jesd_ctrl_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            pio_jesd_ctrl_reset_export                             : out std_logic;                                        -- export
            pio_jesd_ctrl_write_export                             : out std_logic;                                        -- export
            pio_jesd_ctrl_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            pio_pps_address_export                                 : out std_logic_vector(1 downto 0);                     -- export
            pio_pps_clk_export                                     : out std_logic;                                        -- export
            pio_pps_read_export                                    : out std_logic;                                        -- export
            pio_pps_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            pio_pps_reset_export                                   : out std_logic;                                        -- export
            pio_pps_write_export                                   : out std_logic;                                        -- export
            pio_pps_writedata_export                               : out std_logic_vector(31 downto 0);                    -- export
            pio_system_info_address_export                         : out std_logic_vector(4 downto 0);                     -- export
            pio_system_info_clk_export                             : out std_logic;                                        -- export
            pio_system_info_read_export                            : out std_logic;                                        -- export
            pio_system_info_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            pio_system_info_reset_export                           : out std_logic;                                        -- export
            pio_system_info_write_export                           : out std_logic;                                        -- export
            pio_system_info_writedata_export                       : out std_logic_vector(31 downto 0);                    -- export
            pio_wdi_external_connection_export                     : out std_logic;                                        -- export
            ram_bf_weights_address_export                          : out std_logic_vector(14 downto 0);                    -- export
            ram_bf_weights_clk_export                              : out std_logic;                                        -- export
            ram_bf_weights_read_export                             : out std_logic;                                        -- export
            ram_bf_weights_readdata_export                         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_bf_weights_reset_export                            : out std_logic;                                        -- export
            ram_bf_weights_write_export                            : out std_logic;                                        -- export
            ram_bf_weights_writedata_export                        : out std_logic_vector(31 downto 0);                    -- export
            ram_diag_data_buffer_bsn_address_export                : out std_logic_vector(20 downto 0);                    -- export
            ram_diag_data_buffer_bsn_clk_export                    : out std_logic;                                        -- export
            ram_diag_data_buffer_bsn_read_export                   : out std_logic;                                        -- export
            ram_diag_data_buffer_bsn_readdata_export               : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_diag_data_buffer_bsn_reset_export                  : out std_logic;                                        -- export
            ram_diag_data_buffer_bsn_write_export                  : out std_logic;                                        -- export
            ram_diag_data_buffer_bsn_writedata_export              : out std_logic_vector(31 downto 0);                    -- export
            ram_equalizer_gains_address_export                     : out std_logic_vector(13 downto 0);                    -- export
            ram_equalizer_gains_clk_export                         : out std_logic;                                        -- export
            ram_equalizer_gains_cross_address_export               : out std_logic_vector(13 downto 0);                    -- export
            ram_equalizer_gains_cross_clk_export                   : out std_logic;                                        -- export
            ram_equalizer_gains_cross_read_export                  : out std_logic;                                        -- export
            ram_equalizer_gains_cross_readdata_export              : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_equalizer_gains_cross_reset_export                 : out std_logic;                                        -- export
            ram_equalizer_gains_cross_write_export                 : out std_logic;                                        -- export
            ram_equalizer_gains_cross_writedata_export             : out std_logic_vector(31 downto 0);                    -- export
            ram_equalizer_gains_read_export                        : out std_logic;                                        -- export
            ram_equalizer_gains_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_equalizer_gains_reset_export                       : out std_logic;                                        -- export
            ram_equalizer_gains_write_export                       : out std_logic;                                        -- export
            ram_equalizer_gains_writedata_export                   : out std_logic_vector(31 downto 0);                    -- export
            ram_fil_coefs_address_export                           : out std_logic_vector(14 downto 0);                    -- export
            ram_fil_coefs_clk_export                               : out std_logic;                                        -- export
            ram_fil_coefs_read_export                              : out std_logic;                                        -- export
            ram_fil_coefs_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_fil_coefs_reset_export                             : out std_logic;                                        -- export
            ram_fil_coefs_write_export                             : out std_logic;                                        -- export
            ram_fil_coefs_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            ram_scrap_address_export                               : out std_logic_vector(8 downto 0);                     -- export
            ram_scrap_clk_export                                   : out std_logic;                                        -- export
            ram_scrap_read_export                                  : out std_logic;                                        -- export
            ram_scrap_readdata_export                              : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_scrap_reset_export                                 : out std_logic;                                        -- export
            ram_scrap_write_export                                 : out std_logic;                                        -- export
            ram_scrap_writedata_export                             : out std_logic_vector(31 downto 0);                    -- export
            ram_ss_ss_wide_address_export                          : out std_logic_vector(13 downto 0);                    -- export
            ram_ss_ss_wide_clk_export                              : out std_logic;                                        -- export
            ram_ss_ss_wide_read_export                             : out std_logic;                                        -- export
            ram_ss_ss_wide_readdata_export                         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_ss_ss_wide_reset_export                            : out std_logic;                                        -- export
            ram_ss_ss_wide_write_export                            : out std_logic;                                        -- export
            ram_ss_ss_wide_writedata_export                        : out std_logic_vector(31 downto 0);                    -- export
            ram_st_bst_address_export                              : out std_logic_vector(11 downto 0);                    -- export
            ram_st_bst_clk_export                                  : out std_logic;                                        -- export
            ram_st_bst_read_export                                 : out std_logic;                                        -- export
            ram_st_bst_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_st_bst_reset_export                                : out std_logic;                                        -- export
            ram_st_bst_write_export                                : out std_logic;                                        -- export
            ram_st_bst_writedata_export                            : out std_logic_vector(31 downto 0);                    -- export
            ram_st_histogram_address_export                        : out std_logic_vector(12 downto 0);                    -- export
            ram_st_histogram_clk_export                            : out std_logic;                                        -- export
            ram_st_histogram_read_export                           : out std_logic;                                        -- export
            ram_st_histogram_readdata_export                       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_st_histogram_reset_export                          : out std_logic;                                        -- export
            ram_st_histogram_write_export                          : out std_logic;                                        -- export
            ram_st_histogram_writedata_export                      : out std_logic_vector(31 downto 0);                    -- export
            ram_st_sst_address_export                              : out std_logic_vector(14 downto 0);                    -- export
            ram_st_sst_clk_export                                  : out std_logic;                                        -- export
            ram_st_sst_read_export                                 : out std_logic;                                        -- export
            ram_st_sst_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_st_sst_reset_export                                : out std_logic;                                        -- export
            ram_st_sst_write_export                                : out std_logic;                                        -- export
            ram_st_sst_writedata_export                            : out std_logic_vector(31 downto 0);                    -- export
            ram_st_xsq_address_export                              : out std_logic_vector(15 downto 0);                    -- export
            ram_st_xsq_clk_export                                  : out std_logic;                                        -- export
            ram_st_xsq_read_export                                 : out std_logic;                                        -- export
            ram_st_xsq_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_st_xsq_reset_export                                : out std_logic;                                        -- export
            ram_st_xsq_write_export                                : out std_logic;                                        -- export
            ram_st_xsq_writedata_export                            : out std_logic_vector(31 downto 0);                    -- export
            ram_wg_address_export                                  : out std_logic_vector(13 downto 0);                    -- export
            ram_wg_clk_export                                      : out std_logic;                                        -- export
            ram_wg_read_export                                     : out std_logic;                                        -- export
            ram_wg_readdata_export                                 : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            ram_wg_reset_export                                    : out std_logic;                                        -- export
            ram_wg_write_export                                    : out std_logic;                                        -- export
            ram_wg_writedata_export                                : out std_logic_vector(31 downto 0);                    -- export
            reg_aduh_monitor_address_export                        : out std_logic_vector(5 downto 0);                     -- export
            reg_aduh_monitor_clk_export                            : out std_logic;                                        -- export
            reg_aduh_monitor_read_export                           : out std_logic;                                        -- export
            reg_aduh_monitor_readdata_export                       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_aduh_monitor_reset_export                          : out std_logic;                                        -- export
            reg_aduh_monitor_write_export                          : out std_logic;                                        -- export
            reg_aduh_monitor_writedata_export                      : out std_logic_vector(31 downto 0);                    -- export
            reg_bdo_destinations_address_export                    : out std_logic_vector(8 downto 0);                     -- export
            reg_bdo_destinations_clk_export                        : out std_logic;                                        -- export
            reg_bdo_destinations_read_export                       : out std_logic;                                        -- export
            reg_bdo_destinations_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bdo_destinations_reset_export                      : out std_logic;                                        -- export
            reg_bdo_destinations_write_export                      : out std_logic;                                        -- export
            reg_bdo_destinations_writedata_export                  : out std_logic_vector(31 downto 0);                    -- export
            reg_bf_scale_address_export                            : out std_logic_vector(1 downto 0);                     -- export
            reg_bf_scale_clk_export                                : out std_logic;                                        -- export
            reg_bf_scale_read_export                               : out std_logic;                                        -- export
            reg_bf_scale_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bf_scale_reset_export                              : out std_logic;                                        -- export
            reg_bf_scale_write_export                              : out std_logic;                                        -- export
            reg_bf_scale_writedata_export                          : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_align_v2_bf_address_export                     : out std_logic_vector(2 downto 0);                     -- export
            reg_bsn_align_v2_bf_clk_export                         : out std_logic;                                        -- export
            reg_bsn_align_v2_bf_read_export                        : out std_logic;                                        -- export
            reg_bsn_align_v2_bf_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_align_v2_bf_reset_export                       : out std_logic;                                        -- export
            reg_bsn_align_v2_bf_write_export                       : out std_logic;                                        -- export
            reg_bsn_align_v2_bf_writedata_export                   : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_align_v2_xsub_address_export                   : out std_logic_vector(4 downto 0);                     -- export
            reg_bsn_align_v2_xsub_clk_export                       : out std_logic;                                        -- export
            reg_bsn_align_v2_xsub_read_export                      : out std_logic;                                        -- export
            reg_bsn_align_v2_xsub_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_align_v2_xsub_reset_export                     : out std_logic;                                        -- export
            reg_bsn_align_v2_xsub_write_export                     : out std_logic;                                        -- export
            reg_bsn_align_v2_xsub_writedata_export                 : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_input_address_export                   : out std_logic_vector(7 downto 0);                     -- export
            reg_bsn_monitor_input_clk_export                       : out std_logic;                                        -- export
            reg_bsn_monitor_input_read_export                      : out std_logic;                                        -- export
            reg_bsn_monitor_input_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_input_reset_export                     : out std_logic;                                        -- export
            reg_bsn_monitor_input_write_export                     : out std_logic;                                        -- export
            reg_bsn_monitor_input_writedata_export                 : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_aligned_bf_address_export           : out std_logic_vector(3 downto 0);                     -- export
            reg_bsn_monitor_v2_aligned_bf_clk_export               : out std_logic;                                        -- export
            reg_bsn_monitor_v2_aligned_bf_read_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_aligned_bf_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_aligned_bf_reset_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_aligned_bf_write_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_aligned_bf_writedata_export         : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_aligned_xsub_address_export         : out std_logic_vector(2 downto 0);                     -- export
            reg_bsn_monitor_v2_aligned_xsub_clk_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_aligned_xsub_read_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_aligned_xsub_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_aligned_xsub_reset_export           : out std_logic;                                        -- export
            reg_bsn_monitor_v2_aligned_xsub_write_export           : out std_logic;                                        -- export
            reg_bsn_monitor_v2_aligned_xsub_writedata_export       : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_beamlet_output_address_export       : out std_logic_vector(3 downto 0);                     -- export
            reg_bsn_monitor_v2_beamlet_output_clk_export           : out std_logic;                                        -- export
            reg_bsn_monitor_v2_beamlet_output_read_export          : out std_logic;                                        -- export
            reg_bsn_monitor_v2_beamlet_output_readdata_export      : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_beamlet_output_reset_export         : out std_logic;                                        -- export
            reg_bsn_monitor_v2_beamlet_output_write_export         : out std_logic;                                        -- export
            reg_bsn_monitor_v2_beamlet_output_writedata_export     : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_bst_offload_address_export          : out std_logic_vector(3 downto 0);                     -- export
            reg_bsn_monitor_v2_bst_offload_clk_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_bst_offload_read_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_bst_offload_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_bst_offload_reset_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_bst_offload_write_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_bst_offload_writedata_export        : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_ring_rx_bf_address_export           : out std_logic_vector(3 downto 0);                     -- export
            reg_bsn_monitor_v2_ring_rx_bf_clk_export               : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_rx_bf_read_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_rx_bf_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_ring_rx_bf_reset_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_rx_bf_write_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_rx_bf_writedata_export         : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_ring_rx_xst_address_export          : out std_logic_vector(6 downto 0);                     -- export
            reg_bsn_monitor_v2_ring_rx_xst_clk_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_rx_xst_read_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_rx_xst_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_ring_rx_xst_reset_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_rx_xst_write_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_rx_xst_writedata_export        : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_ring_tx_bf_address_export           : out std_logic_vector(3 downto 0);                     -- export
            reg_bsn_monitor_v2_ring_tx_bf_clk_export               : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_tx_bf_read_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_tx_bf_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_ring_tx_bf_reset_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_tx_bf_write_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_tx_bf_writedata_export         : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_ring_tx_xst_address_export          : out std_logic_vector(6 downto 0);                     -- export
            reg_bsn_monitor_v2_ring_tx_xst_clk_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_tx_xst_read_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_tx_xst_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_ring_tx_xst_reset_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_tx_xst_write_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_ring_tx_xst_writedata_export        : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_rx_align_bf_address_export          : out std_logic_vector(4 downto 0);                     -- export
            reg_bsn_monitor_v2_rx_align_bf_clk_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_rx_align_bf_read_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_rx_align_bf_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_rx_align_bf_reset_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_rx_align_bf_write_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_rx_align_bf_writedata_export        : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_rx_align_xsub_address_export        : out std_logic_vector(6 downto 0);                     -- export
            reg_bsn_monitor_v2_rx_align_xsub_clk_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_rx_align_xsub_read_export           : out std_logic;                                        -- export
            reg_bsn_monitor_v2_rx_align_xsub_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_rx_align_xsub_reset_export          : out std_logic;                                        -- export
            reg_bsn_monitor_v2_rx_align_xsub_write_export          : out std_logic;                                        -- export
            reg_bsn_monitor_v2_rx_align_xsub_writedata_export      : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_sst_offload_address_export          : out std_logic_vector(2 downto 0);                     -- export
            reg_bsn_monitor_v2_sst_offload_clk_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_sst_offload_read_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_sst_offload_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_sst_offload_reset_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_sst_offload_write_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_sst_offload_writedata_export        : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_monitor_v2_xst_offload_address_export          : out std_logic_vector(2 downto 0);                     -- export
            reg_bsn_monitor_v2_xst_offload_clk_export              : out std_logic;                                        -- export
            reg_bsn_monitor_v2_xst_offload_read_export             : out std_logic;                                        -- export
            reg_bsn_monitor_v2_xst_offload_readdata_export         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_monitor_v2_xst_offload_reset_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_xst_offload_write_export            : out std_logic;                                        -- export
            reg_bsn_monitor_v2_xst_offload_writedata_export        : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_scheduler_address_export                       : out std_logic_vector(0 downto 0);                     -- export
            reg_bsn_scheduler_clk_export                           : out std_logic;                                        -- export
            reg_bsn_scheduler_read_export                          : out std_logic;                                        -- export
            reg_bsn_scheduler_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_scheduler_reset_export                         : out std_logic;                                        -- export
            reg_bsn_scheduler_write_export                         : out std_logic;                                        -- export
            reg_bsn_scheduler_writedata_export                     : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_source_v2_address_export                       : out std_logic_vector(2 downto 0);                     -- export
            reg_bsn_source_v2_clk_export                           : out std_logic;                                        -- export
            reg_bsn_source_v2_read_export                          : out std_logic;                                        -- export
            reg_bsn_source_v2_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_source_v2_reset_export                         : out std_logic;                                        -- export
            reg_bsn_source_v2_write_export                         : out std_logic;                                        -- export
            reg_bsn_source_v2_writedata_export                     : out std_logic_vector(31 downto 0);                    -- export
            reg_bsn_sync_scheduler_xsub_address_export             : out std_logic_vector(3 downto 0);                     -- export
            reg_bsn_sync_scheduler_xsub_clk_export                 : out std_logic;                                        -- export
            reg_bsn_sync_scheduler_xsub_read_export                : out std_logic;                                        -- export
            reg_bsn_sync_scheduler_xsub_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_bsn_sync_scheduler_xsub_reset_export               : out std_logic;                                        -- export
            reg_bsn_sync_scheduler_xsub_write_export               : out std_logic;                                        -- export
            reg_bsn_sync_scheduler_xsub_writedata_export           : out std_logic_vector(31 downto 0);                    -- export
            reg_crosslets_info_address_export                      : out std_logic_vector(3 downto 0);                     -- export
            reg_crosslets_info_clk_export                          : out std_logic;                                        -- export
            reg_crosslets_info_read_export                         : out std_logic;                                        -- export
            reg_crosslets_info_readdata_export                     : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_crosslets_info_reset_export                        : out std_logic;                                        -- export
            reg_crosslets_info_write_export                        : out std_logic;                                        -- export
            reg_crosslets_info_writedata_export                    : out std_logic_vector(31 downto 0);                    -- export
            reg_diag_data_buffer_bsn_address_export                : out std_logic_vector(4 downto 0);                     -- export
            reg_diag_data_buffer_bsn_clk_export                    : out std_logic;                                        -- export
            reg_diag_data_buffer_bsn_read_export                   : out std_logic;                                        -- export
            reg_diag_data_buffer_bsn_readdata_export               : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_diag_data_buffer_bsn_reset_export                  : out std_logic;                                        -- export
            reg_diag_data_buffer_bsn_write_export                  : out std_logic;                                        -- export
            reg_diag_data_buffer_bsn_writedata_export              : out std_logic_vector(31 downto 0);                    -- export
            reg_dp_block_validate_bsn_at_sync_bf_address_export    : out std_logic_vector(2 downto 0);                     -- export
            reg_dp_block_validate_bsn_at_sync_bf_clk_export        : out std_logic;                                        -- export
            reg_dp_block_validate_bsn_at_sync_bf_read_export       : out std_logic;                                        -- export
            reg_dp_block_validate_bsn_at_sync_bf_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dp_block_validate_bsn_at_sync_bf_reset_export      : out std_logic;                                        -- export
            reg_dp_block_validate_bsn_at_sync_bf_write_export      : out std_logic;                                        -- export
            reg_dp_block_validate_bsn_at_sync_bf_writedata_export  : out std_logic_vector(31 downto 0);                    -- export
            reg_dp_block_validate_bsn_at_sync_xst_address_export   : out std_logic_vector(1 downto 0);                     -- export
            reg_dp_block_validate_bsn_at_sync_xst_clk_export       : out std_logic;                                        -- export
            reg_dp_block_validate_bsn_at_sync_xst_read_export      : out std_logic;                                        -- export
            reg_dp_block_validate_bsn_at_sync_xst_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dp_block_validate_bsn_at_sync_xst_reset_export     : out std_logic;                                        -- export
            reg_dp_block_validate_bsn_at_sync_xst_write_export     : out std_logic;                                        -- export
            reg_dp_block_validate_bsn_at_sync_xst_writedata_export : out std_logic_vector(31 downto 0);                    -- export
            reg_dp_block_validate_err_bf_address_export            : out std_logic_vector(4 downto 0);                     -- export
            reg_dp_block_validate_err_bf_clk_export                : out std_logic;                                        -- export
            reg_dp_block_validate_err_bf_read_export               : out std_logic;                                        -- export
            reg_dp_block_validate_err_bf_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dp_block_validate_err_bf_reset_export              : out std_logic;                                        -- export
            reg_dp_block_validate_err_bf_write_export              : out std_logic;                                        -- export
            reg_dp_block_validate_err_bf_writedata_export          : out std_logic_vector(31 downto 0);                    -- export
            reg_dp_block_validate_err_xst_address_export           : out std_logic_vector(3 downto 0);                     -- export
            reg_dp_block_validate_err_xst_clk_export               : out std_logic;                                        -- export
            reg_dp_block_validate_err_xst_read_export              : out std_logic;                                        -- export
            reg_dp_block_validate_err_xst_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dp_block_validate_err_xst_reset_export             : out std_logic;                                        -- export
            reg_dp_block_validate_err_xst_write_export             : out std_logic;                                        -- export
            reg_dp_block_validate_err_xst_writedata_export         : out std_logic_vector(31 downto 0);                    -- export
            reg_dp_selector_address_export                         : out std_logic_vector(0 downto 0);                     -- export
            reg_dp_selector_clk_export                             : out std_logic;                                        -- export
            reg_dp_selector_read_export                            : out std_logic;                                        -- export
            reg_dp_selector_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dp_selector_reset_export                           : out std_logic;                                        -- export
            reg_dp_selector_write_export                           : out std_logic;                                        -- export
            reg_dp_selector_writedata_export                       : out std_logic_vector(31 downto 0);                    -- export
            reg_dp_shiftram_address_export                         : out std_logic_vector(4 downto 0);                     -- export
            reg_dp_shiftram_clk_export                             : out std_logic;                                        -- export
            reg_dp_shiftram_read_export                            : out std_logic;                                        -- export
            reg_dp_shiftram_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dp_shiftram_reset_export                           : out std_logic;                                        -- export
            reg_dp_shiftram_write_export                           : out std_logic;                                        -- export
            reg_dp_shiftram_writedata_export                       : out std_logic_vector(31 downto 0);                    -- export
            reg_dp_xonoff_address_export                           : out std_logic_vector(1 downto 0);                     -- export
            reg_dp_xonoff_clk_export                               : out std_logic;                                        -- export
            reg_dp_xonoff_read_export                              : out std_logic;                                        -- export
            reg_dp_xonoff_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dp_xonoff_reset_export                             : out std_logic;                                        -- export
            reg_dp_xonoff_write_export                             : out std_logic;                                        -- export
            reg_dp_xonoff_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            reg_dpmm_ctrl_address_export                           : out std_logic_vector(0 downto 0);                     -- export
            reg_dpmm_ctrl_clk_export                               : out std_logic;                                        -- export
            reg_dpmm_ctrl_read_export                              : out std_logic;                                        -- export
            reg_dpmm_ctrl_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dpmm_ctrl_reset_export                             : out std_logic;                                        -- export
            reg_dpmm_ctrl_write_export                             : out std_logic;                                        -- export
            reg_dpmm_ctrl_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            reg_dpmm_data_address_export                           : out std_logic_vector(0 downto 0);                     -- export
            reg_dpmm_data_clk_export                               : out std_logic;                                        -- export
            reg_dpmm_data_read_export                              : out std_logic;                                        -- export
            reg_dpmm_data_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_dpmm_data_reset_export                             : out std_logic;                                        -- export
            reg_dpmm_data_write_export                             : out std_logic;                                        -- export
            reg_dpmm_data_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            reg_epcs_address_export                                : out std_logic_vector(2 downto 0);                     -- export
            reg_epcs_clk_export                                    : out std_logic;                                        -- export
            reg_epcs_read_export                                   : out std_logic;                                        -- export
            reg_epcs_readdata_export                               : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_epcs_reset_export                                  : out std_logic;                                        -- export
            reg_epcs_write_export                                  : out std_logic;                                        -- export
            reg_epcs_writedata_export                              : out std_logic_vector(31 downto 0);                    -- export
            reg_fpga_temp_sens_address_export                      : out std_logic_vector(2 downto 0);                     -- export
            reg_fpga_temp_sens_clk_export                          : out std_logic;                                        -- export
            reg_fpga_temp_sens_read_export                         : out std_logic;                                        -- export
            reg_fpga_temp_sens_readdata_export                     : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_fpga_temp_sens_reset_export                        : out std_logic;                                        -- export
            reg_fpga_temp_sens_write_export                        : out std_logic;                                        -- export
            reg_fpga_temp_sens_writedata_export                    : out std_logic_vector(31 downto 0);                    -- export
            reg_fpga_voltage_sens_address_export                   : out std_logic_vector(3 downto 0);                     -- export
            reg_fpga_voltage_sens_clk_export                       : out std_logic;                                        -- export
            reg_fpga_voltage_sens_read_export                      : out std_logic;                                        -- export
            reg_fpga_voltage_sens_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_fpga_voltage_sens_reset_export                     : out std_logic;                                        -- export
            reg_fpga_voltage_sens_write_export                     : out std_logic;                                        -- export
            reg_fpga_voltage_sens_writedata_export                 : out std_logic_vector(31 downto 0);                    -- export
            reg_hdr_dat_address_export                             : out std_logic_vector(6 downto 0);                     -- export
            reg_hdr_dat_clk_export                                 : out std_logic;                                        -- export
            reg_hdr_dat_read_export                                : out std_logic;                                        -- export
            reg_hdr_dat_readdata_export                            : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_hdr_dat_reset_export                               : out std_logic;                                        -- export
            reg_hdr_dat_write_export                               : out std_logic;                                        -- export
            reg_hdr_dat_writedata_export                           : out std_logic_vector(31 downto 0);                    -- export
            reg_mmdp_ctrl_address_export                           : out std_logic_vector(0 downto 0);                     -- export
            reg_mmdp_ctrl_clk_export                               : out std_logic;                                        -- export
            reg_mmdp_ctrl_read_export                              : out std_logic;                                        -- export
            reg_mmdp_ctrl_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_mmdp_ctrl_reset_export                             : out std_logic;                                        -- export
            reg_mmdp_ctrl_write_export                             : out std_logic;                                        -- export
            reg_mmdp_ctrl_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            reg_mmdp_data_address_export                           : out std_logic_vector(0 downto 0);                     -- export
            reg_mmdp_data_clk_export                               : out std_logic;                                        -- export
            reg_mmdp_data_read_export                              : out std_logic;                                        -- export
            reg_mmdp_data_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_mmdp_data_reset_export                             : out std_logic;                                        -- export
            reg_mmdp_data_write_export                             : out std_logic;                                        -- export
            reg_mmdp_data_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            reg_nof_crosslets_address_export                       : out std_logic_vector(0 downto 0);                     -- export
            reg_nof_crosslets_clk_export                           : out std_logic;                                        -- export
            reg_nof_crosslets_read_export                          : out std_logic;                                        -- export
            reg_nof_crosslets_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_nof_crosslets_reset_export                         : out std_logic;                                        -- export
            reg_nof_crosslets_write_export                         : out std_logic;                                        -- export
            reg_nof_crosslets_writedata_export                     : out std_logic_vector(31 downto 0);                    -- export
            reg_nw_10gbe_eth10g_address_export                     : out std_logic_vector(0 downto 0);                     -- export
            reg_nw_10gbe_eth10g_clk_export                         : out std_logic;                                        -- export
            reg_nw_10gbe_eth10g_read_export                        : out std_logic;                                        -- export
            reg_nw_10gbe_eth10g_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_nw_10gbe_eth10g_reset_export                       : out std_logic;                                        -- export
            reg_nw_10gbe_eth10g_write_export                       : out std_logic;                                        -- export
            reg_nw_10gbe_eth10g_writedata_export                   : out std_logic_vector(31 downto 0);                    -- export
            reg_nw_10gbe_mac_address_export                        : out std_logic_vector(12 downto 0);                    -- export
            reg_nw_10gbe_mac_clk_export                            : out std_logic;                                        -- export
            reg_nw_10gbe_mac_read_export                           : out std_logic;                                        -- export
            reg_nw_10gbe_mac_readdata_export                       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_nw_10gbe_mac_reset_export                          : out std_logic;                                        -- export
            reg_nw_10gbe_mac_write_export                          : out std_logic;                                        -- export
            reg_nw_10gbe_mac_writedata_export                      : out std_logic_vector(31 downto 0);                    -- export
            reg_remu_address_export                                : out std_logic_vector(2 downto 0);                     -- export
            reg_remu_clk_export                                    : out std_logic;                                        -- export
            reg_remu_read_export                                   : out std_logic;                                        -- export
            reg_remu_readdata_export                               : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_remu_reset_export                                  : out std_logic;                                        -- export
            reg_remu_write_export                                  : out std_logic;                                        -- export
            reg_remu_writedata_export                              : out std_logic_vector(31 downto 0);                    -- export
            reg_ring_info_address_export                           : out std_logic_vector(1 downto 0);                     -- export
            reg_ring_info_clk_export                               : out std_logic;                                        -- export
            reg_ring_info_read_export                              : out std_logic;                                        -- export
            reg_ring_info_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_ring_info_reset_export                             : out std_logic;                                        -- export
            reg_ring_info_write_export                             : out std_logic;                                        -- export
            reg_ring_info_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            reg_ring_lane_info_bf_address_export                   : out std_logic_vector(1 downto 0);                     -- export
            reg_ring_lane_info_bf_clk_export                       : out std_logic;                                        -- export
            reg_ring_lane_info_bf_read_export                      : out std_logic;                                        -- export
            reg_ring_lane_info_bf_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_ring_lane_info_bf_reset_export                     : out std_logic;                                        -- export
            reg_ring_lane_info_bf_write_export                     : out std_logic;                                        -- export
            reg_ring_lane_info_bf_writedata_export                 : out std_logic_vector(31 downto 0);                    -- export
            reg_ring_lane_info_xst_address_export                  : out std_logic_vector(0 downto 0);                     -- export
            reg_ring_lane_info_xst_clk_export                      : out std_logic;                                        -- export
            reg_ring_lane_info_xst_read_export                     : out std_logic;                                        -- export
            reg_ring_lane_info_xst_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_ring_lane_info_xst_reset_export                    : out std_logic;                                        -- export
            reg_ring_lane_info_xst_write_export                    : out std_logic;                                        -- export
            reg_ring_lane_info_xst_writedata_export                : out std_logic_vector(31 downto 0);                    -- export
            reg_sdp_info_address_export                            : out std_logic_vector(3 downto 0);                     -- export
            reg_sdp_info_clk_export                                : out std_logic;                                        -- export
            reg_sdp_info_read_export                               : out std_logic;                                        -- export
            reg_sdp_info_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_sdp_info_reset_export                              : out std_logic;                                        -- export
            reg_sdp_info_write_export                              : out std_logic;                                        -- export
            reg_sdp_info_writedata_export                          : out std_logic_vector(31 downto 0);                    -- export
            reg_si_address_export                                  : out std_logic_vector(0 downto 0);                     -- export
            reg_si_clk_export                                      : out std_logic;                                        -- export
            reg_si_read_export                                     : out std_logic;                                        -- export
            reg_si_readdata_export                                 : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_si_reset_export                                    : out std_logic;                                        -- export
            reg_si_write_export                                    : out std_logic;                                        -- export
            reg_si_writedata_export                                : out std_logic_vector(31 downto 0);                    -- export
            reg_stat_enable_bst_address_export                     : out std_logic_vector(1 downto 0);                     -- export
            reg_stat_enable_bst_clk_export                         : out std_logic;                                        -- export
            reg_stat_enable_bst_read_export                        : out std_logic;                                        -- export
            reg_stat_enable_bst_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_stat_enable_bst_reset_export                       : out std_logic;                                        -- export
            reg_stat_enable_bst_write_export                       : out std_logic;                                        -- export
            reg_stat_enable_bst_writedata_export                   : out std_logic_vector(31 downto 0);                    -- export
            reg_stat_enable_sst_address_export                     : out std_logic_vector(0 downto 0);                     -- export
            reg_stat_enable_sst_clk_export                         : out std_logic;                                        -- export
            reg_stat_enable_sst_read_export                        : out std_logic;                                        -- export
            reg_stat_enable_sst_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_stat_enable_sst_reset_export                       : out std_logic;                                        -- export
            reg_stat_enable_sst_write_export                       : out std_logic;                                        -- export
            reg_stat_enable_sst_writedata_export                   : out std_logic_vector(31 downto 0);                    -- export
            reg_stat_enable_xst_address_export                     : out std_logic_vector(0 downto 0);                     -- export
            reg_stat_enable_xst_clk_export                         : out std_logic;                                        -- export
            reg_stat_enable_xst_read_export                        : out std_logic;                                        -- export
            reg_stat_enable_xst_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_stat_enable_xst_reset_export                       : out std_logic;                                        -- export
            reg_stat_enable_xst_write_export                       : out std_logic;                                        -- export
            reg_stat_enable_xst_writedata_export                   : out std_logic_vector(31 downto 0);                    -- export
            reg_stat_hdr_dat_bst_address_export                    : out std_logic_vector(6 downto 0);                     -- export
            reg_stat_hdr_dat_bst_clk_export                        : out std_logic;                                        -- export
            reg_stat_hdr_dat_bst_read_export                       : out std_logic;                                        -- export
            reg_stat_hdr_dat_bst_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_stat_hdr_dat_bst_reset_export                      : out std_logic;                                        -- export
            reg_stat_hdr_dat_bst_write_export                      : out std_logic;                                        -- export
            reg_stat_hdr_dat_bst_writedata_export                  : out std_logic_vector(31 downto 0);                    -- export
            reg_stat_hdr_dat_sst_address_export                    : out std_logic_vector(5 downto 0);                     -- export
            reg_stat_hdr_dat_sst_clk_export                        : out std_logic;                                        -- export
            reg_stat_hdr_dat_sst_read_export                       : out std_logic;                                        -- export
            reg_stat_hdr_dat_sst_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_stat_hdr_dat_sst_reset_export                      : out std_logic;                                        -- export
            reg_stat_hdr_dat_sst_write_export                      : out std_logic;                                        -- export
            reg_stat_hdr_dat_sst_writedata_export                  : out std_logic_vector(31 downto 0);                    -- export
            reg_stat_hdr_dat_xst_address_export                    : out std_logic_vector(5 downto 0);                     -- export
            reg_stat_hdr_dat_xst_clk_export                        : out std_logic;                                        -- export
            reg_stat_hdr_dat_xst_read_export                       : out std_logic;                                        -- export
            reg_stat_hdr_dat_xst_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_stat_hdr_dat_xst_reset_export                      : out std_logic;                                        -- export
            reg_stat_hdr_dat_xst_write_export                      : out std_logic;                                        -- export
            reg_stat_hdr_dat_xst_writedata_export                  : out std_logic_vector(31 downto 0);                    -- export
            reg_tr_10gbe_eth10g_address_export                     : out std_logic_vector(2 downto 0);                     -- export
            reg_tr_10gbe_eth10g_clk_export                         : out std_logic;                                        -- export
            reg_tr_10gbe_eth10g_read_export                        : out std_logic;                                        -- export
            reg_tr_10gbe_eth10g_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_tr_10gbe_eth10g_reset_export                       : out std_logic;                                        -- export
            reg_tr_10gbe_eth10g_write_export                       : out std_logic;                                        -- export
            reg_tr_10gbe_eth10g_writedata_export                   : out std_logic_vector(31 downto 0);                    -- export
            reg_tr_10gbe_mac_address_export                        : out std_logic_vector(14 downto 0);                    -- export
            reg_tr_10gbe_mac_clk_export                            : out std_logic;                                        -- export
            reg_tr_10gbe_mac_read_export                           : out std_logic;                                        -- export
            reg_tr_10gbe_mac_readdata_export                       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_tr_10gbe_mac_reset_export                          : out std_logic;                                        -- export
            reg_tr_10gbe_mac_write_export                          : out std_logic;                                        -- export
            reg_tr_10gbe_mac_writedata_export                      : out std_logic_vector(31 downto 0);                    -- export
            reg_unb_pmbus_address_export                           : out std_logic_vector(5 downto 0);                     -- export
            reg_unb_pmbus_clk_export                               : out std_logic;                                        -- export
            reg_unb_pmbus_read_export                              : out std_logic;                                        -- export
            reg_unb_pmbus_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_unb_pmbus_reset_export                             : out std_logic;                                        -- export
            reg_unb_pmbus_write_export                             : out std_logic;                                        -- export
            reg_unb_pmbus_writedata_export                         : out std_logic_vector(31 downto 0);                    -- export
            reg_unb_sens_address_export                            : out std_logic_vector(5 downto 0);                     -- export
            reg_unb_sens_clk_export                                : out std_logic;                                        -- export
            reg_unb_sens_read_export                               : out std_logic;                                        -- export
            reg_unb_sens_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_unb_sens_reset_export                              : out std_logic;                                        -- export
            reg_unb_sens_write_export                              : out std_logic;                                        -- export
            reg_unb_sens_writedata_export                          : out std_logic_vector(31 downto 0);                    -- export
            reg_wdi_address_export                                 : out std_logic_vector(0 downto 0);                     -- export
            reg_wdi_clk_export                                     : out std_logic;                                        -- export
            reg_wdi_read_export                                    : out std_logic;                                        -- export
            reg_wdi_readdata_export                                : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_wdi_reset_export                                   : out std_logic;                                        -- export
            reg_wdi_write_export                                   : out std_logic;                                        -- export
            reg_wdi_writedata_export                               : out std_logic_vector(31 downto 0);                    -- export
            reg_wg_address_export                                  : out std_logic_vector(5 downto 0);                     -- export
            reg_wg_clk_export                                      : out std_logic;                                        -- export
            reg_wg_read_export                                     : out std_logic;                                        -- export
            reg_wg_readdata_export                                 : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            reg_wg_reset_export                                    : out std_logic;                                        -- export
            reg_wg_write_export                                    : out std_logic;                                        -- export
            reg_wg_writedata_export                                : out std_logic_vector(31 downto 0);                    -- export
            reset_reset_n                                          : in  std_logic                     := 'X';             -- reset_n
            rom_system_info_address_export                         : out std_logic_vector(12 downto 0);                    -- export
            rom_system_info_clk_export                             : out std_logic;                                        -- export
            rom_system_info_read_export                            : out std_logic;                                        -- export
            rom_system_info_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            rom_system_info_reset_export                           : out std_logic;                                        -- export
            rom_system_info_write_export                           : out std_logic;                                        -- export
            rom_system_info_writedata_export                       : out std_logic_vector(31 downto 0)                     -- export
        );
    end component qsys_lofar2_unb2b_sdp_station;
end qsys_lofar2_unb2b_sdp_station_pkg;
