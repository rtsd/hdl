--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Test bench to verify tr_nonbonded using the PHY GX IP
-- Description:
--   The test bench sends diagnostics data from FN to BN and from BN to FN. The
--   data gets serialized by tr_nonbonded.
--
--            fn_dp_tx_sosi_arr                       bn_dp_rx_sosi_arr
--            fn_dp_tx_siso_arr     fn_tx_dataout     bn_dp_rx_siso_arr
--                   |                   |                   |
--                   |                   |                   |
--  |--------------| | |---------------| | |---------------| | |--------------|
--  |              |-->|               |-->|               |-->|              |
--  |Diagnostics FN|   |tr_nonbonded FN|   |tr_nonbonded BN|   |Diagnostics BN|
--  |              |<--|               |<--|               |<--|              |
--  |--------------| | |---------------| | |---------------| | |--------------|
--                   |                   |                   |
--                   |                   |                   |
--            fn_dp_rx_sosi_arr     fn_rx_datain      bn_dp_tx_sosi_arr
--            fn_dp_rx_siso_arr                       bn_dp_tx_siso_arr
--
-- Remarks:
--   1) The number of data streams is set by c_nof_gx=2.
--   2) With g_sim_level=0 tr_nonbonded simulates the PHY GX IP technology.
--      With g_sim_level=1 tr_nonbonded simulates the PHY GX model.
--
-- Usage:
--   > as 10
--   > run 50 us
--   . observe that bn_dp_rx_sosi_arr and fn_dp_rx_sosi_arr receive counter
--   . the tb is self checking because after about 10 us the diagnostics
--     components should show:
--        fn_snk_diag_res_val=active and fn_snk_diag_res=0 indicating OK
--        bn_snk_diag_res_val=active and bn_snk_diag_res=0 indicating OK
--
-- Usage with enforced error:
--   > as 10
--   > do tb_tr_nonbonded.do
--   . when the link error occurs the bn_snk_diag_res will become /= 0
--     due to the link error.
--

library IEEE, tr_nonbonded_lib, common_lib, dp_lib, diagnostics_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tb_tr_nonbonded is
  generic (
    g_tb_end    : boolean := true;  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
    g_data_w    : natural := 32;
    g_sim_level : natural := 1
  );
  port (
    tb_end : out std_logic
  );
end entity tb_tr_nonbonded;

architecture str of tb_tr_nonbonded is
  constant c_sim                 : boolean := true;
  constant c_nof_gx              : natural := 1;
  constant c_mbps                : natural := 6250;

  constant tr_clk_period         : time := 6.4 ns;  -- 156.25 MHz
  constant st_clk_period         : time := 5 ns;  -- 200 MHz

  constant cal_rec_clk_period    : time := 25 ns;  -- 40 MHz

  signal cal_rec_clk             : std_logic := '0';
  signal fn_tr_clk               : std_logic := '0';
  signal bn_tr_clk               : std_logic := '0';

  signal st_clk                  : std_logic := '0';
  signal st_rst                  : std_logic := '1';

  signal verify_en               : std_logic;

  signal fn_tx_dataout           : std_logic_vector(c_nof_gx - 1 downto 0);
  signal fn_rx_datain            : std_logic_vector(c_nof_gx - 1 downto 0);

  signal fn_dp_rx_sosi_arr       : t_dp_sosi_arr(c_nof_gx - 1 downto 0);
  signal fn_dp_rx_siso_arr       : t_dp_siso_arr(c_nof_gx - 1 downto 0);

  signal fn_dp_tx_sosi_arr       : t_dp_sosi_arr(c_nof_gx - 1 downto 0);
  signal fn_dp_tx_siso_arr       : t_dp_siso_arr(c_nof_gx - 1 downto 0);

  signal fn_src_val_cnt          : t_slv_32_arr(c_nof_gx - 1 downto 0);

  signal fn_snk_diag_res         : std_logic_vector(c_nof_gx - 1 downto 0);
  signal fn_snk_diag_res_val     : std_logic_vector(c_nof_gx - 1 downto 0);
  signal fn_snk_val_cnt          : t_slv_32_arr(c_nof_gx - 1 downto 0);

  signal bn_dp_rx_sosi_arr       : t_dp_sosi_arr(c_nof_gx - 1 downto 0);
  signal bn_dp_rx_siso_arr       : t_dp_siso_arr(c_nof_gx - 1 downto 0);

  signal bn_dp_tx_sosi_arr       : t_dp_sosi_arr(c_nof_gx - 1 downto 0);
  signal bn_dp_tx_siso_arr       : t_dp_siso_arr(c_nof_gx - 1 downto 0);

  signal bn_src_val_cnt          : t_slv_32_arr(c_nof_gx - 1 downto 0);

  signal bn_snk_diag_res         : std_logic_vector(c_nof_gx - 1 downto 0);
  signal bn_snk_diag_res_val     : std_logic_vector(c_nof_gx - 1 downto 0);
  signal bn_snk_val_cnt          : t_slv_32_arr(c_nof_gx - 1 downto 0);

  signal bn_clk_en               : std_logic := '0';
begin
  fn_tr_clk  <= not fn_tr_clk after tr_clk_period / 2;
  bn_tr_clk  <= not bn_tr_clk after tr_clk_period / 2 when bn_clk_en = '1' else '0';

  p_bn_tr_clk : process
  begin
    bn_clk_en <= '0';
    if g_sim_level = 0 then
      wait for 1.6 ns;  -- with PHY use bn_tr_clk 1/4 cycle behind fn_tr_clk, with sim model the all tr_clk must have the same phase
    end if;
    bn_clk_en <= '1';
    wait;
  end process;

  st_rst       <= '0' after 100 ns;
  st_clk       <= not st_clk after st_clk_period / 2;
  cal_rec_clk  <= not cal_rec_clk after cal_rec_clk_period / 2;

  u_tr_nonbonded_fn: entity WORK.tr_nonbonded
  generic map (
    g_data_w         => g_data_w,
    g_nof_gx         => c_nof_gx,
    g_mbps           => c_mbps,
    g_sim            => c_sim,
    g_sim_level      => g_sim_level,
    g_tx             => true,
    g_rx             => true,
    g_fifos          => true
  )
  port map (
    st_rst             => st_rst,
    st_clk             => st_clk,

    tr_clk             => fn_tr_clk,
    cal_rec_clk        => cal_rec_clk,

    --Serial data
    tx_dataout         => fn_tx_dataout,
    rx_datain          => fn_rx_datain,

    --Parallel data
    rx_rst             => OPEN,
    rx_clk             => OPEN,
    rx_sosi_arr        => OPEN,
    rx_siso_arr        => (others => c_dp_siso_rst),

    tx_rst             => OPEN,
    tx_clk             => OPEN,
    tx_sosi_arr        => (others => c_dp_sosi_rst),
    tx_siso_arr        => OPEN,

    dp_rx_sosi_arr     => fn_dp_rx_sosi_arr,
    dp_rx_siso_arr     => fn_dp_rx_siso_arr,
    dp_tx_sosi_arr     => fn_dp_tx_sosi_arr,
    dp_tx_siso_arr     => fn_dp_tx_siso_arr,

    tx_align_en        => (others => '0'),
    rx_align_en        => (others => '0')
  );

  u_diagnostics_fn: entity diagnostics_lib.diagnostics
  generic map (
    g_dat_w          => g_data_w,
    g_nof_streams    => c_nof_gx
  )
  port map (
    rst              => st_rst,
    clk              => st_clk,

    snk_out_arr      => fn_dp_rx_siso_arr,
    snk_in_arr       => fn_dp_rx_sosi_arr,
    snk_diag_en      => (others => '1'),
    snk_diag_md      => (others => '1'),
    snk_diag_res     => fn_snk_diag_res,
    snk_diag_res_val => fn_snk_diag_res_val,
    snk_val_cnt      => fn_snk_val_cnt,

    src_out_arr      => fn_dp_tx_sosi_arr,
    src_in_arr       => fn_dp_tx_siso_arr,
    src_diag_en      => (others => '1'),
    src_diag_md      => (others => '1'),
    src_val_cnt      => fn_src_val_cnt
  );

  u_tr_nonbonded_bn: entity WORK.tr_nonbonded
  generic map (
    g_data_w         => g_data_w,
    g_nof_gx         => c_nof_gx,
    g_mbps           => c_mbps,
    g_sim            => c_sim,
    g_sim_level      => g_sim_level,
    g_tx             => true,
    g_rx             => true,
    g_fifos          => true
  )
  port map (
    st_rst         => st_rst,
    st_clk         => st_clk,

    tr_clk         => bn_tr_clk,
    cal_rec_clk    => cal_rec_clk,

    --Serial data
    tx_dataout     => fn_rx_datain,
    rx_datain      => fn_tx_dataout,

    --Parallel data
    rx_rst         => OPEN,
    rx_clk         => OPEN,
    rx_sosi_arr    => OPEN,
    rx_siso_arr    => (others => c_dp_siso_rst),

    tx_rst         => OPEN,
    tx_clk         => OPEN,
    tx_sosi_arr    => (others => c_dp_sosi_rst),
    tx_siso_arr    => OPEN,

    dp_rx_sosi_arr => bn_dp_rx_sosi_arr,
    dp_rx_siso_arr => bn_dp_rx_siso_arr,
    dp_tx_sosi_arr => bn_dp_tx_sosi_arr,
    dp_tx_siso_arr => bn_dp_tx_siso_arr,

    tx_align_en        => (others => '0'),
    rx_align_en        => (others => '0')
  );

  u_diagnostics_bn: entity diagnostics_lib.diagnostics
  generic map (
    g_dat_w          => g_data_w,
    g_nof_streams    => c_nof_gx
  )
  port map (
    rst              => st_rst,
    clk              => st_clk,

    snk_out_arr      => bn_dp_rx_siso_arr,
    snk_in_arr       => bn_dp_rx_sosi_arr,
    snk_diag_en      => (others => '1'),
    snk_diag_md      => (others => '1'),
    snk_diag_res     => bn_snk_diag_res,
    snk_diag_res_val => bn_snk_diag_res_val,
    snk_val_cnt      => bn_snk_val_cnt,

    src_out_arr      => bn_dp_tx_sosi_arr,
    src_in_arr       => bn_dp_tx_siso_arr,
    src_diag_en      => (others => '1'),
    src_diag_md      => (others => '1'),
    src_val_cnt      => bn_src_val_cnt
  );

  -- Verification
  verify_en <= '0', '1' after 10 us;

  p_verify : process(verify_en, bn_snk_diag_res_val, bn_snk_diag_res, fn_snk_diag_res_val, fn_snk_diag_res)
  begin
    if verify_en = '1' then
      assert bn_snk_diag_res_val = c_slv1(c_nof_gx - 1 downto 0)
        report "BN rx diag not valid error"
        severity ERROR;
      assert bn_snk_diag_res = c_slv0(c_nof_gx - 1 downto 0)
        report "BN rx diag data error"
        severity ERROR;
      assert fn_snk_diag_res_val = c_slv1(c_nof_gx - 1 downto 0)
        report "FN rx diag not valid error"
        severity ERROR;
      assert fn_snk_diag_res = c_slv0(c_nof_gx - 1 downto 0)
        report "FN rx diag data error"
        severity ERROR;
    end if;
  end process;

  -- Duration
  p_tb_end : process
  begin
    tb_end <= '0';
    wait for 50 us;
    tb_end <= '1';
    if g_tb_end = false then
      report "Tb Simulation finished."
        severity NOTE;
    else
      report "Tb Simulation finished."
        severity FAILURE;
    end if;
    wait;
  end process;
end architecture str;
