-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Verify nw_ping_response.vhd
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_nw_ping_response is
end tb_nw_ping_response;

architecture tb of tb_nw_ping_response is
  -- Use c_tb_timeout to raise ERROR if there is no result
  constant c_tb_timeout : natural := 100;

  constant c_data_w : natural := 64;

  constant c_nof_ping_packet_fields : natural := 21;
  constant c_nof_udp_packet_fields : natural := 20;

  constant c_ping_packet : t_common_field_arr(c_nof_ping_packet_fields - 1 downto 0) := ( ( field_name_pad("eth_dst_mac"       ), "RW", 48, field_default(x"ffffffffffff") ),
                                                                                        ( field_name_pad("eth_src_mac"       ), "RW", 48, field_default(x"e840f2acff78") ),
                                                                                        ( field_name_pad("eth_type"          ), "RW", 16, field_default(x"0800") ),
                                                                                        ( field_name_pad("ip_version"        ), "RW",  4, field_default(4) ),
                                                                                        ( field_name_pad("ip_header_length"  ), "RW",  4, field_default(5) ),
                                                                                        ( field_name_pad("ip_services"       ), "RW",  8, field_default(0) ),
                                                                                        ( field_name_pad("ip_total_length"   ), "RW", 16, field_default(x"54") ),
                                                                                        ( field_name_pad("ip_identification" ), "RW", 16, field_default(x"3daf") ),
                                                                                        ( field_name_pad("ip_flags"          ), "RW",  3, field_default(2) ),
                                                                                        ( field_name_pad("ip_fragment_offset"), "RW", 13, field_default(0) ),
                                                                                        ( field_name_pad("ip_time_to_live"   ), "RW",  8, field_default(x"40") ),
                                                                                        ( field_name_pad("ip_protocol"       ), "RW",  8, field_default(1) ),
                                                                                        ( field_name_pad("ip_header_checksum"), "RW", 16, field_default(x"e580") ),
                                                                                        ( field_name_pad("ip_src_addr"       ), "RW", 32, field_default(x"0a570224") ),
                                                                                        ( field_name_pad("ip_dst_addr"       ), "RW", 32, field_default(x"0a5700a8") ),
                                                                                        ( field_name_pad("icmp_type"         ), "RW",  8, field_default(8) ),
                                                                                        ( field_name_pad("icmp_code"         ), "RW",  8, field_default(0) ),
                                                                                        ( field_name_pad("icmp_checksum"     ), "RW", 16, field_default(x"117B") ),
                                                                                        ( field_name_pad("icmp_identifier"   ), "RW", 16, field_default(x"7052") ),
                                                                                        ( field_name_pad("icmp_seq_number"   ), "RW", 16, field_default(x"0001") ),
                                                                                        ( field_name_pad("icmp_payload"      ), "RW", 160, field_default(x"123456789ABCDEF01234123456789ABCDEF01234") ) );

  constant c_udp_packet : t_common_field_arr(c_nof_udp_packet_fields - 1 downto 0) := ( ( field_name_pad("eth_dst_mac"       ), "RW", 48, field_default(x"00074306C700") ),
                                                                                      ( field_name_pad("eth_src_mac"       ), "RW", 48, field_default(0) ),
                                                                                      ( field_name_pad("eth_type"          ), "RW", 16, field_default(x"0800") ),
                                                                                      ( field_name_pad("ip_version"        ), "RW",  4, field_default(4) ),
                                                                                      ( field_name_pad("ip_header_length"  ), "RW",  4, field_default(5) ),
                                                                                      ( field_name_pad("ip_services"       ), "RW",  8, field_default(0) ),
                                                                                      ( field_name_pad("ip_total_length"   ), "RW", 16, field_default(810) ),
                                                                                      ( field_name_pad("ip_identification" ), "RW", 16, field_default(0) ),
                                                                                      ( field_name_pad("ip_flags"          ), "RW",  3, field_default(2) ),
                                                                                      ( field_name_pad("ip_fragment_offset"), "RW", 13, field_default(0) ),
                                                                                      ( field_name_pad("ip_time_to_live"   ), "RW",  8, field_default(127) ),
                                                                                      ( field_name_pad("ip_protocol"       ), "RW",  8, field_default(17) ),
                                                                                      ( field_name_pad("ip_header_checksum"), "RW", 16, field_default(0) ),
                                                                                      ( field_name_pad("ip_src_addr"       ), "RW", 32, field_default(0) ),
                                                                                      ( field_name_pad("ip_dst_addr"       ), "RW", 32, field_default(x"C0A80001") ),
                                                                                      ( field_name_pad("udp_src_port"      ), "RW", 16, field_default(0) ),
                                                                                      ( field_name_pad("udp_dst_port"      ), "RW", 16, field_default(0) ),
                                                                                      ( field_name_pad("udp_total_length"  ), "RW", 16, field_default(790) ),
                                                                                      ( field_name_pad("udp_checksum"      ), "RW", 16, field_default(0) ),
                                                                                      ( field_name_pad("udp_payload"       ), "RW", 160, field_default(x"123456789ABCDEF01234123456789ABCDEF01234") ) );

  constant c_ping_packet_len : natural := field_slv_out_len(c_ping_packet);
  constant c_udp_packet_len  : natural := field_slv_out_len(c_udp_packet);

  constant c_ping_packet_slv : std_logic_vector(c_ping_packet_len - 1 downto 0) := field_map_defaults(c_ping_packet);
  constant c_udp_packet_slv  : std_logic_vector(c_udp_packet_len  - 1 downto 0) := field_map_defaults(c_udp_packet);

  constant c_ping_packet_field_sel : std_logic_vector(c_nof_ping_packet_fields - 1 downto 0) := (others => '0');
  constant c_udp_packet_field_sel : std_logic_vector(c_nof_udp_packet_fields - 1 downto 0) := (others => '0');

  constant c_nof_ping_responses : natural := 20;

  signal tb_end             : std_logic := '0';
  signal clk                : std_logic := '1';
  signal rst                : std_logic := '1';
  signal ping_dp_field_blk_snk_in  : t_dp_sosi;
  signal ping_dp_field_blk_snk_out : t_dp_siso;
  signal ping_dp_field_blk_src_out : t_dp_sosi;
  signal ping_dp_field_blk_src_in  : t_dp_siso;

  signal udp_dp_field_blk_snk_in  : t_dp_sosi;
  signal udp_dp_field_blk_snk_out : t_dp_siso;
  signal udp_dp_field_blk_src_out : t_dp_sosi;
  signal udp_dp_field_blk_src_in  : t_dp_siso;

  signal nw_ping_response_snk_in  : t_dp_sosi;
  signal nw_ping_response_src_in  : t_dp_siso;
  signal nw_ping_response_src_out : t_dp_sosi;

  signal cnt : natural := 0;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  p_packets : process(clk)
  begin
    if rising_edge(clk) then

      ping_dp_field_blk_snk_in.data(c_ping_packet_len - 1  downto 0 ) <= c_ping_packet_slv;
      ping_dp_field_blk_snk_in.valid <= ping_dp_field_blk_snk_out.ready;
      ping_dp_field_blk_snk_in.sop <= ping_dp_field_blk_snk_out.ready;
      ping_dp_field_blk_snk_in.eop <= ping_dp_field_blk_snk_out.ready;

      udp_dp_field_blk_snk_in.data(c_udp_packet_len - 1  downto 0 ) <= c_udp_packet_slv;
      udp_dp_field_blk_snk_in.valid <= udp_dp_field_blk_snk_out.ready;
      udp_dp_field_blk_snk_in.sop <= udp_dp_field_blk_snk_out.ready;
      udp_dp_field_blk_snk_in.eop <= udp_dp_field_blk_snk_out.ready;
    end if;
  end process;

  p_stimuli : process(clk)
  variable start : boolean := false;
  begin
    if rising_edge(clk) then
      if cnt = 5 then
        nw_ping_response_snk_in.valid <= '0';
        nw_ping_response_snk_in.sop <= '0';
        nw_ping_response_snk_in.eop <= '0';
        if ping_dp_field_blk_src_out.sop = '1' then
          start := true;
        end if;
        if start and ping_dp_field_blk_src_out.valid = '1' then
          nw_ping_response_snk_in <= ping_dp_field_blk_src_out;
        end if;
        if udp_dp_field_blk_src_out.eop = '1' then
          cnt <= 0;
          start := false;
        end if;

      else
        nw_ping_response_snk_in.valid <= '0';
        nw_ping_response_snk_in.sop <= '0';
        nw_ping_response_snk_in.eop <= '0';
        if udp_dp_field_blk_src_out.sop = '1' then
          start := true;
        end if;
        if start and udp_dp_field_blk_src_out.valid = '1' then
          nw_ping_response_snk_in <= udp_dp_field_blk_src_out;
        end if;
        if ping_dp_field_blk_src_out.eop = '1' then
          cnt <= cnt + 1;
          start := false;
        end if;
      end if;
    end if;
  end process;

  p_tb_end : process
  begin
    for I in 0 to 2 * c_nof_ping_responses loop
      proc_common_wait_until_evt(c_tb_timeout, clk, nw_ping_response_src_out.eop);
    end loop;
    tb_end <= '1';
    -- End with WAIT to avoid Warning: (vcom-1090) Possible infinite loop:
    -- Process contains no WAIT statement.
    wait;
  end process;

  nw_ping_response_src_in.xon <= '1';
  nw_ping_response_src_in.ready <= '1';
  dut : entity work.nw_ping_response
    port map (
      clk => clk,
      rst => rst,

      snk_in => nw_ping_response_snk_in,

      src_in  => nw_ping_response_src_in,
      src_out => nw_ping_response_src_out,

      eth_src_mac => x"14dda9ec2e0f"

    );

  ping_dp_field_blk_src_in.ready <= '1';
  ping_dp_field_blk_src_in.xon <= '1';
  udp_dp_field_blk_src_in.ready <= '1';
  udp_dp_field_blk_src_in.xon <= '1';

  u_ping_dp_field_blk : entity dp_lib.dp_field_blk
    generic map (
      g_field_arr      => c_ping_packet,
      g_field_sel      => c_ping_packet_field_sel,
      g_snk_data_w     => c_ping_packet_len,
      g_src_data_w     => c_data_w,
      g_in_symbol_w    => c_byte_w,
      g_out_symbol_w   => c_byte_w
    )
    port map (
      dp_clk       => clk,
      dp_rst       => rst,

      mm_clk       => '0',
      mm_rst       => '0',

      snk_in       => ping_dp_field_blk_snk_in,
      snk_out      => ping_dp_field_blk_snk_out,

      src_out      => ping_dp_field_blk_src_out,
      src_in       => ping_dp_field_blk_src_in
    );

  u_udp_dp_field_blk : entity dp_lib.dp_field_blk
    generic map (
      g_field_arr      => c_udp_packet,
      g_field_sel      => c_udp_packet_field_sel,
      g_snk_data_w     => c_udp_packet_len,
      g_src_data_w     => c_data_w,
      g_in_symbol_w    => c_byte_w,
      g_out_symbol_w   => c_byte_w
    )
    port map (
      dp_clk       => clk,
      dp_rst       => rst,

      mm_clk       => '0',
      mm_rst       => '0',

      snk_in       => udp_dp_field_blk_snk_in,
      snk_out      => udp_dp_field_blk_snk_out,

      src_out      => udp_dp_field_blk_src_out,
      src_in       => udp_dp_field_blk_src_in
    );
end tb;
