###############################################################################
# 
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################


README.txt for $HDL_WORK/libraries/dsp/wpfb/quartus_iwave
VERSION 01 - 20231219

Contents:

1) Purpose
2) Description
3) Parameters (ALMA vs LOFAR2)
4) Synthesis
5) Agilex7 issues
6) Remarks
7) References



1) Purpose

  To investigate the resource usage, timing reports with fmax summary and time critical
  paths by synthesis of the Subband Filterbank (wpfb_unit_dev.vhd) for the Agilex 7
  (agi027_1e1v) FPGA with buildset iwave.


2) Description 

  The synthesis is the second step to verify the correct working and limits of the wpfb
  and underlying designs and libraries for this new FPGA. Therefore all the FPGA specific
  components (IPs) are ported and sometimes an implementation is made to still be able to
  use the generic designs for this and all other FPGA types. 

  I created a Quartus project wpfb_unit_dev.qpf and qsf with the QUI that gets copied into
  build/iwave/quartus/iwave_synthesis_wpfb_lofar2/iwave_synthesis_wpfb_lofar2.qpf by
  the quartus_iwave/lofar2/hdllib.cfg.
  The wpfb_unit_dev component has too many IO to fit on the FPGA.
  Therefore it is necessary to use virtual pins, as explained in [1]. No separate
  After doing doing 'quartus_config iwave', then the iwave_synthesis_wpfb_lofar2.qpf
  (or .qsf) in the build dir can be opened in the QUI (started via run_quartus iwave &)
  and then do 'Start compilation'.


3) Parameters (ALMA vs LOFAR2)

  It is preferable to make a distinction between the selected parameters (ALMA vs LOFAR2).
  Normally with applications we will use revisions and a separate quartus folder.
  But when a synthesis folder is made for a particularly design, almost all the time the
  foldername is quartus or quartus_<buildset>. So this is the reason I made a distinction
  between alma and lofar2 in the quartus_iwave folder.

  . iwave_synthesis_wpfb_lofar2.vhd:
    synthesis compilation has runned with this design as top-level entity.

  . iwave_synthesis_wpfb_alma.vhd:
    The design for ALMA is already there. Intended to be a top-level entity for synthesis with
    ALMA parameters. Currently the same as LOFAR2. Therefore I added it with multiple comments
    with which parameters have to be changed when the ALMA parameters are determined.


4) Synthesis

 a) Results
  
    The Design Documentation can be found on Confluence [2].

    Important synthesis results are in the Design Assistant report. In the directionary it can be found
    under: $HDL_BUILD/iwave/quartus/iwave_synthesis_wpfb_lofar2/output_files/iwave_synthesis_wpfb_lofar2
      .fit.place.rpt    or
      .fit.finalize.rpt         ->  for resource usage
      .drc.partitioned.rpt      ->  for severity failures
      .tq.drc.signoff.rpt       ->  for timing analyzer

    It also can be found when running via QUI under:
      .  Fitter > Place Stage > Resource Usage Summary (/ Resource Utilazation by Entity) -> Resource usage
      .  Timing Analyzer > Fmax Summary                                                   -> Maximum Fclk due to timing path 
      .  Timing Analyzer > Worst-Case Timing Paths > Setup > (dp_clk or mm_clk)           -> Timing critical paths

    The most important information from these reports of WPFB implementation synthesis results with the 
    LOFAR2 parameters can be found on Confluence, see [3].

    The synthesis of the WPFB with the ALMA design parameters still needs to be done. Therefore this system-
    parameters must be determined, changed and included in the top-level entity design.

 b) How-to-do with QUI
  
    . Use the terminal command quartus_config <buildset> to create/update all the projectfiles for iwave.
    . QUI is the Quartus GUI. Run this software by the terminal command run_quartus <buildset> 
    . Open the project (.qpf or .qsf) in the $HDL_BUILD/iwave/quartus/iwave_synthesis_wpfb_lofar2 folder
    . by redo 'Start compilation'. After this compilation has ended open the fitter report with the QUI
      or in the outpul_files.

 c) How-to-do with terminal command using the ARGS Tooling (git repository)

    . Use the terminal command quartus_config <buildset> to create/update all the projectfiles for iwave.
    . Make sure numpy is installed.
    . Use the terminal command build_image <buildset> <project_design> <options --rev=<revision-to-use> 
      --seed=<seed(s)-to-use-for-fitting>>
      to run the complete synthesis. Such as: build_image unb2c unb2c_test --rev=unb2c_test_minimal
    . Use the next terminal commands to run parts of the the build_image command separately,
      that are needed by the synthesis:
      . Use the terminal command gen_rom_mmap.py --avalon -d <project_design> -r <revision-to-use>
        to generate fpgamap.py M&C Python client include file.
        Add flag --avalon to use config file qsys/sopc for base addresses.
        Such as: gen_rom_mmap.py --avalon -d unb2c_test -r unb2c_test_minimal
      . Use the terminal command run_qcomp <buildset> <project_design> <options --rev --clk --seed --32bit>
        to create the registermap for the buildset.
        Such as: run_qcomp unb2c unb2c_test_minimal
      . Use the terminal command run_rbf buildset> <project_design> <options --rev>
        Such as: run_rbf unb2c unb2c_test_minimal
  

5) Agilex7 issues

  Important crictical warnings:
  . Critical Warning(20759):  Use the Reset Release IP in Intel Agilex 7 FPGA designs to ensure a successful configuration.
                              For more information about the Reset Release IP, refer to the Intel Agilex Configuration User Guide. 
    FW engineer comment:      The Reset Release Intel FPGA IP available in the Intel Quartus Prime Pro software is necessary to use
                              in all Intel Agilex devices to hold your design in reset until the FPGA has finished with the
                              configuration process. This means for us that the Reset Release IP must be implemented in the BSP
                              (Board Support Package). See [4] and [5].

  Inimportant crictical warnings:
  . Critical Warning(332012): Synopsys Design Constraints File file not found: 'iwave_synthesis_wpfb_lofar2.sdc'.
                              A Synopsys Design Constraints File is required by the Timing Analyzer to get proper
                              timing constraints. Without it, the Compiler will not properly optimize the design. 
  . Critical Warning(19317):  No user constrained base clocks found in the design. Calling "derive_clocks -period 1.0" 
  . Critical Warning(332148): Timing requirements not met 
    FW engineer comment:      No need to a sdc file because default clock by Quartus is 1 GHz for finding Fmax.

  . Critical Warning(12677):  No exact pin location assignment(s) for 2 pins of 2 total pins.
                              For the list of pins please refer to the I/O Assignment Warnings table in the fitter report 
    FW engineer comment:      The two clock pins (dp_clock and mm_clock) are not assigned to virtual pins (commented out),
                              but Quartus will self specifies the fitter assigned location.
                              Refer Assignments > Pin Planner in Quartus to see tis locations.

  . Critical Warning(18636):  Compilation Report contains advance information. Specifications for device 
                              AGIB027R31A1I1VB are subject to change. Contact Intel for information on availability.
                              No programming file will be generated. 
    FW engineer comment:      The selected device part is for configuration is AGIB027R31A1I1VB, because the real 
                              device part is unknown until delivery. It is likely that the selected part, used 
                              for configuration, is not currently in production, and this inference can be drawn
                              from the presence of the warning.

  . Solutions to solved Critical Warnings and Errors are described in the qsf file.


6) Remarks:

  . 20231220_BrD: Updated set_global_assignment -name DEVICE from AGIB027R31A1I1VB to AGIB027R31B1E1V
                  This is the delivered part.


7) References:

 [1] "Virtual Pin Assignments in a Partial Design", Apr 17, 2021,
     https://www.youtube.com/watch?v=QET0lC-jdAQ
 [2] "Synthesis of the wpfb_unit_dev with quartus_iwave", Dec 18, 2023,
     https://support.astron.nl/confluence/display/SBe/FW+Design+Document%3A+Synthesis+of+the+wpfb_unit_dev+with+quartus_iwave
 [3] "Subband Filterbank = wpfb synthesis results iwave Agilex7", Dec 19, 2023,
     https://support.astron.nl/confluence/display/SBe/Subband+Filterbank+%3D+wpfb+synthesis+results+iwave+Agilex7s
 [4] "File: ug-ag-config-683673-777132-1.pdf; AN 891: Using the Reset Release Intel FPGA IP - User Guide", Sept 30, 2019 
     https://www.intel.com/content/dam/support/us/en/programmable/support-resources/bulk-container/pdfs/literature/an/archives/an891-19-3.pdf
     See Reset Release Chapter 4.3. Gating the PLL Reset Signal
 [5] "An Essential Reset for Intel® Stratix® 10 & Intel Agilex™ Devices", Jun 28, 2021
     https://www.youtube.com/watch?v=qhGfZwX9jKw
