-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Model the links between the FN and BN in the UniBoard TR mesh
-- Description: See unb1_board_mesh_reorder_bidir.vhd

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;

entity unb1_board_mesh_model_sosi is
  generic (
    g_reorder   : boolean := true
  );
  port (
    -- FN to BN
    fn0_tx_sosi_2arr  : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));  -- _2arr = (node id 3,2,1,0)(tr lane 3,2,1,0)
    fn1_tx_sosi_2arr  : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));
    fn2_tx_sosi_2arr  : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));
    fn3_tx_sosi_2arr  : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));

    bn0_rx_sosi_2arr  : out t_unb1_board_mesh_sosi_2arr;
    bn1_rx_sosi_2arr  : out t_unb1_board_mesh_sosi_2arr;
    bn2_rx_sosi_2arr  : out t_unb1_board_mesh_sosi_2arr;
    bn3_rx_sosi_2arr  : out t_unb1_board_mesh_sosi_2arr;

    -- BN to FN
    bn0_tx_sosi_2arr  : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));
    bn1_tx_sosi_2arr  : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));
    bn2_tx_sosi_2arr  : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));
    bn3_tx_sosi_2arr  : in  t_unb1_board_mesh_sosi_2arr := (others => (others => c_dp_sosi_rst));

    fn0_rx_sosi_2arr  : out t_unb1_board_mesh_sosi_2arr;
    fn1_rx_sosi_2arr  : out t_unb1_board_mesh_sosi_2arr;
    fn2_rx_sosi_2arr  : out t_unb1_board_mesh_sosi_2arr;
    fn3_rx_sosi_2arr  : out t_unb1_board_mesh_sosi_2arr
  );
end unb1_board_mesh_model_sosi;

architecture beh of unb1_board_mesh_model_sosi is
begin
  -- Functional mesh connect for transpose
  no_reorder : if g_reorder = false generate
    -- BN(i)(j) <= FN(j)(i)
    bn0_rx_sosi_2arr(0) <= fn0_tx_sosi_2arr(0);
    bn0_rx_sosi_2arr(1) <= fn1_tx_sosi_2arr(0);
    bn0_rx_sosi_2arr(2) <= fn2_tx_sosi_2arr(0);
    bn0_rx_sosi_2arr(3) <= fn3_tx_sosi_2arr(0);

    bn1_rx_sosi_2arr(0) <= fn0_tx_sosi_2arr(1);
    bn1_rx_sosi_2arr(1) <= fn1_tx_sosi_2arr(1);
    bn1_rx_sosi_2arr(2) <= fn2_tx_sosi_2arr(1);
    bn1_rx_sosi_2arr(3) <= fn3_tx_sosi_2arr(1);

    bn2_rx_sosi_2arr(0) <= fn0_tx_sosi_2arr(2);
    bn2_rx_sosi_2arr(1) <= fn1_tx_sosi_2arr(2);
    bn2_rx_sosi_2arr(2) <= fn2_tx_sosi_2arr(2);
    bn2_rx_sosi_2arr(3) <= fn3_tx_sosi_2arr(2);

    bn3_rx_sosi_2arr(0) <= fn0_tx_sosi_2arr(3);
    bn3_rx_sosi_2arr(1) <= fn1_tx_sosi_2arr(3);
    bn3_rx_sosi_2arr(2) <= fn2_tx_sosi_2arr(3);
    bn3_rx_sosi_2arr(3) <= fn3_tx_sosi_2arr(3);

    -- FN(i)(j) <= BN(j)(i)
    fn0_rx_sosi_2arr(0) <= bn0_tx_sosi_2arr(0);
    fn0_rx_sosi_2arr(1) <= bn1_tx_sosi_2arr(0);
    fn0_rx_sosi_2arr(2) <= bn2_tx_sosi_2arr(0);
    fn0_rx_sosi_2arr(3) <= bn3_tx_sosi_2arr(0);

    fn1_rx_sosi_2arr(0) <= bn0_tx_sosi_2arr(1);
    fn1_rx_sosi_2arr(1) <= bn1_tx_sosi_2arr(1);
    fn1_rx_sosi_2arr(2) <= bn2_tx_sosi_2arr(1);
    fn1_rx_sosi_2arr(3) <= bn3_tx_sosi_2arr(1);

    fn2_rx_sosi_2arr(0) <= bn0_tx_sosi_2arr(2);
    fn2_rx_sosi_2arr(1) <= bn1_tx_sosi_2arr(2);
    fn2_rx_sosi_2arr(2) <= bn2_tx_sosi_2arr(2);
    fn2_rx_sosi_2arr(3) <= bn3_tx_sosi_2arr(2);

    fn3_rx_sosi_2arr(0) <= bn0_tx_sosi_2arr(3);
    fn3_rx_sosi_2arr(1) <= bn1_tx_sosi_2arr(3);
    fn3_rx_sosi_2arr(2) <= bn2_tx_sosi_2arr(3);
    fn3_rx_sosi_2arr(3) <= bn3_tx_sosi_2arr(3);
  end generate;

  -- Actual UniBoard PCB mesh connect for transpose
  gen_reorder : if g_reorder = true generate
                                            -- BN, phy  <= FN, phy
    bn0_rx_sosi_2arr(0) <= fn3_tx_sosi_2arr(1);  -- 0,0 <= 3,1
    bn0_rx_sosi_2arr(1) <= fn2_tx_sosi_2arr(0);  -- 0,1 <= 2,0
    bn0_rx_sosi_2arr(2) <= fn1_tx_sosi_2arr(0);  -- 0,2 <= 1,0
    bn0_rx_sosi_2arr(3) <= fn0_tx_sosi_2arr(0);  -- 0,3 <= 0,0

    bn1_rx_sosi_2arr(0) <= fn3_tx_sosi_2arr(0);  -- 1,0 <= 3,0
    bn1_rx_sosi_2arr(1) <= fn2_tx_sosi_2arr(1);  -- 1,1 <= 2,1
    bn1_rx_sosi_2arr(2) <= fn1_tx_sosi_2arr(1);  -- 1,2 <= 1,1
    bn1_rx_sosi_2arr(3) <= fn0_tx_sosi_2arr(3);  -- 1,3 <= 0,3

    bn2_rx_sosi_2arr(0) <= fn3_tx_sosi_2arr(2);  -- 2,0 <= 3,2
    bn2_rx_sosi_2arr(1) <= fn2_tx_sosi_2arr(2);  -- 2,1 <= 2,2
    bn2_rx_sosi_2arr(2) <= fn0_tx_sosi_2arr(2);  -- 2,2 <= 0,2
    bn2_rx_sosi_2arr(3) <= fn1_tx_sosi_2arr(3);  -- 2,3 <= 1,3

    bn3_rx_sosi_2arr(0) <= fn3_tx_sosi_2arr(3);  -- 3,0 <= 3,3
    bn3_rx_sosi_2arr(1) <= fn0_tx_sosi_2arr(1);  -- 3,1 <= 0,1
    bn3_rx_sosi_2arr(2) <= fn2_tx_sosi_2arr(3);  -- 3,2 <= 2,3
    bn3_rx_sosi_2arr(3) <= fn1_tx_sosi_2arr(2);  -- 3,3 <= 1,2

                                             -- FN, phy <= BN, phy
    fn0_rx_sosi_2arr(0) <= bn0_tx_sosi_2arr(3);  -- 0,0 <= 0,3
    fn0_rx_sosi_2arr(1) <= bn3_tx_sosi_2arr(1);  -- 0,1 <= 3,1
    fn0_rx_sosi_2arr(2) <= bn2_tx_sosi_2arr(2);  -- 0,2 <= 2,2
    fn0_rx_sosi_2arr(3) <= bn1_tx_sosi_2arr(3);  -- 0,3 <= 1,3

    fn1_rx_sosi_2arr(0) <= bn0_tx_sosi_2arr(2);  -- 1,0 <= 0,2
    fn1_rx_sosi_2arr(1) <= bn1_tx_sosi_2arr(2);  -- 1,1 <= 1,2
    fn1_rx_sosi_2arr(2) <= bn3_tx_sosi_2arr(3);  -- 1,2 <= 3,3
    fn1_rx_sosi_2arr(3) <= bn2_tx_sosi_2arr(3);  -- 1,3 <= 2,3

    fn2_rx_sosi_2arr(0) <= bn0_tx_sosi_2arr(1);  -- 2,0 <= 0,1
    fn2_rx_sosi_2arr(1) <= bn1_tx_sosi_2arr(1);  -- 2,1 <= 1,1
    fn2_rx_sosi_2arr(2) <= bn2_tx_sosi_2arr(1);  -- 2,2 <= 2,1
    fn2_rx_sosi_2arr(3) <= bn3_tx_sosi_2arr(2);  -- 2,3 <= 3,2

    fn3_rx_sosi_2arr(0) <= bn1_tx_sosi_2arr(0);  -- 3,0 <= 1,0
    fn3_rx_sosi_2arr(1) <= bn0_tx_sosi_2arr(0);  -- 3,1 <= 0,0
    fn3_rx_sosi_2arr(2) <= bn2_tx_sosi_2arr(0);  -- 3,2 <= 2,0
    fn3_rx_sosi_2arr(3) <= bn3_tx_sosi_2arr(0);  -- 3,3 <= 3,0
  end generate;
end beh;
