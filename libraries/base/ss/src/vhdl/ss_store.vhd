-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose: Controller that store blocks of g_nof_ch_in complex input data
--          words in a dual page data buffer
-- Description:
--   Write databuf control for g_nof_ch_in complex input data words and pulse
--   store_done for each g_nof_ch_in data words that have been written.
-- Remarks:
-- . The SS stores the complex input data as concatenated data = im & re with
--   data width 2 * g_dsp_data_w.
-- . The SS does not use input sop and eop, because it uses a ch_cnt. Hence
--   the input_sosi only needs to carry im, re and valid, the sop and eop are
--   ignored. The ch_cnt is needed anyway to set the store_mosi.address. The
--   g_nof_ch_in defines the number of valid per input block, so from sop to
--   eop. The ss_store assumes that the first valid corresponds to a sop. The
--   ch_cnt restarts at the and of a block, so when ch_cnt = g_nof_ch_in-1.
-- . The store_done signal occurs when the last data of the block is being
--   written, so 1 cycle after the input_sosi.eop.

entity ss_store is
  generic (
    g_dsp_data_w  : natural;  -- = width of sosi.im = width of sosi.re
    g_nof_ch_in   : natural;  -- = nof valid per input block (sop to eop)
    g_use_complex : boolean := true  -- = TRUE --> use RE and IM field. FALSE = use DATA field
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;

    -- Streaming
    input_sosi    : in  t_dp_sosi;

    -- Timing
    store_done    : out std_logic;

    -- Write databuf control
    store_mosi    : out t_mem_mosi
  );
end ss_store;

architecture rtl of ss_store is
  signal ch_cnt           : integer range 0 to g_nof_ch_in - 1;
  signal nxt_ch_cnt       : integer;

  signal i_store_mosi     : t_mem_mosi;
  signal nxt_store_mosi   : t_mem_mosi := c_mem_mosi_rst;

  signal nxt_store_done   : std_logic;
begin
  store_mosi <= i_store_mosi;

  p_reg : process (clk, rst)
  begin
    if rst = '1' then
      -- Internal registers.
      ch_cnt          <= 0;
      -- Output registers.
      i_store_mosi    <= c_mem_mosi_rst;
      store_done      <= '0';
    elsif rising_edge(clk) then
      -- Internal registers.
      ch_cnt          <= nxt_ch_cnt;
      -- Output registers.
      i_store_mosi    <= nxt_store_mosi;
      store_done      <= nxt_store_done;
    end if;
  end process;

  p_ch_cnt : process (ch_cnt, input_sosi)
  begin
    nxt_store_done <= '0';
    nxt_ch_cnt <= ch_cnt;

    if input_sosi.valid = '1' then
      if ch_cnt = g_nof_ch_in - 1 then
        nxt_store_done <= '1';
        nxt_ch_cnt <= 0;
      else
        nxt_ch_cnt <= ch_cnt + 1;
      end if;
    end if;
  end process;

  -- store
  nxt_store_mosi.wr      <= input_sosi.valid;
  nxt_store_mosi.address <= TO_MEM_ADDRESS(ch_cnt) when input_sosi.valid = '1' else i_store_mosi.address;

  -- Use complex data fields
  gen_complex : if g_use_complex generate
    nxt_store_mosi.wrdata  <= RESIZE_MEM_DATA(input_sosi.im(g_dsp_data_w - 1 downto 0) & input_sosi.re(g_dsp_data_w - 1 downto 0)) when input_sosi.valid = '1' else i_store_mosi.wrdata;
  end generate;

  -- Use regular data field
  gen_non_complex : if not(g_use_complex) generate
    nxt_store_mosi.wrdata  <= RESIZE_MEM_DATA(input_sosi.data(c_nof_complex * g_dsp_data_w - 1 downto 0)) when input_sosi.valid = '1' else i_store_mosi.wrdata;
  end generate;
end rtl;
