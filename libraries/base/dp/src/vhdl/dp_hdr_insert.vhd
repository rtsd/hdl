--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Adds a header of g_hdr_nof_words long and g_data_w wide to a data stream with
-- the same width g_data_w.
-- g_data_w must be a multiple of both c_word_w (write data width used by the RAM)
-- and g_symbol_w.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_hdr_insert is
  generic (
    g_data_w          : natural;
    g_symbol_w        : natural;
    g_hdr_nof_words   : natural;
    g_internal_bypass : boolean := false;  -- can be used to eliminate external GENERATE-statements/wiring
    g_init_hdr        : string := "UNUSED";
    g_dp_on_at_init   : std_logic := '0'  -- Enable data path at default
  );
  port (
    mm_rst    : in  std_logic;
    mm_clk    : in  std_logic;

    st_rst    : in  std_logic;
    st_clk    : in  std_logic;

    ram_mosi : in  t_mem_mosi;  -- Hdr write port
    reg_mosi : in  t_mem_mosi;  -- Hdr release

    snk_out   : out t_dp_siso;
    snk_in    : in  t_dp_sosi;

    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end dp_hdr_insert;

architecture str of dp_hdr_insert is
  signal hdr_siso        : t_dp_siso;
  signal hdr_sosi        : t_dp_sosi;

  -- element (0) becomes the head, element (1) becomes the tail
  signal concat_siso_arr : t_dp_siso_arr(0 to 1);
  signal concat_sosi_arr : t_dp_sosi_arr(0 to 1);
begin
  no_bypass: if g_internal_bypass = false generate
    u_dp_ram_from_mm : entity work.mms_dp_ram_from_mm
    generic map (
      g_ram_wr_nof_words => g_hdr_nof_words * (g_data_w / c_word_w),
      g_ram_rd_dat_w     => g_data_w,
      g_init_file        => g_init_hdr,
      g_dp_on_at_init    => g_dp_on_at_init
    )
    port map (
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,

      st_rst      => st_rst,
      st_clk      => st_clk,

      reg_mosi    => reg_mosi,
      ram_mosi    => ram_mosi,

      src_in      => hdr_siso,
      src_out     => hdr_sosi
    );

    hdr_siso           <= concat_siso_arr(0);
    snk_out            <= concat_siso_arr(1);
    concat_sosi_arr(0) <= hdr_sosi;
    concat_sosi_arr(1) <= snk_in;

    u_dp_concat : entity work.dp_concat  -- RL = 1
    generic map (
      g_data_w    => g_data_w,
      g_symbol_w  => g_symbol_w
    )
    port map (
      rst         => st_rst,
      clk         => st_clk,
      -- ST sinks
      snk_out_arr => concat_siso_arr,
      snk_in_arr  => concat_sosi_arr,
      -- ST source
      src_in      => src_in,
      src_out     => src_out
    );
  end generate;

  gen_bypass : if g_internal_bypass = true generate
    src_out <= snk_in;
    snk_out <= src_in;
  end generate;
end str;
