-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

entity tb_tb_diag_rx_seq is
end tb_tb_diag_rx_seq;

architecture tb of tb_tb_diag_rx_seq is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_tx_cnt_incr    : INTEGER := 1;
  -- g_tx_mod         : INTEGER := 0;
  -- g_rx_use_steps   : BOOLEAN := TRUE;
  -- g_rx_steps_arr   : t_integer_arr(c_diag_seq_rx_reg_nof_steps-1 DOWNTO 0) := (OTHERS=>1);
  -- g_seq_dat_w      : NATURAL := 16

  u_diag_sel                   : entity work.tb_diag_rx_seq generic map ( 1,   0, false, (1,   1, 1, 1), 8);
  u_diag_steps_incr_1          : entity work.tb_diag_rx_seq generic map ( 1,   0,  true, (1,   1, 1, 1), 8);
  u_diag_steps_incr_neg_7      : entity work.tb_diag_rx_seq generic map (-7,   0,  true, (1,  -7, 1, 1), 8);
  u_diag_steps_tx_mod          : entity work.tb_diag_rx_seq generic map ( 1, 255,  true, (1,   2, 1, 1), 8);  -- rx_step = 2**g_seq_dat_w - tx_mod + g_tx_cnt_incr = 256 - 255 + 1 = 2
  u_diag_steps_tx_mod_incr_7   : entity work.tb_diag_rx_seq generic map ( 7, 255,  true, (7,   8, 7, 7), 8);  -- rx_step = 2**g_seq_dat_w - tx_mod + g_tx_cnt_incr = 256 - 255 + 7 = 8
  u_diag_steps_tx_mod_incr_5   : entity work.tb_diag_rx_seq generic map ( 5, 127,  true, (5, 134, 5, 5), 8);  -- rx_step = 2**g_seq_dat_w - tx_mod + g_tx_cnt_incr = 256 - 127 + 5 = 134
end tb;
