-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Purpose : Get info from ppsh into mm_clk domain
-- Description: See ppsh.vhd

entity mm_ppsh is
  generic (
    g_technology    : natural := c_tech_select_default;
    g_ext_clk_freq  : natural := 200 * 10**6  -- clock frequency of clk in Hz
  );
  port (
    ext_rst            : in  std_logic;
    ext_clk            : in  std_logic;
    ext_pps            : in  std_logic;
    ext_capture_edge   : in std_logic := '0';
    pps_pulse          : out std_logic;  -- pps pulse one clock cycle in ext_clk domain (i.e. 1 Hz pulse)
    pps_toggle         : out std_logic;  -- pps toggle in ext_clk domain (i.e. 0.5 Hz square wave)
    -- MM control
    mm_rst             : in  std_logic;
    mm_clk             : in  std_logic;
    mm_pps_pulse       : out std_logic;  -- pps_pulse in mm_clk domain
    mm_pps_toggle      : out std_logic;  -- pps_toggle in mm_clk domain
    mm_pps_capture_cnt : out std_logic_vector(ceil_log2(g_ext_clk_freq) - 1 downto 0)
  );
end mm_ppsh;

architecture str of mm_ppsh is
  -- ext_clk domain
  signal i_pps_pulse                : std_logic;
  signal i_pps_toggle               : std_logic;
  signal pps_capture_cnt            : std_logic_vector(ceil_log2(g_ext_clk_freq) - 1 downto 0);

  -- mm_clk domain
  signal i_mm_pps_pulse             : std_logic;
  signal i_mm_pps_toggle            : std_logic;
  signal nxt_mm_pps_toggle          : std_logic;
  signal i_mm_pps_capture_cnt       : std_logic_vector(ceil_log2(g_ext_clk_freq) - 1 downto 0);
  signal nxt_mm_pps_capture_cnt     : std_logic_vector(ceil_log2(g_ext_clk_freq) - 1 downto 0);
begin
  pps_pulse          <= i_pps_pulse;
  pps_toggle         <= i_pps_toggle;

  mm_pps_pulse       <= i_mm_pps_pulse;
  mm_pps_toggle      <= i_mm_pps_toggle;
  mm_pps_capture_cnt <= i_mm_pps_capture_cnt;

  p_mm_clk : process (mm_clk, mm_rst)
  begin
    if mm_rst = '1' then
      i_mm_pps_toggle      <= '0';
      i_mm_pps_capture_cnt <= (others => '1');
    elsif rising_edge(mm_clk) then
      i_mm_pps_toggle      <= nxt_mm_pps_toggle;
      i_mm_pps_capture_cnt <= nxt_mm_pps_capture_cnt;
    end if;
  end process;

  u_pps : entity work.ppsh
  generic map (
    g_technology => g_technology,
    g_clk_freq => g_ext_clk_freq
  )
  port map (
    rst           => ext_rst,
    clk           => ext_clk,
    -- PPS
    pps_ext       => ext_pps,
    pps_sys       => i_pps_pulse,
    -- MM control
    pps_toggle    => i_pps_toggle,
    capture_edge  => ext_capture_edge,
    capture_cnt   => pps_capture_cnt
  );

  -- Use pps_pulse to get the PPS info from u_pps into the MM clock domain,
  -- because after the pps_pulse the pps_toggle and pps_capture_cnt are stable
  u_pps_pulse : entity common_lib.common_spulse
  port map (
    in_rst       => ext_rst,
    in_clk       => ext_clk,
    in_pulse     => i_pps_pulse,
    out_rst      => mm_rst,
    out_clk      => mm_clk,
    out_pulse    => i_mm_pps_pulse
  );

  nxt_mm_pps_toggle      <= i_pps_toggle    when i_mm_pps_pulse = '1' else i_mm_pps_toggle;
  nxt_mm_pps_capture_cnt <= pps_capture_cnt when i_mm_pps_pulse = '1' else i_mm_pps_capture_cnt;
end str;
