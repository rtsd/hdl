-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: D. van der Schuur, E. Kooistra
-- Purpose:
-- . Split a user-defined header from a data block e.g. to from an Eth frame
-- Description:
-- . The dp_offload_tx_v3 --> dp_offload_rx form a pair.
-- . The header and data block can be split at symbol level, to support a header
--   that does not have an integer number of g_data_w. The header length is
--   defined by c_nof_header_symbols.
-- . The header contents can be monitored and is defined by g_hdr_field_arr.
-- . The data block is output via src_out_arr. If the data block contains an
--   integer number of g_data_w words, then src_out_arr.empty = 0.
-- . Default the g_symbol_w = 0 which implies g_symbol_w = g_data_w.
--   - If the header length is not an integer number of g_data_w, then
--     g_symbol_w has to be set such that c_nof_symbols_per_data and
--     c_nof_header_symbols are both integers. The g_symbol_w is then used
--     to control the split.
--   - If the data block length is not an integer number of g_data_w, then
--     g_symbol_w defines the symbol width of the data and is used to
--     control the snk_out_arr empty field.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;

entity dp_offload_rx is
  generic (
    g_nof_streams   : natural;
    g_data_w        : natural;
    g_symbol_w      : natural := 0;  -- default 0 yields g_symbol_w = g_data_w
    g_hdr_field_arr : t_common_field_arr;
    g_remove_crc    : boolean := false;
    g_crc_nof_words : natural := 0
  );
  port (
    mm_rst             : in  std_logic;
    mm_clk             : in  std_logic;

    dp_rst             : in  std_logic;
    dp_clk             : in  std_logic;

    reg_hdr_dat_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_hdr_dat_miso   : out t_mem_miso;

    snk_in_arr         : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    snk_out_arr        : out t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    src_out_arr        : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_in_arr         : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

    hdr_fields_out_arr : out t_slv_1024_arr(g_nof_streams - 1 downto 0);  -- Valid at src_out_arr(i).sop, use for sosi.sync
    hdr_fields_raw_arr : out t_slv_1024_arr(g_nof_streams - 1 downto 0)  -- Valid at src_out_arr(i).sop and beyond, use for sosi.bsn
  );
end dp_offload_rx;

architecture str of dp_offload_rx is
  constant c_symbol_w                 : natural := sel_a_b(g_symbol_w = 0, g_data_w, g_symbol_w);
  constant c_nof_symbols_per_data     : natural := g_data_w / c_symbol_w;
  constant c_nof_header_symbols       : natural := field_slv_len(g_hdr_field_arr) / c_symbol_w;

  constant c_field_sel                 : std_logic_vector(g_hdr_field_arr'range) := (others => '0');  -- Not used in sink mode but requires set range

  signal dp_split_src_out_2arr        : t_dp_sosi_2arr_2(g_nof_streams - 1 downto 0);
  signal dp_split_src_in_2arr         : t_dp_siso_2arr_2(g_nof_streams - 1 downto 0);

  signal reg_hdr_dat_mosi_arr         : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_hdr_dat_miso_arr         : t_mem_miso_arr(g_nof_streams - 1 downto 0);

  signal dp_field_blk_slv_out         : t_slv_1024_arr(g_nof_streams - 1 downto 0) := (others => (others => '0'));
  signal dp_field_blk_slv_out_val     : std_logic_vector(g_nof_streams - 1 downto 0);

  signal dp_field_blk_src_out_arr     : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal reg_dp_field_blk_slv_out     : t_slv_1024_arr(g_nof_streams - 1 downto 0);
  signal nxt_reg_dp_field_blk_slv_out : t_slv_1024_arr(g_nof_streams - 1 downto 0) := (others => (others => '0'));

  signal dp_tail_remove_src_out_arr   : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dp_tail_remove_src_in_arr    : t_dp_siso_arr(g_nof_streams - 1 downto 0);

  constant c_dp_field_blk_snk_data_w : natural := g_data_w;
  constant c_dp_field_blk_src_data_w : natural := field_slv_in_len(field_arr_set_mode(g_hdr_field_arr,  "RO"));
begin
  ---------------------------------------------------------------------------------------
  -- Remove header
  ---------------------------------------------------------------------------------------
  gen_nof_streams: for i in 0 to g_nof_streams - 1 generate
    u_dp_split : entity work.dp_split
    generic map (
      g_data_w        => g_data_w,
      g_symbol_w      => c_symbol_w,
      g_nof_symbols   => c_nof_header_symbols
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,

      snk_out     => snk_out_arr(i),
      snk_in      => snk_in_arr(i),

      src_in_arr  => dp_split_src_in_2arr(i),
      src_out_arr => dp_split_src_out_2arr(i)
    );

    -- In dp_split index 0 is head and index 1 is tail, but dp_split uses 0 TO
    -- 1 range and dp_split_src_in_2arr()() uses 1 DOWNTO 0 range, so:
    -- . dp_split_src_in_2arr()(1) is the header part, and
    -- . dp_split_src_in_2arr()(0) is the tail part.
    dp_split_src_in_2arr(i)(1) <= c_dp_siso_rdy;  -- flow control for rx header to MM is always ready
  end generate;

  ---------------------------------------------------------------------------------------
  -- Feed header block to dp_field_blk that enables MM reading the fields and
  -- field forwarding to data path
  ---------------------------------------------------------------------------------------
  gen_dp_field_blk : for i in 0 to g_nof_streams - 1 generate
    u_dp_field_blk : entity work.dp_field_blk
    generic map (
      g_field_arr    => field_arr_set_mode(g_hdr_field_arr,  "RO"),
      g_field_sel    => c_field_sel,
      g_snk_data_w   => c_dp_field_blk_snk_data_w,  -- g_data_w,
      g_src_data_w   => c_dp_field_blk_src_data_w,  -- field_slv_in_len(field_arr_set_mode(g_hdr_field_arr , "RO"))
      g_in_symbol_w  => c_symbol_w,
      g_out_symbol_w => c_symbol_w,
      g_mode         => 2  -- sink mode
    )
    port map (
      dp_rst       => dp_rst,
      dp_clk       => dp_clk,

      mm_rst       => mm_rst,
      mm_clk       => mm_clk,

      snk_in       => dp_split_src_out_2arr(i)(1),

      src_out      => dp_field_blk_src_out_arr(i),

--      slv_out      => dp_field_blk_slv_out(i)(field_slv_len(g_hdr_field_arr)-1 DOWNTO 0),
--      slv_out_val  => dp_field_blk_slv_out_val(i),

      reg_slv_mosi => reg_hdr_dat_mosi_arr(i),
      reg_slv_miso => reg_hdr_dat_miso_arr(i)
    );

    dp_field_blk_slv_out(i)(c_dp_field_blk_src_data_w - 1 downto 0) <= dp_field_blk_src_out_arr(i).data(c_dp_field_blk_src_data_w - 1 downto 0);
    dp_field_blk_slv_out_val(i) <= dp_field_blk_src_out_arr(i).valid;
  end generate;

  ---------------------------------------------------------------------------------------
  -- Remove CRC if phy link is used
  ---------------------------------------------------------------------------------------
  gen_dp_tail_remove : for i in 0 to g_nof_streams - 1 generate
    u_dp_tail_remove : entity work.dp_tail_remove
    generic map (
      g_data_w      => g_data_w,
      g_symbol_w    => c_symbol_w,
      g_nof_symbols => sel_a_b(g_remove_crc, g_crc_nof_words, 0)
    )
    port map (
      st_rst  => dp_rst,
      st_clk  => dp_clk,

      snk_out => dp_split_src_in_2arr(i)(0),
      snk_in  => dp_split_src_out_2arr(i)(0),  -- tail part

      src_in  => dp_tail_remove_src_in_arr(i),
      src_out => dp_tail_remove_src_out_arr(i)
    );
  end generate;

  src_out_arr <= dp_tail_remove_src_out_arr;
  dp_tail_remove_src_in_arr <= src_in_arr;

  ---------------------------------------------------------------------------------------
  -- Sync header fields to dp_tail_remove_src_out_arr(i).sop
  ---------------------------------------------------------------------------------------

  -- Store the header fields
  p_clk : process(dp_clk, dp_rst)
  begin
    if dp_rst = '1' then
      reg_dp_field_blk_slv_out <= (others => (others => '0'));
    elsif rising_edge(dp_clk) then
      reg_dp_field_blk_slv_out <= nxt_reg_dp_field_blk_slv_out;
    end if;
  end process;

  gen_reg_dp_fields_blk_slv : for i in 0 to g_nof_streams - 1 generate
    nxt_reg_dp_field_blk_slv_out(i) <= dp_field_blk_slv_out(i) when dp_field_blk_slv_out_val(i) = '1' else reg_dp_field_blk_slv_out(i);
  end generate;

  -- Put the fields that can only be active at sop on the entity output at src_out.sop (e.g. sosi sync field)
  gen_hdr_fields_out_arr : for i in 0 to g_nof_streams - 1 generate
    hdr_fields_out_arr(i) <= reg_dp_field_blk_slv_out(i) when dp_tail_remove_src_out_arr(i).sop = '1' else (others => '0');
  end generate;

  -- Put the fields that can be valid during entire packet on the entity combinatorially (e.g. all sosi slv fields)
  hdr_fields_raw_arr <= reg_dp_field_blk_slv_out;

  ---------------------------------------------------------------------------------------
  -- MM control & monitoring
  ---------------------------------------------------------------------------------------
  u_common_mem_mux_hdr_dat : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => ceil_log2(field_nof_words(g_hdr_field_arr, c_word_w))
  )
  port map (
    mosi     => reg_hdr_dat_mosi,
    miso     => reg_hdr_dat_miso,
    mosi_arr => reg_hdr_dat_mosi_arr,
    miso_arr => reg_hdr_dat_miso_arr
  );
end str;
