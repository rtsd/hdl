###############################################################################
#
# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Back node specific pin assignments

# -- Backplane Interface
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/BACK_NODE_tr_pins.tcl

# -- FN to BN Interface added 08-01-2010
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/BACK_NODE_mesh_pins.tcl

# -- ADC Interface
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/BACK_NODE_adc_pins.tcl
