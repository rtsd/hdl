# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and other 
# software and tools, and its AMPP partner logic functions, and any output 
# files any of the foregoing (including device programming or simulation 
# files), and any associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License Subscription 
# Agreement, Altera MegaCore Function License Agreement, or other applicable 
# license agreement, including, without limitation, that your use is for the 
# sole purpose of programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the applicable 
# agreement for further details.







puts " "
puts "---------------------------------------------------------------------------------------------"
puts "-- checking the integrity of the Abstract phy force file altera_emif_nios_forces_abphy.sv ---"
puts "---------------------------------------------------------------------------------------------"

proc ::findFiles {} {
  set filelst [list "../altera_emif_arch_nf_151/sim/altera_emif_nios_forces_abphy.sv" \
    "../../altera_emif_arch_nf_151/sim/altera_emif_nios_forces_abphy.sv" \
    "../../../altera_emif_arch_nf_151/sim/altera_emif_nios_forces_abphy.sv" \
    "../altera_emif_arch_nf_160/sim/altera_emif_nios_forces_abphy.sv" \
    "../../altera_emif_arch_nf_160/sim/altera_emif_nios_forces_abphy.sv" \
    "../../../altera_emif_arch_nf_160/sim/altera_emif_nios_forces_abphy.sv"]

  set files {}
  foreach file_item $filelst {
    if { [file exists $file_item] } {
      lappend files $file_item
    }
  }
  return $files
}

set force_files [join [findFiles] \n]
foreach force_file $force_files {
  puts "checking integrity of : $force_file"
  set fid [open $force_file r]
  set file_data [read $fid]
  close $fid
  set file_ok   0
  foreach line [split $file_data "\n"] {
    if {[regexp -- {endmodule} $line]} {  
      set file_ok  1
    }
  }
  if { $file_ok == 0 } {
    puts "******** file corrupted re-writing : $force_file *******************"
    set fid [open $force_file w]

    puts $fid "// (C) 2001-2015 Altera Corporation. All rights reserved."
    puts $fid "// Your use of Altera Corporation's design tools, logic functions and other "
    puts $fid "// software and tools, and its AMPP partner logic functions, and any output" 
    puts $fid "// files any of the foregoing (including device programming or simulation "
    puts $fid "// files), and any associated documentation or information are expressly subject "
    puts $fid "// to the terms and conditions of the Altera Program License Subscription "
    puts $fid "// Agreement, Altera MegaCore Function License Agreement, or other applicable "
    puts $fid "// license agreement, including, without limitation, that your use is for the "
    puts $fid "// sole purpose of programming logic devices manufactured by Altera and sold by" 
    puts $fid "// Altera or its authorized distributors.  Please refer to the applicable "
    puts $fid "// agreement for further details."
    puts $fid " "
    puts $fid "module altera_emif_nios_force_abphy #("
    puts $fid "  parameter DIAG_USE_ABSTRACT_PHY                   = 0"
    puts $fid ")   ("
    puts $fid "  output reg                 cal_bus_clk,"
    puts $fid "  output reg                 cal_bus_avl_write,"
    puts $fid "  output reg \[19:0\]          cal_bus_avl_address,"
    puts $fid "  output reg \[31:0\]          cal_bus_avl_write_data,"
    puts $fid "  input                      runAbstractPhySim"
    puts $fid ")\;"
    puts $fid " "
    puts $fid "  timeunit 1ns\;"
    puts $fid "  timeprecision 1ps\;"
    puts $fid " "
    puts $fid "endmodule "
    close $fid
    
  } else {
    puts "file PASS : $force_file"
  }
}
puts " "

