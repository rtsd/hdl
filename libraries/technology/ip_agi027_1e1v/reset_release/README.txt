###############################################################################
# 
# Copyright 2024
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################


README.txt for $HDL_WORK/libraries/technology/ip_agi027_1e1v/reset_release
VERSION 01 - 20240105

Contents:

1) IP component
2) Compilation, simulation and verification
3) Synthesis
4) Remarks
5) References


1) IP component

  The reset_release IP component was created with Quartus 23.2 for agi027_1e1v.
  That was the result of a critical warning [1] during synthesis of
  iwave_synthesis_wpfb_lofar2.vhd, that is the top-level entity for 
  wpfb_unit_dev.vhd, when Agilex 7 is used. For more see 4) Remarks.
  A familar name that can be used is user_rst_clkgate due to the IP catalog name 
  altera_s10_user_rst_clkgate in Quartus. s10 is because it must also be used for 
  Stratix10.

  Description:
  This IP outputs nINIT_DONE after finishing device initalization. 
  User mode initialization can begin as soon as the nINIT_DONE signal asserts.

  The generated IPs are not kept in git repository, only the ip source files:

    ip_agi027_1e1v_reset_release_ci.ip
    ip_agi027_1e1v_reset_release_ri.ip

  Therefore first the IP needs to be generated using:

    generate_ip_libs iwave
  

2) Compilation, simulation and verification
  
  compile_ip.tcl is manually created (by using the msim_setup.tcl file, which is
  part of the generated IPs) and this compile_ip.tcl is added in the hdllib.cfg 
  to 'modelsim_compile_ip_files =' and gets compiled before the other code.
  However, a separate testbench has not been created for the FPGA reset release 
  IP in ModelSim Simulation and no other verification have to be done for creating
  the IP. 


3) Synthesis

  No synthesis trials were done by ASTRON, because this will implicitely be done
  when the IP is used in a design. For the iwave example_design also this IP (ci)
  is used, no issues are reported by the QUI during compilation.

  The QIP file:

    ip_agi027_1e1v_reset_release_ci.qip
    ip_agi027_1e1v_reset_release_ri.qip

  is included in the hdllib.cfg and contains what is needed to synthesize the IP.


4) Remarks

a) Necessary use for Agilex devices

  . Related to the critical warning [1]

  . According to the user guide [2] and a YouTube video from Intel [3], this IP is 
    necessary to use in all Intel Stratix 10 and Intel Agilex devices to hold your
    design in reset until the FPGAs have finished with the configuration process. 
    This means that this IP must be used, and some additions need to be made in the
    BSP (Board Support Package) and application design.

  . See [4] for documentation about this critcial warning on Confluence.

b) Reset output port can be a Reset Interface (ri) or Conduit Interface (ci):

  . ci =  conduit interface,   is the selected type of reset output port
    Desc. :   legacy reset output
  . ri =  reset interface,     is the selected type of reset output port
    Desc. :   allow reset connection in Platform Designer

  . The ci IP is used in the vendor FPGA Design Example. 

c) Choose between using or not using a separate library in altera_libraries:
  
   When multiple IPs are generated, each utilizing the same IP function but with
   different settings, it results in the generation of the same library name: 

    . When containing the same source files, no separate library has to be
      created (only when it is preferable for a reason).
    . When containing a different source file, as opposed to the previously
      used unique library names. This leads to issues. To address this, shared 
      libraries are combined within a single library with the IP-specific library
      name in the build directory when 'generate_ip_libs' is used. Therefore, a
      directory is manually created in 'altera_libraries' with the IP-specific
      library name, containing two files: 'compile_ip.tcl' and 'hdllib.cfg'.
 
  I)  No separate library (currently used)
  
    So when the same source files are used by the IPs (no hashes) use: 

       hdl_lib_name = ip_agi027_1e1v_<lib_name>
       hdl_library_clause_name = ip_agi027_1e1v_<lib_name>_lib

    Therefore the compile_ip.tcl has to vmap IP specific libraries, compile all
    IP source files into these libraries and to compile the sim source files:

      #repeat for all ip specific libraries
      vmap <lib_name>_<ip_specific> 

      set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_<ip_name>" 
        #repeat for all IP source files. When multiple same files? Only one IP source file is
        #needed for compilation into those libraries 
        vlog -sv <ip_specific>.sv -work <ip_specific>

      #Repeat for all IPs
      set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_<ip_name>/sim"
        vcom "$IP_DIR/<ip_name>.vhd"

  II) Separate library
  
    So when different source files are used by the IPs (hashes) or when it is
    preferable to move the library to the altera_libraries, use generated IP
    specific library clause name and IP specific lib uses sim.

    The generated ip_agi027_1e1v_<lib_name>.vhd uses an IP specific library name.
    Therefore the hdllib.cfg uses the IP specific part as library clause name and,
    in addition, uses lib uses sim to make it known:
  
      hdl_lib_name = ip_agi027_1e1v_<lib_name>
      hdl_library_clause_name = ip_agi027_1e1v_<lib_name>_<ip_specific>
      hdl_lib_uses_sim = ip_agi027_1e1v_<ip_specific>

    Therefore the compile_ip.tcl has only to compile the sim source files:

      #Repeat for all IPs
      set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_<ip_name>/sim"
        vcom "$IP_DIR/<ip_name>.vhd"

      #This means:    
      set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_reset_release_ci/sim"
        vcom "$IP_DIR/ip_agi027_1e1v_reset_release_ci.vhd"
      set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_reset_release_ri/sim"
        vcom "$IP_DIR/ip_agi027_1e1v_reset_release_ri.vhd"

    Therefore the altera_libraries hdllib.cfg uses ip specific part as library clause 
    name and assign the altera_libraries compile_ip.tcl to 'modelsim_compile_ip_files =':
    
      hdl_lib_name = ip_agi027_1e1v_<ip_specific>
      hdl_library_clause_name = <ip_specific>

      modelsim_compile_ip_files = 
          $HDL_WORK/libraries/technology/ip_agi027_1e1v/altera_libraries/<ip_specific>/compile_ip.tcl

    Therefore the altera_libraries compile.tcl has to vmap IP specific libraries, compile all
    IP source files into these 'shared' libraries:
 
      #repeat for all ip specific libraries
      vmap <lib_name>_<ip_specific> 
     
      set IP_DIR "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_agi027_1e1v_<ip_name>" 
        #repeat for all IP source files. When multiple same files? Only one IP source file is
        #needed for compilation into those libraries 
        vlog -sv <ip_specific>.sv -work <ip_specific>


5) References:

  [1] Critical Warning(20759):  Use the Reset Release IP in Intel Agilex 7 FPGA designs to ensure a successful configuration. 
                                For more information about the Reset Release IP, refer to the Intel Agilex Configuration User Guide.
  [2] "File: ug-ag-config-683673-777132-1.pdf; AN 891: Using the Reset Release Intel FPGA IP - User Guide", Sept 30, 2019 
      https://www.intel.com/content/dam/support/us/en/programmable/support-resources/bulk-container/pdfs/literature/an/archives/an891-19-3.pdf
      See Reset Release Chapter 4.3. Gating the PLL Reset Signal
  [3] "An Essential Reset for Intel® Stratix® 10 & Intel Agilex™ Devices", Jun 28, 2021
      https://www.youtube.com/watch?v=qhGfZwX9jKw
  [4] "Key porting highlights: Reset Release IP - needed for BSP", Jan 05, 2024,
      https://support.astron.nl/confluence/display/SBe/Reset+Release+IP+-+needed+for+BSP
