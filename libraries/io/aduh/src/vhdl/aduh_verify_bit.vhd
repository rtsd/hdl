--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Verify the adc08d1020 test pattern 0 1 0 0 1 1 0 0 1 0 per bit
-- Description:
--   Used in aduh_verify, see there for explanation

entity aduh_verify_bit is
  generic (
    g_nof_symbols_per_data : natural := 4  -- Fixed, big endian in_sosi.data, t0 in MSsymbol, so [h:0] = [t0]&[t1]&[t2]&[t3]
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    -- ST input
    in_val         : in  std_logic;
    in_dat         : in  std_logic_vector(0 to g_nof_symbols_per_data - 1);
    in_dat_err     : in  std_logic := '0';

    -- Static control input (connect via MM or leave open to use default)
    verify_res     : out std_logic;
    verify_res_val : out std_logic;
    verify_res_ack : in  std_logic
  );
end aduh_verify_bit;

architecture rtl of aduh_verify_bit is
  constant c_nof_init    : natural := 2;  -- need two dat words to initialize the ref_dat

  type t_state is (s_init, s_verify);

  function func_tp_seq(prev_nibble, nibble : std_logic_vector) return std_logic_vector is
  begin
    if unsigned(prev_nibble) = 16#4# and unsigned(nibble) = 16#C# then return TO_UVEC(16#9#, g_nof_symbols_per_data); end if;
    if unsigned(prev_nibble) = 16#C# and unsigned(nibble) = 16#9# then return TO_UVEC(16#3#, g_nof_symbols_per_data); end if;
    if unsigned(prev_nibble) = 16#9# and unsigned(nibble) = 16#3# then return TO_UVEC(16#2#, g_nof_symbols_per_data); end if;
    if unsigned(prev_nibble) = 16#3# and unsigned(nibble) = 16#2# then return TO_UVEC(16#4#, g_nof_symbols_per_data); end if;
    if unsigned(prev_nibble) = 16#2# and unsigned(nibble) = 16#4# then return TO_UVEC(16#C#, g_nof_symbols_per_data); end if;

    if unsigned(prev_nibble) = 16#9# and unsigned(nibble) = 16#9# then return TO_UVEC(16#2#, g_nof_symbols_per_data); end if;
    if unsigned(prev_nibble) = 16#9# and unsigned(nibble) = 16#2# then return TO_UVEC(16#6#, g_nof_symbols_per_data); end if;
    if unsigned(prev_nibble) = 16#2# and unsigned(nibble) = 16#6# then return TO_UVEC(16#4#, g_nof_symbols_per_data); end if;
    if unsigned(prev_nibble) = 16#6# and unsigned(nibble) = 16#4# then return TO_UVEC(16#9#, g_nof_symbols_per_data); end if;
    if unsigned(prev_nibble) = 16#4# and unsigned(nibble) = 16#9# then return TO_UVEC(16#9#, g_nof_symbols_per_data); end if;

    return TO_UVEC(0, g_nof_symbols_per_data);  -- else return invalid value
  end;

  signal ref_dat             : std_logic_vector(0 to g_nof_symbols_per_data - 1);
  signal nxt_ref_dat         : std_logic_vector(0 to g_nof_symbols_per_data - 1);
  signal prev_ref_dat        : std_logic_vector(0 to g_nof_symbols_per_data - 1);
  signal nxt_prev_ref_dat    : std_logic_vector(0 to g_nof_symbols_per_data - 1);

  signal state               : t_state;
  signal nxt_state           : t_state;
  signal init_done           : std_logic;  -- fits NATURAL RANGE 0 TO c_nof_init-1;
  signal nxt_init_done       : std_logic;
  signal i_verify_res        : std_logic;
  signal nxt_verify_res      : std_logic;
  signal i_verify_res_val    : std_logic;
  signal nxt_verify_res_val  : std_logic;
begin
  verify_res     <= i_verify_res;
  verify_res_val <= i_verify_res_val;

  p_reg : process (rst, clk)
  begin
    if rst = '1' then
      ref_dat          <= (others => '0');
      prev_ref_dat     <= (others => '0');
      init_done        <= '0';
      i_verify_res     <= '0';
      i_verify_res_val <= '0';
      state            <= s_init;
    elsif rising_edge(clk) then
      ref_dat          <= nxt_ref_dat;
      prev_ref_dat     <= nxt_prev_ref_dat;
      init_done        <= nxt_init_done;
      i_verify_res     <= nxt_verify_res;
      i_verify_res_val <= nxt_verify_res_val;
      state            <= nxt_state;
    end if;
  end process;

  p_state : process(ref_dat, prev_ref_dat, init_done, i_verify_res, i_verify_res_val, state, in_val, in_dat_err, in_dat, verify_res_ack)
  begin
    nxt_ref_dat        <= ref_dat;
    nxt_prev_ref_dat   <= prev_ref_dat;
    nxt_init_done      <= init_done;
    nxt_verify_res     <= i_verify_res;
    nxt_verify_res_val <= i_verify_res_val;
    nxt_state          <= state;

    if state = s_init then
      if in_val = '1' then
        if in_dat_err = '1' then
          nxt_verify_res <= '1';  -- if the first two data word already contain incorrect pattern symbols
        end if;
        if init_done = '0' then
          nxt_ref_dat   <= in_dat;
          nxt_init_done <= '1';  -- init done because c_nof_init-1 = 1 valid in_dat have been received
        else
          nxt_prev_ref_dat   <= in_dat;  -- prepare prev_ref_dat and ref_dat for next in_dat
          nxt_ref_dat        <= func_tp_seq(ref_dat, in_dat);
          nxt_verify_res_val <= '1';
          nxt_state          <= s_verify;
        end if;
      end if;
    else  -- state = s_verify
      if verify_res_ack = '1' then
        -- prepare for new verification interval
        nxt_init_done      <= '0';
        nxt_verify_res     <= '0';
        nxt_verify_res_val <= '0';
        nxt_state          <= s_init;
      else
        -- update verify_res during this verification interval
        -- . operate independent of in_val='1', because the ADC data is continous
        nxt_prev_ref_dat <= ref_dat;  -- prepare prev_ref_dat and prev_ref_dat for next in_dat
        nxt_ref_dat      <= func_tp_seq(prev_ref_dat, ref_dat);
        if in_dat_err = '1' then
          nxt_verify_res <= '1';  -- capture incorrect pattern symbols
        end if;
        if unsigned(in_dat) /= unsigned(ref_dat) then
          nxt_verify_res <= '1';  -- capture incorrect pattern sequence
        end if;
      end if;
    end if;
  end process;
end rtl;
