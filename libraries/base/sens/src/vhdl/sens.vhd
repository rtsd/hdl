-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use i2c_lib.i2c_pkg.all;

entity sens is
  generic (
    g_temp_high         : natural := 127;
    g_bus_dat_w         : natural := 8;
    g_sens_temp_volt_sz : natural := 9;  -- Should match nof read bytes via I2C in the sens_ctrl SEQUENCE list
    g_sim               : natural := 0
  );
  port (
    clk          : in    std_logic;
    rst          : in    std_logic;
    -- i2c bus
    scl          : inout std_logic;
    sda          : inout std_logic;
    -- read results
    sens_evt     : out   std_logic;
    sens_data    : out   std_logic_vector(g_sens_temp_volt_sz * g_bus_dat_w - 1 downto 0)
  );
end entity;

architecture str of sens is
  constant cs_sim                  : boolean := is_true(g_sim);

  -- SENS read out rate settings
  constant c_sens_update_clk_cnt_w : natural := 27;  -- 2^27 @ 160 MHz = 0.84 sec, 2^27 @ 200 MHz = 0.67 sec
  constant c_update_clk_cnt_w      : natural := sel_a_b(g_sim, 15, c_sens_update_clk_cnt_w);  -- define sens update rate

  -- I2C clock rate settings
  constant c_i2c_sens_clk_cnt      : natural := 399;  -- (200 MHz/ 5 / (399+1)) = 100 kbps
  constant c_i2c_sens_comma_w      : natural := 0;  -- 2**c_i2c_comma_w * system clock period comma time after I2C start and after each octet
                                                      -- 0 = no comma time
  constant c_sens_clk_cnt          : natural := sel_a_b(g_sim, 1, c_i2c_sens_clk_cnt);  -- define I2C clock rate
  constant c_sens_comma_w          : natural := sel_a_b(g_sim, 0, c_i2c_sens_comma_w);  -- define I2C comma time

  constant c_sens_phy              : t_c_i2c_phy := (c_sens_clk_cnt, c_sens_comma_w);

  signal smbus_in_dat  : std_logic_vector(g_bus_dat_w - 1 downto 0);
  signal smbus_in_val  : std_logic;
  signal smbus_out_dat : std_logic_vector(g_bus_dat_w - 1 downto 0);
  signal smbus_out_val : std_logic;
  signal smbus_out_err : std_logic;
  signal smbus_out_ack : std_logic;
begin
  ctrl : entity work.sens_ctrl
  generic map (
    g_clk_cnt_w => c_update_clk_cnt_w,
    g_evt_dat_w => sens_data'LENGTH,
    g_temp_high => g_temp_high
  )
  port map (
    clk         => clk,
    rst         => rst,
    in_dat      => smbus_out_dat,
    in_val      => smbus_out_val,
    in_err      => smbus_out_err,
    in_ack      => smbus_out_ack,
    out_dat     => smbus_in_dat,
    out_val     => smbus_in_val,
    evt_val_reg => sens_evt,
    evt_dat_reg => sens_data
  );

  smbus : entity i2c_lib.i2c_smbus
  generic map (
    g_i2c_phy   => c_sens_phy
  )
  port map (
    gs_sim      => cs_sim,
    clk         => clk,
    rst         => rst,
    in_dat      => smbus_in_dat,
    in_req      => smbus_in_val,
    out_dat     => smbus_out_dat,
    out_val     => smbus_out_val,
    out_err     => smbus_out_err,
    out_ack     => smbus_out_ack,
    scl         => scl,
    sda         => sda
  );
end architecture;
