-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for tech_mac_10g the 10G Ethernet IP technology wrapper.
-- Description:
--   The tb is self checking based on:
--   . proc_tech_mac_10g_rx_packet() for expected header and data type
--   . rx_pkt_cnt = tx_pkt_cnt > 0 must be true at the tb_end.
-- Usage:
--   > as 10
--   > run -all

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use WORK.tech_mac_10g_component_pkg.all;
use WORK.tb_tech_mac_10g_pkg.all;

entity tb_tech_mac_10g is
  -- Test bench control parameters
  generic (
    g_technology : natural := c_tech_select_default;
    g_tb_end     : boolean := true;  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
    g_no_dut     : boolean := false;  -- default FALSE to verify the DUT, else use TRUE to verify the tb itself without the DUT
    --   g_data_type = c_tb_tech_mac_10g_data_type_symbols  = 0
    --   g_data_type = c_tb_tech_mac_10g_data_type_counter  = 1
    g_data_type  : natural := c_tb_tech_mac_10g_data_type_counter  -- c_tb_tech_mac_10g_data_type_symbols
  );
  port (
    tb_end : out std_logic
  );
end tb_tech_mac_10g;

architecture tb of tb_tech_mac_10g is
  constant clk_156_period       : time :=  6.4 ns;  -- 156.25 MHz
  constant phy_delay            : time :=  0 ns;

  constant c_pkt_length_arr     : t_nat_natural_arr := array_init(0, 50, 1) & (1472, 1473) & 9000;  -- frame longer than 1518-46 = 1472 is received with rx_sosi.err = 8
                                                                                                    -- jumbo frame is 9018-46 = 8972
  constant c_nof_pkt            : natural := c_pkt_length_arr'length;

  constant c_dst_mac            : std_logic_vector(c_network_eth_mac_slv'range) := X"10FA01020300";
  constant c_src_mac            : std_logic_vector(c_network_eth_mac_slv'range) := X"123456789ABC";  -- = 12-34-56-78-9A-BC
  constant c_src_mac_tx         : std_logic_vector(c_network_eth_mac_slv'range) := c_src_mac;
  --CONSTANT c_src_mac_tx         : STD_LOGIC_VECTOR(c_network_eth_mac_slv'RANGE) := X"100056789ABC";  -- = 10-00-56-78-9A-BC
  constant c_ethertype          : std_logic_vector(c_network_eth_type_slv'range) := X"10FA";
  constant c_etherlen           : std_logic_vector(c_network_eth_type_slv'range) := "0000000000010000";

  -- Packet headers
  constant c_eth_header_ethertype : t_network_eth_header := (c_dst_mac, c_src_mac_tx, c_ethertype);
  constant c_eth_header_etherlen  : t_network_eth_header := (c_dst_mac, c_src_mac_tx, c_etherlen);

  signal total_header      : t_network_total_header := c_network_total_header_ones;  -- default fill all fields with value 1

  -- Clocks and reset
  signal tx_end            : std_logic := '0';
  signal rx_end            : std_logic := '0';
  signal mm_clk            : std_logic := '0';  -- memory-mapped bus clock
  signal mm_rst            : std_logic;  -- reset synchronous with mm_clk
  signal clk_312           : std_logic := '1';  -- 312.5  MHz
  signal clk_156           : std_logic := '0';  -- 156.25 MHz
  signal tx_ref_clk_312    : std_logic := '1';
  signal tx_ref_clk_156    : std_logic := '0';
  signal tx_rst            : std_logic;  -- reset synchronous with tx_ref_clk_156
  signal rx_phy_clk_312    : std_logic := '1';
  signal rx_phy_clk_156    : std_logic := '0';
  signal rx_rst            : std_logic;  -- reset synchronous with rx_phy_clk_156 = tx_ref_clk_156 in this tb

  -- 10G MAC control interface
  signal mm_init           : std_logic := '1';
  signal mac_mosi          : t_mem_mosi;
  signal mac_mosi_wrdata   : std_logic_vector(c_word_w - 1 downto 0);  -- 32 bit;
  signal mac_miso          : t_mem_miso;
  signal mac_miso_rdval    : std_logic;
  signal mac_miso_rddata   : std_logic_vector(c_word_w - 1 downto 0);  -- 32 bit;

  -- 10G MAC transmit interface
  -- . The tb is the ST source
  signal tx_siso           : t_dp_siso;
  signal tx_sosi           : t_dp_sosi;
  signal tx_sosi_data      : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);  -- 64 bit

  -- 10G MAC receive interface
  -- . The tb is the ST sink
  signal rx_siso           : t_dp_siso;
  signal rx_sosi           : t_dp_sosi;
  signal rx_sosi_data      : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);  -- 64 bit

  -- 10G MAC XGMII interface
  signal xgmii_link_status : std_logic_vector(c_tech_mac_10g_link_status_w - 1 downto 0);  -- 2 bit
  signal xgmii_tx_data     : std_logic_vector(c_xgmii_w - 1 downto 0);  -- 72 bit
  signal xgmii_rx_data     : std_logic_vector(c_xgmii_w - 1 downto 0);  -- 72 bit

  -- Verification
  signal tx_pkt_cnt        : natural := 0;
  signal rx_pkt_cnt        : natural := 0;
  signal rx_toggle         : std_logic;  -- toggle after every received packet
begin
  clk_156 <= not clk_156 after clk_156_period / 2;
  clk_312 <= not clk_312 after clk_156_period / 4;
  tx_ref_clk_156 <= clk_156;  -- mac_10g tx reference clock
  tx_ref_clk_312 <= clk_312;
  rx_phy_clk_156 <= clk_156;  -- use clk_156 to model PHY
  rx_phy_clk_312 <= clk_312;

  tx_rst <= '1', '0' after clk_156_period * 10;
  rx_rst <= '1', '0' after clk_156_period * 10;

  -- debug signals to ease monitoring in wave window
  mac_mosi_wrdata <= mac_mosi.wrdata(c_word_w - 1 downto 0);
  mac_miso_rddata <= mac_miso.rddata(c_word_w - 1 downto 0);
  mac_miso_rdval <= '1' when mac_mosi.rd = '1' and mac_miso.waitrequest = '0' else '0';  -- c_rd_latency = 1

  tx_sosi_data <= tx_sosi.data(c_tech_mac_10g_data_w - 1 downto 0);
  rx_sosi_data <= rx_sosi.data(c_tech_mac_10g_data_w - 1 downto 0);

  -- Use signal to leave unused fields 'X'
  total_header.eth <= c_eth_header_ethertype;

  u_mm_setup : entity work.tb_tech_mac_10g_setup
  generic map (
    g_technology    => g_technology,
    g_src_mac       => c_src_mac
  )
  port map (
    tb_end    => rx_end,
    mm_clk    => mm_clk,
    mm_rst    => mm_rst,
    mm_init   => mm_init,
    mac_mosi  => mac_mosi,
    mac_miso  => mac_miso
  );

  -- Packet transmitter
  u_transmitter : entity work.tb_tech_mac_10g_transmitter
  generic map (
    g_data_type       => g_data_type,
    g_pkt_length_arr1 => c_pkt_length_arr
  )
  port map (
    mm_init        => mm_init,
    total_header   => total_header,
    tx_clk         => tx_ref_clk_156,
    tx_siso        => tx_siso,
    tx_sosi        => tx_sosi,
    link_fault     => OPEN,
    tx_end         => tx_end
  );

  no_dut : if g_no_dut = true generate
    -- ST loopback
    rx_sosi <= tx_sosi;
    tx_siso <= rx_siso;
  end generate;

  gen_dut : if g_no_dut = false generate
    dut : entity work.tech_mac_10g
    generic map (
      g_technology          => g_technology,
      g_pre_header_padding  => true
    )
    port map (
      -- MM
      mm_clk            => mm_clk,
      mm_rst            => mm_rst,
      csr_mosi          => mac_mosi,  -- CSR = control status register
      csr_miso          => mac_miso,

      -- ST
      tx_clk_312        => tx_ref_clk_312,
      tx_clk_156        => tx_ref_clk_156,  -- 156.25 MHz local reference
      tx_rst            => tx_rst,
      tx_snk_in         => tx_sosi,  -- 64 bit data
      tx_snk_out        => tx_siso,

      rx_clk_312        => rx_phy_clk_312,
      rx_clk_156        => rx_phy_clk_156,  -- 156.25 MHz from local reference or from rx phy (dependent on g_technology)
      rx_rst            => rx_rst,
      rx_src_out        => rx_sosi,  -- 64 bit data
      rx_src_in         => rx_siso,

      -- XGMII
      xgmii_link_status => xgmii_link_status,
      xgmii_tx_data     => xgmii_tx_data,  -- 72 bit
      xgmii_rx_data     => xgmii_rx_data  -- 72 bit
    );
  end generate;

  -- Loopback XGMII
  u_link_connect : entity work.tb_tech_mac_10g_link_connect
  generic map (
    g_loopback    => true,
    g_link_delay  => phy_delay
  )
  port map (
    -- XGMII layer connect
    xgmii_tx_data => xgmii_tx_data,
    xgmii_rx_data => xgmii_rx_data
  );

  -- Packet receiver
  u_receiver : entity work.tb_tech_mac_10_receiver
  generic map (
    g_data_type       => g_data_type
  )
  port map (
    mm_init        => mm_init,
    total_header   => total_header,
    rx_clk         => rx_phy_clk_156,
    rx_sosi        => rx_sosi,
    rx_siso        => rx_siso,
    rx_toggle      => rx_toggle
  );

  -- Verification
  u_verify_rx_at_eop : entity work.tb_tech_mac_10_verify_rx_at_eop
  generic map (
    g_no_padding     => g_no_dut,
    g_pkt_length_arr => c_pkt_length_arr
  )
  port map (
    tx_clk         => tx_ref_clk_156,
    tx_sosi        => tx_sosi,
    rx_clk         => rx_phy_clk_156,
    rx_sosi        => rx_sosi
  );

  u_verify_rx_pkt_cnt : entity work.tb_tech_mac_10g_verify_rx_pkt_cnt
  generic map (
    g_nof_pkt     => c_nof_pkt
  )
  port map (
    tx_clk         => tx_ref_clk_156,
    tx_sosi        => tx_sosi,
    rx_clk         => rx_phy_clk_156,
    rx_sosi        => rx_sosi,
    tx_pkt_cnt     => tx_pkt_cnt,
    rx_pkt_cnt     => rx_pkt_cnt,
    rx_end         => rx_end
  );

  -- Stop the simulation
  u_simulation_end : entity work.tb_tech_mac_10g_simulation_end
  generic map (
    g_tb_end            => g_tb_end,
    g_nof_clk_to_rx_end => 1000
  )
  port map (
    clk       => clk_156,
    tx_end    => tx_end,
    rx_end    => rx_end,
    tb_end    => tb_end
  );
end tb;
