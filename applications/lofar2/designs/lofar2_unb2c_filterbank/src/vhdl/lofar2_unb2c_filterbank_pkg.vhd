--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;

package lofar2_unb2c_filterbank_pkg is
 -----------------------------------------------------------------------------
  -- Revision control
  -----------------------------------------------------------------------------

  type t_lofar2_unb2c_filterbank_config is record
    nof_streams_jesd204b           : natural;
    nof_streams_db                 : natural;
    nof_streams_input              : natural;
    dp_clk_freq                    : natural;
  end record;

  --                                                          nofjesd, nofdb, nofinput
  constant c_full             : t_lofar2_unb2c_filterbank_config := (     12,     2,       12, c_unb2c_board_ext_clk_freq_200M );
  constant c_full_256MHz      : t_lofar2_unb2c_filterbank_config := (     12,     2,       12, c_unb2c_board_ext_clk_freq_256M );

  -- Function to select the revision configuration.
  function func_sel_revision_rec(g_design_name : string) return t_lofar2_unb2c_filterbank_config;
end lofar2_unb2c_filterbank_pkg;

package body lofar2_unb2c_filterbank_pkg is
  function func_sel_revision_rec(g_design_name : string) return t_lofar2_unb2c_filterbank_config is
  begin
    if    g_design_name = "lofar2_unb2c_filterbank_full"        then return c_full;
    elsif g_design_name = "lofar2_unb2c_filterbank_full_256MHz" then return c_full_256MHz;
    else  return c_full;
    end if;

  end;
end lofar2_unb2c_filterbank_pkg;
