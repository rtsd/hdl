-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose: Shift register when in_val is active with optional flush at in_eop.
-- Description:
--   The shift register contains g_nof_dat-1 shift registers, so it directly
--   combinatorially includes in_dat. The shift register shifts when in_val
--   is active.
--   If g_flush_en is FALSE then the shift register is always filled when valid
--   data comes out. Hence the out_dat then shifts out with in_val.
--   If g_flush_en is TRUE then the shift register flushes itself when in_eop
--   = '1'. The in_eop marks the last valid in_dat of a block.
--   The out_cnt counts the number of in_val modulo g_nof_dat and restarts at
--   the in_eop. The in_cnt starts at 1, in this way the out_cnt=0 marks the
--   out_data_vec that contain a complete set of g_nof_dat number of new
--   out_dat.
-- Remarks:
-- . Optionally the output can be pipelined via g_pipeline.

entity common_shiftreg is
  generic (
    g_pipeline  : natural := 0;  -- pipeline output
    g_flush_en  : boolean := false;  -- use true to flush shift register when in_eop is active else only shift at active in_val
    g_nof_dat   : positive := 4;  -- nof data in the shift register, including in_dat, so >= 1
    g_dat_w     : natural := 16
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;

    in_dat       : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val       : in  std_logic := '1';
    in_sop       : in  std_logic := '0';
    in_eop       : in  std_logic := '0';

    out_req      : in  std_logic := '0';
    out_data_vec : out std_logic_vector(g_nof_dat * g_dat_w - 1 downto 0);
    out_val_vec  : out std_logic_vector(g_nof_dat - 1 downto 0);
    out_sop_vec  : out std_logic_vector(g_nof_dat - 1 downto 0);
    out_eop_vec  : out std_logic_vector(g_nof_dat - 1 downto 0);
    out_cnt      : out std_logic_vector(sel_a_b(g_nof_dat = 1, 1, ceil_log2(g_nof_dat)) - 1 downto 0);  -- avoid ISE synthesis failure on NULL range for g_nof_dat=1

    out_dat      : out std_logic_vector(g_dat_w - 1 downto 0);  -- = out_data_vec(0)
    out_val      : out std_logic;  -- = out_val_vec(0)
    out_sop      : out std_logic;  -- = out_sop_vec(0)
    out_eop      : out std_logic  -- = out_eop_vec(0)
  );
end common_shiftreg;

architecture str of common_shiftreg is
  type t_data_arr  is array (integer range <>) of std_logic_vector(g_dat_w - 1 downto 0);

  constant c_cnt_w      : natural := out_cnt'length;

  signal shift_en       : std_logic;
  signal flush          : std_logic;

  signal data_arr       : t_data_arr(g_nof_dat - 1 downto 0) := (others => (others => '0'));
  signal nxt_data_arr   : t_data_arr(g_nof_dat - 2 downto 0);
  signal val_arr        : std_logic_vector(g_nof_dat - 1 downto 0);
  signal nxt_val_arr    : std_logic_vector(g_nof_dat - 2 downto 0);
  signal sop_arr        : std_logic_vector(g_nof_dat - 1 downto 0);
  signal nxt_sop_arr    : std_logic_vector(g_nof_dat - 2 downto 0);
  signal eop_arr        : std_logic_vector(g_nof_dat - 1 downto 0);
  signal nxt_eop_arr    : std_logic_vector(g_nof_dat - 2 downto 0);

  signal in_cnt         : std_logic_vector(c_cnt_w - 1 downto 0);
  signal nxt_in_cnt     : std_logic_vector(c_cnt_w - 1 downto 0);

  signal data_vec       : std_logic_vector(g_nof_dat * g_dat_w - 1 downto 0);
  signal val_vec        : std_logic_vector(g_nof_dat - 1 downto 0);
  signal sop_vec        : std_logic_vector(g_nof_dat - 1 downto 0);
  signal eop_vec        : std_logic_vector(g_nof_dat - 1 downto 0);

  signal i_out_data_vec : std_logic_vector(g_nof_dat * g_dat_w - 1 downto 0);
  signal i_out_val_vec  : std_logic_vector(g_nof_dat - 1 downto 0);
  signal i_out_sop_vec  : std_logic_vector(g_nof_dat - 1 downto 0);
  signal i_out_eop_vec  : std_logic_vector(g_nof_dat - 1 downto 0);
begin
  no_sreg : if g_nof_dat = 1 generate
    -- directly assign the inputs to avoid NULL array warning in gen_sreg
    data_vec   <= in_dat;
    val_vec(0) <= in_val;
    sop_vec(0) <= in_sop;
    eop_vec(0) <= in_eop;
  end generate;

  gen_sreg : if g_nof_dat > 1 generate
    -- Combinatorial input via high index
    data_arr(g_nof_dat - 1) <= in_dat;
    val_arr( g_nof_dat - 1) <= in_val;
    sop_arr( g_nof_dat - 1) <= in_sop;
    eop_arr( g_nof_dat - 1) <= in_eop;

    -- Register
    p_clk : process(rst, clk)
    begin
      if rst = '1' then
        val_arr( g_nof_dat - 2 downto 0) <= (others => '0');
        sop_arr( g_nof_dat - 2 downto 0) <= (others => '0');
        eop_arr( g_nof_dat - 2 downto 0) <= (others => '0');
        in_cnt                         <= TO_UVEC(1, c_cnt_w);
      elsif rising_edge(clk) then
        data_arr(g_nof_dat - 2 downto 0) <= nxt_data_arr(g_nof_dat - 2 downto 0);
        val_arr( g_nof_dat - 2 downto 0) <= nxt_val_arr( g_nof_dat - 2 downto 0);
        sop_arr( g_nof_dat - 2 downto 0) <= nxt_sop_arr( g_nof_dat - 2 downto 0);
        eop_arr( g_nof_dat - 2 downto 0) <= nxt_eop_arr( g_nof_dat - 2 downto 0);
        in_cnt                         <= nxt_in_cnt;
      end if;
    end process;

    -- Shift control
    u_flush : entity work.common_switch
    generic map (
      g_rst_level => '0'
    )
    port map (
      clk         => clk,
      rst         => rst,
      switch_high => in_eop,
      switch_low  => eop_arr(0),
      out_level   => flush
    );

    shift_en <= in_val or flush when g_flush_en = true else in_val or out_req;

    nxt_data_arr(g_nof_dat - 2 downto 0) <= data_arr(g_nof_dat - 1 downto 1) when shift_en = '1' else data_arr(g_nof_dat - 2 downto 0);
    nxt_val_arr( g_nof_dat - 2 downto 0) <= val_arr( g_nof_dat - 1 downto 1) when shift_en = '1' else val_arr( g_nof_dat - 2 downto 0);
    nxt_sop_arr( g_nof_dat - 2 downto 0) <= sop_arr( g_nof_dat - 1 downto 1) when shift_en = '1' else sop_arr( g_nof_dat - 2 downto 0);
    nxt_eop_arr( g_nof_dat - 2 downto 0) <= eop_arr( g_nof_dat - 1 downto 1) when shift_en = '1' else eop_arr( g_nof_dat - 2 downto 0);

    p_in_cnt : process(in_cnt, eop_arr, shift_en)
    begin
      nxt_in_cnt <= in_cnt;
      if eop_arr(0) = '1' then
        nxt_in_cnt <= TO_UVEC(1, c_cnt_w);
      elsif shift_en = '1' then
        if unsigned(in_cnt) < g_nof_dat - 1 then
          nxt_in_cnt <= INCR_UVEC(in_cnt, 1);
        else
          nxt_in_cnt <= (others => '0');
        end if;
      end if;
    end process;

    gen_data_vec : for I in g_nof_dat - 1 downto 0 generate
      data_vec((I + 1) * g_dat_w - 1 downto I * g_dat_w) <= data_arr(I);  -- map arr to output vector
      val_vec(I) <= val_arr(I) and shift_en;
      sop_vec(I) <= sop_arr(I) and shift_en;
      eop_vec(I) <= eop_arr(I) and shift_en;
    end generate;
  end generate;

  -- Pipeline output
  out_data_vec <= i_out_data_vec;
  out_val_vec  <= i_out_val_vec;
  out_sop_vec  <= i_out_sop_vec;
  out_eop_vec  <= i_out_eop_vec;

  out_dat      <= i_out_data_vec(g_dat_w - 1 downto 0);
  out_val      <= i_out_val_vec(0);
  out_sop      <= i_out_sop_vec(0);
  out_eop      <= i_out_eop_vec(0);

  u_out_data_vec : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline,
    g_in_dat_w  => g_nof_dat * g_dat_w,
    g_out_dat_w => g_nof_dat * g_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => data_vec,
    out_dat => i_out_data_vec
  );

  u_out_val_vec : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline,
    g_in_dat_w  => g_nof_dat,
    g_out_dat_w => g_nof_dat
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => val_vec,
    out_dat => i_out_val_vec
  );

  u_out_sop_vec : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline,
    g_in_dat_w  => g_nof_dat,
    g_out_dat_w => g_nof_dat
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => sop_vec,
    out_dat => i_out_sop_vec
  );

  u_out_eop_vec : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline,
    g_in_dat_w  => g_nof_dat,
    g_out_dat_w => g_nof_dat
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => eop_vec,
    out_dat => i_out_eop_vec
  );

  u_out_cnt : entity work.common_pipeline
  generic map (
    g_pipeline  => g_pipeline,
    g_in_dat_w  => c_cnt_w,
    g_out_dat_w => c_cnt_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_cnt,
    out_dat => out_cnt
  );
end str;
