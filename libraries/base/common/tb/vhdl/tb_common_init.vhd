-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_init is
end tb_common_init;

architecture tb of tb_common_init is
  constant c_reset_len  : natural := 3;
  constant c_latency_w  : natural := 4;

  constant clk_period   : time := 10 ns;

  signal rst      : std_logic;
  signal clk      : std_logic := '0';
  signal hold     : std_logic;
  signal init     : std_logic;
begin
  clk <= not clk  after clk_period / 2;

  u_reset : entity work.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => '0',  -- release reset after some clock cycles
    clk       => clk,
    out_rst   => rst
  );

  u_init: entity work.common_init
  generic map (
    g_latency_w => c_latency_w
  )
  port map (
    rst   => rst,
    clk   => clk,
    hold  => hold,
    init  => init
  );
end tb;
