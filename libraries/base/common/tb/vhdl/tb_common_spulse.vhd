-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for common_spulse.
-- Description:
--   The tb is not self checking, so manually observe working in Wave window.
-- Usage:
-- > as 10
-- > run 1 us

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_spulse is
end tb_common_spulse;

architecture tb of tb_common_spulse is
  constant c_meta_delay    : natural := 2;

  --CONSTANT in_clk_period   : TIME := 10 ns;
  constant in_clk_period   : time := 27 ns;
  constant out_clk_period  : time := 17 ns;

  signal in_rst    : std_logic;
  signal out_rst   : std_logic;
  signal in_clk    : std_logic := '0';
  signal out_clk   : std_logic := '0';
  signal in_pulse  : std_logic;
  signal out_pulse : std_logic;
begin
  in_clk  <= not in_clk  after in_clk_period / 2;
  out_clk <= not out_clk after out_clk_period / 2;

  p_in_stimuli : process
  begin
    in_rst <= '1';
    in_pulse <= '0';
    wait until rising_edge(in_clk);
    in_rst <= '0';
    for I in 0 to 9 loop
      wait until rising_edge(in_clk);
    end loop;
    in_pulse <= '1';
    wait until rising_edge(in_clk);
    in_pulse <= '0';
    wait;
  end process;

  u_out_rst : entity work.common_areset
  port map (
    in_rst   => in_rst,
    clk      => out_clk,
    out_rst  => out_rst
  );

  u_spulse : entity work.common_spulse
  generic map (
    g_delay_len => c_meta_delay
  )
  port map (
    in_clk     => in_clk,
    in_rst     => in_rst,
    in_pulse   => in_pulse,
    out_clk    => out_clk,
    out_rst    => out_rst,
    out_pulse  => out_pulse
  );
end tb;
