-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_sync_checker
-- Description:
-- Usage:
-- > as 3
-- > run -all

entity tb_tb_dp_sync_checker is
end tb_tb_dp_sync_checker;

architecture tb of tb_tb_dp_sync_checker is
  constant c_nof_repeat            : natural := 100;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--    -- general
--    g_flow_control_stimuli : t_dp_flow_control_enum := e_active;   -- always active, random or pulse flow control
--    g_flow_control_verify  : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
--    -- specific
--    g_in_dat_w             : NATURAL  := 32;
--    g_in_nof_words         : NATURAL  := 1;
--    g_nof_repeat           : NATURAL  := 100;
--    g_pkt_len              : NATURAL  := 16;  -- must be a multiple of g_in_nof_words
--    g_pkt_gap              : NATURAL  := 0;
--    g_sync_period          : NATURAL  := 16;  -- The sync period generated in the stimuli.
--    -- DUT
--    g_nof_blk_per_sync     : POSITIVE := 16  -- The sync period as expected by the sync_checker

  u_sync_ok    : entity work.tb_dp_sync_checker generic map(e_active, e_active, 16, 1, c_nof_repeat, 16, 0, 16, 16);
  u_sync_early : entity work.tb_dp_sync_checker generic map(e_active, e_active, 16, 1, c_nof_repeat, 16, 0, 14, 16);
  u_sync_late  : entity work.tb_dp_sync_checker generic map(e_active, e_active, 16, 1, c_nof_repeat, 16, 0, 18, 16);

  u_rnd_sync_ok    : entity work.tb_dp_sync_checker generic map(e_random, e_active, 16, 1, c_nof_repeat, 16, 0, 16, 16);
  u_rnd_sync_early : entity work.tb_dp_sync_checker generic map(e_random, e_active, 16, 1, c_nof_repeat, 16, 0, 14, 16);
  u_rnd_sync_late  : entity work.tb_dp_sync_checker generic map(e_random, e_active, 16, 1, c_nof_repeat, 16, 0, 18, 16);
end tb;
