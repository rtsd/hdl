--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Cross clock domain between dvr_clk and ctlr_clk
-- Description:
--
-- Remarks:
-- . If the dvr_clk=ctlr_clk then the clock domain crossing logic defaults
--   to wires. However dvr_clk could also be the dp_clk or the mm_clk and then
--   the clock domain crossing logic is needed.
--   No need to cross dvr_start_address and dvr_nof_data, because these
--   are stable when the dvr_en is stable.

library IEEE, technology_lib, tech_ddr_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_select_pkg.all;
use technology_lib.technology_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity io_ddr_cross_domain is
  generic (
    g_cross_domain : boolean := true;
    g_delay_len    : natural := c_meta_delay_len
  );
  port (
    -- Driver clock domain
    dvr_clk                : in  std_logic;
    dvr_rst                : in  std_logic;

    dvr_done               : out std_logic;
    dvr_en                 : in  std_logic;
    dvr_wr_not_rd          : in  std_logic;
    dvr_start_address      : in  std_logic_vector;
    dvr_nof_data           : in  std_logic_vector;
    dvr_wr_flush_en        : in  std_logic := '0';

    -- DDR controller clock domain
    ctlr_clk               : in  std_logic;
    ctlr_rst               : in  std_logic;

    ctlr_dvr_done          : in  std_logic;
    ctlr_dvr_en            : out std_logic;
    ctlr_dvr_wr_not_rd     : out std_logic;
    ctlr_dvr_start_address : out std_logic_vector;
    ctlr_dvr_nof_data      : out std_logic_vector;
    ctlr_dvr_wr_flush_en   : out std_logic := '0'
  );
end io_ddr_cross_domain;

architecture str of io_ddr_cross_domain is
  signal dvr_en_busy  : std_logic;
  signal new_dvr_done : std_logic;
begin
  no_cross : if g_cross_domain = false generate
    -- dvr_clk --> ctlr_clk
    ctlr_dvr_en            <= dvr_en;
    ctlr_dvr_wr_not_rd     <= dvr_wr_not_rd;
    ctlr_dvr_start_address <= dvr_start_address;
    ctlr_dvr_nof_data      <= dvr_nof_data;
    ctlr_dvr_wr_flush_en   <= dvr_wr_flush_en;

    -- ctlr_clk --> dvr_clk
    dvr_done               <= ctlr_dvr_done;
  end generate;

  gen_cross : if g_cross_domain = true generate
    -- dvr_clk --> ctlr_clk
    u_common_spulse_ctlr_dvr_en : entity common_lib.common_spulse
    generic map (
      g_delay_len => g_delay_len
    )
    port map (
      in_rst    => dvr_rst,
      in_clk    => dvr_clk,
      in_pulse  => dvr_en,
      in_busy   => dvr_en_busy,
      out_rst   => ctlr_rst,
      out_clk   => ctlr_clk,
      out_pulse => ctlr_dvr_en
    );

    -- Only register into the other clock domain
    ctlr_dvr_wr_not_rd     <= dvr_wr_not_rd     when rising_edge(ctlr_clk);
    ctlr_dvr_start_address <= dvr_start_address when rising_edge(ctlr_clk);
    ctlr_dvr_nof_data      <= dvr_nof_data      when rising_edge(ctlr_clk);

    u_common_spulse_ctlr_dvr_wr_flush_en : entity common_lib.common_spulse
    generic map (
      g_delay_len => g_delay_len
    )
    port map (
      in_rst    => dvr_rst,
      in_clk    => dvr_clk,
      in_pulse  => dvr_wr_flush_en,
      out_rst   => ctlr_rst,
      out_clk   => ctlr_clk,
      out_pulse => ctlr_dvr_wr_flush_en
    );

    -- ctlr_clk --> dvr_clk
    u_common_async_dvr_done : entity common_lib.common_async
    generic map (
      g_rst_level => '0',
      g_delay_len => g_delay_len
    )
    port map (
      rst  => dvr_rst,
      clk  => dvr_clk,
      din  => ctlr_dvr_done,
      dout => new_dvr_done
    );

    -- Ensure previous dvr_done goes low after new dvr_en
    dvr_done <= new_dvr_done and not dvr_en_busy;
  end generate;
end str;
