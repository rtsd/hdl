-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_mem_pkg.all;

entity mms_diagnostics is
  generic (
    g_sim           : boolean := false;
    g_data_w        : natural;
    g_block_len     : natural := 0;  -- 0: data+valid only; >0 add sop, eop
    g_nof_streams   : natural;
    g_src_latency   : natural := 1;
    g_snk_latency   : natural := 1;
    g_separate_clk  : boolean := false
  );
  port (
    mm_rst           : in  std_logic;
    mm_clk           : in  std_logic;

    st_rst           : in  std_logic := '0';
    st_clk           : in  std_logic := '0';

    src_rst          : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
    src_clk          : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');

    snk_rst          : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
    snk_clk          : in  std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');

    snk_out_arr      : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr       : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    src_in_arr       : in  t_dp_siso_arr(g_nof_streams - 1 downto 0);
    src_out_arr      : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);

    mm_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    mm_miso          : out t_mem_miso := c_mem_miso_rst;

    -- Enable status outputs can be used to trigger other hardware
    src_en_out       : out std_logic_vector(g_nof_streams - 1 downto 0);
    snk_en_out       : out std_logic_vector(g_nof_streams - 1 downto 0)
  );
end mms_diagnostics;

architecture str of mms_diagnostics is
  signal src_diag_en      : std_logic_vector(g_nof_streams - 1 downto 0);
  signal src_diag_md      : std_logic_vector(g_nof_streams - 1 downto 0);

  signal src_val_cnt      : t_slv_32_arr(g_nof_streams - 1 downto 0);
  signal src_val_cnt_clr  : std_logic_vector(g_nof_streams - 1 downto 0);

  signal snk_diag_en      : std_logic_vector(g_nof_streams - 1 downto 0);
  signal snk_diag_md      : std_logic_vector(g_nof_streams - 1 downto 0);

  signal snk_diag_res     : std_logic_vector(g_nof_streams - 1 downto 0);
  signal snk_diag_res_val : std_logic_vector(g_nof_streams - 1 downto 0);

  signal snk_val_cnt      : t_slv_32_arr(g_nof_streams - 1 downto 0);
  signal snk_val_cnt_clr  : std_logic_vector(g_nof_streams - 1 downto 0);
begin
  src_en_out <= src_diag_en;
  snk_en_out <= snk_diag_en;

  u_diagnostics: entity work.diagnostics
  generic map (
    g_dat_w          => g_data_w,
    g_block_len      => g_block_len,
    g_nof_streams    => g_nof_streams,
    g_src_latency    => g_src_latency,
    g_snk_latency    => g_snk_latency,
    g_separate_clk   => g_separate_clk
     )
  port map (
    rst              => st_rst,
    clk              => st_clk,

    src_rst          => src_rst,
    src_clk          => src_clk,

    snk_rst          => snk_rst,
    snk_clk          => snk_clk,

    snk_out_arr      => snk_out_arr,
    snk_in_arr       => snk_in_arr,
    snk_diag_en      => snk_diag_en,
    snk_diag_md      => snk_diag_md,
    snk_diag_res     => snk_diag_res,
    snk_diag_res_val => snk_diag_res_val,
    snk_val_cnt      => snk_val_cnt,
    snk_val_cnt_clr  => snk_val_cnt_clr,

    src_out_arr      => src_out_arr,
    src_in_arr       => src_in_arr,
    src_diag_en      => src_diag_en,
    src_diag_md      => src_diag_md,
    src_val_cnt      => src_val_cnt,
    src_val_cnt_clr  => src_val_cnt_clr
  );

  u_diagnostics_reg: entity work.diagnostics_reg
  generic map(
    g_nof_streams  => g_nof_streams,
    g_separate_clk => g_separate_clk
  )
  port map (
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,

    st_rst              => st_rst,
    st_clk              => st_clk,

    src_rst             => src_rst,
    src_clk             => src_clk,

    snk_rst             => snk_rst,
    snk_clk             => snk_clk,

    sla_in              => mm_mosi,
    sla_out             => mm_miso,

    st_src_en           => src_diag_en,
    st_src_md           => src_diag_md,
    st_src_cnt          => src_val_cnt,
    st_src_cnt_clr_evt  => src_val_cnt_clr,

    st_snk_en           => snk_diag_en,
    st_snk_md           => snk_diag_md,
    st_snk_cnt          => snk_val_cnt,
    st_snk_cnt_clr_evt  => snk_val_cnt_clr,
    st_snk_diag_val     => snk_diag_res_val,
    st_snk_diag_res     => snk_diag_res
  );
end str;
