-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;

entity mmm_unb1_correlator is
  generic (
    g_sim         : boolean := false;  -- FALSE: use SOPC; TRUE: use mm_file I/O
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0
  );
  port (
    xo_clk                   : in  std_logic;
    xo_rst_n                 : in  std_logic;
    xo_rst                   : in  std_logic;

    mm_rst                   : in  std_logic;
    mm_clk                   : out std_logic;
    mm_locked                : out std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi             : out t_mem_mosi;
    reg_wdi_miso             : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi : out t_mem_mosi;
    reg_unb_system_info_miso : in  t_mem_miso;
    rom_unb_system_info_mosi : out t_mem_mosi;
    rom_unb_system_info_miso : in  t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        : out t_mem_mosi;
    reg_unb_sens_miso        : in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi            : out t_mem_mosi;
    reg_ppsh_miso            : in  t_mem_miso;

    -- Data buffers
    reg_diag_data_buf_mosi   : out t_mem_mosi;
    reg_diag_data_buf_miso   : in  t_mem_miso;
    ram_diag_data_buf_mosi   : out t_mem_mosi;
    ram_diag_data_buf_miso   : in  t_mem_miso;

    -- eth1g
    eth1g_tse_clk            : out std_logic;
    eth1g_mm_rst             : out std_logic;
    eth1g_tse_mosi           : out t_mem_mosi;
    eth1g_tse_miso           : in  t_mem_miso;
    eth1g_reg_mosi           : out t_mem_mosi;
    eth1g_reg_miso           : in  t_mem_miso;
    eth1g_reg_interrupt      : in  std_logic;
    eth1g_ram_mosi           : out t_mem_mosi;
    eth1g_ram_miso           : in  t_mem_miso;

    -- Speed test using internal PLL:
    clk_clk                  : in std_logic;
    clk_clk_in_reset_reset_n : in std_logic;
    altpll_1_c0_clk          : out std_logic

  );
end mmm_unb1_correlator;

architecture str of mmm_unb1_correlator is
  constant c_mm_clk_period   : time := 8 ns;  -- 125 MHz

  constant c_sim_node_type : string(1 to 2) := sel_a_b(g_sim_node_nr < 4, "FN", "BN");
  constant c_sim_node_nr   : natural := sel_a_b(c_sim_node_type = "BN", g_sim_node_nr - 4, g_sim_node_nr);

  signal i_mm_clk   : std_logic := '1';

  -----------------------------------------------------------------------------
  -- this component declaration is copy-pasted from Quartus v11.1 QSYS builder
  -- . Note the SLV->SL edits, e.g. coe_address_export_from_the_reg_wdi.
  -----------------------------------------------------------------------------
  component qsys_unb1_correlator is
      port (
          coe_ram_write_export_from_the_avs_eth_0       : out std_logic;  -- export
          coe_reg_read_export_from_the_avs_eth_0        : out std_logic;  -- export
          mm_clk                                        : out std_logic;  -- clk
          coe_address_export_from_the_pio_system_info   : out std_logic_vector(4 downto 0);  -- export
          coe_address_export_from_the_pio_pps           : out std_logic;  -- _vector(0 downto 0);                     -- export
          coe_reset_export_from_the_pio_pps             : out std_logic;  -- export
          coe_readdata_export_to_the_pio_pps            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          coe_writedata_export_from_the_pio_system_info : out std_logic_vector(31 downto 0);  -- export
          coe_reset_export_from_the_reg_unb_sens        : out std_logic;  -- export
          coe_tse_write_export_from_the_avs_eth_0       : out std_logic;  -- export
          coe_reset_export_from_the_reg_wdi             : out std_logic;  -- export
          coe_clk_export_from_the_rom_system_info       : out std_logic;  -- export
          coe_read_export_from_the_reg_unb_sens         : out std_logic;  -- export
          coe_write_export_from_the_reg_unb_sens        : out std_logic;  -- export
          coe_clk_export_from_the_reg_unb_sens          : out std_logic;  -- export
          coe_reg_writedata_export_from_the_avs_eth_0   : out std_logic_vector(31 downto 0);  -- export
          coe_read_export_from_the_reg_wdi              : out std_logic;  -- export
          coe_reg_write_export_from_the_avs_eth_0       : out std_logic;  -- export
          coe_readdata_export_to_the_reg_unb_sens       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          coe_ram_address_export_from_the_avs_eth_0     : out std_logic_vector(9 downto 0);  -- export
          coe_clk_export_from_the_pio_pps               : out std_logic;  -- export
          coe_readdata_export_to_the_pio_system_info    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          coe_writedata_export_from_the_rom_system_info : out std_logic_vector(31 downto 0);  -- export
          coe_address_export_from_the_reg_wdi           : out std_logic;  -- _vector(0 downto 0);                     -- export
          coe_reset_export_from_the_avs_eth_0           : out std_logic;  -- export
          coe_write_export_from_the_pio_system_info     : out std_logic;  -- export
          coe_tse_address_export_from_the_avs_eth_0     : out std_logic_vector(9 downto 0);  -- export
          coe_write_export_from_the_pio_pps             : out std_logic;  -- export
          coe_write_export_from_the_rom_system_info     : out std_logic;  -- export
          coe_irq_export_to_the_avs_eth_0               : in  std_logic                     := 'X';  -- export
          coe_read_export_from_the_rom_system_info      : out std_logic;  -- export
          phasedone_from_the_altpll_0                   : out std_logic;  -- export
          reset_n                                       : in  std_logic                     := 'X';  -- reset_n
          coe_tse_writedata_export_from_the_avs_eth_0   : out std_logic_vector(31 downto 0);  -- export
          coe_tse_readdata_export_to_the_avs_eth_0      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          coe_ram_read_export_from_the_avs_eth_0        : out std_logic;  -- export
          clk_0                                         : in  std_logic                     := 'X';  -- clk
          coe_writedata_export_from_the_reg_unb_sens    : out std_logic_vector(31 downto 0);  -- export
          tse_clk                                       : out std_logic;  -- clk
          epcs_clk                                      : out std_logic;  -- clk
          coe_reg_readdata_export_to_the_avs_eth_0      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          out_port_from_the_pio_debug_wave              : out std_logic_vector(31 downto 0);  -- export
          coe_tse_read_export_from_the_avs_eth_0        : out std_logic;  -- export
          coe_writedata_export_from_the_reg_wdi         : out std_logic_vector(31 downto 0);  -- export
          coe_reset_export_from_the_pio_system_info     : out std_logic;  -- export
          coe_read_export_from_the_pio_system_info      : out std_logic;  -- export
          coe_clk_export_from_the_reg_wdi               : out std_logic;  -- export
          coe_ram_readdata_export_to_the_avs_eth_0      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          out_port_from_the_pio_wdi                     : out std_logic;  -- export
          coe_clk_export_from_the_avs_eth_0             : out std_logic;  -- export
          coe_readdata_export_to_the_rom_system_info    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          coe_write_export_from_the_reg_wdi             : out std_logic;  -- export
          coe_readdata_export_to_the_reg_wdi            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          coe_read_export_from_the_pio_pps              : out std_logic;  -- export
          coe_clk_export_from_the_pio_system_info       : out std_logic;  -- export
          coe_writedata_export_from_the_pio_pps         : out std_logic_vector(31 downto 0);  -- export
          coe_reset_export_from_the_rom_system_info     : out std_logic;  -- export
          coe_tse_waitrequest_export_to_the_avs_eth_0   : in  std_logic                     := 'X';  -- export
          coe_address_export_from_the_reg_unb_sens      : out std_logic_vector(2 downto 0);  -- export
          coe_reg_address_export_from_the_avs_eth_0     : out std_logic_vector(3 downto 0);  -- export
          coe_address_export_from_the_rom_system_info   : out std_logic_vector(9 downto 0);  -- export
          areset_to_the_altpll_0                        : in  std_logic                     := 'X';  -- export
          locked_from_the_altpll_0                      : out std_logic;  -- export
          coe_ram_writedata_export_from_the_avs_eth_0   : out std_logic_vector(31 downto 0);  -- export
          c3_from_the_altpll_0                          : out std_logic;  -- export
          ram_diag_data_buf_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          ram_diag_data_buf_read_export                 : out std_logic;  -- export
          ram_diag_data_buf_writedata_export            : out std_logic_vector(31 downto 0);  -- export
          ram_diag_data_buf_write_export                : out std_logic;  -- export
          ram_diag_data_buf_address_export              : out std_logic_vector(14 downto 0);  -- export
          ram_diag_data_buf_clk_export                  : out std_logic;  -- export
          ram_diag_data_buf_reset_export                : out std_logic;  -- export
          reg_diag_data_buf_readdata_export             : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
          reg_diag_data_buf_read_export                 : out std_logic;  -- export
          reg_diag_data_buf_writedata_export            : out std_logic_vector(31 downto 0);  -- export
          reg_diag_data_buf_write_export                : out std_logic;  -- export
          reg_diag_data_buf_address_export              : out std_logic_vector(9 downto 0);  -- export
          reg_diag_data_buf_clk_export                  : out std_logic;  -- export
          reg_diag_data_buf_reset_export                : out std_logic;  -- export
          clk_clk                  : in std_logic;
          reset_reset_n : in std_logic;
          altpll_1_c0_clk          : out std_logic;
          altpll_1_areset_conduit_export     : in  std_logic;
          altpll_1_phasedone_conduit_export  : out std_logic;
          altpll_1_locked_conduit_export     : out std_logic
      );
  end component qsys_unb1_correlator;
begin
  mm_clk   <= i_mm_clk;

  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $HDL_IOFILE_SIM_DIR.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    i_mm_clk   <= not i_mm_clk after c_mm_clk_period / 2;
    mm_locked  <= '0', '1' after c_mm_clk_period * 5;

    u_mm_file_reg_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                               port map(mm_rst, i_mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                               port map(mm_rst, i_mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                               port map(mm_rst, i_mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_unb_sens        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
                                               port map(mm_rst, i_mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso );

    u_mm_file_reg_ppsh            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                               port map(mm_rst, i_mm_clk, reg_ppsh_mosi, reg_ppsh_miso );

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    u_mm_file_reg_eth             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                               port map(mm_rst, i_mm_clk, eth1g_reg_mosi, eth1g_reg_miso );

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  ----------------------------------------------------------------------------
  -- SOPC or QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys_unb1_correlator : if g_sim = false generate
    u_qsys_unb1_correlator : qsys_unb1_correlator
    port map (
      clk_0                                         => xo_clk,
      reset_n                                       => xo_rst_n,
      mm_clk                                        => i_mm_clk,
      tse_clk                                       => eth1g_tse_clk,

       -- the_altpll_0
      locked_from_the_altpll_0                      => mm_locked,
      phasedone_from_the_altpll_0                   => OPEN,
      areset_to_the_altpll_0                        => xo_rst,

      -- the_avs_eth_0
      coe_clk_export_from_the_avs_eth_0             => OPEN,
      coe_reset_export_from_the_avs_eth_0           => eth1g_mm_rst,
      coe_tse_address_export_from_the_avs_eth_0     => eth1g_tse_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      coe_tse_write_export_from_the_avs_eth_0       => eth1g_tse_mosi.wr,
      coe_tse_writedata_export_from_the_avs_eth_0   => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      coe_tse_read_export_from_the_avs_eth_0        => eth1g_tse_mosi.rd,
      coe_tse_readdata_export_to_the_avs_eth_0      => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      coe_tse_waitrequest_export_to_the_avs_eth_0   => eth1g_tse_miso.waitrequest,
      coe_reg_address_export_from_the_avs_eth_0     => eth1g_reg_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      coe_reg_write_export_from_the_avs_eth_0       => eth1g_reg_mosi.wr,
      coe_reg_writedata_export_from_the_avs_eth_0   => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      coe_reg_read_export_from_the_avs_eth_0        => eth1g_reg_mosi.rd,
      coe_reg_readdata_export_to_the_avs_eth_0      => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      coe_irq_export_to_the_avs_eth_0               => eth1g_reg_interrupt,
      coe_ram_address_export_from_the_avs_eth_0     => eth1g_ram_mosi.address(c_unb1_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      coe_ram_write_export_from_the_avs_eth_0       => eth1g_ram_mosi.wr,
      coe_ram_writedata_export_from_the_avs_eth_0   => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      coe_ram_read_export_from_the_avs_eth_0        => eth1g_ram_mosi.rd,
      coe_ram_readdata_export_to_the_avs_eth_0      => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),

      -- the_reg_unb_sens
      coe_clk_export_from_the_reg_unb_sens          => OPEN,
      coe_reset_export_from_the_reg_unb_sens        => OPEN,
      coe_address_export_from_the_reg_unb_sens      => reg_unb_sens_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      coe_read_export_from_the_reg_unb_sens         => reg_unb_sens_mosi.rd,
      coe_readdata_export_to_the_reg_unb_sens       => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_unb_sens        => reg_unb_sens_mosi.wr,
      coe_writedata_export_from_the_reg_unb_sens    => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_debug_wave
      out_port_from_the_pio_debug_wave              => OPEN,

      -- the_pio_pps
      coe_clk_export_from_the_pio_pps               => OPEN,
      coe_reset_export_from_the_pio_pps             => OPEN,
      coe_address_export_from_the_pio_pps           => reg_ppsh_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1),  -- 1 bit address width so must use (0) instead of (0 DOWNTO 0)
      coe_read_export_from_the_pio_pps              => reg_ppsh_mosi.rd,
      coe_readdata_export_to_the_pio_pps            => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_pps             => reg_ppsh_mosi.wr,
      coe_writedata_export_from_the_pio_pps         => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_system_info: actually a avs_common_mm instance
      coe_clk_export_from_the_pio_system_info       => OPEN,
      coe_reset_export_from_the_pio_system_info     => OPEN,
      coe_address_export_from_the_pio_system_info   => reg_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_pio_system_info      => reg_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_pio_system_info    => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_pio_system_info     => reg_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_pio_system_info => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_rom_system_info
      coe_clk_export_from_the_rom_system_info       => OPEN,
      coe_reset_export_from_the_rom_system_info     => OPEN,
      coe_address_export_from_the_rom_system_info   => rom_unb_system_info_mosi.address(c_unb1_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      coe_read_export_from_the_rom_system_info      => rom_unb_system_info_mosi.rd,
      coe_readdata_export_to_the_rom_system_info    => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_rom_system_info     => rom_unb_system_info_mosi.wr,
      coe_writedata_export_from_the_rom_system_info => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb1_board.
      out_port_from_the_pio_wdi                     => pout_wdi,

      -- the_reg_wdi: Manual WDI override; causes FPGA reconfiguration if WDI is enabled (g_use_phy).
      coe_clk_export_from_the_reg_wdi               => OPEN,
      coe_reset_export_from_the_reg_wdi             => OPEN,
      coe_address_export_from_the_reg_wdi           => reg_wdi_mosi.address(0),
      coe_read_export_from_the_reg_wdi              => reg_wdi_mosi.rd,
      coe_readdata_export_to_the_reg_wdi            => reg_wdi_miso.rddata(c_word_w - 1 downto 0),
      coe_write_export_from_the_reg_wdi             => reg_wdi_mosi.wr,
      coe_writedata_export_from_the_reg_wdi         => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),

      ram_diag_data_buf_readdata_export             => ram_diag_data_buf_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_data_buf_read_export                 => ram_diag_data_buf_mosi.rd,
      ram_diag_data_buf_writedata_export            => ram_diag_data_buf_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buf_write_export                => ram_diag_data_buf_mosi.wr,
      ram_diag_data_buf_address_export              => ram_diag_data_buf_mosi.address(14 downto 0),
      ram_diag_data_buf_clk_export                  => OPEN,
      ram_diag_data_buf_reset_export                => OPEN,

      reg_diag_data_buf_readdata_export             => reg_diag_data_buf_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_data_buf_read_export                 => reg_diag_data_buf_mosi.rd,
      reg_diag_data_buf_writedata_export            => reg_diag_data_buf_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buf_write_export                => reg_diag_data_buf_mosi.wr,
      reg_diag_data_buf_address_export              => reg_diag_data_buf_mosi.address(9 downto 0),
      reg_diag_data_buf_clk_export                  => OPEN,
      reg_diag_data_buf_reset_export                => OPEN,

      clk_clk                    => clk_clk,
      reset_reset_n   => clk_clk_in_reset_reset_n,
      altpll_1_c0_clk            => altpll_1_c0_clk,

      altpll_1_areset_conduit_export       => xo_rst,
      altpll_1_phasedone_conduit_export    => OPEN,
      altpll_1_locked_conduit_export       => open
      );
  end generate;
end str;
