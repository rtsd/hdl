#------------------------------------------------------------------------------
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# DS: This file is based on (read: severely stripped down) Megawizard-generated
# file msim_setup.tcl.
# tr_xaui is the first module I did this for.

set IP_DIR "$env(HDL_BUILD_DIR)/unb1/qmegawiz/ip_stratixiv_mac_10g_sim"

#vlib ./work/         ;# Assume library work already exists
#vmap work ./work/

vcom     "$IP_DIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_handshake_clock_crosser_0001.vho" -work work 
vcom     "$IP_DIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0003.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0006.vho"                           -work work 
vcom     "$IP_DIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0005.vho"                           -work work 
vcom     "$IP_DIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0002.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0004.vho"                           -work work 
vcom     "$IP_DIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0003.vho"                           -work work 
vcom     "$IP_DIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0001.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0002.vho"                           -work work 
vcom     "$IP_DIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0001.vho"                           -work work 
vcom     "$IP_DIR/altera_reset_controller/altera_reset_controller_0001.vho"                                   -work work 
vcom     "$IP_DIR/altera_merlin_traffic_limiter/altera_merlin_traffic_limiter_0003.vho"                       -work work 
vcom     "$IP_DIR/altera_merlin_traffic_limiter/altera_merlin_traffic_limiter_0002.vho"                       -work work 
vcom     "$IP_DIR/altera_merlin_traffic_limiter/altera_merlin_traffic_limiter_0001.vho"                       -work work 
vcom     "$IP_DIR/altera_merlin_router/altera_merlin_router_0006.vho"                                         -work work 
vcom     "$IP_DIR/altera_merlin_router/altera_merlin_router_0005.vho"                                         -work work 
vcom     "$IP_DIR/altera_merlin_router/altera_merlin_router_0004.vho"                                         -work work 
vcom     "$IP_DIR/altera_merlin_router/altera_merlin_router_0003.vho"                                         -work work 
vcom     "$IP_DIR/altera_merlin_router/altera_merlin_router_0002.vho"                                         -work work 
vcom     "$IP_DIR/altera_merlin_router/altera_merlin_router_0001.vho"                                         -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0017.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_master_agent/altera_merlin_master_agent_0003.vho"                             -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0016.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0015.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0014.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0013.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0012.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0011.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0010.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0009.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0008.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0007.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_master_agent/altera_merlin_master_agent_0002.vho"                             -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0006.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0005.vho"                               -work work 
vcom     "$IP_DIR/altera_avalon_sc_fifo/altera_avalon_sc_fifo_0004.vho"                                       -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0004.vho"                               -work work 
vcom     "$IP_DIR/altera_avalon_sc_fifo/altera_avalon_sc_fifo_0003.vho"                                       -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0003.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0002.vho"                               -work work 
vcom     "$IP_DIR/altera_avalon_sc_fifo/altera_avalon_sc_fifo_0002.vho"                                       -work work 
vcom     "$IP_DIR/altera_avalon_sc_fifo/altera_avalon_sc_fifo_0001.vho"                                       -work work 
vcom     "$IP_DIR/altera_merlin_slave_agent/altera_merlin_slave_agent_0001.vho"                               -work work 
vcom     "$IP_DIR/altera_merlin_master_agent/altera_merlin_master_agent_0001.vho"                             -work work 
vcom     "$IP_DIR/altera_merlin_slave_translator/altera_merlin_slave_translator_0007.vho"                     -work work 
vcom     "$IP_DIR/altera_merlin_slave_translator/altera_merlin_slave_translator_0006.vho"                     -work work 
vcom     "$IP_DIR/altera_merlin_slave_translator/altera_merlin_slave_translator_0005.vho"                     -work work 
vcom     "$IP_DIR/altera_merlin_slave_translator/altera_merlin_slave_translator_0004.vho"                     -work work 
vcom     "$IP_DIR/altera_merlin_slave_translator/altera_merlin_slave_translator_0003.vho"                     -work work 
vcom     "$IP_DIR/altera_merlin_slave_translator/altera_merlin_slave_translator_0002.vho"                     -work work 
vcom     "$IP_DIR/altera_merlin_master_translator/altera_merlin_master_translator_0003.vho"                   -work work 
vcom     "$IP_DIR/altera_merlin_slave_translator/altera_merlin_slave_translator_0001.vho"                     -work work 
vcom     "$IP_DIR/altera_merlin_master_translator/altera_merlin_master_translator_0002.vho"                   -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0011.vho"                                                     -work work 
vcom     "$IP_DIR/altera_avalon_dc_fifo/altera_avalon_dc_fifo_0002.vho"                                       -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0010.vho"                                                     -work work 
vcom     "$IP_DIR/altera_avalon_dc_fifo/altera_avalon_dc_fifo_0001.vho"                                       -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0009.vho"                                                     -work work 
vcom     "$IP_DIR/altera_avalon_st_splitter/altera_avalon_st_splitter_0005.vho"                               -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0008.vho"                                                     -work work 
vcom     "$IP_DIR/altera_avalon_st_delay/altera_avalon_st_delay_0002.vho"                                     -work work 
vcom     "$IP_DIR/error_adapter/error_adapter_0003.vho"                                                       -work work 
vcom     "$IP_DIR/altera_avalon_st_delay/altera_avalon_st_delay_0001.vho"                                     -work work 
vlog     "$IP_DIR/altera_eth_packet_overflow_control/mentor/altera_eth_packet_overflow_control.v"             -work work 
vlog     "$IP_DIR/altera_eth_crc_pad_rem/mentor/altera_eth_crc_pad_rem.v"                                     -work work 
vlog     "$IP_DIR/altera_eth_crc_pad_rem/mentor/altera_eth_crc_rem.v"                                         -work work 
vlog     "$IP_DIR/altera_eth_crc_pad_rem/mentor/altera_packet_stripper.v"                                     -work work 
vlog -sv "$IP_DIR/altera_eth_crc_pad_rem/altera_avalon_st_pipeline_stage.sv"                                  -work work 
vlog     "$IP_DIR/altera_eth_crc_pad_rem/altera_avalon_st_pipeline_base.v"                                    -work work 
vlog     "$IP_DIR/altera_eth_frame_status_merger/mentor/altera_eth_frame_status_merger.v"                     -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0007.vho"                                                     -work work 
vcom     "$IP_DIR/altera_avalon_st_splitter/altera_avalon_st_splitter_0004.vho"                               -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0006.vho"                                                     -work work 
vlog     "$IP_DIR/altera_eth_lane_decoder/mentor/altera_eth_lane_decoder.v"                                   -work work 
vlog     "$IP_DIR/altera_eth_link_fault_detection/mentor/altera_eth_link_fault_detection.v"                   -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0005.vho"                                                     -work work 
vcom     "$IP_DIR/altera_avalon_st_splitter/altera_avalon_st_splitter_0003.vho"                               -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0004.vho"                                                     -work work 
vlog     "$IP_DIR/altera_eth_link_fault_generation/mentor/altera_eth_link_fault_generation.v"                 -work work 
vlog     "$IP_DIR/altera_eth_xgmii_termination/mentor/altera_eth_xgmii_termination.v"                         -work work 
vlog     "$IP_DIR/altera_eth_packet_formatter/mentor/altera_eth_packet_formatter.v"                           -work work 
vlog     "$IP_DIR/altera_eth_10gmem_statistics_collector/mentor/altera_eth_10gmem_statistics_collector.v"     -work work 
vcom     "$IP_DIR/altera_avalon_st_splitter/altera_avalon_st_splitter_0002.vho"                               -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0003.vho"                                                     -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0002.vho"                                                     -work work 
vcom     "$IP_DIR/error_adapter/error_adapter_0002.vho"                                                       -work work 
vlog     "$IP_DIR/altera_eth_frame_decoder/mentor/altera_eth_frame_decoder.v"                                 -work work 
vlog -sv "$IP_DIR/altera_eth_frame_decoder/altera_avalon_st_pipeline_stage.sv"                                -work work 
vlog     "$IP_DIR/altera_eth_frame_decoder/altera_avalon_st_pipeline_base.v"                                  -work work 
vcom     "$IP_DIR/timing_adapter/timing_adapter_0001.vho"                                                     -work work 
vcom     "$IP_DIR/altera_avalon_st_splitter/altera_avalon_st_splitter_0001.vho"                               -work work 
vcom     "$IP_DIR/altera_avalon_st_pipeline_stage/altera_avalon_st_pipeline_stage_0001.vho"                   -work work 
vlog     "$IP_DIR/altera_eth_crc/mentor/altera_eth_crc.v"                                                     -work work 
vlog     "$IP_DIR/altera_eth_crc/mentor/crc32.v"                                                              -work work 
vlog     "$IP_DIR/altera_eth_crc/mentor/gf_mult32_kc.v"                                                       -work work 
vlog     "$IP_DIR/altera_eth_address_inserter/mentor/altera_eth_address_inserter.v"                           -work work 
vcom     "$IP_DIR/multiplexer/multiplexer_0001.vho"                                                           -work work 
vcom     "$IP_DIR/error_adapter/error_adapter_0001.vho"                                                       -work work 
vlog     "$IP_DIR/altera_eth_pause_ctrl_gen/mentor/altera_eth_pause_ctrl_gen.v"                               -work work 
vlog     "$IP_DIR/altera_eth_pause_ctrl_gen/mentor/altera_eth_pause_controller.v"                             -work work 
vlog     "$IP_DIR/altera_eth_pause_ctrl_gen/mentor/altera_eth_pause_gen.v"                                    -work work 
vlog     "$IP_DIR/altera_eth_pause_beat_conversion/mentor/altera_eth_pause_beat_conversion.v"                 -work work 
vlog     "$IP_DIR/altera_eth_pkt_backpressure_control/mentor/altera_eth_pkt_backpressure_control.v"           -work work 
vlog     "$IP_DIR/altera_eth_pad_inserter/mentor/altera_eth_pad_inserter.v"                                   -work work 
vlog     "$IP_DIR/altera_eth_packet_underflow_control/mentor/altera_eth_packet_underflow_control.v"           -work work 
vcom     "$IP_DIR/altera_avalon_mm_bridge/altera_avalon_mm_bridge_0001.vho"                                   -work work 
vcom     "$IP_DIR/altera_merlin_master_translator/altera_merlin_master_translator_0001.vho"                   -work work 
vcom     "$IP_DIR/ip_stratixiv_mac_10g.vhd"                                                                                                                                                                                           
vcom     "$IP_DIR/ip_stratixiv_mac_10g_tx_eth_pkt_backpressure_control.vhd"                                                                                                                                                           
vcom     "$IP_DIR/ip_stratixiv_mac_10g_rx_eth_pkt_backpressure_control.vhd"                                                                                                                                                           
vcom     "$IP_DIR/ip_stratixiv_mac_10g_tx_eth_frame_decoder.vhd"                                                                                                                                                                      
vcom     "$IP_DIR/ip_stratixiv_mac_10g_rx_eth_frame_decoder.vhd"                                                                                                                                                                      
vcom     "$IP_DIR/ip_stratixiv_mac_10g_tx_eth_crc_inserter.vhd"                                                                                                                                                                       
vcom     "$IP_DIR/ip_stratixiv_mac_10g_rx_eth_crc_checker.vhd"                                                                                                                                                                        


