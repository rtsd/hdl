-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . MMP-wrapper that adds MM clock domain RAM readout and and multi-instance
--   support to st_histogram.
-- Description:
-- . Adds logic to move st_histogram RAM contents into the dual clock RAM for
--   readout in MM clock domain.
-- . Per instance there are at least (or more dependent on g_nof_bins) two
--   block RAM:
--   . one dual page block RAM in st_histogram in the dp_clk domain that
--     accumulate or hold the bin values for every sync interval,
--   . one dual clock block RAM here to provide the read access to the
--     page with the hold bin values via the mm_clk domain.

library IEEE, common_lib, mm_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mmp_st_histogram is
  generic (
    g_nof_instances          : natural;
    g_data_w                 : natural;
    g_nof_bins               : natural;
    g_nof_data_per_sync      : natural;
    g_nof_data_per_sync_diff : natural
  );
  port (
    dp_clk     : in  std_logic;
    dp_rst     : in  std_logic;

    snk_in_arr : in  t_dp_sosi_arr(g_nof_instances - 1 downto 0);

    mm_clk     : in  std_logic;
    mm_rst     : in  std_logic;

    ram_copi   : in  t_mem_copi;
    ram_cipo   : out t_mem_cipo
  );
end mmp_st_histogram;

architecture str of mmp_st_histogram is
  -------------------------------------------------------------------------------
  -- st_histogram instances
  -------------------------------------------------------------------------------
  signal st_histogram_ram_copi_arr  : t_mem_copi_arr(g_nof_instances - 1 downto 0);
  signal st_histogram_ram_cipo_arr  : t_mem_cipo_arr(g_nof_instances - 1 downto 0);

  -------------------------------------------------------------------------------
  -- Dual clock RAM
  -------------------------------------------------------------------------------
  constant c_reg_adr_w : natural := 1;
  constant c_ram_adr_w : natural := ceil_log2(g_nof_bins);
  constant c_ram_dat_w : natural := ceil_log2(g_nof_data_per_sync + 1);

  constant c_ram                    : t_c_mem := (latency  => 1,
                                                  adr_w    => c_ram_adr_w,
                                                  dat_w    => c_ram_dat_w,
                                                  nof_dat  => g_nof_bins,
                                                  init_sl  => '0');

  constant c_addr_high : natural := g_nof_bins - 1;

  signal wr_copi_arr : t_mem_copi_arr(g_nof_instances - 1 downto 0);

  -------------------------------------------------------------------------------
  -- Logic to move st_histogram RAM contents into the dual clock RAM
  -------------------------------------------------------------------------------
  signal ram_fill          : std_logic;
  signal ram_filling       : std_logic;
  signal nxt_ram_filling   : std_logic;
  signal ram_address       : std_logic_vector(c_ram_adr_w - 1 downto 0);
  signal nxt_ram_address   : std_logic_vector(c_ram_adr_w - 1 downto 0);
  signal prv_ram_address   : std_logic_vector(c_ram_adr_w - 1 downto 0);

  -------------------------------------------------------------------------------
  -- MM multiplexing
  -------------------------------------------------------------------------------
  signal ram_copi_arr  : t_mem_copi_arr(g_nof_instances - 1 downto 0);
  signal ram_cipo_arr  : t_mem_cipo_arr(g_nof_instances - 1 downto 0);
begin
  -------------------------------------------------------------------------------
  -- st_histogram instances
  -------------------------------------------------------------------------------
  gen_st_histogram : for i in 0 to g_nof_instances - 1 generate
    u_st_histogram : entity work.st_histogram
    generic map(
      g_data_w            => g_data_w,
      g_nof_bins          => g_nof_bins,
      g_nof_data_per_sync => g_nof_data_per_sync,
      g_nof_data_per_sync_diff => g_nof_data_per_sync_diff
    )
    port map (
      dp_clk       => dp_clk,
      dp_rst       => dp_rst,

      snk_in       => snk_in_arr(i),

      ram_mosi     => st_histogram_ram_copi_arr(i),
      ram_miso     => st_histogram_ram_cipo_arr(i)
    );
  end generate;

  -------------------------------------------------------------------------------
  -- Dual clock RAM: DP write side, MM read side
  -- . How do we get the st_histogram RAM contents into the RAMs below?
  --   . DPRAM -> read>write process -> MM RAM
  -------------------------------------------------------------------------------
  gen_common_ram_crw_cw : for i in 0 to g_nof_instances - 1 generate
    u_common_ram_crw_cw : entity common_lib.common_ram_crw_cw
    generic map (
      g_technology     => c_tech_select_default,
      g_ram            => c_ram,
      g_init_file      => "UNUSED"
    )
    port map (
      mm_clk    => mm_clk,
      mm_rst    => mm_rst,
      mm_wr_en  => ram_copi_arr(i).wr,
      mm_wr_dat => ram_copi_arr(i).wrdata(c_ram_dat_w - 1 downto 0),
      mm_adr    => ram_copi_arr(i).address(c_ram_adr_w - 1 downto 0),
      mm_rd_en  => ram_copi_arr(i).rd,
      mm_rd_dat => ram_cipo_arr(i).rddata(c_ram_dat_w - 1 downto 0),
      mm_rd_val => ram_cipo_arr(i).rdval,

      st_clk    => dp_clk,
      st_rst    => dp_rst,
      st_wr_en  => wr_copi_arr(i).wr,
      st_adr    => wr_copi_arr(i).address(c_ram_adr_w - 1 downto 0),
      st_wr_dat => wr_copi_arr(i).wrdata(c_ram_dat_w - 1 downto 0)
     );
  end generate;

  -------------------------------------------------------------------------------
  -- Use snk_in Sync pulse to trigger RAM copy
  -- . use pipeline>=st_histogram I/O latency - don't copy too soon (clash with clear)
  -------------------------------------------------------------------------------
  u_common_pipeline_sl : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => 10
  )
  port map (
    clk     => dp_clk,
    rst     => dp_rst,
    in_dat  => snk_in_arr(0).sync,  -- Use only the status signal of st_histogram instance 0
    out_dat => ram_fill
  );

  -------------------------------------------------------------------------------
  -- Logic to move st_histogram RAM contents into the dual clock RAM above
  -------------------------------------------------------------------------------
  -- Keep track of ram_filling status and ram_address (used for reading and writing)
  nxt_ram_filling <= '0' when TO_UINT(ram_address) = c_addr_high else '1' when ram_fill = '1' else ram_filling;
  nxt_ram_address <= (others => '0') when ram_filling = '0' else INCR_UVEC(ram_address, 1) when ram_filling = '1' else ram_address;

  -- Do read request on ram_copi when ram_filling
  gen_copi_arr: for i in 0 to g_nof_instances - 1 generate
    st_histogram_ram_copi_arr(i).wr                              <= '0';
    st_histogram_ram_copi_arr(i).wrdata                          <= (others => '0');
    st_histogram_ram_copi_arr(i).rd                              <= ram_filling;
    st_histogram_ram_copi_arr(i).address(c_ram_adr_w - 1 downto 0) <= ram_address;
  end generate;

  -- Forward the read histogram data from ram_cipo into write copi of dual clock RAM
  gen_rd_cipo_to_wr_copi: for i in 0 to g_nof_instances - 1 generate
    wr_copi_arr(i).wr                              <= st_histogram_ram_cipo_arr(i).rdval;
    wr_copi_arr(i).wrdata(c_ram_dat_w - 1 downto 0)  <= st_histogram_ram_cipo_arr(i).rddata(c_ram_dat_w - 1 downto 0);
    wr_copi_arr(i).address(c_ram_adr_w - 1 downto 0) <= prv_ram_address;
  end generate;

  -- Registers
  p_clk : process(dp_clk, dp_rst) is
  begin
    if dp_rst = '1' then
      ram_address     <= (others => '0');
      prv_ram_address <= (others => '0');
      ram_filling     <= '0';
    elsif rising_edge(dp_clk) then
      ram_address     <= nxt_ram_address;
      prv_ram_address <= ram_address;
      ram_filling     <= nxt_ram_filling;
    end if;
  end process;

  -------------------------------------------------------------------------------
  -- MM multiplexing
  -------------------------------------------------------------------------------
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_instances,
    g_mult_addr_w => c_ram_adr_w
  )
  port map (
    mosi     => ram_copi,
    miso     => ram_cipo,
    mosi_arr => ram_copi_arr,
    miso_arr => ram_cipo_arr
  );
end str;
