#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for the ss_reorder entity.

   Description:

   This testcase is used in conjunction with the tb_mmf_ss_reorder.vhd file.

   The testcase script starts modelsim when c_start_modelsim=1 and loads the
   tb_mmf_ss_reorder simulation configuration.
   Generics for the VHDL testbench are passed to modelsim to configure the
   current simulation.
   To observe the VHDL signals in the wave window modelsim needs to be
   started manually and then c_start_modelsim must be set to 0 and the generics
   also must be set manually.

   Usage:

   > python tc_mmf_ss_reorder.py --unb 0 --bn 0 --sim 

"""

###############################################################################
# System imports
import test_case
import node_io
import unb_apertif as apr
import pi_diag_block_gen
import pi_diag_data_buffer
import pi_ss_reorder
import dsp_test
import sys
import pylab as pl
import numpy as np
import scipy as sp
import random
from tools import *
from common import *

############################
# Create a test case object
############################
tc = test_case.Testcase('TB - ', '')

# Constants/Generics that are shared between VHDL and Python 
# are placed within pragma's to be parsed to ModelSim:
  
# START_VHDL_GENERICS
c_nof_inputs       = 8              
c_nof_outputs      = 16             # The number of outputs 
c_dsp_data_w       = 16             # The data width of the data
c_frame_size       = 16             # Number of samples in a frame (between assertion of SOP and EOP
c_ram_init_file    = "UNUSED"       # Path and name of the file that holds initial content for the selection memory
c_pipeline_in      = 1              # pipeline in_data                     
c_pipeline_in_m    = 1              # pipeline in_data for M-fold fan out  
c_pipeline_out     = 1              # pipeline out_data                    
c_nof_frames       = 1              # Number of frames to be captured
# END_VHDL_GENERICS

tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test bench for ss_reorder unit' )
tc.append_log(3, '>>>')
tc.append_log(3, '')
tc.set_result('PASSED')

#############################
# Python specific constants #
#############################

# Block Generator
c_bg_nof_streams     = c_nof_inputs
c_bg_ram_size        = c_nof_frames*2**ceil_log2(c_frame_size)

# Data Buffer
c_db_nof_streams     = c_nof_outputs
c_db_ram_size        = c_bg_ram_size

c_stimuli_length_par = c_nof_frames*c_frame_size
c_stimuli_length     = c_stimuli_length_par*c_nof_inputs
c_select_w           = ceil_log2(c_nof_inputs)
c_select_word_w      = c_nof_outputs*c_select_w
c_blocks_per_sync    = 16

# Create access object for nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

# Create block generator instance
bg = pi_diag_block_gen.PiDiagBlockGen(tc, io, c_bg_nof_streams, tc.nodeBnNrs, ramSizePerChannel=c_bg_ram_size)

# Create databuffer instances
db_re = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = 'REAL', nofStreams=c_db_nof_streams, nodeNr=tc.nodeBnNrs, ramSizePerStream=c_db_ram_size)
db_im = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, instanceName = 'IMAG', nofStreams=c_db_nof_streams, nodeNr=tc.nodeBnNrs, ramSizePerStream=c_db_ram_size)

# Create dsp_test instance for helpful methods
dsp_test = dsp_test.DspTest(c_stimuli_length_par, c_dsp_data_w, c_dsp_data_w)

# Create subband select reorder instance                                                                                   
ss_reorder = pi_ss_reorder.PiSsReorder(tc, io, c_nof_inputs, c_nof_outputs, c_frame_size, nodeNr=tc.nodeBnNrs)

###############################################################################
#
# Create and write the selection buffer
#
###############################################################################
select_buf = []
for i in range(c_frame_size):
    select_value = CommonBits(i*i, c_select_word_w)
    select_buf.append(select_value[c_select_word_w-1:0])

ss_reorder.write_selects(select_buf) 

###############################################################################
#
# Read the selection buffer and verify
#
###############################################################################
read_selects = ss_reorder.read_selects() 

###############################################################################
#
# Create and write the stimuli to the BG
#
###############################################################################
# Prepare x stimuli for block generator
x_re_arr    = []
x_im_arr    = []  
bg_data_arr = []

for i in range(c_nof_inputs):
    x_re_stream = []
    x_im_stream = []
    for j in range(c_stimuli_length_par):
        x_re_stream.append(j*c_nof_inputs + i)
        x_im_stream.append(j*c_nof_inputs + i)
    x_re_arr.append(x_re_stream)
    x_im_arr.append(x_im_stream)
    bg_data_arr.append(dsp_test.concatenate_two_lists(x_re_stream, x_im_stream, c_dsp_data_w))  

# Write setting for the block generator:
bg.write_block_gen_settings(samplesPerPacket=c_frame_size, blocksPerSync=c_blocks_per_sync, gapSize=0, memLowAddr=0, memHighAddr=c_stimuli_length_par-1, BSNInit=0)

# Write the stimuli to the block generator and enable the block generator
for i in range(c_nof_inputs):
    bg.write_waveform_ram(data=bg_data_arr[i], channelNr= i)
    
bg.write_enable()

# Poll the databuffer to check if the response is there.
# Retry after 3 seconds so we don't issue too many MM reads in case of simulation.
do_until_ge(db_re.read_nof_words, ms_retry=3000, val=c_stimuli_length_par, s_timeout=3600)

###############################################################################
#
# Read output data from data buffer
#
###############################################################################
db_out_re = []
db_out_im = []
for i in range(c_nof_outputs):
    db_out_re.append(flatten(db_re.read_data_buffer(streamNr=i, n=c_stimuli_length_par, radix='dec', width=c_dsp_data_w)))
    db_out_im.append(flatten(db_im.read_data_buffer(streamNr=i, n=c_stimuli_length_par, radix='dec', width=c_dsp_data_w)))

y_re_arr = db_out_re
y_im_arr = db_out_im

###############################################################################
#
# Create reference list and verify
#
###############################################################################
ss_reorder.create_reference_and_verify(x_re_arr, x_im_arr, select_buf, y_re_arr, y_im_arr)

###############################################################################
# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(3, '>>>')

sys.exit(tc.get_result())