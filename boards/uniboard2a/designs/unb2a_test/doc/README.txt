Quick steps to compile and use design [unb2a_test] in RadionHDL
---------------------------------------------------------------


The following revisions are available for unb2a_test (see the directories in ../revisions/):

    unb2a_test_1GbE        : same as unb2a_minimal design but with extra 1GbE offload
                             STATUS: copied from unb2_test but not tested yet

    unb2a_test_10GbE       : same as unb2a_test_1GbE but with 10GbE offload channels (24 QSFP channels)
                             STATUS: copied from unb2_test but not tested yet

    unb2a_test_ddr_MB_I    : same as unb2a_test_1GbE but with 1 DDR4 module (I)
                             STATUS: tested OK

    unb2a_test_ddr_MB_II   : same as unb2a_test_1GbE but with 1 DDR4 module (II)
                             STATUS: tested OK

    unb2a_test_ddr_MB_I_II : same as unb2a_test_1GbE but with 2 DDR4 modules (I,II)
                             STATUS: tested OK

    unb2a_test_all         : unb2a_test_1GbE + unb2a_test_10GbE + unb2a_test_ddr_MB_I_II
                             STATUS: copied from unb2_test but not tested yet



-> In case of a new installation, the IP's have to be generated for Arria10. 
   In the: $HDL_WORK/libraries/technology/ip_arria10_e3sge3
   directory; run the bash script: ./generate-all-ip.sh
   -> The TSE IP gives a lot of critical warnings. To fix them, run this patch:
      cd $HDL_WORK/libraries/technology/ip_arria10_e3sge3/tse_sgmii_lvds
      ./run_patch.sh



1. Start with the Oneclick Commands:
    python $HDL_WORK/tools/oneclick/base/modelsim_config.py -t unb2a
    python $HDL_WORK/tools/oneclick/base/quartus_config.py -t unb2a


2. Generate MMM for QSYS (select one of these revisions):
    run_qsys unb2a unb2a_test_1GbE
    run_qsys unb2a unb2a_test_10GbE
    run_qsys unb2a unb2a_test_ddr_MB_I
    run_qsys unb2a unb2a_test_ddr_MB_II
    run_qsys unb2a unb2a_test_ddr_MB_I_II
    run_qsys unb2a unb2a_test_all



3. -> From here either continue to Modelsim (simulation) or Quartus (synthesis)

Simulation
----------
    # in bash do:
    rm -r ${HDL_IOFILE_SIM_DIR}/* # (optional)
    run_modelsim unb2a

Further Modelsim instructions: see the README file in the ../revisions/* directories



Synthesis
---------
Quartus instructions: (select one of these revisions):
    run_qcomp unb2a unb2a_test_1GbE
    run_qcomp unb2a unb2a_test_10GbE
    run_qcomp unb2a unb2a_test_ddr_MB_I
    run_qcomp unb2a unb2a_test_ddr_MB_II
    run_qcomp unb2a unb2a_test_ddr_MB_I_II
    run_qcomp unb2a unb2a_test_all


In case of needing the Quartus GUI for inspection (this starts the Quartus 15.1 GUI):
    run_quartus unb2a
load the project now from the build directory.


4. Load firmware
----------------
Using JTAG: Start the Quartus GUI and open: tools->programmer.
            Then click auto-detect; (click 4x ok)
            Use 'change file' to select the correct .sof file (in $HDL_WORK/build/unb2a/quartus/unb2a_test_...) for each FPGA
            Select the FPGA(s) which has to be programmed
            Click 'start'
Using EPCS: See step 6 below.


5. Testing on hardware
----------------------
Assuming the firmware is loaded and running already in the FPGA, the firmware can be tested from the connected
LCU computer. See the README file in the ../revisions/* directories for examples.



6.
(a)
Programming the EPCS flash.
when the EPCS module works an RBF file can be generated to program the flash,
then the .sof file file can be converted to .rbf with the 'run_rbf' script.
For generating a User image .RBF file:

    run_rbf unb2a unb2a_test_[revision]

For generating a Factory image .RBF file:

    run_rbf unb2a --unb2_factory unb2a_test_[revision]

The .RBF file is now in $HDL_WORK/build/unb2a/quartus/unb2a_test_[revision]
Now copy the .RBF file to the LCU host with 'scp'

(b)
Then to program the FPGA(s) via the LCU host, use the python script.

Program User image:
    python util_epcs.py --unb 1 --fn 0 -n 7 -s unb2a_test_[revision].rbf
Program Factory image:
    python util_epcs.py --unb 1 --fn 0 -n 3 -s unb2a_test_[revision].rbf

-> For extra info on RBF files on Uniboard2, see: $HDL_WORK/libraries/io/epcs/doc/README.txt

To start the User image:
    python util_remu.py --unb 1 --fn 0 -n 6  # ignore timeout error
To start the Factory image:
    python util_remu.py --unb 1 --fn 0 -n 5  # ignore timeout error



An alternative method to write the flash is via a .JIC file and JTAG:

Firstly a JIC file has to be generated from the SOF file.
In Quartus GUI; open current project; File -> Convert Programming Files.
Then setup:
- Output programming file: JIC
- Configuration device: EPCQL1024
- Mode: Active Serial x4
- Flash Loader: Add/Select Device Arria10/10AX115U4E3
- SOF Data: add file (the generated .sof file)
  - click the .sof file; Set property 'Compression' to ON
- Press 'Generate'
Then program the .JIC file (output_file.jic) to EPCS flash:
- Make sure that the JTAG (on server connected to board) runs at 16MHz:
  c:\altera\15.0\quartus\bin64\jtagconfig USB-BlasterII JtagClock 16M
- open tools->programmer
- make sure the 4 fpga icons have the device 10AX115U4F45ES
- right-click each fpga icon and attach flash device EPCQL1024
- optional see (*1)
- right-click each EPCQL1024 and change file from <none> to output_file.jic
- select click each Program/Configure radiobutton
- click start and wait for 'Successful'

(*1) When error select correct SFL (serial flash loader) from Altera service request for each FPGA:
     right-click each fpga and change file from <none> to sfl_enhanced_01_02e360dd.sof
     (in $HDL_WORK/boards/uniboard2/libraries/unb2a_board/quartus)


7.
Optionally you can readout the messages the NIOS application "unb_osy" prints over the JTAG interface.
First make sure that the JTAG cable is set to a correct speed. Use the command:

  jtagconfig --setparam "USB-BlasterII [USB-1]" JtagClock 8M

Then use this command to read from the JTAG terminal:

  cd /home/software/Altera/15.1/quartus
  ./bin/nios2-terminal --cable 1 --device=1

Where device is 1..4, selecting 1 of the 4 FPGAs

