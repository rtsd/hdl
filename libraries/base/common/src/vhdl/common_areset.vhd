-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra
-- Purpose: Immediately apply reset and synchronously release it at rising clk
-- Description:
--   When in_rst gets asserted, then the out_rst gets asserted immediately (=
--   asynchronous reset apply).
--   When in_rst gets de-assered, then out_rst gets de-asserted after
--   g_delay_len cycles (= synchronous reset release) + g_tree_len cycles
--   (synchronous reset tree).
--
--   The in_rst assert level is set by g_in_rst_level.
--   The out_rst assert level is set by c_out_rst_level = g_rst_level.
--
-- * g_delay_len: Long enough to ensure that the o_rst output of u_async has
--   recovered from meta-stability that can occur when a flipflop (FF) clocks
--   in the asynchrounous in_rst while it changes level.
-- * g_tree_len:
--   . Use g_tree_len = 0 to only have u_async, so with asynchrounous path from
--     in_rst to out_rst.
--   . Use g_tree_len = 1 to only have u_pipe of one flipflop (FF) to break the
--     asynchronous path, but with only one clk cycle extra latency.
--   . Use g_tree_len = c_tree_delay_len >> 1 to have a multi stage FF delay
--     line that can be expanded into a synchronous reset tree by means of FF
--     register duplication by the synthesis tool. The multi stage register
--     duplication eases timing closure in case of large fanout for out_rst.
-- Remarks:
-- . The in_rst can also synchronise other signals than a reset, e.g. a locked
--   signal from a PLL.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_areset is
  generic (
    g_in_rst_level : std_logic := '1';  -- = in_rst level
    g_rst_level    : std_logic := '1';  -- = out_rst level (keep original generic
                                        --   name for backward compatibility)
    g_delay_len    : natural := c_meta_delay_len;
    g_tree_len     : natural := 1
  );
  port (
    in_rst    : in  std_logic;
    clk       : in  std_logic;
    out_rst   : out std_logic
  );
end;

architecture str of common_areset is
  constant c_out_rst_value   : natural   := to_int(g_rst_level);
  constant c_out_rst_level   : std_logic := g_rst_level;
  constant c_out_rst_level_n : std_logic := not g_rst_level;

  signal i_rst               : std_logic;
  signal o_rst               : std_logic;
begin
  i_rst <= in_rst when g_in_rst_level = '1' else not in_rst;

  -- 2009
  -- Capture asynchronous reset assertion, to also support i_rst when there is
  -- no clk.
  without_pipe : if g_tree_len = 0 generate
    u_async : entity work.common_async
    generic map (
      g_rst_level => c_out_rst_level,
      g_delay_len => g_delay_len
    )
    port map (
      rst  => i_rst,
      clk  => clk,
      din  => c_out_rst_level_n,
      dout => out_rst
    );
  end generate;

  -- 2024
  -- Pass on synchronized reset with sufficient g_tree_len to ease timing
  -- closure by FF duplication in out_rst tree. Keep rst = '0' to break
  -- combinatorial path with in_rst to ease timing closure in the reset tree
  -- network. Use g_tree_len = 0 for no clocked reset tree as in 2009.
  -- Instantiate u_async again to keep 2009 and 2024 completely independent.
  -- To avoid delta-cycle differences due to e.g. out_rst <= o_rst when
  -- g_tree_len = 0, that could lead to different results in a simulation tb.
  with_pipe : if g_tree_len > 0 generate
    u_async : entity work.common_async
    generic map (
      g_rst_level => c_out_rst_level,
      g_delay_len => g_delay_len
    )
    port map (
      rst  => i_rst,
      clk  => clk,
      din  => c_out_rst_level_n,
      dout => o_rst
    );

    u_pipe : entity work.common_async
    generic map (
      g_rst_level => c_out_rst_level,
      g_delay_len => g_tree_len  -- must be positive in common_async
    )
    port map (
      rst  => '0',
      clk  => clk,
      din  => o_rst,
      dout => out_rst
    );
  end generate;
end str;
