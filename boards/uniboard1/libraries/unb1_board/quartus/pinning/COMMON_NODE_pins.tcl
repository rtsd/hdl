###############################################################################
#
# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Common pin assignments for front_node and back_node

# -- General: clk, pps, wdi, inta, intb
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_general_pins.tcl

# -- FPGA Interconnects Front-Node Back-Node
#    Clocks only as transceiver pins are now different (08-01-2010)
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_tr_clk_pins.tcl
#source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_tr_pins.tcl

# -- 1GbE Control Interface
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_1Gbe_pins.tcl

# -- SO-DIMM Memory Banks I and II
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_ddr_I_pins.tcl
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_ddr_II_pins.tcl

# -- I2C Interface to Sensors
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_sensor_pins.tcl

# -- Other: version[1:0], id[7:0], testio[7:0]
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/quartus/pinning/COMMON_NODE_other_pins.tcl
