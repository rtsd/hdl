--------------------------------------------------------------------------------
--
-- Author: Daniel van der Schuur
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Keep g_nof_channels serialized accumulators in an array of circular buffers
-- Description:
-- . A single adder per input stream is used to keep g_nof_channels separate sums per stream.
-- . For each stream, the intermediate sums are preserved in a circular buffer (FIFO).
-- . The FIFOs output the running sum such that it aligns with the corresponding channel (to be added
--   to the stored channel sum) at the adder inputs.
-- . The accumulators are cleared on the incoming sync pulse.

entity corr_accumulator is
  generic (
    g_nof_inputs       : natural;  -- Number of input streams
    g_nof_channels     : natural;  -- Number of running sums to keep per input stream
    g_nof_channel_accs : natural;  -- Maximum number of timesamples to accumulate (per channel)
    g_data_w           : natural;  -- Complex input data width
    g_note_is_ful      : boolean := false  -- when TRUE report NOTE when FIFO goes full, fifo overflow is always reported as FAILURE
                                           -- when FALSE no note reports, to speed up simulation and avoid many notes
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    src_out_arr    : out t_dp_sosi_arr(g_nof_inputs - 1 downto 0)
  );
end corr_accumulator;

architecture str of corr_accumulator is
  -- Complex accumulator data width: wide enough to support g_nof_channel_accs accumulations
  constant c_acc_data_w               : natural := g_data_w + ceil_log2(g_nof_channel_accs);

  signal reg_snk_in_arr               :t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  signal acc_cnt                      : std_logic_vector(ceil_log2(g_nof_channels) downto 0);
  signal nxt_acc_cnt                  : std_logic_vector(ceil_log2(g_nof_channels) downto 0);

  signal corr_adder_snk_in_2arr_2     : t_dp_sosi_2arr_2(g_nof_inputs - 1 downto 0);  -- Array of pairs
  signal corr_adder_src_out_arr       : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  signal dp_fifo_sc_src_in_arr        : t_dp_siso_arr(g_nof_inputs - 1 downto 0);
  signal dp_fifo_sc_src_out_arr       : t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

  signal output_valid                 : std_logic;
  signal output_cnt                   : std_logic_vector(ceil_log2(g_nof_channels) downto 0);
  signal nxt_output_cnt               : std_logic_vector(ceil_log2(g_nof_channels) downto 0);
  signal output_sync                  : std_logic;
begin
  -----------------------------------------------------------------------------
  -- Adder inputs: current snk_in_arr + corresponding running sum from the FIFO.
  -- On an input sync, we'll feed zeros into the second adder input to reset
  -- the accumulator value for each channel.
  -----------------------------------------------------------------------------
  gen_adder_inputs : for i in 0 to g_nof_inputs - 1 generate
    -- Adder input 0: multiplier output
    corr_adder_snk_in_2arr_2(i)(0).valid <= reg_snk_in_arr(i).valid;
    corr_adder_snk_in_2arr_2(i)(0).re    <= RESIZE_DP_DSP_DATA(reg_snk_in_arr(i).re(g_data_w - 1 downto 0));
    corr_adder_snk_in_2arr_2(i)(0).im    <= RESIZE_DP_DSP_DATA(reg_snk_in_arr(i).im(g_data_w - 1 downto 0));
    corr_adder_snk_in_2arr_2(i)(0).sync  <= reg_snk_in_arr(i).sync;

    -- Adder input 1: FIFO buffer output
    corr_adder_snk_in_2arr_2(i)(1).valid <= dp_fifo_sc_src_out_arr(i).valid;
    corr_adder_snk_in_2arr_2(i)(1).re    <= (others => '0') when dp_fifo_sc_src_out_arr(0).sync = '1' else dp_fifo_sc_src_out_arr(i).re;
    corr_adder_snk_in_2arr_2(i)(1).im    <= (others => '0') when dp_fifo_sc_src_out_arr(0).sync = '1' else dp_fifo_sc_src_out_arr(i).im;
    corr_adder_snk_in_2arr_2(i)(1).sync  <= dp_fifo_sc_src_out_arr(0).sync;
  end generate;

  -----------------------------------------------------------------------------
  -- Keep track of the number of valid words that entered the adder
  -- . used to align the FIFO output with the incoming current data at the adder
  --   inputs.
  -----------------------------------------------------------------------------
  nxt_acc_cnt <= INCR_UVEC(acc_cnt, 1) when (snk_in_arr(0).valid = '1' and unsigned(acc_cnt) < g_nof_channels) else acc_cnt;

  -----------------------------------------------------------------------------
  -- Complex adder stage
  -----------------------------------------------------------------------------
  u_corr_adder : entity work.corr_adder
  generic map (
    g_nof_inputs => g_nof_inputs,
    g_data_w     => c_acc_data_w
  )
  port map (
    clk            => clk,
    rst            => rst,

    snk_in_2arr_2  => corr_adder_snk_in_2arr_2,
    src_out_arr    => corr_adder_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- FIFOs to store the accumulator sums
  -----------------------------------------------------------------------------
  gen_dp_fifo_sc : for i in 0 to g_nof_inputs - 1 generate
    u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
    generic map (
      g_note_is_ful    => g_note_is_ful,
      g_data_w         => 2 * c_acc_data_w,
      g_use_ctrl       => false,
      g_use_sync       => sel_a_b(i = 0, true, false),  -- Pass on sync of stream 0.
      g_use_complex    => true,
      g_fifo_size      => g_nof_channels,
      g_fifo_af_margin => 1
    )
    port map (
      rst         => rst,
      clk         => clk,

      snk_in      => corr_adder_src_out_arr(i),
      src_in      => dp_fifo_sc_src_in_arr(i),
      src_out     => dp_fifo_sc_src_out_arr(i)
    );

    -- Shift out the first accumulated value when it aligns with the corresponding current value at the adder input
    -- . this produces the dp_fifo_sc_src_out_arr data in sync with reg_snk_in_arr data
    dp_fifo_sc_src_in_arr(i).ready <= snk_in_arr(0).valid when unsigned(acc_cnt) >= g_nof_channels else '0';
  end generate;

  -----------------------------------------------------------------------------
  -- Output g_nof_channel_accs per stream once per integration period
  -----------------------------------------------------------------------------
  output_valid <= dp_fifo_sc_src_out_arr(0).valid and dp_fifo_sc_src_out_arr(0).sync;

  -- Count the valid output cycles
  nxt_output_cnt   <= (others => '0') when (output_valid = '1' and unsigned(output_cnt) = g_nof_channels - 1) else INCR_UVEC(output_cnt, 1) when output_valid = '1' else output_cnt;
  -- Tag the valid output block with a sync pulse on the first channel
  output_sync  <= '1' when (output_valid = '1' and unsigned(output_cnt) = 0) else '0';

  gen_src_out_arr : for i in 0 to g_nof_inputs - 1 generate
    src_out_arr(i).re    <= RESIZE_DP_DSP_DATA(dp_fifo_sc_src_out_arr(i).re);
    src_out_arr(i).im    <= RESIZE_DP_DSP_DATA(dp_fifo_sc_src_out_arr(i).im);
    src_out_arr(i).valid <= output_valid;
    src_out_arr(i).sync  <= output_sync;
  end generate;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      acc_cnt                  <= (others => '0');
      output_cnt               <= (others => '0');
      reg_snk_in_arr           <= (others => c_dp_sosi_rst);
    elsif rising_edge(clk) then
      acc_cnt                  <= nxt_acc_cnt;
      output_cnt               <= nxt_output_cnt;
      reg_snk_in_arr           <= snk_in_arr;
    end if;
  end process;
end str;
