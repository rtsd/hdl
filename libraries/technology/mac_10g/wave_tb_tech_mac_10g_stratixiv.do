onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/mm_clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/mm_rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/csr_mosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/csr_miso
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/tx_clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/tx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/tx_snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/tx_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/rx_clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/rx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/rx_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/rx_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/xgmii_tx_data
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/xgmii_rx_data
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/mm_rst_n
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/tx_rst_n
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_ip_stratixiv -expand -group u0 -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_ip_stratixiv/u0/rx_rst_n
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/fifo_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/fifo_ful
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/fifo_emp
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/fifo_reg
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/nxt_fifo_reg
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/fifo_reg_valid
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/nxt_fifo_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/nxt_fifo_ful
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/nxt_fifo_emp
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/ff_siso
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/ff_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -expand -group u_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/u_dp_latency_adapter/i_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/wr_ful
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/rd_emp
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/i_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/fifo_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -expand -group u_rx_dp_latency_fifo -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_latency_fifo/i_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/pad_siso
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/pad_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/nxt_pad_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/in_siso
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/next_in_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/pend_in_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/in_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/concat_siso_arr
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -expand -group gen_rx_insert -group u_rx_dp_pad_insert -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_rx_insert/u_rx_dp_pad_insert/concat_sosi_arr
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group gen_tx_remove -group u_tx_dp_pad_remove -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_tx_remove/u_tx_dp_pad_remove/rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group gen_tx_remove -group u_tx_dp_pad_remove -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_tx_remove/u_tx_dp_pad_remove/clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group gen_tx_remove -group u_tx_dp_pad_remove -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_tx_remove/u_tx_dp_pad_remove/snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group gen_tx_remove -group u_tx_dp_pad_remove -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_tx_remove/u_tx_dp_pad_remove/snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group gen_tx_remove -group u_tx_dp_pad_remove -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_tx_remove/u_tx_dp_pad_remove/src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group gen_tx_remove -group u_tx_dp_pad_remove -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_tx_remove/u_tx_dp_pad_remove/src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group gen_tx_remove -group u_tx_dp_pad_remove -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_tx_remove/u_tx_dp_pad_remove/split_siso_arr
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group gen_tx_remove -group u_tx_dp_pad_remove -radix unsigned /tb_tech_mac_10g/gen_dut/dut/gen_tx_remove/u_tx_dp_pad_remove/split_sosi_arr
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/reg_ready
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/reg_val
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -group no_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/no_fifo/u_latency/i_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/fifo_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/fifo_ful
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/fifo_emp
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/fifo_reg
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/nxt_fifo_reg
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/fifo_reg_valid
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/nxt_fifo_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/nxt_fifo_ful
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/nxt_fifo_emp
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/ff_siso
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/ff_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_rx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_rx_dp_latency_adapter/i_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/reg_ready
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/reg_val
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -group gen_fifo -group u_latency -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/gen_fifo/u_latency/i_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/fifo_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/fifo_ful
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/fifo_emp
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/fifo_reg
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/nxt_fifo_reg
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/fifo_reg_valid
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/nxt_fifo_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/nxt_fifo_ful
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/nxt_fifo_emp
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/ff_siso
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/ff_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -group u_tx_dp_latency_adapter -radix unsigned /tb_tech_mac_10g/gen_dut/dut/u_tx_dp_latency_adapter/i_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/mm_clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/mm_rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/csr_mosi
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/csr_miso
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_clk
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/xgmii_tx_data
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/xgmii_rx_data
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in_data
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_mac_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_remove_snk_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/tx_remove_snk_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out_data
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_out_rl1
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_mac_src_in_rl1
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_fifo_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_fifo_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_insert_src_out
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/rx_insert_src_in
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/fifo_usedw
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/fifo_ful
add wave -noupdate -expand -group tb_tech_mac_10g -group gen_dut -expand -group dut -radix unsigned /tb_tech_mac_10g/gen_dut/dut/fifo_emp
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tb_end
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_clk
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_rst
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_ref_clk
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_phy_clk
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_rst
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/total_header
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_init
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/mm_mosi_wrdata
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_mosi
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_miso
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/mm_miso_rdval
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/mm_miso_rddata
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_en
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_siso
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned -expand -subitemconfig {/tb_tech_mac_10g/tx_sosi.sync {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.bsn {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.data {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.re {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.im {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.valid {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.sop {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.eop {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.empty {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.channel {-height 15 -radix unsigned} /tb_tech_mac_10g/tx_sosi.err {-height 15 -radix unsigned}} /tb_tech_mac_10g/tx_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/tx_sosi_data
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_siso
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned -expand -subitemconfig {/tb_tech_mac_10g/rx_sosi.sync {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.bsn {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.data {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.re {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.im {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.valid {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.sop {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.eop {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.empty {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.channel {-height 15 -radix unsigned} /tb_tech_mac_10g/rx_sosi.err {-height 15 -radix unsigned}} /tb_tech_mac_10g/rx_sosi
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/rx_sosi_data
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/xgmii_tx_data
add wave -noupdate -expand -group tb_tech_mac_10g -radix hexadecimal /tb_tech_mac_10g/xgmii_rx_data
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/expected_sosi_arr
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/tx_pkt_cnt
add wave -noupdate -expand -group tb_tech_mac_10g -radix unsigned /tb_tech_mac_10g/rx_pkt_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5200000000 fs} 0}
configure wave -namecolwidth 453
configure wave -valuecolwidth 115
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {0 fs} {22694672506 fs}
