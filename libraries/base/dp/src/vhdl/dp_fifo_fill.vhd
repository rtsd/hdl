--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Wrapper of dp_fifo_fill_sc.vhd
-- Description: See dp_fifo_fill_core.vhd
-- Remark:
--   This wrapper is for backwards compatibility, better use dp_fifo_fill_sc.vhd
--   for new designs.

library IEEE, common_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity dp_fifo_fill is
  generic (
    g_technology     : natural := c_tech_select_default;
    g_data_w         : natural := 16;
    g_bsn_w          : natural := 1;
    g_empty_w        : natural := 1;
    g_channel_w      : natural := 1;
    g_error_w        : natural := 1;
    g_use_bsn        : boolean := false;
    g_use_empty      : boolean := false;
    g_use_channel    : boolean := false;
    g_use_error      : boolean := false;
    g_use_sync       : boolean := false;
    g_use_complex    : boolean := false;  -- TRUE feeds the concatenated complex fields (im & re) through the FIFO instead of the data field.
    g_fifo_fill      : natural := 0;
    g_fifo_size      : natural := 256;  -- (32+2) * 256 = 1 M9K, g_data_w+2 for sop and eop
    g_fifo_af_margin : natural := 4;  -- >=4, Nof words below max (full) at which fifo is considered almost full for snk_out.ready
    g_fifo_af_xon    : natural := 0;  -- >=0, Nof words below max (full) at which fifo is considered almost full for snk_out.xon
    g_fifo_rl        : natural := 1  -- use RL=0 for internal show ahead FIFO, default use RL=1 for internal normal FIFO
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    -- Monitor FIFO filling
    wr_ful      : out std_logic;
    usedw       : out std_logic_vector(ceil_log2(largest(g_fifo_size, g_fifo_fill + g_fifo_af_margin + 2)) - 1 downto 0);  -- = ceil_log2(c_fifo_size)-1 DOWNTO 0
    rd_emp      : out std_logic;

    -- MM control FIFO filling (assume 32 bit MM interface)
    wr_usedw_32b : out std_logic_vector(c_word_w - 1 downto 0);  -- = wr_usedw
    rd_usedw_32b : out std_logic_vector(c_word_w - 1 downto 0);  -- = rd_usedw
    rd_fill_32b  : in  std_logic_vector(c_word_w - 1 downto 0) := TO_UVEC(g_fifo_fill, c_word_w);

    -- ST sink
    snk_out     : out t_dp_siso;
    snk_in      : in  t_dp_sosi;
    -- ST source
    src_in      : in  t_dp_siso;
    src_out     : out t_dp_sosi
  );
end dp_fifo_fill;

architecture str of dp_fifo_fill is
begin
  u_dp_fifo_fill_sc : entity work.dp_fifo_fill_sc
  generic map (
    g_technology     => g_technology,
    g_data_w         => g_data_w,
    g_bsn_w          => g_bsn_w,
    g_empty_w        => g_empty_w,
    g_channel_w      => g_channel_w,
    g_error_w        => g_error_w,
    g_use_bsn        => g_use_bsn,
    g_use_empty      => g_use_empty,
    g_use_channel    => g_use_channel,
    g_use_error      => g_use_error,
    g_use_sync       => g_use_sync,
    g_use_complex    => g_use_complex,
    g_fifo_fill      => g_fifo_fill,
    g_fifo_size      => g_fifo_size,
    g_fifo_af_margin => g_fifo_af_margin,
    g_fifo_af_xon    => g_fifo_af_xon,
    g_fifo_rl        => g_fifo_rl
  )
  port map (
    rst         => rst,
    clk         => clk,

    -- Monitor FIFO filling
    wr_ful      => wr_ful,
    usedw       => usedw,
    rd_emp      => rd_emp,

    -- MM control FIFO filling (assume 32 bit MM interface)
    wr_usedw_32b => wr_usedw_32b,
    rd_usedw_32b => rd_usedw_32b,
    rd_fill_32b  => rd_fill_32b,

    -- ST sink
    snk_out     => snk_out,
    snk_in      => snk_in,
    -- ST source
    src_in      => src_in,
    src_out     => src_out
  );
end str;
