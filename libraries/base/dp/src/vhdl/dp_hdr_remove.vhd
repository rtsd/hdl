--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_hdr_remove is
  generic (
    g_data_w        : natural;
    g_symbol_w      : natural;
    g_hdr_nof_words : natural
  );
  port (
    mm_rst  : in  std_logic;
    mm_clk  : in  std_logic;

    st_rst  : in  std_logic;
    st_clk  : in  std_logic;

    snk_out : out t_dp_siso;
    snk_in  : in  t_dp_sosi;

    sla_in   : in  t_mem_mosi;
    sla_out  : out t_mem_miso;

    src_in  : in  t_dp_siso;
    src_out : out t_dp_sosi
  );
end dp_hdr_remove;

architecture str of dp_hdr_remove is
  constant c_nof_symbols : natural := (g_data_w * g_hdr_nof_words) / g_symbol_w;

  -- element (0) gets the head, element (1) gets the tail
  signal split_siso_arr : t_dp_siso_arr(0 to 1);
  signal split_sosi_arr : t_dp_sosi_arr(0 to 1);
begin
  u_dp_ram_to_mm : entity work.dp_ram_to_mm
  generic map (
    g_ram_rd_nof_words => g_hdr_nof_words * (g_data_w / c_word_w),
    g_ram_wr_dat_w     => g_data_w
  )
  port map (
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,

    st_rst           => st_rst,
    st_clk           => st_clk,

    sla_out          => sla_out,
    sla_in           => sla_in,

    snk_in           => split_sosi_arr(0),
    snk_out          => split_siso_arr(0)
  );

  split_siso_arr(1)  <= src_in;
  src_out            <= split_sosi_arr(1);

  u_split : entity work.dp_split  -- RL = 1
  generic map (
    g_data_w        => g_data_w,
    g_symbol_w      => g_symbol_w,
    g_nof_symbols   => c_nof_symbols
  )
  port map (
    rst         => st_rst,
    clk         => st_clk,
    -- ST sinks
    snk_out     => snk_out,
    snk_in      => snk_in,
    -- ST source
    src_in_arr  => split_siso_arr,
    src_out_arr => split_sosi_arr
  );
end str;
