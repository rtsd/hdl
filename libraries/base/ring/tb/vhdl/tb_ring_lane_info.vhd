-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle

-- Purpose:
-- . test bench for ring_lane_info.vhd (and ring_lane_info_reg.vhd)
-- Description:
--
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.ring_pkg.all;

entity tb_ring_lane_info is
end tb_ring_lane_info;

architecture tb of tb_ring_lane_info is
  constant c_dp_clk_period        : time := 5 ns;  -- 200 MHz
  constant c_mm_clk_period        : time := 20 ns;  -- 50 MHz

  constant c_cross_clock_domain_latency : natural := 20;

  -- used mm_adresses on mm bus
  constant c_mm_addr_lane_direction          : natural := 0;
  constant c_mm_addr_transport_nof_hops      : natural := 1;

  signal tb_end              : std_logic := '0';
  signal tb_mm_reg_end       : std_logic := '0';

  signal dp_clk              : std_logic := '1';  -- digital data path clock = 200 MHz (deser factor 4);
  signal dp_rst              : std_logic;

  signal mm_clk              : std_logic := '1';  -- MM control clock = 50 MHz
  signal mm_rst              : std_logic;

  signal reg_mosi            : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso            : t_mem_miso;

  -- signals used to change settings of ring_lane_info
  signal lane_direction     : std_logic := '0';

  signal lane_info  : t_ring_lane_info;

  -- signals used for response of mm bus
  signal mm_natural_response : natural;
begin
  dp_rst <= '1', '0' after c_dp_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;

  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;

  p_mm_reg_stimuli : process
  begin
    reg_mosi <= c_mem_mosi_rst;

    -- initialyze
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 100);

    -- default all register hold value 0, try to write 1 in all registers
    proc_mem_mm_bus_wr(c_mm_addr_transport_nof_hops,     11, mm_clk, reg_miso, reg_mosi);
    proc_mem_mm_bus_wr(c_mm_addr_lane_direction,         1,  mm_clk, reg_miso, reg_mosi);  -- RO
    proc_common_wait_some_cycles(mm_clk, c_cross_clock_domain_latency);

    proc_mem_mm_bus_rd(c_mm_addr_transport_nof_hops, mm_clk, reg_mosi);  proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);  proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response = 11
      report "wrong c_mm_addr_transport_nof_hops"
      severity ERROR;

    proc_mem_mm_bus_rd(c_mm_addr_lane_direction, mm_clk, reg_mosi);  proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    mm_natural_response <= TO_UINT(reg_miso.rddata);  proc_common_wait_some_cycles(mm_clk, 1);
    assert mm_natural_response /= 1
      report "wrong lane_direction (not read only)"
      severity ERROR;

    proc_common_wait_some_cycles(mm_clk, 100);
    tb_mm_reg_end <= '1';
    wait;
  end process;

  -- check if values in lane_info match with expected values
  p_lane_info_stimuli : process
  begin
    proc_common_wait_until_high(mm_clk, tb_mm_reg_end);  -- wait for p_mm_reg_stimuli done

    assert TO_UINT(lane_info.transport_nof_hops) = 11
      report "wrong lane_info.transport_nof_hops value"
      severity ERROR;
    assert lane_info.lane_direction = '0'
      report "wrong lane_info.lane_direction value"
      severity ERROR;

    proc_common_wait_some_cycles(mm_clk, 100);
    tb_end <= '1';
    wait;
  end process;

  -- SDP info
  u_dut: entity work.ring_lane_info
    port map (
      mm_clk    => mm_clk,
      mm_rst    => mm_rst,

      dp_clk    => dp_clk,
      dp_rst    => dp_rst,

      reg_mosi  => reg_mosi,
      reg_miso  => reg_miso,

      lane_direction => lane_direction,

      lane_info => lane_info
    );
end tb;
