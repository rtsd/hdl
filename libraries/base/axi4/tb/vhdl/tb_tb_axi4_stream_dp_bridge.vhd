-- --------------------------------------------------------------------------
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- Author:
-- . Reinier van der Walle
-- Purpose:
-- . Multi-TB for axi4_stream_dp_bridge.
-- Description:
-- . Tests multiple instances of tb_axi4_stream_dp_bridge with different
--   ready-latency configurations.

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_axi4_stream_dp_bridge is
end tb_tb_axi4_stream_dp_bridge;

architecture tb of tb_tb_axi4_stream_dp_bridge is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 2
  -- > run -all --> OK

  -- g_dp_rl               : NATURAL := 1;
  -- g_axi4_rl             : NATURAL := 0;

  u_dp_1_axi4_0             : entity work.tb_axi4_stream_dp_bridge generic map (1, 0);
  u_dp_1_axi4_1             : entity work.tb_axi4_stream_dp_bridge generic map (1, 1);
  u_dp_0_axi4_0             : entity work.tb_axi4_stream_dp_bridge generic map (0, 0);
  u_dp_0_axi4_1             : entity work.tb_axi4_stream_dp_bridge generic map (0, 1);
end tb;
