###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# -- include ddr3 pins
source $::env(HDL_WORK)/boards/uniboard1/libraries/unb1_board/src/tcl/COMMON_NODE_ddr_I_rec_pins.tcl

# -- include the clock pin
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_general_pins.tcl

# --include the 1GbE pins
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_1Gbe_pins.tcl

# -- include the testIO pins
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_other_pins.tcl

# -- I2C Interface to Sensors
source $::env(UNB)/Firmware/designs/unb_common/src/tcl/COMMON_NODE_sensor_pins.tcl

#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IN[0].oct_rzqin
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IN[0].alert_n

#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_OU[0].par
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_OU[0].act_n
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_OU[0].bg[0]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_OU[0].bg[1]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_OU[0].dm[8]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_OU[0].a[16]

#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[0]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[1]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[2]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[3]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[4]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[5]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[6]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[7]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dbi_n[8]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dqs_n[8]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dqs[8]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dq[64]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dq[65]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dq[66]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dq[67]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dq[68]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dq[69]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dq[70]
#set_instance_assignment -name VIRTUAL_PIN ON -to MB_I_IO[0].dq[71]
