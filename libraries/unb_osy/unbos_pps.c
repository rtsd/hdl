#include <unbos_pps.h>
#include <osx_timer.h>
#include <unbosx_util.h>

// Only do an implementation if there is one
#ifdef PIO_PPS_BASE

// Poll the PPS-level (bit 31) for it
// to toggle. Do not forget to yank the
// watchdog every now and then ...
int UNBOS_pps_wait( void ) {
	alt_u32    pps_info, pps_level;
	clock_t    stop = TIMER_0_TICKS_PER_SEC + (TIMER_0_TICKS_PER_SEC>>7);

	pps_info   = IORD_ALTERA_AVALON_PIO_DATA(PIO_PPS_BASE);
	pps_level  = pps_info & 0x80000000;
	stop      += osx_clock();
	do {
		pps_info  = IORD_ALTERA_AVALON_PIO_DATA(PIO_PPS_BASE);
		pps_info &= 0x80000000;
		UNBOSX_ResetWatchdog();
	} while( (pps_info^pps_level)==0 && osx_clock()<stop );
	return (pps_info^pps_level);
}
#endif
