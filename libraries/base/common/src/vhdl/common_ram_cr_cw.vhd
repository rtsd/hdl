-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   -
-- Changed by:
--   D.F. Brouwer
-- Description:
--   Dual clock domain
--   Use port a only for write in write clock domain
--   Use port b only for read  in read  clock domain
-- Remarks:
--   The crw_crw RAM covers all other variants, which were utilized by other
--   common RAM variant files. However, because the crw_crw IP is no longer
--   supported as it was previously used for previous FPGA technology identifiers
--   (device types) by the Agilex 7 (agi027_1e1v), the individual IPs should be
--   used. As a result, this file has been modified. [1]
-- Reference:
--   [1] Based on the architecture of common_ram_crw_crw.vhd.

library IEEE, technology_lib, tech_memory_lib;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_ram_cr_cw is
  generic (
    g_technology : natural := c_tech_select_default;
    g_ram        : t_c_mem := c_mem_ram;
    g_init_file  : string := "UNUSED"
  );
  port (
    -- Write port clock domain
    wr_rst    : in  std_logic := '0';
    wr_clk    : in  std_logic;
    wr_clken  : in  std_logic := '1';
    wr_en     : in  std_logic := '0';
    wr_adr    : in  std_logic_vector(g_ram.adr_w - 1 downto 0) := (others => '0');
    wr_dat    : in  std_logic_vector(g_ram.dat_w - 1 downto 0) := (others => '0');
    -- Read port clock domain
    rd_rst    : in  std_logic := '0';
    rd_clk    : in  std_logic;
    rd_clken  : in  std_logic := '1';
    rd_en     : in  std_logic := '1';
    rd_adr    : in  std_logic_vector(g_ram.adr_w - 1 downto 0) := (others => '0');
    rd_dat    : out std_logic_vector(g_ram.dat_w - 1 downto 0);
    rd_val    : out std_logic
  );
end common_ram_cr_cw;

architecture str of common_ram_cr_cw is
  constant c_rd_latency : natural := sel_a_b(g_ram.latency < 2,            g_ram.latency,              2);  -- handle read latency 1 or 2 in RAM
  constant c_pipeline   : natural := sel_a_b(g_ram.latency > c_rd_latency, g_ram.latency - c_rd_latency, 0);  -- handle rest of read latency > 2 in pipeline

  -- Intermediate signal for extra pipelining
  signal ram_rd_dat       : std_logic_vector(rd_dat'range);

  -- Map sl to single bit slv for rd_val pipelining
  signal ram_rd_en        : std_logic_vector(0 downto 0);
  signal ram_rd_val       : std_logic_vector(0 downto 0);
begin
  assert g_ram.latency >= 1
    report "common_ram_cr_cw : only support read latency >= 1"
    severity FAILURE;

  -- memory access
  u_cr_cw : entity tech_memory_lib.tech_memory_ram_cr_cw
  generic map (
    g_technology => g_technology,
    g_adr_w      => g_ram.adr_w,
    g_dat_w      => g_ram.dat_w,
    g_nof_words  => g_ram.nof_dat,
    g_rd_latency => c_rd_latency,
    g_init_file  => g_init_file
  )
  port map
  (
    wrclock   => wr_clk,
    wrclocken => wr_clken,
    wren      => wr_en,
    wraddress => wr_adr,
    data      => wr_dat,
    rdclock   => rd_clk,
    rdclocken => rd_clken,
    rdaddress => rd_adr,
    q         => ram_rd_dat
  );

  -- read output
  u_pipe : entity work.common_pipeline
  generic map (
    g_pipeline   => c_pipeline,
    g_in_dat_w   => g_ram.dat_w,
    g_out_dat_w  => g_ram.dat_w
  )
  port map (
    clk     => rd_clk,
    clken   => rd_clken,
    in_dat  => ram_rd_dat,
    out_dat => rd_dat
  );

  -- rd_val control
  ram_rd_en(0) <= rd_en;

  rd_val <= ram_rd_val(0);

  u_rd_val : entity work.common_pipeline
  generic map (
    g_pipeline   => g_ram.latency,
    g_in_dat_w   => 1,
    g_out_dat_w  => 1
  )
  port map (
    clk     => rd_clk,
    clken   => rd_clken,
    in_dat  => ram_rd_en,
    out_dat => ram_rd_val
  );
end str;
