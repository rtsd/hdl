-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

--14-6-2005
--file i2cslave.vhd
--entity description of i2c slave for the RCU
--written by A.W. Gunst

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity i2cslave is
  generic (
    g_rx_filter       : boolean := true;  -- when TRUE enable I2C input SCL/SDA signal filtering
    g_address         : std_logic_vector(6 downto 0) := "0000001";  -- Slave I2C address
    g_nof_ctrl_bytes  : natural := 3  -- size of control register in bytes
  );
  port(
    clk         : in  std_logic;  -- system clock (clk freq >> SCL freq)
    SDA         : inout std_logic;  -- I2C Serial Data Line
    SCL         : in  std_logic;  -- I2C Serial Clock Line
    RST         : in  std_logic;  -- optional reset bit
    CTRL_REG    : out std_logic_vector(8 * g_nof_ctrl_bytes - 1 downto 0)  -- ctrl for RCU control
  );
end i2cslave;

architecture rtl of i2cslave is
  function strong( sl : in std_logic ) return std_logic is
  begin
    if sl = 'H'THEN
      return '1';
    elsif sl = 'L' then
      return '0';
    else
      return sl;
    end if;
  end;

  type   state  is (reset, read_addr, read_data, write_data, acknowledge, nacknowledge, wacknowledge, wnacknowledge);

  -- Start of g_rx_filter related declarations
  constant c_meta_len   : natural := 3;  -- use 3 FF to tackle meta stability between SCL and clk domain
  constant c_clk_cnt_w  : natural := 5;  -- use lower effective clk rate
  constant c_line_len   : natural := 7;  -- use FF line to filter SCL
                                           -- The maximum bit rate is 100 kbps, so 10 us SCL period. The pullup rise time
                                           -- of SCL and SDA is worst case (10k pullup) about 2 us, so a line_len of about
                                           -- 1 us suffices. At 200 MHz the line covers is 2^5 * 7 * 5 ns = 1.12 us of SCL,
                                           -- respectively 1.4 us at 160 MHz.
  constant c_version    : std_logic_vector(3 downto 0) := "0001";

  signal clk_en         : std_logic := '0';
  signal nxt_clk_en     : std_logic;
  signal clk_cnt        : unsigned(c_clk_cnt_w - 1 downto 0) := (others => '0');
  signal nxt_clk_cnt    : unsigned(clk_cnt'range);

  signal scl_meta       : std_logic_vector(0 to c_meta_len - 1);
  signal scl_line       : std_logic_vector(0 to c_line_len - 1);
  signal scl_or         : std_logic;
  signal nxt_scl_or     : std_logic;
  signal scl_and        : std_logic;
  signal nxt_scl_and    : std_logic;
  signal scl_hi         : std_logic;
  signal scl_lo         : std_logic;
  signal scl_rx         : std_logic;
  signal nxt_scl_rx     : std_logic;

  signal sda_meta       : std_logic_vector(0 to c_meta_len - 1);
  signal sda_line       : std_logic_vector(0 to c_line_len - 1);
  signal sda_or         : std_logic;
  signal nxt_sda_or     : std_logic;
  signal sda_and        : std_logic;
  signal nxt_sda_and    : std_logic;
  signal sda_hi         : std_logic;
  signal sda_lo         : std_logic;
  signal sda_rx         : std_logic;
  signal nxt_sda_rx     : std_logic;
  -- End of g_rx_filter related declarations

  signal start          : std_logic;
  signal stop           : std_logic;

  signal streset        : std_logic;  -- bit to reset the start and stop flip flops
  signal latch_ctrl     : std_logic;
  signal latch_ctrl_dly : std_logic;
  signal ctrl_tmp       : std_logic_vector(8 * g_nof_ctrl_bytes - 1 downto 0);  -- register to read and write to temporarily
  signal i_ctrl_reg     : std_logic_vector(8 * g_nof_ctrl_bytes - 1 downto 0);  -- output register
  signal rw             : std_logic;  -- bit to indicate a read or a write
  signal ctrladr        : std_logic_vector(6 downto 0);  -- I2C address register
  signal bitcnt         : natural range 0 to 7;  -- bitcnt for reading bits
  signal bytecnt        : natural range 0 to g_nof_ctrl_bytes - 1;  -- bytenct for reading bytes
  signal current_state  : state;

  signal tri_en         : std_logic;  -- signal to enable the tristate buffer for the SDA line
  signal sda_int        : std_logic;  -- internal SDA line to drive the SDA pin
  signal wbitcnt        : natural range 0 to 8;  -- bitcnt for writing bits
  signal wbytecnt       : natural range 0 to g_nof_ctrl_bytes - 1;  -- bytenct for writing bytes
  signal zeroedge_state : state;
begin
  CTRL_REG <= i_ctrl_reg;

  no_rx : if g_rx_filter = false generate
    scl_rx <= strong(SCL);
    sda_rx <= strong(SDA);
  end generate;

  gen_rx : if g_rx_filter = true generate
    p_clk : process(rst, clk)
    begin
      if RST = '1' then
--         clk_cnt  <= (OTHERS => '0');
--         clk_en   <= '0';
        -- SCL
        scl_meta <= (others => '1');
        scl_line <= (others => '1');
        scl_or   <= '1';
        scl_and  <= '1';
        scl_hi   <= '1';
        scl_lo   <= '0';
        scl_rx   <= '1';
        -- SDA
        sda_meta <= (others => '1');
        sda_line <= (others => '1');
        sda_or   <= '1';
        sda_and  <= '1';
        sda_hi   <= '1';
        sda_lo   <= '0';
        sda_rx   <= '1';
      elsif rising_edge(clk) then
        clk_cnt  <= clk_cnt + 1;
        clk_en   <= nxt_clk_en;
        if clk_en = '1' then
          -- SCL
          scl_meta <= strong(SCL) & scl_meta(0 to scl_meta'high - 1);
          scl_line <= scl_meta(scl_meta'high) & scl_line(0 to scl_line'high - 1);
          scl_or   <= nxt_scl_or;
          scl_and  <= nxt_scl_and;
          scl_hi   <=     scl_or and scl_and;
          scl_lo   <= not(scl_or or  scl_and);
          scl_rx   <= nxt_scl_rx;
          -- SDA
          sda_meta <= strong(SDA) & sda_meta(0 to sda_meta'high - 1);
          sda_line <= sda_meta(sda_meta'high) & sda_line(0 to sda_line'high - 1);
          sda_or   <= nxt_sda_or;
          sda_and  <= nxt_sda_and;
          sda_hi   <=     sda_or and sda_and;
          sda_lo   <= not(sda_or or  sda_and);
          sda_rx   <= nxt_sda_rx;
        end if;
      end if;
    end process;

    nxt_clk_en <= '1' when signed(clk_cnt) = -1 else '0';

    -- SCL
    nxt_scl_or  <= '0' when unsigned(scl_line) =  0 else '1';
    nxt_scl_and <= '1' when   signed(scl_line) = -1 else '0';
    nxt_scl_rx  <= '1' when scl_hi = '1' else '0' when scl_lo = '1' else scl_rx;

    -- SDA
    nxt_sda_or  <= '0' when unsigned(sda_line) =  0 else '1';
    nxt_sda_and <= '1' when   signed(sda_line) = -1 else '0';
    nxt_sda_rx  <= '1' when sda_hi = '1' else '0' when sda_lo = '1' else sda_rx;
  end generate;

  startcontrol: process(streset, RST, sda_rx)
  begin
    if streset = '1' or RST = '1' then
      start <= '0';
    elsif falling_edge(sda_rx) then
      if scl_rx = '1' then
        start <= '1';
      else
        start <= '0';
      end if;
    end if;
  end process;

  stopcontrol: process(streset, RST, sda_rx)
  begin
    if streset = '1' or RST = '1' then
      stop <= '0';
    elsif rising_edge(sda_rx) then
      if scl_rx = '1' then
        stop <= '1';
      else
        stop <= '0';
      end if;
    end if;
  end process;

  control: process(RST, scl_rx)  -- i2c slave
  begin
    if RST = '1' then
      --reset input connected to bit 17 of CTRL register, hence default for CTRL[17] must be '0' so RST will act as a spike.
      --if the spike is strong enough, then this works also in hardware for the rest of the logic connected to RST.

      -- Rising edge registers:
      streset        <= '0';
      latch_ctrl     <= '0';
      latch_ctrl_dly <= '0';
      ctrl_tmp       <= (others => '0');
      --i_ctrl_reg     <= X"0007c7"; --all supplies on, LBL 1a mode
      --i_ctrl_reg     <= X"00F979"; --LBL mode 1a
      --i_ctrl_reg     <= X"00FD79"; --LBL mode 1b
      --i_ctrl_reg     <= X"00FB7A"; --LBH mode 1a
      --i_ctrl_reg     <= X"00FF7A"; --LBH mode 1b
      --i_ctrl_reg     <= X"00FFA4"; --HBA mode 2
      --i_ctrl_reg     <= X"00FF94"; --HBA mode 3
      --i_ctrl_reg     <= X"00FF84"; --HBA mode 4
      --i_ctrl_reg     <= X"00F179"; --LBL mode 1a incl. 16 dB attenuation
      i_ctrl_reg     <= (others => '0');
      i_ctrl_reg(23 downto 20) <= c_version;
      rw             <= '0';
      ctrladr        <= (others => '0');
      bitcnt         <= 0;
      bytecnt        <= 0;
      current_state  <= reset;

      -- Falling edge registers:
      sda_int        <= 'H';
      tri_en         <= '0';
      wbitcnt        <= 0;
      wbytecnt       <= 0;
      zeroedge_state <= reset;

    elsif rising_edge(scl_rx) then

      -- Latch CTRL register
      latch_ctrl_dly <= latch_ctrl;
      if latch_ctrl_dly = '1' then  -- latch ctrl register after ack
        i_ctrl_reg <= ctrl_tmp;
        i_ctrl_reg(23 downto 20) <= c_version;
      end if;

      -- Statemachine
      --   default assignments (others keep their value accross states during the access)
      streset       <= '0';
      latch_ctrl    <= '0';
      bitcnt        <= 0;
      current_state <= zeroedge_state;
      if start = '1' then
        streset       <= '1';  -- reset start bit
        ctrladr       <= ctrladr(5 downto 0) & sda_rx;  -- first commands of read_addr state should immediately be executed
        bitcnt        <= 1;
        bytecnt       <= 0;
        current_state <= read_addr;
      elsif stop = '1' then  -- only recognized if there occurs an SCL edge between I2C Stop and Start
        streset       <= '1';  -- reset stop bit
        bytecnt       <= 0;
        current_state <= reset;
      else
        case zeroedge_state is
          when reset => null;  -- only a Start gets the statemachines out of reset, all SCL edges are ignored until then
          when read_addr =>
            if bitcnt < 7 then
              ctrladr <= ctrladr(5 downto 0) & sda_rx;  -- shift the data to the left
                                                       --shift a new bit in (MSB first)
              bitcnt <= bitcnt + 1;
            else
              rw <= sda_rx;  -- last bit indicates a read or a write
              if ctrladr = g_address then
                current_state <= acknowledge;
              else
                current_state <= reset;
              end if;
            end if;
          when read_data =>
            if bitcnt < 7 then
              ctrl_tmp(8 * g_nof_ctrl_bytes - 1 downto 1) <= ctrl_tmp(8 * g_nof_ctrl_bytes - 2 downto 0);  -- shift the data to the left
              ctrl_tmp(0) <= sda_rx;  -- shift a new bit in (MSB first)
              bitcnt <= bitcnt + 1;
            else  -- reading the last bit and going immediately to the ack state
              ctrl_tmp(8 * g_nof_ctrl_bytes - 1 downto 1) <= ctrl_tmp(8 * g_nof_ctrl_bytes - 2 downto 0);  -- shift the data to the left
              ctrl_tmp(0) <= sda_rx;  -- shift a new bit in (MSB first)
              if bytecnt < g_nof_ctrl_bytes - 1 then  -- first bytes
                bytecnt <= bytecnt + 1;
                current_state <= acknowledge;  -- acknowledge the successfull read of a byte
              else  -- last byte
                latch_ctrl <= '1';  -- latch at g_nof_ctrl_bytes-th byte (or a multiple of that, due to bytecnt wrap)
                bytecnt <= 0;  -- wrap byte count
                current_state <= acknowledge;  -- acknowledge also for the last byte
              end if;
            end if;
          when write_data => null;
          when acknowledge =>
            if rw = '0' then
              current_state <= read_data;  -- acknowledge state is one clock period active
            else
              current_state <= write_data;
            end if;
          when nacknowledge => null;
            --When the master addresses another slave, then the statemachine directly goes into reset state.
            --The slave always answers with ACK on a master write data, so the nacknowledge state is never
            --reached.
          when wacknowledge =>
            if sda_rx = '0' then
              current_state <= write_data;  -- write went OK, continue writing bytes to the master
            else
              current_state <= reset;  -- write failed, so abort the transfer
            end if;
          when wnacknowledge =>
            --NACK is used to signal the end of a master read access. The slave can not known whether
            --the write transfer to the master was succesful and it does not need to know anyway.
            if sda_rx = '1' then
              current_state <= reset;  -- last write went OK, no more bytes to write to the master
            else
              --By making current_state <= reset the CTRL register can only be read once in one transfer.
              --If more bytes are read then the master will see them as 0xFF (due to SDA pull up). By
              --making current_state <= write_data the CTRL register wraps if it is read more than once.
              --current_state <= reset;      --last write went OK, abort further transfer
              current_state <= write_data;  -- last write went OK, wrap and continue writing bytes to the master
            end if;
          when others =>
            current_state <= reset;
        end case;
      end if;

    elsif falling_edge(scl_rx) then

      -- Statemachine
      --   default assignments (others keep their value accross states during the access)
      sda_int        <= 'H';
      tri_en         <= '0';
      wbitcnt        <= 0;
      zeroedge_state <= current_state;
      if start = '1' then
        wbytecnt       <= 0;
        zeroedge_state <= reset;
      elsif stop = '1' then  -- only recognized if there occurs an SCL edge between I2C Stop and Start
        wbytecnt       <= 0;
        zeroedge_state <= reset;
      else
        case current_state is
          when reset => null;
          when read_addr => null;
          when read_data => null;
          when write_data =>
            if wbitcnt < 8 then
              tri_en <= '1';  -- enable tri-state buffer to write SDA
              if i_ctrl_reg(8 * g_nof_ctrl_bytes - 1 - (8 * wbytecnt + wbitcnt)) = '0' then
                sda_int <= '0';  -- copy one bit to SDA (MSB first), else default to 'H'
              end if;
              wbitcnt <= wbitcnt + 1;
            else
              if wbytecnt < g_nof_ctrl_bytes - 1 then  -- first bytes
                wbytecnt <= wbytecnt + 1;
                zeroedge_state <= wacknowledge;  -- wait till master acknowledges the successfull read of a byte
              else  -- last byte
                wbytecnt <= 0;  -- wrap byte count
                zeroedge_state <= wnacknowledge;  -- wait till master nacknowledges the successfull read of a byte
              end if;
            end if;
          when acknowledge =>
            tri_en <= '1';  -- enable tri-state buffer to write SDA
            sda_int <= '0';  -- acknowledge data
          when nacknowledge => null;
            --This state is never reached.
            --To answer an NACK the tri_en can remain '0', because the default SDA is 'H' due to the pull up.
          when wacknowledge => null;
          when wnacknowledge => null;
          when others =>
            zeroedge_state <= reset;
        end case;
      end if;
    end if;
  end process;

  --control the tri state buffer
  SDA <= sda_int when tri_en = '1' else 'Z';
end rtl;
