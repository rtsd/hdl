-------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;

entity unb2c_led is
  generic (
    g_design_name   : string  := "unb2c_led";
    g_design_note   : string  := "UNUSED";
    g_technology    : natural := c_tech_arria10_e2sg;
    g_sim           : boolean := false;  -- Overridden by TB
    g_sim_unb_nr    : natural := 0;
    g_sim_node_nr   : natural := 0;
    g_stamp_date    : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time    : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn     : natural := 0;  -- SVN revision    -- set by QSF
    g_factory_image : boolean := true
  );
  port (
    CLK          : in    std_logic;
    ETH_CLK      : in    std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2c_board_aux.testio_w - 1 downto 0);
    QSFP_LED     : out   std_logic_vector(c_unb2c_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end unb2c_led;

architecture str of unb2c_led is
  -- Firmware version x.y
  constant c_fw_version         : t_unb2c_board_fw_version := (2, 0);
  constant c_reset_len          : natural := 40000;  -- 4;  -- >= c_meta_delay_len from common_pkg
  constant c_mm_clk_freq        : natural := c_unb2c_board_mm_clk_freq_50M;

  -- System
  signal divclk                 : std_logic;
  signal mm_locked              : std_logic;

  signal clk125                 : std_logic := '1';
  signal clk100                 : std_logic := '1';
  signal clk50                  : std_logic := '1';

  signal cs_sim                 : std_logic;
  signal xo_ethclk              : std_logic;
  signal xo_rst                 : std_logic;
  signal xo_rst_n               : std_logic;
  signal mm_rst                 : std_logic;

  signal pulse_10Hz             : std_logic;
  signal pulse_10Hz_extended    : std_logic;
  signal mm_pulse_ms            : std_logic;
  signal mm_pulse_s             : std_logic;

  signal led_toggle             : std_logic;
  signal led_flash              : std_logic;
  signal led_flash_red          : std_logic;
  signal led_flash_green        : std_logic;

  -- QSFP leds
  signal qsfp_green_led_arr     : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr       : std_logic_vector(c_unb2c_board_tr_qsfp.nof_bus - 1 downto 0);

  constant c_CNT_1HZ : natural := 20000000;
  signal r_CNT_1HZ : natural range 0 to c_CNT_1HZ;
  signal r_TOGGLE_1HZ   : std_logic := '0';
  signal leddiv   : std_logic := '0';
  signal clk200 : std_logic;
begin
  xo_rst_n   <= not xo_rst;

  -----------------------------------------------------------------------------
  -- xo_ethclk = ETH_CLK
  -----------------------------------------------------------------------------

  divclk <= clk200;  -- ETH_CLK(1);   -- use the ETH_CLK pin as xo_clk

  leddiv <= r_TOGGLE_1HZ;

  p_led : process (divclk) is
  begin
    if rising_edge(divclk) then
      if r_CNT_1HZ = c_CNT_1HZ - 1 then  -- -1, since counter starts at 0
        r_TOGGLE_1HZ <= not r_TOGGLE_1HZ;
        r_CNT_1HZ    <= 0;
      else
        r_CNT_1HZ <= r_CNT_1HZ + 1;
      end if;
    end if;
  end process p_led;

  -- by using the fpll, the CLKUSR is used for calibration. So in case fpll does not work, check CLKUSR

  u_unb2c_board_clk200_pll : entity unb2c_board_lib.unb2c_board_clk200_pll
  generic map (
    g_use_fpll   => true,  -- FALSE, -- switch fpll or fixedpll
    g_technology => g_technology
  )
  port map (
    arst       => xo_rst,
    clk200     => CLK,
    st_clk200  => clk200
  );

  xo_ethclk <= ETH_CLK(0);  -- use the ETH_CLK pin as xo_clk

  u_common_areset_xo : entity common_lib.common_areset
  generic map (
    g_rst_level => '1',  -- power up default will be inferred in FPGA
    g_delay_len => c_reset_len
  )
  port map (
    in_rst    => '0',  -- release reset after some clock cycles
    clk       => xo_ethclk,
    out_rst   => xo_rst
  );

  u_unb2c_board_clk125_pll : entity unb2c_board_lib.unb2c_board_clk125_pll
  generic map (
    g_use_fpll   => true,  -- FALSE, -- switch fpll or fixedpll
    g_technology => g_technology
  )
  port map (
    arst       => xo_rst,
    clk125     => xo_ethclk,
    c1_clk50   => clk50,
    pll_locked => mm_locked
  );

  u_unb2c_board_node_ctrl : entity unb2c_board_lib.unb2c_board_node_ctrl
  generic map (
    g_pulse_us => c_mm_clk_freq / (10**6)  -- nof system clock cycles to get us period, equal to system clock frequency / 10**6
  )
  port map (
    -- MM clock domain reset
    mm_clk      => clk50,
    mm_locked   => mm_locked,
    mm_rst      => mm_rst,
    -- WDI extend
    mm_wdi_in   => mm_pulse_s,
    -- Pulses
    mm_pulse_us => OPEN,
    mm_pulse_ms => mm_pulse_ms,
    mm_pulse_s  => mm_pulse_s  -- could be used to toggle a LED
  );

  ------------------------------------------------------------------------------
  -- Toggle red LED when unb2c_minimal is running, green LED for other designs.
  ------------------------------------------------------------------------------
  led_flash_red   <= sel_a_b(g_factory_image = true,  led_flash, '0');
  led_flash_green <= sel_a_b(g_factory_image = false, led_flash, '0');

  u_extend : entity common_lib.common_pulse_extend
  generic map (
    g_extend_w => 22  -- (2^22) / 50e6 = 0.083886 th of 1 sec
  )
  port map (
    rst     => mm_rst,
    clk     => clk50,
    p_in    => mm_pulse_s,
    ep_out  => led_flash
  );

  -- Red LED control
  TESTIO(c_unb2c_board_testio_led_red)   <= led_flash_red;

  -- Green LED control
  TESTIO(c_unb2c_board_testio_led_green) <= led_flash_green;

  u_common_pulser_10Hz : entity common_lib.common_pulser
  generic map (
    g_pulse_period => 100,
    g_pulse_phase  => 100 - 1
  )
  port map (
    rst            => mm_rst,
    clk            => clk50,
    clken          => '1',
    pulse_en       => mm_pulse_ms,
    pulse_out      => pulse_10Hz
  );

  u_extend_10Hz : entity common_lib.common_pulse_extend
  generic map (
    g_extend_w => 21  -- (2^21) / 50e6 = 0.041943 th of 1 sec
  )
  port map (
    rst     => mm_rst,
    clk     => clk50,
    p_in    => pulse_10Hz,
    ep_out  => pulse_10Hz_extended
  );

  u_toggle : entity common_lib.common_toggle
  port map (
    rst         => mm_rst,
    clk         => clk50,
    in_dat      => mm_pulse_s,
    out_dat     => led_toggle
  );

  QSFP_LED(2)  <= pulse_10Hz_extended;

  QSFP_LED(6)  <= led_toggle;
  QSFP_LED(7)  <= not led_toggle;
  QSFP_LED(10) <= leddiv;
  QSFP_LED(11) <= not leddiv;

  -- red LEDs on bottom
  QSFP_LED(1)  <= clk200;
  QSFP_LED(5)  <= ETH_CLK(0);
  QSFP_LED(9)  <= ETH_CLK(1);
end str;
