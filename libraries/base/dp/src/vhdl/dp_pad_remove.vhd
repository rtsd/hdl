--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Remove padding octects from the head of a padded frame
-- Description:
--   This dp_pad_remove merely uses dp_split. It throws away the head and it
--   passes on the tail.
-- Remark:
-- . Ready latency (RL) = 1
-- . Reverse operation is dp_pad_insert

entity dp_pad_remove is
  generic (
    g_data_w          : natural := 64;
    g_symbol_w        : natural := 8;  -- g_data_w/g_symbol_w must be integer
    g_nof_padding     : natural := 6;  -- must be >= 0 (because dp_split can support 0, although using g_nof_padding = 0 is void)
    g_internal_bypass : boolean := false  -- can be used to eliminate external GENERATE-statements/wiring
  );
  port (
    rst     : in  std_logic;
    clk     : in  std_logic;
    -- ST sinks
    snk_out : out t_dp_siso;
    snk_in  : in  t_dp_sosi;
    -- ST source
    src_in  : in  t_dp_siso;
    src_out : out t_dp_sosi
  );
end dp_pad_remove;

architecture str of dp_pad_remove is
  -- element (0) gets the head, element (1) gets the tail
  signal split_siso_arr : t_dp_siso_arr(0 to 1);
  signal split_sosi_arr : t_dp_sosi_arr(0 to 1);
begin
  no_bypass : if g_internal_bypass = false generate
    -- Remove the padding octets from the head and output the tail
    split_siso_arr(0) <= c_dp_siso_rdy;
    split_siso_arr(1) <= src_in;

    src_out <= split_sosi_arr(1);

    u_split : entity work.dp_split  -- RL = 1
    generic map (
      g_data_w        => g_data_w,
      g_symbol_w      => g_symbol_w,
      g_nof_symbols   => g_nof_padding
    )
    port map (
      rst         => rst,
      clk         => clk,
      -- ST sinks
      snk_out     => snk_out,
      snk_in      => snk_in,
      -- ST source
      src_in_arr  => split_siso_arr,
      src_out_arr => split_sosi_arr
    );
  end generate;

  gen_bypass : if g_internal_bypass = true generate
    src_out <= snk_in;
    snk_out <= src_in;
  end generate;
end str;
