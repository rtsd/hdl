-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Test statistics offload with "ethernet packet statistics" in wave window only
-- Usage:
--   > as 7    # default
--   > as 14   # for detailed debugging of Eth sim tx link
--   > run -a  # takes about 250 us --> c_eth_runtime_timeout
--
-- Remark:
-- - Only verify that there is statistics offload.
-- - The statistics data gets undefined, due to that the c_nof_block_per_sync
--   is too short to offload 12 SST packets via 1GbE. This causes that new
--   data is available before the old data has been offloaded. Therefore use
--   c_eth_check_nof_packets = 1 instead of S_pn = 12.
-------------------------------------------------------------------------------
library IEEE, common_lib, unb2c_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, eth_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;
  use IEEE.math_real.all;
  use common_lib.common_pkg.all;
  use common_lib.tb_common_pkg.all;
  use common_lib.common_str_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use mm_lib.mm_file_pkg.all;
  use mm_lib.mm_file_unb_pkg.all;
  use diag_lib.diag_pkg.all;
  use unb2c_board_lib.unb2c_board_pkg.all;
  use wpfb_lib.wpfb_pkg.all;
  use eth_lib.eth_pkg.all;
  use lofar2_sdp_lib.sdp_pkg.all;

entity tb_lofar2_unb2c_sdp_station is
end tb_lofar2_unb2c_sdp_station;

architecture tb of tb_lofar2_unb2c_sdp_station is
  constant c_sim                          : boolean := true;
  constant c_unb_nr                       : natural := 0;  -- UniBoard 0
  constant c_node_nr                      : natural := 0;
  constant c_id                           : std_logic_vector(7 downto 0) := "00000000";
  constant c_version                      : std_logic_vector(1 downto 0) := "00";

  constant c_eth_clk_period               : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period               : time := 5 ns;
  constant c_bck_ref_clk_period           : time := 5 ns;

  constant c_tb_clk_period                : time := 100 ps;  -- use fast tb_clk to speed up M&C

  constant c_nof_block_per_sync           : natural := 16;
  constant c_nof_clk_per_sync             : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_pps_period                   : natural := c_nof_clk_per_sync;
  constant c_wpfb_sim                     : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands,
                                                                                       c_nof_block_per_sync);

  -- WG
  constant c_bsn_start_wg                 : natural := 2;  -- start WG at this BSN to instead of some BSN, to avoid
                                                           -- mismatches in exact expected data values
  constant c_ampl_sp_0                    : natural := 2**(c_sdp_W_adc - 1) / 2;  -- in number of lsb
  constant c_subband_sp_0                 : real := 102.0;  -- Select subband at index 102 = 102/1024 * 200MHz =
                                                            -- 19.921875 MHz

  -- 1GbE output
  constant c_eth_check_nof_packets        : natural := 1;
  constant c_eth_runtime_timeout          : time := 300 us;

  -- MM
  constant c_mm_file_reg_bsn_source_v2    : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_bsn_scheduler_wg : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_reg_stat_enable_sst  : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_STAT_ENABLE_SST";
  constant c_mm_file_reg_stat_hdr_dat_sst : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_STAT_HDR_DAT_SST";

  -- Tb
  signal tb_end               : std_logic := '0';
  signal sim_done             : std_logic := '0';
  signal eth_done             : std_logic := '0';
  signal verify_done          : std_logic := '0';
  signal tb_clk               : std_logic := '0';

  -- WG
  signal current_bsn_wg       : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- DUT
  signal ext_clk              : std_logic := '0';
  signal pps                  : std_logic := '0';
  signal ext_pps              : std_logic := '0';
  signal pps_rst              : std_logic := '0';

  signal WDI                  : std_logic;
  signal INTA                 : std_logic;
  signal INTB                 : std_logic;

  signal eth_clk_slv          : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0) := (others => '0');
  signal eth_txp_slv          : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);
  signal eth_rxp_slv          : std_logic_vector(c_unb2c_board_nof_eth - 1 downto 0);

  -- back transceivers
  signal JESD204B_SERIAL_DATA : std_logic_vector(c_sdp_S_pn - 1 downto 0);
  signal JESD204B_REFCLK      : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref      : std_logic;
  signal jesd204b_sync_n      : std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);
begin
  -- System setup
  ext_clk <= (not ext_clk) or tb_end after c_ext_clk_period / 2;  -- External clock (200 MHz)

  -- Ethernet ref clock (125 MHz)
  eth_clk_slv(0) <= (not eth_clk_slv(0)) or tb_end after c_eth_clk_period / 2;
  eth_clk_slv(1) <= '0';  -- not used

  -- JESD sample clock (200MHz)
  JESD204B_REFCLK <= (not JESD204B_REFCLK) or tb_end after c_bck_ref_clk_period / 2;

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  -- External PPS
  proc_common_gen_pulse(10, c_pps_period, '1', pps_rst, ext_clk, pps);
  jesd204b_sysref <= pps;
  ext_pps <= pps;

  -- >> DUT <<
  u_lofar_unb2c_sdp_station : entity work.lofar2_unb2c_sdp_station
  generic map (
    g_design_name            => "lofar2_unb2c_sdp_station_bf",
    g_design_note            => "",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr,
    g_wpfb                   => c_wpfb_sim,
    g_scope_selected_subband => natural(c_subband_sp_0)
  )
  port map (
    -- GENERAL
    CLK                  => ext_clk,
    PPS                  => pps,
    WDI                  => WDI,
    INTA                 => INTA,
    INTB                 => INTB,

    -- Others
    VERSION              => c_version,
    ID                   => c_id,
    TESTIO               => open,

    -- 1GbE Control Interface
    ETH_CLK              => eth_clk_slv,
    ETH_SGIN             => eth_rxp_slv,
    ETH_SGOUT            => eth_txp_slv,

    -- LEDs
    QSFP_LED             => open,

    -- back transceivers
    JESD204B_SERIAL_DATA => JESD204B_SERIAL_DATA,
    JESD204B_REFCLK      => JESD204B_REFCLK,

    -- jesd204b syncronization signals
    JESD204B_SYSREF      => jesd204b_sysref,
    JESD204B_SYNC_N      => jesd204b_sync_n
  );

  -----------------------------------------------------------------------------
  -- Stimuli
  --   MM slave accesses via file IO
  tb_clk <= (not tb_clk) or tb_end after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn                       : natural;
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    proc_common_wait_until_hi_lo(ext_clk, ext_pps);

    -- Enable BS
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 3,                   0, tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 2,                   1, tb_clk);  -- Init BSN = 0
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 1,  c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0,        16#00000003#, tb_clk);  -- Enable BS at PPS

    -- Enable WG
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 1, integer(0.0 * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 2, integer(c_subband_sp_0 * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, 3, integer(real(c_ampl_sp_0) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;
    v_bsn := c_bsn_start_wg;
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, v_bsn, tb_clk);  -- first write low then high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,     0, tb_clk);  -- assume v_bsn < 2**31-1

    -- Wait for ADUH monitor to have filled with WG data
    wait for c_sdp_T_sub * c_sdp_N_taps;
    wait for c_sdp_T_sub * 2;

    -- Offload enable
    mmf_mm_bus_wr(c_mm_file_reg_stat_enable_sst, 0, 1, tb_clk);

    -- End Simulation
    proc_common_wait_until_high(ext_clk, eth_done);
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, eth_done, tb_end);
    wait;
  end process;

  -- >> Verify proper DUT output using Ethernet packet statistics <<
  u_eth_statistics : entity eth_lib.eth_statistics
  generic map (
    g_runtime_nof_packets => c_eth_check_nof_packets,
    g_runtime_timeout     => c_eth_runtime_timeout
  )
  port map (
    eth_serial_in => eth_txp_slv(0),
    tb_end        => eth_done
  );
end tb;
