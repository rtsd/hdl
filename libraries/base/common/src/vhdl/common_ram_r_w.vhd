-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use work.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_ram_r_w is
  generic (
    g_technology : natural := c_tech_select_default;
    g_ram        : t_c_mem := c_mem_ram;
    g_init_file  : string := "UNUSED";
    g_true_dual_port : boolean := true
  );
  port (
    rst       : in  std_logic := '0';
    clk       : in  std_logic;
    clken     : in  std_logic := '1';
    wr_en     : in  std_logic := '0';
    wr_adr    : in  std_logic_vector(g_ram.adr_w - 1 downto 0) := (others => '0');
    wr_dat    : in  std_logic_vector(g_ram.dat_w - 1 downto 0) := (others => '0');
    rd_en     : in  std_logic := '1';
    rd_adr    : in  std_logic_vector(g_ram.adr_w - 1 downto 0);
    rd_dat    : out std_logic_vector(g_ram.dat_w - 1 downto 0);
    rd_val    : out std_logic
  );
end common_ram_r_w;

architecture str of common_ram_r_w is
begin
  -- Use port a only for write
  -- Use port b only for read

  u_rw_rw : entity work.common_ram_rw_rw
  generic map (
    g_technology => g_technology,
    g_ram        => g_ram,
    g_init_file  => g_init_file,
    g_true_dual_port => g_true_dual_port
  )
  port map (
    rst       => rst,
    clk       => clk,
    clken     => clken,
    wr_en_a   => wr_en,
    wr_en_b   => '0',
    wr_dat_a  => wr_dat,
    --wr_dat_b  => (OTHERS=>'0'),
    adr_a     => wr_adr,
    adr_b     => rd_adr,
    rd_en_a   => '0',
    rd_en_b   => rd_en,
    rd_dat_a  => OPEN,
    rd_dat_b  => rd_dat,
    rd_val_a  => OPEN,
    rd_val_b  => rd_val
  );
end str;
