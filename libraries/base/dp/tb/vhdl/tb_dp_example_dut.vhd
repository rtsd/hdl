-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Test bench without DUT to show stimuli and verification
-- Description:
--
-- . Block diagram:
--   The p_stimuli_st generates as stream of packets with counter data. The
--   verification procedures check that then packets are received properly
--   according to the streaming interfaces requirements. The stimuli and
--   verification procedures come from tb_dp_pkg.vhd.
--
--     p_stimuli_st                               proc_dp_verify_*()
--
--       stimuli_src_in                             verify_snk_out
--       stimuli_src_out                            verify_snk_in
--               |   ____________________________    |
--               |  | DUT                        |   |
--               |  |                            |   |
--            <-----| dut_snk_out     dut_src_in |<-----
--            ----->| dut_snk_in      dut_src_out|----->
--                  |____________________________|
--
--   The verification can detect missing data by check that the received data
--   is incrementing ok during the entire test. See eg. tb_dp_pipeline,
--   tb_dp_pipeline_ready.
--   The verification relies on the counter data remaining in tact. There is a
--   variant of proc_dp_verify_data() that can accept an data increment other
--   than +1, see eg. tb_dp_packet_merge. This is useful in case the DUT
--   removes data from the stream.
--
--   For DUT that modify the data stream it is often possible and useful to
--   also create an inverse function of the DUT that undo's the modification,
--   see eg. tb2_dp_mux, tb_dp_distribute, tb_uth.
--
--     p_stimuli_st                               proc_dp_verify_*()
--
--       stimuli_src_in                             verify_snk_out
--       stimuli_src_out                            verify_snk_in
--               |   _________          _________   |
--               |  |         |        |         |  |
--            <-----| inverse |<-------|   DUT   |<-----
--            ----->|   DUT   |------->|         |----->
--                  |_________|        |_________|
--
--   The p_stimuli_st and proc_dp_verify_*() can also be generated multiple
--   times in parallel to fit DUT which have multiple input or output streams,
--   see eg. tb_dp_bsn_align.
--
-- . Flow control:
--   The tb can also verify the flow control. Via g_flow_control_stimuli the
--   data valid output rate of the source can be controlled. The backpressure
--   flow control from the verification is controlled by g_flow_control_verify.
--   Typically start with noflow control, so both e_active. Then set one to
--   e_random and then both to e_random. The e_pulse flow control option is
--   extra and not always useful.
--
-- . Data symbols:
--   The proc_dp_gen_block_data() can also generate data with >1 symbols per
--   data word. The counter then counts per symbol and the verification then
--   needs to be done with proc_dp_verify_symbols(), see eg. tb_dp_latency_fifo,
--   tb_dp_fifo_dc_mixed_widths.
--
-- . Other sosi data fields (BSN, channel, empty)
--   The transport of the other sosi fields can also be verified using counter
--   data, similar as for the sosi.data, but instead only active at the sop or
--   eop. See eg. tb_dp_pad_insert_remove.
--
-- . Sosi control fields.
--   There are also dedicated procedures to verify the sop, eop and sync.
--
-- . Testbench result
--   If no errors are reported then the testbench has ran ok. The
--   proc_dp_verify_value() can be used to verify that the stimuli have been
--   applied at all. The verify_en* signal can be used to ensure that the
--   verification does not start too soon.
--
-- . Multi testbench
--   This tb_dp_example_dut can be instantiated multiple times is a multi
--   tb_tb_dp_example_dut test bench to verify the DUT for different
--   parameter configurations. When the tb_tb_dp_example_dut runs ok then it
--   can be added to the overall DP library regression testbench in
--   tb_tb_tb_dp_backpressure.vhd.
--
-- Usage:
-- > as 10
-- > run -all
--
-- Remarks:
-- . The tb can also be used to verify components without flow control by
--   using g_flow_control_verify=e_active. Using g_flow_control_stimuli=
--   e_pulse can then be useful to model a source with a certain data valid
--   duty cycle. By using g_flow_control_stimuli=e_active and g_pkt_gap>0 it
--   is possible to model a source with data valid during the packet (ie no
--   invalid between sop and eop) and a data invalid gap between the packets.
--   For components that do support snk_in.valid and src_out.ready flow
--   control it is typically sufficient to verify two cases: both with
--   e_active and both with e_random. Verifing all 9 possible combinations
--   of flow control like in this example tb is typically not necessary or
--   may take too much time to simulate.
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_example_dut is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_random;  -- always active, random or pulse flow control
    -- specific
    g_no_dut                 : boolean := false;
    g_dat_w                  : natural := 16;
    g_nof_repeat             : natural := 5;
    g_pkt_len                : natural := 5;
    g_pkt_gap                : natural := 4
  );
end tb_dp_example_dut;

architecture tb of tb_dp_example_dut is
  -- dp_stream_stimuli
  constant c_stimuli_pulse_active     : natural := 3;
  constant c_stimuli_pulse_period     : natural := 7;

  constant c_data_init                : natural := 0;
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_err_init                 : natural := 247;
  constant c_channel_init             : natural := 5;  -- fixed

  -- dp_stream_verify
  constant c_verify_pulse_active      : natural := 1;
  constant c_verify_pulse_period      : natural := 5;

  constant c_data_max                 : unsigned(g_dat_w - 1 downto 0) := (others => '1');
  constant c_dsp_max                  : unsigned(g_dat_w - 1 downto 0) := (others => '1');

  --CONSTANT c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_rst;  -- default 0 is no wrap
  constant c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := TO_DP_SOSI_UNSIGNED('0', '0', '0', '0', c_data_max, c_dsp_max, c_dsp_max, c_unsigned_0, c_unsigned_0, c_unsigned_0, c_unsigned_0);
  constant c_verify_snk_in_cnt_gap    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_ones;  -- default only accept increment +1

  -- both
  constant c_sync_period              : natural := 10;
  constant c_sync_offset              : natural := 7;

  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';
  signal tb_end                     : std_logic := '0';

  signal stimuli_src_in             : t_dp_siso;
  signal stimuli_src_out            : t_dp_sosi;
  signal stimuli_src_out_data       : std_logic_vector(g_dat_w - 1 downto 0);

  signal verify_snk_in_enable       : t_dp_sosi_sl := c_dp_sosi_sl_rst;
  signal last_snk_in                : t_dp_sosi;
  signal last_snk_in_evt            : std_logic;
  signal verify_last_snk_in_evt     : t_dp_sosi_sl := c_dp_sosi_sl_rst;

  signal dut_snk_out                : t_dp_siso;
  signal dut_snk_in                 : t_dp_sosi;

  signal dut_src_in                 : t_dp_siso;
  signal dut_src_out                : t_dp_sosi;

  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal verify_snk_in_data         : std_logic_vector(g_dat_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  u_dp_stream_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 15,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_stimuli_pulse_active,
    g_pulse_period   => c_stimuli_pulse_period,
    g_flow_control   => g_flow_control_stimuli,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_data_init      => c_data_init,
    g_bsn_init       => c_bsn_init,
    g_err_init       => c_err_init,
    g_channel_init   => c_channel_init,
    -- specific
    g_in_dat_w       => g_dat_w,
    g_nof_repeat     => g_nof_repeat,
    g_pkt_len        => g_pkt_len,
    g_pkt_gap        => g_pkt_gap
  )
  port map (
    rst                 => rst,
    clk                 => clk,

    -- Generate stimuli
    src_in              => stimuli_src_in,
    src_out             => stimuli_src_out,

    -- End of stimuli
    last_snk_in         => last_snk_in,  -- expected verify_snk_in after end of stimuli
    last_snk_in_evt     => last_snk_in_evt,  -- trigger verify to verify the last_snk_in
    tb_end              => tb_end  -- signal end of tb as far as this dp_stream_stimuli is concerned
  );

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Select fields that need to be verified
  -- . during the test
  verify_snk_in_enable.sync    <= '1';  -- or '0'
  verify_snk_in_enable.bsn     <= '1';
  verify_snk_in_enable.data    <= '1';
  verify_snk_in_enable.re      <= '0';
  verify_snk_in_enable.im      <= '0';
  verify_snk_in_enable.valid   <= '1';
  verify_snk_in_enable.sop     <= '1';
  verify_snk_in_enable.eop     <= '1';
  verify_snk_in_enable.empty   <= '0';
  verify_snk_in_enable.channel <= '1';
  verify_snk_in_enable.err     <= '1';

  -- . after the test
  verify_last_snk_in_evt.sync    <= last_snk_in_evt;
  verify_last_snk_in_evt.bsn     <= last_snk_in_evt;
  verify_last_snk_in_evt.data    <= last_snk_in_evt;
  verify_last_snk_in_evt.re      <= '0';
  verify_last_snk_in_evt.im      <= '0';
  verify_last_snk_in_evt.valid   <= last_snk_in_evt;
  verify_last_snk_in_evt.sop     <= last_snk_in_evt;
  verify_last_snk_in_evt.eop     <= last_snk_in_evt;
  verify_last_snk_in_evt.empty   <= '0';
  verify_last_snk_in_evt.channel <= last_snk_in_evt;
  verify_last_snk_in_evt.err     <= last_snk_in_evt;

  u_dp_stream_verify : entity work.dp_stream_verify
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 14,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_verify_pulse_active,
    g_pulse_period   => c_verify_pulse_period,
    g_flow_control   => g_flow_control_verify,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_snk_in_cnt_max => c_verify_snk_in_cnt_max,
    g_snk_in_cnt_gap => c_verify_snk_in_cnt_gap,
    -- specific
    g_in_dat_w       => g_dat_w,
    g_pkt_len        => g_pkt_len
  )
  port map (
    rst                        => rst,
    clk                        => clk,

    -- Verify data
    snk_out                    => verify_snk_out,
    snk_in                     => verify_snk_in,

    -- During stimuli
    verify_snk_in_enable       => verify_snk_in_enable,  -- enable verify to verify that the verify_snk_in fields are incrementing

    -- End of stimuli
    expected_snk_in            => last_snk_in,  -- expected verify_snk_in after end of stimuli
    verify_expected_snk_in_evt => verify_last_snk_in_evt  -- trigger verify to verify the last_snk_in
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  -- Connect stimuli input stream to DUT sink
  stimuli_src_in <= dut_snk_out;
  dut_snk_in     <= stimuli_src_out;

  no_dut : if g_no_dut = true generate
    dut_snk_out <= dut_src_in;
    dut_src_out <= dut_snk_in;
  end generate;

  -- DUT function
  gen_dut : if g_no_dut = false generate
    u_dut : entity work.dp_example_dut
    port map (
      rst       => rst,
      clk       => clk,

      snk_out   => dut_snk_out,
      snk_in    => dut_snk_in,

      src_in    => dut_src_in,
      src_out   => dut_src_out
    );
  end generate;

  -- Connect DUT source output stream to verification
  dut_src_in     <= verify_snk_out;
  verify_snk_in  <= dut_src_out;

  ------------------------------------------------------------------------------
  -- Auxiliary
  ------------------------------------------------------------------------------

  -- Map to slv to ease monitoring in wave window
  stimuli_src_out_data <= stimuli_src_out.data(g_dat_w - 1 downto 0);
  verify_snk_in_data   <= verify_snk_in.data(g_dat_w - 1 downto 0);
end tb;
