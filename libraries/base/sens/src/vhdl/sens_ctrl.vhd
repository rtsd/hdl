-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use i2c_lib.i2c_smbus_pkg.all;
use i2c_lib.i2c_dev_max1617_pkg.all;
use i2c_lib.i2c_dev_max6652_pkg.all;

entity sens_ctrl is
  generic (
    g_clk_cnt_w : natural;
    g_evt_dat_w : natural;
    g_temp_high : natural := 127
  );
  port (
    clk         : in std_logic;
    rst         : in std_logic;
    out_dat     : out std_logic_vector(7 downto 0);
    out_val     : out std_logic;
    in_dat      : in  std_logic_vector(7 downto 0);
    in_val      : in  std_logic;
    in_err      : in  std_logic;
    in_ack      : in  std_logic;
    evt_val_reg : out std_logic;
    evt_dat_reg : out std_logic_vector(g_evt_dat_w - 1 downto 0)
  );
end entity;

architecture rtl of sens_ctrl is
  constant ADR_MAX6652     : natural := MAX6652_ADR_GND;
  constant ADR_MAX1617_BP  : natural := MAX1617_ADR_MID_MID;
  constant ADR_MAX1617_AP0 : natural := MAX1617_ADR_LOW_LOW;
  constant ADR_MAX1617_AP1 : natural := MAX1617_ADR_LOW_HIGH;
  constant ADR_MAX1617_AP2 : natural := MAX1617_ADR_HIGH_LOW;
  constant ADR_MAX1617_AP3 : natural := MAX1617_ADR_HIGH_HIGH;

  type SEQUENCE is array (natural range <>) of natural;

  constant SEQ : SEQUENCE := (
    SMBUS_C_NOP,
    SMBUS_READ_BYTE,  ADR_MAX6652,     MAX6652_REG_READ_VIN_2_5,
    SMBUS_READ_BYTE,  ADR_MAX6652,     MAX6652_REG_READ_VIN_3_3,
    SMBUS_READ_BYTE,  ADR_MAX6652,     MAX6652_REG_READ_VCC,
    SMBUS_READ_BYTE,  ADR_MAX6652,     MAX6652_REG_READ_TEMP,
    SMBUS_READ_BYTE,  ADR_MAX1617_BP,  MAX1617_CMD_READ_REMOTE_TEMP,
--  For debugging, use AP temp fields in RSR to read other info from the sensor, e.g.:
--     SMBUS_READ_BYTE , ADR_MAX1617_BP , MAX1617_CMD_READ_STATUS,
--     SMBUS_READ_BYTE , ADR_MAX1617_BP , MAX1617_CMD_READ_CONFIG,
--     SMBUS_READ_BYTE , ADR_MAX1617_BP , MAX1617_CMD_READ_REMOTE_HIGH,
--     SMBUS_READ_BYTE , ADR_MAX1617_BP , MAX1617_CMD_READ_REMOTE_LOW,
    SMBUS_READ_BYTE,  ADR_MAX1617_AP0, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_READ_BYTE,  ADR_MAX1617_AP1, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_READ_BYTE,  ADR_MAX1617_AP2, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_READ_BYTE,  ADR_MAX1617_AP3, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_WRITE_BYTE, ADR_MAX6652,     MAX6652_REG_CONFIG,           MAX6652_CONFIG_LINE_FREQ_SEL + MAX6652_CONFIG_START,
    SMBUS_WRITE_BYTE, ADR_MAX1617_BP,  MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP0, MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP1, MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP2, MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP3, MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_BP,  MAX1617_CMD_WRITE_REMOTE_HIGH, g_temp_high,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP0, MAX1617_CMD_WRITE_REMOTE_HIGH, g_temp_high,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP1, MAX1617_CMD_WRITE_REMOTE_HIGH, g_temp_high,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP2, MAX1617_CMD_WRITE_REMOTE_HIGH, g_temp_high,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP3, MAX1617_CMD_WRITE_REMOTE_HIGH, g_temp_high,
    SMBUS_C_NOP
  );

  signal seq_start       : std_logic;  -- The I2C sequence to read out the sensors is started when clk_cnt = 0
  signal nxt_seq_start   : std_logic;

  signal seq_cnt         : natural range 0 to SEQ'high;
  signal nxt_seq_cnt     : natural;

  signal clk_cnt         : std_logic_vector(g_clk_cnt_w - 1 downto 0);
  signal nxt_clk_cnt     : std_logic_vector(clk_cnt'range);

  signal evt_val         : std_logic;
  signal nxt_evt_val     : std_logic;
  signal evt_dat         : std_logic_vector(evt_dat_reg'range);
  signal nxt_evt_dat     : std_logic_vector(evt_dat_reg'range);
  signal i_evt_dat_reg   : std_logic_vector(evt_dat_reg'range);
  signal nxt_evt_dat_reg : std_logic_vector(evt_dat_reg'range);
begin
  evt_dat_reg <= i_evt_dat_reg;

  regs: process(rst, clk)
  begin
    if rst = '1' then
      seq_start      <= '0';
      seq_cnt        <= 0;
      clk_cnt        <= (others => '0');
      evt_val        <= '0';
      evt_val_reg    <= '0';
      evt_dat        <= (others => '0');
      i_evt_dat_reg  <= (others => '0');
    elsif rising_edge(clk) then
      seq_start      <= nxt_seq_start;
      seq_cnt        <= nxt_seq_cnt;
      clk_cnt        <= nxt_clk_cnt;
      evt_val        <= nxt_evt_val;
      evt_val_reg    <= evt_val;
      evt_dat        <= nxt_evt_dat;
      i_evt_dat_reg  <= nxt_evt_dat_reg;
    end if;
  end process;

  nxt_clk_cnt <= std_logic_vector(unsigned(clk_cnt) + 1);

  nxt_seq_start <= '1' when unsigned(clk_cnt) = 0 else '0';

  p_seq_cnt : process(seq_cnt, seq_start, in_ack)
  begin
    nxt_seq_cnt <= seq_cnt;
    if seq_start = '1' then
      nxt_seq_cnt <= 0;
    elsif seq_cnt < SEQ'high and in_ack = '1' then
      nxt_seq_cnt <= seq_cnt + 1;
    end if;
  end process;

  out_dat <= std_logic_vector(to_unsigned(SEQ(seq_cnt), 8));
  out_val <= not rst when seq_cnt < SEQ'high else '0';

  p_evt_dat : process(evt_dat, seq_start, in_dat, in_val)
  begin
    nxt_evt_dat <= evt_dat;
    if seq_start = '1' then
      nxt_evt_dat <= (others => '0' );
    elsif in_val = '1' then
      nxt_evt_dat <= evt_dat(evt_dat'high - in_dat'length downto 0) & in_dat;
    end if;
  end process;

  nxt_evt_val <= '1' when seq_cnt = SEQ'high - 1 and in_ack = '1' else '0';

  nxt_evt_dat_reg <= evt_dat when evt_val = '1' else i_evt_dat_reg;
end rtl;
