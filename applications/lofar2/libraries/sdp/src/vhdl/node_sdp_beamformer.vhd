-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- . Implements the functionality of the Beamformer (BF) in the
--   LOFAR2 SDPFW design.
-- Description:
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, reorder_lib, st_lib, mm_lib, ring_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use ring_lib.ring_pkg.all;
use work.sdp_pkg.all;

entity node_sdp_beamformer is
  generic (
    -- Use no default, to force instance to set it
    g_sim                           : boolean := false;
    g_sim_sdp                       : t_sdp_sim := c_sdp_sim;
    g_beamset_id                    : natural;
    g_use_bdo_transpose             : boolean;
    g_nof_bdo_destinations_max      : natural;
    g_scope_selected_beamlet        : natural := 0;
    g_subband_raw_dat_w             : natural;  -- default: c_sdp_W_subband;
    g_subband_raw_fraction_w        : natural  -- default: 0
  );
  port (
    dp_clk        : in  std_logic;
    dp_rst        : in  std_logic;

    dp_bsn_source_new_interval  : in  std_logic;

    in_sosi_arr   : in  t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
    from_ri_sosi  : in  t_dp_sosi;
    to_ri_sosi    : out t_dp_sosi;
    bf_udp_sosi   : out t_dp_sosi;
    bf_udp_siso   : in  t_dp_siso;
    bst_udp_sosi  : out t_dp_sosi;
    bst_udp_siso  : in  t_dp_siso;

    mm_rst        : in  std_logic;
    mm_clk        : in  std_logic;

    ram_ss_ss_wide_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    ram_ss_ss_wide_miso       : out t_mem_miso;
    ram_bf_weights_mosi       : in  t_mem_mosi := c_mem_mosi_rst;
    ram_bf_weights_miso       : out t_mem_miso;
    reg_bf_scale_mosi         : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bf_scale_miso         : out t_mem_miso;
    reg_hdr_dat_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    reg_hdr_dat_miso          : out t_mem_miso;
    reg_bdo_destinations_copi : in  t_mem_copi := c_mem_mosi_rst;
    reg_bdo_destinations_cipo : out t_mem_cipo;
    reg_dp_xonoff_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_xonoff_miso        : out t_mem_miso;
    ram_st_bst_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    ram_st_bst_miso           : out t_mem_miso;
    reg_stat_enable_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_stat_enable_miso      : out t_mem_miso;
    reg_stat_hdr_dat_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_stat_hdr_dat_miso     : out t_mem_miso;
    reg_bsn_align_copi        : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_align_cipo        : out t_mem_cipo;

    reg_bsn_monitor_v2_bsn_align_input_copi  : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_bsn_align_input_cipo  : out t_mem_cipo;
    reg_bsn_monitor_v2_bsn_align_output_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_bsn_align_output_cipo : out t_mem_cipo;
    reg_bsn_monitor_v2_bst_offload_copi      : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_bst_offload_cipo      : out t_mem_cipo;
    reg_bsn_monitor_v2_beamlet_output_copi   : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_beamlet_output_cipo   : out t_mem_cipo;

    sdp_info  : in t_sdp_info;
    ring_info : in t_ring_info;
    gn_id     : in std_logic_vector(c_sdp_W_gn_id - 1 downto 0);

    -- beamlet data output
    bdo_eth_src_mac  : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    bdo_ip_src_addr  : in std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    bdo_udp_src_port : in std_logic_vector(c_network_udp_port_w - 1 downto 0);

    bdo_hdr_fields_out : out std_logic_vector(1023 downto 0);  -- Needed by nw_10GbE for PING/ARP

    -- beamlet statistics offload
    stat_eth_src_mac  : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    stat_ip_src_addr  : in std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    stat_udp_src_port : in std_logic_vector(c_network_udp_port_w - 1 downto 0)

  );
end node_sdp_beamformer;

architecture str of node_sdp_beamformer is
  -- Note that the sdp library contains src/python/sdp_hex.py to generate hex files.
  constant c_bf_select_file_prefix : string := "data/bf_unit_ss_wide";
  constant c_bf_weights_file_name  : string := sel_a_b(g_sim, "data/bf_unit_weights", "UNUSED");

  constant c_nof_masters : positive := 2;

  -- beamlet statistics
  signal ram_st_offload_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_offload_miso     : t_mem_miso := c_mem_miso_rst;

  signal master_mem_mux_mosi     : t_mem_mosi := c_mem_mosi_rst;
  signal master_mem_mux_miso     : t_mem_miso := c_mem_miso_rst;
  signal master_mosi_arr         : t_mem_mosi_arr(0 to c_nof_masters - 1) := (others => c_mem_mosi_rst);
  signal master_miso_arr         : t_mem_miso_arr(0 to c_nof_masters - 1) := (others => c_mem_miso_rst);

  signal bsel_sosi_arr           : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal local_bf_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal bf_sum_sosi             : t_dp_sosi := c_dp_sosi_rst;
  signal bf_out_sosi             : t_dp_sosi := c_dp_sosi_rst;
  signal mon_bf_udp_sosi         : t_dp_sosi := c_dp_sosi_rst;
  signal scope_local_bf_sosi_arr : t_dp_sosi_integer_arr(c_sdp_N_pol_bf - 1 downto 0);
  signal scope_bf_sum_sosi_arr   : t_dp_sosi_integer_arr(c_sdp_N_pol_bf - 1 downto 0);
  signal scope_bf_out_sosi_arr   : t_dp_sosi_integer_arr(c_sdp_N_pol_bf - 1 downto 0);
  signal beamlet_scale           : std_logic_vector(c_sdp_W_beamlet_scale-1 downto 0);

  signal rn_index                : natural range 0 to c_sdp_N_pn_max - 1 := 0;
  signal ref_sync                : std_logic;
begin
  -- Use register to ease timing closure.
  rn_index <= TO_UINT(SUB_UVEC(gn_id, ring_info.O_rn)) when rising_edge(dp_clk);
  ref_sync <= in_sosi_arr(0).sync when rising_edge(dp_clk);

  ---------------------------------------------------------------
  -- Beamlet Subband Select
  ---------------------------------------------------------------
  u_reorder_col_wide : entity reorder_lib.reorder_col_wide
  generic map (
    g_wb_factor          => c_sdp_P_pfb,  -- g_wb_factor is only used for number of parallel streams
    g_dsp_data_w         => g_subband_raw_dat_w,
    g_nof_ch_in          => c_sdp_N_sub * c_sdp_Q_fft,
    g_nof_ch_sel         => c_sdp_S_sub_bf * c_sdp_Q_fft,
    g_select_file_prefix => c_bf_select_file_prefix,
    g_use_complex        => true
  )
  port map(
    input_sosi_arr  => in_sosi_arr,
    output_sosi_arr => bsel_sosi_arr,

    ram_ss_ss_wide_mosi  => ram_ss_ss_wide_mosi,
    ram_ss_ss_wide_miso  => ram_ss_ss_wide_miso,

    mm_rst       => mm_rst,
    mm_clk       => mm_clk,
    dp_clk       => dp_clk,
    dp_rst       => dp_rst
  );

  ---------------------------------------------------------------
  -- Local BF
  ---------------------------------------------------------------
  u_sdp_beamformer_local : entity work.sdp_beamformer_local
  generic map (
    g_bf_weights_file_name => c_bf_weights_file_name,
    g_raw_dat_w            => g_subband_raw_dat_w,
    g_raw_fraction_w       => g_subband_raw_fraction_w
  )
  port map (
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    ram_bf_weights_mosi => ram_bf_weights_mosi,
    ram_bf_weights_miso => ram_bf_weights_miso,

    in_sosi_arr        => bsel_sosi_arr,
    out_sosi           => local_bf_sosi
  );

  ---------------------------------------------------------------
  -- Remote BF
  ---------------------------------------------------------------
  u_sdp_beamformer_remote : entity work.sdp_beamformer_remote
  port map (
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    rn_index           => rn_index,
    local_bf_sosi      => local_bf_sosi,
    from_ri_sosi       => from_ri_sosi,
    to_ri_sosi         => to_ri_sosi,
    bf_sum_sosi        => bf_sum_sosi,

    reg_bsn_align_copi => reg_bsn_align_copi,
    reg_bsn_align_cipo => reg_bsn_align_cipo,

    reg_bsn_monitor_v2_bsn_align_input_copi  => reg_bsn_monitor_v2_bsn_align_input_copi,
    reg_bsn_monitor_v2_bsn_align_input_cipo  => reg_bsn_monitor_v2_bsn_align_input_cipo,

    reg_bsn_monitor_v2_bsn_align_output_copi => reg_bsn_monitor_v2_bsn_align_output_copi,
    reg_bsn_monitor_v2_bsn_align_output_cipo => reg_bsn_monitor_v2_bsn_align_output_cipo
  );

  ---------------------------------------------------------------
  -- Scale Beamlets
  ---------------------------------------------------------------
  u_mms_dp_scale : entity dp_lib.mms_dp_scale
    generic map (
      g_complex_data       => true,
      g_complex_gain       => false,
      g_gain_init_re       => 2**(c_sdp_W_beamlet_scale-1),
      g_gain_w             => c_sdp_W_beamlet_scale,
      g_in_dat_w           => c_sdp_W_beamlet_sum,
      g_out_dat_w          => c_sdp_W_beamlet,
      g_lsb_w              => c_sdp_W_beamlet_scale-1,
      g_lsb_round          => true,
      g_lsb_round_clip     => false,
      g_msb_clip           => true,
      g_msb_clip_symmetric => true
    )
    port map(
      dp_clk         => dp_clk,
      dp_rst         => dp_rst,

      in_sosi        => bf_sum_sosi,
      out_sosi       => bf_out_sosi,

      mm_rst         => mm_rst,
      mm_clk         => mm_clk,

      reg_gain_re    => beamlet_scale,

      reg_gain_re_mosi => reg_bf_scale_mosi,
      reg_gain_re_miso => reg_bf_scale_miso
    );

  ---------------------------------------------------------------
  -- Beamlet Data Output (BDO)
  ---------------------------------------------------------------
  u_sdp_beamformer_output : entity work.sdp_beamformer_output
  generic map(
    g_beamset_id            => g_beamset_id,
    g_use_transpose         => g_use_bdo_transpose,
    g_nof_destinations_max  => g_nof_bdo_destinations_max
  )
  port map (
    mm_rst => mm_rst,
    mm_clk => mm_clk,
    dp_rst => dp_rst,
    dp_clk => dp_clk,

    in_sosi            => bf_out_sosi,
    out_sosi           => mon_bf_udp_sosi,
    out_siso           => bf_udp_siso,

    beamlet_scale      => beamlet_scale,
    sdp_info           => sdp_info,
    gn_id              => gn_id,

    eth_src_mac        => bdo_eth_src_mac,
    ip_src_addr        => bdo_ip_src_addr,
    udp_src_port       => bdo_udp_src_port,

    hdr_fields_out     => bdo_hdr_fields_out,

    reg_hdr_dat_mosi      => reg_hdr_dat_mosi,
    reg_hdr_dat_miso      => reg_hdr_dat_miso,
    reg_destinations_copi => reg_bdo_destinations_copi,
    reg_destinations_cipo => reg_bdo_destinations_cipo,
    reg_dp_xonoff_mosi    => reg_dp_xonoff_mosi,
    reg_dp_xonoff_miso    => reg_dp_xonoff_miso
  );
  bf_udp_sosi <= mon_bf_udp_sosi;

  u_bsn_mon_beamlet_output : entity dp_lib.mms_dp_bsn_monitor_v2
  generic map (
    g_nof_streams        => 1,
    g_cross_clock_domain => true,
    g_sync_timeout       => c_sdp_N_clk_sync_timeout,
    g_bsn_w              => c_dp_stream_bsn_w,
    g_error_bi           => 0,
    g_cnt_sop_w          => c_word_w,
    g_cnt_valid_w        => c_word_w,
    g_cnt_latency_w      => c_word_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    reg_mosi       => reg_bsn_monitor_v2_beamlet_output_copi,
    reg_miso       => reg_bsn_monitor_v2_beamlet_output_cipo,

    -- Streaming clock domain
    dp_rst         => dp_rst,
    dp_clk         => dp_clk,
    ref_sync       => ref_sync,

    in_siso_arr(0) => bf_udp_siso,
    in_sosi_arr(0) => mon_bf_udp_sosi
  );

  ---------------------------------------------------------------
  -- Beamlet Statistics (BST)
  ---------------------------------------------------------------
  u_beamlet_stats : entity st_lib.st_sst
  generic map(
    g_nof_stat      => c_sdp_S_sub_bf * c_sdp_N_pol_bf,
    g_in_data_w     => c_sdp_W_beamlet_sum,
    g_stat_data_w   => c_longword_w,
    g_stat_data_sz  => c_longword_sz / c_word_sz,
    g_stat_multiplex => c_sdp_N_pol_bf
  )
  port map (
    mm_rst          => mm_rst,
    mm_clk          => mm_clk,
    dp_rst          => dp_rst,
    dp_clk          => dp_clk,
    in_complex      => bf_sum_sosi,
    ram_st_sst_mosi => master_mem_mux_mosi,
    ram_st_sst_miso => master_mem_mux_miso
  );

  ---------------------------------------------------------------
  -- MM master multiplexer
  ---------------------------------------------------------------
  -- Connect 2 mm_masters to the common_mem_mux output
  master_mosi_arr(0)  <= ram_st_bst_mosi;  -- MM access via QSYS MM bus
  ram_st_bst_miso     <= master_miso_arr(0);
  master_mosi_arr(1)  <= ram_st_offload_mosi;  -- MM access by SST offload
  ram_st_offload_miso <= master_miso_arr(1);

  u_mem_master_mux : entity mm_lib.mm_master_mux
  generic map (
    g_nof_masters    => c_nof_masters,
    g_rd_latency_min => 1  -- read latency of statistics RAM is 1
  )
  port map (
    mm_clk => mm_clk,

    master_mosi_arr => master_mosi_arr,
    master_miso_arr => master_miso_arr,
    mux_mosi        => master_mem_mux_mosi,
    mux_miso        => master_mem_mux_miso
  );

  ---------------------------------------------------------------
  -- BST UDP offload
  ---------------------------------------------------------------
  u_sdp_bst_udp_offload: entity work.sdp_statistics_offload
  generic map (
    g_statistics_type   => "BST",
    g_offload_node_time => sel_a_b(g_sim, g_sim_sdp.offload_node_time, c_sdp_offload_node_time),
    g_beamset_id        => g_beamset_id
  )
  port map (
    mm_clk    => mm_clk,
    mm_rst    => mm_rst,

    dp_clk    => dp_clk,
    dp_rst    => dp_rst,

    master_mosi => ram_st_offload_mosi,
    master_miso => ram_st_offload_miso,

    reg_enable_mosi  => reg_stat_enable_mosi,
    reg_enable_miso  => reg_stat_enable_miso,

    reg_hdr_dat_mosi => reg_stat_hdr_dat_mosi,
    reg_hdr_dat_miso => reg_stat_hdr_dat_miso,

    reg_bsn_monitor_v2_offload_copi => reg_bsn_monitor_v2_bst_offload_copi,
    reg_bsn_monitor_v2_offload_cipo => reg_bsn_monitor_v2_bst_offload_cipo,

    in_sosi      => bf_sum_sosi,
    new_interval => dp_bsn_source_new_interval,

    out_sosi  => bst_udp_sosi,
    out_siso  => bst_udp_siso,

    eth_src_mac  => stat_eth_src_mac,
    udp_src_port => stat_udp_src_port,
    ip_src_addr  => stat_ip_src_addr,

    gn_index     => TO_UINT(gn_id),
    ring_info    => ring_info,
    sdp_info     => sdp_info,
    weighted_subbands_flag => '1'  -- because BF uses in_sosi_arr = fsub_sosi_arr, so weighted subbands
  );

  ---------------------------------------------------------------
  -- SIGNAL SCOPES
  ---------------------------------------------------------------
  u_sdp_scope_local_bf : entity work.sdp_scope
    generic map (
      g_sim            => g_sim,
      g_selection      => g_scope_selected_beamlet,
      g_nof_input      => 1,
      g_n_deinterleave => c_sdp_N_pol_bf,
      g_dat_w          => c_sdp_W_beamlet_sum
    )
    port map (
      clk            => dp_clk,
      rst            => dp_rst,
      sp_sosi_arr(0) => local_bf_sosi,
      scope_sosi_arr => scope_local_bf_sosi_arr
    );

  u_sdp_scope_bf_sum : entity work.sdp_scope
    generic map (
      g_sim            => g_sim,
      g_selection      => g_scope_selected_beamlet,
      g_nof_input      => 1,
      g_n_deinterleave => c_sdp_N_pol_bf,
      g_dat_w          => c_sdp_W_beamlet_sum

    )
    port map (
      clk            => dp_clk,
      rst            => dp_rst,
      sp_sosi_arr(0) => bf_sum_sosi,
      scope_sosi_arr => scope_bf_sum_sosi_arr
    );

  u_sdp_scope_bf_out : entity work.sdp_scope
    generic map (
      g_sim            => g_sim,
      g_selection      => g_scope_selected_beamlet,
      g_nof_input      => 1,
      g_n_deinterleave => c_sdp_N_pol_bf,
      g_dat_w          => c_sdp_W_beamlet

    )
    port map (
      clk            => dp_clk,
      rst            => dp_rst,
      sp_sosi_arr(0) => bf_out_sosi,
      scope_sosi_arr => scope_bf_out_sosi_arr
    );
end str;
