------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, dp_lib, eth_lib, tech_tse_lib, bf_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use dp_lib.dp_stream_pkg.all;
use eth_lib.eth_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use bf_lib.bf_pkg.all;

entity unb1_fn_bf is
  generic (
    g_design_name : string  := "unb1_fn_bf";
    g_design_note : string  := "UNUSED";
    g_sim         : boolean := false;  -- Overridden by TB
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0;
    g_stamp_date  : natural := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time  : natural := 0;  -- Time (HHMMSS)   -- set by QSF
    g_stamp_svn   : natural := 0;  -- SVN revision    -- set by QSF
    g_bf          : t_c_bf  := c_bf
  );
  port (
   -- GENERAL
    CLK           : in    std_logic;  -- System Clock
    PPS           : in    std_logic;  -- System Sync
    WDI           : out   std_logic;  -- Watchdog Clear
    INTA          : inout std_logic;  -- FPGA interconnect line
    INTB          : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION       : in    std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0);
    ID            : in    std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
    TESTIO        : inout std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    sens_sc       : inout std_logic;
    sens_sd       : inout std_logic;

    -- 1GbE Control Interface
    ETH_clk       : in    std_logic;
    ETH_SGIN      : in    std_logic;
    ETH_SGOUT     : out   std_logic
  );
end unb1_fn_bf;

architecture str of unb1_fn_bf is
  constant c_bf_offload            : boolean := false;  -- Offload BF out(0) datapath to 1GbE UDP TX port
  constant c_use_phy               : t_c_unb1_board_use_phy := (1, 0, 0, 0, 0, 0, 0, 1);
  constant c_fw_version            : t_unb1_board_fw_version := (2, 3);  -- firmware version x.y
  constant c_nof_streams           : natural := c_eth_nof_udp_ports;
  constant c_weights_write_only    : boolean := true;  -- When set to TRUE the M9K blocks are forced to Simple Dual Port mode. When FALSE it is True Dual Port.

  -- Use default RAM inti files. The RAM init file for simulation lies one ../ level further way then for synthesis
  constant c_bf_weights_file_name  : string := "UNUSED";
  constant c_ss_wide_file_prefix   : string := "hex/ss_wide";
  constant c_block_gen_file_prefix : string := "UNUSED";

    -- BF offload
  constant c_hdr_nof_words          : natural := c_network_total_header_32b_nof_words;
  constant c_dp_ram_mm_nof_words    : natural := c_hdr_nof_words * (c_eth_data_w / c_word_w);
  constant c_dp_ram_mm_adr_w        : natural := ceil_log2(c_dp_ram_mm_nof_words);

  -- System
  signal cs_sim                     : std_logic;
  signal xo_clk                     : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_locked                  : std_logic;
  signal mm_rst                     : std_logic;

  signal dp_rst                     : std_logic;
  signal dp_clk                     : std_logic;
  signal dp_pps                     : std_logic;

  signal app_led_red                : std_logic := '0';
  signal app_led_green              : std_logic := '1';

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- eth1g
  signal eth1g_tse_clk              : std_logic;
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi := c_mem_mosi_rst;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;
  signal eth1g_led                  : t_tech_tse_led;

  -- eth1g UDP streaming ports
  signal eth1g_udp_tx_sosi_arr      : t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0);
  signal eth1g_udp_tx_siso_arr      : t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0);

  -- MM registers and RAM
  -- . block generator
  signal reg_diag_bg_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal reg_diag_bg_miso           : t_mem_miso;
  signal ram_diag_bg_mosi           : t_mem_mosi := c_mem_mosi_rst;
  signal ram_diag_bg_miso           : t_mem_miso;
  -- . beam former
  signal ram_bf_weights_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal ram_bf_weights_miso        : t_mem_miso;
  signal ram_st_sst_bf_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_sst_bf_miso         : t_mem_miso;
  signal reg_st_sst_bf_mosi         : t_mem_mosi := c_mem_mosi_rst;
  signal reg_st_sst_bf_miso         : t_mem_miso;
  signal ram_ss_ss_wide_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal ram_ss_ss_wide_miso        : t_mem_miso;

  -- . uniboard sensors
  signal reg_unb_sens_mosi          : t_mem_mosi := c_mem_mosi_rst;
  signal reg_unb_sens_miso          : t_mem_miso;

  signal reg_diagnostics_mosi       : t_mem_mosi;
  signal reg_diagnostics_miso       : t_mem_miso;
  -- . dp_ram_from_mm for DP offload (header insertion)
  signal reg_dp_ram_from_mm_mosi    : t_mem_mosi;
  signal reg_dp_ram_from_mm_miso    : t_mem_miso := c_mem_miso_rst;

  signal ram_dp_ram_from_mm_mosi    : t_mem_mosi;
  signal ram_dp_ram_from_mm_miso    : t_mem_miso := c_mem_miso_rst;

  signal ram_dp_ram_to_mm_mosi      : t_mem_mosi;
  signal ram_dp_ram_to_mm_miso      : t_mem_miso;

  signal reg_dp_split_mosi          : t_mem_mosi;
  signal reg_dp_split_miso          : t_mem_miso;

  signal reg_dp_pkt_merge_mosi      : t_mem_mosi;
  signal reg_dp_pkt_merge_miso      : t_mem_miso;

  -- ST interface
  signal beams_sosi_arr             : t_dp_sosi_arr(g_bf.nof_bf_units - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl : entity unb1_board_lib.ctrl_unb1_board
  generic map (
    g_sim           => g_sim,
    g_design_name   => g_design_name,
    g_design_note   => g_design_note,
    g_stamp_date    => g_stamp_date,
    g_stamp_time    => g_stamp_time,
    g_stamp_svn     => g_stamp_svn,
    g_fw_version    => c_fw_version,
    g_mm_clk_freq   => c_unb1_board_mm_clk_freq_50M,
    g_use_phy       => c_use_phy,
    g_udp_offload   => sel_a_b(c_bf_offload, true, false),
    g_aux           => c_unb1_board_aux,
    g_udp_offload_nof_streams => c_nof_streams
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_clk                   => xo_clk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,
    mm_rst                   => mm_rst,

    dp_rst                   => dp_rst,
    dp_clk                   => dp_clk,
    dp_pps                   => OPEN,
    dp_rst_in                => dp_rst,
    dp_clk_in                => dp_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,  -- 125 MHz from xo_clk PLL in SOPC system
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- eth1g UDP streaming ports to offload BF out
    udp_tx_sosi_arr        =>  eth1g_udp_tx_sosi_arr,
    udp_tx_siso_arr        =>  eth1g_udp_tx_siso_arr,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    sens_sc                  => sens_sc,
    sens_sd                  => sens_sd,
    -- . 1GbE Control Interface
    ETH_clk                  => ETH_clk,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- MM master
  -----------------------------------------------------------------------------
  u_mmm : entity work.mmm_unb1_fn_bf
  generic map (
    g_sim         => g_sim,
    g_sim_unb_nr  => g_sim_unb_nr,
    g_sim_node_nr => g_sim_node_nr,
    g_bf          => g_bf
   )
  port map(
    xo_clk                   => xo_clk,
    xo_rst_n                 => xo_rst_n,
    xo_rst                   => xo_rst,

    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,
    mm_locked                => mm_locked,

    -- PIOs
    pout_wdi                 => pout_wdi,

    -- Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- system_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- eth1g
    eth1g_tse_clk            => eth1g_tse_clk,
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- Diagnostics
    reg_diagnostics_mosi     => reg_diagnostics_mosi,
    reg_diagnostics_miso     => reg_diagnostics_miso,

    -- . block generator
    reg_diag_bg_mosi         => reg_diag_bg_mosi,
    reg_diag_bg_miso         => reg_diag_bg_miso,
    ram_diag_bg_mosi         => ram_diag_bg_mosi,
    ram_diag_bg_miso         => ram_diag_bg_miso,

    -- beamformer
    ram_ss_ss_wide_mosi      => ram_ss_ss_wide_mosi,
    ram_ss_ss_wide_miso      => ram_ss_ss_wide_miso,
    ram_bf_weights_mosi      => ram_bf_weights_mosi,
    ram_bf_weights_miso      => ram_bf_weights_miso,
    ram_st_sst_bf_mosi       => ram_st_sst_bf_mosi,
    ram_st_sst_bf_miso       => ram_st_sst_bf_miso,
    reg_st_sst_bf_mosi       => reg_st_sst_bf_mosi,
    reg_st_sst_bf_miso       => reg_st_sst_bf_miso,

    -- dp_offload               -- dp_offload
    reg_dp_ram_from_mm_mosi  => reg_dp_ram_from_mm_mosi,
    reg_dp_ram_from_mm_miso  => reg_dp_ram_from_mm_miso,
    ram_dp_ram_from_mm_mosi  => ram_dp_ram_from_mm_mosi,
    ram_dp_ram_from_mm_miso  => ram_dp_ram_from_mm_miso,
    reg_dp_split_mosi        => reg_dp_split_mosi,
    reg_dp_split_miso        => reg_dp_split_miso,
    reg_dp_pkt_merge_mosi    => reg_dp_pkt_merge_mosi,
    reg_dp_pkt_merge_miso    => reg_dp_pkt_merge_miso
  );

  u_node_unb1_fn_bf : entity work.node_unb1_fn_bf
  generic map(
    g_bf                    => g_bf,
    g_bf_offload            => c_bf_offload,
    g_bf_weights_file_name  => c_bf_weights_file_name,
    g_ss_wide_file_prefix   => c_ss_wide_file_prefix,
    g_block_gen_file_prefix => c_block_gen_file_prefix,
    g_weights_write_only    => c_weights_write_only
  )
  port map(
    -- System
    mm_rst                  => mm_rst,
    mm_clk                  => mm_clk,
    dp_rst                  => dp_rst,
    dp_clk                  => dp_clk,
    -- MM interface
    -- . block generator
    reg_diag_bg_mosi        => reg_diag_bg_mosi,
    reg_diag_bg_miso        => reg_diag_bg_miso,
    ram_diag_bg_mosi        => ram_diag_bg_mosi,
    ram_diag_bg_miso        => ram_diag_bg_miso,

    -- . beam former
    ram_bf_weights_mosi     => ram_bf_weights_mosi,
    ram_bf_weights_miso     => ram_bf_weights_miso,
    ram_ss_ss_wide_mosi     => ram_ss_ss_wide_mosi,
    ram_ss_ss_wide_miso     => ram_ss_ss_wide_miso,
    ram_st_sst_bf_mosi      => ram_st_sst_bf_mosi,
    ram_st_sst_bf_miso      => ram_st_sst_bf_miso,
    reg_st_sst_bf_mosi      => reg_st_sst_bf_mosi,
    reg_st_sst_bf_miso      => reg_st_sst_bf_miso,

    -- . hdr_insert for dp offload
    reg_hdr_insert_mosi     => reg_dp_ram_from_mm_mosi,
    ram_hdr_insert_mosi     => ram_dp_ram_from_mm_mosi,

    -- ST interface
    out_bst_sosi_arr        => OPEN,  -- 16b beamlets
    out_qua_sosi_arr        => beams_sosi_arr,  -- 8b beamlets

    -- DP offload of 16b beamlets to 1GbE via ctrl_unb_common
    bf_out_offload_tx_sosi_arr => eth1g_udp_tx_sosi_arr,
    bf_out_offload_tx_siso_arr => eth1g_udp_tx_siso_arr
  );
end;
