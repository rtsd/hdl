#------------------------------------------------------------------------------
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# DS: This file is based on (read: severely stripped down) Megawizard-generated
# file msim_setup.tcl.
# The purpose of this file is to compile all files required for the XAUI SOFT IP - 
# which is quite a list and tedious to have to add to the MPF (also in the
# correct compile order). Bonus of this is also that there will be no errors
# when making all_mod without having run the XAUI megawizard first.

set IP_DIR "$env(HDL_BUILD_DIR)/unb1/qmegawiz/ip_stratixiv_phy_xaui_soft_sim"

#vlib ./work/       ;# EK: Assume library work already exists


vlog -sv "$IP_DIR/altera_xcvr_xaui/altera_xcvr_functions.sv"                           -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/altera_xcvr_functions.sv"                    -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_pma_functions.sv"                               -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_pma_functions.sv"                        -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/altera_xcvr_xaui.sv"                                -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/hxaui_csr_h.sv"                                     -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/hxaui_csr.sv"                                       -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_mgmt2dec_phyreconfig.sv"                   -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_mgmt2dec_xaui.sv"                          -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/altera_xcvr_xaui.sv"                         -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/hxaui_csr_h.sv"                              -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/hxaui_csr.sv"                                -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_mgmt2dec_phyreconfig.sv"            -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_mgmt2dec_xaui.sv"                   -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/alt_pma_ch_controller_tgx.v"                        -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_pma_ch_controller_tgx.v"                 -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/alt_pma_controller_tgx.v"                           -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_pma_controller_tgx.v"                    -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_reset_ctrl_lego.sv"                             -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_reset_ctrl_tgx_cdrauto.sv"                      -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_resync.sv"                                 -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_csr_common_h.sv"                           -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_csr_common.sv"                             -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_csr_pcs8g_h.sv"                            -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_csr_pcs8g.sv"                              -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_csr_selector.sv"                           -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_mgmt2dec.sv"                               -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/altera_wait_generate.v"                             -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_reset_ctrl_lego.sv"                      -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_reset_ctrl_tgx_cdrauto.sv"               -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_resync.sv"                          -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_csr_common_h.sv"                    -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_csr_common.sv"                      -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_csr_pcs8g_h.sv"                     -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_csr_pcs8g.sv"                       -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_csr_selector.sv"                    -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_mgmt2dec.sv"                        -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/altera_wait_generate.v"                      -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_pcs.v"                         -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_reset.v"                       -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx.v"                          -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_8b10b_dec.v"                -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_channel_synch.v"            -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_deskew.v"                   -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_deskew_channel.v"           -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_deskew_ram.v"               -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_invalid_code_det.v"         -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_parity.v"                   -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_parity_4b.v"                -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_parity_6b.v"                -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_rate_match.v"               -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_rate_match_ram.v"           -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_rl_chk_6g.v"                -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_rx_sm.v"                       -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_tx.v"                          -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_tx_8b10b_enc.v"                -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_soft_xaui_tx_idle_conv.v"                -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/l_modules.v"                                 -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/serdes_4_unit_lc_siv.v"                      -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/serdes_4_unit_siv.v"                         -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/serdes_4unit.v"                              -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/sxaui.v"                                            -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/sxaui.v"                                     -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/siv_xcvr_low_latency_phy_nr.sv"                     -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/siv_xcvr_low_latency_phy_nr.sv"              -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/siv_xcvr_xaui.sv"                                   -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/siv_xcvr_xaui.sv"                            -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/alt4gxb_vo.v"                                       -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt4gxb_vo.v"                                -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_h.sv"                             -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_h.sv"                      -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_siv.sv"                           -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_siv.sv"                    -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_analog.sv"                        -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_analog.sv"                 -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_analog_tgx.v"                     -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_analog_tgx.v"              -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_offset_cancellation.sv"           -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_offset_cancellation.sv"    -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_offset_cancellation_tgx.v"        -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_offset_cancellation_tgx.v" -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_eyemon_tgx.sv"                    -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_eyemon_tgx.sv"             -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_dfe_tgx.sv"                       -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_dfe_tgx.sv"                -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/alt_xcvr_reconfig_basic_tgx.v"                      -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/alt_mutex_acq.v"                                    -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/alt_dprio.v"                                        -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_reconfig_basic_tgx.v"               -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_mutex_acq.v"                             -work work 
vlog     "$IP_DIR/altera_xcvr_xaui/mentor/alt_dprio.v"                                 -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_arbiter.sv"                                -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/alt_xcvr_m2s.sv"                                    -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_arbiter.sv"                         -work work 
vlog -sv "$IP_DIR/altera_xcvr_xaui/mentor/alt_xcvr_m2s.sv"                             -work work 
