-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http:--www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http:--www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.mdio_pkg.all;

-- Selection of register addresses of the Vitesse VSC8684-11.
-- To prevent targeting the wrong device number, each register constant has a
-- _dev# postfix to indicate the host device of the register.
-- The following _en, _dis and _set postfixed constants are the register
-- contents with the target bit(s) set or unset.

package mdio_vitesse_vsc8486_pkg is
  constant c_mdio_vsc8486_prtad : std_logic_vector(c_mdio_phy_hdr_prtad_len - 1 downto 0) := "00000";

  -- XAUI loopbacks, closest to XAUI side first: B, C, E. G, J
  -- =========================================================

  -- XAUI LOOPBACK B
  -- ===============
  constant c_mdio_vsc8486_lpbk_xaui_b_dev4        : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"800E";  -- bit 13 of this reg.
  constant c_mdio_vsc8486_lpbk_xaui_b_dev4_en     : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"3800";  -- b'00111000 00000000
  constant c_mdio_vsc8486_lpbk_xaui_b_dev4_dis    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"1800";  -- b'00011000 00000000

  -- XAUI LOOPBACK C
  -- ===============mdio_vitesse_vsc8486_pkg
  constant c_mdio_vsc8486_lpbk_xaui_c_dev4        : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"800F";  -- bit 2 of this reg.
  constant c_mdio_vsc8486_lpbk_xaui_c_dev4_en     : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0664";  -- b'00000110 01100100
  constant c_mdio_vsc8486_lpbk_xaui_c_dev4_dis    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0660";  -- b'00000110 01100000

  -- XAUI LOOPBACK E
  -- ===============
  constant c_mdio_vsc8486_lpbk_xaui_e_dev3        : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8005";  -- bit 2 of this reg.
  constant c_mdio_vsc8486_lpbk_xaui_e_dev3_en     : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0004";  -- b'00000000 00000100
  constant c_mdio_vsc8486_lpbk_xaui_e_dev3_dis    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0000";  -- b'00000000 00000000

  -- XAUI LOOPBACK G
  -- ===============
  constant c_mdio_vsc8486_lpbk_xaui_g_dev3        : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0000";  -- bit 14 of this reg.
  constant c_mdio_vsc8486_lpbk_xaui_g_dev3_en     : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"6200";  -- b'01100010 00000000
  constant c_mdio_vsc8486_lpbk_xaui_g_dev3_dis    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"2200";  -- b'00100010 00000000

  -- XAUI LOOPBACK J
  -- ===============
  constant c_mdio_vsc8486_lpbk_xaui_j_dev2        : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0000";  -- bit 14 of this reg.
  constant c_mdio_vsc8486_lpbk_xaui_j_dev2_en     : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"6040";  -- b'01100000 01000000
  constant c_mdio_vsc8486_lpbk_xaui_j_dev2_dis    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"2040";  -- b'00100000 01000000

  -- XFI loopbacks, closest to XFI side first: K, H, F, D, A
  -- ========================================================

  -- XFI LOOPBACK K
  -- ==============
  constant c_mdio_vsc8486_lpbk_xfi_k_dev1         : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8000";  -- bit 8 of this reg. (0 to enable)
  constant c_mdio_vsc8486_lpbk_xfi_k_dev1_en      : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"B4DF";  -- b'10110100 11011111
  constant c_mdio_vsc8486_lpbk_xfi_k_dev1_dis     : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"B5DF";  -- b'10110101 11011111

  -- RX EQUALIZATION
  -- ===============
  constant c_mdio_vsc8486_rx_eq_dev1              : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8002";
  constant c_mdio_vsc8486_rx_eq_dev1_set          : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8D55";

  -- EPCS
  -- ====
  constant c_mdio_vsc8486_epcs_ovr_frc_dev1       : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"E605";  -- bit 11 to override pin value, bit 10 to enable EPCS
  constant c_mdio_vsc8486_epcs_ovr_frc_dev1_en    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0C00";  -- b'00001100 00000000
  constant c_mdio_vsc8486_epcs_ovr_frc_dev1_dis   : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0000";  -- b'00000000 00000000

  -- LEDs
  -- ====
  constant c_mdio_vsc8486_led_alarm_dev1          : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"E901";
  constant c_mdio_vsc8486_led_alarm_dev1_en       : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"283A";

  -- PCS: FIFO STATUS: Used to determine whether our data/idle ratio is OK.
  --                   Overflow:  add more idle patterns that can be dropped
  --                   Underflow: Less idle pattern necessary
  -- =======================================================================
  -- Bit  Description
  --
  -- 15:4 Reserved (read 0)
  -- 3    RX FIFO Overflow
  -- 2    RX FIFO Underflow
  -- 1    TX FIFO Overflow
  -- 0    TX FIFO Underflow
  constant c_mdio_vsc8486_pcs_fifo_stat_dev3      : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8009";

  -- PCS: Monitor the number of added/dropped idle patterns (to compensate clock rate disparity)
  constant c_mdio_vsc8486_pcs_tx_add_cnt_dev3     : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"800C";
  constant c_mdio_vsc8486_pcs_tx_drop_cnt_dev3    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"800D";
  constant c_mdio_vsc8486_pcs_rx_add_cnt_dev3     : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"800E";
  constant c_mdio_vsc8486_pcs_rx_drop_cnt_dev3    : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"800F";

  -- PCS (PMA clock domain = read side of TX FIFO = write side of RX FIFO)
  constant c_mdio_vsc8486_pcs_tx_seqerr_cnt_dev3  : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8010";  -- PCS TX sequencing error count
  constant c_mdio_vsc8486_pcs_rx_seqerr_cnt_dev3  : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8011";  -- PCS RX sequencing error count
  constant c_mdio_vsc8486_pcs_tx_blkerr_cnt_dev3  : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8012";  -- PCS TX block encode error count
  constant c_mdio_vsc8486_pcs_rx_blkerr_cnt_dev3  : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8013";  -- PCS RX block decode error count
  constant c_mdio_vsc8486_pcs_tx_charerr_cnt_dev3 : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8014";  -- PCS TX char encode error count
  constant c_mdio_vsc8486_pcs_rx_charerr_cnt_dev3 : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"8015";  -- PCS RX char decode error count

  -- PHY XS STATUS 3
  -- ===============
  constant c_mdio_vsc8486_phy_xs_stat3_dev4       : std_logic_vector(c_mdio_phy_data_width - 1 downto 0) := x"0018";

  -- Vitesse VSC8684 initializion sequence for auto execution by mdio_ctlr FSM.
  -- These MDIO settings work on the UniBoard with the tr_xaui module and optical fibre links.
  -- =========================================================================================
  constant c_mdio_vsc8486_init_cmd_arr : t_mdio_cmd_arr(0 to 1) := (
    -- Enable EPCS (override pin value & set EPCS = 2 bits set). EPCS is disabled and pin not overridden by default.
--    ('1', TO_UVEC(1, c_mdio_phy_hdr_devad_len), c_mdio_vsc8486_epcs_ovr_frc_dev1, c_mdio_vsc8486_epcs_ovr_frc_dev1_en),
    -- Enable status LEDs
    ('1', TO_UVEC(1, c_mdio_phy_hdr_devad_len), c_mdio_vsc8486_led_alarm_dev1, c_mdio_vsc8486_led_alarm_dev1_en),
    -- Set RX equalization.
    ('1', TO_UVEC(1, c_mdio_phy_hdr_devad_len), c_mdio_vsc8486_rx_eq_dev1, c_mdio_vsc8486_rx_eq_dev1_set)
  );

  constant c_mdio_vsc8486_init_epcs_dis_cmd_arr : t_mdio_cmd_arr(0 to 2) :=  (
    -- Disable EPCS to taget PC
    ('1', TO_UVEC(1, c_mdio_phy_hdr_devad_len), c_mdio_vsc8486_epcs_ovr_frc_dev1, c_mdio_vsc8486_epcs_ovr_frc_dev1_dis),
    -- Enable status LEDs
    ('1', TO_UVEC(1, c_mdio_phy_hdr_devad_len), c_mdio_vsc8486_led_alarm_dev1, c_mdio_vsc8486_led_alarm_dev1_en),
    -- Set RX equalization.
    ('1', TO_UVEC(1, c_mdio_phy_hdr_devad_len), c_mdio_vsc8486_rx_eq_dev1, c_mdio_vsc8486_rx_eq_dev1_set)
  );
end mdio_vitesse_vsc8486_pkg;

package body mdio_vitesse_vsc8486_pkg is
end mdio_vitesse_vsc8486_pkg;
