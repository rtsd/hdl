-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: Eric Kooistra, 3 Sept 2021
-- Purpose: Verify dp_bsn_align_v2
-- Description:
--   The tb verifies:
--   . DUT alignment of g_nof_streams >= 2
--   . DUT restart via g_tb_nof_restart > 1
--   . gaps or no gaps between blocks via g_block_period >= g_block_size
--   . g_bsn_latency_max in combination with g_tb_diff_delay
--   . g_use_mm_output using DUT MM to DP or external MM to DP in tb
--   . g_data_replacement_value for a remote g_disable_stream_id (it is
--     sufficient to verify one stream)
--   . g_data_replacement_value and lost flag channel(0) bit for a lost
--     remote g_lost_stream_id
--   . the local stream cannot be disabled or lost, because if it does,
--     then there is no output, which is verified by g_tb_nof_restart >= 2
--   . g_lost_bsn_id to loose a single block in stream 1 and verify that
--     it gets replaced and flagged.
--   . array of one or more BSN aligners via g_nof_aligners_max >= 1,
--     using chain_node_index_arr, only support tb for g_use_mm_output = false
-- Remark:
--   For this BSN aligner component it was essential to have an almost
--   complete, reviewed, detailed design document, because it is a complex
--   component. Main difference after review was addding build in support
--   for streaming output via g_use_mm_output. The clear design made it
--   much easier to achieve a draft implementation that was almost correct.
--   For the DUT it implementation it was also essential to use the p_reg,
--   p_comb coding template.
--   The initial DUT implementation did not change much anymore after the
--   first tb tests, expect for some small (but important) corrections.
--   Each feature of the tb took several hours or up to a day to add. Much
--   time was also spent to regularly clean up and simply the code. The
--   last tb feature that verifies a chain of DUTs is realy nice to see,
--   e.g. by expanding dut_out_sosi_2_arr in the Wave window. The ascii
--   drawing at gen_bsn_align_chain was used to more easily connect and
--   clarify the wiring for the chain of DUTs.
--   Implementing the tb took more time than the DUT, but the design
--   document and the especially the design decisions took the most time.
--   The design decision to use a circular buffer instead of FIFOs and the
--   design decision to rely on a local reference and fixed latencies were
--   important. Initial prestudy end 2020 was done based on experience with
--   aligning streams in LOFAR1 and with the bsn_aligner.vhd in APERTIF.
--   About a in autumn 2021 the detailed design and implementation were done.
--   . prestudy ~ 1 week
--   . design decisions doc ~ 1 week (based on prestudy)
--   . detailed design doc ~ 1 week
--   . review and process review ~ 2 days
--   . implement DUT ~ 2 days (core in 1 day, features one more day)
--   . implement tb (and adding to tb_tb) ~ 1 week
--   . implement tb_mmp ~ 1 day
--   Total: ~ 6 weeks
--
-- Usage:
-- > as 10
-- > run -all

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_bsn_align_v2 is
  generic (
    -- DUT
    g_nof_streams            : natural := 2;  -- number of input and output streams
    g_bsn_latency_max        : natural := 2;  -- Maximum travel latency of a remote block in number of block periods
    g_bsn_latency_first_node : natural := 1;
    g_nof_aligners_max       : positive := 8;  -- 1 when only align at last node, > 1 when align at every intermediate node
    g_block_size             : natural := 11;  -- > 1, g_block_size=1 is not supported
    g_block_period           : natural := 20;  -- >= g_block_size, = g_block_size + c_gap_size
    g_bsn_w                  : natural := c_dp_stream_bsn_w;  -- number of bits in sosi BSN
    g_data_w                 : natural := 16;  -- number of bits in sosi data
    g_data_replacement_value : integer := 17;  -- output sosi data replacement value for missing input blocks
    g_disable_stream_id      : natural := 0;  -- default 0 to enable all streams, > 0 selects stream that will be disabled
    g_lost_stream_id         : natural := 0;  -- default 0 to have all streams, > 0 selects stream that will be lost
    g_lost_bsn_id            : natural := 0;  -- for stream 1 the block with bsn = g_lost_bsn_id will be lost
    g_bsn_init               : natural := 3;  -- use > 0 to have no lost data for g_lost_bsn_id = 0
    g_use_mm_output          : boolean := false;  -- output via MM or via streaming DP
    g_pipeline_input         : natural := 0;  -- >= 0, choose 0 for wires, choose 1 to ease timing closure of in_sosi_arr
    g_pipeline_output        : natural := 0;  -- >= 0, choose 0 for wires, choose 1 to ease timing closure of out_sop_arr
    g_rd_latency             : natural := 1;  -- 1 or 2, choose 2 to ease timing closure

    -- TB
    g_tb_diff_delay          : integer := 0;  -- 0 = aligned inputs, -1 = max input delay for no loss,
                                                   -- >~ g_bsn_latency_max * g_block_period will give loss
    g_tb_nof_restart         : natural := 2;  -- number of times to restart the input stimuli
    g_tb_nof_blocks          : natural := 30  -- number of input blocks per restart, choose > circular buffer size, so
                                              -- > c_align_latency_nof_blocks
  );
end tb_dp_bsn_align_v2;

architecture tb of tb_dp_bsn_align_v2 is
  constant c_rl                       : natural := 1;

  constant c_data_w                   : natural := 16;
  constant c_data_init                : integer := 0;
  constant c_bsn_w                    : natural := 16;  -- use <= 31 bit to fit NATURAL
  constant c_channel_init             : integer := 0;
  constant c_err_init                 : natural := 247;
  constant c_sync_period              : natural := 7;
  constant c_sync_offset              : natural := 2;

  -- maximum nof clk delay between any inputs, <= c_align_latency_nof_clk
  -- . the -1 is due to some acceptable pipeline detail related to dp_block_from_mm
  constant c_diff_delay_max           : natural := g_bsn_latency_max * g_block_period - sel_a_b(g_rd_latency > 1, 0, 1);
  constant c_diff_delay               : natural := sel_a_b(g_tb_diff_delay < 0, c_diff_delay_max, g_tb_diff_delay);

  -- Return input delay as function of inputs stream index I
  function func_input_delay(I : natural) return natural is
    variable v : natural;
  begin
    if g_nof_streams > 1 then
      v := c_diff_delay * I / (g_nof_streams - 1);
    else
      v := 0;
    end if;
    return v;
  end;

  constant c_gap_size                 : natural := g_block_period - g_block_size;

  -- Fixed use stream 1 to verify g_lost_bsn_id. Use 0 for g_nof_streams = 1.
  constant c_lost_bsn_stream_id       : natural := sel_a_b(g_nof_streams > 1, 1, 0);

  -- In tb no support (yet) for immediate aligned output at first node, when c_nof_aligners_max > 1
  constant c_use_aligner_at_first_node : boolean := true;

  -- In the tb only support MM interface verification for c_nof_aligners_max = 1
  constant c_nof_aligners_max  : positive := sel_a_b(g_use_mm_output, 1, g_nof_aligners_max);

  -- DUT latency of chain of DUTs is same as DUT latency of one DUT, so
  -- independent c_nof_aligners_max. This is because the c_dut_latency of the
  -- other DUTs is covered by the buffer latency.
  constant c_mm_to_dp_latency         : natural := 1;
  constant c_dut_latency              : natural := g_pipeline_input + g_rd_latency + c_mm_to_dp_latency + g_pipeline_output;

  -- DUT buffer latency for chain of DUTs
  constant c_align_latency_nof_blocks : natural := sel_a_b(c_nof_aligners_max = 1,
               g_bsn_latency_max,
               g_bsn_latency_max * (c_nof_aligners_max - 1) + g_bsn_latency_first_node);  -- number blocks
  constant c_align_latency_nof_valid  : natural := c_align_latency_nof_blocks * g_block_size;  -- number of data samples
  constant c_align_latency_nof_clk    : natural := c_align_latency_nof_blocks * g_block_period;  -- number clk cycles

  -- Total DUT chain latency
  constant c_total_latency            : natural := c_dut_latency + c_align_latency_nof_clk;
  constant c_verify_nof_blocks        : natural := g_tb_nof_blocks - c_align_latency_nof_blocks;  -- skip last blocks that are still in the DUT buffer

  type t_tb_state is (s_idle, s_start, s_restart);

  type t_data_arr    is array (g_nof_streams - 1 downto 0) of std_logic_vector(c_data_w - 1 downto 0);
  type t_bsn_arr     is array (g_nof_streams - 1 downto 0) of std_logic_vector(c_bsn_w - 1 downto 0);
  type t_err_arr     is array (g_nof_streams - 1 downto 0) of std_logic_vector(c_dp_stream_error_w - 1 downto 0);
  type t_channel_arr is array (g_nof_streams - 1 downto 0) of std_logic_vector(c_dp_stream_channel_w - 1 downto 0);

  type t_reg is record
    -- p_write_arr
    sync         : std_logic;
    bsn          : std_logic_vector(g_bsn_w - 1 downto 0);
    out_sosi_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  end record;

  type t_dut_sosi_2arr is array (natural range <>) of t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal tb_end_arr            : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
  signal tb_end                : std_logic;
  signal clk                   : std_logic := '1';
  signal rst                   : std_logic := '1';
  signal sl1                   : std_logic := '1';

  signal chain_node_index_arr  : t_nat_natural_arr(0 to c_nof_aligners_max - 1) := array_init(0, c_nof_aligners_max, 1);

  signal stream_en_arr         : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '1');  -- default all streams are enabled
  signal stream_lost_arr       : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- default no streams are lost
  signal in_bsn_lost_arr       : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- default no blocks are lost
  signal in_bsn_lost           : std_logic;  -- = in_bsn_lost_arr(c_lost_bsn_stream_id)
  signal exp_bsn_lost_arr      : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- default no blocks are lost

  signal ref_siso_arr          : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal ref_sosi_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0);  -- generated stimuli
  signal rx_sosi_arr           : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);  -- received stimuli
  signal in_sosi_arr           : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);  -- input stimuli

  signal in_sync_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
  signal in_sop_arr            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal in_eop_arr            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal in_val_arr            : std_logic_vector(g_nof_streams - 1 downto 0);
  signal in_data_arr           : t_data_arr;
  signal in_bsn_arr            : t_bsn_arr;
  signal in_channel_arr        : t_channel_arr;
  signal in_err_arr            : t_err_arr;

  signal mm_copi_arr           : t_mem_copi_arr(g_nof_streams - 1 downto 0);
  signal mm_copi               : t_mem_copi;  -- read access to output block, all output streams share same mm_copi
  signal mm_cipo_arr           : t_mem_cipo_arr(g_nof_streams - 1 downto 0);
  signal mm_sosi               : t_dp_sosi;  -- streaming information that signals that an output block can be read
  signal mm_done_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mm_done               : std_logic;
  signal mm_sosi_arr           : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal dut_in_sosi_2arr      : t_dut_sosi_2arr(0 to c_nof_aligners_max - 1);
  signal dut_out_sosi_2arr     : t_dut_sosi_2arr(0 to c_nof_aligners_max - 1);
  signal dut_sosi_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0);  -- last BSN aligner output
  signal r                     : t_reg;
  signal nxt_r                 : t_reg;

  signal out_sosi_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal out_sosi              : t_dp_sosi;

  signal out_sync_arr          : std_logic_vector(g_nof_streams - 1 downto 0);
  signal out_sop_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
  signal out_eop_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
  signal out_val_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
  signal out_data_arr          : t_data_arr;
  signal hold_data_arr         : t_data_arr;
  signal out_bsn_arr           : t_bsn_arr;
  signal out_bsn               : integer;
  signal out_channel_arr       : t_channel_arr;
  signal out_err_arr           : t_err_arr;

  signal tb_state              : t_tb_state;
  signal restart_cnt_arr       : t_nat_integer_arr(g_nof_streams - 1 downto 0) := (others => -1);
  signal restart_cnt           : integer := 0;
  signal ref_sosi_arr_dly      : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_sosi_arr_exp      : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_sosi_exp          : t_dp_sosi;
  signal verify_done_arr       : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
  signal verify_sosi_en_arr    : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');

  signal hold_out_sop_arr         : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');
  signal expected_out_bsn_arr     : t_bsn_arr;
  signal expected_out_data_arr    : t_data_arr;
  signal expected_out_channel_arr : t_channel_arr;

  -- Debug signals to view in Wave window
  signal dbg_func_delay_max             : natural := func_input_delay(g_nof_streams - 1);

  signal dbg_c_align_latency_nof_blocks : natural := c_align_latency_nof_blocks;
  signal dbg_c_align_latency_nof_valid  : natural := c_align_latency_nof_valid;
  signal dbg_c_align_latency_nof_clk    : natural := c_align_latency_nof_clk;
  signal dbg_c_total_latency            : natural := c_total_latency;
  signal dbg_c_verify_nof_blocks        : natural := c_verify_nof_blocks;

  -- Debug signals to view that verification conditions actually occur
  signal dbg_verify_sosi_control_arr    : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- '1' when sosi control is verified
  signal dbg_verify_passed_on_data_arr  : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- '1' when passed on data is verified
  signal dbg_verify_replaced_data_arr   : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- '1' when replaced data is verified
  signal dbg_verify_bsn_arr             : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- '1' when bsn in all streams is verified
  signal dbg_verify_no_lost_flag_arr    : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- '1' when lost data flag = 0 is verified
  signal dbg_verify_lost_flag_arr       : std_logic_vector(g_nof_streams - 1 downto 0) := (others => '0');  -- '1' when lost data flag = 1 is verified
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  gen_stimuli : for I in g_nof_streams - 1 downto 0 generate
    p_stimuli : process
      variable v_sync      : std_logic := '0';
      variable v_bsn       : natural;
      variable v_data      : natural := c_data_init;
      variable v_channel   : natural := c_channel_init;
      variable v_err       : natural := c_err_init;
    begin
      v_data := v_data + I;
      ref_sosi_arr(I) <= c_dp_sosi_rst;
      proc_common_wait_until_low(clk, rst);
      proc_common_wait_some_cycles(clk, 10);
      restart_cnt_arr(I) <= restart_cnt_arr(I) + 1;

      -- Begin of stimuli
      for S in 0 to g_tb_nof_restart - 1 loop
        v_bsn := g_bsn_init;
        for R in 0 to g_tb_nof_blocks - 1 loop
          v_sync := sel_a_b(v_bsn mod c_sync_period = c_sync_offset, '1', '0');
          proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data, 0, 0, g_block_size, v_channel, v_err, v_sync, TO_UVEC(v_bsn, c_bsn_w), clk, sl1, ref_siso_arr(I), ref_sosi_arr(I));
          v_bsn  := v_bsn + 1;
          v_data := v_data + g_block_size;
          proc_common_wait_some_cycles(clk, c_gap_size);  -- create gap between frames
        end loop;
        -- no gap between restarts, to ease verification by maintaining fixed
        -- latency of out_sosi_arr_exp
        restart_cnt_arr(I) <= restart_cnt_arr(I) + 1;
      end loop;

      -- End of stimuli
      -- . default g_bsn_latency_max blocks remain in DUT buffer
      expected_out_bsn_arr(I) <= TO_UVEC(v_bsn - 1 - c_align_latency_nof_blocks, c_bsn_w);
      expected_out_data_arr(I) <= TO_UVEC(v_data - 1 - c_align_latency_nof_valid, c_data_w);
      -- . default no data is lost, so all channel(0) lost data flags are 0
      expected_out_channel_arr(I) <= TO_DP_CHANNEL(0);

      -- Account for disturbed remote input streams
      if I > 0 then
        if I = g_disable_stream_id then
          -- Expected stream disable replacement data value
          expected_out_data_arr(I) <= TO_UVEC(g_data_replacement_value, c_data_w);
        end if;
        if I = g_lost_stream_id then
          -- Expected stream lost replacement data value and expected lost
          -- flag channel(0) value
          expected_out_data_arr(I) <= TO_UVEC(g_data_replacement_value, c_data_w);
          expected_out_channel_arr(I) <= TO_DP_CHANNEL(1);
        end if;
      end if;

      proc_common_wait_some_cycles(clk, 100);
      verify_done_arr(I) <= '1';
      proc_common_wait_some_cycles(clk, 1);
      verify_done_arr(I) <= '0';

      tb_end_arr(I) <= '1';
      wait;
    end process;
  end generate;

  tb_end <= vector_and(tb_end_arr);

  -- Model misalignment latency between the input streams.
  -- . it is sufficient to only model misalignment for the first DUT in case
  --   c_nof_aligners_max > 1.
  gen_rx_sosi_arr : for I in g_nof_streams - 1 downto 0 generate
    rx_sosi_arr(I) <= transport ref_sosi_arr(I) after func_input_delay(I) * clk_period;
  end generate;

  -- Model enable/disable remote input stream
  stream_en_arr(g_disable_stream_id) <= '0' when g_disable_stream_id > 0;

  -- Model lost remote input stream
  stream_lost_arr(g_lost_stream_id) <= '1' when g_lost_stream_id > 0;

  -- Model lost block on one remote input stream c_lost_bsn_stream_id
  in_bsn_lost_arr(c_lost_bsn_stream_id) <= '1' when TO_UINT(rx_sosi_arr(c_lost_bsn_stream_id).bsn) = g_lost_bsn_id else '0';
  in_bsn_lost <= in_bsn_lost_arr(c_lost_bsn_stream_id);

  p_in_sosi_arr : process(rx_sosi_arr, stream_lost_arr, in_bsn_lost_arr)
  begin
    in_sosi_arr <= rx_sosi_arr;
    -- Model entirely lost remote input stream
    if stream_lost_arr(g_lost_stream_id) = '1' then
      in_sosi_arr(g_lost_stream_id) <= RESET_DP_SOSI_CTRL(rx_sosi_arr(g_lost_stream_id));
    end if;
    -- Model single lost block in a stream (stream c_lost_bsn_stream_id = 1)
    if in_bsn_lost_arr(c_lost_bsn_stream_id) = '1' then
      in_sosi_arr(c_lost_bsn_stream_id) <= RESET_DP_SOSI_CTRL(rx_sosi_arr(c_lost_bsn_stream_id));
    end if;
  end process;

  -- Use tb_state to view tb progress in Wave window
  restart_cnt <= restart_cnt_arr(0);

  p_tb_state : process(restart_cnt)
  begin
    tb_state <= s_idle;
    if restart_cnt = 0 then tb_state <= s_start; end if;
    if restart_cnt = 1 then tb_state <= s_restart; end if;
    if restart_cnt > 1 then tb_state <= s_restart; end if;
  end process;

  mon_sosi : for I in g_nof_streams - 1 downto 0 generate
    -- Ease in_sosi_arr monitoring
    in_sync_arr(I)    <= in_sosi_arr(I).sync;
    in_sop_arr(I)     <= in_sosi_arr(I).sop;
    in_eop_arr(I)     <= in_sosi_arr(I).eop;
    in_val_arr(I)     <= in_sosi_arr(I).valid;
    in_data_arr(I)    <= in_sosi_arr(I).data(c_data_w - 1 downto 0);
    in_bsn_arr(I)     <= in_sosi_arr(I).bsn(c_bsn_w - 1 downto 0);
    in_channel_arr(I) <= in_sosi_arr(I).channel;
    in_err_arr(I)     <= in_sosi_arr(I).err;

    -- Ease out_sosi_arr monitoring and verification
    out_sync_arr(I)    <= out_sosi_arr(I).sync;
    out_sop_arr(I)     <= out_sosi_arr(I).sop;
    out_eop_arr(I)     <= out_sosi_arr(I).eop;
    out_val_arr(I)     <= out_sosi_arr(I).valid;
    out_data_arr(I)    <= out_sosi_arr(I).data(c_data_w - 1 downto 0);
    out_bsn_arr(I)     <= out_sosi_arr(I).bsn(c_bsn_w - 1 downto 0);
    out_channel_arr(I) <= out_sosi_arr(I).channel;
    out_err_arr(I)     <= out_sosi_arr(I).err;
  end generate;

  out_sosi <= out_sosi_arr(0);  -- take out_sosi control and info from out_sosi_arr(0)

  out_bsn <= TO_UINT(out_sosi.bsn);  -- = out_bsn_arr().bsn = out_sosi_arr(I).bsn

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION, use multiple ways to increase coverage
  -- a) Use proc_dp_verify_*() to verify output sosi format
  -- b) Use delayed in_sosi_arr as expected out_sosi_arr to verify output sosi
  ------------------------------------------------------------------------------

  ref_sosi_arr_dly <= transport ref_sosi_arr after c_total_latency * clk_period;
  out_sosi_arr_exp <= ref_sosi_arr_dly when rising_edge(clk);
  out_sosi_exp <= out_sosi_arr_exp(0);  -- take out_sosi_exp control and info from out_sosi_arr_exp(0)

  gen_verify_ctrl : for I in g_nof_streams - 1 downto 0 generate
    -- . Verify that sop and eop come in pairs
    proc_dp_verify_sop_and_eop(clk, out_val_arr(I), out_sop_arr(I), out_eop_arr(I), hold_out_sop_arr(I));

    -- . No data verification here, using p_verify_sosi is easier than using proc_dp_verify_data().

    -- . Verify that the stimuli have been applied at all
    hold_data_arr(I) <= out_data_arr(I) when out_val_arr(I) = '1';  -- hold last valid data
    proc_dp_verify_value("out_data_arr", e_equal, clk, verify_done_arr(I), expected_out_data_arr(I), hold_data_arr(I));
    proc_dp_verify_value("out_bsn_arr", e_equal, clk, verify_done_arr(I), expected_out_bsn_arr(I), out_bsn_arr(I));
    proc_dp_verify_value("out_channel_arr", e_equal, clk, verify_done_arr(I), expected_out_channel_arr(I), out_channel_arr(I));
  end generate;

  -- . Use delayed in_sosi_arr as expected out_sosi_arr, this is possible
  --   because the DUT has no flow control and has a fixed latency.
  p_verify_sosi_en_arr : process(out_sosi_exp)
  begin
    if c_diff_delay <= c_align_latency_nof_clk then
      verify_sosi_en_arr <= (others => '1');
      if TO_UINT(out_sosi_exp.bsn) - g_bsn_init >= c_verify_nof_blocks then
        verify_sosi_en_arr <= (others => '0');
      end if;
    end if;
  end process;

  exp_bsn_lost_arr(c_lost_bsn_stream_id) <= '1' when TO_UINT(out_sosi_arr_exp(c_lost_bsn_stream_id).bsn) = g_lost_bsn_id else '0';

  gen_verify_streams : for I in g_nof_streams - 1 downto 0 generate
    p_verify_stream : process(clk)
    begin
      if rising_edge(clk) then
        dbg_verify_sosi_control_arr(I) <= '0';
        dbg_verify_passed_on_data_arr(I) <= '0';
        dbg_verify_replaced_data_arr(I) <= '0';
        dbg_verify_bsn_arr(I) <= '0';
        dbg_verify_no_lost_flag_arr(I) <= '0';
        dbg_verify_lost_flag_arr(I) <= '0';
        if verify_sosi_en_arr(I) = '1' and out_sosi_arr_exp(I).valid = '1' then
           -- Verify sosi control fields
           dbg_verify_sosi_control_arr(I) <= '1';
           assert out_sosi_arr(I).sync = out_sosi_arr_exp(I).sync
             report "Wrong sync for output " & int_to_str(I)
             severity ERROR;
           assert out_sosi_arr(I).sop = out_sosi_arr_exp(I).sop
             report "Wrong sop for output " & int_to_str(I)
             severity ERROR;
           assert out_sosi_arr(I).eop = out_sosi_arr_exp(I).eop
             report "Wrong eop for output " & int_to_str(I)
             severity ERROR;
           assert out_sosi_arr(I).valid = out_sosi_arr_exp(I).valid
             report "Wrong valid for output " & int_to_str(I)
             severity ERROR;

           -- Verify data field
           if stream_en_arr(I) = '1' and stream_lost_arr(I) = '0' and exp_bsn_lost_arr(I) = '0' then
             -- verify passed on data
             dbg_verify_passed_on_data_arr(I) <= '1';
             assert out_sosi_arr(I).data = out_sosi_arr_exp(I).data
               report "Wrong data for output stream " & int_to_str(I) & " : "
                      & int_to_str(TO_UINT(out_sosi_arr(I).data)) & " /= "
                      & int_to_str(TO_UINT(out_sosi_arr_exp(I).data))
               severity ERROR;
           else
             -- verify lost data stream at g_disable_stream_id or g_lost_stream_id or g_lost_bsn_id
             dbg_verify_replaced_data_arr(I) <= '1';
             assert TO_UINT(out_sosi_arr(I).data) = g_data_replacement_value
               report "Wrong replacement data for output stream " & int_to_str(I) & " : "
                      & int_to_str(TO_UINT(out_sosi_arr(I).data)) & " /= "
                      & int_to_str(g_data_replacement_value)
               severity ERROR;
           end if;

           -- Verify sop info fields
           if out_sosi_arr_exp(I).sop = '1' then
             -- bsn field
             dbg_verify_bsn_arr(I) <= '1';
             assert out_sosi_arr(I).bsn = out_sosi_arr_exp(I).bsn
               report "Wrong bsn for output " & int_to_str(I)
               severity ERROR;

             -- channel field with lost flag bit 0
             if stream_lost_arr(I) = '0' and exp_bsn_lost_arr(I) = '0' then
               -- verify no lost stream
               dbg_verify_no_lost_flag_arr(I) <= '1';
               assert out_sosi_arr(I).channel = TO_DP_CHANNEL(0)
                 report "Wrong lost flag bit in channel /= 0 for output " & int_to_str(I)
                 severity ERROR;
             else
               -- verify lost stream g_lost_stream_id or lost block g_lost_bsn_id
               dbg_verify_lost_flag_arr(I) <= '1';
               assert out_sosi_arr(I).channel = TO_DP_CHANNEL(1)
                 report "Wrong lost flag bit channel /= 1 for output " & int_to_str(I)
                 severity ERROR;
             end if;
           end if;
        end if;
      end if;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  -- Connect all inputs to first DUT
  dut_in_sosi_2arr(0) <= in_sosi_arr;

  u_bsn_align : entity work.dp_bsn_align_v2
  generic map (
    g_nof_streams                => g_nof_streams,
    g_bsn_latency_max            => g_bsn_latency_max,
    g_bsn_latency_first_node     => g_bsn_latency_first_node,
    g_nof_aligners_max           => c_nof_aligners_max,
    g_block_size                 => g_block_size,
    g_bsn_w                      => g_bsn_w,
    g_data_w                     => g_data_w,
    g_data_replacement_value     => g_data_replacement_value,
    g_use_mm_output              => g_use_mm_output,
    g_pipeline_input             => g_pipeline_input,
    g_pipeline_output            => g_pipeline_output,
    g_rd_latency                 => g_rd_latency
  )
  port map (
    dp_rst         => rst,
    dp_clk         => clk,
    -- Control
    chain_node_index => chain_node_index_arr(0),
    stream_en_arr    => stream_en_arr,
    -- Streaming input
    in_sosi_arr    => dut_in_sosi_2arr(0),
    -- Output via local MM interface in dp_clk domain
    mm_copi        => mm_copi,
    mm_cipo_arr    => mm_cipo_arr,
    mm_sosi        => mm_sosi,

    -- Output via streaming DP interface
    out_sosi_arr   => dut_out_sosi_2arr(0)
  );

  -- Simulate series of DUT, when g_use_mm_output = FALSE and
  -- g_nof_aligners_max > 1. Use same local in_sosi_arr(0) input for all BSN
  -- aligners, because all DUT have same local reference. Connect the remote
  -- in_sosi_arr(> 0) inputs via the BSN aligners:
  --
  --   remote
  --   in_sosi_arr(> 0) ------> DUT --------> DUT --------> DUT --> dut_sosi_arr()(> 0)
  --                        /->  0  --X   /->  1  --X   /->  2  --> dut_sosi_arr()(= 0)
  --   local               /             /             / .      .
  --   in_sosi_arr(= 0) --/-------------/-------------/  .      .
  --                         .      .      .      .      .      .
  --                         .      .      .      .      .      .
  --                         v      .      .      .      .      .
  --        dut_in_sosi_2arr(0)     v      .      .      .      .
  --              dut_out_sosi_2arr(0)     v      .      .      .
  --                      dut_in_sosi_2arr(1)     v      .      .
  --                            dut_out_sosi_2arr(1)     v      .
  --                                    dut_in_sosi_2arr(2)     v
  --                                          dut_out_sosi_2arr(2)
  --
  gen_bsn_align_chain : for I in 1 to c_nof_aligners_max - 1 generate
    u_bsn_align : entity work.dp_bsn_align_v2
    generic map (
      g_nof_streams                => g_nof_streams,
      g_bsn_latency_max            => g_bsn_latency_max,
      g_bsn_latency_first_node     => g_bsn_latency_first_node,
      g_nof_aligners_max           => c_nof_aligners_max,
      g_block_size                 => g_block_size,
      g_bsn_w                      => g_bsn_w,
      g_data_w                     => g_data_w,
      g_data_replacement_value     => g_data_replacement_value,
      g_use_mm_output              => g_use_mm_output,
      g_pipeline_input             => g_pipeline_input,
      g_pipeline_output            => g_pipeline_output,
      g_rd_latency                 => g_rd_latency
    )
    port map (
      dp_rst         => rst,
      dp_clk         => clk,
      -- Control
      chain_node_index => chain_node_index_arr(I),
      stream_en_arr    => stream_en_arr,
      -- Streaming input
      in_sosi_arr    => dut_in_sosi_2arr(I),
      -- Output via streaming DP interface
      out_sosi_arr   => dut_out_sosi_2arr(I)
    );

    -- Connect remote and local between DUTs in the chain of DUTs
    p_connect : process(dut_out_sosi_2arr, in_sosi_arr)
    begin
      -- connect the remote inputs, by connecting all inputs from previous DUT
      dut_in_sosi_2arr(I) <= dut_out_sosi_2arr(I - 1);

      -- connect the local input, same for all DUTs
      dut_in_sosi_2arr(I)(0) <= in_sosi_arr(0);
    end process;
  end generate;

  -- Connect output from last DUT, so only verify last output
  dut_sosi_arr <= dut_out_sosi_2arr(c_nof_aligners_max - 1);

  ------------------------------------------------------------------------------
  -- MM to streaming DP
  ------------------------------------------------------------------------------
  no_use_mm_output : if not g_use_mm_output generate
    out_sosi_arr <= dut_sosi_arr;
  end generate;

  gen_use_mm_output : if g_use_mm_output generate
    mm_copi <= mm_copi_arr(0);
    mm_done <= mm_done_arr(0);  -- for viewing only

    gen_mm_to_dp : for I in 0 to g_nof_streams - 1 generate
      u_mm_to_dp: entity work.dp_block_from_mm
      generic map (
        g_user_size          => 1,
        g_data_size          => 1,
        g_step_size          => 1,
        g_nof_data           => g_block_size,
        g_word_w             => g_data_w,
        g_mm_rd_latency      => g_rd_latency,
        g_reverse_word_order => false
      )
      port map (
        rst           => rst,
        clk           => clk,
        start_pulse   => mm_sosi.sop,
        start_address => 0,
        mm_done       => mm_done_arr(I),
        mm_mosi       => mm_copi_arr(I),
        mm_miso       => mm_cipo_arr(I),
        out_sosi      => mm_sosi_arr(I),
        out_siso      => c_dp_siso_rdy
      );
    end generate;

    p_comb : process(r, mm_sosi, mm_sosi_arr)
      variable v : t_reg;
    begin
      v := r;

      -- hold mm_sosi.sync, bsn
      if mm_sosi.sop = '1' then
        v.sync := mm_sosi.sync;
        v.bsn  := mm_sosi.bsn(g_bsn_w - 1 downto 0);
      end if;

      -- apply mm_sosi.sync, bsn at sop to all streams in out_sosi_arr
      v.out_sosi_arr := mm_sosi_arr;
      if mm_sosi_arr(0).sop = '1' then
        v.out_sosi_arr := func_dp_stream_arr_set(v.out_sosi_arr, r.sync, "SYNC");
        v.out_sosi_arr := func_dp_stream_arr_set(v.out_sosi_arr, r.bsn, "BSN");
      else
        -- hold sosi.bsn until next sop, to easy view in wave window
        for I in 0 to g_nof_streams - 1 loop
          v.out_sosi_arr(I).bsn := r.out_sosi_arr(I).bsn;
        end loop;
      end if;

      -- next state
      nxt_r <= v;
    end process;

    p_reg : process(clk)
    begin
      if rising_edge(clk) then
        r <= nxt_r;
      end if;
    end process;

    out_sosi_arr <= nxt_r.out_sosi_arr;
  end generate;
end tb;
