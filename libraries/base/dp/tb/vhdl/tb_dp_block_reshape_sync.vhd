-------------------------------------------------------------------------------
--
-- Copyright (C) 2018
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--   Eric Kooistra, 26 Apr 2018
-- Purpose:
-- . Test bench for dp_block_reshape_sync.
-- Description:
--   Verify that the input sosi is the same as the output sosi when two
--   dp_block_reshape_sync components are used in series, whereby the second
--   reshapes the sop,eop block size back to the original input block
--   size. Block diagram:
--
--    stimuli --> dp_block_reshape_sync --> dp_block_reshape_sync --> verify
--
--   Pipelining and flow control are already covered by tb_dp_counter.vhd.
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_block_reshape_sync is
  generic (
    g_input_nof_data_per_blk   : natural :=  9;
    g_input_nof_blk_per_sync   : natural := 12;  -- 6 * 108 = 12 * 9
    g_reshape_nof_data_per_blk : natural := 18;
    g_reshape_nof_blk_per_sync : natural :=  3;  -- 12 *  54 =  3 * 18
    g_reshape_bsn              : boolean := true;  -- when TRUE reshape BSN else when FALSE pass on BSN
    g_pipeline_src_out         : natural := 1;
    g_pipeline_src_in          : natural := 0
  );
end tb_dp_block_reshape_sync;

architecture tb of tb_dp_block_reshape_sync is
  constant c_pipeline : natural := sel_a_b(g_pipeline_src_out + g_pipeline_src_in > 0, 1, 0);

  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_clk_period : time := 5 ns;

  constant c_nof_sync                 : natural :=  6;
  constant c_gap_size                 : natural :=  4;

  signal clk                  : std_logic := '1';
  signal rst                  : std_logic := '1';
  signal tb_end               : std_logic := '0';

  signal stimuli_end          : std_logic;
  signal stimuli_sosi         : t_dp_sosi;
  signal stimuli_siso         : t_dp_siso;
  signal prev_reshape_sosi    : t_dp_sosi;
  signal reshape_sosi         : t_dp_sosi;
  signal reshape_siso         : t_dp_siso;
  signal prev_verify_sosi     : t_dp_sosi;
  signal verify_sosi          : t_dp_sosi;
  signal verify_siso          : t_dp_siso := c_dp_siso_rdy;
  signal pipeline_dut_sosi    : t_dp_sosi;
  signal pipeline_dut_siso    : t_dp_siso := c_dp_siso_rdy;
  signal pipeline_total_sosi  : t_dp_sosi;
  signal pipeline_total_siso  : t_dp_siso := c_dp_siso_rdy;
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  -- Generate snk_in with data frames
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => g_input_nof_blk_per_sync,
    g_nof_repeat  => g_input_nof_blk_per_sync * c_nof_sync,
    g_pkt_len     => g_input_nof_data_per_blk,
    g_pkt_gap     => c_gap_size
  )
  port map (
    rst               => rst,
    clk               => clk,

    -- Generate stimuli
    src_in            => stimuli_siso,
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_reshape_dut : entity work.dp_block_reshape_sync
  generic map (
    g_reshape_nof_data_per_blk => g_reshape_nof_data_per_blk,
    g_reshape_nof_blk_per_sync => g_reshape_nof_blk_per_sync,
    g_reshape_bsn              => g_reshape_bsn,
    g_pipeline_src_out         => g_pipeline_src_out,
    g_pipeline_src_in          => g_pipeline_src_in
  )
  port map (
    clk         => clk,
    rst         => rst,

    snk_in      => stimuli_sosi,
    snk_out     => stimuli_siso,

    src_out     => reshape_sosi,
    src_in      => reshape_siso
  );

  u_reshape_back : entity work.dp_block_reshape_sync
  generic map (
    g_reshape_nof_data_per_blk => g_input_nof_data_per_blk,
    g_reshape_nof_blk_per_sync => g_input_nof_blk_per_sync,
    g_reshape_bsn              => g_reshape_bsn,
    g_pipeline_src_out         => g_pipeline_src_out,
    g_pipeline_src_in          => g_pipeline_src_in
  )
  port map (
    clk         => clk,
    rst         => rst,

    snk_in      => reshape_sosi,
    snk_out     => reshape_siso,

    src_out     => verify_sosi,
    src_in      => verify_siso
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  u_pipeline_dut : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_pipeline  -- = c_pipeline in u_reshape_dut
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => stimuli_sosi,
    -- ST source
    src_in      => pipeline_dut_siso,
    src_out     => pipeline_dut_sosi
  );

  u_pipeline_total : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_pipeline * 2  -- = c_pipeline in u_reshape_dut + c_pipeline in u_reshape_back
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => stimuli_sosi,
    -- ST source
    src_in      => pipeline_total_siso,
    src_out     => pipeline_total_sosi
  );

  prev_reshape_sosi <= reshape_sosi when rising_edge(clk);
  prev_verify_sosi <= verify_sosi when rising_edge(clk);

  p_verify_sosi : process(clk)
    variable v_global_bsn : natural;
    variable v_local_bsn  : natural;
  begin
    if rising_edge(clk) then
      -- verify all sosi fields
      assert verify_sosi.data = pipeline_total_sosi.data
        report "Wrong sosi.data "
        severity ERROR;
      assert verify_sosi.re = pipeline_total_sosi.re
        report "Wrong sosi.re "
        severity ERROR;
      assert verify_sosi.im = pipeline_total_sosi.im
        report "Wrong sosi.im "
        severity ERROR;
      assert verify_sosi.sync = pipeline_total_sosi.sync
        report "Wrong sosi.sync "
        severity ERROR;
      assert verify_sosi.valid = pipeline_total_sosi.valid
        report "Wrong sosi.valid "
        severity ERROR;
      assert verify_sosi.sop = pipeline_total_sosi.sop
        report "Wrong sosi.sop "
        severity ERROR;
      assert verify_sosi.eop = pipeline_total_sosi.eop
        report "Wrong sosi.eop "
        severity ERROR;

      if g_reshape_bsn = false then
        assert verify_sosi.bsn = pipeline_total_sosi.bsn
          report "Wrong sosi.bsn"
          severity ERROR;
      else
        if pipeline_dut_sosi.sync = '1' then
          v_global_bsn := TO_UINT(pipeline_dut_sosi.bsn);
        end if;
        if reshape_sosi.sync = '1' then
          assert TO_UINT(reshape_sosi.bsn) = v_global_bsn
            report "Wrong global sosi.bsn at sync"
            severity ERROR;
          v_local_bsn := 0;
        elsif reshape_sosi.sop = '1' then
          v_local_bsn := v_local_bsn + 1;
          assert TO_UINT(reshape_sosi.bsn) = v_local_bsn
            report "Wrong local sosi.bsn increment at sop"
            severity ERROR;
        end if;
      end if;

      assert verify_sosi.empty = pipeline_total_sosi.empty
        report "Wrong sosi.empty"
        severity ERROR;
      assert verify_sosi.err = pipeline_total_sosi.err
        report "Wrong sosi.err"
        severity ERROR;
    end if;
  end process;

  tb_end <= '0', stimuli_end after (1 + 10 * c_pipeline) * c_clk_period;
end tb;
