library IEEE, lpm, altera_mf, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

architecture stratix of pfs_fir_mac is
  component altmult_accum
  generic (
    intended_device_family         : string;
    width_a                        : natural;
    width_b                        : natural;
    representation_a               : string;
    representation_b               : string;
    lpm_type                       : string;
    width_result                   : natural;
    input_source_a                 : string;
    input_source_b                 : string;
    multiplier_rounding            : string;
    multiplier_saturation          : string;
    port_mult_is_saturated         : string;
    accumulator_rounding           : string;
    accumulator_saturation         : string;
    port_accum_is_saturated        : string;
    accum_direction                : string;
    input_reg_a                    : string;
    input_aclr_a                   : string;
    input_reg_b                    : string;
    input_aclr_b                   : string;
    multiplier_reg                 : string;
    multiplier_aclr                : string;
    accum_sload_reg                : string;
    accum_sload_pipeline_reg       : string;
    output_reg                     : string;
    output_aclr                    : string;
    dedicated_multiplier_circuitry : string
  );
  port (
    dataa                          : in  std_logic_vector(width_a - 1 downto 0);
    datab                          : in  std_logic_vector(width_b - 1 downto 0);
    accum_sload                    : in  std_logic;
    aclr0                          : in  std_logic;
    clock0                         : in  std_logic;
    result                         : out std_logic_vector(width_result - 1 downto 0)
  );
  end component;

  -- NOTE: although it appears otherwise, bit growth in the accumulation is accounted for!
  signal acc_out       : std_logic_vector(g_a_in_w + g_b_in_w - 1 downto 0);
begin
  registers : process (clk, rst)
  begin
    if rst = '1' then
      result <= (others => '0');
    elsif rising_edge(clk) then
      result <= acc_out(acc_out'high downto acc_out'length  - result'length);
    end if;
  end process;

  mac : altmult_accum
  generic map (
    intended_device_family         => c_rsp_device_family,
    width_a                        => g_a_in_w,
    width_b                        => g_b_in_w,
    representation_a               => "SIGNED",
    representation_b               => "SIGNED",
    lpm_type                       => "altmult_accum",
    width_result                   => g_a_in_w + g_b_in_w,
    input_source_a                 => "DATAA",
    input_source_b                 => "DATAB",
    multiplier_rounding            => "NO",
    multiplier_saturation          => "NO",
    port_mult_is_saturated         => "UNUSED",
    accumulator_rounding           => "NO",
    accumulator_saturation         => "NO",
    port_accum_is_saturated        => "UNUSED",
    accum_direction                => "ADD",
    input_reg_a                    => "CLOCK0",
    input_aclr_a                   => "ACLR0",
    input_reg_b                    => "CLOCK0",
    input_aclr_b                   => "ACLR0",
    multiplier_reg                 => "CLOCK0",
    multiplier_aclr                => "ACLR0",
    accum_sload_reg                => "CLOCK0",
    accum_sload_pipeline_reg       => "CLOCK0",
    output_reg                     => "CLOCK0",
    output_aclr                    => "ACLR0",
    dedicated_multiplier_circuitry => "YES"
  )
  port map (
    dataa                          => data_a,
    datab                          => data_b,
    accum_sload                    => res_clr,
    aclr0                          => rst,
    clock0                         => clk,
    result                         => acc_out
  );
end stratix;
