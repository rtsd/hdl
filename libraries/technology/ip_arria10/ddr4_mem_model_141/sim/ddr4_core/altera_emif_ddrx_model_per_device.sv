// (C) 2001-2014 Altera Corporation. All rights reserved.
// Your use of Altera Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License Subscription 
// Agreement, Altera MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the applicable 
// agreement for further details.


///////////////////////////////////////////////////////////////////////////////
// memory model per device in a given depth expansion
//
///////////////////////////////////////////////////////////////////////////////
module altera_emif_ddrx_model_per_device
    # (

   parameter PROTOCOL_ENUM                                                = "",
   parameter MEM_FORMAT_ENUM                                              = "",
   parameter MEM_RANKS_PER_DIMM                                           = 0,
   parameter MEM_NUM_OF_DIMMS                                             = 0,
   parameter MEM_AC_PAR_EN                                                = 0,
   parameter MEM_DM_EN                                                    = 0,

   parameter PORT_MEM_CKE_WIDTH                                           = 1,
   parameter PORT_MEM_BA_WIDTH                                            = 1,
   parameter PORT_MEM_BG_WIDTH                                            = 1,
   parameter PORT_MEM_C_WIDTH                                             = 1,
   parameter PORT_MEM_A_WIDTH                                             = 1,
   parameter PORT_MEM_CS_N_WIDTH                                          = 1,
   parameter PORT_MEM_RAS_N_WIDTH                                         = 1,
   parameter PORT_MEM_CAS_N_WIDTH                                         = 1,
   parameter PORT_MEM_WE_N_WIDTH                                          = 1,
   parameter PORT_MEM_ACT_N_WIDTH                                         = 1,
   parameter PORT_MEM_DQS_WIDTH                                           = 1,
   parameter PORT_MEM_DQS_N_WIDTH                                         = 1,
   parameter PORT_MEM_DQ_WIDTH                                            = 1,               
   parameter PORT_MEM_DM_WIDTH                                            = 1,
   parameter PORT_MEM_DBI_N_WIDTH                                         = 1,
   parameter PORT_MEM_RESET_N_WIDTH                                       = 1,
   parameter PORT_MEM_PAR_WIDTH                                           = 1,
   parameter PORT_MEM_ALERT_N_WIDTH                                       = 1,

   parameter MEM_ROW_ADDR_WIDTH                                           = 1,
   parameter MEM_COL_ADDR_WIDTH                                           = 1,
   parameter MEM_TRTP                                                     = 0,
   parameter MEM_TRCD                                                     = 0,
   parameter MEM_INIT_MRS0                                                = 0,
   parameter MEM_INIT_MRS1                                                = 0,
   parameter MEM_INIT_MRS2                                                = 0,
   parameter MEM_INIT_MRS3                                                = 0,
   parameter MEM_MIRROR_ADDRESSING_EN                                     = 0,
   parameter MEM_DEPTH_IDX                                                = -1,
   parameter MEM_CFG_GEN_SBE                                              = 0,
   parameter MEM_CFG_GEN_DBE                                              = 0
  )  (

   input  logic                        [PORT_MEM_A_WIDTH-1:0]            mem_a,
   input  logic                        [PORT_MEM_BA_WIDTH-1:0]           mem_ba,
   input  logic                        [PORT_MEM_BG_WIDTH-1:0]           mem_bg,
   input  logic                        [PORT_MEM_C_WIDTH-1:0]            mem_c,
   input  logic                                                          mem_ck,
   input  logic                                                          mem_ck_n,
   input  logic                        [PORT_MEM_CKE_WIDTH - 1:0]        mem_cke,
   input  logic                        [PORT_MEM_CS_N_WIDTH - 1:0]       mem_cs_n,
   input  logic                        [PORT_MEM_RAS_N_WIDTH - 1:0]      mem_ras_n,
   input  logic                        [PORT_MEM_CAS_N_WIDTH - 1:0]      mem_cas_n,
   input  logic                        [PORT_MEM_WE_N_WIDTH - 1:0]       mem_we_n,
   input  logic                        [PORT_MEM_ACT_N_WIDTH - 1:0]      mem_act_n,
   input  logic                        [PORT_MEM_RESET_N_WIDTH - 1:0]    mem_reset_n,
   input  logic                        [PORT_MEM_DM_WIDTH - 1:0]         mem_dm,
   inout  tri                          [PORT_MEM_DBI_N_WIDTH - 1:0]      mem_dbi_n,
   inout  tri                          [PORT_MEM_DQ_WIDTH - 1:0]         mem_dq,
   inout  tri                          [PORT_MEM_DQS_WIDTH - 1:0]        mem_dqs,
   inout  tri                          [PORT_MEM_DQS_N_WIDTH - 1:0]      mem_dqs_n,
   output logic                        [PORT_MEM_ALERT_N_WIDTH-1:0]      mem_alert_n,
   input  logic                        [PORT_MEM_PAR_WIDTH-1:0]          mem_par,
   input  logic                                                          mem_odt

  );
   timeunit 1ps;
   timeprecision 1ps;

   localparam MEM_CS_PER_RANK                                            = 1;
   localparam MEM_NUMBER_OF_RANKS                                        = ((MEM_RANKS_PER_DIMM == 0) ? (PORT_MEM_CS_N_WIDTH/MEM_CS_PER_RANK) : MEM_RANKS_PER_DIMM);

   localparam MEM_PHYS_RANKS                                             =  MEM_NUMBER_OF_RANKS;
   localparam PORT_MEM_DQ_A_WIDTH                                        = (MEM_PHYS_RANKS == 8) ? 72 : 40;
   localparam PORT_MEM_DQ_B_WIDTH                                        = (MEM_PHYS_RANKS == 8) ? 72 : (PORT_MEM_DQ_WIDTH - PORT_MEM_DQ_A_WIDTH);
   localparam PORT_MEM_DQS_A_WIDTH                                       = (MEM_PHYS_RANKS == 8) ? PORT_MEM_DQS_WIDTH : ((PORT_MEM_DQS_WIDTH * PORT_MEM_DQ_A_WIDTH) / PORT_MEM_DQ_WIDTH);
   localparam PORT_MEM_DQS_B_WIDTH                                       = (MEM_PHYS_RANKS == 8) ? PORT_MEM_DQS_WIDTH : (PORT_MEM_DQS_WIDTH - PORT_MEM_DQS_A_WIDTH);

   localparam MEM_DQ_A_U                                                 = PORT_MEM_DQ_A_WIDTH-1;
   localparam MEM_DQ_A_L                                                 = 0;
   localparam MEM_DQ_B_U                                                 = 71;
   localparam MEM_DQ_B_L                                                 = (MEM_PHYS_RANKS == 8) ? 0 : PORT_MEM_DQ_A_WIDTH;
   localparam MEM_DQS_A_U                                                = PORT_MEM_DQS_A_WIDTH-1;
   localparam MEM_DQS_A_L                                                = 0;
   localparam MEM_DQS_B_U                                                = (PORT_MEM_DQS_WIDTH-1);
   localparam MEM_DQS_B_L                                                = (MEM_PHYS_RANKS == 8) ? 0 : PORT_MEM_DQS_A_WIDTH;


   reg                                 [PORT_MEM_A_WIDTH-1:0]            a;
   reg                                 [PORT_MEM_BA_WIDTH-1:0]           ba;
   reg                                 [PORT_MEM_BG_WIDTH-1:0]           bg;
   reg                                 [PORT_MEM_C_WIDTH-1:0]            c;
   reg                                                                   ck;
   reg                                                                   ck_n;
   reg                                                                   cke;
   reg                                 [MEM_NUMBER_OF_RANKS-1:0]         cs_n;
   reg                                 [PORT_MEM_CS_N_WIDTH-1:0]         cs_rdimm_n;
   reg                                                                   ras_n;
   reg                                                                   cas_n;
   reg                                                                   we_n;
   reg                                                                   act_n;
   reg                                 [PORT_MEM_RESET_N_WIDTH-1:0]      reset_n;
   reg                                                                   odt;
   reg                                 [MEM_NUMBER_OF_RANKS-1:0]         alert_n;
   reg                                 [PORT_MEM_PAR_WIDTH-1:0]          par;
   reg                                                                   single_bit_alert_n;

   reg                                 [PORT_MEM_DM_WIDTH-1:0]           dm;

   generate

      if (MEM_FORMAT_ENUM == "MEM_FORMAT_RDIMM") begin
         
         always @(*) begin
            ras_n <= a[16];
            cas_n <= a[15];
            we_n  <= a[14];
         end
       
      end
      else begin

         always @(*) begin

            a <= mem_a;
            ba <= mem_ba;
            bg <= mem_bg;
            c <= mem_c;
            ck <= mem_ck;
            ck_n <= mem_ck_n;
            cke <= mem_cke;
            cs_n <= mem_cs_n;
            ras_n <= mem_ras_n;
            cas_n <= mem_cas_n;
            we_n <= mem_we_n;
            act_n <= mem_act_n;
            reset_n <= mem_reset_n;
            odt <= mem_odt;
            par <= mem_par;
            mem_alert_n <= single_bit_alert_n;

            if (MEM_DM_EN != 0) begin
               if ((PROTOCOL_ENUM != "PROTOCOL_DDR4") && (PORT_MEM_DM_WIDTH != PORT_MEM_DQS_WIDTH) || 
                   (PROTOCOL_ENUM == "PROTOCOL_DDR4") && (PORT_MEM_DBI_N_WIDTH != PORT_MEM_DQS_WIDTH)) begin
                  $display("Memory model DM width must equal DQS width.");
                  $finish;
               end
            end
            else
            begin
               dm <= #10 {PORT_MEM_DM_WIDTH{1'b0}};
            end

         end

      end

   endgenerate

   generate
      reg my_parity;
      reg [4:0] err_out_shiftreg = 5'b11111;
      if (MEM_AC_PAR_EN) begin
         always @(posedge mem_ck) begin
            if (mem_cke) begin
               my_parity <= ^{mem_a, mem_ba, mem_ras_n, mem_cas_n, mem_we_n};
               err_out_shiftreg[4:1] <= err_out_shiftreg[3:0];
               if (cs_n != {PORT_MEM_CS_N_WIDTH{1'b1}}) begin
                  err_out_shiftreg[1:0] <= {2{my_parity == mem_par}};
               end else begin
                  err_out_shiftreg[0] <= 1'b1;
               end
            end
         end
      end
      assign single_bit_alert_n = (PROTOCOL_ENUM == "PROTOCOL_DDR4" ? &alert_n : err_out_shiftreg[4]);
   endgenerate


   generate
      if ((PROTOCOL_ENUM == "PROTOCOL_DDR4") && (MEM_FORMAT_ENUM == "MEM_FORMAT_RDIMM")) begin : gen_ddr4_rcd_chip

         altera_emif_ddr4_model_rcd_chip ddr4_rcd_chip (
            .DCKE       (mem_cke),
            .DODT       (mem_odt),
            .DCS_n      (mem_cs_n),
            .DC         (mem_c),
            
            .DA         (mem_a),
            .DBA        (mem_ba),
            .DBG        (mem_bg),
            .DACT_n     (mem_act_n),
            
            .CK_t       (mem_ck),
            .CK_c       (mem_ck_n),
            
            .DRST_n     (mem_reset_n),
            
            .DPAR       (mem_par),
            
            .ERROR_IN_n (single_bit_alert_n),
            
            .BODT       (),
            .BCKE       (),
            .BCOM       (),
            .BCK_t      (),
            .BCK_c      (),
            .BVrefCA    (),
            
            .QACKE      (cke),
            .QBCKE      (),
            .QAODT      (odt),
            .QBODT      (),
            .QACS_n     (cs_n),
            .QBCS_n     (),
            .QAC        (c),
            .QBC        (),
            
            .QAA        (a),
            .QBA        (),
            .QABA       (ba),
            .QABG       (bg),
            .QBBA       (),
            .QBBG       (),
            .QAACT_n    (act_n),
            .QBACT_n    (),
            
            .Y_t        (ck),
            .Y_c        (ck_n),
            
            .QRST_n     (reset_n),
            
            .QAPAR      (par),
            .QBPAR      (),
            
            .ALERT_n    (mem_alert_n),
            
            .SDA        (),
            .SA         (),
            .SCL        (),
            .BFUNC      (),
            .VDDSPD     (),
            
            .VDD        (1'b1),
            .VSS        (1'b0),
            .AVDD       (1'b1),
            .PVDD       (1'b1),
            .PVSS       (1'b0)
         );
      end

      if ((PROTOCOL_ENUM == "PROTOCOL_DDR4") && (MEM_FORMAT_ENUM == "MEM_FORMAT_LRDIMM")) begin : gen_ddr4_db_chips
         initial
         begin
            $display("Simulation of DDR4 LRDIMM is not supported in this version of Quartus.");
            $finish;
         end
      end

      if ((PROTOCOL_ENUM == "PROTOCOL_DDR3") && (MEM_FORMAT_ENUM == "MEM_FORMAT_RDIMM")) begin : gen_ddr3_rdimm_chip
         initial
         begin
            $display("Simulation of DDR3 RDIMM is not supported in this version of Quartus.");
            $finish;
         end
      end

      if ((PROTOCOL_ENUM == "PROTOCOL_DDR3") && (MEM_FORMAT_ENUM == "MEM_FORMAT_LRDIMM")) begin : gen_ddr3_lrdimm_chip
         initial
         begin
            $display("Simulation of DDR3 LRDIMM is not supported in this version of Quartus.");
            $finish;
         end
      end

   endgenerate


   generate

      if (MEM_FORMAT_ENUM == "MEM_FORMAT_LRDIMM") begin : gen_lrdimm_mem
         // Simulation of LRDIMMs is not supported in this version of Quartus.
      end
      else begin : gen_mem_rank
         genvar rank;
         for (rank = 0; rank < MEM_NUMBER_OF_RANKS; rank = rank + 1)  begin : rank_gen
            altera_emif_ddrx_model_rank #(

               .PROTOCOL_ENUM                                 (PROTOCOL_ENUM),
               .PORT_MEM_BA_WIDTH                             (PORT_MEM_BA_WIDTH),
               .PORT_MEM_BG_WIDTH                             (PORT_MEM_BG_WIDTH),
               .PORT_MEM_C_WIDTH                              (PORT_MEM_C_WIDTH),
               .PORT_MEM_A_WIDTH                              (PORT_MEM_A_WIDTH),
               .MEM_ROW_ADDR_WIDTH                            (MEM_ROW_ADDR_WIDTH),
               .MEM_COL_ADDR_WIDTH                            (MEM_COL_ADDR_WIDTH),
               .PORT_MEM_DQS_WIDTH                            (PORT_MEM_DQS_WIDTH),
               .PORT_MEM_DQ_WIDTH                             (PORT_MEM_DQ_WIDTH),
               .PORT_MEM_DM_WIDTH                             (PORT_MEM_DM_WIDTH),
               .PORT_MEM_DBI_N_WIDTH                          (PORT_MEM_DBI_N_WIDTH),

               .MEM_DM_EN                                     (MEM_DM_EN),
               .MEM_TRTP                                      (MEM_TRTP),
               .MEM_TRCD                                      (MEM_TRCD),
               .MEM_INIT_MRS0                                 (MEM_INIT_MRS0),
               .MEM_INIT_MRS1                                 (MEM_INIT_MRS1),
               .MEM_INIT_MRS2                                 (MEM_INIT_MRS2),
               .MEM_INIT_MRS3                                 (MEM_INIT_MRS3),
               .MEM_MIRROR_ADDRESSING                         (MEM_MIRROR_ADDRESSING_EN & (rank & 1'b1)),
               .MEM_DEPTH_IDX                                 (MEM_DEPTH_IDX),
               .MEM_RANK_IDX                                  (rank),
               .MEM_CFG_GEN_SBE                               (MEM_CFG_GEN_SBE),
               .MEM_CFG_GEN_DBE                               (MEM_CFG_GEN_DBE)

            ) rank_inst (

               .mem_a         (a),
               .mem_ba        (ba),
               .mem_bg        (bg),
               .mem_c         (c),
               .mem_ck        (ck),
               .mem_ck_n      (ck_n),
               .mem_cke       (cke),
               .mem_ras_n     (ras_n),
               .mem_cas_n     (cas_n),
               .mem_we_n      (we_n),
               .mem_act_n     (act_n),
               .mem_reset_n   (reset_n),
               .mem_dm        (mem_dm),
               .mem_dbi_n     (mem_dbi_n),
               .mem_dq        (mem_dq),
               .mem_dqs       (mem_dqs),
               .mem_dqs_n     (mem_dqs_n),
               .mem_odt       (odt),
               .mem_cs_n      (cs_n[rank]),
               .mem_alert_n   (alert_n[rank]),
               .mem_par       (par)

            );
         end

      end

  endgenerate

endmodule
