--------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose : Fast simulation model for 10G XGMII over 4 lanes XAUI
-- Description :
-- Remark :
-- . The sim_xaui has the same entity ports and generics as tech_xaui
--   so that it can directly be mapped in tech_xaui.
-- . The model uses 10/8 overhead to transport the control signalling. All 4
--   XAUI lanes are used. Therefore the line rate per lane is 3.125 Mbps.

library IEEE, common_lib, tech_transceiver_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;

entity sim_xaui is
  generic(
    g_sim             : boolean := false;
    g_nof_xaui        : natural := 1
  );
  port (
    -- Transceiver PLL reference clock
    tr_clk                    : in  std_logic;  -- 156.25 MHz
    tr_rst                    : in  std_logic;

    -- Calibration & reconfig clock
    cal_rec_clk               : in  std_logic := '0';  -- not used in model, but available to have same port map as tech_xaui

    -- MM interface
    mm_clk                    : in  std_logic := '0';  -- not used in model, but available to have same port map as tech_xaui
    mm_rst                    : in  std_logic := '0';

    xaui_mosi                 : in  t_mem_mosi := c_mem_mosi_rst;
    xaui_miso                 : out t_mem_miso;

    -- XGMII interface
    tx_clk_arr                : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- 156.25 MHz, externally connect tr_clk to tx_clk_arr or connect rx_clk_arr to tx_clk_arr
    rx_clk_arr_out            : out std_logic_vector(g_nof_xaui - 1 downto 0);  -- recovered clock per XAUI, rx_clk_arr = tx_clk_arr in this model
    rx_clk_arr_in             : in  std_logic_vector(g_nof_xaui - 1 downto 0);  -- externally connect to rx_clk_arr_out to avoid clock delta-delay

    txc_tx_ready_arr          : out std_logic_vector(g_nof_xaui - 1 downto 0);
    rxc_rx_ready_arr          : out std_logic_vector(g_nof_xaui - 1 downto 0);

    txc_rx_channelaligned_arr : out std_logic_vector(g_nof_xaui - 1 downto 0);

    -- Serial I/O
    xgmii_tx_dc_arr           : in  t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);
    xgmii_rx_dc_arr           : out t_xgmii_dc_arr(g_nof_xaui - 1 downto 0);

    xaui_tx_arr               : out t_xaui_arr(g_nof_xaui - 1 downto 0);
    xaui_rx_arr               : in  t_xaui_arr(g_nof_xaui - 1 downto 0)
  );
end sim_xaui;

architecture wrap of sim_xaui is
  constant c_tr_clk_period         : time := 6.4 ns;  -- 156.25 MHz  --> model line rate : 156.25 MHz * 10/8 (encoding) * 64b (data width) / 4 (XAUI lanes) = 3125 Mbps / lane

  constant c_xaui_serdes_data_w    : natural := c_xgmii_data_w / c_nof_xaui_lanes;  -- = 16b = 64 b / 4
  constant c_xaui_serdes_ctrl_w    : natural := c_xgmii_ctrl_w / c_nof_xaui_lanes;  -- =  2b =  8 b / 4

  -- XGMII control bits (one for each XGMII lane):
  signal xgmii_tx_c_arr            : t_xgmii_c_arr(g_nof_xaui - 1 downto 0);
  signal xgmii_rx_c_arr            : t_xgmii_c_arr(g_nof_xaui - 1 downto 0);

  -- XGMII data
  signal xgmii_tx_d_arr            : t_xgmii_d_arr(g_nof_xaui - 1 downto 0);
  signal xgmii_rx_d_arr            : t_xgmii_d_arr(g_nof_xaui - 1 downto 0);
begin
  -- Model rx_clk = tx_clk = tr_clk
  rx_clk_arr_out <= tx_clk_arr;

  -- No MM model
  xaui_miso <= c_mem_miso_rst;

  gen_nof_xaui : for i in g_nof_xaui - 1 downto 0 generate
    -- Rewire XGMII
    xgmii_tx_d_arr(i) <= func_xgmii_d(xgmii_tx_dc_arr(i));
    xgmii_tx_c_arr(i) <= func_xgmii_c(xgmii_tx_dc_arr(i));

    xgmii_rx_dc_arr(i) <= func_xgmii_dc(xgmii_rx_d_arr(i), xgmii_rx_c_arr(i));

    -- Model tx_ready
    u_areset_tx_rdy : entity common_lib.common_areset
    generic map(
      g_rst_level => '0',
      g_delay_len => 40
    )
    port map(
      clk     => tx_clk_arr(i),
      in_rst  => tr_rst,
      out_rst => txc_tx_ready_arr(i)
    );

    -- Model rx_ready
    u_areset_rx_rdy : entity common_lib.common_areset
    generic map(
      g_rst_level => '0',
      g_delay_len => 80
    )
    port map(
      clk     => rx_clk_arr_in(i),
      in_rst  => tr_rst,
      out_rst => rxc_rx_ready_arr(i)
    );

    -- Model rx_channelaligned
    u_areset_rx_channelaligned : entity common_lib.common_areset
    generic map(
      g_rst_level => '0',
      g_delay_len => 120
    )
    port map(
      clk     => tx_clk_arr(i),
      in_rst  => '0',
      out_rst => txc_rx_channelaligned_arr(i)
    );

    gen_serdes: for j in c_nof_xaui_lanes - 1 downto 0 generate
      u_ser: entity tech_transceiver_lib.sim_transceiver_serializer
      generic map (
        g_data_w        => c_xaui_serdes_data_w,
        g_tr_clk_period => c_tr_clk_period
      )
      port map (
        tr_clk             => tr_clk,
        tr_rst             => tr_rst,

        tx_in_data         => xgmii_tx_d_arr(i)(j * c_xaui_serdes_data_w + c_xaui_serdes_data_w - 1 downto j * c_xaui_serdes_data_w),
        tx_in_ctrl         => xgmii_tx_c_arr(i)(j * c_xaui_serdes_ctrl_w + c_xaui_serdes_ctrl_w - 1 downto j * c_xaui_serdes_ctrl_w),

        tx_serial_out      => xaui_tx_arr(i)(j)
      );

      u_des: entity tech_transceiver_lib.sim_transceiver_deserializer
      generic map (
        g_data_w        => c_xaui_serdes_data_w,
        g_tr_clk_period => c_tr_clk_period
      )
      port map (
        tr_clk             => tr_clk,
        tr_rst             => tr_rst,

        rx_out_data        => xgmii_rx_d_arr(i)(j * c_xaui_serdes_data_w + c_xaui_serdes_data_w - 1 downto j * c_xaui_serdes_data_w),
        rx_out_ctrl        => xgmii_rx_c_arr(i)(j * c_xaui_serdes_ctrl_w + c_xaui_serdes_ctrl_w - 1 downto j * c_xaui_serdes_ctrl_w),

        rx_serial_in       => xaui_rx_arr(i)(j)
      );
    end generate;

  end generate;
end wrap;
