-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Provide AXI-4-stream interfaces + standard avalon MM interfaces for
--          eth_tester.vhd such that it can be used to create a Vivado IP block.
-- Description:
-- . The rdma_generator_eth_tester_wrapper uses axi4_stream_dp_bridge to convert the dp
--   sosi/siso interfaces of the eth_tester into AXI4-Stream interfaces.
-- . In order for this component to be suitable as a Vivado IP, the ports are
--   exclusively STD_LOGIC(_VECTOR) where the widths are hard-coded as demanded
--   by the Vivado IP creator (only supports VHDL-93).
-- Remark
-- . Avalon is used for all MM interfaces, which can be bridged to AXI4-Lite in
--   vivado using the AXI AMM Bridge IP.

library IEEE, common_lib, dp_lib, axi4_lib, eth_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_components_pkg.all;
use axi4_lib.axi4_stream_pkg.all;
use eth_lib.eth_pkg.all;
use eth_lib.eth_tester_pkg.all;
use work.rdma_generator_pkg.all;

entity rdma_generator_eth_tester_wrapper is
  port (
    -- Clocks and reset
    mm_clk             : in  std_logic;
    st_clk             : in  std_logic;
    st_pps             : in  std_logic;
    aresetn            : in  std_logic;
    -- UDP transmit interface
    eth_src_mac        : in  std_logic_vector(6 * 8 - 1 downto 0);
    ip_src_addr        : in  std_logic_vector(4 * 8 - 1 downto 0);
    udp_src_port       : in  std_logic_vector(2 * 8 - 1 downto 0);

    tx_fifo_rd_emp_arr : out std_logic_vector(0 downto 0);

    -- tx_udp
    -- Source In and Sink Out
    tx_udp_tready    : in std_logic;

    -- Source Out and Sink In
    tx_udp_tvalid    : out std_logic;
    tx_udp_tdata     : out std_logic_vector(512 - 1 downto 0);
    tx_udp_tstrb     : out std_logic_vector(512 / 8 - 1 downto 0);
    tx_udp_tkeep     : out std_logic_vector(512 / 8 - 1 downto 0);
    tx_udp_tlast     : out std_logic;
    tx_udp_tid       : out std_logic_vector(4 - 1 downto 0);
    tx_udp_tdest     : out std_logic_vector(32 - 1 downto 0);
    tx_udp_tuser     : out std_logic_vector(70 - 1 downto 0);

    -- rx_udp
    -- Source In and Sink Out
    rx_udp_tready    : out std_logic;

    -- Source Out and Sink In
    rx_udp_tvalid    : in std_logic;
    rx_udp_tdata     : in std_logic_vector(512 - 1 downto 0);
    rx_udp_tstrb     : in std_logic_vector(512 / 8 - 1 downto 0);
    rx_udp_tkeep     : in std_logic_vector(512 / 8 - 1 downto 0);
    rx_udp_tlast     : in std_logic;
    rx_udp_tid       : in std_logic_vector(4 - 1 downto 0);
    rx_udp_tdest     : in std_logic_vector(32 - 1 downto 0);
    rx_udp_tuser     : in std_logic_vector(70 - 1 downto 0);

    -- reg_bg_ctrl
    reg_bg_ctrl_avs_address                     : in  std_logic_vector(32 - 1 downto 0);
    reg_bg_ctrl_avs_read                        : in  std_logic;
    reg_bg_ctrl_avs_readdata                    : out std_logic_vector(32 - 1 downto 0);
    reg_bg_ctrl_avs_readdatavalid               : out std_logic;
    reg_bg_ctrl_avs_waitrequest                 : out std_logic;
    reg_bg_ctrl_avs_write                       : in  std_logic;
    reg_bg_ctrl_avs_writedata                   : in  std_logic_vector(32 - 1 downto 0);

    -- reg_hdr_dat
    reg_hdr_dat_avs_address                     : in  std_logic_vector(32 - 1 downto 0);
    reg_hdr_dat_avs_read                        : in  std_logic;
    reg_hdr_dat_avs_readdata                    : out std_logic_vector(32 - 1 downto 0);
    reg_hdr_dat_avs_readdatavalid               : out std_logic;
    reg_hdr_dat_avs_waitrequest                 : out std_logic;
    reg_hdr_dat_avs_write                       : in  std_logic;
    reg_hdr_dat_avs_writedata                   : in  std_logic_vector(32 - 1 downto 0);

    -- reg_bsn_monitor_v2_tx
    reg_bsn_monitor_v2_tx_avs_address           : in  std_logic_vector(32 - 1 downto 0);
    reg_bsn_monitor_v2_tx_avs_read              : in  std_logic;
    reg_bsn_monitor_v2_tx_avs_readdata          : out std_logic_vector(32 - 1 downto 0);
    reg_bsn_monitor_v2_tx_avs_readdatavalid     : out std_logic;
    reg_bsn_monitor_v2_tx_avs_waitrequest       : out std_logic;
    reg_bsn_monitor_v2_tx_avs_write             : in  std_logic;
    reg_bsn_monitor_v2_tx_avs_writedata         : in  std_logic_vector(32 - 1 downto 0);

    -- reg_strobe_total_count_tx
    reg_strobe_total_count_tx_avs_address       : in  std_logic_vector(32 - 1 downto 0);
    reg_strobe_total_count_tx_avs_read          : in  std_logic;
    reg_strobe_total_count_tx_avs_readdata      : out std_logic_vector(32 - 1 downto 0);
    reg_strobe_total_count_tx_avs_readdatavalid : out std_logic;
    reg_strobe_total_count_tx_avs_waitrequest   : out std_logic;
    reg_strobe_total_count_tx_avs_write         : in  std_logic;
    reg_strobe_total_count_tx_avs_writedata     : in  std_logic_vector(32 - 1 downto 0);

    -- reg_dp_split
    reg_dp_split_avs_address                    : in  std_logic_vector(32 - 1 downto 0);
    reg_dp_split_avs_read                       : in  std_logic;
    reg_dp_split_avs_readdata                   : out std_logic_vector(32 - 1 downto 0);
    reg_dp_split_avs_readdatavalid              : out std_logic;
    reg_dp_split_avs_waitrequest                : out std_logic;
    reg_dp_split_avs_write                      : in  std_logic;
    reg_dp_split_avs_writedata                  : in  std_logic_vector(32 - 1 downto 0);

    -- reg_bsn_monitor_v2_rx
    reg_bsn_monitor_v2_rx_avs_address           : in  std_logic_vector(32 - 1 downto 0);
    reg_bsn_monitor_v2_rx_avs_read              : in  std_logic;
    reg_bsn_monitor_v2_rx_avs_readdata          : out std_logic_vector(32 - 1 downto 0);
    reg_bsn_monitor_v2_rx_avs_readdatavalid     : out std_logic;
    reg_bsn_monitor_v2_rx_avs_waitrequest       : out std_logic;
    reg_bsn_monitor_v2_rx_avs_write             : in  std_logic;
    reg_bsn_monitor_v2_rx_avs_writedata         : in  std_logic_vector(32 - 1 downto 0);

    -- reg_strobe_total_count_rx
    reg_strobe_total_count_rx_avs_address       : in  std_logic_vector(32 - 1 downto 0);
    reg_strobe_total_count_rx_avs_read          : in  std_logic;
    reg_strobe_total_count_rx_avs_readdata      : out std_logic_vector(32 - 1 downto 0);
    reg_strobe_total_count_rx_avs_readdatavalid : out std_logic;
    reg_strobe_total_count_rx_avs_waitrequest   : out std_logic;
    reg_strobe_total_count_rx_avs_write         : in  std_logic;
    reg_strobe_total_count_rx_avs_writedata     : in  std_logic_vector(32 - 1 downto 0)

   );
end rdma_generator_eth_tester_wrapper;

architecture str of rdma_generator_eth_tester_wrapper is
  constant c_nof_byte                   : natural := c_rdma_generator_nof_octet_output_100gbe;

  signal rx_udp_sosi_arr                : t_dp_sosi_arr(0 downto 0) := (others => c_dp_sosi_rst);
  signal rx_udp_siso_arr                : t_dp_siso_arr(0 downto 0) := (others => c_dp_siso_rdy);
  signal tx_udp_sosi_arr                : t_dp_sosi_arr(0 downto 0) := (others => c_dp_sosi_rst);
  signal tx_udp_siso_arr                : t_dp_siso_arr(0 downto 0) := (others => c_dp_siso_rdy);

  signal rx_udp_axi4_sosi               : t_axi4_sosi := c_axi4_sosi_rst;
  signal rx_udp_axi4_siso               : t_axi4_siso := c_axi4_siso_rst;
  signal tx_udp_axi4_sosi               : t_axi4_sosi := c_axi4_sosi_rst;
  signal tx_udp_axi4_siso               : t_axi4_siso := c_axi4_siso_rst;

  signal reg_bg_ctrl_copi               : t_mem_copi := c_mem_copi_rst;
  signal reg_bg_ctrl_cipo               : t_mem_cipo;
  signal reg_hdr_dat_copi               : t_mem_copi := c_mem_copi_rst;
  signal reg_hdr_dat_cipo               : t_mem_cipo;
  signal reg_bsn_monitor_v2_tx_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_tx_cipo     : t_mem_cipo;
  signal reg_strobe_total_count_tx_copi : t_mem_copi := c_mem_copi_rst;
  signal reg_strobe_total_count_tx_cipo : t_mem_cipo;
  signal reg_dp_split_copi              : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_split_cipo              : t_mem_cipo;

  signal reg_bsn_monitor_v2_rx_copi     : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_rx_cipo     : t_mem_cipo;
  signal reg_strobe_total_count_rx_copi : t_mem_copi := c_mem_copi_rst;
  signal reg_strobe_total_count_rx_cipo : t_mem_cipo;

  signal mm_rst : std_logic := '0';
  signal st_rst : std_logic := '0';
begin
  u_eth_tester : entity eth_lib.eth_tester
  generic map (
    g_nof_octet_generate => c_rdma_generator_nof_octet_generate_100gbe,
    g_nof_octet_output   => c_rdma_generator_nof_octet_output_100gbe,
    g_use_eth_header     => false,
    g_use_ip_udp_header  => false,
    g_use_dp_header      => true,
    g_hdr_field_arr      => c_rdma_generator_dp_hdr_field_arr,
    g_hdr_field_sel      => c_rdma_generator_dp_hdr_field_sel,
    g_remove_crc         => false
  )
  port map (
    -- Clocks and reset
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,
    st_rst             => st_rst,
    st_clk             => st_clk,
    st_pps             => st_pps,

    -- UDP transmit interface
    eth_src_mac        => eth_src_mac,
    ip_src_addr        => ip_src_addr,
    udp_src_port       => udp_src_port,

    tx_fifo_rd_emp_arr => tx_fifo_rd_emp_arr,

    tx_udp_sosi_arr    => tx_udp_sosi_arr,
    tx_udp_siso_arr    => tx_udp_siso_arr,

    -- UDP receive interface
    rx_udp_sosi_arr    => rx_udp_sosi_arr,

    -- Memory Mapped Slaves (one per stream)
    reg_bg_ctrl_copi               => reg_bg_ctrl_copi,
    reg_bg_ctrl_cipo               => reg_bg_ctrl_cipo,
    reg_hdr_dat_copi               => reg_hdr_dat_copi,
    reg_hdr_dat_cipo               => reg_hdr_dat_cipo,
    reg_bsn_monitor_v2_tx_copi     => reg_bsn_monitor_v2_tx_copi,
    reg_bsn_monitor_v2_tx_cipo     => reg_bsn_monitor_v2_tx_cipo,
    reg_strobe_total_count_tx_copi => reg_strobe_total_count_tx_copi,
    reg_strobe_total_count_tx_cipo => reg_strobe_total_count_tx_cipo,
    reg_dp_split_copi              => reg_dp_split_copi,
    reg_dp_split_cipo              => reg_dp_split_cipo,

    reg_bsn_monitor_v2_rx_copi     => reg_bsn_monitor_v2_rx_copi,
    reg_bsn_monitor_v2_rx_cipo     => reg_bsn_monitor_v2_rx_cipo,
    reg_strobe_total_count_rx_copi => reg_strobe_total_count_rx_copi,
    reg_strobe_total_count_rx_cipo => reg_strobe_total_count_rx_cipo
  );

  -- DP to AXI4
  u_axi4_tx_udp : entity axi4_lib.axi4_stream_dp_bridge
  generic map (
    g_axi4_rl => 0,
    g_dp_rl   => 1,
    g_active_low_rst => true
  )
  port map (
    in_clk => st_clk,
    in_rst => aresetn,

    dp_rst => st_rst,

    dp_in_sosi => tx_udp_sosi_arr(0),
    dp_in_siso => tx_udp_siso_arr(0),

    axi4_out_sosi => tx_udp_axi4_sosi,
    axi4_out_siso => tx_udp_axi4_siso
  );

  u_axi4_rx_udp : entity axi4_lib.axi4_stream_dp_bridge
  generic map (
    g_axi4_rl => 0,
    g_dp_rl   => 1,
    g_active_low_rst => true
  )
  port map (
    in_clk => st_clk,
    in_rst => aresetn,

    axi4_in_sosi => rx_udp_axi4_sosi,
    axi4_in_siso => rx_udp_axi4_siso,

    dp_out_sosi => rx_udp_sosi_arr(0),
    dp_out_siso => rx_udp_siso_arr(0)
  );

  -- Wire Records to IN/OUT ports.
  -- tx_udp
  tx_udp_axi4_siso.tready <= tx_udp_tready;

  tx_udp_tvalid <= tx_udp_axi4_sosi.tvalid;
  tx_udp_tstrb  <= tx_udp_axi4_sosi.tstrb;
  tx_udp_tlast  <= tx_udp_axi4_sosi.tlast;
  tx_udp_tid    <= tx_udp_axi4_sosi.tid;
  tx_udp_tdest  <= tx_udp_axi4_sosi.tdest;
  tx_udp_tuser  <= tx_udp_axi4_sosi.tuser;
  -- reverse order of bytes
  gen_tx_data : for I in 0 to c_nof_byte - 1 generate
    tx_udp_tdata( (I + 1) * c_octet_w - 1 downto I * c_octet_w) <= tx_udp_axi4_sosi.tdata((c_nof_byte - I) * c_octet_w - 1 downto (c_nof_byte - 1 - I) * c_octet_w);
    tx_udp_tkeep(I) <= tx_udp_axi4_sosi.tkeep(c_nof_byte - 1 - I);
  end generate;

  -- rx_udp
  rx_udp_tready <= rx_udp_axi4_siso.tready;

  rx_udp_axi4_sosi.tvalid <= rx_udp_tvalid;
  rx_udp_axi4_sosi.tstrb  <= rx_udp_tstrb;
  rx_udp_axi4_sosi.tlast  <= rx_udp_tlast;
  rx_udp_axi4_sosi.tid    <= rx_udp_tid;
  rx_udp_axi4_sosi.tdest  <= rx_udp_tdest;
  rx_udp_axi4_sosi.tuser  <= rx_udp_tuser;
  -- reverse order of bytes
  gen_rx_data : for I in 0 to c_nof_byte - 1 generate
    rx_udp_axi4_sosi.tdata( (I + 1) * c_octet_w - 1 downto I * c_octet_w) <= rx_udp_tdata((c_nof_byte - I) * c_octet_w - 1 downto (c_nof_byte - 1 - I) * c_octet_w);
    rx_udp_axi4_sosi.tkeep(I) <= rx_udp_tkeep(c_nof_byte - 1 - I);
  end generate;

  -- reg_bg_ctrl
  -- copi
  reg_bg_ctrl_copi.address                    <= reg_bg_ctrl_avs_address;
  reg_bg_ctrl_copi.wrdata                     <= RESIZE_UVEC(reg_bg_ctrl_avs_writedata, c_mem_data_w);
  reg_bg_ctrl_copi.wr                         <= reg_bg_ctrl_avs_write;
  reg_bg_ctrl_copi.rd                         <= reg_bg_ctrl_avs_read;
  -- cipo
  reg_bg_ctrl_avs_readdata                    <= RESIZE_UVEC_32(reg_bg_ctrl_cipo.rddata);
  reg_bg_ctrl_avs_readdatavalid               <= reg_bg_ctrl_cipo.rdval;
  reg_bg_ctrl_avs_waitrequest                 <= reg_bg_ctrl_cipo.waitrequest;

  -- reg_hdr_dat
  -- copi
  reg_hdr_dat_copi.address                    <= reg_hdr_dat_avs_address;
  reg_hdr_dat_copi.wrdata                     <= RESIZE_UVEC(reg_hdr_dat_avs_writedata, c_mem_data_w);
  reg_hdr_dat_copi.wr                         <= reg_hdr_dat_avs_write;
  reg_hdr_dat_copi.rd                         <= reg_hdr_dat_avs_read;
  -- cipo
  reg_hdr_dat_avs_readdata                    <= RESIZE_UVEC_32(reg_hdr_dat_cipo.rddata);
  reg_hdr_dat_avs_readdatavalid               <= reg_hdr_dat_cipo.rdval;
  reg_hdr_dat_avs_waitrequest                 <= reg_hdr_dat_cipo.waitrequest;

  -- reg_bsn_monitor_v2_tx
  -- copi
  reg_bsn_monitor_v2_tx_copi.address          <= reg_bsn_monitor_v2_tx_avs_address;
  reg_bsn_monitor_v2_tx_copi.wrdata           <= RESIZE_UVEC(reg_bsn_monitor_v2_tx_avs_writedata, c_mem_data_w);
  reg_bsn_monitor_v2_tx_copi.wr               <= reg_bsn_monitor_v2_tx_avs_write;
  reg_bsn_monitor_v2_tx_copi.rd               <= reg_bsn_monitor_v2_tx_avs_read;
  -- cipo
  reg_bsn_monitor_v2_tx_avs_readdata          <= RESIZE_UVEC_32(reg_bsn_monitor_v2_tx_cipo.rddata);
  reg_bsn_monitor_v2_tx_avs_readdatavalid     <= reg_bsn_monitor_v2_tx_cipo.rdval;
  reg_bsn_monitor_v2_tx_avs_waitrequest       <= reg_bsn_monitor_v2_tx_cipo.waitrequest;

  -- reg_strobe_total_count_tx
  -- copi
  reg_strobe_total_count_tx_copi.address      <= reg_strobe_total_count_tx_avs_address;
  reg_strobe_total_count_tx_copi.wrdata       <= RESIZE_UVEC(reg_strobe_total_count_tx_avs_writedata, c_mem_data_w);
  reg_strobe_total_count_tx_copi.wr           <= reg_strobe_total_count_tx_avs_write;
  reg_strobe_total_count_tx_copi.rd           <= reg_strobe_total_count_tx_avs_read;
  -- cipo
  reg_strobe_total_count_tx_avs_readdata      <= RESIZE_UVEC_32(reg_strobe_total_count_tx_cipo.rddata);
  reg_strobe_total_count_tx_avs_readdatavalid <= reg_strobe_total_count_tx_cipo.rdval;
  reg_strobe_total_count_tx_avs_waitrequest   <= reg_strobe_total_count_tx_cipo.waitrequest;

  -- reg_dp_split
  -- copi
  reg_dp_split_copi.address                   <= reg_dp_split_avs_address;
  reg_dp_split_copi.wrdata                    <= RESIZE_UVEC(reg_dp_split_avs_writedata, c_mem_data_w);
  reg_dp_split_copi.wr                        <= reg_dp_split_avs_write;
  reg_dp_split_copi.rd                        <= reg_dp_split_avs_read;
  -- cipo
  reg_dp_split_avs_readdata                   <= RESIZE_UVEC_32(reg_dp_split_cipo.rddata);
  reg_dp_split_avs_readdatavalid              <= reg_dp_split_cipo.rdval;
  reg_dp_split_avs_waitrequest                <= reg_dp_split_cipo.waitrequest;

  -- reg_bsn_monitor_v2_rx
  -- copi
  reg_bsn_monitor_v2_rx_copi.address          <= reg_bsn_monitor_v2_rx_avs_address;
  reg_bsn_monitor_v2_rx_copi.wrdata           <= RESIZE_UVEC(reg_bsn_monitor_v2_rx_avs_writedata, c_mem_data_w);
  reg_bsn_monitor_v2_rx_copi.wr               <= reg_bsn_monitor_v2_rx_avs_write;
  reg_bsn_monitor_v2_rx_copi.rd               <= reg_bsn_monitor_v2_rx_avs_read;
  -- cipo
  reg_bsn_monitor_v2_rx_avs_readdata          <= RESIZE_UVEC_32(reg_bsn_monitor_v2_rx_cipo.rddata);
  reg_bsn_monitor_v2_rx_avs_readdatavalid     <= reg_bsn_monitor_v2_rx_cipo.rdval;
  reg_bsn_monitor_v2_rx_avs_waitrequest       <= reg_bsn_monitor_v2_rx_cipo.waitrequest;

  -- reg_strobe_total_count_rx
  -- copi
  reg_strobe_total_count_rx_copi.address      <= reg_strobe_total_count_rx_avs_address;
  reg_strobe_total_count_rx_copi.wrdata       <= RESIZE_UVEC(reg_strobe_total_count_rx_avs_writedata, c_mem_data_w);
  reg_strobe_total_count_rx_copi.wr           <= reg_strobe_total_count_rx_avs_write;
  reg_strobe_total_count_rx_copi.rd           <= reg_strobe_total_count_rx_avs_read;
  -- cipo
  reg_strobe_total_count_rx_avs_readdata      <= RESIZE_UVEC_32(reg_strobe_total_count_rx_cipo.rddata);
  reg_strobe_total_count_rx_avs_readdatavalid <= reg_strobe_total_count_rx_cipo.rdval;
  reg_strobe_total_count_rx_avs_waitrequest   <= reg_strobe_total_count_rx_cipo.waitrequest;
end str;
