-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi page memory
-- Description:
--   When *_next_page pulses then the next access will occur in the next page.
-- Remarks:
-- . See common_paged_ram_rw_rw for details.

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library common_lib;
use work.common_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_paged_ram_r_w is
  generic (
    g_technology      : natural := c_tech_select_default;
    g_str             : string := "use_adr";
    g_data_w          : natural;
    g_nof_pages       : natural := 2;  -- >= 2
    g_page_sz         : natural;
    g_wr_start_page   : natural := 0;
    g_rd_start_page   : natural := 0;
    g_rd_latency      : natural := 1
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    clken        : in  std_logic := '1';
    wr_next_page : in  std_logic;
    wr_adr       : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    wr_en        : in  std_logic := '0';
    wr_dat       : in  std_logic_vector(g_data_w - 1 downto 0) := (others => '0');
    rd_next_page : in  std_logic;
    rd_adr       : in  std_logic_vector(ceil_log2(g_page_sz) - 1 downto 0) := (others => '0');
    rd_en        : in  std_logic := '1';
    rd_dat       : out std_logic_vector(g_data_w - 1 downto 0);
    rd_val       : out std_logic
  );
end common_paged_ram_r_w;

architecture str of common_paged_ram_r_w is
begin
  u_rw_rw : entity work.common_paged_ram_rw_rw
  generic map (
    g_technology     => g_technology,
    g_str            => g_str,
    g_data_w         => g_data_w,
    g_nof_pages      => g_nof_pages,
    g_page_sz        => g_page_sz,
    g_start_page_a   => g_wr_start_page,
    g_start_page_b   => g_rd_start_page,
    g_rd_latency     => g_rd_latency,
    g_true_dual_port => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    clken       => clken,
    next_page_a => wr_next_page,
    adr_a       => wr_adr,
    wr_en_a     => wr_en,
    wr_dat_a    => wr_dat,
    rd_en_a     => '0',
    rd_dat_a    => OPEN,
    rd_val_a    => OPEN,
    next_page_b => rd_next_page,
    adr_b       => rd_adr,
    wr_en_b     => '0',
    wr_dat_b    => (others => '0'),
    rd_en_b     => rd_en,
    rd_dat_b    => rd_dat,
    rd_val_b    => rd_val
  );
end str;
