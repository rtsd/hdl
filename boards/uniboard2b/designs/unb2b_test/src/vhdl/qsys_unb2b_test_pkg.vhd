-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_unb2b_test_pkg is
  -----------------------------------------------------------------------------
  -- this component declaration is copy-pasted from Quartus QSYS builder generated file:
  -- $HDL_WORK/build/unb2b/quartus/unb2b_test_ddr/qsys_unb2b_test/sim/qsys_unb2b_test.vhd
  -----------------------------------------------------------------------------

    component qsys_unb2b_test is
        port (
            avs_eth_0_clk_export                                      : out std_logic;  -- avs_eth_0_clk.export
            avs_eth_0_irq_export                                      : in  std_logic                     := '0';  -- avs_eth_0_irq.export
            avs_eth_0_ram_address_export                              : out std_logic_vector(9 downto 0);  -- avs_eth_0_ram_address.export
            avs_eth_0_ram_read_export                                 : out std_logic;  -- avs_eth_0_ram_read.export
            avs_eth_0_ram_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- avs_eth_0_ram_readdata.export
            avs_eth_0_ram_write_export                                : out std_logic;  -- avs_eth_0_ram_write.export
            avs_eth_0_ram_writedata_export                            : out std_logic_vector(31 downto 0);  -- avs_eth_0_ram_writedata.export
            avs_eth_0_reg_address_export                              : out std_logic_vector(3 downto 0);  -- avs_eth_0_reg_address.export
            avs_eth_0_reg_read_export                                 : out std_logic;  -- avs_eth_0_reg_read.export
            avs_eth_0_reg_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- avs_eth_0_reg_readdata.export
            avs_eth_0_reg_write_export                                : out std_logic;  -- avs_eth_0_reg_write.export
            avs_eth_0_reg_writedata_export                            : out std_logic_vector(31 downto 0);  -- avs_eth_0_reg_writedata.export
            avs_eth_0_reset_export                                    : out std_logic;  -- avs_eth_0_reset.export
            avs_eth_0_tse_address_export                              : out std_logic_vector(9 downto 0);  -- avs_eth_0_tse_address.export
            avs_eth_0_tse_read_export                                 : out std_logic;  -- avs_eth_0_tse_read.export
            avs_eth_0_tse_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- avs_eth_0_tse_readdata.export
            avs_eth_0_tse_waitrequest_export                          : in  std_logic                     := '0';  -- avs_eth_0_tse_waitrequest.export
            avs_eth_0_tse_write_export                                : out std_logic;  -- avs_eth_0_tse_write.export
            avs_eth_0_tse_writedata_export                            : out std_logic_vector(31 downto 0);  -- avs_eth_0_tse_writedata.export
            avs_eth_1_clk_export                                      : out std_logic;  -- avs_eth_1_clk.export
            avs_eth_1_irq_export                                      : in  std_logic                     := '0';  -- avs_eth_1_irq.export
            avs_eth_1_ram_address_export                              : out std_logic_vector(9 downto 0);  -- avs_eth_1_ram_address.export
            avs_eth_1_ram_read_export                                 : out std_logic;  -- avs_eth_1_ram_read.export
            avs_eth_1_ram_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- avs_eth_1_ram_readdata.export
            avs_eth_1_ram_write_export                                : out std_logic;  -- avs_eth_1_ram_write.export
            avs_eth_1_ram_writedata_export                            : out std_logic_vector(31 downto 0);  -- avs_eth_1_ram_writedata.export
            avs_eth_1_reg_address_export                              : out std_logic_vector(3 downto 0);  -- avs_eth_1_reg_address.export
            avs_eth_1_reg_read_export                                 : out std_logic;  -- avs_eth_1_reg_read.export
            avs_eth_1_reg_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- avs_eth_1_reg_readdata.export
            avs_eth_1_reg_write_export                                : out std_logic;  -- avs_eth_1_reg_write.export
            avs_eth_1_reg_writedata_export                            : out std_logic_vector(31 downto 0);  -- avs_eth_1_reg_writedata.export
            avs_eth_1_reset_export                                    : out std_logic;  -- avs_eth_1_reset.export
            avs_eth_1_tse_address_export                              : out std_logic_vector(9 downto 0);  -- avs_eth_1_tse_address.export
            avs_eth_1_tse_read_export                                 : out std_logic;  -- avs_eth_1_tse_read.export
            avs_eth_1_tse_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- avs_eth_1_tse_readdata.export
            avs_eth_1_tse_waitrequest_export                          : in  std_logic                     := '0';  -- avs_eth_1_tse_waitrequest.export
            avs_eth_1_tse_write_export                                : out std_logic;  -- avs_eth_1_tse_write.export
            avs_eth_1_tse_writedata_export                            : out std_logic_vector(31 downto 0);  -- avs_eth_1_tse_writedata.export
            clk_clk                                                   : in  std_logic                     := '0';  -- clk.clk
            pio_pps_address_export                                    : out std_logic_vector(0 downto 0);  -- pio_pps_address.export
            pio_pps_clk_export                                        : out std_logic;  -- pio_pps_clk.export
            pio_pps_read_export                                       : out std_logic;  -- pio_pps_read.export
            pio_pps_readdata_export                                   : in  std_logic_vector(31 downto 0) := (others => '0');  -- pio_pps_readdata.export
            pio_pps_reset_export                                      : out std_logic;  -- pio_pps_reset.export
            pio_pps_write_export                                      : out std_logic;  -- pio_pps_write.export
            pio_pps_writedata_export                                  : out std_logic_vector(31 downto 0);  -- pio_pps_writedata.export
            pio_system_info_address_export                            : out std_logic_vector(4 downto 0);  -- pio_system_info_address.export
            pio_system_info_clk_export                                : out std_logic;  -- pio_system_info_clk.export
            pio_system_info_read_export                               : out std_logic;  -- pio_system_info_read.export
            pio_system_info_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => '0');  -- pio_system_info_readdata.export
            pio_system_info_reset_export                              : out std_logic;  -- pio_system_info_reset.export
            pio_system_info_write_export                              : out std_logic;  -- pio_system_info_write.export
            pio_system_info_writedata_export                          : out std_logic_vector(31 downto 0);  -- pio_system_info_writedata.export
            pio_wdi_external_connection_export                        : out std_logic;  -- pio_wdi_external_connection.export
            ram_diag_bg_10gbe_address_export                          : out std_logic_vector(16 downto 0);  -- ram_diag_bg_10gbe_address.export
            ram_diag_bg_10gbe_clk_export                              : out std_logic;  -- ram_diag_bg_10gbe_clk.export
            ram_diag_bg_10gbe_read_export                             : out std_logic;  -- ram_diag_bg_10gbe_read.export
            ram_diag_bg_10gbe_readdata_export                         : in  std_logic_vector(31 downto 0) := (others => '0');  -- ram_diag_bg_10gbe_readdata.export
            ram_diag_bg_10gbe_reset_export                            : out std_logic;  -- ram_diag_bg_10gbe_reset.export
            ram_diag_bg_10gbe_write_export                            : out std_logic;  -- ram_diag_bg_10gbe_write.export
            ram_diag_bg_10gbe_writedata_export                        : out std_logic_vector(31 downto 0);  -- ram_diag_bg_10gbe_writedata.export
            ram_diag_bg_1gbe_address_export                           : out std_logic_vector(10 downto 0);  -- ram_diag_bg_1gbe_address.export
            ram_diag_bg_1gbe_clk_export                               : out std_logic;  -- ram_diag_bg_1gbe_clk.export
            ram_diag_bg_1gbe_read_export                              : out std_logic;  -- ram_diag_bg_1gbe_read.export
            ram_diag_bg_1gbe_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => '0');  -- ram_diag_bg_1gbe_readdata.export
            ram_diag_bg_1gbe_reset_export                             : out std_logic;  -- ram_diag_bg_1gbe_reset.export
            ram_diag_bg_1gbe_write_export                             : out std_logic;  -- ram_diag_bg_1gbe_write.export
            ram_diag_bg_1gbe_writedata_export                         : out std_logic_vector(31 downto 0);  -- ram_diag_bg_1gbe_writedata.export
            ram_diag_data_buffer_10gbe_address_export                 : out std_logic_vector(16 downto 0);  -- ram_diag_data_buffer_10gbe_address.export
            ram_diag_data_buffer_10gbe_clk_export                     : out std_logic;  -- ram_diag_data_buffer_10gbe_clk.export
            ram_diag_data_buffer_10gbe_read_export                    : out std_logic;  -- ram_diag_data_buffer_10gbe_read.export
            ram_diag_data_buffer_10gbe_readdata_export                : in  std_logic_vector(31 downto 0) := (others => '0');  -- ram_diag_data_buffer_10gbe_readdata.export
            ram_diag_data_buffer_10gbe_reset_export                   : out std_logic;  -- ram_diag_data_buffer_10gbe_reset.export
            ram_diag_data_buffer_10gbe_write_export                   : out std_logic;  -- ram_diag_data_buffer_10gbe_write.export
            ram_diag_data_buffer_10gbe_writedata_export               : out std_logic_vector(31 downto 0);  -- ram_diag_data_buffer_10gbe_writedata.export
            ram_diag_data_buffer_1gbe_address_export                  : out std_logic_vector(10 downto 0);  -- ram_diag_data_buffer_1gbe_address.export
            ram_diag_data_buffer_1gbe_clk_export                      : out std_logic;  -- ram_diag_data_buffer_1gbe_clk.export
            ram_diag_data_buffer_1gbe_read_export                     : out std_logic;  -- ram_diag_data_buffer_1gbe_read.export
            ram_diag_data_buffer_1gbe_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => '0');  -- ram_diag_data_buffer_1gbe_readdata.export
            ram_diag_data_buffer_1gbe_reset_export                    : out std_logic;  -- ram_diag_data_buffer_1gbe_reset.export
            ram_diag_data_buffer_1gbe_write_export                    : out std_logic;  -- ram_diag_data_buffer_1gbe_write.export
            ram_diag_data_buffer_1gbe_writedata_export                : out std_logic_vector(31 downto 0);  -- ram_diag_data_buffer_1gbe_writedata.export
            ram_diag_data_buffer_ddr_mb_i_address_export              : out std_logic_vector(10 downto 0);  -- ram_diag_data_buffer_ddr_mb_i_address.export
            ram_diag_data_buffer_ddr_mb_i_clk_export                  : out std_logic;  -- ram_diag_data_buffer_ddr_mb_i_clk.export
            ram_diag_data_buffer_ddr_mb_i_read_export                 : out std_logic;  -- ram_diag_data_buffer_ddr_mb_i_read.export
            ram_diag_data_buffer_ddr_mb_i_readdata_export             : in  std_logic_vector(31 downto 0) := (others => '0');  -- ram_diag_data_buffer_ddr_mb_i_readdata.export
            ram_diag_data_buffer_ddr_mb_i_reset_export                : out std_logic;  -- ram_diag_data_buffer_ddr_mb_i_reset.export
            ram_diag_data_buffer_ddr_mb_i_write_export                : out std_logic;  -- ram_diag_data_buffer_ddr_mb_i_write.export
            ram_diag_data_buffer_ddr_mb_i_writedata_export            : out std_logic_vector(31 downto 0);  -- ram_diag_data_buffer_ddr_mb_i_writedata.export
            ram_diag_data_buffer_ddr_mb_ii_address_export             : out std_logic_vector(10 downto 0);  -- ram_diag_data_buffer_ddr_mb_ii_address.export
            ram_diag_data_buffer_ddr_mb_ii_clk_export                 : out std_logic;  -- ram_diag_data_buffer_ddr_mb_ii_clk.export
            ram_diag_data_buffer_ddr_mb_ii_read_export                : out std_logic;  -- ram_diag_data_buffer_ddr_mb_ii_read.export
            ram_diag_data_buffer_ddr_mb_ii_readdata_export            : in  std_logic_vector(31 downto 0) := (others => '0');  -- ram_diag_data_buffer_ddr_mb_ii_readdata.export
            ram_diag_data_buffer_ddr_mb_ii_reset_export               : out std_logic;  -- ram_diag_data_buffer_ddr_mb_ii_reset.export
            ram_diag_data_buffer_ddr_mb_ii_write_export               : out std_logic;  -- ram_diag_data_buffer_ddr_mb_ii_write.export
            ram_diag_data_buffer_ddr_mb_ii_writedata_export           : out std_logic_vector(31 downto 0);  -- ram_diag_data_buffer_ddr_mb_ii_writedata.export
            reg_bsn_monitor_10gbe_address_export                      : out std_logic_vector(10 downto 0);  -- reg_bsn_monitor_10gbe_address.export
            reg_bsn_monitor_10gbe_clk_export                          : out std_logic;  -- reg_bsn_monitor_10gbe_clk.export
            reg_bsn_monitor_10gbe_read_export                         : out std_logic;  -- reg_bsn_monitor_10gbe_read.export
            reg_bsn_monitor_10gbe_readdata_export                     : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_bsn_monitor_10gbe_readdata.export
            reg_bsn_monitor_10gbe_reset_export                        : out std_logic;  -- reg_bsn_monitor_10gbe_reset.export
            reg_bsn_monitor_10gbe_write_export                        : out std_logic;  -- reg_bsn_monitor_10gbe_write.export
            reg_bsn_monitor_10gbe_writedata_export                    : out std_logic_vector(31 downto 0);  -- reg_bsn_monitor_10gbe_writedata.export
            reg_bsn_monitor_1gbe_address_export                       : out std_logic_vector(4 downto 0);  -- reg_bsn_monitor_1gbe_address.export
            reg_bsn_monitor_1gbe_clk_export                           : out std_logic;  -- reg_bsn_monitor_1gbe_clk.export
            reg_bsn_monitor_1gbe_read_export                          : out std_logic;  -- reg_bsn_monitor_1gbe_read.export
            reg_bsn_monitor_1gbe_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_bsn_monitor_1gbe_readdata.export
            reg_bsn_monitor_1gbe_reset_export                         : out std_logic;  -- reg_bsn_monitor_1gbe_reset.export
            reg_bsn_monitor_1gbe_write_export                         : out std_logic;  -- reg_bsn_monitor_1gbe_write.export
            reg_bsn_monitor_1gbe_writedata_export                     : out std_logic_vector(31 downto 0);  -- reg_bsn_monitor_1gbe_writedata.export
            reg_diag_bg_10gbe_address_export                          : out std_logic_vector(2 downto 0);  -- reg_diag_bg_10gbe_address.export
            reg_diag_bg_10gbe_clk_export                              : out std_logic;  -- reg_diag_bg_10gbe_clk.export
            reg_diag_bg_10gbe_read_export                             : out std_logic;  -- reg_diag_bg_10gbe_read.export
            reg_diag_bg_10gbe_readdata_export                         : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_bg_10gbe_readdata.export
            reg_diag_bg_10gbe_reset_export                            : out std_logic;  -- reg_diag_bg_10gbe_reset.export
            reg_diag_bg_10gbe_write_export                            : out std_logic;  -- reg_diag_bg_10gbe_write.export
            reg_diag_bg_10gbe_writedata_export                        : out std_logic_vector(31 downto 0);  -- reg_diag_bg_10gbe_writedata.export
            reg_diag_bg_1gbe_address_export                           : out std_logic_vector(2 downto 0);  -- reg_diag_bg_1gbe_address.export
            reg_diag_bg_1gbe_clk_export                               : out std_logic;  -- reg_diag_bg_1gbe_clk.export
            reg_diag_bg_1gbe_read_export                              : out std_logic;  -- reg_diag_bg_1gbe_read.export
            reg_diag_bg_1gbe_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_bg_1gbe_readdata.export
            reg_diag_bg_1gbe_reset_export                             : out std_logic;  -- reg_diag_bg_1gbe_reset.export
            reg_diag_bg_1gbe_write_export                             : out std_logic;  -- reg_diag_bg_1gbe_write.export
            reg_diag_bg_1gbe_writedata_export                         : out std_logic_vector(31 downto 0);  -- reg_diag_bg_1gbe_writedata.export
            reg_diag_data_buffer_10gbe_address_export                 : out std_logic_vector(5 downto 0);  -- reg_diag_data_buffer_10gbe_address.export
            reg_diag_data_buffer_10gbe_clk_export                     : out std_logic;  -- reg_diag_data_buffer_10gbe_clk.export
            reg_diag_data_buffer_10gbe_read_export                    : out std_logic;  -- reg_diag_data_buffer_10gbe_read.export
            reg_diag_data_buffer_10gbe_readdata_export                : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_data_buffer_10gbe_readdata.export
            reg_diag_data_buffer_10gbe_reset_export                   : out std_logic;  -- reg_diag_data_buffer_10gbe_reset.export
            reg_diag_data_buffer_10gbe_write_export                   : out std_logic;  -- reg_diag_data_buffer_10gbe_write.export
            reg_diag_data_buffer_10gbe_writedata_export               : out std_logic_vector(31 downto 0);  -- reg_diag_data_buffer_10gbe_writedata.export
            reg_diag_data_buffer_1gbe_address_export                  : out std_logic_vector(4 downto 0);  -- reg_diag_data_buffer_1gbe_address.export
            reg_diag_data_buffer_1gbe_clk_export                      : out std_logic;  -- reg_diag_data_buffer_1gbe_clk.export
            reg_diag_data_buffer_1gbe_read_export                     : out std_logic;  -- reg_diag_data_buffer_1gbe_read.export
            reg_diag_data_buffer_1gbe_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_data_buffer_1gbe_readdata.export
            reg_diag_data_buffer_1gbe_reset_export                    : out std_logic;  -- reg_diag_data_buffer_1gbe_reset.export
            reg_diag_data_buffer_1gbe_write_export                    : out std_logic;  -- reg_diag_data_buffer_1gbe_write.export
            reg_diag_data_buffer_1gbe_writedata_export                : out std_logic_vector(31 downto 0);  -- reg_diag_data_buffer_1gbe_writedata.export
            reg_diag_data_buffer_ddr_mb_i_address_export              : out std_logic_vector(4 downto 0);  -- reg_diag_data_buffer_ddr_mb_i_address.export
            reg_diag_data_buffer_ddr_mb_i_clk_export                  : out std_logic;  -- reg_diag_data_buffer_ddr_mb_i_clk.export
            reg_diag_data_buffer_ddr_mb_i_read_export                 : out std_logic;  -- reg_diag_data_buffer_ddr_mb_i_read.export
            reg_diag_data_buffer_ddr_mb_i_readdata_export             : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_data_buffer_ddr_mb_i_readdata.export
            reg_diag_data_buffer_ddr_mb_i_reset_export                : out std_logic;  -- reg_diag_data_buffer_ddr_mb_i_reset.export
            reg_diag_data_buffer_ddr_mb_i_write_export                : out std_logic;  -- reg_diag_data_buffer_ddr_mb_i_write.export
            reg_diag_data_buffer_ddr_mb_i_writedata_export            : out std_logic_vector(31 downto 0);  -- reg_diag_data_buffer_ddr_mb_i_writedata.export
            reg_diag_data_buffer_ddr_mb_ii_address_export             : out std_logic_vector(4 downto 0);  -- reg_diag_data_buffer_ddr_mb_ii_address.export
            reg_diag_data_buffer_ddr_mb_ii_clk_export                 : out std_logic;  -- reg_diag_data_buffer_ddr_mb_ii_clk.export
            reg_diag_data_buffer_ddr_mb_ii_read_export                : out std_logic;  -- reg_diag_data_buffer_ddr_mb_ii_read.export
            reg_diag_data_buffer_ddr_mb_ii_readdata_export            : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_data_buffer_ddr_mb_ii_readdata.export
            reg_diag_data_buffer_ddr_mb_ii_reset_export               : out std_logic;  -- reg_diag_data_buffer_ddr_mb_ii_reset.export
            reg_diag_data_buffer_ddr_mb_ii_write_export               : out std_logic;  -- reg_diag_data_buffer_ddr_mb_ii_write.export
            reg_diag_data_buffer_ddr_mb_ii_writedata_export           : out std_logic_vector(31 downto 0);  -- reg_diag_data_buffer_ddr_mb_ii_writedata.export
            reg_diag_rx_seq_10gbe_address_export                      : out std_logic_vector(4 downto 0);  -- reg_diag_rx_seq_10gbe_address.export
            reg_diag_rx_seq_10gbe_clk_export                          : out std_logic;  -- reg_diag_rx_seq_10gbe_clk.export
            reg_diag_rx_seq_10gbe_read_export                         : out std_logic;  -- reg_diag_rx_seq_10gbe_read.export
            reg_diag_rx_seq_10gbe_readdata_export                     : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_rx_seq_10gbe_readdata.export
            reg_diag_rx_seq_10gbe_reset_export                        : out std_logic;  -- reg_diag_rx_seq_10gbe_reset.export
            reg_diag_rx_seq_10gbe_write_export                        : out std_logic;  -- reg_diag_rx_seq_10gbe_write.export
            reg_diag_rx_seq_10gbe_writedata_export                    : out std_logic_vector(31 downto 0);  -- reg_diag_rx_seq_10gbe_writedata.export
            reg_diag_rx_seq_1gbe_address_export                       : out std_logic_vector(2 downto 0);  -- reg_diag_rx_seq_1gbe_address.export
            reg_diag_rx_seq_1gbe_clk_export                           : out std_logic;  -- reg_diag_rx_seq_1gbe_clk.export
            reg_diag_rx_seq_1gbe_read_export                          : out std_logic;  -- reg_diag_rx_seq_1gbe_read.export
            reg_diag_rx_seq_1gbe_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_rx_seq_1gbe_readdata.export
            reg_diag_rx_seq_1gbe_reset_export                         : out std_logic;  -- reg_diag_rx_seq_1gbe_reset.export
            reg_diag_rx_seq_1gbe_write_export                         : out std_logic;  -- reg_diag_rx_seq_1gbe_write.export
            reg_diag_rx_seq_1gbe_writedata_export                     : out std_logic_vector(31 downto 0);  -- reg_diag_rx_seq_1gbe_writedata.export
            reg_diag_rx_seq_ddr_mb_i_address_export                   : out std_logic_vector(2 downto 0);  -- reg_diag_rx_seq_ddr_mb_i_address.export
            reg_diag_rx_seq_ddr_mb_i_clk_export                       : out std_logic;  -- reg_diag_rx_seq_ddr_mb_i_clk.export
            reg_diag_rx_seq_ddr_mb_i_read_export                      : out std_logic;  -- reg_diag_rx_seq_ddr_mb_i_read.export
            reg_diag_rx_seq_ddr_mb_i_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_rx_seq_ddr_mb_i_readdata.export
            reg_diag_rx_seq_ddr_mb_i_reset_export                     : out std_logic;  -- reg_diag_rx_seq_ddr_mb_i_reset.export
            reg_diag_rx_seq_ddr_mb_i_write_export                     : out std_logic;  -- reg_diag_rx_seq_ddr_mb_i_write.export
            reg_diag_rx_seq_ddr_mb_i_writedata_export                 : out std_logic_vector(31 downto 0);  -- reg_diag_rx_seq_ddr_mb_i_writedata.export
            reg_diag_rx_seq_ddr_mb_ii_address_export                  : out std_logic_vector(2 downto 0);  -- reg_diag_rx_seq_ddr_mb_ii_address.export
            reg_diag_rx_seq_ddr_mb_ii_clk_export                      : out std_logic;  -- reg_diag_rx_seq_ddr_mb_ii_clk.export
            reg_diag_rx_seq_ddr_mb_ii_read_export                     : out std_logic;  -- reg_diag_rx_seq_ddr_mb_ii_read.export
            reg_diag_rx_seq_ddr_mb_ii_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_rx_seq_ddr_mb_ii_readdata.export
            reg_diag_rx_seq_ddr_mb_ii_reset_export                    : out std_logic;  -- reg_diag_rx_seq_ddr_mb_ii_reset.export
            reg_diag_rx_seq_ddr_mb_ii_write_export                    : out std_logic;  -- reg_diag_rx_seq_ddr_mb_ii_write.export
            reg_diag_rx_seq_ddr_mb_ii_writedata_export                : out std_logic_vector(31 downto 0);  -- reg_diag_rx_seq_ddr_mb_ii_writedata.export
            reg_diag_tx_seq_10gbe_address_export                      : out std_logic_vector(3 downto 0);  -- reg_diag_tx_seq_10gbe_address.export
            reg_diag_tx_seq_10gbe_clk_export                          : out std_logic;  -- reg_diag_tx_seq_10gbe_clk.export
            reg_diag_tx_seq_10gbe_read_export                         : out std_logic;  -- reg_diag_tx_seq_10gbe_read.export
            reg_diag_tx_seq_10gbe_readdata_export                     : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_tx_seq_10gbe_readdata.export
            reg_diag_tx_seq_10gbe_reset_export                        : out std_logic;  -- reg_diag_tx_seq_10gbe_reset.export
            reg_diag_tx_seq_10gbe_write_export                        : out std_logic;  -- reg_diag_tx_seq_10gbe_write.export
            reg_diag_tx_seq_10gbe_writedata_export                    : out std_logic_vector(31 downto 0);  -- reg_diag_tx_seq_10gbe_writedata.export
            reg_diag_tx_seq_1gbe_address_export                       : out std_logic_vector(1 downto 0);  -- reg_diag_tx_seq_1gbe_address.export
            reg_diag_tx_seq_1gbe_clk_export                           : out std_logic;  -- reg_diag_tx_seq_1gbe_clk.export
            reg_diag_tx_seq_1gbe_read_export                          : out std_logic;  -- reg_diag_tx_seq_1gbe_read.export
            reg_diag_tx_seq_1gbe_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_tx_seq_1gbe_readdata.export
            reg_diag_tx_seq_1gbe_reset_export                         : out std_logic;  -- reg_diag_tx_seq_1gbe_reset.export
            reg_diag_tx_seq_1gbe_write_export                         : out std_logic;  -- reg_diag_tx_seq_1gbe_write.export
            reg_diag_tx_seq_1gbe_writedata_export                     : out std_logic_vector(31 downto 0);  -- reg_diag_tx_seq_1gbe_writedata.export
            reg_diag_tx_seq_ddr_mb_i_address_export                   : out std_logic_vector(1 downto 0);  -- reg_diag_tx_seq_ddr_mb_i_address.export
            reg_diag_tx_seq_ddr_mb_i_clk_export                       : out std_logic;  -- reg_diag_tx_seq_ddr_mb_i_clk.export
            reg_diag_tx_seq_ddr_mb_i_read_export                      : out std_logic;  -- reg_diag_tx_seq_ddr_mb_i_read.export
            reg_diag_tx_seq_ddr_mb_i_readdata_export                  : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_tx_seq_ddr_mb_i_readdata.export
            reg_diag_tx_seq_ddr_mb_i_reset_export                     : out std_logic;  -- reg_diag_tx_seq_ddr_mb_i_reset.export
            reg_diag_tx_seq_ddr_mb_i_write_export                     : out std_logic;  -- reg_diag_tx_seq_ddr_mb_i_write.export
            reg_diag_tx_seq_ddr_mb_i_writedata_export                 : out std_logic_vector(31 downto 0);  -- reg_diag_tx_seq_ddr_mb_i_writedata.export
            reg_diag_tx_seq_ddr_mb_ii_address_export                  : out std_logic_vector(1 downto 0);  -- reg_diag_tx_seq_ddr_mb_ii_address.export
            reg_diag_tx_seq_ddr_mb_ii_clk_export                      : out std_logic;  -- reg_diag_tx_seq_ddr_mb_ii_clk.export
            reg_diag_tx_seq_ddr_mb_ii_read_export                     : out std_logic;  -- reg_diag_tx_seq_ddr_mb_ii_read.export
            reg_diag_tx_seq_ddr_mb_ii_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_diag_tx_seq_ddr_mb_ii_readdata.export
            reg_diag_tx_seq_ddr_mb_ii_reset_export                    : out std_logic;  -- reg_diag_tx_seq_ddr_mb_ii_reset.export
            reg_diag_tx_seq_ddr_mb_ii_write_export                    : out std_logic;  -- reg_diag_tx_seq_ddr_mb_ii_write.export
            reg_diag_tx_seq_ddr_mb_ii_writedata_export                : out std_logic_vector(31 downto 0);  -- reg_diag_tx_seq_ddr_mb_ii_writedata.export
            reg_dpmm_ctrl_address_export                              : out std_logic_vector(0 downto 0);  -- reg_dpmm_ctrl_address.export
            reg_dpmm_ctrl_clk_export                                  : out std_logic;  -- reg_dpmm_ctrl_clk.export
            reg_dpmm_ctrl_read_export                                 : out std_logic;  -- reg_dpmm_ctrl_read.export
            reg_dpmm_ctrl_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_dpmm_ctrl_readdata.export
            reg_dpmm_ctrl_reset_export                                : out std_logic;  -- reg_dpmm_ctrl_reset.export
            reg_dpmm_ctrl_write_export                                : out std_logic;  -- reg_dpmm_ctrl_write.export
            reg_dpmm_ctrl_writedata_export                            : out std_logic_vector(31 downto 0);  -- reg_dpmm_ctrl_writedata.export
            reg_dpmm_data_address_export                              : out std_logic_vector(0 downto 0);  -- reg_dpmm_data_address.export
            reg_dpmm_data_clk_export                                  : out std_logic;  -- reg_dpmm_data_clk.export
            reg_dpmm_data_read_export                                 : out std_logic;  -- reg_dpmm_data_read.export
            reg_dpmm_data_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_dpmm_data_readdata.export
            reg_dpmm_data_reset_export                                : out std_logic;  -- reg_dpmm_data_reset.export
            reg_dpmm_data_write_export                                : out std_logic;  -- reg_dpmm_data_write.export
            reg_dpmm_data_writedata_export                            : out std_logic_vector(31 downto 0);  -- reg_dpmm_data_writedata.export
            reg_epcs_address_export                                   : out std_logic_vector(2 downto 0);  -- reg_epcs_address.export
            reg_epcs_clk_export                                       : out std_logic;  -- reg_epcs_clk.export
            reg_epcs_read_export                                      : out std_logic;  -- reg_epcs_read.export
            reg_epcs_readdata_export                                  : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_epcs_readdata.export
            reg_epcs_reset_export                                     : out std_logic;  -- reg_epcs_reset.export
            reg_epcs_write_export                                     : out std_logic;  -- reg_epcs_write.export
            reg_epcs_writedata_export                                 : out std_logic_vector(31 downto 0);  -- reg_epcs_writedata.export
            reg_eth10g_back0_address_export                           : out std_logic_vector(5 downto 0);  -- reg_eth10g_back0_address.export
            reg_eth10g_back0_clk_export                               : out std_logic;  -- reg_eth10g_back0_clk.export
            reg_eth10g_back0_read_export                              : out std_logic;  -- reg_eth10g_back0_read.export
            reg_eth10g_back0_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_eth10g_back0_readdata.export
            reg_eth10g_back0_reset_export                             : out std_logic;  -- reg_eth10g_back0_reset.export
            reg_eth10g_back0_write_export                             : out std_logic;  -- reg_eth10g_back0_write.export
            reg_eth10g_back0_writedata_export                         : out std_logic_vector(31 downto 0);  -- reg_eth10g_back0_writedata.export
            reg_eth10g_back1_address_export                           : out std_logic_vector(5 downto 0);  -- reg_eth10g_back1_address.export
            reg_eth10g_back1_clk_export                               : out std_logic;  -- reg_eth10g_back1_clk.export
            reg_eth10g_back1_read_export                              : out std_logic;  -- reg_eth10g_back1_read.export
            reg_eth10g_back1_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_eth10g_back1_readdata.export
            reg_eth10g_back1_reset_export                             : out std_logic;  -- reg_eth10g_back1_reset.export
            reg_eth10g_back1_write_export                             : out std_logic;  -- reg_eth10g_back1_write.export
            reg_eth10g_back1_writedata_export                         : out std_logic_vector(31 downto 0);  -- reg_eth10g_back1_writedata.export
            reg_eth10g_qsfp_ring_address_export                       : out std_logic_vector(6 downto 0);  -- reg_eth10g_qsfp_ring_address.export
            reg_eth10g_qsfp_ring_clk_export                           : out std_logic;  -- reg_eth10g_qsfp_ring_clk.export
            reg_eth10g_qsfp_ring_read_export                          : out std_logic;  -- reg_eth10g_qsfp_ring_read.export
            reg_eth10g_qsfp_ring_readdata_export                      : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_eth10g_qsfp_ring_readdata.export
            reg_eth10g_qsfp_ring_reset_export                         : out std_logic;  -- reg_eth10g_qsfp_ring_reset.export
            reg_eth10g_qsfp_ring_write_export                         : out std_logic;  -- reg_eth10g_qsfp_ring_write.export
            reg_eth10g_qsfp_ring_writedata_export                     : out std_logic_vector(31 downto 0);  -- reg_eth10g_qsfp_ring_writedata.export
            reg_fpga_temp_sens_address_export                         : out std_logic_vector(2 downto 0);  -- reg_fpga_temp_sens_address.export
            reg_fpga_temp_sens_clk_export                             : out std_logic;  -- reg_fpga_temp_sens_clk.export
            reg_fpga_temp_sens_read_export                            : out std_logic;  -- reg_fpga_temp_sens_read.export
            reg_fpga_temp_sens_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_fpga_temp_sens_readdata.export
            reg_fpga_temp_sens_reset_export                           : out std_logic;  -- reg_fpga_temp_sens_reset.export
            reg_fpga_temp_sens_write_export                           : out std_logic;  -- reg_fpga_temp_sens_write.export
            reg_fpga_temp_sens_writedata_export                       : out std_logic_vector(31 downto 0);  -- reg_fpga_temp_sens_writedata.export
            reg_fpga_voltage_sens_address_export                      : out std_logic_vector(3 downto 0);  -- reg_fpga_voltage_sens_address.export
            reg_fpga_voltage_sens_clk_export                          : out std_logic;  -- reg_fpga_voltage_sens_clk.export
            reg_fpga_voltage_sens_read_export                         : out std_logic;  -- reg_fpga_voltage_sens_read.export
            reg_fpga_voltage_sens_readdata_export                     : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_fpga_voltage_sens_readdata.export
            reg_fpga_voltage_sens_reset_export                        : out std_logic;  -- reg_fpga_voltage_sens_reset.export
            reg_fpga_voltage_sens_write_export                        : out std_logic;  -- reg_fpga_voltage_sens_write.export
            reg_fpga_voltage_sens_writedata_export                    : out std_logic_vector(31 downto 0);  -- reg_fpga_voltage_sens_writedata.export
            reg_io_ddr_mb_i_address_export                            : out std_logic_vector(15 downto 0);  -- reg_io_ddr_mb_i_address.export
            reg_io_ddr_mb_i_clk_export                                : out std_logic;  -- reg_io_ddr_mb_i_clk.export
            reg_io_ddr_mb_i_read_export                               : out std_logic;  -- reg_io_ddr_mb_i_read.export
            reg_io_ddr_mb_i_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_io_ddr_mb_i_readdata.export
            reg_io_ddr_mb_i_reset_export                              : out std_logic;  -- reg_io_ddr_mb_i_reset.export
            reg_io_ddr_mb_i_write_export                              : out std_logic;  -- reg_io_ddr_mb_i_write.export
            reg_io_ddr_mb_i_writedata_export                          : out std_logic_vector(31 downto 0);  -- reg_io_ddr_mb_i_writedata.export
            reg_io_ddr_mb_ii_address_export                           : out std_logic_vector(15 downto 0);  -- reg_io_ddr_mb_ii_address.export
            reg_io_ddr_mb_ii_clk_export                               : out std_logic;  -- reg_io_ddr_mb_ii_clk.export
            reg_io_ddr_mb_ii_read_export                              : out std_logic;  -- reg_io_ddr_mb_ii_read.export
            reg_io_ddr_mb_ii_readdata_export                          : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_io_ddr_mb_ii_readdata.export
            reg_io_ddr_mb_ii_reset_export                             : out std_logic;  -- reg_io_ddr_mb_ii_reset.export
            reg_io_ddr_mb_ii_write_export                             : out std_logic;  -- reg_io_ddr_mb_ii_write.export
            reg_io_ddr_mb_ii_writedata_export                         : out std_logic_vector(31 downto 0);  -- reg_io_ddr_mb_ii_writedata.export
            reg_mmdp_ctrl_address_export                              : out std_logic_vector(0 downto 0);  -- reg_mmdp_ctrl_address.export
            reg_mmdp_ctrl_clk_export                                  : out std_logic;  -- reg_mmdp_ctrl_clk.export
            reg_mmdp_ctrl_read_export                                 : out std_logic;  -- reg_mmdp_ctrl_read.export
            reg_mmdp_ctrl_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_mmdp_ctrl_readdata.export
            reg_mmdp_ctrl_reset_export                                : out std_logic;  -- reg_mmdp_ctrl_reset.export
            reg_mmdp_ctrl_write_export                                : out std_logic;  -- reg_mmdp_ctrl_write.export
            reg_mmdp_ctrl_writedata_export                            : out std_logic_vector(31 downto 0);  -- reg_mmdp_ctrl_writedata.export
            reg_mmdp_data_address_export                              : out std_logic_vector(0 downto 0);  -- reg_mmdp_data_address.export
            reg_mmdp_data_clk_export                                  : out std_logic;  -- reg_mmdp_data_clk.export
            reg_mmdp_data_read_export                                 : out std_logic;  -- reg_mmdp_data_read.export
            reg_mmdp_data_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_mmdp_data_readdata.export
            reg_mmdp_data_reset_export                                : out std_logic;  -- reg_mmdp_data_reset.export
            reg_mmdp_data_write_export                                : out std_logic;  -- reg_mmdp_data_write.export
            reg_mmdp_data_writedata_export                            : out std_logic_vector(31 downto 0);  -- reg_mmdp_data_writedata.export
            reg_remu_address_export                                   : out std_logic_vector(2 downto 0);  -- reg_remu_address.export
            reg_remu_clk_export                                       : out std_logic;  -- reg_remu_clk.export
            reg_remu_read_export                                      : out std_logic;  -- reg_remu_read.export
            reg_remu_readdata_export                                  : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_remu_readdata.export
            reg_remu_reset_export                                     : out std_logic;  -- reg_remu_reset.export
            reg_remu_write_export                                     : out std_logic;  -- reg_remu_write.export
            reg_remu_writedata_export                                 : out std_logic_vector(31 downto 0);  -- reg_remu_writedata.export
            reg_tr_10gbe_back0_address_export                         : out std_logic_vector(17 downto 0);  -- reg_tr_10gbe_back0_address.export
            reg_tr_10gbe_back0_clk_export                             : out std_logic;  -- reg_tr_10gbe_back0_clk.export
            reg_tr_10gbe_back0_read_export                            : out std_logic;  -- reg_tr_10gbe_back0_read.export
            reg_tr_10gbe_back0_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_tr_10gbe_back0_readdata.export
            reg_tr_10gbe_back0_reset_export                           : out std_logic;  -- reg_tr_10gbe_back0_reset.export
            reg_tr_10gbe_back0_waitrequest_export                     : in  std_logic                     := '0';  -- reg_tr_10gbe_back0_waitrequest.export
            reg_tr_10gbe_back0_write_export                           : out std_logic;  -- reg_tr_10gbe_back0_write.export
            reg_tr_10gbe_back0_writedata_export                       : out std_logic_vector(31 downto 0);  -- reg_tr_10gbe_back0_writedata.export
            reg_tr_10gbe_back1_address_export                         : out std_logic_vector(17 downto 0);  -- reg_tr_10gbe_back1_address.export
            reg_tr_10gbe_back1_clk_export                             : out std_logic;  -- reg_tr_10gbe_back1_clk.export
            reg_tr_10gbe_back1_read_export                            : out std_logic;  -- reg_tr_10gbe_back1_read.export
            reg_tr_10gbe_back1_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_tr_10gbe_back1_readdata.export
            reg_tr_10gbe_back1_reset_export                           : out std_logic;  -- reg_tr_10gbe_back1_reset.export
            reg_tr_10gbe_back1_waitrequest_export                     : in  std_logic                     := '0';  -- reg_tr_10gbe_back1_waitrequest.export
            reg_tr_10gbe_back1_write_export                           : out std_logic;  -- reg_tr_10gbe_back1_write.export
            reg_tr_10gbe_back1_writedata_export                       : out std_logic_vector(31 downto 0);  -- reg_tr_10gbe_back1_writedata.export
            reg_tr_10gbe_qsfp_ring_address_export                     : out std_logic_vector(18 downto 0);  -- reg_tr_10gbe_qsfp_ring_address.export
            reg_tr_10gbe_qsfp_ring_clk_export                         : out std_logic;  -- reg_tr_10gbe_qsfp_ring_clk.export
            reg_tr_10gbe_qsfp_ring_read_export                        : out std_logic;  -- reg_tr_10gbe_qsfp_ring_read.export
            reg_tr_10gbe_qsfp_ring_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_tr_10gbe_qsfp_ring_readdata.export
            reg_tr_10gbe_qsfp_ring_reset_export                       : out std_logic;  -- reg_tr_10gbe_qsfp_ring_reset.export
            reg_tr_10gbe_qsfp_ring_waitrequest_export                 : in  std_logic                     := '0';  -- reg_tr_10gbe_qsfp_ring_waitrequest.export
            reg_tr_10gbe_qsfp_ring_write_export                       : out std_logic;  -- reg_tr_10gbe_qsfp_ring_write.export
            reg_tr_10gbe_qsfp_ring_writedata_export                   : out std_logic_vector(31 downto 0);  -- reg_tr_10gbe_qsfp_ring_writedata.export
            reg_unb_pmbus_address_export                              : out std_logic_vector(5 downto 0);  -- reg_unb_pmbus_address.export
            reg_unb_pmbus_clk_export                                  : out std_logic;  -- reg_unb_pmbus_clk.export
            reg_unb_pmbus_read_export                                 : out std_logic;  -- reg_unb_pmbus_read.export
            reg_unb_pmbus_readdata_export                             : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_unb_pmbus_readdata.export
            reg_unb_pmbus_reset_export                                : out std_logic;  -- reg_unb_pmbus_reset.export
            reg_unb_pmbus_write_export                                : out std_logic;  -- reg_unb_pmbus_write.export
            reg_unb_pmbus_writedata_export                            : out std_logic_vector(31 downto 0);  -- reg_unb_pmbus_writedata.export
            reg_unb_sens_address_export                               : out std_logic_vector(5 downto 0);  -- reg_unb_sens_address.export
            reg_unb_sens_clk_export                                   : out std_logic;  -- reg_unb_sens_clk.export
            reg_unb_sens_read_export                                  : out std_logic;  -- reg_unb_sens_read.export
            reg_unb_sens_readdata_export                              : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_unb_sens_readdata.export
            reg_unb_sens_reset_export                                 : out std_logic;  -- reg_unb_sens_reset.export
            reg_unb_sens_write_export                                 : out std_logic;  -- reg_unb_sens_write.export
            reg_unb_sens_writedata_export                             : out std_logic_vector(31 downto 0);  -- reg_unb_sens_writedata.export
            reg_wdi_address_export                                    : out std_logic_vector(0 downto 0);  -- reg_wdi_address.export
            reg_wdi_clk_export                                        : out std_logic;  -- reg_wdi_clk.export
            reg_wdi_read_export                                       : out std_logic;  -- reg_wdi_read.export
            reg_wdi_readdata_export                                   : in  std_logic_vector(31 downto 0) := (others => '0');  -- reg_wdi_readdata.export
            reg_wdi_reset_export                                      : out std_logic;  -- reg_wdi_reset.export
            reg_wdi_write_export                                      : out std_logic;  -- reg_wdi_write.export
            reg_wdi_writedata_export                                  : out std_logic_vector(31 downto 0);  -- reg_wdi_writedata.export
            reset_reset_n                                             : in  std_logic                     := '0';  -- reset.reset_n
            rom_system_info_address_export                            : out std_logic_vector(9 downto 0);  -- rom_system_info_address.export
            rom_system_info_clk_export                                : out std_logic;  -- rom_system_info_clk.export
            rom_system_info_read_export                               : out std_logic;  -- rom_system_info_read.export
            rom_system_info_readdata_export                           : in  std_logic_vector(31 downto 0) := (others => '0');  -- rom_system_info_readdata.export
            rom_system_info_reset_export                              : out std_logic;  -- rom_system_info_reset.export
            rom_system_info_write_export                              : out std_logic;  -- rom_system_info_write.export
            rom_system_info_writedata_export                          : out std_logic_vector(31 downto 0)  -- rom_system_info_writedata.export
        );
    end component qsys_unb2b_test;
end qsys_unb2b_test_pkg;
