--------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;

package lofar2_unb2c_ring_pkg is
 -----------------------------------------------------------------------------
  -- Revision control
  -----------------------------------------------------------------------------

  type t_lofar2_unb2c_ring_config is record
    N_ring_lanes      : natural;
    N_error_counts    : natural;
  end record;

  constant c_one  : t_lofar2_unb2c_ring_config := (1, 8);
  constant c_full : t_lofar2_unb2c_ring_config := (8, 8);

  -- Function to select the revision configuration.
  function func_sel_revision_rec(g_design_name : string) return t_lofar2_unb2c_ring_config;
end lofar2_unb2c_ring_pkg;

package body lofar2_unb2c_ring_pkg is
  function func_sel_revision_rec(g_design_name : string) return t_lofar2_unb2c_ring_config is
  begin
    if    g_design_name = "lofar2_unb2c_ring_one"        then return c_one;
    else  return c_full;
    end if;

  end;
end lofar2_unb2c_ring_pkg;
