-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for dp_bsn_scheduler
-- Description:
-- . Write scheduled_bsn[]
-- . Read current_bsn[]
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |           wr scheduled_bsn[31: 0] / rd current_bsn[31: 0]             |  0
--  |-----------------------------------------------------------------------|
--  |           wr scheduled_bsn[63:32] / rd current_bsn[63:32]             |  1
--  |-----------------------------------------------------------------------|

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity dp_bsn_scheduler_reg is
  generic (
    g_cross_clock_domain : boolean := true  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
  );
  port (
    -- Clocks and reset
    mm_rst            : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk            : in  std_logic;  -- memory-mapped bus clock
    st_rst            : in  std_logic;  -- reset synchronous with st_clk
    st_clk            : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in            : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out           : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    st_current_bsn    : in  std_logic_vector;
    st_scheduled_bsn  : out std_logic_vector
  );
end dp_bsn_scheduler_reg;

architecture rtl of dp_bsn_scheduler_reg is
  constant c_bsn_w : natural := st_current_bsn'length;

  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => 1,
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => 2,
                                  init_sl  => '0');

  -- Registers in mm_clk domain
  signal mm_current_bsn      : std_logic_vector(c_longword_w - 1 downto 0) := (others => '0');
  signal mm_current_bsn_hi   : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');
  signal mm_scheduled_bsn    : std_logic_vector(c_longword_w - 1 downto 0) := (others => '0');
  signal mm_scheduled_bsn_wr : std_logic;
begin
  ------------------------------------------------------------------------------
  -- MM register access in the mm_clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out             <= c_mem_miso_rst;
      -- Access event, register values
      mm_scheduled_bsn    <= (others => '0');
      mm_scheduled_bsn_wr <= '0';
      mm_current_bsn_hi   <= (others => '0');
    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Access event defaults
      mm_scheduled_bsn_wr <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Write scheduled BSN
          when 0 =>
            mm_scheduled_bsn(31 downto  0) <= sla_in.wrdata(31 downto 0);
          when 1 =>
            mm_scheduled_bsn(63 downto 32) <= sla_in.wrdata(31 downto 0);
            mm_scheduled_bsn_wr <= '1';  -- host must first write low then this high part

          when others => null;  -- not used MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Read current BSN
          when 0 =>
            sla_out.rddata(31 downto  0) <= mm_current_bsn(31 downto  0);
            mm_current_bsn_hi            <= mm_current_bsn(63 downto 32);  -- first read low part and preserve high part
          when 1 =>
            sla_out.rddata(31 downto  0) <= mm_current_bsn_hi;

          when others => null;  -- not used MM addresses
        end case;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Transfer register value between mm_clk and st_clk domain.
  -- If the function of the register ensures that the value will not be used
  -- immediately when it was set, then the transfer between the clock domains
  -- can be done by wires only. Otherwise if the change in register value can
  -- have an immediate effect then the bit or word value needs to be transfered
  -- using:
  --
  -- . common_async            --> for single-bit level signal
  -- . common_spulse           --> for single-bit pulse signal
  -- . common_reg_cross_domain --> for a multi-bit (a word) signal
  --
  -- Typically always use a crossing component for the single bit signals (to
  -- be on the save side) and only use a crossing component for the word
  -- signals if it is necessary (to avoid using more logic than necessary).
  ------------------------------------------------------------------------------

  no_cross : if g_cross_clock_domain = false generate
    mm_current_bsn(c_bsn_w - 1 downto 0) <= st_current_bsn;  -- MM user may read current_bsn twice to avoid small chance that the high part of the double word changed (i.e. incremented)

    p_st_clk : process(st_rst, st_clk)
    begin
      if st_rst = '1' then
        st_scheduled_bsn <= TO_UVEC(0, c_bsn_w);
      elsif rising_edge(st_clk) then
        if mm_scheduled_bsn_wr = '1' then
          st_scheduled_bsn <= mm_scheduled_bsn(c_bsn_w - 1 downto 0);  -- use wr of mm_scheduled_bsn high part for in_new to ensure proper transfer of double word
        end if;
      end if;
    end process;
  end generate;  -- no_cross

  gen_cross : if g_cross_clock_domain = true generate
    -- write occurs with sufficient margin before it is used, still use common_reg_cross_domain nonetheless
    u_init_bsn : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => mm_rst,
      in_clk      => mm_clk,
      in_new      => mm_scheduled_bsn_wr,  -- use wr of mm_scheduled_bsn high part for in_new to ensure proper transfer of double word
      in_dat      => mm_scheduled_bsn(c_bsn_w - 1 downto 0),
      in_done     => OPEN,  -- pulses when no more pending in_new
      out_rst     => st_rst,
      out_clk     => st_clk,
      out_dat     => st_scheduled_bsn,
      out_new     => open
    );

    -- thanks to mm_current_bsn_hi the double word can be read reliably
    u_current_bsn : entity common_lib.common_reg_cross_domain
    port map (
      in_rst      => st_rst,
      in_clk      => st_clk,
      in_new      => '1',  -- could use t_dp_sosi sop here to indicate in_new, but using default '1' is fine too
      in_dat      => st_current_bsn,
      in_done     => OPEN,  -- pulses when no more pending in_new
      out_rst     => mm_rst,
      out_clk     => mm_clk,
      out_dat     => mm_current_bsn(c_bsn_w - 1 downto 0),
      out_new     => open
    );
  end generate;  -- gen_cross
end rtl;
