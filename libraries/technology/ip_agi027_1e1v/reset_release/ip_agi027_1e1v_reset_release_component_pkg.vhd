-------------------------------------------------------------------------------
--
-- Copyright 2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author:
-- . D.F. Brouwer
-- Purpose:
-- . Component declarations for reset release IP blocks
-- Description:
-- . Wrapper for reset release IPs, specific for technology c_tech_agi027_1e1v
--   that is used by the iwave FPGA platform.
-- . The Reset Release IP is necessary to use for Agilex7 devices.
--   See README.txt for more information about this.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package ip_agi027_1e1v_reset_release_component_pkg is
  -----------------------------------------------------------------------------
  -- Agilex 7 components
  -----------------------------------------------------------------------------

  component ip_agi027_1e1v_reset_release_ci is
    port (
      ninit_done  : out std_logic   -- ninit_done
    );
  end component;

  component ip_agi027_1e1v_reset_release_ri is
    port (
      ninit_done  : out std_logic   -- reset
    );
  end component ip_agi027_1e1v_reset_release_ri;
end ip_agi027_1e1v_reset_release_component_pkg;

package body ip_agi027_1e1v_reset_release_component_pkg is
end ip_agi027_1e1v_reset_release_component_pkg;
