-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra, D. vd Schuur
-- Purpose: Simple memory access (for MM control interface)
-- Description:
--
-- Assume the MM bus is for a 32 bit processor, therefore on the processor
-- side of a memory peripheral typcially use c_word_w = 32 for the address
-- and data fields in the MM bus records. However the MM bus can also be used
-- on the user side of a memory peripheral and there the data width should
-- not be limited by the processor type but rather by the maximum user data
-- width on the streaming interface.
--
-- The std_logic_vector widths in the record need to be defined, because in
-- a record they can not be unconstrained. A signal that needs less address
-- or data width simply leaves the unused MSbits at 'X'. The actually used
-- width of a memory gets set via a generic record type t_c_mem.
--
-- The alternative is to not put the std_logic_vector elements in the record,
-- and declare them seperately, however then the compact representation that
-- records provide gets lost, because the record then only contains wr_en and
-- rd_en. Another alternative is to define the address as a integer and the
-- data as an integer. However this limits their range to 32 bit numbers,
-- which can be too few for data.

-- Do not change these widths, because c_word_w just fits in a VHDL INTEGER
-- Should wider address range or data width be needed, then define a new
-- record type eg. t_mem_ctlr or t_mem_bus for that with
-- sufficient widths.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

package common_mem_pkg is
  -- Choose smallest maximum slv lengths that fit all use cases, because unconstrained record fields slv is not allowed
  constant c_mem_address_w  : natural := 32;  -- address range (suits 32-bit processor)
  constant c_mem_data_w     : natural := 72;  -- data width (suit up to 8 bytes, that can also be 9 bit bytes)
  constant c_mem_address_sz : natural := c_mem_address_w / c_byte_w;
  constant c_mem_data_sz    : natural := c_mem_data_w / c_byte_w;

  type t_mem_miso is record  -- Master In Slave Out. For backward compatibility only. Use t_mem_copi for new designs.
    rddata      : std_logic_vector(c_mem_data_w - 1 downto 0);  -- data width (suits 1, 2 or 4 bytes)
    rdval       : std_logic;
    waitrequest : std_logic;
  end record;

  type t_mem_mosi is record  -- Master Out Slave In.  For backward compatibility only. Use t_mem_cipo for new designs.
    address     : std_logic_vector(c_mem_address_w - 1 downto 0);  -- address range (suits 32-bit processor)
    wrdata      : std_logic_vector(c_mem_data_w - 1 downto 0);  -- data width (suits 1, 2 or 4 bytes)
    wr          : std_logic;
    rd          : std_logic;
  end record;

  constant c_mem_miso_rst : t_mem_miso := ((others => '0'), '0', '0');
  constant c_mem_mosi_rst : t_mem_mosi := ((others => '0'), (others => '0'), '0', '0');

  -- Multi port array for MM records
  type t_mem_miso_arr is array (integer range <>) of t_mem_miso;
  type t_mem_mosi_arr is array (integer range <>) of t_mem_mosi;

  -- MOSI/MISO subtypes
  subtype t_mem_copi is t_mem_mosi;  -- Controller Out Peripheral In
  subtype t_mem_cipo is t_mem_miso;  -- Peripheral In Controller Out

  constant c_mem_cipo_rst : t_mem_cipo := c_mem_miso_rst;
  constant c_mem_copi_rst : t_mem_copi := c_mem_mosi_rst;

  subtype t_mem_cipo_arr is t_mem_miso_arr;
  subtype t_mem_copi_arr is t_mem_mosi_arr;

  -- Reset only the control fields of the MM record
  function RESET_MEM_MOSI_CTRL(mosi : t_mem_mosi) return t_mem_mosi;  -- deprecated, use RESET_MEM_COPI_CTRL() instead
  function RESET_MEM_COPI_CTRL(copi : t_mem_copi) return t_mem_copi;
  function RESET_MEM_COPI_CTRL(copi_arr : t_mem_copi_arr) return t_mem_copi_arr;

  function RESET_MEM_MISO_CTRL(miso : t_mem_miso) return t_mem_miso;  -- deprecated, use RESET_MEM_CIPO_CTRL() instead
  function RESET_MEM_CIPO_CTRL(cipo : t_mem_cipo) return t_mem_cipo;
  function RESET_MEM_CIPO_CTRL(cipo_arr : t_mem_cipo_arr) return t_mem_cipo_arr;

  -- Resize functions to fit an integer or an SLV in the corresponding t_mem_miso or t_mem_mosi field width
  function TO_MEM_ADDRESS(n : integer) return std_logic_vector;  -- unsigned, use integer to support 32 bit range
  function TO_MEM_DATA(   n : integer) return std_logic_vector;  -- unsigned, alias of TO_MEM_DATA()
  function TO_MEM_UDATA(  n : integer) return std_logic_vector;  -- unsigned, use integer to support 32 bit range
  function TO_MEM_SDATA(  n : integer) return std_logic_vector;  -- sign extended
  function RESIZE_MEM_ADDRESS(vec : std_logic_vector) return std_logic_vector;  -- unsigned
  function RESIZE_MEM_DATA(   vec : std_logic_vector) return std_logic_vector;  -- unsigned, alias of RESIZE_MEM_UDATA
  function RESIZE_MEM_UDATA(  vec : std_logic_vector) return std_logic_vector;  -- unsigned
  function RESIZE_MEM_SDATA(  vec : std_logic_vector) return std_logic_vector;  -- sign extended
  function RESIZE_MEM_XDATA(  vec : std_logic_vector) return std_logic_vector;  -- set unused MSBits to 'X'

  ------------------------------------------------------------------------------
  -- Procedures to access MM bus
  -- . no mm_clk, combinatoral inputs only, to allow use in a state machine
  -- . similar proc_mem_mm_bus_*() procs in tb_common_mem_pkg.vhd do have
  --   mm_clk inputs
  -- . if mm_copi.waitrequest is used, then issue the MM access and externaly
  --   check and wait for mm_copi.waitrequest = '0' before removing the MM
  --   access.
  ------------------------------------------------------------------------------
  procedure proc_mem_bus_wr(constant wr_addr : in  natural;
                            constant wr_data : in  integer;
                            signal   mm_copi : out t_mem_copi);

  procedure proc_mem_bus_wr(constant wr_addr : in  natural;
                            constant wr_data : in  std_logic_vector;
                            signal   mm_copi : out t_mem_copi);

  procedure proc_mem_bus_rd(constant wr_addr : in  natural;
                            signal   mm_copi : out t_mem_copi);

  ------------------------------------------------------------------------------
  -- Burst memory access (for DDR access interface)
  ------------------------------------------------------------------------------

  -- Choose smallest maximum slv lengths that fit all use cases, because unconstrained record fields slv is not allowed
  constant c_mem_ctlr_address_w    : natural := 32;
  constant c_mem_ctlr_data_w       : natural := 576;
  constant c_mem_ctlr_burstsize_w  : natural := c_mem_ctlr_address_w;

  type t_mem_ctlr_miso is record
    rddata        : std_logic_vector(c_mem_ctlr_data_w - 1 downto 0);
    rdval         : std_logic;
    waitrequest_n : std_logic;  -- comparable to DP siso.ready
    done          : std_logic;  -- comparable to DP siso.xon, not part of Avalon bus, can eg. act as init done or init ok or ready for next block, useful for DDR controller
    cal_ok        : std_logic;
    cal_fail      : std_logic;
  end record;

  type t_mem_ctlr_mosi is record
    address       : std_logic_vector(c_mem_ctlr_address_w - 1 downto 0);
    wrdata        : std_logic_vector(c_mem_ctlr_data_w - 1 downto 0);
    wr            : std_logic;
    rd            : std_logic;
    burstbegin    : std_logic;
    burstsize     : std_logic_vector(c_mem_ctlr_burstsize_w - 1 downto 0);
    flush         : std_logic;  -- not part of Avalon bus, but useful for DDR driver
  end record;

  constant c_mem_ctlr_miso_rst : t_mem_ctlr_miso := ((others => '0'), '0', '0', '0', '0', '0');
  constant c_mem_ctlr_mosi_rst : t_mem_ctlr_mosi := ((others => '0'), (others => '0'), '0', '0', '0', (others => '0'), '0');

  -- Multi port array for mem_ctlr records
  type t_mem_ctlr_miso_arr is array (integer range <>) of t_mem_ctlr_miso;
  type t_mem_ctlr_mosi_arr is array (integer range <>) of t_mem_ctlr_mosi;

  -- Resize functions to fit an integer or an SLV in the corresponding t_mem_ctlr_miso or t_mem_ctlr_mosi field width
  function TO_MEM_CTLR_ADDRESS(  n : integer) return std_logic_vector;  -- unsigned, use integer to support 32 bit range
  function TO_MEM_CTLR_DATA(     n : integer) return std_logic_vector;  -- unsigned
  function TO_MEM_CTLR_BURSTSIZE(n : integer) return std_logic_vector;  -- unsigned

  function RESIZE_MEM_CTLR_ADDRESS(  vec : std_logic_vector) return std_logic_vector;  -- unsigned
  function RESIZE_MEM_CTLR_DATA(     vec : std_logic_vector) return std_logic_vector;  -- unsigned
  function RESIZE_MEM_CTLR_BURSTSIZE(vec : std_logic_vector) return std_logic_vector;  -- unsigned

  ------------------------------------------------------------------------------
  -- RAM block memory and MM register defintions
  ------------------------------------------------------------------------------
  type t_c_mem is record
    latency   : natural;  -- read latency
    adr_w     : natural;
    dat_w     : natural;
    nof_dat   : natural;  -- optional, nof dat words <= 2**adr_w
    init_sl   : std_logic;  -- optional, init all dat words to std_logic '0', '1' or 'X'
    --init_file : STRING;     -- "UNUSED", unconstrained length can not be in record
  end record;

  constant c_mem_ram_rd_latency : natural := 2;  -- note common_ram_crw_crw(stratix4) now also supports read latency 1
  constant c_mem_ram            : t_c_mem := (c_mem_ram_rd_latency, 10,  9, 2**10, 'X');  -- 1 M9K

  constant c_mem_reg_rd_latency : natural := 1;
  constant c_mem_reg            : t_c_mem := (c_mem_reg_rd_latency,  1, 32,     1, 'X');

  constant c_mem_reg_init_w     : natural := 1 * 256 * 32;  -- >= largest expected value of dat_w*nof_dat (256 * 32 bit = 1k byte)

  ------------------------------------------------------------------------------
  -- Functions to swap endianess
  ------------------------------------------------------------------------------
  function func_mem_swap_endianess(mm : t_mem_miso; sz : natural) return t_mem_miso;
  function func_mem_swap_endianess(mm : t_mem_mosi; sz : natural) return t_mem_mosi;
end common_mem_pkg;

package body common_mem_pkg is
  -- Reset only the control fields of the MM record
  function RESET_MEM_MOSI_CTRL(mosi : t_mem_mosi) return t_mem_mosi is
    variable v_mosi : t_mem_mosi := mosi;
  begin
    v_mosi.rd := '0';
    v_mosi.wr := '0';
    return v_mosi;
  end RESET_MEM_MOSI_CTRL;

  function RESET_MEM_COPI_CTRL(copi : t_mem_copi) return t_mem_copi is
  begin
    return RESET_MEM_MOSI_CTRL(copi);
  end;

  function RESET_MEM_COPI_CTRL(copi_arr : t_mem_copi_arr) return t_mem_copi_arr is
    variable v_copi_arr : t_mem_copi_arr(copi_arr'range) := copi_arr;
  begin
    for I in copi_arr'range loop
      v_copi_arr(I) := RESET_MEM_COPI_CTRL(copi_arr(I));
    end loop;
    return v_copi_arr;
  end;

  function RESET_MEM_MISO_CTRL(miso : t_mem_miso) return t_mem_miso is
    variable v_miso : t_mem_miso := miso;
  begin
    v_miso.rdval       := '0';
    v_miso.waitrequest := '0';
    return v_miso;
  end RESET_MEM_MISO_CTRL;

  function RESET_MEM_CIPO_CTRL(cipo : t_mem_cipo) return t_mem_cipo is
  begin
    return RESET_MEM_MISO_CTRL(cipo);
  end RESET_MEM_CIPO_CTRL;

  function RESET_MEM_CIPO_CTRL(cipo_arr : t_mem_cipo_arr) return t_mem_cipo_arr is
    variable v_cipo_arr : t_mem_cipo_arr(cipo_arr'range) := cipo_arr;
  begin
    for I in cipo_arr'range loop
      v_cipo_arr(I) := RESET_MEM_CIPO_CTRL(cipo_arr(I));
    end loop;
    return v_cipo_arr;
  end RESET_MEM_CIPO_CTRL;

  -- Resize functions to fit an integer or an SLV in the corresponding t_mem_miso or t_mem_mosi field width
  function TO_MEM_ADDRESS(n : integer) return std_logic_vector is
  begin
    return RESIZE_UVEC(TO_SVEC(n, 32), c_mem_address_w);
  end TO_MEM_ADDRESS;

  function TO_MEM_DATA(n : integer) return std_logic_vector is
  begin
    return TO_MEM_UDATA(n);
  end TO_MEM_DATA;

  function TO_MEM_UDATA(n : integer) return std_logic_vector is
  begin
    return RESIZE_UVEC(TO_SVEC(n, 32), c_mem_data_w);
  end TO_MEM_UDATA;

  function TO_MEM_SDATA(n : integer) return std_logic_vector is
  begin
    return RESIZE_SVEC(TO_SVEC(n, 32), c_mem_data_w);
  end TO_MEM_SDATA;

  function RESIZE_MEM_ADDRESS(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_mem_address_w);
  end RESIZE_MEM_ADDRESS;

  function RESIZE_MEM_DATA(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_MEM_UDATA(vec);
  end RESIZE_MEM_DATA;

  function RESIZE_MEM_UDATA(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_mem_data_w);
  end RESIZE_MEM_UDATA;

  function RESIZE_MEM_SDATA(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_SVEC(vec, c_mem_data_w);
  end RESIZE_MEM_SDATA;

  function RESIZE_MEM_XDATA(vec : std_logic_vector) return std_logic_vector is
    variable v_vec : std_logic_vector(c_mem_data_w - 1 downto 0) := (others => 'X');
  begin
    v_vec(vec'length - 1 downto 0) := vec;
    return v_vec;
  end RESIZE_MEM_XDATA;

  -- Procedures to access MM bus
  procedure proc_mem_bus_wr(constant wr_addr : in  natural;
                            constant wr_data : in  integer;
                            signal   mm_copi : out t_mem_copi) is
  begin
    mm_copi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_copi.wrdata  <= TO_MEM_DATA(wr_data);
    mm_copi.wr      <= '1';
  end proc_mem_bus_wr;

  procedure proc_mem_bus_wr(constant wr_addr : in  natural;
                            constant wr_data : in  std_logic_vector;
                            signal   mm_copi : out t_mem_copi) is
  begin
    mm_copi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_copi.wrdata  <= RESIZE_MEM_DATA(wr_data);
    mm_copi.wr      <= '1';
  end proc_mem_bus_wr;

  procedure proc_mem_bus_rd(constant wr_addr : in  natural;
                            signal   mm_copi : out t_mem_copi) is
  begin
    mm_copi.address <= TO_MEM_ADDRESS(wr_addr);
    mm_copi.rd      <= '1';
  end proc_mem_bus_rd;

  -- Resize functions to fit an integer or an SLV in the corresponding t_mem_miso or t_mem_mosi field width
  function TO_MEM_CTLR_ADDRESS(n : integer) return std_logic_vector is
  begin
    return RESIZE_UVEC(TO_SVEC(n, 32), c_mem_ctlr_address_w);
  end TO_MEM_CTLR_ADDRESS;

  function TO_MEM_CTLR_DATA(n : integer) return std_logic_vector is
  begin
    return RESIZE_UVEC(TO_SVEC(n, 32), c_mem_ctlr_data_w);
  end TO_MEM_CTLR_DATA;

  function TO_MEM_CTLR_BURSTSIZE(n : integer) return std_logic_vector is
  begin
    return RESIZE_UVEC(TO_SVEC(n, 32), c_mem_ctlr_burstsize_w);
  end TO_MEM_CTLR_BURSTSIZE;

  function RESIZE_MEM_CTLR_ADDRESS(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_mem_ctlr_address_w);
  end RESIZE_MEM_CTLR_ADDRESS;

  function RESIZE_MEM_CTLR_DATA(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_mem_ctlr_data_w);
  end RESIZE_MEM_CTLR_DATA;

  function RESIZE_MEM_CTLR_BURSTSIZE(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_mem_ctlr_burstsize_w);
  end RESIZE_MEM_CTLR_BURSTSIZE;

  -- Functions to swap endianess
  function func_mem_swap_endianess(mm : t_mem_miso; sz : natural) return t_mem_miso is
    variable v_mm : t_mem_miso;
  begin
    -- Master In Slave Out
    v_mm.rddata  := hton(mm.rddata, sz);
    return v_mm;
  end func_mem_swap_endianess;

  function func_mem_swap_endianess(mm : t_mem_mosi; sz : natural) return t_mem_mosi is
    variable v_mm : t_mem_mosi;
  begin
    -- Master Out Slave In
    v_mm.address := mm.address;
    v_mm.wrdata  := hton(mm.wrdata, sz);
    v_mm.wr      := mm.wr;
    v_mm.rd      := mm.rd;
    return v_mm;
  end func_mem_swap_endianess;
end common_mem_pkg;
