--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose : Fast simulation model for 10G XGMII over 1 lane
-- Description :
-- Remark:
-- . The sim_10gbase_r has the same entity ports and generics as tech_10gbase_r
--   so that it can directly be mapped in tech_10gbase_r.
-- . The model uses 10/8 overhead to transport the control signalling. Therefore
--   the line rate becomes 12.5 Gbps instead of 10.3125 M for the technology.

library IEEE, common_lib, tech_pll_lib, tech_transceiver_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity sim_10gbase_r is
  generic (
    g_sim            : boolean := false;
    g_nof_channels   : natural := 1
  );
  port (
    -- Transceiver ATX PLL reference clock
    tr_ref_clk_644          : in  std_logic;  -- 644.531250 MHz

    -- XGMII clocks
    clk_156                 : in std_logic;  -- 156.25 MHz
    rst_156                 : in std_logic;

    -- XGMII interface
    xgmii_tx_ready_arr      : out std_logic_vector(g_nof_channels - 1 downto 0);  -- can be used for xon flow control
    xgmii_rx_ready_arr      : out std_logic_vector(g_nof_channels - 1 downto 0);  -- typically leave not connected
    xgmii_tx_dc_arr         : in  t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit
    xgmii_rx_dc_arr         : out t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit

    -- PHY serial IO
    tx_serial_arr           : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_serial_arr           : in  std_logic_vector(g_nof_channels - 1 downto 0)
  );
end sim_10gbase_r;

architecture str of sim_10gbase_r is
  constant c_tr_clk_period    : time := tech_pll_clk_156_period;  -- 6.400020 ns ~= 156.25 MHz --> model line rate : 156.25 MHz * 10/8 (encoding) * 64b (data width) = 12500 Mbps / lane

  constant c_serdes_data_w    : natural := c_xgmii_data_w;  -- 64 b
  constant c_serdes_ctrl_w    : natural := c_xgmii_ctrl_w;  -- 8 b

  -- XGMII control bits (one for each XGMII lane):
  signal xgmii_tx_c_arr            : t_xgmii_c_arr(g_nof_channels - 1 downto 0);
  signal xgmii_rx_c_arr            : t_xgmii_c_arr(g_nof_channels - 1 downto 0);

  -- XGMII data
  signal xgmii_tx_d_arr            : t_xgmii_d_arr(g_nof_channels - 1 downto 0);
  signal xgmii_rx_d_arr            : t_xgmii_d_arr(g_nof_channels - 1 downto 0);
begin
  gen_nof_10gbase_r : for i in g_nof_channels - 1 downto 0 generate
    -- Rewire XGMII
    xgmii_tx_d_arr(i) <= func_xgmii_d(xgmii_tx_dc_arr(i));
    xgmii_tx_c_arr(i) <= func_xgmii_c(xgmii_tx_dc_arr(i));

    xgmii_rx_dc_arr(i) <= func_xgmii_dc(xgmii_rx_d_arr(i), xgmii_rx_c_arr(i));

    -- Model tx_ready
    u_areset_tx_rdy : entity common_lib.common_areset
    generic map(
      g_rst_level => '0',
      g_delay_len => 40
    )
    port map(
      clk     => clk_156,
      in_rst  => rst_156,
      out_rst => xgmii_tx_ready_arr(i)
    );

    -- Model rx_ready
    u_areset_rx_rdy : entity common_lib.common_areset
    generic map(
      g_rst_level => '0',
      g_delay_len => 80
    )
    port map(
      clk     => clk_156,
      in_rst  => rst_156,
      out_rst => xgmii_rx_ready_arr(i)
    );

    u_ser: entity tech_transceiver_lib.sim_transceiver_serializer
    generic map (
      g_data_w        => c_serdes_data_w,
      g_tr_clk_period => c_tr_clk_period
    )
    port map (
      tr_clk             => clk_156,
      tr_rst             => rst_156,

      tx_in_data         => xgmii_tx_d_arr(i),
      tx_in_ctrl         => xgmii_tx_c_arr(i),

      tx_serial_out      => tx_serial_arr(i)
    );

    u_des: entity tech_transceiver_lib.sim_transceiver_deserializer
    generic map (
      g_data_w        => c_serdes_data_w,
      g_tr_clk_period => c_tr_clk_period
    )
    port map (
      tr_clk             => clk_156,
      tr_rst             => rst_156,

      rx_out_data        => xgmii_rx_d_arr(i),
      rx_out_ctrl        => xgmii_rx_c_arr(i),

      rx_serial_in       => rx_serial_arr(i)
    );
  end generate;
end str;
