--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

package dp_packet_pkg is
  ------------------------------------------------------------------------------
  -- DP Packet definition:
  --
  --         ----------------------------------------------
  --         |chan| bsn        | data ...             |err|
  --         ----------------------------------------------
  --           32   64           N * w                 32
  --
  --  len = ceil_div(field width / data width) so e.g.:
  --
  --  len =     4    8           N * 1                  4    for w  =  8 .. 9
  --  len =     4    7           N * 1                  4    for w  = 10
  --  len =     3    6           N * 1                  3    for w  = 11 .. 12
  --  len =     3    5           N * 1                  3    for w  = 13 .. 15
  --  len =     2    4           N * 1                  2    for w  = 16 .. 31
  --  len =     1    2           N * 1                  1    for w  = 32 .. 63
  --  len =     1    1           N * 1                  1    for w >= 64
  --

  -- The DP packet field widths
  constant c_dp_packet_channel_w   : natural := 32;  -- 4 bytes
  constant c_dp_packet_bsn_w       : natural := 64;  -- 8 bytes
  constant c_dp_packet_error_w     : natural := 32;  -- 4 bytes

  constant c_dp_packet_sync_bi     : natural := c_dp_packet_bsn_w - 1;  -- = use MSbit [63] of BSN field to transport the SOSI sync
  constant c_dp_packet_bsn_hi      : natural := c_dp_packet_bsn_w - 2;  -- = use rest [62:0] of BSN field to transport the SOSI BSN

  -- Determine the length in nof data words to fit the DP packet overhead fields
  function func_dp_packet_overhead_len(c_data_w : natural) return natural;
end dp_packet_pkg;

package body dp_packet_pkg is
  function func_dp_packet_overhead_len(c_data_w : natural) return natural is
  begin
    -- Calculate the total DP PACKET overhead length of header (channel and bsn words) + lenght of tail (error words).
    return ceil_div(c_dp_packet_channel_w, c_data_w) +
           ceil_div(c_dp_packet_bsn_w, c_data_w) +
           ceil_div(c_dp_packet_error_w, c_data_w);
  end;
end dp_packet_pkg;
