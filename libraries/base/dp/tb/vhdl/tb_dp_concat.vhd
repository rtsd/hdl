-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_concat is
  generic (
    g_data_w               : natural := 16 * 3;  -- g_data_w/g_symbol_w must be an integer
    g_symbol_w             : natural := 16;
    g_random_control       : boolean := true  -- use TRUE for random snk_in.valid and src_in.ready control
  );
end tb_dp_concat;

architecture tb of tb_dp_concat is
  constant c_period               : time    := 100 ns;

  constant c_rl                   : natural := 1;
  constant c_nof_symbols_per_data : natural := g_data_w / g_symbol_w;

  constant c_sync_period          : natural := 3;
  constant c_sync_offset          : natural := 1;

  constant c_verify_mode          : boolean := c_nof_symbols_per_data = 1;  -- when FALSE verify per frame

  signal tb_end         : std_logic := '0';
  signal rst            : std_logic;
  signal clk            : std_logic := '0';

  signal random_0       : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1       : std_logic_vector(15 downto 0) := (others => '0');
  signal random_2       : std_logic_vector(17 downto 0) := (others => '0');

  signal in_en          : std_logic_vector(0 to 1) := (others => '1');
  signal in_siso_arr    : t_dp_siso_arr(0 to 1);
  signal in_sosi_arr    : t_dp_sosi_arr(0 to 1) := (others => c_dp_sosi_rst);
  signal out_siso       : t_dp_siso := c_dp_siso_rdy;
  signal out_sosi       : t_dp_sosi;

  signal verify_sync    : std_logic := '0';
  signal verify_en      : std_logic := '0';
  signal verify_done    : std_logic;

  signal in_data_0      : std_logic_vector(g_data_w - 1 downto 0);
  signal in_val_0       : std_logic;
  signal in_sop_0       : std_logic;
  signal in_eop_0       : std_logic;
  signal in_data_1      : std_logic_vector(g_data_w - 1 downto 0);
  signal in_val_1       : std_logic;
  signal in_sop_1       : std_logic;
  signal in_eop_1       : std_logic;
  signal out_data       : std_logic_vector(g_data_w - 1 downto 0);
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_eop        : std_logic;
  signal prev_out_ready : std_logic_vector(0 to c_rl);
  signal prev_out_data  : std_logic_vector(g_data_w - 1 downto 0);
  signal expected_data  : std_logic_vector(g_data_w - 1 downto 0);
  signal hold_out_sop   : std_logic;
begin
  -- run 100 us

  rst  <= '1', '0' after c_period * 7;
  clk  <=  not clk or tb_end after c_period / 2;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);
  random_2 <= func_common_random(random_2) when rising_edge(clk);

  gen_random_control : if g_random_control = true generate
    in_en(0)       <= random_0(random_0'high);
    in_en(1)       <= random_1(random_1'high);
    out_siso.ready <= random_2(random_2'high);
  end generate;  -- -- else the in_en and out_siso lines are always active

  -- For output verify the p_stimuli_0 and p_stimuli_1 must generate head and tail frames that result in concatenated output frames that
  -- have ever incrementing valid data.

  p_stimuli_0 : process
    variable v_bsn : natural := 0;
  begin
    wait for 15 * c_period;
    wait until rising_edge(clk);

    -- Head frames
    -- . Stimulus c_sync='1' must match v_bsn MOD c_sync_period(=3) = c_sync_offset(=1) to fit proc_dp_verify_sync()
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,   1,  1, v_bsn + 0, '0', clk, in_en(0), in_siso_arr(0), in_sosi_arr(0));  -- 1
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,   3,  1, v_bsn + 1, '1', clk, in_en(0), in_siso_arr(0), in_sosi_arr(0));  -- 3
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,   5,  5, v_bsn + 2, '0', clk, in_en(0), in_siso_arr(0), in_sosi_arr(0));  -- 5,  6,  7,  8,  9
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,  16, 10, v_bsn + 3, '0', clk, in_en(0), in_siso_arr(0), in_sosi_arr(0));  -- 16, 17, 18, 19, 20, 21, 22, 23, 24, 25
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,  37, 50, v_bsn + 4, '1', clk, in_en(0), in_siso_arr(0), in_sosi_arr(0));
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w, 137, 101, v_bsn + 5, '0', clk, in_en(0), in_siso_arr(0), in_sosi_arr(0));

    wait;
  end process;

  p_stimuli_1 : process
  begin
    verify_done <= '0';
    wait for 10 * c_period;
    wait until rising_edge(clk);

    -- Tail frames
    -- . Set c_sync='1' for tail frame to verify that it is ignored
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,   2,  1, 0, '1', clk, in_en(1), in_siso_arr(1), in_sosi_arr(1));  -- 2
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,   4,  1, 0, '1', clk, in_en(1), in_siso_arr(1), in_sosi_arr(1));  -- 4
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,  10,  6, 0, '1', clk, in_en(1), in_siso_arr(1), in_sosi_arr(1));  -- 10, 11, 12, 13, 14, 15
    wait for 15 * c_period;
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,  26, 11, 0, '1', clk, in_en(1), in_siso_arr(1), in_sosi_arr(1));  -- 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w,  87, 50, 0, '1', clk, in_en(1), in_siso_arr(1), in_sosi_arr(1));
    proc_dp_gen_frame(c_rl, g_data_w, g_symbol_w, 238, 100, 0, '1', clk, in_en(1), in_siso_arr(1), in_sosi_arr(1));

    -- the end
    expected_data <= TO_UVEC(136, g_data_w);
    wait for 10 * c_period;
    verify_done <= '1';
    wait until rising_edge(clk);
    verify_done <= '0';

    wait for 100 * c_period;
    wait until rising_edge(clk);
    tb_end <= '1';
    wait;
  end process;

  dut : entity work.dp_concat
  generic map (
    g_data_w    => g_data_w,
    g_symbol_w  => g_symbol_w
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_out_arr => in_siso_arr,  -- OUT = request to upstream ST source
    snk_in_arr  => in_sosi_arr,
    src_in      => out_siso,  -- IN  = request from downstream ST sink
    src_out     => out_sosi
  );

  -- Input data
  in_data_0 <= in_sosi_arr(0).data(g_data_w - 1 downto 0);
  in_data_1 <= in_sosi_arr(1).data(g_data_w - 1 downto 0);
  in_val_0 <= in_sosi_arr(0).valid;
  in_sop_0 <= in_sosi_arr(0).sop;
  in_eop_0 <= in_sosi_arr(0).eop;
  in_val_1 <= in_sosi_arr(1).valid;
  in_sop_1 <= in_sosi_arr(1).sop;
  in_eop_1 <= in_sosi_arr(1).eop;

  -- Output data
  out_data <= out_sosi.data(g_data_w - 1 downto 0);
  out_val  <= out_sosi.valid;
  out_sop  <= out_sosi.sop;
  out_eop  <= out_sosi.eop;

  -- Output verify
  proc_dp_verify_en(c_verify_mode, clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, verify_en);

  -- Using proc_dp_verify_data() only suits c_nof_symbols_per_data = 1, therefore use proc_dp_verify_symbols() instead
  gen_verify_data : if c_nof_symbols_per_data = 1 generate
    proc_dp_verify_data("out_sosi.data", c_rl, clk, verify_en, out_siso.ready, out_sosi.valid, out_data, prev_out_data);
  end generate;

  -- Using proc_dp_verify_symbols() suits c_nof_symbols_per_data >= 1
  proc_dp_verify_symbols(c_rl, g_data_w, g_symbol_w, clk, verify_en, out_siso.ready, out_sosi.valid, out_sosi.eop, out_data, out_sosi.empty, prev_out_data);

  proc_dp_verify_valid(c_rl, clk, verify_en, out_siso.ready, prev_out_ready, out_sosi.valid);
  proc_dp_verify_sop_and_eop(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, hold_out_sop);

  verify_sync <= '1' when verify_en = '1';
  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_sync, out_sosi.sync, out_sosi.sop, out_sosi.bsn);

  -- Check that the test has ran at all
  proc_dp_verify_value(e_at_least, clk, verify_done, expected_data, out_data);
end tb;
