--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
--   Basic deserializer model for fast transceiver simulation
-- Description:
--   See sim_transceiver_serializer.vhd
-- Remarks:

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity sim_transceiver_deserializer is
  generic(
    g_data_w         : natural := 32;
    g_tr_clk_period  : time := 6.4 ns
  );
  port(
    tb_end        : in  std_logic := '0';

    tr_clk        : in  std_logic;
    tr_rst        : in  std_logic;

    rx_out_data   : out std_logic_vector(g_data_w - 1 downto 0);
    rx_out_ctrl   : out std_logic_vector(g_data_w / c_byte_w - 1 downto 0);
    rx_out_sop    : out std_logic_vector(g_data_w / c_byte_w - 1 downto 0);
    rx_out_eop    : out std_logic_vector(g_data_w / c_byte_w - 1 downto 0);

    rx_serial_in  : in  std_logic
  );
end sim_transceiver_deserializer;

architecture beh of sim_transceiver_deserializer is
  constant c_line_clk_period    : time    := g_tr_clk_period * 8 / 10 / g_data_w;
  constant c_nof_bytes_per_data : natural := g_data_w / c_byte_w;
begin
  p_deserialize: process
    variable v_rx_out_data : std_logic_vector(g_data_w - 1 downto 0);
    variable v_rx_out_ctrl : std_logic_vector(g_data_w / c_byte_w - 1 downto 0);
    variable v_rx_out_sop  : std_logic_vector(g_data_w / c_byte_w - 1 downto 0);
    variable v_rx_out_eop  : std_logic_vector(g_data_w / c_byte_w - 1 downto 0);
  begin
    --rx_out_data <= (OTHERS=>'0');
    rx_out_ctrl <= (others => '0');
    rx_out_sop  <= (others => '0');
    rx_out_eop  <= (others => '0');

    wait until tr_rst = '0';

    -- Align to tr_clk
    wait until rising_edge(tr_clk);

    while tb_end = '0' loop
      -- Wait for half of a serial clk period so data is stable when sampling
      wait for c_line_clk_period / 2;

      -- Data word deserialization cycle
      for byte in 0 to c_nof_bytes_per_data - 1 loop
        -- Deserialize each data byte using 10 bits per byte from the line
        for bit in 0 to c_byte_w - 1 loop
          v_rx_out_data(byte * c_byte_w + bit) := rx_serial_in;  -- Get the 8 data bits of the data byte from the line
          wait for c_line_clk_period;
        end loop;
        v_rx_out_ctrl(byte) := rx_serial_in;  -- Get the 1 control bit from the line for each byte
        wait for c_line_clk_period;
        v_rx_out_sop(byte) := '0';  -- Get the SOP/EOP (tenth) bit from the line
        v_rx_out_eop(byte) := '0';
        if rx_serial_in = '1' then
          v_rx_out_sop(byte) := '1';
        elsif rx_serial_in = 'U' then
          v_rx_out_eop(byte) := '1';
        end if;
        if byte < c_nof_bytes_per_data - 1 then
          wait for c_line_clk_period;  -- exit loop in last half line clock cycle
        end if;
      end loop;

      -- Realign to tr_clk rising edge
      wait until rising_edge(tr_clk);

      -- End of this deserialization cycle: the rx data word has been assembled.
      rx_out_data <= v_rx_out_data;
      rx_out_ctrl <= v_rx_out_ctrl;
      rx_out_sop  <= v_rx_out_sop;
      rx_out_eop  <= v_rx_out_eop;
    end loop;
  end process;
end beh;
