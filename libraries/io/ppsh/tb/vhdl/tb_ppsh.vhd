-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_ppsh is
end tb_ppsh;

architecture tb of tb_ppsh is
  constant c_clk_freq     : natural := 1000;  -- clock frequency in Hz
  constant c_clk_period   : time    := 1000000 us / c_clk_freq;
  constant c_pps_default_period   : natural := c_clk_freq;  -- 1 s takes c_clk_freq clk cycles
  constant c_pps_skew     : time    := 7 * c_clk_period / 10;
  constant c_cnt_w        : natural := ceil_log2(c_clk_freq);

  -- The state name tells what kind of test is being done
  type t_state_enum is (
    s_idle,
    s_default_period,
    s_shorter_period,
    s_longer_period,
    s_missing_pps,
    s_end
  );

  signal tb_end           : std_logic := '0';
  signal tb_state         : t_state_enum;
  signal rst              : std_logic := '1';
  signal clk              : std_logic := '1';
  signal pps              : std_logic;

  -- DUT
  signal pps_ext          : std_logic;
  signal pps_sys          : std_logic;
  signal pps_toggle       : std_logic;
  signal pps_stable       : std_logic;
  signal pps_stable_ack   : std_logic := '0';
  signal capture_edge     : std_logic;
  signal capture_cnt      : std_logic_vector(c_cnt_w - 1 downto 0);
  signal offset_cnt       : std_logic_vector(c_cnt_w - 1 downto 0);
  signal expected_cnt     : std_logic_vector(c_cnt_w - 1 downto 0);

  -- Verify
  signal verify_s         : real := 0.0;  -- provides time line marker for p_verify in Wave Window
begin
  -- Usage: 'run -all', observe unsigned capture_cnt, there should occur no
  --        REPORT errors.

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------
  rst <= '1', '0' after 7 * c_clk_period;
  clk <= not clk or tb_end after c_clk_period / 2;

  -- Verify that using the falling capture edge indeed does change timing by
  -- using a c_pps_skew that is > 0.5 c_clk_period and < c_clk_period
  p_capture_edge : process
  begin
    capture_edge <= '0';
    wait for 5000 ms;
    capture_edge <= '1';  -- will be verified by p_verify
    wait for 2000 ms;
    capture_edge <= '0';
    wait;
  end process;

  p_verify_pps_stable : process
  begin
    pps_stable_ack <= '0';
    wait for 9000 ms;  -- wait until p_capture_edge is done
    if pps_stable /= '0' then
      report "PPSH : Unexpected pps_stable, should be 0."
        severity ERROR;
    end if;
    -- ack PPS stable monitor
    pps_stable_ack <= '1';
    wait for 1 * c_clk_period;
    pps_stable_ack <= '0';
    wait for 10 ms;
    if pps_stable /= '1' then
      report "PPSH : Unexpected pps_stable, should be 1."
        severity ERROR;
    end if;
    wait for 13000 ms;  -- wait until first loop in p_pps_default_period is done
    if pps_stable /= '0' then
      report "PPSH : Unexpected pps_stable, should have become 0."
        severity ERROR;
    end if;
    wait;
  end process;

  -- Verify the capture_cnt
  p_pps_default_period : process
  begin
    tb_state <= s_idle;
    pps <= '0';
    expected_cnt <= TO_UVEC(c_pps_default_period, c_cnt_w);
    wait until rst = '0';
    wait for 10 * c_clk_period;
    wait until rising_edge(clk);  -- get synchronous to clk
    -- Correct PPS period
    tb_state <= s_default_period;
    for I in 1 to 20 loop
      pps <= '1';
      wait for I * c_clk_period;  -- the PPS pulse width is arbitrary, only the timing of the rising edge is relevant
      pps <= '0';
      wait for (c_pps_default_period - I) * c_clk_period;
    end loop;
    -- One too short PPS period
    tb_state <= s_shorter_period;
    pps <= '1';
    wait for (c_pps_default_period / 4) * c_clk_period;  -- the PPS pulse width is arbitrary, only the timing of the rising edge is relevant
    pps <= '0';
    wait for (c_pps_default_period - c_pps_default_period / 4 - 1) * c_clk_period;
    -- Some correct PPS periods
    tb_state <= s_default_period;
    for I in 1 to 5 loop
      pps <= '1';
      wait for I * c_clk_period;  -- the PPS pulse width is arbitrary, only the timing of the rising edge is relevant
      pps <= '0';
      wait for (c_pps_default_period - I) * c_clk_period;
    end loop;
    -- One too long PPS period
    tb_state <= s_longer_period;
    pps <= '1';
    wait for (c_pps_default_period / 4) * c_clk_period;  -- the PPS pulse width is arbitrary, only the timing of the rising edge is relevant
    pps <= '0';
    wait for (c_pps_default_period - c_pps_default_period / 4 + 1) * c_clk_period;
    -- Some correct PPS periods
    tb_state <= s_default_period;
    for I in 1 to 5 loop
      pps <= '1';
      wait for I * c_clk_period;  -- the PPS pulse width is arbitrary, only the timing of the rising edge is relevant
      pps <= '0';
      wait for (c_pps_default_period - I) * c_clk_period;
    end loop;
    -- Missing PPS pulses
    tb_state <= s_missing_pps;
    wait for (7 * c_pps_default_period) * c_clk_period;
    -- Some correct PPS periods
    tb_state <= s_default_period;
    for I in 1 to 5 loop
      pps <= '1';
      wait for I * c_clk_period;  -- the PPS pulse width is arbitrary, only the timing of the rising edge is relevant
      pps <= '0';
      wait for (c_pps_default_period - I) * c_clk_period;
    end loop;

    -- End
    tb_state <= s_end;
    wait for c_pps_default_period * c_clk_period;
    tb_end <= '1';
    wait;
  end process;

  -- Apply some PPS to CLK skew
  pps_ext <= transport pps after c_pps_skew;

  -----------------------------------------------------------------------------
  -- DUT: PPSH
  -----------------------------------------------------------------------------

  dut : entity work.ppsh
  generic map (
    g_clk_freq    => c_clk_freq
  )
  port map (
    rst           => rst,
    clk           => clk,
    -- PPS
    pps_ext       => pps_ext,
    pps_sys       => pps_sys,
    -- MM control
    pps_toggle     => pps_toggle,
    pps_stable     => pps_stable,
    pps_stable_ack => pps_stable_ack,
    capture_edge   => capture_edge,
    capture_cnt    => capture_cnt,
    offset_cnt     => offset_cnt,
    expected_cnt   => expected_cnt
  );

  -----------------------------------------------------------------------------
  -- Verify capture_cnt
  -----------------------------------------------------------------------------

  -- Simple verify scheme that matches the stimuli from p_pps_default_period
  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      -- Check all used stimuli values in general
      if unsigned(capture_cnt) /= c_clk_freq   and
         unsigned(capture_cnt) /= c_clk_freq + 1 and
         unsigned(capture_cnt) /= c_clk_freq - 1 and
         unsigned(capture_cnt) /= 0            and
         unsigned(capture_cnt) /= 2**capture_cnt'length - 1 then
        report "PPSH : Unexpected capture count value."
          severity ERROR;
      end if;

      -- Verify influence of PPS capture edge selection
      if (NOW > 6000 ms) and (NOW <= 6000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= c_clk_freq + 1 then
          report "PPSH : Unexpected capture count value at 6 s."
            severity ERROR;
        end if;
        verify_s <= 6.0;
      end if;

      if (NOW > 7000 ms) and (NOW <= 7000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= c_clk_freq then
          report "PPSH : Unexpected capture count value at 7 s."
            severity ERROR;
        end if;
        verify_s <= 7.0;
      end if;

      if (NOW > 8000 ms) and (NOW <= 8000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= c_clk_freq - 1 then
          report "PPSH : Unexpected capture count value at 8 s."
            severity ERROR;
        end if;
        verify_s <= 8.0;
      end if;

      -- Verify external PPS period fluctuations at specific stimuli moments
      if (NOW > 10000 ms) and (NOW <= 10000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= c_clk_freq then
          report "PPSH : Unexpected capture count value at 10 s."
            severity ERROR;
        end if;
        verify_s <= 10.0;
      end if;

      if (NOW > 22000 ms) and (NOW <= 22000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= c_clk_freq - 1 then
          report "PPSH : Unexpected capture count value at 22 s."
            severity ERROR;
        end if;
        verify_s <= 22.0;
      end if;

      if (NOW > 25000 ms) and (NOW <= 25000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= c_clk_freq then
          report "PPSH : Unexpected capture count value at 25 s."
            severity ERROR;
        end if;
        verify_s <= 25.0;
      end if;

      if (NOW > 28000 ms) and (NOW <= 28000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= c_clk_freq + 1 then
          report "PPSH : Unexpected capture count value at 28 s."
            severity ERROR;
        end if;
        verify_s <= 28.0;
      end if;

      if (NOW > 30000 ms) and (NOW <= 30000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= c_clk_freq then
          report "PPSH : Unexpected capture count value at 30 s."
            severity ERROR;
        end if;
        verify_s <= 30.0;
      end if;

      if (NOW > 35000 ms) and (NOW <= 35000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= 2**capture_cnt'length - 1 then
          report "PPSH : Unexpected capture count value at 35 s."
            severity ERROR;
        end if;
        verify_s <= 35.0;
      end if;

      if (NOW > 49000 ms) and (NOW <= 49000 ms + c_clk_period) then
        if unsigned(capture_cnt) /= 2**capture_cnt'length - 1 then
          report "PPSH : Unexpected capture count value at 49 s."
            severity ERROR;
        end if;
        verify_s <= 49.0;
      end if;

      -- check if offset_cnt is counting
      if (NOW > 7500 ms) and (NOW <= 7500 ms + c_clk_period) then
        if unsigned(offset_cnt) /= 475 then
          report "PPSH : Unexpected offset count value at 7.5 s."
            severity ERROR;
        end if;
        verify_s <= 7.5;
      end if;

      if (NOW > 7700 ms) and (NOW <= 7700 ms + c_clk_period) then
        if unsigned(offset_cnt) /= 675 then
          report "PPSH : Unexpected offset count value at 7.7 s."
            severity ERROR;
        end if;
        verify_s <= 7.7;
      end if;
    end if;
  end process;
end tb;
