################################################################################
#
# Copyright (C) 2011
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################
 
# >>> Generate Sinus Waveform
# . Usage at TCLSH prompt% source wg_waveform.tcl
# . Result diag_sin_2048x18.txt = tcl_sin_2048x18.txt = m_sin_2048x18.txt

global env

source       "$env(UNB)/Firmware/modules/Lofar/diag/src/tcl/wg.tcl"
set dir_name "$env(UNB)/Firmware/modules/Lofar/diag/src/data/"


set nof_samples  1024                              ;# nof samples in waveform
set sample_w     8                                 ;# nof bits per waveform sample
set amplitude    127                               ;# sinus top-top will be -amplitude to + amplitude
set offset       0                                 ;# DC offset
set phase        0                                 ;# initial phase
set nof_periods  1                                 ;# nof sinus periods in waveform, so freq is nof_periods/nof_samples * fs
set period       [expr $nof_samples/$nof_periods]
set waveform     [wg_adc [wg_calculate_analogue_waveform sinus $amplitude $offset $phase $period 0 $nof_samples] $sample_w]
set f            {}
append f         "$dir_name" "tcl_sin_1024x8.txt"
wg_write_file    $f $waveform
