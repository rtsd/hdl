-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Generate st_histogram input data, verify RAM contents. TB is self checking.
-- ModelSim usage:
-- . (open project, compile)
-- . (load simulation config)
-- . as 8
-- . run -a
-- . For signals 'stimuli_data' and 'histogram' select Format->Analog(automatic)
--   . set Radix to 'decimal' for signed input data.
-- Description:
--  . tb_st_histogram generates and visualizes an input sine wave and the
--    resulting histogram in the wave window.
-- . Verification be eye (wave window):
--   . For the sine input (default), observe input 'stimuli_data' and output
--     'histogram' signals. Set Radix->Decimal and Format->Analog (automatic)
--     in QuestaSim.
--   . For counter data, observe that:
--     . There are 4 sync periods in which 3 packets of 1024 words are generated;
--     . histogram_snk_in.data = 0..1023, 3 times per sync
--       . st_histogram has 256 bins so uses the 8 MS bits of snk_in.data
--       . st_histogram will count 4*0..255 instead of 0..1023 per packet
--       . st_histogram will count 12 occurences (3 packets * 4 * 0..255) per sync.
--     . bin_writer_mosi writes bin counts 1..12 per sync interval;
--     . Both RAMs are used twice: RAM 0, RAM 1, RAM 0, RAM 1;
--     . RAM clearing completes just before the end of each sync interval.
-- . Automatic verification - in each sync period:
--   . the RAM contents are read out via ram_mosi/miso and compared to the
--     expected bin counts. This is done only for g_stimuli_mode = counter
--     and dc because that is sufficient and easily done automatically.
--     . The counter mode yields the same value in all bins
--     . DC mode yields max value in one bin and zero in other bins.
--   . the sum of all bins is checked against the expected g_nof_data_per_sync.
--     . this is done for all modes 'counter', 'dc', 'random' and 'sine'.
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_st_histogram is
  generic(
    g_nof_sync          : natural := 4;  -- We're simulating at least 4 g_nof_sync so both RAMs are written and cleared twice.
    g_data_w            : natural := 3;  -- Determines maximum number of bins (2^g_data_w)
    g_nof_bins          : natural := 8;  -- Lower than or equal to 2^g_data_w. Higher is allowed but makes no sense.
    g_nof_data_per_sync : natural := 20;  -- >= g_nof_bins. Determines max required RAM data width. e.g. 11b to store max bin count '1024'.
    g_stimuli_mode      : string  := "sine";  -- "counter", "dc", "sine" or "random"
    g_data_type         : string  := "signed";  -- use "signed" if g_stimuli_mode="sine"
    g_lock_sine         : boolean := true  -- TRUE to lock the sine wave to Sync - produces sparse histogram with low number of non-zero samples (occuring 2*c_sine_nof_periods)
    );  -- FALSE produces a dense histogram as the drifting sine wave hits more levels.
end tb_st_histogram;

architecture tb of tb_st_histogram is
  ---------------------------------------------------------------------------
  -- Constants derived from generics
  ---------------------------------------------------------------------------
  constant c_expected_ram_content_counter : natural := g_nof_data_per_sync / g_nof_bins;
  constant c_nof_levels_per_bin           : natural := (2**g_data_w) / g_nof_bins;  -- e.g. 2 values per bin if g_data_w=9 (512 levels) and g_nof_bins=256
  constant c_ram_dat_w                    : natural := ceil_log2(g_nof_data_per_sync + 1);
  constant c_ram_adr_w                    : natural := ceil_log2(g_nof_bins);

  ---------------------------------------------------------------------------
  -- Clocks and resets
  ---------------------------------------------------------------------------
  constant c_dp_clk_period : time := 5 ns;

  signal dp_rst            : std_logic;
  signal dp_clk            : std_logic := '1';

  signal tb_end            : std_logic := '0';

  ----------------------------------------------------------------------------
  -- stimuli
  ----------------------------------------------------------------------------
  constant c_sine_amplitude              : real := real((2**g_data_w) / 2 - 1);
  constant c_sine_nof_periods            : real := 1.0;
  constant c_sine_nof_samples_per_period : real := real(g_nof_data_per_sync) / c_sine_nof_periods;
  constant c_sine_time_step_denom        : real := sel_a_b(g_lock_sine, MATH_2_PI, 5.0);  -- Use 5 instead of 2 pi to create unlocked, drifting sine wave
  constant c_sine_time_step              : real := c_sine_time_step_denom / c_sine_nof_samples_per_period;

  signal stimuli_en          : std_logic := '1';
  signal stimuli_src_out     : t_dp_sosi;
  signal nxt_stimuli_src_out : t_dp_sosi;
  signal stimuli_src_in      : t_dp_siso;
  signal stimuli_count       : real;
  signal stimuli_data        : std_logic_vector(g_data_w - 1 downto 0);  -- QuestaSim: Format->Analog, Radix->Decimal
  signal random_data         : std_logic_vector(g_data_w - 1 downto 0);
  signal stimuli_done        : std_logic;

  ----------------------------------------------------------------------------
  -- st_histogram
  ----------------------------------------------------------------------------
  signal st_histogram_snk_in       : t_dp_sosi;
  signal st_histogram_ram_mosi     : t_mem_mosi;
  signal prv_st_histogram_ram_mosi : t_mem_mosi;
  signal st_histogram_ram_miso     : t_mem_miso;

  ----------------------------------------------------------------------------
  -- Automatic verification of RAM readout
  ----------------------------------------------------------------------------
  -- Use these 4 signals to verify histogram by eye in the wave window
  signal histogram_data         : natural;  -- QuestaSim: Format->Analog
  signal histogram_bin_unsigned : natural;
  signal histogram_bin_signed   : integer;  -- QuestaSim: Radix->Decimal
  signal histogram_valid        : std_logic;

  signal sum_of_bins            : natural;
  signal verification_done      : std_logic;
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 10;

  ----------------------------------------------------------------------------
  -- Stimuli: generate st_histogram input data
  ----------------------------------------------------------------------------
  stimuli_src_in <= c_dp_siso_rdy;

  -- Generate g_nof_sync packets of g_nof_data_per_sync words
  p_generate_packets : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    nxt_stimuli_src_out <= c_dp_sosi_rst;
    stimuli_done <= '0';
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(dp_clk, 5);

    -- Generate a block of counter data every sync
    if g_stimuli_mode = "counter" then
      for I in 0 to g_nof_sync - 1 loop
        v_sosi.sync    := '1';
        v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_data_w - 1 downto 0));  -- wrap when >= 2**g_data_w
        proc_dp_gen_block_data(g_data_w, TO_UINT(v_sosi.data), g_nof_data_per_sync, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn, dp_clk, stimuli_en, stimuli_src_in, nxt_stimuli_src_out);
      end loop;
    end if;

    -- Generate a DC level that increments every sync
    if g_stimuli_mode = "dc" then
      nxt_stimuli_src_out.valid <= '1';
      for I in 0 to g_nof_sync - 1 loop
        nxt_stimuli_src_out.data <= INCR_UVEC(stimuli_src_out.data, 1);  -- all g_nof_data_per_sync cycles
        nxt_stimuli_src_out.sync <= '1';  -- cycle 0
        wait for 5 ns;
        for j in 1 to g_nof_data_per_sync - 1 loop  -- cycles 1..g_nof_data_per_sync-1
          nxt_stimuli_src_out.sync <= '0';
          wait for 5 ns;
        end loop;
      end loop;
    end if;

    -- Generate a sine wave
    if g_stimuli_mode = "sine" then
      nxt_stimuli_src_out <= stimuli_src_out;
      nxt_stimuli_src_out.valid <= '1';
      stimuli_count <= 0.0;
      for I in 0 to g_nof_sync - 1 loop
        nxt_stimuli_src_out.sync <= '1';  -- cycle 0
        wait for 5 ns;
        for j in 1 to g_nof_data_per_sync - 1 loop  -- cycles 1..g_nof_data_per_sync-1
          nxt_stimuli_src_out.sync <= '0';
          nxt_stimuli_src_out.data(g_data_w - 1 downto 0) <= TO_SVEC( integer(round( c_sine_amplitude * sin(stimuli_count) )), g_data_w);
          stimuli_count <= stimuli_count + c_sine_time_step;
          wait for 5 ns;
        end loop;
      end loop;
    end if;

    -- Generate pseudo random noise
    if g_stimuli_mode = "random" then
      nxt_stimuli_src_out.valid <= '1';
      for I in 0 to g_nof_sync - 1 loop
        random_data <= (others => '0');
        nxt_stimuli_src_out.sync <= '1';  -- cycle 0
        wait for 5 ns;
        for j in 1 to g_nof_data_per_sync - 1 loop
          nxt_stimuli_src_out.sync <= '0';
          random_data <=  func_common_random(random_data);
          nxt_stimuli_src_out.data(g_data_w - 1 downto 0) <= random_data;  -- all g_nof_data_per_sync cycles
          wait for 5 ns;
        end loop;
      end loop;
    end if;

    stimuli_done <= '1';
    proc_common_wait_some_cycles(dp_clk, 50);
    tb_end <= '1';
    wait;
  end process;

  p_stimuli_src_out: process(dp_rst, dp_clk) is
  begin
    if dp_rst = '1' then
      stimuli_src_out <= c_dp_sosi_rst;
    elsif rising_edge(dp_clk) then
      stimuli_src_out <= nxt_stimuli_src_out;
    end if;
  end process;

  -- signal to verify histogram by eye in the wave window
  stimuli_data <= stimuli_src_out.data(g_data_w - 1 downto 0);

  ----------------------------------------------------------------------------
  -- st_histogram
  ----------------------------------------------------------------------------
  st_histogram_snk_in <= stimuli_src_out;

  u_st_histogram : entity work.st_histogram
  generic map(
    g_data_w            => g_data_w,
    g_nof_bins          => g_nof_bins,
    g_nof_data_per_sync => g_nof_data_per_sync,
    g_data_type         => g_data_type
  )
  port map (
    dp_clk       => dp_clk,
    dp_rst       => dp_rst,

    snk_in       => st_histogram_snk_in,

    ram_mosi     => st_histogram_ram_mosi,
    ram_miso     => st_histogram_ram_miso
  );

  ----------------------------------------------------------------------------
  -- Readout and verification of RAM contents
  -- . The table below shows what RAM we are reading here ('RAM read') via the
  --   ram_mosi/miso interface, and what the expected RAM contents are.
  --
  -- Counter data (the same every sync excl. sync 0):
  ---+-------------+-------------+----------+-----------------------+
  -- | Sync period | RAM written | RAM read | RAM contents          |
  -- +-------------+-------------+----------+-----------------------+
  -- | 0           | 0           | 1        | 256 addresses *  0    |
  -- | 1           | 1           | 0        | 256 addresses * 12    |
  -- | 2           | 0           | 1        | 256 addresses * 12    |
  -- | 3           | 1           | 0        | 256 addresses * 12    |
  -- +-------------+-------------+----------+-----------------------+
  --
  -- DC data (increments level every sync: 0, 1, 2, 3, ..):
  ---+-------------+-------------+----------+-----------------------+
  -- | Sync period | RAM written | RAM read | RAM contents          |
  -- +-------------+-------------+----------+-----------------------+
  -- | 0           | 0           | 1        | 256 addresses *  0    |
  -- | 1           | 1           | 0        | Addr 1: 1024, others 0|
  -- | 2           | 0           | 1        | Addr 2: 1024, others 0|
  -- | 3           | 1           | 0        | Addr 3: 1024, others 0|
  -- +-------------+-------------+----------+-----------------------+
  ----------------------------------------------------------------------------
  -- Perform MM read and put result in ram_rd_word
  p_verify_mm_read : process
  begin
    st_histogram_ram_mosi.wr <= '0';
    for i in 0 to g_nof_sync - 1 loop
      proc_common_wait_until_high(dp_clk, stimuli_src_out.sync);
      proc_common_wait_some_cycles(dp_clk, 10);
      for j in 0 to g_nof_bins - 1 loop
        proc_mem_mm_bus_rd(j, dp_clk, st_histogram_ram_mosi);
      end loop;
    end loop;
    wait;
  end process;

  -- Help signals that contain the histogram bins+data
  histogram_bin_unsigned <= TO_UINT(              prv_st_histogram_ram_mosi.address(c_ram_adr_w - 1 downto 0));
  histogram_bin_signed   <= TO_SINT(offset_binary(prv_st_histogram_ram_mosi.address(c_ram_adr_w - 1 downto 0)));
  histogram_data         <= TO_UINT(st_histogram_ram_miso.rddata(c_ram_dat_w - 1 downto 0)) when st_histogram_ram_miso.rdval = '1'ELSE 0;
  histogram_valid        <= st_histogram_ram_miso.rdval;

  -- Perform verification of ram_rd_word when ram_rd_word_valid
  p_verify_assert : process
  begin
    verification_done <= '0';
    for i in 0 to g_nof_sync - 1 loop
      sum_of_bins <= 0;
      proc_common_wait_until_high(dp_clk, stimuli_src_out.sync);
      for j in 0 to g_nof_bins - 1 loop
        proc_common_wait_until_high(dp_clk, histogram_valid);
        if i = 0 then  -- Sync period 0: we expect RAM to contain zeros
          assert histogram_data = 0
            report "RAM contains wrong bin count (expected 0, actual " & integer'image(histogram_data) & ")"
            severity ERROR;
        else  -- Sync period 1 onwards
          if g_stimuli_mode = "counter" then
            -- Counter data: bin values remain the same every sync
            assert histogram_data = c_expected_ram_content_counter
              report "RAM contains wrong bin count (expected " & integer'image(c_expected_ram_content_counter) & ", actual " & integer'image(histogram_data) & ")"
              severity ERROR;
          elsif g_stimuli_mode = "dc" then
            -- DC data: DC level increments every sync
            if j = (i / c_nof_levels_per_bin) then  -- Check bin address and account for multiple levels per bin
              -- this address (j) should contain the DC level total count of this sync period (i)
              assert histogram_data = g_nof_data_per_sync
                report "RAM contains wrong bin count (expected " & integer'image(g_nof_data_per_sync) & ", actual " & integer'image(histogram_data) & ")"
                severity ERROR;
            else
              -- this address should contain zero
              assert histogram_data = 0
                report "RAM contains wrong bin count (expected 0, actual " & integer'image(histogram_data) & ")"
                severity ERROR;
            end if;
          end if;
        end if;
        sum_of_bins <= sum_of_bins + histogram_data;  -- Keep the sum of all bins
        wait for 5 ns;
      end loop;

      -- Check the sum of all bins
      if i > 0 then  -- Skip sync 0 (histogram still all zeros)
        assert sum_of_bins = g_nof_data_per_sync
          report "Sum of bins not equal to g_nof_data_per_sync (expected " & integer'image(g_nof_data_per_sync) & ", actual " & integer'image(sum_of_bins) & ")"
          severity ERROR;
      end if;
    end loop;
    verification_done <= '1';  -- We have blocking proc_common_wait_until_high procedures above so we need to know if we make it here.
    wait;
  end process;

  -- Check if verification was done at all
  p_check_verification_done : process
  begin
    proc_common_wait_until_high(dp_clk, stimuli_done);
    proc_common_wait_some_cycles(dp_clk, 50);
    assert verification_done = '1'
      report "Verification failed"
      severity ERROR;
    wait;
  end process;

  -- Register MOSI to store the read address
  p_clk: process(dp_rst, dp_clk) is
  begin
    if dp_rst = '1' then
      prv_st_histogram_ram_mosi <= c_mem_mosi_rst;
    elsif rising_edge(dp_clk) then
      prv_st_histogram_ram_mosi <= st_histogram_ram_mosi;
    end if;
  end process;
end tb;
