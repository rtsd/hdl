-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity common_complex_add_sub is
  generic (
    g_direction       : string  := "ADD";  -- or "SUB"
    g_representation  : string  := "SIGNED";  -- or "UNSIGNED"
    g_pipeline_input  : natural := 0;  -- 0 or 1
    g_pipeline_output : natural := 1;  -- >= 0
    g_in_dat_w        : natural := 8;
    g_out_dat_w       : natural := 9  -- only support g_out_dat_w=g_in_dat_w and g_out_dat_w=g_in_dat_w+1
  );
  port (
    clk      : in  std_logic;
    clken    : in  std_logic := '1';
    in_ar    : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_ai    : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_br    : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_bi    : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    out_re   : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_im   : out std_logic_vector(g_out_dat_w - 1 downto 0)
  );
end common_complex_add_sub;

architecture str of common_complex_add_sub is
begin
  add_re : entity work.common_add_sub
  generic map (
    g_direction       => g_direction,
    g_representation  => g_representation,
    g_pipeline_input  => g_pipeline_input,
    g_pipeline_output => g_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_a    => in_ar,
    in_b    => in_br,
    result  => out_re
  );

  add_im : entity work.common_add_sub
  generic map (
    g_direction       => g_direction,
    g_representation  => g_representation,
    g_pipeline_input  => g_pipeline_input,
    g_pipeline_output => g_pipeline_output,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_a    => in_ai,
    in_b    => in_bi,
    result  => out_im
  );
end str;
