-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;
use work.tb_common_pkg.all;

-- Purpose: Test bench to check reinterleave function visually
-- Usage:
--   > do wave_reinterleave.do
--   > run 400ns
--   > observe how the DUT inputs are reinterleaved onto the DUT outputs.
-- Remark: This TB is meant to be easy to the eyes. For stress-testing, use the tb_tb.

entity tb_common_reinterleave is
  generic (
    g_dat_w          : natural := 16;  -- Data width including concatenated stream ID byte (if used)
    g_nof_in         : natural := 2;  -- Max 6 if stream ID is used
    g_deint_block_size  : natural := 2;
    g_nof_out        : natural := 2;
    g_inter_block_size : natural := 2;
    g_concat_id      : boolean := true;  -- Concatenate a 1 byte stream ID 0xA..F @ MSB so user can follow streams in wave window
    g_cnt_sync       : boolean := true  -- When TRUE all generated streams start at 0, else they're offset by 16 counter values.
 );
end;

architecture rtl of tb_common_reinterleave is
  type t_dat_arr is array (integer range <>) of std_logic_vector(g_dat_w - 1 downto 0);
  type t_val_arr is array (integer range <>) of std_logic;

  -----------------------------------------------------------------------------
  -- Standard TB clocking, RST and control
  -----------------------------------------------------------------------------
  constant c_clk_period : time := 10 ns;

  signal clk            : std_logic := '1';
  signal rst            : std_logic;
  signal tb_end         : std_logic := '0';

  -----------------------------------------------------------------------------
  -- Override g_dat_w if user want to concatenate stream IDs
  -----------------------------------------------------------------------------
  constant c_id_w       : natural := c_nibble_w;  -- HEX 0xA..0xF

  constant c_cnt_dat_w  : natural := sel_a_b(g_concat_id, g_dat_w - c_id_w, g_dat_w);

  constant c_nof_id     : natural := 6;  -- HEX 0xA..0xF
  constant c_id_arr     : t_slv_4_arr(c_nof_id - 1 downto 0) := (x"F", x"E", x"D", x"C", x"B", x"A");

  signal cnt_ena        : std_logic;
  signal cnt_rdy        : std_logic := '1';

  -----------------------------------------------------------------------------
  -- Array of DUT input buses
  -----------------------------------------------------------------------------
  signal dut_in_dat_arr : t_dat_arr(g_nof_in - 1 downto 0);
  signal dut_in_val_arr : t_val_arr(g_nof_in - 1 downto 0);

  -- DUT input array flattened into SLV
  signal dut_in_dat     : std_logic_vector(g_nof_in * g_dat_w - 1 downto 0);

  -----------------------------------------------------------------------------
  -- DUT output array
  -----------------------------------------------------------------------------
  signal dut_out_dat_arr : t_dat_arr(g_nof_out - 1 downto 0);

  -- DUT output Array flattened into SLV (= REVERSE FUNCTION input)
  signal dut_out_dat     : std_logic_vector(g_nof_out * g_dat_w - 1 downto 0);
  signal dut_out_val     : std_logic_vector(g_nof_out - 1 downto 0);

  -----------------------------------------------------------------------------
  -- REVERSE FUNCTION output
  -----------------------------------------------------------------------------
  signal rev_out_dat     : std_logic_vector(g_nof_in * g_dat_w - 1 downto 0);
  signal rev_out_val     : std_logic_vector(g_nof_in - 1 downto 0);

  -- REVERSE FUNCTION output array
  signal rev_out_dat_arr : t_dat_arr(g_nof_in - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Standard TB clocking, RST and control
  -----------------------------------------------------------------------------
  clk    <= not clk or tb_end after c_clk_period / 2;
  rst    <= '1', '0' after 3 * c_clk_period;

  -----------------------------------------------------------------------------
  -- Generate g_nof_in test data streams with counter data plus optional ID
  -----------------------------------------------------------------------------
  cnt_ena <= '0', '1' after 20 * c_clk_period;

  gen_cnt_dat : for i in 0 to g_nof_in - 1 generate
    -- Generate counter data. Let the generated stream start at 0 unless user wants
    -- the streams to be asynchronous (then separate them by an offset of 10).
    proc_common_gen_data(1, sel_a_b(g_cnt_sync, 0, i * 16), rst, clk, cnt_ena, cnt_rdy, dut_in_dat_arr(i)(c_cnt_dat_w - 1 downto 0), dut_in_val_arr(i));
    -- Concatenate ID if desired
    gen_id_data : if g_concat_id = true generate
      dut_in_dat_arr(i)(g_dat_w - 1 downto c_cnt_dat_w) <= c_id_arr(i);
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- The I/O of common_reinterleave and its lower level components operate
  -- on the same data width share the same clock, so if g_nof_in>g_nof_out,
  -- lower the effective input data rate accordingly by introducing gaps.
  -----------------------------------------------------------------------------
  gen_dut_in_dat_gaps : if g_nof_in > g_nof_out generate
    p_cnt_dat_gaps : process
    begin
      cnt_rdy <= '0';
      wait for c_clk_period * (ceil_div(g_nof_in, g_nof_out) - 1);
      cnt_rdy <= '1';
      wait for c_clk_period;
    end process;
  end generate;

  -----------------------------------------------------------------------------
  -- Map the array of counter data streams to SLV
  -----------------------------------------------------------------------------
  gen_wires_in : for i in 0 to g_nof_in - 1 generate
    dut_in_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w) <= dut_in_dat_arr(i);
  end generate;

  -----------------------------------------------------------------------------
  -- DUT
  -----------------------------------------------------------------------------
  u_reinterleave : entity work.common_reinterleave
  generic map (
    g_nof_in         => g_nof_in,
    g_deint_block_size  => g_deint_block_size,
    g_nof_out        => g_nof_out,
    g_inter_block_size => g_inter_block_size,
    g_dat_w          => g_dat_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_dat     => dut_in_dat,
    in_val     => dut_in_val_arr(0),  -- All input streams should be synchronous in terms of timing

    out_dat    => dut_out_dat,
    out_val    => dut_out_val
  );

  -----------------------------------------------------------------------------
  -- Map DUT output SLV to array of streams (to ease viewing in wave window)
  -----------------------------------------------------------------------------
  gen_dut_out : for i in 0 to g_nof_out - 1 generate
    dut_out_dat_arr(i) <= dut_out_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w);
  end generate;

  -----------------------------------------------------------------------------
  -- REVERSE FUNCTION; the outputs should match the DUT inputs (with delay)
  -----------------------------------------------------------------------------
  u_rev_reinterleave : entity work.common_reinterleave
  generic map (
    g_nof_in         => g_nof_out,  -- Note the reversed generics
    g_deint_block_size  => g_inter_block_size,
    g_nof_out        => g_nof_in,
    g_inter_block_size => g_deint_block_size,
    g_dat_w          => g_dat_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_dat     => dut_out_dat,
    in_val     => dut_out_val(0),

    out_dat    => rev_out_dat,
    out_val    => rev_out_val
  );

  -----------------------------------------------------------------------------
  -- Map REV output SLV to array of streams
  -----------------------------------------------------------------------------
  gen_rev_out : for i in 0 to g_nof_out - 1 generate
    rev_out_dat_arr(i) <= rev_out_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w);
  end generate;
end rtl;
