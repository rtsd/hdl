-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author : R. vd Walle
-- Purpose:
-- Calculate Crosslets Statistics
-- Description:
-- Consists of st_xsq_mm_to_dp connected to st_xsq_arr. in_b_arr comes directly
-- from st_xsq_mm_to_dp but all streams in in_a_arr are x_sosi_arr(0) with corrected
-- indices done by a process.
-- Remarks:
--  . More detail, see:
--    https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+Correlator
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity st_xst is
  generic (
    g_nof_streams       : natural := 1;
    g_nof_crosslets     : natural := 1;
    g_nof_signal_inputs : natural := 2;
    g_in_data_w         : natural := 18;  -- width of the data to be accumulated
    g_stat_data_w       : natural := 54;  -- statistics accumulator width
    g_stat_data_sz      : natural := 2  -- statistics word width >= statistics accumulator width and fit in a power of 2 multiple 32b MM words
  );
  port (
    mm_rst : in  std_logic;
    mm_clk : in  std_logic;
    dp_rst : in  std_logic;
    dp_clk : in  std_logic;

    -- Streaming
    in_sosi : in  t_dp_sosi;

    -- DP Memory Mapped
    mm_mosi     : out t_mem_mosi;
    mm_miso_arr : in  t_mem_miso_arr(g_nof_streams - 1 downto 0);

    -- MM Memory Mapped
    ram_st_xsq_mosi : in  t_mem_mosi;
    ram_st_xsq_miso : out t_mem_miso
  );
end st_xst;

architecture str of st_xst is
  type t_reg is record
    busy            : std_logic;
    in_a_index      : natural;
    in_b_index      : natural;
    x_sosi_0_re     : t_slv_64_arr(g_nof_signal_inputs - 1 downto 0);
    x_sosi_0_im     : t_slv_64_arr(g_nof_signal_inputs - 1 downto 0);
    in_a_sosi_arr   : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  end record;

  constant c_reg_rst : t_reg := ('0', 0, 0, (others => (others => '0')), (others => (others => '0')), (others => c_dp_sosi_rst) );

  signal r     : t_reg;
  signal nxt_r : t_reg;

  signal in_a_sosi_arr :  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal in_b_sosi_arr :  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal x_sosi_arr :  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
begin
  -- MM -> DP
  st_xsq_mm_to_dp : entity work.st_xsq_mm_to_dp
  generic map(
    g_nof_streams       => g_nof_streams,
    g_nof_crosslets     => g_nof_crosslets,
    g_nof_signal_inputs => g_nof_signal_inputs,
    g_dsp_data_w        => g_in_data_w
  )
  port map(
    rst          => dp_rst,
    clk          => dp_clk,
    in_sosi      => in_sosi,
    mm_mosi      => mm_mosi,
    mm_miso_arr  => mm_miso_arr,
    out_sosi_arr => x_sosi_arr
  );

  -- in_b_sosi_arr = x_sosi_arr
  in_b_sosi_arr <= x_sosi_arr;
  in_a_sosi_arr <= nxt_r.in_a_sosi_arr;

  -- Register process
  p_reg : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(dp_clk) then
      r <= nxt_r;
    end if;
  end process;

  -- Combinatorial process to create in_a_index and in_b_index and reoredering x_sosi_arr(0) data.
  p_comb : process(r, x_sosi_arr)
    variable v : t_reg;
    variable v_in_a_index      : natural;
    variable v_in_b_index      : natural;
    variable v_x_sosi_0_re : t_slv_64_arr(g_nof_signal_inputs - 1 downto 0);
    variable v_x_sosi_0_im : t_slv_64_arr(g_nof_signal_inputs - 1 downto 0);
  begin
    v := r;
    v_in_a_index := r.in_a_index;
    v_in_b_index := r.in_b_index;
    v_x_sosi_0_re := r.x_sosi_0_re;
    v_x_sosi_0_im := r.x_sosi_0_im;
    -- initiate next block
    if r.busy = '0' and x_sosi_arr(0).sop = '1' then
      v.busy := '1';
    -- Continue block
    elsif r.busy = '1' then
      -- Indices counters to select data order
      if r.in_b_index < g_nof_signal_inputs - 1 then
        v_in_b_index := r.in_b_index + 1;
      else
        v_in_b_index := 0;
        if r.in_a_index < g_nof_signal_inputs - 1 then
          v_in_a_index := r.in_a_index + 1;
        else
          v_in_a_index := 0;
        end if;
      end if;
    end if;
    -- End of block
    if x_sosi_arr(0).eop = '1' then
      v.busy := '0';
      v_in_a_index := 0;
      v_in_b_index := 0;
    end if;

    -- Capture x_sosi_arr(0) data
    v_x_sosi_0_re(v_in_b_index) := x_sosi_arr(0).re;
    v_x_sosi_0_im(v_in_b_index) := x_sosi_arr(0).im;

    -- reorder x_sosi_arr(0) data to follow in_a_index instead of in_b_index. All sosi in in_a_sosi_arr are identical.
    for I in 0 to g_nof_streams - 1 loop
      v.in_a_sosi_arr(I) := x_sosi_arr(0);
      v.in_a_sosi_arr(I).re := v_x_sosi_0_re(v_in_a_index);
      v.in_a_sosi_arr(I).im := v_x_sosi_0_im(v_in_a_index);
    end loop;

    v.in_a_index := v_in_a_index;
    v.in_b_index := v_in_b_index;
    v.x_sosi_0_re := v_x_sosi_0_re;
    v.x_sosi_0_im := v_x_sosi_0_im;

    nxt_r <= v;
  end process;

  -- st_xsq instances
  st_xsq_arr : entity work.st_xsq_arr
  generic map (
    g_nof_streams       => g_nof_streams,
    g_nof_crosslets     => g_nof_crosslets,
    g_nof_signal_inputs => g_nof_signal_inputs,
    g_in_data_w         => g_in_data_w,
    g_stat_data_w       => g_stat_data_w,
    g_stat_data_sz      => g_stat_data_sz
  )
  port map (

    mm_rst => mm_rst,
    mm_clk => mm_clk,
    dp_rst => dp_rst,
    dp_clk => dp_clk,

    -- Streaming
    in_a_arr => in_a_sosi_arr,
    in_b_arr => in_b_sosi_arr,

    -- Memory Mapped
    ram_st_xsq_mosi => ram_st_xsq_mosi,
    ram_st_xsq_miso => ram_st_xsq_miso
  );
end str;
