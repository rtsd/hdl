#!/usr/bin/env python
###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Calculates the integrates Stokes parameters for comparison with the test 
   sequences in tb_iquv.vhd                                                 
   
   Usage:
   Edit the source code to set nint=8 or 16
   > ./iquv_checker.py
   
"""



nint = 8                               # number of samples to integrate  (typ. 8 or 16)
nints_per_block = 4
npoints = nint * nints_per_block       # number of samples in a block (typ. 32 or 64)
xpol = []
ypol = []



# real part of X pol counts up
for index in range(npoints):
    xpol.append(index + 1j * 0)
    ypol.append(0 + 1j * 0)
 
# real part of Y pol counts up
for index in range(npoints):
    xpol.append(0 + 1j * 0)
    ypol.append(index + 1j * 0)
 
# imag part of X pol counts up
for index in range(npoints):
    xpol.append(0 + 1j * index)
    ypol.append(0 + 1j * 0)
 
# imag part of Y pol counts up
for index in range(npoints):
    xpol.append(0 + 1j * 0)
    ypol.append(0 + 1j * index)
 
# real part of X and Y pols count up
for index in range(npoints):
    xpol.append(index + 1j * 0)
    ypol.append(index + 1j * 0)
 
# imag part of X and Y pols count up
for index in range(npoints):
    xpol.append(0 + 1j * index)
    ypol.append(0 + 1j * index)
 
# real and imag parts of X and Y pols count up
for index in range(npoints):
    xpol.append(index + 1j * index)
    ypol.append(index + 1j * index)
 
# real imag parts of X and Y pols have fixed dissimliar values
for index in range(npoints):
    xpol.append(-8 + 1j * 20)
    ypol.append(100 + 1j * -12)
 
# full scale 12 bit inputs
# assume for now that -2048 never occurs
for index in range(npoints):
    xpol.append(2047 + 1j * 2047)
    ypol.append(-2047 + 1j * -2047)
 

# calculate the outputs
for blockno in range(9):
    print ('Block cycle ', blockno)
    for ii in range(nints_per_block):
        accumI = 0
        accumQ = 0
        accumU = 0
        accumV = 0
        for jj in range(nint):
            index = blockno*npoints + ii*nint + jj
            Ival = xpol[index] * xpol[index].conjugate() + ypol[index] * ypol[index].conjugate()
            Qval = xpol[index] * xpol[index].conjugate() - ypol[index] * ypol[index].conjugate()
            Uval = xpol[index] * ypol[index].conjugate() + ypol[index] * xpol[index].conjugate()
            Vval = ypol[index] * xpol[index].conjugate() - xpol[index] * ypol[index].conjugate()
            accumI = accumI + Ival
            accumQ = accumQ + Qval
            accumU = accumU + Uval
            accumV = accumV + Vval
        print ( 'Integrated results Iint = ',accumI,', Qint = ',accumQ,', Uint = ',accumU,', Vint = ',accumV)




