library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_mem_pkg.all;

architecture str of pfs_coefsbuf is
  constant c_coefs_rom   : t_c_mem := (latency  => 2,
                                       adr_w    => g_addr_w,
                                       dat_w    => g_data_w,
                                       nof_dat  => g_nof_coefs,  -- <= 2**g_addr_w
                                       init_sl  => '0');
begin
  rom : entity common_lib.common_rom
  generic map (
    g_ram             => c_coefs_rom,
    g_init_file       => "data/pfs_coefsbuf_1024.hex"  -- Quartus .hex extension, replaced by .bin in common_rom works for XST
    --g_init_file       => "data/pfs_coefsbuf_1024.bin"  -- Synplify fails on file extension change to .bin in common_rom and requires extra ../
  )
  port map (
    rst               => rst,
    clk               => clk,
    rd_adr            => addr,
    rd_dat            => data
  );
end str;
