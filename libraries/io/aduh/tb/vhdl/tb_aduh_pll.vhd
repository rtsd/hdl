-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.aduh_pll_pkg.all;

entity tb_aduh_pll is
end tb_aduh_pll;

architecture tb of tb_aduh_pll is
  -- Conclusion from the c_model_rx_clk_* investigations:
  -- . It appears that it is not possible to avoid the DCLK->rx_clk divide by 2
  --   phase uncertainty by releasing the CDA reset in the dp-clk domain.
  constant c_model_rx_clk_phase_uncertainty_in_time  : boolean := true;  -- When TRUE model DPA lock at different relative SCLK phase in time (0..dp_deser_factor-1)
  constant c_model_rx_clk_phase_uncertainty_in_space : boolean := false;  -- When TRUE model DPA lock at different relative SCLK phase for each of the rx_clk (0..nof_clocks-1)
  constant c_model_rx_clk_hold_reference             : boolean := true;  -- When TRUE AND nof_clocks>1 then keep rx_clk(0) active so that rx_clk(0) is a reference

  -- Conclusion from the c_model_lvds_skew investigations:
  -- . It appears that the DPA model in altera_mf.vhd does not model DPA for
  --   semi static skew, e.g. skew that varies with temperature. It can only
  --   model ppm drift which occurs in soft CDR (clock data recovery) mode,
  --   such as needed e.g. for SGMII. In the Chip Planner the SERDES_RX that
  --   implements the DPA is connected but presented as a black box.
  constant c_model_lvds_skew     : boolean := false;  -- When TRUE model DPA lock at different relative SCLK phase in time (0..dp_deser_factor-1)

  -- Conclusion from the c_model_dclk_phase investigations:
  -- . Use
  --     c_model_rx_clk_phase_uncertainty_in_time  = TRUE;
  --     c_model_rx_clk_phase_uncertainty_in_space = FALSE;
  --     c_model_rx_clk_hold_reference             = TRUE;
  -- . To model the use_lvds_clk_rst it is necessary to deliberately start the
  --   the DCLK_CD with a different phase then the DCLK_AB and then check that
  --   a DCLK_RST_CD causes it to align again. This modelling is a bit fussy,
  --   because using lvds_clk_rst in lvdsh.vhd also makes that the dp_pll_reset
  --   gets synchronized to the dp_clk domain and that causes the
  --   c_model_rx_clk_phase_uncertainty_in_time effect to become void anyway.
  --   Anyway it is safe to state that using the ADC DCLK_RST is a proper
  --   means to align the lvds_clk from all ADU in a system to the dp_clk.
  constant c_model_dclk_phase    : boolean := false;  -- When TRUE start DCLK_CD with different phase then DCLK_AB, else use same phase for both

  -- TYPE t_c_aduh_pll_ai IS RECORD
  --   nof_sp            : NATURAL;  -- = 4;     -- Fixed support 4 signal paths A,B,C,D, whether they contain active data depends on nof_adu
  --   nof_adu           : NATURAL;  -- = 2;     -- When 2 ADUs then use all 4 ports A,B,C,D, one ADU on ports A,B and one ADU on ports C,D,
  --                                             -- when 1 ADU then only use ports C,D
  --   nof_ports         : NATURAL;  -- = 2;     -- Fixed 2 ADC BI ports per ADU
  --   port_w            : NATURAL;  -- = 8;     -- Fixed 8 bit ADC BI port width, the ADC sample width is also 8 bit
  --   nof_ovr           : NATURAL;  -- = 1;     -- There is 1 overflow bit per ADU, use 0 to ignore the overflow input
  --   lvds_data_rate    : NATURAL;  -- = 800;   -- The ADC sample rate is 800 Msps, so the LVDS rate is 800 Mbps per ADC BI data line,
  --   use_dpa           : BOOLEAN;  -- = TRUE;  -- When TRUE use LVDS_RX with DPA, else used fixed IOE delays and/or lvds_clk_phase instead of DPA
  --   use_lvds_clk      : BOOLEAN;  -- = TRUE;  -- When TRUE use the one or both ADC BI lvds_clk, else use the single dp_clk to capture the lvds data
  --   use_lvds_clk_rst  : BOOLEAN;  -- = FALSE; -- When TRUE then support reset pulse to ADU to align the lvds_clk to the dp_clk, else no support
  --   lvds_clk_phase    : NATURAL;  -- = 0;     -- Use PLL phase 0 for edge aligned, phase 180 for center aligned. Only for no DPA
  --   nof_clocks        : NATURAL;  -- = 2;     -- Must be <= nof_adu
  --                                             -- 1 --> Use ADC BI clock D or dp_clk for one or both ADU
  --                                             -- 2 --> Use ADC BI clock A for/from ADU-AB and clock D for/from the ADU-CD
  --   lvds_deser_factor : NATURAL;  -- = 2;     -- The ADC sampled data comes in with a DDR lvds_clk, so lvds_data_rate / 2 or
  --                                             -- the 4 when the Data Path clock dp_clk is also used as LVDS data reference lvds_clk clock
  --   dp_deser_factor   : NATURAL;  -- = 4;     -- The Data Path clock dp_clk frequency is 200 MHz, so lvds_data_rate / 4
  -- END RECORD;
  -- Uncomment one of the c_ai settings:
--   CONSTANT c_ai   : t_c_aduh_pll_ai := (4, 2, 2, 8, 0, 800, FALSE, FALSE, FALSE,  0, 1, 4, 4);  -- model no dpa,  use   dp_clk, lvds_clk_phase =  0
--   CONSTANT c_ai   : t_c_aduh_pll_ai := (4, 2, 2, 8, 0, 800, FALSE,  TRUE, FALSE,  0, 2, 2, 4);  -- model no dpa,  use lvds_clk, lvds_clk_phase =  0
--   CONSTANT c_ai   : t_c_aduh_pll_ai := (4, 2, 2, 8, 0, 800, FALSE,  TRUE, FALSE, 90, 2, 2, 4);  -- model no dpa,  use lvds_clk, lvds_clk_phase = 90
--   CONSTANT c_ai   : t_c_aduh_pll_ai := (4, 2, 2, 8, 0, 800,  TRUE, FALSE, FALSE,  0, 1, 4, 4);  -- model use dpa, use   dp_clk
  constant c_ai   : t_c_aduh_pll_ai := (4, 2, 2, 8, 0, 800,  true,  true, false,  0, 2, 2, 4);  -- model use dpa, use lvds_clk
--   CONSTANT c_ai   : t_c_aduh_pll_ai := (4, 2, 2, 8, 0, 800,  TRUE,  TRUE,  TRUE,  0, 2, 2, 4);  -- model use dpa, use lvds_clk, use lvds_clk_rst

  -- Conclusion on c_ai:
  -- . Using DPA is preferred.
  -- . First using dp_clk as LVDS_RX PLL reference is the preferred scheme.
  -- . If DCLK_RST is supported by BN - ADU then using lvds_clk as LVDS_RX PLL reference with lvds_clk_rst to ADU is preferred.
  -- . Not using DPA and using the lvds_clk should also be tried and verified on the BN - ADU hardware.

  constant c_sample_freq         : natural := c_ai.lvds_data_rate;  -- Msps
  constant c_sample_period       : time := 1000000 ps / c_sample_freq;

  constant c_pll_reset_time      : time := 20 ns;  -- minimum 10 ns
  constant c_measurement_period  : time := 50 us;  -- minimum depends on number of data line toggles for DPA, which can take 10-s of us for MSbit counter data,
                                                     -- because DPA lock is only achieved after about 256 toggles

  constant c_lvds_skew_zero      : t_natural_arr(7 downto 0) := ( 0,  0,  0,  0,  0,  0,  0,  0);  -- ps unit
  constant c_lvds_skew_init      : t_natural_arr(7 downto 0) := (70, 60, 50, 40, 30, 20, 10,  0);  -- ps unit
  constant c_lvds_skew_ovr       : natural := 55;  -- ps unit
  constant c_lvds_skew_clk       : natural := 35;  -- ps unit

  constant c_dp_clk_period       : time := c_ai.dp_deser_factor * c_sample_period;
  constant c_dp_clk_skew         : natural := 335;  -- ps unit, model skew between dp_clk and SCLK, both are in lock with the same reference

  constant c_ana_diff            : natural := 16;  -- analogue offset value between the port A, B, C, D

  constant c_adc_w               : natural := c_ai.port_w;  -- 8 bit
  constant c_dp_dat_w            : natural := c_adc_w * c_ai.dp_deser_factor;  -- 32 bit

  type t_dp_data_arr is array (integer range <>) of std_logic_vector(c_dp_dat_w - 1 downto 0);

  -- ADC : c_ai.port_w = 8-bit
  signal ANA_DAT             : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- Common ADC reference data source
  signal ANA_A               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port A
  signal ANA_B               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port B
  signal ANA_C               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port C
  signal ANA_D               : std_logic_vector(c_ai.port_w - 1 downto 0) := (others => '0');  -- ADC port D
  signal ANA_OVR             : std_logic := '0';
  signal SCLK                : std_logic := '1';  -- central sample clock = 800 MHz
  signal DCLK_AB             : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_CD             : std_logic;  -- digital lvds clock   = 400 MHz (DDR)
  signal DCLK_RST_AB         : std_logic;  -- synchronise digital lvds clock
  signal DCLK_RST_CD         : std_logic;  -- synchronise digital lvds clock
  signal DIG_A               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_B               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_C               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_D               : std_logic_vector(c_ai.port_w - 1 downto 0);
  signal DIG_OVR_AB          : std_logic := '0';
  signal DIG_OVR_CD          : std_logic := '0';

  signal test_pattern_en     : std_logic := '0';
  signal lvds_skew_di        : t_natural_arr(7 downto 0) := c_lvds_skew_init;
  signal lvds_skew_dq        : t_natural_arr(7 downto 0) := c_lvds_skew_init;
  signal lvds_skew_ovr       : natural := c_lvds_skew_ovr;
  signal lvds_skew_dclk      : natural := c_lvds_skew_clk;

  -- Digital
  signal dp_clk_ref          : std_logic := '1';  -- digital data path clock = 200 MHz (deser factor 4);
  signal dp_clk              : std_logic := '1';  -- digital data path clock = 200 MHz (deser factor 4);

  -- DUT
  signal dp_restart          : std_logic_vector(c_ai.nof_clocks - 1 downto 0);
  signal dp_delay_settings   : t_natural_arr(func_aduh_pll_lvds_dat_w(c_ai) - 1 downto 0) := (others => 0);  -- ADC_BI IOE data delay settings when g_use_dpa = FALSE
  signal dp_cda_settings     : t_natural_arr(func_aduh_pll_lvds_dat_w(c_ai) - 1 downto 0) := (others => 1);  -- ADC_BI channel data alignment settings
  signal dp_sosi             : t_dp_sosi_arr(0 to c_ai.nof_sp - 1);  -- ADC_BI ports [0:3] = [A,B,C,D]

  -- Verify
  signal restart_any         : std_logic := '0';
  signal verify_valid        : std_logic_vector(0 to c_ai.nof_sp - 1) := (others => '0');
  signal verify_restart      : std_logic_vector(0 to c_ai.nof_sp - 1) := (others => '0');
  signal verify_en           : std_logic_vector(0 to c_ai.nof_sp - 1) := (others => '0');
  signal verify_en_all       : std_logic := '0';
  signal verify_data         : t_dp_data_arr(   0 to c_ai.nof_sp - 1);
  signal prev_verify_data    : t_dp_data_arr(   0 to c_ai.nof_sp - 1);
  signal sl0                 : std_logic := '0';
  signal sl1                 : std_logic := '1';
  signal slv0                : std_logic_vector(0 downto 0) := "0";
begin
  -----------------------------------------------------------------------------
  -- ADU0 and ADU1 for BN port A,B and C,D
  -----------------------------------------------------------------------------

  -- Same analogue reference signal for all ADC, use incrementing data to ease the verification
--   ANA_DAT <= NOT ANA_DAT WHEN rising_edge(SCLK);
--   ANA_A   <= ANA_DAT;
--   ANA_B   <= ANA_DAT;
--   ANA_C   <= ANA_DAT;
--   ANA_D   <= ANA_DAT;
  ANA_DAT <= INCR_UVEC(ANA_DAT, 1) when rising_edge(SCLK);
  ANA_A   <= INCR_UVEC(ANA_DAT, 0 * c_ana_diff);
  ANA_B   <= INCR_UVEC(ANA_DAT, 1 * c_ana_diff);
  ANA_C   <= INCR_UVEC(ANA_DAT, 2 * c_ana_diff);
  ANA_D   <= INCR_UVEC(ANA_DAT, 3 * c_ana_diff);
  ANA_OVR <= not ANA_OVR when rising_edge(SCLK);  -- simple overflow model used for both ADC

  -- Same sample clock for all ADC
  SCLK <= not SCLK after c_sample_period / 2;

  -- National ADC
  u_adu_AB : entity work.adc08d1020
  generic map (
    g_dclk_init_phase => 0
  )
  port map (
    AI              => TO_SINT(ANA_A),
    AQ              => TO_SINT(ANA_B),
    AOVR            => ANA_OVR,
    CLK             => SCLK,
    DCLK            => DCLK_AB,
    DCLK_RST        => '0',
    DI              => DIG_A,
    DQ              => DIG_B,
    OVR             => DIG_OVR_AB,

    test_pattern_en => test_pattern_en,
    lvds_skew_di    => lvds_skew_di,
    lvds_skew_dq    => lvds_skew_dq,
    lvds_skew_ovr   => lvds_skew_ovr,
    lvds_skew_dclk  => lvds_skew_dclk
  );

  u_adu_CD : entity work.adc08d1020
  generic map (
    g_dclk_init_phase => sel_a_b(c_model_dclk_phase, 1, 0)
  )
  port map (
    AI              => TO_SINT(ANA_C),
    AQ              => TO_SINT(ANA_D),
    AOVR            => ANA_OVR,
    CLK             => SCLK,
    DCLK            => DCLK_CD,
    DCLK_RST        => DCLK_RST_CD,
    DI              => DIG_C,
    DQ              => DIG_D,
    OVR             => DIG_OVR_CD,

    test_pattern_en => test_pattern_en,
    lvds_skew_di    => lvds_skew_di,
    lvds_skew_dq    => lvds_skew_dq,
    lvds_skew_ovr   => lvds_skew_ovr,
    lvds_skew_dclk  => lvds_skew_dclk
  );

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------

  dp_clk_ref <= not dp_clk_ref after c_dp_clk_period / 2;
  dp_clk <= transport dp_clk_ref after c_dp_clk_skew * 1 ps;

  -- Uncomment to try different CDA settings then the init default:
--   dp_cda_settings <= (0, 1, 2, 3, 0, 1, 2, 3,   1, 1, 1, 1, 2, 2, 2, 2,   2, 2, 2, 2, 3, 3, 3, 3,   0, 3, 1, 3, 4, 2, 2, 0);
--   dp_cda_settings <= (0, 0, 0, 0, 0, 0, 0, 0,   1, 1, 1, 1, 1, 1, 1, 1,   2, 2, 2, 2, 2, 2, 2, 2,   3, 3, 3, 3, 3, 3, 3, 3);
--   dp_cda_settings <= (0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0);

  p_dp_restart : process
  begin
    lvds_skew_di <= c_lvds_skew_zero;
    lvds_skew_dq <= c_lvds_skew_zero;

    -- Wait for the ADU
    dp_restart <= (others => '1');  -- assert the LVDS RX PLL reset
    wait until rising_edge(DCLK_AB);
    wait until rising_edge(DCLK_CD);  -- wait to ensure that the DCLKs are active
    wait for 1 * c_sample_period;  -- set arbitrary initial phase

    -- Run several measurement with ADU
    for I in 0 to 10 loop
      wait for c_pll_reset_time;  -- ensure mimimum LVDS RX PLL reset time

      if c_model_rx_clk_phase_uncertainty_in_time = true then
        -- The next measurement will start 1 SCLK c_sample_period later to
        -- verify that the fixed CDA settings are suitable.
        -- It appears in simulation that for the odd I it takes much longer for
        -- the DPA to lock than for the even I.
        wait for I * c_sample_period;
      end if;

      if c_model_rx_clk_phase_uncertainty_in_space = true then
        -- Wait some extra 1 SCLK c_sample_period for each PLL. The assumption
        -- is that using I*c_sample_period to get a different SCLK phase for
        -- the dp_restart may cause CDA misalignment between the ADUs when
        -- g_use_dp_clk_for_cda_reset=FALSE in the lvdsh. In simulation the PLL
        -- lock time is deterministic, so influencing the SCLK phase of
        -- dp_start is a way to model the uncertainty of the real PLL lock time
        -- and of the relative phase of the divided rx_clk (= dclk / 2).
        -- It appears in simulation that in both cases the data output from
        -- ADU0 and ADU1 gets offset by 1 sample. Using the dp_clk to release
        -- the cda_reset does not help. Probably because internally it gets
        -- retimed to the rx_clk domain anyway and the phase relation between
        -- the rx_clk derived from ADU0 and ADU1 may differ.
        for J in c_ai.nof_clocks - 1 downto 0 loop
          dp_restart(J) <= '0';  -- release the LVDS RX PLL(J) reset
          wait for c_sample_period;
        end loop;
      end if;

      dp_restart <= (others => '0');  -- release the LVDS RX PLL reset

      -- Begin of the measurement

      if c_model_lvds_skew = true then
        wait for c_measurement_period;
        lvds_skew_di <= (700, 500, 250, 140, 130, 320, 60, 50);  -- ps unit
        lvds_skew_dq <= (700, 500, 250, 140, 130, 320, 60, 50);  -- ps unit
        wait for 10 * c_measurement_period;
        lvds_skew_di <= c_lvds_skew_zero;
        lvds_skew_dq <= c_lvds_skew_zero;
      end if;

      wait for c_measurement_period;
      dp_restart    <= (others => '1');  -- assert the LVDS RX PLL reset

      if c_model_rx_clk_hold_reference = true then
        -- Do not assert LVDS RX PLL reset for rx_clk 0, so keep this one
        -- running for reference to verify the dp_sosi sample data alignment
        -- with the other clock(s) when c_ai.nof_clocks > 1.
        if c_ai.nof_clocks > 1 then
          dp_restart(0) <= '0';
        end if;
      end if;

      -- End of the measurement
    end loop;
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- DUT: ADUH with PLL
  -----------------------------------------------------------------------------

  -- Assume the ADU board has no skew between the ADCs. This is not too much
  -- simplifcation. Any skew between the ADC will be constant and can then be
  -- compensated for in a fixed, preset way.
  dut : entity work.aduh_pll
  generic map (
    g_ai => c_ai
  )
  port map (
    -- PHY ADU Interface

    -- . ADU_AB
    ADC_BI_AB_OVR    => '0',
    ADC_BI_A         => DIG_A,
    ADC_BI_B         => DIG_B,
    ADC_BI_A_CLK     => DCLK_AB,
    ADC_BI_A_CLK_RST => DCLK_RST_AB,

    -- . ADU_CD
    ADC_BI_CD_OVR    => '0',
    ADC_BI_C         => DIG_C,
    ADC_BI_D         => DIG_D,
    ADC_BI_D_CLK     => DCLK_CD,
    ADC_BI_D_CLK_RST => DCLK_RST_CD,

    -- DP Interface
    dp_clk           => dp_clk,

    -- . Control
    restart          => dp_restart,
    delay_settings   => dp_delay_settings,
    cda_settings     => dp_cda_settings,

    -- . Streaming
    src_out          => dp_sosi  -- = [0:3] = ADC_BI ports [A,B,C,D]
  );

  -----------------------------------------------------------------------------
  -- Verify dp_sosi
  -- . verify that there is valid data, i.e. that the test has run at all
  -- . verify that the valid data per ADC port is incrementing
  -- . verify that the valid data between the ADC ports has the correct diff (alignment)
  -----------------------------------------------------------------------------

  restart_any   <= vector_or(dp_restart);
  verify_en_all <= vector_and(verify_en);

  p_verify_valid : process
  begin
    while true loop
      wait until falling_edge(restart_any);
      wait for c_measurement_period;
      if verify_en_all = '0' then
        report "ADUH with PLL: No valid data, test may not be running."
          severity ERROR;
      end if;
    end loop;
  end process;

  -- Verify dp_sosi[0,1,2,3].data symbols compared to ADC samples from [ANA_A, ANA_B, ANA_C, ANA_D]
  gen_verify_increment : for I in 0 to c_ai.nof_sp - 1 generate
    -- Enable verify after the valid goes active and continue until valid goes low
    verify_valid(I) <= dp_sosi(I).valid when rising_edge(dp_clk);
    verify_restart(I) <= dp_restart((I * c_ai.nof_clocks) / c_ai.nof_sp);
    verify_en(I) <= verify_valid(I) and not verify_restart(I);  -- also and with dp_restart, because of latency in dp_sosi.valid

    -- Verify that the symbols in the data are incrementing per ADC (ready not used, empty not used)
    verify_data(I) <= dp_sosi(I).data(c_dp_dat_w - 1 downto 0);
    proc_dp_verify_symbols(1, c_dp_dat_w, c_adc_w, dp_clk, verify_en(I), sl1, dp_sosi(I).valid, sl0, verify_data(I), slv0, prev_verify_data(I));
  end generate;  -- gen_verify_increment

  -- Verify the data alignment between the ADCs by checking the expected c_ana_diff offset
  gen_verify_alignment : for I in 1 to c_ai.nof_sp - 1 generate
    p_alignment : process(dp_clk)
    begin
      if rising_edge(dp_clk) then
        if verify_en_all = '1' then
          for J in 0 to c_ai.dp_deser_factor - 1 loop
            if verify_data(I)((J + 1) * c_adc_w - 1 downto J * c_adc_w) /= INCR_UVEC(verify_data(I - 1)((J + 1) * c_adc_w - 1 downto J * c_adc_w), c_ana_diff) then
              report "ADUH with PLL: Wrong alignment between ADCs."
                severity ERROR;
            end if;
          end loop;
        end if;
      end if;
    end process;
  end generate;  -- gen_verify_alignment
end tb;
