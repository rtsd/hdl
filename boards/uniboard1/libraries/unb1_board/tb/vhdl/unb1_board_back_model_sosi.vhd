-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Model the UniBoard backplane of the Apertif beamformer
-- Description:
--   Model the backplane at sosi level. See unb1_board_back_model_sl for more details.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_pkg.all;
use work.unb1_board_pkg.all;
use work.tb_unb1_board_pkg.all;

entity unb1_board_back_model_sosi is
  port (
    backplane_in_sosi_4arr    : in  t_unb1_board_back_sosi_4arr;  -- _4arr = (UNB 3,2,1,0)(BN 3,2,1,0)(phy bus 2,1,0)(tr lane 3,2,1,0)
    backplane_out_sosi_4arr   : out t_unb1_board_back_sosi_4arr
  );
end unb1_board_back_model_sosi;

architecture beh of unb1_board_back_model_sosi is
begin
  gen_bn : for BN in 0 to c_unb1_board_nof_bn - 1 generate
    ----------------------------------------------------------------------------
    -- Interconnect BN --> BN streaming with actual backplane routing
    ----------------------------------------------------------------------------

    backplane_out_sosi_4arr(0)(BN)(0) <= backplane_in_sosi_4arr(3)(BN)(0);
    backplane_out_sosi_4arr(0)(BN)(1) <= backplane_in_sosi_4arr(1)(BN)(2);
    backplane_out_sosi_4arr(0)(BN)(2) <= backplane_in_sosi_4arr(2)(BN)(2);

    backplane_out_sosi_4arr(1)(BN)(0) <= backplane_in_sosi_4arr(3)(BN)(1);
    backplane_out_sosi_4arr(1)(BN)(1) <= backplane_in_sosi_4arr(2)(BN)(0);
    backplane_out_sosi_4arr(1)(BN)(2) <= backplane_in_sosi_4arr(0)(BN)(1);

    backplane_out_sosi_4arr(2)(BN)(0) <= backplane_in_sosi_4arr(1)(BN)(1);
    backplane_out_sosi_4arr(2)(BN)(1) <= backplane_in_sosi_4arr(3)(BN)(2);
    backplane_out_sosi_4arr(2)(BN)(2) <= backplane_in_sosi_4arr(0)(BN)(2);

    backplane_out_sosi_4arr(3)(BN)(0) <= backplane_in_sosi_4arr(0)(BN)(0);
    backplane_out_sosi_4arr(3)(BN)(1) <= backplane_in_sosi_4arr(1)(BN)(0);
    backplane_out_sosi_4arr(3)(BN)(2) <= backplane_in_sosi_4arr(2)(BN)(1);
    --                      ^   ^  ^                            ^      ^
    --                      |   |  |                            |      |
    --                      |   |  BN_BI RX phy bu              |      BN_BI TX phy bus
    --                      |   |                               Transmitting UniBoard
    --                      |   Same scheme applies to all back nodes
    --                      Receiving UniBoard
  end generate;
end beh;
