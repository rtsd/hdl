#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for unb1_minimal

Usage:

   --rep = number of intervals that diagnostics results are verified
   --sim targets a running simulation.

Description:
   This test case tests:
   - system info
   - read sensors
   - read ppsh
   - write to wdi to force reload from bank 0
   - flash access: write image to bank 1
   - remote update: start image in bank 1

"""

###############################################################################
# System imports
import sys
import signal
import test_case
import node_io
import pi_system_info
import pi_unb_sens
import pi_unb_fpga_sens
import pi_unb_fpga_voltage_sens
import pi_ppsh
import pi_wdi
import pi_epcs
import pi_remu
import pi_eth
import pi_debug_wave

from tools import *
from common import *
from pi_common import *


def test_info(tc,io,cmd):
    tc.set_section_id('Read System Info - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    info = pi_system_info.PiSystemInfo(tc, io)
    info.read_system_info()
    tc.append_log(3, '')
    info.read_use_phy()
    tc.append_log(3, '')
    design_name = info.read_design_name()
    tc.append_log(1, '>>> design_name=%s' % design_name)
    tc.append_log(3, '')
    info.read_stamps()
    tc.append_log(3, '')
    info.read_design_note()
    
    expected_design_name = tc.gpString
    if expected_design_name != '':
        tc.set_section_id('Verify System Info - ')
        compared=True
        for name in design_name:
            if (name != expected_design_name): 
                tc.set_result('FAILED')
                compared=False
                tc.append_log(2, '>>> design_name mismatch!! (%s != %s)' % (name,expected_design_name))
        tc.append_log(1, '>>> Verify design_name == %s: %s' % (expected_design_name,compared))



def read_regmap(tc,io,cmd):
    tc.set_section_id('Update REGMAP - ')
    info = pi_system_info.PiSystemInfo(tc, io)
    tc.append_log(1, '>>> reading REGMAPs')
    info.make_register_info()
    tc.append_log(1, '>>> reload NodeIO class')
    return node_io.NodeIO(tc.nodeImages, tc.base_ip)


    
def test_sensors(tc,io,cmd):
    tc.set_section_id('Read sensors - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    
    sens = pi_unb_sens.PiUnbSens(tc, io)

    sens.read_unb_sensors()
    tc.append_log(3, '')
    #sens.read_fpga_temperature()
    tc.append_log(3, '')
    sens.read_eth_temperature()
    tc.append_log(3, '')
    sens.read_unb_current()
    sens.read_unb_voltage()
    sens.read_unb_power()

    # Read internal FPGA temp sensor:
    tc.set_section_id('Read internal fpga sensors - ')
    sens = pi_unb_fpga_sens.PiUnbFpgaSens(tc, io)
    sens.read_fpga_temperature()
    sens = pi_unb_fpga_voltage_sens.PiUnbFpgaVoltageSens(tc, io)
    sens.read_fpga_voltage()


def test_ppsh(tc,io,cmd):
    tc.set_section_id('Read PPSH capture count - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    
    Ppsh = pi_ppsh.PiPpsh(tc, io)
    Ppsh.read_ppsh_capture_cnt()
    tc.append_log(3, '')



def test_wdi(tc,io,cmd):
    tc.set_section_id('Reset to image in bank 0 using WDI - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    Wdi = pi_wdi.PiWdi(tc, io)
    Wdi.write_wdi_override()
    tc.append_log(3, '')
    tc.append_log(3, '>>> Booting...')
    tc.sleep(5.0)



def test_remu(tc,io,cmd):
    tc.set_section_id('REMU start image in bank 1 - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')

    dummy_tc = test_case.Testcase('Dummy TB - ', '',logfilename='REMU-log')
    dummy_tc.set_result('PASSED')
    
    Remu = pi_remu.PiRemu(dummy_tc, io)
    try:
        Remu.write_user_reconfigure()
    except:
        pass # ignoring FAILED

    if dummy_tc.get_result() == 'FAILED':
        tc.append_log(1, 'Result=%s but ignoring this' % dummy_tc.get_result())

    tc.append_log(3, '>>> Booting...')
    tc.sleep(5.0)
    tc.append_log(3, '')



def test_eth(tc,io,cmd):
    tc.set_section_id('ETH status - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    
    eth = pi_eth.PiEth(tc, io)
    hdr=eth.read_hdr(0)
    eth.disassemble_hdr(hdr)
    tc.append_log(3, '')



def test_flash(tc,io,cmd):
    tc.set_section_id('Flash write to bank 1 - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    
    Epcs = pi_epcs.PiEpcs(tc, io)
    path_to_rbf = instanceName = tc.gpString
    Epcs.write_raw_binary_file("user", path_to_rbf)
    tc.append_log(3, '')

    tc.set_section_id('Flash read/verify bank 1 - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> Read from flash (pi_epcs.py)')
    tc.append_log(3, '>>>')
    
    path_to_rbf = instanceName = tc.gpString
    Epcs.read_and_verify_raw_binary_file("user", path_to_rbf)
    tc.append_log(3, '')


def set_led(tc,dw,led,text):
    tc.append_log(3, text)
    dw.set_led(led)
    tc.sleep(1.0)

def test_leds(tc,io,cmd):
    tc.set_section_id('LED test - ')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    tc.append_log(3, '>>>')
    dw =  pi_debug_wave.PiDebugWave(tc, io)
    set_led(tc,dw,'off',   '')
    set_led(tc,dw,'red',  'RED on')
    set_led(tc,dw,'off',  'RED off')
    set_led(tc,dw,'green','GREEN on')
    set_led(tc,dw,'off',  'GREEN off')
    set_led(tc,dw,'both', 'ORANGE (RED+GREEN) on')
    set_led(tc,dw,'off',  'ORANGE (RED+GREEN) off')
    tc.append_log(3, '')


def sleep(tc,io,cmd):
    tc.set_section_id('%s - ' % cmd)
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))
    if cmd == 'sleep1':
        tc.sleep(1.0)
    elif cmd == 'sleep5':
        tc.sleep(5.0)

def show_help(tc,io,cmd):
    tc.set_section_id('%s - ' % cmd)
    tc.append_log(1, '>>> %s' % help_text(tc,io,cmd))


# Avaliable commands
Cmd = dict()
Cmd['REGMAP']  = (read_regmap,  'using pi_system_info to read register info (access PIO_SYSTEM_INFO) and store REGMAPs','')
Cmd['INFO']    = (test_info,    'using pi_system_info to read system info (access PIO_SYSTEM_INFO)','(-s for expected design_name)')
Cmd['FLASH']   = (test_flash,   'using pi_epcs to program/verify flash','(-s for .rbf file)')
Cmd['SENSORS'] = (test_sensors, 'using pi_unb_sens to readout sensors (access REG_UNB_SENS)','')
Cmd['LED']     = (test_leds,    'using pi_debug_wave to set LEDs (access PIO_DEBUG_WAVE)','')
Cmd['PPSH']    = (test_ppsh,    'using pi_ppsh to read PPSH capture count (access PIO_PPS)','')
Cmd['ETH']     = (test_eth,     'using pi_eth to read eth status','')
Cmd['REMU']    = (test_remu,    'using pi_remu to load user image (access REG_REMU)','')
Cmd['WDI']     = (test_wdi,     'using pi_wdi to reset to image in bank 0 (access REG_WDI)','')
Cmd['sleep1']  = (sleep,        'Sleep 1 second','')
Cmd['sleep5']  = (sleep,        'Sleep 5 seconds','')
Cmd['example'] = (show_help,    'show several example commands','')
Cmd['help']    = (show_help,    'show help on commands','')


def help_text(tc,io,cmd):
    str=''
    if cmd == 'help':
        tc.append_log(0, '\n')
        tc.append_log(0, '>>> Help:')
        tc.append_log(0, 'Usage: %s <nodes> <command sequence> [-v..] [--rep ...]' % sys.argv[0])
        tc.append_log(0, '')
        tc.append_log(0, '    <nodes>: use: --unb N --fn N --bn N (N is a number or vector) or:')
        tc.append_log(0, '    <nodes>: use: --gn N (N is a number or vector)')
        tc.append_log(0, '    <command sequence>: use: --seq <command(s) separated by ",">:')
        tc.append_log(0, '')
        for cmd in sorted(Cmd):
            tc.append_log(0, '    .  %s\t%s  %s' % (cmd,Cmd[cmd][1],Cmd[cmd][2]))
        tc.append_log(0, '')
        tc.append_log(0, '    [-vN]: verbose level N (default=5): %s' % tc.verbose_levels())
        tc.append_log(0, '    [--rep N]: N=number of repeats, where -1 is forever, non-stop')
        help_text(tc,io,'example')
    elif cmd == 'example':
        tc.append_log(0, '')
        tc.append_log(0, '>>> Examples:')
        tc.append_log(0, '')
        tc.append_log(0, 'Getting INFO from all nodes on 1 Uniboard: %s --gn 0:7 --seq INFO' % sys.argv[0])
        tc.append_log(0, '')
        tc.append_log(0, '[reset, load user img] sequence: --seq REGMAP,WDI,REGMAP,REMU,REGMAP,INFO')
        tc.append_log(0, '[flash+start user img] sequence: --seq FLASH,WDI,REGMAP,REMU,REGMAP,INFO -s file.rbf')
        tc.append_log(0, '[re-read info,sensors] sequence: --seq INFO,PPSH,SENSORS --rep -1 -s expected_design_name')
        tc.append_log(0, '[reset to factory]     sequence: --seq WDI,REGMAP')
        tc.append_log(0, '[program user image]   sequence: --seq FLASH -s file.rbf')
        tc.append_log(0, '[load user image]      sequence: --seq REMU,REGMAP')
        tc.append_log(0, '[modelsim BG-DB test] arguments: --unb 0 --fn 0 --seq BGDB --sim -r 0:2')
        tc.append_log(0, '\n')
    else:
        str = Cmd[cmd][1]
    return str


def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    tc.repeat=0


##################################################################################################################
# Main
#
# Create a test case object
tc = test_case.Testcase('TB - ', '')
tc.set_result('PASSED')
dgnName = tc.gpString
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Title : Test bench (%s) on nodes %s, %s' % (sys.argv[0],tc.unb_nodes_string(''),dgnName))
tc.append_log(0, '>>> Commandline : %s' % " ".join(sys.argv))
tc.append_log(3, '>>>')


# Create access object for nodes
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

signal.signal(signal.SIGINT, signal_handler)

##################################################################################################################
# Run tests
while tc.repeat != 0: # -1 for non-stop
    tc.repeat -= 1
    tc.next_run()
    tc.append_log(3, '')
    
    try:
        for cmd in tc.sequence:
            tc.set_section_id('Next command: %s ' % cmd)
            tc.append_log(1, '>>> Testrun %d (@%.02fs) - ' % (tc.get_nof_runs(),tc.get_run_time()))

            if cmd == 'REGMAP': # reload node_io:
                io = Cmd[cmd][0](tc,io,cmd)
            else:
                Cmd[cmd][0](tc,io,cmd)


    except KeyError:
        print 'Unknown command:',cmd
        cmd='help'
        Cmd[cmd][0](tc,io,cmd)
#    except:
#        print 'Catched error:',sys.exc_info()[0]



##################################################################################################################
# End
tc.set_section_id('')
tc.append_log(3, '')
tc.append_log(3, '>>>')
tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
tc.append_log(0, '>>> Number of runs=%d' % tc.get_nof_runs())
tc.append_log(0, '>>> Number of errors=%d' % tc.get_nof_errors())
tc.append_log(0, '>>> Runtime=%f seconds (%f hours)' % (tc.get_run_time(),tc.get_run_time()/3600))
tc.append_log(3, '>>>')

sys.exit(tc.get_result())

