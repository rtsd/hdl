library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

architecture rtl of pft_reverse is
  signal i_rdaddr   : std_logic_vector(rdaddr'range);
  signal nxt_rdaddr : std_logic_vector(rdaddr'range);
  signal i_rden     : std_logic;
  signal nxt_rden   : std_logic;
  signal rdrdy      : std_logic;
begin
  rdaddr    <= i_rdaddr;
  rden      <= i_rden;
  page_done <= rdrdy;

  registers : process (rst, clk)
  begin
    if rst = '1' then
      -- Output signals.
      i_rdaddr <= (others => '0');
      i_rden   <= '0';
      -- Internal signals.
    elsif rising_edge(clk) then
      -- Output signals.
      i_rdaddr <= nxt_rdaddr;
      i_rden   <= nxt_rden;
      -- Internal signals.
    end if;
  end process;

  read_enable_control : process (i_rden, page_rdy, rdrdy)
  begin
    nxt_rden <= i_rden;
    if page_rdy = '1' then
      nxt_rden <= '1';
    elsif rdrdy = '1' then
      nxt_rden <= '0';
    end if;
  end process;

  read_addr_control : process (i_rdaddr, i_rden)
  begin
    rdrdy      <= '0';
    nxt_rdaddr <= i_rdaddr;
    if unsigned(i_rdaddr) >= g_fft_sz - 1 then
      nxt_rdaddr <= (others => '0');
      rdrdy      <= '1';
    elsif i_rden = '1' then
      nxt_rdaddr <= std_logic_vector(unsigned(i_rdaddr) + 1);
    end if;
  end process;

  out_dat_re <= rddata_re when rdval = '1' else (others => '0');
  out_dat_im <= rddata_im when rdval = '1' else (others => '0');
  out_val    <= '1' when rdval = '1' else '0';
  out_sync   <= '1' when rdsync = '1' else '0';
end rtl;
