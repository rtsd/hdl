-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: pipelined FFT
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pft_unswitch is
  generic (
    g_fft_sz_w        : natural;
    g_dat_w           : natural
  );
  port (
    in_re             : in std_logic_vector(g_dat_w - 1 downto 0);
    in_im             : in std_logic_vector(g_dat_w - 1 downto 0);
    in_val            : in std_logic;
    in_sync           : in std_logic;
    switch_en         : in std_logic;
    out_re            : out std_logic_vector(g_dat_w - 1 downto 0);
    out_im            : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val           : out std_logic;
    out_sync          : out std_logic;
    clk               : in std_logic;
    rst               : in std_logic
  );
end pft_unswitch;

architecture rtl of pft_unswitch is
signal cnt          : std_logic_vector(g_fft_sz_w downto 0);
signal nxt_cnt      : std_logic_vector(cnt'range);

signal lfsr_bit1    : std_logic;
signal lfsr_bit2    : std_logic;

signal lfsr_en      : std_logic;

signal nxt_out_val  : std_logic;
signal nxt_out_sync : std_logic;
signal nxt_out_re   : std_logic_vector(in_re'range);
signal nxt_out_im   : std_logic_vector(in_im'range);
begin
  registers : process (rst, clk)
  begin
    if rst = '1' then
      cnt      <= (others => '0');
      out_val  <= '0';
      out_sync <= '0';
      out_re   <= (others => '0');
      out_im   <= (others => '0');
    elsif rising_edge(clk) then
      cnt      <= nxt_cnt;
      out_val  <= nxt_out_val;
      out_sync <= nxt_out_sync;
      out_re   <= nxt_out_re;
      out_im   <= nxt_out_im;
    end if;
  end process;

  counter: process(cnt, in_val, in_sync)
  begin
    nxt_cnt <= cnt;
    if in_sync = '1' then
      nxt_cnt <= (others => '0');
    elsif in_val = '1' then
      nxt_cnt <= std_logic_vector(unsigned(cnt) + 1);
    end if;
  end process;

  lfsr_ctrl: process(cnt, in_val)
  begin
    if signed(cnt) = -1 and in_val = '1' then
      lfsr_en <= '1';
    else
      lfsr_en <= '0';
    end if;
  end process;

  proc: process(in_re, in_im, in_val, in_sync, cnt, lfsr_bit1, lfsr_bit2, switch_en)
  begin
    nxt_out_val  <= in_val;
    nxt_out_sync <= in_sync and in_val;
    nxt_out_re   <= in_re;
    nxt_out_im   <= in_im;
    if    ((cnt(0) = '0' and cnt(cnt'high) = lfsr_bit1)
       or (cnt(0) = '1' and cnt(cnt'high) = lfsr_bit2)) and (switch_en = '1') then
        nxt_out_re <= std_logic_vector(-signed(in_re));
        nxt_out_im <= std_logic_vector(-signed(in_im));
    end if;
  end process;

  lfsr: entity work.pft_lfsr
  port map (
    clk      => clk,
    rst      => rst,
    in_en    => lfsr_en,
    out_bit1 => lfsr_bit1,
    out_bit2 => lfsr_bit2
  );
end rtl;
