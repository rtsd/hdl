-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;

package eth_pkg is
  constant c_eth_data_w                : natural := c_tech_tse_data_w;  -- = c_word_w
  constant c_eth_empty_w               : natural := c_tech_tse_empty_w;  -- = ceil_log2(c_word_sz) = 2;
  constant c_eth_error_w               : natural := c_tech_tse_error_w;  -- = 6, but no error field, pass error info on via checksum or CRC /= 0 in packet word

  constant c_eth_rx_ready_latency      : natural := c_tech_tse_rx_ready_latency;  -- = 2 = default when FIFO is used
  constant c_eth_tx_ready_latency      : natural := c_tech_tse_tx_ready_latency;  -- = 0, c_tech_tse_tx_ready_latency + 3 = TX_ALMOST_FULL
  constant c_eth_ready_latency         : natural := 1;  -- = 1, fixed ETH module internal RL

  -- Maximum feasible frame size
  constant c_eth_max_frame_sz          : natural := 1024 * 9;  -- 9 kByte fits 9020 = word align (2) + eth header (14) + maximum jumbo payload (9000) + CRC (4)
  constant c_eth_max_frame_nof_words   : natural := c_eth_max_frame_sz / c_word_sz;
  constant c_eth_max_frame_nof_words_w : natural := ceil_log2(c_eth_max_frame_nof_words);  -- = 12 bit

  -- Actual frame space in RAM <= c_eth_max_frame_sz, default >= 1520, <= 9020 for jumbo
  --CONSTANT c_eth_frame_sz              : NATURAL := 1024*3/2;  -- Use 1536 = 3/2 M9K, to benefit from Rx and Tx in single buffer of 2*1.5=3 M9K, but does
                                                                 -- yield simulation warning: Address pointed at port A is out of bound!
  --CONSTANT c_eth_frame_sz              : NATURAL := 1024*8;  -- Use 8192 = 8 M9K for jumbo frame support with payload size <= 8172
  --CONSTANT c_eth_frame_sz              : NATURAL := 1024*9;  -- Use 9216 = 9 M9K for jumbo frame support with payload size <= 9000
  constant c_eth_frame_sz              : natural := 1024 * 2;  -- Use 2048 = 2 M9K to avoid simulation warning: Address pointed at port A is out of bound!
                                                               -- when the module is used in an Nios II SOPC system
                                                               -- The FIFOs seem to require nof words to be a power of 2, but that is taken care of locally if necessary
  constant c_eth_frame_nof_words       : natural := c_eth_frame_sz / c_word_sz;
  constant c_eth_frame_nof_words_w     : natural := ceil_log2(c_eth_frame_nof_words);  -- >= 9 bit, <= 12 bit

  type t_eth_data_arr is array (integer range <>) of std_logic_vector(c_eth_data_w - 1 downto 0);

  ------------------------------------------------------------------------------
  -- Definitions for eth header status
  ------------------------------------------------------------------------------

  type t_eth_hdr_status is record
    is_arp            : std_logic;
    is_ip             : std_logic;
    is_icmp           : std_logic;
    is_udp            : std_logic;
    ip_checksum       : std_logic_vector(c_halfword_w - 1 downto 0);
    ip_checksum_val   : std_logic;
    ip_checksum_is_ok : std_logic;
    udp_port          : std_logic_vector(c_network_udp_port_slv'range);
    is_dhcp           : std_logic;
  end record;

  constant c_eth_hdr_status_rst : t_eth_hdr_status := ('0', '0', '0', '0',
                                                       (others => '0'), '0', '0',
                                                       (others => '0'), '0');

  ------------------------------------------------------------------------------
  -- Definitions for eth demux udp
  ------------------------------------------------------------------------------

  constant c_eth_nof_udp_ports : natural := 4;  -- support 2, 4, 8, ... UDP off-load ports
  constant c_eth_channel_w     : natural := ceil_log2(c_eth_nof_udp_ports + 1);  -- + 1 for all other packets that go to the default port
  constant c_eth_nof_channels  : natural := 2**c_eth_channel_w;

  ------------------------------------------------------------------------------
  -- MM register map
  ------------------------------------------------------------------------------

  constant c_eth_reg_demux_nof_words    : natural := c_eth_nof_udp_ports;
  constant c_eth_reg_config_nof_words   : natural := 4;
  constant c_eth_reg_control_nof_words  : natural := 1;
  constant c_eth_reg_frame_nof_words    : natural := 1;
  constant c_eth_reg_status_nof_words   : natural := 1;

  constant c_eth_reg_demux_wi           : natural := 0;
  constant c_eth_reg_config_wi          : natural := c_eth_reg_demux_wi   + c_eth_reg_demux_nof_words;
  constant c_eth_reg_control_wi         : natural := c_eth_reg_config_wi  + c_eth_reg_config_nof_words;
  constant c_eth_reg_frame_wi           : natural := c_eth_reg_control_wi + c_eth_reg_control_nof_words;
  constant c_eth_reg_status_wi          : natural := c_eth_reg_frame_wi   + c_eth_reg_frame_nof_words;
  constant c_eth_reg_continue_wi        : natural := c_eth_reg_status_wi  + c_eth_reg_status_nof_words;

  -- . write/read back registers
  type t_eth_demux_ports_arr is array(1 to c_eth_nof_udp_ports) of std_logic_vector(c_network_udp_port_w - 1 downto 0);
  type t_eth_mm_reg_demux is record
    udp_ports_en : std_logic_vector(1 to c_eth_nof_udp_ports);  -- [16]
    udp_ports    : t_eth_demux_ports_arr;  -- [15:0]
  end record;

  type t_eth_mm_reg_config is record
    udp_port             : std_logic_vector(c_network_udp_port_w - 1 downto 0);  -- [15:0]
    ip_address           : std_logic_vector(c_network_ip_addr_w - 1 downto 0);  -- [31:0]
    mac_address          : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);  -- [15:0], [31:0]
  end record;

  type t_eth_mm_reg_control is record
    tx_nof_words      : std_logic_vector(c_eth_max_frame_nof_words_w - 1 downto 0);  -- 12 bit
    tx_empty          : std_logic_vector(c_eth_empty_w - 1 downto 0);  -- 2 bit
    tx_request        : std_logic;  -- 1 bit
    tx_en             : std_logic;  -- 1 bit
    rx_en             : std_logic;  -- 1 bit
  end record;

  type t_eth_mm_reg_control_bi is record  -- bit indices
    tx_nof_words      : natural;  -- [26:18]
    tx_empty          : natural;  -- [17:16]
    tx_request        : natural;  -- [2]
    tx_en             : natural;  -- [1]
    rx_en             : natural;  -- [0]
  end record;

  constant c_eth_mm_reg_control_bi  : t_eth_mm_reg_control_bi := (18, 16, 2, 1, 0);
  constant c_eth_mm_reg_control_rst : t_eth_mm_reg_control := ((others => '0'), (others => '0'), '0', '0', '0');

  -- . read only registers
  type t_eth_mm_reg_frame is record
    is_dhcp           : std_logic;
    is_udp_ctrl_port  : std_logic;
    is_udp            : std_logic;
    is_icmp           : std_logic;
    ip_address_match  : std_logic;
    ip_checksum_is_ok : std_logic;
    is_ip             : std_logic;
    is_arp            : std_logic;
    mac_address_match : std_logic;
    eth_mac_error     : std_logic_vector(c_eth_error_w - 1 downto 0);
  end record;

  type t_eth_mm_reg_frame_bi is record  -- bit indices
    is_dhcp           : natural;  -- [15]
    is_udp_ctrl_port  : natural;  -- [14]
    is_udp            : natural;  -- [13]
    is_icmp           : natural;  -- [12]
    ip_address_match  : natural;  -- [11]
    ip_checksum_is_ok : natural;  -- [10]
    is_ip             : natural;  -- [9]
    is_arp            : natural;  -- [8]
    mac_address_match : natural;  -- [7]
    eth_mac_error     : natural;  -- [6] not used, [5:0] = TSE MAC error
  end record;

  constant c_eth_mm_reg_frame_bi  : t_eth_mm_reg_frame_bi := (15, 14, 13, 12, 11, 10, 9, 8, 7, 0);
  constant c_eth_mm_reg_frame_rst : t_eth_mm_reg_frame    := ('0', '0', '0', '0', '0', '0', '0', '0', '0', (others => '0'));

  type t_eth_mm_reg_status is record
    rx_nof_words      : std_logic_vector(c_eth_max_frame_nof_words_w - 1 downto 0);  -- 12 bit
    rx_empty          : std_logic_vector(c_eth_empty_w - 1 downto 0);  -- 2 bit
    tx_avail          : std_logic;  -- 1 bit
    tx_done           : std_logic;  -- 1 bit
    rx_avail          : std_logic;  -- 1 bit
  end record;

  type t_eth_mm_reg_status_bi is record  -- bit indices
    rx_nof_words      : natural;  -- [26:18]
    rx_empty          : natural;  -- [17:16]
    tx_avail          : natural;  -- [2]
    tx_done           : natural;  -- [1]
    rx_avail          : natural;  -- [0]
  end record;

  constant c_eth_mm_reg_status_bi  : t_eth_mm_reg_status_bi := (18, 16, 2, 1, 0);
  constant c_eth_mm_reg_status_rst : t_eth_mm_reg_status    := ((others => '0'), (others => '0'), '0', '0', '0');

  -- Register mapping functions
  function func_eth_mm_reg_demux(  mm_reg : std_logic_vector)     return t_eth_mm_reg_demux;
  function func_eth_mm_reg_demux(  mm_reg : t_eth_mm_reg_demux)   return std_logic_vector;
  function func_eth_mm_reg_config( mm_reg : std_logic_vector)     return t_eth_mm_reg_config;
  function func_eth_mm_reg_config( mm_reg : t_eth_mm_reg_config)  return std_logic_vector;
  function func_eth_mm_reg_control(mm_reg : std_logic_vector)     return t_eth_mm_reg_control;
  function func_eth_mm_reg_control(mm_reg : t_eth_mm_reg_control) return std_logic_vector;
  function func_eth_mm_reg_frame(  mm_reg : std_logic_vector)     return t_eth_mm_reg_frame;
  function func_eth_mm_reg_frame(  mm_reg : t_eth_mm_reg_frame)   return std_logic_vector;
  function func_eth_mm_reg_status( mm_reg : std_logic_vector)     return t_eth_mm_reg_status;
  function func_eth_mm_reg_status( mm_reg : t_eth_mm_reg_status)  return std_logic_vector;

  ------------------------------------------------------------------------------
  -- Definitions for eth_mm_registers
  ------------------------------------------------------------------------------

  constant c_eth_reg_nof_words    : natural := c_eth_reg_demux_nof_words +
                                               c_eth_reg_config_nof_words +
                                               c_eth_reg_control_nof_words +
                                               c_eth_reg_frame_nof_words +
                                               c_eth_reg_status_nof_words;
  constant c_eth_reg_addr_w       : natural := ceil_log2(c_eth_reg_nof_words + 1);  -- + 1 for c_eth_continue_wi

  ------------------------------------------------------------------------------
  -- Definitions for ETH Rx packet buffer and Tx packet buffer
  ------------------------------------------------------------------------------

  -- Use MM bus data width = c_word_w = 32
  constant c_eth_ram_rx_offset : natural := 0;
  constant c_eth_ram_tx_offset : natural := c_eth_frame_nof_words;
  constant c_eth_ram_nof_words : natural := c_eth_frame_nof_words * 2;
  constant c_eth_ram_addr_w    : natural := ceil_log2(c_eth_ram_nof_words);
end eth_pkg;

package body eth_pkg is
  -- Register mapping functions
  function func_eth_mm_reg_demux(mm_reg : std_logic_vector) return t_eth_mm_reg_demux is
    variable v_reg : t_eth_mm_reg_demux;
  begin
    -- Demux UDP MM registers
    for I in 1 to c_eth_nof_udp_ports loop
      v_reg.udp_ports_en(I) := mm_reg(c_network_udp_port_w   + (I - 1) * c_word_w);  -- [16]   = UDP port enable
      v_reg.udp_ports(I)    := mm_reg(c_network_udp_port_w - 1 + (I - 1) * c_word_w downto (I - 1) * c_word_w);  -- [15:0] = UDP port number
    end loop;

    return v_reg;
  end func_eth_mm_reg_demux;

  function func_eth_mm_reg_demux(mm_reg : t_eth_mm_reg_demux) return std_logic_vector is
    variable v_reg : std_logic_vector(c_eth_reg_demux_nof_words * c_word_w - 1 downto 0);
  begin
    v_reg := (others => '0');  -- rsvd
    for I in 1 to c_eth_nof_udp_ports loop
      v_reg(c_network_udp_port_w   + (I - 1) * c_word_w)                       := mm_reg.udp_ports_en(I);  -- [16]   = UDP port enable
      v_reg(c_network_udp_port_w - 1 + (I - 1) * c_word_w downto (I - 1) * c_word_w) := mm_reg.udp_ports(I);  -- [15:0] = UDP port number
    end loop;
    return v_reg;
  end func_eth_mm_reg_demux;

  -- MM config register
  function func_eth_mm_reg_config(mm_reg : std_logic_vector) return t_eth_mm_reg_config is
    variable v_reg : t_eth_mm_reg_config;
  begin
    v_reg.udp_port                        := mm_reg(c_network_udp_port_w + 3 * c_word_w - 1 downto 3 * c_word_w);  -- [15:0]  = control UDP port number
    v_reg.ip_address                      := mm_reg(                     3 * c_word_w - 1 downto 2 * c_word_w);  -- [31:0]  = this node IP address
    v_reg.mac_address(47 downto 32)       := mm_reg(        c_halfword_w +  c_word_w - 1 downto   c_word_w);  -- [47:32] = this node MAC address
    v_reg.mac_address(31 downto 0)        := mm_reg(               c_word_w - 1 downto   0);  -- [31:0]  = this node MAC address
    return v_reg;
  end func_eth_mm_reg_config;

  function func_eth_mm_reg_config(mm_reg : t_eth_mm_reg_config) return std_logic_vector is
    variable v_reg : std_logic_vector(c_eth_reg_config_nof_words * c_word_w - 1 downto 0);
  begin
    v_reg                                                      := (others => '0');  -- rsvd
    v_reg(c_network_udp_port_w + 3 * c_word_w - 1 downto 3 * c_word_w) := mm_reg.udp_port;  -- [15:0]  = control UDP port number
    v_reg(                     3 * c_word_w - 1 downto 2 * c_word_w) := mm_reg.ip_address;  -- [31:0]  = this node IP address
    v_reg(        c_halfword_w +  c_word_w - 1 downto   c_word_w) := mm_reg.mac_address(47 downto 32);  -- [47:32] = this node MAC address
    v_reg(                       c_word_w - 1 downto   0)        := mm_reg.mac_address(31 downto 0);  -- [31:0]  = this node MAC address
    return v_reg;
  end func_eth_mm_reg_config;

  -- MM control register
  function func_eth_mm_reg_control(mm_reg : std_logic_vector) return t_eth_mm_reg_control is
    variable v_reg : t_eth_mm_reg_control;
  begin
    v_reg.tx_nof_words := mm_reg(c_eth_max_frame_nof_words_w + c_eth_empty_w + 16 - 1 downto c_eth_empty_w + 16);  -- [29:18]
    v_reg.tx_empty     := mm_reg(                            c_eth_empty_w + 16 - 1 downto               16);  -- [17:16]
    v_reg.tx_request   := mm_reg(                                                                     2);  -- [2]
    v_reg.tx_en        := mm_reg(                                                                     1);  -- [1]
    v_reg.rx_en        := mm_reg(                                                                     0);  -- [0]
    return v_reg;
  end func_eth_mm_reg_control;

  function func_eth_mm_reg_control(mm_reg : t_eth_mm_reg_control) return std_logic_vector is
    variable v_reg : std_logic_vector(c_eth_reg_control_nof_words * c_word_w - 1 downto 0);
  begin
    v_reg                                                                         := (others => '0');  -- rsvd
    v_reg(c_eth_max_frame_nof_words_w + c_eth_empty_w + 16 - 1 downto c_eth_empty_w + 16) := mm_reg.tx_nof_words;  -- [29:18]
    v_reg(                            c_eth_empty_w + 16 - 1 downto               16) := mm_reg.tx_empty;  -- [17:16]
    v_reg(                                                                     2) := mm_reg.tx_request;  -- [2]
    v_reg(                                                                     1) := mm_reg.tx_en;  -- [1]
    v_reg(                                                                     0) := mm_reg.rx_en;  -- [0]
    return v_reg;
  end func_eth_mm_reg_control;

  -- MM frame register
  function func_eth_mm_reg_frame(mm_reg : std_logic_vector) return t_eth_mm_reg_frame is
    variable v_reg : t_eth_mm_reg_frame;
  begin
    v_reg.is_dhcp           := mm_reg(              c_byte_w + 7);  -- [15]
    v_reg.is_udp_ctrl_port  := mm_reg(              c_byte_w + 6);  -- [14]
    v_reg.is_udp            := mm_reg(              c_byte_w + 5);  -- [13]
    v_reg.is_icmp           := mm_reg(              c_byte_w + 4);  -- [12]
    v_reg.ip_address_match  := mm_reg(              c_byte_w + 3);  -- [11]
    v_reg.ip_checksum_is_ok := mm_reg(              c_byte_w + 2);  -- [10]
    v_reg.is_ip             := mm_reg(              c_byte_w + 1);  -- [9]
    v_reg.is_arp            := mm_reg(              c_byte_w + 0);  -- [8]
    v_reg.mac_address_match := mm_reg(              c_byte_w - 1);  -- [7]
    v_reg.eth_mac_error     := mm_reg(c_eth_error_w - 1 downto 0);  -- [7] not used, [5:0] = TSE MAC error
    return v_reg;
  end func_eth_mm_reg_frame;

  function func_eth_mm_reg_frame(mm_reg : t_eth_mm_reg_frame) return std_logic_vector is
    variable v_reg : std_logic_vector(c_eth_reg_frame_nof_words * c_word_w - 1 downto 0);
  begin
    v_reg                           := (others => '0');  -- rsvd
    v_reg(              c_byte_w + 7) := mm_reg.is_dhcp;  -- [15]
    v_reg(              c_byte_w + 6) := mm_reg.is_udp_ctrl_port;  -- [14]
    v_reg(              c_byte_w + 5) := mm_reg.is_udp;  -- [13]
    v_reg(              c_byte_w + 4) := mm_reg.is_icmp;  -- [12]
    v_reg(              c_byte_w + 3) := mm_reg.ip_address_match;  -- [11]
    v_reg(              c_byte_w + 2) := mm_reg.ip_checksum_is_ok;  -- [10]
    v_reg(              c_byte_w + 1) := mm_reg.is_ip;  -- [9]
    v_reg(              c_byte_w + 0) := mm_reg.is_arp;  -- [8]
    v_reg(              c_byte_w - 1) := mm_reg.mac_address_match;  -- [7]
    v_reg(c_eth_error_w - 1 downto 0) := mm_reg.eth_mac_error;  -- [6] not used, [5:0] = TSE MAC error
    return v_reg;
  end func_eth_mm_reg_frame;

  -- MM status register
  function func_eth_mm_reg_status(mm_reg : std_logic_vector) return t_eth_mm_reg_status is
    variable v_reg : t_eth_mm_reg_status;
  begin
    v_reg.rx_nof_words := mm_reg(c_eth_max_frame_nof_words_w + c_eth_empty_w + 16 - 1 downto c_eth_empty_w + 16);  -- [29:18]
    v_reg.rx_empty     := mm_reg(                            c_eth_empty_w + 16 - 1 downto               16);  -- [17:16]
    v_reg.tx_avail     := mm_reg(                                                                     2);  -- [2]
    v_reg.tx_done      := mm_reg(                                                                     1);  -- [1]
    v_reg.rx_avail     := mm_reg(                                                                     0);  -- [0]
    return v_reg;
  end func_eth_mm_reg_status;

  function func_eth_mm_reg_status(mm_reg : t_eth_mm_reg_status) return std_logic_vector is
    variable v_reg : std_logic_vector(c_eth_reg_status_nof_words * c_word_w - 1 downto 0);
  begin
    v_reg                                                                         := (others => '0');  -- rsvd
    v_reg(c_eth_max_frame_nof_words_w + c_eth_empty_w + 16 - 1 downto c_eth_empty_w + 16) := mm_reg.rx_nof_words;  -- [29:18]
    v_reg(                            c_eth_empty_w + 16 - 1 downto               16) := mm_reg.rx_empty;  -- [17:16]
    v_reg(                                                                     2) := mm_reg.tx_avail;  -- [2]
    v_reg(                                                                     1) := mm_reg.tx_done;  -- [1]
    v_reg(                                                                     0) := mm_reg.rx_avail;  -- [0]
    return v_reg;
  end func_eth_mm_reg_status;
end eth_pkg;
