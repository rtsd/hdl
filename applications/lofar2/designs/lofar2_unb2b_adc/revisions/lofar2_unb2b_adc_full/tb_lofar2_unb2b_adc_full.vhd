-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: Jonathan Hargreaves
-- Purpose: Tb to show that lofar2_unb2b_adc_full can simulate
-- Description:
--   Must use c_sim = TRUE to speed up simulation
--   This is a compile-only test bench
-- Usage:
--   Load sim    # check that design can load in vsim
--   > as 10     # check that the hierarchy for g_design_name is complete
--   > run -a    # check that design can simulate some us without error

library IEEE, common_lib, unb2b_board_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_lofar2_unb2b_adc_full is
end tb_lofar2_unb2b_adc_full;

architecture tb of tb_lofar2_unb2b_adc_full is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;  -- Back node 3
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2b_board_fw_version := (1, 0);

  constant c_eth_clk_period  : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period  : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_pps_period      : natural := 1000;

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal pps_rst             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal pmbus_scl           : std_logic;
  signal pmbus_sda           : std_logic;

  -- back transceivers
  signal bck_rx              : std_logic_vector(c_unb2b_board_nof_tr_jesd204b + c_unb2b_board_start_tr_jesd204b - 1  downto c_unb2b_board_nof_tr_jesd204b);
  signal bck_ref_clk         : std_logic := '1';

  -- jesd204b syncronization signals
  signal jesd204b_sysref     : std_logic;
  signal jesd204b_sync_n     : std_logic_vector(c_unb2b_board_nof_sync_jesd204b - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  bck_ref_clk <= not bck_ref_clk after c_bck_ref_clk_period / 2;  -- JESD sample clock (200MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, ext_clk, pps);
  jesd204b_sysref <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_lofar_unb2b_adc_full : entity work.lofar2_unb2b_adc_full
  generic map (
    g_sim         => c_sim,
    g_sim_unb_nr  => c_unb_nr,
    g_sim_node_nr => c_node_nr
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- I2C Interface to Sensors
    SENS_SC      => sens_scl,
    SENS_SD      => sens_sda,

    PMBUS_SC     => pmbus_scl,
    PMBUS_SD     => pmbus_sda,
    PMBUS_ALERT  => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- LEDs
    QSFP_LED     => open,

    -- back transceivers
    BCK_RX       => bck_rx,
    BCK_REF_CLK  => bck_ref_clk,

    -- jesd204b syncronization signals
    JESD204B_SYSREF => jesd204b_sysref,
    JESD204B_SYNC_N => jesd204b_sync_n
  );

  ------------------------------------------------------------------------------
  -- Simulation end
  ------------------------------------------------------------------------------
  sim_done <= '0', '1' after 1 us;

  proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
end tb;
