--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Test bench for tr_xaui using diagnostics and PHY loop back with delay and link_fault
-- Description:
-- Usage:
--   > as 10
--   > run -a

library IEEE, common_lib, dp_lib, diagnostics_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;

entity tb_tr_xaui is
  generic (
    g_technology         : natural := c_tech_stratixiv;
    g_tb_end             : boolean := true;  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
    g_sim_level          : natural := 0  -- 0 = use IP; 1 = use fast serdes model
  );
  port (
    tb_end : out std_logic
  );
end entity tb_tr_xaui;

architecture str of tb_tr_xaui is
  constant c_sim                 : boolean := true;  -- tr_xaui_align_dly has time delay ~ 1 sec, so not suitable for simulation

  constant tr_clk_period         : time := 6.4 ns;  -- 156.25 MHz
  constant tx_clk_period         : time := 6.4 ns;  -- 156.25 MHz
  constant mm_clk_period         : time := 25 ns;  -- 40 MHz
  constant cal_rec_clk_period    : time := 25 ns;  -- 40 MHz

  constant phy_delay             : time :=  sel_a_b(g_sim_level > 0, tr_clk_period * 1, 1 ns);  -- the sim_xaui only works without unit PHY delays

  constant c_nof_streams         : natural := 1;
  constant c_xgmii_data_w        : natural := 64;
  constant c_nof_lanes           : natural := 4;

  signal tr_clk                  : std_logic := '1';
  signal tr_rst                  : std_logic;

  signal xaui_tx_arr             : std_logic_vector(c_nof_lanes - 1 downto 0);
  signal xaui_tx_arr_dly         : std_logic_vector(c_nof_lanes - 1 downto 0);
  signal xaui_rx_arr             : std_logic_vector(c_nof_lanes - 1 downto 0);

  signal rx_sosi_arr             : t_dp_sosi_arr(c_nof_streams - 1 downto 0);
  signal rx_siso_arr             : t_dp_siso_arr(c_nof_streams - 1 downto 0);

  signal tx_sosi_arr             : t_dp_sosi_arr(c_nof_streams - 1 downto 0);
  signal tx_siso_arr             : t_dp_siso_arr(c_nof_streams - 1 downto 0);

  signal src_diag_en             : std_logic_vector(c_nof_streams - 1 downto 0);
  signal src_val_cnt             : t_slv_32_arr(c_nof_streams - 1 downto 0);

  signal snk_diag_en             : std_logic_vector(c_nof_streams - 1 downto 0);
  signal snk_diag_res            : std_logic_vector(c_nof_streams - 1 downto 0);
  signal snk_diag_res_val        : std_logic_vector(c_nof_streams - 1 downto 0);
  signal snk_val_cnt             : t_slv_32_arr(c_nof_streams - 1 downto 0);

  signal tx_clk                  : std_logic := '1';
  signal tx_rst                  : std_logic;

  signal rx_clk                  : std_logic;
  signal rx_rst                  : std_logic;

  signal mm_clk                  : std_logic := '1';
  signal mm_rst                  : std_logic := '0';

  signal cal_rec_clk             : std_logic := '1';

  signal link_fault              : std_logic;
  signal verify_en               : std_logic;
begin
  -- Duration
  p_tb_end : process
  begin
    tb_end <= '0';
    link_fault <= '0';

    snk_diag_en <= (others => '0');
    src_diag_en <= (others => '0');
    verify_en <= '0';
    wait for 2 us;  -- use e.g. wait for 1800 ns to check that p_verify indeed detects errors

    -- verify link
    proc_common_wait_until_low(rx_clk, rx_rst);
    proc_common_wait_some_cycles(tx_clk, 20);
    src_diag_en <= (others => '1');  -- first enable Tx seq
    proc_common_wait_some_cycles(rx_clk, 20);
    snk_diag_en <= (others => '1');  -- then enable Rx seq
    proc_common_wait_some_cycles(rx_clk, 50);
    verify_en <= '1';  -- p_verify that Rx seq result is OK
    wait for 5 us;

    -- model link fault
    verify_en <= '0';  -- p_verify stop
    snk_diag_en <= (others => '0');  -- disable Rx seq to avoid Error due to link_fault, keep Tx seq enabled
    link_fault <= '1';
    wait for 1 us;

    -- verify link recovery after link fault
    link_fault <= '0';
    wait for 1 us;
    proc_common_wait_until_low(rx_clk, rx_rst);
    proc_common_wait_some_cycles(rx_clk, 20);
    snk_diag_en <= (others => '1');  -- enable Rx seq again
    proc_common_wait_some_cycles(rx_clk, 50);
    verify_en <= '1';  -- p_verify that Rx seq result is OK
    wait for 5 us;

    -- Stop the simulation
    tb_end <= '1';
    if g_tb_end = false then
      report "Tb Simulation finished."
        severity NOTE;
    else
      report "Tb Simulation finished."
        severity FAILURE;
    end if;
    wait;
  end process;

  tr_clk  <= not tr_clk after tr_clk_period / 2;
  tx_clk  <= not tx_clk after tx_clk_period / 2;
  mm_clk  <= not mm_clk after mm_clk_period / 2;
  cal_rec_clk  <= not cal_rec_clk after cal_rec_clk_period / 2;

  tr_rst <= '1', '0' after tr_clk_period * 4;

  -- Verify and duration
  p_verify : process (rx_clk)
  begin
    if rising_edge(rx_clk) then
      if verify_en = '1' then
        assert unsigned(snk_diag_res) = 0 and signed(snk_diag_res_val) = -1
          report "tb_tr_xaui : Wrong diagnostics result value"
          severity ERROR;
      end if;
    end if;
  end process;

  -- Loopback:
  xaui_tx_arr_dly <= transport xaui_tx_arr after phy_delay;
  xaui_rx_arr <= xaui_tx_arr_dly when link_fault = '0' else (others => '0');

  u_tr_xaui: entity WORK.tr_xaui
  generic map (
    g_technology       => g_technology,
    g_sim              => c_sim,
    g_sim_level        => g_sim_level,
    g_use_mdio         => true
  )
  port map (
    tr_clk             => tr_clk,
    tr_rst             => tr_rst,

    cal_rec_clk        => cal_rec_clk,

    mm_clk             => mm_clk,
    mm_rst             => mm_rst,

    --Parallel data
    tx_clk_arr(0)      => tx_clk,
    tx_rst_arr(0)      => tx_rst,
    tx_sosi_arr        => tx_sosi_arr,
    tx_siso_arr        => tx_siso_arr,

    rx_clk_arr_out(0)  => rx_clk,
    rx_clk_arr_in(0)   => rx_clk,
    rx_rst_arr(0)      => rx_rst,
    rx_sosi_arr        => rx_sosi_arr,
    rx_siso_arr        => rx_siso_arr,

    --Serial data
    xaui_tx_arr(0)     => xaui_tx_arr,
    xaui_rx_arr(0)     => xaui_rx_arr
  );

  u_diagnostics: entity diagnostics_lib.diagnostics
  generic map (
    g_dat_w          => c_xgmii_data_w,
    g_nof_streams    => c_nof_streams,
    g_separate_clk   => true
     )
  port map (
    src_rst(0)       => tx_rst,
    src_clk(0)       => tx_clk,

    snk_rst(0)       => rx_rst,
    snk_clk(0)       => rx_clk,

    snk_out_arr      => rx_siso_arr,
    snk_in_arr       => rx_sosi_arr,
    snk_diag_en      => snk_diag_en,
    snk_diag_md      => (others => '1'),
    snk_diag_res     => snk_diag_res,
    snk_diag_res_val => snk_diag_res_val,
    snk_val_cnt      => snk_val_cnt,

    src_out_arr      => tx_sosi_arr,
    src_in_arr       => tx_siso_arr,
    src_diag_en      => src_diag_en,
    src_diag_md      => (others => '1'),
    src_val_cnt      => src_val_cnt
  );
end architecture str;
