--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Combine mac_10g and xaui for c_tech_stratixiv
-- Description:
--
--   Blockdiagram:
--
--                                 ref
--                             156 156
--                _________    | ^  |   ____________
--                |       |    | |  |   |          |
--                |       |    | |  \-->|          |
--                |       |<---/ \------|          |
--                |       |             |          |
--                |       |    XGMII    |          |
--     tx_snk --->|tech_  |------------>|tech_     |---> xaui_tx[3:0]
--     rx_src <---|mac_10g|<------------|xaui      |<--- xaui_rx[3:0]
--                |       |             |          |
--                |_______|--\       /--|__________|
--                    |      |       |
--                  mac_mm   |       |
--                           |       v
--                       (   |    xgmii_tx_ready       )
--                       (   v    txc_rx_channelaligned)
--     tx_snk_out.xon <--(xgmii_link_status[1:0]       )
--
-- . g_direction
--   "TX_RX" = Default support bidir
--   "TX_ONLY" = Uses a bidir MAC and connects the MAC TX to the XAUI TX and
--       also to the MAC RX. The MAC RX needs to see XGMII Rx data otherwise
--       the Tx does not start. Therefore the simple approach using
--       xgmii_internal_dc_arr is implemented. Three other implementations have
--       been considered:
--       . Instantiate a bidir MAC for Tx and another bidir MAC for Rx, to
--         avoid leaving xgmii_rx_dc_arr from XAUI PHY unconnected.
--       . Instantiate a Tx only MAC IP , but that is not tried.
--       . Configuring the bidir MAC to Tx unidirectional via MM still does
--         not seem to work without activity on the XGMII Rx data.
--   "RX_ONLY" = Same as "TX_RX", just leave the Tx ST and Tx XAUI pins not
--       connected. Synthesis will optimize away what is not needed.
--
-- Remarks:
-- . xgmii_link_status:
--   A link fault eg. due to rx disconnect is detected by the link fault status:
--     0 = OK
--     1 = local fault
--     2 = remote fault
--
--   From google search:
--     Link fault Operation
--     1) Device B detects loss of signal. Local fault is signaled by PHY of Device B to Device B.
--     2) Device B ceases transmission of MAC frames and transmits remote fault to Device A.
--     3) Device A receives remote fault from Device B.
--     4) Device A stops sending frames, continuously generates Idle.
--

library IEEE, common_lib, dp_lib, technology_lib, tech_xaui_lib, tech_mac_10g_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;

entity ip_stratixiv_eth_10g is
  generic (
    g_sim                     : boolean := false;
    g_sim_level               : natural := 0;  -- 0 = use XAUI IP; 1 = use fast serdes model
    g_nof_channels            : natural := 1;
    g_direction               : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_pre_header_padding      : boolean := false
  );
  port (
    -- Transceiver PLL reference clock
    tr_ref_clk_156   : in  std_logic;  -- 156.25 MHz for XAUI
    tr_ref_rst_156   : in  std_logic;

    -- Calibration & reconfig clock
    cal_rec_clk      : in  std_logic;

    -- Data clocks
    tx_clk_arr_in    : in  std_logic_vector(g_nof_channels - 1 downto 0);  -- 156.25 MHz, externally connect tr_ref_clk_156 to tx_clk_arr_in or connect rx_clk_arr to tx_clk_arr_in
    tx_rst_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_clk_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);
    rx_clk_arr_in    : in  std_logic_vector(g_nof_channels - 1 downto 0);  -- externally connect to rx_clk_arr_out to avoid clock delta-delay
    rx_rst_arr_out   : out std_logic_vector(g_nof_channels - 1 downto 0);

    -- MM
    mm_clk           : in  std_logic;
    mm_rst           : in  std_logic;

    mac_mosi         : in  t_mem_mosi;  -- MAG_10G (CSR)
    mac_miso         : out t_mem_miso;

    xaui_mosi        : in  t_mem_mosi := c_mem_mosi_rst;  -- XAUI control
    xaui_miso        : out t_mem_miso;

    -- ST
    tx_snk_in_arr    : in  t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ 156 MHz tx_clk_arr_in
    tx_snk_out_arr   : out t_dp_siso_arr(g_nof_channels - 1 downto 0);

    rx_src_out_arr   : out t_dp_sosi_arr(g_nof_channels - 1 downto 0);  -- 64 bit data @ 156 MHz rx_clk_arr_in
    rx_src_in_arr    : in  t_dp_siso_arr(g_nof_channels - 1 downto 0);

    -- XAUI serial IO
    xaui_tx_arr      : out t_xaui_arr(g_nof_channels - 1 downto 0);
    xaui_rx_arr      : in  t_xaui_arr(g_nof_channels - 1 downto 0)
  );
end ip_stratixiv_eth_10g;

architecture str of ip_stratixiv_eth_10g is
  -- Enable or disable the conditions that must be ok to release tx_snk_out_arr xon
  constant c_check_link_status       : boolean := g_direction /= "TX_ONLY";
  constant c_check_rx_channelaligned : boolean := g_direction /= "TX_ONLY";
  constant c_check_xgmii_tx_ready    : boolean := g_direction /= "RX_ONLY";

  -- MAG_10G control status registers
  signal mac_mosi_arr              : t_mem_mosi_arr(g_nof_channels - 1 downto 0);
  signal mac_miso_arr              : t_mem_miso_arr(g_nof_channels - 1 downto 0);

  -- ST
  signal i_tx_rst_arr_out          : std_logic_vector(g_nof_channels - 1 downto 0);
  signal i_rx_rst_arr_out          : std_logic_vector(g_nof_channels - 1 downto 0);

  signal txc_tx_ready_arr          : std_logic_vector(g_nof_channels - 1 downto 0);  -- tx_ready in tx_clk_arr_in domain, can be used for xon flow control
  signal rxc_rx_ready_arr          : std_logic_vector(g_nof_channels - 1 downto 0);  -- rx_ready in rx_clk_arr domain, typically leave not connected

  signal txc_rx_channelaligned_arr : std_logic_vector(g_nof_channels - 1 downto 0);  -- rx_channelaligned in tx_clk_arr_in domain, from PHY XAUI, indicates
                                                                                   -- that all 4 RX channels are aligned when asserted

  -- XON control
  signal mac_snk_out_arr           : t_dp_siso_arr(g_nof_channels - 1 downto 0);

  -- XGMII
  signal xgmii_link_status_arr     : t_tech_mac_10g_xgmii_status_arr(g_nof_channels - 1 downto 0);  -- 2 bit, from MAC_10g
  signal xgmii_tx_ready_arr        : std_logic_vector(g_nof_channels - 1 downto 0);  -- 1 bit, from PHY XAUI
  signal xgmii_tx_dc_arr           : t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit
  signal xgmii_rx_dc_arr           : t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit
  signal xgmii_internal_dc_arr     : t_xgmii_dc_arr(g_nof_channels - 1 downto 0);  -- 72 bit
begin
  -- Clocks and reset
  tx_rst_arr_out <= i_tx_rst_arr_out;
  rx_rst_arr_out <= i_rx_rst_arr_out;

  i_tx_rst_arr_out <= not txc_tx_ready_arr when g_direction /= "RX_ONLY" else i_rx_rst_arr_out;  -- in case of RX_ONLY use the rx rst also for tx to have an active rst release, clock domain crossing issues can be ignored
  i_rx_rst_arr_out <= not rxc_rx_ready_arr when g_direction /= "TX_ONLY" else i_tx_rst_arr_out;  -- in case of TX_ONLY use the tx rst also for rx to have an active rst release, clock domain crossing issues can be ignored

  xgmii_tx_ready_arr <= txc_tx_ready_arr;

  gen_mac : for I in 0 to g_nof_channels - 1 generate
    tx_snk_out_arr(I).ready <= mac_snk_out_arr(I).ready;  -- pass on MAC cycle accurate backpressure

    p_xon_flow_control : process(tx_clk_arr_in)
      variable v_xgmii_link_status     : std_logic_vector(c_tech_mac_10g_link_status_w - 1 downto 0) := "00";
      variable v_txc_rx_channelaligned : std_logic := '1';
      variable v_xgmii_tx_ready        : std_logic := '1';
    begin
      if rising_edge(tx_clk_arr_in(I)) then
        tx_snk_out_arr(I).xon <= '0';

        -- First gather all conditions that are enabled to affect xon. The default value is such that it enables xon when the condition is not checked.
        if c_check_link_status      = true then v_xgmii_link_status     := xgmii_link_status_arr(I);     end if;  -- check both remote fault [1] and local fault [0]
        if c_check_rx_channelaligned = true then v_txc_rx_channelaligned := txc_rx_channelaligned_arr(I); end if;  -- check that all 4 RX channels are aligned
        if c_check_xgmii_tx_ready   = true then v_xgmii_tx_ready        := xgmii_tx_ready_arr(I);        end if;

        -- Now apply the conditions to xon
        if v_xgmii_tx_ready = '1' and v_txc_rx_channelaligned = '1' and v_xgmii_link_status = "00" then
          tx_snk_out_arr(I).xon <= '1';  -- XON when Tx PHY is ready and XGMII is ok
        end if;
      end if;
    end process;

    u_tech_mac_10g : entity tech_mac_10g_lib.tech_mac_10g
    generic map (
      g_technology          => c_tech_stratixiv,
      g_pre_header_padding  => g_pre_header_padding
    )
    port map (
      -- MM
      mm_clk            => mm_clk,
      mm_rst            => mm_rst,
      csr_mosi          => mac_mosi_arr(I),
      csr_miso          => mac_miso_arr(I),

      -- ST
      tx_clk_156        => tx_clk_arr_in(I),
      tx_rst            => i_tx_rst_arr_out(I),
      tx_snk_in         => tx_snk_in_arr(I),  -- 64 bit data
      tx_snk_out        => mac_snk_out_arr(I),

      rx_clk_156        => rx_clk_arr_in(I),
      rx_rst            => i_rx_rst_arr_out(I),
      rx_src_out        => rx_src_out_arr(I),  -- 64 bit data
      rx_src_in         => rx_src_in_arr(I),

      -- XGMII
      xgmii_link_status => xgmii_link_status_arr(I),
      xgmii_tx_data     => xgmii_tx_dc_arr(I),
      xgmii_rx_data     => xgmii_internal_dc_arr(I)
    );
  end generate;

  xgmii_internal_dc_arr <= xgmii_tx_dc_arr when g_direction = "TX_ONLY" else xgmii_rx_dc_arr;

  u_tech_xaui: entity tech_xaui_lib.tech_xaui
  generic map (
    g_technology      => c_tech_stratixiv,
    g_sim             => g_sim,
    g_sim_level       => g_sim_level,
    g_nof_xaui        => g_nof_channels  -- Up to 3 (hard XAUI only) supported
  )
  port map (
    -- Transceiver PLL reference clock
    tr_clk                    => tr_ref_clk_156,
    tr_rst                    => tr_ref_rst_156,

    -- Calibration & reconfig clock
    cal_rec_clk               => cal_rec_clk,

    -- MM interface
    mm_clk                    => mm_clk,
    mm_rst                    => mm_rst,

    xaui_mosi                 => xaui_mosi,
    xaui_miso                 => xaui_miso,

    -- XGMII interface
    tx_clk_arr                => tx_clk_arr_in,
    rx_clk_arr_out            => rx_clk_arr_out,
    rx_clk_arr_in             => rx_clk_arr_in,

    txc_tx_ready_arr          => txc_tx_ready_arr,  -- tx_ready in tx_clk_arr_in domain, can be used for xon flow control
    rxc_rx_ready_arr          => rxc_rx_ready_arr,  -- rx_ready in rx_clk_arr domain, typically leave not connected

    txc_rx_channelaligned_arr => txc_rx_channelaligned_arr,  -- rx_channelaligned in tx_clk_arr_in domain, indicates that all 4 RX channels are aligned when asserted

    xgmii_tx_dc_arr           => xgmii_tx_dc_arr,
    xgmii_rx_dc_arr           => xgmii_rx_dc_arr,

    -- XAUI serial IO
    xaui_tx_arr               => xaui_tx_arr,
    xaui_rx_arr               => xaui_rx_arr
  );

  -----------------------------------------------------------------------------
  -- MM bus mux
  -----------------------------------------------------------------------------
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_channels,
    g_mult_addr_w => func_tech_mac_10g_csr_addr_w(c_tech_stratixiv)
  )
  port map (
    mosi     => mac_mosi,
    miso     => mac_miso,
    mosi_arr => mac_mosi_arr,
    miso_arr => mac_miso_arr
  );
end str;
