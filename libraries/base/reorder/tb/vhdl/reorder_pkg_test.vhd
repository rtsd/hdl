-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : E. Kooistra
-- Purpose:
--   Test bench to verify functions in reorder_pkg.vhd.
-- Description:
--
-- Usage:
-- > as 3
-- > run -all
-- * The tb is self stopping and self checking, tb_end will stop the simulation
--   by stopping the clk and thus all toggling.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.reorder_pkg.all;

entity reorder_pkg_test is
  generic(
    g_nof_blocks_per_packet : natural := 4;
    g_nof_data_per_block    : natural := 488;
    g_nof_words_per_data    : natural := 1
  );
end reorder_pkg_test;

architecture tb of reorder_pkg_test is
  constant c_clk_period   : time := 10 ns;
  constant c_nof_data     : natural := g_nof_blocks_per_packet * g_nof_data_per_block;

  signal rst              : std_logic;
  signal clk              : std_logic := '1';
  signal tb_end           : std_logic := '0';

  -- Verify default and alternative (lu = look up) implementation of r_transpose
  signal r_transpose      : t_reorder_transpose := c_reorder_transpose_rst;
  signal r_transpose_lu   : t_reorder_transpose := c_reorder_transpose_rst;
  signal in_address       : natural := 0;
  signal in_val           : std_logic := '0';
  signal out_address      : natural;
  signal out_address_lu   : natural;
  signal out_val          : std_logic;
  signal exp_address      : natural;

  constant c_exp_addresses_arr : t_natural_arr := func_reorder_transpose_indices(g_nof_blocks_per_packet,
                                                                                 g_nof_data_per_block,
                                                                                 g_nof_words_per_data);

  constant c_impl_addresses_arr : t_natural_arr := func_reorder_transpose_indices_impl(g_nof_blocks_per_packet,
                                                                                       g_nof_data_per_block,
                                                                                       g_nof_words_per_data);
begin

  assert c_exp_addresses_arr = c_impl_addresses_arr
    report "Wrong func_reorder_transpose_indices_impl()"
    severity failure;

  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  p_stimuli : process
  begin
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    for I in 0 to c_nof_data - 1 loop
      in_val <= '1';
      in_address <= I;
      r_transpose    <= func_reorder_transpose(
        g_nof_blocks_per_packet, g_nof_data_per_block, g_nof_words_per_data, r_transpose);
      r_transpose_lu <= func_reorder_transpose_look_up(
        g_nof_blocks_per_packet, g_nof_data_per_block, g_nof_words_per_data, r_transpose_lu);
      proc_common_wait_some_cycles(clk, 1);
    end loop;
    in_val <= '0';
    r_transpose <= c_reorder_transpose_rst;
    r_transpose_lu <= c_reorder_transpose_rst;

    proc_common_wait_some_cycles(clk, 5);
    tb_end <= '1';
    wait;
  end process;

  out_address    <= TO_UINT(r_transpose.select_copi.address);
  out_address_lu <= TO_UINT(r_transpose_lu.select_copi.address);
  out_val        <= r_transpose.select_copi.rd;

  exp_address <= c_exp_addresses_arr(in_address);

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if in_val = '1' then
        -- Only when valid expect that out_address = exp_address
        assert out_address = exp_address
          report "Wrong transpose address"
          severity error;
      end if;
      -- Always expect that out_address_lu = out_address
      assert out_address_lu = out_address
        report "Wrong transpose_lu address"
        severity error;
    end if;
  end process;
end tb;
