--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, dp_lib, diag_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mm_rx_logger is
  generic (
    g_technology    : natural := c_tech_select_default;
    g_dat_w         : natural;
    g_fifo_wr_depth : natural := 128  -- Only put powers of 2 here.
   );
  port (
    rx_rst             : in  std_logic;
    rx_clk             : in  std_logic;

    data_in            : in  std_logic_vector(g_dat_w - 1 downto 0);

    log_en_evt         : in  std_logic := '0';  -- Trigger inputs, can be overridden by MM control.
    log_dis_evt        : in  std_logic := '0';

    mst_log_en         : out std_logic;  -- Can be used to enable a slave logger simultaneously. Then this output on the master instance...
    sla_log_en         : in  std_logic := '0';  -- ...connects to this input on the slave instance.

    mm_rst             : in  std_logic;
    mm_clk             : in  std_logic;

    -- MM registers
    ctrl_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    ctrl_miso          : out t_mem_miso;

    data_mosi          : in  t_mem_mosi := c_mem_mosi_rst;
    data_miso          : out t_mem_miso := c_mem_miso_rst

  );
end mm_rx_logger;

architecture str of mm_rx_logger is
  constant c_data_wr_rd_ratio          : natural := g_dat_w / c_word_w;
  constant c_dpmm_fifo_wr_depth        : natural := 16;
  constant c_data_dpmm_fifo_rd_depth   : natural := c_data_wr_rd_ratio * c_dpmm_fifo_wr_depth;
  constant c_data_dpmm_fifo_rd_usedw_w : natural := ceil_log2(c_data_dpmm_fifo_rd_depth);

  signal data_dpmm_fifo_rd_usedw : std_logic_vector(c_data_dpmm_fifo_rd_usedw_w - 1 downto 0);

  signal data_log_fifo_sosi      : t_dp_sosi;
  signal data_log_fifo_siso      : t_dp_siso;

  signal data_flusher_sosi       : t_dp_sosi;
  signal data_flusher_siso       : t_dp_siso;

  signal data_dpmm_fifo_sosi     : t_dp_sosi;
  signal data_dpmm_fifo_siso     : t_dp_siso;

  signal ovr_data_dpmm_fifo_siso : t_dp_siso;  -- overridden ready signal

  signal data_mm_sosi            : t_dp_sosi;
  signal data_mm_siso            : t_dp_siso;

  signal flush_en                : std_logic;

  signal data_mm_rd              : std_logic;
  signal data_mm_rd_data         : std_logic_vector(c_word_w - 1 downto 0);
  signal data_mm_rd_val          : std_logic;
  signal data_mm_rd_usedw        : std_logic_vector(c_word_w - 1 downto 0);

  signal trig_log_en             : std_logic;
  signal log_en                  : std_logic;
  signal mm_ovr                  : std_logic := '0';  -- Normal operation (mm_over='0')    : trigger enable/disable inputs control log_en
                                                     -- Overridden operation (mm_over='1'): MM master controls log_en

  signal mm_trig_on              : std_logic;
  signal mm_trig_one_shot        : std_logic;
  signal mm_trig_nof_words       : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_trig_nof_logged_words: std_logic_vector(c_word_w - 1 downto 0);

  signal trig_en_evt             : std_logic;
  signal trig_dis_evt            : std_logic;

  signal mm_trig_en_evt          : std_logic;
  signal mm_trig_dis_evt         : std_logic;
begin
  -- ============== logged 'data' data path (256 bits of data as example) =======================================================
  --                                                                              |
  --              256     /----------\     256     /---------\     256     /---------------\      32     /---------------\
  -- data_in ------/----->| log fifo |------/----->| flusher |------/----->| dp to mm fifo |------/----->| dp_fifo_to_mm |---> MM
  --                      \----------/             \---------/             \---------------/             \---------------/
  --                                                                              |
  -- ============================================================================================================================

  trig_en_evt  <= log_en_evt  when mm_ovr = '0' else mm_trig_en_evt;
  trig_dis_evt <= log_dis_evt when mm_ovr = '0' else mm_trig_dis_evt;

  -- FSM to enable/disable logging based on trigger inputs
  u_mm_rx_logger_trig : entity work.mm_rx_logger_trig
  port map (
    clk           => rx_clk,
    rst           => rx_rst,
    trig_on       => mm_trig_on,
    one_shot      => mm_trig_one_shot,
    log_en_evt    => trig_en_evt,
    log_dis_evt   => trig_dis_evt,
    log_nof_words => mm_trig_nof_words,
    log_cnt       => mm_trig_nof_logged_words,
    log_en        => trig_log_en
  );

  log_en     <= trig_log_en or sla_log_en;  -- Allow slave input to enable logging
  mst_log_en <= log_en;  -- Forward log_en signal to master output

  data_log_fifo_sosi.valid <= log_en;
  data_log_fifo_sosi.data(g_dat_w - 1 downto 0) <= data_in;

  -- We'll force the valid to '1' so all data words are stored.
  -- The FIFO's almost_full will de-assert it's snk ready signal. We'll use
  -- that to flush the FIFO on the src side.
  u_data_log_fifo : entity dp_lib.dp_fifo_sc
  generic map (
    g_technology        => g_technology,
    g_data_w            => g_dat_w,
    g_use_ctrl          => false,
    g_fifo_size         => g_fifo_wr_depth
  )
  port map (
    rst      => rx_rst,
    clk      => rx_clk,

    snk_out  => data_log_fifo_siso,
    snk_in   => data_log_fifo_sosi,

    src_in   => data_flusher_siso,
    src_out  => data_flusher_sosi
  );

  -- dp_flush to flush data log FIFO when almost full.
  u_data_flush: entity dp_lib.dp_flush
  generic map (
    g_framed_xon    => false,
    g_framed_xoff   => false
  )
  port map (
    rst          => rx_rst,
    clk          => rx_clk,
    -- ST sink
    snk_in       => data_flusher_sosi,
    snk_out      => data_flusher_siso,
    -- ST source
    src_in       => ovr_data_dpmm_fifo_siso,
    src_out      => data_dpmm_fifo_sosi,
    -- Enable flush
    flush_en     => flush_en
  );

  flush_en <= not data_log_fifo_siso.ready;  -- We'll flush when the data log fifo is almost full.

  -- overridden siso.ready signal from dpmm_fifo to flusher: we do not want any data in our dpmm FIFO until we've stopped logging
  -- for mm readout. Otherwise our (shallow) dpmm FIFO will be filled to the max (and de-assert ready), then the data FIFO will fill
  -- to the max which causes the flusher to flush new data, while outdated data remains in the dpmm FIFO.
  ovr_data_dpmm_fifo_siso.ready <= '0' when log_en = '1' else data_dpmm_fifo_siso.ready;  -- not ready when logging is enabled
  ovr_data_dpmm_fifo_siso.xon   <= data_dpmm_fifo_siso.xon;  -- leave xon as it is

  u_data_dpmm_fifo : entity dp_lib.dp_fifo_dc_mixed_widths
  generic map (
    g_technology        => g_technology,
    g_wr_data_w         => g_dat_w,
    g_rd_data_w         => c_word_w,
    g_use_ctrl          => false,
    g_wr_fifo_size      => c_dpmm_fifo_wr_depth
  )
  port map (
    wr_rst   => rx_rst,
    wr_clk   => rx_clk,
    rd_rst   => mm_rst,
    rd_clk   => mm_clk,

    snk_out  => data_dpmm_fifo_siso,
    snk_in   => data_dpmm_fifo_sosi,

    wr_usedw => OPEN,
    rd_usedw => data_dpmm_fifo_rd_usedw,
    rd_emp   => OPEN,

    src_in   => data_mm_siso,
    src_out  => data_mm_sosi
  );

  u_data_fifo_to_mm : entity dp_lib.dp_fifo_to_mm
  generic map(
    g_fifo_size =>  c_data_dpmm_fifo_rd_depth
  )
  port map (
     rst       => mm_rst,
     clk       => mm_clk,

     snk_out   => data_mm_siso,
     snk_in    => data_mm_sosi,
     usedw     => data_dpmm_fifo_rd_usedw,  -- used words from the clock crossing FIFO (NOT the logging FIFO)

     mm_rd     => data_mm_rd,
     mm_rddata => data_mm_rd_data,
     mm_rdval  => data_mm_rd_val,
     mm_usedw  => data_mm_rd_usedw  -- used words resized to 32 bits
  );

  -- data read output to mm bus
  data_miso.rddata(c_word_w - 1 downto 0) <= data_mm_rd_data;
  data_miso.rdval                       <= data_mm_rd_val;
  data_mm_rd                            <= data_mosi.rd;

  -- ============== MM control ========================================================================

  u_ctrl_reg : entity work.mm_rx_logger_reg
  port map (
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,
    rx_rst                   => rx_rst,
    rx_clk                   => rx_clk,

    sla_in                   => ctrl_mosi,
    sla_out                  => ctrl_miso,

    rx_trig_on               => mm_trig_on,
    rx_trig_one_shot         => mm_trig_one_shot,
    rx_mm_ovr                => mm_ovr,
    rx_log_en_evt            => mm_trig_en_evt,
    rx_log_dis_evt           => mm_trig_dis_evt,
    rx_trig_nof_words        => mm_trig_nof_words,
    rx_trig_nof_logged_words => mm_trig_nof_logged_words,
    mm_data_usedw            => data_mm_rd_usedw
  );
end str;
