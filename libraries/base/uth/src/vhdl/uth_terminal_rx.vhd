--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Receive UTH frames from n = g_nof_input input streams via m =
--          g_nof_output SOSI data output streams.
-- Description:
-- Data flow:
--       snk--> dp_fifo_fill
--        in--> dp_uth_rx
--       pkt--> dp_distribute n --> m
--      dist--> dp_fifo_fill
--       out--> dp_packet_dec
--          -->src
-- Remark:
-- . The FIFO inside dp_distribute is not used, instead FIFOs directly at the
--   snk input can be instantiated.
-- . The input FIFO is typically not needed.
-- . The output FIFO is needed if the source applies backpressure at frame
--   level (e.g. to align frames). This then blocks pending frames from the
--   dp_distribute.
-- . The g_use_uth_err can be used to insert the UTH rx error bit (based on
--   the CRC) in the original DP packet err field at bit index g_uth_err_bi.

library IEEE, common_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.uth_pkg.all;
use technology_lib.technology_select_pkg.all;

entity uth_terminal_rx is
  generic (
    g_technology            : natural := c_tech_select_default;
    -- Terminal IO
    g_nof_input             : natural := 4;  -- >= 1
    g_nof_output            : natural := 3;  -- >= 1
    g_mux_mode              : natural := 0;  -- default use 0 for non-blocking mux in dp_distribute, use 1 to preserve input order
    -- . DP Packet
    g_data_w                : natural := 32;  -- DP data width
    g_packet_data_w         : natural := 32;  -- DP packet data width, must be >= g_data_w, same width is also used for the Uthernet packet data (fixed packing ratio 1/1)
    -- . Uthernet
    g_uth_nof_ch            : natural := 2;  -- selects UTH TLEN based on the snk_in channel_lo part that is not in the DP Packet CHAN field
    g_uth_len_max           : natural := 255;  -- uth_rx g_uth_len_max < uth_tx g_uth_typ_ofs
    g_uth_typ_arr           : t_natural_arr := array_init(256, 2, 1);  -- = (256, 257), array length must match g_uth_nof_ch
    g_uth_len_arr           : t_natural_arr := array_init(256, 10);  -- = (10, 10), array length must match g_uth_nof_ch
    g_use_uth_err           : boolean := true;  -- when TRUE insert the one bit CRC status from uth_rx in the received DP packet err field, else pass on the received DP packet err field unchanged
    g_uth_err_bi            : natural := 0;  -- bit index for where to insert the UTH err bit
    g_uth_timeout_w         : natural := 0;  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive uth_rx snk_in.valid
    -- Input FIFO passes DP/Uthernet packets
    g_input_use_fifo        : boolean := false;
    g_input_fifo_af_margin  : natural := c_fifo_afull_margin;  -- = 4, nof words below max (full) at which fifo is considered almost full
    g_input_fifo_fill       : natural := 0;
    g_input_fifo_size       : natural := c_bram_m9k_fifo_depth;  -- = 256
    -- Output FIFO passes DP packets (because the dp_packet_dec to get src_out_arr is placed after the output FIFO)
    g_output_use_fifo       : boolean := false;
    g_output_fifo_af_margin : natural := c_fifo_afull_margin;  -- = 4, nof words below max (full) at which fifo is considered almost full
    g_output_fifo_fill      : natural := 0;
    g_output_fifo_size      : natural := c_bram_m9k_fifo_depth  -- = 256
  );
  port (
    rst               : in  std_logic;
    clk               : in  std_logic;
    -- ST sinks
    snk_out_arr       : out t_dp_siso_arr(g_nof_input - 1 downto 0);
    snk_in_arr        : in  t_dp_sosi_arr(g_nof_input - 1 downto 0);
    -- ST source
    src_in_arr        : in  t_dp_siso_arr(g_nof_output - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr       : out t_dp_sosi_arr(g_nof_output - 1 downto 0);
    -- Monitor outputs
    mon_pkt_sosi_arr  : out t_dp_sosi_arr(g_nof_input - 1 downto 0);
    mon_dist_sosi_arr : out t_dp_sosi_arr(g_nof_output - 1 downto 0)
  );
end uth_terminal_rx;

architecture str of uth_terminal_rx is
  constant c_packet_channel_lo : natural := true_log2(g_uth_nof_ch);

  constant c_complex_w         : natural := g_data_w / 2;  -- DP complex data width of Im, Re packet as Im & Re in g_data_w

  signal in_siso_arr      : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal in_sosi_arr      : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  signal pkt_siso_arr     : t_dp_siso_arr(g_nof_input - 1 downto 0);
  signal pkt_sosi_arr     : t_dp_sosi_arr(g_nof_input - 1 downto 0);
  signal dist_siso_arr    : t_dp_siso_arr(g_nof_output - 1 downto 0);
  signal dist_sosi_arr    : t_dp_sosi_arr(g_nof_output - 1 downto 0);
  signal out_siso_arr     : t_dp_siso_arr(g_nof_output - 1 downto 0);
  signal out_sosi_arr     : t_dp_sosi_arr(g_nof_output - 1 downto 0);

  signal src_data_arr     : t_dp_sosi_arr(g_nof_output - 1 downto 0);
begin
  -- Make internal sosi available for debug monitoring
  mon_pkt_sosi_arr  <= pkt_sosi_arr;
  mon_dist_sosi_arr <= dist_sosi_arr;

  ------------------------------------------------------------------------------
  -- Input FIFO (g_input_use_fifo)
  ------------------------------------------------------------------------------

  no_input_fifo : if g_input_use_fifo = false generate
    snk_out_arr <= in_siso_arr;
    in_sosi_arr <= snk_in_arr;
  end generate;

  gen_input_fifo : for I in g_nof_input - 1 downto 0 generate
    gen_fifo : if g_input_use_fifo = true generate
      -- Input FIFO passes DP/Uthernet packets, so the sosi control fields (sync, bsn, empty, channel and error) are encoded in the packet data
      u_fifo_fill : entity dp_lib.dp_fifo_fill
      generic map (
        g_technology     => g_technology,
        g_data_w         => g_packet_data_w,
        g_fifo_fill      => g_input_fifo_fill,
        g_fifo_af_margin => g_input_fifo_af_margin,
        g_fifo_size      => g_input_fifo_size,
        g_fifo_rl        => 1
      )
      port map (
        rst       => rst,
        clk       => clk,
        -- ST sink
        snk_out   => snk_out_arr(I),
        snk_in    => snk_in_arr(I),
        -- ST source
        src_in    => in_siso_arr(I),
        src_out   => in_sosi_arr(I)
      );
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- UTHernet receiver
  ------------------------------------------------------------------------------

  gen_input : for I in g_nof_input - 1 downto 0 generate
    --u_uth_rx : ENTITY work.uth_rx(rtl_adapt)  -- requires using g_fifo_af_margin >= 10
    u_uth_rx : entity work.uth_rx(rtl_hold)  -- can use default g_fifo_af_margin >= 4
    generic map (
      g_data_w        => g_packet_data_w,
      g_len_max       => g_uth_len_max,
      g_nof_ch        => g_uth_nof_ch,
      g_typ_arr       => g_uth_typ_arr,
      g_len_arr       => g_uth_len_arr,
      g_use_this_siso => true,  -- default use TRUE for best throughput performance
      g_use_src_in    => true,  -- default use TRUE for src_in.ready flow control else use FALSE to avoid RL adapter when src_in.ready='1' fixed
      g_timeout_w     => g_uth_timeout_w
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sinks
      snk_out   => in_siso_arr(I),
      snk_in    => in_sosi_arr(I),
      -- ST source
      src_in    => pkt_siso_arr(I),
      src_out   => pkt_sosi_arr(I)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- DP distribute: g_nof_input --> g_nof_output
  ------------------------------------------------------------------------------

  u_distribute : entity dp_lib.dp_distribute
  generic map (
    g_technology      => g_technology,
    -- Distribution IO
    g_tx              => false,
    g_nof_input       => g_nof_input,
    g_nof_output      => g_nof_output,
    g_transpose       => false,
    g_code_channel_lo => true,
    g_data_w          => g_packet_data_w,
    -- Scheduling
    g_rx_mux_mode     => g_mux_mode,  -- default 0 for non-blocking mux, but 1 also works in simulation and on hardware provided that the inputs are garantueed to arrive
    -- Input FIFO
    g_use_fifo        => false
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sinks
    snk_out_arr => pkt_siso_arr,
    snk_in_arr  => pkt_sosi_arr,
    -- ST source
    src_in_arr  => dist_siso_arr,
    src_out_arr => dist_sosi_arr
  );

  ------------------------------------------------------------------------------
  -- Output FIFO (g_output_use_fifo)
  ------------------------------------------------------------------------------

  no_output_fifo : if g_output_use_fifo = false generate
    dist_siso_arr <= out_siso_arr;
    out_sosi_arr  <= dist_sosi_arr;
  end generate;

  gen_output_fifo : for I in g_nof_output - 1 downto 0 generate
    gen_fifo : if g_output_use_fifo = true generate
      -- Output FIFO passes DP packets, so the sosi control fields (sync, bsn, empty, channel and error) are encoded in the packet data.
      -- However if g_use_uth_err=TRUE then the Uthernet CRC error status is passed on via the sosi error field using 1 bit.
      u_fifo_fill : entity dp_lib.dp_fifo_fill
      generic map (
        g_technology     => g_technology,
        g_data_w         => g_packet_data_w,
        g_error_w        => c_uth_crc_err_w,  -- = 1, one bit CRC error status from uth_rx
        g_use_error      => g_use_uth_err,
        g_use_channel    => false,  -- FALSE because c_packet_channel_lo is coded in the CHAN field of the DP packet data
        g_fifo_fill      => g_output_fifo_fill,
        g_fifo_af_margin => g_output_fifo_af_margin,
        g_fifo_size      => g_output_fifo_size,
        g_fifo_rl        => 1
      )
      port map (
        rst       => rst,
        clk       => clk,
        -- ST sink
        snk_out   => dist_siso_arr(I),
        snk_in    => dist_sosi_arr(I),
        -- ST source
        src_in    => out_siso_arr(I),
        src_out   => out_sosi_arr(I)
      );
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- DP Packet decoder
  ------------------------------------------------------------------------------

  gen_output : for I in g_nof_output - 1 downto 0 generate
    u_dec : entity dp_lib.dp_packet_dec
    generic map (
      g_data_w        => g_packet_data_w,
      g_channel_lo    => c_packet_channel_lo,
      g_phy_err_bi    => g_uth_err_bi,
      g_use_phy_err   => g_use_uth_err,
      g_use_this_siso => true  -- default use TRUE for best throughput performance
    )
    port map (
      rst       => rst,
      clk       => clk,
      -- ST sinks
      snk_out   => out_siso_arr(I),
      snk_in    => out_sosi_arr(I),
      -- ST source
      src_in    => src_in_arr(I),
      src_out   => src_data_arr(I)
    );
  end generate;

  ------------------------------------------------------------------------------
  -- Unmap sosi.data = sosi.im & sosi.re
  ------------------------------------------------------------------------------

  p_src_out_arr : process(src_data_arr)
  begin
    src_out_arr <= src_data_arr;
    for I in g_nof_output - 1 downto 0 loop
      src_out_arr(I).im <= RESIZE_DP_DSP_DATA(src_data_arr(I).data(2 * c_complex_w - 1 downto c_complex_w));
      src_out_arr(I).re <= RESIZE_DP_DSP_DATA(src_data_arr(I).data(  c_complex_w - 1 downto 0));
    end loop;
  end process;
end str;
