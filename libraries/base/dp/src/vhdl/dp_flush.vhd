-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose: Flush snk_in when src_in will be not ready for a long time.
-- Description:
--   Provide a stream with an alternative sink when its normal end sink may
--   be not ready for such a long time that it affects the stream (e.g. fifo's
--   fill up too much, gaps occur). This component, when enabled, keeps the
--   input stream streaming by simply being always ready and discarding the
--   data (not forwarding the stream to its source output). When disabled, it
--   does not interfere with the stream in any way.
--
--   Four flush modes:
--   * Streaming mode (both g_framed_xon and g_framed_xoff are FALSE)
--     When flush_en goes low the output is disabled immediately, but when
--     flush_en goes high the output is enabled only after taking account of
--     the g_ready_latency to avoid that flushed data gets output as valid
--     data too early.
--   * Framed mode (both g_framed_xon and g_framed_xoff are TRUE)
--     When flush_en goes low then the output is disabled at the next
--     snk_in.sop, to ensure that an ongoing frame does not loose its tail.
--     When flush_en goes high then the output is enabled at the next
--     snk_in.sop to ensure that an ongoig frame is not passed on without its
--     head.
--   * Only g_framed_xon=TRUE
--     The output is disabled immediately, but enabled only at a snk_in.sop.
--   * Only g_framed_xoff=TRUE
--     The output is disabled at a snk_in.sop, but enabled based on the RL.
-- Remark:
-- . This dp_flush supports XON/XOFF control via MM flush_en as well as via the
--   downstream src_in.xon.
-- . The snk_out.xon is always '1' so the upsteam components can be always on,
--   because if necessary this component does flush
-- . If g_use_framed_channel = TRUE then the framed xon or xoff also requires
--   that the channel matches g_framed_channel. Otherwise the g_framed_channel
--   number is ignored and any sop will affect framed xon or xoff. Use the
--   actual needed width for g_framed_channel_w to ease timing closure.

entity dp_flush is
  generic (
    g_ready_latency      : natural := 1;  -- >= 0
    g_framed_xon         : boolean := true;
    g_framed_xoff        : boolean := false;
    g_use_framed_channel : boolean := false;
    g_framed_channel_w   : natural := c_dp_stream_channel_w;
    g_framed_channel     : natural := 0
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_in       : in  t_dp_sosi;
    snk_out      : out t_dp_siso;
    -- ST source
    src_in       : in  t_dp_siso;
    src_out      : out t_dp_sosi;
    -- Enable flush
    flush_en     : in  std_logic := '0'
  );
end dp_flush;

architecture rtl of dp_flush is
  signal flush_dly    : std_logic_vector(0 to g_ready_latency);  -- use 0 TO high for delay lines, rather than high DOWNTO 0
  signal snk_flush    : std_logic;
  signal snk_flush_hi : std_logic;
  signal snk_flush_lo : std_logic;
  signal src_en       : std_logic;
  signal src_en_hi    : std_logic;
  signal src_en_lo    : std_logic;
begin
  -- Combine MM control flush_en and streaming control src_in.xon
  flush_dly(0) <= flush_en or not src_in.xon;  -- use flush_dly(0) combinatorially, so that flush_dly supports all g_ready_latency >= 0
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      flush_dly(1 to g_ready_latency) <= (others => '0');
    elsif rising_edge(clk) then
      flush_dly(1 to g_ready_latency) <= flush_dly(0 to g_ready_latency - 1);
    end if;
  end process;

  -- Set snk_out.xon
  snk_out.xon   <= '1';  -- the upsteam components can be always on, because if necessary this component does flush

  -- Apply snk_flush to the snk_out.ready
  snk_out.ready <= src_in.ready or snk_flush;

  -- Apply src_en to the src_out control fields
  p_src_out : process(snk_in, src_en)
  begin
    src_out       <= snk_in;  -- default sosi
    src_out.valid <= snk_in.valid and src_en;  -- overrule sosi control
    src_out.sop   <= snk_in.sop   and src_en;
    src_out.eop   <= snk_in.eop   and src_en;
    src_out.sync  <= snk_in.sync  and src_en;
  end process;

  p_src_en : process(snk_in, flush_dly)
    variable v_hi : std_logic;
    variable v_lo : std_logic;
  begin
    -- for src_en there is no difference for RL = 0 or RL > 0
    -- default for streaming XON/XOFF control
    v_lo :=     flush_dly(0);  -- active flush disables the output immediately
    v_hi := not flush_dly(g_ready_latency);  -- inactive flush must account g_ready_latency before enabling the output again
    -- account for sop in case of framed XON or framed XOFF control
    if g_framed_xoff = true then
      v_lo := v_lo and snk_in.sop;  -- wait for sop to disable the output, to ensure that the output does not get disabled during a frame
      if g_use_framed_channel = true and unsigned(snk_in.channel(g_framed_channel_w - 1 downto 0)) /= g_framed_channel then
        v_lo := '0';
      end if;
    end if;
    if g_framed_xon = true then
      v_hi := v_hi and snk_in.sop;  -- wait for sop to enable  the output, to ensure that the output does not get enabled  during a frame
      if g_use_framed_channel = true and unsigned(snk_in.channel(g_framed_channel_w - 1 downto 0)) /= g_framed_channel then
        v_hi := '0';
      end if;
    end if;
    src_en_lo <= v_lo;
    src_en_hi <= v_hi;
  end process;

  u_src_en : entity common_lib.common_switch
  generic map (
    g_rst_level    => '1',
    g_priority_lo  => true,
    g_or_high      => true,
    g_and_low      => true
  )
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => src_en_hi,
    switch_low  => src_en_lo,
    out_level   => src_en
  );

  p_snk_flush : process(snk_in, flush_dly)
    variable v_hi : std_logic;
    variable v_lo : std_logic;
  begin
    -- for snk_flush there is a difference between RL = 0 and RL > 0, because for RL = 0 it must keep on flushing until there is a valid or even until there is a sop
    -- default for streaming XON/XOFF control
    v_hi :=     flush_dly(0);
    v_lo := not flush_dly(0);  -- equivalent snk_flush <= flush_dly(0)
    if g_ready_latency = 0 then
      v_lo := v_lo and snk_in.valid;  -- keep on flushing until their is a valid, then stop flushing to let src_in.ready take over the acknowledge flow control
    end if;
    -- account for sop in case of framed XON or framed XOFF control
    if g_framed_xoff = true then
      v_hi := v_hi and snk_in.sop;  -- wait for sop before start flushing the input, to ensure that src_in.ready controls snk_out.ready during the ongoing frame
      if g_use_framed_channel = true and unsigned(snk_in.channel(g_framed_channel_w - 1 downto 0)) /= g_framed_channel then
        v_hi := '0';
      end if;
    end if;
    if g_framed_xon = true then
      if g_ready_latency = 0 then
        v_lo := v_lo and snk_in.sop;  -- keep on flushing until their is a sop
        if g_use_framed_channel = true and unsigned(snk_in.channel(g_framed_channel_w - 1 downto 0)) /= g_framed_channel then
          v_lo := '0';
        end if;
      end if;
    end if;
    snk_flush_hi <= v_hi;
    snk_flush_lo <= v_lo;
  end process;

  u_snk_flush : entity common_lib.common_switch
  generic map (
    g_rst_level    => '1',
    g_priority_lo  => true,  -- priority does not matter
    g_or_high      => true,
    g_and_low      => true
  )
  port map (
    rst         => rst,
    clk         => clk,
    switch_high => snk_flush_hi,
    switch_low  => snk_flush_lo,
    out_level   => snk_flush
  );
end rtl;
