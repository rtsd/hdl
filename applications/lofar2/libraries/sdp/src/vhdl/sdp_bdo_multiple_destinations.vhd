-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose:
--   Construct beamformer data output (BDO) payloads for multiple destinations.
-- Description:
-- * The nof_destinations_max determines how to MM control the beamlet packet
--   destination(s) MAC/IP/UDP:
--   . nof_destinations_max = 1 then use sdp_bdo_one_destination.vhd and
--     MM program the one destination MAC/IP/UDP from dp_offload_tx_v3.
--   . nof_destinations_max > 1 then use sdp_bdo_multiple_destinations.vhd and
--     MM program the one or more destination MAC/IP/UDP via
--     sdp_bdo_destinations_reg.
-- * Get nof_destinations from sdp_bdo_destinations_reg. The nof_destinations
--   is MM programmable between 1 and nof_destinations_max. The
--   nof_destinations must be in range 1:g_nof_destinations_max, then
--   nof_destinations_act = nof_destinations, as defined in
--   sdp_bdo_destinations_reg.
-- * The nof_blocks_per_packet is not MM programmable, but based on DN =
--   nof_destinations to ensure that the packet fits in a jumbo frame or
--   equal to c_sdp_bdo_reorder_nof_blocks_max to make maximum use of the
--   available reorder buffer size.
-- * Merge, reorder and unmerge beamlet data for nof_destinations > 1 from:
--       (int8) [t] [nof_blocks_per_packet][S_sub_bf / nof_destinations] [N_pol_bf][N_complex]
--     to:
--       (int8) [t] [S_sub_bf / nof_destinations][nof_blocks_per_packet] [N_pol_bf][N_complex]
--
--   . where (int8) [N_pol_bf][N_complex] = c_sdp_W_dual_pol_beamlet = 32b
--     dual polarization beamlet word
--   . where nof_destinations packets together transport the S_sub_bf beamlets.
-- * Only support transposed beamlet data output, because each destination
--   must receive all blocks (= time samples) per beamlet.
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L4+SDPFW+Decision%3A+Multiple+beamlet+output+destinations
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, reorder_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use reorder_lib.reorder_pkg.all;
  use work.sdp_pkg.all;
  use work.sdp_bdo_pkg.all;

entity sdp_bdo_multiple_destinations is
  generic (
    g_nof_destinations_max : natural := 1;
    g_beamset_id           : natural := 0
  );
  port (
    mm_clk   : in  std_logic;
    mm_rst   : in  std_logic;

    dp_clk   : in  std_logic;
    dp_rst   : in  std_logic;

    -- MM register
    reg_destinations_copi : in  t_mem_copi;
    reg_destinations_cipo : out t_mem_cipo;

    destinations_info     : out t_sdp_bdo_destinations_info;

    -- Streaming data
    snk_in   : in  t_dp_sosi;
    src_out  : out t_dp_sosi;

    -- Streaming data output info dependent on DN = nof_destinations
    nof_blocks_per_packet                     : out natural;
    nof_beamlets_per_block_first_destinations : out natural;
    nof_beamlets_per_block_last_destination   : out natural
  );
end sdp_bdo_multiple_destinations;

architecture str of sdp_bdo_multiple_destinations is
  constant c_beamset_beamlet_index  : natural := g_beamset_id * c_sdp_S_sub_bf;

  -- Look up table constants as function of nof_destinations in range(g_nof_destinations_max)
  constant c_m                                             : natural := g_nof_destinations_max;
  constant c_reorder_nof_blocks_arr                        : t_natural_arr(1 to c_m) :=
             func_sdp_bdo_reorder_nof_blocks_look_up_table(c_m);
  constant c_reorder_nof_ch_arr                            : t_natural_arr(1 to c_m) :=
             func_sdp_bdo_reorder_nof_ch_look_up_table(c_m);
  constant c_nof_beamlets_per_block_first_destinations_arr : t_natural_arr(1 to c_m) :=
             func_sdp_bdo_nof_beamlets_per_block_first_destinations_look_up_table(c_m);
  constant c_nof_beamlets_per_block_last_destination_arr   : t_natural_arr(1 to c_m) :=
             func_sdp_bdo_nof_beamlets_per_block_last_destination_look_up_table(c_m);
  constant c_nof_ch_per_packet_first_destinations_arr      : t_natural_arr(1 to c_m) :=
             func_sdp_bdo_nof_ch_per_packet_first_destinations_look_up_table(c_m);
  constant c_nof_ch_per_packet_last_destination_arr        : t_natural_arr(1 to c_m) :=
             func_sdp_bdo_nof_ch_per_packet_last_destination_look_up_table(c_m);

  constant c_nof_ch_per_packet_max  : natural := largest(c_nof_ch_per_packet_first_destinations_arr);
  constant c_nof_ch_per_packet_w    : natural := ceil_log2(c_nof_ch_per_packet_max + 1);

  -- Reorder all c_sdp_nof_beamlets_per_block = c_sdp_S_sub_bf = 488 beamlets
  -- in buffer, such that they appear with first all blocks (= time samples)
  -- for beamlet index 0 and last all blocks for beamlet index 477.
  constant c_reorder_nof_beamlets_per_block       : natural := c_sdp_nof_beamlets_per_block;

  signal i_destinations_info : t_sdp_bdo_destinations_info;
  signal s_DN                : natural range 1 to g_nof_destinations_max;  -- number of destinations

  -- Dynamic merge, reorder, unmerge packet sizes
  signal reorder_nof_blocks                        : natural;
  signal reorder_nof_blocks_slv                    : std_logic_vector(c_sdp_bdo_reorder_nof_blocks_w - 1 downto 0);
  signal reorder_nof_ch                            : natural;
  signal nof_ch_per_packet_first_destinations      : natural;
  signal nof_ch_per_packet_first_destinations_slv  : std_logic_vector(c_nof_ch_per_packet_w - 1 downto 0);
  -- The nof_ch_per_packet_last_destination is used only for view in Wave
  -- window, because dp_packet_unmerge automatically will output that number
  -- of remaining number of ch for the last block.
  signal nof_ch_per_packet_last_destination        : natural;

  signal select_copi            : t_mem_copi := c_mem_copi_rst;
  signal select_cipo            : t_mem_cipo := c_mem_cipo_rst;
  signal r_transpose            : t_reorder_transpose;
  signal d_transpose            : t_reorder_transpose;

  signal merge_src_out          : t_dp_sosi;
  signal merge_word             : t_sdp_dual_pol_beamlet_in_word;
  signal reorder_src_out        : t_dp_sosi;
  signal reorder_word           : t_sdp_dual_pol_beamlet_in_word;
  signal reorder_busy           : std_logic;
  signal unmerge_src_out        : t_dp_sosi;
  signal unmerge_word           : t_sdp_dual_pol_beamlet_in_word;
begin
  destinations_info <= i_destinations_info;
  nof_blocks_per_packet <= reorder_nof_blocks;
  src_out <= unmerge_src_out;

  -----------------------------------------------------------------------------
  -- Multiple destinations info register
  -----------------------------------------------------------------------------
  -- Use dynamic sizes for beamlet data output to multiple destination.
  u_sdp_bdo_destinations_reg : entity work.sdp_bdo_destinations_reg
    generic map (
      g_nof_destinations_max => g_nof_destinations_max
    )
    port map (
      -- Clocks and reset
      mm_clk   => mm_clk,
      mm_rst   => mm_rst,

      dp_clk   => dp_clk,
      dp_rst   => dp_rst,

      reg_copi => reg_destinations_copi,
      reg_cipo => reg_destinations_cipo,

      -- sdp info
      destinations_info => i_destinations_info
    );

  -----------------------------------------------------------------------------
  -- Register look up table values dependent on DN to ease timing closure
  -----------------------------------------------------------------------------
  p_reg : process(dp_clk)
    variable v_DN : natural;  -- number of destinations
  begin
    if rising_edge(dp_clk) then
      v_DN := i_destinations_info.nof_destinations_act;

      -- Semi constant values that depend on DN = nof_destinations_act set via MM
      s_DN <= v_DN;
      reorder_nof_blocks                        <= c_reorder_nof_blocks_arr(s_DN);
      reorder_nof_ch                            <= c_reorder_nof_ch_arr(s_DN);
      nof_beamlets_per_block_first_destinations <= c_nof_beamlets_per_block_first_destinations_arr(s_DN);
      nof_beamlets_per_block_last_destination   <= c_nof_beamlets_per_block_last_destination_arr(s_DN);
      nof_ch_per_packet_first_destinations      <= c_nof_ch_per_packet_first_destinations_arr(s_DN);
      nof_ch_per_packet_last_destination        <= c_nof_ch_per_packet_last_destination_arr(s_DN);
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- dp_packet_merge
  -----------------------------------------------------------------------------
  -- Use slv in port map to avoid vcom-1436: Actual expression (function call
  -- "TO_UVEC") of formal "nof_pkt" is not globally static.
  reorder_nof_blocks_slv <= to_uvec(reorder_nof_blocks, c_sdp_bdo_reorder_nof_blocks_w);

  u_dp_packet_merge : entity dp_lib.dp_packet_merge
    generic map(
      g_use_ready     => false,  -- no flow control
      g_nof_pkt       => c_sdp_bdo_reorder_nof_blocks_max,
      g_bsn_increment => 1
    )
    port map(
      rst     => dp_rst,
      clk     => dp_clk,

      nof_pkt     => reorder_nof_blocks_slv,
      nof_pkt_out => open,  -- Valid at src_out.sop

      snk_in  => snk_in,
      src_out => merge_src_out
    );

  -- Debug signals for view in Wave window
  merge_word <= unpack_data(merge_src_out.data(c_sdp_W_dual_pol_beamlet - 1 downto 0));

  -----------------------------------------------------------------------------
  -- reorder_col_select
  -- . See tb_reorder_col_select_all.vhd for how to control col_select_copi /
  --   cipo with p_reorder_transpose.
  -- . Reorder nof_ch_sel = c_nof_ch_in = reorder_nof_ch, because the reorder
  --   outputs all input.
  -----------------------------------------------------------------------------
  u_reorder_col_select : entity reorder_lib.reorder_col_select
    generic map (
      g_dsp_data_w  => c_sdp_W_dual_pol_beamlet / c_nof_complex,  -- = 32b / 2
      g_nof_ch_in   => c_sdp_bdo_reorder_nof_ch_max,
      g_nof_ch_sel  => c_sdp_bdo_reorder_nof_ch_max,
      g_use_complex => false
    )
    port map (
      dp_rst          => dp_rst,
      dp_clk          => dp_clk,

      reorder_busy    => reorder_busy,

      -- Dynamic reorder block size control input
      nof_ch_in       => reorder_nof_ch,
      nof_ch_sel      => reorder_nof_ch,

      -- Captured reorder block size control used for output_sosi
      output_nof_ch_in  => open,
      output_nof_ch_sel => open,

      -- Memory Mapped
      col_select_mosi => select_copi,
      col_select_miso => select_cipo,

      -- Streaming
      input_sosi      => merge_src_out,
      output_sosi     => reorder_src_out
    );

  -- Debug signals for view in Wave window
  reorder_word <= unpack_data(reorder_src_out.data(c_sdp_W_dual_pol_beamlet - 1 downto 0));

  -- Use synchronous reset in d signals
  p_dp_clk : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      r_transpose <= d_transpose;
    end if;
  end process;

  -- Pass on beamlet data in transposed order
  select_copi <= r_transpose.select_copi;

  p_reorder_transpose : process(dp_rst, select_cipo, reorder_nof_blocks, r_transpose)
    variable v : t_reorder_transpose;
  begin
    if select_cipo.waitrequest = '0' then
      -- Read from reorder_col_select page
      v := func_reorder_transpose(reorder_nof_blocks, c_reorder_nof_beamlets_per_block, r_transpose);
    else
      -- No read, new reorder_col_select page not available yet
      v := c_reorder_transpose_rst;
    end if;
    -- Synchronous reset
    if dp_rst = '1' then
      v := c_reorder_transpose_rst;
    end if;
    d_transpose <= v;
  end process;

  -----------------------------------------------------------------------------
  -- dp_packet_unmerge for nof_destinations
  -----------------------------------------------------------------------------
  nof_ch_per_packet_first_destinations_slv <= to_uvec(nof_ch_per_packet_first_destinations, c_nof_ch_per_packet_w);

  u_dp_packet_unmerge : entity dp_lib.dp_packet_unmerge
    generic map (
      g_use_ready     => false,  -- no flow control
      g_nof_pkt       => g_nof_destinations_max,
      g_pkt_len       => c_nof_ch_per_packet_max,
      g_bsn_increment => 0
    )
    port map (
      rst     => dp_rst,
      clk     => dp_clk,

      pkt_len     => nof_ch_per_packet_first_destinations_slv,
      pkt_len_out => open,  -- Valid at src_out.sop

      snk_in      => reorder_src_out,
      src_out     => unmerge_src_out
    );

  -- Debug signals for view in Wave window
  unmerge_word <= unpack_data(unmerge_src_out.data(c_sdp_W_dual_pol_beamlet - 1 downto 0));
end str;
