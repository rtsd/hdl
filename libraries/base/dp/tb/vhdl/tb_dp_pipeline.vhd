-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_pipeline is
  generic (
    g_pipeline : natural := 5
  );
end tb_dp_pipeline;

architecture tb of tb_dp_pipeline is
  -- See tb_dp_pkg.vhd for explanation and run time

  -- DUT ready latency
  constant c_dut_latency    : natural := 1;  -- fixed 1 for dp_pipeline
  constant c_tx_latency     : natural := c_dut_latency;  -- TX ready latency of TB
  constant c_tx_void        : natural := sel_a_b(c_tx_latency, 1, 0);  -- used to avoid empty range VHDL warnings when c_tx_latency=0
  constant c_tx_offset_sop  : natural := 3;
  constant c_tx_period_sop  : natural := 7;  -- sop in data valid cycle 3,  10,  17, ...
  constant c_tx_offset_eop  : natural := 5;  -- eop in data valid cycle   5,  12,  19, ...
  constant c_tx_period_eop  : natural := c_tx_period_sop;
  constant c_tx_offset_sync : natural := 3;  -- sync in data valid cycle 3, 20, 37, ...
  constant c_tx_period_sync : natural := 17;
  constant c_rx_latency     : natural := c_dut_latency;  -- RX ready latency from DUT
  constant c_verify_en_wait : natural := 4 + g_pipeline;  -- wait some cycles before asserting verify enable

  constant c_random_w       : natural := 19;

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '0';
  signal rst            : std_logic;
  signal sync           : std_logic;
  signal lfsr1          : std_logic_vector(c_random_w - 1 downto 0) := (others => '0');
  signal lfsr2          : std_logic_vector(c_random_w   downto 0) := (others => '0');

  signal cnt_dat        : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal cnt_val        : std_logic;
  signal cnt_en         : std_logic;

  signal tx_data        : t_dp_data_arr(0 to c_tx_latency + c_tx_void)    := (others => (others => '0'));
  signal tx_val         : std_logic_vector(0 to c_tx_latency + c_tx_void) := (others => '0');

  signal in_ready       : std_logic;
  signal in_data        : std_logic_vector(c_dp_data_w - 1 downto 0) := (others => '0');
  signal in_sync        : std_logic;
  signal in_val         : std_logic;
  signal in_sop         : std_logic;
  signal in_eop         : std_logic;

  signal in_siso        : t_dp_siso;
  signal in_sosi        : t_dp_sosi := c_dp_sosi_rst;
  signal out_siso       : t_dp_siso;
  signal out_sosi       : t_dp_sosi;

  signal out_ready      : std_logic;
  signal prev_out_ready : std_logic_vector(0 to c_rx_latency);
  signal out_data       : std_logic_vector(c_dp_data_w - 1 downto 0);
  signal out_sync       : std_logic;
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_eop        : std_logic;
  signal hold_out_sop   : std_logic;
  signal prev_out_data  : std_logic_vector(out_data'range);

  signal state          : t_dp_state_enum;

  signal verify_en      : std_logic;
  signal verify_done    : std_logic;

  signal exp_data       : std_logic_vector(c_dp_data_w - 1 downto 0) := TO_UVEC(sel_a_b(g_pipeline = 0, 18953, 18952), c_dp_data_w);
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  -- Sync interval
  proc_dp_sync_interval(clk, sync);

  -- Input data
  cnt_val <= in_ready and cnt_en;

  proc_dp_cnt_dat(rst, clk, cnt_val, cnt_dat);
  proc_dp_tx_data(c_tx_latency, rst, clk, cnt_val, cnt_dat, tx_data, tx_val, in_data, in_val);
  proc_dp_tx_ctrl(c_tx_offset_sync, c_tx_period_sync, in_data, in_val, in_sync);
  proc_dp_tx_ctrl(c_tx_offset_sop, c_tx_period_sop, in_data, in_val, in_sop);
  proc_dp_tx_ctrl(c_tx_offset_eop, c_tx_period_eop, in_data, in_val, in_eop);

  -- Stimuli control
  proc_dp_count_en(rst, clk, sync, lfsr1, state, verify_done, tb_end, cnt_en);
  proc_dp_out_ready(rst, clk, sync, lfsr2, out_ready);

  -- Output verify
  proc_dp_verify_en(c_verify_en_wait, rst, clk, sync, verify_en);
  proc_dp_verify_data("out_sosi.data", c_rx_latency, clk, verify_en, out_ready, out_val, out_data, prev_out_data);
  proc_dp_verify_valid(c_rx_latency, clk, verify_en, out_ready, prev_out_ready, out_val);
  proc_dp_verify_sop_and_eop(c_rx_latency, false, clk, out_val, out_val, out_sop, out_eop, hold_out_sop);  -- Verify that sop and eop come in pairs, no check on valid between eop and sop
  proc_dp_verify_ctrl(c_tx_offset_sync, c_tx_period_sync, "sync", clk, verify_en, out_data, out_val, out_sync);
  proc_dp_verify_ctrl(c_tx_offset_sop, c_tx_period_sop, "sop", clk, verify_en, out_data, out_val, out_sop);
  proc_dp_verify_ctrl(c_tx_offset_eop, c_tx_period_eop, "eop", clk, verify_en, out_data, out_val, out_eop);

  -- Check that the test has ran at all
  proc_dp_verify_value(e_equal, clk, verify_done, exp_data, out_data);

  ------------------------------------------------------------------------------
  -- DUT dp_pipeline
  ------------------------------------------------------------------------------

  -- map sl, slv to record
  in_ready <= in_siso.ready;  -- SISO
  in_sosi.data(c_dp_data_w - 1 downto 0) <= in_data;  -- SOSI
  in_sosi.sync                         <= in_sync;
  in_sosi.valid                        <= in_val;
  in_sosi.sop                          <= in_sop;
  in_sosi.eop                          <= in_eop;

  out_siso.ready <= out_ready;  -- SISO
  out_data <= out_sosi.data(c_dp_data_w - 1 downto 0);  -- SOSI
  out_sync <= out_sosi.sync;
  out_val  <= out_sosi.valid;
  out_sop  <= out_sosi.sop;
  out_eop  <= out_sosi.eop;

  dut : entity work.dp_pipeline
  generic map (
    g_pipeline => g_pipeline
  )
  port map (
    rst         => rst,
    clk         => clk,
    snk_out     => in_siso,  -- OUT = request to upstream ST source
    snk_in      => in_sosi,
    src_in      => out_siso,  -- IN  = request from downstream ST sink
    src_out     => out_sosi
  );
end tb;
