-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

-- Purpose: Assign one of g_nof_in input streams to the output based on in_sel input
-- Description: The input streams are concatenated into one SLV.
-- Remarks:

entity common_multiplexer is
  generic (
    g_pipeline_in  : natural := 0;
    g_pipeline_out : natural := 0;
    g_nof_in       : natural;
    g_dat_w        : natural
 );
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;

    in_sel      : in  std_logic_vector(ceil_log2(g_nof_in) - 1 downto 0);
    in_dat      : in  std_logic_vector(g_nof_in * g_dat_w - 1 downto 0);
    in_val      : in  std_logic;

    out_dat     : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val     : out std_logic
  );
end;

architecture str of common_multiplexer is
begin
  u_select_symbol : entity work.common_select_symbol
  generic map (
    g_pipeline_in  => g_pipeline_in,
    g_pipeline_out => g_pipeline_out,
    g_nof_symbols  => g_nof_in,
    g_symbol_w     => g_dat_w,
    g_sel_w        => ceil_log2(g_nof_in)
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => in_dat,
    in_val     => in_val,

    in_sel     => in_sel,

    out_symbol => out_dat,
    out_val    => out_val
  );
end str;
