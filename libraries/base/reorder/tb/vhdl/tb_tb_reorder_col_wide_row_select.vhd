-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Test multiple instances of tb_reorder_col_wide_row_select
-- Usage:
-- > as 10
-- > run -all
--
-- Description: See tb_reorder_col_wide_row_select

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_reorder_col_wide_row_select is
end tb_tb_reorder_col_wide_row_select;

architecture tb of tb_tb_reorder_col_wide_row_select is
  constant c_nof_sync       : natural := 3;
  constant c_dsp_data_w     : natural := 16;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Generics:
  --  g_nof_inputs                   : NATURAL := 6; -- also nof rows
  --  g_nof_outputs                  : NATURAL := 2;
  --  g_dsp_data_w                   : NATURAL := 16;
  --  g_nof_sync                     : NATURAL := 5;
  --  g_nof_block_per_sync           : NATURAL := 4;
  --  g_nof_ch_in                    : NATURAL := 1024; -- nof input words per block, identical for all input streams.
  --  g_nof_ch_sel_col               : NATURAL := 2; -- nof of sequential collums to select per row.
  --  g_nof_ch_sel_offset            : NATURAL := 2; -- nof offsets defined
  --  g_ch_sel_offsets               : t_natural_arr := (0, 16);
  --  g_ch_sel_step                  : NATURAL := 3; -- offset step size to increase per sync interval
  --  g_reorder_row_select_pipe_in   : NATURAL := 1;
  --  g_reorder_row_select_pipe_in_m : NATURAL := 1;
  --  g_reorder_row_select_pipe_out  : NATURAL := 1

  u_sdp          : entity work.tb_reorder_col_wide_row_select generic map (6, 1, c_dsp_data_w, c_nof_sync, 4, 1024, 2, 2, (0, 16), 3, 0, 1, 1);
  u_max_out      : entity work.tb_reorder_col_wide_row_select generic map (8, 1, c_dsp_data_w, c_nof_sync, 1, 512, 8, 8, (0, 64, 128, 192, 256, 320, 384, 448), 0, 0, 1, 1);  -- rows * cols * offsets = 8*8*8 = 512
  u_multiple_out : entity work.tb_reorder_col_wide_row_select generic map (2, 5, c_dsp_data_w, c_nof_sync, 4, 1024, 2, 2, (0, 16), 3, 0, 1, 1);
end tb;
