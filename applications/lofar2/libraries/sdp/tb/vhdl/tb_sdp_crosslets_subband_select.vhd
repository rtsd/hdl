-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose: Verify sdp_crosslets_subband_select.
-- Usage:
-- > as 10
-- > run -all
-- * The tb is self stopping and self checking,tb_end will stop the simulation by
--   stopping the clk and thus all toggling.
--
-- Description: The tb starts the dut by writing a scheduled bsn to the bsn_scheduler
-- via MM. The offsets and step are configured using MM. The dut makes the subband
-- selection based on the MM configuration and N_crosslets. The TB then verifies out_sosi
-- and cur_crosslets_info of the dut by comparing it to the expected output.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.sdp_pkg.all;

entity tb_sdp_crosslets_subband_select is
end tb_sdp_crosslets_subband_select;

architecture tb of tb_sdp_crosslets_subband_select is
  constant c_clk_period           : time := 10 ns;
  constant c_mm_clk_period        : time := 20 ns;
  constant c_rl                   : natural := 1;
  constant c_nof_sync             : natural := 5;
  constant c_nof_block_per_sync   : natural := 4;
  constant c_dsp_data_w           : natural := c_sdp_W_subband;

  constant c_N_crosslets          : natural := 2;
  constant c_crosslet_offsets     : t_natural_arr(0 to c_N_crosslets - 1) := (0, 15);
  constant c_crosslet_step        : natural := 3;  -- offset step size to increase per sync interval

  constant c_nof_ch_in            : natural := 1024;  -- nof input words per block, identical for all input streams.
  constant c_nof_ch_sel_row       : natural := c_sdp_P_pfb;
  constant c_nof_ch_sel_col       : natural := c_sdp_Q_fft;  -- nof of sequential columns to select per row.
  constant c_nof_ch_sel           : natural := c_N_crosslets * c_nof_ch_sel_col * c_nof_ch_sel_row;

  constant c_ctrl_interval_size   : natural := c_nof_block_per_sync * c_nof_ch_in;
  constant c_scheduled_bsn        : natural := 11;
  constant c_nof_block_dly        : natural := c_nof_block_per_sync;

  signal tb_end             : std_logic;
  signal rst                : std_logic;
  signal clk                : std_logic := '1';
  signal mm_clk             : std_logic := '1';

  signal mm_mosi             : t_mem_mosi := c_mem_mosi_rst;
  signal mm_miso             : t_mem_miso;
  signal rd_crosslet_offsets : t_natural_arr(0 to c_N_crosslets - 1) := (0, 15);
  signal rd_crosslet_step    : natural;

  signal mm_trigger_mosi    : t_mem_mosi := c_mem_mosi_rst;
  signal mm_trigger_miso    : t_mem_miso;

  signal st_en              : std_logic := '1';
  signal st_bsn             : natural := c_scheduled_bsn - c_nof_block_dly;
  signal st_siso_arr        : t_dp_siso_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_siso_rdy);
  signal st_sosi_arr        : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);
  signal exp_sosi           : t_dp_sosi := c_dp_sosi_rst;

  signal in_sosi_arr        : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);

  signal out_sosi           : t_dp_sosi;

  signal exp_cur_crosslets_info_rec   : t_sdp_crosslets_info;
  signal cur_crosslets_info_rec       : t_sdp_crosslets_info;
  signal exp_prev_crosslets_info_rec  : t_sdp_crosslets_info;
  signal prev_crosslets_info_rec      : t_sdp_crosslets_info;
begin
  clk <= (not clk) or tb_end after c_clk_period / 2;
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  p_mm_stimuli : process
    variable k : natural;
  begin
    proc_common_wait_until_low(mm_clk, rst);
    proc_common_wait_some_cycles(mm_clk, 50);  -- Give dut some time to start

    -- Set BSN sync scheduler
    proc_mem_mm_bus_wr(1, c_ctrl_interval_size, mm_clk, mm_trigger_miso, mm_trigger_mosi);
    proc_mem_mm_bus_wr(2, c_scheduled_bsn, mm_clk, mm_trigger_miso, mm_trigger_mosi);
    proc_mem_mm_bus_wr(3, 0, mm_clk, mm_trigger_miso, mm_trigger_mosi);
    proc_mem_mm_bus_wr(0, 1, mm_clk, mm_trigger_miso, mm_trigger_mosi);  -- enable

    -- Set crosslet info
    for I in 0 to c_N_crosslets - 1 loop
      proc_mem_mm_bus_wr(I, c_crosslet_offsets(I), mm_clk, mm_miso, mm_mosi);  -- offsets
    end loop;
    proc_mem_mm_bus_wr(15, c_crosslet_step, mm_clk, mm_miso, mm_mosi);  -- step
    proc_common_wait_cross_clock_domain_latency(c_clk_period, c_mm_clk_period);

    -- Verify that MM reads the active crosslets_info
    -- a) Readback crosslet info after rst release
    for I in 0 to c_N_crosslets - 1 loop
      proc_mem_mm_bus_rd(I, mm_clk, mm_miso, mm_mosi);  -- offsets
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      rd_crosslet_offsets(I) <= TO_UINT(mm_miso.rddata(c_word_w - 1 downto 0));
    end loop;
    proc_mem_mm_bus_rd(15, mm_clk, mm_miso, mm_mosi);
    proc_mem_mm_bus_rd_latency(1, mm_clk);
    rd_crosslet_step <= TO_UINT(mm_miso.rddata(c_word_w - 1 downto 0));
    proc_common_wait_some_cycles(mm_clk, 1);
    -- Verify that readback crosslet info is active crosslets_info
    for I in 0 to c_N_crosslets - 1 loop
      assert rd_crosslet_offsets(I) = 0
        report "Wrong crosslet offset after rst."
        severity ERROR;
    end loop;
    assert rd_crosslet_step = 0
      report "Wrong crosslet step after rst."
      severity ERROR;

    -- b) Read crosslets_info in every sync interval
    while true loop
      proc_common_wait_until_hi_lo(clk, out_sosi.sync);
      proc_common_wait_cross_clock_domain_latency(c_clk_period, c_mm_clk_period);
      -- Readback crosslet info
      for I in 0 to c_N_crosslets - 1 loop
        proc_mem_mm_bus_rd(I, mm_clk, mm_miso, mm_mosi);  -- offsets
        proc_mem_mm_bus_rd_latency(1, mm_clk);
        rd_crosslet_offsets(I) <= TO_UINT(mm_miso.rddata(c_word_w - 1 downto 0));
      end loop;
      proc_mem_mm_bus_rd(15, mm_clk, mm_miso, mm_mosi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      rd_crosslet_step <= TO_UINT(mm_miso.rddata(c_word_w - 1 downto 0));
      proc_common_wait_some_cycles(mm_clk, 1);
      -- Verify that readback crosslet info is active crosslets_info
      for I in 0 to c_N_crosslets - 1 loop
        assert rd_crosslet_offsets(I) = exp_cur_crosslets_info_rec.offset_arr(I)
          report "Wrong active crosslet offset in output sync interval."
          severity ERROR;
      end loop;
      assert rd_crosslet_step = exp_cur_crosslets_info_rec.step
        report "Wrong active crosslet step in output sync interval."
        severity ERROR;
    end loop;

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Data blocks
  ------------------------------------------------------------------------------
  gen_stimuli : for K in 0 to c_sdp_P_pfb - 1 generate
    p_st_stimuli : process
      variable v_re  : natural := 0 + k * 2**5;
      variable v_im  : natural := 1 + k * 2**5;
    begin
      tb_end <= '0';
      st_sosi_arr(K) <= c_dp_sosi_rst;
      proc_common_wait_until_low(clk, rst);

      -- Run some sync intervals with DSP counter data for the real and imag fields
      wait until rising_edge(clk);
      for I in 0 to c_nof_sync - 1 loop
        proc_dp_gen_block_data(c_rl, false, c_dsp_data_w, c_dsp_data_w, 0, v_re, v_im,
                               c_nof_ch_in, 0, 0, '1', "0", clk, st_en, st_siso_arr(K), st_sosi_arr(K));  -- next sync
        v_re := v_re + c_nof_ch_in;
        v_im := v_im + c_nof_ch_in;
        for J in 0 to c_nof_block_per_sync - 2 loop  -- provide sop and eop for block reference
          proc_dp_gen_block_data(c_rl, false, c_dsp_data_w, c_dsp_data_w, 0, v_re, v_im,
                                 c_nof_ch_in, 0, 0, '0', "0", clk, st_en, st_siso_arr(K), st_sosi_arr(K));  -- no sync
          v_re := v_re + c_nof_ch_in;
          v_im := v_im + c_nof_ch_in;
        end loop;
      end loop;
      st_sosi_arr(K) <= c_dp_sosi_rst;
      proc_common_wait_some_cycles(clk, c_nof_ch_in);
      proc_common_wait_some_cycles(clk, 10);
      tb_end <= '1';
      wait;
    end process;
  end generate;

  -- Time stimuli
  st_bsn <= st_bsn + 1 when rising_edge(clk) and (st_sosi_arr(0).eop = '1');

  -- Add BSN to the ST data
  p_in_sosi : process(st_sosi_arr, st_bsn)
  begin
    in_sosi_arr <= st_sosi_arr;
    for I in 0 to c_sdp_P_pfb - 1 loop
      in_sosi_arr(I).bsn <= TO_DP_BSN(st_bsn);
    end loop;
  end process;

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------
  p_generate_exp_data : process
    variable v_col : natural := 0;
    variable v_row : natural := 0;
    variable v_offset : natural := 0;
    variable v_sync_ix : natural := 0;  -- ix = index
    variable v_k : natural := 0;
  begin
    for I in 0 to c_nof_sync * c_nof_block_per_sync - 1 loop
      v_sync_ix := I / c_nof_block_per_sync;
      exp_sosi <= c_dp_sosi_rst;

      -- In a tb it is ok and it can be handy to use rising_edge() on a combinatorial signal
      wait until rising_edge(out_sosi.sop);

      -- Current sync interval crosslets_info
      exp_cur_crosslets_info_rec.step <= c_crosslet_step;
      for C in 0 to c_nof_ch_sel_col - 1 loop
        exp_cur_crosslets_info_rec.offset_arr(C) <= c_crosslet_offsets(C) + v_sync_ix * c_crosslet_step;
      end loop;

      -- Previous sync interval crosslets_info
      if out_sosi.sync = '1' then
        exp_prev_crosslets_info_rec <= exp_cur_crosslets_info_rec;
      end if;

      -- Crosslet data
      for J in 0 to c_nof_ch_sel - 1 loop
        v_offset := J / (c_nof_ch_sel_col * c_nof_ch_sel_row);
        v_col := J mod c_nof_ch_sel_col;
        v_row := (J / c_nof_ch_sel_col) mod c_nof_ch_sel_row;
        v_k := c_nof_ch_sel_col * v_sync_ix * c_crosslet_step;

        exp_sosi <= c_dp_sosi_rst;
        exp_sosi.valid <= '1';
        if J = 0 then
          exp_sosi.sop <= '1';
          if I mod c_nof_block_per_sync = 0 then
            exp_sosi.sync <= '1';
          end if;
        elsif j = c_nof_ch_sel - 1 then
          exp_sosi.eop <= '1';
        end if;

        exp_sosi.re <= RESIZE_DP_DSP_DATA(TO_DP_DSP_DATA((I + c_nof_block_dly) * c_nof_ch_in +
                                                         v_k +
                                                         c_nof_ch_sel_col * c_crosslet_offsets(v_offset) +
                                                         v_col +
                                                         v_row * 2**5)(c_sdp_W_crosslet - 1 downto 0));
        exp_sosi.im <= RESIZE_DP_DSP_DATA(TO_DP_DSP_DATA(1 +
                                                         (I + c_nof_block_dly) * c_nof_ch_in +
                                                         v_k +
                                                         c_nof_ch_sel_col * c_crosslet_offsets(v_offset) +
                                                         v_col +
                                                         v_row * 2**5)(c_sdp_W_crosslet - 1 downto 0));
        proc_common_wait_some_cycles(clk, 1);
      end loop;

      exp_sosi <= c_dp_sosi_rst;
    end loop;
    wait;
  end process;

  p_verify_out_data : process(clk)
  begin
    if rising_edge(clk) then
      assert out_sosi.valid = exp_sosi.valid
        report "Wrong out_sosi.valid"
        severity ERROR;
      assert out_sosi.sop = exp_sosi.sop
        report "Wrong out_sosi.sop"
        severity ERROR;
      assert out_sosi.eop = exp_sosi.eop
        report "Wrong out_sosi.eop"
        severity ERROR;
      assert out_sosi.sync = exp_sosi.sync
        report "Wrong out_sosi.sync"
        severity ERROR;

      assert cur_crosslets_info_rec = exp_cur_crosslets_info_rec
        report "Wrong cur_crosslets_info"
        severity ERROR;
      assert prev_crosslets_info_rec = exp_prev_crosslets_info_rec
        report "Wrong prev_crosslets_info"
        severity ERROR;

      if exp_sosi.valid = '1' then
        assert out_sosi.re = exp_sosi.re
          report "Wrong out_sosi.re"
          severity ERROR;
        assert out_sosi.im = exp_sosi.im
          report "Wrong out_sosi.im"
          severity ERROR;
      end if;
    end if;
  end process;

  u_dut : entity work.sdp_crosslets_subband_select
  generic map (
    g_N_crosslets  => c_N_crosslets,
    g_ctrl_interval_size_min => 1
  )
  port map (
    dp_rst         => rst,
    dp_clk         => clk,

    mm_rst         => rst,
    mm_clk         => mm_clk,

    reg_crosslets_info_mosi => mm_mosi,
    reg_crosslets_info_miso => mm_miso,

    reg_bsn_sync_scheduler_xsub_mosi  => mm_trigger_mosi,
    reg_bsn_sync_scheduler_xsub_miso  => mm_trigger_miso,

    -- Streaming
    in_sosi_arr => in_sosi_arr,
    out_sosi    => out_sosi,

    cur_crosslets_info_rec  => cur_crosslets_info_rec,
    prev_crosslets_info_rec => prev_crosslets_info_rec
  );
end tb;
