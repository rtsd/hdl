-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, tech_mult_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use technology_lib.technology_select_pkg.all;

--
-- Function: Signed complex multiply
--   p = a * b       when g_conjugate_b = FALSE
--     = (ar + j ai) * (br + j bi)
--     =  ar*br - ai*bi + j ( ar*bi + ai*br)
--
--   p = a * conj(b) when g_conjugate_b = TRUE
--     = (ar + j ai) * (br - j bi)
--     =  ar*br + ai*bi + j (-ar*bi + ai*br)
--
-- Architectures:
-- . rtl          : uses RTL to have all registers in one clocked process
-- . str          : uses two RTL instances of common_mult_add2 for out_pr and out_pi
-- . str_stratix4 : uses two Stratix4 instances of common_mult_add2 for out_pr and out_pi
-- . stratix4     : uses MegaWizard component from common_complex_mult(stratix4).vhd
-- . rtl_dsp      : uses RTL with one process (as in Altera example)
-- . altera_rtl   : uses RTL with one process (as in Altera example, by Raj R. Thilak)
--
-- Preferred architecture: 'str', see synth\quartus\common_top.vhd

entity common_complex_mult is
  generic (
    g_technology       : natural  := c_tech_select_default;
    g_variant          : string := "IP";
    g_in_a_w           : positive;
    g_in_b_w           : positive;
    g_out_p_w          : positive;  -- default use g_out_p_w = g_in_a_w+g_in_b_w = c_prod_w
    g_conjugate_b      : boolean := false;
    g_pipeline_input   : natural := 1;  -- 0 or 1
    g_pipeline_product : natural := 0;  -- 0 or 1
    g_pipeline_adder   : natural := 1;  -- 0 or 1
    g_pipeline_output  : natural := 1  -- >= 0
  );
  port (
    rst        : in   std_logic := '0';
    clk        : in   std_logic;
    clken      : in   std_logic := '1';
    in_ar      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_ai      : in   std_logic_vector(g_in_a_w - 1 downto 0);
    in_br      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_bi      : in   std_logic_vector(g_in_b_w - 1 downto 0);
    in_val     : in   std_logic := '1';
    out_pr     : out  std_logic_vector(g_out_p_w - 1 downto 0);
    out_pi     : out  std_logic_vector(g_out_p_w - 1 downto 0);
    out_val    : out  std_logic
  );
end common_complex_mult;

architecture str of common_complex_mult is
  constant c_pipeline        : natural := g_pipeline_input + g_pipeline_product + g_pipeline_adder + g_pipeline_output;

  -- MegaWizard IP ip_stratixiv_complex_mult was generated with latency c_dsp_latency = 3
  constant c_dsp_latency     : natural := 3;

  -- Extra output pipelining is only needed when c_pipeline > c_dsp_latency
  constant c_pipeline_output : natural := sel_a_b(c_pipeline > c_dsp_latency, c_pipeline - c_dsp_latency, 0);

  signal result_re : std_logic_vector(g_out_p_w - 1 downto 0);
  signal result_im : std_logic_vector(g_out_p_w - 1 downto 0);
begin
  -- User specificied latency must be >= MegaWizard IP dsp_mult_add2 latency
  assert c_pipeline >= c_dsp_latency
    report "tech_complex_mult: pipeline value not supported"
    severity FAILURE;

  -- Propagate in_val with c_pipeline latency
  u_out_val : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline  => c_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => clken,
    in_dat  => in_val,
    out_dat => out_val
  );

  u_complex_mult : entity tech_mult_lib.tech_complex_mult
  generic map(
    g_technology       => g_technology,
    g_variant          => g_variant,
    g_in_a_w           => g_in_a_w,
    g_in_b_w           => g_in_b_w,
    g_out_p_w          => g_out_p_w,
    g_conjugate_b      => g_conjugate_b,
    g_pipeline_input   => g_pipeline_input,
    g_pipeline_product => g_pipeline_product,
    g_pipeline_adder   => g_pipeline_adder,
    g_pipeline_output  => g_pipeline_output
  )
  port map(
    rst        => rst,
    clk        => clk,
    clken      => clken,
    in_ar      => in_ar,
    in_ai      => in_ai,
    in_br      => in_br,
    in_bi      => in_bi,
    result_re  => result_re,
    result_im  => result_im
  );

  ------------------------------------------------------------------------------
  -- Extra output pipelining
  ------------------------------------------------------------------------------

  u_output_re_pipe : entity common_lib.common_pipeline  -- pipeline output
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline_output,
    g_in_dat_w       => g_out_p_w,
    g_out_dat_w      => g_out_p_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => std_logic_vector(result_re),
    out_dat => out_pr
  );

  u_output_im_pipe : entity common_lib.common_pipeline  -- pipeline output
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline_output,
    g_in_dat_w       => g_out_p_w,
    g_out_dat_w      => g_out_p_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => std_logic_vector(result_im),
    out_dat => out_pi
  );
end str;
