--------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
-- . Merge nof_pkt input packets into one output packet.
-- Description:
-- . Merge nof_pkt snk_in blocks per src_out block. If nof_pkt = 0 then there
--   is no output. If nof_pkt = 1 then the output is the same as the input.
-- . The output is the same as the registered input, except for the SOP and EOP
--   signal; these are overridden to create output packets as long as multiple
--   (g_nof_pkt) input packets.
-- . Uses bsn of first snk_in block as bsn for merged output src_out block.
-- . The snk_in.err fields of the input blocks are OR-ed to reflect combined
--   error status of the merged blocks in src_out.err of the output block.
-- . When g_bsn_increment = 0 then the snk_in.bsn is not checked.
-- . When g_bsn_increment > 0 then the src_out.err[g_bsn_err_bi] = '1', if
--   the snk_in.bsn did not increment by g_bsn_increment for the merged snk_in
--   blocks in the src_out block. If the bsn increment for the merged blocks is
--   correct, then src_out.err[g_bsn_err_bi] = '0' to indicate that the
--   merged blocks are consecutive, so no input block got lost for this merge.
--
-- Remarks:
-- . g_nof_pkt statically sets the number of packets to merge into one in absence of
--   dynamic nof_pkt control. When the nof_pkt control input is used, g_nof_pkt sets
--   the maximum number of packets that can be merged.
-- . the nof_pkt control input is read on completion of the current block. The current
--   working value is kept in r (t_reg).
-- . with g_align_at_sync=true the merge can be forced to restart at a snk_in.sync.
-- . Optional flow control dependent on g_use_ready and g_pipeline_ready, see
--   description of dp_add_flow_control.
-- . The input packets do not have to have equal length, because they are
--   merged based on counting their eop.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_packet_merge is
  generic (
    g_use_ready       : boolean := true;
    g_pipeline_ready  : boolean := false;
    g_nof_pkt         : natural;
    g_align_at_sync   : boolean := false;
    g_bsn_increment   : natural := 0;
    g_bsn_err_bi      : natural := 0  -- bit index (bi) in scr_out.err for snk_in.bsn error
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    nof_pkt     : in  std_logic_vector(ceil_log2(g_nof_pkt + 1) - 1 downto 0) := to_uvec(g_nof_pkt, ceil_log2(g_nof_pkt + 1));
    nof_pkt_out : out std_logic_vector(ceil_log2(g_nof_pkt + 1) - 1 downto 0);  -- Valid at src_out.sop

    snk_out     : out t_dp_siso;
    snk_in      : in  t_dp_sosi;

    src_in      : in  t_dp_siso := c_dp_siso_rdy;
    src_out     : out t_dp_sosi
  );
end dp_packet_merge;

architecture rtl of dp_packet_merge is
  constant c_nof_pkt_max : natural := g_nof_pkt + 1;
  constant c_nof_pkt_w   : natural := ceil_log2(c_nof_pkt_max);

  type t_reg is record
    nof_pkt     : natural range 0 to c_nof_pkt_max;
    pkt_cnt     : natural range 0 to c_nof_pkt_max;
    align_cnt   : natural range 0 to c_nof_pkt_max;
    busy        : std_logic;
    sync        : std_logic;
    next_bsn    : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
    bsn_err     : std_logic;
    src_out     : t_dp_sosi;
  end record;

  constant c_reg_rst : t_reg := (0, 0, 0, '0', '0', (others => '0'), '0', c_dp_sosi_rst);

  signal r, nxt_r : t_reg;
begin
  -- Map logic function outputs to entity outputs
  nof_pkt_out <= to_uvec(r.nof_pkt, c_nof_pkt_w);

  u_dp_add_flow_control : entity work.dp_add_flow_control
    generic map (
      g_use_ready       => g_use_ready,
      g_pipeline_ready  => g_pipeline_ready
    )
    port map (
      rst         => rst,
      clk         => clk,

      snk_out     => snk_out,
      snk_in      => nxt_r.src_out,  -- combinatorial input
      snk_in_reg  => r.src_out,  -- registered input

      src_in      => src_in,
      src_out     => src_out
    );

  -- p_reg function state register
  r <= nxt_r when rising_edge(clk);

  -- Logic function
  p_comb : process(rst, r, snk_in, nof_pkt)
    variable v : t_reg;
  begin
    ----------------------------------------------------------------------------
    -- Default
    v := r;

    ----------------------------------------------------------------------------
    -- Function: merge input packets into one output packet
    --                       _         _         _         _         _         _            _         _         _
    --   snk_in.sop    _____|0|_______|1|_______|2|_______|0|_______|1|_______|2|__________|0|_______|1|_______|2|_______
    --                               _         _         _         _         _         _            _         _         _
    --   snk_in.eop    _____________|0|_______|1|_______|2|_______|0|_______|1|_______|2|__________|0|_______|1|_______|2
    --
    --   r.nof_pkt                                       3                             3                                3
    --   v.pkt_cnt     0             1         2         0         1         2         0            1         2         0
    --   r.pkt_cnt     x               1         2         0         1         2            0         1         2
    --                       ___________________________   ___________________________      _____________________________
    --   v.busy        _____|                           |_|                           |____|
    --                       _                             _                                _
    --   v.src_out.sop _____|0|___________________________|1|______________________________|2|___________________________
    --                                                   _                             _                                _
    --   v.src_out.eop _________________________________|0|___________________________|1|______________________________|2
    --

    -- input packet counter
    if snk_in.eop = '1' then
      if r.pkt_cnt < r.nof_pkt - 1 then
        v.pkt_cnt := r.pkt_cnt + 1;
      else
        v.pkt_cnt := 0;
      end if;
    end if;

    -- output packet bsn, sync, sop and eop
    v.src_out         := snk_in;
    v.src_out.bsn     := r.src_out.bsn;
    v.src_out.channel := r.src_out.channel;
    v.src_out.empty   := r.src_out.empty;
    v.src_out.err     := r.src_out.err;
    v.src_out.sync    := '0';
    v.src_out.sop     := '0';
    v.src_out.eop     := '0';

    if snk_in.sop = '1' then
      if r.pkt_cnt = 0 then
        v.src_out.sop     := '1';
        v.src_out.sync    := r.sync;  -- use sync from previous merged packet if it occurred on pkt_cnt /= 0.
        v.src_out.bsn     := snk_in.bsn;  -- use BSN     of first packet for merged packet
        v.src_out.channel := snk_in.channel;  -- use channel of first packet for merged packet
        v.sync            := '0';  -- reset captured sync.
        if snk_in.sync = '1' then
          v.src_out.sync  := '1';  -- set out sync to '1' if this first block contains the sync.
        end if;
        v.next_bsn        := INCR_UVEC(snk_in.bsn, g_bsn_increment);  -- expected bsn for next snk_in block
        v.bsn_err         := '0';  -- bsn of first block is correct by default
      else
        if snk_in.sync = '1' then
          v.sync := '1';  -- Capture sync if it occurs on a pkt_cnt /= 0 so we can use it in the next merged packet
        end if;
        v.next_bsn        := INCR_UVEC(r.next_bsn, g_bsn_increment);  -- expected bsn for next snk_in block
        if unsigned(snk_in.bsn) /= unsigned(r.next_bsn) then
          v.bsn_err       := '1';
        end if;
      end if;
    end if;

    if snk_in.eop = '1' then
      if r.pkt_cnt = 0 then
        v.src_out.err := snk_in.err;  -- take err field of first packet
      else
        v.src_out.err := r.src_out.err or snk_in.err;  -- OR the err fields of the packets to reflect combined error status.
      end if;
      if g_bsn_increment > 0 then
        v.src_out.err(g_bsn_err_bi) := r.bsn_err;  -- report bsn error bit in case of missing snk_in block
      end if;

      if r.pkt_cnt = r.nof_pkt - 1 then
        v.src_out.eop   := '1';
        v.src_out.empty := snk_in.empty;
      end if;
    end if;

    -- capture nof_pkt between output packets when there is no merge busy
    -- . For determining busy, using snk_in.sop is functionally equivalent to
    --   using v.src_out.sop, but using snk_in.sop may ease timing closure.
    if snk_in.sop = '1' then
      v.busy := '1';
    end if;
    if v.src_out.eop = '1' then
      v.busy := '0';
    end if;

    if g_align_at_sync then
      if snk_in.sync = '1' then
        if r.pkt_cnt > 0 then
          v.align_cnt := r.nof_pkt - r.pkt_cnt;  -- = 0 when sync occurs at integer number of merged blocks, > 0 when (re)align to sync is needed
        end if;
      end if;
    end if;

    if v.busy = '0' then
      v.nof_pkt := TO_UINT(nof_pkt) - r.align_cnt;  -- use shorter next merge to (re)align to sync (when g_align_at_sync=false then r.align_cnt=0)
      v.align_cnt := 0;
    end if;

    -- force sosi control to inactive for no output when nof_pkt=0
    if r.nof_pkt = 0 then
      v.src_out.valid := '0';
      v.src_out.sync  := '0';
      v.src_out.sop   := '0';
      v.src_out.eop   := '0';
      v.busy          := '0';
    end if;

    ----------------------------------------------------------------------------
    -- Reset and nxt_r
    if rst = '1' then
      v := c_reg_rst;
    end if;

    nxt_r <= v;
  end process;
end rtl;
