-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose : Create one gxb_reconfig module for all ALTGX instances.

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use technology_lib.technology_pkg.all;

entity ip_stratixiv_gxb_reconfig_v101 is
  generic (
    g_nof_gx        : natural;
    g_fromgxb_bus_w : natural := 17;
    g_togxb_bus_w   : natural := 4
  );
  port (
    reconfig_clk     : in std_logic;
    reconfig_fromgxb : in std_logic_vector(tech_ceil_div(g_nof_gx, 4) * g_fromgxb_bus_w - 1 downto 0);
    busy             : out std_logic;
    reconfig_togxb   : out std_logic_vector(g_togxb_bus_w - 1 downto 0)
  );
end ip_stratixiv_gxb_reconfig_v101;

architecture str of ip_stratixiv_gxb_reconfig_v101 is
begin
  gen_gx_reconfig_4 : if g_nof_gx <= 4 generate
    u_gx_reconfig_4 : entity work.ip_stratixiv_gxb_reconfig_v101_4
    port map (
      reconfig_clk        => reconfig_clk,
      reconfig_fromgxb    => reconfig_fromgxb,
      busy                => busy,
      reconfig_togxb      => reconfig_togxb
    );
  end generate;

  gen_gx_reconfig_8 : if g_nof_gx > 4 and g_nof_gx <= 8 generate
    u_gx_reconfig_8 : entity work.ip_stratixiv_gxb_reconfig_v101_8
    port map (
      reconfig_clk        => reconfig_clk,
      reconfig_fromgxb    => reconfig_fromgxb,
      busy                => busy,
      reconfig_togxb      => reconfig_togxb
    );
  end generate;

  gen_gx_reconfig_12 : if g_nof_gx > 8 and g_nof_gx <= 12 generate
    u_gx_reconfig_12 : entity work.ip_stratixiv_gxb_reconfig_v101_12
    port map (
      reconfig_clk        => reconfig_clk,
      reconfig_fromgxb    => reconfig_fromgxb,
      busy                => busy,
      reconfig_togxb      => reconfig_togxb
    );
  end generate;
end str;
