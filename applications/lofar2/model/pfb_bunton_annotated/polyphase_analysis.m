function outBins = polyphase_analysis(inData, filt, Nfft, Nstep)

% John Bunton CSIRO 2003

% inData = input time data
% filt = coefficients of prototype lowpass FIR filter
% Nfft = length of FFT, prefilter length = length(filt) / Nfft, if not then
%        'filt' is padded with zeros to a multiple of Nfft
% Nstep = increment along inData between FFT output blocks, so downsample
%         inData by Nstep, use Nstep = Nfft for critical sampling of outBins
% outBins = output data 2D one dimension time the other frequency

% Support padding Ncoefs with zeros, if filt is too short
Ntaps = ceil(length(filt) / Nfft);
Ncoefs = Ntaps * Nfft;
bfir = (1:Ncoefs) * 0;  % = zeros(1, Ncoefs)
bfir(1:length(filt)) = filt;

nof_blocks = floor((length(inData) - Ncoefs) / Nstep);
outBins = zeros(nof_blocks, Nfft);

% Remove comment to try interleaved filterbank, produces critically sampled
% outputs as well as intermediate frequency outputs
%Nfft = Nfft * 2;
%Ntaps = Ntaps / 2;

for k = 0:nof_blocks-1
    % Filter input using bfir as window, with downsampling rate Nstep
    tapData = bfir .* inData((1:Ncoefs) + Nstep*k);
    % Sum the FIR filter taps
    pfsData = (1:Nfft) * 0;
    for m = 0:Ntaps-1
        pfsData = pfsData + tapData((1:Nfft) + Nfft*m);
    end
    % FFT
    outBins(k+1, 1:Nfft) = fft(pfsData);
    if k < 3
        %inData((1:Ncoefs) + Nstep*k)
        %tapData
        %pfsData
        %outBins(k+1, :)
    end
end
%plot(real(pfsData))
