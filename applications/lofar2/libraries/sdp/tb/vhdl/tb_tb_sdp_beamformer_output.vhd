-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : E. Kooistra
-- Purpose: Verify multiple variations of tb_sdp_beamformer_output
-- Description:
-- Usage:
-- > as 5
-- > run -all
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_sdp_beamformer_output is
end tb_tb_sdp_beamformer_output;

architecture tb of tb_tb_sdp_beamformer_output is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_nof_repeat            : natural := 10;
  -- g_beamset_id            : natural := 0;
  -- g_use_transpose         : boolean := false;
  -- g_nof_destinations_max  : natural := 1;
  -- g_nof_destinations      : natural := 1;
  -- g_sim_force_bsn_error   : boolean := false

  -- One BDO destination
  u_one_identity          : entity work.tb_sdp_beamformer_output generic map( 50, 0, false, 1, 1, false);
  u_one_transpose         : entity work.tb_sdp_beamformer_output generic map( 50, 0,  true, 1, 1, false);
  u_one_transpose_bset_1  : entity work.tb_sdp_beamformer_output generic map( 50, 1,  true, 1, 1, false);

  -- Multiple BDO destinations
  -- . prime number combination
  u_multi_7_3        : entity work.tb_sdp_beamformer_output generic map( 50, 0,  true,  7,  3, false);
  u_multi_7_3_bset_1 : entity work.tb_sdp_beamformer_output generic map( 50, 1,  true,  7,  3, false);
  -- . use 1 destnation, when more are available
  u_multi_16_1       : entity work.tb_sdp_beamformer_output generic map( 50, 0,  true, 16,  1, false);
  -- . use all destinations that are available
  u_multi_16_16      : entity work.tb_sdp_beamformer_output generic map( 50, 0,  true, 16, 16, false);
  -- . use prime number of destination from maximum available
  u_multi_32_7       : entity work.tb_sdp_beamformer_output generic map( 50, 0,  true, 32,  7, false);
  -- . use unfeasible number of destination, to check that it becomes 1 less
  u_multi_32_32      : entity work.tb_sdp_beamformer_output generic map( 50, 0,  true, 32, 32, false);
  -- . use maximum number of destinations
  u_multi_32_31      : entity work.tb_sdp_beamformer_output generic map( 50, 0,  true, 32, 31, false);
end tb;
