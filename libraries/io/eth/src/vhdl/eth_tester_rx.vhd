-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- AUthor: E. Kooistra
-- Purpose: Test the 1GbE interface by sending and counting received packets.
-- Description: Rx part of eth_tester, see detailed design in [1]
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+ETH+tester+unit+for+1GbE

library IEEE, common_lib, dp_lib, diag_lib, tech_tse_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use work.eth_tester_pkg.all;

entity eth_tester_rx is
  generic (
    g_bg_sync_timeout  : natural := 220 * 10**6;  -- 10% margin for nominal 1 s with st_clk at 200MHz
    g_nof_octet_unpack : natural  := 1;
    g_nof_octet_input  : natural  := 4;  -- must be multiple of g_nof_octet_generate
    g_use_dp_header    : boolean  := true;
    g_hdr_field_arr    : t_common_field_arr := c_eth_tester_hdr_field_arr;
    g_remove_crc       : boolean := true  -- use TRUE when using sim_tse and tech_tse link interface,
                                          -- use FALSE when streaming link interface
  );
  port (
    -- Clocks and reset
    mm_rst             : in  std_logic;
    mm_clk             : in  std_logic;
    st_rst             : in  std_logic;
    st_clk             : in  std_logic;
    ref_sync           : in  std_logic;

    exp_length         : in natural range 0 to 2**c_halfword_w - 1;  -- 16 bit

    -- UDP receive interface
    rx_udp_sosi        : in  t_dp_sosi;

    -- Memory Mapped Slaves (one per stream)
    reg_bsn_monitor_v2_rx_copi     : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_rx_cipo     : out t_mem_cipo;
    reg_strobe_total_count_rx_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_strobe_total_count_rx_cipo : out t_mem_cipo
  );
end eth_tester_rx;

architecture str of eth_tester_rx is
  constant c_nof_total_counts     : natural := 3;  -- 0 = nof_sop, 1 = nof_valid, 2 = nof_crc_corrupt

  constant c_empty_w              : natural := ceil_log2(g_nof_octet_input);
  constant c_error_w              : natural := 1;
  constant c_in_data_w            : natural := g_nof_octet_input * c_octet_w;
  constant c_repack_data_w        : natural := g_nof_octet_unpack * c_octet_w;
  constant c_nof_repack_words     : natural := g_nof_octet_input / g_nof_octet_unpack;

  -- Rx FIFO size can be much less than rx_block_sz_max, because st_clk >
  -- eth_clk rate, but with st level tx-rx loopback the Rx FIFO does need
  -- rx_block_sz_max FIFO size.
  constant rx_block_sz_max   : natural := ceil_div(c_eth_tester_rx_block_len_max, g_nof_octet_input);
  constant c_fifo_size       : natural := true_log_pow2(rx_block_sz_max);

  signal rx_udp_data         : std_logic_vector(c_word_w - 1 downto 0);
  signal rx_offload_sosi     : t_dp_sosi;
  signal rx_offload_data     : std_logic_vector(c_word_w - 1 downto 0);
  signal decoded_sosi        : t_dp_sosi;
  signal decoded_data        : std_logic_vector(c_word_w - 1 downto 0);
  signal decoded_length      : natural;
  signal rx_fifo_siso        : t_dp_siso;
  signal rx_fifo_sosi        : t_dp_sosi;
  signal rx_fifo_data        : std_logic_vector(c_word_w - 1 downto 0);
  signal rx_fifo_wr_ful      : std_logic;
  signal rx_fifo_usedw       : std_logic_vector(ceil_log2(c_fifo_size) - 1 downto 0);
  signal unpacked_sosi       : t_dp_sosi;
  signal unpacked_data       : std_logic_vector(c_octet_w - 1 downto 0);
  signal crc_corrupt         : std_logic := '0';

  signal strobe_cnt_ref_sync : std_logic;
  signal in_strobe_arr       : std_logic_vector(c_nof_total_counts - 1 downto 0);

  signal hdr_fields_out_slv  : std_logic_vector(1023 downto 0);
  signal hdr_fields_raw_slv  : std_logic_vector(1023 downto 0);
  signal hdr_fields_out_rec  : t_eth_tester_header;
  signal hdr_fields_raw_rec  : t_eth_tester_header;
begin
  -- View sosi.data in Wave Window
  rx_udp_data     <= rx_udp_sosi.data(c_word_w - 1 downto 0);
  rx_offload_data <= rx_offload_sosi.data(c_word_w - 1 downto 0);
  decoded_data    <= decoded_sosi.data(c_word_w - 1 downto 0);
  rx_fifo_data    <= rx_fifo_sosi.data(c_word_w - 1 downto 0);
  unpacked_data   <= unpacked_sosi.data(c_octet_w - 1 downto 0);

  -------------------------------------------------------------------------------
  -- Rx ETH/UDP/IP packets with packed BG data
  -------------------------------------------------------------------------------

  u_dp_offload_rx : entity dp_lib.dp_offload_rx
  generic map (
    g_nof_streams   => 1,
    g_data_w        => c_in_data_w,
    g_symbol_w      => c_octet_w,
    g_hdr_field_arr => g_hdr_field_arr,
    g_remove_crc    => g_remove_crc,
    g_crc_nof_words => 1
  )
  port map (
    mm_rst    => mm_rst,
    mm_clk    => mm_clk,

    dp_rst    => st_rst,
    dp_clk    => st_clk,

    snk_in_arr(0)         => rx_udp_sosi,
    src_out_arr(0)        => rx_offload_sosi,

    hdr_fields_out_arr(0) => hdr_fields_out_slv,  -- Valid at src_out_arr(i).sop, use for sosi.sync
    hdr_fields_raw_arr(0) => hdr_fields_raw_slv  -- Valid at src_out_arr(i).sop and beyond, use for sosi.bsn
  );

  -- View record in Wave Window
  hdr_fields_out_rec <= func_eth_tester_map_header(hdr_fields_out_slv);
  hdr_fields_raw_rec <= func_eth_tester_map_header(hdr_fields_raw_slv);

  p_set_meta: process(rx_offload_sosi, hdr_fields_out_slv, hdr_fields_raw_slv)
  begin
    decoded_sosi      <= rx_offload_sosi;
    if g_use_dp_header then
      decoded_length    <=       TO_UINT(hdr_fields_raw_slv(field_hi(c_eth_tester_hdr_field_arr, "dp_length") downto field_lo(c_eth_tester_hdr_field_arr, "dp_length")));
      decoded_sosi.sync <=            sl(hdr_fields_out_slv(field_hi(c_eth_tester_hdr_field_arr, "dp_sync") downto field_lo(c_eth_tester_hdr_field_arr, "dp_sync")));
      decoded_sosi.bsn  <= RESIZE_DP_BSN(hdr_fields_raw_slv(field_hi(c_eth_tester_hdr_field_arr, "dp_bsn" ) downto field_lo(c_eth_tester_hdr_field_arr, "dp_bsn")));
    end if;
    -- Map rx_offload_sosi.err c_tech_tse_error_w = 6 bit value on to c_error_w = 1 bit decoded_sosi.err value
    decoded_sosi.err <= TO_DP_ERROR(0);
    if unsigned(rx_offload_sosi.err(c_tech_tse_error_w - 1 downto 0)) /= 0 then
      decoded_sosi.err <= TO_DP_ERROR(1);
    end if;
  end process;

  -- synthesis translate_off
  p_verify_length : process(st_clk)
  begin
    if rising_edge(st_clk) then
      if g_use_dp_header and decoded_sosi.sop = '1' then
        assert decoded_length = exp_length
          report "Unexpected Rx length" & natural'image(decoded_length) & " /= " & natural'image(exp_length) & " expected length from Tx"
          severity ERROR;
      end if;
    end if;
  end process;
  -- synthesis translate_on

  u_rx_fifo : entity dp_lib.dp_fifo_sc
  generic map (
    g_data_w         => c_in_data_w,
    g_bsn_w          => c_diag_bg_bsn_init_w,  -- = 64 bit
    g_empty_w        => c_empty_w,
    g_error_w        => c_error_w,
    g_use_bsn        => true,
    g_use_empty      => true,
    g_use_sync       => true,
    g_use_error      => true,
    g_fifo_size      => c_fifo_size
  )
  port map (
    rst         => st_rst,
    clk         => st_clk,
    -- Monitor FIFO filling
    wr_ful      => rx_fifo_wr_ful,
    usedw       => rx_fifo_usedw,
    -- ST sink
    snk_in      => decoded_sosi,
    -- ST source
    src_in      => rx_fifo_siso,
    src_out     => rx_fifo_sosi
  );

  u_unpack : entity dp_lib.dp_repack_data
  generic map (
    g_in_dat_w       => c_in_data_w,
    g_in_nof_words   => 1,
    g_in_symbol_w    => c_octet_w,
    g_out_dat_w      => c_repack_data_w,
    g_out_nof_words  => c_nof_repack_words,
    g_out_symbol_w   => c_octet_w
  )
  port map (
    rst              => st_rst,
    clk              => st_clk,
    snk_out          => rx_fifo_siso,
    snk_in           => rx_fifo_sosi,
    src_out          => unpacked_sosi
  );

  -------------------------------------------------------------------------------
  -- Rx packet monitors
  -------------------------------------------------------------------------------

  u_mms_dp_bsn_monitor_v2 : entity dp_lib.mms_dp_bsn_monitor_v2
  generic map (
    g_nof_streams  => 1,
    g_sync_timeout => g_bg_sync_timeout
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    reg_mosi       => reg_bsn_monitor_v2_rx_copi,
    reg_miso       => reg_bsn_monitor_v2_rx_cipo,

    -- Streaming clock domain
    dp_rst         => st_rst,
    dp_clk         => st_clk,
    ref_sync       => ref_sync,

    in_sosi_arr(0) => unpacked_sosi
  );

  -- Rx CRC result is available at last octet
  p_crc_corrupt : process(st_clk)
  begin
    if rising_edge(st_clk) then
      crc_corrupt <= '0';
      if unpacked_sosi.eop = '1' and unpacked_sosi.err(0) = '1' then
        crc_corrupt <= '1';
      end if;
    end if;
  end process;

  in_strobe_arr(0) <= unpacked_sosi.sop;  -- count total nof Rx packets
  in_strobe_arr(1) <= unpacked_sosi.valid;  -- count total nof Rx valid samples
  in_strobe_arr(2) <= crc_corrupt;  -- count total nof corrupted Rx packets

  strobe_cnt_ref_sync <= unpacked_sosi.sync when g_use_dp_header else ref_sync;

  u_dp_strobe_total_count : entity dp_lib.dp_strobe_total_count
  generic map (
    g_nof_counts  => c_nof_total_counts,
    g_count_w     => c_longword_w,
    g_clip        => true
  )
  port map (
    dp_rst        => st_rst,
    dp_clk        => st_clk,

    ref_sync      => strobe_cnt_ref_sync,
    in_strobe_arr => in_strobe_arr,

    mm_rst        => mm_rst,
    mm_clk        => mm_clk,

    reg_mosi      => reg_strobe_total_count_rx_copi,
    reg_miso      => reg_strobe_total_count_rx_cipo
  );
end str;
