-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. van der Walle
-- Purpose: Wrapper around append_crc that provides DP interfaces by converting
-- the AXI4 interfaces using axi4_stream_dp_bridge.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library axi4_lib, dp_lib;
use axi4_lib.axi4_stream_pkg.all;
use dp_lib.dp_stream_pkg.all;

use work.crc_pkg.all;
use work.bit_width_config_pkg.all;
use work.rdma_pkg.all;

entity append_crc_dp_wrapper is
  port ( 
    dp_clk          : in std_logic;
    dp_rst          : in std_logic;
    
    out_dp_sosi     : out t_dp_sosi := c_dp_sosi_rst;
    out_dp_siso     : in  t_dp_siso := c_dp_siso_rdy;

    in_dp_sosi      : in  t_dp_sosi := c_dp_sosi_rst;
    in_dp_siso      : out t_dp_siso := c_dp_siso_rdy
  );
end append_crc_dp_wrapper;

architecture str of append_crc_dp_wrapper is

  signal i_stream_stop      : std_logic;
  signal o_stream_stop      : std_logic;
  
  signal o_stream           : AXI4_Streaming_RDMA_t := AXI4_Streaming_RDMA_t_ZERO;
  signal i_stream           : AXI4_Streaming_RDMA_t := AXI4_Streaming_RDMA_t_ZERO;
  
  signal o_stream_axi4_sosi : t_axi4_sosi := c_axi4_sosi_rst;
  signal o_stream_axi4_siso : t_axi4_siso := c_axi4_siso_rst;
  signal i_stream_axi4_sosi : t_axi4_sosi := c_axi4_sosi_rst;
  signal i_stream_axi4_siso : t_axi4_siso := c_axi4_siso_rst;


begin 
  -- Wire Records to IN/OUT ports.
  -- o_stream
  i_stream_stop   <= not o_stream_axi4_siso.tready;

  o_stream_axi4_sosi.tvalid <= o_stream.valid;
  o_stream_axi4_sosi.tlast  <= o_stream.last;
  o_stream_axi4_sosi.tdata  <= o_stream.data;
  o_stream_axi4_sosi.tkeep  <= o_stream.keep;
  
  -- i_stream
  i_stream_axi4_siso.tready <= not o_stream_stop;

  i_stream.valid <= i_stream_axi4_sosi.tvalid;  
  i_stream.last  <= i_stream_axi4_sosi.tlast;
  i_stream.data  <= i_stream_axi4_sosi.tdata;
  i_stream.keep  <= i_stream_axi4_sosi.tkeep;
  
  u_dp_to_axi : entity axi4_lib.axi4_stream_dp_bridge
  generic map (
    g_use_empty => true,
    g_axi4_rl   => 1,
    g_dp_rl     => 1
  )
  port map (
    in_clk => dp_clk,
    in_rst => dp_rst,

    axi4_out_sosi => i_stream_axi4_sosi,
    axi4_out_siso => i_stream_axi4_siso,

    dp_in_sosi => in_dp_sosi,
    dp_in_siso => in_dp_siso
  );

  u_axi4_to_dp : entity axi4_lib.axi4_stream_dp_bridge
  generic map (
    g_use_empty => true,
    g_axi4_rl   => 1,
    g_dp_rl     => 1
  )
  port map (
    in_clk => dp_clk,
    in_rst => dp_rst,

    axi4_in_sosi => o_stream_axi4_sosi,
    axi4_in_siso => o_stream_axi4_siso,

    dp_out_sosi => out_dp_sosi,
    dp_out_siso => out_dp_siso
  );

u_append_crc : entity work.append_crc
  generic map(
    g_AREA_SPEED_TRADEOFF_FACTOR     => 4,
    g_INIT_CRC                       => f_ICRC_INIT,
    g_BIT_REVERSE_IN                 => true,
    g_BIT_REVERSE_OUT                => true,
    g_XOR_CRC_OUT                    => (31 downto 0 => '1'),
    g_OR_MASK                        => to_slv(c_RoCEv2_HEADER_INVARIANT_MASK),
    g_INVALID_CYCLES_BETWEEN_PACKETS => 1    
  )
  port map(
    i_clk         => dp_clk,
    i_clk_reset   => dp_rst,
    i_stream      => i_stream,
    o_stream_stop => o_stream_stop,
    o_stream      => o_stream,
    i_stream_stop => i_stream_stop
  );
    
end str;
