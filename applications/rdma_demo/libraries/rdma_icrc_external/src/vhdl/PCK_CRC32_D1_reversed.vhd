--------------------------------------------------------------------------------
-- Copyright (C) 1999-2008 Easics NV.
-- This source file may be used and distributed without restriction
-- provided that this copyright statement is not removed from the file
-- and that any derivative work contains the original copyright notice
-- and the associated disclaimer.
--
-- THIS SOURCE FILE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS
-- OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
-- WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
--
-- Purpose : synthesizable CRC function
--   * polynomial: x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x^1 + 1
--   * data width: 1
--
-- Info : tools@easics.be
--        http://www.easics.com
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package PCK_CRC32_D1_reversed is
  -- polynomial: x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x^1 + 1
  -- data width: 1
  -- convention: the first serial bit is D[0]
  function prevCRC32_D1
    (Data : std_logic;
      crc : std_logic_vector(31 downto 0))
    return std_logic_vector;
end PCK_CRC32_D1_reversed;


package body PCK_CRC32_D1_reversed is

  -- polynomial: x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x^1 + 1
  -- data width: 1
  -- convention: the first serial bit is D[0]
  function prevCRC32_D1
    (Data : std_logic;
      crc : std_logic_vector(31 downto 0))
    return std_logic_vector is

    variable d       : std_logic_vector(0 downto 0);
    variable c       : std_logic_vector(31 downto 0);
    variable old_crc : std_logic_vector(31 downto 0);

  begin
    d(0) := Data;
    c    := crc;

    old_crc(0)  := c(0) xor c(1);
    old_crc(1)  := c(0) xor c(2);
    old_crc(2)  := c(3);
    old_crc(3)  := c(0) xor c(4);
    old_crc(4)  := c(0) xor c(5);
    old_crc(5)  := c(6);
    old_crc(6)  := c(0) xor c(7);
    old_crc(7)  := c(0) xor c(8);
    old_crc(8)  := c(9);
    old_crc(9)  := c(0) xor c(10);
    old_crc(10) := c(0) xor c(11);
    old_crc(11) := c(0) xor c(12);
    old_crc(12) := c(13);
    old_crc(13) := c(14);
    old_crc(14) := c(15);
    old_crc(15) := c(0) xor c(16);
    old_crc(16) := c(17);
    old_crc(17) := c(18);
    old_crc(18) := c(19);
    old_crc(19) := c(20);
    old_crc(20) := c(21);
    old_crc(21) := c(0) xor c(22);
    old_crc(22) := c(0) xor c(23);
    old_crc(23) := c(24);
    old_crc(24) := c(25);
    old_crc(25) := c(0) xor c(26);
    old_crc(26) := c(27);
    old_crc(27) := c(28);
    old_crc(28) := c(29);
    old_crc(29) := c(30);
    old_crc(30) := c(31);
    old_crc(31) := c(0) xor d(0);
    return old_crc;
  end prevCRC32_D1;

end PCK_CRC32_D1_reversed;
