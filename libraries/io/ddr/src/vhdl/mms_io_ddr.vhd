-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, technology_lib, tech_ddr_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;
use technology_lib.technology_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity mms_io_ddr is
  generic(
    g_sim_model               : boolean := false;
    g_technology              : natural := c_tech_select_default;
    g_tech_ddr                : t_c_tech_ddr;
    g_wr_data_w               : natural := 32;
    g_wr_fifo_depth           : natural := 256;  -- defined at DDR side of the FIFO, >=16 and independent of wr burst size, default >= 256 because 32b*256 fits in 1 M9K so c_ctlr_data_w=256b will require 8 M9K
    g_rd_fifo_depth           : natural := 256;  -- defined at DDR side of the FIFO, >=16 AND > max number of rd burst sizes (so > c_rd_fifo_af_margin), default >= 256 because 32b*256 fits in 1 M9K so c_ctlr_data_w=256b will require 8 M9K
    g_rd_data_w               : natural := 32;
    g_wr_flush_mode           : string := "VAL";  -- "VAL", "SOP", "SYN"
    g_wr_flush_use_channel    : boolean := false;
    g_wr_flush_start_channel  : natural := 0;
    g_wr_flush_nof_channels   : positive := 1
  );
  port (
    -- DDR reference clock
    ctlr_ref_clk        : in    std_logic;
    ctlr_ref_rst        : in    std_logic;

    -- DDR controller clock domain
    ctlr_clk_out        : out   std_logic;
    ctlr_rst_out        : out   std_logic;

    ctlr_clk_in         : in    std_logic;  -- connect ctlr_clk_out to ctlr_clk_in at top level to avoid potential delta-cycle differences between the same clock
    ctlr_rst_in         : in    std_logic;  -- connect ctlr_rst_out to ctlr_rst_in at top level

    -- MM clock + reset
    mm_rst              : in    std_logic := '1';
    mm_clk              : in    std_logic := '0';

    -- MM interface
    reg_io_ddr_mosi     : in    t_mem_mosi := c_mem_mosi_rst;  -- register for DDR controller status info
    reg_io_ddr_miso     : out   t_mem_miso;

    -- Write FIFO clock domain
    wr_clk              : in    std_logic;
    wr_rst              : in    std_logic;

    wr_fifo_usedw       : out   std_logic_vector(ceil_log2(g_wr_fifo_depth * (func_tech_ddr_ctlr_data_w(g_tech_ddr) / g_wr_data_w) ) - 1 downto 0);  -- for monitoring purposes
    wr_sosi             : in    t_dp_sosi;
    wr_siso             : out   t_dp_siso;

    -- Read FIFO clock domain
    rd_clk              : in    std_logic;
    rd_rst              : in    std_logic;

    rd_fifo_usedw       : out   std_logic_vector(ceil_log2(g_rd_fifo_depth * (func_tech_ddr_ctlr_data_w(g_tech_ddr) / g_rd_data_w) ) - 1 downto 0);
    rd_sosi             : out   t_dp_sosi;
    rd_siso             : in    t_dp_siso;

    -- DDR3 pass on termination control from master to slave controller
    term_ctrl_out       : out   t_tech_ddr3_phy_terminationcontrol;
    term_ctrl_in        : in    t_tech_ddr3_phy_terminationcontrol := c_tech_ddr3_phy_terminationcontrol_rst;

    -- DDR3 PHY external interface
    phy3_in             : in    t_tech_ddr3_phy_in := c_tech_ddr3_phy_in_x;
    phy3_io             : inout t_tech_ddr3_phy_io;
    phy3_ou             : out   t_tech_ddr3_phy_ou;

    -- DDR4 PHY external interface
    phy4_in             : in    t_tech_ddr4_phy_in := c_tech_ddr4_phy_in_x;
    phy4_io             : inout t_tech_ddr4_phy_io;
    phy4_ou             : out   t_tech_ddr4_phy_ou;

    -- DDR Calibration result
    ddr_cal_ok          : out   std_logic := '0'
  );
end mms_io_ddr;

architecture str of mms_io_ddr is
  signal mm_dvr_miso         : t_mem_ctlr_miso;
  signal mm_dvr_mosi         : t_mem_ctlr_mosi;

  signal reg_io_ddr_mosi_arr : t_mem_mosi_arr(1 downto 0);
  signal reg_io_ddr_miso_arr : t_mem_miso_arr(1 downto 0);
begin
  -- Combine the reg map of io_ddr and io_ddr_reg
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => 2,
    g_mult_addr_w => ceil_log2(8)
  )
  port map (
    mosi     => reg_io_ddr_mosi,
    miso     => reg_io_ddr_miso,
    mosi_arr => reg_io_ddr_mosi_arr,
    miso_arr => reg_io_ddr_miso_arr
  );

  u_io_ddr : entity work.io_ddr
  generic map (
    g_sim_model              => g_sim_model,
    g_technology             => g_technology,
    g_tech_ddr               => g_tech_ddr,
    g_cross_domain_dvr_ctlr  => true,  -- mm_dvr_mosi from io_ddr_reg is in mm_clk domain and needs be crossed to the ctlr_clk_in domain by io_ddr_cross_domain in io_ddr
    g_wr_data_w              => g_wr_data_w,
    g_wr_fifo_depth          => g_wr_fifo_depth,
    g_rd_fifo_depth          => g_rd_fifo_depth,
    g_rd_data_w              => g_rd_data_w,
    g_wr_flush_mode          => g_wr_flush_mode,
    g_wr_flush_use_channel   => false,
    g_wr_flush_start_channel => 0,
    g_wr_flush_nof_channels  => 1
  )
  port map (
    -- DDR reference clock
    ctlr_ref_clk  => ctlr_ref_clk,
    ctlr_ref_rst  => ctlr_ref_rst,

    -- DDR controller clock domain
    ctlr_clk_out  => ctlr_clk_out,  -- output clock of the ddr controller is used as DP clk.
    ctlr_rst_out  => ctlr_rst_out,

    ctlr_clk_in   => ctlr_clk_in,
    ctlr_rst_in   => ctlr_rst_in,

    -- MM clock + reset
    mm_rst        => mm_rst,
    mm_clk        => mm_clk,

    -- MM register map for DDR controller status info
    reg_io_ddr_mosi => reg_io_ddr_mosi_arr(0),
    reg_io_ddr_miso => reg_io_ddr_miso_arr(0),

    -- Driver clock domain
    dvr_clk       => mm_clk,
    dvr_rst       => mm_rst,

    dvr_miso      => mm_dvr_miso,
    dvr_mosi      => mm_dvr_mosi,

    -- Write FIFO clock domain
    wr_clk        => wr_clk,
    wr_rst        => wr_rst,

    wr_fifo_usedw => OPEN,
    wr_sosi       => wr_sosi,
    wr_siso       => wr_siso,

    -- Read FIFO clock domain
    rd_clk        => rd_clk,
    rd_rst        => rd_rst,

    rd_fifo_usedw => OPEN,
    rd_sosi       => rd_sosi,
    rd_siso       => rd_siso,

    term_ctrl_out => OPEN,
    term_ctrl_in  => OPEN,

    phy3_in       => phy3_in,
    phy3_io       => phy3_io,
    phy3_ou       => phy3_ou,

    phy4_in       => phy4_in,
    phy4_io       => phy4_io,
    phy4_ou       => phy4_ou,

    ddr_cal_ok    => ddr_cal_ok
  );

  -- MM register map for DDR controller write and read access control via MM
  u_io_ddr_reg : entity work.io_ddr_reg
  port map(
    -- Clocks and reset
    mm_rst          => mm_rst,
    mm_clk          => mm_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in          => reg_io_ddr_mosi_arr(1),
    sla_out         => reg_io_ddr_miso_arr(1),

    -- MM registers in st_clk domain
    dvr_miso        => mm_dvr_miso,
    dvr_mosi        => mm_dvr_mosi
  );
end str;
