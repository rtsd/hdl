-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package qsys_unb2_test_pkg is
  -----------------------------------------------------------------------------
  -- this component declaration is copy-pasted from Quartus QSYS builder generated file:
  -- $HDL_WORK/build/unb2/quartus/unb2_test_ddr/qsys_unb2_test/sim/qsys_unb2_test.vhd
  -----------------------------------------------------------------------------

    component qsys_unb2_test is
        port (
            avs_eth_0_clk_export                            : out std_logic;  -- export
            avs_eth_0_irq_export                            : in  std_logic                     := 'X';  -- export
            avs_eth_0_ram_address_export                    : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_ram_read_export                       : out std_logic;  -- export
            avs_eth_0_ram_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_ram_write_export                      : out std_logic;  -- export
            avs_eth_0_ram_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reg_address_export                    : out std_logic_vector(3 downto 0);  -- export
            avs_eth_0_reg_read_export                       : out std_logic;  -- export
            avs_eth_0_reg_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_reg_write_export                      : out std_logic;  -- export
            avs_eth_0_reg_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            avs_eth_0_reset_export                          : out std_logic;  -- export
            avs_eth_0_tse_address_export                    : out std_logic_vector(9 downto 0);  -- export
            avs_eth_0_tse_read_export                       : out std_logic;  -- export
            avs_eth_0_tse_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_0_tse_waitrequest_export                : in  std_logic                     := 'X';  -- export
            avs_eth_0_tse_write_export                      : out std_logic;  -- export
            avs_eth_0_tse_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            avs_eth_1_clk_export                            : out std_logic;  -- export
            avs_eth_1_irq_export                            : in  std_logic                     := 'X';  -- export
            avs_eth_1_ram_address_export                    : out std_logic_vector(9 downto 0);  -- export
            avs_eth_1_ram_read_export                       : out std_logic;  -- export
            avs_eth_1_ram_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_1_ram_write_export                      : out std_logic;  -- export
            avs_eth_1_ram_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            avs_eth_1_reg_address_export                    : out std_logic_vector(3 downto 0);  -- export
            avs_eth_1_reg_read_export                       : out std_logic;  -- export
            avs_eth_1_reg_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_1_reg_write_export                      : out std_logic;  -- export
            avs_eth_1_reg_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            avs_eth_1_reset_export                          : out std_logic;  -- export
            avs_eth_1_tse_address_export                    : out std_logic_vector(9 downto 0);  -- export
            avs_eth_1_tse_read_export                       : out std_logic;  -- export
            avs_eth_1_tse_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            avs_eth_1_tse_waitrequest_export                : in  std_logic                     := 'X';  -- export
            avs_eth_1_tse_write_export                      : out std_logic;  -- export
            avs_eth_1_tse_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            clk_clk                                         : in  std_logic                     := 'X';  -- clk
            pio_pps_address_export                          : out std_logic_vector(0 downto 0);  -- export
            pio_pps_clk_export                              : out std_logic;  -- export
            pio_pps_read_export                             : out std_logic;  -- export
            pio_pps_readdata_export                         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_pps_reset_export                            : out std_logic;  -- export
            pio_pps_write_export                            : out std_logic;  -- export
            pio_pps_writedata_export                        : out std_logic_vector(31 downto 0);  -- export
            pio_system_info_address_export                  : out std_logic_vector(4 downto 0);  -- export
            pio_system_info_clk_export                      : out std_logic;  -- export
            pio_system_info_read_export                     : out std_logic;  -- export
            pio_system_info_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            pio_system_info_reset_export                    : out std_logic;  -- export
            pio_system_info_write_export                    : out std_logic;  -- export
            pio_system_info_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            pio_wdi_external_connection_export              : out std_logic;  -- export
            ram_diag_bg_10gbe_address_export                : out std_logic_vector(16 downto 0);  -- export
            ram_diag_bg_10gbe_clk_export                    : out std_logic;  -- export
            ram_diag_bg_10gbe_read_export                   : out std_logic;  -- export
            ram_diag_bg_10gbe_readdata_export               : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_bg_10gbe_reset_export                  : out std_logic;  -- export
            ram_diag_bg_10gbe_write_export                  : out std_logic;  -- export
            ram_diag_bg_10gbe_writedata_export              : out std_logic_vector(31 downto 0);  -- export
            ram_diag_bg_1gbe_address_export                 : out std_logic_vector(10 downto 0);  -- export
            ram_diag_bg_1gbe_clk_export                     : out std_logic;  -- export
            ram_diag_bg_1gbe_read_export                    : out std_logic;  -- export
            ram_diag_bg_1gbe_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_bg_1gbe_reset_export                   : out std_logic;  -- export
            ram_diag_bg_1gbe_write_export                   : out std_logic;  -- export
            ram_diag_bg_1gbe_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_10gbe_address_export       : out std_logic_vector(16 downto 0);  -- export
            ram_diag_data_buffer_10gbe_clk_export           : out std_logic;  -- export
            ram_diag_data_buffer_10gbe_read_export          : out std_logic;  -- export
            ram_diag_data_buffer_10gbe_readdata_export      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_10gbe_reset_export         : out std_logic;  -- export
            ram_diag_data_buffer_10gbe_write_export         : out std_logic;  -- export
            ram_diag_data_buffer_10gbe_writedata_export     : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_1gbe_address_export        : out std_logic_vector(10 downto 0);  -- export
            ram_diag_data_buffer_1gbe_clk_export            : out std_logic;  -- export
            ram_diag_data_buffer_1gbe_read_export           : out std_logic;  -- export
            ram_diag_data_buffer_1gbe_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_1gbe_reset_export          : out std_logic;  -- export
            ram_diag_data_buffer_1gbe_write_export          : out std_logic;  -- export
            ram_diag_data_buffer_1gbe_writedata_export      : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_ddr_mb_i_address_export    : out std_logic_vector(10 downto 0);  -- export
            ram_diag_data_buffer_ddr_mb_i_clk_export        : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_i_read_export       : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_i_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_ddr_mb_i_reset_export      : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_i_write_export      : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_i_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            ram_diag_data_buffer_ddr_mb_ii_address_export   : out std_logic_vector(10 downto 0);  -- export
            ram_diag_data_buffer_ddr_mb_ii_clk_export       : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_ii_read_export      : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_ii_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            ram_diag_data_buffer_ddr_mb_ii_reset_export     : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_ii_write_export     : out std_logic;  -- export
            ram_diag_data_buffer_ddr_mb_ii_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_10gbe_address_export            : out std_logic_vector(10 downto 0);  -- export
            reg_bsn_monitor_10gbe_clk_export                : out std_logic;  -- export
            reg_bsn_monitor_10gbe_read_export               : out std_logic;  -- export
            reg_bsn_monitor_10gbe_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_10gbe_reset_export              : out std_logic;  -- export
            reg_bsn_monitor_10gbe_write_export              : out std_logic;  -- export
            reg_bsn_monitor_10gbe_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_bsn_monitor_1gbe_address_export             : out std_logic_vector(4 downto 0);  -- export
            reg_bsn_monitor_1gbe_clk_export                 : out std_logic;  -- export
            reg_bsn_monitor_1gbe_read_export                : out std_logic;  -- export
            reg_bsn_monitor_1gbe_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_bsn_monitor_1gbe_reset_export               : out std_logic;  -- export
            reg_bsn_monitor_1gbe_write_export               : out std_logic;  -- export
            reg_bsn_monitor_1gbe_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_diag_bg_10gbe_address_export                : out std_logic_vector(2 downto 0);  -- export
            reg_diag_bg_10gbe_clk_export                    : out std_logic;  -- export
            reg_diag_bg_10gbe_read_export                   : out std_logic;  -- export
            reg_diag_bg_10gbe_readdata_export               : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_bg_10gbe_reset_export                  : out std_logic;  -- export
            reg_diag_bg_10gbe_write_export                  : out std_logic;  -- export
            reg_diag_bg_10gbe_writedata_export              : out std_logic_vector(31 downto 0);  -- export
            reg_diag_bg_1gbe_address_export                 : out std_logic_vector(2 downto 0);  -- export
            reg_diag_bg_1gbe_clk_export                     : out std_logic;  -- export
            reg_diag_bg_1gbe_read_export                    : out std_logic;  -- export
            reg_diag_bg_1gbe_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_bg_1gbe_reset_export                   : out std_logic;  -- export
            reg_diag_bg_1gbe_write_export                   : out std_logic;  -- export
            reg_diag_bg_1gbe_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_10gbe_address_export       : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buffer_10gbe_clk_export           : out std_logic;  -- export
            reg_diag_data_buffer_10gbe_read_export          : out std_logic;  -- export
            reg_diag_data_buffer_10gbe_readdata_export      : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_10gbe_reset_export         : out std_logic;  -- export
            reg_diag_data_buffer_10gbe_write_export         : out std_logic;  -- export
            reg_diag_data_buffer_10gbe_writedata_export     : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_1gbe_address_export        : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buffer_1gbe_clk_export            : out std_logic;  -- export
            reg_diag_data_buffer_1gbe_read_export           : out std_logic;  -- export
            reg_diag_data_buffer_1gbe_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_1gbe_reset_export          : out std_logic;  -- export
            reg_diag_data_buffer_1gbe_write_export          : out std_logic;  -- export
            reg_diag_data_buffer_1gbe_writedata_export      : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_ddr_mb_i_address_export    : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buffer_ddr_mb_i_clk_export        : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_i_read_export       : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_i_readdata_export   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_ddr_mb_i_reset_export      : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_i_write_export      : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_i_writedata_export  : out std_logic_vector(31 downto 0);  -- export
            reg_diag_data_buffer_ddr_mb_ii_address_export   : out std_logic_vector(4 downto 0);  -- export
            reg_diag_data_buffer_ddr_mb_ii_clk_export       : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_ii_read_export      : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_ii_readdata_export  : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_data_buffer_ddr_mb_ii_reset_export     : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_ii_write_export     : out std_logic;  -- export
            reg_diag_data_buffer_ddr_mb_ii_writedata_export : out std_logic_vector(31 downto 0);  -- export
            reg_diag_rx_seq_10gbe_address_export            : out std_logic_vector(4 downto 0);  -- export
            reg_diag_rx_seq_10gbe_clk_export                : out std_logic;  -- export
            reg_diag_rx_seq_10gbe_read_export               : out std_logic;  -- export
            reg_diag_rx_seq_10gbe_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_10gbe_reset_export              : out std_logic;  -- export
            reg_diag_rx_seq_10gbe_write_export              : out std_logic;  -- export
            reg_diag_rx_seq_10gbe_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_diag_rx_seq_1gbe_address_export             : out std_logic_vector(2 downto 0);  -- export
            reg_diag_rx_seq_1gbe_clk_export                 : out std_logic;  -- export
            reg_diag_rx_seq_1gbe_read_export                : out std_logic;  -- export
            reg_diag_rx_seq_1gbe_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_1gbe_reset_export               : out std_logic;  -- export
            reg_diag_rx_seq_1gbe_write_export               : out std_logic;  -- export
            reg_diag_rx_seq_1gbe_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_diag_rx_seq_ddr_mb_i_address_export         : out std_logic_vector(2 downto 0);  -- export
            reg_diag_rx_seq_ddr_mb_i_clk_export             : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_i_read_export            : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_i_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_ddr_mb_i_reset_export           : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_i_write_export           : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_i_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            reg_diag_rx_seq_ddr_mb_ii_address_export        : out std_logic_vector(2 downto 0);  -- export
            reg_diag_rx_seq_ddr_mb_ii_clk_export            : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_ii_read_export           : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_ii_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_rx_seq_ddr_mb_ii_reset_export          : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_ii_write_export          : out std_logic;  -- export
            reg_diag_rx_seq_ddr_mb_ii_writedata_export      : out std_logic_vector(31 downto 0);  -- export
            reg_diag_tx_seq_10gbe_address_export            : out std_logic_vector(3 downto 0);  -- export
            reg_diag_tx_seq_10gbe_clk_export                : out std_logic;  -- export
            reg_diag_tx_seq_10gbe_read_export               : out std_logic;  -- export
            reg_diag_tx_seq_10gbe_readdata_export           : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_tx_seq_10gbe_reset_export              : out std_logic;  -- export
            reg_diag_tx_seq_10gbe_write_export              : out std_logic;  -- export
            reg_diag_tx_seq_10gbe_writedata_export          : out std_logic_vector(31 downto 0);  -- export
            reg_diag_tx_seq_1gbe_address_export             : out std_logic_vector(1 downto 0);  -- export
            reg_diag_tx_seq_1gbe_clk_export                 : out std_logic;  -- export
            reg_diag_tx_seq_1gbe_read_export                : out std_logic;  -- export
            reg_diag_tx_seq_1gbe_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_tx_seq_1gbe_reset_export               : out std_logic;  -- export
            reg_diag_tx_seq_1gbe_write_export               : out std_logic;  -- export
            reg_diag_tx_seq_1gbe_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_diag_tx_seq_ddr_mb_i_address_export         : out std_logic_vector(1 downto 0);  -- export
            reg_diag_tx_seq_ddr_mb_i_clk_export             : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_i_read_export            : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_i_readdata_export        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_tx_seq_ddr_mb_i_reset_export           : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_i_write_export           : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_i_writedata_export       : out std_logic_vector(31 downto 0);  -- export
            reg_diag_tx_seq_ddr_mb_ii_address_export        : out std_logic_vector(1 downto 0);  -- export
            reg_diag_tx_seq_ddr_mb_ii_clk_export            : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_ii_read_export           : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_ii_readdata_export       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_diag_tx_seq_ddr_mb_ii_reset_export          : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_ii_write_export          : out std_logic;  -- export
            reg_diag_tx_seq_ddr_mb_ii_writedata_export      : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_ctrl_address_export                    : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_ctrl_clk_export                        : out std_logic;  -- export
            reg_dpmm_ctrl_read_export                       : out std_logic;  -- export
            reg_dpmm_ctrl_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_ctrl_reset_export                      : out std_logic;  -- export
            reg_dpmm_ctrl_write_export                      : out std_logic;  -- export
            reg_dpmm_ctrl_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            reg_dpmm_data_address_export                    : out std_logic_vector(0 downto 0);  -- export
            reg_dpmm_data_clk_export                        : out std_logic;  -- export
            reg_dpmm_data_read_export                       : out std_logic;  -- export
            reg_dpmm_data_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_dpmm_data_reset_export                      : out std_logic;  -- export
            reg_dpmm_data_write_export                      : out std_logic;  -- export
            reg_dpmm_data_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            reg_epcs_address_export                         : out std_logic_vector(2 downto 0);  -- export
            reg_epcs_clk_export                             : out std_logic;  -- export
            reg_epcs_read_export                            : out std_logic;  -- export
            reg_epcs_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_epcs_reset_export                           : out std_logic;  -- export
            reg_epcs_write_export                           : out std_logic;  -- export
            reg_epcs_writedata_export                       : out std_logic_vector(31 downto 0);  -- export
            reg_eth10g_back0_address_export                 : out std_logic_vector(5 downto 0);  -- export
            reg_eth10g_back0_clk_export                     : out std_logic;  -- export
            reg_eth10g_back0_read_export                    : out std_logic;  -- export
            reg_eth10g_back0_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_eth10g_back0_reset_export                   : out std_logic;  -- export
            reg_eth10g_back0_write_export                   : out std_logic;  -- export
            reg_eth10g_back0_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_eth10g_back1_address_export                 : out std_logic_vector(5 downto 0);  -- export
            reg_eth10g_back1_clk_export                     : out std_logic;  -- export
            reg_eth10g_back1_read_export                    : out std_logic;  -- export
            reg_eth10g_back1_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_eth10g_back1_reset_export                   : out std_logic;  -- export
            reg_eth10g_back1_write_export                   : out std_logic;  -- export
            reg_eth10g_back1_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_eth10g_qsfp_ring_address_export             : out std_logic_vector(6 downto 0);  -- export
            reg_eth10g_qsfp_ring_clk_export                 : out std_logic;  -- export
            reg_eth10g_qsfp_ring_read_export                : out std_logic;  -- export
            reg_eth10g_qsfp_ring_readdata_export            : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_eth10g_qsfp_ring_reset_export               : out std_logic;  -- export
            reg_eth10g_qsfp_ring_write_export               : out std_logic;  -- export
            reg_eth10g_qsfp_ring_writedata_export           : out std_logic_vector(31 downto 0);  -- export
            reg_fpga_sens_address_export                    : out std_logic_vector(2 downto 0);  -- export
            reg_fpga_sens_clk_export                        : out std_logic;  -- export
            reg_fpga_sens_read_export                       : out std_logic;  -- export
            reg_fpga_sens_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_fpga_sens_reset_export                      : out std_logic;  -- export
            reg_fpga_sens_write_export                      : out std_logic;  -- export
            reg_fpga_sens_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            reg_io_ddr_mb_i_address_export                  : out std_logic_vector(15 downto 0);  -- export
            reg_io_ddr_mb_i_clk_export                      : out std_logic;  -- export
            reg_io_ddr_mb_i_read_export                     : out std_logic;  -- export
            reg_io_ddr_mb_i_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_io_ddr_mb_i_reset_export                    : out std_logic;  -- export
            reg_io_ddr_mb_i_write_export                    : out std_logic;  -- export
            reg_io_ddr_mb_i_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_io_ddr_mb_ii_address_export                 : out std_logic_vector(15 downto 0);  -- export
            reg_io_ddr_mb_ii_clk_export                     : out std_logic;  -- export
            reg_io_ddr_mb_ii_read_export                    : out std_logic;  -- export
            reg_io_ddr_mb_ii_readdata_export                : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_io_ddr_mb_ii_reset_export                   : out std_logic;  -- export
            reg_io_ddr_mb_ii_write_export                   : out std_logic;  -- export
            reg_io_ddr_mb_ii_writedata_export               : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_ctrl_address_export                    : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_ctrl_clk_export                        : out std_logic;  -- export
            reg_mmdp_ctrl_read_export                       : out std_logic;  -- export
            reg_mmdp_ctrl_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_ctrl_reset_export                      : out std_logic;  -- export
            reg_mmdp_ctrl_write_export                      : out std_logic;  -- export
            reg_mmdp_ctrl_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            reg_mmdp_data_address_export                    : out std_logic_vector(0 downto 0);  -- export
            reg_mmdp_data_clk_export                        : out std_logic;  -- export
            reg_mmdp_data_read_export                       : out std_logic;  -- export
            reg_mmdp_data_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_mmdp_data_reset_export                      : out std_logic;  -- export
            reg_mmdp_data_write_export                      : out std_logic;  -- export
            reg_mmdp_data_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            reg_remu_address_export                         : out std_logic_vector(2 downto 0);  -- export
            reg_remu_clk_export                             : out std_logic;  -- export
            reg_remu_read_export                            : out std_logic;  -- export
            reg_remu_readdata_export                        : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_remu_reset_export                           : out std_logic;  -- export
            reg_remu_write_export                           : out std_logic;  -- export
            reg_remu_writedata_export                       : out std_logic_vector(31 downto 0);  -- export
            reg_tr_10gbe_back0_address_export               : out std_logic_vector(17 downto 0);  -- export
            reg_tr_10gbe_back0_clk_export                   : out std_logic;  -- export
            reg_tr_10gbe_back0_read_export                  : out std_logic;  -- export
            reg_tr_10gbe_back0_readdata_export              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_tr_10gbe_back0_reset_export                 : out std_logic;  -- export
            reg_tr_10gbe_back0_waitrequest_export           : in  std_logic                     := 'X';  -- export
            reg_tr_10gbe_back0_write_export                 : out std_logic;  -- export
            reg_tr_10gbe_back0_writedata_export             : out std_logic_vector(31 downto 0);  -- export
            reg_tr_10gbe_back1_address_export               : out std_logic_vector(17 downto 0);  -- export
            reg_tr_10gbe_back1_clk_export                   : out std_logic;  -- export
            reg_tr_10gbe_back1_read_export                  : out std_logic;  -- export
            reg_tr_10gbe_back1_readdata_export              : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_tr_10gbe_back1_reset_export                 : out std_logic;  -- export
            reg_tr_10gbe_back1_waitrequest_export           : in  std_logic                     := 'X';  -- export
            reg_tr_10gbe_back1_write_export                 : out std_logic;  -- export
            reg_tr_10gbe_back1_writedata_export             : out std_logic_vector(31 downto 0);  -- export
            reg_tr_10gbe_qsfp_ring_address_export           : out std_logic_vector(18 downto 0);  -- export
            reg_tr_10gbe_qsfp_ring_clk_export               : out std_logic;  -- export
            reg_tr_10gbe_qsfp_ring_read_export              : out std_logic;  -- export
            reg_tr_10gbe_qsfp_ring_readdata_export          : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_tr_10gbe_qsfp_ring_reset_export             : out std_logic;  -- export
            reg_tr_10gbe_qsfp_ring_waitrequest_export       : in  std_logic                     := 'X';  -- export
            reg_tr_10gbe_qsfp_ring_write_export             : out std_logic;  -- export
            reg_tr_10gbe_qsfp_ring_writedata_export         : out std_logic_vector(31 downto 0);  -- export
            reg_unb_sens_address_export                     : out std_logic_vector(2 downto 0);  -- export
            reg_unb_sens_clk_export                         : out std_logic;  -- export
            reg_unb_sens_read_export                        : out std_logic;  -- export
            reg_unb_sens_readdata_export                    : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_sens_reset_export                       : out std_logic;  -- export
            reg_unb_sens_write_export                       : out std_logic;  -- export
            reg_unb_sens_writedata_export                   : out std_logic_vector(31 downto 0);  -- export
            reg_wdi_address_export                          : out std_logic_vector(0 downto 0);  -- export
            reg_wdi_clk_export                              : out std_logic;  -- export
            reg_wdi_read_export                             : out std_logic;  -- export
            reg_wdi_readdata_export                         : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_wdi_reset_export                            : out std_logic;  -- export
            reg_wdi_write_export                            : out std_logic;  -- export
            reg_wdi_writedata_export                        : out std_logic_vector(31 downto 0);  -- export
            reset_reset_n                                   : in  std_logic                     := 'X';  -- reset_n
            rom_system_info_address_export                  : out std_logic_vector(9 downto 0);  -- export
            rom_system_info_clk_export                      : out std_logic;  -- export
            rom_system_info_read_export                     : out std_logic;  -- export
            rom_system_info_readdata_export                 : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            rom_system_info_reset_export                    : out std_logic;  -- export
            rom_system_info_write_export                    : out std_logic;  -- export
            rom_system_info_writedata_export                : out std_logic_vector(31 downto 0);  -- export
            reg_unb_pmbus_readdata_export                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- export
            reg_unb_pmbus_read_export                       : out std_logic;  -- export
            reg_unb_pmbus_writedata_export                  : out std_logic_vector(31 downto 0);  -- export
            reg_unb_pmbus_write_export                      : out std_logic;  -- export
            reg_unb_pmbus_address_export                    : out std_logic_vector(2 downto 0);  -- export
            reg_unb_pmbus_clk_export                        : out std_logic;  -- export
            reg_unb_pmbus_reset_export                      : out std_logic  -- export
        );
    end component qsys_unb2_test;
end qsys_unb2_test_pkg;
