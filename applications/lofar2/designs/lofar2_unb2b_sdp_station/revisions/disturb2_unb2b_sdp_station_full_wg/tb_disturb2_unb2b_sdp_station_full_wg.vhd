-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Self-checking testbench for simulating disturb2_unb2b_sdp_station_wg using WG data.
--
-- Description:
--   MM control actions:
--
--   1) Enable calc mode for WG via reg_diag_wg with:
--      1. freq = 19.921875MHz (subband 102, at mid of normal beamlet)
--      2. freg = 19.43359375 MHz (subband 99.5, at mid of shifted beamlet)
--      Use same ampl for both WG. Use c_beamlet_scale such that beamlet output
--      will fit in W_beamlet = 8 bits.
--
--   2) Read current BSN from reg_bsn_scheduler_wg and write reg_bsn_scheduler_wg
--      to trigger start of WG at BSN.
--
--   3) Verify beamlet data in 10GbE output for beamset 0 (with the normal
--      beamlets) and beamset 1 (with the frequency shifted beamlets).
--      . verify subband 102 in beamset 0
--      . verify subband  99.5 in beamset 1
--      . These subbands also appear at about half power in the other beamset
--        but these values are not verified, because they are at the edge of
--        the subband and will in practise not be used. Therefore choose WG
--        c_wg_ampl_sp_2 = c_wg_ampl_sp_0 / 3 to distinghuis them.
--        The purpose of the 2x oversampled filterbank is to only use the
--        beamlet frequencies that are around the center of the subband, so
--        the half of the subbands from beamset 0 and half of the subbands
--        from beamset 1.
--
-- Usage:
--   > as 7    # default
--   > as 12   # for detailed debugging
--   > run -a
--   . manually add missing beamlet_arr2_im/re to wave window, and manually
--     copy beamlet_arr2_im[195:215] and [1170:1190] and the rx_beamlet_*
--     index signals to have a more compact view of the relevant indices.
--   . If c_subband_phase = 0.0 then beamlet_arr2_re entries are 0.
--   . manually add top level generics/constants to wave window.
--   Takes about 1h.
--
-- Remark: TB is based on tb_lofar2_unb2c_sdp_station_bf.
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, i2c_lib, mm_lib, dp_lib, diag_lib, lofar2_sdp_lib, wpfb_lib, tech_pll_lib, tr_10GbE_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_str_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use diag_lib.diag_pkg.all;
use wpfb_lib.wpfb_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tb_disturb2_unb2b_sdp_station_full_wg is
end tb_disturb2_unb2b_sdp_station_full_wg;

architecture tb of tb_disturb2_unb2b_sdp_station_full_wg is
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 0;
  constant c_id              : std_logic_vector(7 downto 0) := "00000000";
  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb2b_board_fw_version := (1, 0);

  constant c_eth_clk_period      : time := 8 ns;  -- 125 MHz XO on UniBoard
  constant c_ext_clk_period      : time := 5 ns;
  constant c_bck_ref_clk_period  : time := 5 ns;
  constant c_sa_clk_period       : time := tech_pll_clk_644_period;  -- 644MHz
  constant c_pps_period          : natural := 1000;

  constant c_tb_clk_period       : time := 100 ps;  -- use fast tb_clk to speed up M&C
  constant c_cable_delay         : time := 12 ns;

  constant c_nof_block_per_sync  : natural := 16;
  constant c_nof_clk_per_sync    : natural := c_nof_block_per_sync * c_sdp_N_fft;
  constant c_wpfb_sim            : t_wpfb := func_wpfb_set_nof_block_per_sync(c_sdp_wpfb_subbands, c_nof_block_per_sync);
  constant c_wpfb_complex_sim    : t_wpfb := func_wpfb_map_real_input_wpfb_parameters_to_complex_input(c_wpfb_sim);

  -- WG
  constant c_bsn_start_wg         : natural := 2;  -- start WG at this BSN to instead of some BSN, to avoid mismatches in exact expected data values
  -- . ampl
  constant c_beamlet_scale        : real := 1.0 / 2.0**9;
  constant c_wg_ampl_sp_0         : natural := natural(7.0 / c_beamlet_scale);  -- choose < 8.0 to have no beamlet output overflow with unit weights and unit beamlet scale
  constant c_wg_ampl_sp_2         : natural := c_wg_ampl_sp_0 / 3;  -- use different ampl for sp_0 and sp_2 to distinghuis them
  -- . phase
  constant c_subband_phase        : real := 0.0;  -- wanted subband phase in degrees = WG phase at sop
  constant c_wg_latency           : integer := c_diag_wg_latency + c_sdp_shiftram_latency - 0;  -- -0 to account for BSN scheduler start trigger latency
  constant c_subband_sp_0         : real := 102.0;  -- use WG at sp-0 for subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
  constant c_subband_sp_2         : real :=  99.5;  -- use WG at sp-2 for subband at index 99.5 = 99.5/1024 * 200MHz = 19.43359375 MHz
  constant c_subband_freq_sp_0    : real := real(c_subband_sp_0) / real(c_sdp_N_fft);  -- normalized by fs = f_adc = 200 MHz = dp_clk rate
  constant c_subband_freq_sp_2    : real := real(c_subband_sp_2) / real(c_sdp_N_fft);  -- normalized by fs = f_adc = 200 MHz = dp_clk rate
  constant c_wg_phase_offset_sp_0 : real := 360.0 * real(c_wg_latency) * c_subband_freq_sp_0;  -- c_diag_wg_latency is in dp_clk cycles
  constant c_wg_phase_offset_sp_2 : real := 360.0 * real(c_wg_latency) * c_subband_freq_sp_2;  -- c_diag_wg_latency is in dp_clk cycles
  constant c_wg_phase_sp_0        : real := c_subband_phase + c_wg_phase_offset_sp_0;  -- WG phase in degrees
  constant c_wg_phase_sp_2        : real := c_subband_phase + c_wg_phase_offset_sp_2;  -- WG phase in degrees

  -- WPFB, use default unit subband weights (= 1.0 + 0j)
  constant c_exp_subband_ampl_sp_0 : real := real(c_wg_ampl_sp_0) * c_sdp_wpfb_subband_sp_ampl_ratio;
  constant c_exp_subband_ampl_sp_2 : real := real(c_wg_ampl_sp_2) * c_sdp_wpfb_subband_sp_ampl_ratio;

  constant c_subband_phase_offset  : real := -90.0;  -- WG with zero phase sinus yields subband with -90 degrees phase (negative Im, zero Re)
  constant c_exp_subband_phase     : real := c_subband_phase + c_subband_phase_offset;

  -- BF
  -- . use one active WG per subband, so beamlet_sum of one subband
  -- . use default unit BF weights (= 1.0 + 0j), so beamlet_sum = subband
  -- . use beamlet output = beamlet_sum * c_beamlet_scale
  constant c_exp_beamlet_scale     : natural := natural(c_beamlet_scale * real(c_sdp_unit_beamlet_scale));  -- c_sdp_unit_beamlet_scale = 2**15;
  constant c_exp_beamlet_ampl_sp_0 : real := c_exp_subband_ampl_sp_0 * c_beamlet_scale;
  constant c_exp_beamlet_ampl_sp_2 : real := c_exp_subband_ampl_sp_2 * c_beamlet_scale;
  constant c_exp_beamlet_phase     : real := c_exp_subband_phase;
  constant c_exp_beamlet_re_sp_0   : integer := integer(COMPLEX_RE(c_exp_beamlet_ampl_sp_0, c_exp_beamlet_phase));
  constant c_exp_beamlet_re_sp_2   : integer := integer(COMPLEX_RE(c_exp_beamlet_ampl_sp_2, c_exp_beamlet_phase));
  constant c_exp_beamlet_im_sp_0   : integer := integer(COMPLEX_IM(c_exp_beamlet_ampl_sp_0, c_exp_beamlet_phase));
  constant c_exp_beamlet_im_sp_2   : integer := integer(COMPLEX_IM(c_exp_beamlet_ampl_sp_2, c_exp_beamlet_phase));

  constant c_exp_beamlet_index    : natural := natural(c_subband_sp_0) * c_sdp_N_pol;
  constant c_exp_beamlet_index_os : natural := c_sdp_N_pol_bf * c_sdp_cep_nof_beamlets_per_block + natural(ROUND(c_subband_sp_2)) * c_sdp_N_pol;

  type t_real_arr is array (integer range <>) of real;
  type t_slv_64_subbands_arr is array (integer range <>) of t_slv_64_arr(0 to c_sdp_S_sub_bf);

  -- MM
  -- . Address widths of a single MM instance
  constant c_addr_w_reg_diag_wg           : natural := 2;
  constant c_addr_w_reg_dp_xonoff         : natural := 1;
  constant c_addr_w_reg_bf_scale          : natural := 1;
  -- . Address spans of a single MM instance
  constant c_mm_span_reg_diag_wg          : natural := 2**c_addr_w_reg_diag_wg;
  constant c_mm_span_reg_dp_xonoff        : natural := 2**c_addr_w_reg_dp_xonoff;
  constant c_mm_span_reg_bf_scale         : natural := 2**c_addr_w_reg_bf_scale;

  constant c_mm_file_reg_ppsh             : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "PIO_PPS";
  constant c_mm_file_reg_bsn_source_v2    : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SOURCE_V2";
  constant c_mm_file_reg_bsn_scheduler_wg : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BSN_SCHEDULER";
  constant c_mm_file_reg_diag_wg          : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_WG";
  constant c_mm_file_reg_dp_xonoff        : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_DP_XONOFF";
  constant c_mm_file_reg_bf_scale         : string := mmf_unb_file_prefix(c_unb_nr, c_node_nr) & "REG_BF_SCALE";

  -- Tb
  signal tb_end              : std_logic := '0';
  signal sim_done            : std_logic := '0';
  signal tb_clk              : std_logic := '0';
  signal rd_data             : std_logic_vector(c_32 - 1 downto 0);

  -- WG
  signal current_bsn_wg      : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  -- BF
  signal rd_beamlet_scale    : std_logic_vector(15 downto 0);

  -- 10GbE
  -- . header fields
  signal rx_beamlet_scale        : std_logic_vector(15 downto 0);
  signal rx_beamlet_index_offset : natural := 0;
  signal rx_blocks_per_packet    : std_logic_vector(7 downto 0);
  signal rx_beamlets_per_block   : std_logic_vector(15 downto 0);
  signal rx_block_period         : std_logic_vector(15 downto 0);
  signal rx_bsn                  : std_logic_vector(63 downto 0);

  -- . payload indices and data arrays
  signal rx_beamlet_blk      : natural := 0;
  signal rx_beamlet_cnt      : natural := 0;
  signal rx_beamlet_valid    : std_logic := '0';
  signal rx_beamlet_sop      : std_logic := '0';

  signal beamlet_arr2_re : t_slv_8_arr(c_sdp_R_os * c_sdp_N_pol_bf * c_sdp_cep_nof_beamlets_per_block - 1 downto 0);
  signal beamlet_arr2_im : t_slv_8_arr(c_sdp_R_os * c_sdp_N_pol_bf * c_sdp_cep_nof_beamlets_per_block - 1 downto 0);

  signal tr_10GbE_src_out       : t_dp_sosi;
  signal tr_ref_clk_312         : std_logic := '0';
  signal tr_ref_clk_156         : std_logic := '0';
  signal tr_ref_rst_156         : std_logic := '0';

  -- DUT
  signal ext_clk             : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal ext_pps             : std_logic := '0';
  signal pps_rst             : std_logic := '1';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
  signal eth_rxp             : std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
  signal pmbus_scl           : std_logic;
  signal pmbus_sda           : std_logic;

  signal SA_CLK              : std_logic := '1';
  signal si_lpbk_0           : std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  ext_clk <= not ext_clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (125 MHz)
  SA_CLK <= not SA_CLK after c_sa_clk_period / 2;  -- Serial Gigabit IO sa clock (644 MHz)
  pps_rst <= '0' after c_ext_clk_period * 2;

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up
  pmbus_scl <= 'H';  -- pull up
  pmbus_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(5, c_pps_period, '1', pps_rst, ext_clk, pps);
  ext_pps <= pps;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_disturb2_unb2b_sdp_station_full_wg : entity work.disturb2_unb2b_sdp_station_full_wg
  generic map (
    g_design_name            => "disturb2_unb2b_sdp_station_full_wg",
    g_design_note            => "SIM Disturb2 SDP station full design WG",
    g_sim                    => c_sim,
    g_sim_unb_nr             => c_unb_nr,
    g_sim_node_nr            => c_node_nr,
    g_wpfb                   => c_wpfb_sim,
    g_wpfb_complex           => c_wpfb_complex_sim
  )
  port map (
    -- GENERAL
    CLK          => ext_clk,
    PPS          => pps,
    WDI          => WDI,
    INTA         => INTA,
    INTB         => INTB,

    -- Others
    VERSION      => c_version,
    ID           => c_id,
    TESTIO       => open,

    -- I2C Interface to Sensors
    SENS_SC      => sens_scl,
    SENS_SD      => sens_sda,

    PMBUS_SC     => pmbus_scl,
    PMBUS_SD     => pmbus_sda,
    PMBUS_ALERT  => open,

    -- 1GbE Control Interface
    ETH_CLK      => eth_clk,
    ETH_SGIN     => eth_rxp,
    ETH_SGOUT    => eth_txp,

    -- Transceiver clocks
    SA_CLK       => SA_CLK,
    -- front transceivers
    QSFP_1_RX    => si_lpbk_0,
    QSFP_1_TX    => si_lpbk_0,

    -- LEDs
    QSFP_LED     => open
  );

    u_unb2_board_clk644_pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
    port map (
      refclk_644 => SA_CLK,
      rst_in     => pps_rst,
      clk_156    => tr_ref_clk_156,
      clk_312    => tr_ref_clk_312,
      rst_156    => tr_ref_rst_156,
      rst_312    => open
    );

    u_tr_10GbE: entity tr_10GbE_lib.tr_10GbE
    generic map (
      g_sim           => true,
      g_sim_level     => 1,
      g_nof_macs      => 1,
      g_use_mdio      => false
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644      => SA_CLK,
      tr_ref_clk_312      => tr_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
      tr_ref_clk_156      => tr_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
      tr_ref_rst_156      => tr_ref_rst_156,  -- for 10GBASE-R or for XAUI

      -- MM interface
      mm_rst              => pps_rst,
      mm_clk              => tb_clk,

      -- DP interface
      dp_rst              => pps_rst,
      dp_clk              => ext_clk,

      serial_rx_arr(0)    => si_lpbk_0(0),

      src_out_arr(0)      => tr_10GbE_src_out
    );

  ------------------------------------------------------------------------------
  -- MM slave accesses via file IO
  ------------------------------------------------------------------------------
  tb_clk  <= not tb_clk after c_tb_clk_period / 2;  -- Testbench MM clock

  p_mm_stimuli : process
    variable v_bsn                   : natural;
    variable v_offset                : natural;
    variable v_beamlet_index_offset  : natural;
  begin
    -- Wait for DUT power up after reset
    wait for 1 us;

    proc_common_wait_until_hi_lo(ext_clk, ext_pps);

    ----------------------------------------------------------------------------
    -- Use both beamsets, 0 for BF with normal subbands, 1 for BF with shifted subbands
    ----------------------------------------------------------------------------
    for bset in 0 to c_sdp_N_beamsets - 1 loop
      -- Enable beamlet output
      v_offset := bset * c_mm_span_reg_dp_xonoff;
      mmf_mm_bus_wr(c_mm_file_reg_dp_xonoff, v_offset + 0, 1, tb_clk);

      -- MM beamlet_scale
      -- . write
      v_offset := bset * c_mm_span_reg_bf_scale;
      mmf_mm_bus_wr(c_mm_file_reg_bf_scale, v_offset + 0, c_exp_beamlet_scale, tb_clk);
      proc_common_wait_cross_clock_domain_latency(c_tb_clk_period, c_ext_clk_period, c_common_cross_clock_domain_latency * 2);

      -- . readback
      mmf_mm_bus_rd(c_mm_file_reg_bf_scale, v_offset + 0, rd_data, tb_clk);
      rd_beamlet_scale <= rd_data(15 downto 0);
      proc_common_wait_some_cycles(tb_clk, 1);
      assert TO_UINT(rd_beamlet_scale) = c_exp_beamlet_scale
        report "Wrong MM read beamlet_scale for beamset " & natural'image(bset)
        severity ERROR;
    end loop;

    ----------------------------------------------------------------------------
    -- Enable BS
    ----------------------------------------------------------------------------
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 3,                   0, tb_clk);
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 2,                   1, tb_clk);  -- Init BSN = 0
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 1,  c_nof_clk_per_sync, tb_clk);  -- nof_block_per_sync
    mmf_mm_bus_wr(c_mm_file_reg_bsn_source_v2, 0,        16#00000003#, tb_clk);  -- Enable BS at PPS

    ----------------------------------------------------------------------------
    -- Enable WG
    ----------------------------------------------------------------------------
    --   0 : mode[7:0]           --> off=0, calc=1, repeat=2, single=3)
    --       nof_samples[31:16]  --> <= c_ram_wg_size=1024
    --   1 : phase[15:0]
    --   2 : freq[30:0]
    --   3 : ampl[16:0]
    -- WG at signal input 0
    v_offset := 0 * c_mm_span_reg_diag_wg;
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 1, integer(c_wg_phase_sp_0 * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 2, integer(c_subband_sp_0 * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 3, integer(real(c_wg_ampl_sp_0) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl
    -- WG at signal input 2
    v_offset := 2 * c_mm_span_reg_diag_wg;
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 0, 1024 * 2**16 + 1, tb_clk);  -- nof_samples, mode calc
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 1, integer(c_wg_phase_sp_2 * c_diag_wg_phase_unit), tb_clk);  -- phase offset in degrees
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 2, integer(c_subband_sp_2 * c_sdp_wg_subband_freq_unit), tb_clk);  -- freq
    mmf_mm_bus_wr(c_mm_file_reg_diag_wg, v_offset + 3, integer(real(c_wg_ampl_sp_2) * c_sdp_wg_ampl_lsb), tb_clk);  -- ampl

    -- Read current BSN
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 0, current_bsn_wg(31 downto  0), tb_clk);
    mmf_mm_bus_rd(c_mm_file_reg_bsn_scheduler_wg, 1, current_bsn_wg(63 downto 32), tb_clk);
    proc_common_wait_some_cycles(tb_clk, 1);

    -- Write scheduler BSN to trigger start of WG at next block
    v_bsn := TO_UINT(current_bsn_wg) + 2;
    assert v_bsn <= c_bsn_start_wg
      report "Too late to start WG: " & int_to_str(v_bsn) & " > " & int_to_str(c_bsn_start_wg)
      severity ERROR;
    v_bsn := c_bsn_start_wg;
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 0, v_bsn, tb_clk);  -- first write low then high part
    mmf_mm_bus_wr(c_mm_file_reg_bsn_scheduler_wg, 1,     0, tb_clk);  -- assume v_bsn < 2**31-1

    -- Wait for enough WG data and start of sync interval
    mmf_mm_wait_until_value(c_mm_file_reg_bsn_scheduler_wg, 0,  -- read BSN low
                            "UNSIGNED", rd_data, ">=", c_nof_block_per_sync * 2,  -- this is the wait until condition
                            c_sdp_T_sub, tb_clk);

    ---------------------------------------------------------------------------
    -- Read 10GbE Stream
    ---------------------------------------------------------------------------

    -- Read 3 packets to make sure we get 1 from each beamset. It can happen that two packets
    -- (but not three) from the same beamset are received back to back.
    for packet in 0 to 2 loop
      -- Get beamlet_index from packet header
      proc_common_wait_until_clk_and_high(ext_clk, tr_10GbE_src_out.sop);
      for I in 0 to 8 loop  -- Packet header is 9.25 words wide, which can be discarded
        if I = 7 then
          rx_beamlet_scale <= tr_10GbE_src_out.data(55 downto 40);
          v_beamlet_index_offset := c_sdp_N_pol_bf * TO_UINT(tr_10GbE_src_out.data(39 downto 24));  -- Read beamlet index
          rx_beamlet_index_offset <= v_beamlet_index_offset;
          rx_blocks_per_packet <= tr_10GbE_src_out.data(23 downto 16);
          rx_beamlets_per_block <= tr_10GbE_src_out.data(15 downto 0);
        end if;
        if I = 8 then
          rx_block_period <= tr_10GbE_src_out.data(63 downto 48);
          rx_bsn(63 downto 16) <= tr_10GbE_src_out.data(47 downto 0);
        end if;
        proc_common_wait_until_clk_and_high(ext_clk, tr_10GbE_src_out.valid);
      end loop;
      rx_bsn(15 downto 0) <= tr_10GbE_src_out.data(63 downto 48);

      -- Get all c_sdp_cep_nof_blocks_per_packet = 4 blocks from the packet
      for blk in 0 to c_sdp_cep_nof_blocks_per_packet - 1 loop
        -- . First word contains 1 header part of two bytes and 3 beamlets [0:2].
        -- . expect c_sdp_cep_nof_beamlets_per_block = c_sdp_S_sub_bf = 488 dual pol
        --   and complex beamlets per packet, so 2 dual pol beamlets/64b data word.
        -- . Beamlets array is stored big endian in the data, so X.real index 0 first
        --   in MSByte of tr_10GbE_src_out.data.
        rx_beamlet_blk <= blk;
        rx_beamlet_cnt <= 0;
        rx_beamlet_valid <= '1';
        rx_beamlet_sop <= '1';
        beamlet_arr2_re(v_beamlet_index_offset + 0) <= tr_10GbE_src_out.data(47 downto 40);
        beamlet_arr2_im(v_beamlet_index_offset + 0) <= tr_10GbE_src_out.data(39 downto 32);
        beamlet_arr2_re(v_beamlet_index_offset + 1) <= tr_10GbE_src_out.data(31 downto 24);
        beamlet_arr2_im(v_beamlet_index_offset + 1) <= tr_10GbE_src_out.data(23 downto 16);
        beamlet_arr2_re(v_beamlet_index_offset + 2) <= tr_10GbE_src_out.data(15 downto 8);
        beamlet_arr2_im(v_beamlet_index_offset + 2) <= tr_10GbE_src_out.data(7 downto 0);
        proc_common_wait_until_clk_and_high(ext_clk, tr_10GbE_src_out.valid);
        rx_beamlet_cnt <= rx_beamlet_cnt + 1;
        rx_beamlet_sop <= '0';
        -- . get beamlets during block, there are 4 complex beamlets per 64b word
        for I in 1 to (c_sdp_N_pol_bf * c_sdp_cep_nof_beamlets_per_block / 4) - 1 loop
          beamlet_arr2_re(v_beamlet_index_offset + I * 4 - 1) <= tr_10GbE_src_out.data(63 downto 56);
          beamlet_arr2_im(v_beamlet_index_offset + I * 4 - 1) <= tr_10GbE_src_out.data(55 downto 48);
          beamlet_arr2_re(v_beamlet_index_offset + I * 4 + 0) <= tr_10GbE_src_out.data(47 downto 40);
          beamlet_arr2_im(v_beamlet_index_offset + I * 4 + 0) <= tr_10GbE_src_out.data(39 downto 32);
          beamlet_arr2_re(v_beamlet_index_offset + I * 4 + 1) <= tr_10GbE_src_out.data(31 downto 24);
          beamlet_arr2_im(v_beamlet_index_offset + I * 4 + 1) <= tr_10GbE_src_out.data(23 downto 16);
          beamlet_arr2_re(v_beamlet_index_offset + I * 4 + 2) <= tr_10GbE_src_out.data(15 downto 8);
          beamlet_arr2_im(v_beamlet_index_offset + I * 4 + 2) <= tr_10GbE_src_out.data(7 downto 0);
          proc_common_wait_until_clk_and_high(ext_clk, tr_10GbE_src_out.valid);
          rx_beamlet_cnt <= rx_beamlet_cnt + 1;
        end loop;
        -- . get last beamlet of block
        beamlet_arr2_re(v_beamlet_index_offset + c_sdp_N_pol_bf * c_sdp_cep_nof_beamlets_per_block - 1) <= tr_10GbE_src_out.data(63 downto 56);
        beamlet_arr2_im(v_beamlet_index_offset + c_sdp_N_pol_bf * c_sdp_cep_nof_beamlets_per_block - 1) <= tr_10GbE_src_out.data(55 downto 48);
        -- Loop for next block in packet
      end loop;
      -- Make rx_beamlet_valid low for next header or after loop during verify.
      -- Cannot wait one ext_clk cycle here to include last beamlet of block,
      -- because next packet sop may follow immediately after this packet eop.
      rx_beamlet_valid <= '0';

      -- Loop and wait for next packet.
    end loop;

    ---------------------------------------------------------------------------
    -- Verify 10GbE UDP offload
    ---------------------------------------------------------------------------
    print_str("");
    print_str("WG:");
    print_str(". sp_0 at mid subband,");
    print_str(". sp_2 at subband edge, so at mid of os subband.");
    print_str(". c_wg_ampl_sp_0            = " & int_to_str(c_wg_ampl_sp_0));
    print_str(". c_wg_ampl_sp_2            = " & int_to_str(c_wg_ampl_sp_2));
    print_str(". c_subband_sp_0            = " & real_to_str(c_subband_sp_0, 20, 6));
    print_str(". c_subband_sp_2            = " & real_to_str(c_subband_sp_2, 20, 6));
    print_str("WPFB:");
    print_str(". c_exp_subband_ampl_sp_0   = " & real_to_str(c_exp_subband_ampl_sp_0, 20, 6));
    print_str(". c_exp_subband_ampl_sp_2   = " & real_to_str(c_exp_subband_ampl_sp_2, 20, 6));
    print_str("");
    print_str("Beamlet output: (sp_0 at mid subband, sp_2 at subband edge, so at mid of os subband):");
    print_str(". c_exp_beamlet_index       = " & int_to_str(c_exp_beamlet_index));
    print_str(". c_exp_beamlet_index_os    = " & int_to_str(c_exp_beamlet_index_os));
    print_str(". c_exp_beamlet_ampl_sp_0   = " & real_to_str(c_exp_beamlet_ampl_sp_0, 20, 6));
    print_str(". c_exp_beamlet_ampl_sp_2   = " & real_to_str(c_exp_beamlet_ampl_sp_2, 20, 6));
    print_str(". c_exp_beamlet_phase       = " & real_to_str(c_exp_beamlet_phase, 20, 6));
    print_str("");
    print_str(". c_beamlet_re at index     = " & int_to_str(TO_SINT(beamlet_arr2_re(c_exp_beamlet_index))));
    print_str(". c_beamlet_re at index_os  = " & int_to_str(TO_SINT(beamlet_arr2_re(c_exp_beamlet_index_os))));
    print_str(". c_exp_beamlet_re_sp_0     = " & int_to_str(integer(c_exp_beamlet_re_sp_0)));
    print_str(". c_exp_beamlet_re_sp_2     = " & int_to_str(integer(c_exp_beamlet_re_sp_2)));
    print_str("");
    print_str(". c_beamlet_im at index     = " & int_to_str(TO_SINT(beamlet_arr2_im(c_exp_beamlet_index))));
    print_str(". c_beamlet_im at index_os  = " & int_to_str(TO_SINT(beamlet_arr2_im(c_exp_beamlet_index_os))));
    print_str(". c_exp_beamlet_im_sp_0     = " & int_to_str(integer(c_exp_beamlet_im_sp_0)));
    print_str(". c_exp_beamlet_im_sp_2     = " & int_to_str(integer(c_exp_beamlet_im_sp_2)));

    -- WG at subband center will yield same subband value in every subband period
    assert signed(beamlet_arr2_re(c_exp_beamlet_index)) = c_exp_beamlet_re_sp_0
      report "Wrong 10GbE beamlet output /= c_exp_beamlet_re_sp_0 in beamset 0"
      severity ERROR;
    assert signed(beamlet_arr2_im(c_exp_beamlet_index)) = c_exp_beamlet_im_sp_0
      report "Wrong 10GbE beamlet output /= c_exp_beamlet_im_sp_0 in beamset 0"
      severity ERROR;
    -- WG at subband edge will change phase 180 degrees in every subband period, so expect factor +-1
    assert signed(beamlet_arr2_re(c_exp_beamlet_index_os)) = c_exp_beamlet_re_sp_2 or
           signed(beamlet_arr2_re(c_exp_beamlet_index_os)) = -c_exp_beamlet_re_sp_2
      report "Wrong 10GbE beamlet output /= c_exp_beamlet_re_sp_2 in beamset 1 (shifted subbands)"
      severity ERROR;
    assert signed(beamlet_arr2_im(c_exp_beamlet_index_os)) = c_exp_beamlet_im_sp_2 or
           signed(beamlet_arr2_im(c_exp_beamlet_index_os)) = -c_exp_beamlet_im_sp_2
      report "Wrong 10GbE beamlet output /= c_exp_beamlet_im_sp_2 in beamset 1 (shifted subbands)"
      severity ERROR;

    ---------------------------------------------------------------------------
    -- End Simulation
    ---------------------------------------------------------------------------
    sim_done <= '1';
    proc_common_wait_some_cycles(ext_clk, 100);
    proc_common_stop_simulation(true, ext_clk, sim_done, tb_end);
    wait;
  end process;
end tb;
