-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Provide MM access to the ST output of a FIFO.
-- Description:
--   An MM master can directly read the FIFO, provided that the ST data width
--   fits within the MM data width (g_mm_word_w) and that both have the same
--   latency (read latency = ready latency = 1).
--   The MM master can read the mm_usedw[] value to know how many valid data
--   words it can read in a loop, until it needs to check mm_usedw[] again.
--   When mm_usedw[] > 0 then the MM master can be sure that the read
--   mm_rddata will be valid. Therefore it is not needed to pass on
--   snk_in.valid and in this way the MM master does not need to check whether
--   there is valid data in the FIFO for every read fifo data access.
-- Remark:
-- . Assume the FIFO will never go completely full, so usedw[] only needs to
--   fit values <= g_fifo_size-1
-- . This dp_fifo_to_mm connects to the MM clock domain side of the FIFO, hence
--   is operates entirely in the MM clock domain.
-- . The ST sop, eop, empty, channel and error fields are not supported.

entity dp_fifo_to_mm is
  generic (
    g_fifo_size   : natural := 512;
    g_mm_word_w   : natural := c_word_w  -- = 32
  );
  port (
    rst           : in  std_logic;  -- MM clock domain
    clk           : in  std_logic;
    -- ST sink connected to FIFO output
    snk_out       : out t_dp_siso;
    snk_in        : in  t_dp_sosi;
    usedw         : in  std_logic_vector(ceil_log2(g_fifo_size) - 1 downto 0);
    -- Control for FIFO read access
    mm_rd         : in  std_logic;  -- MM read pulse to read the mm_rddata from snk_in.data
    mm_rddata     : out std_logic_vector(g_mm_word_w - 1 downto 0);
    mm_rdval      : out std_logic;
    mm_usedw      : out std_logic_vector(g_mm_word_w - 1 downto 0)
  );
end dp_fifo_to_mm;

architecture str of dp_fifo_to_mm is
begin
  -- Wires
  mm_usedw <= RESIZE_UVEC(usedw, g_mm_word_w);

  -- XON fixed to '1' after reset
  snk_out.xon <= not rst;

  snk_out.ready <= mm_rd;  -- Combinatorial, because FIFO RL=1 already fits MM read latency=1
  mm_rddata     <= RESIZE_UVEC(snk_in.data, g_mm_word_w);
  mm_rdval      <= snk_in.valid;
end str;
