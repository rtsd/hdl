/******************************************************************************
*                                                                             *
* License Agreement                                                           *
*                                                                             *
* Copyright (c) 2006 Altera Corporation, San Jose, California, USA.           *
* All rights reserved.                                                        *
*                                                                             *
* Permission is hereby granted, free of charge, to any person obtaining a     *
* copy of this software and associated documentation files (the "Software"),  *
* to deal in the Software without restriction, including without limitation   *
* the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
* and/or sell copies of the Software, and to permit persons to whom the       *
* Software is furnished to do so, subject to the following conditions:        *
*                                                                             *
* The above copyright notice and this permission notice shall be included in  *
* all copies or substantial portions of the Software.                         *
*                                                                             *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
* DEALINGS IN THE SOFTWARE.                                                   *
*                                                                             *
* This agreement shall be governed in all respects by the laws of the State   *
* of California and by the laws of the United States of America.              *
*                                                                             *
* Altera does not recommend, suggest or require that this reference design    *
* file be used in conjunction or combination with any other product.          *
******************************************************************************/

/*
 * This file provides a very minimal printf implementation for use with very
 * small applications.  Only the following format strings are supported:
 *   %x
 *   %s
 *   %c
 *   %%
 */
#include <system.h>
#include <osx_stdio.h>
#include "altera_avalon_jtag_uart_regs.h"

typedef void (*output_fn)(char , char*);


/* soo = start of output */
int fmt_function(output_fn out, char* soo, const char* fmt, va_list args) {
    char              c;
    const char*       w;
    const char* const p = soo;

    /* Process format string. */
    w = fmt;
    while( (c=*w++)!=0 ) {
        /* If not a format escape character, just print  */
        /* character.  Otherwise, process format string. */
        if( c!='%' ) {
			out(c, soo++);	
        } else {
            /* Get format character.  If none     */
            /* available, processing is complete. */
			if( (c=*w++)==0 )
				break;
			/* Bollox. Need to to woik! */
			switch( c ) {
				case '%':
                    /* Process "%" escape sequence. */
					out(c, soo++);
					break;

				case 'c':
					{
                    	int v = va_arg(args, int);
	                    out((char)v, soo++);
					}
					break;

				case 'x':
					{
						/* Process hexadecimal number format. */
						int           digit_shift;
						unsigned long v = va_arg(args, unsigned long);
						unsigned long digit;

						out('0', soo++);
						out('x', soo++);

						/* If the number value is zero, just print and continue. */
						if( v==0 ) {
							out('0', soo++);
							break;
						}

						/* Find first non-zero digit. */
						digit_shift = 28;
						while( !(v & (0xF<<digit_shift)) )
							digit_shift -= 4;

						/* Print digits. */
						for( ; digit_shift>=0; digit_shift-=4 ) {
							digit = (v & (0xF << digit_shift)) >> digit_shift;
							if( digit<=9 )
								c = '0' + digit;
							else
								c = 'a' + digit - 10;
							out(c, soo++);
						}
					}
					break;

				case 'd':
					{
						int   v = va_arg(args, int);
						char  digitbuf[ ((sizeof(int) * 8)/3) + 1 ];
						char* dptr = &digitbuf[0];

						/* we must, in this case, print at least one digit */
						if( v==0 )
							*dptr++ = '0';
						/* figure out which digits to print.
						 * this will only do something if v!=0 yet
						 */
						while( v ) {
							*dptr++ = '0' + (v%10);
							v /= 10;
						}
						/* the digits we must output are now in reverse order
						 * so we print them starting from the end.
						 * Note: dptr points one *past* the character to 
						 * output!
						 */
						while( --dptr>=&digitbuf[0] )
							out(*dptr, soo++);
					}
					break;

				case 's':
					{
                    	/* Process string format. */
						char *s = va_arg(args, char *);

						while( *s )
							out(*s++, soo++);
					}
					break;

				default:
					break;
			}
        }
    }
	out('\0', soo++);
    return (int)(soo-p);
}

/* Put functions: to stdout or to string */
void osx_putchar(char c, char* _ptr) {
	unsigned int          ntry = 32;
	volatile unsigned int control;
	if( c!='\0' ) {
		do {
			control = IORD_ALTERA_AVALON_JTAG_UART_CONTROL(JTAG_UART_0_BASE);
			
			if( (control&ALTERA_AVALON_JTAG_UART_CONTROL_WSPACE_MSK)!=0 ) {
				IOWR_ALTERA_AVALON_JTAG_UART_DATA(JTAG_UART_0_BASE, c);
				break;
			} 
#if 0
			else {
				// buffer was full - apparently - so if we do not
				// detect JTAG activity since previous I/O we 
				// do not output this character but carry on
				// (otherwise the firmware would hang)
				if( (control&ALTERA_AVALON_JTAG_UART_CONTROL_AC_MSK)==0 ) {
					// AC(tivity) bit NOT set -> no-one is lissnin!
					break;
				}
			}
#endif
		} while( --ntry );
	}
	// Reset the ACtivity bit (by writing a '1') so we can always detect if someone
	// starts lissnin
//	IOWR_ALTERA_AVALON_JTAG_UART_CONTROL(JTAG_UART_0_BASE, ALTERA_AVALON_JTAG_UART_CONTROL_AC_MSK);
	return;
}

#if 0
void osx_sputchar(char c, char* ptr) {
	*ptr = c;
}

int osx_vsprintf(char* str, const char* fmt, va_list args ) {
	return fmt_function(osx_sputchar, str, fmt, args);
}

/* OSX sprintf function */
int osx_sprintf(char* str, const char* fmt, ... ) {
	int     r;
	va_list args;
	va_start(args, fmt);
	r = fmt_function(osx_sputchar, str, fmt, args);
	va_end(args);
	return r;
}
#endif
int osx_printf(const char* fmt, ...) {
	int     r;
	va_list args;
	va_start(args, fmt);
	r = fmt_function(osx_putchar, 0, fmt, args);
	va_end(args);
	return r;
}

