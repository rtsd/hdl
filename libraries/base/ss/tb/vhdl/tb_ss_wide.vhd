-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

-- Usage:
-- > as 10
-- > run -all
-- . Observe in_sosi_arr and out_sosi_arr in the Wave window
--
-- Description:

entity tb_ss_wide is
end tb_ss_wide;

architecture tb of tb_ss_wide is
  constant c_clk_period             : time := 10 ns;

  constant c_rl                   : natural := 1;
  constant c_dsp_data_w           : natural := 16;

  constant c_nof_sync             : natural := 5;

  constant c_wb_factor            : natural := 4;
  constant c_nof_ch_in            : natural := 256;
  constant c_nof_ch_sel           : natural := 192;
  constant c_nof_block_per_sync   : natural := 4;

  constant c_adr_incr             : natural := 2**ceil_log2(c_nof_ch_sel / c_wb_factor);
  constant c_select_file_prefix   : string  := "../../../src/data/select_buf";

  signal rst             : std_logic;
  signal clk             : std_logic := '1';
  signal tb_end          : std_logic;

  signal mm_mosi         : t_mem_mosi;
  signal mm_miso         : t_mem_miso;
  signal mm_done         : std_logic;

  signal st_en           : std_logic := '1';
  signal st_siso_arr     : t_dp_siso_arr(c_wb_factor - 1 downto 0) := (others => c_dp_siso_rdy);
  signal st_sosi_arr     : t_dp_sosi_arr(c_wb_factor - 1 downto 0) := (others => c_dp_sosi_rst);

  signal bsn             : natural := 10;

  signal in_sosi_arr     : t_dp_sosi_arr(c_wb_factor - 1 downto 0) := (others => c_dp_sosi_rst);

  signal out_siso_arr    : t_dp_siso_arr(c_wb_factor - 1 downto 0) := (others => c_dp_siso_rdy);
  signal out_sosi_arr    : t_dp_sosi_arr(c_wb_factor - 1 downto 0);
begin
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  -- MM domain
  p_mm_stimuli : process
  begin
    mm_mosi <= c_mem_mosi_rst;
    mm_done <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    for J in 0 to c_wb_factor - 1 loop
      for I in 0 to c_nof_ch_sel / c_wb_factor - 1 loop
        proc_mem_mm_bus_wr(J * c_adr_incr + I, I, clk, mm_mosi);  -- Write the default SS map: (0, 1, 2, ... c_nof_ch_sel-1) ==>  (0, 1, 2, ... c_nof_ch_sel-1)
      end loop;
    end loop;
    mm_done <= '1';

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Data blocks
  ------------------------------------------------------------------------------
  gen_stimuli : for K in 0 to c_wb_factor - 1 generate
    p_st_stimuli : process
      variable v_re  : natural := 0;
      variable v_im  : natural := 1;
    begin
      tb_end <= '0';
      st_sosi_arr(K) <= c_dp_sosi_rst;
      proc_common_wait_until_high(clk, mm_done);

      -- Run some sync intervals with DSP counter data for the real and imag fields
      wait until rising_edge(clk);
      for I in 0 to c_nof_sync - 1 loop
        proc_dp_gen_block_data(c_rl, false, c_dsp_data_w, c_dsp_data_w, 0, v_re, v_im, c_nof_ch_in / c_wb_factor, 0, 0, '1', "0", clk, st_en, st_siso_arr(K), st_sosi_arr(K));  -- next sync
        v_re := v_re + c_nof_ch_in;
        v_im := v_im + c_nof_ch_in;
        for J in 0 to c_nof_block_per_sync - 2 loop  -- provide sop and eop for block reference
          proc_dp_gen_block_data(c_rl, false, c_dsp_data_w, c_dsp_data_w, 0, v_re, v_im, c_nof_ch_in / c_wb_factor, 0, 0, '0', "0", clk, st_en, st_siso_arr(K), st_sosi_arr(K));  -- no sync
          v_re := v_re + c_nof_ch_in;
          v_im := v_im + c_nof_ch_in;
        end loop;
      end loop;
      st_sosi_arr(K) <= c_dp_sosi_rst;
      proc_common_wait_some_cycles(clk, 100);
      -- Run some sync intervals more
      wait until rising_edge(clk);
      for I in 0 to c_nof_sync - 1 loop
        proc_dp_gen_block_data(c_rl, false, c_dsp_data_w, c_dsp_data_w, 0, v_re, v_im, c_nof_ch_in / c_wb_factor, 0, 0, '1', "0", clk, st_en, st_siso_arr(K), st_sosi_arr(K));  -- next sync
        v_re := v_re + c_nof_ch_in;
        v_im := v_im + c_nof_ch_in;
        for J in 0 to c_nof_block_per_sync - 2 loop  -- provide sop and eop for block reference
          proc_dp_gen_block_data(c_rl, false, c_dsp_data_w, c_dsp_data_w, 0, v_re, v_im, c_nof_ch_in / c_wb_factor, 0, 0, '0', "0", clk, st_en, st_siso_arr(K), st_sosi_arr(K));  -- no sync
          v_re := v_re + c_nof_ch_in;
          v_im := v_im + c_nof_ch_in;
        end loop;
      end loop;
      proc_common_wait_some_cycles(clk, 10);
      tb_end <= '1';
      wait;
    end process;
  end generate;

  -- Time stimuli
  bsn <= bsn + 1 when rising_edge(clk) and (st_sosi_arr(0).eop = '1');  -- OR st_sosi.sync='1');

  -- Add BSN to the ST data
  p_in_sosi : process(st_sosi_arr, bsn)
  begin
    for I in 0 to c_wb_factor - 1 loop
      in_sosi_arr(I)     <= st_sosi_arr(I);
      in_sosi_arr(I).bsn <= TO_DP_BSN(bsn);
    end loop;
  end process;

  u_dut : entity work.ss_wide
  generic map (
    g_wb_factor          => c_wb_factor,
    g_dsp_data_w         => c_dsp_data_w,
    g_nof_ch_in          => c_nof_ch_in,
    g_nof_ch_sel         => c_nof_ch_sel,
    g_select_file_prefix => c_select_file_prefix
  )
  port map (
    mm_rst         => rst,
    mm_clk         => clk,
    dp_rst         => rst,
    dp_clk         => clk,

    -- Memory Mapped
    ram_ss_ss_wide_mosi => mm_mosi,
    ram_ss_ss_wide_miso => mm_miso,

    -- Streaming
    input_sosi_arr     => in_sosi_arr,

    output_sosi_arr    => out_sosi_arr,
    output_siso_arr    => out_siso_arr
  );
end tb;
