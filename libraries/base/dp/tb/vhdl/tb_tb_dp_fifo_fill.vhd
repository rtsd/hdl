-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_fifo_fill is
end tb_tb_dp_fifo_fill;

architecture tb of tb_tb_dp_fifo_fill is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 10
  -- > run 300 us                 --> OK

  -- Try FIFO settings : GENERIC MAP (g_dut_use_bsn, g_dut_use_empty, g_dut_use_channel, g_dut_use_sync, g_dut_fifo_rl, g_dut_fifo_size, g_dut_fifo_fill)

  u_rl_1_fill_0          : entity work.tb_dp_fifo_fill generic map (false, false, false, false, 1, 64, 0);  -- no fill ==> dp_fifo_sc
  u_rl_1_fill_1          : entity work.tb_dp_fifo_fill generic map (false, false, false, false, 1, 64, 1);
  u_rl_1_fill_3          : entity work.tb_dp_fifo_fill generic map (false, false, false, false, 1, 64, 3);
  u_rl_1_fill_30         : entity work.tb_dp_fifo_fill generic map (false, false, false, false, 1, 64, 30);
  u_rl_1_fill_size       : entity work.tb_dp_fifo_fill generic map (false, false, false, false, 1, 64, 64);
  u_rl_1_fill_30_bsn     : entity work.tb_dp_fifo_fill generic map (true,  false, false, false, 1, 64, 30);
  u_rl_1_fill_30_empty   : entity work.tb_dp_fifo_fill generic map (false, true,  false, false, 1, 64, 30);
  u_rl_1_fill_30_channel : entity work.tb_dp_fifo_fill generic map (false, false, true,  false, 1, 64, 30);
  u_rl_1_fill_30_sync    : entity work.tb_dp_fifo_fill generic map (false, false, false, true,  1, 64, 30);
  u_rl_1_fill_30_all     : entity work.tb_dp_fifo_fill generic map (true,  true,  true,  true,  1, 64, 30);

  u_rl_0_fill_0          : entity work.tb_dp_fifo_fill generic map (false, false, false, false, 0, 64, 0);  -- no fill ==> dp_fifo_sc
  u_rl_0_fill_1          : entity work.tb_dp_fifo_fill generic map (false, false, false, false, 0, 64, 1);
end tb;
