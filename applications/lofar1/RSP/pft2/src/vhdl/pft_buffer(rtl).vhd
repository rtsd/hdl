library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library common_lib;
use common_lib.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

architecture rtl of pft_buffer is
  constant c_latency       : natural := 2;
  constant c_adr_w         : natural := g_fft_size_w + 1;
  constant c_nof_words     : natural := 2**c_adr_w;

  constant c_ram   : t_c_mem := (latency  => c_latency,
                                 adr_w    => c_adr_w,
                                 dat_w    => 2 * g_dat_w,
                                 nof_dat  => c_nof_words,  -- <= 2**g_addr_w
                                 init_sl  => '0');

  signal rd_dat            : std_logic_vector(2 * g_dat_w - 1 downto 0);
  signal wr_dat            : std_logic_vector(rd_dat'range);
  signal rd_adr_paged      : std_logic_vector(c_adr_w - 1 downto 0);
  signal wr_adr_paged      : std_logic_vector(c_adr_w - 1 downto 0);
  signal wr_adr            : std_logic_vector(g_fft_size_w - 1 downto 0);
  signal nxt_wr_adr        : std_logic_vector(wr_adr'range);
  signal wr_page           : std_logic;
  signal nxt_wr_page       : std_logic;
  signal rd_page           : std_logic;
  signal nxt_rd_page       : std_logic;
  signal wr_en             : std_logic;

  signal pipe_val          : std_logic_vector(c_latency - 1 downto 0);
  signal nxt_pipe_val      : std_logic_vector(pipe_val'range);

  function bit_rev(adr : in std_logic_vector) return std_logic_vector is
    variable result: std_logic_vector(adr'range);
  begin
    for i in adr'high downto 0 loop
      result(i) := adr(adr'high - i);
    end loop;
    return result;
  end function;
begin
  nxt_rd_page <= not nxt_wr_page;
  rd_adr_paged <= rd_page & rd_adr;

  registers : process (rst, clk)
  begin
    if rst = '1' then
      pipe_val   <= (others => '0');
      wr_adr     <= (others => '0');
      wr_page    <= '0';
      rd_page    <= '1';
    elsif rising_edge(clk) then
      pipe_val   <= nxt_pipe_val;
      wr_adr     <= nxt_wr_adr;
      wr_page    <= nxt_wr_page;
      rd_page    <= nxt_rd_page;
    end if;
  end process;

  pipe_proc: process(pipe_val, rd_en)
  begin
    nxt_pipe_val  <= rd_en & pipe_val (pipe_val 'high downto 1);
    rd_val  <= pipe_val(0);
  end process;

  wr_adr_proc : process (wr_adr, wr_en, wr_page, wr_sync)
  begin
    nxt_wr_adr  <= wr_adr;
    nxt_wr_page <= wr_page;
    rd_rdy      <= '0';
    rd_sync     <= '0';
    if wr_en = '1' then
      if signed(wr_adr) = -1 or wr_sync = '1' then
        rd_rdy      <= '1';
        rd_sync     <= wr_sync;
        nxt_wr_page <= not wr_page;
        nxt_wr_adr  <= (others => '0');
      else
        nxt_wr_adr <= std_logic_vector(unsigned(wr_adr) + 1);
      end if;
    end if;
    wr_adr_paged <= wr_page & bit_rev(wr_adr);
  end process;

  -- combine real and imaginary to a single data word
  rd_proc : process (rd_dat, pipe_val)
  begin
    rd_re  <= rd_dat(rd_dat'high downto rd_im'length);
    rd_im  <= rd_dat(rd_im'range);
    -- synthesis translate off
    if pipe_val(0) = '0' then
      rd_re <= (others => '0');
      rd_im <= (others => '0');
    end if;
    --synthesis translate on
  end process;

  wr_dat <= wr_re & wr_im;
  wr_en  <= wr_val;

--  -- ram module
--  ram : ENTITY common_lib.common_dpram
--  GENERIC MAP (
--    g_dat_w                => 2*g_dat_w,
--    g_adr_w                => c_adr_w,
--    g_nof_words            => c_nof_words
--  )
--  PORT MAP (
--    rd_dat                 => rd_dat,
--    rd_adr                 => rd_adr_paged,
--    rd_en                  => rd_en,
--    wr_dat                 => wr_dat,
--    wr_adr                 => wr_adr_paged,
--    wr_en                  => wr_en,
--    clk                    => clk,
--    rst                    => rst
--  );

  ram : entity common_lib.common_ram_r_w
  generic map (
    g_ram        => c_ram
  )
  port map (
    rst       => rst,
    clk       => clk,
    wr_en     => wr_en,
    wr_adr    => wr_adr_paged,
    wr_dat    => wr_dat,
    rd_en     => rd_en,
    rd_adr    => rd_adr_paged,
    rd_dat    => rd_dat
  );
end rtl;
