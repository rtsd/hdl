--   
--           _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
--         _/                   _/  ____________________________________________ 
--         _/                  _/  /                                           / 
--         _/_/               _/  /                                   M25P128 / 
--          _/_/_/           _/  /                                           /  
--             _/_/         _/  /                                   128Mbit / 
--              _/_/       _/  /                   Low Voltage, SPI, 50Mhz / 
--                _/      _/  /                                           / 
--                _/     _/  /                     VHDL Behavioral Model / 
--                _/    _/  /                               Version 1.0 / 
--             _/_/    _/  /                                           /
--          _/_/_/    _/  /     Copyright (c) 2007 STMicroelectronics / 
--        _/_/_/     _/  /___________________________________________/ 
--  _/_/_/_/_/      _/    
--  
--
-------------------------------------------------------
-- Author: Hugues CREUSY
--February 2004
-- VHDL model
-- project: M25P64 50 MHz,
-- release: 1.0
-----------------------------------------------------
-- Unit   : M25P128 driver
-----------------------------------------------------

-------------------------------------------------------------
-- These VHDL models are provided "as is" without warranty
-- of any kind, included but not limited to, implied warranty
-- of merchantability and fitness for a particular purpose.
-------------------------------------------------------------

-------------------------------------------------------------
--				M25P64 DRIVER
-------------------------------------------------------------

LIBRARY IEEE;
	USE IEEE.STD_LOGIC_1164.ALL;
LIBRARY WORK;
	USE WORK.stimuli_spi.ALL;

-------------------------------------------------------------
--				ENTITY
-------------------------------------------------------------
ENTITY Driver IS

 PORT(	VCC: OUT REAL;
	clk: OUT std_logic;
	din: OUT std_logic;
	cs_valid: OUT std_logic;
	hard_protect: OUT std_logic;
	hold: OUT std_logic
	);

END Driver;

architecture stimuli of Driver is 

begin
driver: process

	CONSTANT thigh : TIME := 10 ns;
	CONSTANT tlow  : TIME := 10 ns;

	begin

clk<='0';
din<='1';
cs_valid<='0';
hold<='1';
hard_protect<='1';

-- ALIM(25 ns ,100 ns, 1 ms,3 ms,3.2,2.7,2.5,0.2,0.05,Vcc);
ALIM(1 ns ,5 ns, 1 ms,3 ms,0.0,2.5,2.7,3.5,3.3,Vcc);
WAIT FOR 15 ms;

Read_ID (thigh,tlow,23,clk,din,cs_valid);
   WAIT FOR 10*tLOW;
-- hold condition test during a WREN
READ (thigh,tlow,"000000000000000000000000", 1, clk,din,cs_valid);
	  WAIT FOR 10*tLOW;
READ (thigh,tlow,"000000000000000000000000", 1, clk,din,cs_valid);
	  WAIT FOR 10*tLOW;
	WAIT FOR 5010 us;
fast_READ (thigh,tlow,"000000000000000000000010", 2, clk,din,cs_valid); -- Read last byte of Sector 30, and then first byte of Sector 31
	WAIT FOR 10*tLOW;
fast_READ (thigh,tlow,"000000000000000000000011", 2, clk,din,cs_valid); -- Read the last byte of the memory , and then the first one
	WAIT FOR 10*tLOW;
-- WRSR to reset BP(i) bits
WREN(thigh,tlow,clk,din,cs_valid);
          WAIT FOR 10*tLOW;
WRSR (thigh,tlow,"00000000",clk,din,cs_valid);
          WAIT FOR 14965 us; 
RDSR(thigh,tlow,120,clk,din,cs_valid); 
          WAIT FOR 10*tLOW;
-- page prog on a not protected sector
-- WREN(thigh,tlow,clk,din,cs_valid);
--           WAIT FOR 10*tLOW;  
PP (thigh,tlow,  "000011111100001111000011","10101010", 15, clk,din,cs_valid);
--          WAIT FOR 4965 us; 
-- RDSR during the prog cycle
RDSR(thigh,tlow,120,clk,din,cs_valid); 
          WAIT FOR 10*tLOW;
          WAIT FOR 4965 us;
-- RDSR at the end of the prog cycle
RDSR(thigh,tlow,120,clk,din,cs_valid);
          WAIT FOR 10*tLOW;



ALIM(25 ns ,100 ns, 1 ms,3 ms,3.2,2.7,2.5,0.2,0.05,Vcc);
WAIT;
end process;
	
end stimuli;
