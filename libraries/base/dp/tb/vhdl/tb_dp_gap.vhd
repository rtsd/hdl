-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_gap is
end tb_dp_gap;

architecture tb of tb_dp_gap is
  constant c_clk_period   : time    := 10 ns;
  constant c_dat_w        : natural := c_dp_stream_data_w;  -- use full SOSI data width to ease viewing in wave window

  constant c_rl           : natural := 1;
  constant c_tx_init      : natural := 0;

  signal rst              : std_logic;
  signal clk              : std_logic := '1';

  signal tx_enable        : std_logic;

  signal tx_siso          : t_dp_siso;
  signal tx_sosi          : t_dp_sosi;

  signal gap_siso         : t_dp_siso;
  signal gap_sosi         : t_dp_sosi;

  signal verify_en        : std_logic;
  signal prev_ready1      : std_logic;
  signal prev_ready2      : std_logic_vector(0 downto 0);
  signal prev_data1       : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev_data2       : std_logic_vector(c_dat_w - 1 downto 0);
begin
  -- Run for 12us

  rst <= '1', '0' after c_clk_period * 7;
  clk <= not clk after c_clk_period / 2;

  p_stim : process
  begin
    tx_enable      <= '0';

    gap_siso.ready <= '0';
    wait until rst = '0';
    wait until rising_edge(clk);  -- get synchronous to clk
    tx_enable      <= '1';

    -- Run for 200 cycles; dp_gap should not interfere with the ready signal here.
    wait for c_clk_period * 200;

    -- Assert ready of sink connected to DUT. dp_gap should insert 5-cycle gaps every 100 cycles.
    gap_siso.ready <= '1';
    wait for c_clk_period * 500;

    -- Deassert sink's ready and re-assert after 100 cycles. Dp_gap should reset its counters
    -- after the first 5 invalid cycles already: it does not touch its ready signal because
    -- a very long gap occured on its own.
    gap_siso.ready <= '0';  -- dp_gap will wait in s_wait_for_val here
    wait for c_clk_period * 100;
    gap_siso.ready <= '1';

    -- Now let's create a gap of exactly 5 cycles
    wait for c_clk_period * 50;
    gap_siso.ready <= '0';
    wait for c_clk_period * 5;  -- gap of 5 cycles
    gap_siso.ready <= '1';  -- dp_gap should reset its counters

    -- As a final test we'll create a series of smaller (than 5) gaps. dp_gap should force a gap
    -- of 5 cycles every 100 cycles.
    wait for c_clk_period * 10;
    -- ...a gap of 1 cycle:
    gap_siso.ready <= '0';
    wait for c_clk_period * 1;
    gap_siso.ready <= '1';
    wait for c_clk_period * 10;
    -- ...a gap of 2 cycles:
    gap_siso.ready <= '0';
    wait for c_clk_period * 2;
    gap_siso.ready <= '1';
    wait for c_clk_period * 10;
    -- ...a gap of 3 cycles:
    gap_siso.ready <= '0';
    wait for c_clk_period * 3;
    gap_siso.ready <= '1';
    wait for c_clk_period * 10;
    --...and  a gap of 4 cycles:
    gap_siso.ready <= '0';
    wait for c_clk_period * 4;
    gap_siso.ready <= '1';

    -- Let run for another 100 cycles, deassert sink's ready to indicate end of test.
    wait for c_clk_period * 100;
    gap_siso.ready <= '0';

    wait;
  end process;

   -- Generate tx_sosi for DUT using counter data generator
  proc_dp_gen_data(c_rl, c_dat_w, c_tx_init, rst, clk, tx_enable, tx_siso, tx_sosi);

  -- Enable verification when valid output data is expected, i.e. when prev_data should be /= 'X'
  verify_en <= '0', '1' after c_clk_period * 221;

  -- Verify dut output stream ready - valid relation, prev_ready is an auxiliary signal needed by the proc
  proc_common_verify_valid(c_rl, clk, verify_en, gap_siso.ready, prev_ready1, gap_sosi.valid);
  proc_dp_verify_valid(    c_rl, clk, verify_en, gap_siso.ready, prev_ready2, gap_sosi.valid);

  -- Verify DUT output incrementing data, prev_data is an auxiliary signal needed by the proc
  proc_common_verify_data(             c_rl, clk, verify_en, gap_siso.ready, gap_sosi.valid, gap_sosi.data(c_dat_w - 1 downto 0), prev_data1);
  proc_dp_verify_data("gap_sosi.data", c_rl, clk, verify_en, gap_siso.ready, gap_sosi.valid, gap_sosi.data(c_dat_w - 1 downto 0), prev_data2);

  dut : entity work.dp_gap
  generic map (
    g_dat_len => 100,
    g_gap_len => 5,
    g_gap_extend => true
  )
  port map (
    rst       => rst,
    clk       => clk,

    snk_out   => tx_siso,
    snk_in    => tx_sosi,

    src_in    => gap_siso,
    src_out   => gap_sosi
  );
end tb;
