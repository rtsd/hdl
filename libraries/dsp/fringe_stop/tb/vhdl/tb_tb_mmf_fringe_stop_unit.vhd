-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose:  Testbench for the fringe_stop_unit using tb_mmf_fringe_stop_unit
--
-- Usage
--   > as 5
--   > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_mmf_fringe_stop_unit is
end tb_tb_mmf_fringe_stop_unit;

architecture tb of tb_tb_mmf_fringe_stop_unit is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
--    g_tb_index                  : NATURAL := 0;      -- use different index to avoid MM file conflict in multi tb
--    g_mm_stimuli_enable         : BOOLEAN := FALSE;  -- use TRUE to activate p_mm_stimuli, else FALSE to apply mm_file IO via Python
--    g_mm_clk_faster_than_dp_clk : BOOLEAN := TRUE;   -- when TRUE than mm_clk > dp_clk.
--    g_in_valid_gapsize          : NATURAL := 0

  u_mm_slower_no_gap    : entity work.tb_mmf_fringe_stop_unit generic map (0, true, false, 0);  -- use no gap to enable verification of phasor period
  u_mm_faster_with_gap  : entity work.tb_mmf_fringe_stop_unit generic map (1, true, true,  1);  -- use gap to verify valid gaps
end tb;
