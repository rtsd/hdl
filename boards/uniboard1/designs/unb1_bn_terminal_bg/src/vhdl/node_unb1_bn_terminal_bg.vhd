-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, diag_lib, unb1_board_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;

entity node_unb1_bn_terminal_bg is
  generic(
    g_sim                     : boolean := false;
    g_sim_level               : natural := 0;  -- 0 = simulate GX IP, 1 = use fast serial behavioural model
    -- Application interface
    g_use_bg                  : boolean := true;
    -- Terminals interface
    g_usr_nof_streams         : natural := 16;
    g_usr_data_w              : natural := 32;  -- im&re
    g_usr_block_len           : natural := 96;  -- nof complex samples per BG block
    g_usr_block_per           : natural := 256;  -- block period = block length + gap length
    -- .back
    g_use_back                : boolean := false;
    g_back_nof_serial         : natural := 4;
    g_back_gx_mbps            : natural := 5000;
    -- .mesh
    g_use_mesh                : boolean := true;
    g_mesh_nof_serial         : natural := 3;  -- default only use the 3 full featured transceivers in the mesh (out of maximal 4), using only 2 is not enough
    g_mesh_gx_mbps            : natural := 5000;
    g_mesh_ena_reorder        : boolean := true;
    g_mesh_use_rx             : boolean := true;  -- can be FALSE for BG, use TRUE to support for bidirectional TR diagnostics
    g_mesh_mon_select         : natural := 0;  -- 0 = no data monitor buffers via MM, else see unb_terminals_mesh.vhd
    g_mesh_mon_nof_words      : natural := 1024;
    g_mesh_mon_use_sync       : boolean := true;
    -- Auxiliary Interface
    g_aux                     : t_c_unb1_board_aux := c_unb1_board_aux
  );
  port(
    -- System
    chip_id                  : in  std_logic_vector(g_aux.chip_id_w - 1 downto 0);  -- [2:0]
    bck_id                   : in  std_logic_vector(c_unb1_board_nof_uniboard_w - 1 downto 0);  -- [7:3]; only [1:0] required to index boards 3:0 in a subrack

    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;  -- 50 MHz from xo_clk PLL in SOPC system
    dp_rst                   : in  std_logic;
    dp_clk                   : in  std_logic;  -- 200 MHz from CLK system clock
    dp_pps                   : in  std_logic;  -- external PPS pulse in dp_clk domain
    tr_mesh_clk              : in  std_logic := '0';  -- 156.25 MHz from SB_CLK transceiver clock
    tr_back_clk              : in  std_logic := '0';  -- 156.25 MHz from SA_CLK transceiver clock
    cal_clk                  : in  std_logic;  -- 40 MHz from xo_clk PLL in SOPC system

    -- Streaming data input when g_use_bg = FALSE
    in_sosi_arr              : in  t_dp_sosi_arr(g_usr_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
    in_siso_arr              : out t_dp_siso_arr(g_usr_nof_streams - 1 downto 0);

    -- MM interface
    -- . block generator
    ram_diag_bg_mosi         : in  t_mem_mosi := c_mem_mosi_rst;
    ram_diag_bg_miso         : out t_mem_miso;
    reg_diag_bg_mosi         : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diag_bg_miso         : out t_mem_miso;
    -- . tr_nonbonded mesh
    reg_mesh_tr_nonbonded_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mesh_tr_nonbonded_miso    : out t_mem_miso;
    reg_mesh_diagnostics_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_mesh_diagnostics_miso     : out t_mem_miso;
    -- . tr_nonbonded back
    reg_back_tr_nonbonded_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_back_tr_nonbonded_miso    : out t_mem_miso;
    reg_back_diagnostics_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    reg_back_diagnostics_miso     : out t_mem_miso;
    -- . diag_data_buffer
    ram_mesh_diag_data_buf_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    ram_mesh_diag_data_buf_miso   : out t_mem_miso;

    -- Mesh serial interface (tr_nonbonded)
    mesh_tx_serial_2arr           : out t_unb1_board_mesh_sl_2arr;  -- Tx
    mesh_rx_serial_2arr           : in  t_unb1_board_mesh_sl_2arr := (others => (others => '0'));  -- Rx support for diagnostics

    -- Back serial interface (tr_nonbonded)
    back_tx_serial_2arr           : out t_unb1_board_back_sl_2arr;  -- Tx
    back_rx_serial_2arr           : in  t_unb1_board_back_sl_2arr := (others => (others => '0'))  -- Rx
  );
end node_unb1_bn_terminal_bg;

architecture str of node_unb1_bn_terminal_bg is
  -- Terminals
  constant c_mesh_usr_nof_input : natural := g_usr_nof_streams / c_unb1_board_nof_fn;  -- 16 / 4 = 4

  constant c_back_usr_nof_input : natural := g_usr_nof_streams / c_unb1_board_nof_uniboard;  -- 16 / 4 = 4

  constant c_rx_timeout_w       : natural := 0;  -- ceil_log2(g_usr_block_per);  -- uth_rx timeout_cnt

  signal bg_siso_arr           : t_dp_siso_arr(g_usr_nof_streams - 1 downto 0);  -- for XON/OFF flow control
  signal bg_sosi_arr           : t_dp_sosi_arr(g_usr_nof_streams - 1 downto 0);

  signal bg_siso_2arr          : t_unb1_board_back_siso_2arr;
  signal bg_sosi_2arr          : t_unb1_board_back_sosi_2arr;

  -- Mesh: user side
  signal mesh_tx_usr_siso_2arr : t_unb1_board_mesh_siso_2arr;
  signal mesh_tx_usr_sosi_2arr : t_unb1_board_mesh_sosi_2arr;

  -- Back: user side
  signal back_tx_usr_siso_2arr : t_unb1_board_back_siso_2arr;
  signal back_tx_usr_sosi_2arr : t_unb1_board_back_sosi_2arr;

  signal back_rx_usr_siso_2arr : t_unb1_board_back_siso_2arr;
  signal back_rx_usr_sosi_2arr : t_unb1_board_back_sosi_2arr;
begin
  -----------------------------------------------------------------------------
  -- Block generator
  -----------------------------------------------------------------------------
  gen_bg : if g_use_bg = true generate
    u_bg : entity diag_lib.mms_diag_block_gen
    generic map(
      g_nof_streams      => g_usr_nof_streams,
      g_buf_dat_w        => g_usr_data_w,
      g_buf_addr_w       => ceil_log2(g_usr_block_len),
      g_file_name_prefix => "UNUSED"
    )
    port map(
      -- System
      mm_rst           => mm_rst,
      mm_clk           => mm_clk,
      dp_rst           => dp_rst,
      dp_clk           => dp_clk,
      en_sync          => dp_pps,
      -- MM interface
      reg_bg_ctrl_mosi => reg_diag_bg_mosi,
      reg_bg_ctrl_miso => reg_diag_bg_miso,
      ram_bg_data_mosi => ram_diag_bg_mosi,
      ram_bg_data_miso => ram_diag_bg_miso,
      -- ST interface
      out_siso_arr     => bg_siso_arr,
      out_sosi_arr     => bg_sosi_arr
    );
  end generate;

  no_bg : if g_use_bg = false generate
    bg_sosi_arr  <= in_sosi_arr;
    in_siso_arr  <= bg_siso_arr;
  end generate;

  -----------------------------------------------------------------------------
  -- Map the 16 BG streams into 4*4 streams; 4 streams for each UniBoard 3..0.
  -----------------------------------------------------------------------------
  gen_bg_unb  : for i in 0 to c_unb1_board_nof_uniboard - 1 generate
    gen_bg_st : for j in 0 to c_back_usr_nof_input - 1 generate
      bg_siso_arr(i * c_back_usr_nof_input + j) <= bg_siso_2arr(i)(j);
      bg_sosi_2arr(i)(j)                    <= bg_sosi_arr(i * c_back_usr_nof_input + j);
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- No back terminals: Block gen out to mesh terminals
  -----------------------------------------------------------------------------
  no_back: if g_use_back = false generate
    mesh_tx_usr_sosi_2arr <= func_unb1_board_connect_2arr(bg_sosi_2arr);
    bg_siso_2arr          <= func_unb1_board_connect_2arr(mesh_tx_usr_siso_2arr);
  end generate;

  -----------------------------------------------------------------------------
  -- Back terminals
  -----------------------------------------------------------------------------
  gen_back: if g_use_back = true generate
    -----------------------------------------------------------------------------
    -- Feed the 4 UniBoard-destination streams into the unb_terminals_back user side.
    -----------------------------------------------------------------------------
    bg_siso_2arr          <= back_tx_usr_siso_2arr;
    back_tx_usr_sosi_2arr <= bg_sosi_2arr;

    u_terminals_back: entity unb1_board_lib.unb1_board_terminals_back
    generic map (
      g_sim              => g_sim,
      g_sim_level        => g_sim_level,
      -- System
      g_nof_bus            => c_unb1_board_nof_uniboard,  -- = 4 Uniboard in subrack, this UniBoard and 3 other UniBoards, so each UniBoard can be indexed by logical index 3:0
      -- User
      g_usr_use_complex    => true,
      g_usr_data_w         => g_usr_data_w,
      g_usr_frame_len      => g_usr_block_len,
      g_usr_nof_streams    => c_mesh_usr_nof_input,
      -- Phy
      g_phy_nof_serial     => g_back_nof_serial,
      g_phy_gx_mbps        => g_back_gx_mbps,
      g_phy_rx_fifo_size   => c_bram_m9k_fifo_depth,
      -- Tx
      g_tx_input_use_fifo  => true,
      -- Rx
      g_rx_output_use_fifo => false,  -- no need for Rx output FIFOs to ensure that dp_distribute does not get blocked due to substantial backpressure on another output
      g_rx_timeout_w       => c_rx_timeout_w
    )
    port map (
      bck_id                 => bck_id,

      tr_clk                 => tr_back_clk,
      cal_clk                => cal_clk,

      mm_rst                 => mm_rst,
      mm_clk                 => mm_clk,

      dp_rst                 => dp_rst,
      dp_clk                 => dp_clk,

      -- User I/O side (4 uniboards)(4 i/o streams)
      tx_usr_siso_2arr       => back_tx_usr_siso_2arr,
      tx_usr_sosi_2arr       => back_tx_usr_sosi_2arr,
      rx_usr_siso_2arr       => back_rx_usr_siso_2arr,
      rx_usr_sosi_2arr       => back_rx_usr_sosi_2arr,

      -- Serial (tr_nonbonded)
      tx_serial_2arr         => back_tx_serial_2arr,  -- Tx
      rx_serial_2arr         => back_rx_serial_2arr,  -- Rx

      -- MM Control
      reg_tr_nonbonded_mosi  => reg_back_tr_nonbonded_mosi,
      reg_tr_nonbonded_miso  => reg_back_tr_nonbonded_miso,

      reg_diagnostics_mosi   => reg_back_diagnostics_mosi,
      reg_diagnostics_miso   => reg_back_diagnostics_miso
    );

    -----------------------------------------------------------------------------
    -- Back terminals -> transpose -> mesh terminals
    -----------------------------------------------------------------------------
    mesh_tx_usr_sosi_2arr <= func_unb1_board_transpose_2arr(back_rx_usr_sosi_2arr);
    back_rx_usr_siso_2arr <= func_unb1_board_transpose_2arr(mesh_tx_usr_siso_2arr);
  end generate;

  -----------------------------------------------------------------------------
  -- Mesh terminals
  -----------------------------------------------------------------------------
  gen_mesh: if g_use_mesh = true generate
    u_terminals_mesh : entity unb1_board_lib.unb1_board_terminals_mesh
    generic map (
      g_sim                     => g_sim,
      g_sim_level               => g_sim_level,
      -- System
      g_node_type               => e_bn,
      g_nof_bus                 => c_unb1_board_nof_fn,  -- 4 to 4 nodes in mesh
      -- User
      g_usr_use_complex         => true,
      g_usr_data_w              => g_usr_data_w,
      g_usr_frame_len           => g_usr_block_len,
      g_usr_nof_streams         => c_mesh_usr_nof_input,
      -- Phy
      g_phy_nof_serial          => g_mesh_nof_serial,
      g_phy_gx_mbps             => g_mesh_gx_mbps,
      g_phy_rx_fifo_size        => c_bram_m9k_fifo_depth,
      g_phy_ena_reorder         => g_mesh_ena_reorder,
      -- Tx
      g_use_tx                  => true,  -- user Tx must be TRUE for BG in BN,
      g_tx_input_use_fifo       => true,  -- Tx input uses FIFO to create slack for inserting DP Packet and Uthernet frame headers
      -- Rx
      g_use_rx                  => g_mesh_use_rx,  -- optionally do support diag Rx
      g_rx_output_use_fifo      => false,  -- no user Rx
      -- Monitoring
      g_mon_select              => g_mesh_mon_select,
      g_mon_nof_words           => g_mesh_mon_nof_words,
      g_mon_use_sync            => g_mesh_mon_use_sync
    )
    port map (
      chip_id                => chip_id,

      mm_rst                 => mm_rst,
      mm_clk                 => mm_clk,
      dp_rst                 => dp_rst,
      dp_clk                 => dp_clk,
      dp_sync                => dp_pps,
      tr_clk                 => tr_mesh_clk,
      cal_clk                => cal_clk,

      -- User interface (4 nodes)(4 input streams)
      tx_usr_siso_2arr       => mesh_tx_usr_siso_2arr,
      tx_usr_sosi_2arr       => mesh_tx_usr_sosi_2arr,  -- Tx (user Rx from FN to BN is unused)

      -- Serial mesh interface (tr_nonbonded)
      tx_serial_2arr         => mesh_tx_serial_2arr,  -- Tx
      rx_serial_2arr         => mesh_rx_serial_2arr,  -- Rx

      -- MM Control
      reg_tr_nonbonded_mosi  => reg_mesh_tr_nonbonded_mosi,
      reg_tr_nonbonded_miso  => reg_mesh_tr_nonbonded_miso,

      reg_diagnostics_mosi   => reg_mesh_diagnostics_mosi,
      reg_diagnostics_miso   => reg_mesh_diagnostics_miso,

      -- . diag_data_buffer
      ram_diag_data_buf_mosi => ram_mesh_diag_data_buf_mosi,
      ram_diag_data_buf_miso => ram_mesh_diag_data_buf_miso
    );
  end generate;
end str;
