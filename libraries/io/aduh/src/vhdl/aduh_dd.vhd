-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.aduh_dd_pkg.all;

-- Purpose: ADUH = ADU Handler using DDIO (no PLL).
--          Handles the LVDS Rx interface between a BN and two ADCs on one ADU.
-- Description:
-- Remarks:
-- . Compensate for DQ port rewire on ADU
-- . Convert offset binary of ADCs on ADU to two's complement.
-- . The c_rx_fifo_fill is set as a constant and not made programmable via MM
--   control, because this would require using a separate FIFO for each signal
--   path. This is not needed for Apertif where the A and B can share a FIFO
--   in lvds_dd, similar as C and D.
-- . This aduh_dd can not use the config IO interface on lvdsh_dd to set the
--   input delays, because this is not supported by all ADC_BI pins. Instead
--   use setup and hold times via set_input_delay in a SDC file to effectively
--   let the fitter control these delays.
--   For more details on the input timing constraints see:
--   . common/build/synth/quartus_iobuf_in_dd/common_iobuf_in_dd.qsf
--   . aduh/build/synth/quartus/aduh_dd.qsf
--   . an433
-- . The ADU ADC can output the data relative to the DCLK clock either at:
--   . phase  0 degrees = source aligned
--   . phase 90 degrees = center aligned
--   The input timing constrainst in the SDC must be set accordingly.
-- . Output reset pulse:
--   . use g_ai.clk_rst_invert is TRUE to compensate when connected to pin _N,
--   . use g_ai.clk_rst_invert is FALSE              when connected to pin _P,
--   see also in BACK_NODE_adc_pins.tcl and adu_half.vhd

entity aduh_dd is
  generic (
    g_sim               : boolean := false;
    g_nof_dp_phs_clk    : natural := 1;  -- nof dp_phs_clk that can be used to detect the word phase
    g_ai                : t_c_aduh_dd_ai := c_aduh_dd_ai
  );
  port (
    -- LVDS Interface
    -- . g_ai.nof_sp = 4, fixed support 4 signal paths A,B,C,D
    ADC_BI_A             : in  std_logic_vector(g_ai.port_w - 1 downto 0) := (others => '0');  -- fixed ADC_BI port width port_w = 8
    ADC_BI_B             : in  std_logic_vector(g_ai.port_w - 1 downto 0) := (others => '0');  -- each ADC_BI port carries the data from 1 signal path
    ADC_BI_C             : in  std_logic_vector(g_ai.port_w - 1 downto 0) := (others => '0');  -- ports A,B connect to one ADU, ports C,D connect to another ADU
    ADC_BI_D             : in  std_logic_vector(g_ai.port_w - 1 downto 0) := (others => '0');

    ADC_BI_A_CLK         : in  std_logic := '0';  -- lvds clock from ADU_AB
    ADC_BI_D_CLK         : in  std_logic := '0';  -- lvds clock from ADU_CD

    ADC_BI_A_CLK_RST     : out std_logic;  -- release synchronises ADU_AB DCLK divider
    ADC_BI_D_CLK_RST     : out std_logic;  -- release synchronises ADU_CD DCLK divider

    -- MM Interface
    ab_status            : out std_logic_vector(c_word_w - 1 downto 0);
    ab_locked            : out std_logic;
    ab_stable            : out std_logic;
    ab_stable_ack        : in  std_logic := '0';
    ab_dp_phs_clk_en_vec : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0) := (others => '1');

    cd_status            : out std_logic_vector(c_word_w - 1 downto 0);
    cd_locked            : out std_logic;
    cd_stable            : out std_logic;
    cd_stable_ack        : in  std_logic := '0';
    cd_dp_phs_clk_en_vec : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0) := (others => '1');

    -- DP Interface
    dp_rst               : in  std_logic;
    dp_clk               : in  std_logic;
    dp_phs_clk_vec       : in  std_logic_vector(g_nof_dp_phs_clk - 1 downto 0);

    -- . Streaming
    src_out_arr          : out t_dp_sosi_arr(0 to g_ai.nof_sp - 1)
  );
end aduh_dd;

architecture str of aduh_dd is
  constant c_use_lvdsh_dd_phs4   : boolean := g_nof_dp_phs_clk > 1;

  constant c_dp_factor           : natural := g_ai.rx_factor * g_ai.dd_factor;

  constant c_in_dat_w            : natural :=               g_ai.nof_ports * g_ai.port_w;
  constant c_rx_dat_w            : natural := c_dp_factor * g_ai.nof_ports * g_ai.port_w;

  -- Delay values (range 0..15) for input D1 delay for input clock and data[]
  constant c_in_clk_delay_a      : natural := g_ai.deskew.clk_delay_a;
  constant c_in_clk_delay_d      : natural := g_ai.deskew.clk_delay_d;
  constant c_in_dat_delay_arr_ab : t_natural_arr(g_ai.nof_ports * g_ai.port_w - 1 downto 0) := g_ai.deskew.dat_delay_arr_a & g_ai.deskew.dat_delay_arr_b;
  constant c_in_dat_delay_arr_cd : t_natural_arr(g_ai.nof_ports * g_ai.port_w - 1 downto 0) := g_ai.deskew.dat_delay_arr_c & g_ai.deskew.dat_delay_arr_d;

  -- Use c_rx_fifo_fill sufficiently larger than the moment where *_CLK_RST gets applied
  constant c_rx_fifo_size        : natural := 32;  -- see common_fifo_dc_lock_control used in lvds_dd for comment
  constant c_rx_fifo_fill        : natural := 17;  -- see common_fifo_dc_lock_control used in lvds_dd for comment
  constant c_rx_fifo_margin      : natural := 2;  -- use +-2 because with fill level 10001b = 17 accept +-1 so 10000b = 16 and 10010b = 18,
                                                   -- but also +2 for misvalue 10011b = 19 in case rd_usedw is not clocked in reliably

  signal ADC_BI_A_rewire     : std_logic_vector(ADC_BI_A'range);
  signal ADC_BI_B_rewire     : std_logic_vector(ADC_BI_B'range);
  signal ADC_BI_C_rewire     : std_logic_vector(ADC_BI_C'range);
  signal ADC_BI_D_rewire     : std_logic_vector(ADC_BI_D'range);

  signal in_clk_ab_rst       : std_logic;
  signal in_clk_cd_rst       : std_logic;

  signal in_dat_ab           : std_logic_vector(c_in_dat_w - 1 downto 0);
  signal in_dat_cd           : std_logic_vector(c_in_dat_w - 1 downto 0);

  signal obin_dat_ab         : std_logic_vector(c_rx_dat_w - 1 downto 0);
  signal obin_dat_cd         : std_logic_vector(c_rx_dat_w - 1 downto 0);
  signal obin_val_ab         : std_logic;
  signal obin_val_cd         : std_logic;

  signal rx_dat_ab           : std_logic_vector(c_rx_dat_w - 1 downto 0);
  signal rx_dat_cd           : std_logic_vector(c_rx_dat_w - 1 downto 0);
  signal rx_val_ab           : std_logic;
  signal rx_val_cd           : std_logic;
begin
  assert g_ai.nof_sp = 4 and g_ai.nof_adu = 2 and g_ai.nof_ports = 2
    report "aduh_dd : expects input 4 signal paths via 2 ports from 2 ADU"
    severity FAILURE;

  -- I port A and C wire default
  ADC_BI_A_rewire <= ADC_BI_A;
  ADC_BI_C_rewire <= ADC_BI_C;

  -- Q port B and D are rewired on ADU:
  ADC_BI_B_rewire(0) <= ADC_BI_B(6);
  ADC_BI_B_rewire(1) <= ADC_BI_B(7);
  ADC_BI_B_rewire(2) <= ADC_BI_B(4);
  ADC_BI_B_rewire(3) <= ADC_BI_B(5);
  ADC_BI_B_rewire(4) <= ADC_BI_B(2);
  ADC_BI_B_rewire(5) <= ADC_BI_B(3);
  ADC_BI_B_rewire(6) <= ADC_BI_B(0);
  ADC_BI_B_rewire(7) <= ADC_BI_B(1);

  ADC_BI_D_rewire(0) <= ADC_BI_D(6);
  ADC_BI_D_rewire(1) <= ADC_BI_D(7);
  ADC_BI_D_rewire(2) <= ADC_BI_D(4);
  ADC_BI_D_rewire(3) <= ADC_BI_D(5);
  ADC_BI_D_rewire(4) <= ADC_BI_D(2);
  ADC_BI_D_rewire(5) <= ADC_BI_D(3);
  ADC_BI_D_rewire(6) <= ADC_BI_D(0);
  ADC_BI_D_rewire(7) <= ADC_BI_D(1);

  -- Concatenate per half ADU (i.e. per clock)
  in_dat_ab <= ADC_BI_A_rewire & ADC_BI_B_rewire;
  in_dat_cd <= ADC_BI_C_rewire & ADC_BI_D_rewire;

  -- c_dp_factor = 4 time samples per src_out_arr data word
  p_src_out_arr : process(rx_dat_ab, rx_val_ab, rx_dat_cd, rx_val_cd)
  begin
    src_out_arr <= (others => c_dp_sosi_rst);
    for I in 0 to c_dp_factor - 1 loop
      src_out_arr(0).data((I + 1) * g_ai.port_w - 1 downto I * g_ai.port_w) <= rx_dat_ab((I + 1) * c_in_dat_w            - 1 downto I * c_in_dat_w + g_ai.port_w);  -- A at [t0, t1, t2, t3], [t4, t5, t6, t7], ...
      src_out_arr(1).data((I + 1) * g_ai.port_w - 1 downto I * g_ai.port_w) <= rx_dat_ab((I + 1) * c_in_dat_w - g_ai.port_w - 1 downto I * c_in_dat_w            );  -- B at [t0, t1, t2, t3], [t4, t5, t6, t7], ...
      src_out_arr(2).data((I + 1) * g_ai.port_w - 1 downto I * g_ai.port_w) <= rx_dat_cd((I + 1) * c_in_dat_w            - 1 downto I * c_in_dat_w + g_ai.port_w);  -- C at [t0, t1, t2, t3], [t4, t5, t6, t7], ...
      src_out_arr(3).data((I + 1) * g_ai.port_w - 1 downto I * g_ai.port_w) <= rx_dat_cd((I + 1) * c_in_dat_w - g_ai.port_w - 1 downto I * c_in_dat_w            );  -- D at [t0, t1, t2, t3], [t4, t5, t6, t7], ...
    end loop;
    src_out_arr(0).valid <= rx_val_ab;
    src_out_arr(1).valid <= rx_val_ab;
    src_out_arr(2).valid <= rx_val_cd;
    src_out_arr(3).valid <= rx_val_cd;
  end process;

  -- Connect ADU 0 via port A and B using only clock from A
  gen_lvdsh_dd : if c_use_lvdsh_dd_phs4 = false generate
  ab_status <= (others => '0');
  cd_status <= (others => '0');

  u_ab : entity work.lvdsh_dd
  generic map (
    g_in_dat_w          => c_in_dat_w,
    g_in_dat_delay_arr  => c_in_dat_delay_arr_ab,
    g_in_clk_delay      => c_in_clk_delay_a,
    g_in_clk_rst_invert => g_ai.clk_rst_invert,
    g_rx_big_endian     => true,
    g_rx_factor         => g_ai.rx_factor,
    g_rx_fifo_size      => c_rx_fifo_size,
    g_rx_fifo_fill      => c_rx_fifo_fill,
    g_rx_fifo_margin    => c_rx_fifo_margin
  )
  port map (
    -- PHY input interface
    in_clk        => ADC_BI_A_CLK,
    in_dat        => in_dat_ab,
    in_clk_rst    => ADC_BI_A_CLK_RST,

    -- DD --> Rx domain interface at in_clk rate or g_rx_factor lower rate (via FIFO)
    rx_rst        => dp_rst,
    rx_clk        => dp_clk,
    rx_dat        => obin_dat_ab,  -- big endian rx_dat output for rx_factor = 2, samples AB at [t0, t1, t2, t3], [t4, t5, t6, t7], ...
    rx_val        => obin_val_ab,

    rx_locked     => ab_locked,
    rx_stable     => ab_stable,
    rx_stable_ack => ab_stable_ack
  );

  -- Connect ADU 1 via port C and D using only clock from D
  u_cd : entity work.lvdsh_dd
  generic map (
    g_in_dat_w          => c_in_dat_w,
    g_in_dat_delay_arr  => c_in_dat_delay_arr_cd,
    g_in_clk_delay      => c_in_clk_delay_d,
    g_in_clk_rst_invert => g_ai.clk_rst_invert,
    g_rx_big_endian     => true,
    g_rx_factor         => g_ai.rx_factor,
    g_rx_fifo_size      => c_rx_fifo_size,
    g_rx_fifo_fill      => c_rx_fifo_fill,
    g_rx_fifo_margin    => c_rx_fifo_margin
  )
  port map (
    -- PHY input interface
    in_clk        => ADC_BI_D_CLK,
    in_dat        => in_dat_cd,
    in_clk_rst    => ADC_BI_D_CLK_RST,

    -- DD --> Rx domain interface at in_clk rate or g_rx_factor lower rate (via FIFO)
    rx_rst        => dp_rst,
    rx_clk        => dp_clk,
    rx_dat        => obin_dat_cd,  -- big endian rx_dat output for rx_factor = 2, samples CD at [t0, t1, t2, t3], [t4, t5, t6, t7], ...
    rx_val        => obin_val_cd,

    rx_locked     => cd_locked,
    rx_stable     => cd_stable,
    rx_stable_ack => cd_stable_ack
  );
  end generate;

  gen_lvdsh_dd_phs4 : if c_use_lvdsh_dd_phs4 = true generate
    -- Connect ADU 0 via port A and B using only clock from A
    ADC_BI_A_CLK_RST <= in_clk_ab_rst when g_ai.clk_rst_invert = false else not in_clk_ab_rst;

    in_clk_ab_rst <= '0';  -- no ADC clk reset

    u_lvdsh_dd_phs4_ab : entity work.lvdsh_dd_phs4
    generic map (
      g_sim            => g_sim,
      g_wb_factor      => c_dp_factor,  -- fixed wideband factor = 4
      g_nof_dp_phs_clk => g_nof_dp_phs_clk,  -- nof dp_phs_clk that can be used to detect lock
      g_in_dat_w       => c_in_dat_w  -- nof PHY data bits
    )
    port map (
      -- PHY input interface
      in_clk              => ADC_BI_A_CLK,
      in_dat              => in_dat_ab,  -- input samples AB [t0], [t1], [t2], [t3], [t4], [t5], [t6], [t7], ... --> time

      -- DD --> Rx domain interface at in_clk rate or g_wb_factor lower rate (via FIFO)
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,
      dp_phs_clk_vec      => dp_phs_clk_vec,
      dp_phs_clk_en_vec   => ab_dp_phs_clk_en_vec,
      dp_dat              => obin_dat_ab,  -- big endian output samples AB at [t0, t1, t2, t3], [t4, t5, t6, t7], ...
      dp_val              => obin_val_ab,

      -- Rx status monitor
      out_status          => ab_status,
      out_word_locked     => ab_locked,
      out_word_stable     => ab_stable,
      out_word_stable_ack => ab_stable_ack
    );

    -- Connect ADU 1 via port C and D using only clock from D
    ADC_BI_D_CLK_RST <= in_clk_cd_rst when g_ai.clk_rst_invert = false else not in_clk_cd_rst;

    in_clk_cd_rst <= '0';  -- no ADC clk reset

    u_lvdsh_dd_phs4_cd : entity work.lvdsh_dd_phs4
    generic map (
      g_sim            => g_sim,
      g_wb_factor      => c_dp_factor,  -- fixed wideband factor = 4
      g_nof_dp_phs_clk => g_nof_dp_phs_clk,  -- nof dp_phs_clk that can be used to detect lock
      g_in_dat_w       => c_in_dat_w  -- nof PHY data bits
    )
    port map (
      -- PHY input interface
      in_clk              => ADC_BI_D_CLK,
      in_dat              => in_dat_cd,  -- input samples CD [t0], [t1], [t2], [t3], [t4], [t5], [t6], [t7], ... --> time

      -- DD --> Rx domain interface at in_clk rate or g_wb_factor=4 lower rate (via FIFO)
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,
      dp_phs_clk_vec      => dp_phs_clk_vec,
      dp_phs_clk_en_vec   => cd_dp_phs_clk_en_vec,
      dp_dat              => obin_dat_cd,  -- big endian output samples CD at [t0, t1, t2, t3], [t4, t5, t6, t7], ...
      dp_val              => obin_val_cd,

      -- Rx status monitor
      out_status          => cd_status,
      out_word_locked     => cd_locked,
      out_word_stable     => cd_stable,
      out_word_stable_ack => cd_stable_ack
    );
  end generate;

  -- Use register stage to map offset binary to two's complement, this will allow synthesis to use the FF q_not output for the high bit and the FF q output for the other bits
  p_dp_clk : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      for I in 0 to c_dp_factor * g_ai.nof_ports - 1 loop
        rx_dat_ab((I + 1) * g_ai.port_w - 1 downto I * g_ai.port_w) <= offset_binary(obin_dat_ab((I + 1) * g_ai.port_w - 1 downto I * g_ai.port_w));
        rx_dat_cd((I + 1) * g_ai.port_w - 1 downto I * g_ai.port_w) <= offset_binary(obin_dat_cd((I + 1) * g_ai.port_w - 1 downto I * g_ai.port_w));
      end loop;
      rx_val_ab <= obin_val_ab;
      rx_val_cd <= obin_val_cd;
    end if;
  end process;
end str;
