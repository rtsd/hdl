-------------------------------------------------------------------------------
--
-- Copyright (C) 2024
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author: E. Kooistra
-- Purpose: Circular buffer for dp_bsn_align_v2
-- Description:
-- . Default use same buffer size for all streams.
-- . Optionally use reduced size for the remote stream to save block RAM. The
--   reduced size is possible when there is only one remote input (so
--   g_nof_streams = 2) and that input has been passed along a series of
--   bsn aligners (so g_nof_aligners_max > 1). The local input requires the
--   default buffer size (= g_ram_buf), but the remote input then can use a
--   reduced buffer size (g_remote_ram_buf).
-- . The buffer sizes have to be a power of two number of blocks.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_mem_pkg.all;

entity dp_bsn_align_buffer is
  generic (
    g_nof_streams      : natural;
    g_nof_aligners_max : natural := 1;
    -- Default RAM buffer size for all streams
    g_ram_buf          : t_c_mem;
    -- Optional smaller size for remote stream when c_use_reduced_remote_buffer = true
    g_remote_ram_buf   : t_c_mem := c_mem_ram
  );
  port (
    dp_rst       : in  std_logic := '0';
    dp_clk       : in  std_logic;
    wr_copi_arr  : in  t_mem_copi_arr(g_nof_streams - 1 downto 0);
    rd_copi      : in  t_mem_copi;
    rd_cipo_arr  : out t_mem_cipo_arr(g_nof_streams - 1 downto 0)
  );
end dp_bsn_align_buffer;

architecture str of dp_bsn_align_buffer is
  constant c_use_reduced_remote_buffer : boolean := g_nof_streams = 2 and g_nof_aligners_max > 1;
begin
  use_uniform_data_buffer : if c_use_reduced_remote_buffer = false generate
    gen_data_buffer : for I in 0 to g_nof_streams - 1 generate
      u_data_buffer : entity common_lib.common_ram_r_w
      generic map (
        g_ram     => g_ram_buf
      )
      port map (
        rst       => dp_rst,
        clk       => dp_clk,
        wr_en     => wr_copi_arr(I).wr,
        wr_adr    => wr_copi_arr(I).address(g_ram_buf.adr_w - 1 downto 0),
        wr_dat    => wr_copi_arr(I).wrdata(g_ram_buf.dat_w - 1 downto 0),
        rd_en     => rd_copi.rd,
        rd_adr    => rd_copi.address(g_ram_buf.adr_w - 1 downto 0),
        rd_dat    => rd_cipo_arr(I).rddata(g_ram_buf.dat_w - 1 downto 0),
        rd_val    => rd_cipo_arr(I).rdval
      );
    end generate;
  end generate;

  use_reduced_data_buffer : if c_use_reduced_remote_buffer = true generate
    -- Local data buffer has default size
    u_local_data_buffer : entity common_lib.common_ram_r_w
    generic map (
      g_ram     => g_ram_buf
    )
    port map (
      rst       => dp_rst,
      clk       => dp_clk,
      wr_en     => wr_copi_arr(0).wr,
      wr_adr    => wr_copi_arr(0).address(g_ram_buf.adr_w - 1 downto 0),
      wr_dat    => wr_copi_arr(0).wrdata(g_ram_buf.dat_w - 1 downto 0),
      rd_en     => rd_copi.rd,
      rd_adr    => rd_copi.address(g_ram_buf.adr_w - 1 downto 0),
      rd_dat    => rd_cipo_arr(0).rddata(g_ram_buf.dat_w - 1 downto 0),
      rd_val    => rd_cipo_arr(0).rdval
    );

    -- Remote data buffer has reduced size, to save block RAM
    u_remote_data_buffer : entity common_lib.common_ram_r_w
    generic map (
      g_ram     => g_remote_ram_buf
    )
    port map (
      rst       => dp_rst,
      clk       => dp_clk,
      wr_en     => wr_copi_arr(1).wr,
      wr_adr    => wr_copi_arr(1).address(g_remote_ram_buf.adr_w - 1 downto 0),
      wr_dat    => wr_copi_arr(1).wrdata(g_remote_ram_buf.dat_w - 1 downto 0),
      rd_en     => rd_copi.rd,
      rd_adr    => rd_copi.address(g_remote_ram_buf.adr_w - 1 downto 0),
      rd_dat    => rd_cipo_arr(1).rddata(g_remote_ram_buf.dat_w - 1 downto 0),
      rd_val    => rd_cipo_arr(1).rdval
    );
  end generate;
end str;
