--
--        _/_/           _/_/_/
--      _/_/_/_/         _/_/_/
--      _/_/_/_/_/       _/_/_/              ____________________________________________
--      _/_/_/_/_/       _/_/_/             /                                           /
--      _/_/_/_/_/       _/_/_/            /                                 M25P128   /
--      _/_/_/_/_/       _/_/_/           /                                           /
--      _/_/_/_/_/_/     _/_/_/          /                                  128Mbit  /
--      _/_/_/_/_/_/     _/_/_/         /                   Low Voltage, SPI, 50Mhz /
--      _/_/_/ _/_/_/    _/_/_/        /                                           /
--      _/_/_/  _/_/_/   _/_/_/       /                     VHDL Behavioral Model /
--      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.0 /
--      _/_/_/    _/_/_/ _/_/_/     /                                           /
--      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2008 Numonyx B.V. /
--      _/_/_/      _/_/_/_/_/    /___________________________________________/
--      _/_/_/       _/_/_/_/
--      _/_/          _/_/_/
--
--
--             NUMONYX
--
--
--
-------------------------------------------------------
-- Author: Hugues CREUSY
--February 2004
-- VHDL model
-- project: M25P64 50 MHz,
-- release: 1.0
-----------------------------------------------------
-- Unit   : Test Bench
-----------------------------------------------------

-------------------------------------------------------------
-- These VHDL models are provided "as is" without warranty
-- of any kind, included but not limited to, implied warranty
-- of merchantability and fitness for a particular purpose.
-------------------------------------------------------------


--------------------------------------------------------------
--				TEST BENCH
--------------------------------------------------------------
LIBRARY IEEE;
	USE IEEE.STD_LOGIC_1164.ALL;
LIBRARY STD;
	USE std.textio.ALL;

ENTITY TestBench IS
END TestBench;

------------------------------------------------------------
-- Test architecture:
------------------------------------------------------------
ARCHITECTURE Top OF TestBench IS

COMPONENT Driver
PORT(VCC: OUT REAL; clk: OUT std_logic;din: OUT std_logic;cs_valid: OUT std_logic;hard_protect: OUT std_logic;hold: OUT std_logic);
END COMPONENT ;

Component M25P128 
Generic ( 

        MemoryFileName : string;
        TimingCheckOn  : boolean 
        );

PORT (      VCC           : In Real;
	    C, D, S, Hold : In Std_Logic;
            W_Vpp         : In real;
	    Q             : Out std_logic);
END Component;

SIGNAL Vcc: REAL:=0.0;	
SIGNAL clock, data, W, hold, Q: std_logic;
signal W_VPP: real :=0.0;
SIGNAL S: std_logic;

---------------------------------------------------------
-----------  body of structural architecture ------------
---------------------------------------------------------
Begin

Vpp_process : process(W) begin
   if (W='0') then W_Vpp<=0.5;
              else W_Vpp<=9.0; -- Fast
   end if;
end process;

Tester: Driver
 Port Map(VCC=>Vcc,clk=>clock,din=>data,cs_valid=>S,hard_protect=>W,hold=>hold);


DUT: M25P128
 Generic map(TimingCheckOn => false,MemoryFileName => "memory_file")
 Port Map(VCC=>Vcc,C=>clock,D=>data,S=>s,W_Vpp=>W_Vpp,HOLD=>hold,Q=>Q);

End Top;
