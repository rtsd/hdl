-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, tst_lib, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose: Test bench for common_select_m_symbols.vhd
-- Usage:
-- > as 3
-- > run -all
--   p_verify self-checks the output of the reorder by using an inverse reorder
-- Remark:
-- . The verification assumes that the reorderings can be inversed, so all
--   inputs are output. It requires only one extra common_select_m_symbols
--   unit that has the same selection settings to obtain the original
--   input data.

entity tb_common_select_m_symbols is
  generic (
    g_nof_input     : natural := 4;
    g_nof_output    : natural := 4;
    g_symbol_w      : natural := 16;
    g_pipeline_in   : natural := 0;  -- pipeline in_data
    g_pipeline_in_m : natural := 0;  -- pipeline in_data for M-fold fan out
    g_pipeline_out  : natural := 1;  -- pipeline out_data
    g_select_arr    : t_natural_arr := (2, 3, 0, 1)
  );
end tb_common_select_m_symbols;

architecture tb of tb_common_select_m_symbols is
  constant clk_period        : time := 10 ns;

  -- Stimuli constants
  constant c_frame_len       : natural := 17;
  constant c_frame_sop       : natural := 1;
  constant c_frame_eop       : natural := (c_frame_sop + c_frame_len - 1) mod c_frame_len;

  -- DUT constants
  constant c_N               : natural := largest(g_nof_input, g_nof_output);

  constant c_dut_pipeline    : natural := g_pipeline_in + g_pipeline_in_m + g_pipeline_out;
  constant c_total_pipeline  : natural := c_dut_pipeline * 2;  -- factor for DUT, inverse DUT

  constant c_select_w        : natural := ceil_log2(g_nof_input);

  -- Stimuli
  signal tb_end             : std_logic := '0';
  signal rst                : std_logic;
  signal clk                : std_logic := '1';

  -- DUT input
  signal in_select_arr      : t_natural_arr(g_nof_output - 1 downto 0) := g_select_arr;
  signal in_select_vec      : std_logic_vector(g_nof_output * ceil_log2(g_nof_input) - 1 downto 0);
  signal in_data_vec        : std_logic_vector(g_nof_input * g_symbol_w - 1 downto 0);
  signal in_dat             : std_logic_vector(            g_symbol_w - 1 downto 0) := (others => '0');
  signal in_val             : std_logic;
  signal in_sop             : std_logic;
  signal in_eop             : std_logic;
  signal in_sync            : std_logic;

  -- DUT output
  signal reorder_data_vec   : std_logic_vector(g_nof_output * g_symbol_w - 1 downto 0);
  signal reorder_val        : std_logic;
  signal reorder_sop        : std_logic;
  signal reorder_eop        : std_logic;
  signal reorder_sync       : std_logic;

  -- inverse output
  signal inverse_select_arr : t_natural_arr(g_nof_output - 1 downto 0) :=  g_select_arr;
  signal inverse_select_vec : std_logic_vector(g_nof_output * ceil_log2(g_nof_input) - 1 downto 0) := (others => '0');

  -- Verify output
  signal out_data_vec       : std_logic_vector(g_nof_input * g_symbol_w - 1 downto 0);
  signal out_val            : std_logic;
  signal out_sop            : std_logic;
  signal out_eop            : std_logic;
  signal out_sync           : std_logic;

  -- Verify
  signal exp_data_vec       : std_logic_vector(g_nof_input * g_symbol_w - 1 downto 0);
  signal exp_val            : std_logic;
  signal exp_sop            : std_logic;
  signal exp_eop            : std_logic;
  signal exp_sync           : std_logic;
begin
  -- Stimuli
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after 3 * clk_period;

  tb_end <= '1' when signed(in_dat) = -1 else '0';

  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      in_val <= '0';
      in_dat <= (others => '0');
    elsif rising_edge(clk) then
      in_val <= '1';
      in_dat <= std_logic_vector(signed(in_dat) + 1);
    end if;
  end process;

  gen_in_data_vec: for I in g_nof_input - 1 downto 0 generate
    in_data_vec((I + 1) * g_symbol_w - 1 downto I * g_symbol_w) <= INCR_UVEC(in_dat, I);
  end generate;

  in_sop  <= in_val when TO_UINT(in_dat) mod c_frame_len = c_frame_sop else '0';
  in_eop  <= in_val when TO_UINT(in_dat) mod c_frame_len = c_frame_eop else '0';
  in_sync <= '0';

  gen_in_select_vec: for K in g_nof_output - 1 downto 0 generate
    in_select_vec((K + 1) * c_select_w - 1 downto K * c_select_w) <= TO_UVEC(in_select_arr(K), c_select_w);
  end generate;

  -- DUT
  u_reorder_in : entity work.common_select_m_symbols
  generic map (
    g_nof_input     => g_nof_input,
    g_nof_output    => g_nof_output,
    g_symbol_w      => g_symbol_w,
    g_pipeline_in   => g_pipeline_in,
    g_pipeline_in_m => g_pipeline_in_m,
    g_pipeline_out  => g_pipeline_out
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => in_data_vec,
    in_val     => in_val,
    in_sop     => in_sop,
    in_eop     => in_eop,
    in_sync    => in_sync,

    in_select  => in_select_vec,

    out_data   => reorder_data_vec,
    out_val    => reorder_val,
    out_sop    => reorder_sop,
    out_eop    => reorder_eop,
    out_sync   => reorder_sync
  );

  gen_inverse_select_vec: for K in g_nof_output - 1 downto 0 generate
    inverse_select_vec((K + 1) * c_select_w - 1 downto K * c_select_w) <= TO_UVEC(inverse_select_arr(K), c_select_w);
  end generate;

  u_inverse_out : entity work.common_select_m_symbols
  generic map (
    g_nof_input     => g_nof_output,
    g_nof_output    => g_nof_input,
    g_symbol_w      => g_symbol_w,
    g_pipeline_in   => g_pipeline_in,
    g_pipeline_in_m => g_pipeline_in_m,
    g_pipeline_out  => g_pipeline_out
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_data    => reorder_data_vec,
    in_val     => reorder_val,
    in_sop     => reorder_sop,
    in_eop     => reorder_eop,
    in_sync    => reorder_sync,

    in_select  => inverse_select_vec,

    out_data   => out_data_vec,
    out_val    => out_val,
    out_sop    => out_sop,
    out_eop    => out_eop,
    out_sync   => out_sync
  );

  -- Verification
  p_verify : process(rst, clk)
  begin
    if rst = '1' then
    elsif rising_edge(clk) then
      if exp_data_vec /= out_data_vec then report "Unexpected out_data_vec" severity ERROR; end if;
      if exp_val      /= out_val      then report "Unexpected out_val"      severity ERROR; end if;
      if exp_sop      /= out_sop      then report "Unexpected out_sop"      severity ERROR; end if;
      if exp_eop      /= out_eop      then report "Unexpected out_eop"      severity ERROR; end if;
      if exp_sync     /= out_sync     then report "Unexpected out_sync"     severity ERROR; end if;
    end if;
  end process;

  -- pipeline data input
  u_out_dat : entity work.common_pipeline
  generic map (
    g_pipeline  => c_total_pipeline,
    g_in_dat_w  => g_nof_input * g_symbol_w,
    g_out_dat_w => g_nof_input * g_symbol_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_data_vec,
    out_dat => exp_data_vec
  );

  -- pipeline control input
  u_out_val : entity work.common_pipeline_sl
  generic map (
    g_pipeline => c_total_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_val,
    out_dat => exp_val
  );

  u_out_sop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => c_total_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sop,
    out_dat => exp_sop
  );

  u_out_eop : entity work.common_pipeline_sl
  generic map (
    g_pipeline => c_total_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_eop,
    out_dat => exp_eop
  );

  u_out_sync : entity work.common_pipeline_sl
  generic map (
    g_pipeline => c_total_pipeline
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_sync,
    out_dat => exp_sync
  );
end tb;
