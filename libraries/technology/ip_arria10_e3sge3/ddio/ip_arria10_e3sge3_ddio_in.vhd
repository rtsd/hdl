-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Wrapper for ip_arria10_e3sge3_ddio_in_1 to support g_width >= 1

library ieee;
use ieee.std_logic_1164.all;

entity ip_arria10_e3sge3_ddio_in is
  generic (
    g_width : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';  -- Not Connected
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
end ip_arria10_e3sge3_ddio_in;

architecture str of ip_arria10_e3sge3_ddio_in is
  component ip_arria10_e3sge3_ddio_in_1 is
        port (
                datain    : in  std_logic_vector(0 downto 0) := (others => '0');  -- pad_in.export
                inclock   : in  std_logic                    := '0';  -- ck.export
                aclr      : in  std_logic                    := '0';  -- aclr.export
                dataout_h : out std_logic_vector(0 downto 0);  -- dataout_h.fragment
                dataout_l : out std_logic_vector(0 downto 0)  -- dataout_l.fragment
        );
  end component;
begin
  gen_w : for I in g_width - 1 downto 0 generate
    u_ip_arria10_e3sge3_ddio_in_1 : ip_arria10_e3sge3_ddio_in_1
    port map (
      datain    => in_dat(I downto I),
      inclock   => in_clk,
      aclr      => rst,
      dataout_h => out_dat_hi(I downto I),
      dataout_l => out_dat_lo(I downto I)
    );
  end generate;
end str;
