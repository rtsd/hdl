-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multi testbench for tech_mac_10g the 10G Ethernet IP technology wrapper.
-- Description:
-- Usage:
--   > as 5
--   > run -all

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use work.tb_tech_mac_10g_pkg.all;

entity tb_tb_tech_mac_10g is
end tb_tb_tech_mac_10g;

architecture tb of tb_tb_tech_mac_10g is
  constant c_tb_end_vec : std_logic_vector(7 downto 0) := (others => '1');
  signal   tb_end_vec   : std_logic_vector(7 downto 0) := c_tb_end_vec;  -- sufficiently long to fit all tb instances
  signal   tb_end       : std_logic := '0';
begin
--    g_technology : NATURAL := c_tech_select_default;
--    g_tb_end     : BOOLEAN := TRUE;   -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
--    g_no_dut     : BOOLEAN := FALSE;  -- default FALSE to verify the DUT, else use TRUE to verify the tb itself without the DUT
--    g_data_type  : NATURAL := c_tb_tech_mac_10g_data_type_symbols
--                              c_tb_tech_mac_10g_data_type_counter

  u_no_dut_counter       : entity work.tb_tech_mac_10g generic map (c_tech_select_default, false,  true, c_tb_tech_mac_10g_data_type_counter) port map (tb_end_vec(0));
  u_no_dut_symbols       : entity work.tb_tech_mac_10g generic map (c_tech_select_default, false,  true, c_tb_tech_mac_10g_data_type_symbols) port map (tb_end_vec(1));

  u_tech_mac_10g_counter : entity work.tb_tech_mac_10g generic map (c_tech_select_default, false, false, c_tb_tech_mac_10g_data_type_counter) port map (tb_end_vec(2));
  u_tech_mac_10g_symbols : entity work.tb_tech_mac_10g generic map (c_tech_select_default, false, false, c_tb_tech_mac_10g_data_type_symbols) port map (tb_end_vec(3));

  tb_end <= '1' when tb_end_vec = c_tb_end_vec else '0';

  p_tb_end : process
  begin
    wait until tb_end = '1';
    wait for 1 ns;
    report "Multi tb simulation finished."
      severity FAILURE;
    wait;
  end process;
end tb;
