-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- The dp_mon is dropped inbetween a source and a sink. It does not affect the
-- passing stream in any way - it monitors the stream and keeps useful stats.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity dp_mon is
  generic (
    g_latency : natural := 1
  );
  port (
    rst          : in  std_logic := '0';
    clk          : in  std_logic;
    en           : in  std_logic;

    snk_out      : out t_dp_siso;
    snk_in       : in  t_dp_sosi;

    src_in       : in  t_dp_siso;
    src_out      : out t_dp_sosi;

    clr          : in  std_logic := '0';  -- clears the word count

    -- All output stats could be in a record (t_dp_mon) in the future
    word_cnt     : out std_logic_vector(32 - 1 downto 0)  -- Number of words accepted by the connected sink
  );
end dp_mon;

architecture rtl of dp_mon is
  signal snk_accept      : std_logic;
begin
  snk_out <= src_in;
  src_out <= snk_in;

  snk_accept <= snk_in.valid when g_latency > 0 else snk_in.valid and src_in.ready;

  u_word_cntr : entity common_lib.common_counter
  generic map (
    g_width     => 32,
    g_step_size => 1,
    g_latency   => 0
  )
  port map (
    rst     => rst,
    clk     => clk,
    cnt_clr => clr,
    cnt_en  => snk_accept,
    count   => word_cnt
  );
end rtl;
