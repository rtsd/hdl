-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Copied from LOFAR1 RSP/common by Eric Kooistra on 13 Jan 2021 to use with pfs

-- Function: Add or subtract of two products a0b0 and a1b1
--
--     result = in_a0*in_b0 + in_a1*in_b1 when g_add_sub = "ADD"
--     result = in_a0*in_b0 - in_a1*in_b1 when g_add_sub = "SUB"
--
--     out_dat = RESIZE(result)
--
-- Architectures:
-- . rtl    : uses RTL to have all registers in one clocked process
-- . virtex : uses Coregen component for the two multipliers and an adder in RTL
--

library ieee, common_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use common_lib.common_pkg.all;

entity common_mult_add is
  generic (
    g_in_a_w     : positive;
    g_in_b_w     : positive;
    g_out_dat_w  : positive;
    g_add_sub    : string := "ADD";
    g_pipeline   : integer := 3
  );
  port (
    clk        : in  std_logic;
    rst        : in  std_logic := '0';
    in_a0      : in  std_logic_vector(g_in_a_w - 1 downto 0);
    in_a1      : in  std_logic_vector(g_in_a_w - 1 downto 0);
    in_b0      : in  std_logic_vector(g_in_b_w - 1 downto 0);
    in_b1      : in  std_logic_vector(g_in_b_w - 1 downto 0);
    out_dat    : out std_logic_vector(g_out_dat_w - 1 downto 0)
  );
begin
 -- ASSERT g_pipeline=3
  --  REPORT "pipeline value not supported"
   -- SEVERITY FAILURE;
end common_mult_add;

architecture rtl of common_mult_add is
  constant c_prod_w         : natural := g_in_a_w + g_in_b_w;
  constant c_sum_w          : natural := c_prod_w + 1;

  signal in_a0_p    : std_logic_vector(g_in_a_w - 1 downto 0);
  signal in_b0_p    : std_logic_vector(g_in_b_w - 1 downto 0);
  signal in_a1_p    : std_logic_vector(g_in_a_w - 1 downto 0);
  signal in_b1_p    : std_logic_vector(g_in_b_w - 1 downto 0);
  signal prod0      : signed(c_prod_w - 1 downto 0);
  signal prod1      : signed(c_prod_w - 1 downto 0);
  signal result     : signed(c_sum_w - 1 downto 0);
  signal nxt_result : signed(c_sum_w - 1 downto 0);
begin
  out_dat <= std_logic_vector(resize(result, out_dat'length));

  p_clk : process (clk)
  begin
    if rising_edge(clk) then
      in_a0_p <= in_a0;
      in_b0_p <= in_b0;
      in_a1_p <= in_a1;
      in_b1_p <= in_b1;
      prod0   <= signed(in_a0_p) * signed(in_b0_p);
      prod1   <= signed(in_a1_p) * signed(in_b1_p);
      result  <= nxt_result;
    end if;
  end process;

  gen_add : if g_add_sub = "ADD" generate
    nxt_result <= resize(prod0, c_sum_w) + prod1;
  end generate;

  gen_sub : if g_add_sub = "SUB" generate
    nxt_result <= resize(prod0, c_sum_w) - prod1;
  end generate;
end rtl;
