-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

-- Purpose: Verify multiple variations of tb_dp_packet
-- Description:
-- Usage:
-- > as 2
-- > run -all

entity tb_tb_dp_packet is
end tb_tb_dp_packet;

architecture tb of tb_tb_dp_packet is
  constant c_nof_repeat             : natural := 10;
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --                                                        g_data_w, in_en, src_in.ready, nof repeat
  u_16_act_act         : entity work.tb_dp_packet generic map (16, e_active, e_active, c_nof_repeat);
  u_16_act_rnd         : entity work.tb_dp_packet generic map (16, e_active, e_random, c_nof_repeat);
  u_16_act_pls         : entity work.tb_dp_packet generic map (16, e_active, e_pulse,  c_nof_repeat);

  u_16_rnd_act         : entity work.tb_dp_packet generic map (16, e_random, e_active, c_nof_repeat);
  u_16_rnd_rnd         : entity work.tb_dp_packet generic map (16, e_random, e_random, c_nof_repeat);
  u_16_rnd_pls         : entity work.tb_dp_packet generic map (16, e_random, e_pulse,  c_nof_repeat);

  u_16_pls_act         : entity work.tb_dp_packet generic map (16, e_pulse,  e_active, c_nof_repeat);
  u_16_pls_rnd         : entity work.tb_dp_packet generic map (16, e_pulse,  e_random, c_nof_repeat);
  u_16_pls_pls         : entity work.tb_dp_packet generic map (16, e_pulse,  e_pulse,  c_nof_repeat);

  u_8_act_act          : entity work.tb_dp_packet generic map ( 8, e_active, e_active, c_nof_repeat);
  u_8_act_rnd          : entity work.tb_dp_packet generic map ( 8, e_active, e_random, c_nof_repeat);
  u_8_act_pls          : entity work.tb_dp_packet generic map ( 8, e_active, e_pulse,  c_nof_repeat);

  u_8_rnd_act          : entity work.tb_dp_packet generic map ( 8, e_random, e_active, c_nof_repeat);
  u_8_rnd_rnd          : entity work.tb_dp_packet generic map ( 8, e_random, e_random, c_nof_repeat);
  u_8_rnd_pls          : entity work.tb_dp_packet generic map ( 8, e_random, e_pulse,  c_nof_repeat);

  u_8_pls_act          : entity work.tb_dp_packet generic map ( 8, e_pulse,  e_active, c_nof_repeat);
  u_8_pls_rnd          : entity work.tb_dp_packet generic map ( 8, e_pulse,  e_random, c_nof_repeat);
  u_8_pls_pls          : entity work.tb_dp_packet generic map ( 8, e_pulse,  e_pulse,  c_nof_repeat);

  u_32_act_act         : entity work.tb_dp_packet generic map (32, e_active, e_active, c_nof_repeat);
  u_32_act_rnd         : entity work.tb_dp_packet generic map (32, e_active, e_random, c_nof_repeat);
  u_32_act_pls         : entity work.tb_dp_packet generic map (32, e_active, e_pulse,  c_nof_repeat);

  u_32_rnd_act         : entity work.tb_dp_packet generic map (32, e_random, e_active, c_nof_repeat);
  u_32_rnd_rnd         : entity work.tb_dp_packet generic map (32, e_random, e_random, c_nof_repeat);
  u_32_rnd_pls         : entity work.tb_dp_packet generic map (32, e_random, e_pulse,  c_nof_repeat);

  u_32_pls_act         : entity work.tb_dp_packet generic map (32, e_pulse,  e_active, c_nof_repeat);
  u_32_pls_rnd         : entity work.tb_dp_packet generic map (32, e_pulse,  e_random, c_nof_repeat);
  u_32_pls_pls         : entity work.tb_dp_packet generic map (32, e_pulse,  e_pulse,  c_nof_repeat);
end tb;
