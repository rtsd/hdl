-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose: Multi dp for tb_dp_rsn_source
-- Remark:
-- . Derived from tb_tb_dp_bsn_source_v2
-- Usage:
-- > as 4
-- > run -all --> OK

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_rsn_source is
end tb_tb_dp_rsn_source;

architecture tb of tb_tb_dp_rsn_source is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- from tb_dp_rsn_source.vhd
  --
  -- g_pps_interval   : NATURAL := 240
  -- g_bs_block_size  : NATURAL := 32
  -- g_rs_block_size  : NATURAL := 5 --23

  -----------------------------------------------------------------------------
  -- Tests with g_rs_block_size /= g_bs_block_size
  -----------------------------------------------------------------------------
  u_12_3_3   : entity work.tb_dp_rsn_source generic map (12,  3,  3);  -- smallest block size
  u_16_8_4   : entity work.tb_dp_rsn_source generic map (16,  8,  4);  -- integer number of blocks per g_pps_interval
  u_25_5     : entity work.tb_dp_rsn_source generic map (25,  5,  5);
  u_29_17_23 : entity work.tb_dp_rsn_source generic map (29, 17, 23);  -- fractional number of blocks per g_pps_interval

  u_9_4_5    : entity work.tb_dp_rsn_source generic map (9, 4, 5);  -- 2 g_bs_block_size < g_pps_interval < 2 g_rs_block_size
  u_9_5_4    : entity work.tb_dp_rsn_source generic map (9, 5, 4);  -- 2 g_bs_block_size > g_pps_interval > 2 g_rs_block_size
  u_9_5_9    : entity work.tb_dp_rsn_source generic map (9, 5, 9);  -- 1 g_rs_block_size/g_pps_interval
  u_9_9_5    : entity work.tb_dp_rsn_source generic map (9, 9, 5);  -- 1 g_bs_block_size/g_pps_interval

  -----------------------------------------------------------------------------
  -- Same tests as with tb_dp_bsn_source_v2 with g_rs_block_size = g_bs_block_size
  -----------------------------------------------------------------------------
  -- test integer case
  u_20_10  : entity work.tb_dp_rsn_source generic map (20, 10, 10);  -- 20 // 10 = 2, 20 MOD 10 = 0, 20/10 = 2 block/sync
  u_22_11  : entity work.tb_dp_rsn_source generic map (22, 11, 11);  -- 22 // 11 = 2, 22 MOD 11 = 0, 22/11 = 2 block/sync
  u_39_13  : entity work.tb_dp_rsn_source generic map (39, 13, 13);  -- 39 // 13 = 3, 39 MOD 13 = 0, 39/13 = 3 block/sync

  -- test smallest nof block per sync
  u_10_10  : entity work.tb_dp_rsn_source generic map (10, 10, 10);  -- 1 block/sync
  u_5_5    : entity work.tb_dp_rsn_source generic map (5, 5, 5);  -- 1 block/sync

  -- test smallest g_block_size case
  u_3_3    : entity work.tb_dp_rsn_source generic map (3, 3, 3);  -- 3 // 3 = 1, 3 MOD 3 = 0, 3/3 = 1 block/sync
  u_6_3    : entity work.tb_dp_rsn_source generic map (6, 3, 3);  -- 6 // 3 = 2, 6 MOD 3 = 0, 6/3 = 2 block/sync
  u_7_3    : entity work.tb_dp_rsn_source generic map (7, 3, 3);  -- 7 // 3 = 2, 7 MOD 3 = 1, 7/3 = 2.33 block/sync

  -- test lofar case with 0.5 fraction in average nof block/sync
  u_20_8   : entity work.tb_dp_rsn_source generic map (20, 8, 8);  -- 20 // 8 = 2, 20 MOD 8 = 4, 20/8 = 2.5 block/sync

  -- test fractional (corner) cases
  u_18_9   : entity work.tb_dp_rsn_source generic map (18, 9, 9);  -- 18 MOD 9 = 0
  u_17_9   : entity work.tb_dp_rsn_source generic map (17, 9, 9);  -- 17 MOD 9 = 8 = g_block_size - 1
  u_19_9   : entity work.tb_dp_rsn_source generic map (19, 9, 9);  -- 19 MOD 9 = 1
  u_20_9   : entity work.tb_dp_rsn_source generic map (20, 9, 9);  -- 20 MOD 9 = 2
  u_25_9   : entity work.tb_dp_rsn_source generic map (25, 9, 9);  -- 25 MOD 9 = 7
  u_26_9   : entity work.tb_dp_rsn_source generic map (26, 9, 9);  -- 26 MOD 9 = 8 = g_block_size - 1
  u_27_9   : entity work.tb_dp_rsn_source generic map (27, 9, 9);  -- 27 MOD 9 = 0

  -- test some prime values
  u_17_3   : entity work.tb_dp_rsn_source generic map (17, 3, 3);  -- 17 // 3 = 5, 17 MOD 3 = 2, 17/3 = 5.66 block/sync
  u_101_17 : entity work.tb_dp_rsn_source generic map (101, 17, 17);  -- 101 // 17 = 5, 101 MOD 17 = 16, 101/17 = 5.9411 block/sync
end tb;
