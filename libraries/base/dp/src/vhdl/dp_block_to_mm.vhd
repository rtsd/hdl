-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker
-- Purpose:
-- . get a block of data from a stream and write it to a memory mapped (MM) location.
-- Description:
-- . https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+filterbank
-- --------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_block_to_mm is
  generic (
    g_data_size  : natural;
    g_step_size  : natural;
    g_nof_data   : natural
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    start_address : in  natural range 0 to g_step_size * g_nof_data;
    mm_mosi       : out t_mem_mosi;
    in_sosi       : in  t_dp_sosi
  );
end dp_block_to_mm;

architecture rtl of dp_block_to_mm is
  constant c_mem_size : natural := g_step_size * g_nof_data;

  type t_reg is record
    wr         : std_logic;
    word_index : natural;
    step_index : natural;
  end record;

  constant c_reg_rst : t_reg := ('0', 0, 0);

  signal r : t_reg;
  signal d : t_reg;
  signal address : natural := 0;
begin
  address <= start_address + r.word_index + r.step_index;

  p_mm_mosi : process(address, in_sosi, d)
  begin
    mm_mosi <= c_mem_mosi_rst;  -- default to avoid ** Warning: (vsim-8684) No drivers exist on out port mm_mosi.rd
    mm_mosi.address <= TO_MEM_ADDRESS(address);
    mm_mosi.wrdata  <= RESIZE_MEM_DATA(in_sosi.data);
    mm_mosi.wr      <= d.wr;
  end process;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(clk) then
      r <= d;
    end if;
  end process;

  p_comb : process(r, in_sosi)
  begin
    d <= r;
    d.wr <= '0';
    -- while receiving block
    if in_sosi.valid = '1' then
      -- continue with block
      if r.step_index <= c_mem_size then
        d.wr <= '1';
        if r.word_index < g_data_size - 1 then
          d.word_index <= r.word_index + 1;
        else
          d.word_index <= 0;
          d.step_index <= r.step_index + g_step_size;
        end if;
      end if;
    end if;
    -- if end of block
    if in_sosi.eop = '1' then
      -- prepare for next block
      d.word_index <= 0;
      d.step_index <= 0;
    end if;
  end process;
end rtl;
