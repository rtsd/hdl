--------------------------------------------------------------------------------
--   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
--   Copyright (C) 2009-2010
--   ASTRON (Netherlands Institute for Radio Astronomy)
--   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
--   This file is part of the UniBoard software suite.
--   The file is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

library ieee, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity rTwoBFStage is
  generic (
    -- generics for this stage
    g_nof_chan      : natural := 0;  -- Exponent of nr of subbands (0 means 1 subband)
    g_stage         : natural;  -- The stage indices are ..., 3, 2, 1. The input stage has the highest index, the output stage has index 1.
    g_bf_lat        : natural := 1;  -- Digital pipelining latency
    -- generics for rTwoBF
    g_bf_use_zdly   : natural := 1;  -- >= 1. Stage high downto g_bf_use_zdly will will use g_bf_in_a_zdly and g_bf_out_zdly
    g_bf_in_a_zdly  : natural := 0;  -- g_bf_in_a_zdly+g_bf_out_d_zdly must be <= the stage z^(-1) delay, note that stage 1 has only one z^(-1) delay
    g_bf_out_d_zdly : natural := 0  -- The stage z^(-1) delays are ..., 4, 2, 1.
  );
  port (
    clk      : in  std_logic;
    rst      : in  std_logic;
    in_re    : in  std_logic_vector;
    in_im    : in  std_logic_vector;
    in_val   : in  std_logic;
    in_sel   : in  std_logic;
    out_re   : out std_logic_vector;
    out_im   : out std_logic_vector;
    out_val  : out std_logic;
    out_sel  : out std_logic
  );
end entity rTwoBFStage;

architecture str of rTwoBFStage is
  -- Optionally move some z-1 delay into this BF stage, default 0
  constant c_bf_in_a_zdly   : natural := sel_a_b(g_stage >= g_bf_use_zdly, g_bf_in_a_zdly, 0);
  constant c_bf_out_b_zdly  : natural := sel_a_b(g_stage >= g_bf_use_zdly, g_bf_out_d_zdly, 0);

  constant c_bf_zdly        : natural := c_bf_in_a_zdly + c_bf_out_b_zdly;
  constant c_feedback_zdly  : natural := pow2(g_stage-1);

  -- The BF adds, subtracts or passes the data on, so typically c_out_dat_w = c_in_dat_w + 1
  constant c_in_dat_w       : natural := in_re'length;  -- re and im have same width
  constant c_out_dat_w      : natural := out_re'length;  -- re and im have same width

  -- Concatenate im & re into complex data to potentially ease synthesis to make more efficient use of block RAM memory for z-1 data feedback line
  signal bf_complex        : std_logic_vector(c_nof_complex * c_in_dat_w - 1 downto 0);
  signal bf_complex_dly    : std_logic_vector(bf_complex'range);
  signal bf_re             : std_logic_vector(in_re'range);
  signal bf_re_dly         : std_logic_vector(in_re'range);
  signal bf_im             : std_logic_vector(in_im'range);
  signal bf_im_dly         : std_logic_vector(in_im'range);
  signal bf_sel            : std_logic;
  signal bf_val            : std_logic;
  signal bf_val_dly        : std_logic;

  signal stage_complex     : std_logic_vector(c_nof_complex * c_out_dat_w - 1 downto 0);
  signal stage_complex_dly : std_logic_vector(stage_complex'range);
  signal stage_re          : std_logic_vector(out_re'range);
  signal stage_im          : std_logic_vector(out_im'range);
  signal stage_sel         : std_logic;
  signal stage_val         : std_logic;
begin
  ------------------------------------------------------------------------------
  -- butterfly
  ------------------------------------------------------------------------------

  u_bf_re : entity work.rTwoBF
  generic map (
    g_in_a_zdly  => c_bf_in_a_zdly,
    g_out_d_zdly => c_bf_out_b_zdly
  )
  port map (
    clk     => clk,
    in_a    => bf_re_dly,
    in_b    => in_re,
    in_sel  => in_sel,
    in_val  => in_val,
    out_c   => stage_re,
    out_d   => bf_re
  );

  u_bf_im : entity work.rTwoBF
  generic map (
    g_in_a_zdly  => c_bf_in_a_zdly,
    g_out_d_zdly => c_bf_out_b_zdly
  )
  port map (
    clk     => clk,
    in_a    => bf_im_dly,
    in_b    => in_im,
    in_sel  => in_sel,
    in_val  => in_val,
    out_c   => stage_im,
    out_d   => bf_im
  );

  ------------------------------------------------------------------------------
  -- feedback fifo
  ------------------------------------------------------------------------------

  bf_sel <= in_sel;
  bf_val <= in_val;

  bf_complex <= bf_im & bf_re;
  bf_re_dly  <= bf_complex_dly(  c_in_dat_w - 1 downto 0);
  bf_im_dly  <= bf_complex_dly(2 * c_in_dat_w - 1 downto c_in_dat_w);

  -- share FIFO for Im & Re
  u_feedback : entity common_lib.common_delay
  generic map (
    g_dat_w  => bf_complex'length,
    g_depth  => c_feedback_zdly * (2**g_nof_chan) - c_bf_zdly
  )
  port map (
    clk     => clk,
    in_dat  => bf_complex,
    in_val  => bf_val,
    out_dat => bf_complex_dly
  );

  -- compensate for feedback fifo
  u_stage_sel : entity common_lib.common_bit_delay
  generic map (
    g_depth => c_feedback_zdly * (2**g_nof_chan)
  )
  port map (
    clk     => clk,
    rst     => rst,
    in_clr  => '0',
    in_val  => bf_val,
    in_bit  => bf_sel,
    out_bit => stage_sel
  );

  -- compensate for feedback fifo
  u_stage_val : entity common_lib.common_bit_delay
  generic map (
    g_depth => c_feedback_zdly * (2**g_nof_chan)
  )
  port map (
    clk     => clk,
    rst     => rst,
    in_clr  => '0',
    in_val  => bf_val,
    in_bit  => bf_val,
    out_bit => bf_val_dly
  );

  -- after the z^(-1) stage delay the bf_val_dly goes high and remains high and acts as an enable for in_val to out_val
  stage_val <= in_val and bf_val_dly;

  ------------------------------------------------------------------------------
  -- stage output pipelining
  ------------------------------------------------------------------------------

  stage_complex <= stage_im & stage_re;

  u_pipeline_out : entity common_lib.common_pipeline
  generic map (
    g_pipeline  => g_bf_lat,
    g_in_dat_w  => stage_complex'length,
    g_out_dat_w => stage_complex'length
  )
  port map (
    clk     => clk,
    in_dat  => stage_complex,
    out_dat => stage_complex_dly
  );

  out_re <= stage_complex_dly(  c_out_dat_w - 1 downto 0);
  out_im <= stage_complex_dly(2 * c_out_dat_w - 1 downto c_out_dat_w);

  u_out_sel : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => g_bf_lat
  )
  port map (
    clk     => clk,
    in_dat  => stage_sel,
    out_dat => out_sel
  );

  u_out_val : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => g_bf_lat
  )
  port map (
    clk     => clk,
    in_dat  => stage_val,
    out_dat => out_val
  );
end str;
