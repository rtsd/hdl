-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- The inferred Altera code was obtained using template insert with Quartus 14.0a10.
-- Quartus II VHDL Template
-- Simple Dual-Port RAM with different read/write addresses but
-- single read/write clock

library ieee;
use ieee.std_logic_1164.all;

entity ip_arria10_e2sg_simple_dual_port_ram_single_clock is
  generic
  (
    DATA_WIDTH : natural := 8;
    ADDR_WIDTH : natural := 6
  );
  port
  (
    clk   : in std_logic;
    raddr : in natural range 0 to 2**ADDR_WIDTH - 1;
    waddr : in natural range 0 to 2**ADDR_WIDTH - 1;
    data  : in std_logic_vector((DATA_WIDTH - 1) downto 0);
    we    : in std_logic := '1';
    q   : out std_logic_vector((DATA_WIDTH - 1) downto 0)
  );
end ip_arria10_e2sg_simple_dual_port_ram_single_clock;

architecture rtl of ip_arria10_e2sg_simple_dual_port_ram_single_clock is
  -- Build a 2-D array type for the RAM
  subtype word_t is std_logic_vector((DATA_WIDTH - 1) downto 0);
  type memory_t is array(2**ADDR_WIDTH - 1 downto 0) of word_t;

  -- Declare the RAM signal.
  signal ram : memory_t;
begin
  process(clk)
  begin
  if(rising_edge(clk)) then
    if(we = '1') then
      ram(waddr) <= data;
    end if;

    -- On a read during a write to the same address, the read will
    -- return the OLD data at the address
    q <= ram(raddr);
  end if;
  end process;
end rtl;
