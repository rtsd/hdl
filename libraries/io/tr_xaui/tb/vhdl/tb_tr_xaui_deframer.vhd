-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for tr_xaui_framer --> tr_xaui_deframer
-- Description:
--   The tb is self checking.
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_tr_xaui_deframer is
end tb_tr_xaui_deframer;

architecture tb of tb_tr_xaui_deframer is
  constant c_clk_period   : time    := 10 ns;
  constant c_rl           : natural := 1;
  constant c_tx_init      : natural := 0;

  signal tb_end           : std_logic := '0';
  signal rst              : std_logic;
  signal clk              : std_logic := '1';

  signal tx_enable        : std_logic;

  signal tx_siso          : t_dp_siso;
  signal tx_sosi          : t_dp_sosi;

  signal gap_siso         : t_dp_siso;
  signal gap_sosi         : t_dp_sosi;

  signal xgmii_tx_d       : std_logic_vector(c_xgmii_data_w - 1 downto 0);
  signal xgmii_tx_c       : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0);

  signal xgmii_rx_d       : std_logic_vector(c_xgmii_data_w - 1 downto 0);
  signal xgmii_rx_c       : std_logic_vector(c_xgmii_nof_lanes - 1 downto 0);

  signal deframer_sosi    : t_dp_sosi;
  signal deframer_siso    : t_dp_siso;

  signal verify_en        : std_logic;
  signal prev_data1       : std_logic_vector(c_xgmii_data_w - 1 downto 0);
  signal prev_data2       : std_logic_vector(c_xgmii_data_w - 1 downto 0);

  signal xgmii_rx_d_hi      : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);
  signal xgmii_rx_d_lo      : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);

  signal xgmii_tx_d_hi      : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);
  signal xgmii_tx_d_lo      : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);

  signal prev_xgmii_tx_d_hi : std_logic_vector(c_xgmii_data_w / 2 - 1 downto 0);
  signal prev_xgmii_tx_c_hi : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0);

  signal xgmii_tx_c_hi      : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0);
  signal xgmii_tx_c_lo      : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0);

  signal xgmii_rx_c_hi      : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0);
  signal xgmii_rx_c_lo      : std_logic_vector(c_xgmii_nof_lanes / 2 - 1 downto 0);

  signal misalign           : std_logic;
begin
  rst <= '1', '0' after c_clk_period * 7;
  clk <= not clk or tb_end after c_clk_period / 2;

  xgmii_tx_d_hi <= xgmii_tx_d(c_xgmii_data_w  - 1 downto c_xgmii_data_w / 2);
  xgmii_tx_d_lo <= xgmii_tx_d(c_xgmii_data_w / 2 - 1 downto 0);

  xgmii_tx_c_hi <= xgmii_tx_c(c_xgmii_nof_lanes  - 1 downto c_xgmii_nof_lanes / 2);
  xgmii_tx_c_lo <= xgmii_tx_c(c_xgmii_nof_lanes / 2 - 1 downto 0);

  xgmii_rx_d <= xgmii_rx_d_hi & xgmii_rx_d_lo;
  xgmii_rx_c <= xgmii_rx_c_hi & xgmii_rx_c_lo;

  p_stim : process
  begin
    tx_enable <= '0';
    verify_en <= '0';
    misalign  <= '0';

    wait for c_clk_period * 100;
    tx_enable <= '1';
    gap_siso.ready <= '1';

    wait for c_clk_period * 20;
    verify_en <= '1';

    -- Let run for a while with aligned data
    wait for 5 us;

    for I in 0 to 3 loop
      -- Now change the 32b word boundary
      wait until deframer_sosi.valid = '0';
      wait until rising_edge(clk);
      misalign <= not misalign;

      -- Run for a while
      wait for 5 us;
    end loop;

    -- Stop the simulation
    tb_end <= '1';
    assert false
      report "Simulation finished."
      severity NOTE;
    wait;
  end process;

   -- Generate tx_sosi for DUT using counter data generator
  proc_dp_gen_data(c_rl, c_xgmii_data_w, c_tx_init, rst, clk, tx_enable, tx_siso, tx_sosi);

  -- Introduce some gaps in the streaming data to make sure framer handles them correctly.
  u_dp_gap : entity dp_lib.dp_gap
  generic map (
    g_dat_len    => 40,
    g_gap_len    => 20
  )
  port map (
    rst       => rst,
    clk       => clk,

    snk_out   => tx_siso,
    snk_in    => tx_sosi,

    src_in    => gap_siso,
    src_out   => gap_sosi
  );

  u_framer : entity work.tr_xaui_framer
  generic map (
    g_dat_len => 100,
    g_gap_len => 5
   )
  port map (
    tx_rst     => rst,
    tx_clk     => clk,

    snk_out    => gap_siso,
    snk_in     => gap_sosi,

    xgmii_tx_d => xgmii_tx_d,
    xgmii_tx_c => xgmii_tx_c
  );

  -- ============== We'll emulate misalignment here ===========================

  -- Aligned data:
  -- RX Hi:  0 1 2 3 4 5
  -- RX Lo:  0 1 2 3 4 5
  xgmii_rx_d_hi <= xgmii_tx_d_lo when misalign = '1' else xgmii_tx_d_hi;
  xgmii_rx_c_hi <= xgmii_tx_c_lo when misalign = '1' else xgmii_tx_c_hi;

  -- Misligned data:
  -- RX Hi:  1 2 3 4 5 6
  -- RX Lo:  0 1 2 3 4 5
  xgmii_rx_d_lo <= prev_xgmii_tx_d_hi when misalign = '1' else xgmii_tx_d_lo;
  xgmii_rx_c_lo <= prev_xgmii_tx_c_hi when misalign = '1' else xgmii_tx_c_lo;

  p_clk : process(clk, rst)
  begin
    if rst = '1' then
      prev_xgmii_tx_d_hi <= x"07070707";
      prev_xgmii_tx_c_hi <= x"F";
    elsif rising_edge(clk) then
      prev_xgmii_tx_d_hi <= xgmii_tx_d_hi;
      prev_xgmii_tx_c_hi <= xgmii_tx_c_hi;
    end if;
  end process;

  -- ===========================================================================

  dut : entity work.tr_xaui_deframer
  port map (
    rx_rst     => rst,
    rx_clk     => clk,

    xgmii_rx_d => xgmii_rx_d,
    xgmii_rx_c => xgmii_rx_c,

    src_out    => deframer_sosi

  );

  -- Verify DUT output incrementing data, prev_data is an auxiliary signal needed by the proc
  proc_common_verify_data(c_rl, clk, verify_en, deframer_siso.ready, deframer_sosi.valid, deframer_sosi.data(c_xgmii_data_w - 1 downto 0), prev_data1);
  proc_dp_verify_data("data", c_rl, clk, verify_en, deframer_siso.ready, deframer_sosi.valid, deframer_sosi.data(c_xgmii_data_w - 1 downto 0), prev_data2);
end tb;
