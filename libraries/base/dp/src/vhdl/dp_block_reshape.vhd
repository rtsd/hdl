--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Eric Kooistra, 26 Apr 2018
-- Purpose:
-- . Reshape the block size by redefining the sop and eop. The sync interval
--   must remain the same. To also change the output sync interval use
--   dp_block_reshape_sync.vhd.
--
-- Description:
--   The g_reshape_nof_data_per_blk must fit in the sync interval, to keep the
--   output sync interval the same as the input sync interval.
--
--   The first snk_in.valid yields the first src_out.sop. The
--   g_reshape_nof_data_per_blk then defines the src_out.eop. Subsequent
--   src_out.sop and src_out.eop occur after every g_reshape_nof_data_per_blk
--   input valids. The snk_in.sop and snk_in.eop are ignored. The other sosi
--   fields are treated as follows:
--
--   * sync:
--     The snk_in.sync is passed on to the output sync. Hence the output sync
--     interval contains the same number of data is the input sync interval.
--     The user must take care that the sync interval will contain an integer
--     multiple of g_reshape_nof_data_per_blk, because otherwise the
--     src_out.sync will not always occur at an src_out.sop.
--     same as the input sync interval.
--   * bsn, channel are passed on unchanged. This means that at the first sop at
--     the sync they are correct, but for the other src_out.sop in sync
--     interval they fit the input block rate instead of the output block rate.
--     Hence the global BSN at the input sync is passed on correctly to the
--     output sync.
--   * empty, error are passed on unchanged. This means that at the last eop
--     before the next sync they are correct, but for the other src_out.eop in
--     sync interval they fit the input block rate instead of the output block
--     rate.
--
--   The advantage of passing the sop sosi fields and eop sosi fields unchanged
--   is that a next dp_block_reshape instance that reshapes back to the original
--   snk_in block size recovers them.
--
--   The dp_block_reshape supports pipelining and flow control, because it for
--   that it relies on dp_counter.
--
--   The src_index_arr[1:0] with sop index and valid index are also output. The
--   sop index uses g_input_nof_data_per_sync and g_nof_counters = 2, instead
--   of only 1. If the src_index_arr is left OPEN (unused), then this extra
--   dp_counter level will get optimized away by synthesis and then the value
--   of g_input_nof_data_per_sync is don't care.
--
--   The dp_block_reshape resembles dp_block_gen. The differences are in:
--   . details in how they treat the sync, BSN and channel
--   . dp_block_gen can act as block source, dp_block_reshape can only alter
--     an input block stream.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_block_reshape is
  generic (
    g_input_nof_data_per_sync  : natural := 2;  -- nof data per input sync interval, used only for sop_index
    g_reshape_nof_data_per_blk : natural := 2;  -- nof data per output block
    g_pipeline_src_out         : natural := 1;  -- Pipeline source outputs (all sosi)
    g_pipeline_src_in          : natural := 0  -- Pipeline source inputs (all siso). This will also pipeline src_out.
  );
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;

    snk_in        : in  t_dp_sosi;
    snk_out       : out t_dp_siso;

    src_out       : out t_dp_sosi;
    src_in        : in  t_dp_siso := c_dp_siso_rdy;
    src_index_arr : out t_natural_arr(1 downto 0)  -- [1] sop index, [0] valid index
  );
end dp_block_reshape;

architecture str of dp_block_reshape is
  constant c_nof_counters       : natural := 2;  -- counter [0] is used for block reshape and valid index,
                                                 -- counter [1] is only used for sop_index
  constant c_nof_block_per_sync : natural := sel_a_b(g_input_nof_data_per_sync > g_reshape_nof_data_per_blk,
                                                     g_input_nof_data_per_sync / g_reshape_nof_data_per_blk, 1);

  constant c_range_start   : t_nat_natural_arr(c_nof_counters - 1 downto 0) := (others => 0);
  constant c_range_stop    : t_nat_natural_arr(c_nof_counters - 1 downto 0) := (c_nof_block_per_sync, g_reshape_nof_data_per_blk);
  constant c_range_step    : t_nat_natural_arr(c_nof_counters - 1 downto 0) := (others => 1);

  signal cnt_sosi_arr      : t_dp_sosi_arr(c_nof_counters - 1 downto 0);
  signal input_src_out     : t_dp_sosi;
begin
  u_dp_counter : entity work.dp_counter
  generic map (
    g_nof_counters     => c_nof_counters,
    g_range_start      => c_range_start,
    g_range_stop       => c_range_stop,
    g_range_step       => c_range_step,
    g_pipeline_src_out => g_pipeline_src_out,
    g_pipeline_src_in  => g_pipeline_src_in
  )
  port map (
    clk               => clk,
    rst               => rst,

    snk_in            => snk_in,
    snk_out           => snk_out,

    src_out           => input_src_out,
    src_in            => src_in,

    count_src_out_arr => cnt_sosi_arr
  );

  src_index_arr(1) <= TO_UINT(cnt_sosi_arr(1).data);  -- sop index
  src_index_arr(0) <= TO_UINT(cnt_sosi_arr(0).data);  -- valid index

  p_wire : process(input_src_out, cnt_sosi_arr)
  begin
    src_out     <= input_src_out;
    src_out.sop <= cnt_sosi_arr(0).sop;
    src_out.eop <= cnt_sosi_arr(0).eop;
  end process;
end str;
