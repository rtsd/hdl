-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Job van Wee
-- Purpose: Spread out the input sosi over a sosi array.
--
-- Description:
--  The input sosi gets split up and spread out over the sosi array.
--
-- Remark:
--  Use VHDL coding template from:
--  https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
--  The output vector must be larger than the input vector.

library IEEE, dp_lib, common_lib;
use IEEE.std_logic_1164.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_pkg.all;

entity ddrctrl_output_repack is
  generic (
    g_nof_streams   : positive := 12;
    g_data_w        : natural  := 14
  );
  port (
    in_sosi         : in  t_dp_sosi                               := c_dp_sosi_init;
    out_sosi_arr    : out t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_init)
  );
end ddrctrl_output_repack;

architecture rtl of ddrctrl_output_repack is
begin
  -- putting the data from the stream into different streams.
  gen_repack_data : for I in 0 to g_nof_streams - 1 generate
    out_sosi_arr(I).data(g_data_w - 1 downto 0)           <= in_sosi.data(g_data_w * (I + 1) - 1 downto g_data_w * I);
    out_sosi_arr(I).bsn(c_dp_stream_bsn_w - 1 downto 0)   <= in_sosi.bsn(c_dp_stream_bsn_w - 1 downto 0);
    out_sosi_arr(I).valid                               <= in_sosi.valid;
    out_sosi_arr(I).sop                                 <= in_sosi.sop;
    out_sosi_arr(I).eop                                 <= in_sosi.eop;
  end generate;
end rtl;
