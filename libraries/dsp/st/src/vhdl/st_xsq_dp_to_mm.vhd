-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author : R. vd Walle
-- Purpose:
-- . Write a block of xsq data with size (g_nof_crosslets * g_nof_signal_inputs)
--   to RAM.
-- Description:
-- After every in_sosi.sop the st_xsq_dp_to_mm.vhd writes the block of data
-- to RAM and can be read by MM.
-- --------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity st_xsq_dp_to_mm is
  generic (
    g_nof_crosslets      : natural;
    g_nof_signal_inputs  : natural;
    g_dsp_data_w         : natural := 16
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    in_sosi       : in  t_dp_sosi;
    mm_mosi       : in t_mem_mosi;
    mm_miso       : out t_mem_miso;
    out_sosi_info : out t_dp_sosi
  );
end st_xsq_dp_to_mm;

architecture rtl of st_xsq_dp_to_mm is
  constant c_nof_data : natural :=  g_nof_crosslets * g_nof_signal_inputs;
  constant c_mm_ram_adr_w : natural := ceil_log2(c_nof_data);
  constant c_mm_ram_dat_w : natural := c_nof_complex * g_dsp_data_w;

  signal ram_wr_mosi   :  t_mem_mosi := c_mem_mosi_rst;

  signal reg_sosi_info : t_dp_sosi := c_dp_sosi_rst;
  signal next_page : std_logic;
begin
  u_dp_block_to_mm : entity dp_lib.dp_block_to_mm
  generic map(
    g_data_size => 1,
    g_step_size => 1,
    g_nof_data => c_nof_data
  )
  port map (
    rst => rst,
    clk => clk,
    start_address => 0,
    mm_mosi => ram_wr_mosi,
    in_sosi => in_sosi
  );

  u_common_paged_ram_r_w : entity common_lib.common_paged_ram_r_w
  generic map(
    g_data_w        => c_mm_ram_dat_w,
    g_page_sz       => c_nof_data,
    g_wr_start_page => 0,
    g_rd_start_page => 1
  )
  port map (
    rst => rst,
    clk => clk,
    wr_next_page => in_sosi.eop,
    wr_adr       => ram_wr_mosi.address(c_mm_ram_adr_w - 1 downto 0),
    wr_en        => ram_wr_mosi.wr,
    wr_dat       => ram_wr_mosi.wrdata(c_mm_ram_dat_w - 1 downto 0),
    rd_next_page => in_sosi.eop,
    rd_adr       => mm_mosi.address(c_mm_ram_adr_w - 1 downto 0),
    rd_en        => mm_mosi.rd,
    rd_dat       => mm_miso.rddata(c_mm_ram_dat_w - 1 downto 0),
    rd_val       => mm_miso.rdval
  );

  p_control : process(rst, clk)
  begin
    if rst = '1' then
      out_sosi_info <= c_dp_sosi_rst;
      reg_sosi_info <= c_dp_sosi_rst;
      next_page <= '0';
    elsif rising_edge(clk) then
      if in_sosi.sop = '1' then
        reg_sosi_info <= in_sosi;
      end if;
      if in_sosi.eop = '1' then
        next_page <= '1';
        out_sosi_info <= reg_sosi_info;
        out_sosi_info.eop <= '1';
        out_sosi_info.err <= in_sosi.err;
      else
        out_sosi_info <= c_dp_sosi_rst;
        next_page <= '0';
      end if;
    end if;
  end process;
end rtl;
