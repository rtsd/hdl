library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package pft_pkg is
  constant c_pft_stage_dat_w : natural := 20;

  type pft_mode_type is (
    PFT_MODE_BITREV,
    PFT_MODE_COMPLEX,
    PFT_MODE_REAL2
  );

  type pft_bf_type is (
    PFT_BF1,
    PFT_BF2
  );
end pft_pkg;

package body pft_pkg is
end pft_pkg;
