-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose: Deinterleave input into g_nof_out output streams based on g_block_size.
-- Description:
--   The output streams are concatenated into one SLV. Only one output is active
--   at a time. The active output is selected automatically and incrementally,
--   starting with output 0. The next output is selected after g_block_size
--   valid words on the currently selected output.
-- Remarks:

entity common_deinterleave is
  generic (
    g_nof_out    : natural;
    g_dat_w      : natural;
    g_block_size : natural;
    g_align_out  : boolean := false
 );
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;

    in_dat      : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val      : in  std_logic;

    out_dat     : out std_logic_vector(g_nof_out * g_dat_w - 1 downto 0);
    out_val     : out std_logic_vector(g_nof_out - 1 downto 0)
  );
end;

architecture rtl of common_deinterleave is
  -----------------------------------------------------------------------------
  -- Multiplexer input selection control
  -----------------------------------------------------------------------------
  constant c_demux_out_sel_w : natural := ceil_log2(g_nof_out);

  signal demux_out_sel       : std_logic_vector(c_demux_out_sel_w - 1 downto 0);
  signal nxt_demux_out_sel   : std_logic_vector(c_demux_out_sel_w - 1 downto 0);
  signal sch_demux_out_sel   : std_logic_vector(c_demux_out_sel_w - 1 downto 0);

  signal demux_out_dat       : std_logic_vector(g_nof_out * g_dat_w - 1 downto 0);
  signal demux_out_val       : std_logic_vector(g_nof_out - 1 downto 0);

  constant c_demux_val_cnt_w  : natural := ceil_log2(g_block_size+1);
  signal demux_val_cnt        : std_logic_vector(c_demux_val_cnt_w - 1 downto 0);
  signal nxt_demux_val_cnt    : std_logic_vector(c_demux_val_cnt_w - 1 downto 0);
begin
  u_demux : entity work.common_demultiplexer
  generic map (
    g_nof_out => g_nof_out,
    g_dat_w   => g_dat_w
  )
  port map (
    in_dat     => in_dat,
    in_val     => in_val,

    out_sel    => demux_out_sel,
    out_dat    => demux_out_dat,
    out_val    => demux_out_val
  );

  -----------------------------------------------------------------------------
  -- Demultiplexer output selection
  -----------------------------------------------------------------------------
  -- Scheduled input
  sch_demux_out_sel <= INCR_UVEC(demux_out_sel, 1) when unsigned(demux_out_sel) < g_nof_out - 1 else (others => '0');

  p_nxt_demux_out_sel : process(in_val, demux_out_sel, demux_val_cnt, sch_demux_out_sel)
  begin
    nxt_demux_out_sel    <= demux_out_sel;
    nxt_demux_val_cnt    <= demux_val_cnt;

    if in_val = '1' then
      if unsigned(demux_val_cnt) = g_block_size-1 then
        nxt_demux_val_cnt <= TO_UVEC(0, c_demux_val_cnt_w);
        nxt_demux_out_sel  <= sch_demux_out_sel;
      else
        nxt_demux_val_cnt <= INCR_UVEC(demux_val_cnt, 1);
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      demux_out_sel    <= (others => '0');
      demux_val_cnt    <= (others => '0');
    elsif rising_edge(clk) then
      demux_out_sel    <= nxt_demux_out_sel;
      demux_val_cnt    <= nxt_demux_val_cnt;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Forward the deinterleaved data to the outputs without alignment
  -----------------------------------------------------------------------------
  gen_no_align: if g_align_out = false generate
    out_dat <= demux_out_dat;
    out_val <= demux_out_val;
  end generate;

  -----------------------------------------------------------------------------
  -- Align the output streams by adding pipeline stages
  -----------------------------------------------------------------------------
  gen_align_out: if g_align_out = true generate
    gen_inter: for i in 0 to g_nof_out - 1 generate
      u_shiftreg : entity work.common_shiftreg
      generic map (
        g_pipeline  => g_nof_out * g_block_size - (i + 1) * g_block_size,
        g_nof_dat   => 1,
        g_dat_w     => g_dat_w
      )
      port map (
        rst          => rst,
        clk          => clk,

        in_dat       => demux_out_dat(i * g_dat_w + g_dat_w - 1 downto i * g_dat_w),
        in_val       => demux_out_val(i),

        out_dat      => out_dat(i * g_dat_w + g_dat_w - 1 downto i * g_dat_w),
        out_val      => out_val(i)
      );
    end generate;
  end generate;
end rtl;
