-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
--
-- Author: E. Kooistra, 26 feb 2023
-- Purpose: Regression multi tb for tb_lofar2_unb2c_sdp_station_fsub
-- Description:
--   The multi tb only has one instance, so the tb_tb is more a wrapper to
--   ensure that always the same tb generics are used in the regression test.
--   This allows modifying the generics in the tb.
-- Usage:
-- > as 4
-- > run -all

library IEEE, lofar2_sdp_lib;
use IEEE.std_logic_1164.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity tb_tb_lofar2_unb2c_sdp_station_fsub is
end tb_tb_lofar2_unb2c_sdp_station_fsub;

architecture tb of tb_tb_lofar2_unb2c_sdp_station_fsub is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
-- Commented to save sim time in regression test
--  u_fsub_only_co : ENTITY work.tb_lofar2_unb2c_sdp_station_fsub
--  GENERIC MAP (
--    g_sp                            => 3,     -- signal path index in range(S_pn = 12) of co-polarization
--    g_co_wg_ampl                    => 0.5,   -- WG normalized amplitude, use same WG settings for both polarizations (g_sp and c_cross_sp)
--    g_cross_wg_ampl                 => 0.4,   -- WG normalized amplitude, use same WG settings for both polarizations (g_sp and c_cross_sp)
--    g_cross_wg_phase                => 90.0,  -- WG phase in degrees for cross-sp, relative to co-sp
--    g_subband                       => 102,   -- select subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
--    g_co_subband_weight_gain        => 1.0,   -- subband weight normalized gain, for co-polarization in g_sp
--    g_co_subband_weight_phase       => 30.0,  -- subband weight phase rotation in degrees, for co-polarization in g_sp
--    g_use_cross_weight              => FALSE,
--    g_sp_cross_subband_weight_gain  => 0.5,   -- subband weight normalized gain, for cross polarization of g_sp
--    g_sp_cross_subband_weight_phase => -10.0, -- subband weight phase rotation in degrees, for cross polarization of g_sp
--    g_read_all_SST                  => TRUE   -- when FALSE only read SST for g_subband, to save sim time
--  );

  u_fsub_use_cross : entity work.tb_lofar2_unb2c_sdp_station_fsub
  generic map (
    g_sp                            => 3,  -- signal path index in range(S_pn = 12) of co-polarization
    g_co_wg_ampl                    => 0.5,  -- WG normalized amplitude, use same WG settings for both polarizations (g_sp and c_cross_sp)
    g_cross_wg_ampl                 => 0.4,  -- WG normalized amplitude, use same WG settings for both polarizations (g_sp and c_cross_sp)
    g_cross_wg_phase                => 90.0,  -- WG phase in degrees for cross-sp, relative to co-sp
    g_subband                       => 102,  -- select subband at index 102 = 102/1024 * 200MHz = 19.921875 MHz
    g_co_subband_weight_gain        => 1.0,  -- subband weight normalized gain, for co-polarization in g_sp
    g_co_subband_weight_phase       => 30.0,  -- subband weight phase rotation in degrees, for co-polarization in g_sp
    g_use_cross_weight              => true,
    g_sp_cross_subband_weight_gain  => 0.5,  -- subband weight normalized gain, for cross polarization of g_sp
    g_sp_cross_subband_weight_phase => -10.0,  -- subband weight phase rotation in degrees, for cross polarization of g_sp
    g_read_all_SST                  => true  -- when FALSE only read SST for g_subband, to save sim time
  );
end tb;
