-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: E. Kooistra
-- Purpose:
--   Count strobes
-- Description:
-- * g_nof_counts >= 1, maximum c_nof_counts_max = 15
--   There are g_nof_counts in parallel, one clear that clears them all
--   . count any strobe like e.g. sync, sop, valid, error flag, ...
--   . MM write of clear registers clears all counter immediately, the
--     clear data value is dont care.
--   . the count clips at 2**g_count_w-1 in case of overflow when g_clip =
--     TRUE, else it wraps and continues from 0.
-- * g_count_w <= 64
--   Internally the counts have width g_count_w <= 64, on the MM interface
--   all counts use 2 words, so 64 bits.
-- * ref_sync:
--   . Enable or re-enable count starts at a ref_sync.
--   . MM read of a count shows the strobe count that is captured at a ref_sync
--    (default input ref_sync = '1' when not used)
-- * g_mm_w = c_word_w
--   Default use g_mm_w = 32b for MM, but support < 32 to ease verification of
--   count values > 2**g_mm_w.
--
-------------------------------------------------------------------------------
-- REGMAP
-- . address span is (c_nof_counts_max + 1)*2 = 32
-- . address width is c_dp_strobe_total_count_reg_adr_w - ceil_log2(32) = 5
-------------------------------------------------------------------------------
--  wi                          Bits    R/W Name                        Default
--  ===========================================================================
--  0                           [31..0] RO  count[0]                    0x0
--  1                           [63.32]
--  2                           [31..0] RO  count[1]                    0x0
--  3                           [63.32]
--  .                            .      .   .                           .
--  .                            .
--  (g_nof_counts-1)*2          [31..0] RO  count[g_nof_counts-1]       0x0
--  (g_nof_counts-1)*2 + 1      [63.32]
--  .                            .      .   .                           .
--  .                            .
--  (c_nof_counts_max-1)*2      [31..0] RO  count[c_nof_counts_max-1]   0x0
--  (c_nof_counts_max-1)*2 + 1  [63.32]
--  c_nof_counts_max*2          [31..0] RW  clear                       0x0
--  c_nof_counts_max*2 + 1      [31..0] RW  flush                       0x0
--  ===========================================================================
--
-- Remark:
-- * This dp_strobe_total_count could have been a common_strobe_total_count
--   component, because it does not use sosi/siso signals. However it is fine
--   to keep it in dp_lib, to avoid extra work of moving and renaming.
-- * Use MM clear to clear the total counters, or use in_clr input signal.
-- * The MM counter values are held at sync. Use flush to force the current
--   last counter values into the MM counter values. This is useful if the
--   ref_sync stopped already.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_components_pkg.all;

entity dp_strobe_total_count is
  generic (
    g_mm_w           : natural := c_word_w;
    g_nof_counts     : natural := 1;  -- actual nof counts, <= c_nof_counts_max
    g_count_w        : natural := c_longword_w;  -- actual count width, max is c_longword_w due to two mm word width
    g_clip           : boolean := true
  );
  port (
    dp_rst        : in std_logic;
    dp_clk        : in std_logic;

    ref_sync      : in std_logic := '1';
    in_strobe_arr : in std_logic_vector(g_nof_counts - 1 downto 0);
    in_clr        : in std_logic := '0';

    mm_rst        : in std_logic;
    mm_clk        : in std_logic;

    reg_mosi      : in t_mem_mosi := c_mem_mosi_rst;
    reg_miso      : out t_mem_miso
  );
end dp_strobe_total_count;

architecture rtl of dp_strobe_total_count is
  -- Fixed by REGMAP
  constant c_nof_counts_max : natural := c_dp_strobe_total_count_reg_nof_counts_max;
  constant c_clear_adr      : natural := c_dp_strobe_total_count_reg_clear_adr;
  constant c_flush_adr      : natural := c_dp_strobe_total_count_reg_flush_adr;

  -- Define the size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => c_dp_strobe_total_count_reg_adr_w,
                                  dat_w    => g_mm_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => c_dp_strobe_total_count_reg_nof_words,
                                  init_sl  => '0');

  type t_cnt_arr is array (integer range <>) of std_logic_vector(g_count_w - 1 downto 0);

  -- Registers in dp_clk domain
  signal ref_sync_reg       : std_logic := '0';
  signal ref_sync_reg2      : std_logic := '0';
  signal in_strobe_reg_arr  : std_logic_vector(g_nof_counts - 1 downto 0) := (others => '0');
  signal in_strobe_reg2_arr : std_logic_vector(g_nof_counts - 1 downto 0) := (others => '0');
  signal rd_reg             : std_logic_vector(c_mm_reg.nof_dat * c_mm_reg.dat_w - 1 downto 0) := (others => '0');
  signal mm_cnt_clear       : std_logic;
  signal mm_cnt_flush       : std_logic;
  signal dp_cnt_clear       : std_logic;
  signal dp_cnt_flush       : std_logic;
  signal cnt_clr            : std_logic := '0';
  signal cnt_en             : std_logic := '0';
  signal cnt_en_arr         : std_logic_vector(g_nof_counts - 1 downto 0);
  signal cnt_arr            : t_cnt_arr(g_nof_counts - 1 downto 0);
  signal hold_cnt_arr       : t_cnt_arr(g_nof_counts - 1 downto 0) := (others => (others => '0'));
begin
  assert g_nof_counts <= c_nof_counts_max
    report "Too many counters to fit REGMAP."
    severity FAILURE;
  assert g_count_w <= g_mm_w * 2
    report "Too wide counter to fit REGMAP."
    severity FAILURE;

  -- Only clear on MM write, to allow MM read of all register fields without clear
  mm_cnt_clear <= reg_mosi.wr when TO_UINT(reg_mosi.address(c_mm_reg.adr_w - 1 downto 0)) = c_clear_adr else '0';
  mm_cnt_flush <= reg_mosi.wr when TO_UINT(reg_mosi.address(c_mm_reg.adr_w - 1 downto 0)) = c_flush_adr else '0';

  u_common_spulse_clear : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_cnt_clear,
    out_rst   => dp_rst,
    out_clk   => dp_clk,
    out_pulse => dp_cnt_clear
  );

  -- Support cnt clear via either MM or via an input strobe, use register
  -- to ease timing closure
  cnt_clr <= dp_cnt_clear or in_clr when rising_edge(dp_clk);

  u_common_spulse_flush : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_cnt_flush,
    out_rst   => dp_rst,
    out_clk   => dp_clk,
    out_pulse => dp_cnt_flush
  );

  -- Register inputs to ease timing closure
  -- . register ref_sync to ease timing closure for ref_sync fanout
  -- . register in_strobe_arr to preserve alignment with ref_sync
  ref_sync_reg <= ref_sync when rising_edge(dp_clk);
  in_strobe_reg_arr <= in_strobe_arr when rising_edge(dp_clk);

  -- Register inputs again to be able to align registered cnt_en and
  -- pipelined ref_sync pulse
  ref_sync_reg2 <= ref_sync_reg when rising_edge(dp_clk);
  in_strobe_reg2_arr <= in_strobe_reg_arr when rising_edge(dp_clk);

  -- . clear strobe counters immediately at cnt_clr
  -- . start strobe counters after ref_sync, e.g. to align strobe counters
  --   in different nodes in case the input was (already) active during
  --   the cnt_clr
  p_cnt_en : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      cnt_en <= '0';
    elsif rising_edge(dp_clk) then
      if cnt_clr = '1' then
        cnt_en <= '0';
      elsif ref_sync_reg = '1' then
        -- use ref_sync_reg have cnt_en high aligned with ref_sync_reg2
        cnt_en <= '1';
      end if;
    end if;
  end process;

  -- strobe counters
  gen_counters : for I in 0 to g_nof_counts - 1 generate
    cnt_en_arr(I) <= cnt_en and in_strobe_reg2_arr(I);

    u_counter : entity common_lib.common_counter
    generic map (
      g_width => g_count_w,
      g_clip  => g_clip
    )
    port map (
      rst => dp_rst,
      clk => dp_clk,

      cnt_clr => cnt_clr,
      cnt_en  => cnt_en_arr(I),
      count   => cnt_arr(I)
    );
  end generate;

  -- Hold counter values at ref_sync_reg2 to have stable values for MM read
  -- for comparision between nodes. Use mm_cnt_flush/dp_cnt_flush to support
  -- observing the current counter values via MM.
  p_hold_counters : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      if cnt_clr = '1' then
        hold_cnt_arr <= (others => (others => '0'));
      elsif ref_sync_reg2 = '1' or dp_cnt_flush = '1' then
        hold_cnt_arr <= cnt_arr;
      end if;
    end if;
  end process;

  -- Register mapping
  gen_cnt : for I in 0 to g_nof_counts - 1 generate
    gen_reg_32b : if g_count_w <= g_mm_w generate
      rd_reg((2 * I + 1) * g_mm_w - 1 downto (2 * I + 0) * g_mm_w) <= RESIZE_UVEC(hold_cnt_arr(I), g_mm_w);  -- low part
      rd_reg((2 * I + 2) * g_mm_w - 1 downto (2 * I + 1) * g_mm_w) <= (others => '0');  -- high part (not used)
    end generate;

    gen_reg_64b : if g_count_w > g_mm_w generate
      rd_reg((2 * I + 1) * g_mm_w - 1 downto (2 * I + 0) * g_mm_w) <= hold_cnt_arr(I)(g_mm_w - 1 downto 0);  -- low part
      rd_reg((2 * I + 2) * g_mm_w - 1 downto (2 * I + 1) * g_mm_w) <= RESIZE_UVEC(hold_cnt_arr(I)(g_count_w - 1 downto g_mm_w), g_mm_w);  -- high part
    end generate;
  end generate;

  u_reg : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => true,
    g_readback           => false,
    g_reg                => c_mm_reg
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => dp_rst,
    st_clk      => dp_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => reg_mosi,
    sla_out     => reg_miso,

    -- MM registers in st_clk domain
    reg_wr_arr  => OPEN,
    reg_rd_arr  => OPEN,
    in_reg      => rd_reg,  -- read only
    out_reg     => open  -- no write
  );
end rtl;
