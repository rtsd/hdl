-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify dp_flush
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-- . The p_verify_flush process checks the flushing
-- . Observe m.flush_en and m.snk_in and m.src_out signals in Wave Window

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_flush is
  generic (
    g_rl           : natural := 0;  -- this tb supports ready latency is 0 or 1
    g_framed_xon   : boolean := true;
    g_framed_xoff  : boolean := true;
    g_in_en        : t_dp_flow_control_enum := e_random;  -- always e_active, e_random or e_pulse flow control
    g_out_ready    : t_dp_flow_control_enum := e_random;  -- always e_active, e_random or e_pulse flow control
    g_nof_repeat   : natural := 100
  );
end tb_dp_flush;

architecture tb of tb_dp_flush is
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  constant c_flush_en_w               : natural := 8;

  signal tb_end             : std_logic := '0';
  signal clk                : std_logic := '1';
  signal rst                : std_logic := '1';

  signal random_0           : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1           : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0            : std_logic;
  signal pulse_1            : std_logic;
  signal pulse_en           : std_logic := '1';

  signal out_ready          : std_logic := '1';
  signal in_en              : std_logic := '1';
  signal in_siso            : t_dp_siso;
  signal in_sosi            : t_dp_sosi := c_dp_sosi_rst;

  signal out_siso           : t_dp_siso := c_dp_siso_rdy;
  signal out_sosi           : t_dp_sosi;
  signal prev_out_ready     : std_logic_vector(g_rl - 1 downto 0);

  -- Signals for easier monitoring in wave window
  type t_mon is record
    snk_out_ready      : std_logic;
    snk_out_xon        : std_logic;
    snk_in_sync        : std_logic;
    snk_in_valid       : std_logic;
    snk_in_sop         : std_logic;
    snk_in_eop         : std_logic;
    flush_en           : std_logic;
    src_in_ready       : std_logic;
    src_in_xon         : std_logic;
    src_out_sync       : std_logic;
    src_out_valid      : std_logic;
    src_out_sop        : std_logic;
    src_out_eop        : std_logic;
    src_out_gap        : std_logic;
  end record;

  signal verify_en                     : std_logic := '0';

  signal m                             : t_mon := ('X', 'X', 'X', 'X', 'X', 'X', '0', 'X', 'X', 'X', 'X', 'X', 'X', '1');
  signal reg_m                         : t_mon := ('X', 'X', 'X', 'X', 'X', 'X', '0', 'X', 'X', 'X', 'X', 'X', 'X', '1');

  signal flush_en                      : std_logic := '0';
  signal flush_en_dly                  : std_logic_vector(0 to g_rl);

  signal reg_mode_flush_en_streaming   : std_logic;
  signal reg_mode_flush_en_framed      : std_logic := '0';
  signal reg_mode_flush_en_framed_xon  : std_logic := '0';
  signal reg_mode_flush_en_framed_xoff : std_logic := '0';

  signal reg_mode_flush_en             : std_logic;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en     <= '1'                     when g_in_en = e_active else
               random_0(random_0'high) when g_in_en = e_random else
               pulse_0                 when g_in_en = e_pulse;

  out_ready <= '1'                     when g_out_ready = e_active else
               random_1(random_1'high) when g_out_ready = e_random else
               pulse_1                 when g_out_ready = e_pulse;

  out_siso.ready <= out_ready when g_rl /= 0 else out_ready and out_sosi.valid;  -- request for RL > 0, acknowledge for RL = 0

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli : process
    constant c_sync_period_w   : natural := 2;
    constant c_sync_bsn        : natural := 1;
    constant c_block_size      : natural := 31;
    constant c_bsn_init        : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0877665544332211";
    constant c_data_w          : natural := 16;
    constant c_data_init       : integer := 0;
    constant c_channel_init    : integer := 0;
    constant c_err_init        : integer := 0;

    variable v_sync            : std_logic := '0';
    variable v_bsn             : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := c_bsn_init;
    variable v_data            : natural := 0;
  begin
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    while true loop
      -- No gaps between the blocks
      proc_dp_gen_block_data(g_rl, true, c_data_w, c_data_w, v_data, 0, 0, c_block_size, c_channel_init, c_err_init, v_sync, v_bsn, clk, in_en, in_siso, in_sosi);
      v_sync      := sel_a_b(unsigned(v_bsn(c_sync_period_w - 1 downto 0)) = c_sync_bsn - 1, '1', '0');  -- sync pulse every at 2**c_sync_period_w block
      v_bsn       := INCR_UVEC(v_bsn, 1);
      v_data      := v_data + c_block_size;
    end loop;

    -- End of stimuli
    wait;
  end process;

  p_flush_stimuli : process
  begin
    flush_en <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    for I in 1 to g_nof_repeat loop
      flush_en <= '1';
      proc_common_wait_some_cycles(clk, 1);  -- at least one cycle
      proc_common_wait_some_cycles(clk, TO_UINT(random_0(c_flush_en_w - 1 downto 0)));  -- random nof cycles
      flush_en <= '0';
      proc_common_wait_some_cycles(clk, 1);  -- at least one cycle
      proc_common_wait_some_cycles(clk, TO_UINT(random_1(c_flush_en_w - 1 downto 0)));  -- random nof cycles
    end loop;
    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  flush_en_dly(0)         <= flush_en;
  flush_en_dly(1 to g_rl) <= flush_en_dly(0 to g_rl - 1) when rising_edge(clk);

  -- These flush_en mode signals suit both g_rl=0 and g_rl>0
  -- Use registered signals to avoid issues with different delta-cycle delays between both actors in_sosi.sop and flush_en_dly
  reg_mode_flush_en_streaming   <= '1' when rising_edge(clk) and                     flush_en_dly(0)   = '1' else
                                   '0' when rising_edge(clk) and                     flush_en_dly(g_rl) = '0';  -- equivalent to: reg_mode_flush_en_streaming <= vector_or(flush_en_dly);
  reg_mode_flush_en_framed      <= '1' when rising_edge(clk) and in_sosi.sop = '1' and flush_en_dly(0)   = '1' else
                                   '0' when rising_edge(clk) and in_sosi.sop = '1' and flush_en_dly(g_rl) = '0';
  reg_mode_flush_en_framed_xon  <= '1' when rising_edge(clk) and                     flush_en_dly(0)   = '1' else
                                   '0' when rising_edge(clk) and in_sosi.sop = '1' and flush_en_dly(g_rl) = '0';
  reg_mode_flush_en_framed_xoff <= '1' when rising_edge(clk) and in_sosi.sop = '1' and flush_en_dly(0)   = '1' else
                                   '0' when rising_edge(clk) and                     flush_en_dly(g_rl) = '0';

  reg_mode_flush_en <= reg_mode_flush_en_streaming   when g_framed_xon = false and g_framed_xoff = false else
                       reg_mode_flush_en_framed      when g_framed_xon = true  and g_framed_xoff = true  else
                       reg_mode_flush_en_framed_xon  when g_framed_xon = true  and g_framed_xoff = false else
                       reg_mode_flush_en_framed_xoff;  -- g_framed_xon=FALSE AND g_framed_xoff=TRUE

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  verify_en <= '1';

  -- Verify some general streaming interface properties
  proc_dp_verify_valid(g_rl, clk, verify_en, out_siso.ready, prev_out_ready, out_sosi.valid);  -- Verify that the out_sosi valid fits with the ready latency

  gen_framed : if g_framed_xon = true and g_framed_xoff = true generate
    proc_dp_verify_gap_invalid(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, m.src_out_gap);  -- Verify that there is no valid data between frames, init out_gap at '1'
  end generate;

  -- Verify the flush modes
  p_verify_flush : process(clk)
  begin
    if rising_edge(clk) then
      if verify_en = '1' then
        if reg_mode_flush_en = '1' then
          if reg_m.src_out_sync = '1' then
            report "DP : Wrong sync, should have been flushed"
              severity ERROR;
          end if;
          if reg_m.src_out_valid = '1' then
            report "DP : Wrong valid, should have been flushed"
              severity ERROR;
          end if;
          if reg_m.src_out_sop = '1' then
            report "DP : Wrong sop, should have been flushed"
              severity ERROR;
          end if;
          if reg_m.src_out_eop = '1' then
            report "DP : Wrong eop, should have been flushed"
              severity ERROR;
          end if;
        else
          if reg_m.snk_in_sync /= reg_m.src_out_sync then
            report "DP : Missing sync, should not have been flushed"
              severity ERROR;
          end if;
          if reg_m.snk_in_valid /= reg_m.src_out_valid then
            report "DP : Missing valid, should not have been flushed"
              severity ERROR;
          end if;
          if reg_m.snk_in_sop /= reg_m.src_out_sop then
            report "DP : Missing sop, should not have been flushed"
              severity ERROR;
          end if;
          if reg_m.snk_in_eop /= reg_m.src_out_eop then
            report "DP : Missing eop, should not have been flushed"
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- DUT : DP_FLUSH
  ------------------------------------------------------------------------------

  u_dut: entity work.dp_flush
  generic map (
    g_ready_latency => g_rl,
    g_framed_xon    => g_framed_xon,
    g_framed_xoff   => g_framed_xoff
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_in       => in_sosi,
    snk_out      => in_siso,
    -- ST source
    src_in       => out_siso,
    src_out      => out_sosi,
    -- Enable flush
    flush_en     => flush_en
  );

  -- Map record field to signals for easier monitoring in wave window
  m.snk_out_ready <= in_siso.ready;
  m.snk_out_xon   <= in_siso.xon;
  m.snk_in_sync   <= in_sosi.sync;
  m.snk_in_valid  <= in_sosi.valid;
  m.snk_in_sop    <= in_sosi.sop;
  m.snk_in_eop    <= in_sosi.eop;
  m.flush_en      <= flush_en;
  m.src_in_ready  <= out_siso.ready;
  m.src_in_xon    <= out_siso.xon;
  m.src_out_sync  <= out_sosi.sync;
  m.src_out_valid <= out_sosi.valid;
  m.src_out_sop   <= out_sosi.sop;
  m.src_out_eop   <= out_sosi.eop;

  -- Use reg_m is registered m signal to match the register stage in reg_mode_flush_en
  reg_m <= m when rising_edge(clk);
end tb;
