-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;

package tech_tse_component_pkg is
  ------------------------------------------------------------------------------
  -- ip_stratixiv
  ------------------------------------------------------------------------------

  -- Copied from $HDL_WORK/libraries/technology/ip_stratixiv/tse_sgmii_lvds/ip_stratixiv_tse_sgmii_lvds.vhd
  component ip_stratixiv_tse_sgmii_lvds is
  port (
     address  : in  std_logic_vector(7 downto 0);
     clk  : in  std_logic;
     ff_rx_a_empty  : out  std_logic;
     ff_rx_a_full : out  std_logic;
     ff_rx_clk  : in  std_logic;
     ff_rx_data : out  std_logic_vector(31 downto 0);
     ff_rx_dsav : out  std_logic;
     ff_rx_dval : out  std_logic;
     ff_rx_eop  : out  std_logic;
     ff_rx_mod  : out  std_logic_vector(1 downto 0);
     ff_rx_rdy  : in  std_logic;
     ff_rx_sop  : out  std_logic;
     ff_tx_a_empty  : out  std_logic;
     ff_tx_a_full : out  std_logic;
     ff_tx_clk  : in  std_logic;
     ff_tx_crc_fwd  : in  std_logic;
     ff_tx_data : in  std_logic_vector(31 downto 0);
     ff_tx_eop  : in  std_logic;
     ff_tx_err  : in  std_logic;
     ff_tx_mod  : in  std_logic_vector(1 downto 0);
     ff_tx_rdy  : out  std_logic;
     ff_tx_septy  : out  std_logic;
     ff_tx_sop  : in  std_logic;
     ff_tx_wren : in  std_logic;
     led_an : out  std_logic;
     led_char_err : out  std_logic;
     led_disp_err : out  std_logic;
     led_link : out  std_logic;
     read : in  std_logic;
     readdata : out  std_logic_vector(31 downto 0);
     ref_clk  : in  std_logic;
     reset  : in  std_logic;
     rx_err : out  std_logic_vector(5 downto 0);
     rx_err_stat  : out  std_logic_vector(17 downto 0);
     rx_frm_type  : out  std_logic_vector(3 downto 0);
     rxp  : in  std_logic;
     tx_ff_uflow  : out  std_logic;
     txp  : out  std_logic;
     waitrequest  : out  std_logic;
     write  : in  std_logic;
     writedata  : in  std_logic_vector(31 downto 0)
  );
  end component;

  -- Copied from $HDL_WORK/libraries/technology/ip_stratixiv/tse_sgmii_gx/ip_stratixiv_tse_sgmii_gx.vhd
  component ip_stratixiv_tse_sgmii_gx is
  port (
    address  : in  std_logic_vector(7 downto 0);
    clk  : in  std_logic;
    ff_rx_a_empty  : out  std_logic;
    ff_rx_a_full : out  std_logic;
    ff_rx_clk  : in  std_logic;
    ff_rx_data : out  std_logic_vector(31 downto 0);
    ff_rx_dsav : out  std_logic;
    ff_rx_dval : out  std_logic;
    ff_rx_eop  : out  std_logic;
    ff_rx_mod  : out  std_logic_vector(1 downto 0);
    ff_rx_rdy  : in  std_logic;
    ff_rx_sop  : out  std_logic;
    ff_tx_a_empty  : out  std_logic;
    ff_tx_a_full : out  std_logic;
    ff_tx_clk  : in  std_logic;
    ff_tx_crc_fwd  : in  std_logic;
    ff_tx_data : in  std_logic_vector(31 downto 0);
    ff_tx_eop  : in  std_logic;
    ff_tx_err  : in  std_logic;
    ff_tx_mod  : in  std_logic_vector(1 downto 0);
    ff_tx_rdy  : out  std_logic;
    ff_tx_septy  : out  std_logic;
    ff_tx_sop  : in  std_logic;
    ff_tx_wren : in  std_logic;
    gxb_cal_blk_clk  : in  std_logic;
    led_an : out  std_logic;
    led_char_err : out  std_logic;
    led_disp_err : out  std_logic;
    led_link : out  std_logic;
    read : in  std_logic;
    readdata : out  std_logic_vector(31 downto 0);
    reconfig_clk : in  std_logic;
    reconfig_fromgxb : out  std_logic_vector(16 downto 0);
    reconfig_togxb : in  std_logic_vector(3 downto 0);
    ref_clk  : in  std_logic;
    reset  : in  std_logic;
    rx_err : out  std_logic_vector(5 downto 0);
    rx_err_stat  : out  std_logic_vector(17 downto 0);
    rx_frm_type  : out  std_logic_vector(3 downto 0);
    rxp  : in  std_logic;
    tx_ff_uflow  : out  std_logic;
    txp  : out  std_logic;
    waitrequest  : out  std_logic;
    write  : in  std_logic;
    writedata  : in  std_logic_vector(31 downto 0)
  );
  end component;

  component ip_stratixiv_gxb_reconfig_v101 is
  generic (
    g_nof_gx        : natural;
    g_fromgxb_bus_w : natural := 17;
    g_togxb_bus_w   : natural := 4
  );
  port (
    reconfig_clk     : in std_logic;
    reconfig_fromgxb : in std_logic_vector(tech_ceil_div(g_nof_gx, 4) * g_fromgxb_bus_w - 1 downto 0);
    busy             : out std_logic;
    reconfig_togxb   : out std_logic_vector(g_togxb_bus_w - 1 downto 0)
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10
  ------------------------------------------------------------------------------

  -- Copied from $HDL_BUILD_DIR/sim/ip_arria10_tse_sgmii_lvds.vhd
  component ip_arria10_tse_sgmii_lvds is
  port (
    clk            : in  std_logic                     := '0';  -- control_port_clock_connection.clk
    reset          : in  std_logic                     := '0';  -- reset_connection.reset
    reg_data_out   : out std_logic_vector(31 downto 0);  -- control_port.readdata
    reg_rd         : in  std_logic                     := '0';  -- .read
    reg_data_in    : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reg_wr         : in  std_logic                     := '0';  -- .write
    reg_busy       : out std_logic;  -- .waitrequest
    reg_addr       : in  std_logic_vector(7 downto 0)  := (others => '0');  -- .address
    ff_rx_clk      : in  std_logic                     := '0';  -- receive_clock_connection.clk
    ff_tx_clk      : in  std_logic                     := '0';  -- transmit_clock_connection.clk
    ff_rx_data     : out std_logic_vector(31 downto 0);  -- receive.data
    ff_rx_eop      : out std_logic;  -- .endofpacket
    rx_err         : out std_logic_vector(5 downto 0);  -- .error
    ff_rx_mod      : out std_logic_vector(1 downto 0);  -- .empty
    ff_rx_rdy      : in  std_logic                     := '0';  -- .ready
    ff_rx_sop      : out std_logic;  -- .startofpacket
    ff_rx_dval     : out std_logic;  -- .valid
    ff_tx_data     : in  std_logic_vector(31 downto 0) := (others => '0');  -- transmit.data
    ff_tx_eop      : in  std_logic                     := '0';  -- .endofpacket
    ff_tx_err      : in  std_logic                     := '0';  -- .error
    ff_tx_mod      : in  std_logic_vector(1 downto 0)  := (others => '0');  -- .empty
    ff_tx_rdy      : out std_logic;  -- .ready
    ff_tx_sop      : in  std_logic                     := '0';  -- .startofpacket
    ff_tx_wren     : in  std_logic                     := '0';  -- .valid
    ff_tx_crc_fwd  : in  std_logic                     := '0';  -- mac_misc_connection.ff_tx_crc_fwd
    ff_tx_septy    : out std_logic;  -- .ff_tx_septy
    tx_ff_uflow    : out std_logic;  -- .tx_ff_uflow
    ff_tx_a_full   : out std_logic;  -- .ff_tx_a_full
    ff_tx_a_empty  : out std_logic;  -- .ff_tx_a_empty
    rx_err_stat    : out std_logic_vector(17 downto 0);  -- .rx_err_stat
    rx_frm_type    : out std_logic_vector(3 downto 0);  -- .rx_frm_type
    ff_rx_dsav     : out std_logic;  -- .ff_rx_dsav
    ff_rx_a_full   : out std_logic;  -- .ff_rx_a_full
    ff_rx_a_empty  : out std_logic;  -- .ff_rx_a_empty
    led_crs        : out std_logic;  -- status_led_connection.crs
    led_link       : out std_logic;  -- .link
    led_col        : out std_logic;  -- .col
    led_an         : out std_logic;  -- .an
    led_char_err   : out std_logic;  -- .char_err
    led_disp_err   : out std_logic;  -- .disp_err
    rx_recovclkout : out std_logic;  -- serdes_control_connection.export
    ref_clk        : in  std_logic                     := '0';  -- pcs_ref_clk_clock_connection.clk
    rxp            : in  std_logic                     := '0';  -- serial_connection.rxp_0
    txp            : out std_logic  -- .txp_0
  );
  end component;

  -- Copied from $HDL_BUILD_DIR/sim/ip_arria10_tse_sgmii_gx.vhd
  component ip_arria10_tse_sgmii_gx is
  port (
    clk                : in  std_logic                     := '0';  -- control_port_clock_connection.clk
    reset              : in  std_logic                     := '0';  -- reset_connection.reset
    reg_data_out       : out std_logic_vector(31 downto 0);  -- control_port.readdata
    reg_rd             : in  std_logic                     := '0';  -- .read
    reg_data_in        : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reg_wr             : in  std_logic                     := '0';  -- .write
    reg_busy           : out std_logic;  -- .waitrequest
    reg_addr           : in  std_logic_vector(7 downto 0)  := (others => '0');  -- .address
    ff_rx_clk          : in  std_logic                     := '0';  -- receive_clock_connection.clk
    ff_tx_clk          : in  std_logic                     := '0';  -- transmit_clock_connection.clk
    ff_rx_data         : out std_logic_vector(31 downto 0);  -- receive.data
    ff_rx_eop          : out std_logic;  -- .endofpacket
    rx_err             : out std_logic_vector(5 downto 0);  -- .error
    ff_rx_mod          : out std_logic_vector(1 downto 0);  -- .empty
    ff_rx_rdy          : in  std_logic                     := '0';  -- .ready
    ff_rx_sop          : out std_logic;  -- .startofpacket
    ff_rx_dval         : out std_logic;  -- .valid
    ff_tx_data         : in  std_logic_vector(31 downto 0) := (others => '0');  -- transmit.data
    ff_tx_eop          : in  std_logic                     := '0';  -- .endofpacket
    ff_tx_err          : in  std_logic                     := '0';  -- .error
    ff_tx_mod          : in  std_logic_vector(1 downto 0)  := (others => '0');  -- .empty
    ff_tx_rdy          : out std_logic;  -- .ready
    ff_tx_sop          : in  std_logic                     := '0';  -- .startofpacket
    ff_tx_wren         : in  std_logic                     := '0';  -- .valid
    ff_tx_crc_fwd      : in  std_logic                     := '0';  -- mac_misc_connection.ff_tx_crc_fwd
    ff_tx_septy        : out std_logic;  -- .ff_tx_septy
    tx_ff_uflow        : out std_logic;  -- .tx_ff_uflow
    ff_tx_a_full       : out std_logic;  -- .ff_tx_a_full
    ff_tx_a_empty      : out std_logic;  -- .ff_tx_a_empty
    rx_err_stat        : out std_logic_vector(17 downto 0);  -- .rx_err_stat
    rx_frm_type        : out std_logic_vector(3 downto 0);  -- .rx_frm_type
    ff_rx_dsav         : out std_logic;  -- .ff_rx_dsav
    ff_rx_a_full       : out std_logic;  -- .ff_rx_a_full
    ff_rx_a_empty      : out std_logic;  -- .ff_rx_a_empty
    led_crs            : out std_logic;  -- status_led_connection.crs
    led_link           : out std_logic;  -- .link
    led_col            : out std_logic;  -- .col
    led_an             : out std_logic;  -- .an
    led_char_err       : out std_logic;  -- .char_err
    led_disp_err       : out std_logic;  -- .disp_err
    rx_recovclkout     : out std_logic;  -- serdes_control_connection.export
    ref_clk            : in  std_logic                     := '0';  -- pcs_ref_clk_clock_connection.clk
    rxp                : in  std_logic                     := '0';  -- serial_connection.rxp
    txp                : out std_logic;  -- .txp
    tx_serial_clk      : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_serial_clk.clk
    rx_cdr_refclk      : in  std_logic                     := '0';  -- rx_cdr_refclk.clk
    tx_analogreset     : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_digitalreset    : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    rx_analogreset     : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_digitalreset    : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    tx_cal_busy        : out std_logic_vector(0 downto 0);  -- tx_cal_busy.tx_cal_busy
    rx_cal_busy        : out std_logic_vector(0 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_set_locktodata  : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_set_locktodata.rx_set_locktodata
    rx_set_locktoref   : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_set_locktoref.rx_set_locktoref
    rx_is_lockedtoref  : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_is_lockedtodata : out std_logic_vector(0 downto 0)  -- rx_is_lockedtodata.rx_is_lockedtodata
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  ------------------------------------------------------------------------------

  -- Copied from $HDL_BUILD_DIR/sim/ip_arria10_e3sge3_tse_sgmii_lvds.vhd
  component ip_arria10_e3sge3_tse_sgmii_lvds is
  port (
    reg_data_out   : out std_logic_vector(31 downto 0);  -- control_port.readdata
    reg_rd         : in  std_logic                     := '0';  -- .read
    reg_data_in    : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reg_wr         : in  std_logic                     := '0';  -- .write
    reg_busy       : out std_logic;  -- .waitrequest
    reg_addr       : in  std_logic_vector(7 downto 0)  := (others => '0');  -- .address
    clk            : in  std_logic                     := '0';  -- control_port_clock_connection.clk
    ff_tx_crc_fwd  : in  std_logic                     := '0';  -- mac_misc_connection.ff_tx_crc_fwd
    ff_tx_septy    : out std_logic;  -- .ff_tx_septy
    tx_ff_uflow    : out std_logic;  -- .tx_ff_uflow
    ff_tx_a_full   : out std_logic;  -- .ff_tx_a_full
    ff_tx_a_empty  : out std_logic;  -- .ff_tx_a_empty
    rx_err_stat    : out std_logic_vector(17 downto 0);  -- .rx_err_stat
    rx_frm_type    : out std_logic_vector(3 downto 0);  -- .rx_frm_type
    ff_rx_dsav     : out std_logic;  -- .ff_rx_dsav
    ff_rx_a_full   : out std_logic;  -- .ff_rx_a_full
    ff_rx_a_empty  : out std_logic;  -- .ff_rx_a_empty
    ref_clk        : in  std_logic                     := '0';  -- pcs_ref_clk_clock_connection.clk
    ff_rx_data     : out std_logic_vector(31 downto 0);  -- receive.data
    ff_rx_eop      : out std_logic;  -- .endofpacket
    rx_err         : out std_logic_vector(5 downto 0);  -- .error
    ff_rx_mod      : out std_logic_vector(1 downto 0);  -- .empty
    ff_rx_rdy      : in  std_logic                     := '0';  -- .ready
    ff_rx_sop      : out std_logic;  -- .startofpacket
    ff_rx_dval     : out std_logic;  -- .valid
    ff_rx_clk      : in  std_logic                     := '0';  -- receive_clock_connection.clk
    reset          : in  std_logic                     := '0';  -- reset_connection.reset
    rx_recovclkout : out std_logic;  -- serdes_control_connection.export
    rxp            : in  std_logic                     := '0';  -- serial_connection.rxp_0
    txp            : out std_logic;  -- .txp_0
    led_crs        : out std_logic;  -- status_led_connection.crs
    led_link       : out std_logic;  -- .link
    led_col        : out std_logic;  -- .col
    led_an         : out std_logic;  -- .an
    led_char_err   : out std_logic;  -- .char_err
    led_disp_err   : out std_logic;  -- .disp_err
    ff_tx_data     : in  std_logic_vector(31 downto 0) := (others => '0');  -- transmit.data
    ff_tx_eop      : in  std_logic                     := '0';  -- .endofpacket
    ff_tx_err      : in  std_logic                     := '0';  -- .error
    ff_tx_mod      : in  std_logic_vector(1 downto 0)  := (others => '0');  -- .empty
    ff_tx_rdy      : out std_logic;  -- .ready
    ff_tx_sop      : in  std_logic                     := '0';  -- .startofpacket
    ff_tx_wren     : in  std_logic                     := '0';  -- .valid
    ff_tx_clk      : in  std_logic                     := '0'  -- transmit_clock_connection.clk
  );
  end component;

  -- Copied from $HDL_BUILD_DIR/sim/ip_arria10_e3sge3_tse_sgmii_gx.vhd
  component ip_arria10_e3sge3_tse_sgmii_gx is
  port (
    reg_data_out       : out std_logic_vector(31 downto 0);  -- control_port.readdata
    reg_rd             : in  std_logic                     := '0';  -- .read
    reg_data_in        : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reg_wr             : in  std_logic                     := '0';  -- .write
    reg_busy           : out std_logic;  -- .waitrequest
    reg_addr           : in  std_logic_vector(7 downto 0)  := (others => '0');  -- .address
    clk                : in  std_logic                     := '0';  -- control_port_clock_connection.clk
    ff_tx_crc_fwd      : in  std_logic                     := '0';  -- mac_misc_connection.ff_tx_crc_fwd
    ff_tx_septy        : out std_logic;  -- .ff_tx_septy
    tx_ff_uflow        : out std_logic;  -- .tx_ff_uflow
    ff_tx_a_full       : out std_logic;  -- .ff_tx_a_full
    ff_tx_a_empty      : out std_logic;  -- .ff_tx_a_empty
    rx_err_stat        : out std_logic_vector(17 downto 0);  -- .rx_err_stat
    rx_frm_type        : out std_logic_vector(3 downto 0);  -- .rx_frm_type
    ff_rx_dsav         : out std_logic;  -- .ff_rx_dsav
    ff_rx_a_full       : out std_logic;  -- .ff_rx_a_full
    ff_rx_a_empty      : out std_logic;  -- .ff_rx_a_empty
    ref_clk            : in  std_logic                     := '0';  -- pcs_ref_clk_clock_connection.clk
    ff_rx_data         : out std_logic_vector(31 downto 0);  -- receive.data
    ff_rx_eop          : out std_logic;  -- .endofpacket
    rx_err             : out std_logic_vector(5 downto 0);  -- .error
    ff_rx_mod          : out std_logic_vector(1 downto 0);  -- .empty
    ff_rx_rdy          : in  std_logic                     := '0';  -- .ready
    ff_rx_sop          : out std_logic;  -- .startofpacket
    ff_rx_dval         : out std_logic;  -- .valid
    ff_rx_clk          : in  std_logic                     := '0';  -- receive_clock_connection.clk
    reset              : in  std_logic                     := '0';  -- reset_connection.reset
    rx_analogreset     : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : out std_logic_vector(0 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk      : in  std_logic                     := '0';  -- rx_cdr_refclk.clk
    rx_digitalreset    : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : out std_logic_vector(0 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref  : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_set_locktodata  : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_set_locktodata.rx_set_locktodata
    rx_set_locktoref   : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_set_locktoref.rx_set_locktoref
    rx_recovclkout     : out std_logic;  -- serdes_control_connection.export
    rxp                : in  std_logic                     := '0';  -- serial_connection.rxp
    txp                : out std_logic;  -- .txp
    led_crs            : out std_logic;  -- status_led_connection.crs
    led_link           : out std_logic;  -- .link
    led_panel_link     : out std_logic;  -- .panel_link
    led_col            : out std_logic;  -- .col
    led_an             : out std_logic;  -- .an
    led_char_err       : out std_logic;  -- .char_err
    led_disp_err       : out std_logic;  -- .disp_err
    ff_tx_data         : in  std_logic_vector(31 downto 0) := (others => '0');  -- transmit.data
    ff_tx_eop          : in  std_logic                     := '0';  -- .endofpacket
    ff_tx_err          : in  std_logic                     := '0';  -- .error
    ff_tx_mod          : in  std_logic_vector(1 downto 0)  := (others => '0');  -- .empty
    ff_tx_rdy          : out std_logic;  -- .ready
    ff_tx_sop          : in  std_logic                     := '0';  -- .startofpacket
    ff_tx_wren         : in  std_logic                     := '0';  -- .valid
    ff_tx_clk          : in  std_logic                     := '0';  -- transmit_clock_connection.clk
    tx_analogreset     : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : out std_logic_vector(0 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_serial_clk      : in  std_logic_vector(0 downto 0)  := (others => '0')  -- tx_serial_clk.clk
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e1sg
  ------------------------------------------------------------------------------

  -- Copied from $HDL_BUILD_DIR/sim/ip_arria10_e1sg_tse_sgmii_lvds.vhd
  component ip_arria10_e1sg_tse_sgmii_lvds is
  port (
    reg_data_out   : out std_logic_vector(31 downto 0);  -- control_port.readdata
    reg_rd         : in  std_logic                     := '0';  -- .read
    reg_data_in    : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reg_wr         : in  std_logic                     := '0';  -- .write
    reg_busy       : out std_logic;  -- .waitrequest
    reg_addr       : in  std_logic_vector(7 downto 0)  := (others => '0');  -- .address
    clk            : in  std_logic                     := '0';  -- control_port_clock_connection.clk
    ff_tx_crc_fwd  : in  std_logic                     := '0';  -- mac_misc_connection.ff_tx_crc_fwd
    ff_tx_septy    : out std_logic;  -- .ff_tx_septy
    tx_ff_uflow    : out std_logic;  -- .tx_ff_uflow
    ff_tx_a_full   : out std_logic;  -- .ff_tx_a_full
    ff_tx_a_empty  : out std_logic;  -- .ff_tx_a_empty
    rx_err_stat    : out std_logic_vector(17 downto 0);  -- .rx_err_stat
    rx_frm_type    : out std_logic_vector(3 downto 0);  -- .rx_frm_type
    ff_rx_dsav     : out std_logic;  -- .ff_rx_dsav
    ff_rx_a_full   : out std_logic;  -- .ff_rx_a_full
    ff_rx_a_empty  : out std_logic;  -- .ff_rx_a_empty
    ref_clk        : in  std_logic                     := '0';  -- pcs_ref_clk_clock_connection.clk
    ff_rx_data     : out std_logic_vector(31 downto 0);  -- receive.data
    ff_rx_eop      : out std_logic;  -- .endofpacket
    rx_err         : out std_logic_vector(5 downto 0);  -- .error
    ff_rx_mod      : out std_logic_vector(1 downto 0);  -- .empty
    ff_rx_rdy      : in  std_logic                     := '0';  -- .ready
    ff_rx_sop      : out std_logic;  -- .startofpacket
    ff_rx_dval     : out std_logic;  -- .valid
    ff_rx_clk      : in  std_logic                     := '0';  -- receive_clock_connection.clk
    reset          : in  std_logic                     := '0';  -- reset_connection.reset
    rx_recovclkout : out std_logic;  -- serdes_control_connection.export
    rxp            : in  std_logic                     := '0';  -- serial_connection.rxp_0
    txp            : out std_logic;  -- .txp_0
    led_crs        : out std_logic;  -- status_led_connection.crs
    led_link       : out std_logic;  -- .link
    led_col        : out std_logic;  -- .col
    led_an         : out std_logic;  -- .an
    led_char_err   : out std_logic;  -- .char_err
    led_disp_err   : out std_logic;  -- .disp_err
    ff_tx_data     : in  std_logic_vector(31 downto 0) := (others => '0');  -- transmit.data
    ff_tx_eop      : in  std_logic                     := '0';  -- .endofpacket
    ff_tx_err      : in  std_logic                     := '0';  -- .error
    ff_tx_mod      : in  std_logic_vector(1 downto 0)  := (others => '0');  -- .empty
    ff_tx_rdy      : out std_logic;  -- .ready
    ff_tx_sop      : in  std_logic                     := '0';  -- .startofpacket
    ff_tx_wren     : in  std_logic                     := '0';  -- .valid
    ff_tx_clk      : in  std_logic                     := '0'  -- transmit_clock_connection.clk
  );
  end component;

  -- Copied from $HDL_BUILD_DIR/sim/ip_arria10_e1sg_tse_sgmii_gx.vhd
  component ip_arria10_e1sg_tse_sgmii_gx is
  port (
    reg_data_out       : out std_logic_vector(31 downto 0);  -- control_port.readdata
    reg_rd             : in  std_logic                     := '0';  -- .read
    reg_data_in        : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reg_wr             : in  std_logic                     := '0';  -- .write
    reg_busy           : out std_logic;  -- .waitrequest
    reg_addr           : in  std_logic_vector(7 downto 0)  := (others => '0');  -- .address
    clk                : in  std_logic                     := '0';  -- control_port_clock_connection.clk
    ff_tx_crc_fwd      : in  std_logic                     := '0';  -- mac_misc_connection.ff_tx_crc_fwd
    ff_tx_septy        : out std_logic;  -- .ff_tx_septy
    tx_ff_uflow        : out std_logic;  -- .tx_ff_uflow
    ff_tx_a_full       : out std_logic;  -- .ff_tx_a_full
    ff_tx_a_empty      : out std_logic;  -- .ff_tx_a_empty
    rx_err_stat        : out std_logic_vector(17 downto 0);  -- .rx_err_stat
    rx_frm_type        : out std_logic_vector(3 downto 0);  -- .rx_frm_type
    ff_rx_dsav         : out std_logic;  -- .ff_rx_dsav
    ff_rx_a_full       : out std_logic;  -- .ff_rx_a_full
    ff_rx_a_empty      : out std_logic;  -- .ff_rx_a_empty
    ref_clk            : in  std_logic                     := '0';  -- pcs_ref_clk_clock_connection.clk
    ff_rx_data         : out std_logic_vector(31 downto 0);  -- receive.data
    ff_rx_eop          : out std_logic;  -- .endofpacket
    rx_err             : out std_logic_vector(5 downto 0);  -- .error
    ff_rx_mod          : out std_logic_vector(1 downto 0);  -- .empty
    ff_rx_rdy          : in  std_logic                     := '0';  -- .ready
    ff_rx_sop          : out std_logic;  -- .startofpacket
    ff_rx_dval         : out std_logic;  -- .valid
    ff_rx_clk          : in  std_logic                     := '0';  -- receive_clock_connection.clk
    reset              : in  std_logic                     := '0';  -- reset_connection.reset
    rx_analogreset     : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : out std_logic_vector(0 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk      : in  std_logic                     := '0';  -- rx_cdr_refclk.clk
    rx_digitalreset    : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : out std_logic_vector(0 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref  : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_set_locktodata  : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_set_locktodata.rx_set_locktodata
    rx_set_locktoref   : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_set_locktoref.rx_set_locktoref
    rx_recovclkout     : out std_logic;  -- serdes_control_connection.export
    rxp                : in  std_logic                     := '0';  -- serial_connection.rxp
    txp                : out std_logic;  -- .txp
    led_crs            : out std_logic;  -- status_led_connection.crs
    led_link           : out std_logic;  -- .link
    led_panel_link     : out std_logic;  -- .panel_link
    led_col            : out std_logic;  -- .col
    led_an             : out std_logic;  -- .an
    led_char_err       : out std_logic;  -- .char_err
    led_disp_err       : out std_logic;  -- .disp_err
    ff_tx_data         : in  std_logic_vector(31 downto 0) := (others => '0');  -- transmit.data
    ff_tx_eop          : in  std_logic                     := '0';  -- .endofpacket
    ff_tx_err          : in  std_logic                     := '0';  -- .error
    ff_tx_mod          : in  std_logic_vector(1 downto 0)  := (others => '0');  -- .empty
    ff_tx_rdy          : out std_logic;  -- .ready
    ff_tx_sop          : in  std_logic                     := '0';  -- .startofpacket
    ff_tx_wren         : in  std_logic                     := '0';  -- .valid
    ff_tx_clk          : in  std_logic                     := '0';  -- transmit_clock_connection.clk
    tx_analogreset     : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : out std_logic_vector(0 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_serial_clk      : in  std_logic_vector(0 downto 0)  := (others => '0')  -- tx_serial_clk.clk
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e2sg
  ------------------------------------------------------------------------------

  component ip_arria10_e2sg_tse_sgmii_lvds is
  port (
    reg_data_out   : out std_logic_vector(31 downto 0);  -- control_port.readdata
    reg_rd         : in  std_logic                     := '0';  -- .read
    reg_data_in    : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reg_wr         : in  std_logic                     := '0';  -- .write
    reg_busy       : out std_logic;  -- .waitrequest
    reg_addr       : in  std_logic_vector(7 downto 0)  := (others => '0');  -- .address
    clk            : in  std_logic                     := '0';  -- control_port_clock_connection.clk
    ff_tx_crc_fwd  : in  std_logic                     := '0';  -- mac_misc_connection.ff_tx_crc_fwd
    ff_tx_septy    : out std_logic;  -- .ff_tx_septy
    tx_ff_uflow    : out std_logic;  -- .tx_ff_uflow
    ff_tx_a_full   : out std_logic;  -- .ff_tx_a_full
    ff_tx_a_empty  : out std_logic;  -- .ff_tx_a_empty
    rx_err_stat    : out std_logic_vector(17 downto 0);  -- .rx_err_stat
    rx_frm_type    : out std_logic_vector(3 downto 0);  -- .rx_frm_type
    ff_rx_dsav     : out std_logic;  -- .ff_rx_dsav
    ff_rx_a_full   : out std_logic;  -- .ff_rx_a_full
    ff_rx_a_empty  : out std_logic;  -- .ff_rx_a_empty
    ref_clk        : in  std_logic                     := '0';  -- pcs_ref_clk_clock_connection.clk
    ff_rx_data     : out std_logic_vector(31 downto 0);  -- receive.data
    ff_rx_eop      : out std_logic;  -- .endofpacket
    rx_err         : out std_logic_vector(5 downto 0);  -- .error
    ff_rx_mod      : out std_logic_vector(1 downto 0);  -- .empty
    ff_rx_rdy      : in  std_logic                     := '0';  -- .ready
    ff_rx_sop      : out std_logic;  -- .startofpacket
    ff_rx_dval     : out std_logic;  -- .valid
    ff_rx_clk      : in  std_logic                     := '0';  -- receive_clock_connection.clk
    reset          : in  std_logic                     := '0';  -- reset_connection.reset
    rx_recovclkout : out std_logic;  -- serdes_control_connection.export
    rxp            : in  std_logic                     := '0';  -- serial_connection.rxp_0
    txp            : out std_logic;  -- .txp_0
    led_crs        : out std_logic;  -- status_led_connection.crs
    led_link       : out std_logic;  -- .link
    led_col        : out std_logic;  -- .col
    led_an         : out std_logic;  -- .an
    led_char_err   : out std_logic;  -- .char_err
    led_disp_err   : out std_logic;  -- .disp_err
    ff_tx_data     : in  std_logic_vector(31 downto 0) := (others => '0');  -- transmit.data
    ff_tx_eop      : in  std_logic                     := '0';  -- .endofpacket
    ff_tx_err      : in  std_logic                     := '0';  -- .error
    ff_tx_mod      : in  std_logic_vector(1 downto 0)  := (others => '0');  -- .empty
    ff_tx_rdy      : out std_logic;  -- .ready
    ff_tx_sop      : in  std_logic                     := '0';  -- .startofpacket
    ff_tx_wren     : in  std_logic                     := '0';  -- .valid
    ff_tx_clk      : in  std_logic                     := '0'  -- transmit_clock_connection.clk
  );
  end component;

  component ip_arria10_e2sg_tse_sgmii_gx is
  port (
    reg_data_out       : out std_logic_vector(31 downto 0);  -- control_port.readdata
    reg_rd             : in  std_logic                     := '0';  -- .read
    reg_data_in        : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    reg_wr             : in  std_logic                     := '0';  -- .write
    reg_busy           : out std_logic;  -- .waitrequest
    reg_addr           : in  std_logic_vector(7 downto 0)  := (others => '0');  -- .address
    clk                : in  std_logic                     := '0';  -- control_port_clock_connection.clk
    ff_tx_crc_fwd      : in  std_logic                     := '0';  -- mac_misc_connection.ff_tx_crc_fwd
    ff_tx_septy        : out std_logic;  -- .ff_tx_septy
    tx_ff_uflow        : out std_logic;  -- .tx_ff_uflow
    ff_tx_a_full       : out std_logic;  -- .ff_tx_a_full
    ff_tx_a_empty      : out std_logic;  -- .ff_tx_a_empty
    rx_err_stat        : out std_logic_vector(17 downto 0);  -- .rx_err_stat
    rx_frm_type        : out std_logic_vector(3 downto 0);  -- .rx_frm_type
    ff_rx_dsav         : out std_logic;  -- .ff_rx_dsav
    ff_rx_a_full       : out std_logic;  -- .ff_rx_a_full
    ff_rx_a_empty      : out std_logic;  -- .ff_rx_a_empty
    ref_clk            : in  std_logic                     := '0';  -- pcs_ref_clk_clock_connection.clk
    ff_rx_data         : out std_logic_vector(31 downto 0);  -- receive.data
    ff_rx_eop          : out std_logic;  -- .endofpacket
    rx_err             : out std_logic_vector(5 downto 0);  -- .error
    ff_rx_mod          : out std_logic_vector(1 downto 0);  -- .empty
    ff_rx_rdy          : in  std_logic                     := '0';  -- .ready
    ff_rx_sop          : out std_logic;  -- .startofpacket
    ff_rx_dval         : out std_logic;  -- .valid
    ff_rx_clk          : in  std_logic                     := '0';  -- receive_clock_connection.clk
    reset              : in  std_logic                     := '0';  -- reset_connection.reset
    rx_analogreset     : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_analogreset.rx_analogreset
    rx_cal_busy        : out std_logic_vector(0 downto 0);  -- rx_cal_busy.rx_cal_busy
    rx_cdr_refclk      : in  std_logic                     := '0';  -- rx_cdr_refclk.clk
    rx_digitalreset    : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_digitalreset.rx_digitalreset
    rx_is_lockedtodata : out std_logic_vector(0 downto 0);  -- rx_is_lockedtodata.rx_is_lockedtodata
    rx_is_lockedtoref  : out std_logic_vector(0 downto 0);  -- rx_is_lockedtoref.rx_is_lockedtoref
    rx_set_locktodata  : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_set_locktodata.rx_set_locktodata
    rx_set_locktoref   : in  std_logic_vector(0 downto 0)  := (others => '0');  -- rx_set_locktoref.rx_set_locktoref
    rx_recovclkout     : out std_logic;  -- serdes_control_connection.export
    rxp                : in  std_logic                     := '0';  -- serial_connection.rxp
    txp                : out std_logic;  -- .txp
    led_crs            : out std_logic;  -- status_led_connection.crs
    led_link           : out std_logic;  -- .link
    led_panel_link     : out std_logic;  -- .panel_link
    led_col            : out std_logic;  -- .col
    led_an             : out std_logic;  -- .an
    led_char_err       : out std_logic;  -- .char_err
    led_disp_err       : out std_logic;  -- .disp_err
    ff_tx_data         : in  std_logic_vector(31 downto 0) := (others => '0');  -- transmit.data
    ff_tx_eop          : in  std_logic                     := '0';  -- .endofpacket
    ff_tx_err          : in  std_logic                     := '0';  -- .error
    ff_tx_mod          : in  std_logic_vector(1 downto 0)  := (others => '0');  -- .empty
    ff_tx_rdy          : out std_logic;  -- .ready
    ff_tx_sop          : in  std_logic                     := '0';  -- .startofpacket
    ff_tx_wren         : in  std_logic                     := '0';  -- .valid
    ff_tx_clk          : in  std_logic                     := '0';  -- transmit_clock_connection.clk
    tx_analogreset     : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_analogreset.tx_analogreset
    tx_cal_busy        : out std_logic_vector(0 downto 0);  -- tx_cal_busy.tx_cal_busy
    tx_digitalreset    : in  std_logic_vector(0 downto 0)  := (others => '0');  -- tx_digitalreset.tx_digitalreset
    tx_serial_clk      : in  std_logic_vector(0 downto 0)  := (others => '0')  -- tx_serial_clk.clk
  );
  end component;
end tech_tse_component_pkg;
