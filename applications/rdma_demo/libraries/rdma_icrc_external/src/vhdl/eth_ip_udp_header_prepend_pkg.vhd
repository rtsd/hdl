--------------------------------------------------------------------------------
-- Title       : Ethernet/IP/UDP header Prepend package
-- Project     : Square Kilometer Array
--------------------------------------------------------------------------------
-- File        : eth_ip_udp_header_prepend_pkg.vhd
-- Author      : William Kamp <william.kamp@aut.ac.nz
-- Company     : High Performance Computing Research Lab, AUT
-- Created     : Thu Jul 11 15:41:59 2019
-- Last update : Tue Jan  7 16:06:03 2020
-- Platform    :
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2019 High Performance Computing Research Lab, AUT
-------------------------------------------------------------------------------
-- Description:
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

package eth_ip_udp_header_prepend_pkg is

    type t_eth_header is record
        dst_mac_addr   : std_logic_vector(47 downto 0);
        src_mac_addr   : std_logic_vector(47 downto 0);
        ethertype      : std_logic_vector(15 downto 0);
    end record;

    type t_ip_header is record
        version         : std_logic_vector(3 downto 0);
        header_len      : std_logic_vector(3 downto 0);
        type_of_service : std_logic_vector(7 downto 0);
        total_length    : std_logic_vector(15 downto 0);
        identification  : std_logic_vector(15 downto 0);
        flags           : std_logic_vector(2 downto 0);
        frag_offset     : std_logic_vector(12 downto 0);
        ttl             : std_logic_vector(7 downto 0);
        protocol        : std_logic_vector(7 downto 0);
        header_checksum : std_logic_vector(15 downto 0);
        src_addr        : std_logic_vector(31 downto 0);
        dst_addr        : std_logic_vector(31 downto 0);
    end record;

    type t_udp_header is record
        src_port       : std_logic_vector(15 downto 0);
        dst_port       : std_logic_vector(15 downto 0);
        length         : std_logic_vector(15 downto 0);
        checksum       : std_logic_vector(15 downto 0);
    end record;

    type t_eth_ip_udp_header is record
        eth : t_eth_header;
        ip  : t_ip_header;
        udp : t_udp_header;
    end record;

    -- vsg_off constant_015 variable_012
    -- autogen start decl
    --------------------------------------------------
    -- Auto-generated code below. You should not edit.
    -- Generated Fri Oct  4 13:18:51 2019
    --------------------------------------------------

    type t_eth_header_a is array(natural range <>) of t_eth_header;

    constant T_ETH_HEADER_ZERO : t_eth_header := (
        dst_mac_addr => (others => '0'),
        src_mac_addr => (others => '0'),
        ethertype    => (others => '0')
    );

    constant T_ETH_HEADER_DONT_CARE : t_eth_header := (
        dst_mac_addr => (others => '-'),
        src_mac_addr => (others => '-'),
        ethertype    => (others => '-')
    );

    constant T_ETH_HEADER_SLV_WIDTH : natural := 112;

    subtype t_eth_header_slv is std_logic_vector(111 downto 0);
    function to_slv (rec : t_eth_header) return t_eth_header_slv;
    function from_slv (slv : t_eth_header_slv) return t_eth_header;

    type t_ip_header_a is array(natural range <>) of t_ip_header;

    constant T_IP_HEADER_ZERO : t_ip_header := (
        version         => (others => '0'),
        header_len      => (others => '0'),
        type_of_service => (others => '0'),
        total_length    => (others => '0'),
        identification  => (others => '0'),
        flags           => (others => '0'),
        frag_offset     => (others => '0'),
        ttl             => (others => '0'),
        protocol        => (others => '0'),
        header_checksum => (others => '0'),
        src_addr        => (others => '0'),
        dst_addr        => (others => '0')
    );

    constant T_IP_HEADER_DONT_CARE : t_ip_header := (
        version         => (others => '-'),
        header_len      => (others => '-'),
        type_of_service => (others => '-'),
        total_length    => (others => '-'),
        identification  => (others => '-'),
        flags           => (others => '-'),
        frag_offset     => (others => '-'),
        ttl             => (others => '-'),
        protocol        => (others => '-'),
        header_checksum => (others => '-'),
        src_addr        => (others => '-'),
        dst_addr        => (others => '-')
    );

    constant T_IP_HEADER_SLV_WIDTH : natural := 160;

    subtype t_ip_header_slv is std_logic_vector(159 downto 0);
    function to_slv (rec : t_ip_header) return t_ip_header_slv;
    function from_slv (slv : t_ip_header_slv) return t_ip_header;

    type t_udp_header_a is array(natural range <>) of t_udp_header;

    constant T_UDP_HEADER_ZERO : t_udp_header := (
        src_port => (others => '0'),
        dst_port => (others => '0'),
        length   => (others => '0'),
        checksum => (others => '0')
    );

    constant T_UDP_HEADER_DONT_CARE : t_udp_header := (
        src_port => (others => '-'),
        dst_port => (others => '-'),
        length   => (others => '-'),
        checksum => (others => '-')
    );

    constant T_UDP_HEADER_SLV_WIDTH : natural := 64;

    subtype t_udp_header_slv is std_logic_vector(63 downto 0);
    function to_slv (rec : t_udp_header) return t_udp_header_slv;
    function from_slv (slv : t_udp_header_slv) return t_udp_header;

    type t_eth_ip_udp_header_a is array(natural range <>) of t_eth_ip_udp_header;

    constant T_ETH_IP_UDP_HEADER_ZERO : t_eth_ip_udp_header := (
        eth => T_ETH_HEADER_ZERO,
        ip  => T_IP_HEADER_ZERO,
        udp => T_UDP_HEADER_ZERO
    );

    constant T_ETH_IP_UDP_HEADER_DONT_CARE : t_eth_ip_udp_header := (
        eth => T_ETH_HEADER_ZERO,
        ip  => T_IP_HEADER_ZERO,
        udp => T_UDP_HEADER_ZERO
    );

    constant T_ETH_IP_UDP_HEADER_SLV_WIDTH : natural := 336;

    subtype t_eth_ip_udp_header_slv is std_logic_vector(335 downto 0);
    function to_slv (rec : t_eth_ip_udp_header) return t_eth_ip_udp_header_slv;
    function from_slv (slv : t_eth_ip_udp_header_slv) return t_eth_ip_udp_header;

    ------------------------------------------------------
    -- End of Autogenerated code. You may add yours below.
    ------------------------------------------------------
    -- autogen end decl

end package eth_ip_udp_header_prepend_pkg;

package body eth_ip_udp_header_prepend_pkg is

    -- autogen start body
    --------------------------------------------------
    -- Auto-generated code below. You should not edit.
    -- Generated Fri Oct  4 13:18:51 2019
    --------------------------------------------------

    function to_slv (rec : t_eth_header) return t_eth_header_slv is
        variable slv : std_logic_vector(111 downto 0);
    begin
        slv(15 downto 0)   := rec.ethertype;
        slv(63 downto 16)  := rec.src_mac_addr;
        slv(111 downto 64) := rec.dst_mac_addr;
        return slv;
    end function to_slv;

    function from_slv (slv : t_eth_header_slv) return t_eth_header is
        variable rec : t_eth_header;
    begin
        rec.ethertype    := slv(15 downto 0);
        rec.src_mac_addr := slv(63 downto 16);
        rec.dst_mac_addr := slv(111 downto 64);
        return rec;
    end function from_slv;

    function to_slv (rec : t_ip_header) return t_ip_header_slv is
        variable slv : std_logic_vector(159 downto 0);
    begin
        slv(31 downto 0)    := rec.dst_addr;
        slv(63 downto 32)   := rec.src_addr;
        slv(79 downto 64)   := rec.header_checksum;
        slv(87 downto 80)   := rec.protocol;
        slv(95 downto 88)   := rec.ttl;
        slv(108 downto 96)  := rec.frag_offset;
        slv(111 downto 109) := rec.flags;
        slv(127 downto 112) := rec.identification;
        slv(143 downto 128) := rec.total_length;
        slv(151 downto 144) := rec.type_of_service;
        slv(155 downto 152) := rec.header_len;
        slv(159 downto 156) := rec.version;
        return slv;
    end function to_slv;

    function from_slv (slv : t_ip_header_slv) return t_ip_header is
        variable rec : t_ip_header;
    begin
        rec.dst_addr        := slv(31 downto 0);
        rec.src_addr        := slv(63 downto 32);
        rec.header_checksum := slv(79 downto 64);
        rec.protocol        := slv(87 downto 80);
        rec.ttl             := slv(95 downto 88);
        rec.frag_offset     := slv(108 downto 96);
        rec.flags           := slv(111 downto 109);
        rec.identification  := slv(127 downto 112);
        rec.total_length    := slv(143 downto 128);
        rec.type_of_service := slv(151 downto 144);
        rec.header_len      := slv(155 downto 152);
        rec.version         := slv(159 downto 156);
        return rec;
    end function from_slv;

    function to_slv (rec : t_udp_header) return t_udp_header_slv is
        variable slv : std_logic_vector(63 downto 0);
    begin
        slv(15 downto 0)  := rec.checksum;
        slv(31 downto 16) := rec.length;
        slv(47 downto 32) := rec.dst_port;
        slv(63 downto 48) := rec.src_port;
        return slv;
    end function to_slv;

    function from_slv (slv : t_udp_header_slv) return t_udp_header is
        variable rec : t_udp_header;
    begin
        rec.checksum := slv(15 downto 0);
        rec.length   := slv(31 downto 16);
        rec.dst_port := slv(47 downto 32);
        rec.src_port := slv(63 downto 48);
        return rec;
    end function from_slv;

    function to_slv (rec : t_eth_ip_udp_header) return t_eth_ip_udp_header_slv is
        variable slv : std_logic_vector(335 downto 0);
    begin
        slv(63 downto 0)    := to_slv(rec.udp);
        slv(223 downto 64)  := to_slv(rec.ip);
        slv(335 downto 224) := to_slv(rec.eth);
        return slv;
    end function to_slv;

    function from_slv (slv : t_eth_ip_udp_header_slv) return t_eth_ip_udp_header is
        variable rec : t_eth_ip_udp_header;
    begin
        rec.udp := from_slv(slv(63 downto 0));
        rec.ip  := from_slv(slv(223 downto 64));
        rec.eth := from_slv(slv(335 downto 224));
        return rec;
    end function from_slv;

    ------------------------------------------------------
    -- End of Autogenerated code. You may add yours below.
    ------------------------------------------------------
    -- autogen end body

end package body eth_ip_udp_header_prepend_pkg;
