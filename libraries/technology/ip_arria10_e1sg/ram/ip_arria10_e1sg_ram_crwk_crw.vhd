-- (C) 2001-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions and other
-- software and tools, and its AMPP partner logic functions, and any output
-- files any of the foregoing (including device programming or simulation
-- files), and any associated documentation or information are expressly subject
-- to the terms and conditions of the Altera Program License Subscription
-- Agreement, Altera MegaCore Function License Agreement, or other applicable
-- license agreement, including, without limitation, that your use is for the
-- sole purpose of programming logic devices manufactured by Altera and sold by
-- Altera or its authorized distributors.  Please refer to the applicable
-- agreement for further details.

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use technology_lib.technology_pkg.all;

library altera_lnsim;
use altera_lnsim.altera_lnsim_components.all;

entity ip_arria10_e1sg_ram_crwk_crw is
  generic (
    g_adr_a_w     : natural := 5;
    g_dat_a_w     : natural := 32;
    g_adr_b_w     : natural := 4;
    g_dat_b_w     : natural := 64;
    g_nof_words_a : natural := 2**5;
    g_nof_words_b : natural := 2**4;
    g_rd_latency  : natural := 1;  -- choose 1 or 2
    g_init_file   : string  := "UNUSED"
  );
  port
  (
    address_a : in std_logic_vector(g_adr_a_w - 1 downto 0);
    address_b : in std_logic_vector(g_adr_b_w - 1 downto 0);
    clk_a     : in std_logic  := '1';
    clk_b     : in std_logic;
    data_a    : in std_logic_vector(g_dat_a_w - 1 downto 0);
    data_b    : in std_logic_vector(g_dat_b_w - 1 downto 0);
    wren_a    : in std_logic  := '0';
    wren_b    : in std_logic  := '0';
    q_a       : out std_logic_vector(g_dat_a_w - 1 downto 0);
    q_b       : out std_logic_vector(g_dat_b_w - 1 downto 0)
  );
end ip_arria10_e1sg_ram_crwk_crw;

architecture SYN of ip_arria10_e1sg_ram_crwk_crw is
  constant c_outdata_reg_a : string := tech_sel_a_b(g_rd_latency = 1, "UNREGISTERED", "CLOCK0");
  constant c_outdata_reg_b : string := tech_sel_a_b(g_rd_latency = 1, "UNREGISTERED", "CLOCK1");

  component altera_syncram
  generic (
          address_reg_b  : string;
          clock_enable_input_a  : string;
          clock_enable_input_b  : string;
          clock_enable_output_a  : string;
          clock_enable_output_b  : string;
          indata_reg_b  : string;
          init_file  : string;
          init_file_layout  : string;
          intended_device_family  : string;
          lpm_type  : string;
          numwords_a  : integer;
          numwords_b  : integer;
          operation_mode  : string;
          outdata_aclr_a  : string;
          outdata_aclr_b  : string;
          outdata_reg_a  : string;
          outdata_reg_b  : string;
          power_up_uninitialized  : string;
          read_during_write_mode_port_a  : string;
          read_during_write_mode_port_b  : string;
          widthad_a  : integer;
          widthad_b  : integer;
          width_a  : integer;
          width_b  : integer;
          width_byteena_a  : integer;
          width_byteena_b  : integer
  );
  port (
      address_a : in std_logic_vector(g_adr_a_w - 1 downto 0);
      address_b : in std_logic_vector(g_adr_b_w - 1 downto 0);
      clock0 : in std_logic;
      clock1 : in std_logic;
      data_a : in std_logic_vector(g_dat_a_w - 1 downto 0);
      data_b : in std_logic_vector(g_dat_b_w - 1 downto 0);
      wren_a : in std_logic;
      wren_b : in std_logic;
      q_a : out std_logic_vector(g_dat_a_w - 1 downto 0);
      q_b : out std_logic_vector(g_dat_b_w - 1 downto 0)
  );
  end component;
begin
  -- Copied from ip_arria10_e1sg_ram_crwk_crw/ram_2port_140/sim/ip_arria10_e1sg_ram_crwk_crw_ram_2port_140_iyfl3wi.vhd
  u_altera_syncram : altera_syncram
  generic map (
          address_reg_b  => "CLOCK1",
          clock_enable_input_a  => "BYPASS",
          clock_enable_input_b  => "BYPASS",
          clock_enable_output_a  => "BYPASS",
          clock_enable_output_b  => "BYPASS",
          indata_reg_b  => "CLOCK1",
          init_file  => g_init_file,
          init_file_layout  => "PORT_B",
          intended_device_family  => "Arria 10",
          lpm_type  => "altera_syncram",
          numwords_a  => g_nof_words_a,
          numwords_b  => g_nof_words_b,
          operation_mode  => "BIDIR_DUAL_PORT",
          outdata_aclr_a  => "NONE",
          outdata_aclr_b  => "NONE",
          outdata_reg_a  => c_outdata_reg_a,
          outdata_reg_b  => c_outdata_reg_b,
          power_up_uninitialized  => "FALSE",
          read_during_write_mode_port_a  => "NEW_DATA_NO_NBE_READ",
          read_during_write_mode_port_b  => "NEW_DATA_NO_NBE_READ",
          widthad_a  => g_adr_a_w,
          widthad_b  => g_adr_b_w,
          width_a  => g_dat_a_w,
          width_b  => g_dat_b_w,
          width_byteena_a  => 1,
          width_byteena_b  => 1
  )
  port map (
      address_a => address_a,
      address_b => address_b,
      clock0 => clk_a,
      clock1 => clk_b,
      data_a => data_a,
      data_b => data_b,
      wren_a => wren_a,
      wren_b => wren_b,
      q_a => q_a,
      q_b => q_b
  );
end SYN;
