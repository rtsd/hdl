-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose: Test bench for the pipelined radix-2 FFT.
--
--          The testbech uses a blockgenerator to generate data for
--          the input of the parallel FFT.
--          The output of the FFT is stored in a databuffer.
--          Both the block generator and databuffer are controlled
--          via a mm interface.
--          Use this testbench in conjunction with ../python/tc_mmf_fft_r2_pipe.py
--
-- Usage:
--   > run -all
--   > Run python script in separate terminal: "python tc_mmf_fft_r2_pipe.py --unb 0 --bn 0 --sim"
--   > Check the results of the python script.
--   > Stop the simulation manually in Modelsim by pressing the stop-button.

library IEEE, common_lib, unb_common_lib, mm_lib, diag_lib, dp_lib, rTwoSDF_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use mm_lib.mm_file_pkg.all;
use dp_lib.dp_stream_pkg.all;
use rTwoSDF_lib.rTwoSDFPkg.all;
use work.fft_pkg.all;

entity tb_mmf_fft_r2_pipe is
  generic(
    g_fft : t_fft := (true, false, false, 1, 1, 0, 64, 8, 14, 0, c_dsp_mult_w, 2, true, 56, 2)
    --  type t_rtwo_fft is record
    --    use_reorder    : boolean;  -- = false for bit-reversed output, true for normal output
    --    use_fft_shift  : boolean;  -- = false for [0, pos, neg] bin frequencies order, true for [neg, 0, pos] bin frequencies order in case of complex input
    --    use_separate   : boolean;  -- = false for complex input, true for two real inputs
    --    nof_chan       : natural;  -- = default 0, defines the number of channels (=time-multiplexed input signals): nof channels = 2**nof_chan
    --    wb_factor      : natural;  -- = default 1, wideband factor
    --    twiddle_offset : natural;  -- = default 0, twiddle offset for PFT sections in a wideband FFT
    --    nof_points     : natural;  -- = 1024, N point FFT
    --    in_dat_w       : natural;  -- = 8, number of input bits
    --    out_dat_w      : natural;  -- = 13, number of output bits: in_dat_w + natural((ceil_log2(nof_points))/2 + 2)
    --    out_gain_w     : natural;  -- = 0, output gain factor applied after the last stage output, before requantization to out_dat_w
    --    stage_dat_w    : natural;  -- = 18, data width used between the stages(= DSP multiplier-width)
    --    guard_w        : natural;  -- = 2,  Guard used to avoid overflow in FFT stage.
    --    guard_enable   : boolean;  -- = true when input needs guarding, false when input requires no guarding but scaling must be skipped at the last stage(s) (used in wb fft)
    --    stat_data_w    : positive; -- = 56
    --    stat_data_sz   : positive; -- = 2
    --  end record;
  );
end tb_mmf_fft_r2_pipe;

architecture tb of tb_mmf_fft_r2_pipe is
  constant c_sim                : boolean := true;

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period      : time := 1 ns;
  constant c_dp_clk_period      : time := 5 ns;
  constant c_dp_pps_period      : natural := 64;

  signal dp_pps                 : std_logic;

  signal mm_rst                 : std_logic;
  signal mm_clk                 : std_logic := '0';

  signal dp_rst                 : std_logic;
  signal dp_clk                 : std_logic := '0';

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  signal reg_diag_bg_mosi          : t_mem_mosi;
  signal reg_diag_bg_miso          : t_mem_miso;

  signal ram_diag_bg_mosi          : t_mem_mosi;
  signal ram_diag_bg_miso          : t_mem_miso;

  signal ram_ss_ss_wide_mosi       : t_mem_mosi;
  signal ram_ss_ss_wide_miso       : t_mem_miso;

  signal ram_diag_data_buf_re_mosi : t_mem_mosi;
  signal ram_diag_data_buf_re_miso : t_mem_miso;

  signal reg_diag_data_buf_re_mosi : t_mem_mosi;
  signal reg_diag_data_buf_re_miso : t_mem_miso;

  signal ram_diag_data_buf_im_mosi : t_mem_mosi;
  signal ram_diag_data_buf_im_miso : t_mem_miso;

  signal reg_diag_data_buf_im_mosi : t_mem_mosi;
  signal reg_diag_data_buf_im_miso : t_mem_miso;

  constant c_nof_channels           : natural  := 2**g_fft.nof_chan;
  constant c_nof_streams            : positive := g_fft.wb_factor;
  constant c_nof_integration        : natural  := 8;
  constant c_bg_block_len           : natural  := g_fft.nof_points * c_nof_integration * c_nof_channels;

  constant c_bg_buf_adr_w           : natural := ceil_log2(c_bg_block_len);
  constant c_bg_data_file_index_arr : t_nat_natural_arr := array_init(0, 16, 1);
  constant c_bg_data_file_prefix    : string := "UNUSED";

  signal bg_siso_arr                : t_dp_siso_arr(0 downto 0) := (others => c_dp_siso_rdy);
  signal bg_sosi_arr                : t_dp_sosi_arr(0 downto 0);

  signal ss_out_sosi_re_arr         : t_dp_sosi_arr(0 downto 0) := (others => c_dp_sosi_rst);
  signal ss_out_sosi_im_arr         : t_dp_sosi_arr(0 downto 0) := (others => c_dp_sosi_rst);

  signal in_re                      : std_logic_vector(g_fft.in_dat_w - 1 downto 0);
  signal in_im                      : std_logic_vector(g_fft.in_dat_w - 1 downto 0);
  signal in_val                     : std_logic := '0';

  signal out_re                     : std_logic_vector(g_fft.out_dat_w - 1 downto 0);
  signal out_im                     : std_logic_vector(g_fft.out_dat_w - 1 downto 0);
  signal out_val                    : std_logic := '0';
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_dp_pps_period, '1', dp_clk, dp_pps);

   ----------------------------------------------------------------------------
  -- Procedure that polls a sim control file that can be used to e.g. get
  -- the simulation time in ns
  ----------------------------------------------------------------------------
  mmf_poll_sim_ctrl_file(c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");

  ----------------------------------------------------------------------------
  -- MM buses
  ----------------------------------------------------------------------------
  u_mm_file_reg_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_BG")
                                           port map(mm_rst, mm_clk, reg_diag_bg_mosi, reg_diag_bg_miso);

  u_mm_file_ram_diag_bg          : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_BG")
                                           port map(mm_rst, mm_clk, ram_diag_bg_mosi, ram_diag_bg_miso);

  u_mm_file_ram_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_re_mosi, ram_diag_data_buf_re_miso);

  u_mm_file_reg_diag_data_buf_re : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_DATA_BUFFER_REAL")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_re_mosi, reg_diag_data_buf_re_miso);

  u_mm_file_ram_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "RAM_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, ram_diag_data_buf_im_mosi, ram_diag_data_buf_im_miso);

  u_mm_file_reg_diag_data_buf_im : mm_file generic map(mmf_unb_file_prefix(0, 0, "BN") & "REG_DIAG_DATA_BUFFER_IMAG")
                                           port map(mm_rst, mm_clk, reg_diag_data_buf_im_mosi, reg_diag_data_buf_im_miso);

  ----------------------------------------------------------------------------
  -- Source: block generator
  ----------------------------------------------------------------------------
  u_bg : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_output_streams => c_nof_streams,
    g_buf_dat_w          => c_nof_complex * g_fft.in_dat_w,
    g_buf_addr_w         => c_bg_buf_adr_w,  -- Waveform buffer size 2**g_buf_addr_w nof samples
    g_file_index_arr     => c_bg_data_file_index_arr,
    g_file_name_prefix   => c_bg_data_file_prefix
  )
  port map(
    -- System
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    dp_rst           => dp_rst,
    dp_clk           => dp_clk,
    en_sync          => dp_pps,
    -- MM interface
    reg_bg_ctrl_mosi => reg_diag_bg_mosi,
    reg_bg_ctrl_miso => reg_diag_bg_miso,
    ram_bg_data_mosi => ram_diag_bg_mosi,
    ram_bg_data_miso => ram_diag_bg_miso,
    -- ST interface
    out_siso_arr     => bg_siso_arr,
    out_sosi_arr     => bg_sosi_arr
  );

  in_re  <= bg_sosi_arr(0).re(g_fft.in_dat_w - 1 downto 0);
  in_im  <= bg_sosi_arr(0).im(g_fft.in_dat_w - 1 downto 0);
  in_val <= bg_sosi_arr(0).valid;

  -- DUT = Device Under Test
  u_dut : entity work.fft_r2_pipe
  generic map(
    g_fft      => g_fft  -- generics for the FFT
  )
  port map(
    clk           => dp_clk,
    rst           => dp_rst,
    in_re         => in_re,
    in_im         => in_im,
    in_val        => in_val,
    out_quant_re  => out_re,
    out_quant_im  => out_im,
    out_val       => out_val
  );

  ss_out_sosi_re_arr(0).data  <= RESIZE_SVEC(out_re, ss_out_sosi_re_arr(0).data'length);
  ss_out_sosi_re_arr(0).valid <= out_val;
  ss_out_sosi_re_arr(0).sync  <= out_val;

  ss_out_sosi_im_arr(0).data  <= RESIZE_SVEC(out_im, ss_out_sosi_im_arr(0).data'length);
  ss_out_sosi_im_arr(0).valid <= out_val;
  ss_out_sosi_im_arr(0).sync  <= out_val;

  ----------------------------------------------------------------------------
  -- Sink: data buffer real
  ----------------------------------------------------------------------------
  u_data_buf_re : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => c_nof_streams,
    g_data_w       => g_fft.out_dat_w,
    g_buf_nof_data => c_bg_block_len,
    g_buf_use_sync => false
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_re_mosi,
    ram_data_buf_miso => ram_diag_data_buf_re_miso,

    reg_data_buf_mosi => reg_diag_data_buf_re_mosi,
    reg_data_buf_miso => reg_diag_data_buf_re_miso,

    -- ST interface
    in_sync           => ss_out_sosi_re_arr(0).sync,
    in_sosi_arr       => ss_out_sosi_re_arr
  );

  ----------------------------------------------------------------------------
  -- Sink: data buffer imag
  ----------------------------------------------------------------------------
  u_data_buf_im : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => c_nof_streams,
    g_data_w       => g_fft.out_dat_w,
    g_buf_nof_data => c_bg_block_len,
    g_buf_use_sync => false
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => dp_rst,
    dp_clk            => dp_clk,

    -- MM interface
    ram_data_buf_mosi => ram_diag_data_buf_im_mosi,
    ram_data_buf_miso => ram_diag_data_buf_im_miso,

    reg_data_buf_mosi => reg_diag_data_buf_im_mosi,
    reg_data_buf_miso => reg_diag_data_buf_im_miso,

    -- ST interface
    in_sync           => ss_out_sosi_im_arr(0).sync,
    in_sosi_arr       => ss_out_sosi_im_arr
  );
end tb;
