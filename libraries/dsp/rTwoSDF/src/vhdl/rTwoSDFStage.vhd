--------------------------------------------------------------------------------
--   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
--   Copyright (C) 2009-2010
--   ASTRON (Netherlands Institute for Radio Astronomy)
--   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
--   This file is part of the UniBoard software suite.
--   The file is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

library ieee, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.twiddlesPkg.all;
use work.rTwoSDFPkg.all;

entity rTwoSDFStage is
  generic (
    g_nof_chan       : natural := 0;  -- Exponent of nr of subbands (0 means 1 subband)
    g_stage          : natural := 8;
    g_stage_offset   : natural := 0;  -- The Stage offset: 0 for normal FFT. Other than 0 in wideband FFT
    g_twiddle_offset : natural := 0;  -- The twiddle offset: 0 for normal FFT. Other than 0 in wideband FFT
    g_scale_enable   : boolean := true;
    g_round_even     : boolean := true;
    g_pipeline       : t_fft_pipeline := c_fft_pipeline  -- internal pipeline settings
  );
  port (
    clk     : in  std_logic;
    rst     : in  std_logic;
    in_re   : in  std_logic_vector;
    in_im   : in  std_logic_vector;
    in_val  : in  std_logic;
    out_re  : out std_logic_vector;
    out_im  : out std_logic_vector;
    out_val : out std_logic
  );
end entity rTwoSDFStage;

architecture str of rTwoSDFStage is
  -- The amplification factor per stage is 2, therefor bit growth defintion of 1.
  -- Scale enable is defined by generic.
  constant c_r2_stage_bit_growth : natural := sel_a_b(g_scale_enable, 1, 0);

  -- counter for ctrl_sel
  constant c_cnt_lat    : integer := 1;
  constant c_cnt_init   : integer := 0;

  signal ctrl_sel       : std_logic_vector(g_stage + g_nof_chan downto 1);

  signal in_sel         : std_logic;

  signal bf_re          : std_logic_vector(in_re'range);
  signal bf_im          : std_logic_vector(in_im'range);
  signal bf_sel         : std_logic;
  signal bf_val         : std_logic;

  signal weight_addr    : std_logic_vector(g_stage-1 downto 1);
  signal weight_re      : wTyp;
  signal weight_im      : wTyp;

  signal mul_out_re     : std_logic_vector(out_re'length - 1 downto 0);
  signal mul_out_im     : std_logic_vector(out_im'length - 1 downto 0);
  signal mul_out_val    : std_logic;

  signal quant_out_re   : std_logic_vector(out_re'range);
  signal quant_out_im   : std_logic_vector(out_im'range);
begin
  ------------------------------------------------------------------------------
  -- stage counter
  ------------------------------------------------------------------------------
  u_control : entity common_lib.common_counter
  generic map (
    g_latency   => c_cnt_lat,
    g_init      => c_cnt_init,
    g_width     => g_stage + g_nof_chan,
    g_step_size => 1
  )
  port map (
    rst         => rst,
    clk         => clk,
    cnt_en      => in_val,
    count       => ctrl_sel
  );

  ------------------------------------------------------------------------------
  -- complex butterfly
  ------------------------------------------------------------------------------
  in_sel <= ctrl_sel(g_stage + g_nof_chan);

  u_butterfly: entity work.rTwoBFStage
  generic map (
    g_nof_chan      => g_nof_chan,
    g_stage         => g_stage,
    g_bf_lat        => g_pipeline.bf_lat,
    g_bf_use_zdly   => g_pipeline.bf_use_zdly,
    g_bf_in_a_zdly  => g_pipeline.bf_in_a_zdly,
    g_bf_out_d_zdly => g_pipeline.bf_out_d_zdly
  )
  port map (
    clk       => clk,
    rst       => rst,
    in_re     => in_re,
    in_im     => in_im,
    in_sel    => in_sel,
    in_val    => in_val,
    out_re    => bf_re,
    out_im    => bf_im,
    out_sel   => bf_sel,
    out_val   => bf_val
  );

  ------------------------------------------------------------------------------
  -- get twiddles
  ------------------------------------------------------------------------------
  weight_addr <= ctrl_sel(g_stage + g_nof_chan - 1 downto g_nof_chan + 1);

  u_weights: entity work.rTwoWeights
  generic map (
    g_stage          => g_stage,
    g_twiddle_offset => g_twiddle_offset,
    g_stage_offset   => g_stage_offset,
    g_lat            => g_pipeline.weight_lat
  )
  port map (
    clk       => clk,
    in_wAdr   => weight_addr,
    weight_re => weight_re,
    weight_im => weight_im
  );

  ------------------------------------------------------------------------------
  -- twiddle multiplication
  ------------------------------------------------------------------------------
  u_TwiddleMult: entity work.rTwoWMul
  generic map (
    g_stage      => g_stage,
    g_round_even => g_round_even,
    g_lat        => g_pipeline.mul_lat
  )
  port map (
    clk         => clk,
    rst         => rst,
    weight_re   => weight_re,
    weight_im   => weight_im,
    in_re       => bf_re,
    in_im       => bf_im,
    in_val      => bf_val,
    in_sel      => bf_sel,
    out_re      => mul_out_re,
    out_im      => mul_out_im,
    out_val     => mul_out_val
  );

  ------------------------------------------------------------------------------
  -- stage requantization
  ------------------------------------------------------------------------------
  u_requantize_re : entity common_lib.common_requantize
  generic map (
    g_representation      => "SIGNED",
    g_lsb_w               => c_r2_stage_bit_growth,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_lsb_round_even      => g_round_even,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => false,
    g_pipeline_remove_lsb => 0,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => mul_out_re'LENGTH,
    g_out_dat_w           => out_re'length
  )
  port map (
    clk        => clk,
    clken      => '1',
    in_dat     => mul_out_re,
    out_dat    => quant_out_re,
    out_ovr    => open
  );

  u_requantize_im : entity common_lib.common_requantize
  generic map (
    g_representation      => "SIGNED",
    g_lsb_w               => c_r2_stage_bit_growth,
    g_lsb_round           => true,
    g_lsb_round_clip      => false,
    g_lsb_round_even      => g_round_even,
    g_msb_clip            => false,
    g_msb_clip_symmetric  => false,
    g_pipeline_remove_lsb => 0,
    g_pipeline_remove_msb => 0,
    g_in_dat_w            => mul_out_im'LENGTH,
    g_out_dat_w           => out_im'length
  )
  port map (
    clk        => clk,
    clken      => '1',
    in_dat     => mul_out_im,
    out_dat    => quant_out_im,
    out_ovr    => open
  );

  ------------------------------------------------------------------------------
  -- output
  ------------------------------------------------------------------------------
  u_re_lat : entity common_lib.common_pipeline
  generic map (
    g_pipeline  => g_pipeline.stage_lat,
    g_in_dat_w  => out_re'length,
    g_out_dat_w => out_re'length
  )
  port map (
    clk     => clk,
    in_dat  => quant_out_re,
    out_dat => out_re
  );

  u_im_lat : entity common_lib.common_pipeline
  generic map (
    g_pipeline  => g_pipeline.stage_lat,
    g_in_dat_w  => out_im'length,
    g_out_dat_w => out_im'length
  )
  port map (
    clk     => clk,
    in_dat  => quant_out_im,
    out_dat => out_im
  );

  u_val_lat : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline => g_pipeline.stage_lat
  )
  port map (
    clk     => clk,
    in_dat  => mul_out_val,
    out_dat => out_val
  );
end str;
