--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- . Calculate the cross- and auto correlations of c_nof_inputs inputs.
-- Description:
-- .

entity correlator is
  generic (
    g_nof_input_streams       : natural;
    g_input_unfold_factor     : natural := 0;  -- 2**g_input_unfold_factor = number of inputs carried on one input stream
    g_nof_pre_mult_folds      : natural := 0;  -- Number of pre-multiplier stage folds.
    g_data_w                  : natural := 16;  -- Complex data width
    g_nof_channels            : natural := 64;  -- Number of (serial) channels per (parallel) input
    g_integration_period      : natural := 0;  -- Expressed as the number of samples per channel to accumulate
    g_inter_channel_delay     : natural := 0;  -- 0: Channels are output back to back. Set to >0 (cycles) for more constant output rate
    g_visibility_buffer_depth : natural := 0;  -- 0: internally set to c_nof_accumulators. Use more depth e.g. in sim with shorter integration period.
    g_note_is_ful             : boolean := false  -- when TRUE report NOTE when FIFO goes full, fifo overflow is always reported as FAILURE
                                                  -- when FALSE no note reports, to speed up simulation and avoid many notes    g_nof_inputs       : NATURAL; -- Number of input streams
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_input_streams - 1 downto 0);

    src_out_arr    : out t_dp_sosi_arr(1 - 1 downto 0)  -- Single output for now
  );
end correlator;

architecture str of correlator is
  constant c_nof_inputs                      : natural := g_nof_input_streams * pow2(g_input_unfold_factor);
  constant c_nof_visibilities                : natural := (c_nof_inputs * (c_nof_inputs + 1)) / 2;

  -----------------------------------------------------------------------------
  -- The array of multiplier input streams can be folded, e.g.:
  -- . c_nof_visibilities=10, g_nof_pre_mult_folds=0 -> c_nof_mults=10
  -- . c_nof_visibilities=10, g_nof_pre_mult_folds=1 -> c_nof_mults= 5
  -- . c_nof_visibilities=10, g_nof_pre_mult_folds=2 -> c_nof_mults= 3
  -- . c_nof_visibilities=10, g_nof_pre_mult_folds=3 -> c_nof_mults= 2
  -- . c_nof_visibilities=10, g_nof_pre_mult_folds=4 -> c_nof_mults= 1
  -- . c_nof_visibilities=10, g_nof_pre_mult_folds<0 -> c_nof_mults= 1
  -----------------------------------------------------------------------------
  constant c_nof_mults                       : natural := sel_a_b(g_nof_pre_mult_folds >= 0, ceil_div(c_nof_visibilities, ceil_pow2(g_nof_pre_mult_folds)), 1);

  -- Nof accumulator sums to keep per stream depends on folding
  constant c_nof_accumulators                : natural := g_nof_channels * pow2(g_nof_pre_mult_folds);

  -- Functionally, the visibility buffer has a minimum depth of c_nof_accumulators. Add more depth if e.g. simulation uses shorter integration period.
  constant c_visibility_buffer_depth         : natural := sel_a_b(g_visibility_buffer_depth = 0, c_nof_accumulators, g_visibility_buffer_depth);

  constant c_mult_output_w                   : natural := 2 * g_data_w;
  constant c_acc_data_w                      : natural := c_mult_output_w + ceil_log2(g_integration_period);

  signal corr_unfolder_src_out_arr           : t_dp_sosi_arr(c_nof_inputs - 1 downto 0);
  signal corr_permutator_src_out_2arr_2      : t_dp_sosi_2arr_2(c_nof_visibilities - 1 downto 0);  -- Array of pairs
  signal corr_folder_src_out_2arr_2          : t_dp_sosi_2arr_2(c_nof_mults - 1 downto 0);  -- Array of pairs, folded
  signal corr_multiplier_src_out_arr         : t_dp_sosi_arr(c_nof_mults - 1 downto 0);
  signal corr_accumulator_src_out_arr        : t_dp_sosi_arr(c_nof_mults - 1 downto 0);
  signal corr_visibility_buffer_src_out_arr  : t_dp_sosi_arr(c_nof_mults - 1 downto 0);
--  SIGNAL corr_output_framer_snk_in_arr       : t_dp_sosi_arr(1-1 DOWNTO 0);
begin
  ------------------------------------------------------------------------------
  -- Check passed parameters
  ------------------------------------------------------------------------------
-- synthesis translate_off
  p_check_parameters : process
  begin
    print_str( "################################################################################");
    print_str( "# correlator.vhd");
    print_str( "# . Number of input streams (g_nof_input_streams)    : " & int_to_str(g_nof_input_streams));
    print_str( "# . Input folding factor (g_input_unfold_factor)     : " & int_to_str(g_input_unfold_factor));
    print_str( "# . Number of inputs (c_nof_inputs)                  : " & int_to_str(c_nof_inputs));
    print_str( "# . Number of visibilities (c_nof_visibilities)      : " & int_to_str(c_nof_visibilities));
    print_str( "# . Visibility folding factor (g_nof_pre_mult_folds) : " & int_to_str(g_nof_pre_mult_folds));
    print_str( "# . Number of parallel DSP streams (c_nof_mults)     : " & int_to_str(c_nof_mults));
    print_str( "# . Integration period (g_integration_period)        : " & int_to_str(g_integration_period));
    print_str( "################################################################################");

    wait;
  end process;

  assert g_integration_period >= c_nof_mults
    report "g_integration_period " & int_to_str(g_integration_period) & " not large enough; cannot fold " & int_to_str(c_nof_mults) & " DSP streams into one stream without losing data"
    severity FAILURE;
-- synthesis translate_on

  ------------------------------------------------------------------------------
  -- Unfold input streams
  ------------------------------------------------------------------------------
  u_corr_unfolder : entity work.corr_unfolder
  generic map (
    g_nof_inputs  => g_nof_input_streams,
    g_nof_unfolds => g_input_unfold_factor
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_in_arr  => snk_in_arr,

    src_out_arr => corr_unfolder_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Create all unique pair permutations of the input streams
  -----------------------------------------------------------------------------
  u_corr_permutator : entity work.corr_permutator
  generic map (
    g_nof_inputs => c_nof_inputs
  )
  port map (
    clk            => clk,
    rst            => rst,

    snk_in_arr     => corr_unfolder_src_out_arr,
    src_out_2arr_2 => corr_permutator_src_out_2arr_2
  );

  -----------------------------------------------------------------------------
  -- Fold the streams before feeding them to the multiplier stage
  -----------------------------------------------------------------------------
  u_corr_folder_2arr_2 : entity work.corr_folder_2arr_2
  generic map (
    g_nof_inputs => c_nof_visibilities,
    g_nof_folds  => g_nof_pre_mult_folds
  )
  port map (
    rst            => rst,
    clk            => clk,

    snk_in_2arr_2  => corr_permutator_src_out_2arr_2,

    src_out_2arr_2 => corr_folder_src_out_2arr_2
  );

  -----------------------------------------------------------------------------
  -- Complex multiplier stage
  -----------------------------------------------------------------------------
  u_corr_multiplier : entity work.corr_multiplier
  generic map (
    g_nof_inputs => c_nof_mults,
    g_data_w     => g_data_w
  )
  port map (
    clk            => clk,
    rst            => rst,

    snk_in_2arr_2  => corr_folder_src_out_2arr_2,
    src_out_arr    => corr_multiplier_src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Accumulator stage
  -----------------------------------------------------------------------------
  u_corr_accumulator : entity work.corr_accumulator
  generic map (
    g_nof_inputs         => c_nof_mults,
    g_nof_channels       => c_nof_accumulators,
    g_nof_channel_accs   => g_integration_period,
    g_data_w             => 2 * g_data_w,  -- Multiplier output data width
    g_note_is_ful        => g_note_is_ful
  )
  port map (
    clk            => clk,
    rst            => rst,

    snk_in_arr     => corr_multiplier_src_out_arr,
    src_out_arr    => corr_accumulator_src_out_arr
  );

  ------------------------------------------------------------------------------
  -- Buffer the visibilities before the folding stage.
  -- . All accumulators output blocks of visibilities at the same time. We need
  --   to buffer the visibilities to source them to the folder at the right
  --   pace.
  ------------------------------------------------------------------------------
  u_corr_visibility_buffer : entity work.corr_visibility_buffer
  generic map (
    g_nof_inputs          => c_nof_mults,
    g_data_w              => c_acc_data_w,
    g_buffer_depth        => c_visibility_buffer_depth,
    g_nof_pre_mult_folds  => g_nof_pre_mult_folds,
    g_inter_channel_delay => g_inter_channel_delay
  )
  port map (
    clk            => clk,
    rst            => rst,

    snk_in_arr     => corr_accumulator_src_out_arr,
    src_out_arr    => corr_visibility_buffer_src_out_arr
  );

  ------------------------------------------------------------------------------
  -- Fold onto one stream
  ------------------------------------------------------------------------------
  u_corr_folder : entity work.corr_folder
  generic map (
    g_nof_inputs => c_nof_mults,
    g_nof_folds  => -1
  )
  port map (
    rst         => rst,
    clk         => clk,

    snk_in_arr  => corr_visibility_buffer_src_out_arr,

    src_out_arr => src_out_arr
  );

  -----------------------------------------------------------------------------
  -- Add SOP, EOP, BSN, Channel and sync to tag the correlator output
  -----------------------------------------------------------------------------
--  u_corr_output_framer : ENTITY work.corr_output_framer
--  GENERIC MAP (
--    g_nof_inputs          => 1,
--    g_nof_words_per_frame => c_nof_visibilities,
--    g_nof_channels        => g_nof_channels,
--    g_nof_frames_per_sync => g_nof_channels
--  )
--  PORT MAP (
--    rst         => rst,
--    clk         => clk,
--
--    snk_in_arr  => corr_output_framer_snk_in_arr,
--
--    src_out_arr => src_out_arr
--  );
end str;
