-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose:
-- . SDP beamlet data output (BDO) destinations register
-- Description:
--              _________
--             |mm_fields|
--             |         |
-- reg_copi -->|  slv_out|------- _wr ----
-- reg_cipo <--|         |               |
--             |         |               v
--             |   slv_in|<--+--- _rd <--+<---- _ro
--             |_________|   |
--                           -----------------> output value
--
-- where:
-- . _ro = actual nof_destinations, actual nof_blocks_per_packet
-- . output value = destinations_info
--
-- References:
-- 1 https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD
-- 2 https://support.astron.nl/confluence/display/L2M/L4+SDPFW+Decision%3A+Multiple+beamlet+output+destinations
--

library IEEE, common_lib, mm_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_field_pkg.all;
  use work.sdp_pkg.all;
  use work.sdp_bdo_pkg.all;

entity sdp_bdo_destinations_reg is
  generic (
    g_nof_destinations_max : natural
  );
  port (
    -- Clocks and reset
    mm_clk   : in  std_logic;
    mm_rst   : in  std_logic;

    dp_clk   : in  std_logic;
    dp_rst   : in  std_logic;

    reg_copi : in  t_mem_copi;
    reg_cipo : out t_mem_cipo;

    destinations_info : out t_sdp_bdo_destinations_info
  );
end sdp_bdo_destinations_reg;

architecture str of sdp_bdo_destinations_reg is
  constant c_fa : t_common_field_arr(c_sdp_bdo_destinations_info_nof_hdr_fields - 1 downto 0) :=
    ( (field_name_pad("nof_blocks_per_packet"),     "RO",  8, field_default(c_sdp_cep_nof_blocks_per_packet)),
      (field_name_pad("nof_destinations_max"),      "RO",  8, field_default(g_nof_destinations_max)),
      (field_name_pad("nof_destinations_act"),      "RO",  8, field_default(1)),
      (field_name_pad("nof_destinations"),          "RW",  8, field_default(1)),

      (field_name_pad("udp_destination_port_31"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_30"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_29"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_28"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_27"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_26"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_25"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_24"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_23"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_22"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_21"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_20"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_19"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_18"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_17"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_16"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_15"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_14"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_13"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_12"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_11"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_10"),   "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_9"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_8"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_7"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_6"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_5"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_4"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_3"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_2"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_1"),    "RW", 16, field_default(0)),
      (field_name_pad("udp_destination_port_0"),    "RW", 16, field_default(0)),

      (field_name_pad("ip_destination_address_31"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_30"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_29"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_28"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_27"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_26"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_25"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_24"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_23"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_22"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_21"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_20"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_19"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_18"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_17"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_16"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_15"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_14"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_13"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_12"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_11"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_10"), "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_9"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_8"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_7"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_6"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_5"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_4"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_3"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_2"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_1"),  "RW", 32, field_default(0)),
      (field_name_pad("ip_destination_address_0"),  "RW", 32, field_default(0)),

      (field_name_pad("eth_destination_mac_31"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_30"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_29"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_28"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_27"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_26"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_25"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_24"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_23"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_22"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_21"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_20"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_19"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_18"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_17"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_16"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_15"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_14"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_13"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_12"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_11"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_10"),    "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_9"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_8"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_7"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_6"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_5"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_4"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_3"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_2"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_1"),     "RW", 48, field_default(0)),
      (field_name_pad("eth_destination_mac_0"),     "RW", 48, field_default(0)) );

  signal mm_fields_in  : std_logic_vector(field_slv_in_len(c_fa) - 1 downto 0);
  signal mm_fields_out : std_logic_vector(field_slv_out_len(c_fa) - 1 downto 0);

  signal destinations_info_rd : t_sdp_bdo_destinations_info := c_sdp_bdo_destinations_info_rst;
  signal destinations_info_wr : t_sdp_bdo_destinations_info := c_sdp_bdo_destinations_info_rst;

  signal nof_destinations_act  : natural := 1;
  signal nof_blocks_per_packet : natural := c_sdp_cep_nof_blocks_per_packet;
begin
  destinations_info <= destinations_info_rd;

  p_destinations_info_rd : process(destinations_info_wr,
                                   nof_destinations_act,
                                   nof_blocks_per_packet)
  begin
    -- read/write fields
    for DI in 0 to g_nof_destinations_max - 1 loop
      -- Default only read/write connect the available fields. Leave unused
      -- write fields open and read fields at rst value, so they will be
      -- optimized away by synthesis.
      destinations_info_rd.eth_destination_mac_arr(DI)    <= destinations_info_wr.eth_destination_mac_arr(DI);
      destinations_info_rd.ip_destination_address_arr(DI) <= destinations_info_wr.ip_destination_address_arr(DI);
      destinations_info_rd.udp_destination_port_arr(DI)   <= destinations_info_wr.udp_destination_port_arr(DI);
    end loop;
    destinations_info_rd.nof_destinations <= destinations_info_wr.nof_destinations;

    -- read only fields
    destinations_info_rd.nof_destinations_act  <= nof_destinations_act;
    destinations_info_rd.nof_destinations_max  <= g_nof_destinations_max;
    destinations_info_rd.nof_blocks_per_packet <= nof_blocks_per_packet;
  end process;

  u_mm_fields: entity mm_lib.mm_fields
  generic map(
    -- With g_nof_destinations_max = 31 and mac_w = 48, ip_w = 32, udp_w = 16
    -- the expected logic FF usage is 31 * (48 + 32 + 16) + 4 * 32 ~= 3104 FF.
    -- Use g_cross_clock_domain false to save clock domain crossing logic,
    -- which is about 2/3 of the total logic (~ 6200 FF). This is save,
    -- because the reg fields are set well before they are used, so any meta
    -- stability will have settled long before that.
    g_cross_clock_domain => false,
    g_use_slv_in_val     => false,  -- use false to save logic when always slv_in_val='1'
    g_field_arr          => c_fa
  )
  port map (
    mm_clk     => mm_clk,
    mm_rst     => mm_rst,

    mm_mosi    => reg_copi,
    mm_miso    => reg_cipo,

    slv_clk    => dp_clk,
    slv_rst    => dp_rst,

    slv_in     => mm_fields_in,
    slv_in_val => '1',

    slv_out    => mm_fields_out
  );

  -- add "RO" fields to mm_fields
  mm_fields_in(field_hi(c_fa, "nof_destinations_act") downto
               field_lo(c_fa, "nof_destinations_act")) <= to_uvec(destinations_info_rd.nof_destinations_act, 8);
  mm_fields_in(field_hi(c_fa, "nof_destinations_max") downto
               field_lo(c_fa, "nof_destinations_max")) <= to_uvec(destinations_info_rd.nof_destinations_max, 8);
  mm_fields_in(field_hi(c_fa, "nof_blocks_per_packet") downto
               field_lo(c_fa, "nof_blocks_per_packet")) <= to_uvec(destinations_info_rd.nof_blocks_per_packet, 8);

  -- get "RW" fields from mm_fields
  destinations_info_wr.eth_destination_mac_arr(0)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_0") downto
                                                                    field_lo(c_fa, "eth_destination_mac_0"));
  destinations_info_wr.eth_destination_mac_arr(1)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_1") downto
                                                                    field_lo(c_fa, "eth_destination_mac_1"));
  destinations_info_wr.eth_destination_mac_arr(2)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_2") downto
                                                                    field_lo(c_fa, "eth_destination_mac_2"));
  destinations_info_wr.eth_destination_mac_arr(3)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_3") downto
                                                                    field_lo(c_fa, "eth_destination_mac_3"));
  destinations_info_wr.eth_destination_mac_arr(4)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_4") downto
                                                                    field_lo(c_fa, "eth_destination_mac_4"));
  destinations_info_wr.eth_destination_mac_arr(5)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_5") downto
                                                                    field_lo(c_fa, "eth_destination_mac_5"));
  destinations_info_wr.eth_destination_mac_arr(6)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_6") downto
                                                                    field_lo(c_fa, "eth_destination_mac_6"));
  destinations_info_wr.eth_destination_mac_arr(7)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_7") downto
                                                                    field_lo(c_fa, "eth_destination_mac_7"));
  destinations_info_wr.eth_destination_mac_arr(8)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_8") downto
                                                                    field_lo(c_fa, "eth_destination_mac_8"));
  destinations_info_wr.eth_destination_mac_arr(9)  <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_9") downto
                                                                    field_lo(c_fa, "eth_destination_mac_9"));
  destinations_info_wr.eth_destination_mac_arr(10) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_10") downto
                                                                    field_lo(c_fa, "eth_destination_mac_10"));
  destinations_info_wr.eth_destination_mac_arr(11) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_11") downto
                                                                    field_lo(c_fa, "eth_destination_mac_11"));
  destinations_info_wr.eth_destination_mac_arr(12) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_12") downto
                                                                    field_lo(c_fa, "eth_destination_mac_12"));
  destinations_info_wr.eth_destination_mac_arr(13) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_13") downto
                                                                    field_lo(c_fa, "eth_destination_mac_13"));
  destinations_info_wr.eth_destination_mac_arr(14) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_14") downto
                                                                    field_lo(c_fa, "eth_destination_mac_14"));
  destinations_info_wr.eth_destination_mac_arr(15) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_15") downto
                                                                    field_lo(c_fa, "eth_destination_mac_15"));
  destinations_info_wr.eth_destination_mac_arr(16) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_16") downto
                                                                    field_lo(c_fa, "eth_destination_mac_16"));
  destinations_info_wr.eth_destination_mac_arr(17) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_17") downto
                                                                    field_lo(c_fa, "eth_destination_mac_17"));
  destinations_info_wr.eth_destination_mac_arr(18) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_18") downto
                                                                    field_lo(c_fa, "eth_destination_mac_18"));
  destinations_info_wr.eth_destination_mac_arr(19) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_19") downto
                                                                    field_lo(c_fa, "eth_destination_mac_19"));
  destinations_info_wr.eth_destination_mac_arr(20) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_20") downto
                                                                    field_lo(c_fa, "eth_destination_mac_20"));
  destinations_info_wr.eth_destination_mac_arr(21) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_21") downto
                                                                    field_lo(c_fa, "eth_destination_mac_21"));
  destinations_info_wr.eth_destination_mac_arr(22) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_22") downto
                                                                    field_lo(c_fa, "eth_destination_mac_22"));
  destinations_info_wr.eth_destination_mac_arr(23) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_23") downto
                                                                    field_lo(c_fa, "eth_destination_mac_23"));
  destinations_info_wr.eth_destination_mac_arr(24) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_24") downto
                                                                    field_lo(c_fa, "eth_destination_mac_24"));
  destinations_info_wr.eth_destination_mac_arr(25) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_25") downto
                                                                    field_lo(c_fa, "eth_destination_mac_25"));
  destinations_info_wr.eth_destination_mac_arr(26) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_26") downto
                                                                    field_lo(c_fa, "eth_destination_mac_26"));
  destinations_info_wr.eth_destination_mac_arr(27) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_27") downto
                                                                    field_lo(c_fa, "eth_destination_mac_27"));
  destinations_info_wr.eth_destination_mac_arr(28) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_28") downto
                                                                    field_lo(c_fa, "eth_destination_mac_28"));
  destinations_info_wr.eth_destination_mac_arr(29) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_29") downto
                                                                    field_lo(c_fa, "eth_destination_mac_29"));
  destinations_info_wr.eth_destination_mac_arr(30) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_30") downto
                                                                    field_lo(c_fa, "eth_destination_mac_30"));
  destinations_info_wr.eth_destination_mac_arr(31) <= mm_fields_out(field_hi(c_fa, "eth_destination_mac_31") downto
                                                                    field_lo(c_fa, "eth_destination_mac_31"));

  destinations_info_wr.ip_destination_address_arr(0)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_0")
                                                                downto field_lo(c_fa, "ip_destination_address_0"));
  destinations_info_wr.ip_destination_address_arr(1)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_1")
                                                                downto field_lo(c_fa, "ip_destination_address_1"));
  destinations_info_wr.ip_destination_address_arr(2)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_2")
                                                                downto field_lo(c_fa, "ip_destination_address_2"));
  destinations_info_wr.ip_destination_address_arr(3)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_3")
                                                                downto field_lo(c_fa, "ip_destination_address_3"));
  destinations_info_wr.ip_destination_address_arr(4)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_4")
                                                                downto field_lo(c_fa, "ip_destination_address_4"));
  destinations_info_wr.ip_destination_address_arr(5)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_5")
                                                                downto field_lo(c_fa, "ip_destination_address_5"));
  destinations_info_wr.ip_destination_address_arr(6)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_6")
                                                                downto field_lo(c_fa, "ip_destination_address_6"));
  destinations_info_wr.ip_destination_address_arr(7)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_7")
                                                                downto field_lo(c_fa, "ip_destination_address_7"));
  destinations_info_wr.ip_destination_address_arr(8)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_8")
                                                                downto field_lo(c_fa, "ip_destination_address_8"));
  destinations_info_wr.ip_destination_address_arr(9)  <= mm_fields_out(field_hi(c_fa, "ip_destination_address_9")
                                                                downto field_lo(c_fa, "ip_destination_address_9"));
  destinations_info_wr.ip_destination_address_arr(10) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_10")
                                                                downto field_lo(c_fa, "ip_destination_address_10"));
  destinations_info_wr.ip_destination_address_arr(11) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_11")
                                                                downto field_lo(c_fa, "ip_destination_address_11"));
  destinations_info_wr.ip_destination_address_arr(12) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_12")
                                                                downto field_lo(c_fa, "ip_destination_address_12"));
  destinations_info_wr.ip_destination_address_arr(13) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_13")
                                                                downto field_lo(c_fa, "ip_destination_address_13"));
  destinations_info_wr.ip_destination_address_arr(14) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_14")
                                                                downto field_lo(c_fa, "ip_destination_address_14"));
  destinations_info_wr.ip_destination_address_arr(15) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_15")
                                                                downto field_lo(c_fa, "ip_destination_address_15"));
  destinations_info_wr.ip_destination_address_arr(16) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_16")
                                                                downto field_lo(c_fa, "ip_destination_address_16"));
  destinations_info_wr.ip_destination_address_arr(17) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_17")
                                                                downto field_lo(c_fa, "ip_destination_address_17"));
  destinations_info_wr.ip_destination_address_arr(18) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_18")
                                                                downto field_lo(c_fa, "ip_destination_address_18"));
  destinations_info_wr.ip_destination_address_arr(19) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_19")
                                                                downto field_lo(c_fa, "ip_destination_address_19"));
  destinations_info_wr.ip_destination_address_arr(20) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_20")
                                                                downto field_lo(c_fa, "ip_destination_address_20"));
  destinations_info_wr.ip_destination_address_arr(21) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_21")
                                                                downto field_lo(c_fa, "ip_destination_address_21"));
  destinations_info_wr.ip_destination_address_arr(22) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_22")
                                                                downto field_lo(c_fa, "ip_destination_address_22"));
  destinations_info_wr.ip_destination_address_arr(23) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_23")
                                                                downto field_lo(c_fa, "ip_destination_address_23"));
  destinations_info_wr.ip_destination_address_arr(24) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_24")
                                                                downto field_lo(c_fa, "ip_destination_address_24"));
  destinations_info_wr.ip_destination_address_arr(25) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_25")
                                                                downto field_lo(c_fa, "ip_destination_address_25"));
  destinations_info_wr.ip_destination_address_arr(26) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_26")
                                                                downto field_lo(c_fa, "ip_destination_address_26"));
  destinations_info_wr.ip_destination_address_arr(27) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_27")
                                                                downto field_lo(c_fa, "ip_destination_address_27"));
  destinations_info_wr.ip_destination_address_arr(28) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_28")
                                                                downto field_lo(c_fa, "ip_destination_address_28"));
  destinations_info_wr.ip_destination_address_arr(29) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_29")
                                                                downto field_lo(c_fa, "ip_destination_address_29"));
  destinations_info_wr.ip_destination_address_arr(30) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_30")
                                                                downto field_lo(c_fa, "ip_destination_address_30"));
  destinations_info_wr.ip_destination_address_arr(31) <= mm_fields_out(field_hi(c_fa, "ip_destination_address_31")
                                                                downto field_lo(c_fa, "ip_destination_address_31"));

  destinations_info_wr.udp_destination_port_arr(0)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_0") downto
                                                                     field_lo(c_fa, "udp_destination_port_0"));
  destinations_info_wr.udp_destination_port_arr(1)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_1") downto
                                                                     field_lo(c_fa, "udp_destination_port_1"));
  destinations_info_wr.udp_destination_port_arr(2)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_2") downto
                                                                     field_lo(c_fa, "udp_destination_port_2"));
  destinations_info_wr.udp_destination_port_arr(3)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_3") downto
                                                                     field_lo(c_fa, "udp_destination_port_3"));
  destinations_info_wr.udp_destination_port_arr(4)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_4") downto
                                                                     field_lo(c_fa, "udp_destination_port_4"));
  destinations_info_wr.udp_destination_port_arr(5)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_5") downto
                                                                     field_lo(c_fa, "udp_destination_port_5"));
  destinations_info_wr.udp_destination_port_arr(6)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_6") downto
                                                                     field_lo(c_fa, "udp_destination_port_6"));
  destinations_info_wr.udp_destination_port_arr(7)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_7") downto
                                                                     field_lo(c_fa, "udp_destination_port_7"));
  destinations_info_wr.udp_destination_port_arr(8)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_8") downto
                                                                     field_lo(c_fa, "udp_destination_port_8"));
  destinations_info_wr.udp_destination_port_arr(9)  <= mm_fields_out(field_hi(c_fa, "udp_destination_port_9") downto
                                                                     field_lo(c_fa, "udp_destination_port_9"));
  destinations_info_wr.udp_destination_port_arr(10) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_10") downto
                                                                     field_lo(c_fa, "udp_destination_port_10"));
  destinations_info_wr.udp_destination_port_arr(11) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_11") downto
                                                                     field_lo(c_fa, "udp_destination_port_11"));
  destinations_info_wr.udp_destination_port_arr(12) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_12") downto
                                                                     field_lo(c_fa, "udp_destination_port_12"));
  destinations_info_wr.udp_destination_port_arr(13) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_13") downto
                                                                     field_lo(c_fa, "udp_destination_port_13"));
  destinations_info_wr.udp_destination_port_arr(14) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_14") downto
                                                                     field_lo(c_fa, "udp_destination_port_14"));
  destinations_info_wr.udp_destination_port_arr(15) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_15") downto
                                                                     field_lo(c_fa, "udp_destination_port_15"));
  destinations_info_wr.udp_destination_port_arr(16) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_16") downto
                                                                     field_lo(c_fa, "udp_destination_port_16"));
  destinations_info_wr.udp_destination_port_arr(17) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_17") downto
                                                                     field_lo(c_fa, "udp_destination_port_17"));
  destinations_info_wr.udp_destination_port_arr(18) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_18") downto
                                                                     field_lo(c_fa, "udp_destination_port_18"));
  destinations_info_wr.udp_destination_port_arr(19) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_19") downto
                                                                     field_lo(c_fa, "udp_destination_port_19"));
  destinations_info_wr.udp_destination_port_arr(20) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_20") downto
                                                                     field_lo(c_fa, "udp_destination_port_20"));
  destinations_info_wr.udp_destination_port_arr(21) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_21") downto
                                                                     field_lo(c_fa, "udp_destination_port_21"));
  destinations_info_wr.udp_destination_port_arr(22) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_22") downto
                                                                     field_lo(c_fa, "udp_destination_port_22"));
  destinations_info_wr.udp_destination_port_arr(23) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_23") downto
                                                                     field_lo(c_fa, "udp_destination_port_23"));
  destinations_info_wr.udp_destination_port_arr(24) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_24") downto
                                                                     field_lo(c_fa, "udp_destination_port_24"));
  destinations_info_wr.udp_destination_port_arr(25) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_25") downto
                                                                     field_lo(c_fa, "udp_destination_port_25"));
  destinations_info_wr.udp_destination_port_arr(26) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_26") downto
                                                                     field_lo(c_fa, "udp_destination_port_26"));
  destinations_info_wr.udp_destination_port_arr(27) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_27") downto
                                                                     field_lo(c_fa, "udp_destination_port_27"));
  destinations_info_wr.udp_destination_port_arr(28) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_28") downto
                                                                     field_lo(c_fa, "udp_destination_port_28"));
  destinations_info_wr.udp_destination_port_arr(29) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_29") downto
                                                                     field_lo(c_fa, "udp_destination_port_29"));
  destinations_info_wr.udp_destination_port_arr(30) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_30") downto
                                                                     field_lo(c_fa, "udp_destination_port_30"));
  destinations_info_wr.udp_destination_port_arr(31) <= mm_fields_out(field_hi(c_fa, "udp_destination_port_31") downto
                                                                     field_lo(c_fa, "udp_destination_port_31"));

  destinations_info_wr.nof_destinations <= to_uint(mm_fields_out(field_hi(c_fa, "nof_destinations") downto
                                                                 field_lo(c_fa, "nof_destinations")));

  -- Register the read only actual values, to ease timing closure
  p_dp_clk : process(dp_clk)
    constant c_nof_blocks_per_packet_arr : t_natural_arr(1 to g_nof_destinations_max) :=
      func_sdp_bdo_reorder_nof_blocks_look_up_table(g_nof_destinations_max);
  begin
    if rising_edge(dp_clk) then
      nof_destinations_act <= func_sdp_bdo_parse_nof_destinations(destinations_info_wr.nof_destinations,
                                                                  g_nof_destinations_max);
      nof_blocks_per_packet <= c_nof_blocks_per_packet_arr(nof_destinations_act);
    end if;
  end process;
end str;
