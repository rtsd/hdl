// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
xnRC4SLnpuhWRzLFjliKEAqIXnff3w/+x8ORdeOvfvGx3iJaaVaMKjr7XM4/36n7
jeLNglWNc9T903gg2jrWmpiVX9TfvzNE9PBP/CJ+eb/PBrT9oike/qVMYHPvPfnu
VwrNZEyYBLvxtIlrFlePSCjld47vwjVK3cSwwHrSou8=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 1328 )
`pragma protect data_block
ynlKBKXcirZuHpwjSmiJYoHUjJD2yErkiWieWXY3fA8FLpfnvGGP8ROXdvMQdr3B
uTE2IgtBXtq8+6Vw65ShPGKsCJtsMp3ANCA31qd2SIbpD9ZHzcEE4QyQ6FQDr5K4
RMduPtzX51ducu22KqQNqR641Og25/0AsQYNz/Y6g7W6a3h+icorrMuOn6ah3zKL
7vsvccf8hqyW1IUuFctfw2lI2FpAPqQjBXxCtDO3V9MJl+7pguIBcuXYOOC5//rx
VhAGNH8YovEaQXzi71p0xm+YlmvE0JqnGyQDTi31Uunah4gquCNkr7G02cfGvB2P
4sv+zeXD6k/aTrsZoyyJf23IYznFqMNZsCSx4Gm8jQk11xGM1shM6mzZs8+G0MrZ
aIHs+xLx6Nk4bqBZjHd7cAfmsHIiWvJsLRelyk/vdXxWQAkk+feTaSUfiL1Sxf8w
TaUCwMgXdFWM0+IK/J+9zqKyFHD34e3lxdSXw+X5dnOS7K3eY8c4YJc2pabEb3CE
22otx4c2LkKP9jd+KDackDY3ASfrhdX/xA+BVz40EyEa7zSm9zsyHTihgnT39Tk1
C82e4IlURqP97HHHsPPaexyX54gE/DXghkaIksnVDZ1YlBnYnhtcFgYIHNZt3GCo
SKz8bynLbsyx7EBcI3nC1CnWBoCGJEPo7DvqMN6q5M557de3FqRUMtz+8k5aOvEI
S3gETdBrnJ2RjLlfZhDitnRz1RFmZGYCIBm7YrocuN8ws624IcKN7eyXNnC83C5Z
WYJEgpbOUxftrY8MSDC5BZN8aAswnDxp+ndZ0OPcnceAKNaaHFkm0FUIzyAlSKZk
n/ec0+lY533YyzdbuBRIO9b4c0mqZn1v6JhEUjZknRuK2zjrRMP60y2avj6Wz0PN
ZBpseiTHTWUafq4RQ2OXcHzDV9S/Ua41MWk23mcersx2pfS2aBVWjWRXaAFL5MOH
V2+xsd7WLaeZbB0CoFpGeX7acCvbWb6qCKqUFipDoYWEjbA3VHnbrUiKCAfWnsCU
8zphlgrSwa8l/7sKBbMO4CGhX/thsW+yoNIueE52kYHde462KF4tS92uw6zlXTI4
JWumJGsgBd2g7mwJV7J2UTH1BTyDsTtBgqZYgURM5WPrXhEGpygKT1juQxTgj85y
4i0tWb3Qofc0j3XpmpBXozZ/7ArXpI7ok+m02ZAkqO/3qp9ZhHXbzi++ib8iUO4W
hIgNgnHeWXz0OEIJT/4IwdUYZMYNtOr/Iu1vMVtKPe4/zarCThAyug9Q2AMi4jxh
xg5o7wojcK1L5DsWzFPdGocjm8b0nGPfYHtKlk8v/z7ZM8RKJtdrC8bUrFdUXAlA
m/+6Bz2C/8/oHzYOuURdncdVE86dZcg5NrmgEF2FX2py5gVXNKPZo/XlwxPsvpdz
3M6cNtZYScuTk8BdOyeFG+F3QyxXXeFAiLy1HEzeD7mINktRSGCiWwERY0Ebbwb3
JOzMoZCctMjX13eAymFpNvpc5xQSJb0pCfcBEs0YynQAUbeH5h3VBunw70DIrqgq
BtKuHVL0mLLQJ1JMyhlSnaGSmfTn9yA9DPr9BPDJitlgFj8zfHsK4MmCacwhZX3K
VjbCX2pNGKceO5Xy+LjkdJoHp1sQQ9b9Hf8LUnc3YFXk4uGX5YbYsTZmOb0jxLZb
jA2/GftldKJw5Au9N3HpEHOlicSmZUFDU1/PT359UGPXGdgs758GEA9toSczq59X
zKApYBp7Hcrxcuy+28OfhnK29bI8dJH9hIWJ75UAr+M=

`pragma protect end_protected
