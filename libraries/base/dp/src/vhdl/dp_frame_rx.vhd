--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_packetizing_pkg.all;

-- Reuse from LOFAR rad_frame_rx.vhd and rad_frame_rx(rtl).vhd

-- Purpose:
--   From the input dp frame PHY stream:
--
--          -----------------------------
--     idle | sfd | fsn | dat ... | crc | idle ...
--          -----------------------------
--
--   detect and output the dp frames for g_sfd:
--
--                -----------------------
--                | fsn | dat ... | brc |
--                -----------------------
--                  sof             eof
--                  val   val ...   val
--
--   Mark the sof and eof of each frame and check the received CRC
-- Description:
--   A dp PHY frame consists of g_sfd, fsn, data and crc. The dp PHY frames
--   are seperated by at least one c_dp_idle word. The received crc is passed
--   on as boolean crc called bcr, where '0' indicates OK and '1' indicates
--   that an crc error.
--   The sof is detected based on a c_dp_idle to g_sfd word transition and the
--   eof is then determined by counting g_block_size data words. The g_sfd word
--   is not passed on, because we have the sof signal. Therefore the internal
--   frame consists of fsn, dat, bcr fields and seperate sof and eof signals.
--   The sof signal is active during the fsn and the eof is active during the
--   brc.
--   While counting valid data words the module is insensitive to c_dp_idle/
--   g_sfd. Hence the output is always a full frame, if for some reason the
--   input framing got corrupted, then this will reflect in the crc so the brc
--   will note error. Perhaps even a frame may get lost, but the frame
--   reception will recover on a next sof detection. Idle words between
--   internal frames are simply passed on, which is fine since out_val is
--   inactive.
-- Remark:
-- . Frame output is flow controlled on frame level only, by the out_xon input.
-- . Typically choose the rx timeout g_timeout_w=0 if a link is fixed after
--   power up (e.g. for RSP RI). If the link can stop and start asynchronously
--   (e.g. changing cables, transmitter restart) then choose g_timeout_w
--   sufficient to fit the frame period (e.g. for RSP Serdes).
-- . In p_state the rx timeout restart after every active in_val_reg, this in
--   principle allows using a smaller timeout that is sufficient to span the
--   inactive gaps of in_val_reg, so then g_timeout_w <= c_rsp_slice_size is
--   suficient. An alternative implementation for the timeout would be to let
--   the timeout run from the sop, the timeout must then at least span the
--   frame period. The frame period can vary a somewhat due to mutliplexing
--   so with c_rsp_slice_size the g_timeout_w would then have to be set to
--   c_rsp_slice_size+1

entity dp_frame_rx is
  generic (
    g_sfd          : std_logic_vector := c_dp_sfd;
    g_lofar        : boolean := false;  -- when TRUE use same CRC as for LOFAR
    g_dat_w        : natural := c_dp_max_w;
    g_block_size   : natural := 1000;
    g_timeout_w    : natural := 0  -- when 0 then no timeout else when > 0 then flush pending rx payload after 2**g_timeout_w clk cylces of inactive in_val
  );
  port (
    rst      : in  std_logic;
    clk      : in  std_logic;
    in_dat   : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_val   : in  std_logic := '1';
    out_dat  : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val  : out std_logic;
    out_sof  : out std_logic;
    out_eof  : out std_logic;
    out_xon  : in  std_logic := '1'
  );
end dp_frame_rx;

architecture rtl of dp_frame_rx is
  -- Use unregistered input to save logic, use registered input to ease achieving timing constrains.
  constant c_input_reg   : boolean := false;

  constant c_idle        : std_logic_vector := c_dp_idle(in_dat'range);
  constant c_sfd         : std_logic_vector := g_sfd(in_dat'range);
  constant c_brc_ok      : std_logic_vector := std_logic_vector(to_unsigned(c_dp_brc_ok, g_dat_w));
  constant c_brc_err     : std_logic_vector := std_logic_vector(to_unsigned(c_dp_brc_err, g_dat_w));

  constant c_timeout_cnt_w : natural := g_timeout_w + 1;

  type t_state is (s_sof, s_fsn, s_eof, s_flush);

  signal crc             : std_logic_vector(c_dp_crc_w - 1 downto 0);
  signal nxt_crc         : std_logic_vector(crc'range);
  signal cnt             : natural range 0 to g_block_size;
  signal nxt_cnt         : natural;

  -- Registered inputs (see c_input_reg)
  signal in_val_reg      : std_logic;
  signal in_dat_reg      : std_logic_vector(in_dat'range);

  signal prev_in_dat_reg : std_logic_vector(in_dat'range);
  signal nxt_out_dat     : std_logic_vector(out_dat'range);
  signal nxt_out_val     : std_logic;
  signal nxt_out_sof     : std_logic;
  signal nxt_out_eof     : std_logic;

  signal xon_reg         : std_logic;
  signal nxt_xon_reg     : std_logic;

  signal state           : t_state;
  signal nxt_state       : t_state;

  -- handle rx timeout
  signal timeout_cnt          : std_logic_vector(c_timeout_cnt_w - 1 downto 0);
  signal timeout_cnt_clr      : std_logic;
  signal nxt_timeout_cnt_clr  : std_logic;
  signal timeout_evt          : std_logic;
  signal flush_en             : std_logic;
  signal nxt_flush_en         : std_logic;
  signal flush_val            : std_logic;

  procedure proc_handle_rx_timeout(signal   valid       : in    std_logic;
                                   signal   timeout_evt : in    std_logic;
                                   signal   clr         : out   std_logic;
                                   variable v_state     : inout t_state) is  -- use variable v_state instead of signal to avoid getting latches
  begin
    if valid = '1' then
      clr <= '1';  -- restart timeout_cnt during frame rx and remain in current state
    else
      clr <= '0';  -- let valid inactive timeout count increment
      if timeout_evt = '1' then  -- can only occur when g_timeout_w>0
        v_state := s_flush;  -- exit to flush state to finish the current rx frame with void data
      end if;
    end if;
  end proc_handle_rx_timeout;
begin
  gen_input_reg : if c_input_reg = true generate
    p_reg : process (clk, rst)
    begin
      if rst = '1' then
        in_val_reg <= '0';
        in_dat_reg <= (others => '0');
      elsif rising_edge(clk) then
        in_val_reg <= in_val;
        in_dat_reg <= in_dat;
      end if;
    end process;
  end generate;

  no_input_reg : if c_input_reg = false generate
    in_val_reg <= in_val;
    in_dat_reg <= in_dat;
  end generate;

  p_clk : process (clk, rst)
  begin
    if rst = '1' then
      timeout_cnt_clr <= '1';
      flush_en        <= '0';
      crc             <= (others => '1');
      cnt             <= 0;
      prev_in_dat_reg <= (others => '0');
      out_dat         <= (others => '0');
      out_val         <= '0';
      out_sof         <= '0';
      out_eof         <= '0';
      xon_reg         <= '0';
      state           <= s_sof;
    elsif rising_edge(clk) then
      timeout_cnt_clr <= nxt_timeout_cnt_clr;
      flush_en        <= nxt_flush_en;
      crc             <= nxt_crc;
      cnt             <= nxt_cnt;
      prev_in_dat_reg <= in_dat_reg;
      out_dat         <= nxt_out_dat;
      out_val         <= nxt_out_val;
      out_sof         <= nxt_out_sof;
      out_eof         <= nxt_out_eof;
      xon_reg         <= nxt_xon_reg;
      state           <= nxt_state;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Rx timeout control
  ------------------------------------------------------------------------------
  no_timeout : if g_timeout_w = 0 generate
    timeout_evt <= '0';
    flush_val   <= '0';
  end generate;

  gen_timeout : if g_timeout_w > 0 generate
    u_timeout_cnt : entity common_lib.common_counter
    generic map (
      g_width     => c_timeout_cnt_w
    )
    port map (
      rst     => rst,
      clk     => clk,
      cnt_clr => timeout_cnt_clr,
      count   => timeout_cnt
    );

    timeout_evt <= timeout_cnt(g_timeout_w);  -- check MSbit for timeout of 2**g_timeout_w clk cycles

    assert not(rising_edge(clk) and timeout_evt = '1')
      report "rad_frame_rx timeout occurred!"
      severity WARNING;

    -- define flush_val to prepare for throttle support of out_val during flush, default no throttle support yet
    flush_val <= flush_en;
  end generate;

  ------------------------------------------------------------------------------
  -- XON/XOFF rx frame flow control
  ------------------------------------------------------------------------------
  nxt_xon_reg <= out_xon;  -- register out_xon to ease timing closure, because out_xon may come from far on the chip and some more cycles latency for xon is acceptable

  ------------------------------------------------------------------------------
  -- Rx state machine
  ------------------------------------------------------------------------------
  p_state: process(state, crc, cnt, in_val_reg, in_dat_reg, prev_in_dat_reg, xon_reg, timeout_evt, flush_val)
    variable v_nxt_state : t_state;  -- use variable instead of signal to avoid getting latches with proc_handle_rx_timeout()
  begin
    nxt_timeout_cnt_clr  <= '1';  -- default no timeout_cnt
    nxt_flush_en         <= '0';

    nxt_crc     <= crc;
    nxt_cnt     <= cnt;
    nxt_out_dat <= in_dat_reg;  -- default pass on input dat
    nxt_out_val <= '0';
    nxt_out_sof <= '0';
    nxt_out_eof <= '0';
    v_nxt_state := state;

    if in_val_reg = '1' then
      nxt_crc <= func_dp_next_crc(g_lofar, in_dat_reg, crc);
    end if;

    case state is
      when s_sof =>
        nxt_crc <= (others => '1');
        nxt_cnt <= 0;
        if xon_reg = '1' then  -- flush any input rx frame if xon_reg is XOFF
          if in_val_reg = '1' then
            if in_dat_reg = c_sfd and prev_in_dat_reg = c_idle then  -- ignore in_val during c_idle
              v_nxt_state := s_fsn;
            end if;
          end if;
        end if;
      when s_fsn =>
        if in_val_reg = '1' then
          nxt_out_val <= '1';
          nxt_out_sof <= '1';
          v_nxt_state := s_eof;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(in_val_reg, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when s_eof =>
        if in_val_reg = '1' then
          nxt_out_val <= '1';
          if cnt = g_block_size then  -- fill in brc based on rx crc result
            nxt_out_eof <= '1';
            nxt_out_dat <= c_brc_ok;
            if c_dp_crc_w > g_dat_w then
              if crc(in_dat'range) /= in_dat_reg then
                nxt_out_dat <= c_brc_err;
              end if;
            else
              if crc /= in_dat_reg(crc'range) then
                nxt_out_dat <= c_brc_err;
              end if;
            end if;
            v_nxt_state := s_sof;
          else
            nxt_cnt <= cnt + 1;  -- count payload data
          end if;
        end if;
        -- Exit to s_flush in case of rx timeout
        proc_handle_rx_timeout(in_val_reg, timeout_evt, nxt_timeout_cnt_clr, v_nxt_state);
      when others =>  -- s_flush                                -- this state can only be reached if g_timeout_w>0
        nxt_flush_en <= '1';  -- enable flush frame due to in_val_reg inactive timeout
        if flush_val = '1' then
          nxt_out_val <= '1';  -- flush rx frame with void data
          if cnt >= g_block_size then
            nxt_out_eof <= '1';
            nxt_out_dat <= c_brc_err;  -- fill in c_brc_err for flushed rx frame
            v_nxt_state := s_sof;
          else
            nxt_cnt <= cnt + 1;  -- count payload data
          end if;
        end if;
    end case;

    nxt_state <= v_nxt_state;
  end process;
end rtl;
