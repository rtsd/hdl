/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2010
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

/* Compile switches. */

/* includes */
#include <system.h>
#include <packet.h>
#include <unbos_pps.h>
#include <unbos_eth.h>
#include <unbosx_util.h>
#include <io2_copy.h>

/* Wether or not we actually _have_ flash, for bookeepingssake
 * we remember wether we tried to initialize it.
 * 42 is the signal that no-one has tried to init it.
 * Otherwise it contains the actual initialization result
 * (0 => OK, -1 => Dang!)
 */
static int didInitFlash = 42;

/* Support this on designs that have it */
#ifdef __ALTERA_AVALON_EPCS_FLASH_CONTROLLER 
#include <altera_avalon_epcs_flash_controller.h>

static alt_flash_epcs_dev tFlashDev = { 
{  
    ALT_LLIST_ENTRY,
    "flash_dev",
    NULL,                 /*open*/
    NULL,                 /*close*/
    alt_epcs_flash_write, /*write*/
    alt_epcs_flash_read,  /*read*/
    alt_epcs_flash_get_info, /*get info*/
    alt_epcs_flash_erase_block,
    alt_epcs_flash_write_block,
    ((void*)EPCS_FLASH_CONTROLLER_0_BASE)
  },
  EPCS_FLASH_CONTROLLER_0_REGISTER_OFFSET + EPCS_FLASH_CONTROLLER_0_BASE
};
#else
// No flash in this design - supply default implementations
// of functions used below: keeps linker happy AND if they
// would happen to be called, they return error.
typedef int alt_flash_dev;
typedef int alt_flash_epcs_dev;
static alt_flash_epcs_dev tFlashDev;

int alt_epcs_flash_init(alt_flash_epcs_dev* p) {
    return -1;
}
int alt_epcs_flash_query(alt_flash_epcs_dev* p) {
    return -1;
}
int alt_epcs_flash_write_block(alt_flash_dev* p, int notused, int i, const void* s, int l) {
    return -1;
}
int alt_epcs_flash_read(alt_flash_dev* p, int i, const void* s, int l) {
    return -1;
}
int alt_epcs_flash_erase_block(alt_flash_dev* p, int b) {
    return -1;
}

#endif

#define PKT_SIZE 1500

// The UniBoard defined opcodes
#define CONFIGE   ((alt_u32)0x8)
#define CONFIGR   ((alt_u32)0x7)
#define CONFIGW   ((alt_u32)0x6)
#define READN     ((alt_u32)0x1)
#define WRITEN    ((alt_u32)0x2)
#define ANDN      ((alt_u32)0x3)
#define ORN       ((alt_u32)0x4)
#define XORN      ((alt_u32)0x5)
#define FREADN    ((alt_u32)0x9)
#define FWRITEN   ((alt_u32)0xa)
#define BITWRITEN ((alt_u32)0xb)
#define WAITPPS   ((alt_u32)0xffffffff)

/* implicit external bitmask for updating bitfields,
 * used by bit_write_f */
alt_u32 bitfield_mask;

/* for dealing with flash ... */
int init_flash();
alt_u32 init_flash_first_w(void* to, void* from);
alt_u32 init_flash_first_r(void* to, void* from);
alt_u32 init_flash_first_e(void* sectoraddr);
alt_u32 flash_write_page(void* to, void* from);
alt_u32 flash_read_page(void* to, void* from);
alt_u32 flash_erase_sector(void* sectoraddr);
alt_u32 no_flash_available(void* to, void* from);
alt_u32 no_flash_available1(void* to);

// Function pointer to the actual flash-write function.
// We let it point at the initialization routine first
static alt_u32 (*flash_writer)(void*, void*) = init_flash_first_w;
static alt_u32 (*flash_reader)(void*, void*) = init_flash_first_r;
static alt_u32 (*flash_eraser)(void*)        = init_flash_first_e;


// Packet handler
unsigned int handlePacket() {
    if( !NET_IS_CONTROLPKT(eth_frmInfo) ) 
		return 0;

	TUInt32               tmp;
	unsigned int          replySz;
	const unsigned int    hdrSzByte = ((4/*eth*/ + 5/*ip*/ + 2/*udp*/)*4);

	// Start processing commands. They start immediately following
	// the UDP/IP header. Write the results into the reply packet,
	// immediately following the UDP header
	replySz = proc_packet( (alt_u32 volatile*)((unsigned char volatile* const)rx+hdrSzByte),
			(alt_u32 volatile*)((unsigned char volatile* const)tx+hdrSzByte) );
	// replySz is now in units of words. We need bytes
	replySz *= 4;

	// Update the IP and UDP headers with the reply size
	// IP: TOTAL LENGTH field is lower 16 bits in header word 4
	//     it is ip hdr + udp hdr + udp payload =
	//           |---28 bytes --| + payloadsize
	// UDP: SIZE is upper 16 bits in word 10 of the header
	//     its value is   udp hdr + udp payload
	//                  | 8 byte| + udp payload

	// Get word 4 from the header (note: they are still in
	// network byte order), clear the lower 16bits
	//        tmp  = ntohl(NET_HDRWORD(ptReply, 4));
	tmp  = UNBOSX_UTIL_ntohl(tx[4]);
	tmp &= 0xffff0000;
	tmp |= ((28+replySz)&0x0000ffff);
	tx[4] = UNBOSX_UTIL_htonl(tmp);

	// Id. for the UDP header
	//        tmp  = ntohl(NET_HDRWORD(ptReply, 10));
	tmp  = UNBOSX_UTIL_ntohl(tx[10]);
	tmp &= 0x0000ffff;
	tmp |= (((8+replySz)&0x0000ffff)<<16);
	tx[10] = UNBOSX_UTIL_htonl(tmp);

	// return the total replysize
	return (44 + replySz);
}


alt_u32 and_f(alt_u32 l, alt_u32 r) {
    return (l&r);
}
alt_u32 or_f(alt_u32 l, alt_u32 r) {
    return (l|r);
}
alt_u32 xor_f(alt_u32 l, alt_u32 r) {
    return (l^r);
}

/* Implicitly make use of the externally provided mask ... */
alt_u32 bit_write_f(alt_u32 l, alt_u32 r) {
	/* Clear the bitpositions in the current value */
	l &= (~bitfield_mask);
	return (l | (r&bitfield_mask));
}

alt_u32 apply(alt_u32* dst, alt_u32* src, unsigned int n, alt_u32 (*f)(alt_u32, alt_u32));

static char const* const pps0 = "0PPS";
static char const* const pps1 = "1PPS";
/* start processing commands startting at "sop" (start of packet)
 * building up the reply starting at "reply". 
 * processing stops when a 0x00 command is encountered
 * Returns the size of the reply
 *
 * As per the revised command format, all fields are by definition
 * a multiple of four bytes AND the addresses are four-byte aligned.
 * This makes processing a pakkit of commands mighty simple: we 
 * do not have to copy field into local storage before we can use 
 * them, we can create pointers and directly de-reference them!
 * w00t.
 */
alt_u32 proc_packet(alt_u32 volatile* sop, alt_u32 volatile* reply) {
    alt_u32           nitem;
    alt_u32           address;
    alt_u32*          data;
    alt_u32           sinc, dinc;
    alt_u32 volatile* begin_of_reply = reply;

    /* Copy over the PSN */

    *reply++ = *sop++;
    while( *sop ) {
		/* start with default source-inc and dest-inc of '1' */
		sinc = dinc = 1;
        switch( *sop ) {

            case WAITPPS:
				if( UNBOS_pps_wait() )
					*((alt_u32*)reply) = *((alt_u32*)&pps1[0]);
				else
					*((alt_u32*)reply) = *((alt_u32*)&pps0[0]);
				reply++;
				sop++;
                break;

			case FREADN:
				/* FIFO Read: do not increment source address */
				sinc = 0;
            case READN:
                /* read n 32bit words, starting at address address */
                /* N is the word following the READ command, the start address 
                 * follows after that
                 *   READN (32bit) N (32bit) ADDRESS (32bit)
                 */
                nitem   = *(sop+1);
                address = *(sop+2);
                if( io2_copy((void*)(reply+1), (void*)address, nitem, sinc, dinc)==0 ) {
                    /* indicate failure */
                    address = ~address;
                    nitem   = 0;
                }
                *reply  = address;
                reply  += (nitem+1);
                /* Update pointer to start of next command */
                sop += 3;
                break;

            case BITWRITEN:
                /* modify n 32bit words from the packet to the range starting at address address,
                 * using mask to update only part of the bits
                 *   BITWRITEN (32bit) N (32bit) ADDRESS (32bit) MASK(32bit) DATA (N*32bit)
                 */
                nitem         = *(sop+1);
                address       = *(sop+2);
				/* set global bitfield mask before entering the apply function!*/
                bitfield_mask = (alt_u32)(*(sop+3));
                data          = (alt_u32*)(sop+4);
                if( apply((alt_u32*)address, data, nitem, bit_write_f)==0 )
                    address = ~address;
                *reply++  = address;
                sop      += (4+nitem);
                break;
                
			case FWRITEN:
				/* FIFO Write: do not increment dest address */
				dinc = 0;
            case WRITEN:
            case ANDN:
            case ORN:
            case XORN:
                /* modify n 32bit words from the packet to the range starting at address address
                 *   WRITEN (32bit) N (32bit) ADDRESS (32bit) DATA (N*32bit)
                 */
                nitem   = *(sop+1);
                address = *(sop+2);
                data    =  (alt_u32*)(sop+3);
                if( ((*sop==WRITEN || *sop==FWRITEN) && io2_copy((void*)address, (void*)data, nitem, sinc, dinc)==0) ||
                    ((*sop==ANDN) && apply((alt_u32*)address, data, nitem, and_f)==0) ||
                    ((*sop==ORN) && apply((alt_u32*)address, data, nitem, or_f)==0) ||
                    ((*sop==XORN) && apply((alt_u32*)address, data, nitem, xor_f)==0) ) {
                    /* indicate failure */
                    address = ~address;
                }
                *reply++ = address; 
                sop     += (nitem+3);
                break;

            case CONFIGW:
                // write a single page of config data. 
                // command format is
                // CONFIGW (32bit) ADDRESS(32bit) DATA(256 bytes)
                address = *(sop+1);
                data    = (alt_u32*)(sop+2);
                if( (*flash_writer)((void*)address, (void*)data)==0 )
                    address = ~address;
                *reply++ = address;
                sop     += (2 + (256/sizeof(alt_u32)));
                break;

            case CONFIGR:
                // read a single page of config data. 
                // command format is
                // CONFIGR (32bit) ADDRESS(32bit) 
                // Skip one word in the reply (for the address or
                // ~address) at which we write the actual data
                address = *(sop+1);
                data    =  (alt_u32*)(reply+1);
                if( (*flash_reader)((void*)data, (void*)address)==0 ) {
                    *reply++ = ~address;
                } else {
                    *reply++  = address;
                    reply    += (256/sizeof(alt_u32));
                }
                // Skip over to the next command in the input sequence
                sop     += 2;
                break;

            case CONFIGE:
                // erase the config sector where the ADDRESS points into
                // command format is
                // CONFIGE (32bit) ADDRESS(32bit) 
                address = *(sop+1);
                if( (*flash_eraser)((void*)address)==0 )
                    address = ~address;
                *reply++ = address;
                sop     += 2;
                break;
                
            default:
                break;
        }
    }
    return (alt_u32)(reply - begin_of_reply);
}



alt_u32 apply(alt_u32* dst, alt_u32* src, unsigned int n, alt_u32 (*f)(alt_u32, alt_u32)) {
    alt_u32 tmp;

    if( (((alt_u32)dst) & 0x3) || (((alt_u32)src) & 0x3) )
		return 0;

    while( n-- ) {
        tmp = IORD(dst, 0);
        tmp = f(tmp, *src);
        IOWR(dst, 0, tmp);
        dst++; src++;
    }
    return 1;
}

int init_flash() {
    if( didInitFlash==42 )
        didInitFlash = alt_epcs_flash_init(&tFlashDev);
    return didInitFlash;
}

alt_u32 init_flash_first_w(void* to, void* from) {
    // depending on wether we succesfully initialized the
    // flash device, set the actual flash-writer function
    // and delegate the actual writing to there
    if( init_flash()<0 ) {
        flash_writer = no_flash_available;
    } else {
        flash_writer = flash_write_page;
    }
    return (*flash_writer)(to, from);
}
alt_u32 init_flash_first_r(void* to, void* from) {
    // depending on wether we succesfully initialized the
    // flash device, set the actual flash-writer function
    // and delegate the actual writing to there
    if( init_flash()<0 ) {
        flash_reader = no_flash_available;
    } else {
        flash_reader = flash_read_page;
    }
    return (*flash_reader)(to, from);
}
alt_u32 init_flash_first_e(void* sectoraddress) {
    // depending on wether we succesfully initialized the
    // flash device, set the actual flash-eraser function
    // and delegate the actual erasing to there
    if( init_flash()<0 ) {
        flash_eraser = no_flash_available1;
    } else {
        flash_eraser = flash_erase_sector;
    }
    return (*flash_eraser)(sectoraddress);
}

// write one page of flashdata to the FLASH-address "to"
// the pages are 256 bytes each
alt_u32 flash_write_page(void* to, void* from) {
    if( alt_epcs_flash_write_block((alt_flash_dev*)&tFlashDev, 0, (int)to, from, 256)<0 )
        return 0;
    return 1;
}
// read one page of flashdata from the FLASH-address "from"
// the pages are 256 bytes each
alt_u32 flash_read_page(void* to, void* from) {
    if( alt_epcs_flash_read((alt_flash_dev*)&tFlashDev, (int)from, to, 256)<0 )
        return 0;
    return 1;
}
// erase the sector that sectoroffset is in
alt_u32 flash_erase_sector(void* sectoroffset) {
    if( alt_epcs_flash_erase_block((alt_flash_dev*)&tFlashDev, (int)sectoroffset)<0 )
        return 0;
    return 1;
}

// the design has no flash - indicate failure
alt_u32 no_flash_available(void* i, void* o) {
    return 0;
}

alt_u32 no_flash_available1(void* i) {
    return no_flash_available(i, (void*)0);
}

