-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Author:
--   D. van der Schuur  May 2012  Original with manual file IO using an editor.
--   E. Kooistra        Feb 2017  Added purpose and description
--                                Added external control by p_mm_stimuli and
--                                p_sim_stimuli
-- Purpose: Testbench for MM and simulation control via file io
-- Description:
--   This testbench verifies mm_file and mm_file_pkg.
--   1) p_mm_stimuli
--     The p_mm_stimuli uses mmf_mm_bus_wr() and mmf_mm_bus_rd() to access a MM
--     slave register instance of common_reg_r_w_dc via mm_file using a MM slave
--     .ctrl and .stat file. The p_mm_stimuli verifies the W/R accesses.
--   2) p_sim_stimuli
--     The p_sim_stimuli waits for get_now and then it uses mmf_sim_get_now() to
--     read the simulator status via mmf_poll_sim_ctrl_file() using a sim.ctrl
--     and sim.stat file. The p_sim_stimuli does not verify read rd_now value,
--     but it does print it.
-- Usage:
--   > as 5
--   > run -all
--   The tb is self stopping and self checking.
--   For example observe mm_mosi, mm_miso, rd_now and out_reg_arr in wave window.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use work.mm_file_pkg.all;

entity tb_mm_file is
  generic (
    g_tb_index           : natural := 0;
    g_mm_nof_accesses    : natural := 100;
    g_mm_timeout         : time := 0 ns;  -- 100 ns;   -- default 0 ns for full speed MM, use > 0 to define number of mm_clk without MM access after which the MM file IO is paused
    g_mm_pause           : time := 1000 ns;  -- defines the time for which MM file IO is paused to reduce the file IO rate when the MM slave is idle
    g_timeout_gap        : integer := -1;  -- 4;    -- no gap when < 0, else force MM access gap after g_timeout_gap wr or rd strobes
    g_cross_clock_domain : boolean := false  -- TRUE
  );
end tb_mm_file;

architecture tb of tb_mm_file is
  constant c_mm_clk_period            : time := c_mmf_mm_clk_period;  -- = 100 ps;
  constant c_mm_nof_dat               : natural := smallest(c_mem_reg_init_w / c_32, g_mm_nof_accesses);
  constant c_mm_rd_latency            : natural := 2;

  constant c_cross_nof_mm_clk         : natural := sel_a_b(g_cross_clock_domain, 100, 0);  -- > 2*24 see common_reg_cross_domain, factor 2 for W/R

  -- Determine node mm_file prefix based on --unb --gn (similar as done in mmf_unb_file_prefix())
  constant c_unb_nr                   : natural := 3;  -- unb
  constant c_pn_nr                    : natural := 1;  -- gn = 0:7
  constant c_node_type                : string(1 to 2) := sel_a_b(c_pn_nr < 4, "FN", "BN");
  constant c_node_nr                  : natural := sel_a_b(c_node_type = "BN", c_pn_nr - 4, c_pn_nr);

  -- Use local mmfiles/ subdirectory in mm project build directory
  constant c_sim_file_pathname        : string := mmf_slave_prefix("TB", g_tb_index) & "sim";
  constant c_reg_r_w_dc_file_pathname : string := mmf_slave_prefix("TB", g_tb_index, "UNB", c_unb_nr, c_node_type, c_node_nr) & "REG_R_W_DC";

  --TYPE t_c_mem IS RECORD
  --  latency   : NATURAL;    -- read latency
  --  adr_w     : NATURAL;
  --  dat_w     : NATURAL;
  --  nof_dat   : NATURAL;    -- optional, nof dat words <= 2**adr_w
  --  init_sl   : STD_LOGIC;  -- optional, init all dat words to std_logic '0', '1' or 'X'
  --  --init_file : STRING;     -- "UNUSED", unconstrained length can not be in record
  --END RECORD;
  constant c_mem_reg            : t_c_mem := (c_mm_rd_latency, ceil_log2(c_mm_nof_dat), c_32, c_mm_nof_dat, '0');

  signal tb_state           : string(1 to 5) := "Init ";
  signal tb_end             : std_logic := '0';
  signal mm_clk             : std_logic := '0';
  signal mm_rst             : std_logic;

  signal get_now            : std_logic := '0';
  signal rd_now             : string(1 to 16);  -- sufficient to fit TIME NOW in ns as a string

  signal mm_mosi            : t_mem_mosi;
  signal mm_miso            : t_mem_miso;
  signal file_wr_data       : std_logic_vector(c_32 - 1 downto 0);
  signal file_rd_data       : std_logic_vector(c_32 - 1 downto 0);

  signal reg_wr_arr         : std_logic_vector(     c_mem_reg.nof_dat - 1 downto 0);
  signal reg_rd_arr         : std_logic_vector(     c_mem_reg.nof_dat - 1 downto 0);
  signal in_new             : std_logic := '1';
  signal in_reg             : std_logic_vector(c_32 * c_mem_reg.nof_dat - 1 downto 0);
  signal out_reg            : std_logic_vector(c_32 * c_mem_reg.nof_dat - 1 downto 0);
  signal out_new            : std_logic;  -- Pulses '1' when new data has been written.

  signal out_reg_arr        : t_slv_32_arr(c_mem_reg.nof_dat - 1 downto 0);
begin
  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 10;

  -- DUT mm access files 'c_reg_r_w_dc_file_pathname'.ctrl and 'c_reg_r_w_dc_file_pathname'.stat
  p_mm_stimuli : process
    variable v_addr : natural;
  begin
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 3);

    -- Write all nof_dat once
    tb_state <= "Write";
    for I in 0 to c_mm_nof_dat - 1 loop
      if I = g_timeout_gap then
        wait for 2 * c_mmf_mm_timeout;
      end if;
      file_wr_data <= TO_UVEC(I, c_32);
      mmf_mm_bus_wr(c_reg_r_w_dc_file_pathname, I, I, mm_clk);
    end loop;

    proc_common_wait_some_cycles(mm_clk, c_cross_nof_mm_clk);

    -- Read all nof_dat once
    tb_state <= "Read ";
    for I in 0 to c_mm_nof_dat - 1 loop
      if I = g_timeout_gap then
        wait for 2 * c_mmf_mm_timeout;
      end if;
      mmf_mm_bus_rd(c_reg_r_w_dc_file_pathname, c_mem_reg.latency, I, file_rd_data, mm_clk);
      assert I = TO_UINT(file_rd_data)
        report "Read data is wrong."
        severity ERROR;
    end loop;

    -- Write/Read
    tb_state <= "Both ";
    for I in 0 to g_mm_nof_accesses - 1 loop
      if I = g_timeout_gap then
        wait for 2 * c_mmf_mm_timeout;
      end if;
      file_wr_data <= TO_UVEC(I, c_32);
      v_addr := I mod c_mm_nof_dat;
      mmf_mm_bus_wr(c_reg_r_w_dc_file_pathname, v_addr, I, mm_clk);
      proc_common_wait_some_cycles(mm_clk, c_cross_nof_mm_clk);
      mmf_mm_bus_rd(c_reg_r_w_dc_file_pathname, c_mem_reg.latency, v_addr, file_rd_data, mm_clk);
      assert TO_UINT(file_wr_data) = TO_UINT(file_rd_data)
        report "Write/read data is wrong."
        severity ERROR;
    end loop;

    proc_common_gen_pulse(mm_clk, get_now);
    tb_state <= "End  ";

    proc_common_wait_some_cycles(mm_clk, g_mm_nof_accesses);
    tb_end <= '1';
    wait;
  end process;

  u_mm_file : entity work.mm_file
  generic map(
    g_file_prefix   => c_reg_r_w_dc_file_pathname,
    g_mm_rd_latency => c_mem_reg.latency,  -- the mm_file g_mm_rd_latency must be >= the MM slave read latency
    g_mm_timeout    => g_mm_timeout,
    g_mm_pause      => g_mm_pause
  )
  port map (
    mm_rst        => mm_rst,
    mm_clk        => mm_clk,

    mm_master_out => mm_mosi,
    mm_master_in  => mm_miso
  );

  -- Target MM reg
  u_reg_r_w_dc : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_in_new_latency     => 0,
    g_readback           => false,
    g_reg                => c_mem_reg
    --g_init_reg           => STD_LOGIC_VECTOR(c_mem_reg_init_w-1 DOWNTO 0) := (OTHERS => '0')
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => mm_rst,
    st_clk      => mm_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => mm_mosi,
    sla_out     => mm_miso,

    -- MM registers in st_clk domain
    reg_wr_arr  => reg_wr_arr,
    reg_rd_arr  => reg_rd_arr,
    in_new      => in_new,
    in_reg      => in_reg,
    out_reg     => out_reg,
    out_new     => out_new
  );

  in_reg <= out_reg;

  p_wire : process(out_reg)
  begin
    for I in c_mem_reg.nof_dat - 1 downto 0 loop
      out_reg_arr(I) <= out_reg((I + 1) * c_32 - 1 downto I * c_32);
    end loop;
  end process;

  -- Also verify simulation status access
  mmf_poll_sim_ctrl_file(mm_clk, c_sim_file_pathname & ".ctrl", c_sim_file_pathname & ".stat");

  p_sim_stimuli : process
  begin
    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    proc_common_wait_until_hi_lo(mm_clk, get_now);
    mmf_sim_get_now(c_sim_file_pathname, rd_now, mm_clk);
    wait;
  end process;
end tb;
