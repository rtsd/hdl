
namespace eval ip_arria10_e1sg_ddr4_8g_2400 {
  proc get_design_libraries {} {
    set libraries [dict create]
    dict set libraries altera_emif_arch_nf_180             1
    dict set libraries altera_avalon_mm_bridge_180         1
    dict set libraries altera_avalon_onchip_memory2_180    1
    dict set libraries altera_merlin_master_translator_180 1
    dict set libraries altera_merlin_slave_translator_180  1
    dict set libraries altera_mm_interconnect_180          1
    dict set libraries altera_reset_controller_180         1
    dict set libraries altera_emif_cal_slave_nf_180        1
    dict set libraries altera_emif_180                     1
    dict set libraries ip_arria10_e1sg_ddr4_8g_2400        1
    return $libraries
  }
  
  proc get_memory_files {QSYS_SIMDIR} {
    set memory_files [list]
    lappend memory_files "$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_seq_params_sim.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_seq_params_synth.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_seq_cal.hex"
    lappend memory_files "$QSYS_SIMDIR/../altera_avalon_onchip_memory2_180/sim/seq_cal_soft_m20k.hex"
    return $memory_files
  }
  
  proc get_common_design_files {USER_DEFINED_COMPILE_OPTIONS USER_DEFINED_VERILOG_COMPILE_OPTIONS USER_DEFINED_VHDL_COMPILE_OPTIONS QSYS_SIMDIR} {
    set design_files [dict create]
    return $design_files
  }
  
  proc get_design_files {USER_DEFINED_COMPILE_OPTIONS USER_DEFINED_VERILOG_COMPILE_OPTIONS USER_DEFINED_VHDL_COMPILE_OPTIONS QSYS_SIMDIR} {
    set design_files [list]
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_top.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                               
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i_io_aux.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                            
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_oct.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                        
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_df_o.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                              
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_bdir_df.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_bdir_se.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_cp_i.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                              
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_df_i.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                              
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_se_i.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                              
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_udir_se_o.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                              
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_core_clks_rsts.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                             
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_io_tiles.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                   
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_io_tiles_abphy.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                             
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_pll.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                        
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/twentynm_io_12_lane_abphy.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                      
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/twentynm_io_12_lane_encrypted_abphy.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                            
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/twentynm_io_12_lane_nf5es_encrypted_abphy.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                      
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_bufs.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                       
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_buf_unused.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                 
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_cal_counter.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_pll_fast_sim.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                               
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_pll_extra_clks.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                             
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hps_clks_rsts.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                              
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_io_tiles_wrap.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                              
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_abphy_mux.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                  
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_avl_if.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                 
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_sideband_if.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                            
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_mmr_if.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                 
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_amm_data_if.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                            
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_hmc_ast_data_if.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                            
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_afi_if.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                     
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_seq_if.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                     
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_emif_arch_nf_regs.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                       
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_oct.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                                     
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_oct_um_fsm.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                              
    lappend design_files "xmvlog $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/altera_std_synchronizer_nocut.v\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                       
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/mem_array_abphy.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                                
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/io_12_lane_bcm__nf5es_abphy.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                    
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/io_12_lane__nf5es_abphy.sv\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                                                        
    lappend design_files "xmvhdl -v93 $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_arch_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_arch_nf_180_e37lt4i.vhd\"  -work altera_emif_arch_nf_180 -cdslib  ./cds_libs/altera_emif_arch_nf_180.cds.lib"                                    
    lappend design_files "xmvlog $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_avalon_mm_bridge_180/sim/altera_avalon_mm_bridge.v\"  -work altera_avalon_mm_bridge_180 -cdslib  ./cds_libs/altera_avalon_mm_bridge_180.cds.lib"                                                                 
    lappend design_files "xmvhdl -v93 $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_avalon_onchip_memory2_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_avalon_onchip_memory2_180_xymx6za.vhd\"  -work altera_avalon_onchip_memory2_180 -cdslib  ./cds_libs/altera_avalon_onchip_memory2_180.cds.lib"
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_merlin_master_translator_180/sim/altera_merlin_master_translator.sv\"  -work altera_merlin_master_translator_180 -cdslib  ./cds_libs/altera_merlin_master_translator_180.cds.lib"                            
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_merlin_slave_translator_180/sim/cadence/altera_merlin_slave_translator.sv\"  -work altera_merlin_slave_translator_180 -cdslib  ./cds_libs/altera_merlin_slave_translator_180.cds.lib"                        
    lappend design_files "xmvhdl -v93 $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_mm_interconnect_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_mm_interconnect_180_7km4trq.vhd\"  -work altera_mm_interconnect_180"                                                                               
    lappend design_files "xmvlog $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_reset_controller_180/sim/cadence/altera_reset_controller.v\"  -work altera_reset_controller_180 -cdslib  ./cds_libs/altera_reset_controller_180.cds.lib"                                                         
    lappend design_files "xmvlog $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_reset_controller_180/sim/cadence/altera_reset_synchronizer.v\"  -work altera_reset_controller_180 -cdslib  ./cds_libs/altera_reset_controller_180.cds.lib"                                                       
    lappend design_files "xmvhdl -v93 $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_cal_slave_nf_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_cal_slave_nf_180_efslyyq.vhd\"  -work altera_emif_cal_slave_nf_180"                                                                         
    lappend design_files "xmvhdl -v93 $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_emif_180/sim/ip_arria10_e1sg_ddr4_8g_2400_altera_emif_180_nz3mdxa.vhd\"  -work altera_emif_180"                                                                                                                
    lappend design_files "xmvhdl -v93 $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/ip_arria10_e1sg_ddr4_8g_2400.vhd\"  -work ip_arria10_e1sg_ddr4_8g_2400"                                                                                                                                                  
    return $design_files
  }
  
  proc get_elab_options {SIMULATOR_TOOL_BITNESS} {
    set ELAB_OPTIONS ""
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ELAB_OPTIONS
  }
  
  
  proc get_sim_options {SIMULATOR_TOOL_BITNESS} {
    set SIM_OPTIONS ""
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $SIM_OPTIONS
  }
  
  
  proc get_env_variables {SIMULATOR_TOOL_BITNESS} {
    set ENV_VARIABLES [dict create]
    set LD_LIBRARY_PATH [dict create]
    dict set ENV_VARIABLES "LD_LIBRARY_PATH" $LD_LIBRARY_PATH
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ENV_VARIABLES
  }
  
  
}
