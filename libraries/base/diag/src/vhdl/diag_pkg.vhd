-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;

package diag_pkg is
  -----------------------------------------------------------------------------
  -- PHY interface tests (e.g. for ethernet, transceivers, lvds, memory)
  -----------------------------------------------------------------------------

  constant c_diag_test_mode_no_tst      : natural := 0;  -- no test, the PHY interface runs in normal user mode
  constant c_diag_test_mode_loop_local  : natural := 1;  -- loop back via PHY chip
  constant c_diag_test_mode_loop_remote : natural := 2;  -- loop back via loopback cable or plug in the connector
  constant c_diag_test_mode_tx          : natural := 4;  -- transmit only
  constant c_diag_test_mode_rx          : natural := 5;  -- receive only
  constant c_diag_test_mode_tx_rx       : natural := 6;  -- transmit and receive

  constant c_diag_test_data_lfsr        : natural := 0;  -- use pseudo random data
  constant c_diag_test_data_incr        : natural := 1;  -- use incrementing counter data

  constant c_diag_test_duration_quick   : natural := 0;  -- end Rx test after 1 data frame or word, end Tx test after correspondingly sufficient data frames or words transmitted, or all memory lines
  constant c_diag_test_duration_normal  : natural := 1;  -- idem for e.g. 100 data frames or words, or full memory
  constant c_diag_test_duration_extra   : natural := 2;  -- idem for e.g. 100000 data frames or words

  constant c_diag_test_result_ok        : natural := 0;  -- test went OK
  constant c_diag_test_result_none      : natural := 1;  -- test did not run, default
  constant c_diag_test_result_timeout   : natural := 2;  -- test started but no valid data was received
  constant c_diag_test_result_error     : natural := 3;  -- test received valid data, but the value was wrong for one or more
  constant c_diag_test_result_illegal   : natural := 4;  -- exception, condition that can not occur in the logic

  -----------------------------------------------------------------------------
  -- Waveform Generator
  -----------------------------------------------------------------------------

  -- control register
  constant c_diag_wg_mode_w             : natural :=  8;
  constant c_diag_wg_nofsamples_w       : natural := 16;  -- >~ minimum data path block size
  constant c_diag_wg_phase_w            : natural := 16;  -- =  c_diag_wg_nofsamples_w
  constant c_diag_wg_freq_w             : natural := 31;  -- >> c_diag_wg_nofsamples_w, determines the minimum frequency = Fs / 2**c_diag_wg_freq_w
  constant c_diag_wg_ampl_w             : natural := 17;  -- Typically fit DSP multiply 18x18 element so use <= 17, to fit unsigned in 18 bit signed,
                                                          -- = waveform data width-1 (sign bit) to be able to make a 1 LSBit amplitude sinus

  constant c_diag_wg_mode_off           : natural := 0;
  constant c_diag_wg_mode_calc          : natural := 1;
  constant c_diag_wg_mode_repeat        : natural := 2;
  constant c_diag_wg_mode_single        : natural := 3;

  type t_diag_wg is record
    mode        : std_logic_vector(c_diag_wg_mode_w       - 1 downto 0);
    nof_samples : std_logic_vector(c_diag_wg_nofsamples_w - 1 downto 0);  -- unsigned value
    phase       : std_logic_vector(c_diag_wg_phase_w      - 1 downto 0);  -- unsigned value
    freq        : std_logic_vector(c_diag_wg_freq_w       - 1 downto 0);  -- unsigned value
    ampl        : std_logic_vector(c_diag_wg_ampl_w       - 1 downto 0);  -- unsigned value, range [0:2**c_diag_wg_ampl_w> normalized to range [0 c_diag_wg_gain>
  end record;

  constant c_diag_wg_ampl_norm          : real := 1.0;  -- Use this default amplitude norm = 1.0 when WG data width = WG waveform buffer data width,
                                                         -- else use extra amplitude unit scaling by (WG data max)/(WG data max + 1)
  constant c_diag_wg_gain_w             : natural := 1;  -- Normalized range [0 1>  maps to fixed point range [0:2**c_diag_wg_ampl_w>
                                                         -- . use gain 2**0             = 1 to have fulle scale without clipping
                                                         -- . use gain 2**g_calc_gain_w > 1 to cause clipping
                                                         -- For c_diag_wg_gain_w = 1 there is clipping from [1 2> For normalized values >= 2**c_diag_wg_gain_w = 2
                                                         -- the behaviour becomes more or less undefined. Due to wrapping it appears that normalized values [2 3>
                                                         -- result in a sinus again. Therefore use normalized range [0 2**c_diag_wg_gain_w>.
  constant c_diag_wg_ampl_unit          : real := 2**REAL(c_diag_wg_ampl_w - c_diag_wg_gain_w) * c_diag_wg_ampl_norm;  -- ^= Full Scale range [-c_wg_full_scale +c_wg_full_scale] without clipping
  constant c_diag_wg_freq_unit          : real := 2**REAL(c_diag_wg_freq_w);  -- ^= c_clk_freq = Fs (sample frequency), assuming one sinus waveform in the buffer
  constant c_diag_wg_phase_unit         : real := 2**REAL(c_diag_wg_phase_w) / 360.0;  -- ^= 1 degree

  constant c_diag_wg_latency            : natural := 10;  -- WG starts 10 cycles after trigger
  constant c_diag_wg_rst : t_diag_wg := (TO_UVEC(c_diag_wg_mode_off, c_diag_wg_mode_w),
                                         TO_UVEC(              1024, c_diag_wg_nofsamples_w),
                                         TO_UVEC(                 0, c_diag_wg_phase_w),
                                         TO_UVEC(                 0, c_diag_wg_freq_w),
                                         TO_UVEC(                 0, c_diag_wg_ampl_w));

  type t_diag_wg_arr is array (integer range <>) of t_diag_wg;

  -----------------------------------------------------------------------------
  -- Block Generator
  -----------------------------------------------------------------------------

  -- control register
  constant c_diag_bg_reg_nof_dat  : natural := 8;
  constant c_diag_bg_reg_adr_w    : natural := ceil_log2(c_diag_bg_reg_nof_dat);
  constant c_diag_bg_reg_adr_span : natural := 2**c_diag_bg_reg_adr_w;

  -- Use c_diag_bg_gapsize_w = 31 to fit gapsize in 31 bit NATURAL. At 200 MHz
  -- clock this allows a gap of 2**31 / 200e6 = 10.7 s
  constant c_diag_bg_mode_w               : natural :=  8;
  constant c_diag_bg_samples_per_packet_w : natural := 24;
  constant c_diag_bg_blocks_per_sync_w    : natural := 24;
  constant c_diag_bg_gapsize_w            : natural := 31;
  constant c_diag_bg_gapsize_max          : natural := c_natural_high;  -- = 2147483647 = 2**31 - 1 = NATURAL'HIGH
  constant c_diag_bg_mem_adrs_w           : natural := 24;
  constant c_diag_bg_mem_low_adrs_w       : natural := c_diag_bg_mem_adrs_w;
  constant c_diag_bg_mem_high_adrs_w      : natural := c_diag_bg_mem_adrs_w;
  constant c_diag_bg_mem_max_adr          : natural := 2**c_diag_bg_mem_adrs_w - 1;
  constant c_diag_bg_bsn_init_w           : natural := 64;

  type t_diag_block_gen is record
    enable             : std_logic;  -- block enable
    enable_sync        : std_logic;  -- block enable on sync pulse
    samples_per_packet : std_logic_vector(c_diag_bg_samples_per_packet_w - 1 downto 0);
    blocks_per_sync    : std_logic_vector(c_diag_bg_blocks_per_sync_w    - 1 downto 0);
    gapsize            : std_logic_vector(c_diag_bg_gapsize_w            - 1 downto 0);
    mem_low_adrs       : std_logic_vector(c_diag_bg_mem_low_adrs_w       - 1 downto 0);
    mem_high_adrs      : std_logic_vector(c_diag_bg_mem_high_adrs_w      - 1 downto 0);
    bsn_init           : std_logic_vector(c_diag_bg_bsn_init_w           - 1 downto 0);
  end record;

  type t_diag_block_gen_integer is record
    enable             : std_logic;
    enable_sync        : std_logic;
    samples_per_packet : natural;
    blocks_per_sync    : natural;
    gapsize            : natural;
    mem_low_adrs       : natural;
    mem_high_adrs      : natural;
    bsn_init           : natural;
  end record;

  constant c_diag_block_gen_rst     : t_diag_block_gen := (         '0',
                                                                    '0',
                                                           TO_UVEC( 256, c_diag_bg_samples_per_packet_w),
                                                           TO_UVEC(  10, c_diag_bg_blocks_per_sync_w),
                                                           TO_UVEC( 128, c_diag_bg_gapsize_w),
                                                           TO_UVEC(   0, c_diag_bg_mem_low_adrs_w),
                                                           TO_UVEC(   1, c_diag_bg_mem_high_adrs_w),
                                                           TO_UVEC(   0, c_diag_bg_bsn_init_w));

  constant c_diag_block_gen_enabled : t_diag_block_gen := (         '1',
                                                                    '0',
                                                           TO_UVEC(  50, c_diag_bg_samples_per_packet_w),
                                                           TO_UVEC(  10, c_diag_bg_blocks_per_sync_w),
                                                           TO_UVEC(   7, c_diag_bg_gapsize_w),
                                                           TO_UVEC(   0, c_diag_bg_mem_low_adrs_w),
                                                           TO_UVEC(  15, c_diag_bg_mem_high_adrs_w),  -- fits any BG buffer that has address width >= 4
                                                           TO_UVEC(   0, c_diag_bg_bsn_init_w));

  type t_diag_block_gen_arr is array (integer range <>) of t_diag_block_gen;
  type t_diag_block_gen_integer_arr is array (integer range <>) of t_diag_block_gen_integer;

  -- Overloaded sel_a_b (from common_pkg) for t_diag_block_gen
  function sel_a_b(sel : boolean; a, b : t_diag_block_gen) return t_diag_block_gen;

  function func_diag_bg_ctrl_integer_to_slv(bg_ctrl_int : t_diag_block_gen_integer) return t_diag_block_gen;
  function func_diag_bg_ctrl_slv_to_integer(bg_ctrl_slv : t_diag_block_gen) return t_diag_block_gen_integer;

  -----------------------------------------------------------------------------
  -- Data buffer
  -----------------------------------------------------------------------------
  constant c_diag_db_reg_nof_dat : natural := 2;
  constant c_diag_db_reg_adr_w   : natural := ceil_log2(c_diag_db_reg_nof_dat);

  constant c_diag_db_max_data_w  : natural := 32;

  type t_diag_data_type_enum is (
    e_data,
    e_complex,  -- im & re
    e_real,
    e_imag
  );

  -----------------------------------------------------------------------------
  -- Data buffer dev
  -----------------------------------------------------------------------------
  constant c_diag_db_dev_reg_nof_dat : natural := 8;  -- Create headroom of 4 registers.
  constant c_diag_db_dev_reg_adr_w   : natural := ceil_log2(c_diag_db_dev_reg_nof_dat);

  -----------------------------------------------------------------------------
  -- CNTR / PSRG sequence test data
  -----------------------------------------------------------------------------

  constant c_diag_seq_tx_reg_nof_dat      : natural := 4;
  constant c_diag_seq_tx_reg_adr_w        : natural := ceil_log2(c_diag_seq_tx_reg_nof_dat);
  constant c_diag_seq_rx_reg_nof_steps_wi : natural := 4;
  constant c_diag_seq_rx_reg_nof_steps    : natural := 4;
  constant c_diag_seq_rx_reg_nof_dat      : natural := c_diag_seq_rx_reg_nof_steps_wi + c_diag_seq_rx_reg_nof_steps;
  constant c_diag_seq_rx_reg_adr_w        : natural := ceil_log2(c_diag_seq_rx_reg_nof_dat);

  -- Record with all diag seq MM register fields
  type t_diag_seq_mm_reg is record
    -- readback control
    tx_init   : std_logic_vector(c_word_w - 1 downto 0);
    tx_mod    : std_logic_vector(c_word_w - 1 downto 0);
    tx_ctrl   : std_logic_vector(c_word_w - 1 downto 0);
    rx_ctrl   : std_logic_vector(c_word_w - 1 downto 0);
    rx_steps  : t_integer_arr(c_diag_seq_rx_reg_nof_steps - 1 downto 0);
    -- read only status
    tx_cnt    : std_logic_vector(c_word_w - 1 downto 0);
    rx_cnt    : std_logic_vector(c_word_w - 1 downto 0);
    rx_stat   : std_logic_vector(c_word_w - 1 downto 0);
    rx_sample : std_logic_vector(c_word_w - 1 downto 0);
  end record;

  constant c_diag_seq_tx_reg_dis        : natural := 0;
  constant c_diag_seq_tx_reg_en_psrg    : natural := 1;
  constant c_diag_seq_tx_reg_en_cntr    : natural := 3;

  type t_diag_seq_mm_reg_arr is array (integer range <>) of t_diag_seq_mm_reg;
end diag_pkg;

package body diag_pkg is
  function sel_a_b(sel : boolean; a, b : t_diag_block_gen) return t_diag_block_gen is
  begin
    if sel = true then
      return a;
    else
      return b;
    end if;

  end;

  function func_diag_bg_ctrl_integer_to_slv(bg_ctrl_int : t_diag_block_gen_integer) return t_diag_block_gen is
  begin
    return (        bg_ctrl_int.enable,
                    bg_ctrl_int.enable_sync,
            TO_UVEC(bg_ctrl_int.samples_per_packet, c_diag_bg_samples_per_packet_w),
            TO_UVEC(bg_ctrl_int.blocks_per_sync,    c_diag_bg_blocks_per_sync_w),
            TO_UVEC(bg_ctrl_int.gapsize,            c_diag_bg_gapsize_w),
            TO_UVEC(bg_ctrl_int.mem_low_adrs,       c_diag_bg_mem_low_adrs_w),
            TO_UVEC(bg_ctrl_int.mem_high_adrs,      c_diag_bg_mem_high_adrs_w),
            TO_UVEC(bg_ctrl_int.bsn_init,           c_diag_bg_bsn_init_w));
  end;

  function func_diag_bg_ctrl_slv_to_integer(bg_ctrl_slv : t_diag_block_gen) return t_diag_block_gen_integer is
  begin
    return (        bg_ctrl_slv.enable,
                    bg_ctrl_slv.enable_sync,
            TO_UINT(bg_ctrl_slv.samples_per_packet),
            TO_UINT(bg_ctrl_slv.blocks_per_sync),
            TO_UINT(bg_ctrl_slv.gapsize),
            TO_UINT(bg_ctrl_slv.mem_low_adrs),
            TO_UINT(bg_ctrl_slv.mem_high_adrs),
            TO_UINT(bg_ctrl_slv.bsn_init));
  end;
end diag_pkg;
