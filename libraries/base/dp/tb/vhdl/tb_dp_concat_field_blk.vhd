-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Test bench for dp_concat_field_blk and dp_offload_rx
-- Description:
--                 u_tx                     u_rx
--                 ___________________      ___________________
--                |dp_concat_field_blk|    |dp_offload_rx      |
-- stimuli_src -->|                   |--->|                   |--> verify_snk
--                | in            out | |  | in            out |
--                |___________________| |  |___________________|
--                                      |
--                               link_offload_sosi
-- Usage:
-- > as 10
-- > run -all
--

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_concat_field_blk is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_pulse;  -- always e_active, e_random or e_pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    -- specific
    g_data_w                 : natural := 64;
    g_nof_repeat             : natural := 100;
    g_pkt_len                : natural := 240;
    g_pkt_gap                : natural := 16
  );
end tb_dp_concat_field_blk;

architecture tb of tb_dp_concat_field_blk is
  constant c_mm_clk_period : time := 1 ns;
  constant c_dp_clk_period : time := 5 ns;

  -- dp_stream_stimuli
  constant c_stimuli_pulse_active     : natural := 3;
  constant c_stimuli_pulse_period     : natural := 4;

  -- dp_stream_verify
  constant c_verify_pulse_active      : natural := 1;
  constant c_verify_pulse_period      : natural := 5;

  constant c_data_max                 : unsigned(g_data_w - 1 downto 0) := (others => '1');
  constant c_dsp_max                  : unsigned(g_data_w - 1 downto 0) := (others => '1');

  --CONSTANT c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_rst;  -- default 0 is no wrap
  constant c_verify_snk_in_cnt_max    : t_dp_sosi_unsigned := TO_DP_SOSI_UNSIGNED('0', '0', '0', '0', c_data_max, c_dsp_max, c_dsp_max, c_unsigned_0, c_unsigned_0, c_unsigned_0, c_unsigned_0);
  constant c_verify_snk_in_cnt_gap    : t_dp_sosi_unsigned := c_dp_sosi_unsigned_ones;  -- default only accept increment +1

  constant c_expected_pkt_len         : natural := g_pkt_len;
  constant c_sync_period              : natural := 5;
  constant c_sync_offset              : natural := 2;

  constant c_hdr_len                  : natural := 7;
  constant c_wait_last_evt            : natural := 100 + g_nof_repeat * c_hdr_len;

  -----------------------------------------------------------------------------
  -- Tx offload
  -----------------------------------------------------------------------------
  -- From apertif_udp_offload_pkg.vhd:
  constant c_udp_offload_nof_hdr_fields : natural := 3 + 12 + 4 + 3;  -- 448b; 7 64b words
  -- Notes:
  -- . pre-calculated ip_header_checksum is valid only for UNB0, FN0 targeting IP 10.10.10.10
  -- . udp_total_length = 176 beamlets * 64b / 8b = 1408B + 14 DP bytes + 8 UDP bytes = 1430B
  constant c_udp_offload_hdr_field_arr : t_common_field_arr(c_udp_offload_nof_hdr_fields - 1 downto 0) := (
         ( field_name_pad("eth_dst_mac"            ), "RW", 48, field_default(x"001B214368AC") ),
         ( field_name_pad("eth_src_mac"            ), "RW", 48, field_default(0) ),
         ( field_name_pad("eth_type"               ), "RW", 16, field_default(x"0800") ),
         ( field_name_pad("ip_version"             ), "RW",  4, field_default(4) ),
         ( field_name_pad("ip_header_length"       ), "RW",  4, field_default(5) ),
         ( field_name_pad("ip_services"            ), "RW",  8, field_default(0) ),
         ( field_name_pad("ip_total_length"        ), "RW", 16, field_default(1450) ),
         ( field_name_pad("ip_identification"      ), "RW", 16, field_default(0) ),
         ( field_name_pad("ip_flags"               ), "RW",  3, field_default(2) ),
         ( field_name_pad("ip_fragment_offset"     ), "RW", 13, field_default(0) ),
         ( field_name_pad("ip_time_to_live"        ), "RW",  8, field_default(127) ),
         ( field_name_pad("ip_protocol"            ), "RW",  8, field_default(17) ),
         ( field_name_pad("ip_header_checksum"     ), "RW", 16, field_default(29928) ),
         ( field_name_pad("ip_src_addr"            ), "RW", 32, field_default(x"C0A80009") ),
         ( field_name_pad("ip_dst_addr"            ), "RW", 32, field_default(x"C0A80001") ),
         ( field_name_pad("udp_src_port"           ), "RW", 16, field_default(0) ),
         ( field_name_pad("udp_dst_port"           ), "RW", 16, field_default(0) ),
         ( field_name_pad("udp_total_length"       ), "RW", 16, field_default(1430) ),
         ( field_name_pad("udp_checksum"           ), "RW", 16, field_default(0) ),
         ( field_name_pad("dp_reserved"            ), "RW", 47, field_default(0) ),
         ( field_name_pad("dp_sync"                ), "RW",  1, field_default(0) ),
         ( field_name_pad("dp_bsn"                 ), "RW", 64, field_default(0) ) );

  -- From apertif_unb1_fn_beamformer_udp_offload.vhd:
  -- Override ('1') only the Ethernet fields so we can use MM defaults there.
  constant c_hdr_field_ovr_init : std_logic_vector(c_udp_offload_nof_hdr_fields - 1 downto 0) := "101" & "111111111111" & "1111" & "100";

  constant c_NODE_ID                    : std_logic_vector(7 downto 0) := TO_UVEC(0, 8);

  signal id_backplane                   : std_logic_vector(c_byte_w - 1 downto 0);
  signal id_chip                        : std_logic_vector(c_byte_w - 1 downto 0);

  signal dp_fifo_sc_src_in              : t_dp_siso := c_dp_siso_rdy;
  signal dp_fifo_sc_src_out             : t_dp_sosi;

  signal dp_offload_tx_snk_in_arr       : t_dp_sosi_arr(0 downto 0);
  signal dp_offload_tx_snk_out_arr      : t_dp_siso_arr(0 downto 0);

  signal tx_hdr_fields_in_arr           : t_slv_1024_arr(0 downto 0);

  signal reg_dp_offload_tx_hdr_dat_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_offload_tx_hdr_dat_miso : t_mem_miso;

  -----------------------------------------------------------------------------
  -- Link
  -----------------------------------------------------------------------------

  signal tx_offload_sosi_arr       : t_dp_sosi_arr(0 downto 0);
  signal tx_offload_siso_arr       : t_dp_siso_arr(0 downto 0);

  signal link_offload_sosi_arr     : t_dp_sosi_arr(0 downto 0);
  signal link_offload_siso_arr     : t_dp_siso_arr(0 downto 0);

  -----------------------------------------------------------------------------
  -- Rx offload
  -----------------------------------------------------------------------------
  signal dp_offload_rx_src_out_arr      : t_dp_sosi_arr(0 downto 0);
  signal dp_offload_rx_src_in_arr       : t_dp_siso_arr(0 downto 0);

  signal rx_hdr_fields_out_arr          : t_slv_1024_arr(0 downto 0);
  signal rx_hdr_fields_raw_arr          : t_slv_1024_arr(0 downto 0);

  signal reg_dp_offload_rx_hdr_dat_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_offload_rx_hdr_dat_miso : t_mem_miso;

  -----------------------------------------------------------------------------
  -- Test
  -----------------------------------------------------------------------------
  signal mm_clk                     : std_logic := '1';
  signal mm_rst                     : std_logic := '1';
  signal dp_clk                     : std_logic := '1';
  signal dp_rst                     : std_logic := '1';
  signal tb_end                     : std_logic := '0';

  signal stimuli_src_in             : t_dp_siso := c_dp_siso_rdy;
  signal stimuli_src_out            : t_dp_sosi;
  signal stimuli_src_out_data       : std_logic_vector(g_data_w - 1 downto 0);

  signal verify_snk_in_enable       : t_dp_sosi_sl := c_dp_sosi_sl_rst;
  signal last_snk_in                : t_dp_sosi;
  signal last_snk_in_evt            : std_logic;
  signal verify_last_snk_in_evt     : t_dp_sosi_sl := c_dp_sosi_sl_rst;

  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal verify_snk_in_data         : std_logic_vector(g_data_w - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  mm_clk <= (not mm_clk) or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 7;
  dp_clk <= (not dp_clk) or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 7;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  u_dp_stream_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 15,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_stimuli_pulse_active,
    g_pulse_period   => c_stimuli_pulse_period,
    g_flow_control   => g_flow_control_stimuli,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    -- specific
    g_in_dat_w       => g_data_w,
    g_nof_repeat     => g_nof_repeat,
    g_pkt_len        => g_pkt_len,
    g_pkt_gap        => g_pkt_gap,
    g_wait_last_evt  => c_wait_last_evt
  )
  port map (
    rst                 => dp_rst,
    clk                 => dp_clk,

    -- Generate stimuli
    src_in              => stimuli_src_in,
    src_out             => stimuli_src_out,

    -- End of stimuli
    last_snk_in         => last_snk_in,  -- expected verify_snk_in after end of stimuli
    last_snk_in_evt     => last_snk_in_evt,  -- trigger verify to verify the last_snk_in
    tb_end              => tb_end  -- signal end of tb as far as this dp_stream_stimuli is concerned
  );

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Select fields that need to be verified
  -- . during the test
  verify_snk_in_enable.sync    <= '1';
  verify_snk_in_enable.bsn     <= '1';
  verify_snk_in_enable.data    <= '1';
  verify_snk_in_enable.re      <= '0';
  verify_snk_in_enable.im      <= '0';
  verify_snk_in_enable.valid   <= '1';
  verify_snk_in_enable.sop     <= '1';
  verify_snk_in_enable.eop     <= '1';
  verify_snk_in_enable.empty   <= '0';
  verify_snk_in_enable.channel <= '0';
  verify_snk_in_enable.err     <= '0';

  -- . after the test
  verify_last_snk_in_evt.sync    <= last_snk_in_evt;
  verify_last_snk_in_evt.bsn     <= last_snk_in_evt;  -- thanks to using rx_hdr_fields_raw_arr for bsn field
  verify_last_snk_in_evt.data    <= last_snk_in_evt;
  verify_last_snk_in_evt.re      <= '0';
  verify_last_snk_in_evt.im      <= '0';
  verify_last_snk_in_evt.valid   <= last_snk_in_evt;
  verify_last_snk_in_evt.sop     <= last_snk_in_evt;
  verify_last_snk_in_evt.eop     <= last_snk_in_evt;
  verify_last_snk_in_evt.empty   <= '0';
  verify_last_snk_in_evt.channel <= '0';
  verify_last_snk_in_evt.err     <= '0';

  u_dp_stream_verify : entity work.dp_stream_verify
  generic map (
    g_instance_nr    => 0,  -- only one stream so choose index 0
    -- flow control
    g_random_w       => 14,  -- use different random width for stimuli and for verify to have different random sequences
    g_pulse_active   => c_verify_pulse_active,
    g_pulse_period   => c_verify_pulse_period,
    g_flow_control   => g_flow_control_verify,  -- always active, random or pulse flow control
    -- initializations
    g_sync_period    => c_sync_period,
    g_sync_offset    => c_sync_offset,
    g_snk_in_cnt_max => c_verify_snk_in_cnt_max,
    g_snk_in_cnt_gap => c_verify_snk_in_cnt_gap,
    -- specific
    g_in_dat_w       => g_data_w,
    g_pkt_len        => c_expected_pkt_len
  )
  port map (
    rst                        => dp_rst,
    clk                        => dp_clk,

    -- Verify data
    snk_out                    => verify_snk_out,
    snk_in                     => verify_snk_in,

    -- During stimuli
    verify_snk_in_enable       => verify_snk_in_enable,  -- enable verify to verify that the verify_snk_in fields are incrementing

    -- End of stimuli
    expected_snk_in            => last_snk_in,  -- expected verify_snk_in after end of stimuli
    verify_expected_snk_in_evt => verify_last_snk_in_evt  -- trigger verify to verify the last_snk_in
  );

  ------------------------------------------------------------------------------
  -- DUT offload Tx
  ------------------------------------------------------------------------------
  stimuli_src_in <= c_dp_siso_rdy;

  -- Use FIFO to mimic apertif_unb1_fn_beamformer_udp_offload.vhd, without FIFO dp_stream_stimuli
  -- would handle the back pressure
  u_dp_fifo_sc : entity work.dp_fifo_sc
  generic map (
    g_data_w         => g_data_w,
    g_bsn_w          => 64,
    g_use_sync       => true,
    g_use_bsn        => true,
    g_fifo_size      => 1024
  )
  port map (
    rst         => dp_rst,
    clk         => dp_clk,

    snk_out     => OPEN,  -- stimuli_src_in
    snk_in      => stimuli_src_out,

    src_in      => dp_fifo_sc_src_in,
    src_out     => dp_fifo_sc_src_out
  );

  dp_offload_tx_snk_in_arr(0) <= dp_fifo_sc_src_out;
  dp_fifo_sc_src_in           <= dp_offload_tx_snk_out_arr(0);

  -- Extract the chip and backplane numbers from c_NODE_ID
  id_backplane <= RESIZE_UVEC(c_NODE_ID(7 downto 3), c_byte_w);
  id_chip      <= RESIZE_UVEC(c_NODE_ID(2 downto 0), c_byte_w);

  -- Wire the hardwired header fields to DP signals and c_NODE_ID
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "eth_src_mac" ) downto field_lo(c_udp_offload_hdr_field_arr, "eth_src_mac"     )) <= x"00228608" & id_backplane & id_chip;
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "udp_src_port") downto field_lo(c_udp_offload_hdr_field_arr, "udp_src_port"    )) <= x"D0" & c_NODE_ID;
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "udp_dst_port") downto field_lo(c_udp_offload_hdr_field_arr, "udp_dst_port"    )) <= x"D0" & c_NODE_ID;
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "ip_src_addr" ) downto field_lo(c_udp_offload_hdr_field_arr, "ip_src_addr"     )) <= x"0A63" & id_backplane & INCR_UVEC(id_chip, 1);

  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "dp_sync"     ) downto field_lo(c_udp_offload_hdr_field_arr, "dp_sync"         )) <= slv(dp_offload_tx_snk_in_arr(0).sync);
  tx_hdr_fields_in_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "dp_bsn"      ) downto field_lo(c_udp_offload_hdr_field_arr, "dp_bsn"          )) <=     dp_offload_tx_snk_in_arr(0).bsn(63 downto 0);

  u_tx : entity work.dp_concat_field_blk
  generic map (
    g_nof_streams    => 1,
    g_data_w         => g_data_w,
    g_symbol_w       => g_data_w,
    g_hdr_field_arr  => c_udp_offload_hdr_field_arr,
    g_hdr_field_sel  => c_hdr_field_ovr_init
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    reg_hdr_dat_mosi      => reg_dp_offload_tx_hdr_dat_mosi,
    reg_hdr_dat_miso      => reg_dp_offload_tx_hdr_dat_miso,

    snk_in_arr            => dp_offload_tx_snk_in_arr,
    snk_out_arr           => dp_offload_tx_snk_out_arr,

    src_out_arr           => tx_offload_sosi_arr,
    src_in_arr            => tx_offload_siso_arr,

    hdr_fields_in_arr     => tx_hdr_fields_in_arr
  );

  ------------------------------------------------------------------------------
  -- Link
  ------------------------------------------------------------------------------

  p_link_offload : process(tx_offload_sosi_arr, link_offload_siso_arr)
  begin
    -- Model active packet fields of tr_10GbE Rx sosi output interface
    link_offload_sosi_arr(0)       <= c_dp_sosi_rst;
    link_offload_sosi_arr(0).data  <= tx_offload_sosi_arr(0).data;
    link_offload_sosi_arr(0).empty <= tx_offload_sosi_arr(0).empty;
    link_offload_sosi_arr(0).valid <= tx_offload_sosi_arr(0).valid;
    link_offload_sosi_arr(0).sop   <= tx_offload_sosi_arr(0).sop;
    link_offload_sosi_arr(0).eop   <= tx_offload_sosi_arr(0).eop;

    tx_offload_siso_arr <= (others => c_dp_siso_rdy);
  end process;

  ------------------------------------------------------------------------------
  -- DUT offload Rx
  ------------------------------------------------------------------------------

  u_rx : entity work.dp_offload_rx
  generic map (
    g_nof_streams         => 1,
    g_data_w              => g_data_w,
    g_hdr_field_arr       => c_udp_offload_hdr_field_arr,
    g_remove_crc          => false,
    g_crc_nof_words       => 0
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => dp_rst,
    dp_clk                => dp_clk,

    reg_hdr_dat_mosi      => reg_dp_offload_rx_hdr_dat_mosi,
    reg_hdr_dat_miso      => reg_dp_offload_rx_hdr_dat_miso,

    snk_in_arr            => link_offload_sosi_arr,
    snk_out_arr           => link_offload_siso_arr,

    src_out_arr           => dp_offload_rx_src_out_arr,
    src_in_arr            => dp_offload_rx_src_in_arr,

    hdr_fields_out_arr    => rx_hdr_fields_out_arr,
    hdr_fields_raw_arr    => rx_hdr_fields_raw_arr
  );

  p_restore_sync_bsn : process(dp_offload_rx_src_out_arr, rx_hdr_fields_out_arr)
  begin
    verify_snk_in      <= dp_offload_rx_src_out_arr(0);
    verify_snk_in.sync <=          sl(rx_hdr_fields_out_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "dp_sync") downto field_lo(c_udp_offload_hdr_field_arr, "dp_sync" )));
    verify_snk_in.bsn  <= RESIZE_UVEC(rx_hdr_fields_raw_arr(0)(field_hi(c_udp_offload_hdr_field_arr, "dp_bsn" ) downto field_lo(c_udp_offload_hdr_field_arr, "dp_bsn"  )), c_dp_stream_bsn_w);
  end process;

  dp_offload_rx_src_in_arr    <= (others => c_dp_siso_rdy);
  dp_offload_rx_src_in_arr(0) <= verify_snk_out;

  ------------------------------------------------------------------------------
  -- Auxiliary
  ------------------------------------------------------------------------------

  -- Map to slv to ease monitoring in wave window
  stimuli_src_out_data <= stimuli_src_out.data(g_data_w - 1 downto 0);
  verify_snk_in_data   <= verify_snk_in.data(g_data_w - 1 downto 0);
end tb;
