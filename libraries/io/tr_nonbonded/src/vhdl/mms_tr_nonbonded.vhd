-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: PHY IO interface for non-bonded gigabit transceivers
-- Description:

library IEEE, common_lib, technology_lib, dp_lib, diag_lib, diagnostics_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity mms_tr_nonbonded is
  generic (
    g_sim             : boolean := false;
    g_sim_level       : natural := 0;  -- Default: 0 = simulate IP. 1 = use fast behavioural model
    g_technology      : natural := c_tech_select_default;
    g_data_w          : natural := 32;
    g_nof_gx          : natural;
    g_mbps            : natural := 6250;  -- Supported: 6250, 5000, 3125, 2500
    g_tx              : boolean := true;
    g_rx              : boolean := true;
    g_tx_fifo_depth   : natural := c_bram_m9k_fifo_depth;  -- = 256 * 32b = 1 M9K, because g_data_w=32b, functionally a depth of c_meta_fifo_depth=16 is sufficient to cross the clock domain
    g_rx_fifo_depth   : natural := c_bram_m9k_fifo_depth;  -- = 256 * 32b = 1 M9K, because g_data_w=32b, functionally a depth of c_meta_fifo_depth=16 is sufficient to cross the clock domain
    g_rx_use_data_buf : boolean := false;
    g_rx_data_buf_nof_words : natural := 1024
  );
  port (
    tb_end                 : in  std_logic := '0';  -- in simulation stop internal clocks when tb_end='1' to support 'run -all'

    -- System
    mm_rst                 : in  std_logic;
    mm_clk                 : in  std_logic;
    st_rst                 : in  std_logic := '0';
    st_clk                 : in  std_logic := '0';
    tr_clk                 : in  std_logic;
    cal_rec_clk            : in  std_logic;
    gp_out                 : out std_logic_vector(2 * g_nof_gx - 1 downto 0);  -- FIFO full monitoring

    -- Serial data I/O
    tx_dataout             : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_datain              : in  std_logic_vector(g_nof_gx - 1 downto 0) := (others => '0');

    -- Streaming I/O
    snk_out_arr            : out t_dp_siso_arr(g_nof_gx - 1 downto 0) := (others => c_dp_siso_rst);
    snk_in_arr             : in  t_dp_sosi_arr(g_nof_gx - 1 downto 0) := (others => c_dp_sosi_rst);

    src_in_arr             : in  t_dp_siso_arr(g_nof_gx - 1 downto 0) := (others => c_dp_siso_rst);
    src_out_arr            : out t_dp_sosi_arr(g_nof_gx - 1 downto 0);

    -- MM interface
    tr_nonbonded_mm_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    tr_nonbonded_mm_miso   : out t_mem_miso := c_mem_miso_rst;

    diagnostics_mm_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    diagnostics_mm_miso    : out t_mem_miso := c_mem_miso_rst;

    ram_diag_data_buf_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_diag_data_buf_miso : out t_mem_miso := c_mem_miso_rst;
    reg_diag_data_buf_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diag_data_buf_miso : out t_mem_miso := c_mem_miso_rst
  );
end mms_tr_nonbonded;

architecture str of mms_tr_nonbonded is
  constant c_nof_select    : natural := 2;
  constant c_nof_select_w  : natural := ceil_log2(c_nof_select);
  constant c_sel_user      : natural := 0;
  constant c_sel_diag      : natural := 1;

  constant c_tx_state_w    : natural := 2;
  constant c_rx_state_w    : natural := 2;

  type t_select_sosi_2arr is array (integer range <>) of t_dp_sosi_arr(c_nof_select - 1 downto 0);
  type t_select_siso_2arr is array (integer range <>) of t_dp_siso_arr(c_nof_select - 1 downto 0);

  type t_select_arr is array (integer range <>) of std_logic_vector(c_nof_select_w - 1 downto 0);

  signal tx_clk                   :  std_logic_vector(g_nof_gx - 1 downto 0);
  signal tx_rst                   :  std_logic_vector(g_nof_gx - 1 downto 0);

  signal rx_clk                   :  std_logic_vector(g_nof_gx - 1 downto 0);
  signal rx_rst                   :  std_logic_vector(g_nof_gx - 1 downto 0);

  signal tx_state                 : std_logic_vector(c_tx_state_w * g_nof_gx - 1 downto 0);
  signal tx_align_en              : std_logic_vector(             g_nof_gx - 1 downto 0);

  signal rx_state                 : std_logic_vector(c_rx_state_w * g_nof_gx - 1 downto 0);
  signal rx_align_en              : std_logic_vector(             g_nof_gx - 1 downto 0);

  signal rx_dataout               : t_slv_32_arr(g_nof_gx - 1 downto 0);

  signal diagnostics_rx_sosi_arr  : t_dp_sosi_arr(g_nof_gx - 1 downto 0);
  signal diagnostics_rx_siso_arr  : t_dp_siso_arr(g_nof_gx - 1 downto 0);

  signal diagnostics_tx_sosi_arr  : t_dp_sosi_arr(g_nof_gx - 1 downto 0);
  signal diagnostics_tx_siso_arr  : t_dp_siso_arr(g_nof_gx - 1 downto 0);

  signal diagnostics_tx_en_arr    : std_logic_vector(g_nof_gx - 1 downto 0);
  signal diagnostics_rx_en_arr    : std_logic_vector(g_nof_gx - 1 downto 0);

  signal mux_in_siso_2arr         : t_select_siso_2arr(g_nof_gx - 1 downto 0);
  signal mux_in_sosi_2arr         : t_select_sosi_2arr(g_nof_gx - 1 downto 0);

  signal mux_out_sosi_arr         : t_dp_sosi_arr(g_nof_gx - 1 downto 0);
  signal mux_out_siso_arr         : t_dp_siso_arr(g_nof_gx - 1 downto 0);

  signal demux_in_siso_arr        : t_dp_siso_arr(g_nof_gx - 1 downto 0);
  signal demux_in_sosi_arr        : t_dp_sosi_arr(g_nof_gx - 1 downto 0);

  signal demux_out_sosi_2arr      : t_select_sosi_2arr(g_nof_gx - 1 downto 0);
  signal demux_out_siso_2arr      : t_select_siso_2arr(g_nof_gx - 1 downto 0);

  -- Init *_select_arr to avoid "Warning: NUMERIC_STD.TO_INTEGER: metavalue detected, returning 0" at startup in simulation
  signal mux_select_arr           : t_select_arr(g_nof_gx - 1 downto 0) := (others => (others => '0'));
  signal nxt_mux_select_arr       : t_select_arr(g_nof_gx - 1 downto 0);

  signal demux_select_arr         : t_select_arr(g_nof_gx - 1 downto 0) := (others => (others => '0'));
  signal nxt_demux_select_arr     : t_select_arr(g_nof_gx - 1 downto 0);
begin
  u_tr_nonbonded : entity work.tr_nonbonded
  generic map (
    g_sim             => g_sim,
    g_sim_level       => g_sim_level,
    g_technology      => g_technology,
    g_data_w          => g_data_w,
    g_nof_gx          => g_nof_gx,
    g_mbps            => g_mbps,
    g_tx              => g_tx,
    g_rx              => g_rx,
    g_fifos           => true,
    g_tx_fifo_depth   => g_tx_fifo_depth,
    g_rx_fifo_depth   => g_rx_fifo_depth
  )
    port map (
      tb_end          => tb_end,

      st_rst          => st_rst,
      st_clk          => st_clk,

      tr_clk          => tr_clk,
      cal_rec_clk     => cal_rec_clk,

      gp_out          => gp_out,

      --Serial data
      tx_dataout      => tx_dataout,
      rx_datain       => rx_datain,

      --Parallel data
      rx_rst          => rx_rst,
      rx_clk          => rx_clk,
      rx_sosi_arr     => OPEN,
      rx_siso_arr     => (others => c_dp_siso_rst),

      tx_rst          => tx_rst,
      tx_clk          => tx_clk,
      tx_sosi_arr     => (others => c_dp_sosi_rst),
      tx_siso_arr     => OPEN,

      dp_rx_sosi_arr  => demux_in_sosi_arr,
      dp_rx_siso_arr  => demux_in_siso_arr,

      dp_tx_sosi_arr  => mux_out_sosi_arr,
      dp_tx_siso_arr  => mux_out_siso_arr,

      tx_state        => tx_state,
      tx_align_en     => tx_align_en,

      rx_state        => rx_state,
      rx_align_en     => rx_align_en,
      rx_dataout      => rx_dataout
    );

  u_tr_nonbonded_reg: entity work.tr_nonbonded_reg
  generic map(
    g_nof_gx     => g_nof_gx
  )
  port map (
    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    tx_rst       => tx_rst,
    tx_clk       => tx_clk,

    rx_rst       => rx_rst,
    rx_clk       => rx_clk,

    sla_in       => tr_nonbonded_mm_mosi,
    sla_out      => tr_nonbonded_mm_miso,

    tx_state     => tx_state,
    tx_align_en  => tx_align_en,

    rx_state     => rx_state,
    rx_align_en  => rx_align_en,
    rx_dataout   => rx_dataout
  );

  u_mms_diagnostics: entity diagnostics_lib.mms_diagnostics
  generic map(
    g_data_w      => g_data_w,
    g_nof_streams => g_nof_gx
  )
  port map (
    mm_rst          => mm_rst,
    mm_clk          => mm_clk,

    st_rst          => st_rst,
    st_clk          => st_clk,

    mm_mosi         => diagnostics_mm_mosi,
    mm_miso         => diagnostics_mm_miso,

    src_out_arr     => diagnostics_tx_sosi_arr,
    src_in_arr      => diagnostics_tx_siso_arr,

    snk_out_arr     => diagnostics_rx_siso_arr,
    snk_in_arr      => diagnostics_rx_sosi_arr,

    src_en_out      => diagnostics_tx_en_arr,
    snk_en_out      => diagnostics_rx_en_arr
  );

  gen_select : for i in 0 to g_nof_gx - 1 generate
    -- 0 = user data,
    snk_out_arr(i)                     <= mux_in_siso_2arr(i)(c_sel_user);
    mux_in_sosi_2arr(i)(c_sel_user)    <= snk_in_arr(i);

    demux_out_siso_2arr(i)(c_sel_user) <= src_in_arr(i);
    src_out_arr(i)                     <= demux_out_sosi_2arr(i)(c_sel_user);

    -- 1 = internal diagnostics data
    diagnostics_tx_siso_arr(i)         <= mux_in_siso_2arr(i)(c_sel_diag);
    mux_in_sosi_2arr(i)(c_sel_diag)    <= diagnostics_tx_sosi_arr(i);

    demux_out_siso_2arr(i)(c_sel_diag) <= diagnostics_rx_siso_arr(i);
    diagnostics_rx_sosi_arr(i)         <= demux_out_sosi_2arr(i)(c_sel_diag);

    -- If user enables internal diagnostics source, the dp_mux automatically forwards that data to the corresponding transmitter
    nxt_mux_select_arr(i) <= slv(diagnostics_tx_en_arr(i));

    -- If user enables internal diagnostics sink, the dp_demux is automatically set to forward the corresponding receiver data
    nxt_demux_select_arr(i) <= slv(diagnostics_rx_en_arr(i));

    u_dp_mux : entity dp_lib.dp_mux
    generic map (
      g_technology        => g_technology,
      -- Mux
      g_sel_ctrl_invert   => true,  -- We're using DOWNTO ranges in our streaming arrays, so we must invert the selection input
      g_mode              => 2,
      g_nof_input         => c_nof_select,
      g_append_channel_lo => false,
      -- Input FIFO
      g_use_fifo          => false,
      g_fifo_size         => array_init(1024, c_nof_select),
      g_fifo_fill         => array_init(   0, c_nof_select)
    )
    port map (
      rst         => st_rst,
      clk         => st_clk,

      sel_ctrl    => TO_UINT(mux_select_arr(i)),

      -- ST sinks
      snk_out_arr => mux_in_siso_2arr(i),
      snk_in_arr  => mux_in_sosi_2arr(i),
      -- ST source
      src_in      => mux_out_siso_arr(i),
      src_out     => mux_out_sosi_arr(i)
    );

    u_dp_demux : entity dp_lib.dp_demux
    generic map (
      g_sel_ctrl_invert   => true,  -- We're using DOWNTO ranges in our streaming arrays, so we must invert the selection input
      g_mode              => 2,
      g_nof_output        => c_nof_select,
      g_remove_channel_lo => false,
      g_combined          => false
    )
    port map (
      rst         => st_rst,
      clk         => st_clk,

      sel_ctrl    => TO_UINT(demux_select_arr(i)),

      -- ST sinks
      snk_out     => demux_in_siso_arr(i),
      snk_in      => demux_in_sosi_arr(i),
      -- ST source
      src_in_arr  => demux_out_siso_2arr(i),
      src_out_arr => demux_out_sosi_2arr(i)
    );
  end generate;

  p_st_clk : process(st_rst, st_clk)
  begin
    if st_rst = '1' then
      mux_select_arr   <= (others => (others => '0'));
      demux_select_arr <= (others => (others => '0'));
    elsif rising_edge(st_clk) then
      mux_select_arr   <= nxt_mux_select_arr;
      demux_select_arr <= nxt_demux_select_arr;
    end if;
  end process;

  -- Optional RX data buffer
  gen_data_buf : if g_rx = true and g_rx_use_data_buf = true generate
    u_data_buf : entity diag_lib.mms_diag_data_buffer
    generic map (
      g_technology   => g_technology,
      g_nof_streams  => g_nof_gx,
      g_data_w       => g_data_w,
      g_buf_nof_data => g_rx_data_buf_nof_words,
      g_buf_use_sync => false
    )
    port map (
      -- System
      mm_rst            => mm_rst,
      mm_clk            => mm_clk,
      dp_rst            => st_rst,
      dp_clk            => st_clk,
      -- MM interface
      ram_data_buf_mosi => ram_diag_data_buf_mosi,
      ram_data_buf_miso => ram_diag_data_buf_miso,
      reg_data_buf_mosi => reg_diag_data_buf_mosi,
      reg_data_buf_miso => reg_diag_data_buf_miso,
      -- ST interface
      in_sosi_arr       => demux_in_sosi_arr
    );
  end generate;
end str;
