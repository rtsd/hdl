-------------------------------------------------------------------------------
-- Author: Harm Jan Pepping : pepping at astron.nl: 2012
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library ieee, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

package fft_pkg is
  -- Default FFT switch and unswitch seeds from LOFAR1
  constant c_fft_lfsr_len      : natural := 41;
  constant c_fft_switch_seed1  : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := "01000101011101110101001011111000101100001";
  constant c_fft_switch_seed2  : std_logic_vector(c_fft_lfsr_len - 1 downto 0) := "11011001000101001011011001110101100101100";

  function fft_switch_new_seed(seed : std_logic_vector; offset : natural) return std_logic_vector;

  -- The FFT gain for an real input sinus signal is 0.5, because a real input
  -- sinus with amplitude A yields two subband phasors each with amplitude
  -- A/2, one at -f_bin and one at +f_bin. The power stays the same, because
  -- the power of an real input sinus is A**2 / 2 and the power of the f_bin
  -- signal complex phasors is (A/2)**2 + (A/2)**2 = A**2 / 2.
  -- For DC at bin 0 the real input gain is 1.0, because DC has only one
  -- phasor component of frequency 0.
  constant c_fft_real_input_gain_sine : real := 0.5;
  constant c_fft_real_input_gain_dc   : real := 1.0;

  -- FFT parameters for pipelined FFT (fft_pipe), parallel FFT (fft_par) and wideband FFT (fft_wide)
  type t_fft is record
    use_reorder    : boolean;  -- = false for bit-reversed output, true for normal output
    use_fft_shift  : boolean;  -- = false for [0, pos, neg] bin frequencies order, true for [neg, 0, pos] bin frequencies order in case of complex input
    use_separate   : boolean;  -- = false for complex input, true for two real inputs
    nof_chan       : natural;  -- = default 0, defines the number of channels (=time-multiplexed input signals): nof channels = 2**nof_chan
    wb_factor      : natural;  -- = default 1, wideband factor
    twiddle_offset : natural;  -- = default 0, twiddle offset for PFT sections in a wideband FFT
    nof_points     : natural;  -- = 1024, N point FFT
    in_dat_w       : natural;  -- = 8,  number of input bits
    out_dat_w      : natural;  -- = 13, number of output bits
    out_gain_w     : natural;  -- = 0, output gain factor applied after the last stage output, before requantization to out_dat_w
    stage_dat_w    : natural;  -- = 18, data width used between the stages(= DSP multiplier-width)
    guard_w        : natural;  -- = 2, guard used to avoid overflow in first FFT stage, compensated in last guard_w nof FFT stages.
                               --   on average the gain per stage is 2 so guard_w = 1, but the gain can be 1+sqrt(2) [Lyons section
                               --   12.3.2], therefore use input guard_w = 2.
    guard_enable   : boolean;  -- = true when input needs guarding, false when input requires no guarding but scaling must be
                               --   skipped at the last stage(s) compensate for input guard (used in wb fft with pipe fft section
                               --   doing the input guard and par fft section doing the output compensation)
    stat_data_w    : positive;  -- = 56
    stat_data_sz   : positive;  -- = 2
  end record;

  constant c_fft   : t_fft := (true, false, true, 0, 4, 0, 1024, 8, 14, 0, c_dsp_mult_w, 2, true, 56, 2);

  -- Check consistancy of the FFT parameters
  function fft_r2_parameter_asserts(g_fft : t_fft) return boolean;  -- the return value is void, because always true or abort due to failure

  -- FFT input width
  function func_fft_in_scale_w(g_fft : t_fft) return natural;

  -- FFT output width
  --                 Removed MSbits     Quantized data    Rounded LSbits
  --   c_raw_dat_w = c_fft.out_gain_w + c_fft.out_dat_w + c_raw_fraction_w
  function func_fft_raw_dat_w(g_fft : t_fft) return natural;
  function func_fft_raw_fraction_w(g_fft : t_fft) return natural;

  -- Definitions for fft slv array (an array can not have unconstraint elements, so choose sufficiently wide 32 bit slv elements)
  subtype  t_fft_slv_arr is t_slv_32_arr;  -- use subtype to ease interfacing to existing types and to have central definition for rtwo components
  constant c_fft_slv_w  : natural := 32;  -- match slv width of t_fft_slv_arr
  function to_fft_svec(n : integer) return std_logic_vector;  -- map to c_fft_slv_w wide slv, no need for to_rtwo_uvec, because natural is subtype of integer
  function resize_fft_uvec(vec : std_logic_vector) return std_logic_vector;  -- map to c_fft_slv_w wide slv
  function resize_fft_svec(vec : std_logic_vector) return std_logic_vector;  -- map to c_fft_slv_w wide slv

  -- FFT shift swaps right and left half of bin axis to shift zero-frequency component to center of spectrum
  function fft_shift(bin : std_logic_vector) return std_logic_vector;
  function fft_shift(bin, w : natural) return natural;
end package fft_pkg;

package body fft_pkg is
  function fft_switch_new_seed(seed : std_logic_vector; offset : natural) return std_logic_vector is
  begin
    return INCR_UVEC(seed, offset);  -- make new unique seed
  end;

  function fft_r2_parameter_asserts(g_fft : t_fft) return boolean is
  begin
    -- nof_points
    assert g_fft.nof_points = 2**true_log2(g_fft.nof_points)
      report "fft_r2: nof_points must be a power of 2"
      severity failure;
    -- wb_factor
    assert g_fft.wb_factor = 2**true_log2(g_fft.wb_factor)
      report "fft_r2: wb_factor must be a power of 2"
      severity failure;
    -- use_reorder
    if g_fft.use_reorder = false then
      assert g_fft.use_separate = false
        report "fft_r2 : without use_reorder there cannot be use_separate for two real inputs"
        severity failure;
      assert g_fft.use_fft_shift = false
        report "fft_r2 : without use_reorder there cannot be use_fft_shift for complex input"
        severity failure;
    end if;
    -- use_separate
    if g_fft.use_separate = true then
      assert g_fft.use_fft_shift = false
        report "fft_r2 : with use_separate there cannot be use_fft_shift for two real inputs"
        severity failure;
    end if;
    return true;
  end;

  -- Scale input data to MSbits of stage data, with g_fft.guard_w bits backoff if g_fft.guard_enable = true
  function func_fft_in_scale_w(g_fft : t_fft) return natural is
    constant c_in_scale_w  : natural := g_fft.stage_dat_w - g_fft.in_dat_w - sel_a_b(g_fft.guard_enable, g_fft.guard_w, 0);
  begin
    return c_in_scale_w;
  end func_fft_in_scale_w;

  -- Raw output data width is equal to the internal data width of the FFT
  function func_fft_raw_dat_w(g_fft : t_fft) return natural is
    constant c_sepa_growth_w  : natural := sel_a_b(g_fft.use_separate, 1, 0);  -- add one bit for add sub growth in separate
    constant c_raw_dat_w      : natural := g_fft.stage_dat_w + c_sepa_growth_w;
  begin
    return c_raw_dat_w;
  end func_fft_raw_dat_w;

  -- Number of LSbits in the raw output data that represent the fraction bits in c_raw_dat_w.
  function func_fft_raw_fraction_w(g_fft : t_fft) return natural is
    constant c_raw_dat_w      : natural := func_fft_raw_dat_w(g_fft);
    constant c_raw_fraction_w : natural := c_raw_dat_w - g_fft.out_dat_w - g_fft.out_gain_w;
  begin
    return c_raw_fraction_w;
  end func_fft_raw_fraction_w;

  function to_fft_svec(n : integer) return std_logic_vector is
  begin
    return RESIZE_SVEC(TO_SVEC(n, 32), c_fft_slv_w);
  end;

  function resize_fft_uvec(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_UVEC(vec, c_fft_slv_w);
  end;

  function resize_fft_svec(vec : std_logic_vector) return std_logic_vector is
  begin
    return RESIZE_SVEC(vec, c_fft_slv_w);
  end;

  function fft_shift(bin : std_logic_vector) return std_logic_vector is
    constant c_w   : natural := bin'length;
    variable v_bin : std_logic_vector(c_w - 1 downto 0) := bin;
  begin
    return not v_bin(c_w - 1) & v_bin(c_w - 2 downto 0);  -- invert MSbit for fft_shift
  end;

  function fft_shift(bin, w : natural) return natural is
  begin
    return TO_UINT(fft_shift(TO_UVEC(bin, w)));
  end;
end fft_pkg;
