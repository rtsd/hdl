-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use unb2b_board_lib.unb2b_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use work.qsys_unb2b_jesd_pkg.all;

entity mmm_unb2b_jesd is
  generic (
    g_sim         : boolean := false;  -- FALSE: use QSYS; TRUE: use mm_file I/O
    g_sim_unb_nr  : natural := 0;
    g_sim_node_nr : natural := 0
  );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi             : out t_mem_mosi;
    reg_wdi_miso             : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi : out t_mem_mosi;
    reg_unb_system_info_miso : in  t_mem_miso;
    rom_unb_system_info_mosi : out t_mem_mosi;
    rom_unb_system_info_miso : in  t_mem_miso;

    -- UniBoard I2C sensors
    reg_unb_sens_mosi        : out t_mem_mosi;
    reg_unb_sens_miso        : in  t_mem_miso;

    reg_fpga_temp_sens_mosi   : out t_mem_mosi;
    reg_fpga_temp_sens_miso   : in  t_mem_miso;
    reg_fpga_voltage_sens_mosi: out t_mem_mosi;
    reg_fpga_voltage_sens_miso: in  t_mem_miso;

    reg_unb_pmbus_mosi       : out t_mem_mosi;
    reg_unb_pmbus_miso       : in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi            : out t_mem_mosi;
    reg_ppsh_miso            : in  t_mem_miso;

    -- eth1g
    eth1g_mm_rst             : out std_logic;
    eth1g_tse_mosi           : out t_mem_mosi;
    eth1g_tse_miso           : in  t_mem_miso;
    eth1g_reg_mosi           : out t_mem_mosi;
    eth1g_reg_miso           : in  t_mem_miso;
    eth1g_reg_interrupt      : in  std_logic;
    eth1g_ram_mosi           : out t_mem_mosi;
    eth1g_ram_miso           : in  t_mem_miso;

    -- EPCS read
    reg_dpmm_data_mosi       : out t_mem_mosi;
    reg_dpmm_data_miso       : in  t_mem_miso;
    reg_dpmm_ctrl_mosi       : out t_mem_mosi;
    reg_dpmm_ctrl_miso       : in  t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi       : out t_mem_mosi;
    reg_mmdp_data_miso       : in  t_mem_miso;
    reg_mmdp_ctrl_mosi       : out t_mem_mosi;
    reg_mmdp_ctrl_miso       : in  t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi            : out t_mem_mosi;
    reg_epcs_miso            : in  t_mem_miso;

    -- Remote Update
    reg_remu_mosi            : out t_mem_mosi;
    reg_remu_miso            : in  t_mem_miso;

    -- JESD
    jesd204_rx_serial_data   : in std_logic;
    jesd204_sync_n_out       : out std_logic;
    jesd204_rx_link_error    : out std_logic;
    jesd204_rx_link_data     : out std_logic_vector(31 downto 0);
    jesd204_rx_link_valid    : out std_logic;
    jesd204_rx_link_ready    : in std_logic;
    jesd204_rx_sysref        : in std_logic;
    jesd204_device_clk       : in std_logic;

    -- JESD databuffer
    ram_diag_data_buf_jesd_mosi   : out t_mem_mosi;
    ram_diag_data_buf_jesd_miso   : in  t_mem_miso;
    reg_diag_data_buf_jesd_mosi   : out t_mem_mosi;
    reg_diag_data_buf_jesd_miso   : in  t_mem_miso
  );
end mmm_unb2b_jesd;

architecture str of mmm_unb2b_jesd is
  constant c_sim_node_nr   : natural := g_sim_node_nr;
  constant c_sim_node_type : string(1 to 2) := "FN";

  signal i_reset_n         : std_logic;

  signal rx_xcvr_ready_in : std_logic;
  signal rx_ready_or_rx_csr_lane_powerdown : std_logic_vector(0 downto 0);
  signal xcvr_rst_ctrl_rx_ready : std_logic_vector(0 downto 0);
  signal rx_csr_lane_powerdown : std_logic_vector(0 downto 0);
  signal rx_link_rst_n : std_logic;
  signal link_clk : std_logic;
  signal frame_clk : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $UPE/sim.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    u_mm_file_reg_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                               port map(mm_rst, mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                               port map(mm_rst, mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                               port map(mm_rst, mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_unb_sens        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_SENS")
                                               port map(mm_rst, mm_clk, reg_unb_sens_mosi, reg_unb_sens_miso );

    u_mm_file_reg_unb_pmbus       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_UNB_PMBUS")
                                               port map(mm_rst, mm_clk, reg_unb_pmbus_mosi, reg_unb_pmbus_miso );

    u_mm_file_reg_fpga_temp_sens  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_TEMP_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_temp_sens_mosi, reg_fpga_temp_sens_miso );

    u_mm_file_reg_fpga_voltage_sens :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_VOLTAGE_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_voltage_sens_mosi, reg_fpga_voltage_sens_miso );

    u_mm_file_reg_ppsh            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                               port map(mm_rst, mm_clk, reg_ppsh_mosi, reg_ppsh_miso );

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    u_mm_file_reg_eth             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_MMS_REG")
                                               port map(mm_rst, mm_clk, eth1g_reg_mosi, eth1g_reg_miso );

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(mm_clk, c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  i_reset_n <= not mm_rst;
  rx_xcvr_ready_in <= rx_ready_or_rx_csr_lane_powerdown(0);
  rx_ready_or_rx_csr_lane_powerdown <= xcvr_rst_ctrl_rx_ready or rx_csr_lane_powerdown;
  ----------------------------------------------------------------------------
  -- QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys : if g_sim = false generate
    u_qsys : qsys_unb2b_jesd
    port map (

      clk_clk                                   => mm_clk,
      reset_reset_n                             => i_reset_n,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2b_board.
      pio_wdi_external_connection_export        => pout_wdi,

      avs_eth_0_reset_export                    => eth1g_mm_rst,
      avs_eth_0_clk_export                      => OPEN,
      avs_eth_0_tse_address_export              => eth1g_tse_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                => eth1g_tse_mosi.wr,
      avs_eth_0_tse_read_export                 => eth1g_tse_mosi.rd,
      avs_eth_0_tse_writedata_export            => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export             => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export          => eth1g_tse_miso.waitrequest,
      avs_eth_0_reg_address_export              => eth1g_reg_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                => eth1g_reg_mosi.wr,
      avs_eth_0_reg_read_export                 => eth1g_reg_mosi.rd,
      avs_eth_0_reg_writedata_export            => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export             => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export              => eth1g_ram_mosi.address(c_unb2b_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                => eth1g_ram_mosi.wr,
      avs_eth_0_ram_read_export                 => eth1g_ram_mosi.rd,
      avs_eth_0_ram_writedata_export            => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export             => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                      => eth1g_reg_interrupt,

      reg_unb_sens_reset_export                 => OPEN,
      reg_unb_sens_clk_export                   => OPEN,
      reg_unb_sens_address_export               => reg_unb_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      reg_unb_sens_write_export                 => reg_unb_sens_mosi.wr,
      reg_unb_sens_writedata_export             => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_read_export                  => reg_unb_sens_mosi.rd,
      reg_unb_sens_readdata_export              => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_unb_pmbus_reset_export                => OPEN,
      reg_unb_pmbus_clk_export                  => OPEN,
      reg_unb_pmbus_address_export              => reg_unb_pmbus_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_pmbus_adr_w - 1 downto 0),
      reg_unb_pmbus_write_export                => reg_unb_pmbus_mosi.wr,
      reg_unb_pmbus_writedata_export            => reg_unb_pmbus_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_pmbus_read_export                 => reg_unb_pmbus_mosi.rd,
      reg_unb_pmbus_readdata_export             => reg_unb_pmbus_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_temp_sens_reset_export           => OPEN,
      reg_fpga_temp_sens_clk_export             => OPEN,
      reg_fpga_temp_sens_address_export         => reg_fpga_temp_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export           => reg_fpga_temp_sens_mosi.wr,
      reg_fpga_temp_sens_writedata_export       => reg_fpga_temp_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export            => reg_fpga_temp_sens_mosi.rd,
      reg_fpga_temp_sens_readdata_export        => reg_fpga_temp_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_reset_export        => OPEN,
      reg_fpga_voltage_sens_clk_export          => OPEN,
      reg_fpga_voltage_sens_address_export      => reg_fpga_voltage_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export        => reg_fpga_voltage_sens_mosi.wr,
      reg_fpga_voltage_sens_writedata_export    => reg_fpga_voltage_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export         => reg_fpga_voltage_sens_mosi.rd,
      reg_fpga_voltage_sens_readdata_export     => reg_fpga_voltage_sens_miso.rddata(c_word_w - 1 downto 0),

      rom_system_info_reset_export              => OPEN,
      rom_system_info_clk_export                => OPEN,
      rom_system_info_address_export            => rom_unb_system_info_mosi.address(c_unb2b_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      rom_system_info_write_export              => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export          => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export               => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export           => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_system_info_reset_export              => OPEN,
      pio_system_info_clk_export                => OPEN,
      pio_system_info_address_export            => reg_unb_system_info_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export              => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export          => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export               => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export           => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_pps_reset_export                      => OPEN,
      pio_pps_clk_export                        => OPEN,
      pio_pps_address_export                    => reg_ppsh_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1 downto 0),
      pio_pps_write_export                      => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                  => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                       => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                   => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),

      reg_wdi_reset_export                      => OPEN,
      reg_wdi_clk_export                        => OPEN,
      reg_wdi_address_export                    => reg_wdi_mosi.address(0 downto 0),
      reg_wdi_write_export                      => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                  => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                       => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                   => reg_wdi_miso.rddata(c_word_w - 1 downto 0),

      reg_remu_reset_export                     => OPEN,
      reg_remu_clk_export                       => OPEN,
      reg_remu_address_export                   => reg_remu_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                     => reg_remu_mosi.wr,
      reg_remu_writedata_export                 => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                      => reg_remu_mosi.rd,
      reg_remu_readdata_export                  => reg_remu_miso.rddata(c_word_w - 1 downto 0),

      reg_epcs_reset_export                     => OPEN,
      reg_epcs_clk_export                       => OPEN,
      reg_epcs_address_export                   => reg_epcs_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                     => reg_epcs_mosi.wr,
      reg_epcs_writedata_export                 => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                      => reg_epcs_mosi.rd,
      reg_epcs_readdata_export                  => reg_epcs_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_reset_export                => OPEN,
      reg_dpmm_ctrl_clk_export                  => OPEN,
      reg_dpmm_ctrl_address_export              => reg_dpmm_ctrl_mosi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                => reg_dpmm_ctrl_mosi.wr,
      reg_dpmm_ctrl_writedata_export            => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                 => reg_dpmm_ctrl_mosi.rd,
      reg_dpmm_ctrl_readdata_export             => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_reset_export                => OPEN,
      reg_mmdp_data_clk_export                  => OPEN,
      reg_mmdp_data_address_export              => reg_mmdp_data_mosi.address(0 downto 0),
      reg_mmdp_data_write_export                => reg_mmdp_data_mosi.wr,
      reg_mmdp_data_writedata_export            => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                 => reg_mmdp_data_mosi.rd,
      reg_mmdp_data_readdata_export             => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_reset_export                => OPEN,
      reg_dpmm_data_clk_export                  => OPEN,
      reg_dpmm_data_address_export              => reg_dpmm_data_mosi.address(0 downto 0),
      reg_dpmm_data_read_export                 => reg_dpmm_data_mosi.rd,
      reg_dpmm_data_readdata_export             => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                => reg_dpmm_data_mosi.wr,
      reg_dpmm_data_writedata_export            => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_reset_export                => OPEN,
      reg_mmdp_ctrl_clk_export                  => OPEN,
      reg_mmdp_ctrl_address_export              => reg_mmdp_ctrl_mosi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                 => reg_mmdp_ctrl_mosi.rd,
      reg_mmdp_ctrl_readdata_export             => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                => reg_mmdp_ctrl_mosi.wr,
      reg_mmdp_ctrl_writedata_export            => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      ram_diag_data_buf_jesd_clk_export         => OPEN,
      ram_diag_data_buf_jesd_reset_export       => OPEN,
      ram_diag_data_buf_jesd_address_export     => ram_diag_data_buf_jesd_mosi.address(17 - 1 downto 0),
      ram_diag_data_buf_jesd_write_export       => ram_diag_data_buf_jesd_mosi.wr,
      ram_diag_data_buf_jesd_writedata_export   => ram_diag_data_buf_jesd_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buf_jesd_read_export        => ram_diag_data_buf_jesd_mosi.rd,
      ram_diag_data_buf_jesd_readdata_export    => ram_diag_data_buf_jesd_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buf_jesd_reset_export       => OPEN,
      reg_diag_data_buf_jesd_clk_export         => OPEN,
      reg_diag_data_buf_jesd_address_export     => reg_diag_data_buf_jesd_mosi.address(6 - 1 downto 0),
      reg_diag_data_buf_jesd_write_export       => reg_diag_data_buf_jesd_mosi.wr,
      reg_diag_data_buf_jesd_writedata_export   => reg_diag_data_buf_jesd_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buf_jesd_read_export        => reg_diag_data_buf_jesd_mosi.rd,
      reg_diag_data_buf_jesd_readdata_export    => reg_diag_data_buf_jesd_miso.rddata(c_word_w - 1 downto 0),

            -- connections to the JESD IP:

            --altjesd_reset_seq_irq_irq                                    =>
            altjesd_reset_seq_pll_locked_in_reset1_dsrt_qual             => i_reset_n,
            --altjesd_reset_seq_pll_reset_reset                            =>
            altjesd_reset_seq_reset_in0_reset                            => mm_rst,
            altjesd_reset_seq_rx_xcvr_ready_in_reset5_dsrt_qual         => '1',
            altjesd_reset_seq_tx_xcvr_ready_in_reset2_dsrt_qual          => rx_xcvr_ready_in,
            --altjesd_rx_csr_cf_export                                     =>
            --altjesd_rx_csr_cs_export                                     =>
            --altjesd_rx_csr_f_export                                      =>
            --altjesd_rx_csr_hd_export                                     =>
            --altjesd_rx_csr_k_export                                      =>
            --altjesd_rx_csr_l_export                                      =>
            altjesd_rx_csr_lane_powerdown_export                         => rx_csr_lane_powerdown,
            --altjesd_rx_csr_m_export                                      =>
            --altjesd_rx_csr_n_export                                      =>
            --altjesd_rx_csr_np_export                                     =>
            --altjesd_rx_csr_rx_testmode_export                            =>
            --altjesd_rx_csr_s_export                                      =>
            altjesd_rx_dev_sync_n_export                                 => jesd204_sync_n_out,
            altjesd_rx_jesd204_rx_dlb_data_export                        => (others => '0'),
            altjesd_rx_jesd204_rx_dlb_data_valid_export                  => (others => '0'),
            altjesd_rx_jesd204_rx_dlb_disperr_export                     => (others => '0'),
            altjesd_rx_jesd204_rx_dlb_errdetect_export                   => (others => '0'),
            altjesd_rx_jesd204_rx_dlb_kchar_data_export                  => (others => '0'),
            altjesd_rx_jesd204_rx_frame_error_export                     => '0',
            altjesd_rx_jesd204_rx_int_irq                                => jesd204_rx_link_error,
            altjesd_rx_jesd204_rx_link_data                              => jesd204_rx_link_data,
            altjesd_rx_jesd204_rx_link_valid                             => jesd204_rx_link_valid,
            altjesd_rx_jesd204_rx_link_ready                             => jesd204_rx_link_ready,
            altjesd_rx_rx_serial_data_rx_serial_data(0)                     => jesd204_rx_serial_data,
            altjesd_rx_rxlink_rst_n_reset_n                              => rx_link_rst_n,
            altjesd_ss_rx_link_reset_out_reset_reset_n                   => rx_link_rst_n,
            --altjesd_ss_rx_frame_reset_out_reset_reset_n                  =>
            --altjesd_rx_rxphy_clk_export                                  =>
            --altjesd_rx_sof_export                                        =>
            --altjesd_rx_somf_export                                       =>
            altjesd_rx_sysref_export                                     => jesd204_rx_sysref,
            --altjesd_ss_rx_corepll_locked_export                          =>
            --altjesd_ss_rx_xcvr_reset_control_pll_powerdown_pll_powerdown =>
            altjesd_ss_rx_xcvr_reset_control_rx_ready_rx_ready           => xcvr_rst_ctrl_rx_ready,
            device_clk_clk                                               => jesd204_device_clk,
            device_clk_reset_reset_n                                     => '1',
            frame_clk_clk                                                => frame_clk,
            pll_out_frame_clk_clk                                        => frame_clk,
            frame_clk_reset_reset_n                                      => '1',
            link_clk_clk                                                 => link_clk,
            pll_out_link_clk_clk                                         => link_clk,
            link_clk_reset_reset_n                                       => '1'
      );
  end generate;
end str;
