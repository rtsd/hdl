-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: Eric Kooistra
-- Purpose: Create sync interval by counting input valids
-- Description:
--
--  The first out_sync is created at the first inval after rst release. The
--  subsequent out_sync are created every m in_val, at the start of a block.
--
--    n = g_nof_clk_per_block
--    m = g_nof_clk_per_sync
--                _____________________________________________________
--    in_val   __|
--
--    blk_cnt    |    0    |    1    |    2    |    3    |    4    |
--
--    val_cnt    |0        |n        |n*2      |n*3 - m  |         |0
--                _                       _                         _
--    out_sync __| |_____________________| |_______________________| |_
--                _____________________________________________________
--    out_val  __|
--                _         _         _         _         _         _
--    out_sop  __| |_______| |_______| |_______| |_______| |_______| |_
--                        _         _         _         _         _
--    out_eop  __________| |_______| |_______| |_______| |_______| |___
--
-- Remark:
-- . Use VHDL coding template from:
--   https://support.astron.nl/confluence/display/SBe/VHDL+design+patterns+for+RTL+coding
-- . The out_sop and out_eop are created as well, for reference.
-- . The out_sync1 for LOFAR1 style is only avaiable if g_pipeline = TRUE,
--   because the pipeline is needed to let the out_sync1 preceed the
--   out_sop and other strobes.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_create_strobes_from_valid is
  generic (
    g_pipeline           : boolean := true;
    g_nof_clk_per_sync   : natural := 200 * 10**6;
    g_nof_clk_per_block  : natural := 1024
  );
  port (
    rst       : in  std_logic := '0';
    clk       : in  std_logic;
    in_val    : in  std_logic;
    out_val   : out std_logic;
    out_sop   : out std_logic;
    out_eop   : out std_logic;
    out_sync  : out std_logic;  -- DP style: sync at sop
    out_sync1 : out std_logic  -- LOFAR1 style: sync before sop
  );
end common_create_strobes_from_valid;

architecture rtl of common_create_strobes_from_valid is
  type t_state is record  -- function state registers
    val_cnt : natural range 0 to g_nof_clk_per_sync - 1;
    blk_cnt : natural range 0 to g_nof_clk_per_block - 1;
  end record;

  type t_outputs is record  -- copy of entity outputs
    out_val  : std_logic;
    out_sop  : std_logic;
    out_eop  : std_logic;
    out_sync : std_logic;
  end record;

  constant c_state_rst   : t_state := (val_cnt => 0, blk_cnt => 0);
  constant c_outputs_rst : t_outputs := ('0', '0', '0', '0');

  signal q : t_state := c_state_rst;  -- stored state with latency one
  signal d : t_state := c_state_rst;  -- zero latency state

  signal o : t_outputs := c_outputs_rst;  -- zero latency outputs
  signal p : t_outputs := c_outputs_rst;  -- pipelined outputs
begin
  -- p_state
  q <= d when rising_edge(clk);

  p_comb : process(rst, q, in_val)
    variable v : t_state;
  begin
    -- Default
    v := q;
    o.out_val <= in_val;
    o.out_sop <= '0';
    o.out_eop <= '0';
    o.out_sync <= '0';

    -- Function
    if in_val = '1' then
      -- maintain in_val counters
      if q.val_cnt >= g_nof_clk_per_sync - 1 then
        v.val_cnt := 0;
      else
        v.val_cnt := v.val_cnt + 1;
      end if;
      if q.blk_cnt >= g_nof_clk_per_block - 1 then
        v.blk_cnt := 0;
      else
        v.blk_cnt := v.blk_cnt + 1;
      end if;
      -- create out_sop at start of block
      if q.blk_cnt = 0 then
        o.out_sop <= '1';
      end if;
      -- create out_eop at end of block
      if q.blk_cnt = g_nof_clk_per_block - 1 then
        o.out_eop <= '1';
      end if;
      -- create out_sync at start of first block of sync interval
      if q.blk_cnt = 0 and q.val_cnt < g_nof_clk_per_block then
        o.out_sync <= '1';
      end if;
    end if;

    -- Reset
    if rst = '1' then
      v := c_state_rst;
    end if;

    -- Result
    d <= v;
  end process;

  -- Output
  p <= o when rising_edge(clk);

  out_val  <= o.out_val  when g_pipeline = false else p.out_val;
  out_sop  <= o.out_sop  when g_pipeline = false else p.out_sop;
  out_eop  <= o.out_eop  when g_pipeline = false else p.out_eop;
  out_sync <= o.out_sync when g_pipeline = false else p.out_sync;

  out_sync1 <= '0' when g_pipeline = false else o.out_sync;
end rtl;
