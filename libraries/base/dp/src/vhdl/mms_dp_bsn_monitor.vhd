-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose : MMS for dp_bsn_monitor
-- Description: See dp_bsn_monitor.vhd

entity mms_dp_bsn_monitor is
  generic (
    g_nof_streams        : positive := 1;
    g_cross_clock_domain : boolean := true;  -- use FALSE when mm_clk and dp_clk are the same, else use TRUE to cross the clock domain
    g_sync_timeout       : natural := 200 * 10**6;
    g_bsn_w              : natural := c_dp_stream_bsn_w;
    g_error_bi           : natural := 0;
    g_cnt_sop_w          : natural := c_word_w;
    g_cnt_valid_w        : natural := c_word_w;
    g_log_first_bsn      : boolean := true  -- Logs first BSN + cycle count. Useful when using BSN aligner.
  );
  port (
    -- Memory-mapped clock domain
    mm_rst      : in  std_logic;
    mm_clk      : in  std_logic;
    reg_mosi    : in  t_mem_mosi;
    reg_miso    : out t_mem_miso;

    -- Streaming clock domain
    dp_rst      : in  std_logic;
    dp_clk      : in  std_logic;
    sync_in     : in  std_logic := '0';  -- Sync input that samples the current bsn number and the number of clockcycles that have expired since the last sop

    in_siso_arr : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    in_sosi_arr : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end mms_dp_bsn_monitor;

architecture str of mms_dp_bsn_monitor is
  constant c_reg_adr_w : natural := ceil_log2(9);

  signal mon_evt_arr           : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mon_sync_timeout_arr  : std_logic_vector(g_nof_streams - 1 downto 0);

  signal mon_ready_stable_arr  : std_logic_vector(g_nof_streams - 1 downto 0);
  signal mon_xon_stable_arr    : std_logic_vector(g_nof_streams - 1 downto 0);

  type t_mon_bsn_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(g_bsn_w - 1 downto 0);
  type t_mon_sop_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(g_cnt_sop_w - 1 downto 0);
  type t_mon_val_arr is array(g_nof_streams - 1 downto 0) of std_logic_vector(g_cnt_valid_w - 1 downto 0);

  signal mon_bsn_at_sync_arr         : t_mon_bsn_arr;
  signal mon_nof_sop_arr             : t_mon_sop_arr;
  signal mon_nof_err_arr             : t_mon_sop_arr;  -- use g_cnt_sop_w, because at maximium all frames have an error
  signal mon_nof_valid_arr           : t_mon_val_arr;
  signal mon_bsn_first_arr           : t_mon_bsn_arr;
  signal mon_bsn_first_cycle_cnt_arr : t_slv_32_arr(g_nof_streams - 1 downto 0);

  signal reg_mosi_arr                : t_mem_mosi_arr(g_nof_streams - 1 downto 0);
  signal reg_miso_arr                : t_mem_miso_arr(g_nof_streams - 1 downto 0);
begin
  u_common_mem_mux : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_reg_adr_w
  )
  port map (
    mosi     => reg_mosi,
    miso     => reg_miso,
    mosi_arr => reg_mosi_arr,
    miso_arr => reg_miso_arr
  );

  gen_stream : for i in 0 to g_nof_streams - 1 generate
    u_reg : entity work.dp_bsn_monitor_reg
    generic map (
      g_cross_clock_domain => g_cross_clock_domain
    )
    port map (
      -- Clocks and reset
      mm_rst                  => mm_rst,
      mm_clk                  => mm_clk,
      st_rst                  => dp_rst,
      st_clk                  => dp_clk,

      -- Memory Mapped Slave in mm_clk domain
      sla_in                  => reg_mosi_arr(i),
      sla_out                 => reg_miso_arr(i),

      -- MM registers in dp_clk domain
      -- . control
      mon_evt                 => mon_evt_arr(i),
      mon_sync_timeout        => mon_sync_timeout_arr(i),
      -- . siso
      mon_ready_stable        => mon_ready_stable_arr(i),
      mon_xon_stable          => mon_xon_stable_arr(i),
      -- . sosi
      mon_bsn_at_sync         => mon_bsn_at_sync_arr(i),
      mon_nof_sop             => mon_nof_sop_arr(i),
      mon_nof_err             => mon_nof_err_arr(i),
      mon_nof_valid           => mon_nof_valid_arr(i),

      mon_bsn_first           => mon_bsn_first_arr(i),
      mon_bsn_first_cycle_cnt => mon_bsn_first_cycle_cnt_arr(i)
    );

    u_mon : entity work.dp_bsn_monitor
    generic map (
      g_sync_timeout  => g_sync_timeout,
      g_error_bi      => g_error_bi,
      g_log_first_bsn => g_log_first_bsn
    )
    port map (
      rst                    => dp_rst,
      clk                    => dp_clk,

      -- ST interface
      in_siso                => in_siso_arr(i),
      in_sosi                => in_sosi_arr(i),
      sync_in                => sync_in,

      -- MM interface
      -- . control
      mon_evt                 => mon_evt_arr(i),  -- pulses when new monitor data is available regarding the previous sync interval
      mon_sync                => OPEN,
      mon_sync_timeout        => mon_sync_timeout_arr(i),
      -- . siso
      mon_ready_stable        => mon_ready_stable_arr(i),
      mon_xon_stable          => mon_xon_stable_arr(i),
      -- . sosi
      mon_bsn_at_sync         => mon_bsn_at_sync_arr(i),
      mon_nof_sop             => mon_nof_sop_arr(i),
      mon_nof_err             => mon_nof_err_arr(i),
      mon_nof_valid           => mon_nof_valid_arr(i),

      mon_bsn_first           => mon_bsn_first_arr(i),
      mon_bsn_first_cycle_cnt => mon_bsn_first_cycle_cnt_arr(i)
    );
  end generate;
end str;
