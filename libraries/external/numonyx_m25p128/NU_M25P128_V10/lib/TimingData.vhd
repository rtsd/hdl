--
--        _/_/           _/_/_/
--      _/_/_/_/         _/_/_/
--      _/_/_/_/_/       _/_/_/              ____________________________________________
--      _/_/_/_/_/       _/_/_/             /                                           /
--      _/_/_/_/_/       _/_/_/            /                                 M25P128   /
--      _/_/_/_/_/       _/_/_/           /                                           /
--      _/_/_/_/_/_/     _/_/_/          /                                  128Mbit  /
--      _/_/_/_/_/_/     _/_/_/         /                   Low Voltage, SPI, 50Mhz /
--      _/_/_/ _/_/_/    _/_/_/        /                                           /
--      _/_/_/  _/_/_/   _/_/_/       /                     VHDL Behavioral Model /
--      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.0 /
--      _/_/_/    _/_/_/ _/_/_/     /                                           /
--      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2008 Numonyx B.V. /
--      _/_/_/      _/_/_/_/_/    /___________________________________________/
--      _/_/_/       _/_/_/_/
--      _/_/          _/_/_/
--
--
--             NUMONYX
--
--
------------------------------------------------------------
-- Here are defined timing data ----------------------------
------------------------------------------------------------

LIBRARY IEEE;
    Use IEEE.std_logic_1164.all;
LIBRARY Work;
    Use work.Def.all;

package TimingData is 


    -----------  Table 8. Power Up Timing and Vwi Threshold --------------
    Constant tVSL       : time :=  60 us;     -- Vcc(min) to Chip Select allowed
    Constant tPUW_min   : time :=   1 ms;     -- Time delay (min) to Write instruction
    Constant tPUW_max   : time :=  10 ms;     -- Time delay (max) to Write instruction


    -----------  Table 14. AC Characteristics  ----------------------
    Constant tCH    : time :=    9 ns;     -- Clock High Time                                           - Min Value
    Constant tCL    : time :=    9 ns;     -- Clock Low Time                                            - Min Value
    Constant tSLCH  : time :=    5 ns;     -- S active Setup Time (relative to C)                       - Min Value
    Constant tCHSL  : time :=    5 ns;     -- S not active Hold Time (relative to C)                    - Min Value
    Constant tDVCH  : time :=    2 ns;     -- Data in Set up time                                       - Min Value
    Constant tCHDX  : time :=    5 ns;     -- Data in Hold time                                         - Min Value
    Constant tCHSH  : time :=    5 ns;     -- S active Hold Time (relative to C)                        - Min Value
    Constant tSHCH  : time :=    5 ns;     -- S not active Set up Time (relative to C)                  - Min Value
    Constant tSHSL  : time :=  100 ns;     -- S Deselected Time                                         - Min Value
    Constant tSHQZ  : time :=    8 ns;     -- Output Disable Time                                       - Max Value
    Constant tCLQV  : time :=    8 ns;     -- Clock Low to Output valid Time                            - Max Value
    Constant tCLQX  : time :=    0 ns;     -- Output Hold Time                                          - Min Value
    Constant tHLCH  : time :=    5 ns;     -- HOLD Setup Time (relative to C)                           - Min Value
    Constant tCHHH  : time :=    5 ns;     -- HOLD Hold Time (relative to C)                            - Min Value
    Constant tHHCH  : time :=    5 ns;     -- HOLD Setup Time (relative to C)                           - Min Value
    Constant tCHHL  : time :=    5 ns;     -- HOLD Hold Time (relative to C)                            - Min Value
    Constant tHHQX  : time :=    8 ns;     -- HOLD to Output Low-Z Time                                 - Max Value
    Constant tHLQZ  : time :=    8 ns;     -- HOLD to Output High-Z Time                                - Max Value
    Constant tWHSL  : time :=   20 ns;     -- Write Protect Setup Time                                  - Min Value
    Constant tSHWL  : time :=  100 ns;     -- Write Protect Hold Time                                   - Min Value
    Constant tVPPHSL: time :=  200 ns;     -- Enhanced Program Supply Voltage High to Chip Select Low   - Min Value 
    Constant tW     : time :=    5 ms;     -- Write Status Register Cycle Time                          - Typ Value
    Constant tPP    : time :=  2.5 ms;     -- Page Program Cycle Time                                   - Typ Value
    Constant tPPH   : time :=  1.2 ms;     -- Page Program Cycle Time                                   - Typ Value
    Constant tSE    : time :=   2 sec;     -- Sector Erase Cycle Time                                   - Typ Value
    Constant tSEH   : time := 1.6 sec;     -- Sector Erase Cycle Time                                   - Typ Value
    Constant tBE    : time := 105 sec;     -- Bulk Erase Cycle Time                                     - Typ Value
    Constant tBEH   : time :=  56 sec;     -- Bulk Erase Cycle Time                                     - Typ Value


    -----------  Long timing constants alias (may be used to reduce simulation delays) ----------------
    Constant PageProgram_time                   : time  :=      tPP; 
    Constant FastPageProgram_time               : time  :=      tPPH; 
    Constant BlockErase_time                    : time  :=      tSE; 
    Constant FastBlockErase_time                : time  :=      tSEH; 
    Constant BulkErase_time                     : time  :=      tBE; 
    Constant FastBulkErase_time                 : time  :=      tBEH; 
    Constant WriteStatusRegister_time           : time  :=       tW; 

end;
