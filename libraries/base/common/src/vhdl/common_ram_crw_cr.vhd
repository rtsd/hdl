-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   -
-- Changed by:
--   D.F. Brouwer
-- Issues:
--   Dual clock support is unavailable for Intel Agilex 7 (agi027_1e1v).
--   See common_ram_rw_rw for more context.

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use work.common_mem_pkg.all;
use technology_lib.technology_select_pkg.all;

entity common_ram_crw_cr is
  generic (
    g_technology : natural := c_tech_select_default;
    g_ram        : t_c_mem := c_mem_ram;
    g_init_file  : string := "UNUSED"
  );
  port (
    -- MM read/write port clock domain
    mm_rst     : in  std_logic := '0';
    mm_clk     : in  std_logic;
    mm_clken   : in  std_logic := '1';
    mm_wr_en   : in  std_logic := '0';
    mm_wr_dat  : in  std_logic_vector(g_ram.dat_w - 1 downto 0) := (others => '0');
    mm_adr     : in  std_logic_vector(g_ram.adr_w - 1 downto 0) := (others => '0');
    mm_rd_en   : in  std_logic := '1';
    mm_rd_dat  : out std_logic_vector(g_ram.dat_w - 1 downto 0);
    mm_rd_val  : out std_logic;

    -- ST read only port clock domain
    st_rst     : in  std_logic := '0';
    st_clk     : in  std_logic;
    st_clken   : in  std_logic := '1';
    st_adr     : in  std_logic_vector(g_ram.adr_w - 1 downto 0) := (others => '0');
    st_rd_en   : in  std_logic := '1';
    st_rd_dat  : out std_logic_vector(g_ram.dat_w - 1 downto 0);
    st_rd_val  : out std_logic
  );
end common_ram_crw_cr;

architecture str of common_ram_crw_cr is
begin
  -- Dual clock domain
  -- Use port a for read/write in MM clock domain
  -- Use port b for read only  in ST clock domain

  u_crw_cr : entity work.common_ram_crw_crw
  generic map (
    g_technology => g_technology,
    g_ram        => g_ram,
    g_init_file  => g_init_file
  )
  port map (
    rst_a     => mm_rst,
    rst_b     => st_rst,
    clk_a     => mm_clk,
    clk_b     => st_clk,
    clken_a   => mm_clken,
    clken_b   => st_clken,
    wr_en_a   => mm_wr_en,
    wr_en_b   => '0',
    wr_dat_a  => mm_wr_dat,
    wr_dat_b  => (others => '0'),
    adr_a     => mm_adr,
    adr_b     => st_adr,
    rd_en_a   => mm_rd_en,
    rd_en_b   => st_rd_en,
    rd_dat_a  => mm_rd_dat,
    rd_dat_b  => st_rd_dat,
    rd_val_a  => mm_rd_val,
    rd_val_b  => st_rd_val
  );
end str;
