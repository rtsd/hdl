#------------------------------------------------------------------------------
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on Qsys-generated file $HDL_WORK/libraries/technology/ip_arria10/ddr4_4g_1600/emif_0_example_design/sim/mentor/msim_setup.tcl.
# 
set IP_DIR "$env(HDL_WORK)/libraries/technology/ip_arria10/ddr4_mem_model_141"

# Assume library work already exists
vmap ed_sim_altera_emif_mem_model_core_ddr4_141 ./work/

# Compile the design files in correct order and map them all to library work

#------------------------------------------------------------------------------
# generic DDR memory model core files
#------------------------------------------------------------------------------
# . Readable files
vlog -sv "$IP_DIR/sim/ddr4_core/altera_emif_ddrx_model.sv"               -work work
vlog -sv "$IP_DIR/sim/ddr4_core/altera_emif_ddrx_model_per_ping_pong.sv" -work work
vlog -sv "$IP_DIR/sim/ddr4_core/altera_emif_ddrx_model_per_device.sv"    -work work
vlog -sv "$IP_DIR/sim/ddr4_core/altera_emif_ddr4_crc_tree.sv"            -work work
vlog -sv "$IP_DIR/sim/ddr4_core/altera_emif_ddrx_model_rank.sv"          -work work
vlog -sv "$IP_DIR/sim/ddr4_core/altera_emif_ddr4_model_rcd_chip.sv"      -work work
vlog -sv "$IP_DIR/sim/ddr4_core/altera_emif_ddrx_model_bidir_delay.sv"   -work work
# . Binary files
#vlog -sv "$IP_DIR/sim/ddr4_core/mentor/altera_emif_ddrx_model.sv"               -work work
#vlog -sv "$IP_DIR/sim/ddr4_core/mentor/altera_emif_ddrx_model_per_ping_pong.sv" -work work
#vlog -sv "$IP_DIR/sim/ddr4_core/mentor/altera_emif_ddrx_model_per_device.sv"    -work work
#vlog -sv "$IP_DIR/sim/ddr4_core/mentor/altera_emif_ddr4_crc_tree.sv"            -work work
#vlog -sv "$IP_DIR/sim/ddr4_core/mentor/altera_emif_ddrx_model_rank.sv"          -work work
#vlog -sv "$IP_DIR/sim/ddr4_core/mentor/altera_emif_ddr4_model_rcd_chip.sv"      -work work
#vlog -sv "$IP_DIR/sim/ddr4_core/mentor/altera_emif_ddrx_model_bidir_delay.sv"   -work work 


#------------------------------------------------------------------------------
# Size specific DDR component files
#------------------------------------------------------------------------------

# . ddr4_4g_1600
vcom     "$IP_DIR/sim/ddr4_4g_1600/ed_sim_altera_emif_mem_model_141_z3tvrmq.vhd"             -work work

# . ddr4_8g_2400
vcom     "$IP_DIR/sim/ddr4_8g_2400/ed_sim_altera_emif_mem_model_141_oes36qy.vhd"             -work work