-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_frame_scheduler is
end tb_tb_dp_frame_scheduler;

architecture tb of tb_tb_dp_frame_scheduler is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 10
  -- > run 12 ms --> OK

  -- Try scheduler settings : GENERIC MAP (g_dut_verify_mux => , g_dut_fifo_rl => )

  u_mux             : entity work.tb_dp_frame_scheduler generic map (true,  1);

  u_scheduler       : entity work.tb_dp_frame_scheduler generic map (false, 1);
  u_scheduler_rl_0  : entity work.tb_dp_frame_scheduler generic map (false, 0);
end tb;
