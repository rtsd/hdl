-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Map the fields of network headers on to a total header array of words
-- Description:
--
-- * 32b, word align = 2 octets
--         wi: |0  1  2  3 |4  5  6  7  8 | 9 10 | ...
--    octet:   |---------------------------------|---------
--         0   |x          |              |      |
--         1   |x   eth    |   ipv4       | udp  | udp payload
--         2   |    14     |   20         |  8   |
--         3   |           |              |      |
--
-- * 64b, word align = 6 octets
--         wi: |0  1  2 |3  4 |5 | ...
--    octet:   |-----------------|---------
--         0   |x       |     |  |
--         1   |x  eth  |ipv4 |u | udp payload
--         2   |x  14   | 20  |d |
--         3   |x    ___|     |p |
--         4   |x   |         |8 |
--         5   |x   |         |  |
--         6   |    |         |  |
--         7   |    |         |  |
--
-- The word align separates the network total header and the udp payload on a word boundary. This allows the network
-- total header to be handled as a t_network_total_header_<word width>_arr. It is also useful for the data path
-- application that can handle the UDP payload on word boundaries. For output to the MAC the word align can be removed
-- using dp_pad_remove. For input from the MAC the word align can be inserted using dp_pad_insert.
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_network_layers_pkg.all;

package common_network_total_header_pkg is
  -- Define total network header that fits all relevant packets in common_network_layers_pkg, because they have the same total header length
  constant c_network_total_header_arp_len   : natural := c_network_eth_header_len + c_network_arp_data_len;  -- = 14 + 28     = 42
  constant c_network_total_header_icmp_len  : natural := c_network_eth_header_len + c_network_ip_header_len + c_network_icmp_header_len;  -- = 14 + 20 + 8 = 42
  constant c_network_total_header_udp_len   : natural := c_network_eth_header_len + c_network_ip_header_len + c_network_udp_header_len;  -- = 14 + 20 + 8 = 42

  constant c_network_total_header_len       : natural := 42;

  -----------------------------------------------------------------------------
  -- Aggregate all supported network headers into one record
  -----------------------------------------------------------------------------

  type t_network_total_header is record
    eth  : t_network_eth_header;
    arp  : t_network_arp_packet;
    ip   : t_network_ip_header;
    icmp : t_network_icmp_header;
    udp  : t_network_udp_header;
  end record;

  constant c_network_total_header_ones : t_network_total_header := (c_network_eth_header_ones,
                                                                    c_network_arp_packet_ones,
                                                                    c_network_ip_header_ones,
                                                                    c_network_icmp_header_ones,
                                                                    c_network_udp_header_ones);

  -----------------------------------------------------------------------------
  -- Map total network header in words array
  -----------------------------------------------------------------------------

  constant c_network_total_header_32b_align_len : natural := 2;  -- to align eth, ip and udp payload to 32 bit boundaries
  constant c_network_total_header_32b_align_w   : natural := c_network_total_header_32b_align_len * c_8;

  constant c_network_total_header_64b_align_len : natural := 6;  -- to align eth, ip and udp payload to 32 bit boundaries and
  constant c_network_total_header_64b_align_w   : natural := c_network_total_header_64b_align_len * c_8;  -- udp payload to 64 bit boundaries

  constant c_network_total_header_32b_nof_words       : natural := (c_network_total_header_32b_align_len + c_network_total_header_len) / 4;  -- = 44 / c_word_sz     = 11
  constant c_network_total_header_64b_nof_words       : natural := (c_network_total_header_64b_align_len + c_network_total_header_len) / 8;  -- = 48 / c_longword_sz = 6

  type t_network_total_header_32b_arr is array(0 to c_network_total_header_32b_nof_words - 1) of std_logic_vector(c_32 - 1 downto 0);
  type t_network_total_header_64b_arr is array(0 to c_network_total_header_64b_nof_words - 1) of std_logic_vector(c_64 - 1 downto 0);

  -- Word indices in the total header array to know when the field in the mapped record is valid
  -- . 32b
  constant c_network_total_header_32b_eth_lo_wi              : natural := 0;  -- first word index
  constant c_network_total_header_32b_eth_dst_mac_wi         : natural := 1;
  constant c_network_total_header_32b_eth_src_mac_wi         : natural := 3;
  constant c_network_total_header_32b_eth_type_wi            : natural := 3;
  constant c_network_total_header_32b_eth_hi_wi              : natural := 3;  -- last word index
  constant c_network_total_header_32b_eth_nof_words          : natural := c_network_total_header_32b_eth_hi_wi - c_network_total_header_32b_eth_lo_wi + 1;

  constant c_network_total_header_32b_ip_lo_wi               : natural := 4;  -- first word index
  constant c_network_total_header_32b_ip_version_wi          : natural := 4;
  constant c_network_total_header_32b_ip_header_length_wi    : natural := 4;
  constant c_network_total_header_32b_ip_services_wi         : natural := 4;
  constant c_network_total_header_32b_ip_total_length_wi     : natural := 4;
  constant c_network_total_header_32b_ip_identification_wi   : natural := 5;
  constant c_network_total_header_32b_ip_flags_wi            : natural := 5;
  constant c_network_total_header_32b_ip_fragment_offset_wi  : natural := 5;
  constant c_network_total_header_32b_ip_time_to_live_wi     : natural := 6;
  constant c_network_total_header_32b_ip_protocol_wi         : natural := 6;
  constant c_network_total_header_32b_ip_header_checksum_wi  : natural := 6;
  constant c_network_total_header_32b_ip_src_ip_addr_wi      : natural := 7;
  constant c_network_total_header_32b_ip_dst_ip_addr_wi      : natural := 8;
  constant c_network_total_header_32b_ip_hi_wi               : natural := 8;  -- last word index
  constant c_network_total_header_32b_ip_nof_words           : natural := c_network_total_header_32b_ip_hi_wi - c_network_total_header_32b_ip_lo_wi + 1;

  constant c_network_total_header_32b_arp_lo_wi              : natural := 4;  -- first word index
  constant c_network_total_header_32b_arp_htype_wi           : natural := 4;
  constant c_network_total_header_32b_arp_ptype_wi           : natural := 4;
  constant c_network_total_header_32b_arp_hlen_wi            : natural := 5;
  constant c_network_total_header_32b_arp_plen_wi            : natural := 5;
  constant c_network_total_header_32b_arp_oper_wi            : natural := 5;
  constant c_network_total_header_32b_arp_sha_wi             : natural := 7;
  constant c_network_total_header_32b_arp_spa_wi             : natural := 8;
  constant c_network_total_header_32b_arp_tha_wi             : natural := 9;
  constant c_network_total_header_32b_arp_tpa_wi             : natural := 10;
  constant c_network_total_header_32b_arp_hi_wi              : natural := 10;  -- last word index
  constant c_network_total_header_32b_arp_nof_words          : natural := c_network_total_header_32b_arp_hi_wi - c_network_total_header_32b_arp_lo_wi + 1;

  constant c_network_total_header_32b_icmp_lo_wi             : natural := 9;  -- first word index
  constant c_network_total_header_32b_icmp_msg_type_wi       : natural := 9;
  constant c_network_total_header_32b_icmp_code_wi           : natural := 9;
  constant c_network_total_header_32b_icmp_checksum_wi       : natural := 9;
  constant c_network_total_header_32b_icmp_id_wi             : natural := 10;
  constant c_network_total_header_32b_icmp_sequence_wi       : natural := 10;
  constant c_network_total_header_32b_icmp_hi_wi             : natural := 10;  -- last word index
  constant c_network_total_header_32b_icmp_nof_words         : natural := c_network_total_header_32b_icmp_hi_wi - c_network_total_header_32b_icmp_lo_wi + 1;

  constant c_network_total_header_32b_udp_lo_wi              : natural := 9;  -- first word index
  constant c_network_total_header_32b_udp_src_port_wi        : natural := 9;
  constant c_network_total_header_32b_udp_dst_port_wi        : natural := 9;
  constant c_network_total_header_32b_udp_total_length_wi    : natural := 10;
  constant c_network_total_header_32b_udp_checksum_wi        : natural := 10;
  constant c_network_total_header_32b_udp_hi_wi              : natural := 10;  -- last word index
  constant c_network_total_header_32b_udp_nof_words          : natural := c_network_total_header_32b_udp_hi_wi - c_network_total_header_32b_udp_lo_wi + 1;

  -- . 64b
  constant c_network_total_header_64b_eth_lo_wi              : natural := 0;  -- first word index
  constant c_network_total_header_64b_eth_dst_mac_wi         : natural := 1;
  constant c_network_total_header_64b_eth_src_mac_wi         : natural := 2;
  constant c_network_total_header_64b_eth_type_wi            : natural := 2;
  constant c_network_total_header_64b_eth_hi_wi              : natural := 2;  -- last word index
  constant c_network_total_header_64b_eth_nof_words          : natural := c_network_total_header_64b_eth_hi_wi - c_network_total_header_64b_eth_lo_wi + 1;

  constant c_network_total_header_64b_ip_lo_wi               : natural := 2;  -- first word index
  constant c_network_total_header_64b_ip_version_wi          : natural := 2;
  constant c_network_total_header_64b_ip_header_length_wi    : natural := 2;
  constant c_network_total_header_64b_ip_services_wi         : natural := 2;
  constant c_network_total_header_64b_ip_total_length_wi     : natural := 2;
  constant c_network_total_header_64b_ip_identification_wi   : natural := 3;
  constant c_network_total_header_64b_ip_flags_wi            : natural := 3;
  constant c_network_total_header_64b_ip_fragment_offset_wi  : natural := 3;
  constant c_network_total_header_64b_ip_time_to_live_wi     : natural := 3;
  constant c_network_total_header_64b_ip_protocol_wi         : natural := 3;
  constant c_network_total_header_64b_ip_header_checksum_wi  : natural := 3;
  constant c_network_total_header_64b_ip_src_ip_addr_wi      : natural := 4;
  constant c_network_total_header_64b_ip_dst_ip_addr_wi      : natural := 4;
  constant c_network_total_header_64b_ip_hi_wi               : natural := 4;  -- last word index
  constant c_network_total_header_64b_ip_nof_words           : natural := c_network_total_header_64b_ip_hi_wi - c_network_total_header_64b_ip_lo_wi + 1;

  constant c_network_total_header_64b_arp_lo_wi              : natural := 2;  -- first word index
  constant c_network_total_header_64b_arp_htype_wi           : natural := 2;
  constant c_network_total_header_64b_arp_ptype_wi           : natural := 2;
  constant c_network_total_header_64b_arp_hlen_wi            : natural := 3;
  constant c_network_total_header_64b_arp_plen_wi            : natural := 3;
  constant c_network_total_header_64b_arp_oper_wi            : natural := 3;
  constant c_network_total_header_64b_arp_sha_wi             : natural := 4;
  constant c_network_total_header_64b_arp_spa_wi             : natural := 4;
  constant c_network_total_header_64b_arp_tha_wi             : natural := 5;
  constant c_network_total_header_64b_arp_tpa_wi             : natural := 5;
  constant c_network_total_header_64b_arp_hi_wi              : natural := 5;  -- last word index
  constant c_network_total_header_64b_arp_nof_words          : natural := c_network_total_header_64b_arp_hi_wi - c_network_total_header_64b_arp_lo_wi + 1;

  constant c_network_total_header_64b_icmp_lo_wi             : natural := 5;  -- first word index
  constant c_network_total_header_64b_icmp_msg_type_wi       : natural := 5;
  constant c_network_total_header_64b_icmp_code_wi           : natural := 5;
  constant c_network_total_header_64b_icmp_checksum_wi       : natural := 5;
  constant c_network_total_header_64b_icmp_id_wi             : natural := 5;
  constant c_network_total_header_64b_icmp_sequence_wi       : natural := 5;
  constant c_network_total_header_64b_icmp_hi_wi             : natural := 5;  -- last word index
  constant c_network_total_header_64b_icmp_nof_words         : natural := c_network_total_header_64b_icmp_hi_wi - c_network_total_header_64b_icmp_lo_wi + 1;

  constant c_network_total_header_64b_udp_lo_wi              : natural := 5;  -- first word index
  constant c_network_total_header_64b_udp_src_port_wi        : natural := 5;
  constant c_network_total_header_64b_udp_dst_port_wi        : natural := 5;
  constant c_network_total_header_64b_udp_total_length_wi    : natural := 5;
  constant c_network_total_header_64b_udp_checksum_wi        : natural := 5;
  constant c_network_total_header_64b_udp_hi_wi              : natural := 5;  -- last word index
  constant c_network_total_header_64b_udp_nof_words          : natural := c_network_total_header_64b_udp_hi_wi - c_network_total_header_64b_udp_lo_wi + 1;

  -----------------------------------------------------------------------------
  -- Functions to map between header record fields and header words array
  -----------------------------------------------------------------------------

  -- Combinatorial map of the total header array on to a network header record (type casting an array to a record is not possible, so therefore we need these functions)
  function func_network_total_header_extract_eth( hdr_arr : t_network_total_header_32b_arr) return t_network_eth_header;
  function func_network_total_header_extract_eth( hdr_arr : t_network_total_header_64b_arr) return t_network_eth_header;
  function func_network_total_header_extract_ip(  hdr_arr : t_network_total_header_32b_arr) return t_network_ip_header;
  function func_network_total_header_extract_ip(  hdr_arr : t_network_total_header_64b_arr) return t_network_ip_header;
  function func_network_total_header_extract_arp( hdr_arr : t_network_total_header_32b_arr) return t_network_arp_packet;
  function func_network_total_header_extract_arp( hdr_arr : t_network_total_header_64b_arr) return t_network_arp_packet;
  function func_network_total_header_extract_icmp(hdr_arr : t_network_total_header_32b_arr) return t_network_icmp_header;
  function func_network_total_header_extract_icmp(hdr_arr : t_network_total_header_64b_arr) return t_network_icmp_header;
  function func_network_total_header_extract_udp( hdr_arr : t_network_total_header_32b_arr) return t_network_udp_header;
  function func_network_total_header_extract_udp( hdr_arr : t_network_total_header_64b_arr) return t_network_udp_header;

  -- Combinatorial map just as above but for network packets without word align field.
  function func_network_total_header_no_align_extract_eth( hdr_arr : t_network_total_header_32b_arr) return t_network_eth_header;
  function func_network_total_header_no_align_extract_eth( hdr_arr : t_network_total_header_64b_arr) return t_network_eth_header;
  function func_network_total_header_no_align_extract_ip(  hdr_arr : t_network_total_header_32b_arr) return t_network_ip_header;
  function func_network_total_header_no_align_extract_ip(  hdr_arr : t_network_total_header_64b_arr) return t_network_ip_header;
  function func_network_total_header_no_align_extract_arp( hdr_arr : t_network_total_header_32b_arr) return t_network_arp_packet;
  function func_network_total_header_no_align_extract_arp( hdr_arr : t_network_total_header_64b_arr) return t_network_arp_packet;
  function func_network_total_header_no_align_extract_icmp(hdr_arr : t_network_total_header_32b_arr) return t_network_icmp_header;
  function func_network_total_header_no_align_extract_icmp(hdr_arr : t_network_total_header_64b_arr) return t_network_icmp_header;
  function func_network_total_header_no_align_extract_udp( hdr_arr : t_network_total_header_32b_arr) return t_network_udp_header;
  function func_network_total_header_no_align_extract_udp( hdr_arr : t_network_total_header_64b_arr) return t_network_udp_header;

  -- Construct the total header array from the individual header records
  function func_network_total_header_construct_eth( eth : t_network_eth_header)                                                          return t_network_total_header_32b_arr;  -- sets unused words to zero
  function func_network_total_header_construct_eth( eth : t_network_eth_header)                                                          return t_network_total_header_64b_arr;  -- sets unused words to zero
  function func_network_total_header_construct_arp( eth : t_network_eth_header; arp : t_network_arp_packet)                              return t_network_total_header_32b_arr;
  function func_network_total_header_construct_arp( eth : t_network_eth_header; arp : t_network_arp_packet)                              return t_network_total_header_64b_arr;
  function func_network_total_header_construct_ip(  eth : t_network_eth_header; ip  : t_network_ip_header)                               return t_network_total_header_32b_arr;  -- sets unused words to zero
  function func_network_total_header_construct_ip(  eth : t_network_eth_header; ip  : t_network_ip_header)                               return t_network_total_header_64b_arr;  -- sets unused words to zero
  function func_network_total_header_construct_icmp(eth : t_network_eth_header; ip  : t_network_ip_header; icmp : t_network_icmp_header) return t_network_total_header_32b_arr;
  function func_network_total_header_construct_icmp(eth : t_network_eth_header; ip  : t_network_ip_header; icmp : t_network_icmp_header) return t_network_total_header_64b_arr;
  function func_network_total_header_construct_udp( eth : t_network_eth_header; ip  : t_network_ip_header; udp  : t_network_udp_header)  return t_network_total_header_32b_arr;
  function func_network_total_header_construct_udp( eth : t_network_eth_header; ip  : t_network_ip_header; udp  : t_network_udp_header)  return t_network_total_header_64b_arr;

  -- Construct the response total header array for a total header array
  function func_network_total_header_response_eth( eth_arr  : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr;
  function func_network_total_header_response_eth( eth_arr  : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr;
  function func_network_total_header_response_arp( arp_arr  : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
                                                                                              ip_addr  : std_logic_vector(c_network_ip_addr_w - 1 downto 0))
                                                                                              return t_network_total_header_32b_arr;
  function func_network_total_header_response_arp( arp_arr  : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
                                                                                              ip_addr  : std_logic_vector(c_network_ip_addr_w - 1 downto 0))
                                                                                              return t_network_total_header_64b_arr;
  function func_network_total_header_response_ip(  ip_arr   : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr;
  function func_network_total_header_response_ip(  ip_arr   : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr;
  function func_network_total_header_response_icmp(icmp_arr : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr;
  function func_network_total_header_response_icmp(icmp_arr : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr;
  function func_network_total_header_response_udp( udp_arr  : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr;
  function func_network_total_header_response_udp( udp_arr  : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr;

  -- Construct the response total header array for a total header array without alignment bytes
  function func_network_total_header_no_align_response_eth( eth_arr  : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr;
  function func_network_total_header_no_align_response_eth( eth_arr  : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr;
  function func_network_total_header_no_align_response_arp( arp_arr  : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
                                                                                              ip_addr  : std_logic_vector(c_network_ip_addr_w - 1 downto 0))
                                                                                              return t_network_total_header_32b_arr;
  function func_network_total_header_no_align_response_arp( arp_arr  : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
                                                                                              ip_addr  : std_logic_vector(c_network_ip_addr_w - 1 downto 0))
                                                                                              return t_network_total_header_64b_arr;
  function func_network_total_header_no_align_response_ip(  ip_arr   : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr;
  function func_network_total_header_no_align_response_ip(  ip_arr   : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr;
  function func_network_total_header_no_align_response_icmp(icmp_arr : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr;
  function func_network_total_header_no_align_response_icmp(icmp_arr : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr;
  function func_network_total_header_no_align_response_udp( udp_arr  : t_network_total_header_32b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr;
  function func_network_total_header_no_align_response_udp( udp_arr  : t_network_total_header_64b_arr; mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr;
end common_network_total_header_pkg;

package body common_network_total_header_pkg is
  -- Assume the total header has been padded with the word align field to have the udp payload at a 32b or 64b boundary
  -- Map the 11 32b words or 6 64b longwords from the total header to the header field records

  function func_network_total_header_extract_eth(hdr_arr : t_network_total_header_32b_arr) return t_network_eth_header is
    variable v_hdr : t_network_eth_header;
  begin
    --                             hdr_arr(0)(31 DOWNTO 16)  -- ignore word align field
    v_hdr.dst_mac(47 downto 32) := hdr_arr(0)(15 downto  0);
    v_hdr.dst_mac(31 downto  0) := hdr_arr(1);
    v_hdr.src_mac(47 downto 16) := hdr_arr(2);
    v_hdr.src_mac(15 downto  0) := hdr_arr(3)(31 downto 16);
    v_hdr.eth_type              := hdr_arr(3)(15 downto  0);
    return v_hdr;
  end;

  function func_network_total_header_extract_eth(hdr_arr : t_network_total_header_64b_arr) return t_network_eth_header is
    variable v_hdr : t_network_eth_header;
  begin
    --                             hdr_arr(0)(63 DOWNTO 16)  -- ignore word align field
    v_hdr.dst_mac(47 downto 32) := hdr_arr(0)(15 downto  0);
    v_hdr.dst_mac(31 downto  0) := hdr_arr(1)(63 downto 32);
    v_hdr.src_mac(47 downto 16) := hdr_arr(1)(31 downto  0);
    v_hdr.src_mac(15 downto  0) := hdr_arr(2)(63 downto 48);
    v_hdr.eth_type              := hdr_arr(2)(47 downto 32);
    return v_hdr;
  end;

  function func_network_total_header_extract_ip(hdr_arr : t_network_total_header_32b_arr) return t_network_ip_header is
    variable v_hdr : t_network_ip_header;
  begin
    v_hdr.version            := hdr_arr(4)(31 downto 28);
    v_hdr.header_length      := hdr_arr(4)(27 downto 24);
    v_hdr.services           := hdr_arr(4)(23 downto 16);
    v_hdr.total_length       := hdr_arr(4)(15 downto  0);
    v_hdr.identification     := hdr_arr(5)(31 downto 16);
    v_hdr.flags              := hdr_arr(5)(15 downto 13);
    v_hdr.fragment_offset    := hdr_arr(5)(12 downto  0);
    v_hdr.time_to_live       := hdr_arr(6)(31 downto 24);
    v_hdr.protocol           := hdr_arr(6)(23 downto 16);
    v_hdr.header_checksum    := hdr_arr(6)(15 downto  0);
    v_hdr.src_ip_addr        := hdr_arr(7);
    v_hdr.dst_ip_addr        := hdr_arr(8);
    return v_hdr;
  end;

  function func_network_total_header_extract_ip(hdr_arr : t_network_total_header_64b_arr) return t_network_ip_header is
    variable v_hdr : t_network_ip_header;
  begin
    v_hdr.version            := hdr_arr(2)(31 downto 28);
    v_hdr.header_length      := hdr_arr(2)(27 downto 24);
    v_hdr.services           := hdr_arr(2)(23 downto 16);
    v_hdr.total_length       := hdr_arr(2)(15 downto  0);
    v_hdr.identification     := hdr_arr(3)(63 downto 48);
    v_hdr.flags              := hdr_arr(3)(47 downto 45);
    v_hdr.fragment_offset    := hdr_arr(3)(44 downto 32);
    v_hdr.time_to_live       := hdr_arr(3)(31 downto 24);
    v_hdr.protocol           := hdr_arr(3)(23 downto 16);
    v_hdr.header_checksum    := hdr_arr(3)(15 downto  0);
    v_hdr.src_ip_addr        := hdr_arr(4)(63 downto 32);
    v_hdr.dst_ip_addr        := hdr_arr(4)(31 downto  0);
    return v_hdr;
  end;

  function func_network_total_header_extract_arp(hdr_arr : t_network_total_header_32b_arr) return t_network_arp_packet is
    variable v_arp : t_network_arp_packet;
  begin
    v_arp.htype              := hdr_arr(4)(31 downto 16);
    v_arp.ptype              := hdr_arr(4)(15 downto  0);
    v_arp.hlen               := hdr_arr(5)(31 downto 24);
    v_arp.plen               := hdr_arr(5)(23 downto 16);
    v_arp.oper               := hdr_arr(5)(15 downto  0);
    v_arp.sha(47 downto 16)  := hdr_arr(6);
    v_arp.sha(15 downto  0)  := hdr_arr(7)(31 downto 16);
    v_arp.spa(31 downto 16)  := hdr_arr(7)(15 downto  0);
    v_arp.spa(15 downto  0)  := hdr_arr(8)(31 downto 16);
    v_arp.tha(47 downto 32)  := hdr_arr(8)(15 downto  0);
    v_arp.tha(31 downto  0)  := hdr_arr(9);
    v_arp.tpa                := hdr_arr(10);
    return v_arp;
  end;

  function func_network_total_header_extract_arp(hdr_arr : t_network_total_header_64b_arr) return t_network_arp_packet is
    variable v_arp : t_network_arp_packet;
  begin
    v_arp.htype              := hdr_arr(2)(31 downto 16);
    v_arp.ptype              := hdr_arr(2)(15 downto  0);
    v_arp.hlen               := hdr_arr(3)(63 downto 56);
    v_arp.plen               := hdr_arr(3)(55 downto 48);
    v_arp.oper               := hdr_arr(3)(47 downto 32);
    v_arp.sha(47 downto 16)  := hdr_arr(3)(31 downto  0);
    v_arp.sha(15 downto  0)  := hdr_arr(4)(63 downto 48);
    v_arp.spa(31 downto 16)  := hdr_arr(4)(47 downto 32);
    v_arp.spa(15 downto  0)  := hdr_arr(4)(31 downto 16);
    v_arp.tha(47 downto 32)  := hdr_arr(4)(15 downto  0);
    v_arp.tha(31 downto  0)  := hdr_arr(5)(63 downto 32);
    v_arp.tpa                := hdr_arr(5)(31 downto  0);
    return v_arp;
  end;

  function func_network_total_header_extract_icmp(hdr_arr : t_network_total_header_32b_arr) return t_network_icmp_header is
    variable v_hdr : t_network_icmp_header;
  begin
    v_hdr.msg_type  := hdr_arr(9)(31 downto 24);
    v_hdr.code      := hdr_arr(9)(23 downto 16);
    v_hdr.checksum  := hdr_arr(9)(15 downto  0);
    v_hdr.id        := hdr_arr(10)(31 downto 16);
    v_hdr.sequence  := hdr_arr(10)(15 downto  0);
    return v_hdr;
  end;

  function func_network_total_header_extract_icmp(hdr_arr : t_network_total_header_64b_arr) return t_network_icmp_header is
    variable v_hdr : t_network_icmp_header;
  begin
    v_hdr.msg_type  := hdr_arr(5)(63 downto 56);
    v_hdr.code      := hdr_arr(5)(55 downto 48);
    v_hdr.checksum  := hdr_arr(5)(47 downto 32);
    v_hdr.id        := hdr_arr(5)(31 downto 16);
    v_hdr.sequence  := hdr_arr(5)(15 downto  0);
    return v_hdr;
  end;

  function func_network_total_header_extract_udp(hdr_arr : t_network_total_header_32b_arr) return t_network_udp_header is
    variable v_hdr : t_network_udp_header;
  begin
    v_hdr.src_port            := hdr_arr(9)(31 downto 16);
    v_hdr.dst_port            := hdr_arr(9)(15 downto  0);
    v_hdr.total_length        := hdr_arr(10)(31 downto 16);
    v_hdr.checksum            := hdr_arr(10)(15 downto  0);
    return v_hdr;
  end;

  function func_network_total_header_extract_udp(hdr_arr : t_network_total_header_64b_arr) return t_network_udp_header is
    variable v_hdr : t_network_udp_header;
  begin
    v_hdr.src_port            := hdr_arr(5)(63 downto 48);
    v_hdr.dst_port            := hdr_arr(5)(47 downto 32);
    v_hdr.total_length        := hdr_arr(5)(31 downto 16);
    v_hdr.checksum            := hdr_arr(5)(15 downto  0);
    return v_hdr;
  end;

  -- Assume the total header has NOT been padded with the word align field
  -- Map the 11 32b words or 6 64b longwords from the total header to the header field records

  function func_network_total_header_no_align_extract_eth(hdr_arr : t_network_total_header_32b_arr) return t_network_eth_header is
    variable v_hdr : t_network_eth_header;
  begin
    v_hdr.dst_mac(47 downto 16) := hdr_arr(0);
    v_hdr.dst_mac(15 downto  0) := hdr_arr(1)(31 downto 16);
    v_hdr.src_mac(47 downto 32) := hdr_arr(1)(15 downto  0);
    v_hdr.src_mac(31 downto  0) := hdr_arr(2);
    v_hdr.eth_type              := hdr_arr(3)(31 downto  16);
    return v_hdr;
  end;

  function func_network_total_header_no_align_extract_eth(hdr_arr : t_network_total_header_64b_arr) return t_network_eth_header is
    variable v_hdr : t_network_eth_header;
  begin
    v_hdr.dst_mac               := hdr_arr(0)(63 downto 16);
    v_hdr.src_mac(47 downto 32) := hdr_arr(0)(15 downto  0);
    v_hdr.src_mac(31 downto  0) := hdr_arr(1)(63 downto 32);
    v_hdr.eth_type              := hdr_arr(1)(31 downto 16);
    return v_hdr;
  end;

  function func_network_total_header_no_align_extract_ip(hdr_arr : t_network_total_header_32b_arr) return t_network_ip_header is
    variable v_hdr : t_network_ip_header;
  begin
    v_hdr.version                   := hdr_arr(3)(15 downto 12);
    v_hdr.header_length             := hdr_arr(3)(11 downto  8);
    v_hdr.services                  := hdr_arr(3)( 7 downto  0);
    v_hdr.total_length              := hdr_arr(4)(31 downto 16);
    v_hdr.identification            := hdr_arr(4)(15 downto  0);
    v_hdr.flags                     := hdr_arr(5)(31 downto 29);
    v_hdr.fragment_offset           := hdr_arr(5)(28 downto 16);
    v_hdr.time_to_live              := hdr_arr(5)(15 downto  8);
    v_hdr.protocol                  := hdr_arr(5)( 7 downto  0);
    v_hdr.header_checksum           := hdr_arr(6)(31 downto 16);
    v_hdr.src_ip_addr(31 downto 16) := hdr_arr(6)(15 downto  0);
    v_hdr.src_ip_addr(15 downto  0) := hdr_arr(7)(31 downto 16);
    v_hdr.dst_ip_addr(31 downto 16) := hdr_arr(7)(15 downto  0);
    v_hdr.dst_ip_addr(15 downto  0) := hdr_arr(8)(31 downto 16);
    return v_hdr;
  end;

  function func_network_total_header_no_align_extract_ip(hdr_arr : t_network_total_header_64b_arr) return t_network_ip_header is
    variable v_hdr : t_network_ip_header;
  begin
    v_hdr.version            := hdr_arr(1)(15 downto 12);
    v_hdr.header_length      := hdr_arr(1)(11 downto  8);
    v_hdr.services           := hdr_arr(1)( 7 downto  0);
    v_hdr.total_length       := hdr_arr(2)(63 downto 48);
    v_hdr.identification     := hdr_arr(2)(47 downto 32);
    v_hdr.flags              := hdr_arr(2)(31 downto 29);
    v_hdr.fragment_offset    := hdr_arr(2)(28 downto 16);
    v_hdr.time_to_live       := hdr_arr(2)(15 downto  8);
    v_hdr.protocol           := hdr_arr(2)( 7 downto  0);
    v_hdr.header_checksum    := hdr_arr(3)(63 downto 48);
    v_hdr.src_ip_addr        := hdr_arr(3)(47 downto 16);
    v_hdr.dst_ip_addr(31 downto 16) := hdr_arr(3)(15 downto  0);
    v_hdr.dst_ip_addr(15 downto  0) := hdr_arr(4)(63 downto 48);
    return v_hdr;
  end;

  function func_network_total_header_no_align_extract_arp(hdr_arr : t_network_total_header_32b_arr) return t_network_arp_packet is
    variable v_arp : t_network_arp_packet;
  begin
    v_arp.htype              := hdr_arr(3)(15 downto  0);
    v_arp.ptype              := hdr_arr(4)(31 downto 16);
    v_arp.hlen               := hdr_arr(4)(15 downto  8);
    v_arp.plen               := hdr_arr(4)( 7 downto  0);
    v_arp.oper               := hdr_arr(5)(31 downto 16);
    v_arp.sha(47 downto 32)  := hdr_arr(5)(15 downto  0);
    v_arp.sha(31 downto  0)  := hdr_arr(6);
    v_arp.spa                := hdr_arr(7);
    v_arp.tha(47 downto 16)  := hdr_arr(8);
    v_arp.tha(15 downto  0)  := hdr_arr(9)(31 downto 16);
    v_arp.tpa(31 downto 16)  := hdr_arr(9)(15 downto  0);
    v_arp.tpa(15 downto  0)  := hdr_arr(9)(31 downto 16);
    return v_arp;
  end;

  function func_network_total_header_no_align_extract_arp(hdr_arr : t_network_total_header_64b_arr) return t_network_arp_packet is
    variable v_arp : t_network_arp_packet;
  begin
    v_arp.htype              := hdr_arr(1)(15 downto  0);
    v_arp.ptype              := hdr_arr(2)(63 downto 48);
    v_arp.hlen               := hdr_arr(2)(47 downto 40);
    v_arp.plen               := hdr_arr(2)(39 downto 32);
    v_arp.oper               := hdr_arr(2)(31 downto 16);
    v_arp.sha(47 downto 32)  := hdr_arr(2)(15 downto  0);
    v_arp.sha(31 downto  0)  := hdr_arr(3)(63 downto 32);
    v_arp.spa                := hdr_arr(3)(31 downto  0);
    v_arp.tha                := hdr_arr(4)(63 downto 16);
    v_arp.tpa(31 downto 16)  := hdr_arr(4)(15 downto  0);
    v_arp.tpa(15 downto  0)  := hdr_arr(5)(63 downto 48);
    return v_arp;
  end;

  function func_network_total_header_no_align_extract_icmp(hdr_arr : t_network_total_header_32b_arr) return t_network_icmp_header is
    variable v_hdr : t_network_icmp_header;
  begin
    v_hdr.msg_type  := hdr_arr(8)(15 downto  8);
    v_hdr.code      := hdr_arr(8)( 7 downto  0);
    v_hdr.checksum  := hdr_arr(9)(31 downto 16);
    v_hdr.id        := hdr_arr(9)(15 downto  0);
    v_hdr.sequence  := hdr_arr(10)(31 downto 16);
    return v_hdr;
  end;

  function func_network_total_header_no_align_extract_icmp(hdr_arr : t_network_total_header_64b_arr) return t_network_icmp_header is
    variable v_hdr : t_network_icmp_header;
  begin
    v_hdr.msg_type  := hdr_arr(4)(47 downto 40);
    v_hdr.code      := hdr_arr(4)(39 downto 32);
    v_hdr.checksum  := hdr_arr(4)(31 downto 16);
    v_hdr.id        := hdr_arr(4)(15 downto  0);
    v_hdr.sequence  := hdr_arr(5)(63 downto 48);
    return v_hdr;
  end;

  function func_network_total_header_no_align_extract_udp(hdr_arr : t_network_total_header_32b_arr) return t_network_udp_header is
    variable v_hdr : t_network_udp_header;
  begin
    v_hdr.src_port            := hdr_arr(8)(15 downto  0);
    v_hdr.dst_port            := hdr_arr(9)(31 downto 16);
    v_hdr.total_length        := hdr_arr(9)(15 downto  0);
    v_hdr.checksum            := hdr_arr(10)(31 downto 16);
    return v_hdr;
  end;

  function func_network_total_header_no_align_extract_udp(hdr_arr : t_network_total_header_64b_arr) return t_network_udp_header is
    variable v_hdr : t_network_udp_header;
  begin
    v_hdr.src_port            := hdr_arr(4)(47 downto 32);
    v_hdr.dst_port            := hdr_arr(4)(31 downto 16);
    v_hdr.total_length        := hdr_arr(4)(15 downto  0);
    v_hdr.checksum            := hdr_arr(5)(63 downto 48);
    return v_hdr;
  end;

  -- Construct the total header array from the individual header records
  function func_network_total_header_construct_eth( eth : t_network_eth_header) return t_network_total_header_32b_arr is
    variable v_total : t_network_total_header_32b_arr := (others => (others => '0'));
  begin
    v_total(0)(31 downto 16) := (others => '0');  -- force word align to zero
    v_total(0)(15 downto  0) := eth.dst_mac(47 downto 32);
    v_total(1)               := eth.dst_mac(31 downto  0);
    v_total(2)               := eth.src_mac(47 downto 16);
    v_total(3)(31 downto 16) := eth.src_mac(15 downto  0);
    v_total(3)(15 downto  0) := eth.eth_type;
    return v_total;
  end;

  function func_network_total_header_construct_eth( eth : t_network_eth_header) return t_network_total_header_64b_arr is
    variable v_total : t_network_total_header_64b_arr := (others => (others => '0'));
  begin
    v_total(0)(63 downto 16) := (others => '0');  -- force word align to zero
    v_total(0)(15 downto  0) := eth.dst_mac(47 downto 32);
    v_total(1)(63 downto 32) := eth.dst_mac(31 downto  0);
    v_total(1)(31 downto  0) := eth.src_mac(47 downto 16);
    v_total(2)(63 downto 48) := eth.src_mac(15 downto  0);
    v_total(2)(47 downto 32) := eth.eth_type;
    return v_total;
  end;

  function func_network_total_header_construct_arp(eth : t_network_eth_header; arp : t_network_arp_packet) return t_network_total_header_32b_arr is
    variable v_total : t_network_total_header_32b_arr;
  begin
    v_total := func_network_total_header_construct_eth(eth);

    v_total(4)(31 downto 16) := arp.htype;
    v_total(4)(15 downto  0) := arp.ptype;
    v_total(5)(31 downto 24) := arp.hlen;
    v_total(5)(23 downto 16) := arp.plen;
    v_total(5)(15 downto  0) := arp.oper;
    v_total(6)               := arp.sha(47 downto 16);
    v_total(7)(31 downto 16) := arp.sha(15 downto  0);
    v_total(7)(15 downto  0) := arp.spa(31 downto 16);
    v_total(8)(31 downto 16) := arp.spa(15 downto  0);
    v_total(8)(15 downto  0) := arp.tha(47 downto 32);
    v_total(9)               := arp.tha(31 downto  0);
    v_total(10)              := arp.tpa;
    return v_total;
  end;

  function func_network_total_header_construct_arp(eth : t_network_eth_header; arp : t_network_arp_packet) return t_network_total_header_64b_arr is
    variable v_total : t_network_total_header_64b_arr;
  begin
    v_total := func_network_total_header_construct_eth(eth);

    v_total(2)(31 downto 16) := arp.htype;
    v_total(2)(15 downto  0) := arp.ptype;
    v_total(3)(63 downto 56) := arp.hlen;
    v_total(3)(55 downto 48) := arp.plen;
    v_total(3)(47 downto 32) := arp.oper;
    v_total(3)(31 downto  0) := arp.sha(47 downto 16);
    v_total(4)(63 downto 48) := arp.sha(15 downto  0);
    v_total(4)(47 downto 16) := arp.spa(31 downto  0);
    v_total(4)(15 downto  0) := arp.tha(47 downto 32);
    v_total(5)(63 downto 32) := arp.tha(31 downto  0);
    v_total(5)(31 downto  0) := arp.tpa;
    return v_total;
  end;

  function func_network_total_header_construct_ip(eth : t_network_eth_header; ip : t_network_ip_header) return t_network_total_header_32b_arr is
    variable v_total : t_network_total_header_32b_arr := (others => (others => '0'));
  begin
    v_total := func_network_total_header_construct_eth(eth);

    v_total(4)(31 downto 28) := ip.version;
    v_total(4)(27 downto 24) := ip.header_length;
    v_total(4)(23 downto 16) := ip.services;
    v_total(4)(15 downto  0) := ip.total_length;
    v_total(5)(31 downto 16) := ip.identification;
    v_total(5)(15 downto 13) := ip.flags;
    v_total(5)(12 downto  0) := ip.fragment_offset;
    v_total(6)(31 downto 24) := ip.time_to_live;
    v_total(6)(23 downto 16) := ip.protocol;
    v_total(6)(15 downto  0) := ip.header_checksum;
    v_total(7)               := ip.src_ip_addr;
    v_total(8)               := ip.dst_ip_addr;
    return v_total;
  end;

  function func_network_total_header_construct_ip(eth : t_network_eth_header; ip : t_network_ip_header) return t_network_total_header_64b_arr is
    variable v_total : t_network_total_header_64b_arr := (others => (others => '0'));
  begin
    v_total := func_network_total_header_construct_eth(eth);

    v_total(2)(31 downto 28) := ip.version;
    v_total(2)(27 downto 24) := ip.header_length;
    v_total(2)(23 downto 16) := ip.services;
    v_total(2)(15 downto  0) := ip.total_length;
    v_total(3)(63 downto 48) := ip.identification;
    v_total(3)(47 downto 45) := ip.flags;
    v_total(3)(44 downto 32) := ip.fragment_offset;
    v_total(3)(31 downto 24) := ip.time_to_live;
    v_total(3)(23 downto 16) := ip.protocol;
    v_total(3)(15 downto  0) := ip.header_checksum;
    v_total(4)(63 downto 32) := ip.src_ip_addr;
    v_total(4)(31 downto  0) := ip.dst_ip_addr;
    return v_total;
  end;

  function func_network_total_header_construct_icmp(eth : t_network_eth_header; ip : t_network_ip_header; icmp : t_network_icmp_header) return t_network_total_header_32b_arr is
    variable v_total : t_network_total_header_32b_arr;
  begin
    v_total := func_network_total_header_construct_ip(eth, ip);

    v_total(9)(31 downto 24)  := icmp.msg_type;
    v_total(9)(23 downto 16)  := icmp.code;
    v_total(9)(15 downto  0)  := icmp.checksum;
    v_total(10)(31 downto 16) := icmp.id;
    v_total(10)(15 downto  0) := icmp.sequence;
    return v_total;
  end;

  function func_network_total_header_construct_icmp(eth : t_network_eth_header; ip : t_network_ip_header; icmp : t_network_icmp_header) return t_network_total_header_64b_arr is
    variable v_total : t_network_total_header_64b_arr;
  begin
    v_total := func_network_total_header_construct_ip(eth, ip);

    v_total(5)(63 downto 56) := icmp.msg_type;
    v_total(5)(55 downto 48) := icmp.code;
    v_total(5)(47 downto 32) := icmp.checksum;
    v_total(5)(31 downto 16) := icmp.id;
    v_total(5)(15 downto  0) := icmp.sequence;
    return v_total;
  end;

  function func_network_total_header_construct_udp( eth : t_network_eth_header; ip : t_network_ip_header; udp : t_network_udp_header) return t_network_total_header_32b_arr is
    variable v_total : t_network_total_header_32b_arr;
  begin
    v_total := func_network_total_header_construct_ip(eth, ip);

    v_total(9)(31 downto 16)  := udp.src_port;
    v_total(9)(15 downto  0)  := udp.dst_port;
    v_total(10)(31 downto 16) := udp.total_length;
    v_total(10)(15 downto  0) := udp.checksum;
    return v_total;
  end;

  function func_network_total_header_construct_udp( eth : t_network_eth_header; ip : t_network_ip_header; udp : t_network_udp_header) return t_network_total_header_64b_arr is
    variable v_total : t_network_total_header_64b_arr;
  begin
    v_total := func_network_total_header_construct_ip(eth, ip);

    v_total(5)(63 downto 48) := udp.src_port;
    v_total(5)(47 downto 32) := udp.dst_port;
    v_total(5)(31 downto 16) := udp.total_length;
    v_total(5)(15 downto  0) := udp.checksum;
    return v_total;
  end;

  -- Construct the response headers
  function func_network_total_header_response_eth(eth_arr  : t_network_total_header_32b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- Default
    v_response := eth_arr;
    -- ETH
    -- . use input src mac for dst mac
    v_response(0)(15 downto  0) := eth_arr(2)(31 downto 16);
    v_response(1)               := eth_arr(2)(15 downto  0) & eth_arr(3)(31 downto 16);
    -- . force eth src mac to this node mac address (because the input dst_mac can be via eth broadcast mac)
    v_response(2)               := mac_addr(47 downto 16);
    v_response(3)(31 downto 16) := mac_addr(15 downto  0);
    return v_response;
  end;

  function func_network_total_header_response_eth(eth_arr  : t_network_total_header_64b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- Default
    v_response := eth_arr;
    -- ETH
    -- . use input src mac for dst mac
    v_response(0)(15 downto  0) := eth_arr(1)(31 downto 16);
    v_response(1)(63 downto 32) := eth_arr(1)(15 downto  0) & eth_arr(2)(63 downto 48);
    -- . force eth src mac to this node mac address (because the input dst_mac can be via eth broadcast mac)
    v_response(1)(31 downto  0) := mac_addr(47 downto 16);
    v_response(2)(63 downto 48) := mac_addr(15 downto  0);
    return v_response;
  end;

  function func_network_total_header_response_arp(arp_arr  : t_network_total_header_32b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
                                                  ip_addr  : std_logic_vector(c_network_ip_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- ETH
    v_response := func_network_total_header_response_eth(arp_arr, mac_addr);
    -- ARP
    -- . force operation arp reply
    v_response(5)(15 downto  0) := TO_UVEC(c_network_arp_oper_reply, 16);
    -- . force sha to this node mac address
    v_response(6)               := mac_addr(47 downto 16);
    v_response(7)(31 downto 16) := mac_addr(15 downto  0);
    -- . force spa to this node ip address
    v_response(7)(15 downto  0) := ip_addr(31 downto 16);
    v_response(8)(31 downto 16) := ip_addr(15 downto  0);
    -- . use input sha for tha
    v_response(8)(15 downto  0) := arp_arr(6)(31 downto 16);
    v_response(9)               := arp_arr(6)(15 downto  0) & arp_arr(7)(31 downto 16);
    -- . use input spa for tpa
    v_response(10)              := arp_arr(7)(15 downto  0) & arp_arr(8)(31 downto 16);
    return v_response;
  end;

  function func_network_total_header_response_arp(arp_arr  : t_network_total_header_64b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
                                                  ip_addr  : std_logic_vector(c_network_ip_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- ETH
    v_response := func_network_total_header_response_eth(arp_arr, mac_addr);
    -- ARP
    -- . force operation arp reply
    v_response(3)(47 downto 32) := TO_UVEC(c_network_arp_oper_reply, 16);
    -- . force sha to this node mac address
    v_response(3)(31 downto  0) := mac_addr(47 downto 16);
    v_response(4)(63 downto 48) := mac_addr(15 downto  0);
    -- . force spa to this node ip address
    v_response(4)(47 downto 16) := ip_addr(31 downto  0);
    -- . use input sha for tha
    v_response(4)(15 downto  0) := arp_arr(3)(31 downto 16);
    v_response(5)(63 downto 32) := arp_arr(3)(15 downto  0) & arp_arr(4)(63 downto 48);
    -- . use input spa for tpa
    v_response(5)(31 downto  0) := arp_arr(4)(47 downto 16);
    return v_response;
  end;

  function func_network_total_header_response_ip(ip_arr   : t_network_total_header_32b_arr;
                                                 mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- ETH
    v_response := func_network_total_header_response_eth(ip_arr, mac_addr);
    -- IP
    -- . force ip header checksum to 0
    v_response(6)(15 downto  0) := TO_UVEC(0, 16);
    -- . swap ip dst_addr and ip src_addr
    v_response(7)               := ip_arr(8);
    v_response(8)               := ip_arr(7);
    return v_response;
  end;

  function func_network_total_header_response_ip(ip_arr   : t_network_total_header_64b_arr;
                                                 mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- ETH
    v_response := func_network_total_header_response_eth(ip_arr, mac_addr);
    -- IP
    -- . force ip header checksum to 0
    v_response(3)(15 downto  0) := TO_UVEC(0, 16);
    -- . swap ip dst_addr and ip src_addr
    v_response(4)(63 downto 32) := ip_arr(4)(31 downto  0);
    v_response(4)(31 downto  0) := ip_arr(4)(63 downto 32);
    return v_response;
  end;

  function func_network_total_header_response_icmp(icmp_arr : t_network_total_header_32b_arr;
                                                   mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- ETH, IP
    v_response := func_network_total_header_response_ip(icmp_arr, mac_addr);
    -- ICMP : force type to icmp reply
    v_response(9)(31 downto 24) := TO_UVEC(c_network_icmp_msg_type_reply, 8);
    -- ICMP : force icmp checksum to 0
    v_response(9)(15 downto  0) := TO_UVEC(0, 16);
    return v_response;
  end;

  function func_network_total_header_response_icmp(icmp_arr : t_network_total_header_64b_arr;
                                                   mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- ETH, IP
    v_response := func_network_total_header_response_ip(icmp_arr, mac_addr);
    -- ICMP : force type to icmp reply
    v_response(5)(63 downto 56) := TO_UVEC(c_network_icmp_msg_type_reply, 8);
    -- ICMP : force icmp checksum to 0
    v_response(5)(15 downto  0) := TO_UVEC(0, 16);
    return v_response;
  end;

  function func_network_total_header_response_udp(udp_arr  : t_network_total_header_32b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- ETH, IP
    v_response := func_network_total_header_response_ip(udp_arr, mac_addr);
    -- UDP : swap udp dst port and udp src port
    v_response(9)               := udp_arr(9)(15 downto  0) & udp_arr(9)(31 downto 16);
    -- UDP : force udp checksum to 0
    v_response(10)(15 downto 0) := TO_UVEC(0, 16);
    return v_response;
  end;

  function func_network_total_header_response_udp(udp_arr  : t_network_total_header_64b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- ETH, IP
    v_response := func_network_total_header_response_ip(udp_arr, mac_addr);
    -- UDP : swap udp dst port and udp src port
    v_response(5)(63 downto 32) := udp_arr(5)(47 downto 32) & udp_arr(5)(63 downto 48);
    -- UDP : force udp checksum to 0
    v_response(5)(15 downto  0) := TO_UVEC(0, 16);
    return v_response;
  end;

  -- Construct the response headers for headers without word align padding
  function func_network_total_header_no_align_response_eth(eth_arr  : t_network_total_header_32b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- Default
    v_response := eth_arr;
    -- ETH
    -- . use input src mac for dst mac
    v_response(0)               := eth_arr(1)(15 downto  0) & eth_arr(2)(31 downto 16);
    v_response(1)(31 downto 16) := eth_arr(2)(15 downto  0);
    -- . force eth src mac to this node mac address (because the input dst_mac can be via eth broadcast mac)
    v_response(1)(15 downto 0)  := mac_addr(47 downto 32);
    v_response(2)               := mac_addr(31 downto  0);
    return v_response;
  end;

  function func_network_total_header_no_align_response_eth(eth_arr  : t_network_total_header_64b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- Default
    v_response := eth_arr;
    -- ETH
    -- . use input src mac for dst mac
    v_response(0)(63 downto 16) := eth_arr(0)(15 downto  0) & eth_arr(1)(63 downto 32);
    -- . force eth src mac to this node mac address (because the input dst_mac can be via eth broadcast mac)
    v_response(0)(15 downto  0) := mac_addr(47 downto 32);
    v_response(1)(63 downto 32) := mac_addr(31 downto  0);
    return v_response;
  end;

  function func_network_total_header_no_align_response_arp(arp_arr  : t_network_total_header_32b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
                                                  ip_addr  : std_logic_vector(c_network_ip_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- ETH
    v_response := func_network_total_header_no_align_response_eth(arp_arr, mac_addr);
    -- ARP
    -- . force operation arp reply
    v_response(5)(15 downto  0) := TO_UVEC(c_network_arp_oper_reply, 16);
    -- . force sha to this node mac address
    v_response(6)               := mac_addr(47 downto 16);
    v_response(7)(31 downto 16) := mac_addr(15 downto  0);
    -- . force spa to this node ip address
    v_response(7)(15 downto  0) := ip_addr(31 downto 16);
    v_response(8)(31 downto 16) := ip_addr(15 downto  0);
    -- . use input sha for tha
    v_response(8)(15 downto  0) := arp_arr(6)(31 downto 16);
    v_response(9)               := arp_arr(6)(15 downto  0) & arp_arr(7)(31 downto 16);
    -- . use input spa for tpa
    v_response(10)              := arp_arr(7)(15 downto  0) & arp_arr(8)(31 downto 16);
    return v_response;
  end;

  function func_network_total_header_no_align_response_arp(arp_arr  : t_network_total_header_64b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
                                                  ip_addr  : std_logic_vector(c_network_ip_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- ETH
    v_response := func_network_total_header_no_align_response_eth(arp_arr, mac_addr);
    -- ARP
    -- . force operation arp reply
    v_response(3)(47 downto 32) := TO_UVEC(c_network_arp_oper_reply, 16);
    -- . force sha to this node mac address
    v_response(3)(31 downto  0) := mac_addr(47 downto 16);
    v_response(4)(63 downto 48) := mac_addr(15 downto  0);
    -- . force spa to this node ip address
    v_response(4)(47 downto 16) := ip_addr(31 downto  0);
    -- . use input sha for tha
    v_response(4)(15 downto  0) := arp_arr(3)(31 downto 16);
    v_response(5)(63 downto 32) := arp_arr(3)(15 downto  0) & arp_arr(4)(63 downto 48);
    -- . use input spa for tpa
    v_response(5)(31 downto  0) := arp_arr(4)(47 downto 16);
    return v_response;
  end;

  function func_network_total_header_no_align_response_ip(ip_arr   : t_network_total_header_32b_arr;
                                                 mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- ETH
    v_response := func_network_total_header_no_align_response_eth(ip_arr, mac_addr);
    -- IP
    -- . force ip header checksum to 0
    v_response(6)(31 downto 16) := TO_UVEC(0, 16);
    -- . swap ip dst_addr and ip src_addr
    v_response(6)(15 downto  0) := ip_arr(7)(15 downto  0);
    v_response(7)(31 downto 16) := ip_arr(8)(31 downto 16);
    v_response(7)(15 downto  0) := ip_arr(6)(15 downto  0);
    v_response(8)(31 downto 16) := ip_arr(7)(31 downto 16);
    return v_response;
  end;

  function func_network_total_header_no_align_response_ip(ip_arr   : t_network_total_header_64b_arr;
                                                 mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- ETH
    v_response := func_network_total_header_no_align_response_eth(ip_arr, mac_addr);
    -- IP
    -- . force ip header checksum to 0
    v_response(3)(63 downto 48) := TO_UVEC(0, 16);
    -- . swap ip dst_addr and ip src_addr
    v_response(3)(47 downto 16) := ip_arr(3)(15 downto  0) & ip_arr(4)(63 downto 48);
    v_response(3)(15 downto  0) := ip_arr(3)(47 downto 32);
    v_response(4)(63 downto 48) := ip_arr(3)(31 downto 16);
    return v_response;
  end;

  function func_network_total_header_no_align_response_icmp(icmp_arr : t_network_total_header_32b_arr;
                                                   mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- ETH, IP
    v_response := func_network_total_header_no_align_response_ip(icmp_arr, mac_addr);
    -- ICMP : force type to icmp reply
    v_response(8)(15 downto  8) := TO_UVEC(c_network_icmp_msg_type_reply, 8);
    -- ICMP : force icmp checksum to 0
    v_response(9)(31 downto 16) := TO_UVEC(0, 16);
    return v_response;
  end;

  function func_network_total_header_no_align_response_icmp(icmp_arr : t_network_total_header_64b_arr;
                                          mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- ETH, IP
    v_response := func_network_total_header_no_align_response_ip(icmp_arr, mac_addr);
    -- ICMP : force type to icmp reply
    v_response(4)(47 downto 40) := TO_UVEC(c_network_icmp_msg_type_reply, 8);
    -- ICMP : force icmp checksum to 0
    v_response(4)(31 downto 16) := TO_UVEC(0, 16);
    return v_response;
  end;

  function func_network_total_header_no_align_response_udp(udp_arr  : t_network_total_header_32b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_32b_arr is
    variable v_response : t_network_total_header_32b_arr;
  begin
    -- ETH, IP
    v_response := func_network_total_header_no_align_response_ip(udp_arr, mac_addr);
    -- UDP : swap udp dst port and udp src port
    v_response(9)               := udp_arr(9)(15 downto  0) & udp_arr(9)(31 downto 16);
    -- UDP : force udp checksum to 0
    v_response(10)(15 downto 0) := TO_UVEC(0, 16);
    return v_response;
  end;

  function func_network_total_header_no_align_response_udp(udp_arr  : t_network_total_header_64b_arr;
                                                  mac_addr : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0)) return t_network_total_header_64b_arr is
    variable v_response : t_network_total_header_64b_arr;
  begin
    -- ETH, IP
    v_response := func_network_total_header_no_align_response_ip(udp_arr, mac_addr);
    -- UDP : swap udp dst port and udp src port
    v_response(5)(63 downto 32) := udp_arr(5)(47 downto 32) & udp_arr(5)(63 downto 48);
    -- UDP : force udp checksum to 0
    v_response(5)(15 downto  0) := TO_UVEC(0, 16);
    return v_response;
  end;
end common_network_total_header_pkg;
