--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.diag_pkg.all;

-- Usage:
-- > do wave_diag_wg_wideband.do
-- > run -all
--
-- . Use select rigth mouse in wave window on wg_dat and choose 'format --> analogue (automatic)'
-- . run 10 us to see CALC mode waveform output at proper automatic scale
-- . run 100 us to see SINGLE and REPEAT mode waveform output at proper automatic scale

entity tb_diag_wg is
  generic (
    g_buf_adr_w      : natural := 10;  -- Waveform buffer address width (requires corresponding c_buf_file)
    g_buf_dat_w      : natural := 8;  -- Waveform buffer stored data width (requires corresponding c_buf_file)
    g_wg_dat_w       : natural := 8  -- Effective waveform sample output width must be <= g_buf_dat_w
  );
end tb_diag_wg;

architecture tb of tb_diag_wg is
  constant c_clk_freq       : natural := 200 * 10**6;  -- Hz
  constant c_clk_period     : time    := (10**9 / c_clk_freq) * 1 ns;

  -- Default settings
  constant c_buf            : t_c_mem := (latency  => 1,
                                          adr_w    => g_buf_adr_w,
                                          dat_w    => g_buf_dat_w,  -- fit DSP multiply 18x18 element
                                          nof_dat  => 2**g_buf_adr_w,  -- = 2**adr_w
                                          init_sl  => '0');
  constant c_buf_file       : string := sel_a_b(c_buf.adr_w = 11 and c_buf.dat_w = 18, "data/diag_sin_2048x18.hex",
                                        sel_a_b(c_buf.adr_w = 10 and c_buf.dat_w = 18, "data/diag_sin_1024x18.hex",
                                        sel_a_b(c_buf.adr_w = 10 and c_buf.dat_w = 8, "data/diag_sin_1024x8.hex", "UNUSED")));

  constant c_wg_nof_samples : natural := c_buf.nof_dat;  -- must be <= c_buf.nof_dat
  constant c_wg_gain_w      : natural := 1;  -- Normalized range [0 1>  maps to fixed point range [0:2**c_diag_wg_ampl_w>
                                              -- . use gain 2**0             = 1 to have fulle scale without clipping
                                              -- . use gain 2**g_calc_gain_w > 1 to cause clipping

  constant c_buf_full_scale : natural := 2**(g_buf_dat_w - 1) - 1;  -- The stored waveform range should also be [-c_buf_full_scale +c_buf_full_scale], so not including -c_buf_full_scale-1
  constant c_wg_full_scale  : natural := 2**(g_wg_dat_w - 1) - 1;
  constant c_ampl_norm      : real := sel_a_b(g_wg_dat_w < g_buf_dat_w, real(c_wg_full_scale) / real(c_wg_full_scale+1), 1.0);
  --CONSTANT c_ampl_norm    : REAL := REAL(c_wg_full_scale)/REAL(c_wg_full_scale+1);     -- Use this if g_wg_dat_w < g_buf_dat_w, to avoid clipping
  --CONSTANT c_ampl_norm    : REAL := 1.0;                                               -- Use this if g_wg_dat_w = g_buf_dat_w, because the stored waveform range is already -+c_buf_full_scale
  --CONSTANT c_ampl_norm    : REAL := REAL(c_buf_full_scale)/REAL(c_buf_full_scale+1);   -- No need to use this, because the stored waveform range is already -+c_buf_full_scale

  constant c_freq_unit      : real := c_diag_wg_freq_unit;  -- ^= c_clk_freq = Fs (sample frequency), assuming one sinus waveform in the buffer
  constant c_ampl_unit      : real := c_diag_wg_ampl_unit * c_ampl_norm;  -- ^= Full Scale range [-c_wg_full_scale +c_wg_full_scale] without clipping
  constant c_phase_unit     : real := c_diag_wg_phase_unit;  -- ^= 1 degree

  signal tb_end         : std_logic;
  signal rst            : std_logic;
  signal clk            : std_logic := '1';
  signal restart        : std_logic;

  signal buf_rddat      : std_logic_vector(c_buf.dat_w - 1 downto 0);
  signal buf_rdval      : std_logic;
  signal buf_addr       : std_logic_vector(c_buf.adr_w - 1 downto 0);
  signal buf_rden       : std_logic;

  signal wg_ctrl        : t_diag_wg;

  signal wg_mode        : natural;
  signal wg_freq        : natural;
  signal wg_ampl        : natural;
  signal wg_nof_samples : natural;
  signal wg_phase       : natural;

  signal wg_ovr         : std_logic;
  signal wg_dat         : std_logic_vector(c_buf.dat_w - 1 downto 0);
  signal wg_val         : std_logic;
  signal wg_sync        : std_logic;
begin
  rst <= '1', '0' after c_clk_period / 10;
  clk <= not clk or tb_end after c_clk_period / 2;

  wg_ctrl.mode        <= TO_UVEC(wg_mode,        c_diag_wg_mode_w);
  wg_ctrl.freq        <= TO_UVEC(wg_freq,        c_diag_wg_freq_w);
  wg_ctrl.ampl        <= TO_UVEC(wg_ampl,        c_diag_wg_ampl_w);
  wg_ctrl.nof_samples <= TO_UVEC(wg_nof_samples, c_diag_wg_nofsamples_w);
  wg_ctrl.phase       <= TO_UVEC(wg_phase,       c_diag_wg_phase_w);

  p_mm : process
  begin
    tb_end         <= '0';
    restart        <= '0';
    wg_mode        <= c_diag_wg_mode_off;

    ---------------------------------------------------------------------------
    -- >>> Single, repeat mode
    wg_nof_samples <= c_wg_nof_samples;

    -- >>> CALC mode
    -- Cosinus with frequency Fs/2 (note sinus Fs/2 yields DC = 0)
--     wg_freq        <= INTEGER(0.5 * c_freq_unit);
--     wg_phase       <= INTEGER(90.0 * c_phase_unit);

    -- Choosing 1.0*Fs to select Fs which is equivalent to DC, hence the DC value is then determined by phase
    -- this also applies to 2.0, 3.0, 4.0 etc
--     wg_freq        <= INTEGER(1.0 * c_freq_unit);
--     wg_phase       <= INTEGER(45.0 * c_phase_unit);

    -- Sinus Fs/16
    wg_freq        <= integer(0.0625 * c_freq_unit);
    --wg_freq        <= INTEGER(511.0/512.0 * c_freq_unit);
    --wg_freq        <= INTEGER(1.0/512.0 * c_freq_unit);
    --wg_freq        <= INTEGER(1.0);  -- minimum value, yields Fs/c_freq_unit Hz sinus
    wg_phase       <= integer(0.0 * c_phase_unit);

    -- Sinus Fs/17
--     wg_freq        <= INTEGER(1.0/17.0 * c_freq_unit);
--     wg_phase       <= INTEGER(0.0 * c_phase_unit);

    wg_ampl        <= integer(1.0 * c_ampl_unit);  -- yields amplitude of c_wg_full_scale
--     wg_ampl        <= INTEGER(1.0/REAL(c_wg_full_scale) * c_ampl_unit);  -- yields amplitude of 1
--     wg_ampl        <= INTEGER(3.0/REAL(c_wg_full_scale) * c_ampl_unit);  -- yields amplitude of 3

    wait until rising_edge(clk);  -- align to rising edge
    wait for c_clk_period * 200;

    ---------------------------------------------------------------------------
    -- >>> Select the different modes

    -- CALC mode
    wg_mode        <= c_diag_wg_mode_calc;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    wait for c_clk_period * 3000;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    wait for c_clk_period * 3000;
    --WAIT FOR 1 sec;

    -- OFF mode
    wg_mode        <= c_diag_wg_mode_off;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    wait for c_clk_period * 200;

    -- SINGLE mode
    wg_mode        <= c_diag_wg_mode_single;
    for I in 0 to 1 loop
      restart      <= '1';
      wait for c_clk_period * 1;
      restart      <= '0';
      wait for c_clk_period * c_buf.nof_dat;
      wait for c_clk_period * 300;
    end loop;

    -- REPEAT mode
    wg_mode        <= c_diag_wg_mode_repeat;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    wait for c_clk_period * c_buf.nof_dat * 5;
    wait for c_clk_period * 200;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    wait for c_clk_period * c_buf.nof_dat * 5;
    wait for c_clk_period * 200;

    -- OFF mode
    wg_mode        <= c_diag_wg_mode_off;
    restart        <= '1';
    wait for c_clk_period * 1;
    restart        <= '0';
    wait for c_clk_period * 200;

    wait for c_clk_period * 100;
    tb_end <= '1';
    wait;
  end process;

  -- Waveform buffer
  u_buf : entity common_lib.common_ram_crw_crw
  generic map (
    g_ram       => c_buf,
    g_init_file => c_buf_file
  )
  port map (
    rst_a     => '0',
    rst_b     => '0',
    clk_a     => clk,
    clk_b     => clk,
    wr_en_a   => '0',
    wr_en_b   => '0',
    wr_dat_a  => (others => '0'),
    wr_dat_b  => (others => '0'),
    adr_a     => (others => '0'),
    adr_b     => buf_addr,
    rd_en_a   => '0',
    rd_en_b   => buf_rden,
    rd_dat_a  => OPEN,
    rd_dat_b  => buf_rddat,
    rd_val_a  => OPEN,
    rd_val_b  => buf_rdval
  );

  -- Waveform generator
  u_wg : entity work.diag_wg
  generic map (
    g_buf_dat_w    => c_buf.dat_w,
    g_buf_addr_w   => c_buf.adr_w,
    g_calc_support => true,
    g_calc_gain_w  => c_wg_gain_w,
    g_calc_dat_w   => g_wg_dat_w
  )
  port map (
    rst            => rst,
    clk            => clk,
    restart        => restart,

    buf_rddat      => buf_rddat,
    buf_rdval      => buf_rdval,
    buf_addr       => buf_addr,
    buf_rden       => buf_rden,

    ctrl           => wg_ctrl,

    out_ovr        => wg_ovr,
    out_dat        => wg_dat,
    out_val        => wg_val,
    out_sync       => wg_sync
  );
end tb;
