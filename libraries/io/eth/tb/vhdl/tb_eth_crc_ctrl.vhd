-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for eth_crc_ctrl
-- Description:
--   Stimuli and tb_end based on proc_dp_count_en() state machine in tb_dp_pkg.vhd
--   Verification by proc_dp_verify_*().
-- Usage:
--   > as 10
--   > run -all

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;
use work.eth_pkg.all;

entity tb_eth_crc_ctrl is
end tb_eth_crc_ctrl;

architecture tb of tb_eth_crc_ctrl is
  -- DUT ready latency
  constant c_dut_latency    : natural := 1;  -- fixed 1 for dp_pipeline
  constant c_tx_latency     : natural := c_dut_latency;  -- TX ready latency of TB
  constant c_tx_void        : natural := sel_a_b(c_tx_latency, 1, 0);  -- used to avoid empty range VHDL warnings when c_tx_latency=0
  constant c_tx_offset_sop  : natural := 0;
  constant c_tx_period_sop  : natural := 17;  -- sop in data valid cycle 0, 17, ...
  constant c_tx_offset_eop  : natural := c_tx_period_sop - 1;
  constant c_tx_period_eop  : natural := c_tx_period_sop;
  constant c_rx_latency     : natural := c_dut_latency;  -- RX ready latency from DUT
  constant c_verify_en_wait : natural := 5;  -- wait some cycles before asserting verify enable

  constant c_random_w       : natural := 19;

  signal tb_end         : std_logic := '0';
  signal clk            : std_logic := '0';
  signal rst            : std_logic;
  signal sync           : std_logic;
  signal lfsr1          : std_logic_vector(c_random_w - 1 downto 0) := (others => '0');
  signal lfsr2          : std_logic_vector(c_random_w   downto 0) := (others => '0');

  signal cnt_dat        : std_logic_vector(c_eth_data_w - 1 downto 0);
  signal cnt_val        : std_logic;
  signal cnt_en         : std_logic;

  signal tx_data        : t_dp_data_arr(0 to c_tx_latency + c_tx_void)    := (others => (others => '0'));
  signal tx_val         : std_logic_vector(0 to c_tx_latency + c_tx_void) := (others => '0');

  signal in_ready       : std_logic;
  signal in_data        : std_logic_vector(c_eth_data_w - 1 downto 0) := (others => '0');
  signal in_val         : std_logic;
  signal in_sop         : std_logic;
  signal in_eop         : std_logic;

  signal out_ready      : std_logic;
  signal prev_out_ready : std_logic_vector(0 to c_rx_latency);
  signal out_data       : std_logic_vector(c_eth_data_w - 1 downto 0);
  signal out_data_1     : std_logic_vector(out_data'range);
  signal out_data_2     : std_logic_vector(out_data'range);
  signal out_data_3     : std_logic_vector(out_data'range);
  signal ctrl_data      : std_logic_vector(out_data'range);
  signal out_empty      : std_logic_vector(c_eth_empty_w - 1 downto 0);
  signal out_empty_1    : std_logic_vector(out_empty'range);
  signal out_val        : std_logic;
  signal out_sop        : std_logic;
  signal out_eop        : std_logic;
  signal out_eop_1      : std_logic;
  signal out_eop_2      : std_logic;

  signal state          : t_dp_state_enum;

  signal verify_en      : std_logic;
  signal verify_done    : std_logic;

  signal exp_data       : std_logic_vector(c_dp_data_w - 1 downto 0) := TO_UVEC(1000, c_dp_data_w);

  constant c_last_word  : natural := 16#70717273#;  -- 0 or > 0, use 0x70717273 to observe all 4 bytes
  constant c_empty      : natural := 0;  -- try all possible values: 0, 1, 2, or 3

  signal snk_in_error   : std_logic_vector(c_word_w - 1 downto 0);  -- use full word range, instead of only range [c_tse_error_w-1:0], to observe all 4 bytes in this tb
  signal snk_in         : t_dp_sosi;
  signal snk_out        : t_dp_siso;

  signal src_in         : t_dp_siso;
  signal src_out        : t_dp_sosi;
  signal src_out_err    : std_logic;
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  -- Sync interval
  proc_dp_sync_interval(clk, sync);

  -- Input data
  cnt_val <= in_ready and cnt_en;

  proc_dp_cnt_dat(rst, clk, cnt_val, cnt_dat);
  proc_dp_tx_data(c_tx_latency, rst, clk, cnt_val, cnt_dat, tx_data, tx_val, in_data, in_val);
  proc_dp_tx_ctrl(c_tx_offset_sop, c_tx_period_sop, in_data, in_val, in_sop);
  proc_dp_tx_ctrl(c_tx_offset_eop, c_tx_period_eop, in_data, in_val, in_eop);

  -- Stimuli control
  proc_dp_count_en(rst, clk, sync, lfsr1, state, verify_done, tb_end, cnt_en);
  proc_dp_out_ready(rst, clk, sync, lfsr2, out_ready);

  -- Output verify
  proc_dp_verify_en(c_verify_en_wait, rst, clk, sync, verify_en);
  proc_dp_verify_data_empty(c_rx_latency, c_last_word, clk, verify_en, out_ready, out_val, out_eop, out_eop_1, out_eop_2, out_data, out_data_1, out_data_2, out_data_3, out_empty, out_empty_1);
  proc_dp_verify_valid(c_rx_latency, clk, verify_en, out_ready, prev_out_ready, out_val);
  proc_dp_verify_ctrl(c_tx_offset_sop, c_tx_period_sop, "sop", clk, verify_en, ctrl_data, out_val, out_sop);
  proc_dp_verify_ctrl(c_tx_offset_eop, c_tx_period_eop, "eop", clk, verify_en, ctrl_data, out_val, out_eop);

  -- create ctrl_data in phase with out_data, but with counter data preserved during empty and c_last_word, to verify missing or unexpected eop
  ctrl_data <= INCR_UVEC(out_data_3, 3) when out_eop = '1' or out_eop_1 = '1' else INCR_UVEC(out_data_1, 1);

  -- Check that the test has ran at all
  proc_dp_verify_value(e_at_least, clk, verify_done, exp_data, out_data);

  ------------------------------------------------------------------------------
  -- DUT eth_crc_ctrl
  ------------------------------------------------------------------------------

  -- Interface TB tx - DUT sink
  in_ready     <= snk_out.ready;
  snk_in_error <= TO_UVEC(c_last_word, snk_in_error'length);  -- use odd number for test purposes, because bit [0] must contain the logic OR of bits [1:5]
  snk_in.data  <= RESIZE_DP_DATA(in_data);
  snk_in.valid <= in_val;
  snk_in.sop   <= in_sop;
  snk_in.eop   <= in_eop;
  snk_in.empty <= TO_DP_EMPTY(c_empty);

  -- Interface DUT source - TB rx
  src_in.ready <= out_ready;
  out_data     <= src_out.data(c_eth_data_w - 1 downto 0);
  out_val      <= src_out.valid;
  out_sop      <= src_out.sop;
  out_eop      <= src_out.eop;
  out_empty    <= src_out.empty(c_eth_empty_w - 1 downto 0);

  dut : entity work.eth_crc_ctrl
  port map (
    rst         => rst,
    clk         => clk,

    -- Streaming Sink
    snk_in_err  => snk_in_error,  -- error vector from TSE MAC t_eth_stream
    snk_in      => snk_in,
    snk_out     => snk_out,

    -- Streaming Source
    src_in      => src_in,
    src_out     => src_out,
    src_out_err => src_out_err
  );
end tb;
