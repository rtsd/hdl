-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Authors : J Hargreaves, L Hiemstra
-- Purpose:
--   AIT - ADC (Jesd) receiver, input, timing and associated diagnostic blocks
-- Description:
--   Unb2b version for lab testing
--   Contains all the signal processing blocks to receive and time the ADC input data
--   See https://support.astron.nl/confluence/display/STAT/L5+SDPFW+DD%3A+ADC+data+input+and+timestamp

library IEEE, common_lib, unb2b_board_lib, diag_lib, aduh_lib, dp_lib, tech_jesd204b_lib, lofar2_sdp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use unb2b_board_lib.unb2b_board_peripherals_pkg.all;
use diag_lib.diag_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.lofar2_unb2b_adc_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;

entity node_adc_input_and_timing is
  generic (
    g_jesd_freq               : string  := "200MHz";
    g_buf_nof_data            : natural := 131072;  -- 8192; --1024;
    g_nof_streams             : natural := 12;
    g_nof_sync_n              : natural := 4;  -- Three ADCs per RCU share a sync
    g_aduh_buffer_nof_symbols : natural := 512;  -- Default 512
    g_bsn_sync_timeout        : natural := 200000000;  -- Default 200M, overide for short simulation
    g_sim                     : boolean := false
  );
  port (
    -- clocks and resets
    mm_clk                         : in std_logic;
    mm_rst                         : in std_logic;
    dp_clk                         : in std_logic;
    dp_rst                         : in std_logic;

    -- mm control buses
    -- JESD
    jesd204b_mosi                  : in  t_mem_mosi := c_mem_mosi_rst;
    jesd204b_miso                  : out t_mem_miso := c_mem_miso_rst;

    -- Shiftram (applies per-antenna delay)
    reg_dp_shiftram_mosi           : in  t_mem_mosi := c_mem_mosi_rst;
    reg_dp_shiftram_miso           : out t_mem_miso := c_mem_miso_rst;

    -- bsn source
    reg_bsn_source_mosi            : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_source_miso            : out t_mem_miso := c_mem_miso_rst;

    -- bsn scheduler
    reg_bsn_scheduler_wg_mosi      : in  t_mem_mosi := c_mem_mosi_rst;
    reg_bsn_scheduler_wg_miso      : out t_mem_miso := c_mem_miso_rst;

    -- WG
    reg_wg_mosi                    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_wg_miso                    : out t_mem_miso := c_mem_miso_rst;
    ram_wg_mosi                    : in  t_mem_mosi := c_mem_mosi_rst;
    ram_wg_miso                    : out t_mem_miso := c_mem_miso_rst;

    -- BSN MONITOR
    reg_bsn_monitor_input_mosi     : in  t_mem_mosi;
    reg_bsn_monitor_input_miso     : out t_mem_miso;

    -- Data buffer for raw samples
    ram_diag_data_buf_jesd_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    ram_diag_data_buf_jesd_miso    : out t_mem_miso;
    reg_diag_data_buf_jesd_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_diag_data_buf_jesd_miso    : out t_mem_miso;

    -- Data buffer for framed samples (variable depth)
    ram_diag_data_buf_bsn_mosi     : in  t_mem_mosi;
    ram_diag_data_buf_bsn_miso     : out t_mem_miso;
    reg_diag_data_buf_bsn_mosi     : in  t_mem_mosi;
    reg_diag_data_buf_bsn_miso     : out t_mem_miso;

    -- Aduh (statistics) monitor
    ram_aduh_monitor_mosi          : in  t_mem_mosi;
    ram_aduh_monitor_miso          : out t_mem_miso;
    reg_aduh_monitor_mosi          : in  t_mem_mosi;
    reg_aduh_monitor_miso          : out t_mem_miso;

    -- JESD control
    jesd_ctrl_mosi                 : in  t_mem_mosi;
    jesd_ctrl_miso                 : out t_mem_miso;

    -- JESD io signals
    jesd204b_serial_data           : in    std_logic_vector((c_unb2b_board_tr_jesd204b.bus_w * c_unb2b_board_tr_jesd204b.nof_bus) - 1 downto 0);
    jesd204b_refclk                : in    std_logic;
    jesd204b_sysref                : in    std_logic;
    jesd204b_sync_n                : out   std_logic_vector((c_unb2b_board_tr_jesd204b.bus_w * c_unb2b_board_tr_jesd204b.nof_bus) - 1 downto 0);

    -- Streaming data output
    out_sosi_arr                   : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)

  );
end node_adc_input_and_timing;

architecture str of node_adc_input_and_timing is
  constant c_nof_streams_jesd204b   : natural := 12;  -- IP is set up for 12 streams

  constant c_mm_jesd_ctrl_reg       : t_c_mem := (latency  => 1,
                                                  adr_w    => 1,
                                                  dat_w    => c_word_w,
                                                  nof_dat  => 1,
                                                  init_sl  => '0');

  -- Waveform Generator
  constant c_wg_buf_directory       : string := "data/";
  constant c_wg_buf_dat_w           : natural := 18;  -- default value of WG that fits 14 bits of ADC data
  constant c_wg_buf_addr_w          : natural := 10;  -- default value of WG for 1024 samples;
  signal wg_out_ovr                 : std_logic_vector(g_nof_streams - 1 downto 0);
  signal wg_out_val                 : std_logic_vector(g_nof_streams - 1 downto 0);
  signal wg_out_data                : std_logic_vector(g_nof_streams * c_wg_buf_dat_w - 1 downto 0);
  signal wg_out_sync                : std_logic_vector(g_nof_streams - 1 downto 0);
  signal trigger_wg                 : std_logic;

  -- Frame parameters TBC
  constant c_bs_bsn_w               : natural := 64;  -- 51;
  constant c_bs_block_size          : natural := 1024;
  constant c_bs_nof_block_per_sync  : natural := 390625;  -- generate a sync every 2s for testing
  constant c_dp_shiftram_nof_samples: natural := 4096;
  constant c_data_w                 : natural := 16;
  constant c_dp_fifo_dc_size        : natural := 64;

  -- QSFP leds
  signal qsfp_green_led_arr         : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);
  signal qsfp_red_led_arr           : std_logic_vector(c_unb2b_board_tr_qsfp.nof_bus - 1 downto 0);

  -- JESD signals
  signal rx_clk                     : std_logic;  -- formerly jesd204b_frame_clk
  signal rx_rst                     : std_logic;
  signal rx_sysref                  : std_logic;

  -- Sosis and sosi arrays
  signal rx_sosi_arr                : t_dp_sosi_arr(c_nof_streams_jesd204b - 1 downto 0);
  signal dp_shiftram_snk_in_arr     : t_dp_sosi_arr(c_nof_streams_jesd204b - 1 downto 0);
  signal ant_sosi_arr               : t_dp_sosi_arr(c_nof_streams_jesd204b - 1 downto 0);
  signal bs_sosi                    : t_dp_sosi;
  signal wg_sosi_arr                : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal mux_sosi_arr               : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal nxt_mux_sosi_arr           : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal st_sosi_arr                : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  signal mm_rst_internal            : std_logic;
  signal mm_jesd_ctrl_reg           : std_logic_vector(c_word_w - 1 downto 0);
  signal jesd204b_disable_arr       : std_logic_vector(g_nof_streams - 1 downto 0);
  signal jesd204b_reset             : std_logic;
begin
  -- The node AIT is reset at power up by mm_rst and under software control by jesd204b_reset.
  -- The mm_rst internal will cause a reset on the rx_rst by the reset sequencer in the u_jesd204b.
  -- The MM jesd204b_reset is intended for node AIT resynchronisation tests of the u_jesd204b.
  -- The MM jesd204b_reset should not be applied in an SDP application, because this will cause
  -- a disturbance in the block timing of the out_sosi_arr(i).sync,bsn,sop,eop. The other logic
  -- in an SDP application assumes that the block timing of the out_sosi_arr(i) only contains
  -- complete blocks, so from sop to eop.

  mm_rst_internal <= mm_rst or mm_jesd_ctrl_reg(31);

  gen_jesd_disable : for I in 0 to c_nof_streams_jesd204b - 1 generate
    jesd204b_disable_arr(i) <= mm_jesd_ctrl_reg(i);
  end generate;

  -----------------------------------------------------------------------------
  -- JESD204B IP (ADC Handler)
  -----------------------------------------------------------------------------

  u_jesd204b: entity tech_jesd204b_lib.tech_jesd204b
  generic map(
    g_sim                => g_sim,
    g_nof_streams        => c_nof_streams_jesd204b,
    g_nof_sync_n         => g_nof_sync_n,
    g_jesd_freq          => g_jesd_freq
  )
  port map(
    jesd204b_refclk      => JESD204B_REFCLK,
    jesd204b_sysref      => JESD204B_SYSREF,
    jesd204b_sync_n_arr  => jesd204b_sync_n,

    rx_sosi_arr          => rx_sosi_arr,
    rx_clk               => rx_clk,
    rx_rst               => rx_rst,
    rx_sysref            => rx_sysref,

    jesd204b_disable_arr  => jesd204b_disable_arr,

    -- MM
    mm_clk               => mm_clk,
    mm_rst               => mm_rst_internal,

    jesd204b_mosi        => jesd204b_mosi,
    jesd204b_miso        => jesd204b_miso,

     -- Serial
    serial_tx_arr        => open,
    serial_rx_arr        => JESD204B_SERIAL_DATA(c_nof_streams_jesd204b - 1 downto 0)
  );

  -----------------------------------------------------------------------------
  -- Time delay: dp_shiftram
  -- . copied from unb1_bn_capture_input (apertif)
  --   Array range reversal is not done because everything is DOWNTO
  -- . the input valid is always '1', even when there is no data
  -----------------------------------------------------------------------------

  gen_force_valid : for I in 0 to c_nof_streams_jesd204b - 1 generate
    p_sosi : process(rx_sosi_arr)
    begin
      dp_shiftram_snk_in_arr(I)       <= rx_sosi_arr(I);
      dp_shiftram_snk_in_arr(I).valid <= '1';
    end process;
  end generate;

  u_dp_shiftram : entity dp_lib.dp_shiftram
  generic map (
    g_nof_streams => c_nof_streams_jesd204b,
    g_nof_words   => c_dp_shiftram_nof_samples,
    g_data_w      => c_data_w,
    g_use_sync_in => true
  )
  port map (
    dp_rst   => rx_rst,
    dp_clk   => rx_clk,

    mm_rst   => mm_rst_internal,
    mm_clk   => mm_clk,

    sync_in  => bs_sosi.sync,

    reg_mosi => reg_dp_shiftram_mosi,
    reg_miso => reg_dp_shiftram_miso,

    snk_in_arr => dp_shiftram_snk_in_arr,

    src_out_arr => ant_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- Timestamp
  -----------------------------------------------------------------------------
  u_bsn_source : entity dp_lib.mms_dp_bsn_source
  generic map (
    g_cross_clock_domain     => true,
    g_block_size             => c_bs_block_size,
    g_nof_block_per_sync     => c_bs_nof_block_per_sync,
    g_bsn_w                  => c_bs_bsn_w
  )
  port map (
    -- Clocks and reset
    mm_rst            => mm_rst_internal,
    mm_clk            => mm_clk,
    dp_rst            => rx_rst,
    dp_clk            => rx_clk,
    dp_pps            => rx_sysref,

    -- Memory-mapped clock domain
    reg_mosi          => reg_bsn_source_mosi,
    reg_miso          => reg_bsn_source_miso,

    -- Streaming clock domain
    bs_sosi           => bs_sosi
  );

  u_bsn_trigger_wg : entity dp_lib.mms_dp_bsn_scheduler
  generic map (
    g_cross_clock_domain => true,
    g_bsn_w              => c_bs_bsn_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst_internal,
    mm_clk      => mm_clk,

    reg_mosi    => reg_bsn_scheduler_wg_mosi,
    reg_miso    => reg_bsn_scheduler_wg_miso,

    -- Streaming clock domain
    dp_rst      => rx_rst,
    dp_clk      => rx_clk,

    snk_in      => bs_sosi,  -- only uses eop (= block sync), bsn[]
    trigger_out => trigger_wg
  );

  -----------------------------------------------------------------------------
  -- WG (Test Signal Generator)
  -----------------------------------------------------------------------------

  u_wg_arr : entity diag_lib.mms_diag_wg_wideband_arr
  generic map (
    g_nof_streams        => g_nof_streams,
    g_cross_clock_domain => true,
    g_buf_dir            => c_wg_buf_directory,

    -- Wideband parameters
    g_wideband_factor    => 1,

    -- Basic WG parameters, see diag_wg.vhd for their meaning
    g_buf_dat_w          => c_wg_buf_dat_w,
    g_buf_addr_w         => c_wg_buf_addr_w,
    g_calc_support       => true,
    g_calc_gain_w        => 1,
    g_calc_dat_w         => c_sdp_W_adc
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst              => mm_rst_internal,
    mm_clk              => mm_clk,

    reg_mosi            => reg_wg_mosi,
    reg_miso            => reg_wg_miso,

    buf_mosi            => ram_wg_mosi,
    buf_miso            => ram_wg_miso,

    -- Streaming clock domain
    st_rst              => rx_rst,
    st_clk              => rx_clk,
    st_restart          => trigger_wg,

    out_sosi_arr        => wg_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- ADC/WG Mux (Input Select)
  -----------------------------------------------------------------------------

  gen_mux : for I in 0 to g_nof_streams - 1 generate
    p_sosi : process(ant_sosi_arr(I), wg_sosi_arr(I))
    begin
      -- Default use the ADC data
      nxt_mux_sosi_arr(I).data  <= ant_sosi_arr(I).data;
      if wg_sosi_arr(I).valid = '1' then
        -- Valid WG data overrules ADC data
        nxt_mux_sosi_arr(I).data <= wg_sosi_arr(I).data;
      end if;
    end process;
  end generate;

  mux_sosi_arr  <= nxt_mux_sosi_arr when rising_edge(rx_clk);

  -----------------------------------------------------------------------------
  -- Concatenate muxed data streams with bsn framing
  -----------------------------------------------------------------------------

  gen_concat : for I in 0 to g_nof_streams - 1 generate
    p_sosi : process(mux_sosi_arr(I), bs_sosi)
    begin
      st_sosi_arr(I)       <= bs_sosi;
      st_sosi_arr(I).data  <= mux_sosi_arr(I).data;
    end process;
  end generate;

  ---------------------------------------------------------------------------------------
  -- Diagnostics on the bsn-framed data
  --   . BSN Monitor (ToDo: can be removed as not part of the spec)
  --   . Aduh monitor
  --   . Data Buffer (variable depth from 1k-128k)
  ---------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------
  -- BSN monitor (Block Checker)
  ---------------------------------------------------------------------------------------
  u_bsn_monitor : entity dp_lib.mms_dp_bsn_monitor
  generic map (
    g_nof_streams        => 1,  -- They're all the same
    g_sync_timeout       => g_bsn_sync_timeout,
    g_bsn_w              => c_bs_bsn_w,
    g_log_first_bsn      => false
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst_internal,
    mm_clk      => mm_clk,
    reg_mosi    => reg_bsn_monitor_input_mosi,
    reg_miso    => reg_bsn_monitor_input_miso,

    -- Streaming clock domain
    dp_rst      => rx_rst,
    dp_clk      => rx_clk,
    in_sosi_arr => st_sosi_arr(0 downto 0)
  );

  -----------------------------------------------------------------------------
  -- Monitor ADU/WG output
  -----------------------------------------------------------------------------
  u_aduh_monitor : entity aduh_lib.mms_aduh_monitor_arr
  generic map (
    g_cross_clock_domain   => true,
    g_nof_streams          => g_nof_streams,
    g_symbol_w             => c_data_w,  -- TBD 16?
    g_nof_symbols_per_data => 1,  -- Wideband factor is 1
    g_nof_accumulations    => 200000512,  -- = 195313 blocks * 1024 samples
    g_buffer_nof_symbols   => g_aduh_buffer_nof_symbols,  -- default 512, larger for full design
    g_buffer_use_sync      => true  -- True to capture all streams synchronously
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst         => mm_rst_internal,
    mm_clk         => mm_clk,

    reg_mosi       => reg_aduh_monitor_mosi,  -- read only access to the signal path data mean sum and power sum registers
    reg_miso       => reg_aduh_monitor_miso,
    buf_mosi       => ram_aduh_monitor_mosi,  -- read and overwrite access to the signal path data buffers
    buf_miso       => ram_aduh_monitor_miso,

    -- Streaming clock domain
    st_rst         => rx_rst,
    st_clk         => rx_clk,

    in_sosi_arr    => st_sosi_arr
  );

 -----------------------------------------------------------------------------
-- Diagnostic Data Buffer
  -----------------------------------------------------------------------------

  u_diag_data_buffer_bsn : entity diag_lib.mms_diag_data_buffer
  generic map (
    g_nof_streams  => g_nof_streams,
    g_data_w       => c_data_w,
    g_buf_nof_data => g_buf_nof_data,
    g_buf_use_sync => true  -- when TRUE start filling the buffer at the in_sync, else after the last word was read
  )
  port map (
    mm_rst            => mm_rst_internal,
    mm_clk            => mm_clk,
    dp_rst            => rx_rst,
    dp_clk            => rx_clk,

    ram_data_buf_mosi => ram_diag_data_buf_bsn_mosi,
    ram_data_buf_miso => ram_diag_data_buf_bsn_miso,
    reg_data_buf_mosi => reg_diag_data_buf_bsn_mosi,
    reg_data_buf_miso => reg_diag_data_buf_bsn_miso,

    in_sosi_arr       => st_sosi_arr,
    in_sync           => st_sosi_arr(0).sync
  );

  -----------------------------------------------------------------------------
  -- Output Stage
  --   . Thin dual clock fifo to cross from jesd frame clock (rx_clk) to dp_clk domain
  -----------------------------------------------------------------------------

  gen_dp_fifo_dc : for I in 0 to g_nof_streams - 1 generate
    u_dp_fifo_dc : entity dp_lib.dp_fifo_dc
      generic map (
        g_data_w         => c_data_w,
        g_use_empty      => false,  -- TRUE,
        g_use_ctrl       => true,
        g_use_sync       => true,
        g_use_bsn        => true,
        g_fifo_size      => c_dp_fifo_dc_size
      )
      port map (
        wr_rst           => rx_rst,
        wr_clk           => rx_clk,
        rd_rst           => dp_rst,
        rd_clk           => dp_clk,
        snk_in           => st_sosi_arr(I),
        src_out          => out_sosi_arr(I)
      );
  end generate;

  -----------------------------------------------------------------------------
  -- JESD Control register
  -----------------------------------------------------------------------------
  u_mm_jesd_ctrl_reg : entity common_lib.common_reg_r_w
  generic map (
    g_reg       => c_mm_jesd_ctrl_reg,
    g_init_reg  => (others => '0')
  )
  port map (
    rst       => mm_rst,
    clk       => mm_clk,
    -- control side
    wr_en     => jesd_ctrl_mosi.wr,
    wr_adr    => jesd_ctrl_mosi.address(c_mm_jesd_ctrl_reg.adr_w - 1 downto 0),
    wr_dat    => jesd_ctrl_mosi.wrdata(c_mm_jesd_ctrl_reg.dat_w - 1 downto 0),
    rd_en     => jesd_ctrl_mosi.rd,
    rd_adr    => jesd_ctrl_mosi.address(c_mm_jesd_ctrl_reg.adr_w - 1 downto 0),
    rd_dat    => jesd_ctrl_miso.rddata(c_mm_jesd_ctrl_reg.dat_w - 1 downto 0),
    rd_val    => OPEN,
    -- data side
    out_reg   => mm_jesd_ctrl_reg,
    in_reg    => mm_jesd_ctrl_reg
  );
end str;
