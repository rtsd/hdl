-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-- Author: Eric Kooistra
-- Date: 17 Apr 2023
-- Purpose:
-- . Multi test bench for dp_calculate_crc.
-- Description:
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_calculate_crc is
end tb_tb_dp_calculate_crc;

architecture tb of tb_tb_dp_calculate_crc is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  --  g_data_w      : NATURAL := 28;
  --  g_crc_w       : NATURAL := 28;
  --  g_gap_size    : NATURAL := 10

  u_no_gap : entity work.tb_dp_calculate_crc generic map (28, 28, 0);
  u_gap    : entity work.tb_dp_calculate_crc generic map (28, 28, 10);
end tb;
