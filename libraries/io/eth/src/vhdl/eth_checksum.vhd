-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

-- Purpose:
--   Can be used to calculate the checksum for IPv4, ICMP and UDP, provided
--   the correct words are provided between sop and eop.
-- Description:
--   Determine the 16 bit 1-complement checksum according IPv4 to for the valid
--   words between snk_in.sop and snk_in.eop, taking in account snk_in.empty.
--   . For checksum verification the result should be 0 when the words are OK.
--   . For checksum calculation the result should be used at the checksum field
--     in the packet header.
-- Remarks:
--   . At the snk_in.sop the checksum is initialized to 0.
--   . At the snk_in.eop the snk_in.empty LSBytes are padded with 0.
--   . The words do not need to be provided in order, because the checksum is
--     based on addition.
--   . Assume that snk_in.sop and snk_in.eop are only active when snk_in.valid
--     is active.
--   . Assume that between packets so from snk_in.eop to next snk_in.sop the
--     snk_in.valid is inactive and that snk_in.valid is only active for new
--     data.

entity eth_checksum is
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;

    snk_in        : in  t_dp_sosi;

    checksum      : out std_logic_vector(c_halfword_w - 1 downto 0);
    checksum_val  : out std_logic
  );
end eth_checksum;

architecture rtl of eth_checksum is
  signal in_data          : std_logic_vector(c_word_w - 1 downto 0);
  signal in_valid         : std_logic;
  signal in_sop           : std_logic;
  signal in_eop           : std_logic;

  signal prev_in_valid    : std_logic;
  signal prev_in_eop      : std_logic;
  signal prev_in_eop_dly  : std_logic;

  signal word_sum_cin     : unsigned(0 downto 0);  -- carry in
  signal word_sum_dat     : unsigned(c_halfword_w - 1 downto 0);
  signal word_sum         : unsigned(c_halfword_w downto 0);
  signal nxt_word_sum     : unsigned(c_halfword_w downto 0);

  signal sum_cin          : unsigned(0 downto 0);  -- carry in
  signal sum_dat          : unsigned(c_halfword_w - 1 downto 0);
  signal sum              : unsigned(c_halfword_w downto 0);
  signal nxt_sum          : unsigned(c_halfword_w downto 0);

  signal last_dat         : unsigned(c_halfword_w - 1 downto 0);

  signal i_checksum       : std_logic_vector(checksum'range);
  signal nxt_checksum     : std_logic_vector(checksum'range);
  signal i_checksum_val   : std_logic;
  signal nxt_checksum_val : std_logic;
begin
  checksum     <= i_checksum;
  checksum_val <= i_checksum_val;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      prev_in_valid   <= '0';
      prev_in_eop     <= '0';
      prev_in_eop_dly <= '0';
      word_sum        <= (others => '0');
      sum             <= (others => '0');
      i_checksum      <= (others => '0');
      i_checksum_val  <= '0';
    elsif rising_edge(clk) then
      -- input
      prev_in_valid   <= in_valid;
      prev_in_eop     <= in_eop;
      prev_in_eop_dly <= prev_in_eop;
      -- internal
      word_sum        <= nxt_word_sum;
      sum             <= nxt_sum;
      -- outputs
      i_checksum      <= nxt_checksum;
      i_checksum_val  <= nxt_checksum_val;
    end if;
  end process;

  -- Combinatorial sink input
  p_zero_empty : process(snk_in, in_eop)
  begin
    in_data <= snk_in.data(c_word_w - 1 downto 0);
    if in_eop = '1' then
      case to_integer(unsigned(snk_in.empty(c_eth_empty_w - 1 downto 0))) is
        when 1 => in_data <= snk_in.data(c_word_w - 1 downto   c_byte_w) & c_slv0(  c_byte_w - 1 downto 0);
        when 2 => in_data <= snk_in.data(c_word_w - 1 downto 2 * c_byte_w) & c_slv0(2 * c_byte_w - 1 downto 0);
        when 3 => in_data <= snk_in.data(c_word_w - 1 downto 3 * c_byte_w) & c_slv0(3 * c_byte_w - 1 downto 0);
        when others => null;
      end case;
    end if;
  end process;

  in_valid <= snk_in.valid;
  in_sop   <= snk_in.sop;
  in_eop   <= snk_in.eop;

  -- Word sum
  word_sum_cin(0) <= '0' when in_sop = '1' else word_sum(c_halfword_w);
  word_sum_dat    <= word_sum(c_halfword_w - 1 downto 0);
  nxt_word_sum    <= unsigned('0' & in_data(c_word_w  - 1 downto c_word_w / 2)) +
                     unsigned(      in_data(c_word_w / 2 - 1 downto          0)) + word_sum_cin when in_valid = '1' else
                     word_sum;

  -- Accumulated sum
  sum_cin(0)  <= sum(c_halfword_w);
  sum_dat     <= sum(c_halfword_w - 1 downto 0);
  nxt_sum     <= (others => '0')                            when in_sop = '1'        else
                 ('0' & sum_dat) + word_sum_dat + sum_cin when prev_in_valid = '1' else
                  sum;

  -- Accumulate the last carry
  last_dat    <= sum(c_halfword_w - 1 downto 0) + sum_cin + word_sum_cin;  -- Also add word_sum_cin in the case that the last word has a carry.

  -- Checksum is 1-complement of the sum
  nxt_checksum     <= not(std_logic_vector(last_dat)) when prev_in_eop_dly = '1' else i_checksum;
  nxt_checksum_val <=                             '1' when prev_in_eop_dly = '1' else
                                                  '0' when in_sop = '1'          else i_checksum_val;
end rtl;
