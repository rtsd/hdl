-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Multi test bench for tb_verify_pfb_wg
-- Description:
--   Use multi tb_tb to simulate ranges of generic settings. The g_tb_index is
--   used to identify the printed results of each tb instance in the
--   transcript logging.
--
-- Remark:
-- . It is easier to use a separate multi tb_tb with parallel tb instances,
--   than to simulate the generic settings in a sequential loop, because this
--   avoid having to code the loop control in the tb.
-- . The results from the transcript logging need to be copied manually and
--   then show them in e.g. a document table or plot. An alternative would
--   have been to log the printed results in a file, however that complicates
--   the tb, because one tb needs to open the file for 'write' and the other
--   tb then need to open it for 'append'. Using manual copy of text from
--   transcript window is acceptable.
-- . Can try different FIR filter coefficients from LOFAR1, pfs_coeff_final.m
--
-- Usage:
--   > as 4
--   > run -all
--   > testbench is selftesting.
--   > observe logging in transcript window
--     . select logging and copy it to tb_verify_pfb_wg.txt, or temp.txt
--     . use 'more temp.txt | grep wpfb_measured_proc_gain_a_dB' to extract
--       the estimated SNR processing gain
--

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_verify_pfb_wg is
end entity tb_tb_verify_pfb_wg;

architecture tb of tb_tb_verify_pfb_wg is
  -- LOFAR1
  constant c_fil_coefs       : string := "data/Coeffs16384Kaiser-quant_1wb";

  -- Modified LOFAR1
  constant c_fil_nodc        : string := "data/Coeffs16384Kaiser-quant-nodc_1wb";

  -- Bypass PFIR to have PFB = FFT
  constant c_fil_bypass      : string := "data/run_pfir_coeff_m_bypass_16taps_1024points_16b_1wb";

  -- PFIR coefficients from pfs_coeff_final.m
  -- . Created FIR filter with Hanning window for almost equal DC per polyphase
  -- . Created FIR filter with 18b or 20b coefficients to compare difference with 16b
  -- MATLAB > cd applications/lofar2/model/
  -- MATLAB > pfs_coeff_final
  -- > cp applications/lofar2/model/data/Coefficient_16KHanning_16b.dat libraries/dsp/filter/src/hex/
  -- > cp applications/lofar2/model/data/Coefficient_16KHanning_18b.dat libraries/dsp/filter/src/hex/
  -- > cp applications/lofar2/model/data/Coefficient_16KKaiser_16b.dat libraries/dsp/filter/src/hex/
  -- > cp applications/lofar2/model/data/Coefficient_16KKaiser_18b.dat libraries/dsp/filter/src/hex/
  -- > cd ../upe_gear/
  -- > . ./init_upe.sh
  -- > cd libraries/dsp/filter/src/python/
  -- > python fil_ppf_create_mifs.py -f ../hex/Coefficient_16KHanning_16b.dat -t 16 -p 1024 -w 1 -c 16
  -- > python fil_ppf_create_mifs.py -f ../hex/Coefficient_16KHanning_18b.dat -t 16 -p 1024 -w 1 -c 18
  -- > python fil_ppf_create_mifs.py -f ../hex/Coefficient_16KKaiser_16b.dat -t 16 -p 1024 -w 1 -c 16
  -- > python fil_ppf_create_mifs.py -f ../hex/Coefficient_16KKaiser_16b.dat -t 16 -p 1024 -w 1 -c 18
  -- > modelsim_config unb2c  # to update verify_pfb build dir
  -- > ll build/unb2c/modelsim/verify_pfb/data/Coefficient_*
  -- Select FIR coefficients from pfs_coeff_final.m via c_fil_hanning and c_fil_kaiser
  constant c_fil_hanning_16b : string := "data/Coefficient_16KHanning_16b_1wb";
  constant c_fil_hanning_18b : string := "data/Coefficient_16KHanning_18b_1wb";
  constant c_fil_hanning_20b : string := "data/Coefficient_16KHanning_20b_1wb";
  constant c_fil_kaiser_16b  : string := "data/Coefficient_16KKaiser_16b_1wb";
  constant c_fil_kaiser_18b  : string := "data/Coefficient_16KKaiser_18b_1wb";
  constant c_fil_kaiser_20b  : string := "data/Coefficient_16KKaiser_20b_1wb";

  signal tb_end : std_logic := '0';  -- tb_end is used to end a tb if it cannot end itself, but is not needed for tb_verify_pfb_wg
                                     -- however, do declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

  constant c_gen_ref                     : boolean := true;
  constant c_gen_g_fil_backoff_w_1       : boolean := false;
  constant c_gen_vary_g_fil_backoff_w    : boolean := false;
  constant c_gen_vary_g_fft_out_dat_w    : boolean := false;
  constant c_gen_2020_jan_18             : boolean := false;
  constant c_gen_vary_wg_integer_freq    : boolean := false;
  constant c_gen_vary_wg_fractional_freq : boolean := false;
  constant c_gen_vary_g_fft_stage_dat_w  : boolean := false;
  constant c_gen_vary_g_fil_in_dat_w     : boolean := false;
  constant c_gen_vary_g_amplitude_a      : boolean := false;
  constant c_gen_vary_c_twiddle_w        : boolean := false;
  constant c_gen_vary_extra_w            : boolean := false;
  constant c_gen_2020_dec                : boolean := false;
  constant c_gen_2022_mar                : boolean := false;
begin
-- generics of tb_verify_pfb_wg
--   g_tb_index        : NATURAL := 0;   -- use g_tb_index to identify and separate print_str() loggings from multi tb
--   g_sel_pfb         : STRING := "WPFB";  -- "WPFB" for APERTIF PFB, "PFB2" for LOFAR1 PBF
--
--   -- WG
--   g_subband_index_a : REAL := 61.0;   -- 0:511
--   g_subband_index_b : REAL := 61.0;   -- 0:511
--   g_amplitude_a     : REAL := 1.0;    -- 1.0 is full scale
--   g_amplitude_b     : REAL := 0.0;    -- 1.0 is full scale
--   g_phase_a         : REAL := 10.0;    -- 0:360 degrees
--   g_phase_b         : REAL := 0.0;    -- 0:360 degrees
--
--   -- WPFB fields in c_wpfb
--   -- . c_sdp_wpfb_subbands from sdp_pkg.vhd:
--   --   . g_fil_backoff_w   = 1
--   --   . g_fil_in_dat_w    = 14 = W_adc
--   --   . g_internal_dat_w  = 16 = number of bits between fil and fft
--   --   . g_fft_out_dat_w   = 18 = W_subband
--   --   . g_fft_out_gain_w  = 1
--   --   . g_fft_stage_dat_w = 18 = c_dsp_mult_w
--   --   . g_fft_guard_w     = 2
--   -- . c_wb1_two_real_1024 from tb_wpfb_unit_wide.vhd:
--   --   . g_fil_backoff_w   = 1
--   --   . g_fil_in_dat_w    = 8
--   --   . g_internal_dat_w  = 16 = number of bits between fil and fft
--   --   . g_fft_out_dat_w   = 16
--   --   . g_fft_out_gain_w  = 1
--   --   . g_fft_stage_dat_w = 18
--   --   . g_fft_guard_w     = 2
--
--   -- FIR filter
--   g_fil_coefs_file_prefix : STRING := "data/Coeffs16384Kaiser-quant_1wb";  -- PFIR coefficients file access
--   g_fil_coefs_file_prefix : STRING := "data/run_pfir_coeff_m_bypass_16taps_1024points_16b_1wb";   -- bypass PFIR
--   g_fil_coefs_file_prefix : STRING := "data/run_pfir_coeff_m_fircls1_16taps_1024points_16b_1wb";
--   g_fil_coef_dat_w        : NATURAL := 16;   -- = 16, data width of the FIR coefficients
--
--   g_fil_coefs_file_prefix : STRING := "data/run_pfir_coeff_m_fircls1_16taps_1024points_18b_1wb";
--   g_fil_coef_dat_w        : NATURAL := 18;   -- = 16, data width of the FIR coefficients
--
--   g_fil_backoff_w         : NATURAL := 0;    -- = 0, number of bits for input backoff to avoid output overflow
--   g_fil_in_dat_w          : NATURAL :=  8;   -- = W_adc, number of input bits
--
--   g_internal_dat_w        : NATURAL := 17;   -- = number of bits between fil and fft, g_internal_dat_w <= g_fft_stage_dat_w - g_fft_guard_w in fft_r2_pipe
--
--   -- FFT
--   g_fft_out_dat_w         : NATURAL := 16;   -- = W_subband, number of output bits
--   g_fft_out_gain_w        : NATURAL := 1;    -- = 1, output gain factor applied after the last stage output, before requantization to out_dat_w
--   g_fft_stage_dat_w       : NATURAL := 18;   -- = c_dsp_mult_w = 18, number of bits that are used inter-stage
--   g_fft_guard_w           : NATURAL := 1     -- = 2
--   g_switch_en             : STD_LOGIC := '0';  -- two real input decorrelation option in PFB2

  --                                                                 g_tb_index
  --                                                                 .      g_sel_pfb
  --                                                                 .       .     g_subband_index_a
  --                                                                 .       .     .     g_subband_index_b
  --                                                                 .       .     .     .    g_amplitude_a
  --                                                                 .       .     .     .    .    g_amplitude_b
  --                                                                 .       .     .     .    .    .     g_phase_a
  --                                                                 .       .     .     .    .    .     .     g_phase_b
  --                                                                 .       .     .     .    .    .     .     .             g_fil_coefs_file_prefix
  --                                                                 .       .     .     .    .    .     .     .             .   g_fil_coef_dat_w
  --                                                                 .       .     .     .    .    .     .     .             .   .  g_fil_backoff_w
  --                                                                 .       .     .     .    .    .     .     .             .   .  .   g_fil_in_dat_w
  --                                                                 .       .     .     .    .    .     .     .             .   .  .   .   g_internal_dat_w
  --                                                                 .       .     .     .    .    .     .     .             .   .  .   .   .   g_fft_out_dat_w
  --                                                                 .       .     .     .    .    .     .     .             .   .  .   .   .   .  g_fft_out_gain_w
  --                                                                 .       .     .     .    .    .     .     .             .   .  .   .   .   .  .   g_fft_stage_dat_w
  --                                                                 .       .     .     .    .    .     .     .             .   .  .   .   .   .  .   .  g_fft_guard_w
  --                                                                 .       .     .     .    .    .     .     .             .   .  .   .   .   .  .   .  .    g_switch_en
  --                                                                 .       .     .     .    .    .     .     .             .   .  .   .   .   .  .   .  .    .
gen_ref : if c_gen_ref generate  -- .       .     .     .    .    .     .     .             .   .  .   .   .   .  .   .  .    .
  -- WPFB                                                            .       .     .     .    .    .     .     .             .   .  .   .   .   .  .   .  .    .
  --u_apertif           : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1001, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1,  8, 16, 18, 1, 18, 2, '0');
  --u_lts_2020_11_23    : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1002, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 16, 18, 1, 18, 2, '0');
  ---- PFB2
  u_lofar1_12b        : entity work.tb_verify_pfb_wg generic map (1003, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 12, 18, 18, 0, 20, 0, '0');
  --u_lofar1_14b        : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1004, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 20, 0, '0');
  --u_lofar1_14b_22     : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1005, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 22, 0, '0');
  --u_lofar1_14b_24     : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1006, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 24, 0, '0');
  ---- WPFB
  --u_wpfb_stage18      : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1007, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');
  --u_wpfb_stage20      : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1008, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 20, 1, '0');
  --u_wpfb_stage22      : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1009, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 22, 1, '0');
  --u_wpfb_stage23      : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1010, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 23, 1, '0');
  --u_wpfb_stage24      : ENTITY work.tb_verify_pfb_wg GENERIC MAP (1011, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 24, 1, '0');
  -- c_twiddle_w = 18
  u_wpfb_lofar2_subbands_lts_2021 : entity work.tb_verify_pfb_wg generic map (1012, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 22, 1, '0');  -- = u_wpfb_stage22
  -- c_twiddle_w = 20
  u_wpfb_lofar2_subbands_dts_18b  : entity work.tb_verify_pfb_wg generic map (1013, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 1, 24, 1, '0');  -- = u_2000
  u_wpfb_lofar2_subbands_dts_19b  : entity work.tb_verify_pfb_wg generic map (1014, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 24, 1, '0');  -- = u_2001

-- Results:
--                                           Coeffs16384Kaiser-quant
--                                            .           Coeffs16384Kaiser-quant-nodc
-- WPFB                                       .            .
--tb-1001 . wpfb_measured_proc_gain_a_dB =   25.54 [dB]   25.65 [dB]
--tb-1002 . wpfb_measured_proc_gain_a_dB =   -0.80 [dB]    2.67 [dB]
-- PFB2
--tb-1003 . wpfb_measured_proc_gain_a_dB =   23.18 [dB]             , = u_lofar1_12b
--tb-1004 . wpfb_measured_proc_gain_a_dB =   15.24 [dB]             , = u_lofar1_14b
--tb-1005 . wpfb_measured_proc_gain_a_dB =   17.03 [dB]             , = u_lofar1_14b_22, improvement is < 3 dB
--tb-1006 . wpfb_measured_proc_gain_a_dB =   17.00 [dB]             , = u_lofar1_14b_24
-- WPFB
--tb-1007 . wpfb_measured_proc_gain_a_dB =    6.11 [dB]    9.94 [dB], = u_wpfb_stage18
--tb-1008 . wpfb_measured_proc_gain_a_dB =   12.38 [dB]   16.48 [dB], = u_wpfb_stage20 : ~3.1 dB per extra g_fft_stage_dat_w bit
--tb-1009 . wpfb_measured_proc_gain_a_dB =   18.79 [dB]   24.29 [dB], = u_wpfb_stage22 : ~3.2 dB per extra g_fft_stage_dat_w bit
--tb-1010 . wpfb_measured_proc_gain_a_dB =   19.86 [dB]   26.58 [dB], = u_wpfb_stage23 : ~1.1 dB per extra g_fft_stage_dat_w bit
--tb-1011 . wpfb_measured_proc_gain_a_dB =   20.08 [dB]   28.17 [dB], = u_wpfb_stage24 : ~0.2 dB per extra g_fft_stage_dat_w bit

--tb-1012 . wpfb_measured_proc_gain_a_dB = 19.26 [dB]  > 18.79 [dB] from u_wpfb_stage22 in 2021, due to now c_twiddle_w = 20 (?)
--tb-1013 . wpfb_measured_proc_gain_a_dB = 20.12 [dB]  = u_2000
--tb-1014 . wpfb_measured_proc_gain_a_dB = 18.50 [dB]  = u_2001

--Conclusion:
--* For g_fft_stage_dat_w <= 22 the processing gain increases ~3 dB per extra g_fft_stage_dat_w bit, therefore choose 22, 23 or 24, more than 24 bit has not benefit.
end generate;

gen_g_fil_backoff_w_1 : if c_gen_g_fil_backoff_w_1 generate
  -- g_subband_index_a = 60.4, to check that with g_fil_backoff_w = 1 there is no FIR filter overflow
  u_149 : entity work.tb_verify_pfb_wg generic map (149, "WPFB", 60.4, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 17, 18, 0, 18, 1, '0');
  -- g_subband_index_a = 60, WG at center subband frequency to determine PFB processing gain
  -- g_fft_guard_w = 1, check that no extra FFT backoff guard at first stage is needed when g_fil_backoff_w = 1
  u_150 : entity work.tb_verify_pfb_wg generic map (150, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 17, 18, 0, 18, 1, '0');
  u_151 : entity work.tb_verify_pfb_wg generic map (151, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 17, 18, 0, 20, 1, '0');
  u_152 : entity work.tb_verify_pfb_wg generic map (152, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 17, 18, 0, 22, 1, '0');
  u_153 : entity work.tb_verify_pfb_wg generic map (153, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 17, 18, 0, 23, 1, '0');
  u_154 : entity work.tb_verify_pfb_wg generic map (154, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 17, 18, 0, 24, 1, '0');
  u_155 : entity work.tb_verify_pfb_wg generic map (155, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 17, 18, 0, 25, 1, '0');
  -- g_fft_guard_w = 2, use extra FFT backoff guard at first FFT stage, which is compensated by no guard at last FFT stage, intermediate stages have backoff guard 1 to compensate for stage gain of factor 2
  u_156 : entity work.tb_verify_pfb_wg generic map (156, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 16, 18, 0, 18, 2, '0');
  u_157 : entity work.tb_verify_pfb_wg generic map (157, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 16, 18, 0, 20, 2, '0');
  u_158 : entity work.tb_verify_pfb_wg generic map (158, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 16, 18, 0, 22, 2, '0');
  u_159 : entity work.tb_verify_pfb_wg generic map (159, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 16, 18, 0, 23, 2, '0');
  u_160 : entity work.tb_verify_pfb_wg generic map (160, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 16, 18, 0, 24, 2, '0');
  u_161 : entity work.tb_verify_pfb_wg generic map (161, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 16, 18, 0, 25, 2, '0');
--Results:
--g_fil_backoff_w = 1
--             g_fft_stage_dat_w
--               .                               g_fft_guard_w = 1    g_fft_guard_w = 2
--tb-149        18 . wpfb_measured_proc_gain_a_dB =   25.56 [dB]                           -- so OK, because no overflow
--tb-150, 156   18 . wpfb_measured_proc_gain_a_dB =    1.05 [dB]           -0.80 [dB]      --  6.11 [dB] for u_wpfb_stage18
--tb-151, 157   20 . wpfb_measured_proc_gain_a_dB =    9.05 [dB]            6.38 [dB]      -- 12.38 [dB] for u_wpfb_stage20
--tb-152, 158   22 . wpfb_measured_proc_gain_a_dB =   16.13 [dB]           15.90 [dB]      -- 18.79 [dB] for u_wpfb_stage22
--                                                    16.52                                -- g_r2_mul_extra_w = 2
--                                                    16.13                                -- g_sepa_extra_w = 2
--tb-153, 159   23 . wpfb_measured_proc_gain_a_dB =   17.22 [dB]           16.78 [dB]      -- 19.86 [dB] for u_wpfb_stage23
--                                                    17.22 [dB]           16.64 [dB]      -- g_internal_dat_w = 16, 15 instead of 17, 16
--                                                    17.22 [dB]           16.78 [dB]      -- g_internal_dat_w = 18, 17 instead of 17, 16
--                                                    17.22 [dB]           16.51 [dB]      -- g_internal_dat_w = 20, 19 instead of 17, 16
--tb-154, 160   24 . wpfb_measured_proc_gain_a_dB =   17.38 [dB]           17.22 [dB]      -- 20.08 [dB] for u_wpfb_stage24
--tb-155, 161   25 . wpfb_measured_proc_gain_a_dB =   17.38 [dB]           17.55 [dB]      -- 20.39 [dB] for u_307
--Conclusion:
--* Using g_fil_backoff_w = 1 decreases the processing gain by ~3 dB (u_wpfb_stage24 - u_154 = 20.08 - 17.38 = 2.70 dB)
--* Using g_fft_guard_w = 2 for the first stage does not decrease the processing gain when g_fft_stage_dat_w ~=> 22. However
--  when g_fil_backoff_w = 1 then it is not necessary to use g_fft_guard_w > 1, because then the input to the FFT is already
--  scaled down by the factor 2 of g_fil_backoff_w = 1.
end generate;

gen_vary_g_fil_backoff_w : if c_gen_vary_g_fil_backoff_w generate
  u_1000 : entity work.tb_verify_pfb_wg generic map (1000, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 22, 1, '0');  -- = u_wpfb_stage22
  u_1001 : entity work.tb_verify_pfb_wg generic map (1001, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 17, 18, 0, 22, 0, '0');
  u_1002 : entity work.tb_verify_pfb_wg generic map (1002, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 18, 19, 0, 22, 0, '0');
  u_1003 : entity work.tb_verify_pfb_wg generic map (1003, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 18, 19, 1, 22, 0, '0');
--Results:
-- . wpfb_measured_proc_gain_a_dB =   18.79 [dB]
-- . wpfb_measured_proc_gain_a_dB =   16.64 [dB]
-- . wpfb_measured_proc_gain_a_dB =   16.89 [dB]
-- . wpfb_measured_proc_gain_a_dB =   15.89 [dB]
end generate;

gen_vary_g_fft_out_dat_w : if c_gen_vary_g_fft_out_dat_w generate
  -- WPFB
  u_100 : entity work.tb_verify_pfb_wg generic map (100, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 22, 1, '0');  -- = u_wpfb_stage22
  u_101 : entity work.tb_verify_pfb_wg generic map (101, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 19, 0, 22, 1, '0');
  u_102 : entity work.tb_verify_pfb_wg generic map (102, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 20, 0, 22, 1, '0');
  -- PFB2
  u_103 : entity work.tb_verify_pfb_wg generic map (103, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 22, 0, '0');
  u_104 : entity work.tb_verify_pfb_wg generic map (104, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 19, 0, 22, 0, '0');
  u_105 : entity work.tb_verify_pfb_wg generic map (105, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 20, 0, 22, 0, '0');
-- Results:
--       g_fft_out_dat_w
-- WPFB    .
--tb-100  18 . wpfb_measured_proc_gain_a_dB =   18.79 [dB]
--tb-101  19 . wpfb_measured_proc_gain_a_dB =   17.38 [dB]
--tb-102  20 . wpfb_measured_proc_gain_a_dB =   17.42 [dB]
-- PFB2
--tb-103  18 . wpfb_measured_proc_gain_a_dB =   17.03 [dB]
--tb-104  19 . wpfb_measured_proc_gain_a_dB =   15.70 [dB]
--tb-105  20 . wpfb_measured_proc_gain_a_dB =   16.36 [dB]
end generate;

gen_2020_jan_18 : if c_gen_2020_jan_18 generate
  u_200  : entity work.tb_verify_pfb_wg generic map (200, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14, 16, 18, 1, 18, 2, '0');  -- = u_lts_2020_11_23
  u_201  : entity work.tb_verify_pfb_wg generic map (201, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_204  : entity work.tb_verify_pfb_wg generic map (204, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 20, 1, '0');  -- = u_wpfb_stage20

-- Results:
-- Table C: PFB processing gain for APERTIF WPFB quick improvements
--
-- tb-200 . wpfb_measured_proc_gain_a_dB =   -0.80 [dB]  current lofar2_unb2b_filterbank settings 2020-11-23
-- tb-201 . wpfb_measured_proc_gain_a_dB =    6.11 [dB]  + g_fil_backoff_w = 0 instead of 1,
--                                                       + g_fft_guard_w = 1 instead of 2,
--                                                       + g_internal_dat_w = 17 instead of 16
-- tb-202 . wpfb_measured_proc_gain_a_dB =    6.53 [dB]  + g_r2_mul_extra_w = 2 instead of 0
-- tb-203 . wpfb_measured_proc_gain_a_dB =    6.53 [dB]  + g_sepa_extra_w = 2 instead of 0
-- tb-204 . wpfb_measured_proc_gain_a_dB =   12.38 [dB]  + g_fft_stage_dat_w = 20 instead of 18
-- tb-205 . wpfb_measured_proc_gain_a_dB =   12.35 [dB]  + g_fft_stage_dat_w = 20 instead of 18, g_r2_mul_extra_w = 2
-- tb-206 . wpfb_measured_proc_gain_a_dB =   14.62 [dB]  + g_fft_stage_dat_w = 20 instead of 18, g_r2_mul_extra_w = 2, g_sepa_extra_w = 2
end generate;

gen_vary_wg_integer_freq : if c_gen_vary_wg_integer_freq generate
  u_2001 : entity work.tb_verify_pfb_wg generic map (2001, "WPFB",  1.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2002 : entity work.tb_verify_pfb_wg generic map (2002, "WPFB",  2.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2003 : entity work.tb_verify_pfb_wg generic map (2003, "WPFB",  3.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2004 : entity work.tb_verify_pfb_wg generic map (2004, "WPFB",  4.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2008 : entity work.tb_verify_pfb_wg generic map (2008, "WPFB",  8.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2016 : entity work.tb_verify_pfb_wg generic map (2016, "WPFB", 16.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2032 : entity work.tb_verify_pfb_wg generic map (2032, "WPFB", 32.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2037 : entity work.tb_verify_pfb_wg generic map (2037, "WPFB", 37.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2061 : entity work.tb_verify_pfb_wg generic map (2061, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2064 : entity work.tb_verify_pfb_wg generic map (2064, "WPFB", 64.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2117 : entity work.tb_verify_pfb_wg generic map (2117, "WPFB", 117.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2128 : entity work.tb_verify_pfb_wg generic map (2128, "WPFB", 128.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2256 : entity work.tb_verify_pfb_wg generic map (2256, "WPFB", 256.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2257 : entity work.tb_verify_pfb_wg generic map (2257, "WPFB", 257.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2373 : entity work.tb_verify_pfb_wg generic map (2373, "WPFB", 373.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_2503 : entity work.tb_verify_pfb_wg generic map (2503, "WPFB", 503.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18

-- Results:
-- g_subband_index_a
--   1 . wpfb_measured_proc_gain_a_dB =    6.95 [dB]
--   2 . wpfb_measured_proc_gain_a_dB =    6.90 [dB]
--   3 . wpfb_measured_proc_gain_a_dB =    6.89 [dB]
--   4 . wpfb_measured_proc_gain_a_dB =    6.81 [dB]
--   8 . wpfb_measured_proc_gain_a_dB =    7.79 [dB]
--  16 . wpfb_measured_proc_gain_a_dB =    7.97 [dB]
--  32 . wpfb_measured_proc_gain_a_dB =    8.29 [dB]
--  37 . wpfb_measured_proc_gain_a_dB =    6.03 [dB]
--  61 . wpfb_measured_proc_gain_a_dB =    6.11 [dB]
--  64 . wpfb_measured_proc_gain_a_dB =    9.06 [dB]
-- 117 . wpfb_measured_proc_gain_a_dB =    6.32 [dB]
-- 128 . wpfb_measured_proc_gain_a_dB =   11.69 [dB], due to wrong wg_measured_snr_a_dB =  80.54 [dB], using c_wg_snr_a_dB = 86.05 [dB] and sst_measured_snr_a_dB = 92.24 [dB], yields 6.20 dB.
-- 256 . wpfb_measured_proc_gain_a_dB = -153.35 [dB], due to wrong wg_measured_snr_a_dB = 246.87 [dB], using c_wg_snr_a_dB = 86.05 [dB] and sst_measured_snr_a_dB = 93.52 [dB], yields 7.47 dB.
-- 257 . wpfb_measured_proc_gain_a_dB =    6.74 [dB]
-- 373 . wpfb_measured_proc_gain_a_dB =    6.37 [dB]
-- 503 . wpfb_measured_proc_gain_a_dB =    6.67 [dB]
end generate;

gen_vary_wg_fractional_freq : if c_gen_vary_wg_fractional_freq generate
  -- Use fractions that fit integer number of periods in sync interval c_N_blk = c_wpfb.nof_blk_per_sync = 10, so c_N_blk*fraction must be integer, to have stable SST value
  -- Need to use g_amplitude_a = 0.9 ~< 0.95 to avoid overflow in PFS output, that occurs for some fractional g_subband_index_a
  -- WG freq 60.0
  u_600 : entity work.tb_verify_pfb_wg generic map (600, "WPFB", 60.0, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_601 : entity work.tb_verify_pfb_wg generic map (601, "WPFB", 60.1, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_602 : entity work.tb_verify_pfb_wg generic map (602, "WPFB", 60.2, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_603 : entity work.tb_verify_pfb_wg generic map (603, "WPFB", 60.3, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_604 : entity work.tb_verify_pfb_wg generic map (604, "WPFB", 60.4, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_605 : entity work.tb_verify_pfb_wg generic map (605, "WPFB", 60.5, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_606 : entity work.tb_verify_pfb_wg generic map (606, "WPFB", 60.6, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_607 : entity work.tb_verify_pfb_wg generic map (607, "WPFB", 60.7, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_608 : entity work.tb_verify_pfb_wg generic map (608, "WPFB", 60.8, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_609 : entity work.tb_verify_pfb_wg generic map (609, "WPFB", 60.9, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  -- WG freq 61.0
  u_610 : entity work.tb_verify_pfb_wg generic map (610, "WPFB", 61.0, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18 freq 61
  u_611 : entity work.tb_verify_pfb_wg generic map (611, "WPFB", 61.1, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_612 : entity work.tb_verify_pfb_wg generic map (612, "WPFB", 61.2, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_613 : entity work.tb_verify_pfb_wg generic map (613, "WPFB", 61.3, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_614 : entity work.tb_verify_pfb_wg generic map (614, "WPFB", 61.4, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_615 : entity work.tb_verify_pfb_wg generic map (615, "WPFB", 61.5, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_616 : entity work.tb_verify_pfb_wg generic map (616, "WPFB", 61.6, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_617 : entity work.tb_verify_pfb_wg generic map (617, "WPFB", 61.7, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_618 : entity work.tb_verify_pfb_wg generic map (618, "WPFB", 61.8, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_619 : entity work.tb_verify_pfb_wg generic map (619, "WPFB", 61.9, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  -- WG freq 62.0
  u_620 : entity work.tb_verify_pfb_wg generic map (620, "WPFB", 62.0, 61.0, 0.9, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18

-- Note>:
-- . For fractional subband frequencies the WG can only generate the average frequency, due to limited period accuracy of WG. This causes
--   the WG SNR to be about 55.1 +- 0.1 dB for fractional subband frequencies, instead of 85.0 dB, so about 30 dB less. The WG quantization
--   noise is not white noise, as can be seen by cw_noise_a in analogue format in the Modelsim Wave Window
-- Results:
-- g_subband_index_a
-- 60.0 . wpfb_measured_proc_gain_a_dB =    6.09 [dB]
-- 60.1 . wpfb_measured_proc_gain_a_dB =   26.57 [dB], the processing gain is higher due to that the WG input SNR is much lower for fractional subband frequencies
-- 60.2 . wpfb_measured_proc_gain_a_dB =   25.58 [dB]
-- 60.3 . wpfb_measured_proc_gain_a_dB =   26.55 [dB]
-- 60.4 . wpfb_measured_proc_gain_a_dB =   26.52 [dB]
-- 60.5 . wpfb_measured_proc_gain_a_dB =   25.73 [dB]
-- 60.6 . wpfb_measured_proc_gain_a_dB =   26.51 [dB]
-- 60.7 . wpfb_measured_proc_gain_a_dB =   26.66 [dB]
-- 60.8 . wpfb_measured_proc_gain_a_dB =   25.70 [dB]
-- 60.9 . wpfb_measured_proc_gain_a_dB =   26.67 [dB]
-- 61.0 . wpfb_measured_proc_gain_a_dB =    6.43 [dB]
-- 61.1 . wpfb_measured_proc_gain_a_dB =   26.64 [dB]
-- 61.2 . wpfb_measured_proc_gain_a_dB =   25.65 [dB]
-- 61.3 . wpfb_measured_proc_gain_a_dB =   26.59 [dB]
-- 61.4 . wpfb_measured_proc_gain_a_dB =   26.45 [dB]
-- 61.5 . wpfb_measured_proc_gain_a_dB =   25.73 [dB]
-- 61.6 . wpfb_measured_proc_gain_a_dB =   26.47 [dB]
-- 61.7 . wpfb_measured_proc_gain_a_dB =   26.56 [dB]
-- 61.8 . wpfb_measured_proc_gain_a_dB =   25.59 [dB]
-- 61.9 . wpfb_measured_proc_gain_a_dB =   26.57 [dB]
-- 62.0 . wpfb_measured_proc_gain_a_dB =    6.06 [dB]
end generate;

gen_vary_g_fft_stage_dat_w : if c_gen_vary_g_fft_stage_dat_w generate
  -- g_internal_dat_w = constant
  -- WPFB
  u_300 : entity work.tb_verify_pfb_wg generic map (300, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_301 : entity work.tb_verify_pfb_wg generic map (301, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 19, 1, '0');
  u_302 : entity work.tb_verify_pfb_wg generic map (302, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 20, 1, '0');  -- = u_wpfb_stage20
  u_303 : entity work.tb_verify_pfb_wg generic map (303, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 21, 1, '0');
  u_304 : entity work.tb_verify_pfb_wg generic map (304, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 22, 1, '0');  -- = u_wpfb_stage22
  u_305 : entity work.tb_verify_pfb_wg generic map (305, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 23, 1, '0');  -- = u_wpfb_stage23
  u_306 : entity work.tb_verify_pfb_wg generic map (306, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 24, 1, '0');  -- = u_wpfb_stage24
  u_307 : entity work.tb_verify_pfb_wg generic map (307, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 25, 1, '0');

  -- PFB2
  u_310 : entity work.tb_verify_pfb_wg generic map (310, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 18, 0, '0');
  u_311 : entity work.tb_verify_pfb_wg generic map (311, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 19, 0, '0');
  u_312 : entity work.tb_verify_pfb_wg generic map (312, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 20, 0, '0');  -- = u_lofar1_14b
  u_313 : entity work.tb_verify_pfb_wg generic map (313, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 21, 0, '0');
  u_314 : entity work.tb_verify_pfb_wg generic map (314, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 22, 0, '0');
  u_315 : entity work.tb_verify_pfb_wg generic map (315, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 23, 0, '0');
  u_316 : entity work.tb_verify_pfb_wg generic map (316, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 24, 0, '0');
  u_317 : entity work.tb_verify_pfb_wg generic map (317, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 25, 0, '0');

  -- WPFB only FFT
  u_320 : entity work.tb_verify_pfb_wg generic map (320, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_321 : entity work.tb_verify_pfb_wg generic map (321, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 19, 1, '0');
  u_322 : entity work.tb_verify_pfb_wg generic map (322, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 20, 1, '0');
  u_323 : entity work.tb_verify_pfb_wg generic map (323, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 21, 1, '0');
  u_324 : entity work.tb_verify_pfb_wg generic map (324, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 22, 1, '0');
  u_325 : entity work.tb_verify_pfb_wg generic map (325, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 23, 1, '0');
  u_326 : entity work.tb_verify_pfb_wg generic map (326, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 24, 1, '0');
  u_327 : entity work.tb_verify_pfb_wg generic map (327, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 25, 1, '0');

  -- PFB2 only FFT
  u_330 : entity work.tb_verify_pfb_wg generic map (330, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 18, 0, '0');
  u_331 : entity work.tb_verify_pfb_wg generic map (331, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 19, 0, '0');
  u_332 : entity work.tb_verify_pfb_wg generic map (332, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 20, 0, '0');
  u_333 : entity work.tb_verify_pfb_wg generic map (333, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 21, 0, '0');
  u_334 : entity work.tb_verify_pfb_wg generic map (334, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 22, 0, '0');
  u_335 : entity work.tb_verify_pfb_wg generic map (335, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 23, 0, '0');
  u_336 : entity work.tb_verify_pfb_wg generic map (336, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 24, 0, '0');
  u_337 : entity work.tb_verify_pfb_wg generic map (337, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 25, 0, '0');

  -- g_internal_dat_w = incrementing with g_fft_stage_dat_w
  -- WPFB
  u_340 : entity work.tb_verify_pfb_wg generic map (340, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_341 : entity work.tb_verify_pfb_wg generic map (341, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 19, 1, '0');
  u_342 : entity work.tb_verify_pfb_wg generic map (342, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 19, 18, 0, 20, 1, '0');
  u_343 : entity work.tb_verify_pfb_wg generic map (343, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 20, 18, 0, 21, 1, '0');
  u_344 : entity work.tb_verify_pfb_wg generic map (344, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 21, 18, 0, 22, 1, '0');
  u_345 : entity work.tb_verify_pfb_wg generic map (345, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 22, 18, 0, 23, 1, '0');
  u_346 : entity work.tb_verify_pfb_wg generic map (346, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 23, 18, 0, 24, 1, '0');
  u_347 : entity work.tb_verify_pfb_wg generic map (347, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 24, 18, 0, 25, 1, '0');

  -- PFB2
  u_350 : entity work.tb_verify_pfb_wg generic map (350, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 18, 0, '0');
  u_351 : entity work.tb_verify_pfb_wg generic map (351, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 19, 18, 0, 19, 0, '0');
  u_352 : entity work.tb_verify_pfb_wg generic map (352, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 20, 18, 0, 20, 0, '0');
  u_353 : entity work.tb_verify_pfb_wg generic map (353, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 21, 18, 0, 21, 0, '0');
  u_354 : entity work.tb_verify_pfb_wg generic map (354, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 22, 18, 0, 22, 0, '0');
  u_355 : entity work.tb_verify_pfb_wg generic map (355, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 23, 18, 0, 23, 0, '0');
  u_356 : entity work.tb_verify_pfb_wg generic map (356, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 24, 18, 0, 24, 0, '0');
  u_357 : entity work.tb_verify_pfb_wg generic map (357, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 25, 18, 0, 25, 0, '0');

-- Results:
-- Table A: PFB processing gain for increasing internal data width
--
-- g_fil_in_dat_w = 14 ADC data (full scale sinus)
--   g_fft_stage_dat_w
--    .                                   Coeffs16384Kaiser-quant                                 Only FFT (bypass FIR)
--    .               g_internal_dat_w :  Constant     Incrementing  Constant     Incrementing    Constant     Constant
--    .                                   APERTIF                    LOFAR1                       APERTIF      LOFAR1
--    .                                   WPFB                       PBF2                         WPFB         PBF2
--   18. wpfb_measured_proc_gain_a_dB =    6.11 [dB]    6.11 [dB]    12.07 [dB]   12.07  [dB]      9.97 [dB]   19.56 [dB]
--   19. wpfb_measured_proc_gain_a_dB =    7.49 [dB]    7.61 [dB]    14.27 [dB]   14.81  [dB]     11.17 [dB]   21.43 [dB]
--   20. wpfb_measured_proc_gain_a_dB =   12.38 [dB]   12.58 [dB]    15.24 [dB]   15.95  [dB]     16.36 [dB]   22.11 [dB]
--   21. wpfb_measured_proc_gain_a_dB =   15.84 [dB]   15.93 [dB]    16.82 [dB]   16.92  [dB]     20.69 [dB]   23.17 [dB]
--   22. wpfb_measured_proc_gain_a_dB =   18.79 [dB]   18.79 [dB]    17.03 [dB]   16.92  [dB]     23.96 [dB]   24.05 [dB]
--   23. wpfb_measured_proc_gain_a_dB =   19.86 [dB]   19.93 [dB]    17.03 [dB]   17.18  [dB]     26.00 [dB]   24.00 [dB]
--   24. wpfb_measured_proc_gain_a_dB =   20.08 [dB]   20.16 [dB]    17.00 [dB]   17.00  [dB]     28.22 [dB]   23.96 [dB]
--   25. wpfb_measured_proc_gain_a_dB =   20.39 [dB]   20.23 [dB]    17.00 [dB]   17.07  [dB]     28.22 [dB]   24.19 [dB]
--
--   . c_twiddle_w                    =   18           18            16           16              18           16
end generate;

gen_vary_g_fil_in_dat_w : if c_gen_vary_g_fil_in_dat_w generate
  u_400 : entity work.tb_verify_pfb_wg generic map (400, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0,  8, 17, 18, 0, 20, 1, '0');
  u_401 : entity work.tb_verify_pfb_wg generic map (401, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0,  9, 17, 18, 0, 20, 1, '0');
  u_402 : entity work.tb_verify_pfb_wg generic map (402, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 10, 17, 18, 0, 20, 1, '0');
  u_403 : entity work.tb_verify_pfb_wg generic map (403, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 11, 17, 18, 0, 20, 1, '0');
  u_404 : entity work.tb_verify_pfb_wg generic map (404, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 12, 17, 18, 0, 20, 1, '0');
  u_405 : entity work.tb_verify_pfb_wg generic map (405, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 13, 17, 18, 0, 20, 1, '0');
  u_406 : entity work.tb_verify_pfb_wg generic map (406, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 20, 1, '0');  -- u_wpfb_stage20

  u_410 : entity work.tb_verify_pfb_wg generic map (410, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0,  8, 18, 18, 0, 20, 0, '0');
  u_411 : entity work.tb_verify_pfb_wg generic map (411, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0,  9, 18, 18, 0, 20, 0, '0');
  u_412 : entity work.tb_verify_pfb_wg generic map (412, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 10, 18, 18, 0, 20, 0, '0');
  u_413 : entity work.tb_verify_pfb_wg generic map (413, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 11, 18, 18, 0, 20, 0, '0');
  u_414 : entity work.tb_verify_pfb_wg generic map (414, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 12, 18, 18, 0, 20, 0, '0');
  u_415 : entity work.tb_verify_pfb_wg generic map (415, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 13, 18, 18, 0, 20, 0, '0');
  u_416 : entity work.tb_verify_pfb_wg generic map (416, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 20, 0, '0');  -- u_lofar1_14b

  u_420 : entity work.tb_verify_pfb_wg generic map (420, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0,  8, 17, 18, 0, 20, 1, '0');
  u_421 : entity work.tb_verify_pfb_wg generic map (421, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0,  9, 17, 18, 0, 20, 1, '0');
  u_422 : entity work.tb_verify_pfb_wg generic map (422, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 10, 17, 18, 0, 20, 1, '0');
  u_423 : entity work.tb_verify_pfb_wg generic map (423, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 11, 17, 18, 0, 20, 1, '0');
  u_424 : entity work.tb_verify_pfb_wg generic map (424, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 12, 17, 18, 0, 20, 1, '0');
  u_425 : entity work.tb_verify_pfb_wg generic map (425, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 13, 17, 18, 0, 20, 1, '0');
  u_426 : entity work.tb_verify_pfb_wg generic map (426, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 20, 1, '0');

  u_430 : entity work.tb_verify_pfb_wg generic map (430, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0,  8, 18, 18, 0, 20, 0, '0');
  u_431 : entity work.tb_verify_pfb_wg generic map (431, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0,  9, 18, 18, 0, 20, 0, '0');
  u_432 : entity work.tb_verify_pfb_wg generic map (432, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 10, 18, 18, 0, 20, 0, '0');
  u_433 : entity work.tb_verify_pfb_wg generic map (433, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 11, 18, 18, 0, 20, 0, '0');
  u_434 : entity work.tb_verify_pfb_wg generic map (434, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 12, 18, 18, 0, 20, 0, '0');
  u_435 : entity work.tb_verify_pfb_wg generic map (435, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 13, 18, 18, 0, 20, 0, '0');
  u_436 : entity work.tb_verify_pfb_wg generic map (436, "PFB2", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 18, 18, 0, 20, 0, '0');

-- Results:
-- The table B) shows the processing gain for different internal ADC input width between 8b and 14b. Conclusions:
--
-- g_fft_stage_dat_w = 20
--   g_fil_in_dat_w ADC data (full scale sinus)
--    .                                   Coeffs16384Kaiser-quant    Only FFT (bypass FIR)
--    .                                   APERTIF      LOFAR1        APERTIF      LOFAR1
--    .                                   WPFB         PBF2          WPFB         PBF2
--    8. wpfb_measured_proc_gain_a_dB =   26.90 [dB]   26.96 [dB]    26.90 [dB]   27.01 [dB]
--    9. wpfb_measured_proc_gain_a_dB =   26.59 [dB]   26.76 [dB]    26.69 [dB]   26.96 [dB]
--   10. wpfb_measured_proc_gain_a_dB =   26.10 [dB]   26.50 [dB]    26.29 [dB]   26.87 [dB]
--   11. wpfb_measured_proc_gain_a_dB =   24.63 [dB]   25.38 [dB]    25.44 [dB]   26.57 [dB]
--   12. wpfb_measured_proc_gain_a_dB =   22.12 [dB]   23.18 [dB]    24.01 [dB]   25.65 [dB]
--   13. wpfb_measured_proc_gain_a_dB =   17.66 [dB]   19.51 [dB]    20.82 [dB]   23.52 [dB]
--   14. wpfb_measured_proc_gain_a_dB =   12.38 [dB]   15.24 [dB]    16.36 [dB]   22.11 [dB]
end generate;

-- 2021_jan_11
gen_vary_g_amplitude_a : if c_gen_vary_g_amplitude_a generate
  u_760 : entity work.tb_verify_pfb_wg generic map (760, "WPFB", 61.0, 61.0, 1.0,      0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18, 1.0
  u_761 : entity work.tb_verify_pfb_wg generic map (761, "WPFB", 61.0, 61.0, 0.5,      0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_762 : entity work.tb_verify_pfb_wg generic map (762, "WPFB", 61.0, 61.0, 0.25,     0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_763 : entity work.tb_verify_pfb_wg generic map (763, "WPFB", 61.0, 61.0, 0.125,    0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_764 : entity work.tb_verify_pfb_wg generic map (764, "WPFB", 61.0, 61.0, 0.0625,   0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_765 : entity work.tb_verify_pfb_wg generic map (765, "WPFB", 61.0, 61.0, 0.03125,  0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
  u_766 : entity work.tb_verify_pfb_wg generic map (766, "WPFB", 61.0, 61.0, 0.015625, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');  -- = u_wpfb_stage18
--Results:
--tb-761 . wpfb_measured_proc_gain_a_dB =    6.11 [dB]
--tb-762 . wpfb_measured_proc_gain_a_dB =    6.15 [dB]
--tb-763 . wpfb_measured_proc_gain_a_dB =    7.05 [dB]
--tb-760 . wpfb_measured_proc_gain_a_dB =    6.73 [dB]
--tb-764 . wpfb_measured_proc_gain_a_dB =    7.18 [dB]
--tb-765 . wpfb_measured_proc_gain_a_dB =    6.79 [dB]
--tb-766 . wpfb_measured_proc_gain_a_dB =    7.03 [dB]
end generate;

gen_vary_c_twiddle_w : if c_gen_vary_c_twiddle_w generate
  -- WPFB only FFT
  u_0 : entity work.tb_verify_pfb_wg generic map (0, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 22, 1, '0');  -- = u_324
  u_1 : entity work.tb_verify_pfb_wg generic map (1, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0, c_fil_bypass, 16, 0, 14, 17, 18, 0, 24, 1, '0');  -- = u_326

-- Rerun the simulation per c_twiddle_w setting ## by first manually doing:
-- > cp libraries/dsp/rTwoSDF/src/vhdl/pkg/twiddlesPkg_w##.vhd libraries/dsp/rTwoSDF/src/vhdl/twiddlesPkg.vhd
-- > mk all
-- and then collect the results from the transcript window:
--
-- Results:
-- c_twiddle_w                            g_stage_dat_w
--   .                                           22            24
--  13 : wpfb_measured_proc_gain_a_dB =    8.49 [dB]     8.76 [dB]
--  14 : wpfb_measured_proc_gain_a_dB =   13.15 [dB]    13.83 [dB]
--  15 : wpfb_measured_proc_gain_a_dB =   18.08 [dB]    19.19 [dB]
--  16 : wpfb_measured_proc_gain_a_dB =   21.53 [dB]    22.85 [dB]
--  17 : wpfb_measured_proc_gain_a_dB =   23.29 [dB]    26.62 [dB]
--  18 : wpfb_measured_proc_gain_a_dB =   23.96 [dB]    28.22 [dB]  -- = u_324, u_326  ==> Choose c_twiddle_w >= g_fft_out_dat_w
--  19 : wpfb_measured_proc_gain_a_dB =   24.54 [dB]    27.76 [dB]
--  20 : wpfb_measured_proc_gain_a_dB =   24.97 [dB]    28.22 [dB]
--  21 : wpfb_measured_proc_gain_a_dB =   24.75 [dB]    28.22 [dB]
end generate;

gen_vary_extra_w : if c_gen_vary_extra_w generate
--Conclusion:
--* If g_fft_stage_dat_w is large enough (~=> 24), then using extra_w has no benefit (as expected)
--* Combination of using both g_r2_mul_extra_w and g_sepa_extra_w has most benefit, for
--  g_fft_stage_dat_w ~=> 22 it is sufficient to use 1, 1.
--* Using g_fft_stage_dat_w = 22 with extra_w 1, 1 yields 19.59 [dB],
--  using g_fft_stage_dat_w = 24 with extra_w 0, 0 yields 20.08 [dB] so ~= 0.49 dB better
--  using g_fft_stage_dat_w = 24 with extra_w 1, 1 yields 20.31 [dB] so ~= 0.72 dB better, but with 2, 2 it is even slightly less.
end generate;

gen_2020_dec : if c_gen_2020_dec generate
  -- g_internal_dat_w = g_fft_stage_dat_w - g_fft_guard_w
  -- g_fft_out_dat_w = 18
  u_800 : entity work.tb_verify_pfb_wg generic map (800, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 18, 0, 18, 1, '0');
  u_801 : entity work.tb_verify_pfb_wg generic map (801, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 18, 0, 19, 1, '0');
  u_802 : entity work.tb_verify_pfb_wg generic map (802, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 19, 18, 0, 20, 1, '0');
  u_803 : entity work.tb_verify_pfb_wg generic map (803, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 20, 18, 0, 21, 1, '0');
  u_804 : entity work.tb_verify_pfb_wg generic map (804, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 21, 18, 0, 22, 1, '0');
  u_805 : entity work.tb_verify_pfb_wg generic map (805, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 22, 18, 0, 23, 1, '0');
  u_806 : entity work.tb_verify_pfb_wg generic map (806, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 23, 18, 0, 24, 1, '0');
  u_807 : entity work.tb_verify_pfb_wg generic map (807, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 24, 18, 0, 25, 1, '0');
  u_808 : entity work.tb_verify_pfb_wg generic map (808, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 25, 18, 0, 26, 1, '0');
  u_809 : entity work.tb_verify_pfb_wg generic map (809, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 26, 18, 0, 27, 1, '0');

  -- g_fft_out_dat_w = 19
  u_810 : entity work.tb_verify_pfb_wg generic map (810, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 19, 0, 18, 1, '0');
  u_811 : entity work.tb_verify_pfb_wg generic map (811, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 19, 0, 19, 1, '0');
  u_812 : entity work.tb_verify_pfb_wg generic map (812, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 19, 19, 0, 20, 1, '0');
  u_813 : entity work.tb_verify_pfb_wg generic map (813, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 20, 19, 0, 21, 1, '0');
  u_814 : entity work.tb_verify_pfb_wg generic map (814, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 21, 19, 0, 22, 1, '0');
  u_815 : entity work.tb_verify_pfb_wg generic map (815, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 22, 19, 0, 23, 1, '0');
  u_816 : entity work.tb_verify_pfb_wg generic map (816, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 23, 19, 0, 24, 1, '0');
  u_817 : entity work.tb_verify_pfb_wg generic map (817, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 24, 19, 0, 25, 1, '0');
  u_818 : entity work.tb_verify_pfb_wg generic map (818, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 25, 19, 0, 26, 1, '0');
  u_819 : entity work.tb_verify_pfb_wg generic map (819, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 26, 19, 0, 27, 1, '0');

  -- g_fft_out_dat_w = 20
  u_820 : entity work.tb_verify_pfb_wg generic map (820, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 17, 20, 0, 18, 1, '0');
  u_821 : entity work.tb_verify_pfb_wg generic map (821, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 18, 20, 0, 19, 1, '0');
  u_822 : entity work.tb_verify_pfb_wg generic map (822, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 19, 20, 0, 20, 1, '0');
  u_823 : entity work.tb_verify_pfb_wg generic map (823, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 20, 20, 0, 21, 1, '0');
  u_824 : entity work.tb_verify_pfb_wg generic map (824, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 21, 20, 0, 22, 1, '0');
  u_825 : entity work.tb_verify_pfb_wg generic map (825, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 22, 20, 0, 23, 1, '0');
  u_826 : entity work.tb_verify_pfb_wg generic map (826, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 23, 20, 0, 24, 1, '0');
  u_827 : entity work.tb_verify_pfb_wg generic map (827, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 24, 20, 0, 25, 1, '0');
  u_828 : entity work.tb_verify_pfb_wg generic map (828, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 25, 20, 0, 26, 1, '0');
  u_829 : entity work.tb_verify_pfb_wg generic map (829, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14, 26, 20, 0, 27, 1, '0');
end generate;

gen_2022_mar_21 : if c_gen_2022_mar generate
  -- WPFB
  --                                                                                                            g_fil_coefs_file_prefix
  --                                                                                                            .   g_fil_coef_dat_w
  --                                                                                                            .   .  g_fil_backoff_w
  --                                                                                                            .   .  .   g_fil_in_dat_w
  --                                                                                                            .   .  .   .   g_internal_dat_w
  --                                                                                                            .   .  .   .   .   g_fft_out_dat_w
  --                                                                                                            .   .  .   .   .   .  g_fft_out_gain_w
  --                                                                                                            .   .  .   .   .   .  .   g_fft_stage_dat_w
  --                                                                                                            .   .  .   .   .   .  .   .  g_fft_guard_w
  --                                                                                                            .   .  .   .   .   .  .   .  .    g_switch_en
  --                                                                                                            .   .  .   .   .   .  .   .  .    .
  -- vary g_fft_out_dat_w
  --u_2000 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2000, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 1, 24, 1, '0');
  --u_2001 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2001, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 24, 1, '0');
  --u_2002 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2002, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 20, 1, 24, 1, '0');
  --u_2003 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2003, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 0, 24, 1, '0');
  --u_2004 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2004, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 0, 24, 1, '0');
  --u_2005 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2005, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 20, 0, 24, 1, '0');
  --u_2006 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2006, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14,  0, 18, 0, 24, 1, '0');
  --u_2007 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2007, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14,  0, 19, 0, 24, 1, '0');
  --u_2008 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2008, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 0, 14,  0, 20, 0, 24, 1, '0');

  -- vary g_fft_stage_dat_w using c_fil_coefs or c_fil_nodc, c_fil_hanning_16b
  --u_2010 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2010, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_16b, 16, 1, 14,  0, 19, 1, 22, 1, '0');
  --u_2011 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2011, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_16b, 16, 1, 14,  0, 19, 1, 23, 1, '0');
  --u_2012 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2012, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_16b, 16, 1, 14,  0, 19, 1, 24, 1, '0');  -- ~= 2001
  --u_2013 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2013, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_16b, 16, 1, 14,  0, 19, 1, 25, 1, '0');
  --u_2014 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2014, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_16b, 16, 1, 14,  0, 19, 1, 26, 1, '0');
  --u_2015 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2015, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_16b, 16, 1, 14,  0, 19, 1, 27, 1, '0');
  --u_2016 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2016, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_16b, 16, 0, 14,  0, 19, 0, 25, 1, '0');
  --u_2017 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2017, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_16b, 16, 0, 14,  0, 19, 0, 26, 1, '0');
  --
  --c_wpfb_lofar2_subbands_dts_18b and c_twiddle_w = 20:
  --u_2070 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2070, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 1, 22, 1, '0');
  --u_2071 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2071, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 1, 23, 1, '0');
  --u_2072 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2072, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 1, 24, 1, '0');  -- = 2000
  --u_2073 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2073, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 1, 25, 1, '0');
  --u_2074 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2074, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 1, 26, 1, '0');
  --u_2075 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2075, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 18, 1, 27, 1, '0');
  --
  --c_wpfb_lofar2_subbands_dts_19b and c_twiddle_w = 20:
  --u_2080 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2080, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 22, 1, '0');
  --u_2081 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2081, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 23, 1, '0');
  --u_2082 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2082, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 24, 1, '0');  -- = 2001
  --u_2083 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2083, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 25, 1, '0');
  --u_2084 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2084, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 26, 1, '0');
  --u_2085 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2085, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 27, 1, '0');
  --
  --g_fft_out_dat_w = 20 and c_twiddle_w = 20:
  --u_2090 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2090, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 20, 1, 22, 1, '0');
  --u_2091 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2091, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 20, 1, 23, 1, '0');
  --u_2092 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2092, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 20, 1, 24, 1, '0');  -- ~= 2001
  --u_2093 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2093, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 20, 1, 25, 1, '0');
  --u_2094 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2094, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 20, 1, 26, 1, '0');
  --u_2095 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2095, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 20, 1, 27, 1, '0');
  --
  --g_fft_out_dat_w = 19 and c_twiddle_w = 20:
  --u_2100 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2100, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 12,  0, 19, 1, 22, 1, '0');
  --u_2101 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2101, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 13,  0, 19, 1, 23, 1, '0');
  --u_2102 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2102, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 24, 1, '0');  -- = 2001
  --u_2103 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2103, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 15,  0, 19, 1, 25, 1, '0');
  --u_2104 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2104, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 16,  0, 19, 1, 26, 1, '0');
  --u_2105 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2105, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 17,  0, 19, 1, 27, 1, '0');
  --
  --u_2030 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2030, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_18b, 18, 1, 14,  0, 19, 1, 22, 1, '0');
  --u_2031 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2031, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_18b, 18, 1, 14,  0, 19, 1, 23, 1, '0');
  --u_2032 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2032, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_18b, 18, 1, 14,  0, 19, 1, 24, 1, '0');  -- ~= 2001
  --u_2033 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2033, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_18b, 18, 1, 14,  0, 19, 1, 25, 1, '0');
  --u_2034 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2034, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_18b, 18, 1, 14,  0, 19, 1, 26, 1, '0');
  --u_2035 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2035, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_18b, 18, 1, 14,  0, 19, 1, 27, 1, '0');

  --u_2040 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2040, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_16b, 16, 1, 14,  0, 19, 1, 22, 1, '0');
  --u_2041 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2041, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_16b, 16, 1, 14,  0, 19, 1, 23, 1, '0');
  --u_2042 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2042, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_16b, 16, 1, 14,  0, 19, 1, 24, 1, '0');  -- ~= 2001
  --u_2043 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2043, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_16b, 16, 1, 14,  0, 19, 1, 25, 1, '0');
  --u_2044 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2044, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_16b, 16, 1, 14,  0, 19, 1, 26, 1, '0');
  --u_2045 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2045, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_16b, 16, 1, 14,  0, 19, 1, 27, 1, '0');

  --u_2050 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2050, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_18b, 18, 1, 14,  0, 19, 1, 22, 1, '0');
  --u_2051 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2051, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_18b, 18, 1, 14,  0, 19, 1, 23, 1, '0');
  --u_2052 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2052, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_18b, 18, 1, 14,  0, 19, 1, 24, 1, '0');  -- ~= 2001
  --u_2053 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2053, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_18b, 18, 1, 14,  0, 19, 1, 25, 1, '0');
  --u_2054 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2054, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_18b, 18, 1, 14,  0, 19, 1, 26, 1, '0');
  --u_2055 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2055, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_kaiser_18b, 18, 1, 14,  0, 19, 1, 27, 1, '0');

  --u_2060 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2060, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_20b, 20, 1, 14,  0, 19, 1, 22, 1, '0');
  --u_2061 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2061, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_20b, 20, 1, 14,  0, 19, 1, 23, 1, '0');
  --u_2062 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2062, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_20b, 20, 1, 14,  0, 19, 1, 24, 1, '0');  -- ~= 2001
  --u_2063 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2063, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_20b, 20, 1, 14,  0, 19, 1, 25, 1, '0');
  --u_2064 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2064, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_20b, 20, 1, 14,  0, 19, 1, 26, 1, '0');
  --u_2065 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2065, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_hanning_20b, 20, 1, 14,  0, 19, 1, 27, 1, '0');

  -- vary g_fil_in_dat_w
  --u_2020 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2020, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1,  8,  0, 19, 1, 24, 1, '0');
  --u_2021 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2021, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1,  9,  0, 19, 1, 24, 1, '0');
  --u_2022 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2022, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 10,  0, 19, 1, 24, 1, '0');
  --u_2023 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2023, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 11,  0, 19, 1, 24, 1, '0');
  --u_2024 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2024, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 12,  0, 19, 1, 24, 1, '0');
  --u_2025 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2025, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 13,  0, 19, 1, 24, 1, '0');
  --u_2026 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2026, "WPFB", 59.0, 59.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 14,  0, 19, 1, 24, 1, '0');  -- = 2001
  --u_2027 : ENTITY work.tb_verify_pfb_wg GENERIC MAP (2027, "WPFB", 61.0, 61.0, 1.0, 0.0,  0.0,  0.0,  c_fil_coefs, 16, 1, 15,  0, 19, 1, 24, 1, '0');

-- Results:
-- c_twiddle_w = 18
-- g_fil_backoff_w = 1
-- g_fft_out_dat_w                        g_fft_out_gain_w
--  . g_stage_dat_w                           1            0
--  .  .                                      .            .
-- 18 24 : wpfb_measured_proc_gain_a_dB = 20.04 [dB]   17.38 [dB]
-- 19 24 : wpfb_measured_proc_gain_a_dB = 18.53 [dB]   20.04 [dB]
-- 20 24 : wpfb_measured_proc_gain_a_dB = 18.79 [dB]   18.53 [dB]
--
-- c_twiddle_w = 18
-- g_fil_backoff_w = g_fft_out_gain_w = 0
-- g_fft_out_dat_w
--  . g_stage_dat_w
--  .  .
-- 18 24 : wpfb_measured_proc_gain_a_dB = 20.16 [dB]
-- 19 24 : wpfb_measured_proc_gain_a_dB = 19.02 [dB]
-- 19 25 : wpfb_measured_proc_gain_a_dB = 19.14 [dB]
-- 19 26 : wpfb_measured_proc_gain_a_dB = 19.23 [dB]
-- 20 24 : wpfb_measured_proc_gain_a_dB = 19.26 [dB]
--
-- c_fil_coefs
-- c_twiddle_w = 18
-- g_fil_backoff_w = g_fft_out_gain_w = 1
-- g_fft_out_dat_w = 19
--                                     c_fil_coefs  c_fil_nodc   c_fil_coefs
-- g_stage_dat_w           c_twiddle_w:   18           18           20
--  .                                      .            .            .
-- 22 : wpfb_measured_proc_gain_a_dB = 16.62 [dB]   22.20 [dB]   16.66 [dB]
-- 23 : wpfb_measured_proc_gain_a_dB = 17.85 [dB]   23.87 [dB]   18.01 [dB]
-- 24 : wpfb_measured_proc_gain_a_dB = 18.53 [dB]   24.76 [dB]   18.50 [dB]
-- 25 : wpfb_measured_proc_gain_a_dB = 19.02 [dB]   25.22 [dB]   19.02 [dB]
-- 26 : wpfb_measured_proc_gain_a_dB = 19.14 [dB]   25.64 [dB]   19.12 [dB]
-- 27 : wpfb_measured_proc_gain_a_dB = 19.23 [dB]   25.78 [dB]   19.28 [dB]
--
-- c_fil_coefs
-- c_twiddle_w = 20
-- g_fil_backoff_w = g_fft_out_gain_w = 1
-- g_stage_dat_w     g_fft_out_dat_w =    18           19           20
--  .
-- 22 : wpfb_measured_proc_gain_a_dB = 17.09 [dB]   16.66 [dB]   16.73 [dB]
-- 23 : wpfb_measured_proc_gain_a_dB = 19.39 [dB]   18.01 [dB]   18.06 [dB]
-- 24 : wpfb_measured_proc_gain_a_dB = 20.12 [dB]   18.50 [dB]   18.77 [dB]
-- 25 : wpfb_measured_proc_gain_a_dB = 20.23 [dB]   19.02 [dB]   19.25 [dB]
-- 26 : wpfb_measured_proc_gain_a_dB = 20.23 [dB]   19.12 [dB]   19.38 [dB]
-- 27 : wpfb_measured_proc_gain_a_dB = 20.31 [dB]   19.28 [dB]   19.43 [dB]
--
-- c_fil_coefs
-- c_twiddle_w = 20
-- g_fil_backoff_w = g_fft_out_gain_w = 1
-- g_stage_dat_w = 24
-- g_fft_out_dat_w = 19
-- g_fil_in_dat_w
--  .
-- 12 . wpfb_measured_proc_gain_a_dB =   24.74 [dB]
-- 13 . wpfb_measured_proc_gain_a_dB =   22.09 [dB]
-- 14 . wpfb_measured_proc_gain_a_dB =   18.50 [dB]
-- 15 . wpfb_measured_proc_gain_a_dB =   13.36 [dB]  +6.02 = 19.38
-- 16 . wpfb_measured_proc_gain_a_dB =    7.55 [dB] +12.04 = 19.59
-- 17 . wpfb_measured_proc_gain_a_dB =    3.00 [dB] +18.06 = 21.06
--
-- c_twiddle_w =18                                  c_fil_       c_fil_       c_fil_       c_fil_      c_fil_
-- g_stage_dat_w                       c_fil_coefs  hanning_16b  hanning_18b  hanning_20b  kaiser_16b  kaiser_18b
--  .                                      .            .            .            .            .           .
-- 22 : wpfb_measured_proc_gain_a_dB = 16.62 [dB]   19.72 [dB]   21.57 [dB]   21.99 [dB]   16.40 [dB]  17.17 [dB]
-- 23 : wpfb_measured_proc_gain_a_dB = 17.85 [dB]   22.23 [dB]   23.72 [dB]   23.85 [dB]   17.47 [dB]  17.91 [dB]
-- 24 : wpfb_measured_proc_gain_a_dB = 18.53 [dB]   23.61 [dB]   24.56 [dB]   24.77 [dB]   18.01 [dB]  18.30 [dB]
-- 25 : wpfb_measured_proc_gain_a_dB = 19.02 [dB]   24.58 [dB]   25.18 [dB]   25.34 [dB]   18.51 [dB]  18.56 [dB]
-- 26 : wpfb_measured_proc_gain_a_dB = 19.14 [dB]   24.75 [dB]   25.37 [dB]   25.70 [dB]   18.91 [dB]  18.66 [dB]
-- 27 : wpfb_measured_proc_gain_a_dB = 19.23 [dB]   24.86 [dB]   25.37 [dB]   25.77 [dB]   18.89 [dB]  18.81 [dB]
--
-- c_twiddle_w = 18
-- g_stage_dat_w
--  .                g_fft_out_dat_w =    19          18
-- 23 : wpfb_measured_proc_gain_a_dB = 16.62 [dB]  17.17 [dB]
-- 24 : wpfb_measured_proc_gain_a_dB = 17.85 [dB]  19.33 [dB]
-- 25 : wpfb_measured_proc_gain_a_dB = 18.53 [dB]  20.04 [dB]
-- 26 : wpfb_measured_proc_gain_a_dB = 19.02 [dB]  20.16 [dB]
-- 27 : wpfb_measured_proc_gain_a_dB = 19.14 [dB]  20.27 [dB]
-- 22 : wpfb_measured_proc_gain_a_dB = 19.23 [dB]  20.39 [dB]
--
-- c_twiddle_w = 18
-- g_fil_backoff_w = 1
-- g_fft_out_gain_w = 1
-- g_fft_out_dat_w = 19
-- g_stage_dat_w = 24
-- g_fil_in_dat_w
--  .
--  8 : wpfb_measured_proc_gain_a_dB = 27.08 [dB]  -- theoretical 20log10(sqrt(512)) = 27.1 dB
--  9 : wpfb_measured_proc_gain_a_dB = 27.00 [dB]
-- 10 : wpfb_measured_proc_gain_a_dB = 26.93 [dB]
-- 11 : wpfb_measured_proc_gain_a_dB = 26.65 [dB]
-- 12 : wpfb_measured_proc_gain_a_dB = 25.77 [dB]
-- 13 : wpfb_measured_proc_gain_a_dB = 22.73 [dB]
-- 14 : wpfb_measured_proc_gain_a_dB = 18.53 [dB]  -- 61.0
-- 14 : wpfb_measured_proc_gain_a_dB = 18.70 [dB]  -- 59.0
-- 15 : wpfb_measured_proc_gain_a_dB = 12.70 [dB]
--
-- Conclusion:
-- . Choose c_twiddle_w = 20 b >= g_fft_out_dat_w = 18 or 19 b (see
--   c_gen_vary_c_twiddle_w): The M20K is 20b and multipliers have 27b.
--   Synthesis shows that using c_twiddle_w = 20b costs no extra M20K or
--   multipliers.
-- . g_fil_coef_dat_w = 16b: The LOFAR1 FIR coefficients are fixed 16b, which
--   is sufficient for the required stop band attenuation of 89 dB, because 16
--   6.02 dB/bit = 96 dB.
-- . Use g_fil_backoff_w = 1 to fit temporary overshoot of FIR filter of about
--   10 %.
-- . Use g_fft_out_gain_w = 1 to compensate for g_fil_backoff_w = 1.
-- . Use g_fft_guard_w to compensate for FFT first stage gain > 2 (I think 1 +
--   sqrt(2) ~= 2.41). Default g_fft_guard_w = 2 would be needed and the FFT
--   then does not scale in its last 2 stages to ensure that the total reponse
--   of the FFT remains unit. With g_fil_backoff_w = 1 and an FIR filter
--   overshoot of about 10 % (is factor 1.1) using g_fft_guard_w = 1 is
--   sufficient, because 1.1 * 2.41 < 2**2 = 4.
-- . wpfb_measured_proc_gain_a_dB is limited by:
--   . stop band attenuation
--   . FIR quantisation noise floor level
--   . DC response not exactly the same for each of the N_fft polyphases, this
--     shows as a ripple in fil_noise_a, which is the difference between the
--     FIR filter output and a matching sine wave. This variation in FIR filter
--     output during a FFT block then cause leakage into other bins and thus a
--     reduction in PFB processing gain compared to FFT processing gain.
--     However the PFB does provide the required stop band attenuation, so the
--     limited processing gain is probably due to the allowed stop band ripple
--     of the FIR filter. Therefore I think the limited processing gain is not
--     an issue or bug.
-- . g_fft_out_dat_w = 19b is needed to accomodate g_fil_in_dat_w = 14 b +
--   log2(sqrt(N_sub)) = 4.5 bit processing gain.
--   . It is strange that wpfb_measured_proc_gain_a_dB is 20.12 [dB] for 18b
--     and only 18.50 [dB] for 19b, but this may be due to the WG stimuli and
--     related quantisation noise. Instead it would have been better to use a
--     REAL SIN generator and REAL gaussian noise as signal input, to avoid
--     WG artefacts.
-- . g_stage_dat_w:
--   . 27b is maximum for DSP multipliers, but does require extra logic and
--     some BRAM
--   . in LOFAR1 g_stage_dat_w = 20b with W_adc = 12b, so for LOFAR2.0 with
--     W_adc = 14b the g_stage_dat_w >= 22b
--   . 24b or 25b seems a good compromise for wpfb_measured_proc_gain_a_dB.
-- . Given a WG amplitude of A_wg the expected subband phasor amplitude will be:
--     A_sub = A_wg * func_wpfb_subband_gain(c_wpfb, fir_filter_dc_gain)
--   The expected SST level for an integration interval of N_int subband blocks
--   is then:
--     SST = func_wpfb_sst_level(A_sub, N_blk)
end generate;
end tb;
