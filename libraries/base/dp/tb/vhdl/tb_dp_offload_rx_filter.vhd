-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--   Test bench for dp_offload_rx_filter
-- Description:
--   The tb creates a data stream and checks if the register can be readline
--   The value that is read via MM should be checked in the wave window.
--
-- Remark:
--   It is not possible to check if the wr_full bit is set. That is because of the
--   assert in de common_fifo_sc definition.
--
-- Usage:
-- > as 10
-- > run -all
-- . signal tb_end will stop the simulation by stopping the clk
--
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_offload_rx_filter is
end tb_dp_offload_rx_filter;

architecture tb of tb_dp_offload_rx_filter is
  constant c_nof_stream     : natural := 1;
  constant c_data_w         : natural := 64;

  constant c_correct_mac    : std_logic_vector(47 downto 0) := x"FFABCDABCDFF";
  constant c_correct_ip     : std_logic_vector(31 downto 0) := x"12345678";
  constant c_correct_length : natural := 50;
  constant c_correct_port   : natural := 4000;

  constant c_wrong_mac      : std_logic_vector(47 downto 0) := x"BBBBBBBBBBBB";
  constant c_wrong_ip       : std_logic_vector(31 downto 0) := x"AAAAAAAA";
  constant c_wrong_length   : natural := 60;
  constant c_wrong_port     : natural := 4001;

  constant c_nof_packets    : natural := 5;

  constant c_nof_hdr_fields : natural := 3 + 12 + 4 + 9 + 1;
  constant c_hdr_field_arr  : t_common_field_arr(c_nof_hdr_fields - 1 downto 0) := ( ( field_name_pad("eth_dst_mac"        ), "  ", 48, field_default(0) ),
                                                                                   ( field_name_pad("eth_src_mac"        ), "  ", 48, field_default(0) ),
                                                                                   ( field_name_pad("eth_type"           ), "  ", 16, field_default(x"0800") ),
                                                                                   ( field_name_pad("ip_version"         ), "  ",  4, field_default(4) ),
                                                                                   ( field_name_pad("ip_header_length"   ), "  ",  4, field_default(5) ),
                                                                                   ( field_name_pad("ip_services"        ), "  ",  8, field_default(0) ),
                                                                                   ( field_name_pad("ip_total_length"    ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_identification"  ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_flags"           ), "  ",  3, field_default(2) ),
                                                                                   ( field_name_pad("ip_fragment_offset" ), "  ", 13, field_default(0) ),
                                                                                   ( field_name_pad("ip_time_to_live"    ), "  ",  8, field_default(127) ),
                                                                                   ( field_name_pad("ip_protocol"        ), "  ",  8, field_default(17) ),
                                                                                   ( field_name_pad("ip_header_checksum" ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("ip_src_addr"        ), "  ", 32, field_default(0) ),
                                                                                   ( field_name_pad("ip_dst_addr"        ), "  ", 32, field_default(0) ),
                                                                                   ( field_name_pad("udp_src_port"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_dst_port"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_total_length"   ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("udp_checksum"       ), "  ", 16, field_default(0) ),
                                                                                   ( field_name_pad("usr_sync"           ), "  ",  1, field_default(1) ),
                                                                                   ( field_name_pad("usr_bsn"            ), "  ", 60, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_0"    ), "  ",  7, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_1"    ), "  ",  9, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_2"    ), "  ", 10, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_3"    ), "  ", 33, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_4"    ), "  ",  5, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_5"    ), "  ",  8, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_field_6"    ), "  ", 27, field_default(0) ),
                                                                                   ( field_name_pad("usr_hdr_word_align" ), "  ", 16, field_default(0) ) );

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_dp_clk_period : time := 5 ns;
  constant c_nof_streams   : positive := 1;
  constant hdr_fields_rst  : t_slv_1024_arr(c_nof_streams - 1 downto 0) := (others => (others => '0'));
  signal dp_rst            : std_logic;
  signal dp_clk            : std_logic := '0';

  signal tb_end            : std_logic := '0';
  signal toggle            : boolean := false;
  signal counter           : natural := 0;

  signal in_sosi           : t_dp_sosi := c_dp_sosi_rst;
  signal snk_in_arr        : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal snk_out_arr       : t_dp_siso_arr(c_nof_streams - 1 downto 0);
  signal src_in_arr        : t_dp_siso_arr(c_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal src_out_arr       : t_dp_sosi_arr(c_nof_streams - 1 downto 0);

  signal hdr_fields_in_arr : t_slv_1024_arr(c_nof_streams - 1 downto 0);
  signal hdr_fields_out_arr: t_slv_1024_arr(c_nof_streams - 1 downto 0);

  signal hdr_fields_wrong_arr : t_slv_1024_arr(c_nof_streams - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------

  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  gen_hdr_in : for i in 0 to c_nof_streams - 1 generate
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "eth_dst_mac"       ) downto field_lo(c_hdr_field_arr, "eth_dst_mac"        )) <= c_correct_mac;
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "ip_dst_addr"       ) downto field_lo(c_hdr_field_arr, "ip_dst_addr"        )) <= c_correct_ip;
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "udp_dst_port"      ) downto field_lo(c_hdr_field_arr, "udp_dst_port"       )) <= TO_UVEC(c_correct_port, 16);
    hdr_fields_in_arr(i)(field_hi(c_hdr_field_arr, "ip_total_length"   ) downto field_lo(c_hdr_field_arr, "ip_total_length"    )) <= TO_UVEC(c_correct_length, 16);
  end generate;

  gen_hdr_wrong : for i in 0 to c_nof_streams - 1 generate
    hdr_fields_wrong_arr(i)(field_hi(c_hdr_field_arr, "eth_dst_mac"       ) downto field_lo(c_hdr_field_arr, "eth_dst_mac"        )) <= c_wrong_mac;
    hdr_fields_wrong_arr(i)(field_hi(c_hdr_field_arr, "ip_dst_addr"       ) downto field_lo(c_hdr_field_arr, "ip_dst_addr"        )) <= c_wrong_ip;
    hdr_fields_wrong_arr(i)(field_hi(c_hdr_field_arr, "udp_dst_port"      ) downto field_lo(c_hdr_field_arr, "udp_dst_port"       )) <= TO_UVEC(c_wrong_port, 16);
    hdr_fields_wrong_arr(i)(field_hi(c_hdr_field_arr, "ip_total_length"   ) downto field_lo(c_hdr_field_arr, "ip_total_length"    )) <= TO_UVEC(c_wrong_length, 16);
  end generate;

  p_stimuli : process
  begin
    proc_common_wait_some_cycles(dp_clk, 10);
    -- Generate a datastream.
    while tb_end = '0' loop
      in_sosi.valid <= '1';
      in_sosi.sop <= '1';
      proc_common_wait_some_cycles(dp_clk, 1);
      in_sosi.sop <= '0';
      proc_common_wait_some_cycles(dp_clk, 63);
      in_sosi.eop <= '1';
      proc_common_wait_some_cycles(dp_clk, 1);
      in_sosi.eop <= '0';
      in_sosi.valid <= '0';
      proc_common_wait_some_cycles(dp_clk, 1);
    end loop;
    wait;  -- add void WAIT statement to avoid (vcom-1090) Possible infinite loop: Process contains no WAIT statement.
  end process;

  gen_connect : for I in 0 to c_nof_streams - 1 generate
    snk_in_arr(I)  <= in_sosi;
  end generate;

  p_special_stimuli : process(in_sosi.sop)
  begin
    if in_sosi.sop = '1' then
      counter <= counter + 1;
      if toggle then
        hdr_fields_out_arr <= hdr_fields_in_arr;
      else
        hdr_fields_out_arr <= hdr_fields_wrong_arr;
      end if;
      toggle <= not toggle;
    else
      hdr_fields_out_arr <= hdr_fields_rst;
    end if;

    if c_nof_packets + 1 <= counter then
      tb_end <= '1';
    end if;
  end process;

  dut : entity work.dp_offload_rx_filter
  generic map(
    g_nof_streams         => c_nof_streams,  -- : POSITIVE;
    g_data_w              => c_data_w,  -- : NATURAL;
    g_hdr_field_arr       => c_hdr_field_arr,  -- : t_common_field_arr;
    g_eth_dst_mac_ena     => true,  -- : BOOLEAN;
    g_ip_dst_addr_ena     => true,  -- : BOOLEAN;
    g_ip_total_length_ena => true,  -- : BOOLEAN;
    g_udp_dst_port_ena    => true  -- : BOOLEAN
  )
  port map(

    dp_rst             => dp_rst,
    dp_clk             => dp_clk,

    snk_in_arr         => snk_in_arr,
    snk_out_arr        => snk_out_arr,

    src_out_arr        => src_out_arr,
    src_in_arr         => src_in_arr,

    hdr_fields_out_arr => hdr_fields_out_arr,
    hdr_fields_in_arr  => hdr_fields_in_arr
  );
end tb;
