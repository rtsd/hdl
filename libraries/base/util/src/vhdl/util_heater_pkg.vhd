-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

package util_heater_pkg is
  ------------------------------------------------------------------------------
  -- Definitions for MM register
  ------------------------------------------------------------------------------

  -- The maximum number of 18x18 multipliers       in the Stratix IV SGX230 = 1288
  -- The maximum number of 1kbit  M9K   RAM blocks in the Stratix IV SGX230 = 1235
  -- The maximum number of 16kbit M144K RAM blocks in the Stratix IV SGX230 = 22

  --CONSTANT c_util_heater_nof_mac4_max     : NATURAL := 352; --stratix4                         -- >= 1288/4 and multiple of c_word_w=32
  constant c_util_heater_nof_mac4_max     : natural := 800;  -- stratix4                         -- >= 1288/4 and multiple of c_word_w=32
  constant c_util_heater_reg_nof_words    : natural := c_util_heater_nof_mac4_max / c_word_w;  -- note: needs adjustment if not multiple of c_word_w

  constant c_util_heater_reg_addr_w       : natural := ceil_log2(c_util_heater_reg_nof_words);

  type t_util_heater_reg_mm_bus is record
    -- Master In Slave Out
    rddata    : std_logic_vector(c_word_w - 1 downto 0);
    -- Master Out Slave In
    address   : std_logic_vector(c_util_heater_reg_addr_w - 1 downto 0);
    wrdata    : std_logic_vector(c_word_w - 1 downto 0);
    wr        : std_logic;
    rd        : std_logic;
  end record;

  constant c_util_heater_reg_mm_bus : t_util_heater_reg_mm_bus := ((others => '0'), (others => '0'), (others => '0'), '0', '0');
end util_heater_pkg;

package body util_heater_pkg is
end util_heater_pkg;
