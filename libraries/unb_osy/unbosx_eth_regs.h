/* -----------------------------------------------------------------------------
 *
 * Copyright (C) 2010
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ---------------------------------------------------------------------------- */

#ifndef AVS_ETH_REGS_H
#define AVS_ETH_REGS_H

/* includes */
#include "io.h"

/* public constants */

// TSE MAC registers
#define AVS_ETH_TSE_PCS_ADDR_BASE        (0x200)
#define AVS_ETH_TSE_RX_FIFO_DEPTH         256
#define AVS_ETH_TSE_TX_FIFO_DEPTH         256
#define AVS_ETH_TSE_TX_READY_LATENCY      1

#define AVS_ETH_TSE_PCS_LINK_STATUS_BIT  (0x4)

// ETH registers
#define AVS_ETH_DEMUX_NOF_UDP_PORTS       4

#define AVS_ETH_REG_DEMUX_NOF_WORDS       AVS_ETH_DEMUX_NOF_UDP_PORTS
#define AVS_ETH_REG_CONFIG_NOF_WORDS      4
#define AVS_ETH_REG_CONTROL_NOF_WORDS     1
#define AVS_ETH_REG_FRAME_NOF_WORDS       1
#define AVS_ETH_REG_STATUS_NOF_WORDS      1
  
#define AVS_ETH_REG_DEMUX_WI              0
#define AVS_ETH_REG_CONFIG_WI            (AVS_ETH_REG_DEMUX_WI   + AVS_ETH_REG_DEMUX_NOF_WORDS)
#define AVS_ETH_REG_CONTROL_WI           (AVS_ETH_REG_CONFIG_WI  + AVS_ETH_REG_CONFIG_NOF_WORDS)
#define AVS_ETH_REG_FRAME_WI             (AVS_ETH_REG_CONTROL_WI + AVS_ETH_REG_CONTROL_NOF_WORDS)
#define AVS_ETH_REG_STATUS_WI            (AVS_ETH_REG_FRAME_WI   + AVS_ETH_REG_FRAME_NOF_WORDS)
#define AVS_ETH_REG_CONTINUE_WI          (AVS_ETH_REG_STATUS_WI  + AVS_ETH_REG_STATUS_NOF_WORDS)

// AVS_ETH_REG_DEMUX fields
#define AVS_ETH_UDP_PORT_MASK            (0xFFFF)
#define AVS_ETH_UDP_PORT_OFST             0
#define AVS_ETH_UDP_PORT_EN_BIT_MASK     (0x10000)
#define AVS_ETH_UDP_PORT_EN_BIT_OFST      16

// AVS_ETH_REG_CONFIG words
#define AVS_ETH_REG_CONFIG_SRC_MAC_LO                  AVS_ETH_REG_CONFIG_WI
#define AVS_ETH_REG_CONFIG_SRC_MAC_HI                 (AVS_ETH_REG_CONFIG_WI + 1)
#define AVS_ETH_REG_CONFIG_SRC_IP                     (AVS_ETH_REG_CONFIG_WI + 2)
#define AVS_ETH_REG_CONFIG_CTRL                       (AVS_ETH_REG_CONFIG_WI + 3)

// AVS_ETH_REG_CONTROL fields
#define AVS_ETH_REG_CONTROL_RX_EN_BIT_MASK            (0x1)
#define AVS_ETH_REG_CONTROL_RX_EN_BIT_OFST             0
#define AVS_ETH_REG_CONTROL_TX_EN_BIT_MASK            (0x2)
#define AVS_ETH_REG_CONTROL_TX_EN_BIT_OFST             1
#define AVS_ETH_REG_CONTROL_TX_REQUEST_BIT_MASK       (0x4)
#define AVS_ETH_REG_CONTROL_TX_REQUEST_BIT_OFST        2
#define AVS_ETH_REG_CONTROL_TX_EMPTY_BIT_MASK         (0x30000)
#define AVS_ETH_REG_CONTROL_TX_EMPTY_BIT_OFST          16
#define AVS_ETH_REG_CONTROL_TX_NOF_WORDS_BIT_MASK     (0xFFFC0000)
#define AVS_ETH_REG_CONTROL_TX_NOF_WORDS_BIT_OFST      18

// AVS_ETH_REG_FRAME fields
#define AVS_ETH_TSE_MAC_ERROR_BIT_MASK                (0x1)
#define AVS_ETH_TSE_MAC_ERROR_BIT_OFST                 0
#define AVS_ETH_TSE_MAC_ERROR_LENGTH_BIT_MASK         (0x2)
#define AVS_ETH_TSE_MAC_ERROR_LENGTH_BIT_OFST          1
#define AVS_ETH_TSE_MAC_ERROR_CRC_BIT_MASK            (0x4)
#define AVS_ETH_TSE_MAC_ERROR_CRC_BIT_OFST             2
#define AVS_ETH_TSE_MAC_ERROR_FIFO_BIT_MASK           (0x8)
#define AVS_ETH_TSE_MAC_ERROR_FIFO_BIT_OFST            3
#define AVS_ETH_TSE_MAC_ERROR_PHY_GMII_BIT_MASK       (0x10)
#define AVS_ETH_TSE_MAC_ERROR_PHY_GMII_BIT_OFST        4
#define AVS_ETH_TSE_MAC_ERROR_COLLISION_BIT_MASK      (0x20)
#define AVS_ETH_TSE_MAC_ERROR_COLLISION_BIT_OFST       5

#define AVS_ETH_REG_FRAME_MAC_ADDRESS_MATCH_BIT_MASK  (0x80)
#define AVS_ETH_REG_FRAME_MAC_ADDRESS_MATCH_BIT_OFST   7
#define AVS_ETH_REG_FRAME_IS_ARP_BIT_MASK             (0x100)
#define AVS_ETH_REG_FRAME_IS_ARP_BIT_OFST              8
#define AVS_ETH_REG_FRAME_IS_IP_BIT_MASK              (0x200)
#define AVS_ETH_REG_FRAME_IS_IP_BIT_OFST               9
#define AVS_ETH_REG_FRAME_IP_CHECKSUM_OK_BIT_MASK     (0x400)
#define AVS_ETH_REG_FRAME_IP_CHECKSUM_OK_BIT_OFST      10
#define AVS_ETH_REG_FRAME_IP_ADDRESS_MATCH_BIT_MASK   (0x800)
#define AVS_ETH_REG_FRAME_IP_ADDRESS_MATCH_BIT_OFST    11
#define AVS_ETH_REG_FRAME_IS_ICMP_BIT_MASK            (0x1000)
#define AVS_ETH_REG_FRAME_IS_ICMP_BIT_OFST             12
#define AVS_ETH_REG_FRAME_IS_UDP_BIT_MASK             (0x2000)
#define AVS_ETH_REG_FRAME_IS_UDP_BIT_OFST              13
#define AVS_ETH_REG_FRAME_IS_UDP_CTRL_PORT_BIT_MASK   (0x4000)
#define AVS_ETH_REG_FRAME_IS_UDP_CTRL_PORT_BIT_OFST    14

// AVS_ETH_REG_STATUS fields
#define AVS_ETH_REG_STATUS_RX_AVAIL_BIT_MASK          (0x1)
#define AVS_ETH_REG_STATUS_RX_AVAIL_BIT_OFST           0
#define AVS_ETH_REG_STATUS_TX_DONE_BIT_MASK           (0x2)
#define AVS_ETH_REG_STATUS_TX_DONE_BIT_OFST            1
#define AVS_ETH_REG_STATUS_TX_AVAIL_BIT_MASK          (0x4)
#define AVS_ETH_REG_STATUS_TX_AVAIL_BIT_OFST           2
#define AVS_ETH_REG_STATUS_RX_EMPTY_BIT_MASK          (0x30000)
#define AVS_ETH_REG_STATUS_RX_EMPTY_BIT_OFST           16
#define AVS_ETH_REG_STATUS_RX_NOF_WORDS_BIT_MASK      (0xFFFC0000)
#define AVS_ETH_REG_STATUS_RX_NOF_WORDS_BIT_OFST       18

// ETH frame
#define AVS_ETH_TYPE_MASK                             (0xFFFF)


/* public macros */

// usage: fill in base address from system.h
// . pcs_addr half word address as defined in table 4.9 in ug_ethernet.pdf
// . mac_addr byte address as defined in table 4.17 in ug_ethernet.pdf
// TSE MAC uses byte unit addressing
#define AVS_ETH_TSE_PCS_RD(base, pcs_addr)       IORD((base), (AVS_ETH_TSE_PCS_ADDR_BASE + (pcs_addr)*2))
#define AVS_ETH_TSE_PCS_WR(base, pcs_addr, data) IOWR((base), (AVS_ETH_TSE_PCS_ADDR_BASE + (pcs_addr)*2), (data))
#define AVS_ETH_TSE_MAC_RD(base, mac_addr)       IORD((base), (mac_addr))
#define AVS_ETH_TSE_MAC_WR(base, mac_addr, data) IOWR((base), (mac_addr), (data))
// ETH REG and ETH RAM use word unit addressing
#define AVS_ETH_REG_RD(base, reg_wi)             IORD((base), (reg_wi))
#define AVS_ETH_REG_WR(base, reg_wi, data)       IOWR((base), (reg_wi), (data))
#define AVS_ETH_RAM_RD(base, ram_wi)             IORD((base), (ram_wi))
#define AVS_ETH_RAM_WR(base, ram_wi, data)       IOWR((base), (ram_wi), (data))

/* public types */

/* public variables */

/* public function protos */

#endif  // AVS_ETH_REGS_H
