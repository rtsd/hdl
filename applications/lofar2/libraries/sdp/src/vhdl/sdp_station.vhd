-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : R vd Walle
-- Purpose:
--   Core design for Lofar2 SDP station
-- Description:
-- * Combines sdp nodes. Contains the UniBoard2 HW version independent LOFAR2
--   SDP application code.
-- * Supports default PFB for LOFAR2 [1]
--   - Uses raw subband input to improve the accuracy of weighted subbands
--     and weighted beamlets
-- * Supports 2x oversampled PFB for DISTURB2 [2]
--   - Still uses quantized subband input, because using raw subbands might
--     require too many DSP multipliers.
-- References:
-- [1] LOFAR2 SDP FW: https://support.astron.nl/confluence/display/L2M/L4+SDP+Firmware+Design+Document
-- [2] DISTURB2 design revision: https://support.astron.nl/confluence/x/vIIjBQ
-- [3] SST, SST_OS packets: https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD
-------------------------------------------------------------------------------

library IEEE, common_lib, diag_lib, dp_lib;
library tech_pll_lib, tech_jesd204b_lib;
library fft_lib, wpfb_lib;
library tr_10GbE_lib, nw_10GbE_lib, eth_lib, ring_lib;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use common_lib.common_field_pkg.all;
  use diag_lib.diag_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use fft_lib.fft_pkg.all;
  use wpfb_lib.wpfb_pkg.all;
  use eth_lib.eth_pkg.all;
  use ring_lib.ring_pkg.all;
  use work.sdp_pkg.all;

entity sdp_station is
  generic (
    g_sim                      : boolean := false;  -- Overridden by TB
    g_sim_sdp                  : t_sdp_sim := c_sdp_sim;  -- Used when g_sim = TRUE, otherwise use HW defaults
    g_sim_sync_timeout         : natural := 1024;
    g_wpfb                     : t_wpfb  := c_sdp_wpfb_subbands;
    g_wpfb_complex             : t_wpfb  := c_sdp_wpfb_complex_subbands;
    g_bsn_nof_clk_per_sync     : natural := c_sdp_N_clk_per_sync;  -- Default 200M, overide for short simulation
    g_scope_selected_subband   : natural := 0;
    -- Use no default, to force instance to set it
    g_no_jesd                  : boolean;  -- when false use Rx JESD204B and WG, else use only WG
    -- When g_use_tech_jesd204b_v2 = false use tech_jesd204b with tech_jesd204b and FIFO in ait,
    -- else use tech_jesd204b_v2 with FIFO in tech_jesd204b_v2
    g_use_tech_jesd204b_v2     : boolean := true; --false;
    g_no_st_histogram          : boolean := true;  -- when false use input histogram, else not to save block RAM
    g_use_fsub                 : boolean;
    g_use_oversample           : boolean;
    g_use_xsub                 : boolean;
    g_use_bf                   : boolean;
    g_use_bdo_transpose        : boolean;
    g_nof_bdo_destinations_max : natural;
    g_use_ring                 : boolean;
    g_P_sq                     : natural
  );
  port (
    -- System
    mm_clk        : in std_logic;
    mm_rst        : in std_logic := '0';

    dp_pps        : in std_logic;
    dp_rst        : in std_logic;
    dp_clk        : in std_logic;

    -- ID
    gn_id         : in std_logic_vector(c_sdp_W_gn_id - 1 downto 0);  -- used for udp src port
    this_bck_id   : in std_logic_vector(6 - 1 downto 0);  -- used for src mac / ip
    this_chip_id  : in std_logic_vector(2 - 1 downto 0);  -- used for src mac / ip

    -- Transceiver clocks
    SA_CLK        : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines

     -- back transceivers (Note: numbered from 0)
    JESD204B_SERIAL_DATA       : in    std_logic_vector(c_sdp_S_pn - 1 downto 0);
                                                  -- Connect to the BCK_RX pins in the top wrapper
    JESD204B_REFCLK            : in    std_logic;  -- Connect to BCK_REF_CLK pin in the top level wrapper

    -- jesd204b syncronization signals
    JESD204B_SYSREF            : in    std_logic;
    JESD204B_SYNC_N            : out   std_logic_vector(c_sdp_N_sync_jesd - 1 downto 0);

    ----------------------------------------------
    -- UDP Offload
    ----------------------------------------------
    udp_tx_sosi_arr            : out t_dp_sosi_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_sosi_rst);
    udp_tx_siso_arr            : in  t_dp_siso_arr(c_eth_nof_udp_ports - 1 downto 0) := (others => c_dp_siso_rdy);

    ----------------------------------------------
    -- AIT
    ----------------------------------------------
    -- JESD
    jesd204b_copi              : in  t_mem_copi := c_mem_copi_rst;
    jesd204b_cipo              : out t_mem_cipo := c_mem_cipo_rst;

    -- JESD control
    jesd_ctrl_copi             : in  t_mem_copi := c_mem_copi_rst;
    jesd_ctrl_cipo             : out t_mem_cipo := c_mem_cipo_rst;

    -- Shiftram (applies per-antenna delay)
    reg_dp_shiftram_copi       : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_shiftram_cipo       : out t_mem_cipo := c_mem_cipo_rst;

    -- bsn source
    reg_bsn_source_v2_copi     : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_source_v2_cipo     : out t_mem_cipo := c_mem_cipo_rst;

    -- bsn scheduler
    reg_bsn_scheduler_wg_copi  : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_scheduler_wg_cipo  : out t_mem_cipo := c_mem_cipo_rst;

    -- WG
    reg_wg_copi                : in  t_mem_copi := c_mem_copi_rst;
    reg_wg_cipo                : out t_mem_cipo := c_mem_cipo_rst;
    ram_wg_copi                : in  t_mem_copi := c_mem_copi_rst;
    ram_wg_cipo                : out t_mem_cipo := c_mem_cipo_rst;

    -- BSN MONITOR
    reg_bsn_monitor_input_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_input_cipo : out t_mem_cipo := c_mem_cipo_rst;

    -- Data buffer bsn
    ram_diag_data_buf_bsn_copi : in  t_mem_copi := c_mem_copi_rst;
    ram_diag_data_buf_bsn_cipo : out t_mem_cipo := c_mem_cipo_rst;
    reg_diag_data_buf_bsn_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_diag_data_buf_bsn_cipo : out t_mem_cipo := c_mem_cipo_rst;

    -- ST Histogram
    ram_st_histogram_copi      : in  t_mem_copi := c_mem_copi_rst;
    ram_st_histogram_cipo      : out t_mem_cipo := c_mem_cipo_rst;

    -- Aduh statistics monitor
    reg_aduh_monitor_copi      : in  t_mem_copi := c_mem_copi_rst;
    reg_aduh_monitor_cipo      : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- FSUB / Oversampled FSUB
    ----------------------------------------------
    -- Subband statistics
    ram_st_sst_copi                     : in  t_mem_copi := c_mem_copi_rst;
    ram_st_sst_cipo                     : out t_mem_cipo := c_mem_cipo_rst;

    -- Spectral Inversion
    reg_si_copi                         : in  t_mem_copi := c_mem_copi_rst;
    reg_si_cipo                         : out t_mem_cipo := c_mem_cipo_rst;

    -- Filter coefficients
    ram_fil_coefs_copi                  : in  t_mem_copi := c_mem_copi_rst;
    ram_fil_coefs_cipo                  : out t_mem_cipo := c_mem_cipo_rst;

    -- Equalizer gains
    ram_equalizer_gains_copi            : in  t_mem_copi := c_mem_copi_rst;
    ram_equalizer_gains_cipo            : out t_mem_cipo := c_mem_cipo_rst;
    ram_equalizer_gains_cross_copi      : in  t_mem_copi := c_mem_copi_rst;
    ram_equalizer_gains_cross_cipo      : out t_mem_cipo := c_mem_cipo_rst;

    -- DP Selector
    reg_dp_selector_copi                : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_selector_cipo                : out t_mem_cipo := c_mem_cipo_rst;

    -- SST UDP offload bsn monitor
    reg_bsn_monitor_v2_sst_offload_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_sst_offload_cipo : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- SDP Info
    ----------------------------------------------
    reg_sdp_info_copi          : in  t_mem_copi := c_mem_copi_rst;
    reg_sdp_info_cipo          : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- RING Info
    ----------------------------------------------
    reg_ring_info_copi          : in  t_mem_copi := c_mem_copi_rst;
    reg_ring_info_cipo          : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- XSUB
    ----------------------------------------------
    -- crosslets_info
    reg_crosslets_info_copi          : in  t_mem_copi := c_mem_copi_rst;
    reg_crosslets_info_cipo          : out t_mem_cipo := c_mem_cipo_rst;

    -- nof_crosslets
    reg_nof_crosslets_copi           : in  t_mem_copi := c_mem_copi_rst;
    reg_nof_crosslets_cipo           : out t_mem_cipo := c_mem_cipo_rst;

    -- bsn_scheduler_xsub
    reg_bsn_sync_scheduler_xsub_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_sync_scheduler_xsub_cipo : out t_mem_cipo := c_mem_cipo_rst;

    -- st_xsq
    ram_st_xsq_copi                  : in  t_mem_copi := c_mem_copi_rst;
    ram_st_xsq_cipo                  : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- BF
    ----------------------------------------------
    -- Beamlet Subband Select
    ram_ss_ss_wide_copi        : in  t_mem_copi := c_mem_copi_rst;
    ram_ss_ss_wide_cipo        : out t_mem_cipo := c_mem_cipo_rst;

    -- Local BF bf weights
    ram_bf_weights_copi        : in  t_mem_copi := c_mem_copi_rst;
    ram_bf_weights_cipo        : out t_mem_cipo := c_mem_cipo_rst;

    -- BF bsn aligner_v2
    reg_bsn_align_v2_bf_copi   : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_align_v2_bf_cipo   : out t_mem_cipo := c_mem_cipo_rst;

    -- BF bsn aligner_v2 bsn monitors
    reg_bsn_monitor_v2_rx_align_bf_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_rx_align_bf_cipo : out t_mem_cipo := c_mem_cipo_rst;
    reg_bsn_monitor_v2_aligned_bf_copi  : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_aligned_bf_cipo  : out t_mem_cipo := c_mem_cipo_rst;

    -- mms_dp_scale Scale Beamlets
    reg_bf_scale_copi          : in  t_mem_copi := c_mem_copi_rst;
    reg_bf_scale_cipo          : out t_mem_cipo := c_mem_cipo_rst;

    -- Beamlet Data Output header fields
    reg_hdr_dat_copi           : in  t_mem_copi := c_mem_copi_rst;
    reg_hdr_dat_cipo           : out t_mem_cipo := c_mem_cipo_rst;
    reg_bdo_destinations_copi  : in  t_mem_copi := c_mem_mosi_rst;
    reg_bdo_destinations_cipo  : out t_mem_cipo := c_mem_cipo_rst;

    -- Beamlet Data Output xonoff
    reg_dp_xonoff_copi         : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_xonoff_cipo         : out t_mem_cipo := c_mem_cipo_rst;

    -- Beamlet Statistics (BST)
    ram_st_bst_copi            : in  t_mem_copi := c_mem_copi_rst;
    ram_st_bst_cipo            : out t_mem_cipo := c_mem_cipo_rst;

    -- BST UDP offload bsn monitor
    reg_bsn_monitor_v2_bst_offload_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_bst_offload_cipo : out t_mem_cipo := c_mem_cipo_rst;

    -- BF beamlet output bsn monitor
    reg_bsn_monitor_v2_beamlet_output_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_beamlet_output_cipo : out t_mem_cipo := c_mem_cipo_rst;

    -- BF ring lane info
    reg_ring_lane_info_bf_copi                 : in  t_mem_copi := c_mem_copi_rst;
    reg_ring_lane_info_bf_cipo                 : out t_mem_cipo := c_mem_cipo_rst;

    -- BF ring bsn monitor rx
    reg_bsn_monitor_v2_ring_rx_bf_copi         : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_ring_rx_bf_cipo         : out t_mem_cipo := c_mem_cipo_rst;

    -- BF ring bsn monitor tx
    reg_bsn_monitor_v2_ring_tx_bf_copi         : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_ring_tx_bf_cipo         : out t_mem_cipo := c_mem_cipo_rst;

    -- BF ring validate err
    reg_dp_block_validate_err_bf_copi          : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_block_validate_err_bf_cipo          : out t_mem_cipo := c_mem_cipo_rst;

    -- BF ring bsn at sync
    reg_dp_block_validate_bsn_at_sync_bf_copi  : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_block_validate_bsn_at_sync_bf_cipo  : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- BST
    ----------------------------------------------
    -- Statistics Enable
    reg_stat_enable_bst_copi      : in  t_mem_copi := c_mem_copi_rst;
    reg_stat_enable_bst_cipo      : out t_mem_cipo := c_mem_cipo_rst;

    -- Statistics header info
    reg_stat_hdr_dat_bst_copi     : in  t_mem_copi := c_mem_copi_rst;
    reg_stat_hdr_dat_bst_cipo     : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- SST
    ----------------------------------------------
    -- Statistics Enable
    reg_stat_enable_sst_copi       : in  t_mem_copi := c_mem_copi_rst;
    reg_stat_enable_sst_cipo       : out t_mem_cipo := c_mem_cipo_rst;

    -- Statistics header info
    reg_stat_hdr_dat_sst_copi      : in  t_mem_copi := c_mem_copi_rst;
    reg_stat_hdr_dat_sst_cipo      : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- XST
    ----------------------------------------------
    -- Statistics Enable
    reg_stat_enable_xst_copi                    : in  t_mem_copi := c_mem_copi_rst;
    reg_stat_enable_xst_cipo                    : out t_mem_cipo := c_mem_cipo_rst;

    -- Statistics header info
    reg_stat_hdr_dat_xst_copi                   : in  t_mem_copi := c_mem_copi_rst;
    reg_stat_hdr_dat_xst_cipo                   : out t_mem_cipo := c_mem_cipo_rst;

    -- XST bsn aligner_v2
    reg_bsn_align_v2_xsub_copi                  : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_align_v2_xsub_cipo                  : out t_mem_cipo := c_mem_cipo_rst;

    -- XST bsn aligner_v2 bsn monitors
    reg_bsn_monitor_v2_rx_align_xsub_copi       : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_rx_align_xsub_cipo       : out t_mem_cipo := c_mem_cipo_rst;
    reg_bsn_monitor_v2_aligned_xsub_copi        : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_aligned_xsub_cipo        : out t_mem_cipo := c_mem_cipo_rst;

    -- XST UDP offload bsn monitor
    reg_bsn_monitor_v2_xst_offload_copi         : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_xst_offload_cipo         : out t_mem_cipo := c_mem_cipo_rst;

    -- XST ring lane info
    reg_ring_lane_info_xst_copi                 : in  t_mem_copi := c_mem_copi_rst;
    reg_ring_lane_info_xst_cipo                 : out t_mem_cipo := c_mem_cipo_rst;

    -- XST ring bsn monitor rx
    reg_bsn_monitor_v2_ring_rx_xst_copi         : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_ring_rx_xst_cipo         : out t_mem_cipo := c_mem_cipo_rst;

    -- XST ring bsn monitor tx
    reg_bsn_monitor_v2_ring_tx_xst_copi         : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_ring_tx_xst_cipo         : out t_mem_cipo := c_mem_cipo_rst;

    -- XST ring validate err
    reg_dp_block_validate_err_xst_copi          : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_block_validate_err_xst_cipo          : out t_mem_cipo := c_mem_cipo_rst;

    -- XST ring bsn at sync
    reg_dp_block_validate_bsn_at_sync_xst_copi  : in  t_mem_copi := c_mem_copi_rst;
    reg_dp_block_validate_bsn_at_sync_xst_cipo  : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- tr 10 GbE for ring
    ----------------------------------------------

    reg_tr_10GbE_mac_copi                       : in  t_mem_copi := c_mem_copi_rst;
    reg_tr_10GbE_mac_cipo                       : out t_mem_cipo := c_mem_cipo_rst;

    reg_tr_10GbE_eth10g_copi                    : in  t_mem_copi := c_mem_copi_rst;
    reg_tr_10GbE_eth10g_cipo                    : out t_mem_cipo := c_mem_cipo_rst;

    -- RING_0 serial
    RING_0_TX: out std_logic_vector(c_quad - 1 downto 0) := (others => '0');
    RING_0_RX: in  std_logic_vector(c_quad - 1 downto 0) := (others => '0');

    -- RING_1 serial
    RING_1_TX : out std_logic_vector(c_quad - 1 downto 0) := (others => '0');
    RING_1_RX : in  std_logic_vector(c_quad - 1 downto 0) := (others => '0');

    ----------------------------------------------
    -- nw 10 GbE for beamlet output
    ----------------------------------------------
    reg_nw_10GbE_mac_copi      : in  t_mem_copi := c_mem_copi_rst;
    reg_nw_10GbE_mac_cipo      : out t_mem_cipo := c_mem_cipo_rst;

    reg_nw_10GbE_eth10g_copi   : in  t_mem_copi := c_mem_copi_rst;
    reg_nw_10GbE_eth10g_cipo   : out t_mem_cipo := c_mem_cipo_rst;

    ----------------------------------------------
    -- QSFP[1] quad for beamlet output and QSFP[0] quad for ring cable
    ----------------------------------------------

    -- QSFP serial (6 QSFP ports per FPGA)
    unb2_board_front_io_serial_tx_arr : out std_logic_vector(6 * c_quad - 1 downto 0) := (others => '0');
    unb2_board_front_io_serial_rx_arr : in  std_logic_vector(6 * c_quad - 1 downto 0) := (others => '0');

    -- QSFP LEDS
    unb2_board_qsfp_leds_tx_sosi_arr : out t_dp_sosi_arr(6 * c_quad - 1 downto 0) := (others => c_dp_sosi_rst);
    unb2_board_qsfp_leds_tx_siso_arr : out t_dp_siso_arr(6 * c_quad - 1 downto 0) := (others => c_dp_siso_rst);
    unb2_board_qsfp_leds_rx_sosi_arr : out t_dp_sosi_arr(6 * c_quad - 1 downto 0) := (others => c_dp_sosi_rst)
  );
end sdp_station;

architecture str of sdp_station is
  -- WPFB subband width
  constant c_fft                : t_fft := func_wpfb_map_wpfb_parameters_to_fft(g_wpfb);
  constant c_fft_raw_dat_w      : natural := func_fft_raw_dat_w(c_fft);
  constant c_fft_raw_fraction_w : natural := func_fft_raw_fraction_w(c_fft);

  constant c_subband_raw_dat_w       : natural := c_fft_raw_dat_w;
  constant c_subband_raw_fraction_w  : natural := c_fft_raw_fraction_w;

  -- Make Tx FIFOs at least c_fifo_tx_fill_margin larger than needed to fit the largest Tx packet
  constant c_fifo_tx_fill_margin     : natural := 10;  -- >= c_fifo_fill_margin = 6 that is used in dp_fifo_fill_eop

  -- 10 GbE Interface for beamlet output
  constant c_nof_10GbE_beamlet_output : natural := 1;

  -- The nw_10GbE/tr_10GbE uses dp_fifo_fill_eop, so rely on releasing packets (beamlets, arp and ping) at eop instead
  -- of at fill level. Make fifo size large enough to fit one packet and the c_fifo_tx_fill_margin.
  constant c_fifo_tx_size_beamlet_output : natural := true_log_pow2(c_sdp_cep_packet_nof_longwords +
                                                                    c_fifo_tx_fill_margin);  -- = 976 + 6 --> 1024
  constant c_fifo_tx_fill_beamlet_output : natural := c_fifo_tx_size_beamlet_output -
                                                      c_fifo_tx_fill_margin;  -- = maximum fill level, so rely on eop

  -- Address widths of a single MM instance
  constant c_addr_w_ram_ss_ss_wide                 : natural := ceil_log2(c_sdp_P_pfb * c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_addr_w_ram_bf_weights                 : natural := ceil_log2(c_sdp_N_pol *
                                                                          c_sdp_P_pfb * c_sdp_S_sub_bf * c_sdp_Q_fft);
  constant c_addr_w_reg_bf_scale                   : natural := 1;
  constant c_addr_w_reg_hdr_dat                    : natural := ceil_log2(
                                                                   field_nof_words(c_sdp_cep_hdr_field_arr, c_word_w));
  constant c_addr_w_reg_bdo_destinations           : natural := c_sdp_reg_bdo_destinations_info_w_one;
  constant c_addr_w_reg_dp_xonoff                  : natural := 1;
  constant c_addr_w_ram_st_bst                     : natural := ceil_log2(c_sdp_S_sub_bf *
                                                                          c_sdp_N_pol * (c_longword_sz / c_word_sz));
  constant c_addr_w_reg_bsn_align_v2_bf            : natural := ceil_log2(c_sdp_P_sum) +
                                                                c_sdp_reg_bsn_align_v2_addr_w;
  constant c_addr_w_reg_bsn_monitor_v2_rx_align_bf : natural := ceil_log2(c_sdp_P_sum) +
                                                                c_sdp_reg_bsn_monitor_v2_addr_w;
  constant c_addr_w_reg_ring_lane_info_bf          : natural := 1;

  -- Read only sdp_info values
  constant c_f_adc     : std_logic := '1';  -- '0' => 160M, '1' => 200M
  constant c_fsub_type : std_logic := '0';  -- '0' => critical sampled PFB, '1' => oversampled PFB

  constant c_lane_payload_nof_longwords_xst : natural := c_sdp_N_crosslets_max * c_sdp_S_pn / 2;  -- = crosslet
                            -- subband select block size divided by 2 as it is repacked from 32b to 64b. = 42 longwords
  constant c_lane_payload_nof_longwords_bf  : natural := (c_sdp_S_sub_bf * c_sdp_N_pol_bf * 9) / 16;  -- = beamlet
                            -- block size repacked from 36b to 64b (9/16 = 36/64). = 549 longwords
  constant c_lane_payload_nof_longwords_max : natural := largest(c_lane_payload_nof_longwords_xst,
                                                                 c_lane_payload_nof_longwords_bf);
  constant c_lane_packet_nof_longwords_max  : natural := c_lane_payload_nof_longwords_max +
                                                         c_ring_dp_hdr_field_size;  -- = 549 + 3 = 552

  -- Use large enough c_lane_total_nof_packets_w, so that lane total nof packets count will not overflow:
  -- . For low band XST crosslets on ring : L_packet = (P_sq - 1) * f_sub = 8 * 195312.5 = 1.5625 M packets/s,
  --   so 2**48 / (1.5625e6 * 3600 * 24 *365.25) = 5.7 years.
  -- . Use same value for BF beamlets on ring.
  constant c_lane_total_nof_packets_w       : natural := 48;  -- <= c_longword_w = 64

  constant c_err_bi                    : natural := 0;
  constant c_nof_err_counts            : natural := 8;
  constant c_bsn_at_sync_check_channel : natural := 1;
  constant c_validate_channel          : boolean := true;
  constant c_validate_channel_mode     : string  := "=";
  constant c_sync_timeout              : natural := sel_a_b(g_sim, g_sim_sync_timeout, c_sdp_N_clk_sync_timeout);
  constant c_sync_timeout_xst          : natural := sel_a_b(g_sim, g_sim_sync_timeout, c_sdp_N_clk_sync_timeout_xsub);

  -- Use same Tx FIFO size for all lanes in the ring to ease the code, no need to optimize Tx FIFO RAM usage per lane.
  -- The tr_10GbE uses dp_fifo_fill_eop, so rely on releasing packets (beamlets, crosslets) at eop instead
  -- of at fill level. Make fifo size large enough to fit one packet and the c_fifo_tx_fill_margin.
  constant c_fifo_tx_size_ring         : natural := true_log_pow2(c_lane_packet_nof_longwords_max +
                                                                  c_fifo_tx_fill_margin);  -- = 552 + 6 --> 1024
  constant c_fifo_tx_fill_ring         : natural := c_fifo_tx_size_ring -
                                                    c_fifo_tx_fill_margin;  -- = maximum fill level, so rely on eop

  constant c_nof_even_lanes            : natural := 3;  -- 0 = XST, 1 = BF_0, 2 = BF_1.
  constant c_lane_nof_if               : natural := 3;  -- 3 different 10GbE interfaces per pair of lanes: QSFP cable,
                                                        -- RING_0 pcb and RING_1 pcb
  constant c_ring_qsfp_if_offset       : natural := 0;  -- QSFP cable signals are indexed at c_lane_nof_if * I.
  constant c_ring_0_if_offset          : natural := 1;  -- RING_0 pcb signals are indexed at c_lane_nof_if * I + 1.
  constant c_ring_1_if_offset          : natural := 2;  -- RING_1 pcb signals are indexed at c_lane_nof_if * I + 2.
  constant c_ring_nof_mac              : natural := c_nof_even_lanes * c_lane_nof_if;

  -- Using c_ring_nof_mac out of 12 (this is NOT optimized away during synthesis), must
  -- match one of the MAC IP variations, e.g. 1, 3, 4, 12, 24, 48
  constant c_ring_nof_mac_ip           : natural := 12;  -- >= c_ring_nof_mac

  type t_dp_sosi_2arr_pfb is array (integer range <>) of t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);

  signal gn_index                   : natural := 0;
  signal this_rn                    : std_logic_vector(c_byte_w - 1 downto 0);

  signal sdp_info                   : t_sdp_info := c_sdp_info_rst;
  signal ring_info                  : t_ring_info;

  ----------------------------------------------
  -- BF
  ----------------------------------------------
  -- Beamlet Subband Select
  signal ram_ss_ss_wide_copi_arr    : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal ram_ss_ss_wide_cipo_arr    : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- Local BF bf weights
  signal ram_bf_weights_copi_arr    : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal ram_bf_weights_cipo_arr    : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- mms_dp_scale Scale Beamlets
  signal reg_bf_scale_copi_arr      : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_bf_scale_cipo_arr      : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- Beamlet Data Output header fields
  signal reg_hdr_dat_copi_arr          : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_hdr_dat_cipo_arr          : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);
  signal reg_bdo_destinations_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_bdo_destinations_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- Beamlet Data Output xonoff
  signal reg_dp_xonoff_copi_arr     : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_dp_xonoff_cipo_arr     : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- Beamlet Statistics (BST)
  signal ram_st_bst_copi_arr        : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal ram_st_bst_cipo_arr        : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- BF bsn align v2
  signal reg_bsn_align_v2_bf_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_bsn_align_v2_bf_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- BF bsn monitor v2 rx align
  signal reg_bsn_monitor_v2_rx_align_bf_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_rx_align_bf_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_cipo_rst);

  -- BF bsn monitor v2 aligned
  signal reg_bsn_monitor_v2_aligned_bf_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_aligned_bf_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_cipo_rst);

  -- BF ring lane info
  signal reg_ring_lane_info_bf_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_ring_lane_info_bf_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- BF ring bsn monitor rx
  signal reg_bsn_monitor_v2_ring_rx_bf_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_ring_rx_bf_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_cipo_rst);

  -- BF ring bsn monitor tx
  signal reg_bsn_monitor_v2_ring_tx_bf_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_ring_tx_bf_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_cipo_rst);

  -- BF ring validate err
  signal reg_dp_block_validate_err_bf_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_copi_rst);
  signal reg_dp_block_validate_err_bf_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_cipo_rst);

  -- BF ring bsn at sync
  signal reg_dp_block_validate_bsn_at_sync_bf_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_copi_rst);
  signal reg_dp_block_validate_bsn_at_sync_bf_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_cipo_rst);

  ----------------------------------------------
  -- BST
  ----------------------------------------------
  -- Statistics Enable
  signal reg_stat_enable_bst_copi_arr  : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_stat_enable_bst_cipo_arr  : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  -- Statistics header info
  signal reg_stat_hdr_dat_bst_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_copi_rst);
  signal reg_stat_hdr_dat_bst_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_mem_cipo_rst);

  signal reg_bsn_monitor_v2_bst_offload_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_bst_offload_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_cipo_rst);

  signal reg_bsn_monitor_v2_beamlet_output_copi_arr : t_mem_copi_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_copi_rst);
  signal reg_bsn_monitor_v2_beamlet_output_cipo_arr : t_mem_cipo_arr(c_sdp_N_beamsets - 1 downto 0) :=
                                                                                             (others => c_mem_cipo_rst);

  ----------------------------------------------

  signal ait_sosi_arr                      : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0);
  signal fsub_raw_sosi_arr                 : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0);
  signal fsub_oversampled_raw_sosi_arr     : t_dp_sosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0);
  signal fsub_raw_sosi_2arr                : t_dp_sosi_2arr_pfb(c_sdp_N_beamsets - 1 downto 0);

  signal xst_bs_sosi                       : t_dp_sosi;  -- block sync reference for Xsub ring latency monitor
  signal bf_bs_sosi                        : t_dp_sosi;  -- block sync reference for BF ring latency monitor

  signal xst_from_ri_sosi                  : t_dp_sosi := c_dp_sosi_rst;
  signal xst_to_ri_sosi                    : t_dp_sosi := c_dp_sosi_rst;
  signal bf_from_ri_sosi_arr               : t_dp_sosi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_dp_sosi_rst);
  signal bf_to_ri_sosi_arr                 : t_dp_sosi_arr(c_sdp_N_beamsets - 1 downto 0) := (others => c_dp_sosi_rst);
  signal lane_rx_cable_sosi_arr            : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0);
  signal lane_tx_cable_sosi_arr            : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0);
  signal lane_rx_board_sosi_arr            : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0);
  signal lane_tx_board_sosi_arr            : t_dp_sosi_arr(c_nof_even_lanes - 1 downto 0);

  signal dp_bsn_source_restart             : std_logic;  -- used to restart WPFB sync interval timing
  signal dp_bsn_source_new_interval        : std_logic;  -- used to mask out first sync interval for SST and BST offload

  signal bf_udp_sosi_arr                   : t_dp_sosi_arr(c_sdp_N_beamsets - 1 downto 0);
  signal bf_udp_siso_arr                   : t_dp_siso_arr(c_sdp_N_beamsets - 1 downto 0);
  signal bf_10GbE_hdr_fields_out_arr       : t_slv_1024_arr(c_sdp_N_beamsets - 1 downto 0);

  -- 10GbE clock
  signal tr_ref_clk_312                    : std_logic;
  signal tr_ref_clk_156                    : std_logic;
  signal tr_ref_rst_156                    : std_logic;

  -- 10GbE ring
  signal tr_10gbe_ring_serial_tx_arr       : std_logic_vector(c_ring_nof_mac_ip - 1 downto 0) := (others => '0');
  signal tr_10gbe_ring_serial_rx_arr       : std_logic_vector(c_ring_nof_mac_ip - 1 downto 0) := (others => '0');

  signal tr_10gbe_ring_snk_in_arr          : t_dp_sosi_arr(c_ring_nof_mac_ip - 1 downto 0) := (others => c_dp_sosi_rst);
  signal tr_10gbe_ring_snk_out_arr         : t_dp_siso_arr(c_ring_nof_mac_ip - 1 downto 0) := (others => c_dp_siso_rdy);
  signal tr_10gbe_ring_src_out_arr         : t_dp_sosi_arr(c_ring_nof_mac_ip - 1 downto 0) := (others => c_dp_sosi_rst);
  signal tr_10gbe_ring_src_in_arr          : t_dp_siso_arr(c_ring_nof_mac_ip - 1 downto 0) := (others => c_dp_siso_rdy);

  -- 10GbE beamlet output
  signal nw_10gbe_beamlet_output_snk_in_arr  : t_dp_sosi_arr(c_nof_10GbE_beamlet_output - 1 downto 0) :=
                                                                                             (others => c_dp_sosi_rst);
  signal nw_10gbe_beamlet_output_snk_out_arr : t_dp_siso_arr(c_nof_10GbE_beamlet_output - 1 downto 0) :=
                                                                                             (others => c_dp_siso_rdy);
  signal nw_10gbe_beamlet_output_src_out_arr : t_dp_sosi_arr(c_nof_10GbE_beamlet_output - 1 downto 0) :=
                                                                                             (others => c_dp_sosi_rst);
  signal nw_10gbe_beamlet_output_src_in_arr  : t_dp_siso_arr(c_nof_10GbE_beamlet_output - 1 downto 0) :=
                                                                                             (others => c_dp_siso_rdy);

  signal nw_10GbE_hdr_fields_in_arr        : t_slv_1024_arr(c_nof_10GbE_beamlet_output - 1 downto 0);

  -- Network mac, ip, udp
  signal cep_eth_src_mac                   : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
  signal cep_ip_src_addr                   : std_logic_vector(c_network_ip_addr_w - 1 downto 0);
  signal cep_udp_src_port                  : std_logic_vector(c_network_udp_port_w - 1 downto 0);
  signal stat_eth_src_mac                  : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
  signal stat_ip_src_addr                  : std_logic_vector(c_network_ip_addr_w - 1 downto 0);
  signal sst_udp_src_port                  : std_logic_vector(c_network_udp_port_w - 1 downto 0);
  signal bst_udp_src_port                  : std_logic_vector(c_network_udp_port_w - 1 downto 0);
  signal xst_udp_src_port                  : std_logic_vector(c_network_udp_port_w - 1 downto 0);
begin
  gn_index <= TO_UINT(gn_id);

  -----------------------------------------------------------------------------
  -- SDP Info register
  -----------------------------------------------------------------------------
  -- . derive beamlet output MAC, IP and UDP Port as in
  --   https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L1%20Interface%20Control%20Documents/STAT%20to%20CEP%20ICD
  -- . these FW default beamlet output source MAC, IP and UDP port for 10GbE are NOT used in sdp_beamformer_output,
  --   because instead they are MM programmable as set by c_sdp_cep_hdr_field_sel
  cep_eth_src_mac  <= c_sdp_cep_eth_src_mac_47_16 &
                      RESIZE_UVEC(this_bck_id, c_byte_w) &
                      RESIZE_UVEC(this_chip_id, c_byte_w);  -- Simply use chip_id since we only use 1 of the 6*4 =
                                                            -- 24 10GbE ports.
  cep_ip_src_addr  <= c_sdp_cep_ip_src_addr_31_16 &
                      RESIZE_UVEC(this_bck_id, c_byte_w) &
                      INCR_UVEC(RESIZE_UVEC(this_chip_id, c_byte_w), 1);  -- +1 to avoid IP = *.*.*.0
  cep_udp_src_port <= c_sdp_cep_udp_src_port_15_8 & gn_id;

  -- . derive statistics offload source MAC/IP/UDP as in:
  --   https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD
  -- . these FW default statistics offload source MAC, IP and UDP port for 1GbE are used, as set by
  --   c_sdp_stat_hdr_field_sel.
  -- . the source MAC, IP are the same as for the M&C, because M&C and statistics offload share the same 1GbE
  stat_eth_src_mac <= c_sdp_stat_eth_src_mac_47_16 &
                      RESIZE_UVEC(this_bck_id, c_byte_w) &
                      RESIZE_UVEC(this_chip_id, c_byte_w);  -- Simply use chip_id since we only use 1 of the 6*4 =
                                                            -- 24 10GbE ports.
  stat_ip_src_addr <= c_sdp_stat_ip_src_addr_31_16 &
                      RESIZE_UVEC(this_bck_id, c_byte_w) &
                      INCR_UVEC(RESIZE_UVEC(this_chip_id, c_byte_w), 1);  -- +1 to avoid IP = *.*.*.0
  sst_udp_src_port <= c_sdp_sst_udp_src_port_15_8 & gn_id;
  bst_udp_src_port <= c_sdp_bst_udp_src_port_15_8 & gn_id;
  xst_udp_src_port <= c_sdp_xst_udp_src_port_15_8 & gn_id;

  u_sdp_info : entity work.sdp_info
  port map(
    -- Clocks and reset
    mm_rst    => mm_rst,  -- reset synchronous with mm_clk
    mm_clk    => mm_clk,  -- memory-mapped bus clock

    dp_clk    => dp_clk,
    dp_rst    => dp_rst,

    reg_mosi  => reg_sdp_info_copi,
    reg_miso  => reg_sdp_info_cipo,

    -- inputs from other blocks
    f_adc     => c_f_adc,
    fsub_type => c_fsub_type,

    -- sdp info
    sdp_info => sdp_info
  );

  -----------------------------------------------------------------------------
  -- Ring info
  -----------------------------------------------------------------------------
  u_ring_info : entity ring_lib.ring_info
  port map (
    mm_rst => mm_rst,
    mm_clk => mm_clk,

    dp_clk => dp_clk,
    dp_rst => dp_rst,

    reg_copi => reg_ring_info_copi,
    reg_cipo => reg_ring_info_cipo,

    ring_info => ring_info
  );

  -- Using register to ease timing closure.
  this_rn <= TO_UVEC(gn_index - TO_UINT(ring_info.O_rn), c_byte_w) when rising_edge(dp_clk);

  -----------------------------------------------------------------------------
  -- node_adc_input_and_timing (AIT)
  --   .Contains JESD receiver, bsn source and associated data buffers, diagnostics and statistics
  -----------------------------------------------------------------------------
  u_ait: entity work.node_sdp_adc_input_and_timing
  generic map(
    g_sim                       => g_sim,
    g_no_jesd                   => g_no_jesd,
    g_no_st_histogram           => g_no_st_histogram,
    g_use_tech_jesd204b_v2      => g_use_tech_jesd204b_v2,
    g_bsn_nof_clk_per_sync      => g_bsn_nof_clk_per_sync
  )
  port map(
    -- clocks and resets
    mm_clk                      => mm_clk,
    mm_rst                      => mm_rst,
    dp_clk                      => dp_clk,
    dp_rst                      => dp_rst,
    dp_pps                      => dp_pps,

    -- mm control buses
    jesd_ctrl_mosi              => jesd_ctrl_copi,
    jesd_ctrl_miso              => jesd_ctrl_cipo,
    jesd204b_mosi               => jesd204b_copi,
    jesd204b_miso               => jesd204b_cipo,
    reg_dp_shiftram_mosi        => reg_dp_shiftram_copi,
    reg_dp_shiftram_miso        => reg_dp_shiftram_cipo,
    reg_bsn_source_v2_mosi      => reg_bsn_source_v2_copi,
    reg_bsn_source_v2_miso      => reg_bsn_source_v2_cipo,
    reg_bsn_scheduler_wg_mosi   => reg_bsn_scheduler_wg_copi,
    reg_bsn_scheduler_wg_miso   => reg_bsn_scheduler_wg_cipo,
    reg_wg_mosi                 => reg_wg_copi,
    reg_wg_miso                 => reg_wg_cipo,
    ram_wg_mosi                 => ram_wg_copi,
    ram_wg_miso                 => ram_wg_cipo,
    reg_bsn_monitor_input_mosi  => reg_bsn_monitor_input_copi,
    reg_bsn_monitor_input_miso  => reg_bsn_monitor_input_cipo,
    ram_diag_data_buf_bsn_mosi  => ram_diag_data_buf_bsn_copi,
    ram_diag_data_buf_bsn_miso  => ram_diag_data_buf_bsn_cipo,
    reg_diag_data_buf_bsn_mosi  => reg_diag_data_buf_bsn_copi,
    reg_diag_data_buf_bsn_miso  => reg_diag_data_buf_bsn_cipo,
    ram_st_histogram_mosi       => ram_st_histogram_copi,
    ram_st_histogram_miso       => ram_st_histogram_cipo,
    reg_aduh_monitor_mosi       => reg_aduh_monitor_copi,
    reg_aduh_monitor_miso       => reg_aduh_monitor_cipo,

     -- Jesd external IOs
    jesd204b_serial_data       => JESD204B_SERIAL_DATA,
    jesd204b_refclk            => JESD204B_REFCLK,
    jesd204b_sysref            => JESD204B_SYSREF,
    jesd204b_sync_n            => JESD204B_SYNC_N,

    -- Streaming data output
    out_sosi_arr               => ait_sosi_arr,
    dp_bsn_source_restart      => dp_bsn_source_restart,
    dp_bsn_source_new_interval => dp_bsn_source_new_interval
  );

  -----------------------------------------------------------------------------
  -- node_sdp_filterbank (FSUB)
  -----------------------------------------------------------------------------
  gen_use_fsub : if g_use_fsub generate
    gen_use_no_oversample : if not g_use_oversample generate  -- Use normal filterbank
      u_fsub : entity work.node_sdp_filterbank
      generic map(
        g_sim                    => g_sim,
        g_sim_sdp                => g_sim_sdp,
        g_wpfb                   => g_wpfb,
        g_scope_selected_subband => g_scope_selected_subband
      )
      port map(
        dp_clk                              => dp_clk,
        dp_rst                              => dp_rst,

        in_sosi_arr                         => ait_sosi_arr,
        fsub_raw_sosi_arr                   => fsub_raw_sosi_arr,
        dp_bsn_source_restart               => dp_bsn_source_restart,
        dp_bsn_source_new_interval          => dp_bsn_source_new_interval,

        sst_udp_sosi                        => udp_tx_sosi_arr(0),
        sst_udp_siso                        => udp_tx_siso_arr(0),

        mm_rst                              => mm_rst,
        mm_clk                              => mm_clk,

        reg_si_mosi                         => reg_si_copi,
        reg_si_miso                         => reg_si_cipo,
        ram_st_sst_mosi                     => ram_st_sst_copi,
        ram_st_sst_miso                     => ram_st_sst_cipo,
        ram_fil_coefs_mosi                  => ram_fil_coefs_copi,
        ram_fil_coefs_miso                  => ram_fil_coefs_cipo,
        ram_gains_mosi                      => ram_equalizer_gains_copi,
        ram_gains_miso                      => ram_equalizer_gains_cipo,
        ram_gains_cross_mosi                => ram_equalizer_gains_cross_copi,
        ram_gains_cross_miso                => ram_equalizer_gains_cross_cipo,
        reg_selector_mosi                   => reg_dp_selector_copi,
        reg_selector_miso                   => reg_dp_selector_cipo,

        reg_enable_mosi                     => reg_stat_enable_sst_copi,
        reg_enable_miso                     => reg_stat_enable_sst_cipo,
        reg_hdr_dat_mosi                    => reg_stat_hdr_dat_sst_copi,
        reg_hdr_dat_miso                    => reg_stat_hdr_dat_sst_cipo,

        reg_bsn_monitor_v2_sst_offload_copi => reg_bsn_monitor_v2_sst_offload_copi,
        reg_bsn_monitor_v2_sst_offload_cipo => reg_bsn_monitor_v2_sst_offload_cipo,

        sdp_info                            => sdp_info,
        gn_id                               => gn_id,
        eth_src_mac                         => stat_eth_src_mac,
        ip_src_addr                         => stat_ip_src_addr,
        udp_src_port                        => sst_udp_src_port
      );

      gen_bf_sosi : for I in 0 to c_sdp_N_beamsets - 1 generate
        -- Wire same subbands to all beamsets
        fsub_raw_sosi_2arr(I) <= fsub_raw_sosi_arr;
      end generate;
    end generate;

    -----------------------------------------------------------------------------
    -- node_sdp_oversampled_filterbank
    -----------------------------------------------------------------------------
    gen_use_oversample : if g_use_oversample generate  -- use oversampled filterbank instead of normal filterbank
      u_fsub : entity work.node_sdp_oversampled_filterbank
      generic map(
        g_sim                    => g_sim,
        g_sim_sdp                => g_sim_sdp,
        g_wpfb                   => g_wpfb,
        g_wpfb_complex           => g_wpfb_complex,
        g_scope_selected_subband => g_scope_selected_subband
      )
      port map(
        dp_clk                              => dp_clk,
        dp_rst                              => dp_rst,

        in_sosi_arr                         => ait_sosi_arr,
        fsub_raw_sosi_arr                   => fsub_oversampled_raw_sosi_arr,
        dp_bsn_source_restart               => dp_bsn_source_restart,
        dp_bsn_source_new_interval          => dp_bsn_source_new_interval,

        sst_udp_sosi                        => udp_tx_sosi_arr(0),
        sst_udp_siso                        => udp_tx_siso_arr(0),

        mm_rst                              => mm_rst,
        mm_clk                              => mm_clk,

        reg_si_mosi                         => reg_si_copi,
        reg_si_miso                         => reg_si_cipo,
        ram_st_sst_mosi                     => ram_st_sst_copi,
        ram_st_sst_miso                     => ram_st_sst_cipo,
        ram_fil_coefs_mosi                  => ram_fil_coefs_copi,
        ram_fil_coefs_miso                  => ram_fil_coefs_cipo,
        ram_gains_mosi                      => ram_equalizer_gains_copi,
        ram_gains_miso                      => ram_equalizer_gains_cipo,
        ram_gains_cross_mosi                => ram_equalizer_gains_cross_copi,
        ram_gains_cross_miso                => ram_equalizer_gains_cross_cipo,
        reg_selector_mosi                   => reg_dp_selector_copi,
        reg_selector_miso                   => reg_dp_selector_cipo,

        reg_enable_mosi                     => reg_stat_enable_sst_copi,
        reg_enable_miso                     => reg_stat_enable_sst_cipo,
        reg_hdr_dat_mosi                    => reg_stat_hdr_dat_sst_copi,
        reg_hdr_dat_miso                    => reg_stat_hdr_dat_sst_cipo,

        reg_bsn_monitor_v2_sst_offload_copi => reg_bsn_monitor_v2_sst_offload_copi,
        reg_bsn_monitor_v2_sst_offload_cipo => reg_bsn_monitor_v2_sst_offload_cipo,

        sdp_info                            => sdp_info,
        gn_id                               => gn_id,
        eth_src_mac                         => stat_eth_src_mac,
        ip_src_addr                         => stat_ip_src_addr,
        udp_src_port                        => sst_udp_src_port
      );

      -- Lower part contains normal subbands, higher part contains shifted subbands.
      -- . Use normal subbands for subband correlator
      fsub_raw_sosi_arr <= fsub_oversampled_raw_sosi_arr(c_sdp_P_pfb - 1 downto 0);

      -- . Use first beamset for normal subbands
      --   Use second beamset for oversampled subbands
      fsub_raw_sosi_2arr(0) <= fsub_oversampled_raw_sosi_arr(    c_sdp_P_pfb - 1 downto 0);
      fsub_raw_sosi_2arr(1) <= fsub_oversampled_raw_sosi_arr(2 * c_sdp_P_pfb - 1 downto c_sdp_P_pfb);
    end generate;
  end generate;

  -----------------------------------------------------------------------------
  -- node_sdp_correlator (XSUB)
  -----------------------------------------------------------------------------
  gen_use_xsub : if g_use_xsub generate
    u_xsub : entity work.node_sdp_correlator
    generic map(
      g_sim                    => g_sim,
      g_sim_sdp                => g_sim_sdp,
      g_P_sq                   => g_P_sq,
      g_subband_raw_dat_w      => c_subband_raw_dat_w,
      g_subband_raw_fraction_w => c_subband_raw_fraction_w
    )
    port map(
      dp_clk                                   => dp_clk,
      dp_rst                                   => dp_rst,

      in_sosi_arr                              => fsub_raw_sosi_arr,

      xst_udp_sosi                             => udp_tx_sosi_arr(1),
      xst_udp_siso                             => udp_tx_siso_arr(1),

      from_ri_sosi                             => xst_from_ri_sosi,
      to_ri_sosi                               => xst_to_ri_sosi,

      xst_bs_sosi                              => xst_bs_sosi,

      mm_rst                                   => mm_rst,
      mm_clk                                   => mm_clk,

      reg_crosslets_info_copi                  => reg_crosslets_info_copi,
      reg_crosslets_info_cipo                  => reg_crosslets_info_cipo,
      reg_nof_crosslets_copi                   => reg_nof_crosslets_copi,
      reg_nof_crosslets_cipo                   => reg_nof_crosslets_cipo,
      reg_bsn_sync_scheduler_xsub_copi         => reg_bsn_sync_scheduler_xsub_copi,
      reg_bsn_sync_scheduler_xsub_cipo         => reg_bsn_sync_scheduler_xsub_cipo,
      ram_st_xsq_copi                          => ram_st_xsq_copi,
      ram_st_xsq_cipo                          => ram_st_xsq_cipo,

      reg_stat_enable_copi                     => reg_stat_enable_xst_copi,
      reg_stat_enable_cipo                     => reg_stat_enable_xst_cipo,
      reg_stat_hdr_dat_copi                    => reg_stat_hdr_dat_xst_copi,
      reg_stat_hdr_dat_cipo                    => reg_stat_hdr_dat_xst_cipo,

      reg_bsn_align_copi                       => reg_bsn_align_v2_xsub_copi,
      reg_bsn_align_cipo                       => reg_bsn_align_v2_xsub_cipo,
      reg_bsn_monitor_v2_bsn_align_input_copi  => reg_bsn_monitor_v2_rx_align_xsub_copi,
      reg_bsn_monitor_v2_bsn_align_input_cipo  => reg_bsn_monitor_v2_rx_align_xsub_cipo,
      reg_bsn_monitor_v2_bsn_align_output_copi => reg_bsn_monitor_v2_aligned_xsub_copi,
      reg_bsn_monitor_v2_bsn_align_output_cipo => reg_bsn_monitor_v2_aligned_xsub_cipo,
      reg_bsn_monitor_v2_xst_offload_copi      => reg_bsn_monitor_v2_xst_offload_copi,
      reg_bsn_monitor_v2_xst_offload_cipo      => reg_bsn_monitor_v2_xst_offload_cipo,

      sdp_info                                 => sdp_info,
      ring_info                                => ring_info,
      gn_id                                    => gn_id,
      stat_eth_src_mac                         => stat_eth_src_mac,
      stat_ip_src_addr                         => stat_ip_src_addr,
      stat_udp_src_port                        => xst_udp_src_port
    );
  end generate;

  -----------------------------------------------------------------------------
  -- nof beamsets node_sdp_beamformers (BF)
  -----------------------------------------------------------------------------
  gen_use_bf : if g_use_bf generate
    -- Beamformers
    gen_bf : for beamset_id in 0 to c_sdp_N_beamsets - 1 generate
      u_bf : entity work.node_sdp_beamformer
      generic map(
        g_sim                       => g_sim,
        g_sim_sdp                   => g_sim_sdp,
        g_beamset_id                => beamset_id,
        g_use_bdo_transpose         => g_use_bdo_transpose,
        g_nof_bdo_destinations_max  => g_nof_bdo_destinations_max,
        g_scope_selected_beamlet    => g_scope_selected_subband,
        g_subband_raw_dat_w         => c_subband_raw_dat_w,
        g_subband_raw_fraction_w    => c_subband_raw_fraction_w
      )
      port map(
        dp_clk                   => dp_clk,
        dp_rst                   => dp_rst,

        in_sosi_arr              => fsub_raw_sosi_2arr(beamset_id),
        from_ri_sosi             => bf_from_ri_sosi_arr(beamset_id),
        to_ri_sosi               => bf_to_ri_sosi_arr(beamset_id),
        bf_udp_sosi              => bf_udp_sosi_arr(beamset_id),
        bf_udp_siso              => bf_udp_siso_arr(beamset_id),
        bst_udp_sosi             => udp_tx_sosi_arr(2 + beamset_id),
        bst_udp_siso             => udp_tx_siso_arr(2 + beamset_id),

        dp_bsn_source_new_interval => dp_bsn_source_new_interval,

        mm_rst                   => mm_rst,
        mm_clk                   => mm_clk,

        ram_ss_ss_wide_mosi      => ram_ss_ss_wide_copi_arr(beamset_id),
        ram_ss_ss_wide_miso      => ram_ss_ss_wide_cipo_arr(beamset_id),
        ram_bf_weights_mosi      => ram_bf_weights_copi_arr(beamset_id),
        ram_bf_weights_miso      => ram_bf_weights_cipo_arr(beamset_id),
        reg_bf_scale_mosi        => reg_bf_scale_copi_arr(beamset_id),
        reg_bf_scale_miso        => reg_bf_scale_cipo_arr(beamset_id),
        reg_hdr_dat_mosi         => reg_hdr_dat_copi_arr(beamset_id),
        reg_hdr_dat_miso         => reg_hdr_dat_cipo_arr(beamset_id),
        reg_bdo_destinations_copi => reg_bdo_destinations_copi_arr(beamset_id),
        reg_bdo_destinations_cipo => reg_bdo_destinations_cipo_arr(beamset_id),
        reg_dp_xonoff_mosi       => reg_dp_xonoff_copi_arr(beamset_id),
        reg_dp_xonoff_miso       => reg_dp_xonoff_cipo_arr(beamset_id),
        ram_st_bst_mosi          => ram_st_bst_copi_arr(beamset_id),
        ram_st_bst_miso          => ram_st_bst_cipo_arr(beamset_id),
        reg_stat_enable_mosi     => reg_stat_enable_bst_copi_arr(beamset_id),
        reg_stat_enable_miso     => reg_stat_enable_bst_cipo_arr(beamset_id),
        reg_stat_hdr_dat_mosi    => reg_stat_hdr_dat_bst_copi_arr(beamset_id),
        reg_stat_hdr_dat_miso    => reg_stat_hdr_dat_bst_cipo_arr(beamset_id),
        reg_bsn_align_copi       => reg_bsn_align_v2_bf_copi_arr(beamset_id),
        reg_bsn_align_cipo       => reg_bsn_align_v2_bf_cipo_arr(beamset_id),
        reg_bsn_monitor_v2_bsn_align_input_copi  => reg_bsn_monitor_v2_rx_align_bf_copi_arr(beamset_id),
        reg_bsn_monitor_v2_bsn_align_input_cipo  => reg_bsn_monitor_v2_rx_align_bf_cipo_arr(beamset_id),
        reg_bsn_monitor_v2_bsn_align_output_copi => reg_bsn_monitor_v2_aligned_bf_copi_arr(beamset_id),
        reg_bsn_monitor_v2_bsn_align_output_cipo => reg_bsn_monitor_v2_aligned_bf_cipo_arr(beamset_id),
        reg_bsn_monitor_v2_bst_offload_copi      => reg_bsn_monitor_v2_bst_offload_copi_arr(beamset_id),
        reg_bsn_monitor_v2_bst_offload_cipo      => reg_bsn_monitor_v2_bst_offload_cipo_arr(beamset_id),
        reg_bsn_monitor_v2_beamlet_output_copi   => reg_bsn_monitor_v2_beamlet_output_copi_arr(beamset_id),
        reg_bsn_monitor_v2_beamlet_output_cipo   => reg_bsn_monitor_v2_beamlet_output_cipo_arr(beamset_id),

        sdp_info                 => sdp_info,
        ring_info                => ring_info,
        gn_id                    => gn_id,

        bdo_eth_src_mac          => cep_eth_src_mac,
        bdo_ip_src_addr          => cep_ip_src_addr,
        bdo_udp_src_port         => cep_udp_src_port,
        bdo_hdr_fields_out       => bf_10GbE_hdr_fields_out_arr(beamset_id),

        stat_eth_src_mac         => stat_eth_src_mac,
        stat_ip_src_addr         => stat_ip_src_addr,
        stat_udp_src_port        => bst_udp_src_port
      );
    end generate;

    -- MM multiplexing
    u_mem_mux_ram_ss_ss_wide : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_ram_ss_ss_wide
    )
    port map (
      mosi     => ram_ss_ss_wide_copi,
      miso     => ram_ss_ss_wide_cipo,
      mosi_arr => ram_ss_ss_wide_copi_arr,
      miso_arr => ram_ss_ss_wide_cipo_arr
    );

    u_mem_mux_ram_bf_weights : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_ram_bf_weights
    )
    port map (
      mosi     => ram_bf_weights_copi,
      miso     => ram_bf_weights_cipo,
      mosi_arr => ram_bf_weights_copi_arr,
      miso_arr => ram_bf_weights_cipo_arr
    );

    u_mem_mux_reg_bsn_align_v2_bf : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_reg_bsn_align_v2_bf
    )
    port map (
      mosi     => reg_bsn_align_v2_bf_copi,
      miso     => reg_bsn_align_v2_bf_cipo,
      mosi_arr => reg_bsn_align_v2_bf_copi_arr,
      miso_arr => reg_bsn_align_v2_bf_cipo_arr
    );

    u_mem_mux_reg_bf_scale : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_reg_bf_scale
    )
    port map (
      mosi     => reg_bf_scale_copi,
      miso     => reg_bf_scale_cipo,
      mosi_arr => reg_bf_scale_copi_arr,
      miso_arr => reg_bf_scale_cipo_arr
    );

    u_mem_mux_reg_hdr_dat : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_reg_hdr_dat
    )
    port map (
      mosi     => reg_hdr_dat_copi,
      miso     => reg_hdr_dat_cipo,
      mosi_arr => reg_hdr_dat_copi_arr,
      miso_arr => reg_hdr_dat_cipo_arr
    );

    u_mem_mux_reg_bdo_destinations : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_reg_bdo_destinations
    )
    port map (
      mosi     => reg_bdo_destinations_copi,
      miso     => reg_bdo_destinations_cipo,
      mosi_arr => reg_bdo_destinations_copi_arr,
      miso_arr => reg_bdo_destinations_cipo_arr
    );

    u_mem_mux_reg_dp_xonoff : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_reg_dp_xonoff
    )
    port map (
      mosi     => reg_dp_xonoff_copi,
      miso     => reg_dp_xonoff_cipo,
      mosi_arr => reg_dp_xonoff_copi_arr,
      miso_arr => reg_dp_xonoff_cipo_arr
    );

    u_mem_mux_ram_st_bst : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_ram_st_bst
    )
    port map (
      mosi     => ram_st_bst_copi,
      miso     => ram_st_bst_cipo,
      mosi_arr => ram_st_bst_copi_arr,
      miso_arr => ram_st_bst_cipo_arr
    );

    u_mem_mux_reg_stat_enable_bst : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_sdp_reg_stat_enable_addr_w
    )
    port map (
      mosi     => reg_stat_enable_bst_copi,
      miso     => reg_stat_enable_bst_cipo,
      mosi_arr => reg_stat_enable_bst_copi_arr,
      miso_arr => reg_stat_enable_bst_cipo_arr
    );

    u_mem_mux_reg_stat_hdr_dat_bst : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_sdp_reg_stat_hdr_dat_addr_w
    )
    port map (
      mosi     => reg_stat_hdr_dat_bst_copi,
      miso     => reg_stat_hdr_dat_bst_cipo,
      mosi_arr => reg_stat_hdr_dat_bst_copi_arr,
      miso_arr => reg_stat_hdr_dat_bst_cipo_arr
    );

    u_mem_mux_reg_bsn_monitor_v2_rx_align_bf : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_addr_w_reg_bsn_monitor_v2_rx_align_bf
    )
    port map (
      mosi     => reg_bsn_monitor_v2_rx_align_bf_copi,
      miso     => reg_bsn_monitor_v2_rx_align_bf_cipo,
      mosi_arr => reg_bsn_monitor_v2_rx_align_bf_copi_arr,
      miso_arr => reg_bsn_monitor_v2_rx_align_bf_cipo_arr
    );

    u_mem_mux_reg_bsn_monitor_v2_aligned_bf : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w
    )
    port map (
      mosi     => reg_bsn_monitor_v2_aligned_bf_copi,
      miso     => reg_bsn_monitor_v2_aligned_bf_cipo,
      mosi_arr => reg_bsn_monitor_v2_aligned_bf_copi_arr,
      miso_arr => reg_bsn_monitor_v2_aligned_bf_cipo_arr
    );

    u_mem_mux_reg_bsn_monitor_v2_bst_offload : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w
    )
    port map (
      mosi     => reg_bsn_monitor_v2_bst_offload_copi,
      miso     => reg_bsn_monitor_v2_bst_offload_cipo,
      mosi_arr => reg_bsn_monitor_v2_bst_offload_copi_arr,
      miso_arr => reg_bsn_monitor_v2_bst_offload_cipo_arr
    );

    u_mem_mux_reg_bsn_monitor_v2_beamlet_output : entity common_lib.common_mem_mux
    generic map (
      g_nof_mosi    => c_sdp_N_beamsets,
      g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w
    )
    port map (
      mosi     => reg_bsn_monitor_v2_beamlet_output_copi,
      miso     => reg_bsn_monitor_v2_beamlet_output_cipo,
      mosi_arr => reg_bsn_monitor_v2_beamlet_output_copi_arr,
      miso_arr => reg_bsn_monitor_v2_beamlet_output_cipo_arr
    );

    -----------------------------------------------------------------------------
    -- DP MUX to multiplex the c_sdp_N_beamsets via one beamlet output 10GbE link
    -----------------------------------------------------------------------------
    -- Assign hdr_fields to nw_10GbE for ARP/PING functionality. Only the fields:
    -- eth_src_mac, ip_src_addr and ip_dst_addr are used. Which are identical for
    -- both beamsets.
    nw_10GbE_hdr_fields_in_arr(0) <= bf_10GbE_hdr_fields_out_arr(0);

    u_dp_mux : entity dp_lib.dp_mux
    generic map (
      g_nof_input => c_sdp_N_beamsets,
      g_sel_ctrl_invert => true,
      g_fifo_size => array_init(0, c_sdp_N_beamsets),  -- no FIFO used but must match g_nof_input
      g_fifo_fill => array_init(0, c_sdp_N_beamsets)  -- no FIFO used but must match g_nof_input
    )
    port map (
      clk => dp_clk,
      rst => dp_rst,

      snk_in_arr  => bf_udp_sosi_arr,
      snk_out_arr => bf_udp_siso_arr,

      src_out => nw_10gbe_beamlet_output_snk_in_arr(0),
      src_in  => nw_10gbe_beamlet_output_snk_out_arr(0)
    );

    ---------------
    -- nw_10GbE beamlet output via front_io QSFP[1]
    ---------------
    u_nw_10GbE_beamlet_output: entity nw_10GbE_lib.nw_10GbE
    generic map (
      g_sim              => g_sim,
      g_sim_level        => 1,
      g_nof_macs         => c_nof_10GbE_beamlet_output,
      g_direction        => "TX_RX",
      g_tx_fifo_fill     => c_fifo_tx_fill_beamlet_output,
      g_tx_fifo_size     => c_fifo_tx_size_beamlet_output,
      g_ip_hdr_field_arr => c_sdp_cep_hdr_field_arr,
      g_xon_backpressure => true

    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644        => SA_CLK,
      tr_ref_clk_312        => tr_ref_clk_312,
      tr_ref_clk_156        => tr_ref_clk_156,
      tr_ref_rst_156        => tr_ref_rst_156,

      -- MM interface
      mm_rst                => mm_rst,
      mm_clk                => mm_clk,

      reg_mac_mosi          => reg_nw_10GbE_mac_copi,
      reg_mac_miso          => reg_nw_10GbE_mac_cipo,

      reg_eth10g_mosi       => reg_nw_10GbE_eth10g_copi,
      reg_eth10g_miso       => reg_nw_10GbE_eth10g_cipo,

      -- DP interface
      dp_rst                => dp_rst,
      dp_clk                => dp_clk,
      dp_pps                => dp_pps,

      snk_out_arr           => nw_10gbe_beamlet_output_snk_out_arr,
      snk_in_arr            => nw_10gbe_beamlet_output_snk_in_arr,

      src_out_arr           => nw_10gbe_beamlet_output_src_out_arr,
      src_in_arr            => nw_10gbe_beamlet_output_src_in_arr,

      -- Serial IO
      serial_tx_arr         => unb2_board_front_io_serial_tx_arr(c_nof_10GbE_beamlet_output + c_quad - 1 downto c_quad),
      serial_rx_arr         => unb2_board_front_io_serial_rx_arr(c_nof_10GbE_beamlet_output + c_quad - 1 downto c_quad),

      hdr_fields_in_arr     => nw_10GbE_hdr_fields_in_arr
    );
  end generate;

  gen_use_ring : if g_use_ring generate
    gen_xst_ring : if g_use_xsub generate
      u_ring_lane_xst : entity ring_lib.ring_lane
      generic map (
        g_lane_direction            => 1,  -- transport in positive direction.
        g_lane_data_w               => c_longword_w,
        g_lane_packet_length        => c_lane_payload_nof_longwords_xst,
        g_lane_total_nof_packets_w  => c_lane_total_nof_packets_w,
        g_use_dp_layer              => true,
        g_nof_rx_monitors           => c_sdp_N_pn_max,
        g_nof_tx_monitors           => c_sdp_N_pn_max,
        g_err_bi                    => c_err_bi,
        g_nof_err_counts            => c_nof_err_counts,
        g_bsn_at_sync_check_channel => c_bsn_at_sync_check_channel,
        g_validate_channel          => c_validate_channel,
        g_validate_channel_mode     => c_validate_channel_mode,
        g_sync_timeout              => c_sync_timeout_xst
      )
      port map (
        mm_rst => mm_rst,
        mm_clk => mm_clk,
        dp_clk => dp_clk,
        dp_rst => dp_rst,

        from_lane_sosi     => xst_from_ri_sosi,
        to_lane_sosi       => xst_to_ri_sosi,
        lane_rx_cable_sosi => lane_rx_cable_sosi_arr(0),
        lane_rx_board_sosi => lane_rx_board_sosi_arr(0),
        lane_tx_cable_sosi => lane_tx_cable_sosi_arr(0),
        lane_tx_board_sosi => lane_tx_board_sosi_arr(0),
        bs_sosi            => xst_bs_sosi,

        reg_ring_lane_info_copi                => reg_ring_lane_info_xst_copi,
        reg_ring_lane_info_cipo                => reg_ring_lane_info_xst_cipo,
        reg_bsn_monitor_v2_ring_rx_copi        => reg_bsn_monitor_v2_ring_rx_xst_copi,
        reg_bsn_monitor_v2_ring_rx_cipo        => reg_bsn_monitor_v2_ring_rx_xst_cipo,
        reg_bsn_monitor_v2_ring_tx_copi        => reg_bsn_monitor_v2_ring_tx_xst_copi,
        reg_bsn_monitor_v2_ring_tx_cipo        => reg_bsn_monitor_v2_ring_tx_xst_cipo,
        reg_dp_block_validate_err_copi         => reg_dp_block_validate_err_xst_copi,
        reg_dp_block_validate_err_cipo         => reg_dp_block_validate_err_xst_cipo,
        reg_dp_block_validate_bsn_at_sync_copi => reg_dp_block_validate_bsn_at_sync_xst_copi,
        reg_dp_block_validate_bsn_at_sync_cipo => reg_dp_block_validate_bsn_at_sync_xst_cipo,

        this_rn   => this_rn,
        N_rn      => ring_info.N_rn,
        rx_select => ring_info.use_cable_to_previous_rn,
        tx_select => ring_info.use_cable_to_next_rn
      );
    end generate;

    gen_bf_ring : if g_use_bf generate
      bf_bs_sosi <= fsub_raw_sosi_arr(0);

      gen_beamset_ring : for beamset_id in 0 to c_sdp_N_beamsets - 1 generate
        u_ring_lane_bf : entity ring_lib.ring_lane
        generic map (
          g_lane_direction            => 1,  -- transport in positive direction.
          g_lane_data_w               => c_longword_w,
          g_lane_packet_length        => c_lane_payload_nof_longwords_bf,
          g_lane_total_nof_packets_w  => c_lane_total_nof_packets_w,
          g_use_dp_layer              => true,
          g_nof_rx_monitors           => 1,
          g_nof_tx_monitors           => 1,
          g_err_bi                    => c_err_bi,
          g_nof_err_counts            => c_nof_err_counts,
          g_bsn_at_sync_check_channel => c_bsn_at_sync_check_channel,
          g_validate_channel          => c_validate_channel,
          g_validate_channel_mode     => c_validate_channel_mode,
          g_sync_timeout              => c_sync_timeout
        )
        port map (
          mm_rst => mm_rst,
          mm_clk => mm_clk,
          dp_clk => dp_clk,
          dp_rst => dp_rst,

          from_lane_sosi     => bf_from_ri_sosi_arr(beamset_id),
          to_lane_sosi       => bf_to_ri_sosi_arr(beamset_id),
          lane_rx_cable_sosi => lane_rx_cable_sosi_arr(1 + beamset_id),
          lane_rx_board_sosi => lane_rx_board_sosi_arr(1 + beamset_id),
          lane_tx_cable_sosi => lane_tx_cable_sosi_arr(1 + beamset_id),
          lane_tx_board_sosi => lane_tx_board_sosi_arr(1 + beamset_id),
          bs_sosi            => bf_bs_sosi,  -- used for bsn and sync

          reg_ring_lane_info_copi                => reg_ring_lane_info_bf_copi_arr(beamset_id),
          reg_ring_lane_info_cipo                => reg_ring_lane_info_bf_cipo_arr(beamset_id),
          reg_bsn_monitor_v2_ring_rx_copi        => reg_bsn_monitor_v2_ring_rx_bf_copi_arr(beamset_id),
          reg_bsn_monitor_v2_ring_rx_cipo        => reg_bsn_monitor_v2_ring_rx_bf_cipo_arr(beamset_id),
          reg_bsn_monitor_v2_ring_tx_copi        => reg_bsn_monitor_v2_ring_tx_bf_copi_arr(beamset_id),
          reg_bsn_monitor_v2_ring_tx_cipo        => reg_bsn_monitor_v2_ring_tx_bf_cipo_arr(beamset_id),
          reg_dp_block_validate_err_copi         => reg_dp_block_validate_err_bf_copi_arr(beamset_id),
          reg_dp_block_validate_err_cipo         => reg_dp_block_validate_err_bf_cipo_arr(beamset_id),
          reg_dp_block_validate_bsn_at_sync_copi => reg_dp_block_validate_bsn_at_sync_bf_copi_arr(beamset_id),
          reg_dp_block_validate_bsn_at_sync_cipo => reg_dp_block_validate_bsn_at_sync_bf_cipo_arr(beamset_id),

          this_rn   => this_rn,
          N_rn      => ring_info.N_rn,
          rx_select => ring_info.use_cable_to_previous_rn,
          tx_select => ring_info.use_cable_to_next_rn
        );
      end generate;

      u_mem_mux_reg_ring_lane_info_bf : entity common_lib.common_mem_mux
      generic map (
        g_nof_mosi    => c_sdp_N_beamsets,
        g_mult_addr_w => c_addr_w_reg_ring_lane_info_bf
      )
      port map (
        mosi     => reg_ring_lane_info_bf_copi,
        miso     => reg_ring_lane_info_bf_cipo,
        mosi_arr => reg_ring_lane_info_bf_copi_arr,
        miso_arr => reg_ring_lane_info_bf_cipo_arr
      );

      u_mem_mux_reg_bsn_monitor_v2_ring_rx_bf : entity common_lib.common_mem_mux
      generic map (
        g_nof_mosi    => c_sdp_N_beamsets,
        g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w
      )
      port map (
        mosi     => reg_bsn_monitor_v2_ring_rx_bf_copi,
        miso     => reg_bsn_monitor_v2_ring_rx_bf_cipo,
        mosi_arr => reg_bsn_monitor_v2_ring_rx_bf_copi_arr,
        miso_arr => reg_bsn_monitor_v2_ring_rx_bf_cipo_arr
      );

      u_mem_mux_reg_bsn_monitor_v2_ring_tx_bf : entity common_lib.common_mem_mux
      generic map (
        g_nof_mosi    => c_sdp_N_beamsets,
        g_mult_addr_w => c_sdp_reg_bsn_monitor_v2_addr_w
      )
      port map (
        mosi     => reg_bsn_monitor_v2_ring_tx_bf_copi,
        miso     => reg_bsn_monitor_v2_ring_tx_bf_cipo,
        mosi_arr => reg_bsn_monitor_v2_ring_tx_bf_copi_arr,
        miso_arr => reg_bsn_monitor_v2_ring_tx_bf_cipo_arr
      );

      u_mem_mux_reg_dp_block_validate_err_bf : entity common_lib.common_mem_mux
      generic map (
        g_nof_mosi    => c_sdp_N_beamsets,
        g_mult_addr_w => c_sdp_reg_dp_block_validate_err_addr_w
      )
      port map (
        mosi     => reg_dp_block_validate_err_bf_copi,
        miso     => reg_dp_block_validate_err_bf_cipo,
        mosi_arr => reg_dp_block_validate_err_bf_copi_arr,
        miso_arr => reg_dp_block_validate_err_bf_cipo_arr
      );

      u_mem_mux_reg_dp_block_validate_bsn_at_sync_bf : entity common_lib.common_mem_mux
      generic map (
        g_nof_mosi    => c_sdp_N_beamsets,
        g_mult_addr_w => c_sdp_reg_dp_block_validate_bsn_at_sync_addr_w
      )
      port map (
        mosi     => reg_dp_block_validate_bsn_at_sync_bf_copi,
        miso     => reg_dp_block_validate_bsn_at_sync_bf_cipo,
        mosi_arr => reg_dp_block_validate_bsn_at_sync_bf_copi_arr,
        miso_arr => reg_dp_block_validate_bsn_at_sync_bf_cipo_arr
      );
    end generate;

    -----------------------------------------------------------------------------
    -- Combine seperate signals into array for tr_10GbE
    -----------------------------------------------------------------------------
    gen_lane_wires : for I in 0 to c_nof_even_lanes - 1 generate
      -- QSFP_RX, use_cable_to_previous_rn=1 -> even lanes receive from cable
      lane_rx_cable_sosi_arr(I) <= tr_10gbe_ring_src_out_arr(c_lane_nof_if * I + c_ring_qsfp_if_offset) when
                                   ring_info.use_cable_to_previous_rn = '1' else c_dp_sosi_rst;

      -- QSFP_TX, use_cable_to_next_rn=1 -> even lanes transmit to cable
      tr_10gbe_ring_snk_in_arr(c_lane_nof_if * I + c_ring_qsfp_if_offset) <= lane_tx_cable_sosi_arr(I) when
                                   ring_info.use_cable_to_next_rn = '1' else c_dp_sosi_rst;

      -- RING_0_RX even lanes receive from RING_0 (from the left)
      lane_rx_board_sosi_arr(I) <= tr_10gbe_ring_src_out_arr(c_lane_nof_if * I + c_ring_0_if_offset);

      -- RING_1_TX even lanes transmit to RING_1 (to the right)
      tr_10gbe_ring_snk_in_arr(c_lane_nof_if * I + c_ring_1_if_offset) <= lane_tx_board_sosi_arr(I);
    end generate;

    -----------------------------------------------------------------------------
    -- tr_10GbE ring via front_io QSFP[0]
    -----------------------------------------------------------------------------
    u_tr_10GbE_ring: entity tr_10GbE_lib.tr_10GbE
    generic map (
      g_sim           => g_sim,
      g_sim_level     => 1,
      g_nof_macs      => c_ring_nof_mac_ip,
      g_direction     => "TX_RX",
      g_tx_fifo_fill  => c_fifo_tx_fill_ring,
      g_tx_fifo_size  => c_fifo_tx_size_ring
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644        => SA_CLK,
      tr_ref_clk_312        => tr_ref_clk_312,
      tr_ref_clk_156        => tr_ref_clk_156,
      tr_ref_rst_156        => tr_ref_rst_156,

      -- MM interface
      mm_rst                => mm_rst,
      mm_clk                => mm_clk,

      reg_mac_mosi          => reg_tr_10GbE_mac_copi,
      reg_mac_miso          => reg_tr_10GbE_mac_cipo,

      reg_eth10g_mosi       => reg_tr_10GbE_eth10g_copi,
      reg_eth10g_miso       => reg_tr_10GbE_eth10g_cipo,

      -- DP interface
      dp_rst                => dp_rst,
      dp_clk                => dp_clk,

      src_out_arr           => tr_10gbe_ring_src_out_arr,
      src_in_arr            => tr_10gbe_ring_src_in_arr,

      snk_out_arr           => tr_10gbe_ring_snk_out_arr,
      snk_in_arr            => tr_10gbe_ring_snk_in_arr,

      -- Serial IO
      serial_tx_arr         => tr_10gbe_ring_serial_tx_arr,
      serial_rx_arr         => tr_10gbe_ring_serial_rx_arr
    );

    -----------------------------------------------------------------------------
    -- Seperate serial tx/rx array
    -----------------------------------------------------------------------------
    -- Seperating the one large serial tx/rx array from tr_10GbE to the c_lane_nof_if = 3
    -- port arrays: QSFP cable port, RING_0 pcb port and RING_1 pcb port.
    gen_ring_serial_wires : for I in 0 to c_nof_even_lanes - 1 generate
      -- QSFP_TX
      unb2_board_front_io_serial_tx_arr(I) <= tr_10gbe_ring_serial_tx_arr(c_lane_nof_if * I + c_ring_qsfp_if_offset);
      -- QSFP_RX
      tr_10gbe_ring_serial_rx_arr(c_lane_nof_if * I + c_ring_qsfp_if_offset) <= unb2_board_front_io_serial_rx_arr(I);

      -- RING_0_TX
      RING_0_TX(I) <= tr_10gbe_ring_serial_tx_arr(c_lane_nof_if * I + c_ring_0_if_offset);
      -- RING_0_RX
      tr_10gbe_ring_serial_rx_arr(c_lane_nof_if * I + c_ring_0_if_offset) <= RING_0_RX(I);

      -- RING_1_TX
      RING_1_TX(I) <= tr_10gbe_ring_serial_tx_arr(c_lane_nof_if * I + c_ring_1_if_offset);
      -- RING_1_RX
      tr_10gbe_ring_serial_rx_arr(c_lane_nof_if * I + c_ring_1_if_offset) <= RING_1_RX(I);
    end generate;
  end generate;

  ---------
  -- PLL
  ---------
  u_tech_pll_xgmii_mac_clocks : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
  port map (
    refclk_644 => SA_CLK,
    rst_in     => mm_rst,
    clk_156    => tr_ref_clk_156,
    clk_312    => tr_ref_clk_312,
    rst_156    => tr_ref_rst_156,
    rst_312    => open
  );

  ------------
  -- LEDs
  ------------
  -- QSFP 1 - Beamlets
  unb2_board_qsfp_leds_tx_siso_arr(c_nof_10GbE_beamlet_output + c_quad - 1 downto c_quad) <=
                                                                                    nw_10gbe_beamlet_output_snk_out_arr;
  unb2_board_qsfp_leds_tx_sosi_arr(c_nof_10GbE_beamlet_output + c_quad - 1 downto c_quad) <=
                                                                                    nw_10gbe_beamlet_output_snk_in_arr;
  unb2_board_qsfp_leds_rx_sosi_arr(c_nof_10GbE_beamlet_output + c_quad - 1 downto c_quad) <=
                                                                                    nw_10gbe_beamlet_output_src_out_arr;

  -- QSFP 0 - Ring
  unb2_board_qsfp_leds_tx_siso_arr(0 downto 0) <= tr_10gbe_ring_snk_out_arr(0 downto 0);
  unb2_board_qsfp_leds_tx_sosi_arr(0 downto 0) <= tr_10gbe_ring_snk_in_arr(0 downto 0);
  unb2_board_qsfp_leds_rx_sosi_arr(0 downto 0) <= tr_10gbe_ring_src_out_arr(0 downto 0);
end str;
