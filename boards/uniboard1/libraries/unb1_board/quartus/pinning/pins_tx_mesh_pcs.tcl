###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

set_location_assignment PIN_W38 -to SB_CLK
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to SB_CLK

set_location_assignment PIN_K36 -to FN_BN_0_TX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_0_TX[0]

set_location_assignment PIN_D36 -to FN_BN_0_TX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_0_TX[1]

set_location_assignment PIN_B36 -to FN_BN_0_TX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_0_TX[2]

set_location_assignment PIN_T36 -to FN_BN_1_TX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_1_TX[0]

set_location_assignment PIN_P36 -to FN_BN_1_TX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_1_TX[1]

set_location_assignment PIN_M36 -to FN_BN_1_TX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_1_TX[2]

set_location_assignment PIN_AF36 -to FN_BN_2_TX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_2_TX[0]

set_location_assignment PIN_AD36 -to FN_BN_2_TX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_2_TX[1]

set_location_assignment PIN_AB36 -to FN_BN_2_TX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_2_TX[2]

set_location_assignment PIN_AT36 -to FN_BN_3_TX[0]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_3_TX[0]

set_location_assignment PIN_AP36 -to FN_BN_3_TX[1]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_3_TX[1]

set_location_assignment PIN_AH36 -to FN_BN_3_TX[2]
set_instance_assignment -name IO_STANDARD "1.4-V PCML" -to FN_BN_3_TX[2]

