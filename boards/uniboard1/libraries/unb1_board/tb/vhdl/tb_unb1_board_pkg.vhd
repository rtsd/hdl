-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;

package tb_unb1_board_pkg is
  -- Aggregate types to contain all TR for all nodes on one side of the mesh or backplane interface

  -- UniBoard TR mesh IO for 4 to 4 nodes : [node][bus][lane]
  type t_unb1_board_mesh_sosi_3arr is array (c_unb1_board_nof_node-1 downto 0) of t_unb1_board_mesh_sosi_2arr;
  type t_unb1_board_mesh_siso_3arr is array (c_unb1_board_nof_node-1 downto 0) of t_unb1_board_mesh_siso_2arr;
  type t_unb1_board_mesh_sl_3arr   is array (c_unb1_board_nof_node-1 downto 0) of t_unb1_board_mesh_sl_2arr;

  -- Subrack with mesh IO for 4 UniBoards, each with TR mesh for 4 to 4 nodes : [unb][node][bus][lane]
  type t_unb1_board_mesh_sosi_4arr is array (c_unb1_board_nof_uniboard - 1 downto 0) of t_unb1_board_mesh_sosi_3arr;
  type t_unb1_board_mesh_siso_4arr is array (c_unb1_board_nof_uniboard - 1 downto 0) of t_unb1_board_mesh_siso_3arr;
  type t_unb1_board_mesh_sl_4arr   is array (c_unb1_board_nof_uniboard - 1 downto 0) of t_unb1_board_mesh_sl_3arr;

  -- UniBoard TR backplane IO for 4 BN, each BN with TR to 3 BN each on the 3 other UniBoards [bn][bus][lane]
  type t_unb1_board_back_sosi_3arr is array (c_unb1_board_nof_node-1 downto 0) of t_unb1_board_back_sosi_2arr;
  type t_unb1_board_back_siso_3arr is array (c_unb1_board_nof_node-1 downto 0) of t_unb1_board_back_siso_2arr;
  type t_unb1_board_back_sl_3arr   is array (c_unb1_board_nof_node-1 downto 0) of t_unb1_board_back_sl_2arr;

  -- UniBoard TR backplane IO for 4 UniBoards * 3 buses to other uniboards : [unb][bn][bus][lane]
  type t_unb1_board_back_sosi_4arr is array (c_unb1_board_nof_uniboard - 1 downto 0) of t_unb1_board_back_sosi_3arr;
  type t_unb1_board_back_siso_4arr is array (c_unb1_board_nof_uniboard - 1 downto 0) of t_unb1_board_back_siso_3arr;
  type t_unb1_board_back_sl_4arr   is array (c_unb1_board_nof_uniboard - 1 downto 0) of t_unb1_board_back_sl_3arr;

  type t_unb1_board_id_arr is array (natural range <>) of std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0);
end tb_unb1_board_pkg;

package body tb_unb1_board_pkg is
end tb_unb1_board_pkg;
