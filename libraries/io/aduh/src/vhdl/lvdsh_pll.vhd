-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose: Handle an LVDS Rx interface on a BN using a PLL
-- Description:
--   The LVDS Rx interface has input data at a rate g_lvds_data_rate = 800 Mbps.
--   The frequency of the reference clock for this LVDS Rx data is given by
--   g_lvds_clk_freq = 400 MHz for the LVDS clock from ADU or e.g. 200 MHz in
--   case the dp_clk digital processing clock is also used as LVDS clock (i.e.
--   then externally dp_clk connects to dp_clk as well as to lvds_clk).
--   The phase skew of the reference clock relative to the LVDS data can be
--   given via g_lvds_clk_phase.
--   The actual g_deser_factor depends on the ratio of the LVDS data rate and
--   the digital processing clock frequency and is given by g_deser_factor.
--   When the digital processing clock runs at 200 MHz then g_deser_factor=4
--   and then this lvdsh_pll needs to uses a PLL to de-serialize the LVDS Rx
--   data. However when g_use_ddio = TRUE and the digital processing clock runs
--   at 400 MHz (so g_deser_factor=2) and then this lvdsh_pll can use DDIO to
--   capture the LVDS data without using a PLL.
--   DPA requires using a PLL, so then g_use_ddio must be FALSE.
--
-- Remarks:
-- . With ADU the lvds_dat data comes in with a DDR lvds_clk clock, so a deser
--   factor of 2, but the fabric logic in the FPGA needs to process it at a 4
--   times lower rate, so g_deser_factor = 4. Therefore this lvdsh_pll supports
--   these different deser factors by having two separate generics
--   g_lvds_clk_freq for the ADU side and g_deser_factor for the FPGA fabric
--   side.
--
-- . DPA can only be used with the two PLLs on the BN LVDS side. Using DPA
--   assumes that the LVDS data has transitions for each line. For an ADC
--   that outputs signed numbers that will be true provided that any DC offset
--   is smaller than the input signal peak-peak range. Otherwise we may have
--   to initially use the ADC test pattern mode to train the DPA.
--
-- . The dp_pll_reset input could also be connected inside this component by
--   adding rx_clk active detection logic based on the dp_clk. Both the dp_clk
--   and the rx_clk run at the same frequency. Hence if both clock a counter
--   and the rx_clk counter starts to differ to much from the dp_clk counter,
--   then the dp_pll_reset can be asserted. This can also be detected by means
--   of the rx_clk - dp_clk clock domain crossing FIFO. If this u_cross_fifo
--   runs full or empty then there is something wrong with the dp_clk - rx_clk
--   relation (e.g. rx_clk stopped, because ADU was removed, or one or both
--   of the clocks have lost lock to their common source).

entity lvdsh_pll is
  generic (
    g_lvds_w           : natural := 16;  -- bits
    g_lvds_data_rate   : natural := 800;  -- Msps
    g_lvds_clk_freq    : natural := 400;  -- MHz
    g_lvds_clk_phase   : natural := 0;  -- degrees, only for no DPA
    g_use_lvds_clk_rst : boolean := false;
    g_deser_factor     : natural := 4;
    g_use_dpa          : boolean := true;
    g_use_ddio         : boolean := false
  );
  port (
    -- PHY LVDS Interface
    lvds_clk_rst      : out   std_logic;  -- release synchronises lvds_clk phase
    lvds_clk          : in    std_logic;
    lvds_dat          : in    std_logic_vector(g_lvds_w - 1 downto 0);

    -- DP Streaming Interface
    dp_clk            : in    std_logic;  -- dp_clk frequency is g_lvds_data_rate / g_deser_factor

    -- . Control
    dp_lvds_reset     : in    std_logic := '0';  -- LVDS interface reset, necessary if the lvds_clk was not present at power up or if it was temporarily removed
    dp_delay_settings : in    t_natural_arr(g_lvds_w - 1 downto 0) := array_init(0, g_lvds_w);  -- only used for non DPA mode
    dp_cda_settings   : in    t_natural_arr(g_lvds_w - 1 downto 0) := array_init(0, g_lvds_w);

    -- . Streaming
    dp_dat            : out   std_logic_vector(g_deser_factor * g_lvds_w - 1 downto 0);
    dp_val            : out   std_logic
  );
end lvdsh_pll;

architecture rtl of lvdsh_pll is
  -- Default use c_use_dp_clk_for_cda_reset is TRUE to use dp_clk to reset the
  -- CDA to avoid g_deser_factor lvds_clk divider phase uncertainty. Else use
  -- rx_clk for investigation purposes to try whether lvds_clk divider phase
  -- uncertainty will occur or on target. The uncertainty appears independend
  -- of wheteher dp_clk is used or not, because the CDA IP function internally
  -- probably reclocks the CDA reset to the rx_clk domain.
  constant c_use_dp_clk_for_cda_reset : boolean := true;

  constant c_wait_cnt_w               : natural := 5;  -- >=1, use wider, e.g. 5, to be able to recognize the rx_dat result during each CDA control step in simulation

  constant c_rx_dat_w                 : natural := g_deser_factor * g_lvds_w;

  type t_cda_state is (s_cda_reset, s_cda_ctrl, s_cda_wait, s_cda_done);

  -- PLL control
  signal dp_lvds_reset_release : std_logic;
  signal dp_pll_reset          : std_logic;
  signal pll_locked            : std_logic;  -- LVDS_RX PLL locked (called rx_locked, but simulation shows it is asynchronous to rx_clk)
  signal rx_pll_locked         : std_logic;

  -- DPA control
  signal rx_cda_settings      : t_natural_arr(g_lvds_w - 1 downto 0) := array_init(0, g_lvds_w);
  signal rx_dpa_reset         : std_logic;  -- LVDS DPA reset (called rx_reset)
  signal rx_dpa_reset_slv     : std_logic_vector(g_lvds_w - 1 downto 0);
  signal rx_dpa_locked_slv    : std_logic_vector(g_lvds_w - 1 downto 0);  -- LVDS DPA locked
  signal rx_dpa_locked        : std_logic;
  signal rx_fifo_reset        : std_logic;
  signal rx_fifo_reset_slv    : std_logic_vector(g_lvds_w - 1 downto 0);

  -- Deserialized data
  signal rx_clk               : std_logic;  -- same frequency as dp_clk, but with unknown phase offset
  signal rx_eye_locked        : std_logic;  -- sampling occurs in the eye
  signal nxt_rx_eye_locked    : std_logic;

  -- Channel Data Alignment
  signal dp_cda_reset         : std_logic;
  signal rx_cda_reset         : std_logic;
  signal cda_reset_slv        : std_logic_vector(g_lvds_w - 1 downto 0);
  signal rx_cda_state         : t_cda_state;
  signal nxt_rx_cda_state     : t_cda_state;
  signal rx_cda_ctrl          : std_logic_vector(g_lvds_w - 1 downto 0);
  signal nxt_rx_cda_ctrl      : std_logic_vector(rx_cda_ctrl'range);
  signal rx_cda_cnt           : natural range 0 to g_deser_factor - 1;
  signal nxt_rx_cda_cnt       : natural;
  signal rx_cda_cnt_en        : std_logic;
  signal rx_wait_cnt          : std_logic_vector(c_wait_cnt_w - 1 downto 0);
  signal nxt_rx_wait_cnt      : std_logic_vector(c_wait_cnt_w - 1 downto 0);
  signal rx_wait_cnt_clr      : std_logic;

  -- Deserialized word aligned data
  signal rx_dat               : std_logic_vector(c_rx_dat_w - 1 downto 0);
  signal rx_val               : std_logic;
  signal nxt_rx_val           : std_logic;
begin
  assert g_use_ddio = false
    report "lvdsh_pll.vhd: LVDS Rx using DDIO without PLL is not supported yet"
    severity FAILURE;

  no_lvds_clk_rst : if g_use_lvds_clk_rst = false generate
    lvds_clk_rst <= '0';
    dp_pll_reset <= dp_lvds_reset;
  end generate;

  gen_lvds_clk_rst : if g_use_lvds_clk_rst = true generate
    ---------------------------------------------------------------------------
    -- Synchronise SCLK -> DCLK divider using dp_clk
    ---------------------------------------------------------------------------

    -- Assign fixed output delay for D5 (0-15 steps of 50 ps, so 750 ps max)
    -- and D6 (0-7 steps of 50 ps, so 350 ps max) via the Quartus, because the
    -- ALTIOBUF MegaWizard IP component can not set this from VHDL.
    -- The ALTIOBUF can be used in case dynamic output delay is needed, but
    -- since all ADU - BN interfaces have the same skew between SCLK and
    -- DCLK_RST using a fixed output delay setting seems OK.

    -- Use the release of dp_lvds_reset to start the lvds_clk_rst pulse.
    u_lvds_reset_release : entity common_lib.common_evt
    generic map (
      g_evt_type => "FALLING",
      g_out_reg  => true
    )
    port map (
      clk      => dp_clk,
      in_sig   => dp_lvds_reset,
      out_evt  => dp_lvds_reset_release
    );

    -- Keep the lvds_clk_rst active for at least 4 sample clock in case of
    -- adc08d1020, so at least 4/g_deser_factor=1 dp_clk cycles.
    u_lvds_clk_reset : entity common_lib.common_pulse_extend
    generic map (
      g_rst_level    => '0',
      g_p_in_level   => '1',
      g_ep_out_level => '1',
      g_extend_w     => 1  -- ep_out will be active for 2**1 = 2 dp_clk cyles
    )
    port map (
      clk     => dp_clk,
      p_in    => dp_lvds_reset_release,
      ep_out  => lvds_clk_rst
    );

    -- Release the LVDS_RX PLL reset somewhat later to be sure that the lvds_clk
    -- is active again after the lvds_clk_rst.
    u_pll_reset : entity common_lib.common_pulse_extend
    generic map (
      g_rst_level    => '1',
      g_p_in_level   => '1',
      g_ep_out_level => '1',
      g_extend_w     => 4  -- ep_out will be active for 2**4 = 16 dp_clk cyles
    )
    port map (
      clk     => dp_clk,
      p_in    => dp_lvds_reset,
      ep_out  => dp_pll_reset
    );
  end generate;

  ---------------------------------------------------------------------------
  -- Input no DPA
  ---------------------------------------------------------------------------

  no_dpa : if g_use_dpa = false generate
    u_lvds_rx : entity work.lvds_rx
    generic map (
      g_lvds_w         => g_lvds_w,
      g_lvds_data_rate => g_lvds_data_rate,
      g_lvds_clk_freq  => g_lvds_clk_freq,
      g_lvds_clk_phase => g_lvds_clk_phase,
      g_deser_factor   => g_deser_factor
    )
    port map (
      pll_areset            => dp_pll_reset,  -- asynchronous, minimum pulse width is 10 ns
      rx_cda_reset          => cda_reset_slv,  -- asynchronous, minimum pulse width is 1 rx_outclock cycle
      rx_channel_data_align => rx_cda_ctrl,  -- the data slips one bit for every pulse, minimum pulse width is 1 rx_outclock cycle
      rx_in                 => lvds_dat,
      rx_inclock            => lvds_clk,
      rx_locked             => pll_locked,
      rx_out                => rx_dat,
      rx_outclock           => rx_clk
    );

    nxt_rx_eye_locked <= rx_pll_locked;
  end generate;  -- no_dpa

  ---------------------------------------------------------------------------
  -- Input use DPA
  ---------------------------------------------------------------------------

  gen_dpa : if g_use_dpa = true generate
    u_lvds_rx_dpa : entity work.lvds_rx_dpa
    generic map (
      g_lvds_w         => g_lvds_w,
      g_lvds_data_rate => g_lvds_data_rate,
      g_lvds_clk_freq  => g_lvds_clk_freq,
      g_deser_factor   => g_deser_factor
    )
    port map (
         pll_areset            => dp_pll_reset,  -- asynchronous, minimum pulse width is 10 ns
         rx_cda_reset          => cda_reset_slv,  -- asynchronous, minimum pulse width is 1 rx_outclock cycle
         rx_channel_data_align => rx_cda_ctrl,  -- the data slips one bit for every pulse, minimum pulse width is 1 rx_outclock cycle
         rx_fifo_reset         => rx_fifo_reset_slv,  -- asynchronous, minimum pulse width is 1 rx_outclock cycle
         rx_in                 => lvds_dat,
         rx_inclock            => lvds_clk,
         rx_reset              => rx_dpa_reset_slv,  -- asynchronous, minimum pulse width is 1 rx_outclock cycle
         rx_dpa_locked         => rx_dpa_locked_slv,
         rx_locked             => pll_locked,
         rx_out                => rx_dat,
         rx_outclock           => rx_clk
    );

    p_rx_dpa_reset : process(dp_pll_reset, rx_clk)
    begin
      if dp_pll_reset = '1' then
        rx_dpa_reset <= '1';
      elsif rising_edge(rx_clk) then
        rx_dpa_reset <= not rx_pll_locked;  -- release the DPA reset after the PLL has locked
      end if;
    end process;

    rx_dpa_reset_slv <= (others => rx_dpa_reset);

    rx_dpa_locked <= vector_and(rx_dpa_locked_slv);

    nxt_rx_eye_locked <= rx_dpa_locked;

    u_rx_fifo_reset_evt : entity common_lib.common_evt
    generic map (
      g_evt_type => "RISING",
      g_out_reg  => true
    )
    port map (
      rst      => dp_pll_reset,
      clk      => rx_clk,
      in_sig   => rx_eye_locked,
      out_evt  => rx_fifo_reset  -- reset the DPA FIFO after the DPA got locked
    );

    rx_fifo_reset_slv <= (others => rx_fifo_reset);
  end generate;  -- gen_dpa

  p_rx_dpa_reg : process(dp_pll_reset, rx_clk)
  begin
    if dp_pll_reset = '1' then
      rx_pll_locked <= '0';
      rx_eye_locked <= '0';
    elsif rising_edge(rx_clk) then
      rx_pll_locked <= pll_locked;
      rx_eye_locked <= nxt_rx_eye_locked;
    end if;
  end process;

  ---------------------------------------------------------------------------
  -- Apply Channel Data Alignment (CDA)
  ---------------------------------------------------------------------------

  -- release the rx_cda_reset when the eye in the LVDS data was found
  -- use the rx_clk to time the CDA reset to try to have the deser_factor divide phase uncertainty in the rx_clk
  p_rx_cda_reset : process(dp_pll_reset, rx_clk)
  begin
    if dp_pll_reset = '1' then
      rx_cda_reset <= '1';
    elsif rising_edge(rx_clk) then
      rx_cda_reset <= not rx_eye_locked;  -- release the CDA reset when the LVDS data is sampled in the eye
    end if;
  end process;

  -- use the dp_clk to time the CDA reset to avoid the deser_factor divide phase uncertainty in the rx_clk
  u_dp_cda_reset : entity common_lib.common_async
  generic map (
    g_rst_level => '1'
  )
  port map (
    rst  => rx_cda_reset,  -- use rx_cda_reset in rx_clk domain
    clk  => dp_clk,
    din  => '0',
    dout => dp_cda_reset  -- use dp_cda_reset in dp_clk domain
  );

  -- release the CDA reset when the LVDS data is sampled in the eye
  cda_reset_slv <= (others => dp_cda_reset) when c_use_dp_clk_for_cda_reset = true else (others => rx_cda_reset);

  -- use the rx_cda_reset also for the rx_cda_ctrl[] in the rx_clk domain
  p_rx_cda_reg : process(rx_cda_reset, rx_clk)
  begin
    -- use the rx_clk because rx_cda_ctrl is in that clock domain and because the rx_clk will be active for sure now
    if rx_cda_reset = '1' then
      rx_cda_state  <= s_cda_reset;
      rx_cda_ctrl   <= (others => '0');
      rx_cda_cnt    <= 0;
      rx_wait_cnt   <= (others => '0');
      rx_val        <= '0';
    elsif rising_edge(rx_clk) then
      rx_cda_state  <= nxt_rx_cda_state;
      rx_cda_ctrl   <= nxt_rx_cda_ctrl;  -- apply the preset nof bit slip pulses
      rx_cda_cnt    <= nxt_rx_cda_cnt;
      rx_wait_cnt   <= nxt_rx_wait_cnt;
      rx_val        <= nxt_rx_val;
    end if;
  end process;

  -- The CDA may need 0:g_deser_factor-1 number of bit slip pulses to align the
  -- deserialized g_deser_factor number of bit wide words.
  -- All ADU-BN connection have the same skew per LVDS data line. The skew
  -- between LVDS data lines may differ. Therefore a fixed number of bit slip
  -- pulses per LVDS data line should be enough to get CDA. This avoids having
  -- to use a training pattern (on ADU we could use the test pattern from the
  -- adc08d1020).

  -- Immediately after rx_cda_reset release apply the preset number of CDA bit
  -- slip pulses for each LVDS data line as set by the dp_cda_settings.

  rx_cda_settings <= dp_cda_settings;  -- the dp_cda_settings are stable, so no need to use register stages to synchronise them into the rx_clk domain

  nxt_rx_cda_cnt  <= rx_cda_cnt + 1 when rx_cda_cnt_en = '1'   else rx_cda_cnt;
  nxt_rx_wait_cnt <= (others => '0')  when rx_wait_cnt_clr = '1' else INCR_UVEC(rx_wait_cnt, 1);

  p_rx_cda_state : process(rx_cda_state, rx_cda_settings, rx_cda_cnt, rx_wait_cnt)
  begin
    nxt_rx_cda_state <= rx_cda_state;

    rx_cda_cnt_en   <= '0';
    rx_wait_cnt_clr <= '1';

    nxt_rx_cda_ctrl <= (others => '0');
    nxt_rx_val <= '0';

    case rx_cda_state is
      when s_cda_reset =>
        nxt_rx_cda_state <= s_cda_ctrl;
      when s_cda_ctrl =>
        if rx_cda_cnt < g_deser_factor - 1 then
          rx_cda_cnt_en <= '1';
          for I in 0 to g_lvds_w - 1 loop
            if rx_cda_settings(I) > rx_cda_cnt then
              nxt_rx_cda_ctrl(I) <= '1';
            end if;
          end loop;
          nxt_rx_cda_state <= s_cda_wait;
        else
          nxt_rx_cda_state <= s_cda_done;
        end if;
      when s_cda_wait =>
        rx_wait_cnt_clr <= '0';
        if rx_wait_cnt(rx_wait_cnt'high) = '1' then
          nxt_rx_cda_state <= s_cda_ctrl;
        end if;
      when others =>  -- = s_cda_done
        nxt_rx_val <= '1';
    end case;
  end process;

  -----------------------------------------------------------------------------
  -- Output cross from the rx_clk domain to the dp_clk domain
  -----------------------------------------------------------------------------

  u_cross_fifo : entity common_lib.common_fifo_dc
  generic map (
    g_dat_w     => c_rx_dat_w,
    g_nof_words => c_meta_fifo_depth
  )
  port map (
    rst     => rx_cda_reset,
    wr_clk  => rx_clk,
    wr_dat  => rx_dat,
    wr_req  => rx_val,
    wr_ful  => OPEN,
    wrusedw => OPEN,
    rd_clk  => dp_clk,
    rd_dat  => dp_dat,
    rd_req  => '1',
    rd_emp  => OPEN,
    rdusedw => OPEN,
    rd_val  => dp_val
  );
end rtl;
