-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------
-- --------------------------------------------------------------------------
-- Author:
-- . Reinier vd Walle
-- Purpose:
-- . Collection of functions for the ring design
-- --------------------------------------------------------------------------
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

package ring_pkg is
  function nof_hops_to_source_rn(hops, this_rn, N_rn, lane_dir : natural) return integer;
  function nof_hops_to_source_rn(hops, this_rn, N_rn : std_logic_vector; lane_dir : natural) return std_logic_vector;  -- return vector length is same as hops vector length
end ring_pkg;

package body ring_pkg is
  function nof_hops_to_source_rn(hops, this_rn, N_rn, lane_dir : natural) return integer is
    variable v_source_rn : integer;
  begin
    if lane_dir > 0 then
      v_source_rn := this_rn - hops;
      if v_source_rn < 0 then  -- Cannot use MOD as N_rn is not a constant.
        v_source_rn := v_source_rn + N_rn;
      end if;

    else
      v_source_rn := this_rn + hops;
      if v_source_rn > N_rn then
        v_source_rn := v_source_rn - N_rn;
      end if;
    end if;

    if (v_source_rn < 0) or (v_source_rn > N_rn) then
      v_source_rn := -1;  -- return -1 for invalid values. This can happen if nof hops > N_rn.
    end if;
    return v_source_rn;
  end;

  function nof_hops_to_source_rn(hops, this_rn, N_rn : std_logic_vector; lane_dir : natural) return std_logic_vector is
  begin
    return TO_SVEC(nof_hops_to_source_rn(TO_UINT(hops), TO_UINT(N_rn), TO_UINT(N_rn), lane_dir), hops'length);
  end;
end ring_pkg;
