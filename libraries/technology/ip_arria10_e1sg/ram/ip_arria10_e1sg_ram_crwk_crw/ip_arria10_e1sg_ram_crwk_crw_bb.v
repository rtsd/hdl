
module ip_arria10_e1sg_ram_crwk_crw (
	data_a,
	data_b,
	address_a,
	address_b,
	wren_a,
	wren_b,
	clock_a,
	clock_b,
	rden_a,
	rden_b,
	q_a,
	q_b);	

	input	[31:0]	data_a;
	input	[7:0]	data_b;
	input	[7:0]	address_a;
	input	[9:0]	address_b;
	input		wren_a;
	input		wren_b;
	input		clock_a;
	input		clock_b;
	input		rden_a;
	input		rden_b;
	output	[31:0]	q_a;
	output	[7:0]	q_b;
endmodule
