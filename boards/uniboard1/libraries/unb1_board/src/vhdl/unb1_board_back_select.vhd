-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Loopback the bus selected by bck_id and pass on the other busses.
-- Description:
--   The tx_*_2arr have index [destination UniBoards][lanes].
--   The rx_*_2arr have index [source      UniBoards][lanes].
--   The bus with destination = source = bck_id has to remain on this UniBoard
--   and is therefore directly looped back between tx_usr and rx_usr. The other
--   busses are passed on from tx_usr to tx_phy and from rx_phy to rx_usr.
-- Remark:
-- . The tx sosi and rx siso outputs of the bus that is not used at the phy
--   side, due to the loopback, are forced to 'X'. This is not strictly
--   necessary but avoids unintentional usage and eases recognition in the
--   simulation Wave Window.
-- . The tx_siso adn rx_sosi inputs of the bus that is not used at the phy side
--   are ignored.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;

entity unb1_board_back_select is
  port (
    bck_id          : in  std_logic_vector(c_unb1_board_nof_uniboard_w - 1 downto 0);
    clk             : in  std_logic;

    -- usr side interface
    tx_usr_sosi_2arr : in  t_unb1_board_back_sosi_2arr;
    tx_usr_siso_2arr : out t_unb1_board_back_siso_2arr;

    rx_usr_sosi_2arr : out t_unb1_board_back_sosi_2arr;
    rx_usr_siso_2arr : in  t_unb1_board_back_siso_2arr;

    -- phy side interface
    tx_phy_sosi_2arr : out t_unb1_board_back_sosi_2arr;
    tx_phy_siso_2arr : in  t_unb1_board_back_siso_2arr;

    rx_phy_sosi_2arr : in  t_unb1_board_back_sosi_2arr;
    rx_phy_siso_2arr : out t_unb1_board_back_siso_2arr
  );
end unb1_board_back_select;

architecture rtl of unb1_board_back_select is
begin
  p_sel: process(tx_usr_sosi_2arr, rx_usr_siso_2arr, tx_phy_siso_2arr, rx_phy_sosi_2arr, bck_id)
  begin
    -- Default pass on all busses between usr and phy...
    tx_phy_sosi_2arr <= tx_usr_sosi_2arr;
    tx_usr_siso_2arr <= tx_phy_siso_2arr;
    rx_usr_sosi_2arr <= rx_phy_sosi_2arr;
    rx_phy_siso_2arr <= rx_usr_siso_2arr;

    -- ...except the user TX bus that matches the board ID. Loop it back as user RX:

    -- . Loop back this bus from user TX to user RX
    rx_usr_sosi_2arr(TO_UINT(bck_id)) <= tx_usr_sosi_2arr(TO_UINT(bck_id));
    tx_usr_siso_2arr(TO_UINT(bck_id)) <= rx_usr_siso_2arr(TO_UINT(bck_id));

    -- . Force not using the phy side of this bus
    tx_phy_sosi_2arr(TO_UINT(bck_id)) <= (others => c_dp_sosi_x);
    rx_phy_siso_2arr(TO_UINT(bck_id)) <= (others => c_dp_siso_x);
  end process;
end rtl;
