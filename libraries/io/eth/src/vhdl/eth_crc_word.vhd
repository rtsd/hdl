-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

-- Purpose:
--   Extract the (enumerated) CRC word from the Ethernet tail.
-- Description:
--   Dependent on snk_in.empty the CRC word is in the last tail word or in the
--   lasts two tail word.
-- Remarks:
-- . This component acts as a stream monitor, therefore it is always ready.
-- . This component can be used parallel to a stream as a branch or in series
--   in a stream whereby the src is connected directly to the snk.
-- . The snk_in CRC word can be the true CRC word or the enumerated CRC
--   derivative. This component does not distinguish.

entity eth_crc_word is
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    -- Streaming Sink
    snk_in         : in  t_dp_sosi;
    snk_out        : out t_dp_siso;

    -- Streaming Source
    src_in         : in  t_dp_siso := c_dp_siso_rdy;
    src_out        : out t_dp_sosi;

    -- CRC word
    crc_word       : out std_logic_vector(c_eth_data_w - 1 downto 0);
    crc_word_val   : out std_logic
  );
end eth_crc_word;

architecture rtl of eth_crc_word is
  signal tail_word        : std_logic_vector(c_eth_data_w - 1 downto 0);
  signal nxt_tail_word    : std_logic_vector(tail_word'range);
  signal i_crc_word       : std_logic_vector(crc_word'range) := (others => '0');
  signal nxt_crc_word     : std_logic_vector(crc_word'range);
  signal i_crc_word_val   : std_logic;
  signal nxt_crc_word_val : std_logic;
begin
  -- Direct connection between snk and src to support series connection of this component in a stream
  src_out <= snk_in;
  snk_out <= src_in;  -- default this component is always ready thanks to c_eth_stream_rdy, but the downstream component may overrule this

  crc_word     <= i_crc_word;
  crc_word_val <= i_crc_word_val;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      i_crc_word_val <= '0';
    elsif rising_edge(clk) then
      tail_word      <= nxt_tail_word;
      i_crc_word     <= nxt_crc_word;
      i_crc_word_val <= nxt_crc_word_val;
    end if;
  end process;

  -- Extract the CRC info from the 1 or 2 words from the Ethernet tail, dependent on empty
  p_extract_crc : process(tail_word, snk_in, i_crc_word_val, i_crc_word)
  begin
    -- keep the last valid data word
    nxt_tail_word <= tail_word;
    if snk_in.valid = '1' then
      nxt_tail_word <= snk_in.data(c_eth_data_w - 1 downto 0);
    end if;

    -- extract the CRC word from the tail
    nxt_crc_word_val <= i_crc_word_val;
    nxt_crc_word     <= i_crc_word;
    if snk_in.sop = '1' then
      nxt_crc_word_val <= '0';
    elsif snk_in.eop = '1' then
      nxt_crc_word_val <= '1';
      nxt_crc_word <= snk_in.data(c_eth_data_w - 1 downto 0);
      case to_integer(unsigned(snk_in.empty(c_eth_empty_w - 1 downto 0))) is
        when 1 => nxt_crc_word <= tail_word(1 * c_byte_w - 1 downto 0) & snk_in.data(4 * c_byte_w - 1 downto 1 * c_byte_w);
        when 2 => nxt_crc_word <= tail_word(2 * c_byte_w - 1 downto 0) & snk_in.data(4 * c_byte_w - 1 downto 2 * c_byte_w);
        when 3 => nxt_crc_word <= tail_word(3 * c_byte_w - 1 downto 0) & snk_in.data(4 * c_byte_w - 1 downto 3 * c_byte_w);
        when others => null;
      end case;
    end if;
  end process;
end rtl;
