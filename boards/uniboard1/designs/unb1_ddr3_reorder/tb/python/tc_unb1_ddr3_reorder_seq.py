#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for unb1_ddr3_reorder

Description:
  Tx seq --> Reorder_transpose --> Rx seq
               |         |
             WR DDR    RD DDR
Usage:
  > simulate tb_unb1_ddr3_reorder.vhd
  > python tc_unb1_ddr3_reorder_seq.py --sim --unb 0 --bn 3 --rep 2 -v 5
  to run the test again (eg. also with other verbosity):
  > python tc_unb1_ddr3_reorder_seq.py --sim --unb 0 --bn 3 --rep 2 -v 3 --read     # continue rx seq checking
  > python tc_unb1_ddr3_reorder_seq.py --sim --unb 0 --bn 3 --rep 2 -v 5 --rerun    # restart rx seq checking
"""

###############################################################################
# System imports
import test_case
import node_io
import pi_diag_block_gen
import pi_diag_tx_seq
import pi_diag_data_buffer
import pi_diag_rx_seq
import pi_io_ddr
import pi_ss_ss_wide
import pi_bsn_monitor
import mem_init_file

from tools import *
from common import *
from pi_common import *


##################################################################################################################

if __name__ == "__main__":

    # Create a test case object

    tc = test_case.Testcase('TB - ', '')
    tc.set_result('PASSED')
    tc.append_log(3, '>>>')
    tc.append_log(1, '>>> Title : Test case for the unb1_ddr3_reorder design on %s' % tc.unb_nodes_string())
    tc.append_log(3, '>>>')
    tc.append_log(3, '>>> Testcase run mode: %s' % tc.runmode)
    tc.append_log(3, '')
        
    c_ena_pre_transpose = True
    c_nof_streams   = 1
    c_data_w_ratio  = 4
    c_wr_chunksize  = 176       # must be a factor of c_data_w_ratio=4
    c_rd_chunksize  = 16        # must be a factor of c_data_w_ratio=4
    c_rd_nof_chunks = 11        # must be c_wr_chunksize = c_rd_nof_chunks*c_rd_chunksize
    c_rd_interval   = c_rd_chunksize 
    c_gapsize       = 0         # This specifies the gapsize in the sequencer and NOT in the blockgenerator 
    c_blocksize     = c_wr_chunksize + c_gapsize
    c_nof_blocks    = sel_a_b(tc.sim, 16, 800000)  # must be multiple of c_rd_chunksize
    c_pagesize      = c_nof_blocks*c_blocksize
    
    if c_ena_pre_transpose:
      c_bg_blocksize = 256
      c_bg_gapsize   = 0
    else:
      c_bg_blocksize = c_wr_chunksize
      c_bg_gapsize    = 256 - c_wr_chunksize
    
    tc.append_log(3, 'Script transpose parameters, these must match the compile parameters of the design:')
    tc.append_log(3, '   . c_ena_pre_transpose  = %s ' % c_ena_pre_transpose)
    tc.append_log(3, '   . c_nof_streams        = %d ' % c_nof_streams)
    tc.append_log(3, '   . c_data_w_ratio       = %d ' % c_data_w_ratio)
    tc.append_log(3, '   . c_wr_chunksize       = %d ' % c_wr_chunksize)
    tc.append_log(3, '   . c_rd_chunksize       = %d ' % c_rd_chunksize)
    tc.append_log(3, '   . c_rd_nof_chunks      = %d ' % c_rd_nof_chunks)
    tc.append_log(3, '   . c_rd_interval        = %d ' % c_rd_interval)
    tc.append_log(3, '   . c_gapsize            = %d ' % c_gapsize)
    tc.append_log(3, '   . c_blocksize          = %d ' % c_blocksize)
    tc.append_log(3, '   . c_nof_blocks         = %d ' % c_nof_blocks)
    tc.append_log(3, '   . c_pagesize           = %d ' % c_pagesize)
    tc.append_log(3, '')
    
    # Create access object for all nodes
    io = node_io.NodeIO(tc.nodeImages, tc.base_ip)
        
    # Create instances for the periperals
    bg = pi_diag_block_gen.PiDiagBlockGen(tc, io, nofChannels=c_nof_streams, ramSizePerChannel=2**14)
    db = pi_diag_data_buffer.PiDiagDataBuffer(tc, io, nofStreams=c_nof_streams)
    tx_seq = pi_diag_tx_seq.PiDiagTxSeq(tc, io, nof_inst=c_nof_streams)
    rx_seq = pi_diag_rx_seq.PiDiagRxSeq(tc, io, nof_inst=c_nof_streams)

    # BSN monitors
    bsn_bg = 0   # bg_sosi_arr
    bsn_wr = 1   # to_mem_sosi
    bsn_rd = 2   # from_mem_sosi
    bsn_db = 3   # db_sosi_arr
    nof_bsn = 4

    bsn = pi_bsn_monitor.PiBsnMonitor(tc, io, nofStreams=nof_bsn)
    
    # Create object for DDR register map
    ddr_status = pi_io_ddr.PiIoDdr(tc, io, nof_inst=1)
    
    # Create subandselect instance for pre-transpose.   
    ss = pi_ss_ss_wide.PiSsSsWide (tc, io, c_wr_chunksize*c_rd_chunksize, c_nof_streams) 
    
    
    ################################################################################
    # Wait for
    ################################################################################
    
    # Wait for power up (reset release)
    io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
    
    # Wait for DDR memory available    
    do_until_eq(ddr_status.read_init_done, ms_retry=3000, val=1, s_timeout=3600)

    if tc.run:
        ################################################################################
        # Write initialisations
        ################################################################################
    
        # Init pre-transpose reorder
        if c_ena_pre_transpose:
            ss_list = []
            for i in range(c_wr_chunksize):
                for j in range(c_rd_chunksize):
                   ss_list.append(i + j*c_bg_blocksize)
            ss.write_selects(ss_list)
    
        # Init rx seq based on description in reorder_sequencer.vhd
        if c_ena_pre_transpose:
            step_0 = 1
            step_1 = c_bg_blocksize  #
            step_2 = 1 + c_bg_blocksize - c_bg_blocksize * c_nof_blocks      
            step_3 = 1 + (c_rd_chunksize - c_rd_nof_chunks) * c_rd_chunksize 
        else:
            step_0 = 1
            step_1 = step_0 + c_blocksize * c_rd_interval - c_rd_chunksize  # Step due to next burst access
            step_2 = step_0 + c_blocksize * c_rd_interval - c_pagesize      # step due to page swap
            step_3 = 1
    
        tc.append_log(3, 'Rx seq step sizes:')
        tc.append_log(3, '   . step_0  = %d ' % step_0)
        tc.append_log(3, '   . step_1  = %d ' % step_1)
        tc.append_log(3, '   . step_2  = %d ' % step_2)
        tc.append_log(3, '   . step_3  = %d ' % step_3)
        tc.append_log(3, '')
        
        # use to_unsigned() to pass on negative values to MM reg
        rx_seq.write_step_3(StepSize=to_unsigned(step_3, c_word_w))
        rx_seq.write_step_2(StepSize=to_unsigned(step_2, c_word_w))
        rx_seq.write_step_1(StepSize=to_unsigned(step_1, c_word_w))
        rx_seq.write_step_0(StepSize=to_unsigned(step_0, c_word_w))
        
        # Init bg
        bg.write_block_gen_settings(samplesPerPacket=c_bg_blocksize, blocksPerSync=c_nof_blocks, gapSize=c_bg_gapsize, BSNInit=0)
    
        ################################################################################
        # Block sequence start
        ################################################################################
        rx_seq.write_enable_cntr()
        tx_seq.write_enable_cntr()
        bg.write_enable()
        
    if tc.rerun:
        # Restart Rx verification
        rx_seq.write_disable()
        io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
        rx_seq.write_enable_cntr()

    if tc.read:
        # Continue Rx verification
        pass        
    
    ################################################################################
    # Monitor results
    ################################################################################
    
    # Control defaults
    nof_mon = sel_a_b(tc.sim, 1, 2)

    # Tx sequence monitor
    for mon in range(nof_mon):
        io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
        tx_seq.read_cnt()

    # Wait for rx data
    do_until_gt(rx_seq.read_cnt, ms_retry=3000, val=0, s_timeout=3600)
    io.wait_for_time(hw_time=0.1, sim_time=(1, 'us'))   # wait some more to ensure all nodes are busy with rx data

    for rep in range(tc.repeat):
        tc.append_log(5, '')
        tc.append_log(3, '>>> Rep-%d' % rep)
        
        # Monitor
        for mon in range(nof_mon):
            io.wait_for_time(hw_time=0.01, sim_time=(1, 'us'))
            rx_seq.read_cnt()
        
        bsn.read_bsn_monitor(streamNr=bsn_db, bsnInit=0, vLevel=3)

        # Verify Rx result
        rx_seq.read_result()
        if tc.get_result() != "PASSED":
            break

        # Wait some interval until to verify Rx result again
        io.wait_for_time(hw_time=3, sim_time=(10, 'us'))
        
    
    # End
    tc.set_section_id('')
    tc.append_log(3, '')
    tc.append_log(3, '>>>')
    tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
    tc.append_log(0, '>>> Runtime=%f seconds (%f hours)' % (tc.get_run_time(),tc.get_run_time()/3600))
    tc.append_log(3, '>>>')
    
