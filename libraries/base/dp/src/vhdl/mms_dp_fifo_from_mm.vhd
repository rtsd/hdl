-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity mms_dp_fifo_from_mm is
  generic (
    g_sim           : boolean := false;
    g_wr_fifo_depth : natural
  );
  port (
    mm_rst          : in    std_logic;
    mm_clk          : in    std_logic;

    wr_sosi         : out   t_dp_sosi;

    -- MM registers
    ctrl_mosi       : in    t_mem_mosi := c_mem_mosi_rst;
    ctrl_miso       : out   t_mem_miso;

    data_mosi       : in    t_mem_mosi := c_mem_mosi_rst;
    data_miso       : out   t_mem_miso := c_mem_miso_rst;

    wr_usedw        : in    std_logic_vector(ceil_log2(g_wr_fifo_depth) - 1 downto 0)
  );
end mms_dp_fifo_from_mm;

architecture str of mms_dp_fifo_from_mm is
  signal mm_wr_usedw       : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_wr_availw      : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_wr_data        : std_logic_vector(c_word_w - 1 downto 0);
  signal mm_wr             : std_logic;
begin
  u_dp_fifo_from_mm : entity dp_lib.dp_fifo_from_mm
  generic map(
    g_fifo_size => g_wr_fifo_depth
  )
  port map (
     rst        => mm_rst,
     clk        => mm_clk,

     src_out    => wr_sosi,
     usedw      => wr_usedw,  -- used words from rd FIFO

     mm_wr      => mm_wr,
     mm_wrdata  => mm_wr_data,
     mm_usedw   => mm_wr_usedw,  -- resized to 32 bits
     mm_availw  => mm_wr_availw  -- resized to 32 bits
  );

  u_dp_fifo_from_mm_reg: entity work.dp_fifo_from_mm_reg
  port map (
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,

    sla_in            => ctrl_mosi,
    sla_out           => ctrl_miso,

    mm_wr_usedw       => mm_wr_usedw,
    mm_wr_availw      => mm_wr_availw
  );

  mm_wr_data <= data_mosi.wrdata(c_word_w - 1 downto 0);
  mm_wr      <= data_mosi.wr;
end str;
