-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker, R van der Walle, E. Kooistra
-- Purpose:
-- . Read a block of data from memory mapped (MM) location and stream it as a block of data.
-- Description:
-- . https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+filterbank
--
-- . g_*_size, g_word_w:
--   The g_*_size values are in number of g_word_w memory words, so e.g.
--   32bit words. The 32bit words are read in data blocks of g_data_size
--   words. The data block contains U = g_data_size / g_user_size
--   number of user words. U is and integer. This occurs e.g. for U = 2
--   when the data block contains:
--   - a complex value, so with real part and imag part
--   - dual polarization value, so with X polarization and Y polarization
--   The user words contain g_user_size number of g_word_w = 32bit words.
--   The output out_sosi.data width is also g_word_w bits.
--
-- . g_step_size:
--   The g_step_size = K * g_data_size, where K >= 1 to allow read out of
--   data blocks that are interleaved by a factor K.
--
-- . g_nof_data and start_pulse, start_address, mm_mosi, mm_miso, mm_done:
--   The g_nof_data is the number of data blocks that are read out via
--   mm_mosi/mm_miso per start_pulse, to form a new aggregate output block
--   with out_sosi.sop and out_sosi.eop. When the read out is done, then
--   mm_done pulses and a new start_pulse can be issued. By incrementing
--   the start_address for every start_pulse it is possible to read out K
--   interleaved sets of data blocks from the memory.
--
-- . g_reverse_word_order per g_user_size:
--   The g_reverse_word_order has no effect when FALSE, but reverses the
--   read out order of the 32bit words per user word when TRUE. The
--   g_reverse_word_order has to apply per user word and not per data block,
--   to preserve the order of the user parts (e.g. real and imag, X and Y
--   polarization) in a data block.
--
-- . sync_in is an optional sync pulse to generate a sync signal at out_sosi.
--   sync_in is not equal to start_pulse as start pulse indicates the start
--   of a packet and not the start of a sync period.
-- . For generating a bsn at out_sosi, the bsn_at_sync should contain the
--   desired bsn at sync_in pulse. If g_bsn_incr_enable then out_sosi.bsn is
--   incremented in this component at every start_pulse, else out_sosi.bsn
--   is kept at bsn_at_sync.
-- --------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.dp_stream_pkg.all;

entity dp_block_from_mm is
  generic (
    g_user_size          : natural;
    g_data_size          : natural;
    g_step_size          : natural;
    g_nof_data           : natural;
    g_word_w             : natural := c_word_w;
    g_mm_rd_latency      : natural := 1;  -- default 1 from rd_en to rd_val, use 2 to ease timing closure
    g_reverse_word_order : boolean := false;
    g_bsn_w              : natural := 1;
    g_bsn_incr_enable    : boolean := true
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    start_pulse   : in  std_logic;
    sync_in       : in  std_logic := '0';  -- Must be syncronous with start_pulse.
    bsn_at_sync   : in  std_logic_vector(g_bsn_w - 1 downto 0) := (others => '0');
    start_address : in  natural;
    mm_done       : out std_logic;  -- = out_sosi.eop
    mm_mosi       : out t_mem_mosi;
    mm_miso       : in  t_mem_miso;
    out_sosi      : out t_dp_sosi;
    out_siso      : in  t_dp_siso
  );
end dp_block_from_mm;

architecture rtl of dp_block_from_mm is
  constant c_mem_size : natural := g_step_size * g_nof_data;

  type t_reg is record
    busy             : std_logic;
    sync             : std_logic;
    sop              : std_logic;
    eop              : std_logic;
    sync_in_detected : std_logic;
    bsn              : std_logic_vector(g_bsn_w - 1 downto 0);
    user_index       : natural range 0 to g_user_size;  -- word index in g_user_size
    data_index       : natural range 0 to g_data_size;  -- default word order index in g_data_size
    word_index       : natural range 0 to g_data_size;  -- default or reversed word order index in g_data_size
    step_address     : natural range 0 to c_mem_size;  -- step address offset
  end record;

  constant c_reg_rst : t_reg := ('0', '0', '0', '0', '0', (others => '0'), 0, 0, 0, 0);

  signal r     : t_reg;
  signal nxt_r : t_reg;

  signal last_mm_address : natural := 0;
  signal mm_address      : natural := 0;
  signal mm_address_rev  : natural := 0;

  signal r_sop_p  : std_logic;
  signal r_eop_p  : std_logic;
  signal r_sync_p : std_logic;
  signal r_bsn_p  : std_logic_vector(g_bsn_w - 1 downto 0);
  signal out_sop  : std_logic;
  signal out_eop  : std_logic;
  signal out_sync : std_logic;
  signal out_bsn  : std_logic_vector(g_bsn_w - 1 downto 0);
begin
  last_mm_address <= g_step_size * (g_nof_data - 1) + g_data_size + start_address - 1;
  mm_address      <= start_address + r.data_index + r.step_address;  -- default word order per g_user_size
  mm_address_rev  <= start_address + r.word_index + r.step_address;  -- reverse word order per g_user_size

  -- Take care of g_mm_rd_latency for out_sosi.sop and out_sosi.eop
  r_sop_p  <= r.sop  when rising_edge(clk);
  r_eop_p  <= r.eop  when rising_edge(clk);
  r_sync_p <= r.sync when rising_edge(clk);
  r_bsn_p  <= r.bsn  when rising_edge(clk);
  out_sop  <= r.sop  when g_mm_rd_latency = 1 else r_sop_p;
  out_eop  <= r.eop  when g_mm_rd_latency = 1 else r_eop_p;
  out_sync <= r.sync when g_mm_rd_latency = 1 else r_sync_p;
  out_bsn  <= r.bsn  when g_mm_rd_latency = 1 else r_bsn_p;

  p_out_sosi : process(mm_miso, out_sop, out_eop, out_sync, out_bsn)
  begin
    out_sosi       <= c_dp_sosi_rst;  -- To avoid Modelsim warnings on conversion to integer from unused fields.
    out_sosi.data  <= RESIZE_DP_DATA(mm_miso.rddata(g_word_w - 1 downto 0));
    out_sosi.valid <= mm_miso.rdval;  -- read latency from mm_mosi.rd to mm_miso.rdval is 1, so same as the ready latency (RL = 1)
    out_sosi.sop   <= out_sop;  -- read latency from mm_mosi.rd to mm_miso.rdval is 1, so r.sop can be used for output sop
    out_sosi.eop   <= out_eop;  -- read latency from mm_mosi.rd to mm_miso.rdval is 1, so r.eop can be used for output eop
    out_sosi.sync  <= out_sync;  -- read latency from mm_mosi.rd to mm_miso.rdval is 1, so r.sync can be used for output sync
    out_sosi.bsn   <= RESIZE_DP_BSN(out_bsn);  -- read latency from mm_mosi.rd to mm_miso.rdval is 1, so r.bsn can be used for output bsn
  end process;

  mm_done <= out_eop;

  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(clk) then
      r <= nxt_r;
    end if;
  end process;

  p_comb : process(r, start_pulse, sync_in, bsn_at_sync, out_siso, mm_address, mm_address_rev, last_mm_address)
    variable v         : t_reg;
    variable v_base    : natural;
    variable v_reverse : natural;
  begin
    v := r;
    v.sop  := '0';
    v.eop  := '0';
    v.sync := '0';

    -- Use default c_mem_mosi_rst to avoid Warning: (vsim-8684) No drivers exist on out port .wr, .wrdata
    mm_mosi <= c_mem_mosi_rst;

    if r.busy = '0' and start_pulse = '1' then
      -- initiate next output block
      v.busy := '1';
      if sync_in = '1' then
        v.sync_in_detected := '1';
        v.bsn := bsn_at_sync;
      end if;
    end if;

    -- use v.busy, instead of r.busy, to allow start_pulse at mm_done, to
    -- support zero gaps between output blocks
    if v.busy = '1' then
      if out_siso.ready = '1' then
        -- Continue with output block
        mm_mosi.rd <= '1';

        -- Calculate data_index for default word order
        if r.data_index < g_data_size - 1 then
          v.data_index := r.data_index + 1;
        else
          v.data_index := 0;
          v.step_address := r.step_address + g_step_size;
        end if;

        -- maintain user_index within g_user_size
        if r.user_index < g_user_size - 1 then
          v.user_index := r.user_index + 1;
        else
          v.user_index := 0;
        end if;

        -- check start of output block
        if r.data_index = 0 and r.step_address = 0 then
          v.sop := '1';
          if v.sync_in_detected = '1' then
            v.sync := '1';
            v.sync_in_detected := '0';
          elsif g_bsn_incr_enable = true then
            v.bsn := INCR_UVEC(r.bsn, 1);
          end if;
        end if;

        -- check end of output block
        if mm_address >= last_mm_address then
          v.eop := '1';
          -- prepare for next output block at next start_pulse
          v.busy := '0';
          v.step_address := 0;
          v.data_index := 0;
          v.user_index := 0;
        end if;
      end if;
    end if;

    -- Default word order
    v.word_index := v.data_index;
    mm_mosi.address <= TO_MEM_ADDRESS(mm_address);

    -- Account for g_reverse_word_order
    if g_reverse_word_order then
      v_base    := v.data_index - v.user_index;  -- data_index of first word in g_user_size
      v_reverse := g_user_size - 1 - v.user_index;  -- reverse word index in g_user_size
      v.word_index := v_base + v_reverse;

      mm_mosi.address <= TO_MEM_ADDRESS(mm_address_rev);
    end if;

    nxt_r <= v;
  end process;
end rtl;
