-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify DP SOSI to DP packets to DP SOSI
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_packet is
  generic (
    g_data_w                 : natural := 16;
    g_in_en                  : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_out_ready              : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_nof_repeat             : natural := 2  -- for constant active stream control using 1 is sufficient, use > 1 to verify longer with random stimuli
  );
end tb_dp_packet;

architecture tb of tb_dp_packet is
  constant c_rl                       : natural := 1;
  constant c_data_init                : integer := 0;
  constant c_data_w                   : integer := sel_a_b(g_data_w < 30, g_data_w, 30);  -- used to avoid INTEGER range error for 2**31 and 2**32
  constant c_data_mod                 : integer := 2**c_data_w;  -- used to avoid TO_UVEC warning for smaller g_data_w : "NUMERIC_STD.TO_UNSIGNED: vector truncated"
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0877665544332211";
  constant c_err_init                 : natural := 247;
  constant c_err_w                    : integer := sel_a_b(c_dp_stream_error_w < 30, c_dp_stream_error_w, 30);  -- used to avoid INTEGER range error for 2**31 and 2**32
  constant c_err_mod                  : integer := 2**c_err_w;  -- used to avoid TO_UVEC warning for smaller c_dp_stream_error_w : "NUMERIC_STD.TO_UNSIGNED: vector truncated"
  constant c_channel_init             : integer := 0;  -- fixed
  constant c_channel_w                : integer := sel_a_b(c_dp_stream_channel_w < 30, c_dp_stream_channel_w, 30);  -- used to avoid INTEGER range error for 2**31 and 2**32
  constant c_channel_mod              : integer := 2**c_channel_w;  -- used to avoid TO_UVEC warning for smaller c_dp_stream_channel_w : "NUMERIC_STD.TO_UNSIGNED: vector truncated"
  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;
  constant c_sync_period_w            : natural := 3;
  constant c_sync_period              : natural := 2**c_sync_period_w;
  constant c_sync_offset              : natural := 4;

  -- Default use these:
  constant c_nof_ch                   : natural := 10;
  constant c_len_arr                  : t_natural_arr(0 to c_nof_ch - 1) := (1, 1, 1, 2, 3, 5, 10, 17, 32, 100);
  -- Use these to verify the entire channel field width.
--   CONSTANT c_nof_ch                   : NATURAL := c_channel_mod + 10;
--   CONSTANT c_len_arr                  : t_natural_arr(0 TO c_nof_ch-1) := array_init(1, c_nof_ch, 0);

  signal tb_end            : std_logic := '0';
  signal clk               : std_logic := '1';
  signal rst               : std_logic := '1';

  signal random_0          : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1          : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0           : std_logic;
  signal pulse_1           : std_logic;
  signal pulse_en          : std_logic := '1';

  signal in_en             : std_logic := '1';
  signal in_siso           : t_dp_siso;
  signal in_sosi           : t_dp_sosi;
  signal in_data           : std_logic_vector(g_data_w - 1 downto 0);
  signal in_val            : std_logic;
  signal in_sop            : std_logic;
  signal in_eop            : std_logic;
  signal in_err            : std_logic_vector(c_dp_stream_error_w - 1 downto 0);
  signal in_channel        : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal in_sync           : std_logic;
  signal in_bsn            : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);

  signal enc_siso          : t_dp_siso := c_dp_siso_rdy;
  signal enc_sosi          : t_dp_sosi;

  signal prev_pkt_ready    : std_logic_vector(0 to c_rl);
  signal pkt_siso          : t_dp_siso := c_dp_siso_rdy;
  signal pkt_sosi          : t_dp_sosi;
  signal pkt_data          : std_logic_vector(g_data_w - 1 downto 0);
  signal pkt_val           : std_logic;
  signal pkt_sop           : std_logic;
  signal pkt_eop           : std_logic;
  signal pkt_sync          : std_logic;

  signal prev_rx_ready     : std_logic_vector(0 to c_rl);
  signal rx_siso           : t_dp_siso := c_dp_siso_rdy;
  signal rx_sosi           : t_dp_sosi;
  signal prev_rx_data      : std_logic_vector(g_data_w - 1 downto 0) := TO_SVEC(c_data_init - 1, g_data_w);
  signal rx_data           : std_logic_vector(g_data_w - 1 downto 0);
  signal rx_val            : std_logic;
  signal rx_sop            : std_logic;
  signal rx_eop            : std_logic;
  signal rx_gap            : std_logic := '1';
  signal rx_err            : std_logic_vector(c_dp_stream_error_w - 1 downto 0);
  signal rx_channel        : std_logic_vector(c_dp_stream_channel_w - 1 downto 0);
  signal rx_sync           : std_logic;
  signal rx_bsn            : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal prev_rx_bsn       : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := INCR_UVEC(c_bsn_init, -1);
  signal prev_rx_channel   : std_logic_vector(c_dp_stream_channel_w - 1 downto 0) := TO_SVEC(c_channel_init - 1, c_dp_stream_channel_w);
  signal prev_rx_err       : std_logic_vector(c_dp_stream_error_w - 1 downto 0) := TO_SVEC(c_err_init - 1, c_dp_stream_error_w);
  signal hold_rx_sop       : std_logic;

  signal verify_en         : std_logic;
  signal verify_done       : std_logic := '0';

  signal expected_rx_data  : std_logic_vector(g_data_w - 1 downto 0);
  signal expected_rx_bsn   : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal expected_rx_err   : std_logic_vector(c_dp_stream_error_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  in_en         <= '1'                     when g_in_en = e_active      else
                   random_0(random_0'high) when g_in_en = e_random      else
                   pulse_0                 when g_in_en = e_pulse;

  rx_siso.ready <= '1'                     when g_out_ready = e_active  else
                   random_1(random_1'high) when g_out_ready = e_random  else
                   pulse_1                 when g_out_ready = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli : process
    variable v_sync      : std_logic := '0';
    variable v_bsn       : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := c_bsn_init;
    variable v_channel   : natural := c_channel_init;
    variable v_data      : natural := c_data_init;
    variable v_err       : natural := c_err_init;
  begin
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Stress test by repeating all channels
    for J in 0 to g_nof_repeat - 1 loop
      -- No gaps between the blocks
      for I in 0 to c_nof_ch - 1 loop
        proc_dp_gen_block_data(c_rl, true, g_data_w, g_data_w, v_data, 0, 0, c_len_arr(I), v_channel, v_err, v_sync, v_bsn, clk, in_en, in_siso, in_sosi);
        v_sync      := sel_a_b(unsigned(v_bsn(c_sync_period_w - 1 downto 0)) = c_sync_offset - 1, '1', '0');  -- sync pulse every at 2**c_sync_period_w block
        v_bsn       := INCR_UVEC(v_bsn, 1);
        v_channel   := ((v_channel + 1) mod c_nof_ch) mod c_channel_mod;
        v_data      := (v_data + c_len_arr(I)) mod c_data_mod;
        v_err       := (v_err + 1) mod c_err_mod;
      end loop;
      -- With gaps between the blocks
      for I in 0 to c_nof_ch - 1 loop
        proc_dp_gen_block_data(c_rl, true, g_data_w, g_data_w, v_data, 0, 0, c_len_arr(I), v_channel, v_err, v_sync, v_bsn, clk, in_en, in_siso, in_sosi);
        proc_common_wait_some_cycles(clk, 7);
        v_sync      := sel_a_b(unsigned(v_bsn(c_sync_period_w - 1 downto 0)) = c_sync_offset - 1, '1', '0');
        v_bsn       := INCR_UVEC(v_bsn, 1);
        v_channel   := ((v_channel + 1) mod c_nof_ch) mod c_channel_mod;
        v_data      := (v_data + c_len_arr(I)) mod c_data_mod;
        v_err       := (v_err + 1) mod c_err_mod;
      end loop;
    end loop;

    -- Signal end of stimuli
    expected_rx_bsn  <= INCR_UVEC(v_bsn, -1);
    expected_rx_data <= TO_UVEC(sel_a_b(v_data > 0, v_data, c_data_mod) - 1, g_data_w);  -- account for data wrap, which is quite likely for small g_data_w
    expected_rx_err  <= TO_UVEC(v_err - 1, expected_rx_err'length);
    proc_common_wait_some_cycles(clk, 100);  -- depends on stream control
    verify_done <= '1';
    proc_common_wait_some_cycles(clk, 1);
    verify_done <= '0';
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  -- Map to slv to ease monitoring in wave window
  in_data    <= in_sosi.data(in_data'range);
  in_val     <= in_sosi.valid;
  in_sop     <= in_sosi.sop;
  in_eop     <= in_sosi.eop;
  in_err     <= in_sosi.err(in_err'range);
  in_channel <= in_sosi.channel(in_channel'range);
  in_sync    <= in_sosi.sync;
  in_bsn     <= in_sosi.bsn(in_bsn'range);

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  verify_en <= '1';

  proc_dp_verify_data("rx_sosi.data", c_rl, clk, verify_en, rx_siso.ready, rx_val, rx_data, prev_rx_data);  -- Verify that the output is incrementing data, like the input stimuli
  proc_dp_verify_value(e_equal, clk, verify_done, expected_rx_data, prev_rx_data);  -- Verify that the stimuli have been applied at all

  proc_dp_verify_data("rx_sosi.bsn", c_rl, clk, verify_en, rx_siso.ready, rx_sop, rx_bsn, prev_rx_bsn);  -- Verify that the output is incrementing bsn, like the input stimuli
  proc_dp_verify_value(e_equal, clk, verify_done, expected_rx_bsn, prev_rx_bsn);  -- Verify that the stimuli have been applied at all

  proc_dp_verify_data("rx_sosi.err", c_rl, clk, verify_en, rx_siso.ready, rx_eop, rx_err, prev_rx_err);  -- Verify that the output is incrementing err, like the input stimuli
  proc_dp_verify_value(e_equal, clk, verify_done, expected_rx_err, prev_rx_err);  -- Verify that the stimuli have been applied at all

  proc_dp_verify_data("rx_sosi.channel", c_rl, to_unsigned(c_nof_ch - 1, 32), clk, verify_en, rx_siso.ready, rx_sop, rx_channel, prev_rx_channel);  -- Verify that the output is incrementing channel MOD c_nof_ch, like the input stimuli

  proc_dp_verify_valid(c_rl, clk, verify_en, pkt_siso.ready, prev_pkt_ready, pkt_val);  -- Verify that the encoder output valid fits with the output ready latency
  proc_dp_verify_valid(c_rl, clk, verify_en, rx_siso.ready, prev_rx_ready, rx_val);  -- Verify that the output valid fits with the output ready latency
  proc_dp_verify_gap_invalid(clk, rx_val, rx_sop, rx_eop, rx_gap);  -- Verify that the output valid is low between blocks
  proc_dp_verify_sop_and_eop(clk, rx_val, rx_sop, rx_eop, hold_rx_sop);  -- Verify that sop and eop come in pairs

  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_en, rx_sync, rx_sop, rx_bsn);  -- Verify that the output sync occurs when expected

  ------------------------------------------------------------------------------
  -- DP SOSI ==> DP PACKET
  ------------------------------------------------------------------------------

  -- Encode the data block to a DP packet
  u_dp_packet_enc : entity work.dp_packet_enc
  generic map (
    g_data_w => g_data_w
  )
  port map (
    rst       => rst,
    clk       => clk,
    -- ST sinks
    snk_out   => in_siso,
    snk_in    => in_sosi,
    -- ST source
    src_in    => enc_siso,
    src_out   => enc_sosi
  );

  -- Restrict to link g_data_w
  enc_siso <= pkt_siso;
  p_link : process(enc_sosi)
  begin
    pkt_sosi      <= enc_sosi;
    pkt_sosi.data <= RESIZE_DP_DATA(enc_sosi.data(g_data_w - 1 downto 0));
  end process;

  -- Map to slv to ease monitoring in wave window
  pkt_data <= pkt_sosi.data(pkt_data'range);
  pkt_val  <= pkt_sosi.valid;
  pkt_sop  <= pkt_sosi.sop;
  pkt_eop  <= pkt_sosi.eop;
  pkt_sync <= pkt_sosi.sync;

  ------------------------------------------------------------------------------
  -- DP PACKET ==> DP SOSI
  ------------------------------------------------------------------------------

  -- Decode the DP packet into a data block
  u_dp_packet_dec : entity work.dp_packet_dec
  generic map (
    g_data_w => g_data_w
  )
  port map (
    rst       => rst,
    clk       => clk,
    -- ST sinks
    snk_out   => pkt_siso,
    snk_in    => pkt_sosi,
    -- ST source
    src_in    => rx_siso,
    src_out   => rx_sosi
  );

  -- Map to slv to ease monitoring in wave window
  rx_data    <= rx_sosi.data(rx_data'range);
  rx_val     <= rx_sosi.valid;
  rx_sop     <= rx_sosi.sop;
  rx_eop     <= rx_sosi.eop;
  rx_err     <= rx_sosi.err(rx_err'range);
  rx_channel <= rx_sosi.channel(rx_channel'range);
  rx_sync    <= rx_sosi.sync;
  rx_bsn     <= rx_sosi.bsn(rx_bsn'range);
end tb;
