--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose: Encode sosi.channel low bit into the high part of the CHAN field of
--          a DP packet.
-- Description:
--   The snk_in data must be DP packet data. The [g_channel_lo-1:0] bits of the
--   sosi.channel field get placed into the [high-1:high-g_channel_lo] bits
--   of the CHAN field. The [high] bit of the CHAN field is not used because it
--   is reserved for future use.
-- Remark:
-- . The dp_packet_enc_channel_lo.vhd is verified in tb_dp_distribute.vhd.
-- . The DP packet CHAN field occurs at the sop. By placing the channel_lo bits
--   in the high part of the CHAN field this dp_packet_enc_channel_lo can
--   combinatorially modifiy the CHAN data at the sop. For small g_data_w the
--   CHAN field can cover multiple data words, therefore using the low part of
--   the CHAN field would imply shifting in the g_channel_lo bits and would
--   require more logic including registers to modify the entire CHAN field.
-- . The snk_in channel is passed on unmodified to the src_out.channel, because
--   DP packets typically do not use the channel field, so the g_channel_lo
--   bits are the only valid bits in the channel field and therefore they may
--   as wel remain, rather then being shifted out.
-- . Pre-conditions:
--   - g_channel_lo+1 <= g_data_w
--   - g_channel_lo+1 <= c_dp_packet_channel_w = 16
--   - g_channel_lo+1 <= c_dp_packet_channel_w - currently used CHAN width

entity dp_packet_enc_channel_lo is
  generic (
    g_data_w      : natural := 32;
    g_channel_lo  : natural := 0
  );
  port (
    -- ST sinks
    snk_out   : out t_dp_siso;
    snk_in    : in  t_dp_sosi;
    -- ST source
    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end dp_packet_enc_channel_lo;

architecture rtl of dp_packet_enc_channel_lo is
begin
  assert g_channel_lo + 1 <= g_data_w
    report "dp_packet_enc_channel_lo : g_channel_lo does not fit in g_data_w"
    severity FAILURE;

  -- Pass on the flow control
  snk_out <= src_in;

  -- Pass on the DP packet and insert the g_channel_lo bits into the CHAN field
  p_sop : process (snk_in)
  begin
    src_out <= snk_in;
    if g_channel_lo > 0 then
      if snk_in.sop = '1' then
        src_out.data(g_data_w - 2 downto g_data_w - 1 - g_channel_lo) <= snk_in.channel(g_channel_lo - 1 downto 0);
      end if;
    end if;
  end process;
end rtl;
