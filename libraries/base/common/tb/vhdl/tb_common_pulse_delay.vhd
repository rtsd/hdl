-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Feed different pulse_delay control settings to common_pulse_delay,
--   verify correct pulse_out
-- Usage:
-- . as 8
-- . run -a
-- . if no failure messages are printed, TB ran OK.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_common_pulse_delay is
end tb_common_pulse_delay;

architecture tb of tb_common_pulse_delay is
  -----------------------------------------------------------------------------
  -- common_pulse_delay parameters
  -----------------------------------------------------------------------------
  constant c_pulse_delay_max   : natural := 100;
  constant c_pulse_delay_max_w : natural := ceil_log2(c_pulse_delay_max + 1);

  -----------------------------------------------------------------------------
  -- Clock & reset
  -----------------------------------------------------------------------------
  constant c_clk_period : time := 5 ns;

  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------
  signal pulse_in    : std_logic;
  signal first_pulse : std_logic;
  signal pulse_delay : std_logic_vector(c_pulse_delay_max_w - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Verification
  ----------------------------------------------------------------------------
  constant c_nof_pulses             : natural := pow2(ceil_log2(c_pulse_delay_max)) - 1;
  constant c_init_cycles            : natural := 12;  -- Number of clock cycles for init (startup after reset)
  constant c_total_delay_cycles     : natural := (c_nof_pulses * (c_nof_pulses + 1)) / 2;  -- sum of functional delays 1..c_nof_pulses
  constant c_total_reg_delay_cycles : natural := c_nof_pulses;  -- 1 extra delay cycles is introduced for every pulse due to register
  constant c_total_cycles           : natural := c_init_cycles + c_total_delay_cycles + c_total_reg_delay_cycles;

  signal tb_end                     : std_logic := '0';
  signal pulse_delay_reg            : std_logic_vector(c_pulse_delay_max_w - 1 downto 0);
  signal pulse_out                  : std_logic;
  signal pulse_count                : natural;
  signal pulse_delay_count          : natural;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  signal nxt_pulse_in          : std_logic;
  signal nxt_pulse_delay       : std_logic_vector(c_pulse_delay_max_w - 1 downto 0);
  signal nxt_pulse_delay_reg   : std_logic_vector(c_pulse_delay_max_w - 1 downto 0);
  signal nxt_pulse_count       : natural;
  signal nxt_pulse_delay_count : natural;
begin
  -----------------------------------------------------------------------------
  -- Clock & reset
  -----------------------------------------------------------------------------
  rst <= '1', '0' after 7 * c_clk_period;
  clk <= not clk or tb_end after c_clk_period / 2;

  -----------------------------------------------------------------------------
  -- Stimuli
  -----------------------------------------------------------------------------
  -- Create the first pulse here, then feed pulse_out to pulse
  first_pulse <= '0', '1' after 10 * c_clk_period, '0' after 11 * c_clk_period;
  nxt_pulse_in <= first_pulse or pulse_out;
  -- Make next pulse delay 1 cycle longer
  nxt_pulse_delay <= INCR_UVEC(pulse_delay, 1) when pulse_in = '1' else pulse_delay;

  -----------------------------------------------------------------------------
  -- common_pulse_delay
  -----------------------------------------------------------------------------
  u_common_pulse_delay : entity work.common_pulse_delay
  generic map (
    g_pulse_delay_max => c_pulse_delay_max,
    g_register_out    => true
  )
  port map (
    clk         => clk,
    rst         => rst,

    pulse_in    => pulse_in,
    pulse_delay => pulse_delay,
    pulse_out   => pulse_out
  );

  -----------------------------------------------------------------------------
  -- Verification
  -- . Check total number of output pulses
  -- . Check input->output pulse delay
  -----------------------------------------------------------------------------
  tb_end <= '0', '1' after c_total_cycles * c_clk_period;

  -- Keep counts
  nxt_pulse_count <= pulse_count + 1 when pulse_out = '1' else pulse_count;
  nxt_pulse_delay_count <= 0 when pulse_in = '1' else pulse_delay_count + 1;

  -- Store the delay setting for comparison to actual pulse delay
  nxt_pulse_delay_reg <= pulse_delay when pulse_in = '1' else pulse_delay_reg;

  -- Perform checks
  p_verify: process(pulse_out, tb_end)
  begin
    -- input/output pulse delay should match setting
    if pulse_out = '1' and pulse_delay_count /= TO_UINT(pulse_delay_reg) then
      report "I/O pulse delay does not match delay setting!"
        severity FAILURE;
    end if;
    -- Total number of pulses should be the maximum allowed by common_pulse_delay's interal counter width
    if tb_end = '1' and pulse_count /= c_nof_pulses then
      report "Incorrect total number of pulses!"
        severity FAILURE;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  p_clk : process (rst, clk)
  begin
    if rst = '1' then
      pulse_delay       <= (others => '0');
      pulse_delay_reg   <= (others => '0');
      pulse_in          <= '0';
      pulse_count       <= 0;
      pulse_delay_count <= 0;
    elsif rising_edge(clk) then
      pulse_delay       <= nxt_pulse_delay;
      pulse_delay_reg   <= nxt_pulse_delay_reg;
      pulse_in          <= nxt_pulse_in;
      pulse_count       <= nxt_pulse_count;
      pulse_delay_count <= nxt_pulse_delay_count;
    end if;
  end process;
end tb;
