-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for common_operation_tree operations
-- Usage:
--   > as 1
--   > run -all
--   . Observe expanded in_data_arr_p and expected = result with radix unsigned
--     or signed in Wave Window
-- . The p_verify makes the tb self checking and asserts when the results are
--   not equal

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_lfsr_sequences_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_operation_tree is
  generic (
    g_operation      : string  := "MAX";  -- supported operations "MAX", "MIN"
    g_representation : string  := "UNSIGNED";
    g_pipeline       : natural := 1;  -- amount of pipelining per stage
    g_pipeline_mod   : natural := 1;  -- only pipeline the stage output by g_pipeline when the stage number MOD g_pipeline_mod = 0
    g_nof_inputs     : natural := 5  -- >= 1
  );
end tb_common_operation_tree;

architecture tb of tb_common_operation_tree is
  constant clk_period      : time := 10 ns;

  constant c_dat_w         : natural := 8;
  constant c_data_vec_w    : natural := g_nof_inputs * c_dat_w;
  constant c_nof_stages    : natural := ceil_log2(g_nof_inputs);

  constant c_smax          : integer :=  2**(c_dat_w - 1) - 1;
  constant c_smin          : integer := -2**(c_dat_w - 1);
  constant c_umax          : integer :=  2**c_dat_w - 1;
  constant c_umin          : integer :=  0;

  constant c_pipeline_tree : natural := g_pipeline * c_nof_stages / g_pipeline_mod;

  type t_data_arr is array (integer range <>) of std_logic_vector(c_dat_w - 1 downto 0);

  -- Use random data values for the g_nof_inputs time in the data_vec
  function func_data_vec(init : integer) return std_logic_vector is
    variable v_data_vec : std_logic_vector(c_data_vec_w - 1 downto 0);
    variable v_in       : std_logic_vector(c_dat_w - 1 downto 0);
  begin
    v_in := TO_SVEC(init, c_dat_w);
    for I in 0 to g_nof_inputs - 1 loop
      v_data_vec((I + 1) * c_dat_w - 1 downto I * c_dat_w) := v_in;
      v_in := func_common_random(v_in);
    end loop;
    return v_data_vec;
  end;

  -- Calculate the expected result of the operation on the data in the data_vec
  function func_result(operation, representation : string; data_vec : std_logic_vector) return std_logic_vector is
    variable v_in     : std_logic_vector(c_dat_w - 1 downto 0);
    variable v_result : integer := 0;
  begin
    -- Init v_result
    if representation = "SIGNED" then
      if operation = "MIN" then v_result := c_smax; end if;
      if operation = "MAX" then v_result := c_smin; end if;
    else
      if operation = "MIN" then v_result := c_umax; end if;
      if operation = "MAX" then v_result := c_umin; end if;
    end if;
    -- Find v_result
    for I in 0 to g_nof_inputs - 1 loop
      v_in := data_vec((I + 1) * c_dat_w - 1 downto I * c_dat_w);
      if representation = "SIGNED" then
        if operation = "MIN" then if v_result > signed(v_in) then v_result := TO_SINT(v_in); end if; end if;
        if operation = "MAX" then if v_result < signed(v_in) then v_result := TO_SINT(v_in); end if; end if;
      else
        if operation = "MIN" then if v_result > unsigned(v_in) then v_result := TO_UINT(v_in); end if; end if;
        if operation = "MAX" then if v_result < unsigned(v_in) then v_result := TO_UINT(v_in); end if; end if;
      end if;
    end loop;
    -- Return v_result
    if representation = "SIGNED" then
      return TO_SVEC(v_result, c_dat_w);
    else
      return TO_UVEC(v_result, c_dat_w);
    end if;
  end;

  signal tb_end             : std_logic := '0';
  signal rst                : std_logic;
  signal clk                : std_logic := '1';

  signal expected_comb      : std_logic_vector(c_dat_w - 1 downto 0);  -- expected combinatorial result

  signal in_val             : std_logic;
  signal in_en_vec          : std_logic_vector(g_nof_inputs - 1 downto 0) := (others => '1');
  signal in_data_vec        : std_logic_vector(c_data_vec_w - 1 downto 0);
  signal in_data_vec_p      : std_logic_vector(c_data_vec_w - 1 downto 0);
  signal in_data_arr_p      : t_data_arr(0 to g_nof_inputs - 1);

  signal result             : std_logic_vector(c_dat_w - 1 downto 0);  -- dut result
  signal expected           : std_logic_vector(c_dat_w - 1 downto 0);  -- expected pipelined result
  signal expected_val       : std_logic;
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 3;

  p_stimuli : process
  begin
    in_data_vec <= (others => '0');
    in_val <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Apply equal dat value inputs
    in_val <= '1';
    for I in c_smin to c_smax loop
      in_data_vec <= func_data_vec(I);
      proc_common_wait_some_cycles(clk, 1);
    end loop;
    in_data_vec <= (others => '0');
    proc_common_wait_some_cycles(clk, 50);

    tb_end <= '1';
    wait;
  end process;

  -- For easier manual analysis in the wave window:
  -- . Pipeline the in_data_vec to align with the result
  -- . Map the concatenated dat in in_data_vec into an in_data_arr_p array
  u_data_vec_p : entity work.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline_tree,
    g_reset_value    => 0,
    g_in_dat_w       => c_data_vec_w,
    g_out_dat_w      => c_data_vec_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_data_vec,
    out_dat => in_data_vec_p
  );

  p_data_arr : process(in_data_vec_p)
  begin
    for I in 0 to g_nof_inputs - 1 loop
      in_data_arr_p(I) <= in_data_vec_p((I + 1) * c_dat_w - 1 downto I * c_dat_w);
    end loop;
  end process;

  expected_comb <= func_result(g_operation, g_representation, in_data_vec);

  u_result : entity work.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline_tree,
    g_reset_value    => 0,
    g_in_dat_w       => c_dat_w,
    g_out_dat_w      => c_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => expected_comb,
    out_dat => expected
  );

  u_expected_val : entity work.common_pipeline_sl
  generic map (
    g_pipeline       => c_pipeline_tree,
    g_reset_value    => 0
  )
  port map (
    rst     => rst,
    clk     => clk,
    in_dat  => in_val,
    out_dat => expected_val
  );

  dut : entity work.common_operation_tree
  generic map (
    g_operation      => g_operation,
    g_representation => g_representation,
    g_pipeline       => g_pipeline,
    g_pipeline_mod   => g_pipeline_mod,
    g_nof_inputs     => g_nof_inputs,
    g_dat_w          => c_dat_w
  )
  port map (
    clk         => clk,
    in_data_vec => in_data_vec,
    in_en_vec   => in_en_vec,
    result      => result
  );

  p_verify : process(rst, clk)
  begin
    if rst = '0' then
      if rising_edge(clk) then
        assert result = expected
          report "Error: wrong result"
          severity ERROR;
      end if;
    end if;
  end process;
end tb;
