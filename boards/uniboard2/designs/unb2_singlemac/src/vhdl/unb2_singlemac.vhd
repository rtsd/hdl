-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, unb_common_lib;
use unb_common_lib.unb_common_pkg.all;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity unb2_singlemac is
  port (
    -- GENERAL
    CLK                    : in    std_logic;  -- External system clock
    PPS                    : in    std_logic;  -- External system sync
    WDI                    : out   std_logic;  -- Watchdog Clear
    INTA                   : inout std_logic;  -- FPGA interconnect line
    INTB                   : inout std_logic;  -- FPGA interconnect line

    -- 1GbE Control Interfaces
    ETH_CLK                : in std_logic;
    ETH_SGIN               : in std_logic_vector(1 downto 0);
    ETH_SGOUT              : out std_logic_vector(1 downto 0);

    -- Transceiver clocks
    SA_CLK                 : in    std_logic;  -- SerDes reference clock front
    SB_CLK                 : in    std_logic;  -- SerDes reference clock back
    BCK_REF_CLK            : in    std_logic;  -- SerDes reference clock back

    -- SO-DIMM DDR4 Memory Bank i2c (common)
--    MB_SCL                 : inout std_logic;
--    MB_SDA                 : inout std_logic;
    -- SO-DIMM DDR4 Memory Bank I
--    MB_I_RZQ       : in   STD_LOGIC;
--    MB_I_REF_CLK   : in    STD_LOGIC; -- External reference clock
--    MB_I_A : out std_logic_vector (13 downto 0);
--    MB_I_ACT_N : out std_logic_vector(0 downto 0);
--    MB_I_BA : out std_logic_vector (1 downto 0);
--    MB_I_BG : out std_logic_vector (1 downto 0);
--    MB_I_CAS_A15 : out std_logic;
--    MB_I_CB : inout std_logic_vector (7 downto 0);
--    MB_I_CK : out std_logic_vector (1 downto 0);
--    MB_I_CK_n : out std_logic_vector (1 downto 0);
--    MB_I_CKE : out std_logic_vector (1 downto 0);
--    MB_I_CS : out std_logic_vector (1 downto 0);
--    MB_I_DM : inout std_logic_vector (8 downto 0);
--    MB_I_DQ : inout std_logic_vector (63 downto 0);
--    MB_I_DQS : inout std_logic_vector (8 downto 0);
--    MB_I_DQS_n : inout std_logic_vector (8 downto 0);
--    MB_I_ODT : out std_logic_vector (1 downto 0);
--    MB_I_PARITY : out std_logic_vector(0 downto 0);
--    MB_I_RAS_A16 : out std_logic;
--    MB_I_WE_A14 : out std_logic;
--    MB_I_RESET_N           : out std_logic_vector(0 downto 0);
--    MB_I_ALERT_N           : in std_logic_vector(0 downto 0);
    -- SO-DIMM DDR4 Memory Bank II
--    MB_II_RZQ    : in   STD_LOGIC;
--    MB_II_REF_CLK : in    STD_LOGIC; -- External reference clock
--    MB_II_A : out std_logic_vector (13 downto 0);
--    MB_II_ACT_N : out std_logic_vector(0 downto 0);
--    MB_II_BA : out std_logic_vector (1 downto 0);
--    MB_II_BG : out std_logic_vector (1 downto 0);
--    MB_II_CAS_A15 : out std_logic;
--    MB_II_CB : inout std_logic_vector (7 downto 0);
--    MB_II_CK : out std_logic_vector (1 downto 0);
--    MB_II_CK_n : out std_logic_vector (1 downto 0);
--    MB_II_CKE : out std_logic_vector (1 downto 0);
--    MB_II_CS : out std_logic_vector (1 downto 0);
--    MB_II_DM : inout std_logic_vector (8 downto 0);
--    MB_II_DQ : inout std_logic_vector (63 downto 0);
--    MB_II_DQS : inout std_logic_vector (8 downto 0);
--    MB_II_DQS_n : inout std_logic_vector (8 downto 0);
--    MB_II_ODT : out std_logic_vector (1 downto 0);
--    MB_II_PARITY : out std_logic_vector(0 downto 0);
--    MB_II_RAS_A16 : out std_logic;
--    MB_II_WE_A14 : out std_logic;
--    MB_II_RESET_N          : out std_logic_vector(0 downto 0);
--    MB_II_ALERT_N           : in std_logic_vector(0 downto 0);

    -- back transceivers
    BCK_SDA : inout std_logic_vector(2 downto 0);
    BCK_SCL : inout std_logic_vector(2 downto 0);
    BCK_ERR : inout std_logic_vector(2 downto 0);
    -- pmbus
    PMBUS_SC : inout std_logic;
    PMBUS_SD : inout std_logic;
    PMBUS_ALERT : in std_logic;
    -- front transceivers
    QSFP_0_RX : in std_logic_vector(0 downto 0);
    QSFP_0_TX : out std_logic_vector(0 downto 0);
    QSFP_SDA : inout std_logic_vector(5 downto 0);
    QSFP_SCL : inout std_logic_vector(5 downto 0);
    QSFP_RST : inout std_logic;
    -- I2C Interface to Sensors
    SENS_SC                : inout   std_logic;
    SENS_SD                : inout std_logic;
     -- Others
--    CFG_DATA               : inout std_logic_vector (3 downto 0);
    VERSION                : in    std_logic_vector(1 downto 0);
    ID                     : in    std_logic_vector(7 downto 0);
    TESTIO                 : inout std_logic_vector(5 downto 0);
    QSFP_LED               : inout std_logic_vector(11 downto 0)
  );
end unb2_singlemac;

architecture str of unb2_singlemac is
   component system_iopll is
     port (
       refclk     : in  std_logic := 'X';  -- clk
       rst        : in  std_logic := 'X';
       locked     : out std_logic;
       outclk_0   : out std_logic;  -- outclk0
       outclk_1   : out std_logic;  -- outclk1
       outclk_2   : out std_logic  -- outclk2
     );
  end component system_iopll;

  component tech_transceiver_arria10_1 is
    generic (
      g_nof_channels      : natural := 1
    );
    port (
      clk                     : in  std_logic;
      reset_p                 : in  std_logic;
      refclk                  : in  std_logic;
      clk_156_arr             : out std_logic_vector(0 downto 0);
      clk_312_arr             : out std_logic_vector(0 downto 0);
      tx_serial_data          : out std_logic_vector(0 downto 0);
      rx_serial_data          : in  std_logic_vector(0 downto 0);
      tx_parallel_data        : in  std_logic_vector(63 downto 0);
      rx_parallel_data        : out std_logic_vector(63 downto 0);
      tx_control              : in  std_logic_vector(7 downto 0);
      rx_control              : out std_logic_vector(7 downto 0)
    );
  end component;

  component ip_arria10_mac_10g is
    port (
      csr_read                        : in  std_logic                     := 'X';  -- read
      csr_write                       : in  std_logic                     := 'X';  -- write
      csr_writedata                   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- writedata
      csr_readdata                    : out std_logic_vector(31 downto 0);  -- readdata
      csr_waitrequest                 : out std_logic;  -- waitrequest
      csr_address                     : in  std_logic_vector(12 downto 0)  := (others => 'X');  -- address
      tx_312_5_clk                    : in  std_logic                     := 'X';  -- clk
      tx_156_25_clk                   : in  std_logic                     := 'X';  -- clk
      rx_312_5_clk                    : in  std_logic                     := 'X';  -- clk
      rx_156_25_clk                   : in  std_logic                     := 'X';  -- clk
      csr_clk                         : in  std_logic                     := 'X';  -- clk
      csr_rst_n                       : in  std_logic                     := 'X';  -- reset_n
      tx_rst_n                        : in  std_logic                     := 'X';  -- reset_n
      rx_rst_n                        : in  std_logic                     := 'X';  -- reset_n
      avalon_st_tx_startofpacket      : in  std_logic                     := 'X';  -- startofpacket
      avalon_st_tx_endofpacket        : in  std_logic                     := 'X';  -- endofpacket
      avalon_st_tx_valid              : in  std_logic                     := 'X';  -- valid
      avalon_st_tx_data               : in  std_logic_vector(63 downto 0) := (others => 'X');  -- data
      avalon_st_tx_empty              : in  std_logic_vector(2 downto 0)  := (others => 'X');  -- empty
      avalon_st_tx_error              : in  std_logic                     := 'X';  -- error
      avalon_st_tx_ready              : out std_logic;  -- ready
      avalon_st_pause_data            : in  std_logic_vector(1 downto 0)  := (others => 'X');  -- data
      xgmii_tx                        : out std_logic_vector(71 downto 0);  -- data
      avalon_st_txstatus_valid        : out std_logic;  -- valid
      avalon_st_txstatus_data         : out std_logic_vector(39 downto 0);  -- data
      avalon_st_txstatus_error        : out std_logic_vector(6 downto 0);  -- error
      xgmii_rx                        : in  std_logic_vector(71 downto 0) := (others => 'X');  -- data
      link_fault_status_xgmii_rx_data : out std_logic_vector(1 downto 0);  -- data
      avalon_st_rx_data               : out std_logic_vector(63 downto 0);  -- data
      avalon_st_rx_startofpacket      : out std_logic;  -- startofpacket
      avalon_st_rx_valid              : out std_logic;  -- valid
      avalon_st_rx_empty              : out std_logic_vector(2 downto 0);  -- empty
      avalon_st_rx_error              : out std_logic_vector(5 downto 0);  -- error
      avalon_st_rx_ready              : in  std_logic                     := 'X';  -- ready
      avalon_st_rx_endofpacket        : out std_logic;  -- endofpacket
      avalon_st_rxstatus_valid        : out std_logic;  -- valid
      avalon_st_rxstatus_data         : out std_logic_vector(39 downto 0);  -- data
      avalon_st_rxstatus_error        : out std_logic_vector(6 downto 0)  -- error
    );
  end component ip_arria10_mac_10g;

    -- constants
    constant cs_sim                : std_logic := '0';
    constant cs_sync               : std_logic := '1';
    --CONSTANT c_block_len            : NATURAL := 180; -- = 1440 user bytes. Including packetizing: 1508 bytes.
    constant c_block_len            : natural := 1118;  -- = 8944 user bytes. Including packetizing: 9012 bytes.

    -- general reset and clock signals
    signal reset_n                 : std_logic := '0';
    signal reset_p                 : std_logic := '0';
    signal pout_wdi                : std_logic := '0';
    signal sys_clk                 : std_logic := '0';
    signal sys_locked              : std_logic := '0';
    signal mm_clk                  : std_logic := '0';
    signal clk_125                 : std_logic := '0';

    -- signals for the transceivers
    signal tx_serial_data_front    : std_logic_vector(0 downto 0);
    signal rx_serial_data_front    : std_logic_vector(0 downto 0);
    signal xgmii_tx                : std_logic_vector(71 downto 0);
    signal xgmii_rx                : std_logic_vector(71 downto 0);
    signal clk_156                 : std_logic_vector(0 downto 0);
    signal clk_312                 : std_logic_vector(0 downto 0);

    -- signals for the MAC
    signal mac_10g_loopback_sop    : std_logic;
    signal mac_10g_loopback_eop    : std_logic;
    signal mac_10g_loopback_valid  : std_logic;
    signal mac_10g_loopback_ready  : std_logic;
    signal mac_10g_loopback_data   : std_logic_vector(63 downto 0);
    signal mac_10g_loopback_empty  : std_logic_vector(2 downto 0);
    signal mac_10g_loopback_err    : std_logic_vector(5 downto 0);

    signal reg_mac_rd              : std_logic;
    signal reg_mac_wr              : std_logic;
    signal reg_mac_waitrequest     : std_logic;
    signal reg_mac_rddata          : std_logic_vector(31 downto 0);
    signal reg_mac_wrdata          : std_logic_vector(31 downto 0);
    signal reg_mac_address         : std_logic_vector(12 downto 0);

    -- signals for the bidirectional and misc ios
    signal inta_in    : std_logic;
    signal intb_in    : std_logic;
    signal testio_in  : std_logic_vector(5 downto 0);
    signal qsfp_led_in  : std_logic_vector(11 downto 0);
    signal bck_err_in : std_logic_vector(2 downto 0);
    signal inta_out   : std_logic;
    signal intb_out   : std_logic;
    signal testio_out : std_logic_vector(5 downto 0);
    signal qsfp_led_out  : std_logic_vector(11 downto 0);
    signal bck_err_out : std_logic_vector(2 downto 0);
    signal ver_id_pmbusalert     : std_logic_vector(10 downto 0);
    signal toggle_count     : std_logic_vector(31 downto 0);
    signal toggle_count1    : std_logic_vector(31 downto 0);
    signal led_state    : std_logic;
begin
    WDI <= 'Z';

--    -- ****** Front side transceivers and MAC ******
--
    QSFP_0_TX  <= tx_serial_data_front;
    rx_serial_data_front <= QSFP_0_RX;

    u_transceiver: tech_transceiver_arria10_1
    generic map (
      g_nof_channels     => 1
    )
    port map(
      clk                => mm_clk,
      reset_p            => reset_p,
      refclk             => SA_CLK,
      clk_156_arr        => clk_156,
      clk_312_arr        => clk_312,
      tx_serial_data     => tx_serial_data_front,
      rx_serial_data     => rx_serial_data_front,
      tx_parallel_data   => xgmii_tx(63 downto 0),
      rx_parallel_data   => xgmii_rx(63 downto 0),
      tx_control         => xgmii_tx(71 downto 64),
      rx_control         => xgmii_rx(71 downto 64)
   );

  u0 : ip_arria10_mac_10g
  port map (
    csr_read                    => reg_mac_rd,
    csr_write                   => reg_mac_wr,
    csr_writedata               => reg_mac_wrdata(31 downto 0),
    csr_readdata                => reg_mac_rddata(31 downto 0),
    csr_waitrequest             => reg_mac_waitrequest,
    csr_address                 => reg_mac_address(12 downto 0),
    tx_312_5_clk                => clk_312(0),
    tx_156_25_clk               => clk_156(0),
    rx_312_5_clk                => clk_312(0),
    rx_156_25_clk               => clk_156(0),
    csr_clk                     => mm_clk,
    csr_rst_n                   => reset_n,
    tx_rst_n                    => reset_n,
    rx_rst_n                    => reset_n,
    avalon_st_tx_startofpacket  => mac_10g_loopback_sop,
    avalon_st_tx_endofpacket    => mac_10g_loopback_eop,
    avalon_st_tx_valid          => mac_10g_loopback_valid,
    avalon_st_tx_data           => mac_10g_loopback_data,
    avalon_st_tx_empty          => mac_10g_loopback_empty,
    avalon_st_tx_error          => mac_10g_loopback_err(0),
    avalon_st_tx_ready          => mac_10g_loopback_ready,
    avalon_st_pause_data        => (others => '0'),
    xgmii_tx                    => xgmii_tx,
    avalon_st_txstatus_valid    => open,
    avalon_st_txstatus_data     => open,
    avalon_st_txstatus_error    => open,
    xgmii_rx                    => xgmii_rx,
    link_fault_status_xgmii_rx_data => open,
    avalon_st_rx_data           => mac_10g_loopback_data,
    avalon_st_rx_startofpacket  => mac_10g_loopback_sop,
    avalon_st_rx_valid          => mac_10g_loopback_valid,
    avalon_st_rx_empty          => mac_10g_loopback_empty(2 downto 0),
    avalon_st_rx_error          => mac_10g_loopback_err(5 downto 0),
    avalon_st_rx_ready          => mac_10g_loopback_ready,
    avalon_st_rx_endofpacket    => mac_10g_loopback_eop,
    avalon_st_rxstatus_valid    => open,
    avalon_st_rxstatus_data     => open,
    avalon_st_rxstatus_error    => open
  );

    -- ****** node control for resets and wdi

    u_node_ctrl : entity unb_common_lib.unb_node_ctrl
      generic map (
        g_pulse_us => c_unb_tse_clk_freq / (10**6)  -- nof system clock cycles to get us period, equal to system clock frequency / 10**6
      )
      port map (
        xo_clk      => ETH_clk,
        xo_rst_n    => reset_n,
        sys_clk     => sys_clk,
        sys_locked  => sys_locked,
        sys_rst     => open,
        st_clk      => clk,
        st_rst      => open,
        wdi_in      => pout_wdi,
        wdi_out     => WDI,  -- overrule default WDI = 'Z' and let SW toggle WDI via pout_wdi to enable the watchdog
        pulse_us    => open,
        pulse_ms    => open,
        pulse_s     => open  -- could be used to toggle a LED
      );

    reset_p <= not reset_n;

    u_system_pll : system_iopll
      port map(
        refclk   => ETH_CLK,
        rst      => reset_p,
        locked   => sys_locked,
        outclk_0 => mm_clk,  -- 100MHz
        outclk_1 => sys_clk,  -- 300MHz
  outclk_2 => clk_125  -- 125MHz for 1ge
     );

-- bidirectional and misc
-- use PPS as output enable

    INTA <= inta_out when PPS = '1' else 'Z';
    INTB <= intb_out when PPS = '1' else 'Z';
    TESTIO <= testio_out;
    QSFP_LED <= qsfp_led_out;
    BCK_ERR <= bck_err_out when PPS = '1' else "ZZZ";

    inta_in <= INTA;
    intb_in <= INTB;
    testio_in <= TESTIO;
    qsfp_led_in <= QSFP_LED;
    bck_err_in <= BCK_ERR;

    inta_out <= intb_in;
    intb_out <= inta_in;
    testio_out(5 downto 3) <= (others => '0');
    testio_out(0) <= CLK;
    testio_out(1) <= mm_clk;
--    qsfp_led_out(11 downto 6) <= qsfp_led_in(5 downto 0);
--    qsfp_led_out(5 downto 0) <= qsfp_led_in(11 downto 6);
    qsfp_led_out(11 downto 6) <= (others => led_state);
    qsfp_led_out(5 downto 0) <= (others => not led_state);
    bck_err_out(2) <= bck_err_in(1);
    bck_err_out(1) <= bck_err_in(0);
    bck_err_out(0) <= bck_err_in(2);

    ver_id_pmbusalert <= version & id & pmbus_alert;

    toggle_led_proc: process(mm_clk, reset_p)
    begin
      if reset_p = '1' then
        toggle_count   <= (others => '0');
        led_state  <= '0';
      else
        if mm_clk'event and mm_clk = '1' then
          if (toggle_count < 100000000) then
            toggle_count   <= toggle_count + 1;
          else
            toggle_count   <= (others => '0');
      led_state      <= not led_state;
          end if;
        end if;
      end if;
    end process;

    toggle_led_proc1: process(clk)
    begin
      if clk'event and clk = '1' then
        if (toggle_count1 < 100000000) then
          toggle_count1   <= toggle_count1 + 1;
        else
          toggle_count1   <= (others => '0');
          testio_out(2)   <= not testio_out(2);
    pout_wdi        <= not pout_wdi;
        end if;
      end if;
    end process;
end str;
