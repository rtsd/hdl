-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author : E. Kooistra
-- Purpose:
--   Multi test bench to verify functions in reorder_pkg.vhd using instances
--   of reorder_pkg_test.vhd.
-- Description:
--
-- Usage:
-- > as 3
-- > run -all
-- * The tb is self stopping and self checking, tb_end will stop the simulation
--   by stopping the clk and thus all toggling.
library IEEE;
use IEEE.std_logic_1164.all;

entity reorder_pkg_test_test is
end reorder_pkg_test_test;

architecture tb of reorder_pkg_test_test is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'

begin
  -- g_nof_blocks_per_packet : natural := 4;
  -- g_nof_data_per_block    : natural := 488;
  -- g_nof_words_per_data    : natural := 1

  u_4_488_1  : entity work.reorder_pkg_test generic map (4, 488, 1);
  u_4_488_2  : entity work.reorder_pkg_test generic map (4, 488, 2);
end tb;
