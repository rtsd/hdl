-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker
-- Purpose:
-- . mm interface for common_variable_delay.vhd to enable output
-- Description:
-- . see common_variable_delay.vhd
-- --------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_mem_pkg.all;

entity mms_common_variable_delay is
  port (
    mm_rst          : in  std_logic;
    mm_clk          : in  std_logic;
    dp_rst          : in  std_logic := '0';
    dp_clk          : in  std_logic;

    -- MM interface
    reg_enable_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_enable_miso : out t_mem_miso;

    delay           : in  natural := 0;
    trigger         : in  std_logic := '0';
    trigger_en      : out std_logic;
    trigger_dly     : out std_logic
  );
end mms_common_variable_delay;

architecture str of mms_common_variable_delay is
  constant c_enable_mem_reg : t_c_mem := (c_mem_reg_rd_latency, 1, 1, 1, '0');

  signal enable_reg : std_logic_vector(c_enable_mem_reg.dat_w * c_enable_mem_reg.nof_dat - 1 downto 0);

  signal enable : std_logic := '0';
begin
  trigger_en <= sl(enable_reg);  -- also provide enable as OUT
  enable <= sl(enable_reg);

  -- device under test
  u_common_variable_delay : entity work.common_variable_delay
  port map (
    rst       => dp_rst,
    clk       => dp_clk,

    delay     => delay,
    enable    => enable,
    in_pulse  => trigger,
    out_pulse => trigger_dly
  );

  u_mms_common_reg : entity work.mms_common_reg
  generic map (
    g_mm_reg       => c_enable_mem_reg
  )
  port map (
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    st_rst         => dp_rst,
    st_clk         => dp_clk,

    reg_mosi       => reg_enable_mosi,
    reg_miso       => reg_enable_miso,

    in_reg         => enable_reg,
    out_reg        => enable_reg
  );
end;
