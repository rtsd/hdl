-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Test bench for mms_unb1_board_sens
--
-- Features:
-- . Verify that the UniBoard sensors are read.
--
-- Usage:
-- . > as 10
-- . > run -all

entity tb_mms_unb1_board_sens is
end tb_mms_unb1_board_sens;

library IEEE, common_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;

architecture tb of tb_mms_unb1_board_sens is
  constant c_sim              : boolean := true;  -- FALSE;
  constant c_repeat           : natural := 2;
  constant c_clk_freq         : natural := 100 * 10**6;
  constant c_clk_period       : time    := (10**9 / c_clk_freq) * 1 ns;
  constant c_rst_period       : time    := 4 * c_clk_period;

  -- Model I2C sensor slaves as on the UniBoard
  constant c_temp_high           : natural := 85;
  constant c_fpga_temp_address   : std_logic_vector(6 downto 0) := "0011000";  -- MAX1618 address LOW LOW
  constant c_fpga_temp           : integer := 60;
  constant c_eth_temp_address    : std_logic_vector(6 downto 0) := "0101001";  -- MAX1618 address MID LOW
  constant c_eth_temp            : integer := 40;
  constant c_hot_swap_address    : std_logic_vector(6 downto 0) := "1000100";  -- LTC4260 address L L L
  constant c_hot_swap_R_sense    : real := 0.01;  -- = 10 mOhm on UniBoard

  constant c_uniboard_current    : real := 5.0;  -- = assume 5.0 A on UniBoard  --> hot swap = 5010 mAmpere (167)
  constant c_uniboard_supply     : real := 48.0;  -- = assume 48.0 V on UniBoard --> hot swap = 48000 mVolt (120)
  constant c_uniboard_adin       : real := -1.0;  -- = NC on UniBoard

  constant c_sens_nof_result  : natural := 4 + 1;
  constant c_sens_expected    : t_natural_arr(0 to c_sens_nof_result - 1) := (60, 40, 167, 120, 0);  -- 4 bytes as read by c_SEQ in unb1_board_sens_ctrl + sens_err

  signal tb_end          : std_logic := '0';
  signal clk             : std_logic := '0';
  signal rst             : std_logic := '1';
  signal start           : std_logic;

  signal reg_mosi        : t_mem_mosi := c_mem_mosi_rst;
  signal reg_miso        : t_mem_miso;

  signal sens_val        : std_logic;
  signal sens_dat        : std_logic_vector(c_byte_w - 1 downto 0);

  signal scl_stretch     : std_logic := 'Z';
  signal scl             : std_logic;
  signal sda             : std_logic;
begin
  rst <= '0' after 4 * c_clk_period;
  clk <= (not clk) or tb_end after c_clk_period / 2;

  -- I2C bus
  scl <= 'H';  -- model I2C pull up
  sda <= 'H';  -- model I2C pull up, use '0' and '1' to verify sens_err

  scl <= scl_stretch;

  sens_clk_stretch : process (scl)
  begin
    if falling_edge(scl) then
      scl_stretch <= '0', 'Z' after 50 ns;  -- < 10 ns to effectively disable stretching, >= 50 ns to enable it
    end if;
  end process;

  p_mm_reg_stimuli : process
    variable v_bsn : natural;
    variable vI    : natural;
    variable vJ    : natural;
  begin
    start     <= '0';
    reg_mosi  <= c_mem_mosi_rst;

    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    for I in 0 to c_repeat - 1 loop
      -- start I2C access
      start <= '1';
      proc_common_wait_some_cycles(clk, 1);
      start <= '0';

      -- wait for I2C access to have finished
      proc_common_wait_some_cycles(clk, sel_a_b(c_sim, 5000, 500000));

      -- read I2C result data
      for I in 0 to c_sens_nof_result - 1 loop
        proc_mem_mm_bus_rd(I, clk, reg_miso, reg_mosi);  -- read sens_data
      end loop;

      proc_common_wait_some_cycles(clk, 1000);
    end loop;

    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  sens_val <= reg_miso.rdval;
  sens_dat <= reg_miso.rddata(c_byte_w - 1 downto 0);

  -- Verify sensor data
  p_verify : process
  begin
    wait until rising_edge(clk);  -- Added this line to avoid warning: (vcom-1090) Possible infinite loop: Process contains no WAIT statement.

    proc_common_wait_until_high(clk, sens_val);
    assert TO_UINT(sens_dat) = c_sens_expected(0)
      report "Wrong FPGA temperature value"
      severity ERROR;
    proc_common_wait_some_cycles(clk, 1);
    assert TO_UINT(sens_dat) = c_sens_expected(1)
      report "Wrong ETH temperature value"
      severity ERROR;
    proc_common_wait_some_cycles(clk, 1);
    assert TO_UINT(sens_dat) = c_sens_expected(2)
      report "Wrong hot swap V sense value"
      severity ERROR;
    proc_common_wait_some_cycles(clk, 1);
    assert TO_UINT(sens_dat) = c_sens_expected(3)
      report "Wrong hot swap V source value"
      severity ERROR;
    proc_common_wait_some_cycles(clk, 1);
    assert TO_UINT(sens_dat) = c_sens_expected(4)
      report "An I2C error occurred"
      severity ERROR;
  end process;

  -- I2C sensors master
  u_mms_unb1_board_sens : entity work.mms_unb1_board_sens
  generic map (
    g_sim       => c_sim,
    g_clk_freq  => c_clk_freq,
    g_temp_high => c_temp_high
  )
  port map (
    -- Clocks and reset
    mm_rst    => rst,
    mm_clk    => clk,
    mm_start  => start,

    -- Memory-mapped clock domain
    reg_mosi  => reg_mosi,
    reg_miso  => reg_miso,

    -- i2c bus
    scl       => scl,
    sda       => sda
  );

  -- I2C slaves that are available for each FPGA
  u_fpga_temp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_fpga_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_fpga_temp
  );

  -- I2C slaves that are available only via FPGA back node 3
  u_eth_temp : entity i2c_lib.dev_max1618
  generic map (
    g_address => c_eth_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_eth_temp
  );

  u_power : entity i2c_lib.dev_ltc4260
  generic map (
    g_address => c_hot_swap_address,
    g_R_sense => c_hot_swap_R_sense
  )
  port map (
    scl               => scl,
    sda               => sda,
    ana_current_sense => c_uniboard_current,
    ana_volt_source   => c_uniboard_supply,
    ana_volt_adin     => c_uniboard_adin
  );
end tb;
