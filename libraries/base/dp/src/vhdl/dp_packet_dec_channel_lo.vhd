--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose: Decode sosi.channel low bits from the high part of the CHAN field
--          of a DP packet.
-- Description:
--   The snk_in data must be DP packet data. The [high-1:high-g_channel_lo]
--   bits of the CHAN field get set to 0 and placed into the [g_channel_lo-1:0]
--   bits of the sosi.channel field. The [high] bit of the CHAN field is not
--   changed because it is reserved for future use.
-- Remark:
-- . See also dp_packet_enc_channel_lo.
-- . The dp_packet_dec_channel_lo.vhd is verified in tb_dp_distribute.vhd.
-- . The sosi.channel field must be valid during the entire frame, so not only
--   at the sop. Therefore it is necessary to use channel_lo_hold.

entity dp_packet_dec_channel_lo is
  generic (
    g_data_w      : natural := 32;
    g_channel_lo  : natural := 0
  );
  port (
    rst       : in std_logic;
    clk       : in std_logic;
    -- ST sinks
    snk_out   : out t_dp_siso;
    snk_in    : in  t_dp_sosi;
    -- ST source
    src_in    : in  t_dp_siso;
    src_out   : out t_dp_sosi
  );
end dp_packet_dec_channel_lo;

architecture rtl of dp_packet_dec_channel_lo is
  signal channel_lo_hold      : std_logic_vector(g_channel_lo - 1 downto 0);
  signal nxt_channel_lo_hold  : std_logic_vector(g_channel_lo - 1 downto 0);
begin
  assert g_channel_lo + 1 <= g_data_w
    report "dp_packet_dec_channel_lo : g_channel_lo does not fit in g_data_w"
    severity FAILURE;

  -- Pass on the flow control
  snk_out <= src_in;

  -- Pass on the DP packet and extract the g_channel_lo bits from CHAN field
  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      channel_lo_hold <= (others => '0');
    elsif rising_edge(clk) then
      channel_lo_hold <= nxt_channel_lo_hold;
    end if;
  end process;

  p_channel_lo : process (snk_in, channel_lo_hold)
    variable v_channel_lo : std_logic_vector(g_channel_lo - 1 downto 0);
  begin
    src_out <= snk_in;
    nxt_channel_lo_hold <= channel_lo_hold;

    if g_channel_lo > 0 then
      src_out.channel(g_channel_lo - 1 downto 0) <= channel_lo_hold;  -- default combinatorially assign the held channel_lo bits to the sosi.channel field after the sop
      if snk_in.sop = '1' then
        -- clear the channel_lo bits in the MSWord of the CHAN field
        src_out.data(g_data_w - 2 downto g_data_w - 1 - g_channel_lo) <= (others => '0');

        -- extract the channel_lo bits from the MSWord of the CHAN field
        v_channel_lo := snk_in.data(g_data_w - 2 downto g_data_w - 1 - g_channel_lo);
        src_out.channel(g_channel_lo - 1 downto 0) <= v_channel_lo;  -- combinatorially assign the channel_lo bits to the sosi.channel field at the sop
        nxt_channel_lo_hold                      <= v_channel_lo;  -- register the channel_lo bits so they can be assigned to the rest of the frame as well
      end if;
    end if;
  end process;
end rtl;
