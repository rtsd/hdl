-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer
-- Purpose:
--   IP components declarations for various devices that get wrapped by the tech components

library IEEE;
use IEEE.std_logic_1164.all;

package tech_iobuf_component_pkg is
  -----------------------------------------------------------------------------
  -- ip_stratixiv
  -----------------------------------------------------------------------------

  component ip_stratixiv_ddio_in is
  generic(
    g_device_family : string := "Stratix IV";
    g_width         : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  component ip_stratixiv_ddio_out is
  generic(
    g_device_family : string  := "Stratix IV";
    g_width         : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10
  -----------------------------------------------------------------------------

  component ip_arria10_ddio_in is
  generic (
    g_width : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';  -- Not Connected
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  component ip_arria10_ddio_out is
  generic(
    g_width : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';  -- Not Connected
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  -----------------------------------------------------------------------------

  component ip_arria10_e3sge3_ddio_in is
  generic (
    g_width : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';  -- Not Connected
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  component ip_arria10_e3sge3_ddio_out is
  generic(
    g_width : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';  -- Not Connected
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e1sg
  -----------------------------------------------------------------------------

  component ip_arria10_e1sg_ddio_in is
  generic (
    g_width : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';  -- Not Connected
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  component ip_arria10_e1sg_ddio_out is
  generic(
    g_width : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';  -- Not Connected
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_arria10_e2sg
  -----------------------------------------------------------------------------

  component ip_arria10_e2sg_ddio_in is
  generic (
    g_width : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';  -- Not Connected
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  component ip_arria10_e2sg_ddio_out is
  generic(
    g_width : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';  -- Not Connected
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  -----------------------------------------------------------------------------
  -- ip_agi027_1e1v
  -----------------------------------------------------------------------------

  component ip_agi027_1e1v_ddio_in is
  generic (
    g_width : natural := 1
  );
  port (
    in_dat      : in  std_logic_vector(g_width - 1 downto 0);
    in_clk      : in  std_logic;
    in_clk_en   : in  std_logic := '1';  -- Not Connected
    rst         : in  std_logic := '0';
    out_dat_hi  : out std_logic_vector(g_width - 1 downto 0);
    out_dat_lo  : out std_logic_vector(g_width - 1 downto 0)
  );
  end component;

  component ip_agi027_1e1v_ddio_out is
  generic(
    g_width : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';  -- Not Connected
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
  end component;
end tech_iobuf_component_pkg;
