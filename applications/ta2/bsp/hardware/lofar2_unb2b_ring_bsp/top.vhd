-- --------------------------------------------------------------------------
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Reinier vd Walle
-- Purpose:
-- . Top-Level for OpenCL Board Support Package of lofar2_unb2b_ring
-- Description:
-- . This BSP makes several IO-channels available to the OpenCL kernel, these
--   include:
--   . up to 4 qsfp 10 GbE
--   . up to 8 ring 10 GbE
--   . from_lane_sosi/to_lane_sosi
--   . M&C
-- --------------------------------------------------------------------------

library IEEE, common_lib, unb2b_board_lib, technology_lib, tech_pll_lib, dp_lib, diag_lib, mm_lib, ta2_channel_cross_lib, ta2_unb2b_10gbe_lib, ta2_unb2b_mm_io_lib, lofar2_sdp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use unb2b_board_lib.unb2b_board_pkg.all;
use unb2b_board_lib.unb2b_board_peripherals_pkg.all;
use dp_lib.dp_stream_pkg.all;
use lofar2_sdp_lib.sdp_pkg.all;
use work.ring_pkg.all;
use work.top_components_pkg.all;

entity top is
  generic (
    g_design_name        : string   := "lofar2_unb2b_ring_bsp";
    g_design_note        : string   := "UNUSED";
    g_technology         : natural  := c_tech_arria10_e1sg;
    g_sim                : boolean  := false;  -- Overridden by TB
    g_sim_unb_nr         : natural  := 0;
    g_sim_node_nr        : natural  := 0;
    g_stamp_date         : natural  := 0;  -- Date (YYYYMMDD) -- set by QSF
    g_stamp_time         : natural  := 0;  -- Time (HHMMSS)   -- set by QSF
    g_revision_id        : string   := "";  -- revision_id, commit hash (first 9 chars) or number
    g_factory_image      : boolean  := false;
    g_protect_addr_range : boolean  := false;
    g_nof_lanes          : positive := 8;  -- must be in range 1 - 8
    g_nof_rx_monitors    : natural  := 16;  -- max = 16
    g_nof_tx_monitors    : natural  := 16  -- max = 16
  );
  port (
    -- GENERAL
    CLK          : in    std_logic;  -- System Clock
    PPS          : in    std_logic;  -- System Sync
    WDI          : out   std_logic;  -- Watchdog Clear
    INTA         : inout std_logic;  -- FPGA interconnect line
    INTB         : inout std_logic;  -- FPGA interconnect line

    -- Others
    VERSION      : in    std_logic_vector(c_unb2b_board_aux.version_w - 1 downto 0);
    ID           : in    std_logic_vector(c_unb2b_board_aux.id_w - 1 downto 0);
    TESTIO       : inout std_logic_vector(c_unb2b_board_aux.testio_w - 1 downto 0);

    -- I2C Interface to Sensors
    SENS_SC      : inout std_logic;
    SENS_SD      : inout std_logic;

    PMBUS_SC     : inout std_logic;
    PMBUS_SD     : inout std_logic;
    PMBUS_ALERT  : in    std_logic := '0';

    -- 1GbE Control Interface
    ETH_CLK      : in    std_logic;
    ETH_SGIN     : in    std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);
    ETH_SGOUT    : out   std_logic_vector(c_unb2b_board_nof_eth - 1 downto 0);

    -- Transceiver clocks
    SA_CLK       : in    std_logic := '0';  -- Clock 10GbE front (qsfp) and ring lines

    -- front transceivers
    QSFP_0_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    QSFP_0_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);

    -- ring transceivers
    RING_0_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');  -- Using qsfp bus width also for ring interfaces
    RING_0_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);
    RING_1_RX    : in    std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0) := (others => '0');
    RING_1_TX    : out   std_logic_vector(c_unb2b_board_tr_qsfp.bus_w - 1 downto 0);

    -- LEDs
    QSFP_LED     : out   std_logic_vector(c_unb2b_board_tr_qsfp_nof_leds - 1 downto 0)
  );
end top;

architecture str of top is
  ---------------
  -- Constants
  ---------------
  constant c_sim_node_type : string(1 to 2) := "FN";

  -- QSFP
  constant c_nof_qsfp_bus           : natural := 1;
  constant c_nof_streams_qsfp       : natural := c_unb2b_board_tr_qsfp.bus_w * c_nof_qsfp_bus;  -- 4

  -- RING
  constant c_nof_ring_bus           : natural := 2;
  constant c_ring_bus_w             : natural := 4;  -- Using 4 phisically, there are 12
  constant c_nof_streams_ring       : natural := c_ring_bus_w * c_nof_ring_bus;  -- c_unb2b_board_tr_ring.bus_w*c_nof_ring_bus; -- 8

  -- 10GbE
  constant c_max_nof_mac            : natural := c_nof_streams_qsfp + c_nof_streams_ring;  -- Use the 12 channel 10GbE IP

  -- Firmware version x.y
  constant c_fw_version             : t_unb2b_board_fw_version := (1, 1);
  constant c_mm_clk_freq            : natural := c_unb2b_board_mm_clk_freq_100M;

  -- OpenCL kernel channel widths as defined in the OpenCL kernel
  constant c_kernel_10gbe_channel_w      : natural := 104;
  constant c_kernel_bs_sosi_channel_w    : natural := 104;
  constant c_kernel_lane_sosi_channel_w  : natural := 168;
  constant c_kernel_mm_io_mosi_channel_w : natural := 72;
  constant c_kernel_mm_io_miso_channel_w : natural := 32;

  -- OpenCL kernel regmap address width as defined in qsys
  constant c_kernel_regmap_addr_w : natural := 8;

  ------------
  -- Types
  ------------
  type t_dp_siso_rx_monitor_2arr is array (integer range <>) of t_dp_siso_arr(g_nof_rx_monitors - 1 downto 0);
  type t_dp_sosi_rx_monitor_2arr is array (integer range <>) of t_dp_sosi_arr(g_nof_rx_monitors - 1 downto 0);
  type t_dp_siso_tx_monitor_2arr is array (integer range <>) of t_dp_siso_arr(g_nof_tx_monitors - 1 downto 0);
  type t_dp_sosi_tx_monitor_2arr is array (integer range <>) of t_dp_sosi_arr(g_nof_tx_monitors - 1 downto 0);

  ------------
  -- Signals
  ------------
  -- System
  signal cs_sim                     : std_logic;
  signal xo_ethclk                  : std_logic;
  signal xo_rst                     : std_logic;
  signal xo_rst_n                   : std_logic;
  signal mm_clk                     : std_logic;
  signal mm_rst                     : std_logic;

  signal st_rst                     : std_logic;
  signal st_clk                     : std_logic;
  signal st_pps                     : std_logic;

  -- PIOs
  signal pout_wdi                   : std_logic;

  -- WDI override
  signal reg_wdi_mosi               : t_mem_mosi;
  signal reg_wdi_miso               : t_mem_miso;

  -- PPSH
  signal reg_ppsh_mosi              : t_mem_mosi;
  signal reg_ppsh_miso              : t_mem_miso;

  -- UniBoard system info
  signal reg_unb_system_info_mosi   : t_mem_mosi;
  signal reg_unb_system_info_miso   : t_mem_miso;
  signal rom_unb_system_info_mosi   : t_mem_mosi;
  signal rom_unb_system_info_miso   : t_mem_miso;

  -- UniBoard I2C sens
  signal reg_unb_sens_mosi          : t_mem_mosi;
  signal reg_unb_sens_miso          : t_mem_miso;

  -- pm bus
  signal reg_unb_pmbus_mosi         : t_mem_mosi;
  signal reg_unb_pmbus_miso         : t_mem_miso;

  -- FPGA sensors
  signal reg_fpga_temp_sens_mosi    : t_mem_mosi;
  signal reg_fpga_temp_sens_miso    : t_mem_miso;
  signal reg_fpga_voltage_sens_mosi : t_mem_mosi;
  signal reg_fpga_voltage_sens_miso : t_mem_miso;

  -- eth1g
  signal eth1g_mm_rst               : std_logic;
  signal eth1g_tse_mosi             : t_mem_mosi;  -- ETH TSE MAC registers
  signal eth1g_tse_miso             : t_mem_miso;
  signal eth1g_reg_mosi             : t_mem_mosi;  -- ETH control and status registers
  signal eth1g_reg_miso             : t_mem_miso;
  signal eth1g_reg_interrupt        : std_logic;  -- Interrupt
  signal eth1g_ram_mosi             : t_mem_mosi;  -- ETH rx frame and tx frame memory
  signal eth1g_ram_miso             : t_mem_miso;

  -- EPCS read
  signal reg_dpmm_data_mosi         : t_mem_mosi;
  signal reg_dpmm_data_miso         : t_mem_miso;
  signal reg_dpmm_ctrl_mosi         : t_mem_mosi;
  signal reg_dpmm_ctrl_miso         : t_mem_miso;

  -- EPCS write
  signal reg_mmdp_data_mosi         : t_mem_mosi;
  signal reg_mmdp_data_miso         : t_mem_miso;
  signal reg_mmdp_ctrl_mosi         : t_mem_mosi;
  signal reg_mmdp_ctrl_miso         : t_mem_miso;

  -- EPCS status/control
  signal reg_epcs_mosi              : t_mem_mosi;
  signal reg_epcs_miso              : t_mem_miso;

  -- Remote Update
  signal reg_remu_mosi              : t_mem_mosi;
  signal reg_remu_miso              : t_mem_miso;

  -- Remote Update
  signal ram_scrap_mosi             : t_mem_mosi;
  signal ram_scrap_miso             : t_mem_miso;

  -- Block Gen
  signal reg_bg_ctrl_mosi           : t_mem_mosi;
  signal reg_bg_ctrl_miso           : t_mem_miso;
  signal ram_bg_data_mosi           : t_mem_mosi;
  signal ram_bg_data_miso           : t_mem_miso;

  -- DP XonOff
  signal reg_dp_xonoff_bg_mosi        : t_mem_mosi;
  signal reg_dp_xonoff_bg_miso        : t_mem_miso;
  signal reg_dp_xonoff_from_lane_mosi : t_mem_mosi;
  signal reg_dp_xonoff_from_lane_miso : t_mem_miso;

  -- tx/rx monitors
  signal reg_bsn_monitor_v2_tx_mosi_arr : t_mem_mosi_arr(g_nof_lanes - 1 downto 0);
  signal reg_bsn_monitor_v2_tx_miso_arr : t_mem_miso_arr(g_nof_lanes - 1 downto 0);
  signal reg_bsn_monitor_v2_rx_mosi_arr : t_mem_mosi_arr(g_nof_lanes - 1 downto 0);
  signal reg_bsn_monitor_v2_rx_miso_arr : t_mem_miso_arr(g_nof_lanes - 1 downto 0);
  signal reg_bsn_monitor_v2_tx_mosi     : t_mem_mosi;
  signal reg_bsn_monitor_v2_tx_miso     : t_mem_miso;
  signal reg_bsn_monitor_v2_rx_mosi     : t_mem_mosi;
  signal reg_bsn_monitor_v2_rx_miso     : t_mem_miso;

  -- MM IO
  signal reg_ta2_unb2b_mm_io_mosi   : t_mem_mosi;
  signal reg_ta2_unb2b_mm_io_miso   : t_mem_miso;

  -- SDP Info
  signal reg_sdp_info_mosi          : t_mem_mosi;
  signal reg_sdp_info_miso          : t_mem_miso;

  -- PLL
  signal clk_156 : std_logic := '0';
  signal clk_312 : std_logic := '0';
  signal rst_156 : std_logic := '0';

  -- QSFP
  signal i_QSFP_TX                  : t_unb2b_board_qsfp_bus_2arr(c_nof_qsfp_bus - 1 downto 0);
  signal i_QSFP_RX                  : t_unb2b_board_qsfp_bus_2arr(c_nof_qsfp_bus - 1 downto 0);

  signal unb2b_board_front_io_serial_tx_arr : std_logic_vector(c_nof_streams_qsfp - 1 downto 0) := (others => '0');
  signal unb2b_board_front_io_serial_rx_arr : std_logic_vector(c_nof_streams_qsfp - 1 downto 0);

  -- RING
  signal i_RING_TX                  : t_unb2b_board_qsfp_bus_2arr(c_nof_ring_bus - 1 downto 0);  -- TODO make ring bus array with 4 elements
  signal i_RING_RX                  : t_unb2b_board_qsfp_bus_2arr(c_nof_ring_bus - 1 downto 0);

  signal unb2b_board_ring_io_serial_tx_arr : std_logic_vector(c_nof_streams_ring - 1 downto 0) := (others => '0');
  signal unb2b_board_ring_io_serial_rx_arr : std_logic_vector(c_nof_streams_ring - 1 downto 0);

  -- QSFP leds
  signal qsfp_green_led_arr         : std_logic_vector(c_nof_qsfp_bus - 1 downto 0);
  signal qsfp_red_led_arr           : std_logic_vector(c_nof_qsfp_bus - 1 downto 0);
  signal unb2b_board_qsfp_leds_tx_src_in_arr : t_dp_siso_arr(c_nof_qsfp_bus * c_quad - 1 downto 0) := (others => c_dp_siso_rst);

  -- Reset
  signal i_reset_n       : std_logic;
  signal i_kernel_rst    : std_logic;

  -- OpenCL kernel
  signal board_kernel_clk_clk                 : std_logic;
  signal board_kernel_clk2x_clk               : std_logic;
  signal board_kernel_reset_reset_n           : std_logic;
  signal board_kernel_reset_reset_n_in        : std_logic;

  signal board_kernel_cra_waitrequest         : std_logic;
  signal board_kernel_cra_readdata            : std_logic_vector(63 downto 0);
  signal board_kernel_cra_readdatavalid       : std_logic;
  signal board_kernel_cra_burstcount          : std_logic_vector(0 downto 0);
  signal board_kernel_cra_writedata           : std_logic_vector(63 downto 0);
  signal board_kernel_cra_address             : std_logic_vector(29 downto 0);
  signal board_kernel_cra_write               : std_logic;
  signal board_kernel_cra_read                : std_logic;
  signal board_kernel_cra_byteenable          : std_logic_vector(7 downto 0);
  signal board_kernel_cra_debugaccess         : std_logic;

  signal board_kernel_irq_irq                 : std_logic_vector(0 downto 0);

  signal board_kernel_register_mem_address    : std_logic_vector(6 downto 0)  := (others => '0');  -- address
  signal board_kernel_register_mem_clken      : std_logic                     := '0';  -- clken
  signal board_kernel_register_mem_chipselect : std_logic                     := '0';  -- chipselect
  signal board_kernel_register_mem_write      : std_logic                     := '0';  -- write
  signal board_kernel_register_mem_readdata   : std_logic_vector(255 downto 0);  -- readdata
  signal board_kernel_register_mem_writedata  : std_logic_vector(255 downto 0)  := (others => '0');  -- writedata
  signal board_kernel_register_mem_byteenable : std_logic_vector(31 downto 0)   := (others => '0');  -- byteenable

  signal ta2_unb2b_10GbE_src_out_arr          : t_dp_sosi_arr(c_max_nof_mac - 1 downto 0) := (others => c_dp_sosi_rst);
  signal ta2_unb2b_10GbE_src_in_arr           : t_dp_siso_arr(c_max_nof_mac - 1 downto 0) := (others => c_dp_siso_rst);
  signal ta2_unb2b_10GbE_snk_out_arr          : t_dp_siso_arr(c_max_nof_mac - 1 downto 0) := (others => c_dp_siso_rst);
  signal ta2_unb2b_10GbE_snk_in_arr           : t_dp_sosi_arr(c_max_nof_mac - 1 downto 0) := (others => c_dp_sosi_rst);
  signal ta2_unb2b_10GbE_tx_serial_r          : std_logic_vector(c_max_nof_mac - 1 downto 0);
  signal ta2_unb2b_10GbE_rx_serial_r          : std_logic_vector(c_max_nof_mac - 1 downto 0);

  signal ta2_unb2b_10GbE_ring_src_out_arr     : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal ta2_unb2b_10GbE_ring_src_in_arr      : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal ta2_unb2b_10GbE_ring_snk_out_arr     : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal ta2_unb2b_10GbE_ring_snk_in_arr      : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);

  signal ta2_unb2b_10GbE_ring_ch_src_out_arr  : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal ta2_unb2b_10GbE_ring_ch_src_in_arr   : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal ta2_unb2b_10GbE_ring_ch_snk_out_arr  : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal ta2_unb2b_10GbE_ring_ch_snk_in_arr   : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);

  signal ta2_unb2b_10GbE_qsfp_src_out_arr     : t_dp_sosi_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_sosi_rst);
  signal ta2_unb2b_10GbE_qsfp_src_in_arr      : t_dp_siso_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_siso_rst);
  signal ta2_unb2b_10GbE_qsfp_snk_out_arr     : t_dp_siso_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_siso_rst);
  signal ta2_unb2b_10GbE_qsfp_snk_in_arr      : t_dp_sosi_arr(c_nof_streams_qsfp - 1 downto 0) := (others => c_dp_sosi_rst);

  signal ta2_unb2b_mm_io_snk_in               : t_dp_sosi;
  signal ta2_unb2b_mm_io_snk_out              : t_dp_siso;
  signal ta2_unb2b_mm_io_src_out              : t_dp_sosi;
  signal ta2_unb2b_mm_io_src_in               : t_dp_siso;

  signal from_lane_sosi_arr                   : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal from_lane_siso_arr                   : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal to_lane_sosi_arr                     : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal to_lane_siso_arr                     : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);

  signal kernel_from_lane_sosi_arr            : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal kernel_from_lane_siso_arr            : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal kernel_to_lane_sosi_arr              : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal kernel_to_lane_siso_arr              : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);

  signal kernel_rx_monitor_sosi_arr           : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal kernel_rx_monitor_siso_arr           : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal kernel_tx_monitor_sosi_arr           : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal kernel_tx_monitor_siso_arr           : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);

  signal rx_monitor_sosi_arr                  : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal rx_monitor_siso_arr                  : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal tx_monitor_sosi_arr                  : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal tx_monitor_siso_arr                  : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);

  signal dp_demux_rx_monitor_sosi_arr         : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_demux_rx_monitor_siso_arr         : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);
  signal dp_demux_tx_monitor_sosi_arr         : t_dp_sosi_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_demux_tx_monitor_siso_arr         : t_dp_siso_arr(c_nof_streams_ring - 1 downto 0) := (others => c_dp_siso_rst);

  signal rx_monitor_sosi_2arr                 : t_dp_sosi_rx_monitor_2arr(c_nof_streams_ring - 1 downto 0) := (others => (others => c_dp_sosi_rst));
  signal rx_monitor_siso_2arr                 : t_dp_siso_rx_monitor_2arr(c_nof_streams_ring - 1 downto 0) := (others => (others => c_dp_siso_rdy));
  signal tx_monitor_sosi_2arr                 : t_dp_sosi_tx_monitor_2arr(c_nof_streams_ring - 1 downto 0) := (others => (others => c_dp_sosi_rst));
  signal tx_monitor_siso_2arr                 : t_dp_siso_tx_monitor_2arr(c_nof_streams_ring - 1 downto 0) := (others => (others => c_dp_siso_rdy));

  signal local_sosi_arr                       : t_dp_sosi_arr(g_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal local_siso_arr                       : t_dp_siso_arr(g_nof_lanes - 1 downto 0) := (others => c_dp_siso_rst);
  signal dp_xonoff_bg_sosi_arr                : t_dp_sosi_arr(g_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_xonoff_bg_siso_arr                : t_dp_siso_arr(g_nof_lanes - 1 downto 0) := (others => c_dp_siso_rst);
  signal dp_xonoff_from_lane_sosi_arr         : t_dp_sosi_arr(g_nof_lanes - 1 downto 0) := (others => c_dp_sosi_rst);
  signal dp_xonoff_from_lane_siso_arr         : t_dp_siso_arr(g_nof_lanes - 1 downto 0) := (others => c_dp_siso_rst);

  signal mux_snk_out_2arr_2                   : t_dp_siso_2arr_2(g_nof_lanes - 1 downto 0);
  signal mux_snk_in_2arr_2                    : t_dp_sosi_2arr_2(g_nof_lanes - 1 downto 0);

  signal bs_sosi                              : t_dp_sosi;
  signal kernel_bs_sosi                       : t_dp_sosi;

  signal gn_index   : natural := 0;
  signal this_rn_id : std_logic_vector(c_sdp_W_gn_id - 1 downto 0);
  signal sdp_info   : t_sdp_info := c_sdp_info_rst;
begin
  assert g_nof_lanes <= c_nof_streams_ring
    report "g_nof_lanes is configured too high!"
    severity ERROR;

  ------------
  -- Front IO
  ------------
  -- put the QSFP_TX/RX ports into arrays
  i_QSFP_RX(0) <= QSFP_0_RX;

  QSFP_0_TX <= i_QSFP_TX(0);

  u_unb2b_board_front_io : entity unb2b_board_lib.unb2b_board_front_io
  generic map (
    g_nof_qsfp_bus => c_nof_qsfp_bus
  )
  port map (
    serial_tx_arr => unb2b_board_front_io_serial_tx_arr,
    serial_rx_arr => unb2b_board_front_io_serial_rx_arr,

    --green_led_arr => qsfp_green_led_arr(c_nof_qsfp_bus-1 DOWNTO 0),
    --red_led_arr   => qsfp_red_led_arr(c_nof_qsfp_bus-1 DOWNTO 0),

    QSFP_RX    => i_QSFP_RX,
    QSFP_TX    => i_QSFP_TX  -- ,

    --QSFP_LED   => QSFP_LED
  );

  ------------------------
  -- qsfp LEDs controller
  ------------------------
  unb2b_board_qsfp_leds_tx_src_in_arr(0).xon <= ta2_unb2b_10GbE_qsfp_snk_out_arr(0).xon;

  u_unb2b_board_qsfp_leds : entity unb2b_board_lib.unb2b_board_qsfp_leds
  generic map (
    g_sim             => g_sim,
    g_factory_image   => g_factory_image,
    g_nof_qsfp        => c_nof_qsfp_bus,
    g_pulse_us        => 1000 / (10**9 / c_mm_clk_freq)  -- nof clk cycles to get us period
  )
  port map (
    rst               => mm_rst,
    clk               => mm_clk,

    tx_siso_arr       => unb2b_board_qsfp_leds_tx_src_in_arr,

    green_led_arr     => qsfp_green_led_arr(c_nof_qsfp_bus - 1 downto 0),
    red_led_arr       => qsfp_red_led_arr(c_nof_qsfp_bus - 1 downto 0)
  );

  gen_leds : for i in 0 to c_nof_qsfp_bus - 1 generate
    QSFP_LED(i * 2)   <=  qsfp_green_led_arr(i);
    QSFP_LED(i * 2 + 1) <=  qsfp_red_led_arr(i);
  end generate;

  ------------
  -- RING IO
  ------------
  i_RING_RX(0) <= RING_0_RX;
  i_RING_RX(1) <= RING_1_RX;
  RING_0_TX <= i_RING_TX(0);
  RING_1_TX <= i_RING_TX(1);

  gen_wire_bus : for i in 0 to c_nof_ring_bus - 1 generate
    gen_wire_signals : for j in 0 to c_ring_bus_w - 1 generate
      i_RING_TX(i)(j) <= unb2b_board_ring_io_serial_tx_arr(i * c_ring_bus_w + j);
      unb2b_board_ring_io_serial_rx_arr(i * c_ring_bus_w + j) <= i_RING_RX(i)(j);
    end generate;
  end generate;

  --------
  -- PLL
  --------
  u_tech_pll_xgmii_mac_clocks : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
  generic map (
    g_technology => c_tech_arria10_e1sg
  )
  port map (
    refclk_644 => SA_CLK,
    rst_in     => mm_rst,
    clk_156    => clk_156,
    clk_312    => clk_312,
    rst_156    => rst_156,
    rst_312    => open
  );

  ----------
  -- 10GbE
  ----------
  -- For the indexing of the lanes we would like to have all even indices (0, 2, 4, 6) to receive from the left (RING_RX_0) and transmit to the right (RING_TX_1).
  -- For the odd indices it should be the other way around, from RING_RX_1 to RING_TX_0. Therefore we need to rewire those signals as follows:
  -- For receiving, instead of the array [0,0; 0,1; 0,2; 0,3; 1,0; 1,1; 1,2; 1,3] we need the array [0,0; 1,0; 0,1; 1,1; 0,2; 1,2; 0,3; 1,3] where each element is (RING bus index, stream index of that bus).
  -- Because all of all the RING busses are concatenated into one array we can do the following:
  -- Rewire [0, 1, 2, 3, 4, 5, 6, 7] to [0, 4, 1, 5, 2, 6, 3, 7]. So now we have the even indices containing the interfaces from RING_0 (receive from the left)
  -- and the odd indices containing RING_1 (receive from the right).
  -- For transmitting we need to have the even indices containing RING_1 (transmit to the right) and the odd having RING_0 (transmit to the left)
  gen_ring_lanes : for I in 0 to c_ring_bus_w - 1 generate
    -- RX side
    ta2_unb2b_10GbE_ring_ch_src_out_arr(2 * I)            <= ta2_unb2b_10GbE_ring_src_out_arr(I);
    ta2_unb2b_10GbE_ring_ch_src_out_arr(2 * I + 1)          <= ta2_unb2b_10GbE_ring_src_out_arr(I + c_ring_bus_w);
    ta2_unb2b_10GbE_ring_src_in_arr(I)                  <= ta2_unb2b_10GbE_ring_ch_src_in_arr(2 * I);
    ta2_unb2b_10GbE_ring_src_in_arr(I + c_ring_bus_w)     <= ta2_unb2b_10GbE_ring_ch_src_in_arr(2 * I + 1);
    -- TX side
    ta2_unb2b_10GbE_ring_snk_in_arr(I)                  <= ta2_unb2b_10GbE_ring_ch_snk_in_arr(2 * I + 1);
    ta2_unb2b_10GbE_ring_snk_in_arr(I + c_ring_bus_w)     <= ta2_unb2b_10GbE_ring_ch_snk_in_arr(2 * I);
    ta2_unb2b_10GbE_ring_ch_snk_out_arr(2 * I + 1)          <= ta2_unb2b_10GbE_ring_snk_out_arr(I);
    ta2_unb2b_10GbE_ring_ch_snk_out_arr(2 * I)            <= ta2_unb2b_10GbE_ring_snk_out_arr(I + c_ring_bus_w);
  end generate;

  -- Wire 8 ring and 4 qsfp to one array of 12 10GbE
  ta2_unb2b_10GbE_snk_in_arr(c_nof_streams_qsfp - 1 downto 0)              <= ta2_unb2b_10GbE_qsfp_snk_in_arr;
  ta2_unb2b_10GbE_snk_in_arr(c_max_nof_mac - 1 downto c_nof_streams_qsfp)  <= ta2_unb2b_10GbE_ring_snk_in_arr;
  ta2_unb2b_10GbE_qsfp_snk_out_arr                                       <= ta2_unb2b_10GbE_snk_out_arr(c_nof_streams_qsfp - 1 downto 0);
  ta2_unb2b_10GbE_ring_snk_out_arr                                       <= ta2_unb2b_10GbE_snk_out_arr(c_max_nof_mac - 1 downto c_nof_streams_qsfp);

  ta2_unb2b_10GbE_qsfp_src_out_arr                                       <= ta2_unb2b_10GbE_src_out_arr(c_nof_streams_qsfp - 1 downto 0);
  ta2_unb2b_10GbE_ring_src_out_arr                                       <= ta2_unb2b_10GbE_src_out_arr(c_max_nof_mac - 1 downto c_nof_streams_qsfp);
  ta2_unb2b_10GbE_src_in_arr(c_nof_streams_qsfp - 1 downto 0)              <= ta2_unb2b_10GbE_qsfp_src_in_arr;
  ta2_unb2b_10GbE_src_in_arr(c_max_nof_mac - 1 downto c_nof_streams_qsfp)  <= ta2_unb2b_10GbE_ring_src_in_arr;

  ta2_unb2b_10GbE_rx_serial_r(c_nof_streams_qsfp - 1 downto 0)             <= unb2b_board_front_io_serial_rx_arr;
  ta2_unb2b_10GbE_rx_serial_r(c_max_nof_mac - 1 downto c_nof_streams_qsfp) <= unb2b_board_ring_io_serial_rx_arr;
  unb2b_board_front_io_serial_tx_arr                                     <= ta2_unb2b_10GbE_tx_serial_r(c_nof_streams_qsfp - 1 downto 0);
  unb2b_board_ring_io_serial_tx_arr                                      <= ta2_unb2b_10GbE_tx_serial_r(c_max_nof_mac - 1 downto c_nof_streams_qsfp);

  -- tr_10GbE
  u_ta2_unb2b_10GbE : entity ta2_unb2b_10GbE_lib.ta2_unb2b_10GbE
  generic map (
    g_nof_mac => c_max_nof_mac,
    g_use_err => true,
    g_use_pll => true
  )
  port map (
    mm_clk       => '0',  -- mm_clk,
    mm_rst       => mm_rst,

    clk_ref_r    => SA_CLK,

    tx_serial_r  => ta2_unb2b_10GbE_tx_serial_r,
    rx_serial_r  => ta2_unb2b_10GbE_rx_serial_r,

    kernel_clk   => board_kernel_clk_clk,
    kernel_reset => i_kernel_rst,

    src_out_arr  => ta2_unb2b_10GbE_src_out_arr,
    src_in_arr   => ta2_unb2b_10GbE_src_in_arr,
    snk_out_arr  => ta2_unb2b_10GbE_snk_out_arr,
    snk_in_arr   => ta2_unb2b_10GbE_snk_in_arr
  );

  --------------------------------------
  -- Monitoring & Control UNB protocol
  --------------------------------------
  u_ta2_unb2b_mm_io : entity ta2_unb2b_mm_io_lib.ta2_unb2b_mm_io
  generic map(
    g_use_opencl => true
  )
  port map (
    mm_clk        =>  mm_clk,
    mm_rst        =>  mm_rst,

    kernel_clk    =>  board_kernel_clk_clk,
    kernel_reset  =>  i_kernel_rst,

    mm_mosi       =>  reg_ta2_unb2b_mm_io_mosi,
    mm_miso       =>  reg_ta2_unb2b_mm_io_miso,

    snk_in        =>  ta2_unb2b_mm_io_snk_in,
    snk_out       =>  ta2_unb2b_mm_io_snk_out,
    src_out       =>  ta2_unb2b_mm_io_src_out,
    src_in        =>  ta2_unb2b_mm_io_src_in
  );

  -----------------------------------------------------------------------------
  -- kernel clock crossing for from/to lane sosi
  -----------------------------------------------------------------------------
  u_ta2_channel_cross_lanes : entity ta2_channel_cross_lib.ta2_channel_cross
  generic map(
    g_nof_streams => g_nof_lanes,
    g_nof_bytes => c_longword_sz,
    g_reverse_bytes => true,
    g_use_bsn => true,
    g_use_sync => true,
    g_use_channel => true
  )
  port map(
    dp_clk             => st_clk,
    dp_rst             => st_rst,
    dp_src_out_arr     => from_lane_sosi_arr(g_nof_lanes - 1 downto 0),
    dp_src_in_arr      => from_lane_siso_arr(g_nof_lanes - 1 downto 0),
    dp_snk_out_arr     => to_lane_siso_arr(g_nof_lanes - 1 downto 0),
    dp_snk_in_arr      => to_lane_sosi_arr(g_nof_lanes - 1 downto 0),

    kernel_clk         => board_kernel_clk_clk,
    kernel_reset       => i_kernel_rst,

    kernel_src_out_arr => kernel_to_lane_sosi_arr(g_nof_lanes - 1 downto 0),
    kernel_src_in_arr  => kernel_to_lane_siso_arr(g_nof_lanes - 1 downto 0),
    kernel_snk_out_arr => kernel_from_lane_siso_arr(g_nof_lanes - 1 downto 0),
    kernel_snk_in_arr  => kernel_from_lane_sosi_arr(g_nof_lanes - 1 downto 0)
  );

  -----------------------------------------------------------------------------
  -- kernel clock crossing for bs sosi
  -----------------------------------------------------------------------------
  u_ta2_channel_cross_bs_sosi : entity ta2_channel_cross_lib.ta2_channel_cross
  generic map(
    g_nof_streams => 1,
    g_nof_bytes => c_word_sz,
    g_reverse_bytes => true,
    g_use_bsn => true,
    g_use_sync => true
  )
  port map(
    dp_clk        => st_clk,
    dp_rst        => st_rst,

    kernel_clk    => board_kernel_clk_clk,
    kernel_reset  => i_kernel_rst,

    dp_snk_in_arr(0)      => bs_sosi,
    kernel_src_out_arr(0) => kernel_bs_sosi
  );

  -----------------------------------------------------------------------------
  -- kernel clock crossing for rx_monitors
  -----------------------------------------------------------------------------
  u_ta2_channel_cross_rx_monitor : entity ta2_channel_cross_lib.ta2_channel_cross
  generic map(
    g_nof_streams => g_nof_lanes,
    g_nof_bytes => c_longword_sz,
    g_reverse_bytes => true,
    g_use_bsn => true,
    g_use_sync => true,
    g_use_channel => true
  )
  port map(
    dp_clk             => st_clk,
    dp_rst             => st_rst,

    dp_src_out_arr     => rx_monitor_sosi_arr(g_nof_lanes - 1 downto 0),
    dp_src_in_arr      => rx_monitor_siso_arr(g_nof_lanes - 1 downto 0),

    kernel_clk         => board_kernel_clk_clk,
    kernel_reset       => i_kernel_rst,

    kernel_snk_out_arr => kernel_rx_monitor_siso_arr(g_nof_lanes - 1 downto 0),
    kernel_snk_in_arr  => kernel_rx_monitor_sosi_arr(g_nof_lanes - 1 downto 0)
  );

  -----------------------------------------------------------------------------
  -- kernel clock crossing for tx_monitors
  -----------------------------------------------------------------------------
  gen_tx_mon_sim_wires: if g_sim = true generate  -- bypass OpenCL kernel in simulation
    kernel_tx_monitor_sosi_arr <= kernel_to_lane_sosi_arr;
    kernel_to_lane_siso_arr <= kernel_tx_monitor_siso_arr;
  end generate;

  u_ta2_channel_cross_tx_monitor : entity ta2_channel_cross_lib.ta2_channel_cross
  generic map(
    g_nof_streams => g_nof_lanes,
    g_nof_bytes => c_longword_sz,
    g_reverse_bytes => true,
    g_use_bsn => true,
    g_use_sync => true,
    g_use_channel => true
  )
  port map(
    dp_clk             => st_clk,
    dp_rst             => st_rst,

    dp_src_out_arr     => tx_monitor_sosi_arr(g_nof_lanes - 1 downto 0),
    dp_src_in_arr      => tx_monitor_siso_arr(g_nof_lanes - 1 downto 0),

    kernel_clk         => board_kernel_clk_clk,
    kernel_reset       => i_kernel_rst,

    kernel_snk_out_arr => kernel_tx_monitor_siso_arr(g_nof_lanes - 1 downto 0),
    kernel_snk_in_arr  => kernel_tx_monitor_sosi_arr(g_nof_lanes - 1 downto 0)
  );

  rx_monitor_siso_arr <= dp_demux_rx_monitor_siso_arr;
  tx_monitor_siso_arr <= dp_demux_tx_monitor_siso_arr;
  p_calc_source_rn : process(tx_monitor_sosi_arr, rx_monitor_sosi_arr)
  begin
    dp_demux_rx_monitor_sosi_arr <= rx_monitor_sosi_arr;
    dp_demux_tx_monitor_sosi_arr <= tx_monitor_sosi_arr;

    for I in 0 to g_nof_lanes - 1 loop
      dp_demux_rx_monitor_sosi_arr(I).channel <= nof_hops_to_source_rn(rx_monitor_sosi_arr(I).channel, this_rn_id, sdp_info.N_rn, ((I + 1) mod 2));  -- Use (I+1) MOD 2 to get 1 if I is even and 0 if I is odd
      dp_demux_tx_monitor_sosi_arr(I).channel <= nof_hops_to_source_rn(tx_monitor_sosi_arr(I).channel, this_rn_id, sdp_info.N_rn, ((I + 1) mod 2));
    end loop;
  end process;

  gen_monitors : for I in 0 to g_nof_lanes - 1 generate
    -----------------------------------------------------------------------------
    -- demux rx_monitor inputs
    -----------------------------------------------------------------------------
    u_dp_demux_rx_monitor : entity dp_lib.dp_demux
    generic map(
      g_nof_output => g_nof_rx_monitors,
      g_sel_ctrl_invert => true
    )
    port map(
      rst => st_rst,
      clk => st_clk,

      snk_out => dp_demux_rx_monitor_siso_arr(I),
      snk_in  => dp_demux_rx_monitor_sosi_arr(I),

      src_in_arr  => rx_monitor_siso_2arr(I),
      src_out_arr => rx_monitor_sosi_2arr(I)
    );
    -----------------------------------------------------------------------------
    -- rx_monitors
    -----------------------------------------------------------------------------
    u_mms_dp_bsn_monitor_v2_rx : entity dp_lib.mms_dp_bsn_monitor_v2
    generic map(
      g_nof_streams => g_nof_rx_monitors
    )
    port map(
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,
      reg_mosi    => reg_bsn_monitor_v2_rx_mosi_arr(I),
      reg_miso    => reg_bsn_monitor_v2_rx_miso_arr(I),

      dp_rst      => st_rst,
      dp_clk      => st_clk,
      ref_sync    => bs_sosi.sync,

      in_siso_arr => rx_monitor_siso_2arr(I),
      in_sosi_arr => rx_monitor_sosi_2arr(I)
    );

    -----------------------------------------------------------------------------
    -- demux tx_monitor inputs
    -----------------------------------------------------------------------------
    u_dp_demux_tx_monitor : entity dp_lib.dp_demux
    generic map(
      g_nof_output => g_nof_tx_monitors,
      g_sel_ctrl_invert => true
    )
    port map(
      rst => st_rst,
      clk => st_clk,

      snk_out => dp_demux_tx_monitor_siso_arr(I),
      snk_in  => dp_demux_tx_monitor_sosi_arr(I),

      src_in_arr  => tx_monitor_siso_2arr(I),
      src_out_arr => tx_monitor_sosi_2arr(I)
    );

    -----------------------------------------------------------------------------
    -- tx_monitors
    -----------------------------------------------------------------------------
    u_mms_dp_bsn_monitor_v2_tx : entity dp_lib.mms_dp_bsn_monitor_v2
    generic map(
      g_nof_streams => g_nof_tx_monitors
    )
    port map(
      mm_rst      => mm_rst,
      mm_clk      => mm_clk,
      reg_mosi    => reg_bsn_monitor_v2_tx_mosi_arr(I),
      reg_miso    => reg_bsn_monitor_v2_tx_miso_arr(I),

      dp_rst      => st_rst,
      dp_clk      => st_clk,
      ref_sync    => bs_sosi.sync,

      in_siso_arr => tx_monitor_siso_2arr(I),
      in_sosi_arr => tx_monitor_sosi_2arr(I)
    );
  end generate;

  u_common_mem_mux_rx_monitors : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_lanes,
    g_mult_addr_w => ceil_log2(g_nof_rx_monitors) + 3
  )
  port map (
    mosi     => reg_bsn_monitor_v2_rx_mosi,
    miso     => reg_bsn_monitor_v2_rx_miso,
    mosi_arr => reg_bsn_monitor_v2_rx_mosi_arr,
    miso_arr => reg_bsn_monitor_v2_rx_miso_arr
  );

  u_common_mem_mux_tx_monitors : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_lanes,
    g_mult_addr_w => ceil_log2(g_nof_tx_monitors) + 3
  )
  port map (
    mosi     => reg_bsn_monitor_v2_tx_mosi,
    miso     => reg_bsn_monitor_v2_tx_miso,
    mosi_arr => reg_bsn_monitor_v2_tx_mosi_arr,
    miso_arr => reg_bsn_monitor_v2_tx_miso_arr
  );

  -----------------------------------------------------------------------------
  -- Design part, mms_diag_block_gen
  -----------------------------------------------------------------------------
  u_mms_diag_block_gen : entity diag_lib.mms_diag_block_gen
  generic map(
    g_nof_streams       => g_nof_lanes,
    g_use_bg_buffer_ram => true,
    g_buf_dat_w         => 32,  -- BG is limited to 32 bits data
    g_buf_addr_w        => 7,
    g_file_name_prefix  => "data/bf_in_data"
  )
  port map(
    -- System
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    dp_rst           => st_rst,
    dp_clk           => st_clk,
    en_sync          => st_pps,

    -- MM interface
    reg_bg_ctrl_mosi => reg_bg_ctrl_mosi,
    reg_bg_ctrl_miso => reg_bg_ctrl_miso,
    ram_bg_data_mosi => ram_bg_data_mosi,
    ram_bg_data_miso => ram_bg_data_miso,

    -- ST interface
    out_siso_arr     => local_siso_arr,
    out_sosi_arr     => local_sosi_arr
  );

  bs_sosi <= local_sosi_arr(0);

  u_mms_dp_xonoff_bg : entity dp_lib.mms_dp_xonoff
  generic map(
    g_nof_streams     => g_nof_lanes,
    g_combine_streams => false,
    g_default_value   => '0'
  )
  port map(
    -- Memory-mapped clock domain
    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    reg_mosi     => reg_dp_xonoff_bg_mosi,
    reg_miso     => reg_dp_xonoff_bg_miso,

    -- Streaming clock domain
    dp_rst      => st_rst,
    dp_clk      => st_clk,

    -- ST sinks
    snk_out_arr  => local_siso_arr,
    snk_in_arr   => local_sosi_arr,
    -- ST source
    src_in_arr   => dp_xonoff_bg_siso_arr,
    src_out_arr  => dp_xonoff_bg_sosi_arr
  );

  u_mms_dp_xonoff_from_lane : entity dp_lib.mms_dp_xonoff
  generic map(
    g_nof_streams     => g_nof_lanes,
    g_combine_streams => false,
    g_default_value   => '1'
  )
  port map(
    -- Memory-mapped clock domain
    mm_rst       => mm_rst,
    mm_clk       => mm_clk,

    reg_mosi     => reg_dp_xonoff_from_lane_mosi,
    reg_miso     => reg_dp_xonoff_from_lane_miso,

    -- Streaming clock domain
    dp_rst      => st_rst,
    dp_clk      => st_clk,

    -- ST sinks
    snk_out_arr  => from_lane_siso_arr(g_nof_lanes - 1 downto 0),
    snk_in_arr   => from_lane_sosi_arr(g_nof_lanes - 1 downto 0),
    -- ST source
    src_in_arr   => dp_xonoff_from_lane_siso_arr(g_nof_lanes - 1 downto 0),
    src_out_arr  => dp_xonoff_from_lane_sosi_arr(g_nof_lanes - 1 downto 0)
  );

  gen_streams : for I in 0 to g_nof_lanes - 1 generate
    -- Multiplex the inputs:
    -- . [0] = from lane sosi
    -- . [1] = BG
    dp_xonoff_from_lane_siso_arr(I) <= mux_snk_out_2arr_2(I)(0);
    dp_xonoff_bg_siso_arr(I)        <= mux_snk_out_2arr_2(I)(1);

    mux_snk_in_2arr_2(I)(0) <= dp_xonoff_from_lane_sosi_arr(I);
    mux_snk_in_2arr_2(I)(1) <= dp_xonoff_bg_sosi_arr(I);

    u_dp_mux : entity dp_lib.dp_mux
    generic map (
      g_technology        => g_technology,
      -- MUX
      g_mode              => 0,
      g_nof_input         => 2,
      g_append_channel_lo => false,
      g_sel_ctrl_invert   => true,  -- Use default FALSE when stream array IO are indexed (0 TO g_nof_input-1), else use TRUE when indexed (g_nof_input-1 DOWNTO 0)
      -- Input FIFO
      g_use_fifo          => false,
      g_fifo_size         => array_init(1024, 2),  -- must match g_nof_input, even when g_use_fifo=FALSE
      g_fifo_fill         => array_init(   0, 2)  -- must match g_nof_input, even when g_use_fifo=FALSE
    )
    port map (
      rst         => st_rst,
      clk         => st_clk,
      -- ST sinks
      snk_out_arr => mux_snk_out_2arr_2(I),  -- [c_mux_nof_input-1:0]
      snk_in_arr  => mux_snk_in_2arr_2(I),  -- [c_mux_nof_input-1:0]
      -- ST source
      src_in      => to_lane_siso_arr(I),
      src_out     => to_lane_sosi_arr(I)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- SDP Info register
  -----------------------------------------------------------------------------
  gn_index <= TO_UINT(ID(c_sdp_W_gn_id - 1 downto 0));
  this_rn_id <= TO_UVEC(gn_index - TO_UINT(sdp_info.O_rn), c_sdp_W_gn_id);
  u_sdp_info : entity lofar2_sdp_lib.sdp_info
  port map(
    -- Clocks and reset
    mm_rst    => mm_rst,  -- reset synchronous with mm_clk
    mm_clk    => mm_clk,  -- memory-mapped bus clock

    dp_clk    => st_clk,
    dp_rst    => st_rst,

    reg_mosi  => reg_sdp_info_mosi,
    reg_miso  => reg_sdp_info_miso,

    -- inputs from other blocks
    gn_index  => gn_index,
    f_adc     => '1',
    fsub_type => '0',

    -- sdp info
    sdp_info => sdp_info
  );

  -----------------------------------------------------------------------------
  -- Freeze wrapper instantiation
  -----------------------------------------------------------------------------
  gen_opencl: if g_sim = false generate
  freeze_wrapper_inst : freeze_wrapper
  port map(
    board_kernel_clk_clk                        => board_kernel_clk_clk,
    board_kernel_clk2x_clk                      => board_kernel_clk2x_clk,
    board_kernel_reset_reset_n                  => board_kernel_reset_reset_n_in,
    board_kernel_irq_irq                        => board_kernel_irq_irq,
    board_kernel_cra_waitrequest                => board_kernel_cra_waitrequest,
    board_kernel_cra_readdata                   => board_kernel_cra_readdata,
    board_kernel_cra_readdatavalid              => board_kernel_cra_readdatavalid,
    board_kernel_cra_burstcount                 => board_kernel_cra_burstcount,
    board_kernel_cra_writedata                  => board_kernel_cra_writedata,
    board_kernel_cra_address                    => board_kernel_cra_address,
    board_kernel_cra_write                      => board_kernel_cra_write,
    board_kernel_cra_read                       => board_kernel_cra_read,
    board_kernel_cra_byteenable                 => board_kernel_cra_byteenable,
    board_kernel_cra_debugaccess                => board_kernel_cra_debugaccess,

    board_kernel_register_mem_address           => board_kernel_register_mem_address,
    board_kernel_register_mem_clken             => board_kernel_register_mem_clken,
    board_kernel_register_mem_chipselect        => board_kernel_register_mem_chipselect,
    board_kernel_register_mem_write             => board_kernel_register_mem_write,
    board_kernel_register_mem_readdata          => board_kernel_register_mem_readdata,
    board_kernel_register_mem_writedata         => board_kernel_register_mem_writedata,
    board_kernel_register_mem_byteenable        => board_kernel_register_mem_byteenable,

    board_kernel_stream_src_10GbE_ring_0_data   => ta2_unb2b_10GbE_ring_ch_src_out_arr(0).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_ring_0_valid  => ta2_unb2b_10GbE_ring_ch_src_out_arr(0).valid,
    board_kernel_stream_src_10GbE_ring_0_ready  => ta2_unb2b_10GbE_ring_ch_src_in_arr(0).ready,
    board_kernel_stream_snk_10GbE_ring_0_data   => ta2_unb2b_10GbE_ring_ch_snk_in_arr(0).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_ring_0_valid  => ta2_unb2b_10GbE_ring_ch_snk_in_arr(0).valid,
    board_kernel_stream_snk_10GbE_ring_0_ready  => ta2_unb2b_10GbE_ring_ch_snk_out_arr(0).ready,

    board_kernel_stream_src_10GbE_ring_1_data   => ta2_unb2b_10GbE_ring_ch_src_out_arr(1).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_ring_1_valid  => ta2_unb2b_10GbE_ring_ch_src_out_arr(1).valid,
    board_kernel_stream_src_10GbE_ring_1_ready  => ta2_unb2b_10GbE_ring_ch_src_in_arr(1).ready,
    board_kernel_stream_snk_10GbE_ring_1_data   => ta2_unb2b_10GbE_ring_ch_snk_in_arr(1).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_ring_1_valid  => ta2_unb2b_10GbE_ring_ch_snk_in_arr(1).valid,
    board_kernel_stream_snk_10GbE_ring_1_ready  => ta2_unb2b_10GbE_ring_ch_snk_out_arr(1).ready,

    board_kernel_stream_src_10GbE_ring_2_data   => ta2_unb2b_10GbE_ring_ch_src_out_arr(2).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_ring_2_valid  => ta2_unb2b_10GbE_ring_ch_src_out_arr(2).valid,
    board_kernel_stream_src_10GbE_ring_2_ready  => ta2_unb2b_10GbE_ring_ch_src_in_arr(2).ready,
    board_kernel_stream_snk_10GbE_ring_2_data   => ta2_unb2b_10GbE_ring_ch_snk_in_arr(2).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_ring_2_valid  => ta2_unb2b_10GbE_ring_ch_snk_in_arr(2).valid,
    board_kernel_stream_snk_10GbE_ring_2_ready  => ta2_unb2b_10GbE_ring_ch_snk_out_arr(2).ready,

    board_kernel_stream_src_10GbE_ring_3_data   => ta2_unb2b_10GbE_ring_ch_src_out_arr(3).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_ring_3_valid  => ta2_unb2b_10GbE_ring_ch_src_out_arr(3).valid,
    board_kernel_stream_src_10GbE_ring_3_ready  => ta2_unb2b_10GbE_ring_ch_src_in_arr(3).ready,
    board_kernel_stream_snk_10GbE_ring_3_data   => ta2_unb2b_10GbE_ring_ch_snk_in_arr(3).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_ring_3_valid  => ta2_unb2b_10GbE_ring_ch_snk_in_arr(3).valid,
    board_kernel_stream_snk_10GbE_ring_3_ready  => ta2_unb2b_10GbE_ring_ch_snk_out_arr(3).ready,

    board_kernel_stream_src_10GbE_ring_4_data   => ta2_unb2b_10GbE_ring_ch_src_out_arr(4).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_ring_4_valid  => ta2_unb2b_10GbE_ring_ch_src_out_arr(4).valid,
    board_kernel_stream_src_10GbE_ring_4_ready  => ta2_unb2b_10GbE_ring_ch_src_in_arr(4).ready,
    board_kernel_stream_snk_10GbE_ring_4_data   => ta2_unb2b_10GbE_ring_ch_snk_in_arr(4).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_ring_4_valid  => ta2_unb2b_10GbE_ring_ch_snk_in_arr(4).valid,
    board_kernel_stream_snk_10GbE_ring_4_ready  => ta2_unb2b_10GbE_ring_ch_snk_out_arr(4).ready,

    board_kernel_stream_src_10GbE_ring_5_data   => ta2_unb2b_10GbE_ring_ch_src_out_arr(5).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_ring_5_valid  => ta2_unb2b_10GbE_ring_ch_src_out_arr(5).valid,
    board_kernel_stream_src_10GbE_ring_5_ready  => ta2_unb2b_10GbE_ring_ch_src_in_arr(5).ready,
    board_kernel_stream_snk_10GbE_ring_5_data   => ta2_unb2b_10GbE_ring_ch_snk_in_arr(5).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_ring_5_valid  => ta2_unb2b_10GbE_ring_ch_snk_in_arr(5).valid,
    board_kernel_stream_snk_10GbE_ring_5_ready  => ta2_unb2b_10GbE_ring_ch_snk_out_arr(5).ready,

    board_kernel_stream_src_10GbE_ring_6_data   => ta2_unb2b_10GbE_ring_ch_src_out_arr(6).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_ring_6_valid  => ta2_unb2b_10GbE_ring_ch_src_out_arr(6).valid,
    board_kernel_stream_src_10GbE_ring_6_ready  => ta2_unb2b_10GbE_ring_ch_src_in_arr(6).ready,
    board_kernel_stream_snk_10GbE_ring_6_data   => ta2_unb2b_10GbE_ring_ch_snk_in_arr(6).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_ring_6_valid  => ta2_unb2b_10GbE_ring_ch_snk_in_arr(6).valid,
    board_kernel_stream_snk_10GbE_ring_6_ready  => ta2_unb2b_10GbE_ring_ch_snk_out_arr(6).ready,

    board_kernel_stream_src_10GbE_ring_7_data   => ta2_unb2b_10GbE_ring_ch_src_out_arr(7).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_ring_7_valid  => ta2_unb2b_10GbE_ring_ch_src_out_arr(7).valid,
    board_kernel_stream_src_10GbE_ring_7_ready  => ta2_unb2b_10GbE_ring_ch_src_in_arr(7).ready,
    board_kernel_stream_snk_10GbE_ring_7_data   => ta2_unb2b_10GbE_ring_ch_snk_in_arr(7).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_ring_7_valid  => ta2_unb2b_10GbE_ring_ch_snk_in_arr(7).valid,
    board_kernel_stream_snk_10GbE_ring_7_ready  => ta2_unb2b_10GbE_ring_ch_snk_out_arr(7).ready,

    board_kernel_stream_src_10GbE_qsfp_0_data   => ta2_unb2b_10GbE_qsfp_src_out_arr(0).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_qsfp_0_valid  => ta2_unb2b_10GbE_qsfp_src_out_arr(0).valid,
    board_kernel_stream_src_10GbE_qsfp_0_ready  => ta2_unb2b_10GbE_qsfp_src_in_arr(0).ready,
    board_kernel_stream_snk_10GbE_qsfp_0_data   => ta2_unb2b_10GbE_qsfp_snk_in_arr(0).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_qsfp_0_valid  => ta2_unb2b_10GbE_qsfp_snk_in_arr(0).valid,
    board_kernel_stream_snk_10GbE_qsfp_0_ready  => ta2_unb2b_10GbE_qsfp_snk_out_arr(0).ready,

    board_kernel_stream_src_10GbE_qsfp_1_data   => ta2_unb2b_10GbE_qsfp_src_out_arr(1).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_qsfp_1_valid  => ta2_unb2b_10GbE_qsfp_src_out_arr(1).valid,
    board_kernel_stream_src_10GbE_qsfp_1_ready  => ta2_unb2b_10GbE_qsfp_src_in_arr(1).ready,
    board_kernel_stream_snk_10GbE_qsfp_1_data   => ta2_unb2b_10GbE_qsfp_snk_in_arr(1).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_qsfp_1_valid  => ta2_unb2b_10GbE_qsfp_snk_in_arr(1).valid,
    board_kernel_stream_snk_10GbE_qsfp_1_ready  => ta2_unb2b_10GbE_qsfp_snk_out_arr(1).ready,

    board_kernel_stream_src_10GbE_qsfp_2_data   => ta2_unb2b_10GbE_qsfp_src_out_arr(2).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_qsfp_2_valid  => ta2_unb2b_10GbE_qsfp_src_out_arr(2).valid,
    board_kernel_stream_src_10GbE_qsfp_2_ready  => ta2_unb2b_10GbE_qsfp_src_in_arr(2).ready,
    board_kernel_stream_snk_10GbE_qsfp_2_data   => ta2_unb2b_10GbE_qsfp_snk_in_arr(2).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_qsfp_2_valid  => ta2_unb2b_10GbE_qsfp_snk_in_arr(2).valid,
    board_kernel_stream_snk_10GbE_qsfp_2_ready  => ta2_unb2b_10GbE_qsfp_snk_out_arr(2).ready,

    board_kernel_stream_src_10GbE_qsfp_3_data   => ta2_unb2b_10GbE_qsfp_src_out_arr(3).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_src_10GbE_qsfp_3_valid  => ta2_unb2b_10GbE_qsfp_src_out_arr(3).valid,
    board_kernel_stream_src_10GbE_qsfp_3_ready  => ta2_unb2b_10GbE_qsfp_src_in_arr(3).ready,
    board_kernel_stream_snk_10GbE_qsfp_3_data   => ta2_unb2b_10GbE_qsfp_snk_in_arr(3).data(c_kernel_10gbe_channel_w - 1 downto 0),
    board_kernel_stream_snk_10GbE_qsfp_3_valid  => ta2_unb2b_10GbE_qsfp_snk_in_arr(3).valid,
    board_kernel_stream_snk_10GbE_qsfp_3_ready  => ta2_unb2b_10GbE_qsfp_snk_out_arr(3).ready,

    board_kernel_stream_src_lane_0_data         => kernel_to_lane_sosi_arr(0).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_lane_0_valid        => kernel_to_lane_sosi_arr(0).valid,
    board_kernel_stream_src_lane_0_ready        => kernel_to_lane_siso_arr(0).ready,
    board_kernel_stream_snk_lane_0_data         => kernel_from_lane_sosi_arr(0).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_lane_0_valid        => kernel_from_lane_sosi_arr(0).valid,
    board_kernel_stream_snk_lane_0_ready        => kernel_from_lane_siso_arr(0).ready,

    board_kernel_stream_src_lane_1_data         => kernel_to_lane_sosi_arr(1).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_lane_1_valid        => kernel_to_lane_sosi_arr(1).valid,
    board_kernel_stream_src_lane_1_ready        => kernel_to_lane_siso_arr(1).ready,
    board_kernel_stream_snk_lane_1_data         => kernel_from_lane_sosi_arr(1).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_lane_1_valid        => kernel_from_lane_sosi_arr(1).valid,
    board_kernel_stream_snk_lane_1_ready        => kernel_from_lane_siso_arr(1).ready,

    board_kernel_stream_src_lane_2_data         => kernel_to_lane_sosi_arr(2).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_lane_2_valid        => kernel_to_lane_sosi_arr(2).valid,
    board_kernel_stream_src_lane_2_ready        => kernel_to_lane_siso_arr(2).ready,
    board_kernel_stream_snk_lane_2_data         => kernel_from_lane_sosi_arr(2).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_lane_2_valid        => kernel_from_lane_sosi_arr(2).valid,
    board_kernel_stream_snk_lane_2_ready        => kernel_from_lane_siso_arr(2).ready,

    board_kernel_stream_src_lane_3_data         => kernel_to_lane_sosi_arr(3).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_lane_3_valid        => kernel_to_lane_sosi_arr(3).valid,
    board_kernel_stream_src_lane_3_ready        => kernel_to_lane_siso_arr(3).ready,
    board_kernel_stream_snk_lane_3_data         => kernel_from_lane_sosi_arr(3).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_lane_3_valid        => kernel_from_lane_sosi_arr(3).valid,
    board_kernel_stream_snk_lane_3_ready        => kernel_from_lane_siso_arr(3).ready,

    board_kernel_stream_src_lane_4_data         => kernel_to_lane_sosi_arr(4).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_lane_4_valid        => kernel_to_lane_sosi_arr(4).valid,
    board_kernel_stream_src_lane_4_ready        => kernel_to_lane_siso_arr(4).ready,
    board_kernel_stream_snk_lane_4_data         => kernel_from_lane_sosi_arr(4).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_lane_4_valid        => kernel_from_lane_sosi_arr(4).valid,
    board_kernel_stream_snk_lane_4_ready        => kernel_from_lane_siso_arr(4).ready,

    board_kernel_stream_src_lane_5_data         => kernel_to_lane_sosi_arr(5).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_lane_5_valid        => kernel_to_lane_sosi_arr(5).valid,
    board_kernel_stream_src_lane_5_ready        => kernel_to_lane_siso_arr(5).ready,
    board_kernel_stream_snk_lane_5_data         => kernel_from_lane_sosi_arr(5).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_lane_5_valid        => kernel_from_lane_sosi_arr(5).valid,
    board_kernel_stream_snk_lane_5_ready        => kernel_from_lane_siso_arr(5).ready,

    board_kernel_stream_src_lane_6_data         => kernel_to_lane_sosi_arr(6).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_lane_6_valid        => kernel_to_lane_sosi_arr(6).valid,
    board_kernel_stream_src_lane_6_ready        => kernel_to_lane_siso_arr(6).ready,
    board_kernel_stream_snk_lane_6_data         => kernel_from_lane_sosi_arr(6).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_lane_6_valid        => kernel_from_lane_sosi_arr(6).valid,
    board_kernel_stream_snk_lane_6_ready        => kernel_from_lane_siso_arr(6).ready,

    board_kernel_stream_src_lane_7_data         => kernel_to_lane_sosi_arr(7).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_lane_7_valid        => kernel_to_lane_sosi_arr(7).valid,
    board_kernel_stream_src_lane_7_ready        => kernel_to_lane_siso_arr(7).ready,
    board_kernel_stream_snk_lane_7_data         => kernel_from_lane_sosi_arr(7).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_lane_7_valid        => kernel_from_lane_sosi_arr(7).valid,
    board_kernel_stream_snk_lane_7_ready        => kernel_from_lane_siso_arr(7).ready,

    board_kernel_stream_snk_rx_monitor_0_data   => kernel_rx_monitor_sosi_arr(0).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_rx_monitor_0_valid  => kernel_rx_monitor_sosi_arr(0).valid,
    board_kernel_stream_snk_rx_monitor_0_ready  => kernel_rx_monitor_siso_arr(0).ready,
    board_kernel_stream_snk_tx_monitor_0_data   => kernel_tx_monitor_sosi_arr(0).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_tx_monitor_0_valid  => kernel_tx_monitor_sosi_arr(0).valid,
    board_kernel_stream_snk_tx_monitor_0_ready  => kernel_tx_monitor_siso_arr(0).ready,

    board_kernel_stream_snk_rx_monitor_1_data   => kernel_rx_monitor_sosi_arr(1).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_rx_monitor_1_valid  => kernel_rx_monitor_sosi_arr(1).valid,
    board_kernel_stream_snk_rx_monitor_1_ready  => kernel_rx_monitor_siso_arr(1).ready,
    board_kernel_stream_snk_tx_monitor_1_data   => kernel_tx_monitor_sosi_arr(1).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_tx_monitor_1_valid  => kernel_tx_monitor_sosi_arr(1).valid,
    board_kernel_stream_snk_tx_monitor_1_ready  => kernel_tx_monitor_siso_arr(1).ready,

    board_kernel_stream_snk_rx_monitor_2_data   => kernel_rx_monitor_sosi_arr(2).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_rx_monitor_2_valid  => kernel_rx_monitor_sosi_arr(2).valid,
    board_kernel_stream_snk_rx_monitor_2_ready  => kernel_rx_monitor_siso_arr(2).ready,
    board_kernel_stream_snk_tx_monitor_2_data   => kernel_tx_monitor_sosi_arr(2).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_tx_monitor_2_valid  => kernel_tx_monitor_sosi_arr(2).valid,
    board_kernel_stream_snk_tx_monitor_2_ready  => kernel_tx_monitor_siso_arr(2).ready,

    board_kernel_stream_snk_rx_monitor_3_data   => kernel_rx_monitor_sosi_arr(3).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_rx_monitor_3_valid  => kernel_rx_monitor_sosi_arr(3).valid,
    board_kernel_stream_snk_rx_monitor_3_ready  => kernel_rx_monitor_siso_arr(3).ready,
    board_kernel_stream_snk_tx_monitor_3_data   => kernel_tx_monitor_sosi_arr(3).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_tx_monitor_3_valid  => kernel_tx_monitor_sosi_arr(3).valid,
    board_kernel_stream_snk_tx_monitor_3_ready  => kernel_tx_monitor_siso_arr(3).ready,

    board_kernel_stream_snk_rx_monitor_4_data   => kernel_rx_monitor_sosi_arr(4).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_rx_monitor_4_valid  => kernel_rx_monitor_sosi_arr(4).valid,
    board_kernel_stream_snk_rx_monitor_4_ready  => kernel_rx_monitor_siso_arr(4).ready,
    board_kernel_stream_snk_tx_monitor_4_data   => kernel_tx_monitor_sosi_arr(4).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_tx_monitor_4_valid  => kernel_tx_monitor_sosi_arr(4).valid,
    board_kernel_stream_snk_tx_monitor_4_ready  => kernel_tx_monitor_siso_arr(4).ready,

    board_kernel_stream_snk_rx_monitor_5_data   => kernel_rx_monitor_sosi_arr(5).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_rx_monitor_5_valid  => kernel_rx_monitor_sosi_arr(5).valid,
    board_kernel_stream_snk_rx_monitor_5_ready  => kernel_rx_monitor_siso_arr(5).ready,
    board_kernel_stream_snk_tx_monitor_5_data   => kernel_tx_monitor_sosi_arr(5).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_tx_monitor_5_valid  => kernel_tx_monitor_sosi_arr(5).valid,
    board_kernel_stream_snk_tx_monitor_5_ready  => kernel_tx_monitor_siso_arr(5).ready,

    board_kernel_stream_snk_rx_monitor_6_data   => kernel_rx_monitor_sosi_arr(6).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_rx_monitor_6_valid  => kernel_rx_monitor_sosi_arr(6).valid,
    board_kernel_stream_snk_rx_monitor_6_ready  => kernel_rx_monitor_siso_arr(6).ready,
    board_kernel_stream_snk_tx_monitor_6_data   => kernel_tx_monitor_sosi_arr(6).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_tx_monitor_6_valid  => kernel_tx_monitor_sosi_arr(6).valid,
    board_kernel_stream_snk_tx_monitor_6_ready  => kernel_tx_monitor_siso_arr(6).ready,

    board_kernel_stream_snk_rx_monitor_7_data   => kernel_rx_monitor_sosi_arr(7).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_rx_monitor_7_valid  => kernel_rx_monitor_sosi_arr(7).valid,
    board_kernel_stream_snk_rx_monitor_7_ready  => kernel_rx_monitor_siso_arr(7).ready,
    board_kernel_stream_snk_tx_monitor_7_data   => kernel_tx_monitor_sosi_arr(7).data(c_kernel_lane_sosi_channel_w - 1 downto 0),
    board_kernel_stream_snk_tx_monitor_7_valid  => kernel_tx_monitor_sosi_arr(7).valid,
    board_kernel_stream_snk_tx_monitor_7_ready  => kernel_tx_monitor_siso_arr(7).ready,

    board_kernel_stream_src_bs_data             => kernel_bs_sosi.data(c_kernel_bs_sosi_channel_w - 1 downto 0),
    board_kernel_stream_src_bs_valid            => kernel_bs_sosi.valid,
    board_kernel_stream_src_bs_ready            => OPEN,

    board_kernel_stream_src_mm_io_data          => ta2_unb2b_mm_io_src_out.data(c_kernel_mm_io_mosi_channel_w - 1 downto 0),
    board_kernel_stream_src_mm_io_valid         => ta2_unb2b_mm_io_src_out.valid,
    board_kernel_stream_src_mm_io_ready         => ta2_unb2b_mm_io_src_in.ready,
    board_kernel_stream_snk_mm_io_data          => ta2_unb2b_mm_io_snk_in.data(c_kernel_mm_io_miso_channel_w - 1 downto 0),
    board_kernel_stream_snk_mm_io_valid         => ta2_unb2b_mm_io_snk_in.valid,
    board_kernel_stream_snk_mm_io_ready         => ta2_unb2b_mm_io_snk_out.ready

  );

    i_kernel_rst <= not board_kernel_reset_reset_n;  -- qsys output used to reset all OpenCL BSP components
  end generate;

  gen_sim: if g_sim = true generate
    i_kernel_rst <= not i_reset_n;
    board_kernel_clk_clk <= st_clk;

    u_mm_file_reg_sdp_info           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, g_sim_node_nr, c_sim_node_type) & "REG_SDP_INFO")
                                              port map(mm_rst, mm_clk, reg_sdp_info_mosi, reg_sdp_info_miso );
    u_mm_file_reg_dp_xonoff_bg       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, g_sim_node_nr, c_sim_node_type) & "REG_DP_XONOFF_BG")
                                              port map(mm_rst, mm_clk, reg_dp_xonoff_bg_mosi, reg_dp_xonoff_bg_miso );
    u_mm_file_reg_dp_xonoff_from_lane: mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, g_sim_node_nr, c_sim_node_type) & "REG_DP_XONOFF_FROM_LANE")
                                              port map(mm_rst, mm_clk, reg_dp_xonoff_from_lane_mosi, reg_dp_xonoff_from_lane_miso );
    u_mm_file_reg_bsn_monitor_rx     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, g_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RX")
                                                port map(mm_rst, mm_clk, reg_bsn_monitor_v2_rx_mosi, reg_bsn_monitor_v2_rx_miso );
    u_mm_file_reg_bsn_monitor_tx     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, g_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_TX")
                                                port map(mm_rst, mm_clk, reg_bsn_monitor_v2_tx_mosi, reg_bsn_monitor_v2_tx_miso );
    u_mm_file_reg_bg_ctrl            : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, g_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG_RING")
                                              port map(mm_rst, mm_clk, reg_bg_ctrl_mosi, reg_bg_ctrl_miso );
  end generate;

  i_reset_n <= not mm_rst;  -- First reset OpenCL components in qsys (board)
  -- Kernel should start later than BSP. Delaying the reset from the qsys output to form the reset of the OpenCL kernel.
  -- This way it is ensured the OpenCL kernel does not start reading/writing data before the components in the OpenCL BSP are ready.
  u_common_areset : entity common_lib.common_areset
  generic map (
    g_rst_level => '0',
    g_delay_len => 9
  )
  port map (
    in_rst  => i_kernel_rst,
    clk     => board_kernel_clk_clk,
    out_rst => board_kernel_reset_reset_n_in
  );
-----------------------------------------------------------------------------
  -- General control function
  -----------------------------------------------------------------------------
  u_ctrl_unb2b_board : entity unb2b_board_lib.ctrl_unb2b_board
  generic map (
    g_sim                     => g_sim,
    g_technology              => g_technology,
    g_design_name             => g_design_name,
    g_design_note             => g_design_note,
    g_stamp_date              => g_stamp_date,
    g_stamp_time              => g_stamp_time,
    g_revision_id             => g_revision_id,
    g_fw_version              => c_fw_version,
    g_mm_clk_freq             => c_mm_clk_freq,
    g_eth_clk_freq            => c_unb2b_board_eth_clk_freq_125M,
    g_aux                     => c_unb2b_board_aux,
    g_factory_image           => g_factory_image,
    g_protect_addr_range      => g_protect_addr_range
  )
  port map (
    -- Clock an reset signals
    cs_sim                   => cs_sim,
    xo_ethclk                => xo_ethclk,
    xo_rst                   => xo_rst,
    xo_rst_n                 => xo_rst_n,

    mm_clk                   => mm_clk,
    mm_rst                   => mm_rst,

    dp_rst                   => st_rst,
    dp_clk                   => st_clk,
    dp_pps                   => st_pps,
    dp_rst_in                => st_rst,
    dp_clk_in                => st_clk,

    -- Toggle WDI
    pout_wdi                 => pout_wdi,

    -- MM buses
    -- REMU
    reg_remu_mosi            => reg_remu_mosi,
    reg_remu_miso            => reg_remu_miso,

    -- EPCS read
    reg_dpmm_data_mosi       => reg_dpmm_data_mosi,
    reg_dpmm_data_miso       => reg_dpmm_data_miso,
    reg_dpmm_ctrl_mosi       => reg_dpmm_ctrl_mosi,
    reg_dpmm_ctrl_miso       => reg_dpmm_ctrl_miso,

    -- EPCS write
    reg_mmdp_data_mosi       => reg_mmdp_data_mosi,
    reg_mmdp_data_miso       => reg_mmdp_data_miso,
    reg_mmdp_ctrl_mosi       => reg_mmdp_ctrl_mosi,
    reg_mmdp_ctrl_miso       => reg_mmdp_ctrl_miso,

    -- EPCS status/control
    reg_epcs_mosi            => reg_epcs_mosi,
    reg_epcs_miso            => reg_epcs_miso,

    -- . Manual WDI override
    reg_wdi_mosi             => reg_wdi_mosi,
    reg_wdi_miso             => reg_wdi_miso,

    -- . System_info
    reg_unb_system_info_mosi => reg_unb_system_info_mosi,
    reg_unb_system_info_miso => reg_unb_system_info_miso,
    rom_unb_system_info_mosi => rom_unb_system_info_mosi,
    rom_unb_system_info_miso => rom_unb_system_info_miso,

    -- . UniBoard I2C sensors
    reg_unb_sens_mosi        => reg_unb_sens_mosi,
    reg_unb_sens_miso        => reg_unb_sens_miso,

    -- . FPGA sensors
    reg_fpga_temp_sens_mosi  => reg_fpga_temp_sens_mosi,
    reg_fpga_temp_sens_miso  => reg_fpga_temp_sens_miso,
    reg_fpga_voltage_sens_mosi  => reg_fpga_voltage_sens_mosi,
    reg_fpga_voltage_sens_miso  => reg_fpga_voltage_sens_miso,

    reg_unb_pmbus_mosi       => reg_unb_pmbus_mosi,
    reg_unb_pmbus_miso       => reg_unb_pmbus_miso,

    -- . PPSH
    reg_ppsh_mosi            => reg_ppsh_mosi,
    reg_ppsh_miso            => reg_ppsh_miso,

    -- eth1g
    eth1g_mm_rst             => eth1g_mm_rst,
    eth1g_tse_mosi           => eth1g_tse_mosi,
    eth1g_tse_miso           => eth1g_tse_miso,
    eth1g_reg_mosi           => eth1g_reg_mosi,
    eth1g_reg_miso           => eth1g_reg_miso,
    eth1g_reg_interrupt      => eth1g_reg_interrupt,
    eth1g_ram_mosi           => eth1g_ram_mosi,
    eth1g_ram_miso           => eth1g_ram_miso,

    -- RAM scrap
    ram_scrap_mosi           => ram_scrap_mosi,
    ram_scrap_miso           => ram_scrap_miso,

    -- FPGA pins
    -- . General
    CLK                      => CLK,
    PPS                      => PPS,
    WDI                      => WDI,
    INTA                     => INTA,
    INTB                     => INTB,
    -- . Others
    VERSION                  => VERSION,
    ID                       => ID,
    TESTIO                   => TESTIO,
    -- . I2C Interface to Sensors
    SENS_SC                  => SENS_SC,
    SENS_SD                  => SENS_SD,
    -- PM bus
    PMBUS_SC                 => PMBUS_SC,
    PMBUS_SD                 => PMBUS_SD,
    PMBUS_ALERT              => PMBUS_ALERT,

    -- . 1GbE Control Interface
    ETH_clk                  => ETH_CLK,
    ETH_SGIN                 => ETH_SGIN,
    ETH_SGOUT                => ETH_SGOUT
  );

  -----------------------------------------------------------------------------
  -- Board qsys
  -----------------------------------------------------------------------------
  gen_board: if g_sim = false generate
  board_inst : board
  port map (
      clk_clk                                   => mm_clk,
      reset_reset_n                             => i_reset_n,

      kernel_clk_clk                            => board_kernel_clk_clk,
      kernel_clk2x_clk                          => board_kernel_clk2x_clk,
      kernel_reset_reset_n                      => board_kernel_reset_reset_n,

      kernel_interface_sw_reset_in_reset        => mm_rst,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2b_board.
      pio_wdi_external_connection_export        => pout_wdi,

      avs_eth_0_reset_export                    => eth1g_mm_rst,
      avs_eth_0_tse_address_export              => eth1g_tse_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                => eth1g_tse_mosi.wr,
      avs_eth_0_tse_read_export                 => eth1g_tse_mosi.rd,
      avs_eth_0_tse_writedata_export            => eth1g_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export             => eth1g_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export          => eth1g_tse_miso.waitrequest,
      avs_eth_0_reg_address_export              => eth1g_reg_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                => eth1g_reg_mosi.wr,
      avs_eth_0_reg_read_export                 => eth1g_reg_mosi.rd,
      avs_eth_0_reg_writedata_export            => eth1g_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export             => eth1g_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export              => eth1g_ram_mosi.address(c_unb2b_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                => eth1g_ram_mosi.wr,
      avs_eth_0_ram_read_export                 => eth1g_ram_mosi.rd,
      avs_eth_0_ram_writedata_export            => eth1g_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export             => eth1g_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                      => eth1g_reg_interrupt,

      reg_unb_sens_address_export               => reg_unb_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_sens_adr_w - 1 downto 0),
      reg_unb_sens_write_export                 => reg_unb_sens_mosi.wr,
      reg_unb_sens_writedata_export             => reg_unb_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_sens_read_export                  => reg_unb_sens_mosi.rd,
      reg_unb_sens_readdata_export              => reg_unb_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_unb_pmbus_address_export              => reg_unb_pmbus_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_pmbus_adr_w - 1 downto 0),
      reg_unb_pmbus_write_export                => reg_unb_pmbus_mosi.wr,
      reg_unb_pmbus_writedata_export            => reg_unb_pmbus_mosi.wrdata(c_word_w - 1 downto 0),
      reg_unb_pmbus_read_export                 => reg_unb_pmbus_mosi.rd,
      reg_unb_pmbus_readdata_export             => reg_unb_pmbus_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_temp_sens_address_export         => reg_fpga_temp_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export           => reg_fpga_temp_sens_mosi.wr,
      reg_fpga_temp_sens_writedata_export       => reg_fpga_temp_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export            => reg_fpga_temp_sens_mosi.rd,
      reg_fpga_temp_sens_readdata_export        => reg_fpga_temp_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_address_export      => reg_fpga_voltage_sens_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export        => reg_fpga_voltage_sens_mosi.wr,
      reg_fpga_voltage_sens_writedata_export    => reg_fpga_voltage_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export         => reg_fpga_voltage_sens_mosi.rd,
      reg_fpga_voltage_sens_readdata_export     => reg_fpga_voltage_sens_miso.rddata(c_word_w - 1 downto 0),

      rom_system_info_address_export            => rom_unb_system_info_mosi.address(c_unb2b_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 4 downto 0),  -- temp fix
      rom_system_info_write_export              => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export          => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export               => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export           => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_system_info_address_export            => reg_unb_system_info_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export              => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export          => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export               => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export           => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_pps_address_export                    => reg_ppsh_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 2 downto 0),  -- temp fix
      pio_pps_write_export                      => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                  => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                       => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                   => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),

      reg_wdi_address_export                    => reg_wdi_mosi.address(0 downto 0),
      reg_wdi_write_export                      => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                  => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                       => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                   => reg_wdi_miso.rddata(c_word_w - 1 downto 0),

      reg_remu_address_export                   => reg_remu_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                     => reg_remu_mosi.wr,
      reg_remu_writedata_export                 => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                      => reg_remu_mosi.rd,
      reg_remu_readdata_export                  => reg_remu_miso.rddata(c_word_w - 1 downto 0),

      reg_epcs_address_export                   => reg_epcs_mosi.address(c_unb2b_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                     => reg_epcs_mosi.wr,
      reg_epcs_writedata_export                 => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                      => reg_epcs_mosi.rd,
      reg_epcs_readdata_export                  => reg_epcs_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_address_export              => reg_dpmm_ctrl_mosi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                => reg_dpmm_ctrl_mosi.wr,
      reg_dpmm_ctrl_writedata_export            => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                 => reg_dpmm_ctrl_mosi.rd,
      reg_dpmm_ctrl_readdata_export             => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_address_export              => reg_mmdp_data_mosi.address(0 downto 0),
      reg_mmdp_data_write_export                => reg_mmdp_data_mosi.wr,
      reg_mmdp_data_writedata_export            => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                 => reg_mmdp_data_mosi.rd,
      reg_mmdp_data_readdata_export             => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_address_export              => reg_dpmm_data_mosi.address(0 downto 0),
      reg_dpmm_data_read_export                 => reg_dpmm_data_mosi.rd,
      reg_dpmm_data_readdata_export             => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                => reg_dpmm_data_mosi.wr,
      reg_dpmm_data_writedata_export            => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_address_export              => reg_mmdp_ctrl_mosi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                 => reg_mmdp_ctrl_mosi.rd,
      reg_mmdp_ctrl_readdata_export             => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                => reg_mmdp_ctrl_mosi.wr,
      reg_mmdp_ctrl_writedata_export            => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      ram_scrap_address_export                  => ram_scrap_mosi.address(8 downto 0),
      ram_scrap_read_export                     => ram_scrap_mosi.rd,
      ram_scrap_readdata_export                 => ram_scrap_miso.rddata(c_word_w - 1 downto 0),
      ram_scrap_write_export                    => ram_scrap_mosi.wr,
      ram_scrap_writedata_export                => ram_scrap_mosi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_rx_address_export      => reg_bsn_monitor_v2_rx_mosi.address(9 downto 0),
      reg_bsn_monitor_v2_rx_read_export         => reg_bsn_monitor_v2_rx_mosi.rd,
      reg_bsn_monitor_v2_rx_readdata_export     => reg_bsn_monitor_v2_rx_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_rx_write_export        => reg_bsn_monitor_v2_rx_mosi.wr,
      reg_bsn_monitor_v2_rx_writedata_export    => reg_bsn_monitor_v2_rx_mosi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_tx_address_export      => reg_bsn_monitor_v2_tx_mosi.address(9 downto 0),
      reg_bsn_monitor_v2_tx_read_export         => reg_bsn_monitor_v2_tx_mosi.rd,
      reg_bsn_monitor_v2_tx_readdata_export     => reg_bsn_monitor_v2_tx_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_tx_write_export        => reg_bsn_monitor_v2_tx_mosi.wr,
      reg_bsn_monitor_v2_tx_writedata_export    => reg_bsn_monitor_v2_tx_mosi.wrdata(c_word_w - 1 downto 0),

      ram_diag_bg_ring_address_export           => ram_bg_data_mosi.address(9 downto 0),
      ram_diag_bg_ring_read_export              => ram_bg_data_mosi.rd,
      ram_diag_bg_ring_readdata_export          => ram_bg_data_miso.rddata(c_word_w - 1 downto 0),
      ram_diag_bg_ring_write_export             => ram_bg_data_mosi.wr,
      ram_diag_bg_ring_writedata_export         => ram_bg_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_diag_bg_ring_address_export           => reg_bg_ctrl_mosi.address(2 downto 0),
      reg_diag_bg_ring_read_export              => reg_bg_ctrl_mosi.rd,
      reg_diag_bg_ring_readdata_export          => reg_bg_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_diag_bg_ring_write_export             => reg_bg_ctrl_mosi.wr,
      reg_diag_bg_ring_writedata_export         => reg_bg_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      reg_dp_xonoff_bg_address_export           => reg_dp_xonoff_bg_mosi.address(2 downto 0),
      reg_dp_xonoff_bg_read_export              => reg_dp_xonoff_bg_mosi.rd,
      reg_dp_xonoff_bg_readdata_export          => reg_dp_xonoff_bg_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_xonoff_bg_write_export             => reg_dp_xonoff_bg_mosi.wr,
      reg_dp_xonoff_bg_writedata_export         => reg_dp_xonoff_bg_mosi.wrdata(c_word_w - 1 downto 0),

      reg_dp_xonoff_from_lane_address_export    => reg_dp_xonoff_from_lane_mosi.address(2 downto 0),
      reg_dp_xonoff_from_lane_read_export       => reg_dp_xonoff_from_lane_mosi.rd,
      reg_dp_xonoff_from_lane_readdata_export   => reg_dp_xonoff_from_lane_miso.rddata(c_word_w - 1 downto 0),
      reg_dp_xonoff_from_lane_write_export      => reg_dp_xonoff_from_lane_mosi.wr,
      reg_dp_xonoff_from_lane_writedata_export  => reg_dp_xonoff_from_lane_mosi.wrdata(c_word_w - 1 downto 0),

      reg_sdp_info_address_export               => reg_sdp_info_mosi.address(c_sdp_reg_sdp_info_addr_w - 1 downto 0),
      reg_sdp_info_write_export                 => reg_sdp_info_mosi.wr,
      reg_sdp_info_writedata_export             => reg_sdp_info_mosi.wrdata(c_word_w - 1 downto 0),
      reg_sdp_info_read_export                  => reg_sdp_info_mosi.rd,
      reg_sdp_info_readdata_export              => reg_sdp_info_miso.rddata(c_word_w - 1 downto 0),

      kernel_cra_waitrequest                    => board_kernel_cra_waitrequest,
      kernel_cra_readdata                       => board_kernel_cra_readdata,
      kernel_cra_readdatavalid                  => board_kernel_cra_readdatavalid,
      kernel_cra_burstcount                     => board_kernel_cra_burstcount,
      kernel_cra_writedata                      => board_kernel_cra_writedata,
      kernel_cra_address                        => board_kernel_cra_address,
      kernel_cra_write                          => board_kernel_cra_write,
      kernel_cra_read                           => board_kernel_cra_read,
      kernel_cra_byteenable                     => board_kernel_cra_byteenable,
      kernel_cra_debugaccess                    => board_kernel_cra_debugaccess,

      kernel_irq_irq                            => board_kernel_irq_irq,

      reg_ta2_unb2b_mm_io_address_export        => reg_ta2_unb2b_mm_io_mosi.address(c_kernel_regmap_addr_w - 1 downto 0),
      reg_ta2_unb2b_mm_io_read_export           => reg_ta2_unb2b_mm_io_mosi.rd,
      reg_ta2_unb2b_mm_io_readdata_export       => reg_ta2_unb2b_mm_io_miso.rddata(c_word_w - 1 downto 0),
      reg_ta2_unb2b_mm_io_write_export          => reg_ta2_unb2b_mm_io_mosi.wr,
      reg_ta2_unb2b_mm_io_writedata_export      => reg_ta2_unb2b_mm_io_mosi.wrdata(c_word_w - 1 downto 0),
      reg_ta2_unb2b_mm_io_waitrequest_export    => reg_ta2_unb2b_mm_io_miso.waitrequest
  );
  end generate;
end str;
