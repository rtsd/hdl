-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Rewire an array of streaming interfaces based.
--
-- Description: The order of the streaming interfaces at the input can be changed towards
--              the output. Streams can be copied or moved. There are two modes to the
--              selection: Based on a selection table or based on the settings of the
--              selection register.
--
--              SELECTION TABLE (g_use_sel_table = TRUE)
--              The inputs are connected to the outputs based on a the values in a selected
--              column of the selection table (g_select_table). The column is selected by
--              the sel_in signal. If sel_in = 3 the third column from the selection table
--              will be used to determine the output. A set of predefined connection schemes
--              can be defined in the selection table.
--
--              SEL_IN (g_use_sel_table = FALSE)
--              The outputs are connected to the inputs based on the settings of the sel_in
--              register. Each output is represented by g_sel_in_w bits in the sel_in register.
--              For instance: an 8-input system with the following sel_in register content
--              will result in a reversed order of the input streams.
--
--              Output nr:  7   6   5   4   3   2   1   0
--              Register:  000 001 010 011 100 101 110 111
--
-- Remarks:
--
--
-- Some more remarks:

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.reorder_pkg.all;

entity reorder_rewire is
  generic(
    g_select_table  : t_reorder_table := c_reorder_table;
    g_nof_streams   : natural     := 8;
    g_sel_in_w      : natural     := 3;
    g_use_sel_table : boolean     := true
  );
  port(
    -- System
    sel_in          : in  std_logic_vector(sel_a_b(g_use_sel_table, g_sel_in_w, g_sel_in_w * g_nof_streams) - 1 downto 0) := (others => '0');

    -- Inputs from transceiver
    input_sosi_arr  : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    input_siso_arr  : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    -- Outputs to processing
    output_sosi_arr : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    output_siso_arr : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy)

  );
end reorder_rewire;

architecture str of reorder_rewire is
begin
  ---------------------------------------------------------------------------------------
  -- Connect outputs in process using Selection Table
  ---------------------------------------------------------------------------------------
  g_selection_table : if g_use_sel_table = true generate
    process(sel_in, input_sosi_arr, output_siso_arr)
      variable v_sel : natural;
    begin
      output_sosi_arr <= (others => c_dp_sosi_rst);
      input_siso_arr  <= (others => c_dp_siso_rdy);
      for I in 0 to g_nof_streams - 1 loop
        v_sel := g_select_table(I, TO_UINT(sel_in));
        output_sosi_arr(I) <= input_sosi_arr(v_sel);
        input_siso_arr(v_sel) <= output_siso_arr(I);
      end loop;
    end process;
  end generate;

  ---------------------------------------------------------------------------------------
  -- Connect outputs in process without Selection Table
  ---------------------------------------------------------------------------------------
  g_no_selection_table : if g_use_sel_table = false generate
    process(sel_in, input_sosi_arr, output_siso_arr)
      variable v_sel : natural;
    begin
      output_sosi_arr <= (others => c_dp_sosi_rst);
      input_siso_arr  <= (others => c_dp_siso_rdy);
      for I in 0 to g_nof_streams - 1 loop
        v_sel := TO_UINT(sel_in((I + 1) * g_sel_in_w - 1 downto I * g_sel_in_w));
        output_sosi_arr(I) <= input_sosi_arr(v_sel);
        input_siso_arr(v_sel) <= output_siso_arr(I);
      end loop;
    end process;
  end generate;
end str;
