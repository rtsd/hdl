-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.mdio_pkg.all;

entity tb_mdio_phy_ctlr is
end tb_mdio_phy_ctlr;

architecture tb of tb_mdio_phy_ctlr is
  constant c_sim        : boolean := true;

  constant clk_period   : time := 10 ns;
  constant c_delay      : natural := 50;

  constant c_st         : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_st;
  constant c_op_addr    : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_addr;
  constant c_op_wr      : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_wr;
  constant c_op_rd      : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_rd;
  constant c_op_rd_incr : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_op_rd_incr;
  constant c_prtad      : std_logic_vector(4 downto 0) := "00000";  -- port address (example)
  constant c_devad      : std_logic_vector(4 downto 0) := "00001";  -- device address (example)
  constant c_ta         : std_logic_vector(1 downto 0) := c_mdio_phy_hdr_ta;

  constant c_hdr_wraddr : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_addr & c_prtad & c_devad & c_ta;
  constant c_hdr_wrdata : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_wr   & c_prtad & c_devad & c_ta;
  constant c_hdr_rddata : std_logic_vector(c_halfword_w - 1 downto 0) := c_st & c_op_rd   & c_prtad & c_devad & c_ta;

  constant c_phy_addr   : std_logic_vector(c_halfword_w - 1 downto 0) := std_logic_vector(to_unsigned(16#A001#, c_halfword_w));
  constant c_phy_data   : std_logic_vector(c_halfword_w - 1 downto 0) := std_logic_vector(to_unsigned(16#D001#, c_halfword_w));

  signal clk            : std_logic := '1';
  signal rst            : std_logic;

  signal tb_end         : std_logic := '0';

  signal mdio_en_evt    : std_logic;
  signal mdio_done      : std_logic;
  signal mdio_done_ack_evt  : std_logic;
  signal hdr            : std_logic_vector(c_halfword_w - 1 downto 0);
  signal tx_dat         : std_logic_vector(c_halfword_w - 1 downto 0);
  signal rx_dat         : std_logic_vector(c_halfword_w - 1 downto 0);

  signal mdc            : std_logic;
  signal mdat_in        : std_logic;
  signal mdat_oen       : std_logic;
  signal mdio           : std_logic;

  -- MDIO slave model
  constant c_slave_reg_addr : natural := 16#A001#;
  constant c_slave_reg_data : natural := 16#1234#;

  signal slave_addr     : std_logic_vector(c_halfword_w - 1 downto 0);  -- MDIO access address
  signal slave_reg_addr : std_logic_vector(c_halfword_w - 1 downto 0);  -- the one data register address
  signal slave_reg_dat  : std_logic_vector(c_halfword_w - 1 downto 0);  -- the one data register data value

  -- Command sequence for mdio_ctlr to auto execute
  -- ==============================================
  constant c_mdio_cmd_arr : t_mdio_cmd_arr(0 to 1) := (
    -- Write MMD
    ('1', c_devad, TO_UVEC(c_slave_reg_addr, c_mdio_phy_data_width), c_phy_data),
    -- Read MMD
    ('0', c_devad, TO_UVEC(c_slave_reg_addr, c_mdio_phy_data_width), (others => '0'))
  );
begin
  clk  <= not clk  or tb_end after clk_period / 2;
  rst  <= '1', '0' after clk_period * 3;

  mdio <= 'H';  -- pull up

  p_verify : process
  begin
    wait for 42 us;
    ----------------------------------------------------------------------------
    -- Check read back value
    ----------------------------------------------------------------------------
    if rx_dat = c_phy_data then
      report "Write MDIO slave reg and readback went OK"
        severity NOTE;
    else
      report "Write MDIO slave reg and readback went wrong"
        severity ERROR;
    end if;

    tb_end <= '1';

    wait;
  end process;

  -- MDIO controller
  u_mdio_ctlr : entity work.mdio_ctlr
  generic map (
     g_mdio_prtad           => c_prtad,
     g_mdio_cmd_arr         => c_mdio_cmd_arr,
     g_mdio_rst_level       => '1',
     g_mdio_rst_cycles      => 10,
     g_mdio_post_rst_cycles => 5
    )
  port map (
    rst           => rst,
    clk           => clk,

    mdio_en_evt   => mdio_en_evt,
    mdio_done     => mdio_done,

    mdio_done_ack_evt => mdio_done_ack_evt,

    hdr           => hdr,
    tx_dat        => tx_dat
  );

  -- MDIO master
  u_mdio_phy : entity work.mdio_phy
  port map (
    gs_sim        => c_sim,

    rst           => rst,
    clk           => clk,

    mdio_en_evt   => mdio_en_evt,
    mdio_done     => mdio_done,

    mdio_done_ack_evt => mdio_done_ack_evt,

    hdr           => hdr,
    tx_dat        => tx_dat,
    rx_dat        => rx_dat,

    -- External clock and serial data.
    mdc           => mdc,
    mdat_in       => mdat_in,
    mdat_oen      => mdat_oen
  );

  u_iobuf : entity common_lib.common_inout
  port map (
    dat_inout        => mdio,
    dat_in_from_line => mdat_in,
    dat_out_to_line  => '0',  -- for output '1' rely on external pull up, only
    dat_out_en       => mdat_oen  -- pull low when mdat_oen (so implicitely mdat_out='0')
  );

  -- MDIO slave
  u_slave : entity work.mmd_slave
  generic map (
    g_st        => c_st,
    g_prtad     => c_prtad,
    g_devad     => c_devad,
    g_reg_addr  => c_slave_reg_addr,
    g_reg_dat   => c_slave_reg_data,
    g_reg_w     => c_halfword_w
  )
  port map (
    mdc         => mdc,
    mdio        => mdio,
    addr        => slave_addr,
    reg_dat     => slave_reg_dat
  );

  slave_reg_addr <= std_logic_vector(to_unsigned(c_slave_reg_addr, c_halfword_w));
end tb;
