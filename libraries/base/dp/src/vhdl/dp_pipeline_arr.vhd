-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

-- Purpose:
--   Pipeline array of g_nof_streams by g_pipeline cycles.
-- Description:
--   See dp_pipeline.

entity dp_pipeline_arr is
  generic (
    g_nof_streams : natural := 1;
    g_pipeline    : natural := 1  -- 0 for wires, > 0 for registers,
  );
  port (
    rst          : in  std_logic;
    clk          : in  std_logic;
    -- ST sink
    snk_out_arr  : out t_dp_siso_arr(g_nof_streams - 1 downto 0);
    snk_in_arr   : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    -- ST source
    src_in_arr   : in  t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
    src_out_arr  : out t_dp_sosi_arr(g_nof_streams - 1 downto 0)
  );
end dp_pipeline_arr;

architecture str of dp_pipeline_arr is
begin
  gen_nof_streams : for I in 0 to g_nof_streams - 1 generate
    u_p : entity work.dp_pipeline
    generic map (
      g_pipeline => g_pipeline
    )
    port map (
      rst          => rst,
      clk          => clk,
      -- ST sink
      snk_out      => snk_out_arr(I),
      snk_in       => snk_in_arr(I),
      -- ST source
      src_in       => src_in_arr(I),
      src_out      => src_out_arr(I)
    );
  end generate;
end str;
