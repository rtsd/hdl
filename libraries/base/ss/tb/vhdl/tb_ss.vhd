-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

-- Usage:
-- > as 10
-- > run -all
-- . Observe in_sosi and out_sosi in the Wave window
--
-- Description:
-- . This tb_ss for the subband select (SS) self verifies the data, sync and BSN
-- . The flow control is selected via g_mode_in_en and g_mode_out_ready.
--
-- Remark:
-- . Use tb_tb_ss for multi-tb DUT verification.

entity tb_ss is
  generic (
    -- Flow control
    g_mode_in_en            : natural := 0;  -- use 0 for active in_sosi.valid control
                                             -- use 1 for random in_sosi.valid control
    g_mode_out_ready        : natural := 0;  -- use 0 for            active out_siso.ready control
                                             -- use 1 for          toggling out_siso.ready control, requires g_nof_ch_sel <= g_nof_ch_in/2
                                             -- use 2 for inverted toggling out_siso.ready control, requires g_nof_ch_sel <= g_nof_ch_in/2
    -- Test duration
    g_nof_sync              : natural := 10;

    -- Functional parameters
    g_reverse_ss_map        : boolean := true;  -- use true to verify that the last data written can be read immediately after the SS page swap

    --g_nof_ch_in             : NATURAL := 256;
    g_nof_ch_in             : natural := 16;
    --g_nof_ch_sel            : NATURAL := 192;
    --g_nof_ch_sel            : NATURAL := 16;
    g_nof_ch_sel            : natural := 12;
    --g_nof_ch_sel            : NATURAL := 8;
    --g_nof_ch_sel            : NATURAL := 7;

    g_use_complex           : boolean := true;

    g_use_output_rl_adapter : boolean := false  -- when true adapt output RL to 1 else the output RL is equal to c_retrieve_lat=2
  );
end tb_ss;

architecture tb of tb_ss is
  constant clk_period             : time := 10 ns;

  constant c_rl                   : natural := 1;
  constant c_dsp_data_w           : natural := 16;
  constant c_bsn_init             : natural := 10;

  constant c_nof_ch_gap           : natural := g_nof_ch_in - g_nof_ch_sel + 1;
  constant c_nof_block_per_sync   : natural := 4;
  constant c_bsn_offset           : natural := c_bsn_init mod c_nof_block_per_sync;

  signal rst             : std_logic;
  signal clk             : std_logic := '1';
  signal tb_end          : std_logic := '0';

  signal mm_reverse_mosi : t_mem_mosi;
  signal mm_dut_mosi     : t_mem_mosi;
  signal mm_done         : std_logic;

  signal random_0        : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal toggle          : std_logic := '1';
  signal verify_en       : std_logic := '0';
  signal verify_bsn      : std_logic := '1';

  signal st_en           : std_logic := '1';
  signal st_siso         : t_dp_siso := c_dp_siso_rdy;
  signal st_sosi         : t_dp_sosi := c_dp_sosi_rst;

  signal bsn             : natural := c_bsn_init;

  signal dp_sosi         : t_dp_sosi := c_dp_sosi_rst;
  signal reverse_sosi    : t_dp_sosi := c_dp_sosi_rst;
  signal in_sosi         : t_dp_sosi := c_dp_sosi_rst;

  signal out_siso        : t_dp_siso := c_dp_siso_rdy;
  signal out_sosi        : t_dp_sosi;
  signal prev_out_sosi   : t_dp_sosi;
  signal hold_out_sosi   : t_dp_sosi;

  signal first_in_bsn    : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  signal first_out_bsn   : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
begin
  clk <= not clk or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 5;

  ------------------------------------------------------------------------------
  -- Write SS select map
  ------------------------------------------------------------------------------
  p_mm_dut_stimuli : process
  begin
    mm_dut_mosi <= c_mem_mosi_rst;
    mm_done <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    for I in 0 to g_nof_ch_sel - 1 loop
      if g_reverse_ss_map = false then
        proc_mem_mm_bus_wr(I, I, clk, mm_dut_mosi);  -- Write the default SS map: (0, 1, 2, ... g_nof_ch_sel-1) ==>  (0, 1, 2, ... g_nof_ch_sel-1)
      else
        proc_mem_mm_bus_wr(I, g_nof_ch_sel - 1 - I, clk, mm_dut_mosi);  -- Write the reverse SS map: (0, 1, 2, ... g_nof_ch_sel-1) ==>  (g_nof_ch_sel-1, ..., 2, 1, 0)
      end if;
    end loop;
    mm_done <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- Flow control : Fixed, toggle or random
  ------------------------------------------------------------------------------
  random_0 <= func_common_random(random_0) when rising_edge(clk);
  toggle   <= not toggle when rising_edge(clk);

  -- Input valid
  st_en <= '1' when g_mode_in_en = 0 else random_0(random_0'high);

  -- Output ready
  -- . toggle ready to model flow control for a dual input multiplexer, use g_nof_ch_sel < g_nof_ch_in/2
  out_siso.ready <= '1'     when g_mode_out_ready = 0 else
                    toggle  when g_mode_out_ready = 1 else
                    not toggle;

  ------------------------------------------------------------------------------
  -- Generate input data blocks
  ------------------------------------------------------------------------------
  p_st_stimuli : process
    variable v_re   : natural := 0;
    variable v_im   : natural := 1;
    variable v_data : natural := 2;
  begin
    st_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_high(clk, mm_done);

    -- Run some sync intervals with DSP counter data for the real and imag fields
    wait until rising_edge(clk);
    for I in 0 to g_nof_sync - 1 loop
      if g_use_complex then
        proc_dp_gen_block_data(c_rl, false, c_dsp_data_w, c_dsp_data_w, 0, v_re, v_im, g_nof_ch_in, 0, 0, '1', "0", clk, st_en, st_siso, st_sosi);  -- next sync
        v_re := v_re + g_nof_ch_in;
        v_im := v_im + g_nof_ch_in;
      else
        proc_dp_gen_block_data(c_rl, true, c_nof_complex * c_dsp_data_w, c_nof_complex * c_dsp_data_w, v_data, 0, 0, g_nof_ch_in, 0, 0, '1', "0", clk, st_en, st_siso, st_sosi);  -- next sync
        v_data := v_data + g_nof_ch_in;
      end if;
      for J in 0 to c_nof_block_per_sync - 2 loop  -- provide sop and eop for block reference
        if g_use_complex then
          proc_dp_gen_block_data(c_rl, false, c_dsp_data_w, c_dsp_data_w, 0, v_re, v_im, g_nof_ch_in, 0, 0, '0', "0", clk, st_en, st_siso, st_sosi);  -- no sync
          v_re := v_re + g_nof_ch_in;
          v_im := v_im + g_nof_ch_in;
        else
          proc_dp_gen_block_data(c_rl, true, c_nof_complex * c_dsp_data_w, c_nof_complex * c_dsp_data_w, v_data, 0, 0, g_nof_ch_in, 0, 0, '0', "0", clk, st_en, st_siso, st_sosi);  -- next sync
          v_data := v_data + g_nof_ch_in;
        end if;
      end loop;
    end loop;
    st_sosi <= c_dp_sosi_rst;
    proc_common_wait_some_cycles(clk, 3 * g_nof_ch_in);
    tb_end <= '1';
    wait;
  end process;

  -- Time stimuli
  bsn <= bsn + 1 when rising_edge(clk) and (st_sosi.eop = '1');

  -- Add BSN to the ST data
  p_dp_sosi : process(st_sosi, bsn)
  begin
    dp_sosi     <= st_sosi;
    dp_sosi.bsn <= TO_DP_BSN(bsn);
  end process;

  ------------------------------------------------------------------------------
  -- Compensate for reverse order read out of DUT so that proc_verify_* have incrementing data
  ------------------------------------------------------------------------------
  gen_reverse_ss : if g_reverse_ss_map = true generate
    p_mm_reverse_stimuli : process
    begin
      mm_reverse_mosi <= c_mem_mosi_rst;
      proc_common_wait_until_low(clk, rst);
      proc_common_wait_some_cycles(clk, 10);

      for I in 0 to g_nof_ch_in - 1 loop
        proc_mem_mm_bus_wr(I, g_nof_ch_in - 1 - I, clk, mm_reverse_mosi);  -- Write the reverse SS map: (0, 1, 2, ... g_nof_ch_in-1) ==>  (g_nof_ch_in-1, ..., 2, 1, 0)
      end loop;

      wait;
    end process;

    u_reverse_ss : entity work.ss
    generic map (
      g_use_output_rl_adapter => g_use_output_rl_adapter,
      g_dsp_data_w            => c_dsp_data_w,
      g_nof_ch_in             => g_nof_ch_in,
      g_nof_ch_sel            => g_nof_ch_in,
      g_use_complex           => g_use_complex
    )
    port map (
      mm_rst         => rst,
      mm_clk         => clk,
      dp_rst         => rst,
      dp_clk         => clk,

      -- Memory Mapped
      ram_ss_ss_mosi => mm_reverse_mosi,
      ram_ss_ss_miso => OPEN,

      -- Streaming
      input_sosi     => dp_sosi,

      output_sosi    => reverse_sosi,
      output_siso    => c_dp_siso_rdy
    );
  end generate;

  ------------------------------------------------------------------------------
  -- DUT : SS
  ------------------------------------------------------------------------------
  in_sosi <= dp_sosi when g_reverse_ss_map = false else reverse_sosi;

  u_dut_ss : entity work.ss
  generic map (
    g_use_output_rl_adapter => g_use_output_rl_adapter,
    g_dsp_data_w            => c_dsp_data_w,
    g_nof_ch_in             => g_nof_ch_in,
    g_nof_ch_sel            => g_nof_ch_sel,
    g_use_complex           => g_use_complex
  )
  port map (
    mm_rst         => rst,
    mm_clk         => clk,
    dp_rst         => rst,
    dp_clk         => clk,

    -- Memory Mapped
    ram_ss_ss_mosi => mm_dut_mosi,
    ram_ss_ss_miso => OPEN,

    -- Streaming
    input_sosi     => in_sosi,

    output_sosi    => out_sosi,
    output_siso    => out_siso
  );

  ------------------------------------------------------------------------------
  -- Verify
  ------------------------------------------------------------------------------
  p_verify_en : process
  begin
    verify_en <= '0';
    proc_common_wait_until_high(clk, in_sosi.eop);  -- now the SS will start outputing the first block
    proc_common_wait_some_cycles(clk, 10);  -- wait a few cycles for the SS latency
    verify_en <= '1';
    wait;
  end process;

  -- verify first BSN to ensure that the output BSN does not slip a frame compared to the corresponding input frame
  p_verify_first_bsn : process
  begin
    first_in_bsn <= TO_DP_BSN(c_bsn_init);
    proc_common_wait_until_high(clk, out_sosi.sop);
    first_out_bsn <= out_sosi.bsn;
    proc_common_wait_some_cycles(clk, 1);
    proc_dp_verify_value(e_equal, clk, verify_bsn, first_in_bsn, first_out_bsn);
    wait;
  end process;

  -- verify incrementing next BSN and sync
  proc_dp_verify_data("out_sosi.bsn", clk, verify_en, out_sosi.sop, out_sosi.bsn, prev_out_sosi.bsn);
  proc_dp_verify_sync(c_nof_block_per_sync, c_bsn_offset, clk, verify_en, out_sosi.sync, out_sosi.sop, out_sosi.bsn);

  -- verify incrementing re and im
  gen_verify_complex: if g_use_complex generate
    proc_dp_verify_data("out_sosi.re", c_unsigned_0, to_unsigned(c_nof_ch_gap, 32), clk, verify_en, out_sosi.valid, out_sosi.re, prev_out_sosi.re);
    proc_dp_verify_data("out_sosi.im", c_unsigned_0, to_unsigned(c_nof_ch_gap, 32), clk, verify_en, out_sosi.valid, out_sosi.im, prev_out_sosi.im);
  end generate;

  gen_verify_non_complex: if not(g_use_complex) generate
    proc_dp_verify_data("out_sosi.data", c_unsigned_0, to_unsigned(c_nof_ch_gap, 32), clk, verify_en, out_sosi.valid, out_sosi.data, prev_out_sosi.data);
  end generate;

  -- verify output framing
  proc_dp_verify_sop_and_eop(clk, out_sosi.valid, out_sosi.sop, out_sosi.eop, hold_out_sosi.sop);
end tb;
