-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, technology_lib, tech_pll_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use technology_lib.technology_pkg.all;

-- Purpose: PLL for UniBoard node CLK input @ 25 MHz
-- Description:
--   c0 = 20 MHz
--   c1 = 40 MHz
--   c2 = 50 MHz
--   c3 = 125 MHz
--

entity unb1_board_clk25_pll is
  generic (
    g_technology : natural := c_tech_stratixiv
  );
  port (
    arst        : in  std_logic := '0';
    clk25       : in  std_logic := '0';  -- connect to UniBoard ETH_clk pin (25 MHz)

    c0_clk20    : out std_logic;  -- PLL c0
    c1_clk40    : out std_logic;  -- PLL c1
    c2_clk50    : out std_logic;  -- PLL c2
    c3_clk125   : out std_logic;  -- PLL c3
    c4_clk200   : out std_logic;  -- PLL c4 FIXME only StratixIV
    pll_locked  : out std_logic
  );
end unb1_board_clk25_pll;

architecture stratixiv of unb1_board_clk25_pll is
begin
  u_pll : entity tech_pll_lib.tech_pll_clk25
  generic map (
    g_technology => g_technology
  )
  port map (
    areset  => arst,
    inclk0  => clk25,
    c0      => c0_clk20,
    c1      => c1_clk40,
    c2      => c2_clk50,
    c3      => c3_clk125,
    c4      => c4_clk200,
    locked  => pll_locked
  );
end stratixiv;
