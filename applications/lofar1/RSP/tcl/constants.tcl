#
# From common(pkg).vhd
#
set c_complex                     2          ;# factor 2 accounting for Re part and Im part of complex number
set c_cpx                         $c_complex
set c_phs                         $c_complex
set c_pol                         2          ;# factor 2 accounting for X and Y polarization
set c_pol_phs                     4          ;# factor 4 accounting for dual polarization and complex

set c_long                        4          ;# factor 4 accounting for 4 bytes

set c_rcu_version                 1          ;# ctrlreg[23:20] = "0001" in RCU i2cslave(rtl).vhd

set c_rsp_ext_clk_freq            200000000
set c_rsp_rcu_clk_freq            $c_rsp_ext_clk_freq
set c_rsp_ext_sync_idelay         0
set c_rsp_rcu_clock_idelay        0

set c_rsp_nof_bp                  1
set c_rsp_nof_ap                  4
set c_rsp_nof_blp_per_ap          1
set c_rsp_nof_blp                 [expr $c_rsp_nof_ap*$c_rsp_nof_blp_per_ap]

set c_rsp_nof_rcu_per_ap          $c_pol                                      ;# = 2, nof RCU per AP (and 1 BLP per AP)
set c_rsp_nof_rcu                 [expr $c_rsp_nof_ap*$c_rsp_nof_rcu_per_ap]  ;# = 8, nof RCU per RSP (and 1 BLP per AP)
set c_rsp_nof_sp                  $c_rsp_nof_rcu;                             ;# = 8, nof signal paths (SP) per RSP (each RCU receives one SP)
  
set c_rsp_nof_subbands          512
set c_rsp_slice_size              [expr $c_complex*$c_rsp_nof_subbands]       ;# = 1024

set c_rsp_nof_lanes                   4
set c_rsp_nof_beamlets              248
set c_rsp_nof_beamlets_per_lane       [expr $c_rsp_nof_beamlets/$c_rsp_nof_lanes]
set c_rsp_nof_antennas               96
set c_rsp_nof_crosslets_per_antenna   1
set c_rsp_nof_crosslets               [expr $c_rsp_nof_crosslets_per_antenna*$c_rsp_nof_antennas]
set c_rsp_nof_crosslets_per_lane      [expr $c_rsp_nof_crosslets/$c_rsp_nof_lanes]
set c_rsp_nof_reflets                 $c_rsp_nof_crosslets_per_antenna
set c_rsp_nof_reflets_ap              [expr $c_rsp_nof_crosslets_per_antenna*$c_rsp_nof_ap]
set c_rsp_nof_beamlets_ap             [expr $c_rsp_nof_reflets_ap+$c_rsp_nof_beamlets]
set c_rsp_nof_let_types               2

set c_rsp_rcu_dat_w              12
set c_blp_out_dat_w              18

set c_rsp_sst_acc_w              54
set c_rsp_bst_acc_w              54
set c_rsp_xst_acc_w              54

set c_diag_reg_wave_dat_w        18
set c_diag_reg_wave_size_small_w 11
set c_diag_wave_factor            2
set c_diag_wave_size              [expr $c_diag_wave_factor*$c_rsp_slice_size]  ;# = 2**c_diag_reg_wave_size_small_w

set c_diag_reg_res_word_w         4
set c_diag_reg_res_size           [expr $c_rsp_nof_beamlets_ap*$c_pol_phs]

set c_bf_reg_coef_w              16
set c_rsp_beam_data_dat_w        24

set c_rsp_ring_dat_w             14

set c_tdsh_protocol_adr_w        11
set c_tdsh_result_adr_w          10
set c_rcuh_protocol_adr_w         9
set c_rcuh_result_adr_w           9

set c_rad_nof_rx_latency          [expr 1+$c_rsp_nof_let_types*$c_rsp_nof_lanes]                   ;# = 1 + 2*4     = 9
set c_rad_nof_rx_status           [expr 1+$c_rsp_nof_let_types*$c_rsp_nof_lanes+$c_rsp_nof_lanes]  ;# = 1 + 2*4 + 4 = 13

set c_ei_status_rsp_tvolt_offset  0
set c_ei_status_rsp_tvolt_size    9
set c_ei_status_rsp_clk_offset    [expr $c_ei_status_rsp_tvolt_offset +                $c_ei_status_rsp_tvolt_size]
set c_ei_status_rsp_clk_size      1
set c_ei_status_rsp_offset        0 ;# = $c_ei_status_rsp_tvolt_offset
set c_ei_status_rsp_size         12 ;# = $c_ei_status_rsp_tvolt_size + $c_ei_status_rsp_clk_size rounded to factor of 4
set c_ei_status_eth_offset        [expr $c_ei_status_rsp_offset       +                $c_ei_status_rsp_size]
set c_ei_status_eth_size         12
set c_ei_status_mep_offset        [expr $c_ei_status_eth_offset       +                $c_ei_status_eth_size]
set c_ei_status_mep_size          4
set c_ei_status_diag_offset       [expr $c_ei_status_mep_offset       +                $c_ei_status_mep_size]
set c_ei_status_diag_size        24
set c_ei_status_bs_offset         [expr $c_ei_status_diag_offset      +                $c_ei_status_diag_size]
set c_ei_status_bs_size          16 ;# For each AP
set c_ei_status_rcuh_offset       [expr $c_ei_status_bs_offset        + $c_rsp_nof_blp*$c_ei_status_bs_size]
set c_ei_status_rcuh_size        12 ;# For each AP
set c_ei_status_rsu_offset        [expr $c_ei_status_rcuh_offset      + $c_rsp_nof_blp*$c_ei_status_rcuh_size]
set c_ei_status_rsu_size          4
set c_ei_status_ado_offset        [expr $c_ei_status_rsu_offset       +                $c_ei_status_rsu_size]
set c_ei_status_ado_size          8
set c_ei_status_rad_offset        [expr $c_ei_status_ado_offset       + $c_rsp_nof_blp*$c_ei_status_ado_size]
set c_ei_status_rad_size          4 ;# For each link
set c_ei_status_rcuh_test_offset  [expr $c_ei_status_rad_offset       + $c_rad_nof_rx_status *$c_ei_status_rad_size]
set c_ei_status_rcuh_test_size    4 ;# For each AP
set c_ei_status_rsr_offset        0 ;# = $c_ei_status_rsp_tvolt_offset
set c_ei_status_rsr_size          [expr $c_ei_status_rcuh_test_offset + $c_rsp_nof_blp*$c_ei_status_rcuh_test_size]

set c_ei_eth_err_noerr            0
set c_ei_eth_err_preamblevalue    1
set c_ei_eth_err_framedelimiter   2
set c_ei_eth_err_preamblelength   3
set c_ei_eth_err_headerlength     4
set c_ei_eth_err_crc              5
set c_ei_eth_err_oddnibblelength  6
set c_ei_eth_err_framelength      7

set c_ei_mep_err_noerr            0
set c_ei_mep_err_type             1
set c_ei_mep_err_addr_blp         2
set c_ei_mep_err_pid              3
set c_ei_mep_err_regid            4
set c_ei_mep_err_offset           5
set c_ei_mep_err_size             6
set c_ei_mep_err_ringcrc          7
set c_ei_mep_err_timeout          8

#
# CEP data output Ethernet frame constants
#

set c_eth_preamble_len            8     ;# ETH preamble (8 octets)
set c_eth_header_len             14     ;# ETH header (14 octets)
set c_ip_header_len              28     ;# IP header (28 octets)
set c_epa_header_len             16     ;# EPA header (16 octets)
set c_phy_header_len              [expr $c_eth_preamble_len + $c_eth_header_len]
set c_cdo_header_len              [expr $c_ip_header_len + $c_epa_header_len]
set c_cep_header_len              [expr $c_phy_header_len + $c_cdo_header_len]
set c_cep_nof_octets              2     ;# two octets per beamlet data halfword to CEP
  
#
# SDO = Subband Data Output constants from common(pkg) and sdo_pkg, see also description in sdo_frame.vhd.
#
set c_ss_sel_zero                      [expr 0x8000]

set c_rsp_sdo_nof_subbands            36      ;# Number of SDO subbands per SP
set c_rsp_sdo_nof_subbands_per_lane    [expr $c_rsp_sdo_nof_subbands/$c_rsp_nof_lanes]

# Define 4 dat (real) = 2 bands (complex) ^= 1 let (dual pol and complex)
set c_sdo_ss_nof_subbands_ap           [expr $c_rsp_sdo_nof_subbands*$c_pol]         ;# = 72
set c_sdo_ss_nof_subbands_rsp          [expr $c_rsp_sdo_nof_subbands*$c_rsp_nof_sp]  ;# = 288
set c_sdo_ss_nof_bands               316      ;# = c_sdo_len_ap_frame_payload/c_rsp_nof_pol


#
# Derived constants
#

# - Calculate ADO scale factor based on word widths in ado(rtl).vhd and nof samples per sync interval for 200 MHz
set dat_in_w    $c_rsp_rcu_dat_w
set nof_acc_w   [expr ceil(log($c_rsp_rcu_clk_freq)/log(2))]
set result_w    32
set acc_w       [expr $dat_in_w + $nof_acc_w]
set c_ado_scale [expr round(pow(2, $acc_w - $result_w))]

#
# RSR status
#
set c_rsr_ok         0
set c_rsr_error      1
set c_rsr_undefined  2

set c_cp_bp              1
set c_cp_statusRdy       0
set c_cp_statusVersion0  1
set c_cp_statusFpgaType  2
set c_cp_statusImageType 3
set c_cp_statusTrigLo    4
set c_cp_statusTrigHi    6
set c_cp_statusVersion1  7
set c_cp_version_w       2
set c_cp_trig_w          [expr $c_cp_statusTrigHi-$c_cp_statusTrigLo+1]
set c_cp_trig_mask       [expr (1 << $c_cp_trig_w) - 1]
set c_cp_trig_ButtonRst  0
set c_cp_trig_TempRst    1
set c_cp_trig_UserRst    2
set c_cp_trig_WdRst      4


#
# From diag(pkg).vhd
#
set c_diag_dev_ri            0
set c_diag_dev_rcux          1
set c_diag_dev_rcuy          2
set c_diag_dev_lcu           3
set c_diag_dev_cep           4
set c_diag_dev_serdes        5

set c_diag_mode_no_tst           0
set c_diag_mode_loop_local       1
set c_diag_mode_loop_line        2
set c_diag_mode_loop_remote      3
set c_diag_mode_tx               4
set c_diag_mode_rx               5
set c_diag_mode_tx_rx            6
# RI specific modes
set c_diag_mode_bus              $c_diag_mode_tx_rx
set c_diag_mode_lane_all         7
set c_diag_mode_lane_single      8
# Serdes specific loopback mode variants
set c_diag_mode_loop_sys_diag    9
set c_diag_mode_loop_par_diag    $c_diag_mode_loop_local
set c_diag_mode_loop_serial     10
set c_diag_mode_loop_metal_line  $c_diag_mode_loop_line
set c_diag_mode_loop_par_line   11

set c_diag_duration_debug    0
set c_diag_duration_quick    1
set c_diag_duration_normal   2
set c_diag_duration_extra    3

set c_diag_res_ok            0
set c_diag_res_none          1
set c_diag_res_sync_timeout  2
set c_diag_res_data_timeout  3
set c_diag_res_word_err      4
set c_diag_res_illegal       5

#
# RCU I2C bus
#
set c_rcuh_i2c_addr_rcu      1
set c_rcuh_i2c_addr_hba      2

# I2C handler time resolution
set c_msec                   [expr round(200e6 * 1e-3)]

#
# HBA control
#
#   In the client the registers are stored in order: SPEED, TBM, LED, DUMMY, VREF, STAT.
#   - TBM = measured bit time
#   - STAT = bit 0 contains the comparator state: 0 is high line input level, 1 for low line input level
#   Reading 2 bytes from the SPEED register yields SPEED and TBM. Similar reading 2 bytes from VREF yields
#   VREF and STAT. In fact note that reading 6 bytes from SPEED yields them all.
#
#   In the server the measured bit time is also stored after YDELAY, so via get word from c_hba_sreg_ydelay
#   one gets ydelay and the measured bit time.
#
set c_hba_nof_servers      16       ;# HBA nof servers

set c_hba_cmd_request       0       ;# HBA client REQUEST register
set c_hba_cmd_response      1       ;# HBA client RESPONSE register
set c_hba_cmd_led           2       ;# HBA client LED register
set c_hba_cmd_vref        124       ;# HBA client VREF register
set c_hba_cmd_speed       125       ;# HBA client SPEED register
set c_hba_cmd_version     126       ;# HBA client VERSION register
set c_hba_reg_request_sz   38       ;# register size in octets
set c_hba_reg_response_sz   4
set c_hba_reg_led_sz        1
set c_hba_reg_vref_sz       1
set c_hba_reg_speed_sz      1
set c_hba_reg_version_sz    1

set c_hba_f_set_byte        2       ;# HBA server function codes
set c_hba_f_get_byte        3
set c_hba_f_set_word        4
set c_hba_f_get_word        5
set c_hba_bc_server         0       ;# HBA server broadcast address
set c_hba_sreg_xdelay       0       ;# HBA server xdelay register address
set c_hba_sreg_ydelay       1       ;# HBA server ydelay register address
set c_hba_sreg_version    254       ;# HBA server version register address
set c_hba_sreg_address    255       ;# HBA server address register address

# - Modem time (modem speed = 40 = 10 kbps, modem prescaler = 1): 
#   . Broadcast request: 38 msec seems minimum for set_word broadcast 16 servers
#   . Unicast request  : 11 msec seems minimum for set_word or get word
# - For modem prescaler = 2 the uc_wait = 20 just works at modem speed = 40 = 5.5 kbps,
#   hence to be safe double the wait times.
# - Use hba_gap_wait as minimal wait after every I2C access to the client, before issueing a new I2C access
set hba_prescaler  1
set hba_prescaler  2
set hba_bc_wait   [expr 40*$hba_prescaler]      ;# used with PROTOCOL_C_WAIT and WG_WAIT
set hba_uc_wait   [expr 16*$hba_prescaler]      ;# used with PROTOCOL_C_WAIT and WG_WAIT
set hba_gap_wait   2                            ;# used with PROTOCOL_C_WAIT and WG_WAIT
set hba_bc_i2c    40       ;# used with WG_WAIT, I2C signalling time +30 is enough at 200 M
set hba_uc_i2c    25       ;# used with WG_WAIT, I2C signalling time +20 is enough at 200 M
set hba_reg_i2c    5       ;# used with WG_WAIT, I2C signalling time  +4 is enough at 50 kbps and 327 us comma per byte,
                           ;# so addr, cmd, 4 bytes = 6 bytes * 9 = 54 bits @ 50 kbps = 1.1 ms, 6 bytes * 327 us = 1.9 ms

set c_hba_vref_default   [expr 0xEC]  ;# Reference default mid level setting
set c_hba_speed_default  40
 
#
# TDS timing distribution clock board
#
set c_tds_clksel_10MHz_sma     1
set c_tds_clksel_10MHz_infini  [expr !$c_tds_clksel_10MHz_sma]
set c_tds_clksel_160MHz        0
set c_tds_clksel_200MHz        [expr !$c_tds_clksel_160MHz]
set c_tds_clksel_pps_sma       0
set c_tds_clksel_pps_infini    [expr !$c_tds_clksel_pps_sma]

#
# From tb_rsp.vhd
#
set c_sim_nof_slices_psync   20
