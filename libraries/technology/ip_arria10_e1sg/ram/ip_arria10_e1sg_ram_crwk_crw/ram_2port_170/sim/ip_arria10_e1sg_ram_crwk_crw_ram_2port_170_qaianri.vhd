-- (C) 2001-2017 Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions and other
-- software and tools, and its AMPP partner logic functions, and any output
-- files any of the foregoing (including device programming or simulation
-- files), and any associated documentation or information are expressly subject
-- to the terms and conditions of the Intel Program License Subscription
-- Agreement, Intel MegaCore Function License Agreement, or other applicable
-- license agreement, including, without limitation, that your use is for the
-- sole purpose of programming logic devices manufactured by Intel and sold by
-- Intel or its authorized distributors.  Please refer to the applicable
-- agreement for further details.

library ieee;
use ieee.std_logic_1164.all;

library altera_lnsim;
use altera_lnsim.altera_lnsim_components.all;

entity ip_arria10_e1sg_ram_crwk_crw_ram_2port_170_qaianri is
    port
    (
        address_a       : in std_logic_vector(7 downto 0);
        address_b       : in std_logic_vector(9 downto 0);
        clock_a       : in std_logic  := '1';
        clock_b       : in std_logic;
        data_a       : in std_logic_vector(31 downto 0);
        data_b       : in std_logic_vector(7 downto 0);
        rden_a       : in std_logic  := '1';
        rden_b       : in std_logic  := '1';
        wren_a       : in std_logic  := '0';
        wren_b       : in std_logic  := '0';
        q_a       : out std_logic_vector(31 downto 0);
        q_b       : out std_logic_vector(7 downto 0)
    );
end ip_arria10_e1sg_ram_crwk_crw_ram_2port_170_qaianri;

architecture SYN of ip_arria10_e1sg_ram_crwk_crw_ram_2port_170_qaianri is
    signal sub_wire0    : std_logic_vector(31 downto 0);
    signal sub_wire1    : std_logic_vector(7 downto 0);
begin
    q_a     <= sub_wire0 (31 downto 0);
    q_b     <= sub_wire1 (7 downto 0);

    altera_syncram_component : altera_syncram
    generic map (
            address_reg_b  => "CLOCK1",
            clock_enable_input_a  => "BYPASS",
            clock_enable_input_b  => "BYPASS",
            clock_enable_output_a  => "BYPASS",
            clock_enable_output_b  => "BYPASS",
            indata_reg_b  => "CLOCK1",
            init_file  => "./ram_1024.hex",
            init_file_layout  => "PORT_B",
            intended_device_family  => "Arria 10",
            lpm_type  => "altera_syncram",
            numwords_a  => 256,
            numwords_b  => 1024,
            operation_mode  => "BIDIR_DUAL_PORT",
            outdata_aclr_a  => "NONE",
            outdata_sclr_a  => "NONE",
            outdata_aclr_b  => "NONE",
            outdata_sclr_b  => "NONE",
            outdata_reg_a  => "CLOCK0",
            outdata_reg_b  => "CLOCK1",
            power_up_uninitialized  => "FALSE",
            read_during_write_mode_port_a  => "NEW_DATA_NO_NBE_READ",
            read_during_write_mode_port_b  => "NEW_DATA_NO_NBE_READ",
            widthad_a  => 8,
            widthad_b  => 10,
            width_a  => 32,
            width_b  => 8,
            width_byteena_a  => 1,
            width_byteena_b  => 1
    )
    port map (
        address_a => address_a,
        address_b => address_b,
        clock0 => clock_a,
        clock1 => clock_b,
        data_a => data_a,
        data_b => data_b,
        rden_a => rden_a,
        rden_b => rden_b,
        wren_a => wren_a,
        wren_b => wren_b,
        q_a => sub_wire0,
        q_b => sub_wire1
    );
end SYN;
