-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE, technology_lib, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use technology_lib.technology_pkg.all;

package tech_mac_10g_component_pkg is
  function func_tech_mac_10g_csr_addr_w(c_technology : natural) return natural;

  constant c_tech_mac_10g_link_status_w        : natural := 2;

  type t_tech_mac_10g_xgmii_status_arr is array(integer range <>) of std_logic_vector(c_tech_mac_10g_link_status_w - 1 downto 0);

  constant c_tech_mac_10g_data_w               : natural := c_xgmii_data_w;  -- = 64

  constant c_tech_mac_10g_symbol_w             : natural := c_byte_w;  -- = 8 bit
  constant c_tech_mac_10g_symbols_per_beat     : natural := c_tech_mac_10g_data_w / c_tech_mac_10g_symbol_w;  -- = 8 symbols

  constant c_tech_mac_10g_empty_w              : natural := 3;
  constant c_tech_mac_10g_tx_error_w           : natural := 1;
  constant c_tech_mac_10g_rx_error_w           : natural := 6;

  constant c_tech_mac_10g_tx_fifo_depth        : natural := 256;  -- nof words for Tx FIFO
  constant c_tech_mac_10g_rx_fifo_depth        : natural := 256;  -- nof words for Rx FIFO

  ------------------------------------------------------------------------------
  -- ip_stratixiv
  ------------------------------------------------------------------------------

  -- Copied from entity $HDL_BUILD_DIR/ip_stratixiv_mac_10g_sim/ip_stratixiv_mac_10g.vhd
  component ip_stratixiv_mac_10g is
  port (
    csr_clk_clk                     : in  std_logic                     := '0';  -- csr_clk.clk
    csr_reset_reset_n               : in  std_logic                     := '0';  -- csr_reset.reset_n
    csr_address                     : in  std_logic_vector(12 downto 0) := (others => '0');  -- csr.address
    csr_waitrequest                 : out std_logic;  -- .waitrequest
    csr_read                        : in  std_logic                     := '0';  -- .read
    csr_readdata                    : out std_logic_vector(31 downto 0);  -- .readdata
    csr_write                       : in  std_logic                     := '0';  -- .write
    csr_writedata                   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    tx_clk_clk                      : in  std_logic                     := '0';  -- tx_clk.clk
    tx_reset_reset_n                : in  std_logic                     := '0';  -- tx_reset.reset_n
    avalon_st_tx_startofpacket      : in  std_logic                     := '0';  -- avalon_st_tx.startofpacket
    avalon_st_tx_valid              : in  std_logic                     := '0';  -- .valid
    avalon_st_tx_data               : in  std_logic_vector(63 downto 0) := (others => '0');  -- .data
    avalon_st_tx_empty              : in  std_logic_vector(2 downto 0)  := (others => '0');  -- .empty
    avalon_st_tx_ready              : out std_logic;  -- .ready
    avalon_st_tx_error              : in  std_logic_vector(0 downto 0)  := (others => '0');  -- .error
    avalon_st_tx_endofpacket        : in  std_logic                     := '0';  -- .endofpacket
    avalon_st_pause_data            : in  std_logic_vector(1 downto 0)  := (others => '0');  -- avalon_st_pause.data
    xgmii_tx_data                   : out std_logic_vector(71 downto 0);  -- xgmii_tx.data
    avalon_st_txstatus_valid        : out std_logic;  -- avalon_st_txstatus.valid
    avalon_st_txstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_txstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    rx_clk_clk                      : in  std_logic                     := '0';  -- rx_clk.clk
    rx_reset_reset_n                : in  std_logic                     := '0';  -- rx_reset.reset_n
    xgmii_rx_data                   : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_rx.data
    avalon_st_rx_startofpacket      : out std_logic;  -- avalon_st_rx.startofpacket
    avalon_st_rx_endofpacket        : out std_logic;  -- .endofpacket
    avalon_st_rx_valid              : out std_logic;  -- .valid
    avalon_st_rx_ready              : in  std_logic                     := '0';  -- .ready
    avalon_st_rx_data               : out std_logic_vector(63 downto 0);  -- .data
    avalon_st_rx_empty              : out std_logic_vector(2 downto 0);  -- .empty
    avalon_st_rx_error              : out std_logic_vector(5 downto 0);  -- .error
    avalon_st_rxstatus_valid        : out std_logic;  -- avalon_st_rxstatus.valid
    avalon_st_rxstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_rxstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    link_fault_status_xgmii_rx_data : out std_logic_vector(1 downto 0)  -- link_fault_status_xgmii_rx.data
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10
  ------------------------------------------------------------------------------

  component ip_arria10_mac_10g is
  port (
    csr_read                        : in  std_logic                     := '0';  -- csr.read
    csr_write                       : in  std_logic                     := '0';  -- .write
    csr_writedata                   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    csr_readdata                    : out std_logic_vector(31 downto 0);  -- .readdata
    csr_waitrequest                 : out std_logic;  -- .waitrequest
    csr_address                     : in  std_logic_vector(12 downto 0) := (others => '0');  -- .address
    tx_312_5_clk                    : in  std_logic                     := '0';  -- tx_312_5_clk.clk
    tx_156_25_clk                   : in  std_logic                     := '0';  -- tx_156_25_clk.clk
    rx_312_5_clk                    : in  std_logic                     := '0';  -- rx_312_5_clk.clk
    rx_156_25_clk                   : in  std_logic                     := '0';  -- rx_156_25_clk.clk
    csr_clk                         : in  std_logic                     := '0';  -- csr_clk.clk
    csr_rst_n                       : in  std_logic                     := '0';  -- csr_rst_n.reset_n
    tx_rst_n                        : in  std_logic                     := '0';  -- tx_rst_n.reset_n
    rx_rst_n                        : in  std_logic                     := '0';  -- rx_rst_n.reset_n
    avalon_st_tx_startofpacket      : in  std_logic                     := '0';  -- avalon_st_tx.startofpacket
    avalon_st_tx_endofpacket        : in  std_logic                     := '0';  -- .endofpacket
    avalon_st_tx_valid              : in  std_logic                     := '0';  -- .valid
    avalon_st_tx_data               : in  std_logic_vector(63 downto 0) := (others => '0');  -- .data
    avalon_st_tx_empty              : in  std_logic_vector(2 downto 0)  := (others => '0');  -- .empty
    avalon_st_tx_error              : in  std_logic                     := '0';  -- .error
    avalon_st_tx_ready              : out std_logic;  -- .ready
    avalon_st_pause_data            : in  std_logic_vector(1 downto 0)  := (others => '0');  -- avalon_st_pause.data
    xgmii_tx                        : out std_logic_vector(71 downto 0);  -- xgmii_tx.data
    avalon_st_txstatus_valid        : out std_logic;  -- avalon_st_txstatus.valid
    avalon_st_txstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_txstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    xgmii_rx                        : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_rx.data
    link_fault_status_xgmii_rx_data : out std_logic_vector(1 downto 0);  -- link_fault_status_xgmii_rx.data
    avalon_st_rx_data               : out std_logic_vector(63 downto 0);  -- avalon_st_rx.data
    avalon_st_rx_startofpacket      : out std_logic;  -- .startofpacket
    avalon_st_rx_valid              : out std_logic;  -- .valid
    avalon_st_rx_empty              : out std_logic_vector(2 downto 0);  -- .empty
    avalon_st_rx_error              : out std_logic_vector(5 downto 0);  -- .error
    avalon_st_rx_ready              : in  std_logic                     := '0';  -- .ready
    avalon_st_rx_endofpacket        : out std_logic;  -- .endofpacket
    avalon_st_rxstatus_valid        : out std_logic;  -- avalon_st_rxstatus.valid
    avalon_st_rxstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_rxstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    unidirectional_en               : out std_logic;  -- unidirectional.en
    unidirectional_remote_fault_dis : out std_logic  -- .remote_fault_dis
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e3sge3
  ------------------------------------------------------------------------------

  component ip_arria10_e3sge3_mac_10g is
  port (
    csr_read                        : in  std_logic                     := '0';  -- csr.read
    csr_write                       : in  std_logic                     := '0';  -- .write
    csr_writedata                   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    csr_readdata                    : out std_logic_vector(31 downto 0);  -- .readdata
    csr_waitrequest                 : out std_logic;  -- .waitrequest
    csr_address                     : in  std_logic_vector(12 downto 0) := (others => '0');  -- .address
    tx_312_5_clk                    : in  std_logic                     := '0';  -- tx_312_5_clk.clk
    tx_156_25_clk                   : in  std_logic                     := '0';  -- tx_156_25_clk.clk
    rx_312_5_clk                    : in  std_logic                     := '0';  -- rx_312_5_clk.clk
    rx_156_25_clk                   : in  std_logic                     := '0';  -- rx_156_25_clk.clk
    csr_clk                         : in  std_logic                     := '0';  -- csr_clk.clk
    csr_rst_n                       : in  std_logic                     := '0';  -- csr_rst_n.reset_n
    tx_rst_n                        : in  std_logic                     := '0';  -- tx_rst_n.reset_n
    rx_rst_n                        : in  std_logic                     := '0';  -- rx_rst_n.reset_n
    avalon_st_tx_startofpacket      : in  std_logic                     := '0';  -- avalon_st_tx.startofpacket
    avalon_st_tx_endofpacket        : in  std_logic                     := '0';  -- .endofpacket
    avalon_st_tx_valid              : in  std_logic                     := '0';  -- .valid
    avalon_st_tx_data               : in  std_logic_vector(63 downto 0) := (others => '0');  -- .data
    avalon_st_tx_empty              : in  std_logic_vector(2 downto 0)  := (others => '0');  -- .empty
    avalon_st_tx_error              : in  std_logic                     := '0';  -- .error
    avalon_st_tx_ready              : out std_logic;  -- .ready
    avalon_st_pause_data            : in  std_logic_vector(1 downto 0)  := (others => '0');  -- avalon_st_pause.data
    xgmii_tx                        : out std_logic_vector(71 downto 0);  -- xgmii_tx.data
    avalon_st_txstatus_valid        : out std_logic;  -- avalon_st_txstatus.valid
    avalon_st_txstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_txstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    xgmii_rx                        : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_rx.data
    link_fault_status_xgmii_rx_data : out std_logic_vector(1 downto 0);  -- link_fault_status_xgmii_rx.data
    avalon_st_rx_data               : out std_logic_vector(63 downto 0);  -- avalon_st_rx.data
    avalon_st_rx_startofpacket      : out std_logic;  -- .startofpacket
    avalon_st_rx_valid              : out std_logic;  -- .valid
    avalon_st_rx_empty              : out std_logic_vector(2 downto 0);  -- .empty
    avalon_st_rx_error              : out std_logic_vector(5 downto 0);  -- .error
    avalon_st_rx_ready              : in  std_logic                     := '0';  -- .ready
    avalon_st_rx_endofpacket        : out std_logic;  -- .endofpacket
    avalon_st_rxstatus_valid        : out std_logic;  -- avalon_st_rxstatus.valid
    avalon_st_rxstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_rxstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    unidirectional_en               : out std_logic;  -- unidirectional.en
    unidirectional_remote_fault_dis : out std_logic  -- .remote_fault_dis
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e1sg
  ------------------------------------------------------------------------------

  component ip_arria10_e1sg_mac_10g is
  port (
    csr_read                        : in  std_logic                     := '0';  -- csr.read
    csr_write                       : in  std_logic                     := '0';  -- .write
    csr_writedata                   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    csr_readdata                    : out std_logic_vector(31 downto 0);  -- .readdata
    csr_waitrequest                 : out std_logic;  -- .waitrequest
    csr_address                     : in  std_logic_vector(12 downto 0) := (others => '0');  -- .address
    tx_312_5_clk                    : in  std_logic                     := '0';  -- tx_312_5_clk.clk
    tx_156_25_clk                   : in  std_logic                     := '0';  -- tx_156_25_clk.clk
    rx_312_5_clk                    : in  std_logic                     := '0';  -- rx_312_5_clk.clk
    rx_156_25_clk                   : in  std_logic                     := '0';  -- rx_156_25_clk.clk
    csr_clk                         : in  std_logic                     := '0';  -- csr_clk.clk
    csr_rst_n                       : in  std_logic                     := '0';  -- csr_rst_n.reset_n
    tx_rst_n                        : in  std_logic                     := '0';  -- tx_rst_n.reset_n
    rx_rst_n                        : in  std_logic                     := '0';  -- rx_rst_n.reset_n
    avalon_st_tx_startofpacket      : in  std_logic                     := '0';  -- avalon_st_tx.startofpacket
    avalon_st_tx_endofpacket        : in  std_logic                     := '0';  -- .endofpacket
    avalon_st_tx_valid              : in  std_logic                     := '0';  -- .valid
    avalon_st_tx_data               : in  std_logic_vector(63 downto 0) := (others => '0');  -- .data
    avalon_st_tx_empty              : in  std_logic_vector(2 downto 0)  := (others => '0');  -- .empty
    avalon_st_tx_error              : in  std_logic                     := '0';  -- .error
    avalon_st_tx_ready              : out std_logic;  -- .ready
    avalon_st_pause_data            : in  std_logic_vector(1 downto 0)  := (others => '0');  -- avalon_st_pause.data
    xgmii_tx                        : out std_logic_vector(71 downto 0);  -- xgmii_tx.data
    avalon_st_txstatus_valid        : out std_logic;  -- avalon_st_txstatus.valid
    avalon_st_txstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_txstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    xgmii_rx                        : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_rx.data
    link_fault_status_xgmii_rx_data : out std_logic_vector(1 downto 0);  -- link_fault_status_xgmii_rx.data
    avalon_st_rx_data               : out std_logic_vector(63 downto 0);  -- avalon_st_rx.data
    avalon_st_rx_startofpacket      : out std_logic;  -- .startofpacket
    avalon_st_rx_valid              : out std_logic;  -- .valid
    avalon_st_rx_empty              : out std_logic_vector(2 downto 0);  -- .empty
    avalon_st_rx_error              : out std_logic_vector(5 downto 0);  -- .error
    avalon_st_rx_ready              : in  std_logic                     := '0';  -- .ready
    avalon_st_rx_endofpacket        : out std_logic;  -- .endofpacket
    avalon_st_rxstatus_valid        : out std_logic;  -- avalon_st_rxstatus.valid
    avalon_st_rxstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_rxstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    unidirectional_en               : out std_logic;  -- unidirectional.en
    unidirectional_remote_fault_dis : out std_logic  -- .remote_fault_dis
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10_e2sg
  ------------------------------------------------------------------------------

  component ip_arria10_e2sg_mac_10g is
  port (
    csr_read                        : in  std_logic                     := '0';  -- csr.read
    csr_write                       : in  std_logic                     := '0';  -- .write
    csr_writedata                   : in  std_logic_vector(31 downto 0) := (others => '0');  -- .writedata
    csr_readdata                    : out std_logic_vector(31 downto 0);  -- .readdata
    csr_waitrequest                 : out std_logic;  -- .waitrequest
    csr_address                     : in  std_logic_vector(12 downto 0) := (others => '0');  -- .address
    tx_312_5_clk                    : in  std_logic                     := '0';  -- tx_312_5_clk.clk
    tx_156_25_clk                   : in  std_logic                     := '0';  -- tx_156_25_clk.clk
    rx_312_5_clk                    : in  std_logic                     := '0';  -- rx_312_5_clk.clk
    rx_156_25_clk                   : in  std_logic                     := '0';  -- rx_156_25_clk.clk
    csr_clk                         : in  std_logic                     := '0';  -- csr_clk.clk
    csr_rst_n                       : in  std_logic                     := '0';  -- csr_rst_n.reset_n
    tx_rst_n                        : in  std_logic                     := '0';  -- tx_rst_n.reset_n
    rx_rst_n                        : in  std_logic                     := '0';  -- rx_rst_n.reset_n
    avalon_st_tx_startofpacket      : in  std_logic                     := '0';  -- avalon_st_tx.startofpacket
    avalon_st_tx_endofpacket        : in  std_logic                     := '0';  -- .endofpacket
    avalon_st_tx_valid              : in  std_logic                     := '0';  -- .valid
    avalon_st_tx_data               : in  std_logic_vector(63 downto 0) := (others => '0');  -- .data
    avalon_st_tx_empty              : in  std_logic_vector(2 downto 0)  := (others => '0');  -- .empty
    avalon_st_tx_error              : in  std_logic                     := '0';  -- .error
    avalon_st_tx_ready              : out std_logic;  -- .ready
    avalon_st_pause_data            : in  std_logic_vector(1 downto 0)  := (others => '0');  -- avalon_st_pause.data
    xgmii_tx                        : out std_logic_vector(71 downto 0);  -- xgmii_tx.data
    avalon_st_txstatus_valid        : out std_logic;  -- avalon_st_txstatus.valid
    avalon_st_txstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_txstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    xgmii_rx                        : in  std_logic_vector(71 downto 0) := (others => '0');  -- xgmii_rx.data
    link_fault_status_xgmii_rx_data : out std_logic_vector(1 downto 0);  -- link_fault_status_xgmii_rx.data
    avalon_st_rx_data               : out std_logic_vector(63 downto 0);  -- avalon_st_rx.data
    avalon_st_rx_startofpacket      : out std_logic;  -- .startofpacket
    avalon_st_rx_valid              : out std_logic;  -- .valid
    avalon_st_rx_empty              : out std_logic_vector(2 downto 0);  -- .empty
    avalon_st_rx_error              : out std_logic_vector(5 downto 0);  -- .error
    avalon_st_rx_ready              : in  std_logic                     := '0';  -- .ready
    avalon_st_rx_endofpacket        : out std_logic;  -- .endofpacket
    avalon_st_rxstatus_valid        : out std_logic;  -- avalon_st_rxstatus.valid
    avalon_st_rxstatus_data         : out std_logic_vector(39 downto 0);  -- .data
    avalon_st_rxstatus_error        : out std_logic_vector(6 downto 0);  -- .error
    unidirectional_en               : out std_logic;  -- unidirectional.en
    unidirectional_remote_fault_dis : out std_logic  -- .remote_fault_dis
  );
  end component;
end tech_mac_10g_component_pkg;

package body tech_mac_10g_component_pkg is
  function func_tech_mac_10g_csr_addr_w(c_technology : natural) return natural is
    variable v_csr_addr_w : natural;
  begin
    case c_technology is
      when c_tech_stratixiv => v_csr_addr_w := 13;
      when c_tech_arria10_proto  => v_csr_addr_w := 13;  -- 13 with INSERT_CSR_ADAPTOR=1 in ip_arria10_mac_10g.qsys, 10 without
      when c_tech_arria10_e3sge3 => v_csr_addr_w := 13;  -- 13 with INSERT_CSR_ADAPTOR=1 in ip_arria10_e3sge3_mac_10g.qsys, 10 without
      when c_tech_arria10_e1sg   => v_csr_addr_w := 13;  -- 13 with INSERT_CSR_ADAPTOR=1 in ip_arria10_e1sg_mac_10g.qsys, 10 without
      when c_tech_arria10_e2sg   => v_csr_addr_w := 13;  -- 13 with INSERT_CSR_ADAPTOR=1 in ip_arria10_e1sg_mac_10g.qsys, 10 without
      when others           => v_csr_addr_w := 13;  -- default to c_tech_stratixiv
    end case;

    return v_csr_addr_w;
  end func_tech_mac_10g_csr_addr_w;
end tech_mac_10g_component_pkg;
