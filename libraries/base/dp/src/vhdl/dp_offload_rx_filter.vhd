-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use common_lib.common_field_pkg.all;

entity dp_offload_rx_filter is
  generic (
    g_bypass              : boolean  := false;
    g_nof_streams         : positive := 1;
    g_data_w              : natural;
    g_hdr_field_arr       : t_common_field_arr;
    g_eth_dst_mac_ena     : boolean;
    g_ip_dst_addr_ena     : boolean;
    g_ip_total_length_ena : boolean;
    g_udp_dst_port_ena    : boolean
  );
  port (
    dp_rst            : in  std_logic;
    dp_clk            : in  std_logic;

    snk_in_arr        : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    snk_out_arr       : out t_dp_siso_arr(g_nof_streams - 1 downto 0);

    src_out_arr       : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    src_in_arr        : in  t_dp_siso_arr(g_nof_streams - 1 downto 0);

    hdr_fields_out_arr: in t_slv_1024_arr(g_nof_streams - 1 downto 0);
    hdr_fields_in_arr : in t_slv_1024_arr(g_nof_streams - 1 downto 0)
  );
end dp_offload_rx_filter;

architecture str of dp_offload_rx_filter  is
  constant c_header_w : natural := field_slv_in_len(field_arr_set_mode(g_hdr_field_arr,  "RO"));
  constant c_nof_words: natural := c_header_w / g_data_w;

  constant c_head : natural := 1;
  constant c_tail : natural := 0;

  subtype eth_dst_mac_range     is natural range field_hi(g_hdr_field_arr, "eth_dst_mac"    ) downto field_lo(g_hdr_field_arr, "eth_dst_mac");
  subtype ip_total_length_range is natural range field_hi(g_hdr_field_arr, "ip_total_length") downto field_lo(g_hdr_field_arr, "ip_total_length");
  subtype ip_dst_addr_range     is natural range field_hi(g_hdr_field_arr, "ip_dst_addr"    ) downto field_lo(g_hdr_field_arr, "ip_dst_addr");
  subtype udp_dst_port_range    is natural range field_hi(g_hdr_field_arr, "udp_dst_port"   ) downto field_lo(g_hdr_field_arr, "udp_dst_port");

  type reg_type is record
    valid       : std_logic_vector(g_nof_streams - 1 downto 0);
    src_out_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  end record;

  signal r, rin : reg_type;
begin
  snk_out_arr <= src_in_arr;

  gen_bypass : if g_bypass = true generate
    src_out_arr <= snk_in_arr;
  end generate;

  no_bypass : if g_bypass = false generate
    p_comb : process(hdr_fields_out_arr, snk_in_arr, r, dp_rst)
      variable v : reg_type;
    begin
      v := r;

      v.src_out_arr := snk_in_arr;
      check : for i in 0 to g_nof_streams - 1 loop
        if snk_in_arr(i).sop = '1' then
          v.valid(i) := '1';

          if g_eth_dst_mac_ena     = true and not(hdr_fields_out_arr(i)(eth_dst_mac_range)     = hdr_fields_in_arr(i)(eth_dst_mac_range)     ) then
            v.valid(i) := '0';
          end if;

          if g_ip_total_length_ena = true and not(hdr_fields_out_arr(i)(ip_total_length_range) = hdr_fields_in_arr(i)(ip_total_length_range) ) then
            v.valid(i) := '0';
          end if;

          if g_ip_dst_addr_ena     = true and not(hdr_fields_out_arr(i)(ip_dst_addr_range)     = hdr_fields_in_arr(i)(ip_dst_addr_range)     ) then
            v.valid(i) := '0';
          end if;

          if g_udp_dst_port_ena    = true and not(hdr_fields_out_arr(i)(udp_dst_port_range)    = hdr_fields_in_arr(i)(udp_dst_port_range)    ) then
            v.valid(i) := '0';
          end if;
        end if;
      end loop check;

      if(dp_rst = '1') then
        v.src_out_arr := (others => c_dp_sosi_rst);
        v.valid       := (others => '0');
      end if;

      rin <= v;
    end process;

    process(r)
    begin
      src_out_arr   <= r.src_out_arr;
      set_val : for i in 0 to g_nof_streams - 1 loop
        src_out_arr(i).valid <= r.valid(i) and r.src_out_arr(i).valid;
        src_out_arr(i).sop   <= r.valid(i) and r.src_out_arr(i).sop;
        src_out_arr(i).eop   <= r.valid(i) and r.src_out_arr(i).eop;
      end loop set_val;
    end process;

    p_regs : process(dp_clk)
    begin
      if rising_edge(dp_clk) then
        r <= rin;
      end if;
    end process;
  end generate;
end str;
