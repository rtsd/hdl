#! /usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Test case for unb1_ddr3 design.

   Description: This script controls the diagnostics module and the DDR3 controller.
                It enables the source part of the diagnostics and it enables a write
                operation n the DDR3. When writing is done it initiates a corresponding
                read operation. When reading is done the status of the sink part of 
                the diagnostics is read and verified. 

   Usage: This script can be used for simulation and for hardware. 
          In simulation it should be used in conjunction with tb_unb_ddr3.vhd:

          > python tc_unb1_ddr3.py --unb 0 --bn 3 --sim 

          In hardware it should be started as: 

          > python tc_unb1_ddr3.py --unb 0 --fn 0

"""

###############################################################################
# System imports
import test_case
import node_io
import pi_ddr3
import pi_diagnostics
import pi_io_ddr

import dsp_test
import sys, os
import subprocess  
import time
import pylab as pl
import numpy as np
import scipy as sp
import random
from tools import *
from common import *  
import mem_init_file

###############################################################################

# Create a test case object
tc = test_case.Testcase('TB - ', '')

# Constants/Generics that are shared between VHDL and Python
# Name                   Value   Default   Description
# START_VHDL_GENERICS
# END_VHDL_GENERICS

c_nof_streams = 1 
c_ram_size    = 530579452
c_in_dat_w    = 64
c_sleep_time  = 0
c_nof_ddr3    = 2 # possible options: 1 or 2

if (tc.sim == True):
    c_ram_size   = 8188
    c_sleep_time = 1

tc.append_log(3, '>>>')
tc.append_log(1, '>>> Title : Test bench for unb1_ddr3' )
tc.append_log(3, '>>>')
tc.append_log(3, '')
tc.set_result('PASSED')

# Create access object for nodes              
io = node_io.NodeIO(tc.nodeImages, tc.base_ip)

# Create DDR3 instance
ddr3 = []
for i in range(c_nof_ddr3):
    ddr3.append(pi_ddr3.PiDDR3(tc, io, [i]))

# Create object for DDR register map
ddr = pi_io_ddr.PiIoDdr(tc, io, nof_inst = 1)

# Create diagnostics instance
diag = pi_diagnostics.PiDiagnostics(tc, io, instanceName='', nof_str=c_nof_streams, data_width=c_in_dat_w)

if __name__ == "__main__":      
    
    ## SETUP 
    ddr.write_set_address(data=0)
    ddr.write_access_size(data=256*32)
        
    # Set diag mode on source and sink side to PRBS
    diag.write_src_md(wrData=0) 
    diag.write_snk_md(wrData=0)   
    
    # Enable src gen and snk mon (they are triggered by the ready signal) 
    diag.write_src_en(wrData=1) 
    diag.write_snk_verify_en(wrData=1)   
        
    # Wait for the DDR3 to become available    
    do_until_eq(ddr.read_init_done, ms_retry=3000, val=1, s_timeout=3600)        
    
    # Set DDR3 controller in write mode
    ddr.write_mode_write()
        
    # Clear source and sink of the diagnostics  
    diag.write_src_cnt_clr(wrData=1) 
    diag.write_snk_cnt_clr(wrData=1)   
    
    # Enable the DDR3 controller for writing
    ddr.write_begin_access()
    
    # Wait until controller is done
    tc.sleep(1)
    do_until_eq(ddr.read_done, ms_retry=3000, val=1, s_timeout=3600)        

    wordsWritten = diag.read_src_cnt()

    # Set DDR3 controller in read mode
    ddr.write_mode_read()
    
    # Enable the DDR3 controller for reading
    ddr.write_begin_access()

    do_until_eq(ddr.read_done, ms_retry=3000, val=1, s_timeout=3600)        

    do_until_eq(diag.read_snk_diag_val, ms_retry=3000, val=1, s_timeout=3600)            

    wordsRead = diag.read_snk_cnt()

    value = diag.read_snk_diag_val()
    if value[0] != 1:
      tc.append_log(3, 'No valid data is read back from the DDR3.')
      tc.set_result('FAILED') 
    else:
      tc.append_log(3, 'Valid data is read back from the DDR3.')
    
    result = diag.read_snk_diag_res()
    if result[0] != 0:
      tc.append_log(3, 'Error in data that is read back from the DDR3.')
      tc.set_result('FAILED')  
    else:
      tc.append_log(3, 'Data read back from the DDR3 is OK!')    

    ###############################################################################
    # End
    tc.set_section_id('')
    tc.append_log(3, '')
    tc.append_log(3, '>>>')
    tc.append_log(0, '>>> Test bench result: %s' % tc.get_result())
    tc.append_log(3, '>>>')
    
    sys.exit(tc.get_result())
