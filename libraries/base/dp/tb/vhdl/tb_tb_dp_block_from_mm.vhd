-- --------------------------------------------------------------------------
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- --------------------------------------------------------------------------

-- --------------------------------------------------------------------------
-- Author:
-- . Pieter Donker
-- Purpose:
-- . Test bench running multiple tb_dp_block_from_mm.vhd with different settings
--
-- Description:
--
-- --------------------------------------------------------------------------

-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk

library IEEE;
use IEEE.std_logic_1164.all;
use work.tb_dp_pkg.all;

entity tb_tb_dp_block_from_mm is
end tb_tb_dp_block_from_mm;

architecture tb of tb_tb_dp_block_from_mm is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- used generics for tb_dp_block_from_mm (g_data_size, g_step_size, g_nof_data)
  u0_tst_1_1_1   : entity work.tb_dp_block_from_mm generic map (1, 1, 1);
  u1_tst_2_2_17  : entity work.tb_dp_block_from_mm generic map (2, 2, 17);
  u2_tst_2_2_256 : entity work.tb_dp_block_from_mm generic map (2, 2, 256);
  u3_tst_2_4_256 : entity work.tb_dp_block_from_mm generic map (2, 4, 256);
  u4_tst_2_6_256 : entity work.tb_dp_block_from_mm generic map (2, 6, 256);
  u5_tst_2_8_256 : entity work.tb_dp_block_from_mm generic map (2, 8, 256);
  u6_tst_3_6_17  : entity work.tb_dp_block_from_mm generic map (3, 6, 17);
end tb;
