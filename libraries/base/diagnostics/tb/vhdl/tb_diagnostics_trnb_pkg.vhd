-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.textio.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;

package tb_diagnostics_trnb_pkg is
  type t_e_diagnostics_trnb_mode is (e_prbs, e_counter);

  -- Global procedures

  procedure proc_diagnostics_trnb_tx_set_mode_prbs(   signal   mm_clk    : in  std_logic;
                                                      signal   mm_mosi   : out t_mem_mosi);

  procedure proc_diagnostics_trnb_rx_set_mode_prbs(   signal   mm_clk    : in  std_logic;
                                                      signal   mm_mosi   : out t_mem_mosi);

  procedure proc_diagnostics_trnb_tx_set_mode_counter(constant c_nof_gx_mask : in  natural;
                                                      signal   mm_clk        : in  std_logic;
                                                      signal   mm_mosi       : out t_mem_mosi);

  procedure proc_diagnostics_trnb_rx_set_mode_counter(constant c_nof_gx_mask : in  natural;
                                                      signal   mm_clk        : in  std_logic;
                                                      signal   mm_mosi       : out t_mem_mosi);

  procedure proc_diagnostics_trnb_run_and_verify(constant c_chip_id           : in  natural;
                                                 constant c_nof_gx            : in  natural;
                                                 constant c_nof_gx_mask       : in  integer;
                                                 constant c_link_delay        : in  real;
                                                 constant c_diag_on_interval  : in  real;
                                                 constant c_diag_off_interval : in  real;
                                                 constant c_mm_clk_1us        : in  real;
                                                 signal   mm_clk              : in  std_logic;
                                                 signal   mm_miso             : in  t_mem_miso;
                                                 signal   mm_mosi             : out t_mem_mosi);

  -- Private procedures
end tb_diagnostics_trnb_pkg;

package body tb_diagnostics_trnb_pkg is
  ------------------------------------------------------------------------------
  -- PROCEDURE: Set all gx to default mode PRBS
  ------------------------------------------------------------------------------

  procedure proc_diagnostics_trnb_tx_set_mode_prbs(signal mm_clk  : in  std_logic;
                                                   signal mm_mosi : out t_mem_mosi) is
  begin
    proc_mem_mm_bus_wr( 1, 0, mm_clk, mm_mosi);  -- set source mode 0 = PRBS, 1 = COUNTER
  end procedure proc_diagnostics_trnb_tx_set_mode_prbs;

  procedure proc_diagnostics_trnb_rx_set_mode_prbs(signal mm_clk  : in  std_logic;
                                                   signal mm_mosi : out t_mem_mosi) is
  begin
    proc_mem_mm_bus_wr(20, 0, mm_clk, mm_mosi);  -- set sink   mode 0 = PRBS, 1 = COUNTER
  end procedure proc_diagnostics_trnb_rx_set_mode_prbs;

  ------------------------------------------------------------------------------
  -- PROCEDURE:  Set the masked gx to mode COUNTER and set the unmasked gx to
  --             default mode PRBS
  ------------------------------------------------------------------------------

  procedure proc_diagnostics_trnb_tx_set_mode_counter(constant c_nof_gx_mask : in  natural;
                                                      signal   mm_clk        : in  std_logic;
                                                      signal   mm_mosi       : out t_mem_mosi) is
  begin
    proc_mem_mm_bus_wr( 1, c_nof_gx_mask, mm_clk, mm_mosi);  -- set source mode 0 = PRBS, 1 = COUNTER
  end procedure proc_diagnostics_trnb_tx_set_mode_counter;

  procedure proc_diagnostics_trnb_rx_set_mode_counter(constant c_nof_gx_mask : in  natural;
                                                      signal   mm_clk        : in  std_logic;
                                                      signal   mm_mosi       : out t_mem_mosi) is
  begin
    proc_mem_mm_bus_wr(20, c_nof_gx_mask, mm_clk, mm_mosi);  -- set source mode 0 = PRBS, 1 = COUNTER
  end procedure proc_diagnostics_trnb_rx_set_mode_counter;

  ------------------------------------------------------------------------------
  -- PROCEDURE:  Run a diagnostic measurement and verify the result
  ------------------------------------------------------------------------------

  procedure proc_diagnostics_trnb_run_and_verify(constant c_chip_id           : in  natural;
                                                 constant c_nof_gx            : in  natural;
                                                 constant c_nof_gx_mask       : in  integer;
                                                 constant c_link_delay        : in  real;
                                                 constant c_diag_on_interval  : in  real;
                                                 constant c_diag_off_interval : in  real;
                                                 constant c_mm_clk_1us        : in  real;
                                                 signal   mm_clk              : in  std_logic;
                                                 signal   mm_miso             : in  t_mem_miso;
                                                 signal   mm_mosi             : out t_mem_mosi) is
    constant c_nof_gx_mask_slv    : std_logic_vector(c_nof_gx - 1 downto 0) := TO_UVEC(c_nof_gx_mask, c_nof_gx);
    variable v_diag_results_valid : std_logic_vector(c_nof_gx - 1 downto 0);
    variable v_diag_results       : std_logic_vector(c_nof_gx - 1 downto 0);
  begin
    -- Enable diagnostics
    proc_mem_mm_bus_wr( 0, c_nof_gx_mask, mm_clk, mm_mosi);  -- set source enable
    proc_common_wait_some_cycles(mm_clk, c_mm_clk_1us * c_link_delay);  -- ensure source data has arrived before enabling the sink
    proc_mem_mm_bus_wr(19, c_nof_gx_mask, mm_clk, mm_mosi);  -- set sink enable

    -- Measurement interval
    proc_common_wait_some_cycles(mm_clk, c_mm_clk_1us * c_diag_on_interval);

    -- Read results
    proc_mem_mm_bus_rd(38, mm_clk, mm_miso, mm_mosi);  -- get sink diag result valid
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    v_diag_results_valid := mm_miso.rddata(c_nof_gx - 1 downto 0) and c_nof_gx_mask_slv;
    proc_mem_mm_bus_rd(39, mm_clk, mm_miso, mm_mosi);  -- get sink diag result
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, mm_clk);
    v_diag_results := mm_miso.rddata(c_nof_gx - 1 downto 0) and c_nof_gx_mask_slv;
    proc_common_wait_some_cycles(mm_clk, 1);

    -- Verify results
    assert TO_UINT(v_diag_results_valid) = c_nof_gx_mask and TO_UINT(v_diag_results) = 0
      report "proc_diagnostics_trnb_run_and_verify: TRNB diagnostics on went wrong on node " & natural'image(c_chip_id)
      severity ERROR;

    -- Disable diagnostics
    proc_mem_mm_bus_wr(19, 0, mm_clk, mm_mosi);  -- set sink disable
    proc_common_wait_some_cycles(mm_clk, c_mm_clk_1us * c_link_delay);  -- ensure disabling the sink before the source data has stopped arriving
    proc_mem_mm_bus_wr( 0, 0, mm_clk, mm_mosi);  -- set source disable

    -- Idle interval
    proc_common_wait_some_cycles(mm_clk, c_mm_clk_1us * c_diag_off_interval);
  end procedure proc_diagnostics_trnb_run_and_verify;
end tb_diagnostics_trnb_pkg;
