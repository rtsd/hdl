###############################################################################
#
# Copyright (C) 2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Constrain the input I/O path
#set_input_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] -max 3 [all_inputs]
#set_input_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] -min 2 [all_inputs]
# Constrain the output I/O path
#set_output_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] -max 3 [all_inputs]
#set_output_delay -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] -min 2 [all_inputs]


# False path the PPS to DDIO:
#set_input_delay  -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] 3 [get_ports {PPS}]
#set_false_path -from {PPS} -to {ctrl_unb2b_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e1sg_ddio_in:\gen_ip_arria10_e1sg:u0|ip_arria10_e1sg_ddio_in_1:\gen_w:0:u_ip_arria10_e1sg_ddio_in_1|ip_arria10_e1sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio~ddio_in_fr}; set_false_path -from {PPS} -to {ctrl_unb2b_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e1sg_ddio_in:\gen_ip_arria10_e1sg:u0|ip_arria10_e1sg_ddio_in_1:\gen_w:0:u_ip_arria10_e1sg_ddio_in_1|ip_arria10_e1sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio~ddio_in_fr}


#set_false_path -from [get_ports {PPS}] -to [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}]

#set_input_delay -min -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] 2 [get_ports {ctrl_unb2b_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|pps_ext_cap}]
#set_input_delay -max -clock [get_clocks {u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2b_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk0}] 4 [get_ports {ctrl_unb2b_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|pps_ext_cap}]

#set_false_path -from {PPS} -to {ctrl_unb2b_board:u_ctrl|mms_ppsh:u_mms_ppsh|ppsh:u_ppsh|common_ddio_in:u_in|tech_iobuf_ddio_in:u_ddio_in|ip_arria10_e1sg_ddio_in:\gen_ip_arria10_e1sg:u0|ip_arria10_e1sg_ddio_in_1:\gen_w:0:u_ip_arria10_e1sg_ddio_in_1|ip_arria10_e1sg_ddio_in_1_altera_gpio_151_ia6gnqq:ip_arria10_ddio_in_1|altera_gpio:core|altera_gpio_one_bit:gpio_one_bit.i_loop[0].altera_gpio_bit_i|input_path.in_path_fr.buffer_data_in_fr_ddio*}



set_time_format -unit ns -decimal_places 3

create_clock -period 125Mhz [get_ports {ETH_CLK}]
create_clock -period 200Mhz [get_ports {CLK}]
create_clock -period 100Mhz [get_ports {CLKUSR}]
create_clock -period 644.53125Mhz [get_ports {SA_CLK}]
create_clock -period 644.53125Mhz [get_ports {SB_CLK}]
create_clock -period 200MHz -name {BCK_REF_CLK} { BCK_REF_CLK }
#create_clock -period 100MHz -name {BCK_REF_CLK} { BCK_REF_CLK }

derive_pll_clocks
derive_clock_uncertainty

set_clock_groups -asynchronous -group {CLK}
set_clock_groups -asynchronous -group {BCK_REF_CLK}
set_clock_groups -asynchronous -group {CLK_USR}
set_clock_groups -asynchronous -group {CLKUSR}
set_clock_groups -asynchronous -group {SA_CLK}
set_clock_groups -asynchronous -group {SB_CLK}
# Do not put ETH_CLK in this list, otherwise the Triple Speed Ethernet does not work

# IOPLL outputs (which have global names defined in the IP qsys settings)
set_clock_groups -asynchronous -group [get_clocks pll_clk20]
set_clock_groups -asynchronous -group [get_clocks pll_clk50]
set_clock_groups -asynchronous -group [get_clocks pll_clk100]
set_clock_groups -asynchronous -group [get_clocks pll_clk125]
set_clock_groups -asynchronous -group [get_clocks pll_clk200]
set_clock_groups -asynchronous -group [get_clocks pll_clk200p]
set_clock_groups -asynchronous -group [get_clocks pll_clk400]


# FPLL outputs
#set_clock_groups -asynchronous -group [get_clocks {*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*mac_clock*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*dp_clk*xcvr_fpll_a10_0|outclk0}]
#set_clock_groups -asynchronous -group [get_clocks {*xcvr_fpll_a10_0|outclk1}]
set_clock_groups -asynchronous -group [get_clocks {*xcvr_fpll_a10_0|outclk3}]


set_clock_groups -asynchronous -group [get_clocks {*xcvr_native_a10_0|g_xcvr_native_insts[*]|rx_pma_clk}]

#set_false_path -from {*u_rst200|u_async|din_meta[2]} -to {*FIFOram*}

#set_clock_groups -asynchronous \
#-group [get_clocks {inst2|xcvr_4ch_native_phy_inst|xcvr_native_a10_0|g_xcvr_native_insts[?]|rx_pma_clk}] \
#-group [get_clocks {inst2|xcvr_pll_inst|xcvr_fpll_a10_0|tx_bonding_clocks[0]}]


set_false_path -from [get_clocks {u_revision|u_ctrl|\gen_mm_clk_hardware:u_unb2b_board_clk125_pll|\gen_fractional_pll:u_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk2}] -to [get_clocks {u_revision|u_ait|u_jesd204b|\gen_ip_arria10_e1sg:u0|u_ip_arria10_e1sg_jesd204b|\gen_jesd204b_rx:gen_jesd204b_rx_corepll_freqsel:u_ip_arria10_e1sg_jesd204b_rx_corepll_200MHz|iopll_0|frame_clk}]
set_false_path -from [get_clocks {u_revision|u_ctrl|\gen_mm_clk_hardware:u_unb2b_board_clk125_pll|\gen_fractional_pll:u_pll|\gen_ip_arria10_e1sg:u0|xcvr_fpll_a10_0|outclk2}] -to [get_clocks {u_revision|u_ait|u_jesd204b|\gen_ip_arria10_e1sg:u0|u_ip_arria10_e1sg_jesd204b|\gen_jesd204b_rx:gen_jesd204b_rx_corepll_freqsel:u_ip_arria10_e1sg_jesd204b_rx_corepll_200MHz|iopll_0|link_clk}]


# false paths added for the jesd test design
#set_false_path -from [get_clocks {*xcvr_fpll_a10_0|outclk2}] -to [get_clocks {*corepll_200MHz|link_clk}]
#set_false_path -from [get_clocks {*corepll_200MHz|link_clk}] -to [get_clocks {*xcvr_fpll_a10_0|outclk2}]
#set_false_path -from [get_clocks {*xcvr_fpll_a10_0|outclk2}] -to [get_clocks {*corepll_200MHz|frame_clk}]
#set_false_path -from [get_clocks {*corepll_200MHz|frame_clk}] -to [get_clocks {*xcvr_fpll_a10_0|outclk2}]

# Constraint on the SYSREF input pin
#    Adjust this to account for any board trace difference between SYSREF and REFCLK
# See page 150: https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug_jesd204b.pdf
set_input_delay -clock BCK_REF_CLK 0 [get_ports JESD204B_SYSREF]
