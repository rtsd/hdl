-------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Author:
--   E. Kooistra        Feb 2017  Initial.
-- Purpose: Multi testbench of tb_mm_file to verify mm_file and mm_file_pkg.
-- Usage:
-- > as 4
-- > run -all

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_mm_file is
end tb_tb_mm_file;

architecture tb of tb_tb_mm_file is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_tb_index           : NATURAL := 0;
  -- g_mm_nof_accesses    : NATURAL := 100;
  -- g_mm_timeout         : TIME := sel_a_b(g_mm_throttle_en, 1 ns, 0 ns);  -- default 0 for full speed MM, use > 0 to define number of mm_clk without MM access after which the MM file IO is paused
  -- g_mm_pause           : TIME := 1 us;                                   -- defines the time for which MM file IO is paused to reduce the file IO rate when the MM slave is idle
  -- g_timeout_gap        : INTEGER := -1;    -- no gap when < 0, else force MM access gap after g_timeout_gap wr or rd strobes
  -- g_cross_clock_domain : BOOLEAN := FALSE --TRUE

  u_one_clk                        : entity work.tb_mm_file generic map (0, 10000,   0 ns, 1 us, -1, false);
  u_one_clk_mm_throttle            : entity work.tb_mm_file generic map (1, 10000, 100 ns, 1 us, -1, false);
  u_cross_clk                      : entity work.tb_mm_file generic map (2,  1000,   0 ns, 1 us, -1, true);
  u_cross_clk_mm_throttle          : entity work.tb_mm_file generic map (3,  1000, 100 ns, 1 us, -1, true);
  u_with_gap_one_clk               : entity work.tb_mm_file generic map (4, 10000,   0 ns, 1 us,  3, false);
  u_with_gap_one_clk_mm_throttle   : entity work.tb_mm_file generic map (5, 10000, 100 ns, 1 us,  3, false);
  u_with_gap_cross_clk             : entity work.tb_mm_file generic map (6,  1000,   0 ns, 1 us,  3, true);
  u_with_gap_cross_clk_mm_throttle : entity work.tb_mm_file generic map (7,  1000, 100 ns, 1 us,  3, true);
end tb;
