-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Test bench compaan_unb1_dp_offload.
-- Description:
-- . The block generator in the design is enabled by default.
-- . Design 'compaan_unb1_dp_offload' requires the 1GbE demux to be set up so
--   received streams are forwarded based on the received destination UDP port.
--   This is done by /tb/python/tc_unb1_board1_dp_offload.py.
-- Usage (manual mode, run compaan_unb1_dp_offload.py for auto mode):
-- . Start ModelSim
-- . lp compaan_unb1_dp_offload
-- . mk compile
-- . double click simulation configuration
-- . as 8
-- . run 2us (wait until MM master did the initial ETH settings before Python ETH access)
-- . in separate console: python tc_unb1_board1_dp_offload.py --unb 0 --fn 0 -r 0:2 --sim
-- . run -a

library IEEE, common_lib, unb1_board_lib, i2c_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use common_lib.tb_common_pkg.all;

entity tb_unb1_eth_10g is
end tb_unb1_eth_10g;

architecture tb of tb_unb1_eth_10g is
  constant c_sim             : boolean := true;

  constant c_unb1_board_nr   : natural := 0;  -- UniBoard 0
  constant c_node_nr_lcu     : natural := 3;  -- FN3
  constant c_node_nr_dut     : natural := 2;  -- FN2
  constant c_id_lcu          : std_logic_vector(7 downto 0) := TO_UVEC(c_unb1_board_nr, c_unb1_board_nof_uniboard_w ) & TO_UVEC(c_node_nr_lcu, c_unb1_board_nof_chip_w);
  constant c_id_dut          : std_logic_vector(7 downto 0) := TO_UVEC(c_unb1_board_nr, c_unb1_board_nof_uniboard_w ) & TO_UVEC(c_node_nr_dut, c_unb1_board_nof_chip_w);
  constant c_version         : std_logic_vector(1 downto 0) := "00";

  constant c_eth_clk_period  : time := 40 ns;
  constant c_sa_clk_period   : time := 6.4 ns;
  constant c_clk_period      : time := 5 ns;

  signal clk                 : std_logic := '0';
  signal sa_clk              : std_logic := '1';

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic;
  signal eth_rxp             : std_logic;

  signal si_fn_0_lcu_tx      : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_1_lcu_tx      : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_2_lcu_tx      : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_3_lcu_tx      : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);

  signal si_fn_0_lcu_rx      : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_1_lcu_rx      : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_2_lcu_rx      : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);
  signal si_fn_3_lcu_rx      : std_logic_vector(c_unb1_board_ci.tr.bus_w - 1 downto 0);

  signal VERSION             : std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0) := c_version;
  signal ID_lcu              : std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0) := c_id_lcu;
  signal ID_dut              : std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0) := c_id_dut;
begin
  ----------------------------------------------------------------------------
  -- Externally generated clocks
  ----------------------------------------------------------------------------
  clk     <= not clk after c_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (25 MHz)
  sa_clk  <= not sa_clk  after c_sa_clk_period / 2;

  ------------------------------------------------------------------------------
  -- u_lcu
  ------------------------------------------------------------------------------
  u_lcu : entity work.unb1_eth_10g
  generic map (
    g_sim          => c_sim,
    g_sim_unb_nr   => c_unb1_board_nr,
    g_sim_node_nr  => c_node_nr_lcu
  )
  port map (
    CLK         => clk,
    PPS         => '0',
    VERSION     => VERSION,
    ID          => ID_lcu,

    -- 1GbE Control Interface
    ETH_clk     => eth_clk,
    ETH_SGIN    => eth_rxp,
    ETH_SGOUT   => eth_txp,

    -- Transceiver clocks
    SA_CLK      => sa_clk,

    -- Serial I/O
    SI_FN_0_RX  => si_fn_0_lcu_rx,
    SI_FN_1_RX  => si_fn_1_lcu_rx,
    SI_FN_2_RX  => si_fn_2_lcu_rx,
    SI_FN_3_RX  => si_fn_3_lcu_rx,
    SI_FN_0_TX  => si_fn_0_lcu_tx,
    SI_FN_1_TX  => si_fn_1_lcu_tx,
    SI_FN_2_TX  => si_fn_2_lcu_tx,
    SI_FN_3_TX  => si_fn_3_lcu_tx
  );

  ------------------------------------------------------------------------------
  -- u_dut
  ------------------------------------------------------------------------------
  u_dut : entity work.unb1_eth_10g
  generic map (
    g_sim          => c_sim,
    g_sim_unb_nr   => c_unb1_board_nr,
    g_sim_node_nr  => c_node_nr_dut
  )
  port map (
    CLK         => clk,
    PPS         => '0',
    VERSION     => VERSION,
    ID          => ID_dut,

    -- 1GbE Control Interface
    ETH_clk     => eth_clk,
    ETH_SGIN    => eth_rxp,
    ETH_SGOUT   => eth_txp,

    -- Transceiver clocks
    SA_CLK      => sa_clk,

    -- Serial I/O
    SI_FN_0_RX  => si_fn_0_lcu_tx,
    SI_FN_1_RX  => si_fn_1_lcu_tx,
    SI_FN_2_RX  => si_fn_2_lcu_tx,
    SI_FN_3_RX  => si_fn_3_lcu_tx,
    SI_FN_0_TX  => si_fn_0_lcu_rx,
    SI_FN_1_TX  => si_fn_1_lcu_rx,
    SI_FN_2_TX  => si_fn_2_lcu_rx,
    SI_FN_3_TX  => si_fn_3_lcu_rx
  );
end tb;
