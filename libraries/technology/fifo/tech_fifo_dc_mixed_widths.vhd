-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer

library ieee, technology_lib;
use ieee.std_logic_1164.all;
use work.tech_fifo_component_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_fifo_lib;
library ip_arria10_fifo_lib;
library ip_arria10_e3sge3_fifo_lib;
library ip_arria10_e1sg_fifo_lib;
library ip_arria10_e2sg_fifo_lib;
library ip_ultrascale_fifo_lib;
library ip_agi027_1e1v_fifo_lib;

entity tech_fifo_dc_mixed_widths is
  generic (
    g_technology : natural := c_tech_select_default;
    g_nof_words  : natural;  -- FIFO size in nof wr_dat words
    g_wrdat_w    : natural;
    g_rddat_w    : natural
  );
  port (
    aclr    : in std_logic  := '0';
    data    : in std_logic_vector(g_wrdat_w - 1 downto 0);
    rdclk   : in std_logic;
    rdreq   : in std_logic;
    wrclk   : in std_logic;
    wrreq   : in std_logic;
    q       : out std_logic_vector(g_rddat_w - 1 downto 0);
    rdempty : out std_logic;
    rdusedw : out std_logic_vector(tech_ceil_log2(g_nof_words * g_wrdat_w / g_rddat_w) - 1 downto 0);
    wrfull  : out std_logic;
    wrusedw : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
end tech_fifo_dc_mixed_widths;

architecture str of tech_fifo_dc_mixed_widths is
begin
  gen_ip_stratixiv : if g_technology = c_tech_stratixiv generate
    u0 : ip_stratixiv_fifo_dc_mixed_widths
    generic map (g_nof_words, g_wrdat_w, g_rddat_w)
    port map (aclr, data, rdclk, rdreq, wrclk, wrreq, q, rdempty, rdusedw, wrfull, wrusedw);
  end generate;

  gen_ip_arria10 : if g_technology = c_tech_arria10_proto generate
    u0 : ip_arria10_fifo_dc_mixed_widths
    generic map (g_nof_words, g_wrdat_w, g_rddat_w)
    port map (aclr, data, rdclk, rdreq, wrclk, wrreq, q, rdempty, rdusedw, wrfull, wrusedw);
  end generate;

  gen_ip_arria10_e3sge3 : if g_technology = c_tech_arria10_e3sge3 generate
    u0 : ip_arria10_e3sge3_fifo_dc_mixed_widths
    generic map (g_nof_words, g_wrdat_w, g_rddat_w)
    port map (aclr, data, rdclk, rdreq, wrclk, wrreq, q, rdempty, rdusedw, wrfull, wrusedw);
  end generate;

  gen_ip_arria10_e1sg : if g_technology = c_tech_arria10_e1sg generate
    u0 : ip_arria10_e1sg_fifo_dc_mixed_widths
    generic map (g_nof_words, g_wrdat_w, g_rddat_w)
    port map (aclr, data, rdclk, rdreq, wrclk, wrreq, q, rdempty, rdusedw, wrfull, wrusedw);
  end generate;

  gen_ip_arria10_e2sg : if g_technology = c_tech_arria10_e2sg generate
    u0 : ip_arria10_e2sg_fifo_dc_mixed_widths
    generic map (g_nof_words, g_wrdat_w, g_rddat_w)
    port map (aclr, data, rdclk, rdreq, wrclk, wrreq, q, rdempty, rdusedw, wrfull, wrusedw);
  end generate;

  gen_ip_ultrascale : if g_technology = c_tech_ultrascale generate
    u0 : ip_ultrascale_fifo_dc_mixed_widths
    generic map (g_nof_words, g_wrdat_w, g_rddat_w)
    port map (aclr, data, rdclk, rdreq, wrclk, wrreq, q, rdempty, rdusedw, wrfull, wrusedw);
  end generate;

  gen_ip_agi027_1e1v : if g_technology = c_tech_agi027_1e1v generate
    u0 : ip_agi027_1e1v_fifo_dc_mixed_widths
    generic map (g_nof_words, g_wrdat_w, g_rddat_w)
    port map (aclr, data, rdclk, rdreq, wrclk, wrreq, q, rdempty, rdusedw, wrfull, wrusedw);
  end generate;
end architecture;
