-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Select a subset of the input data. Reorder the  input data. Redistribute data over multiple outputs.
--
-- Description: This unit creates a parallel set of output streams where each
--              stream can contain data from any input stream.
--
--              The selection mechanism is based on three stages:
--
--              1. Input Reorder Stage
--              2. Selection Stage
--              3. Output Reorder Stage
--
--              1. The input reorder stage redirects the data (within a frame) of
--              the g_nof_inputs input streams to a set of g_nof_internals
--              streams, based on the settings of the selection buffer. The
--              selection buffer contains a selection setting for every clock
--              cycle of a frame.
--
--              2. The selection stage creates output streams that can contain any
--              data in any order from the accordingly input streams.
--
--              3. The output reorder stage performs another reordering stage on
--              the output of the selection stage.

--
-- Remarks:
--

library IEEE, common_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;

entity ss_parallel is
  generic (
    g_technology            : natural := c_tech_select_default;
    g_nof_inputs            : natural := 8;
    g_nof_internals         : natural := 16;
    g_nof_outputs           : natural := 16;
    g_dsp_data_w            : natural := 16;
    g_frame_size_in         : natural := 256;
    g_frame_size_out        : natural := 192;
    g_reorder_in_file_name  : string  := "UNUSED";  -- path_to_file.hex
    g_ss_wide_file_prefix   : string  := "UNUSED";  -- path_to_file
    g_reorder_out_file_name : string  := "UNUSED"  -- path_to_file.hex
  );
  port (
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;
    dp_rst                  : in  std_logic;
    dp_clk                  : in  std_logic;

    -- Memory Mapped
    ram_ss_reorder_in_mosi  : in  t_mem_mosi;
    ram_ss_reorder_in_miso  : out t_mem_miso;
    ram_ss_reorder_out_mosi : in  t_mem_mosi;
    ram_ss_reorder_out_miso : out t_mem_miso;
    ram_ss_ss_wide_mosi     : in  t_mem_mosi;
    ram_ss_ss_wide_miso     : out t_mem_miso;

    -- Streaming
    input_sosi_arr          : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);
    input_siso_arr          : out t_dp_siso_arr(g_nof_inputs - 1 downto 0);
    output_sosi_arr         : out t_dp_sosi_arr(g_nof_outputs - 1 downto 0);
    output_siso_arr         : in  t_dp_siso_arr(g_nof_outputs - 1 downto 0) := (others => c_dp_siso_rdy)
  );
end ss_parallel;

architecture str of ss_parallel is
  signal  ss_wide_in_sosi_arr  : t_dp_sosi_arr(g_nof_internals - 1 downto 0) := (others => c_dp_sosi_rst);
  signal  ss_wide_out_sosi_arr : t_dp_sosi_arr(g_nof_internals - 1 downto 0) := (others => c_dp_sosi_rst);
begin
  -----------------------------------------------------------------------------
  -- Throttle the incoming streams so they provide a consistent packet flow
  -- (no bursting) by enforcing a minimum period of g_frame_size_out
  -----------------------------------------------------------------------------
  gen_dp_throttle_sop : for i in 0 to g_nof_inputs - 1 generate
    u_dp_throttle_sop : entity dp_lib.dp_throttle_sop
    generic map (
      g_period    => g_frame_size_out
    )
    port map (
      rst         => dp_rst,
      clk         => dp_clk,
      snk_out     => input_siso_arr(i),
      snk_in      => input_sosi_arr(i)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Reorder input streams
  -----------------------------------------------------------------------------
  u_input_reorder : entity work.ss_reorder
  generic map(
    g_technology        => g_technology,
    g_nof_inputs        => g_nof_inputs,
    g_nof_outputs       => g_nof_internals,
    g_dsp_data_w        => g_dsp_data_w,
    g_frame_size        => g_frame_size_in,
    g_ram_init_file     => g_reorder_in_file_name,
    g_pipeline_in       => 1,
    g_pipeline_in_m     => 1,
    g_pipeline_out      => 1
  )
  port map(
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,
    dp_rst              => dp_rst,
    dp_clk              => dp_clk,

    -- Memory Mapped
    ram_ss_reorder_mosi => ram_ss_reorder_in_mosi,
    ram_ss_reorder_miso => ram_ss_reorder_in_miso,

    -- Streaming
    input_sosi_arr      => input_sosi_arr,
    output_sosi_arr     => ss_wide_in_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- Serial word selection per stream
  -----------------------------------------------------------------------------
  u_ss_wide : entity work.ss_wide
  generic map (
    g_technology         => g_technology,
    g_wb_factor          => g_nof_internals,
    g_dsp_data_w         => g_dsp_data_w,
    g_nof_ch_in          => g_frame_size_in,
    g_nof_ch_sel         => g_frame_size_out,
    g_select_file_prefix => g_ss_wide_file_prefix
  )
  port map (
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,

    -- Memory Mapped
    ram_ss_ss_wide_mosi  => ram_ss_ss_wide_mosi,
    ram_ss_ss_wide_miso  => ram_ss_ss_wide_miso,

    -- Streaming
    input_sosi_arr       => ss_wide_in_sosi_arr,
    input_siso_arr       => OPEN,
    output_sosi_arr      => ss_wide_out_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- Reorder output streams
  -----------------------------------------------------------------------------
  u_output_reorder : entity work.ss_reorder
  generic map(
    g_technology        => g_technology,
    g_nof_inputs        => g_nof_internals,
    g_nof_outputs       => g_nof_outputs,
    g_dsp_data_w        => g_dsp_data_w,
    g_frame_size        => g_frame_size_out,
    g_ram_init_file     => g_reorder_out_file_name,
    g_pipeline_in       => 1,
    g_pipeline_in_m     => 1,
    g_pipeline_out      => 1
  )
  port map(
    mm_rst              => mm_rst,
    mm_clk              => mm_clk,
    dp_rst              => dp_rst,
    dp_clk              => dp_clk,

    -- Memory Mapped
    ram_ss_reorder_mosi => ram_ss_reorder_out_mosi,
    ram_ss_reorder_miso => ram_ss_reorder_out_miso,

    -- Streaming
    input_sosi_arr      => ss_wide_out_sosi_arr,
    output_sosi_arr     => output_sosi_arr
  );
end str;
