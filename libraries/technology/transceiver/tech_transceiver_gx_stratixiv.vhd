--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Declare IP libraries to ensure default binding in simulation. The IP library clause is ignored by synthesis.
library ip_stratixiv_transceiver_lib;

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use work.tech_transceiver_component_pkg.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity tech_transceiver_gx_stratix_iv is
  generic(
    g_data_w                : natural := 32;  -- Supported: 32, 16.
    g_nof_gx                : natural;
    g_mbps                  : natural;  -- Supported: 6250, 5000, 3125, 2500 when g_data_w=32, else 2500 when g_data_w=16
    g_sim                   : boolean;
    g_tx                    : boolean;
    g_rx                    : boolean
  );
  port(
    cal_rec_clk             : in  std_logic;
    tr_clk                  : in  std_logic;

    rx_clk                  : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_rst                  : out std_logic_vector(g_nof_gx - 1 downto 0);
    rx_sosi_arr             : out t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    rx_siso_arr             : in  t_dp_siso_arr(g_nof_gx - 1 downto 0);

    tx_clk                  : out std_logic_vector(g_nof_gx - 1 downto 0);
    tx_rst                  : out std_logic_vector(g_nof_gx - 1 downto 0);
    tx_sosi_arr             : in  t_dp_sosi_arr(g_nof_gx - 1 downto 0);
    tx_siso_arr             : out t_dp_siso_arr(g_nof_gx - 1 downto 0);

    --Serial I/O
    rx_datain               : in  std_logic_vector(g_nof_gx - 1 downto 0);
    tx_dataout              : out std_logic_vector(g_nof_gx - 1 downto 0);

    -- Ctrl&stat
    tx_state                : out std_logic_vector(2 * g_nof_gx - 1 downto 0);
    tx_align_en             : in  std_logic_vector(g_nof_gx - 1 downto 0);

    rx_state                : out std_logic_vector(2 * g_nof_gx - 1 downto 0);
    rx_align_en             : in  std_logic_vector(g_nof_gx - 1 downto 0)
  );
end tech_transceiver_gx_stratix_iv;

architecture str of tech_transceiver_gx_stratix_iv is
  constant c_align_pattern_32      : std_logic_vector := x"0000BC1C";  -- Change in megafunction first. BC is std. comma symbol (K.28.5).
  constant c_inval_pattern_32      : std_logic_vector := x"3C3C3C3C";  -- comma symbol K.28.1 (decoded as 3C) is used as invalid data indicator.

  constant c_align_pattern_16      : std_logic_vector := x"BCBC";  -- Change in megafunction first. BC is std. comma symbol (K.28.5).
  constant c_inval_pattern_16      : std_logic_vector := x"BCBC";

  constant c_align_pattern         : std_logic_vector(g_data_w - 1 downto 0) := sel_a_b(g_data_w = 16, c_align_pattern_16, c_align_pattern_32);
  constant c_inval_pattern         : std_logic_vector(g_data_w - 1 downto 0) := sel_a_b(g_data_w = 16, c_inval_pattern_16, c_inval_pattern_32);

  constant c_reconf_togxb_bus_w    : natural := 4;
  constant c_reconf_fromgxb_bus_w  : natural := 17;

  constant c_nof_ctrl_bytes        : natural := g_data_w / c_byte_w;

  type t_data_arr is array (natural range <>) of std_logic_vector(g_data_w - 1 downto 0);
  type t_ctrl_arr is array (natural range <>) of std_logic_vector(c_nof_ctrl_bytes - 1 downto 0);

  signal reconfig_togxb            : std_logic_vector(c_reconf_togxb_bus_w - 1 downto 0);
  signal reconfig_fromgxb          : std_logic_vector(c_reconf_fromgxb_bus_w * g_nof_gx - 1 downto 0);

  signal rx_freq_locked            : std_logic_vector(g_nof_gx - 1 downto 0) := (others => '0');
  signal tx_pll_locked             : std_logic_vector(g_nof_gx - 1 downto 0) := (others => '0');
  signal rec_busy                  : std_logic;

  signal trc_tx_pll_powerdown      : std_logic;
  signal trc_tx_digital_rst        : std_logic;

  signal trc_rx_analog_rst         : std_logic;
  signal trc_rx_digital_rst        : std_logic;

  signal i_tx_clk                  : std_logic_vector(g_nof_gx - 1 downto 0);
  signal i_rx_clk                  : std_logic_vector(g_nof_gx - 1 downto 0);

  signal i_tx_rst                  : std_logic_vector(g_nof_gx - 1 downto 0);
  signal i_rx_rst                  : std_logic_vector(g_nof_gx - 1 downto 0);

  signal tr_rst                    : std_logic;

  signal trc_tx_rdy                : std_logic;
  signal trc_rx_rdy                : std_logic;

  signal not_trc_tx_rdy            : std_logic;
  signal not_trc_rx_rdy            : std_logic;

  signal tx_align_en_out           : std_logic_vector(g_nof_gx - 1 downto 0) := (others => '0');

  signal tx_datain                 : t_data_arr(g_nof_gx - 1 downto 0);
  signal rx_dataout                : t_data_arr(g_nof_gx - 1 downto 0);

  signal txc_tx_ctrlenable         : t_ctrl_arr(g_nof_gx - 1 downto 0);
  signal rxc_rx_ctrldetect         : t_ctrl_arr(g_nof_gx - 1 downto 0);

  signal rxc_rx_align_en           : std_logic_vector(g_nof_gx - 1 downto 0);  -- edge sensitive
  signal rxc_rx_aligned            : std_logic_vector(g_nof_gx - 1 downto 0) := (others => '1');
begin
  ------------------------------------------------------------------------------
  -- ALTGX Megafunction
  ------------------------------------------------------------------------------

  gen_altgx: for i in 0 to g_nof_gx - 1 generate  -- Generate g_nof_gx one-channel ALTGX instances

    gen_tr : if g_tx = true and g_rx = true generate  -- Generate duplex transceivers

      gen_32b : if g_data_w = 32 generate
        u_gx: ip_stratixiv_hssi_gx_32b_generic
        generic map (
          g_mbps                   => g_mbps,
          starting_channel_number  => i * 4  -- Starting channel numbers must be 0,4,8,12 etc for individual ALTGX instances
        )  -- Reconfig megawizard: regenerate with 'number of channels' = 4*nof_gx
        port map (
          -- Clocks, in
          pll_inclk           => tr_clk,
          cal_blk_clk         => cal_rec_clk,
          reconfig_clk        => cal_rec_clk,

          -- Clocks, generated (out)
          sl(tx_clkout)       => i_tx_clk(i),
          sl(rx_clkout)       => i_rx_clk(i),

          -- Parallel data I/O
          rx_dataout          => rx_sosi_arr(i).data(g_data_w - 1 downto 0),
          tx_datain           => tx_datain(i),

          -- Serial data I/O
          rx_datain           => slv(rx_datain(i)),
          sl(tx_dataout)      => tx_dataout(i),

          -- Initialization I/O
          sl(rx_freqlocked)   => rx_freq_locked(i),
          sl(pll_locked)      => tx_pll_locked(i),
          pll_powerdown       => slv(trc_tx_pll_powerdown),
          rx_digitalreset     => slv(trc_rx_digital_rst),
          rx_analogreset      => slv(trc_rx_analog_rst),
          tx_digitalreset     => slv(trc_tx_digital_rst),

          -- 8b/10b related
          tx_ctrlenable       => txc_tx_ctrlenable(i),  -- 8b10b
          rx_ctrldetect       => rxc_rx_ctrldetect(i),  -- 8b10b
          rx_enapatternalign  => slv(rxc_rx_align_en(i)),

          -- Reconfig block connections
          reconfig_togxb      => reconfig_togxb,
          reconfig_fromgxb    => reconfig_fromgxb(c_reconf_fromgxb_bus_w * (i + 1) - 1 downto c_reconf_fromgxb_bus_w * (i + 1) - c_reconf_fromgxb_bus_w),

          rx_seriallpbken     => slv('0'),
          gxb_powerdown       => (others => '0')
        );
      end generate;

      gen_16b : if g_data_w = 16 generate
        u_gx: ip_stratixiv_hssi_gx_16b
        generic map (
          starting_channel_number  => i * 4  -- Starting channel numbers must be 0,4,8,12 etc for individual ALTGX instances
        )  -- Reconfig megawizard: regenerate with 'number of channels' = 4*nof_gx
        port map (
          -- Clocks, in
          pll_inclk           => tr_clk,
          cal_blk_clk         => cal_rec_clk,
          reconfig_clk        => cal_rec_clk,

          -- Clocks, generated (out)
          sl(tx_clkout)       => i_tx_clk(i),
          sl(rx_clkout)       => i_rx_clk(i),

          -- Parallel data I/O
          rx_dataout          => rx_dataout(i),
          tx_datain           => tx_datain(i),

          -- Serial data I/O
          rx_datain           => slv(rx_datain(i)),
          sl(tx_dataout)      => tx_dataout(i),

          -- Initialization I/O
          sl(rx_freqlocked)   => rx_freq_locked(i),
          sl(pll_locked)      => tx_pll_locked(i),
          pll_powerdown       => slv(trc_tx_pll_powerdown),
          rx_digitalreset     => slv(trc_rx_digital_rst),
          rx_analogreset      => slv(trc_rx_analog_rst),
          tx_digitalreset     => slv(trc_tx_digital_rst),

          -- 8b/10b related
          tx_ctrlenable       => txc_tx_ctrlenable(i),  -- 8b10b
          rx_ctrldetect       => rxc_rx_ctrldetect(i),  -- 8b10b

          -- Reconfig block connections
          reconfig_togxb      => reconfig_togxb,
          reconfig_fromgxb    => reconfig_fromgxb(c_reconf_fromgxb_bus_w * (i + 1) - 1 downto c_reconf_fromgxb_bus_w * (i + 1) - c_reconf_fromgxb_bus_w)
        );
      end generate;
    end generate;

    gen_rx : if g_tx = false and g_rx = true generate  -- Generate receivers only

      gen_32b : if g_data_w = 32 generate
        u_rx: ip_stratixiv_hssi_rx_32b_generic
        generic map (
          g_mbps                   => g_mbps,
          starting_channel_number  => i * 4  -- Starting channel numbers must be 0,4,8,12 etc for individual ALTGX instances
        )  -- Reconfig megawizard: regenerate with 'number of channels' = 4*nof_gx
        port map (
          cal_blk_clk         => cal_rec_clk,
          gxb_powerdown       => (others => '0'),
          reconfig_clk        => cal_rec_clk,
          reconfig_togxb      => reconfig_togxb,
          rx_analogreset      => slv(trc_rx_analog_rst),
          rx_datain           => slv(rx_datain(i)),
          rx_digitalreset     => slv(trc_rx_digital_rst),
          reconfig_fromgxb    => reconfig_fromgxb(c_reconf_fromgxb_bus_w * (i + 1) - 1 downto c_reconf_fromgxb_bus_w * (i + 1) - c_reconf_fromgxb_bus_w),
          sl(rx_clkout)       => i_rx_clk(i),
          rx_ctrldetect       => rxc_rx_ctrldetect(i),
          rx_dataout          => rx_sosi_arr(i).data(g_data_w - 1 downto 0),
          sl(rx_freqlocked)   => rx_freq_locked(i),
          rx_enapatternalign  => slv(rxc_rx_align_en(i)),

          --ADDITTIONAL SIGNAL:
          rx_cruclk           => slv(tr_clk)
        );
      end generate;

      gen_16b : if g_data_w = 16 generate
        u_rx: ip_stratixiv_hssi_rx_16b
        generic map (
          starting_channel_number  => i * 4  -- Starting channel numbers must be 0,4,8,12 etc for individual ALTGX instances
        )  -- Reconfig megawizard: regenerate with 'number of channels' = 4*nof_gx
        port map (
          -- Clocks, in
          cal_blk_clk         => cal_rec_clk,
          reconfig_clk        => cal_rec_clk,

          -- Clocks, generated (out)
          sl(rx_clkout)       => i_rx_clk(i),

          -- Parallel data I/O
          rx_dataout          => rx_dataout(i),

          -- Serial data I/O
          rx_datain           => slv(rx_datain(i)),

          -- Initialization I/O
          sl(rx_freqlocked)   => rx_freq_locked(i),
          rx_digitalreset     => slv(trc_rx_digital_rst),
          rx_analogreset      => slv(trc_rx_analog_rst),

          -- 8b/10b related
          rx_ctrldetect       => rxc_rx_ctrldetect(i),  -- 8b10b

          -- Reconfig block connections
          reconfig_togxb      => reconfig_togxb,
          reconfig_fromgxb    => reconfig_fromgxb(c_reconf_fromgxb_bus_w * (i + 1) - 1 downto c_reconf_fromgxb_bus_w * (i + 1) - c_reconf_fromgxb_bus_w),

          --ADDITTIONAL SIGNAL:
          rx_cruclk           => slv(tr_clk)
        );
      end generate;
    end generate;

    gen_tx : if g_tx = true and g_rx = false generate
      gen_32b : if g_data_w = 32 generate
        u_tx: ip_stratixiv_hssi_tx_32b_generic
        generic map (
           g_mbps             => g_mbps
        )
        port map (
          cal_blk_clk         => cal_rec_clk,
          gxb_powerdown       => (others => '0'),
          pll_inclk           => tr_clk,
          pll_powerdown       => slv(trc_tx_pll_powerdown),
          tx_ctrlenable       => txc_tx_ctrlenable(i),
          tx_datain           => tx_datain(i),
          tx_digitalreset     => slv(trc_tx_digital_rst),
          sl(pll_locked)      => tx_pll_locked(i),
          sl(tx_clkout)       => i_tx_clk(i),
          sl(tx_dataout)      => tx_dataout(i)
        );
      end generate;

      gen_16b : if g_data_w = 16 generate
        u_tx: ip_stratixiv_hssi_tx_16b
        port map (
          -- Clocks, in
          pll_inclk           => tr_clk,
          cal_blk_clk         => cal_rec_clk,

          -- Clocks, generated (out)
          sl(tx_clkout)       => i_tx_clk(i),

          -- Parallel data I/O
          tx_datain           => tx_datain(i),

          -- Serial data I/O
          sl(tx_dataout)      => tx_dataout(i),

          -- Initialization I/O
          sl(pll_locked)      => tx_pll_locked(i),
          pll_powerdown       => slv(trc_tx_pll_powerdown),
          tx_digitalreset     => slv(trc_tx_digital_rst),

          -- 8b/10b related
          tx_ctrlenable       => txc_tx_ctrlenable(i)  -- 8b10b
        );
      end generate;
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- Reset and alignment
  ------------------------------------------------------------------------------

  u_areset_tr_rst : entity common_lib.common_areset
  generic map(
    g_rst_level => '1'
  )
  port map(
    clk    => tr_clk,
    in_rst => '0',
    out_rst => tr_rst
  );

  gen_tx : if g_tx = true generate
    tx_clk <= i_tx_clk;
    tx_rst <= i_tx_rst;

    not_trc_tx_rdy <= not(trc_tx_rdy);

    u_tech_transceiver_tx_rst : entity work.tech_transceiver_tx_rst
    generic map(
      g_nof_gx => g_nof_gx
    )
    port map (
      tr_clk               => tr_clk,
      tr_rst               => tr_rst,
      trc_tx_pll_powerdown => trc_tx_pll_powerdown,
      tx_pll_locked        => tx_pll_locked,
      trc_tx_digital_rst   => trc_tx_digital_rst,
      trc_tx_rdy           => trc_tx_rdy
    );

    gen_altgx: for i in 0 to g_nof_gx - 1 generate
      u_areset_tx_rst : entity common_lib.common_areset
      generic map(
        g_rst_level => '1'
      )
      port map(
        clk     => i_tx_clk(i),
        in_rst  => not_trc_tx_rdy,
        out_rst => i_tx_rst(i)
      );

      -- ALTGX in 16-bit mode supports and uses internal auto alignment state machine.
      gen_tx_align: if g_data_w = 32 generate
        u_tech_transceiver_tx_align : entity work.tech_transceiver_tx_align
        generic map(
          g_sim    => g_sim,
          g_data_w => g_data_w,
          g_mbps   => g_mbps
        )
        port map (
          tx_clk             => i_tx_clk(i),
          tx_rst             => i_tx_rst(i),
          tx_align_en_in     => tx_align_en(i),
          tx_align_en_out    => tx_align_en_out(i),
          tx_state           => tx_state(2 * i + 1 downto 2 * i)
        );
      end generate;

      -- TX is ready when it's not sending the alignment pattern
      tx_siso_arr(i).ready     <= not tx_align_en_out(i);
      tx_siso_arr(i).xon       <= not tx_align_en_out(i);
      tx_datain(i)             <= c_align_pattern when tx_align_en_out(i) = '1' else tx_sosi_arr(i).data(g_data_w - 1 downto 0) when  tx_sosi_arr(i).valid = '1' else c_inval_pattern;
      txc_tx_ctrlenable(i)     <= (others => '1') when tx_align_en_out(i) = '1' else (others => '0') when tx_sosi_arr(i).valid = '1' else (others => '1');
    end generate;
  end generate;

  gen_rx : if g_rx = true generate
    rx_clk <= i_rx_clk;
    rx_rst <= i_rx_rst;

    u_tech_transceiver_rx_rst : entity work.tech_transceiver_rx_rst
    generic map(
      g_nof_gx => g_nof_gx
    )
    port map (
      tr_clk             => tr_clk,
      tr_rst             => tr_rst,
      rec_busy           => rec_busy,
      rx_freq_locked     => rx_freq_locked,
      trc_rx_analog_rst  => trc_rx_analog_rst,
      trc_rx_digital_rst => trc_rx_digital_rst,
      trc_rx_rdy         => trc_rx_rdy
    );

    not_trc_rx_rdy <= not(trc_rx_rdy);

    i_rx_rst_align: for i in 0 to g_nof_gx - 1 generate
      u_areset_i_rx_rst : entity common_lib.common_areset
      generic map(
        g_rst_level => '1'
      )
      port map(
        clk     => i_rx_clk(i),
        in_rst  => not_trc_rx_rdy,
        out_rst => i_rx_rst(i)
      );

      -- ALTGX in 16-bit mode supports and uses internal auto alignment state machine.
      gen_rx_align: if g_data_w = 32 generate
        u_tech_transceiver_rx_align : entity work.tech_transceiver_rx_align
        generic map(
          g_sim    => g_sim,
          g_data_w => g_data_w,
          g_mbps   => g_mbps
        )
        port map (
          rx_clk             => i_rx_clk(i),
          rx_rst             => i_rx_rst(i),
          rx_align_en_in     => rx_align_en(i),
          rx_align_en_out    => rxc_rx_align_en(i),
          rxc_rx_aligned     => rxc_rx_aligned(i),
          rx_state           => rx_state(2 * i + 1 downto 2 * i)
        );

        -- The transmitters will represent gaps as the control-encoded c_inval pattern
        rx_sosi_arr(i).valid <= not(orv(rxc_rx_ctrldetect(i))) when rxc_rx_aligned(i) = '1' else '0';
      end generate;

      gen_rx_order: if g_data_w = 16 generate
        u_tech_transceiver_rx_align : entity work.tech_transceiver_rx_order
        generic map(
          g_data_w    => g_data_w
        )
        port map (
          rx_clk       => i_rx_clk(i),
          rx_rst       => i_rx_rst(i),

          rx_data_in   => rx_dataout(i),
          rx_ctrl_in   => rxc_rx_ctrldetect(i),

          rx_data_out  => rx_sosi_arr(i).data(g_data_w - 1 downto 0),
          rx_valid_out => rx_sosi_arr(i).valid
        );
      end generate;
    end generate;
  end generate;

  ------------------------------------------------------------------------------
  -- ALTGX_RECONFIG Megafunction
  ------------------------------------------------------------------------------

  u_gxb_reconfig : ip_stratixiv_gxb_reconfig_v91  -- Create one gxb_reconfig module for all ALTGX instances
  generic map (
    g_nof_gx        => g_nof_gx,
    g_fromgxb_bus_w => c_reconf_fromgxb_bus_w,  -- = 17
    g_togxb_bus_w   => c_reconf_togxb_bus_w  -- = 4
  )
  port map (
    reconfig_clk      => cal_rec_clk,
    reconfig_fromgxb  => reconfig_fromgxb,
    busy              => rec_busy,
    reconfig_togxb    => reconfig_togxb
  );
end str;
