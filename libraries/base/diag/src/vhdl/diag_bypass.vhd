--------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity diag_bypass is
  generic (
    g_in_dat_w        : natural := 12;
    g_out_dat_w       : natural := 12;
    g_mod_in_dat_w    : natural := 12;
    g_mod_out_dat_w   : natural := 12
  );
  port (
    clk               : in  std_logic;
    rst               : in  std_logic;
    bypass_en         : in  std_logic;
    in_dat_x          : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_dat_y          : in  std_logic_vector(g_in_dat_w - 1 downto 0);
    in_val            : in  std_logic;
    in_sync           : in  std_logic;
    out_dat_x         : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_dat_y         : out std_logic_vector(g_out_dat_w - 1 downto 0);
    out_val           : out std_logic;
    out_sync          : out std_logic;
    mod_clk           : out std_logic;
    mod_in_dat_x      : out std_logic_vector(g_mod_in_dat_w - 1 downto 0);
    mod_in_dat_y      : out std_logic_vector(g_mod_in_dat_w - 1 downto 0);
    mod_in_val        : out std_logic;
    mod_in_sync       : out std_logic;
    mod_out_dat_x     : in  std_logic_vector(g_mod_out_dat_w - 1 downto 0);
    mod_out_dat_y     : in  std_logic_vector(g_mod_out_dat_w - 1 downto 0);
    mod_out_val       : in  std_logic;
    mod_out_sync      : in  std_logic
  );
end diag_bypass;

architecture rtl of diag_bypass is
  signal nxt_out_dat_x   : std_logic_vector(out_dat_x'range);
  signal nxt_out_dat_y   : std_logic_vector(out_dat_y'range);
  signal nxt_out_val     : std_logic;
  signal nxt_out_sync    : std_logic;
  signal bypass_en_reg   : std_logic;
begin
  mod_in_dat_x <= RESIZE_SVEC(in_dat_x, mod_in_dat_x'length);
  mod_in_dat_y <= RESIZE_SVEC(in_dat_y, mod_in_dat_y'length);
  mod_in_val  <= in_val;
  mod_in_sync <= in_sync;

  registers : process (clk, rst)
  begin
    if rst = '1' then
      out_dat_x   <= (others => '0');
      out_dat_y   <= (others => '0');
      out_val     <= '0';
      out_sync    <= '0';
    elsif rising_edge(clk) then
      out_dat_x   <= nxt_out_dat_x;
      out_dat_y   <= nxt_out_dat_y;
      out_val     <= nxt_out_val;
      out_sync    <= nxt_out_sync;
    end if;
  end process;

  output_switch_proc : process (bypass_en, mod_out_dat_x, mod_out_val, mod_out_sync,
      in_dat_x, in_val, in_sync, mod_out_dat_y, in_dat_y)
  begin
    nxt_out_dat_x <= RESIZE_SVEC(mod_out_dat_x, out_dat_x'length);
    nxt_out_dat_y <= RESIZE_SVEC(mod_out_dat_y, out_dat_y'length);
    nxt_out_val  <= mod_out_val;
    nxt_out_sync <= mod_out_sync;

    if bypass_en = '1' then
      nxt_out_dat_x <= RESIZE_SVEC(in_dat_x, out_dat_x'length);
      nxt_out_dat_y <= RESIZE_SVEC(in_dat_y, out_dat_y'length);
      nxt_out_val <= in_val;
      nxt_out_sync <= in_sync;
    end if;
  end process;

  clock_gating_proc : process (bypass_en, clk)
  begin
    mod_clk <= clk;
    -- synthesis translate_off
    if falling_edge(clk) then
      bypass_en_reg <= bypass_en;
    end if;
    if bypass_en_reg = '1' then
      mod_clk <= '0';
    end if;
    -- synthesis translate_on
  end process;
end rtl;
