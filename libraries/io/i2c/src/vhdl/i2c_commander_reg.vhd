-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for I2C commander
-- Description: See i2c-commander.vhd
-- Remark:
-- . Warning "Case choice must be a locally static expression" due to using
--   operations on constants in CASE statement, can be avoid if necessary by
--   using elsif

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.i2c_pkg.all;
use work.i2c_commander_pkg.all;

entity i2c_commander_reg is
  generic (
    g_i2c_cmdr  : t_c_i2c_cmdr_commander;
    g_i2c_mm    : t_c_i2c_mm := c_i2c_mm
  );
  port (
    -- Clocks and reset
    rst                      : in  std_logic;  -- reset synchronous with clk
    clk                      : in  std_logic;  -- memory-mapped bus clock

    -- Memory Mapped Slave
    sla_in                   : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out                  : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers
    protocol_wr              : out std_logic;
    protocol_index           : out natural range 0 to g_i2c_cmdr.nof_protocols - 1;
    protocol_offset_arr      : out t_natural_arr(0 to g_i2c_cmdr.nof_protocols - 1);
    result_expected_arr      : out t_slv_32_arr(0 to g_i2c_cmdr.nof_protocols - 1);
    protocol_status_rd       : out std_logic;
    protocol_status          : in  std_logic_vector(c_word_w - 1 downto 0);
    result_error_cnt         : in  std_logic_vector(c_word_w - 1 downto 0);
    result_data_arr          : in  t_slv_32_arr(0 to g_i2c_cmdr.nof_result_data_max - 1)
  );
end i2c_commander_reg;

architecture rtl of i2c_commander_reg is
  -- Use shorter aliases for some generic constants
  constant c_nof_protocols       : natural := g_i2c_cmdr.nof_protocols;  -- must be <= c_i2c_max_nof_protocols to fit the t_c_i2c_cmdr_commander record
  constant c_nof_result_data_max : natural := g_i2c_cmdr.nof_result_data_max;

  constant c_protocol_adr_w      : natural := g_i2c_mm.protocol_adr_w;

  -- Example MM register map defined for:
  -- . c_nof_protocols   = 2
  -- . c_nof_result_data_max = 3
  --
  --    0 = protocol 0
  --    1 = protocol 1
  --    2 = protocol offset 0
  --    3 = protocol offset 1
  --    4 = result_expected 0
  --    5 = result_expected 1
  --    6 = protocol status
  --    7 = result_error_cnt
  --    8 = result_data byte 0
  --    9 = result_data byte 1
  --   10 = result_data byte 2
  --
  -- so c_mm_reg_nof_dat = 3*2 + 1 + 1 + 3 = 11

  constant c_mm_reg_nof_dat     : natural := func_i2c_cmdr_mm_reg_nof_dat(g_i2c_cmdr);
  constant c_mm_reg_w           : natural := ceil_log2(c_mm_reg_nof_dat);

  constant c_mm_reg             : t_c_mem := (latency  => 1,
                                              adr_w    => c_mm_reg_w,
                                              dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                              nof_dat  => c_mm_reg_nof_dat,
                                              init_sl  => '0');

  -- Commander registers
  signal i_protocol_offset_arr  : t_natural_arr(0 to c_nof_protocols - 1);
  signal i_result_expected_arr  : t_slv_32_arr(0 to c_nof_protocols - 1);
begin
  protocol_offset_arr <= i_protocol_offset_arr;
  result_expected_arr <= i_result_expected_arr;

  ------------------------------------------------------------------------------
  -- MM register access in the MM clk domain
  -- . Hardcode the shared MM slave register directly in RTL instead of using
  --   the common_reg_r_w instance. Directly using RTL is easier when the large
  --   MM register has multiple different fields and with different read and
  --   write options per field in one MM register.
  ------------------------------------------------------------------------------

  p_mm_reg : process (rst, clk)
    variable v_address           : natural := 0;
    variable v_index_protocol    : natural := 0;
    variable v_index_offset      : integer := 0;
    variable v_index_expected    : integer := 0;
    variable v_index_result_data : integer := 0;
  begin
    if rst = '1' then

      -- Read access
      sla_out <= c_mem_miso_rst;

      -- Access event, register values
      protocol_wr <= '0';

      i_protocol_offset_arr <= g_i2c_cmdr.protocol_offset_arr(0 to c_nof_protocols - 1);
      i_result_expected_arr <= g_i2c_cmdr.result_expected_arr(0 to c_nof_protocols - 1);

    elsif rising_edge(clk) then
      -- Init auxiliary semi-constant variables (do it here within rising_edge section to avoid having to put sla_in in sensitivity list)
      v_address           := TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0));
      v_index_protocol    := v_address;
      v_index_offset      := v_address -   c_nof_protocols;
      v_index_expected    := v_address - 2 * c_nof_protocols;
      v_index_result_data := v_address - 3 * c_nof_protocols - 2;

      -- Read access defaults
      sla_out.rdval <= '0';

      -- Access event defaults
      protocol_wr <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case v_address is

          -- Write commander access indicates the new protocol event, ignore the wrdata
          when 0 to c_nof_protocols - 1 =>
            protocol_wr <= '1';
            protocol_index <= v_index_protocol;

          -- Write offset address per commander protocol
          when c_nof_protocols to 2 * c_nof_protocols - 1 => i_protocol_offset_arr(v_index_offset) <= TO_UINT(sla_in.wrdata(c_protocol_adr_w - 1 downto 0));

          -- Write expected periodic result pattern per commander protocol
          when 2 * c_nof_protocols to 3 * c_nof_protocols - 1 => i_result_expected_arr(v_index_expected) <= sla_in.wrdata(31 downto 0);

          -- The protocol status is read only
          -- The nof result bytes count is read only
          -- The result data bytes are read only
          -- Not used MM addresses
          when others => null;
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case v_address is

          -- Read offset address per commander protocol
          when c_nof_protocols to 2 * c_nof_protocols - 1 => sla_out.rddata(31 downto 0) <= TO_UVEC(i_protocol_offset_arr(v_index_offset), c_word_w);

          -- Read expected periodic result pattern per commander protocol
          when 2 * c_nof_protocols to 3 * c_nof_protocols - 1 => sla_out.rddata(31 downto 0) <= i_result_expected_arr(v_index_expected);

          -- Read the commander status
          when 3 * c_nof_protocols => sla_out.rddata(31 downto 0) <= protocol_status;

          -- Read the nof result errors count
          -- The nof result errors count is 0 when OK, else it shows how often the result byte differed from 0 as indicated by '0' in the expected result pattern
          when 3 * c_nof_protocols + 1 => sla_out.rddata(31 downto 0) <= result_error_cnt;

          -- The result data has 1 byte per address and is read only, but do support overwriting the register
          -- The result data are the result bytes as indicated by '1' in the expected result pattern
          when 3 * c_nof_protocols + 2 to 3 * c_nof_protocols + 2 + c_nof_result_data_max - 1 => sla_out.rddata(31 downto 0) <= RESIZE_UVEC(result_data_arr(v_index_result_data), c_word_w);

          -- The commander triggers are write access only
          -- Not used MM addresses
          when others => null;
        end case;
      end if;
    end if;
  end process;

  -- Detect read commander status event (has to be combinatorial to avoid missing the c_i2c_state_done state, because the state depends on the this pulse)
  protocol_status_rd <= '1' when sla_in.rd = '1' and TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) = 3 * c_nof_protocols else '0';
end rtl;
