--Legal Notice: (C)2019 Altera Corporation. All rights reserved.  Your
--use of Altera Corporation's design tools, logic functions and other
--software and tools, and its AMPP partner logic functions, and any
--output files any of the foregoing (including device programming or
--simulation files), and any associated documentation or information are
--expressly subject to the terms and conditions of the Altera Program
--License Subscription Agreement or other applicable license agreement,
--including, without limitation, that your use is for the sole purpose
--of programming logic devices manufactured by Altera and sold by Altera
--or its authorized distributors.  Please refer to the applicable
--agreement for further details.

-- turn off superfluous VHDL processor warnings
-- altera message_level Level1
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 13469 16735 16788

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity qsys_unb2b_minimal_onchip_memory2_0_altera_avalon_onchip_memory2_180_lo46q2y is
        generic (
                 INIT_FILE : string := "onchip_memory2_0.hex"
                 );
        port (
              -- inputs:
                 signal address : in std_logic_vector(14 downto 0);
                 signal byteenable : in std_logic_vector(3 downto 0);
                 signal chipselect : in std_logic;
                 signal clk : in std_logic;
                 signal clken : in std_logic;
                 signal freeze : in std_logic;
                 signal reset : in std_logic;
                 signal reset_req : in std_logic;
                 signal write : in std_logic;
                 signal writedata : in std_logic_vector(31 downto 0);

              -- outputs:
                 signal readdata : out std_logic_vector(31 downto 0)
              );
end entity qsys_unb2b_minimal_onchip_memory2_0_altera_avalon_onchip_memory2_180_lo46q2y;

architecture europa of qsys_unb2b_minimal_onchip_memory2_0_altera_avalon_onchip_memory2_180_lo46q2y is
  component altsyncram is
generic (
      byte_size : natural;
        init_file : string;
        lpm_type : string;
        maximum_depth : natural;
        numwords_a : natural;
        operation_mode : string;
        outdata_reg_a : string;
        ram_block_type : string;
        read_during_write_mode_mixed_ports : string;
        read_during_write_mode_port_a : string;
        width_a : natural;
        width_byteena_a : natural;
        widthad_a : natural
      );
    port (
    signal q_a : out std_logic_vector(31 downto 0);
        signal wren_a : in std_logic;
        signal byteena_a : in std_logic_vector(3 downto 0);
        signal clock0 : in std_logic;
        signal address_a : in std_logic_vector(14 downto 0);
        signal clocken0 : in std_logic;
        signal data_a : in std_logic_vector(31 downto 0)
      );
  end component altsyncram;
                signal clocken0 :  std_logic;
                signal internal_readdata :  std_logic_vector(31 downto 0);
                signal wren :  std_logic;
begin
  wren <= chipselect and write;
  clocken0 <= clken and not reset_req;
  the_altsyncram : altsyncram
    generic map(
      byte_size => 8,
      init_file => INIT_FILE,
      lpm_type => "altsyncram",
      maximum_depth => 32768,
      numwords_a => 32768,
      operation_mode => "SINGLE_PORT",
      outdata_reg_a => "UNREGISTERED",
      ram_block_type => "AUTO",
      read_during_write_mode_mixed_ports => "DONT_CARE",
      read_during_write_mode_port_a => "DONT_CARE",
      width_a => 32,
      width_byteena_a => 4,
      widthad_a => 15
    )
    port map(
            address_a => address,
            byteena_a => byteenable,
            clock0 => clk,
            clocken0 => clocken0,
            data_a => writedata,
            q_a => internal_readdata,
            wren_a => wren
    );

  --s1, which is an e_avalon_slave
  --s2, which is an e_avalon_slave
  --vhdl renameroo for output signals
  readdata <= internal_readdata;
end europa;
