--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:
-- Description:

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_barrel_shift is
  generic (
    g_nof_streams : natural;
    g_data_w      : natural
  );
  port (
    rst         : in  std_logic;
    clk         : in  std_logic;

    snk_in_arr  : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    shift_in    : in std_logic_vector(ceil_log2(g_nof_streams) - 1 downto 0);

    src_out_arr : out t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    shift_out   : in std_logic_vector(ceil_log2(g_nof_streams) - 1 downto 0)
  );
end dp_barrel_shift;

architecture str of dp_barrel_shift is
  type t_reg is record
    src_out_arr : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
    shift_out   : std_logic_vector(ceil_log2(g_nof_streams) - 1 downto 0);
  end record;

  constant c_reg_defaults : t_reg := ( (others => c_dp_sosi_rst),
                                       (others => '0') );

  signal r, nxt_r : t_reg := c_reg_defaults;
begin
  r <= nxt_r when rising_edge(clk);

  p_comb : process(rst, r, snk_in_arr, shift_in)
    variable v : t_reg;
  begin
    v := r;

    for i in 0 to g_nof_streams - 1 loop
      v.src_out_arr(i + TO_UINT(shift_in)) := snk_in_arr(i);
    end loop;

    if rst = '1' then
      v := c_reg_defaults;
    end if;

    nxt_r <= v;
  end process;
end str;
