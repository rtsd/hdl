#!/usr/bin/env python
###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""Calculates the integrated Stokes parameters and adds multiple streams for 
   comparison with the test sequences in tb_iab_iquv.vhd                                                 
   
   Usage:
   Edit the source code to set nint=8 or 16, and nof_streams=12 or otherwise
   > ./iquv_iab_checker.py
   
"""
import common


nof_streams = 12
nint = 8                               # number of samples to integrate  (typ. 8 or 16)
nints_per_block = 4

c_in_data_w = 12                       # number of bits at the IQUV input
c_iquv_out_w = 18                      # number of bits at the IQUV output
c_accum_w = c_in_data_w*2 + 1 + common.ceil_log2(nint) # number of bits to represent the accumulated result in IQUV
c_lsb_w = c_accum_w - c_iquv_out_w       # number of LSBs to round off the accumulated result

npoints = nint * nints_per_block       # number of samples in a block (typ. 32 or 64)
xpol = []
ypol = []


print ( 'c_accum_w = ', c_accum_w)
print ( 'c_lsb_w = ', c_lsb_w)

# Set the X and Y complex input values as per the test bench
for index in range(npoints):
    xpol.append(10*index + 1j * (50 - 10*index))
    ypol.append(-20*index + 1j * (1000-20*index))
 
# Set the X and Y complex input valus to test the full scale 
for index in range(npoints):
    xpol.append(2047 + 1j * 2047)
    ypol.append(-2047 + 1j * -2047)
 

# calculate the intermediate IQUV outputs
for blockno in range(2):
    print ('Block cycle ', blockno)
    for ii in range(nints_per_block):
        accumI = 0
        accumQ = 0
        accumU = 0
        accumV = 0
        for jj in range(nint):
            index = blockno*npoints + ii*nint + jj
            Ival = xpol[index] * xpol[index].conjugate() + ypol[index] * ypol[index].conjugate()
            Qval = xpol[index] * xpol[index].conjugate() - ypol[index] * ypol[index].conjugate()
            Uval = xpol[index] * ypol[index].conjugate() + ypol[index] * xpol[index].conjugate()
            Vval = ypol[index] * xpol[index].conjugate() - xpol[index] * ypol[index].conjugate()
            accumI = accumI + Ival
            accumQ = accumQ + Qval
            accumU = accumU + Uval
            accumV = accumV + Vval
        #print ( 'Integrated results Iint = ',accumI,', Qint = ',accumQ,', Uint = ',accumU,', Vint = ',accumV)

        # Rescale the outputs as per the IQUV requantizer stage
        # Note that I is always positve
        accumI_rescaled = 2*common.int_requantize(int(accumI.real), c_accum_w, c_iquv_out_w, c_lsb_w, True, True, 0) 
        accumQ_rescaled = common.int_requantize(int(accumQ.real), c_accum_w, c_iquv_out_w, c_lsb_w, True, True, 0) 
        accumU_rescaled = common.int_requantize(int(accumU.real), c_accum_w, c_iquv_out_w, c_lsb_w, True, True, 0) 
        accumV_rescaled = common.int_requantize(int(accumV.imag), c_accum_w, c_iquv_out_w, c_lsb_w, True, True, 0) 
        #print ( 'Iaccum to int = ',int(accumI.real)*2)
        #print ( 'Rescaled Iint = ',accumI_rescaled,', Qint = ',accumQ_rescaled,', Uint = ',accumU_rescaled,', Vint = ',accumV_rescaled)
        

        # Sum all the channels
        # Since they're all the same we can just multiply by nof_streams

        sumI = nof_streams * accumI_rescaled
        sumQ = nof_streams * accumQ_rescaled
        sumU = nof_streams * accumU_rescaled
        sumV = nof_streams * accumV_rescaled

        print ( 'Sum of all input channels for nint = ', nint)
        print ( 'I =  ',sumI,', Q = ',sumQ,', U = ',sumU,', V = ',sumV)

