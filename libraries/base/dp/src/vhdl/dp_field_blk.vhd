-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use work.dp_stream_pkg.all;

-- Purpose:
-- . Source a DP data block of which the contents are read from snk_in.data when snk_in.valid = '1'
-- Description:
-- . The fields in snk_in.data are determined by g_field_arr;
-- . Each field can be overridden via MM when when its corresponding MM override bit is '1',
--   if override = '0' the field is taken from snk_in.data.
-- . The initial (default) values of the override bits are passed via g_field_sel(one bit per field);
-- . Both the SLV as the override (ovr) fields can be read back via MM;
-- . g_mode can be used to select in which mode dp_field_blk should work. Most of the time, the
--   default g_mode = 0 can be used. When using g_mode = 0, it is assumed that dp_field_blk must be
--   used in source mode when g_snk_data_w > g_src_data_w, else in sink mode. This is a typical
--   application as it is often the case that the total width of the field block is larger than the
--   serial data width as shown in the example below. There are cases where the width of the
--   field-block is smaller than the serial data words. Therefore it is possible to force either sink
--   or source mode.
--   . g_mode = 0: auto select mode based on g_snk_data_w and g_src_data_w.
--   . g_mode = 1: use dp_field_blk in source mode regardless of g_*_data_w.
--   . g_mode = 2: use dp_field_blk in sink mode regardless of g_*_data_w.

-- The diagrams below show dp_field_blk in source and sink configuration. For simplicity, the 5
-- fields are defined as follows (fields do not need to respect word boundaries):
-- . field_0 = 32b
-- . field_1 = 96b
-- . field_2 = 32b
--   --------------+
-- . Total   =160b (*1 cycle)
--
-- ============================
-- dp_field_blk in source mode:
-- ============================
--
--     snk_in.data(160b)        _
--        |f0|            | \
--        |f1| =========> |  \
--        |f2|            |   \
--                        |    \     field_slv(160b)     _________                                           __________
--                        |     |          |f0|         |dp_repack|     dp_repack_out_dat(5 cycles * 32b)   |dp_fifo_sc|
--                        |     | =======> |f1| ======> |         | =========> [f0,f1,f1,f1,f2] ==========> |          | ==> src_out
--                        |     |          |f2|         |_________|                                         |__________|
-- mm_fields_slv_out(160b)|    /
--        |f0|            |   /
--        |f1| =========> |  /|
--        |f2|            |_/||
--                           ||
--                           ||
--                           field_override_arr(3b)
--
-- ==========================
-- dp_field_blk in sink mode:
-- ==========================
--                                                _________ mm_fields_slv_in(160b)
--                snk_in.data(5 cycles * 32b)    |dp_repack|        |f0|
-- snk_in =========> [f0,f1,f1,f1,f2] =========> |         | =====> |f1| ========> slv_out
--                                               |_________|        |f2|
--

entity dp_field_blk is
  generic (
    g_field_arr      : t_common_field_arr;
    g_field_sel      : std_logic_vector;
    g_snk_data_w     : natural;
    g_src_data_w     : natural;
    g_in_symbol_w    : natural := 1;
    g_out_symbol_w   : natural := 1;
    g_pipeline_ready : boolean := false;
    g_mode           : natural := 0  -- 0 = auto, 1 = source , 2 = sink
  );
  port (
    dp_rst          : in  std_logic;
    dp_clk          : in  std_logic;

    mm_rst          : in  std_logic;
    mm_clk          : in  std_logic;

    snk_in          : in  t_dp_sosi := c_dp_sosi_rst;
    snk_out         : out t_dp_siso;

    src_in          : in  t_dp_siso := c_dp_siso_rdy;
    src_out         : out t_dp_sosi;

    -- Source mode
    -- . Single cycle SLV input
--    slv_in          : IN  STD_LOGIC_VECTOR(g_snk_data_w-1 DOWNTO 0) := (OTHERS=> '0'); -- Input for the RW fields defined in g_field_arr
--    slv_in_val      : IN  STD_LOGIC := '0';
    -- . Multi cycle block output

    -- Sink mode
    -- . Multi cycle block input
    -- . Single cycle SLV output
--    slv_out         : OUT STD_LOGIC_VECTOR(g_src_data_w-1 DOWNTO 0); -- Output for the RO fields defined in g_field_arr
--    slv_out_val     : OUT STD_LOGIC;

    reg_slv_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_slv_miso    : out t_mem_miso

--    reg_ovr_mosi    : IN  t_mem_mosi := c_mem_mosi_rst;
--    reg_ovr_miso    : OUT t_mem_miso
  );
end dp_field_blk;

architecture str of dp_field_blk is
--  CONSTANT c_ovr_field_arr : t_common_field_arr(g_field_arr'RANGE) := field_ovr_arr(g_field_arr, g_field_sel);

  -- Mode: fields to data block (c_field_to_block=True) or data block to fields (c_field_to_block=False)
  --  a.k.a. wire to narrow                             or narrow     to wide
  constant c_field_to_block : boolean := sel_a_b(g_mode = 0, g_snk_data_w > g_src_data_w, sel_a_b(g_mode = 1, true, false));

  -- Field to block: mm_fields only has SLV output, no input. Set input wirdt to zero.
  constant c_mm_fields_slv_in_w  : natural := sel_a_b(c_field_to_block, 0, g_src_data_w);
  constant c_mm_fields_slv_out_w : natural := sel_a_b(c_field_to_block, g_snk_data_w, 0);

  signal mm_fields_slv_in     : std_logic_vector(c_mm_fields_slv_in_w - 1 downto 0);
  signal mm_fields_slv_in_val : std_logic := '0';  -- default '0' when c_field_to_block = TRUE
  signal mm_fields_slv_out    : std_logic_vector(c_mm_fields_slv_out_w - 1 downto 0);

  signal field_override_arr : std_logic_vector(g_field_arr'range) := g_field_sel;  -- 1 override bit per field

  signal dp_repack_data_snk_in  : t_dp_sosi := c_dp_sosi_rst;
  signal dp_repack_data_snk_out : t_dp_siso;

  signal dp_repack_data_src_out  : t_dp_sosi;
  signal dp_repack_data_src_in   : t_dp_siso;
begin
  ---------------------------------------------------------------------------------------
  -- dp_repack_data in source mode: single-cycle snk_in.data to multi-cycle DP data block
  ---------------------------------------------------------------------------------------
  gen_dp_repack_in_snk_in_data: if c_field_to_block = true generate
    dp_repack_data_snk_in.valid <= snk_in.valid;
    dp_repack_data_snk_in.sop   <= snk_in.valid;
    dp_repack_data_snk_in.eop   <= snk_in.valid;
    dp_repack_data_snk_in.sync  <= snk_in.sync;
    dp_repack_data_snk_in.bsn   <= snk_in.bsn;
    -- MM override bits determine source for each field
    gen_field_wires: for i in g_field_arr'range generate
      dp_repack_data_snk_in.data(field_hi(g_field_arr, i) downto field_lo(g_field_arr, i)) <= mm_fields_slv_out(field_hi(g_field_arr, i) downto field_lo(g_field_arr, i))
                                                                                              when field_override_arr(i) = '1' else
                                                                                              snk_in.data(field_hi(g_field_arr, i) downto field_lo(g_field_arr, i));
    end generate;

    src_out <= dp_repack_data_src_out;
    dp_repack_data_src_in <= src_in;
  end generate;

  ---------------------------------------------------------------------------------------
  -- dp_repack_data in sink mode: multi-cycle DP data block to single-cycle SLV
  ---------------------------------------------------------------------------------------
  gen_dp_repack_in_snk_in: if c_field_to_block = false generate
    dp_repack_data_snk_in <= snk_in;
    dp_repack_data_src_in <= src_in;

    -- Entity output
    src_out <= dp_repack_data_src_out;

    -- MM readout of fields
    mm_fields_slv_in     <= dp_repack_data_src_out.data(g_src_data_w - 1 downto 0);
    mm_fields_slv_in_val <= dp_repack_data_src_out.valid;
  end generate;

  ---------------------------------------------------------------------------------------
  -- dp_repack_data provides base functionality
  ---------------------------------------------------------------------------------------
  snk_out <= dp_repack_data_snk_out;

  u_dp_repack_data : entity work.dp_repack_data
  generic map (
    g_in_dat_w       => g_snk_data_w,
    g_in_nof_words   => ceil_div(g_src_data_w, g_snk_data_w),
    g_out_dat_w      => g_src_data_w,
    g_out_nof_words  => ceil_div(g_snk_data_w, g_src_data_w),
    g_in_symbol_w    => g_in_symbol_w,
    g_out_symbol_w   => g_out_symbol_w,
    g_pipeline_ready => g_pipeline_ready
  )
  port map (
    rst            => dp_rst,
    clk            => dp_clk,

    snk_out        => dp_repack_data_snk_out,
    snk_in         => dp_repack_data_snk_in,

    src_in         => dp_repack_data_src_in,
    src_out        => dp_repack_data_src_out
  );

  ---------------------------------------------------------------------------------------
  -- mm_fields for MM access to each field
  ---------------------------------------------------------------------------------------
  u_mm_fields_slv: entity mm_lib.mm_fields
  generic map(
    g_field_arr => g_field_arr
  )
  port map (
    mm_clk     => mm_clk,
    mm_rst     => mm_rst,

    mm_mosi    => reg_slv_mosi,
    mm_miso    => reg_slv_miso,

    slv_clk    => dp_clk,
    slv_rst    => dp_rst,

    slv_in     => mm_fields_slv_in,
    slv_in_val => mm_fields_slv_in_val,
    slv_out    => mm_fields_slv_out
  );

  ---------------------------------------------------------------------------------------
  -- mm_fields to set override bits
--  ---------------------------------------------------------------------------------------
--  u_mm_fields_ovr: ENTITY mm_lib.mm_fields
--  GENERIC MAP(
--    g_field_arr => field_arr_set_mode(c_ovr_field_arr, "RW") -- override fields are always RW
--  )
--  PORT MAP (
--    mm_clk  => mm_clk,
--    mm_rst  => mm_rst,
--
--    mm_mosi => reg_ovr_mosi,
--    mm_miso => reg_ovr_miso,
--
--    slv_clk => dp_clk,
--    slv_rst => dp_rst,
--
--    slv_out => field_override_arr
--  );
end str;
