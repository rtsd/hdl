// Copyright (C) Altera Corporation. All rights reserved. 
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 11.1sp2 linux32 Build 259 01/26/2012
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6c"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
NTatKXD2vaAi56tZ5DimfYyEBW+JNwsK30ioUF5+LXwIzabb/re9ghIliAm41Kbj
zbloRVszlY3nFtz1WHPT9jPMU0pItMwKUzNVmknZX54s0QnNft+yXWHGFMDhCqww
Ebf5ievExkme2joWSqIoSRCeN2RIunNQaCIQ1jdUM7A=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1888)
GmriwwnyuxUO9Kb8dsFAFx3Mz0qotDSw4V3K01NFzzSitHuWzcwuxsMMadAiof4g
akxItPGw8Tmf5jyDK0zXYmaiwyTJvQoH2rCqPQiNlZ7HG0i+al20vzwwVLZImwkG
N21DOlLLXlByhR0M2iDFYwcRB1JKiuiVkrGKw9YV399rOqogoO7OkU8aMX1jSqjb
phd6Josn0Ufp6sQlC8mhDNCCHmgH1b8KZcxoS4lhNblVkhqgzT+k6EAzGP2RQ9E4
P7SSifAERpCSlxLMLm45wygRLfrwzaueVAMfAUYnn7pdRXg7d7RE83APEMdgMCxC
ll2j/YVIYpS/oumYnXw5hmChfMD3mQo/uKlau5mFfzlXx5O3d0TdJjjwmOH6jnTG
7kLDGSVl1rQCT+GZuLtGbRxT6JTEWsJo1qvnFat0zzqCUvAvoorcnmDDoX6j5n9N
onDpAnoPc9vpafBhgtne1b13mr3tCTDgqOyUaCAO0KWtinCprjhn1JTUlf9jdqxW
J5pm0JhafMDPItj0+3+wEfBazJnZrisfFebTnBwW7D0KMH9acVXnkDCTPT3vIDLi
6z2AXbyfsvV42khqHzT7RCbIJ5hwqYGPanrg+SPUnLzArhaIYDs4jaxiSkzENxPU
6wY8qyJBQ+Btc1xdwiUFUCHD5E/ffXhufnAWv72ZeTjMufyOJAU+vncERpbbx9iA
Sam5UzJ9ZQ/NeatEkQHJ0KL23M3HX1INJQb7CzXXkfqG5kL7cNjmoAdjNwbApK7p
+3PfFFyY/TzztVThLRICywKk3LqrjWU+3sgb72apvw4HSWof8+FL/ANGAiLFdI/y
wbfHOjgizSwcZlwD03o8YymdXmVU1tijW7J4PqgiMuNfDHR3MnidXqfpyl89aHAv
cucpQFcJQQV7JeImnPKOrGdIBUIU77MmZiqfslznxYYvRmq9MiNPUmA/ToSkuNOC
9ayXuTUsGzhSKnd292C4NCeE+yv0nP5vwiOqob6leeMdC6gKW50bPccApzWS0on7
xoZ7efJHFNYu5mdETFqjC8AT7WOCODLwlHOfl1J98CRelR7rZUM5qZVqAfX1cPBb
YZ6W2hlwlWKbvBnw+c6knSZtUAjTgHpv2bxNFsk9hgs2J+EoAGmVzb1eUWM2aUOe
f079GNYnmAOXAr4uIBLqH413OsXhhrPWkkryf8s/br43brql2iwVFXKL78gojBiA
j2zRjp4NV9VAo8fyyBu59d6fdcKiS8gwFvfML2Mt99X0bxNjxZQW8pokdf7jVLVn
tWa998GTkJDYBZQboiPQrEluKvTzAN2kj4QvGDaN+VkAKLC/I3PaR3akDRkv6LKw
glBzKVQyL06abH8Y1uxUMSM8llDNT/5sPnH1voQjiZ4gOMmJS8A93Qbr/yxLz14Q
ndoOYAE3Yl1IN/gLDg97lIXf3ozevYsIajrxZzbj25u/cJllQqK34+b4Zijo9U3P
wqurly4RqeiaP4hc5Dwzx20d/enYIHHwN2BDyYFRYstKRDv9ERJpxXsM2TK4BdCo
i3Xutwlw7jS6v+UeG+sfDym9seDw4yWwN9CTOSGjY3pOpXjWsJd7EBoHyDTpdvyk
OkCNjX8wsZDO31ZT9oMiriler8k0Z0ktzWONwIuSCHb+lo5xyPxEjbIZ9SyNUQgI
/utggPQsJEL2MSPSrvxQ9QkrjkrFIb+/4pk1+3e3QTqTxxDdIAdBxWC+0FTDnq5D
voBFImrR7hy8rxRMsYYu0pZEVlDyxMJjcxlTlmYUh8UdKFd5NJ/Pgf20iF3FNDEl
IXVafVukTx3H1wbM05Z8YLsy4EBqBaTmzs+7aRUVpOjNlHr3y2GxMG6rpzbbOw7u
gpmmaLO5D5TInmZueFw4XSZbylHzq19ZWbP8Cm5VMLjZ1n3DGkGnoMi5u6FJ1Ulg
ABk/PuEHt/uXyMCQ2nxTKwBeQWn8YIDubiSRIVl+emq5cHMODhqjmZ6PEcyXL7uM
XamKvgVkrOyOKdtiF9Zm1dcMrgcmWCTu9/M7eIJ10VZjJIGC6Riftm5LuGAuisF9
7StgAGt6QHAzDmGNnCH40JFkEln+tmVSC++uXNYDjg+5rcXeoMCyBYc67/jFCxrd
BRqdovNmV5xuH2QQpbTBI+RyRa2K/TkuvpEEKtjBhChA0TxFDvClzG8VsGwan6XX
0ErndHaclEsFbGxfP/k7lYpEb1tDCsN0AgC/BZkh3y+sLHAybRrc8IUwcyxe7FOV
EMg5HPOOwiebjFbaNHPsQ7Bdu5O2VWwWN8KJKFIl2lTj2okEEZC71XAU9IR/DQ6k
sGmjoL4U2knsM2EjQxoa2tITclN82WbDPrRPgKAAPoBrJZqrbjtGThZQbact1lq/
rbXfHIQ+0mC5EcTxg95nDIIz8gBRYHG4w1MKepaUCptDsZ3+qDOtoIrchvzBTAia
BYBngDGBJAKTWEi1Gayxo7k7xQcB7PBhUSwW4LGawBu6oQwahqddjZC9fOIwKVd5
+vCKN0rkhBcVPddr9EUQdw==
`pragma protect end_protected
