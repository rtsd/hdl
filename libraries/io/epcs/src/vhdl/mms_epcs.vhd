-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide read and write access to flash
-- Description:
-- . g_sim_flash_model
--   When g_sim_flash_model=TRUE then the m25p128.vhd flash model is
--   instantiated in ip_stratixiv_asmi_parallel forsimulation. It needs to be
--   instantiated inside the IP, because the flash IO isnot available at
--   the port IO, because sythesis automatically connects it to the proper
--   pins.
--   However the m25p128.vhd flash model uses shared variables. This
--   probably causes the failure: "Signal 'blockboundary' has multiple
--   drivers but is not a resolved signal". Therefore ensure that there is
--   only one instance of mms_epcs that has g_sim_flash_model=TRUE.
--
-- Remarks:
-- . To simulate this module, DS added an M25P128 simulation model (source: Numonyx)
--   to the generated ALTASMI_PARALLEL megafunction code, and hooked it up to the correct signals
--   internally.
-- . The simulation model requires real-life (long) powerup delays to be used. Refer to
--   /tb/vhdl/m25p128_model/lib/TimingData.vhd for the delays and references to the corresponding
--   table in the data sheet. These constants can be modified for faster simulation, but I've left them
--   as they were as simulating one page write and read is sufficient.

library IEEE, common_lib, dp_lib, technology_lib, tech_flash_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_flash_lib.tech_flash_component_pkg.all;

entity mms_epcs is
  generic (
    g_technology         : natural := c_tech_select_default;
    g_sim_flash_model    : boolean := false;
    g_protect_addr_range : boolean := false;  -- TRUE protects range set below by requiring specific passphrase to be written via MM
    g_protected_addr_lo  : natural := 0;  -- Byte address
    g_protected_addr_hi  : natural := 6029311  -- Byte address, for UniBoard1 this is 23 sectors*1024 pages*256 bytes -1 = 6029311
  );
  port (
    mm_rst          : in    std_logic;
    mm_clk          : in    std_logic;

    epcs_clk        : in    std_logic;

    -- General MM control:
    epcs_mosi       : in    t_mem_mosi := c_mem_mosi_rst;
    epcs_miso       : out   t_mem_miso;

    -- Data path (epcs) -> MM status and data
    dpmm_ctrl_mosi  : in    t_mem_mosi := c_mem_mosi_rst;
    dpmm_ctrl_miso  : out   t_mem_miso := c_mem_miso_rst;

    dpmm_data_mosi  : in    t_mem_mosi := c_mem_mosi_rst;
    dpmm_data_miso  : out   t_mem_miso := c_mem_miso_rst;

    -- MM -> data path (epcs) status and data
    mmdp_ctrl_mosi  : in    t_mem_mosi := c_mem_mosi_rst;
    mmdp_ctrl_miso  : out   t_mem_miso := c_mem_miso_rst;

    mmdp_data_mosi  : in    t_mem_mosi := c_mem_mosi_rst;
    mmdp_data_miso  : out   t_mem_miso := c_mem_miso_rst
  );
end mms_epcs;

architecture str of mms_epcs is
  -- ASMI_PARALLEL supports page write mode of 256 bytes
  constant c_epcs_page_size            : natural := 256;
  constant c_user_data_w               : natural := c_word_w;
  constant c_epcs_data_w               : natural := 8;
  constant c_epcs_addr_w               : natural := tech_flash_addr_w(g_technology);

  constant c_fifo_depth_bits           : natural := c_epcs_page_size * c_epcs_data_w;
  -- FIFO depths relative to epcs and user data widths
  constant c_epcs_fifo_depth           : natural := c_fifo_depth_bits / c_epcs_data_w * 2;  -- *2 because we need the full depth (without the *2) but we can't 'max out' the FIFO.
  constant c_user_fifo_depth           : natural := c_fifo_depth_bits / c_user_data_w;
  -- We want to monitor FIFO contents on the user side
  signal user_to_epcs_fifo_usedw       : std_logic_vector(ceil_log2(c_user_fifo_depth) - 1 downto 0);
  signal epcs_to_user_fifo_usedw       : std_logic_vector(ceil_log2(c_epcs_fifo_depth / (c_word_w / c_epcs_data_w)) - 1 downto 0);
  -- Signal to stop reading data from epcs flash when we've read one page size
  signal epcs_to_user_fifo_wr_usedw    : std_logic_vector(ceil_log2(c_epcs_fifo_depth) - 1 downto 0);

  signal epcs_rst                      : std_logic;

  -- Signals between FIFOs and ASMI_PARALLEL
  signal epcs_in_addr                  : std_logic_vector(c_epcs_addr_w - 1 downto 0);
  signal epcs_in_datain                : std_logic_vector(c_epcs_data_w - 1 downto 0);
  signal epcs_in_rden                  : std_logic;
  signal epcs_in_read                  : std_logic;
  signal epcs_in_shift_bytes           : std_logic;
  signal epcs_in_wren                  : std_logic;
  signal epcs_in_write                 : std_logic;
  signal epcs_in_sector_erase          : std_logic;

  signal epcs_out_busy                 : std_logic;
  signal epcs_out_data_valid           : std_logic;
  signal epcs_out_dataout              : std_logic_vector(c_epcs_data_w - 1 downto 0);

  signal nxt_epcs_in_addr              : std_logic_vector(c_epcs_addr_w - 1 downto 0);
  signal nxt_epcs_in_datain            : std_logic_vector(c_epcs_data_w - 1 downto 0);
  signal nxt_epcs_in_rden              : std_logic;
  signal nxt_epcs_in_read              : std_logic;
  signal nxt_epcs_in_shift_bytes       : std_logic;
  signal nxt_epcs_in_wren              : std_logic;
  signal nxt_epcs_in_write             : std_logic;
  signal nxt_epcs_in_sector_erase      : std_logic;

  signal nxt_epcs_out_data_valid       : std_logic;
  signal nxt_epcs_out_dataout          : std_logic_vector(c_epcs_data_w - 1 downto 0);

  signal epcs_wr_sosi                  : t_dp_sosi;
  signal epcs_wr_siso                  : t_dp_siso;

  signal epcs_rd_sosi                  : t_dp_sosi;
  signal epcs_rd_siso                  : t_dp_siso;

  signal user_wr_sosi                  : t_dp_sosi;
  signal user_wr_siso                  : t_dp_siso;

  signal user_rd_sosi                  : t_dp_sosi;
  signal user_rd_siso                  : t_dp_siso;

  signal epcs_in_addr_from_reg         : std_logic_vector(c_epcs_addr_w - 1 downto 0);
  signal epcs_in_read_from_reg         : std_logic;
  signal epcs_in_rden_from_reg         : std_logic;
  signal epcs_in_write_from_reg        : std_logic;
  signal epcs_in_sector_erase_from_reg : std_logic;

  signal unprotect_address_range       : std_logic;
  signal addr_in_protected_range       : std_logic;
  signal allow_write                   : std_logic;
begin
  u_epcs_reg: entity work.epcs_reg
  generic map (
    g_epcs_addr_w => c_epcs_addr_w
  )
  port map (
    mm_rst                   => mm_rst,
    mm_clk                   => mm_clk,

    epcs_rst                 => epcs_rst,
    epcs_clk                 => epcs_clk,

    sla_in                   => epcs_mosi,
    sla_out                  => epcs_miso,

    epcs_in_addr             => epcs_in_addr_from_reg,
    epcs_in_read_evt         => epcs_in_read_from_reg,
    epcs_in_rden             => epcs_in_rden_from_reg,
    epcs_in_write_evt        => epcs_in_write_from_reg,
    epcs_in_sector_erase_evt => epcs_in_sector_erase_from_reg,

    epcs_out_busy            => epcs_out_busy,

    unprotect_address_range  => unprotect_address_range
  );

  epcs_wr_siso.ready <= '1';

  u_fifo_user_to_epcs : entity dp_lib.dp_fifo_dc_mixed_widths
  generic map (
    g_technology   => g_technology,
    g_wr_data_w    => c_user_data_w,
    g_rd_data_w    => c_epcs_data_w,
    g_use_ctrl     => false,
    g_wr_fifo_size => c_user_fifo_depth
  )
  port map (
    wr_rst         => mm_rst,
    wr_clk         => mm_clk,
    rd_rst         => epcs_rst,
    rd_clk         => epcs_clk,
    -- ST sink
    snk_out        => user_wr_siso,
    snk_in         => user_wr_sosi,
    -- Monitor FIFO filling
    wr_usedw       => user_to_epcs_fifo_usedw,
    rd_emp         => OPEN,
    -- ST source
    src_in         => epcs_wr_siso,
    src_out        => epcs_wr_sosi
  );

  u_fifo_epcs_to_user : entity dp_lib.dp_fifo_dc_mixed_widths
  generic map (
    g_technology   => g_technology,
    g_wr_data_w    => c_epcs_data_w,
    g_rd_data_w    => c_user_data_w,
    g_use_ctrl     => false,
    g_wr_fifo_size => c_epcs_fifo_depth
  )
  port map (
    wr_rst         => epcs_rst,
    wr_clk         => epcs_clk,
    rd_rst         => mm_rst,
    rd_clk         => mm_clk,
    -- ST sink
    snk_out        => epcs_rd_siso,
    snk_in         => epcs_rd_sosi,
    -- Monitor FIFO filling
    wr_usedw       => epcs_to_user_fifo_wr_usedw,
    rd_usedw       => epcs_to_user_fifo_usedw,
    rd_emp         => OPEN,
    -- ST source
    src_in         => user_rd_siso,
    src_out        => user_rd_sosi
  );

  u_asmi_parallel: entity tech_flash_lib.tech_flash_asmi_parallel
  generic map (
    g_technology      => g_technology,
    g_sim_flash_model => g_sim_flash_model
  )
  port map (
    addr          => epcs_in_addr,
    clkin         => epcs_clk,
    datain        => epcs_in_datain,
    rden          => epcs_in_rden,
    read          => epcs_in_read,
    shift_bytes   => epcs_in_shift_bytes,
    wren          => epcs_in_wren,
    write         => epcs_in_write,
    sector_erase  => epcs_in_sector_erase,

    busy          => epcs_out_busy,
    data_valid    => nxt_epcs_out_data_valid,
    dataout       => nxt_epcs_out_dataout,
    illegal_write => OPEN,
    illegal_erase => open
  );

  u_mms_dp_fifo_to_mm: entity dp_lib.mms_dp_fifo_to_mm
  generic map(
    g_rd_fifo_depth => c_epcs_fifo_depth / (c_word_w / c_epcs_data_w)
  )
  port map (
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    rd_sosi            => user_rd_sosi,
    rd_siso            => user_rd_siso,

    ctrl_mosi          => dpmm_ctrl_mosi,
    ctrl_miso          => dpmm_ctrl_miso,

    data_mosi          => dpmm_data_mosi,
    data_miso          => dpmm_data_miso,

    rd_usedw           => epcs_to_user_fifo_usedw
  );

  u_mms_dp_fifo_from_mm: entity dp_lib.mms_dp_fifo_from_mm
  generic map(
    g_wr_fifo_depth => c_user_fifo_depth
  )
  port map (
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    wr_sosi            => user_wr_sosi,

    ctrl_mosi          => mmdp_ctrl_mosi,
    ctrl_miso          => mmdp_ctrl_miso,

    data_mosi          => mmdp_data_mosi,
    data_miso          => mmdp_data_miso,

    wr_usedw           => user_to_epcs_fifo_usedw
  );

  -- Optional protection of specified address range
  gen_protection: if g_protect_addr_range = true generate
    -- Help signal to show when the address in in the protected range
    addr_in_protected_range <= '1' when TO_UINT(epcs_in_addr_from_reg) >= g_protected_addr_lo and TO_UINT(epcs_in_addr_from_reg) <= g_protected_addr_hi else '0';

    -- Don't allow write if address belongs to protected range and protected range is not unlocked
    allow_write <= '0' when addr_in_protected_range = '1' and unprotect_address_range = '0' else '1';
  end generate;

  no_protection: if g_protect_addr_range = false generate
    allow_write <= '1';
  end generate;

  -- Looking at the timing diagrams of the ALTASMI_PARALLEL megafunction, the interface actually
  -- maps quite well on a DP streaming interface:
  nxt_epcs_in_addr                            <= epcs_in_addr_from_reg;
  nxt_epcs_in_datain                          <= epcs_wr_sosi.data(c_epcs_data_w - 1 downto 0);
  -- We only want to read out one page at a time. Our EPCS->user data FIFO is oversized (*2), so
  -- we need to de-assert the read signal towards the EPCS based on usedw.
  nxt_epcs_in_rden                            <= epcs_in_rden_from_reg when unsigned(epcs_to_user_fifo_wr_usedw) < c_epcs_page_size-1 else '0';
  nxt_epcs_in_read                            <= epcs_in_read_from_reg;

  nxt_epcs_in_shift_bytes                     <= allow_write and epcs_wr_sosi.valid;
  nxt_epcs_in_wren                            <= allow_write and ((epcs_wr_sosi.valid or epcs_in_write_from_reg) or epcs_in_sector_erase_from_reg);
  nxt_epcs_in_sector_erase                    <= allow_write and epcs_in_sector_erase_from_reg;
  nxt_epcs_in_write                           <= allow_write and epcs_in_write_from_reg;

  epcs_rd_sosi.data(c_epcs_data_w - 1 downto 0) <= epcs_out_dataout;
  epcs_rd_sosi.valid                          <= epcs_out_data_valid;

  -- The ASMI_PARALLEL requires that data is set up on the falling edge. The following rising
  -- edge of the ASMI_PARALLEL's 20MHz clkin clocks in the data. The data is required to be held until
  -- the next falling edge:
  --  __ __ __ __ __ __
  -- _||_||_||_||_||_||_
  --         ____
  -- ________|  |_______
  --
  -- All used components expect rising edge clocked signals, so we're reclocking the relevant signals
  -- with falling-edge clocked registers:
  p_epcs_clk: process(epcs_clk, epcs_rst)
  begin
    if epcs_rst = '1' then
      epcs_in_addr          <= (others => '0');
      epcs_in_datain        <= (others => '0');
      epcs_in_rden          <= '0';
      epcs_in_read          <= '0';
      epcs_in_shift_bytes   <= '0';
      epcs_in_wren          <= '0';
      epcs_in_write         <= '0';
      epcs_in_sector_erase  <= '0';

      epcs_out_dataout      <= (others => '0');
      epcs_out_data_valid   <= '0';
    elsif falling_edge(epcs_clk) then
      epcs_in_addr          <= nxt_epcs_in_addr;
      epcs_in_datain        <= nxt_epcs_in_datain;
      epcs_in_rden          <= nxt_epcs_in_rden;
      epcs_in_read          <= nxt_epcs_in_read;
      epcs_in_shift_bytes   <= nxt_epcs_in_shift_bytes;
      epcs_in_wren          <= nxt_epcs_in_wren;
      epcs_in_write         <= nxt_epcs_in_write;
      epcs_in_sector_erase  <= nxt_epcs_in_sector_erase;

      epcs_out_dataout      <= nxt_epcs_out_dataout;
      epcs_out_data_valid   <= nxt_epcs_out_data_valid;
    end if;
  end process;

  u_common_areset: entity common_lib.common_areset
  port map (
    in_rst             => mm_rst,
    clk                => epcs_clk,
    out_rst            => epcs_rst
  );
end str;
