-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

entity tb_i2c_master is
end tb_i2c_master;

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.i2c_smbus_pkg.all;
use work.i2c_dev_max1617_pkg.all;
use work.i2c_dev_max6652_pkg.all;
use work.i2c_pkg.all;

architecture tb of tb_i2c_master is
  constant c_sim                 : boolean := true;  -- FALSE
  constant c_clk_freq_in_MHz     : natural := 100;  -- 100 MHz
  constant c_clk_period          : time    := (10**3 / c_clk_freq_in_MHz) * 1 ns;
  constant c_rst_period          : time    := 4 * c_clk_period;

  constant c_phy_i2c              : t_c_i2c_phy := func_i2c_sel_a_b(c_sim, c_i2c_phy_sim, func_i2c_calculate_phy(c_clk_freq_in_MHz));

  constant c_bus_dat_w           : natural := 8;
  constant c_sens_temp_volt_sz   : natural := 9;  -- Should match nof read bytes via I2C in the sens_ctrl SEQUENCE list

  -- Model I2C sensor slaves as on the LOFAR RSP board
  constant ADR_MAX6652           : natural := MAX6652_ADR_GND;
  constant ADR_MAX1617_BP        : natural := MAX1617_ADR_MID_MID;
  constant ADR_MAX1617_AP0       : natural := MAX1617_ADR_LOW_LOW;
  constant ADR_MAX1617_AP1       : natural := MAX1617_ADR_LOW_HIGH;
  constant ADR_MAX1617_AP2       : natural := MAX1617_ADR_HIGH_LOW;
  constant ADR_MAX1617_AP3       : natural := MAX1617_ADR_HIGH_HIGH;

  constant c_bp_volt_address     : std_logic_vector := TO_UVEC(ADR_MAX6652,     7);  -- MAX6652 address GND
  constant c_bp_temp_address     : std_logic_vector := TO_UVEC(ADR_MAX1617_BP,  7);  -- MAX1618 address MID  MID
  constant c_ap0_temp_address    : std_logic_vector := TO_UVEC(ADR_MAX1617_AP0, 7);  -- MAX1618 address LOW  LOW
  constant c_ap1_temp_address    : std_logic_vector := TO_UVEC(ADR_MAX1617_AP1, 7);  -- MAX1618 address LOW  HIGH
  constant c_ap2_temp_address    : std_logic_vector := TO_UVEC(ADR_MAX1617_AP2, 7);  -- MAX1618 address HIGH LOW
  constant c_ap3_temp_address    : std_logic_vector := TO_UVEC(ADR_MAX1617_AP3, 7);  -- MAX1618 address HIGH HIGH
  constant c_bp_temp             : integer := 60;
  constant c_ap0_temp            : integer := 70;
  constant c_ap1_temp            : integer := 71;
  constant c_ap2_temp            : integer := 72;
  constant c_ap3_temp            : integer := 73;
  constant c_volt_1v2            : natural := 92;  -- 92 *  2.5/192 = 1.2
  constant c_volt_2v5            : natural := 147;  -- 147 *  3.3/192 = 2.5
  constant c_volt_nc             : natural := 13;  -- 13 * 12  /192 = 0.1
  constant c_volt_3v3            : natural := 127;  -- 127 *  5.0/192 = 3.3
  constant c_temp_pcb            : natural := 40;
  constant c_temp_high           : natural := 127;

  constant c_protocol_list : t_nat_natural_arr := (
    SMBUS_READ_BYTE,  ADR_MAX6652,     MAX6652_REG_READ_VIN_2_5,
    SMBUS_READ_BYTE,  ADR_MAX6652,     MAX6652_REG_READ_VIN_3_3,
    SMBUS_READ_BYTE,  ADR_MAX6652,     MAX6652_REG_READ_VCC,
    SMBUS_READ_BYTE,  ADR_MAX6652,     MAX6652_REG_READ_TEMP,
    SMBUS_READ_BYTE,  ADR_MAX1617_BP,  MAX1617_CMD_READ_REMOTE_TEMP,
--  For debugging, use AP temp fields in RSR to read other info from the sensor, e.g.:
--     SMBUS_READ_BYTE , ADR_MAX1617_BP , MAX1617_CMD_READ_STATUS,
--     SMBUS_READ_BYTE , ADR_MAX1617_BP , MAX1617_CMD_READ_CONFIG,
--     SMBUS_READ_BYTE , ADR_MAX1617_BP , MAX1617_CMD_READ_REMOTE_HIGH,
--     SMBUS_READ_BYTE , ADR_MAX1617_BP , MAX1617_CMD_READ_REMOTE_LOW,
    SMBUS_READ_BYTE,  ADR_MAX1617_AP0, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_READ_BYTE,  ADR_MAX1617_AP1, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_READ_BYTE,  ADR_MAX1617_AP2, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_READ_BYTE,  ADR_MAX1617_AP3, MAX1617_CMD_READ_REMOTE_TEMP,
    SMBUS_WRITE_BYTE, ADR_MAX6652,     MAX6652_REG_CONFIG,           MAX6652_CONFIG_LINE_FREQ_SEL + MAX6652_CONFIG_START,
    SMBUS_WRITE_BYTE, ADR_MAX1617_BP,  MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP0, MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP1, MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP2, MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP3, MAX1617_CMD_WRITE_CONFIG,     MAX1617_CONFIG_ID + MAX1617_CONFIG_THERM,
    SMBUS_WRITE_BYTE, ADR_MAX1617_BP,  MAX1617_CMD_WRITE_REMOTE_HIGH, c_temp_high,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP0, MAX1617_CMD_WRITE_REMOTE_HIGH, c_temp_high,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP1, MAX1617_CMD_WRITE_REMOTE_HIGH, c_temp_high,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP2, MAX1617_CMD_WRITE_REMOTE_HIGH, c_temp_high,
    SMBUS_WRITE_BYTE, ADR_MAX1617_AP3, MAX1617_CMD_WRITE_REMOTE_HIGH, c_temp_high,
    SMBUS_C_END
  );

  -- Expected result list for the c_protocol_list
  -- . entries 0 should also be 0 in the result buffer
  -- . entries 1 indicate a read octet that needs to be stored for the user
  constant c_expected_mask : t_nat_natural_arr := (1, 0,
                                                   1, 0,
                                                   1, 0,
                                                   1, 0,
                                                   1, 0,
--                                                    1, 0,
--                                                    1, 0,
--                                                    1, 0,
--                                                    1, 0,
                                                   1, 0,
                                                   1, 0,
                                                   1, 0,
                                                   1, 0,
                                                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

  -- Expected bytes as read by c_protocol_list : 92, 147, 127, 40, 60, 70, 71, 72, 73
  -- . Keep the zeros in like with c_expected_mask to be able to use a single result_cnt for addressing
  constant c_expected_data : t_nat_natural_arr := (c_volt_1v2, 0,
                                                   c_volt_2v5, 0,
                                                   c_volt_3v3, 0,
                                                   c_temp_pcb, 0,
                                                   c_bp_temp,  0,
--                                                    1, 0,
--                                                    1, 0,
--                                                    1, 0,
--                                                    1, 0,
                                                   c_ap0_temp, 0,
                                                   c_ap1_temp, 0,
                                                   c_ap2_temp, 0,
                                                   c_ap3_temp, 0,
                                                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

  -- Control register
  constant c_control_activate     : natural := 1;
  constant c_control_ready_bi     : natural := 1;

  signal tb_end            : std_logic := '0';
  signal clk               : std_logic := '0';
  signal rst               : std_logic := '1';

  signal scl_stretch       : std_logic := 'Z';
  signal scl               : std_logic;
  signal sda               : std_logic;

  -- MM registers interface
  signal control_miso      : t_mem_miso;
  signal control_mosi      : t_mem_mosi;
  signal protocol_miso     : t_mem_miso;
  signal protocol_mosi     : t_mem_mosi;
  signal result_miso       : t_mem_miso;
  signal result_mosi       : t_mem_mosi;

  signal control_status    : std_logic;
  signal control_interrupt : std_logic;

  signal result_rd_dly1    : std_logic;
  signal result_rd_dly2    : std_logic;
  signal result_val        : std_logic;
  signal result_data       : std_logic_vector(c_byte_w - 1 downto 0);
  signal result_cnt        : natural := 0;

  signal expected_ctrl     : natural;
  signal expected_data     : natural;
  signal expected_cnt      : natural := c_expected_data'length;
begin
  -- run -all

  rst <= '0' after 4 * c_clk_period;
  clk <= (not clk) or tb_end after c_clk_period / 2;

  -- I2C bus
  scl <= 'H';  -- model I2C pull up
  sda <= 'H';  -- model I2C pull up

  scl <= scl_stretch;

  sens_clk_stretch : process (scl)
  begin
    if falling_edge(scl) then
      scl_stretch <= '0', 'Z' after 50 ns;  -- < 10 ns to effectively disable stretching, >= 50 ns to enable it
    end if;
  end process;

  p_mm_stimuli : process
  begin
    -- Wait for reset release
    control_mosi   <= c_mem_mosi_rst;
    protocol_mosi  <= c_mem_mosi_rst;
    result_mosi    <= c_mem_mosi_rst;
    control_status <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 10);

    -- Write protocol list
    for I in 0 to c_protocol_list'length - 1 loop
      proc_mem_mm_bus_wr(I, c_protocol_list(I), clk, protocol_miso, protocol_mosi);
    end loop;

    -- Activate protocol list
    proc_mem_mm_bus_wr(0, c_control_activate, clk, control_miso, control_mosi);

    -- Wait for protocol ready
    -- . wait for control_interrupt
    -- . do not use control_mosi to poll control_status, because then it can just occur that the read
    --   that act as acknowledge again clears the control_status before it got noticed in the
    --   WHILE control_status='0' LOOP
    proc_common_wait_until_high(clk, control_interrupt);
    --WHILE control_status='0' LOOP
    proc_mem_mm_bus_rd(0, clk, control_miso, control_mosi);  -- read result available in control_status
    proc_mem_mm_bus_rd_latency(c_mem_reg_rd_latency, clk);
    control_status <= control_miso.rddata(c_control_ready_bi);
    proc_common_wait_some_cycles(clk, 1);
    --END LOOP;

    -- Read result list
    for I in 0 to c_expected_mask'length - 1 loop
      proc_mem_mm_bus_rd(I, clk, result_miso, result_mosi);  -- read result available in result_data
    end loop;

    proc_common_wait_some_cycles(clk, 100);

    -- verify that there happened valid I2C communication
    assert result_cnt = expected_cnt
      report "I2C error in nof result count"
      severity ERROR;

    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  -- Derive rdval from rd (read latency = 2) to mark when result_data read in p_mm_stimuli is valid
  result_rd_dly1 <= result_mosi.rd when rising_edge(clk);
  result_rd_dly2 <= result_rd_dly1 when rising_edge(clk);
  result_val  <= result_rd_dly2;
  result_data <= result_miso.rddata(c_byte_w - 1 downto 0) when result_val = '1' else (others => 'X');

  -- Show the expected result ctrl and expected result data
  expected_ctrl <= c_expected_mask(result_cnt mod c_expected_mask'length);
  expected_data <= c_expected_data(result_cnt mod c_expected_mask'length);

  result_cnt <= result_cnt + 1 when rising_edge(clk) and result_val = '1';

  p_i2c_verify : process
  begin
    -- verify result_data at clk and when result_val='1'
    wait until rising_edge(clk);
    if result_val = '1' then
      if c_expected_mask(result_cnt) = 0 then
        if TO_UINT(result_data) /= 0 then
          report "I2C error in control result byte"
            severity ERROR;
        end if;
      else
        if TO_UINT(result_data) /= expected_data then
          report "I2C error in control result byte"
            severity ERROR;
        end if;
      end if;
    end if;
  end process;

  -- I2C master
  u_i2c_master : entity work.i2c_master
  generic map (
    g_i2c_mm  => c_i2c_mm,
    g_i2c_phy => c_phy_i2c
  )
  port map (
    -- GENERIC Signal
    gs_sim                   => c_sim,

    rst                      => rst,
    clk                      => clk,
    sync                     => '1',

    ---------------------------------------------------------------------------
    -- Memory Mapped Slave interface with Interrupt
    ---------------------------------------------------------------------------
    -- MM slave I2C control register
    mms_control_address      => control_mosi.address(c_i2c_mm.control_adr_w - 1 downto 0),
    mms_control_write        => control_mosi.wr,
    mms_control_read         => control_mosi.rd,
    mms_control_writedata    => control_mosi.wrdata(c_word_w - 1 downto 0),  -- use default MM bus width for control
    mms_control_readdata     => control_miso.rddata(c_word_w - 1 downto 0),
    -- MM slave I2C protocol register
    mms_protocol_address     => protocol_mosi.address(c_i2c_mm.protocol_adr_w - 1 downto 0),
    mms_protocol_write       => protocol_mosi.wr,
    mms_protocol_read        => protocol_mosi.rd,
    mms_protocol_writedata   => protocol_mosi.wrdata(c_byte_w - 1 downto 0),  -- define MM bus data has same width as SMBus data
    mms_protocol_readdata    => protocol_miso.rddata(c_byte_w - 1 downto 0),
    -- MM slave I2C result register
    mms_result_address       => result_mosi.address(c_i2c_mm.result_adr_w - 1 downto 0),
    mms_result_write         => result_mosi.wr,
    mms_result_read          => result_mosi.rd,
    mms_result_writedata     => result_mosi.wrdata(c_byte_w - 1 downto 0),  -- define MM bus data has same width as SMBus data
    mms_result_readdata      => result_miso.rddata(c_byte_w - 1 downto 0),
    -- Interrupt
    ins_result_rdy           => control_interrupt,

    ---------------------------------------------------------------------------
    -- I2C interface
    ---------------------------------------------------------------------------
    scl                      => scl,
    sda                      => sda
  );

  -- I2C slaves
  sens_temp_bp : entity work.dev_max1618
  generic map (
    g_address => c_bp_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_bp_temp
  );

  sens_temp_ap0 : entity work.dev_max1618
  generic map (
    g_address => c_ap0_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_ap0_temp
  );

  sens_temp_ap1 : entity work.dev_max1618
  generic map (
    g_address => c_ap1_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_ap1_temp
  );

  sens_temp_ap2 : entity work.dev_max1618
  generic map (
    g_address => c_ap2_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_ap2_temp
  );

  sens_temp_ap3 : entity work.dev_max1618
  generic map (
    g_address => c_ap3_temp_address
  )
  port map (
    scl  => scl,
    sda  => sda,
    temp => c_ap3_temp
  );

  sens_volt_bp : entity work.dev_max6652
  generic map (
    g_address => c_bp_volt_address
  )
  port map (
    scl       => scl,
    sda       => sda,
    volt_2v5  => c_volt_1v2,
    volt_3v3  => c_volt_2v5,
    volt_12v  => c_volt_nc,
    volt_vcc  => c_volt_3v3,
    temp      => c_temp_pcb
  );
end tb;
