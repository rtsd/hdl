-- -----------------------------------------------------------------------------
--
-- Copyright 2014-2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author : -
-- Changed by : D.F. Brouwer

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;

package tech_mult_pkg is
  type t_c_tech_mult_variant is record
    -- PHY variant within a technology
    name  : string(1 to 3);  -- = "RTL" or " IP"
    ip    : boolean;  -- = TRUE  TRUE = Megawizard IP, FALSE = RTL implemenation
  end record;

  --                                                                      name    ip
  constant c_tech_mult_stratixiv_rtl                  : t_c_tech_mult_variant := ("RTL",  false);
  constant c_tech_mult_stratixiv_ip                   : t_c_tech_mult_variant := (" IP",  true);
  constant c_tech_mult_arria10_rtl                    : t_c_tech_mult_variant := ("RTL",  false);
  constant c_tech_mult_arria10_ip                     : t_c_tech_mult_variant := (" IP",  true);
  constant c_tech_mult_agi027_1e1v_rtl                : t_c_tech_mult_variant := ("RTL",  false);
  constant c_tech_mult_agi027_1e1v_ip                 : t_c_tech_mult_variant := (" IP",  true);
end tech_mult_pkg;
