-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Add flow XON-XOFF control by flushing frames
-- Description:
--   The siso.xon signal provides flow control per block of data, so at
--   sop/eop boundaries. A source that listens to siso.xon can stop streaming
--   blocks when siso.xon = '0' and resume streaming blocks when siso.xon =
--   '1'. The siso.ready signal provides flow control per clock cycle. Hence
--   the siso.ready can also cause a source to stop streaming.
--
--   If a source keeps on streaming blocks of data, becaue it does not listen
--   to siso.xon and also not to siso.ready, then the dp_xonoff can be applied
--   to drop the blocks when siso.xon = '0'. The purpose of dp_xonoff.vhd is
--   then to avoid downstream overflow, when the downstream signals
--   out_siso.xon = '0' to indicate that it cannot accept more blocks.
--   Therefore the dp_xonoff listens to the out_siso.xon and flushes
--   (discards) blocks when out_siso.xon = '0' and resumes letting blocks
--   through when siso.xon = '1' again.
--
--   The dp_xonoff operates per block, so it takes care that complete blocks
--   are flushed when out_siso.xon = '0', or passed on when out_siso.xon =
--   '1'. Discarding blocks is acceptable to avoid overflow. Discarding data
--   in a block is not acceptable, because that would corrupt the block
--   format (e.g. its length, or cause missing sop or eop). Therefore the
--   dp_xonoff uses siso.xon and does not listen to siso.ready.
--
--   The dp_xonoff in_siso.xon = '1' always, because the dp_xonoff will
--   discard incoming blocks if they cannot be passed on. When out_siso.xon
--   = '1', then in_siso.ready <= in_sosi.ready, so passed on. When
--   out_siso.xon = '0', then in_siso.xon = '1' and in_siso.ready <= '1'
--   (so in_siso = c_dp_sosi.rdy), because dp_xonoff can flush incoming
--   blocks at maximum rate, until out_siso.xon = '1' again.
--
--   A dp_fifo will keep on outputing blocks when its out_siso.xon = '0',
--   because it only passes in_siso.xon <= out_siso.xon, so it relies on its
--   upsteam input to listen to the out_siso.xon. If there are still multiple
--   blocks in the FIFO then these will be output. Typicaaly it is not
--   necessary to have a dp_xonoff at the output of the dp_fifo to discard
--   these blocks, because the dp_fifo does listen to out_siso.ready = '0',
--   so the flow control per sample will then stop the dp_fifo from causing
--   downstream overflow.
--
--   When a dp_fifo runs almost full within g_fifo_af_margin, then the
--   in_siso.ready is made '0'. New addition is g_fifo_af_xon that defines a
--   dp_fifo fill margin for making in_sosi.xon = '0'. If the upstream does
--   not listen to ready flow control and xon flow control (e.g. like input
--   from an ADC or a WG), then there may be need for an dp_xonoff at the
--   input of the FIFO to discard input blocks.
--
--   The in_siso.ready = out_siso.ready so passed on unchanged, to support
--   detailed output to input flow control per cycle. The in_siso.xon is
--   always '1', because the out_siso.xon is taken care of in this
--   dp_xonoff.vhd by flushing any in_sosi data when out_siso.xon = '0'.
--
-- . Use tb_dp_fifo_xonoff.vhd to verify dp_xonoff to avoid dp_fifo overflow.
--
-- . When g_bypass=TRUE then the in and out are wired and the component is void.
-- . When g_bypass=FALSE then:
--     The output is ON when flush='0'.
--     The output is OFF when flush='1'.
--     The transition from OFF to ON occurs after an in_sosi.eop so between frames
--     The transition from ON to OFF occurs after an in_sosi.eop so between frames
--     Thanks to frm_busy it is also possible to switch between frames, so it
--     is not necessary that first an eop occurs, before the xon can change.
--     The possibility to switch xon at an eop is needed to be able to switch
--     xon in case there are no gaps between the frames.
-- . The primary control is via out_siso.xon, however there is an option to override
--   the out_siso.xon control and force the output to off by using force_xoff.
-- Remark:
-- . The output controls are not registered.
-- . The xon timing is not cycle critical therefor register flush to ease
--   timing closure
-- . Originally based on rad_frame_onoff from LOFAR RSP firmware

library IEEE;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;

entity dp_xonoff is
  generic (
    g_bypass : boolean := false
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    -- Frame in
    in_siso       : out t_dp_siso;
    in_sosi       : in  t_dp_sosi;
    -- Frame out
    out_siso      : in  t_dp_siso;  -- flush control via out_siso.xon
    out_sosi      : out t_dp_sosi;
    out_en        : out std_logic;  -- for monitoring purposes in tb
    -- Optional override to force XOFF ('1' = enable override)
    force_xoff    : in  std_logic := '0'
  );
end dp_xonoff;

architecture rtl of dp_xonoff is
  signal frm_busy     : std_logic;
  signal frm_busy_reg : std_logic;

  signal flush      : std_logic;
  signal nxt_flush  : std_logic;

  signal i_out_en   : std_logic;
  signal nxt_out_en : std_logic;
begin
  out_en <= i_out_en;

  gen_bypass : if g_bypass = true generate
    in_siso  <= out_siso;
    out_sosi <= in_sosi;
  end generate;

  no_bypass : if g_bypass = false generate
    in_siso.ready <= out_siso.ready or (not i_out_en);  -- pass on ready for detailed flow control per cycle only when output is enabled, otherwise ready = 1
    in_siso.xon <= '1';  -- upstream can remain on, because flush will handle out_siso.xon
    nxt_flush <= not out_siso.xon or force_xoff;  -- use xon for flow control at frame level

    p_clk: process(clk, rst)
    begin
      if rst = '1' then
        frm_busy_reg <= '0';
        flush        <= '0';
        i_out_en     <= '1';
      elsif rising_edge(clk) then
        frm_busy_reg <= frm_busy;
        flush        <= nxt_flush;  -- pipeline register flush to ease timing closure
        i_out_en     <= nxt_out_en;  -- state register out_en because it can only change between frames
      end if;
    end process;

    -- Detect in_sosi frame busy, frm_busy is '1' from sop including sop, until eop excluding eop
    p_frm_busy : process(in_sosi, in_sosi, frm_busy_reg)
    begin
      frm_busy <= frm_busy_reg;
      if in_sosi.sop = '1' then
        frm_busy <= '1';
      elsif in_sosi.eop = '1' then
        frm_busy <= '0';
      end if;
    end process;

    p_out_en : process(flush, i_out_en, frm_busy)
    begin
      nxt_out_en <= i_out_en;
      if frm_busy = '0' then
        if flush = '1' then
          nxt_out_en <= '0';
        else
          nxt_out_en <= '1';
        end if;
      end if;
    end process;

    p_out_sosi : process(in_sosi, i_out_en)
    begin
      -- Pass on sosi data via wires
      out_sosi       <= in_sosi;

      -- XON/XOFF flow control via sosi control
      out_sosi.sync  <= in_sosi.sync  and i_out_en;
      out_sosi.valid <= in_sosi.valid and i_out_en;
      out_sosi.sop   <= in_sosi.sop   and i_out_en;
      out_sosi.eop   <= in_sosi.eop   and i_out_en;
    end process;
  end generate;
end architecture;
