-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: IP components declarations for various devices that get wrapped by the tech components

library IEEE, technology_lib;
use IEEE.std_logic_1164.all;
use technology_lib.technology_pkg.all;

package tech_transceiver_component_pkg is
  ------------------------------------------------------------------------------
  -- ip_stratixiv
  ------------------------------------------------------------------------------

  component ip_stratixiv_hssi_gx_32b_generic is
  generic (
    g_mbps                    : natural;
    starting_channel_number   : natural := 0
  );
  port (
    cal_blk_clk              : in std_logic;
    gxb_powerdown            : in std_logic_vector(0 downto 0);
    pll_inclk                : in std_logic;
    pll_powerdown            : in std_logic_vector(0 downto 0);
    reconfig_clk             : in std_logic;
    reconfig_togxb           : in std_logic_vector(3 downto 0);
    rx_analogreset           : in std_logic_vector(0 downto 0);
    rx_datain                : in std_logic_vector(0 downto 0);
    rx_digitalreset          : in std_logic_vector(0 downto 0);
    rx_enapatternalign       : in std_logic_vector(0 downto 0);
    rx_seriallpbken          : in std_logic_vector(0 downto 0);
    tx_ctrlenable            : in std_logic_vector(3 downto 0);
    tx_datain                : in std_logic_vector(31 downto 0);
    tx_digitalreset          : in std_logic_vector(0 downto 0);
    pll_locked               : out std_logic_vector(0 downto 0);
    reconfig_fromgxb         : out std_logic_vector(16 downto 0);
    rx_byteorderalignstatus  : out std_logic_vector(0 downto 0);
    rx_clkout                : out std_logic_vector(0 downto 0);
    rx_ctrldetect            : out std_logic_vector(3 downto 0);
    rx_dataout               : out std_logic_vector(31 downto 0);
    rx_disperr               : out std_logic_vector(3 downto 0);
    rx_errdetect             : out std_logic_vector(3 downto 0);
    rx_freqlocked            : out std_logic_vector(0 downto 0);
    rx_patterndetect         : out std_logic_vector(3 downto 0);
    tx_clkout                : out std_logic_vector(0 downto 0);
    tx_dataout               : out std_logic_vector(0 downto 0)
  );
  end component;

  component ip_stratixiv_hssi_tx_32b_generic is
  generic(
    g_mbps : natural
  );
  port (
    cal_blk_clk     : in std_logic;
    gxb_powerdown   : in std_logic_vector(0 downto 0);
    pll_inclk       : in std_logic;
    pll_powerdown   : in std_logic_vector(0 downto 0);
    tx_ctrlenable   : in std_logic_vector(3 downto 0);
    tx_datain       : in std_logic_vector(31 downto 0);
    tx_digitalreset : in std_logic_vector(0 downto 0);
    pll_locked      : out std_logic_vector(0 downto 0);
    tx_clkout       : out std_logic_vector(0 downto 0);
    tx_dataout      : out std_logic_vector(0 downto 0)
  );
  end component;

  component ip_stratixiv_hssi_rx_32b_generic is
  generic (
    g_mbps                  : natural;
    starting_channel_number : natural := 0
  );
  port (
    cal_blk_clk              : in std_logic;
    gxb_powerdown            : in std_logic_vector(0 downto 0);
    reconfig_clk             : in std_logic;
    reconfig_togxb           : in std_logic_vector(3 downto 0);
    rx_analogreset           : in std_logic_vector(0 downto 0);
    rx_cruclk                : in std_logic_vector(0 downto 0) :=  (others => '0');
    rx_datain                : in std_logic_vector(0 downto 0);
    rx_digitalreset          : in std_logic_vector(0 downto 0);
    rx_enapatternalign       : in std_logic_vector(0 downto 0);
    reconfig_fromgxb         : out std_logic_vector(16 downto 0);
    rx_byteorderalignstatus  : out std_logic_vector(0 downto 0);
    rx_clkout                : out std_logic_vector(0 downto 0);
    rx_ctrldetect            : out std_logic_vector(3 downto 0);
    rx_dataout               : out std_logic_vector(31 downto 0);
    rx_disperr               : out std_logic_vector(3 downto 0);
    rx_errdetect             : out std_logic_vector(3 downto 0);
    rx_freqlocked            : out std_logic_vector(0 downto 0);
    rx_patterndetect         : out std_logic_vector(3 downto 0)
  );
  end component;

  component ip_stratixiv_hssi_gx_16b is
  generic
  (
    starting_channel_number   : natural := 0
  );
  port
  (
    cal_blk_clk   : in std_logic;
    pll_inclk   : in std_logic;
    pll_powerdown   : in std_logic_vector(0 downto 0);
    reconfig_clk    : in std_logic;
    reconfig_togxb    : in std_logic_vector(3 downto 0);
    rx_analogreset    : in std_logic_vector(0 downto 0);
    rx_datain   : in std_logic_vector(0 downto 0);
    rx_digitalreset   : in std_logic_vector(0 downto 0);
    tx_ctrlenable   : in std_logic_vector(1 downto 0);
    tx_datain   : in std_logic_vector(15 downto 0);
    tx_digitalreset   : in std_logic_vector(0 downto 0);
    pll_locked    : out std_logic_vector(0 downto 0);
    reconfig_fromgxb    : out std_logic_vector(16 downto 0);
    rx_clkout   : out std_logic_vector(0 downto 0);
    rx_ctrldetect   : out std_logic_vector(1 downto 0);
    rx_dataout    : out std_logic_vector(15 downto 0);
    rx_freqlocked   : out std_logic_vector(0 downto 0);
    tx_clkout   : out std_logic_vector(0 downto 0);
    tx_dataout    : out std_logic_vector(0 downto 0)
  );
  end component;

  component ip_stratixiv_hssi_tx_16b is
  port
  (
    cal_blk_clk   : in std_logic;
    pll_inclk   : in std_logic;
    pll_powerdown   : in std_logic_vector(0 downto 0);
    tx_ctrlenable   : in std_logic_vector(1 downto 0);
    tx_datain   : in std_logic_vector(15 downto 0);
    tx_digitalreset   : in std_logic_vector(0 downto 0);
    pll_locked    : out std_logic_vector(0 downto 0);
    tx_clkout   : out std_logic_vector(0 downto 0);
    tx_dataout    : out std_logic_vector(0 downto 0)
  );
  end component;

  component ip_stratixiv_hssi_rx_16b is
  generic
  (
    starting_channel_number   : natural := 0
  );
  port
  (
    cal_blk_clk   : in std_logic;
    reconfig_clk    : in std_logic;
    reconfig_togxb    : in std_logic_vector(3 downto 0);
    rx_analogreset    : in std_logic_vector(0 downto 0);
    rx_cruclk   : in std_logic_vector(0 downto 0) :=  (others => '0');
    rx_datain   : in std_logic_vector(0 downto 0);
    rx_digitalreset   : in std_logic_vector(0 downto 0);
    reconfig_fromgxb    : out std_logic_vector(16 downto 0);
    rx_clkout   : out std_logic_vector(0 downto 0);
    rx_ctrldetect   : out std_logic_vector(1 downto 0);
    rx_dataout    : out std_logic_vector(15 downto 0);
    rx_freqlocked   : out std_logic_vector(0 downto 0)
  );
  end component;

  component ip_stratixiv_gxb_reconfig_v91 is
  generic (
    g_nof_gx        : natural;
    g_fromgxb_bus_w : natural := 17;
    g_togxb_bus_w   : natural := 4
  );
  port (
    reconfig_clk     : in std_logic;
    reconfig_fromgxb : in std_logic_vector(g_nof_gx * g_fromgxb_bus_w - 1 downto 0);
    busy             : out std_logic;
    reconfig_togxb   : out std_logic_vector(g_togxb_bus_w - 1 downto 0)
  );
  end component;

  component ip_stratixiv_gxb_reconfig_v111 is
  generic (
    g_soft          : boolean := false;
    g_nof_gx        : natural;
    g_fromgxb_bus_w : natural := 17;
    g_togxb_bus_w   : natural := 4
  );
  port (
    reconfig_clk     : in std_logic;
    reconfig_fromgxb : in std_logic_vector(tech_ceil_div(g_nof_gx, 4) * g_fromgxb_bus_w - 1 downto 0);
    busy             : out std_logic;
    reconfig_togxb   : out std_logic_vector(g_togxb_bus_w - 1 downto 0)
  );
  end component;

  ------------------------------------------------------------------------------
  -- ip_arria10
  ------------------------------------------------------------------------------
end tech_transceiver_component_pkg;
