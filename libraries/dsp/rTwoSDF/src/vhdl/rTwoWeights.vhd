--------------------------------------------------------------------------------
--   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
--   Copyright (C) 2009-2010
--   ASTRON (Netherlands Institute for Radio Astronomy)
--   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
--   This file is part of the UniBoard software suite.
--   The file is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

-- Purpose: Get twiddles from ROM
-- Description:
--   The twiddles ROM is generated twiddlesPkg.vhd.
-- Remark:
-- . Default use g_lat=1 as for synthesis to improve fmax.
-- . Use g_lat=0 for no digital latency to show the pure functional behavior
--   of a pipelined FFT.
-- . When the pipelined FFT is used in a Wideband FFT configuration the
--   rTwoWeights unit compensates for this by estimating the virtual stage and
--   applying the twiddle-offset and the stage-offset.

library ieee, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use work.twiddlesPkg.all;

entity rTwoWeights is
  generic (
    g_stage          : natural := 4;  -- The stage number of the pft
    g_lat            : natural := 1;  -- latency 0 or 1
    g_twiddle_offset : natural := 0;  -- The twiddle offset: 0 for normal FFT. Other than 0 in wideband FFT
    g_stage_offset   : natural := 0  -- The Stage offset: 0 for normal FFT. Other than 0 in wideband FFT
  );
  port (
    clk       : in  std_logic;
    in_wAdr   : in  std_logic_vector;
    weight_re : out wTyp;
    weight_im : out wTyp
  );
end;

architecture rtl of rTwoWeights is
  constant c_virtual_stage : integer := g_stage + g_stage_offset;  -- Virtual stage based on the real stage and the stage_offset.
  constant c_nof_shifts    : integer := -1 * g_stage_offset;  -- Shift factor when fft is used in wfft configuration

  signal nxt_weight_re : wTyp;
  signal nxt_weight_im : wTyp;
  signal wAdr_shift    : std_logic_vector(c_virtual_stage-1 downto 1);
  signal wAdr_unshift  : std_logic_vector(c_virtual_stage-1 downto 1);
  signal wAdr_tw_offset: integer := 0;
begin
  -- Estimate the correct twiddle address.
  -- In case of a wfft configuration the address will be shifted and the twiddle offset will be added.
  wAdr_unshift   <= RESIZE_UVEC(in_wAdr, wAdr_unshift'length);
  wAdr_shift     <= SHIFT_UVEC(wAdr_unshift, c_nof_shifts) when in_wAdr'length > 0 else (others => '0');
  wAdr_tw_offset <= TO_UINT(wAdr_shift) + g_twiddle_offset when in_wAdr'length > 0 else g_twiddle_offset;

  -- functionality
  p_get_weights : process(wAdr_tw_offset)
  begin
    if c_virtual_stage = 1 then
      nxt_weight_re <= wRe(wMap(0, 1));
      nxt_weight_im <= wIm(wMap(0, 1));
    else
      nxt_weight_re <= wRe(wMap(wAdr_tw_offset, c_virtual_stage));
      nxt_weight_im <= wIm(wMap(wAdr_tw_offset, c_virtual_stage));
    end if;
  end process;

  -- latency
  no_reg : if g_lat = 0 generate
    weight_re <= nxt_weight_re;
    weight_im <= nxt_weight_im;
  end generate;

  gen_reg : if g_lat = 1 generate
    p_clk : process(clk)
    begin
      if rising_edge(clk) then
        weight_re <= nxt_weight_re;
        weight_im <= nxt_weight_im;
      end if;
    end process;
  end generate;

  assert g_lat <= 1
    report "rTwoWeights : g_lat must be 0 or 1"
    severity failure;
end rtl;
