-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
--  Test bench for dp_selector_arr
-- Description:
--  . Two data stream arrays are offered with g_nof_streams=4 to two DUTs. The
--    streams are distinguished by an offset in the data fields, defined by
--    c_pipe_data_offset
--  . One DUT selects one array and the other DUT selects the other array.
--  . The output of the DUTs are compared to the expected output.
--
-- Usage:
-- > as 10
-- > run -all
--
-- Remarks:

library IEEE, common_lib, mm_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_selector_arr is
  generic (
    -- specific
    g_in_dat_w               : natural := 32;
    g_in_nof_words           : natural := 1;
    g_nof_repeat             : natural := 20;
    g_pkt_len                : natural := 16;  -- must be a multiple of g_in_nof_words
    g_pkt_gap                : natural := 4
  );
end tb_dp_selector_arr;

architecture tb of tb_dp_selector_arr is
  constant c_nof_streams   : natural := 4;

  constant c_sync_period            : natural := 10;
  constant c_sync_offset            : natural := 7;

  constant c_data_max               : unsigned(g_in_dat_w - 1 downto 0) := (others => '1');
  constant c_data_init              : integer := -1;
  constant c_bsn_init               : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_err_init               : natural := 247;
  constant c_channel_init           : integer := 5;  -- fixed

  constant c_pipe_data_offset       : integer := 1000;

  signal tb_end                     : std_logic := '0';
  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';

  signal stimuli_en                 : std_logic := '1';
  signal stimuli_src_in             : t_dp_siso;
  signal stimuli_src_out            : t_dp_sosi;
  signal verify_sosi                : t_dp_sosi;

  signal ref_sosi_arr      : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal pipe_sosi_arr     : t_dp_sosi_arr(c_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_pipe_sosi_arr : t_dp_sosi_arr(c_nof_streams - 1 downto 0);
  signal out_ref_sosi_arr  : t_dp_sosi_arr(c_nof_streams - 1 downto 0);

  signal mm_mosi_pipe : t_mem_mosi;
  signal mm_mosi_ref  : t_mem_mosi;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------
  stimuli_en           <= '1';

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli_st : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
  begin
    -- Adjust initial sosi field values by -1 to compensate for auto increment
    v_sosi.bsn     := INCR_UVEC(c_bsn_init,                    -1);
    v_sosi.channel := INCR_UVEC(TO_DP_CHANNEL(c_channel_init), -1);
    v_sosi.data    := INCR_UVEC(TO_DP_DATA(c_data_init),       -1);
    v_sosi.err     := INCR_UVEC(TO_DP_ERROR(c_err_init),       -1);

    stimuli_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Generate c_nof_repeat packets
    for I in 0 to g_nof_repeat - 1 loop
      -- Auto increment v_sosi field values for this packet
      v_sosi.bsn     := INCR_UVEC(v_sosi.bsn, 1);
      v_sosi.sync    := sel_a_b((unsigned(v_sosi.bsn) mod c_sync_period) = c_sync_offset, '1', '0');  -- insert sync starting at BSN=c_sync_offset and with period c_sync_period
      v_sosi.channel := INCR_UVEC(v_sosi.channel, 1);
      v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len);
      v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
      v_sosi.err     := INCR_UVEC(v_sosi.err, 1);

      -- Send packet
      proc_dp_gen_block_data(g_in_dat_w, TO_UINT(v_sosi.data), g_pkt_len, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn, clk, stimuli_en, stimuli_src_in, stimuli_src_out);

      -- Insert optional gap between the packets
      proc_common_wait_some_cycles(clk, g_pkt_gap);
    end loop;

    -- Determine expected sosi field values after end of stimuli
    -- . e_qual
    v_sosi.bsn     := std_logic_vector( unsigned(c_bsn_init) + g_nof_repeat - 1);
    v_sosi.channel := TO_DP_CHANNEL(c_channel_init           + g_nof_repeat - 1);
    v_sosi.err     := TO_DP_ERROR(c_err_init                 + g_nof_repeat - 1);
    -- . account for g_pkt_len
    v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len - 1);
    v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_in_dat_w - 1 downto 0));  -- wrap when >= 2**g_in_dat_w
    verify_sosi    <= v_sosi;
    -- Signal end of stimuli
    proc_common_wait_some_cycles(clk, 100);
    tb_end <= '1';
    wait;
  end process;

  p_verify : process
  begin
    proc_common_wait_some_cycles(clk, g_nof_repeat * (g_pkt_gap + g_pkt_len));  -- Wait until end of simulation.
    proc_common_wait_some_cycles(clk, 50);  -- latency from stimuli to verify depends on the flow control, so wait sufficiently long for last packet to have passed through
    for I in 0 to c_nof_streams - 1 loop
      assert signed(verify_sosi.channel) = signed(out_ref_sosi_arr(I).channel)
        report "Unexpected channel from dut_ref output."
        severity ERROR;
      assert signed(verify_sosi.channel) = signed(out_pipe_sosi_arr(I).channel)
        report "Unexpected channel from dut_pipe output."
        severity ERROR;
      assert signed(verify_sosi.err) = signed(out_ref_sosi_arr(I).err)
        report "Unexpected err from dut_ref output."
        severity ERROR;
      assert signed(verify_sosi.err) = signed(out_pipe_sosi_arr(I).err)
        report "Unexpected err from dut_pipe output."
        severity ERROR;
      assert signed(verify_sosi.bsn) = signed(out_ref_sosi_arr(I).bsn)
        report "Unexpected bsn from dut_ref output."
        severity ERROR;
      assert signed(verify_sosi.bsn) = signed(out_pipe_sosi_arr(I).bsn)
        report "Unexpected bsn from dut_pipe output."
        severity ERROR;
      assert signed(verify_sosi.data) = signed(out_ref_sosi_arr(I).data)
        report "Unexpected data from dut_ref output."
        severity ERROR;
      assert signed(verify_sosi.data) + c_pipe_data_offset = signed(out_pipe_sosi_arr(I).data)
        report "Unexpected data from dut_pipe output."
        severity ERROR;
    end loop;
    wait;
  end process;

  stimuli_src_in <= c_dp_siso_rdy;

  gen_connect : for I in 0 to c_nof_streams - 1 generate
    ref_sosi_arr(i) <= stimuli_src_out;
  end generate;

  -- Add offset to pipe_sosi_arr data to differentiate it from ref_sosi_arr
  p_pipe_sosi: process(stimuli_src_out)
  begin
    for I in 0 to c_nof_streams - 1 loop
      pipe_sosi_arr(I) <= stimuli_src_out;
      pipe_sosi_arr(I).data <= RESIZE_DP_DATA(INCR_UVEC(stimuli_src_out.data, c_pipe_data_offset)(g_in_dat_w - 1 downto 0));
    end loop;
  end process;

  -- DUT that selects pipe_sosi_arr
  u_dut_pipe : entity work.dp_selector_arr
  generic map(
    g_nof_arr     => c_nof_streams,
    g_pipeline    => 1
  )
  port map(
    -- Memory-mapped clock domain
    mm_rst       => rst,
    mm_clk       => clk,

    reg_selector_mosi     => mm_mosi_pipe,
    reg_selector_miso     => OPEN,

    -- Streaming clock domain
    dp_rst       => rst,
    dp_clk       => clk,

    -- ST sinks
    pipe_sosi_arr  => pipe_sosi_arr,
    ref_sosi_arr   => ref_sosi_arr,
    -- ST source
    out_sosi_arr  => out_pipe_sosi_arr
  );

  -- DUT that selects ref_sosi_arr
  u_dut_ref : entity work.dp_selector_arr
  generic map(
    g_nof_arr     => c_nof_streams,
    g_pipeline    => 1
  )
  port map(
    -- Memory-mapped clock domain
    mm_rst       => rst,
    mm_clk       => clk,

    reg_selector_mosi     => mm_mosi_ref,
    reg_selector_miso     => OPEN,

    -- Streaming clock domain
    dp_rst       => rst,
    dp_clk       => clk,

    -- ST sinks
    pipe_sosi_arr  => pipe_sosi_arr,
    ref_sosi_arr   => ref_sosi_arr,
    -- ST source
    out_sosi_arr  => out_ref_sosi_arr
  );

  p_stim: process
  begin
     wait until rst = '0';
     proc_mem_mm_bus_wr(0, x"0", clk, mm_mosi_ref);  -- select ref_sosi_arr on dut_ref
     proc_mem_mm_bus_wr(0, x"1", clk, mm_mosi_pipe);  -- select pipe_sosi_arr on dut_pipe
     wait;
   end process;
end tb;
