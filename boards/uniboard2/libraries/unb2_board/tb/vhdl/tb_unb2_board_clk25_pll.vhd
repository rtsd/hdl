-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Simulate phase behaviour of PLL in normal mode
-- Description:
-- Usage:
-- > as 3
-- > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity tb_unb2_board_clk25_pll is
end tb_unb2_board_clk25_pll;

architecture tb of tb_unb2_board_clk25_pll is
  constant c_ext_clk_period  : time := 40 ns;  -- 25 MHz

  signal tb_end      : std_logic := '0';
  signal ext_clk     : std_logic := '0';
  signal ext_rst     : std_logic;
  signal c0_clk20    : std_logic;
  signal c1_clk50    : std_logic;
  signal c2_clk100   : std_logic;
  signal c3_clk125   : std_logic;
  signal pll_locked  : std_logic;
begin
  tb_end <= '0', '1' after c_ext_clk_period * 5000;

  ext_clk <= not ext_clk or tb_end after c_ext_clk_period / 2;
  ext_rst <= '1', '0' after c_ext_clk_period * 7;

  dut_0 : entity work.unb2_board_clk25_pll
  port map (
    arst      => ext_rst,
    clk25     => ext_clk,

    c0_clk20  => c0_clk20,
    c1_clk50  => c1_clk50,
    c2_clk100  => c2_clk100,
    c3_clk125  => c3_clk125,

    pll_locked => pll_locked
  );
end tb;
