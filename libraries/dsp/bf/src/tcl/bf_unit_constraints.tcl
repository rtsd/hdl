set_global_assignment -name PHYSICAL_SYNTHESIS_REGISTER_DUPLICATION ON
set_instance_assignment -name MAX_FANOUT 4 -to "*:u_bf_control|r.req_d"
set_global_assignment -name PHYSICAL_SYNTHESIS_ASYNCHRONOUS_SIGNAL_PIPELINING ON
set_instance_assignment -name PHYSICAL_SYNTHESIS_REGISTER_DUPLICATION ON -to "*:u_bf_control|r.addr_counter_d*"
