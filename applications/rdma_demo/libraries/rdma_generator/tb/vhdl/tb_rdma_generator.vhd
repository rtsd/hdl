-------------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author: R. vd Walle
-- Purpose: Test bench for rdma_generator_roce_tester using high bandwidth generic
-- configuration, e.g. g_nof_octet_output = 64 and g_nof_octet_generate = 64.
-- Description: Similar to the 1GbE testbench [1] for the eth tester but using generic
-- values for usage with high bandwidth, e.g. 10 / 100 GbE.
-- The g_nof_streams >= 1 are tested independently, using g_bg_ctrl_first for
-- BG in stream 0 and g_bg_ctrl_others for all other streams.
--
-- Usage:
-- > as 8
-- # * E.g. view sosi/data signals in dut/gen_streams/u_rx and u_tx
-- > run -a
--
-- References:
-- [1] https://support.astron.nl/confluence/display/L2M/L6+FWLIB+Design+Document%3A+ETH+tester+unit+for+1GbE

library IEEE, common_lib, dp_lib, diag_lib, technology_lib, tech_tse_lib, eth_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.dp_components_pkg.all;
use diag_lib.diag_pkg.all;
use eth_lib.eth_pkg.all;
use eth_lib.eth_tester_pkg.all;
use eth_lib.tb_eth_tester_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;

entity tb_rdma_generator is
  generic (
    g_tb_index           : natural := 0;  -- use to incremental delay logging from tb instances in tb_tb
    g_nof_sync           : natural := 2;  -- number of BG sync intervals to set c_run_time
    g_nof_streams        : natural := 1;
    g_nof_octet_output   : natural := 64;  -- maximum = 96 bytes as max dp.data field = 768 bits.
    g_nof_octet_generate : natural := 64;

    -- t_diag_block_gen_integer =
    --   sl:  enable
    --   sl:  enable_sync
    --   nat: samples_per_packet
    --   nat: blocks_per_sync
    --   nat: gapsize
    --   nat: mem_low_adrs
    --   nat: mem_high_adrs
    --   nat: bsn_init

    -- for first stream
    g_bg_ctrl_first      : t_diag_block_gen_integer := ('1', '1', 500, 3, 200, 0, c_diag_bg_mem_max_adr, 0);
    -- for other streams
    g_bg_ctrl_others     : t_diag_block_gen_integer := ('1', '1', 300, 3, 10, 0, c_diag_bg_mem_max_adr, 0)
  );
  port (
    tb_end : out std_logic
  );
end tb_rdma_generator;

architecture tb of tb_rdma_generator is
  -- use to distinguish logging from tb instances in tb_tb
  constant c_tb_str                : string := "tb-" & natural'image(g_tb_index) & " : ";

  constant mm_clk_period           : time := 10 ns;  -- 100 MHz
  constant c_nof_st_clk_per_s      : natural := 200 * 10**6;
  constant st_clk_period           : time :=  (10**9 / c_nof_st_clk_per_s) * 1 ns;  -- 5 ns, 200 MHz

  constant c_bg_block_len_first    : natural := ceil_div(g_bg_ctrl_first.samples_per_packet, g_nof_octet_generate);
  constant c_bg_block_len_others   : natural := ceil_div(g_bg_ctrl_others.samples_per_packet, g_nof_octet_generate);
  constant c_bg_block_len_max      : natural := largest(c_bg_block_len_first, c_bg_block_len_others);

  constant c_bg_slot_len_first     : natural := c_bg_block_len_first + g_bg_ctrl_first.gapsize;
  constant c_bg_slot_len_others    : natural := c_bg_block_len_others + g_bg_ctrl_others.gapsize;
  constant c_bg_slot_len_max       : natural := largest(c_bg_slot_len_first, c_bg_slot_len_others);

  constant c_eth_packet_len_first  : natural := func_eth_tester_eth_packet_length(c_bg_block_len_first);
  constant c_eth_packet_len_others : natural := func_eth_tester_eth_packet_length(c_bg_block_len_others);

  -- Use REAL to avoid NATURAL overflow in bps calculation

  constant c_bg_nof_bps_first      : real := real(c_bg_block_len_first * c_octet_w) * real(c_nof_st_clk_per_s) / real(c_bg_slot_len_first);
  constant c_bg_nof_bps_others     : real := real(c_bg_block_len_others * c_octet_w) * real(c_nof_st_clk_per_s) / real(c_bg_slot_len_others);
  constant c_bg_nof_bps_total      : real := c_bg_nof_bps_first + real(g_nof_streams - 1) * c_bg_nof_bps_others;

  constant c_bg_sync_period_first  : natural := c_bg_slot_len_first * g_bg_ctrl_first.blocks_per_sync;
  constant c_bg_sync_period_others : natural := c_bg_slot_len_others * g_bg_ctrl_others.blocks_per_sync;
  constant c_bg_sync_period_max    : natural := largest(c_bg_sync_period_first, c_bg_sync_period_others);

  constant c_run_time              : natural := g_nof_sync * c_bg_sync_period_max;
  constant c_nof_sync_first        : natural := c_run_time / c_bg_sync_period_first;
  constant c_nof_sync_others       : natural := c_run_time / c_bg_sync_period_others;

  -- Expected Tx --> Rx latency values obtained from a tb run
  constant c_tx_exp_latency          : natural := 0;
  constant c_rx_exp_latency_en       : boolean := c_bg_block_len_max >= 50;
  constant c_rx_exp_latency_st       : natural := 19;

  -- CRC is added by Tx TSE IP and removed by dp_offload_rx when g_remove_crc =
  -- g_loopback_eth = TRUE. Therefore internally only application payload
  -- (= block_len) octets are counted.
  constant c_nof_valid_per_packet_first   : natural := c_bg_block_len_first;
  constant c_nof_valid_per_packet_others  : natural := c_bg_block_len_others;

  constant c_total_count_nof_valid_per_sync_first  : natural := g_bg_ctrl_first.blocks_per_sync * c_nof_valid_per_packet_first;
  constant c_total_count_nof_valid_per_sync_others : natural := g_bg_ctrl_others.blocks_per_sync * c_nof_valid_per_packet_others;

  constant c_mon_nof_sop_first       : natural := g_bg_ctrl_first.blocks_per_sync;
  constant c_mon_nof_sop_others      : natural := g_bg_ctrl_others.blocks_per_sync;
  constant c_mon_nof_valid_first_tx  : natural := c_mon_nof_sop_first * ceil_div(c_bg_block_len_first * g_nof_octet_generate, g_nof_octet_output);
  constant c_mon_nof_valid_first_rx  : natural := c_mon_nof_sop_first * c_nof_valid_per_packet_first;
  constant c_mon_nof_valid_others_tx : natural := c_mon_nof_sop_others * ceil_div(c_bg_block_len_others * g_nof_octet_generate, g_nof_octet_output);
  constant c_mon_nof_valid_others_rx : natural := c_mon_nof_sop_others * c_nof_valid_per_packet_others;

  -- Use sim default src MAC, IP, UDP port from eth_tester_pkg.vhd and based on c_gn_index
  constant c_gn_index           : natural := 17;  -- global node index
  constant c_gn_eth_src_mac     : std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0) := c_eth_tester_eth_src_mac_47_16 & func_eth_tester_gn_index_to_mac_15_0(c_gn_index);
  constant c_gn_ip_src_addr     : std_logic_vector(c_network_ip_addr_w - 1 downto 0) := c_eth_tester_ip_src_addr_31_16 & func_eth_tester_gn_index_to_ip_15_0(c_gn_index);
  constant c_gn_udp_src_port    : std_logic_vector(c_network_udp_port_w - 1 downto 0) := c_eth_tester_udp_src_port_15_8 & TO_UVEC(c_gn_index, 8);

  -- Clocks and reset
  signal mm_rst              : std_logic := '1';
  signal mm_clk              : std_logic := '1';
  signal st_rst              : std_logic := '1';
  signal st_clk              : std_logic := '1';
  signal st_pps              : std_logic := '0';
  signal stimuli_end         : std_logic := '0';
  signal i_tb_end            : std_logic := '0';

  -- Use same bg_ctrl for all others streams, this provides sufficient test coverage
  signal bg_ctrl_arr         : t_diag_block_gen_integer_arr(g_nof_streams - 1 downto 0);

  signal tx_fifo_rd_emp_arr  : std_logic_vector(g_nof_streams - 1 downto 0);

  -- ETH UDP data path interface
  signal tx_udp_sosi_arr     : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
  signal tx_udp_siso_arr     : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);
  signal rx_udp_sosi_arr     : t_dp_sosi_arr(g_nof_streams - 1 downto 0);

  -- MM interface
  -- . Tx
  signal reg_bg_ctrl_copi                : t_mem_copi := c_mem_copi_rst;
  signal reg_bg_ctrl_cipo                : t_mem_cipo;
  signal reg_hdr_dat_copi                : t_mem_copi := c_mem_copi_rst;
  signal reg_hdr_dat_cipo                : t_mem_cipo;
  signal reg_bsn_monitor_v2_tx_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_tx_cipo      : t_mem_cipo;
  signal reg_strobe_total_count_tx_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_strobe_total_count_tx_cipo  : t_mem_cipo;
  signal reg_dp_split_copi               : t_mem_copi := c_mem_copi_rst;
  signal reg_dp_split_cipo               : t_mem_cipo;
  -- . Rx
  signal reg_bsn_monitor_v2_rx_copi      : t_mem_copi := c_mem_copi_rst;
  signal reg_bsn_monitor_v2_rx_cipo      : t_mem_cipo;
  signal reg_strobe_total_count_rx_copi  : t_mem_copi := c_mem_copi_rst;
  signal reg_strobe_total_count_rx_cipo  : t_mem_cipo;

  -- . reg_strobe_total_count
  signal tx_total_count_nof_packet_arr        : t_natural_arr(g_nof_streams - 1 downto 0);
  signal rx_total_count_nof_packet_arr        : t_natural_arr(g_nof_streams - 1 downto 0);
  signal exp_total_count_nof_packet_arr       : t_natural_arr(g_nof_streams - 1 downto 0);  -- same for both tx and rx

  signal rx_total_count_nof_valid_arr         : t_natural_arr(g_nof_streams - 1 downto 0);
  signal rx_exp_total_count_nof_valid_arr     : t_natural_arr(g_nof_streams - 1 downto 0);

  signal rx_total_count_nof_corrupted_arr     : t_natural_arr(g_nof_streams - 1 downto 0);
  signal rx_exp_total_count_nof_corrupted_arr : t_natural_arr(g_nof_streams - 1 downto 0);

  -- . reg_bsn_monitor_v2
  signal tx_mon_nof_sop_arr      : t_natural_arr(g_nof_streams - 1 downto 0);
  signal tx_mon_nof_valid_arr    : t_natural_arr(g_nof_streams - 1 downto 0);
  signal tx_mon_latency_arr      : t_natural_arr(g_nof_streams - 1 downto 0);
  signal rx_mon_nof_sop_arr      : t_natural_arr(g_nof_streams - 1 downto 0);
  signal rx_mon_nof_valid_arr    : t_natural_arr(g_nof_streams - 1 downto 0);
  signal rx_mon_latency_arr      : t_natural_arr(g_nof_streams - 1 downto 0);

  -- View in Wave window
  signal dbg_c_run_time                : natural := c_run_time;
  signal dbg_c_mon_nof_sop_first       : natural := c_mon_nof_sop_first;
  signal dbg_c_mon_nof_sop_others      : natural := c_mon_nof_sop_others;
  signal dbg_c_mon_nof_valid_first_tx  : natural := c_mon_nof_valid_first_tx;
  signal dbg_c_mon_nof_valid_first_rx  : natural := c_mon_nof_valid_first_rx;
  signal dbg_c_mon_nof_valid_others_tx : natural := c_mon_nof_valid_others_tx;
  signal dbg_c_mon_nof_valid_others_rx : natural := c_mon_nof_valid_others_rx;
begin
  tb_end <= i_tb_end;

  mm_clk <= (not mm_clk) or i_tb_end after mm_clk_period / 2;
  st_clk <= (not st_clk) or i_tb_end after st_clk_period / 2;
  mm_rst <= '1', '0' after mm_clk_period * 5;
  st_rst <= '1', '0' after st_clk_period * 5;

  -- Using
  --SIGNAL exp_total_count_nof_packet_arr : t_natural_arr(g_nof_streams-1 DOWNTO 0);
  --                         (g_nof_streams-1 DOWNTO 1 => g_nof_sync * g_bg_ctrl_others.blocks_per_sync,
  --                                                 0 => g_nof_sync * g_bg_ctrl_first.blocks_per_sync);
  -- yields verror 1074, verror 1048, therefor use p_init instead, and
  -- therefor use bg_ctrl_arr instead of c_bg_ctrl_arr.
  p_init : process
  begin
    bg_ctrl_arr    <= (others => g_bg_ctrl_others);
    bg_ctrl_arr(0) <= g_bg_ctrl_first;

    exp_total_count_nof_packet_arr    <= (others => c_nof_sync_others * g_bg_ctrl_others.blocks_per_sync);
    exp_total_count_nof_packet_arr(0) <= c_nof_sync_first * g_bg_ctrl_first.blocks_per_sync;

    rx_exp_total_count_nof_valid_arr    <= (others => c_nof_sync_others * c_total_count_nof_valid_per_sync_others);
    rx_exp_total_count_nof_valid_arr(0) <= c_nof_sync_first * c_total_count_nof_valid_per_sync_first;

    rx_exp_total_count_nof_corrupted_arr <= (others => 0);  -- default no Rx errors
    wait;
  end process;

  -----------------------------------------------------------------------------
  -- MM control and monitoring
  -----------------------------------------------------------------------------
  p_mm : process
    variable v_value        : natural;
    variable v_offset       : natural;
    variable v_udp_dst_port : natural;
  begin
    i_tb_end <= '0';

    proc_common_wait_until_low(mm_clk, mm_rst);
    proc_common_wait_some_cycles(mm_clk, 10);

    ---------------------------------------------------------------------------
    -- Rx UDP offload port
    ---------------------------------------------------------------------------
    v_udp_dst_port := TO_UINT(c_eth_tester_udp_dst_port);

    for I in g_nof_streams - 1 downto 0 loop
      v_offset := I * c_eth_tester_reg_hdr_dat_addr_span;
      -- Set destination MAC/IP/UDP port in tx header, increment udp_dst_port per stream
      -- The MM addresses follow from byte address_offset // 4 in eth.peripheral.yaml
      proc_mem_mm_bus_wr(v_offset + 16#7#, v_udp_dst_port + I, mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
      -- use signed to fit 32 b in INTEGER
      proc_mem_mm_bus_wr(v_offset + 16#10#, TO_SINT(c_eth_tester_ip_dst_addr),
          mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
      proc_mem_mm_bus_wr(v_offset + 16#18#, TO_SINT(c_eth_tester_eth_dst_mac(31 downto 0)),
          mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
      proc_mem_mm_bus_wr(v_offset + 16#19#, TO_UINT(c_eth_tester_eth_dst_mac(47 downto 32)),
          mm_clk, reg_hdr_dat_cipo, reg_hdr_dat_copi);
    end loop;

    ---------------------------------------------------------------------------
    -- Stimuli
    ---------------------------------------------------------------------------
    for I in g_nof_streams - 1 downto 0 loop
      v_offset := I * c_diag_bg_reg_adr_span;
      -- Set number of octets per packet for dp_split
      proc_mem_mm_bus_wr(I * 2, bg_ctrl_arr(I).samples_per_packet, mm_clk, reg_dp_split_copi);

      -- Prepare the BG
      proc_mem_mm_bus_wr(v_offset + 1, ceil_div(bg_ctrl_arr(I).samples_per_packet, g_nof_octet_generate),
          mm_clk, reg_bg_ctrl_copi);
      proc_mem_mm_bus_wr(v_offset + 2, bg_ctrl_arr(I).blocks_per_sync,    mm_clk, reg_bg_ctrl_copi);
      proc_mem_mm_bus_wr(v_offset + 3, bg_ctrl_arr(I).gapsize,            mm_clk, reg_bg_ctrl_copi);
      proc_mem_mm_bus_wr(v_offset + 4, bg_ctrl_arr(I).mem_low_adrs,       mm_clk, reg_bg_ctrl_copi);
      proc_mem_mm_bus_wr(v_offset + 5, bg_ctrl_arr(I).mem_high_adrs,      mm_clk, reg_bg_ctrl_copi);
      proc_mem_mm_bus_wr(v_offset + 6, bg_ctrl_arr(I).bsn_init,           mm_clk, reg_bg_ctrl_copi);  -- low part
      proc_mem_mm_bus_wr(v_offset + 7, 0,                                 mm_clk, reg_bg_ctrl_copi);  -- high part
      -- Enable the BG at st_pps pulse.
      proc_mem_mm_bus_wr(v_offset + 0, 3, mm_clk, reg_bg_ctrl_copi);
    end loop;
    proc_common_wait_some_cycles(mm_clk, 10);
    -- Issue an st_pps pulse to start the enabled BG
    proc_common_gen_pulse(st_clk, st_pps);

    -- Run test
    proc_common_wait_some_cycles(st_clk, c_run_time);

    -- Disable the BG
    for I in g_nof_streams - 1 downto 0 loop
      v_offset := I * c_diag_bg_reg_adr_span;
      -- Disable the other BG
      proc_mem_mm_bus_wr(v_offset + 0, 0, mm_clk, reg_bg_ctrl_copi);
    end loop;

    -- Wait until Tx FIFOs have emptied for all streams
    while unsigned(tx_fifo_rd_emp_arr(g_nof_streams - 1 downto 0)) /= 2**g_nof_streams - 1 loop
      proc_common_wait_some_cycles(st_clk, 1);
    end loop;
    proc_common_wait_some_cycles(st_clk, c_bg_sync_period_max);
    stimuli_end <= '1';

    -- Delay logging between different tb instances
    proc_common_wait_some_cycles(st_clk, g_tb_index * 100);

    -- Print logging
    print_str("");  -- log empty line between tb results
    for I in g_nof_streams - 1 downto 0 loop
      if I = 0 then
        print_str(c_tb_str &
            "ETH bit rate (" & natural'image(I) & ") :" &
            " c_bg_nof_bps_first = " & real'image(c_bg_nof_bps_first) & " bps");
      else
        print_str(c_tb_str &
            "ETH bit rate (" & natural'image(I) & ") :" &
            " c_bg_nof_bps_others = " & real'image(c_bg_nof_bps_others) & " bps");
      end if;
    end loop;
    if g_nof_streams > 1 then
        print_str(c_tb_str &
            "ETH bit rate total :" &
            " c_bg_nof_bps_total = " & real'image(c_bg_nof_bps_total) & " bps");
    end if;

    -------------------------------------------------------------------------
    -- Verification: Total counts
    -------------------------------------------------------------------------
    for I in g_nof_streams - 1 downto 0 loop
      -- . read low part, ignore high part (= 0) of two word total counts
      v_offset := I * c_dp_strobe_total_count_reg_adr_span;
      -- Tx total nof packets
      proc_mem_mm_bus_rd(v_offset + 0, mm_clk, reg_strobe_total_count_tx_cipo, reg_strobe_total_count_tx_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      tx_total_count_nof_packet_arr(I) <= TO_UINT(reg_strobe_total_count_tx_cipo.rddata(c_word_w - 1 downto 0));
      -- Rx total nof packets
      proc_mem_mm_bus_rd(v_offset + 0, mm_clk, reg_strobe_total_count_rx_cipo, reg_strobe_total_count_rx_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      rx_total_count_nof_packet_arr(I) <= TO_UINT(reg_strobe_total_count_rx_cipo.rddata(c_word_w - 1 downto 0));
      -- Rx total nof valids
      proc_mem_mm_bus_rd(v_offset + 2, mm_clk, reg_strobe_total_count_rx_cipo, reg_strobe_total_count_rx_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      rx_total_count_nof_valid_arr(I) <= TO_UINT(reg_strobe_total_count_rx_cipo.rddata(c_word_w - 1 downto 0));
      -- Rx total nof corrupted
      proc_mem_mm_bus_rd(v_offset + 4, mm_clk, reg_strobe_total_count_rx_cipo, reg_strobe_total_count_rx_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      rx_total_count_nof_corrupted_arr(I) <= TO_UINT(reg_strobe_total_count_rx_cipo.rddata(c_word_w - 1 downto 0));
      proc_common_wait_some_cycles(mm_clk, 1);

      -- Print logging
      print_str(c_tb_str &
          "Tx total counts monitor(" & natural'image(I) & ") :" &
          " nof_packet = " & natural'image(tx_total_count_nof_packet_arr(I)));

      print_str(c_tb_str &
          "Rx total counts monitor(" & natural'image(I) & ") :" &
          " nof_packet = " & natural'image(rx_total_count_nof_packet_arr(I)) &
          ", nof_valid = " & natural'image(rx_total_count_nof_valid_arr(I)) &
          ", nof_corrupted = " & natural'image(rx_total_count_nof_corrupted_arr(I)));

      -- Verify, only log when wrong
      if c_bg_nof_bps_total < 10.0**9 then
        assert tx_total_count_nof_packet_arr(I) = exp_total_count_nof_packet_arr(I)
          report c_tb_str &
                 "Wrong Tx total nof packets count(" & natural'image(I) &
                 "), Tx count = " & natural'image(tx_total_count_nof_packet_arr(I)) &
                 " /= " & natural'image(exp_total_count_nof_packet_arr(I)) &
                 " = Expected count"
          severity ERROR;

        assert rx_total_count_nof_packet_arr(I) = exp_total_count_nof_packet_arr(I)
          report c_tb_str &
                 "Wrong Rx total nof packets count(" & natural'image(I) &
                 "), Rx count = " & natural'image(rx_total_count_nof_packet_arr(I)) &
                 " /= " & natural'image(exp_total_count_nof_packet_arr(I)) &
                 " = Expected count"
          severity ERROR;

        assert rx_total_count_nof_valid_arr(I) = rx_exp_total_count_nof_valid_arr(I)
          report c_tb_str &
                 "Wrong Rx total nof valids count(" & natural'image(I) &
                 "), Rx count = " & natural'image(rx_total_count_nof_valid_arr(I)) &
                 " /= " & natural'image(rx_exp_total_count_nof_valid_arr(I)) &
                 " = Expected count"
          severity ERROR;

        assert rx_total_count_nof_corrupted_arr(I) = rx_exp_total_count_nof_corrupted_arr(I)
          report c_tb_str &
                 "Wrong Rx total nof corrupted count(" & natural'image(I) &
                 "), Rx count = " & natural'image(rx_total_count_nof_corrupted_arr(I)) &
                 " /= " & natural'image(rx_exp_total_count_nof_corrupted_arr(I)) &
                 " = Expected count"
          severity ERROR;
      else
        -- Verify that Tx total nof packets = Rx total nof packets, also when
        -- BG experiences siso.xon block level flow control, to stay below
        -- 1 Gbps of the 1GbE link rate.
        assert tx_total_count_nof_packet_arr(I) = rx_total_count_nof_packet_arr(I)
          report c_tb_str &
                 "Wrong Tx-Rx total nof packets count(" & natural'image(I) &
                 "), Tx count = " & natural'image(tx_total_count_nof_packet_arr(I)) &
                 " /= " & natural'image(rx_total_count_nof_packet_arr(I)) &
                 " = Rx count"
          severity ERROR;
      end if;
    end loop;

    -------------------------------------------------------------------------
    -- Verification: BSN monitors (yield same values in every sync interval)
    -------------------------------------------------------------------------
    for I in g_nof_streams - 1 downto 0 loop
      v_offset := I * c_dp_bsn_monitor_v2_reg_adr_span;
      -- 3 = nof_sop
      -- 4 = nof_valid
      -- 6 = latency
      -- . Tx
      proc_mem_mm_bus_rd(v_offset + 3, mm_clk, reg_bsn_monitor_v2_tx_cipo, reg_bsn_monitor_v2_tx_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      tx_mon_nof_sop_arr(I) <= TO_UINT(reg_bsn_monitor_v2_tx_cipo.rddata(c_word_w - 1 downto 0));
      proc_mem_mm_bus_rd(v_offset + 4, mm_clk, reg_bsn_monitor_v2_tx_cipo, reg_bsn_monitor_v2_tx_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      tx_mon_nof_valid_arr(I) <= TO_UINT(reg_bsn_monitor_v2_tx_cipo.rddata(c_word_w - 1 downto 0));
      proc_mem_mm_bus_rd(v_offset + 6, mm_clk, reg_bsn_monitor_v2_tx_cipo, reg_bsn_monitor_v2_tx_copi);
      proc_mem_mm_bus_rd_latency(1, mm_clk);
      tx_mon_latency_arr(I) <= TO_UINT(reg_bsn_monitor_v2_tx_cipo.rddata(c_word_w - 1 downto 0));

      -- Print logging
      print_str(c_tb_str &
          "Tx BSN monitor(" & natural'image(I) & ") :" &
          " nof_sop = " & natural'image(tx_mon_nof_sop_arr(I)) &
          ", nof_valid = " & natural'image(tx_mon_nof_valid_arr(I)) &
          ", latency = " & natural'image(tx_mon_latency_arr(I)));

      if c_bg_nof_bps_total < 10.0**9 then
        -- Verify BSN monitors only when the BG sync interval is stable, so
        -- the ETH data rate < 1 Gbps and no BG block flow control.
        -- Verify, only log when wrong
        if I = 0 then
          assert tx_mon_nof_sop_arr(I) = c_mon_nof_sop_first
            report c_tb_str & "Wrong tx nof_sop for stream (" & natural'image(I) & ")"
            severity ERROR;
          assert tx_mon_nof_valid_arr(I) = c_mon_nof_valid_first_tx
            report c_tb_str & "Wrong tx nof_valid for stream (" & natural'image(I) & ")"
            severity ERROR;
        else
          assert tx_mon_nof_sop_arr(I) = c_mon_nof_sop_others
            report c_tb_str & "Wrong tx nof_sop for stream (" & natural'image(I) & ")"
            severity ERROR;
          assert tx_mon_nof_valid_arr(I) = c_mon_nof_valid_others_tx
            report c_tb_str & "Wrong tx nof_valid for stream (" & natural'image(I) & ")"
            severity ERROR;
        end if;
        assert tx_mon_latency_arr(I) = c_tx_exp_latency
          report c_tb_str & "Wrong tx latency for stream (" & natural'image(I) & ")"
          severity ERROR;

        -- For short block lengths the Rx latency appears to become less, the
        -- exact Rx latency is therefore hard to predetermine. The actual
        -- latency is not critical, therefore it is sufficient to only very
        -- the latency when it is more or less fixed.
        if c_rx_exp_latency_en then
          assert almost_equal(rx_mon_latency_arr(I), c_rx_exp_latency_st, 10)
            report c_tb_str & "Wrong rx latency using st interface (" & natural'image(I) & ")"
            severity ERROR;
        end if;
      end if;
    end loop;

    -------------------------------------------------------------------------
    -- End of test
    -------------------------------------------------------------------------
    proc_common_wait_some_cycles(mm_clk, 100);
    i_tb_end <= '1';
    wait;
  end process;

  dut : entity work.rdma_generator_roce_tester
  port map (
    -- Clocks and reset
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,
    st_rst             => st_rst,
    st_clk             => st_clk,
    st_pps             => st_pps,

    -- UDP transmit interface
    eth_src_mac        => c_gn_eth_src_mac,
    ip_src_addr        => c_gn_ip_src_addr,
    udp_src_port       => c_gn_udp_src_port,
    tx_fifo_rd_emp_arr => tx_fifo_rd_emp_arr,

    tx_udp_sosi_arr    => tx_udp_sosi_arr,
    tx_udp_siso_arr    => tx_udp_siso_arr,

    -- UDP receive interface
    rx_udp_sosi_arr    => rx_udp_sosi_arr,

    -- Memory Mapped Slaves (one per stream)
    -- . Tx
    reg_bg_ctrl_copi               => reg_bg_ctrl_copi,
    reg_bg_ctrl_cipo               => reg_bg_ctrl_cipo,
    reg_hdr_dat_copi               => reg_hdr_dat_copi,
    reg_hdr_dat_cipo               => reg_hdr_dat_cipo,
    reg_bsn_monitor_v2_tx_copi     => reg_bsn_monitor_v2_tx_copi,
    reg_bsn_monitor_v2_tx_cipo     => reg_bsn_monitor_v2_tx_cipo,
    reg_strobe_total_count_tx_copi => reg_strobe_total_count_tx_copi,
    reg_strobe_total_count_tx_cipo => reg_strobe_total_count_tx_cipo,
    reg_dp_split_copi              => reg_dp_split_copi,
    reg_dp_split_cipo              => reg_dp_split_cipo,
    -- . Rx
    reg_bsn_monitor_v2_rx_copi     => reg_bsn_monitor_v2_rx_copi,
    reg_bsn_monitor_v2_rx_cipo     => reg_bsn_monitor_v2_rx_cipo,
    reg_strobe_total_count_rx_copi => reg_strobe_total_count_rx_copi,
    reg_strobe_total_count_rx_cipo => reg_strobe_total_count_rx_cipo
  );

    -- Loop back at streaming sosi level
    rx_udp_sosi_arr <= tx_udp_sosi_arr;
end tb;
