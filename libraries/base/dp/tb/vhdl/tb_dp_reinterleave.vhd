-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use work.dp_stream_pkg.all;
use common_lib.common_pkg.all;
use common_lib.tb_common_pkg.all;
use work.tb_dp_pkg.all;

-- Purpose: Test bench to check reinterleave function on DP level
-- Usage:
--   > as 6
--   > run 2us
-- Remark:
--   This TB is only used to visually inspect the wave window to see
--   if all functions well on DP level. The actual reinterleaving itself
--   is verified more thoroughly in tb_common_reinterleave.

entity tb_dp_reinterleave is
  generic (
    g_dat_w          : natural := 16;
    g_nof_in         : natural := 4;
    g_deint_block_size  : natural := 2;
    g_nof_out        : natural := 4;
    g_inter_block_size : natural := 2;
    g_use_complex    : boolean := true;
    g_align_out  : boolean := true
 );
end;

architecture rtl of tb_dp_reinterleave is
  type t_dat_arr is array (integer range <>) of std_logic_vector(g_dat_w - 1 downto 0);
  type t_val_arr is array (integer range <>) of std_logic;

  -----------------------------------------------------------------------------
  -- Standard TB clocking, RST and control
  -----------------------------------------------------------------------------
  constant c_clk_period : time := 10 ns;

  signal clk            : std_logic := '1';
  signal rst            : std_logic;
  signal tb_end         : std_logic := '0';

  signal cnt_ena        : std_logic;

  -----------------------------------------------------------------------------
  --I/O streams
  -----------------------------------------------------------------------------
  constant c_input_val   : natural := 192;
  constant c_input_inval : natural := 64;

  signal snk_in_arr  : t_dp_sosi_arr(g_nof_in - 1 downto 0);
  signal snk_out_arr : t_dp_siso_arr(g_nof_in - 1 downto 0);

  signal src_out_arr : t_dp_sosi_arr(g_nof_out - 1 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Standard TB clocking, RST and control
  -----------------------------------------------------------------------------
  clk    <= not clk or tb_end after c_clk_period / 2;
  rst    <= '1', '0' after 3 * c_clk_period;

  -----------------------------------------------------------------------------
  -- Generate g_nof_in test data streams with counter data
  -----------------------------------------------------------------------------
  cnt_ena <= '0', '1' after 20 * c_clk_period;

  gen_cnt_dat : for i in 0 to g_nof_in - 1 generate
    p_stimuli : process
      variable v_bsn : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := (others => '0');
    begin
      while true loop
        wait until rising_edge(clk);
        proc_dp_gen_block_data(1, sel_a_b(g_use_complex, false, true), g_dat_w, g_dat_w, 0, 0, 0, c_input_val, 0, 0, '1', v_bsn, clk, cnt_ena, snk_out_arr(i), snk_in_arr(i));
        proc_common_wait_some_cycles(clk, c_input_inval);
        v_bsn := INCR_UVEC(v_bsn, 1);
      end loop;
     end process;

    -----------------------------------------------------------------------------
    -- The I/O of common_reinterleave and its lower level components operate
    -- on the same data width share the same clock, so if g_nof_in>g_nof_out,
    -- lower the effective input data rate accordingly by introducing gaps.
    -----------------------------------------------------------------------------
    p_cnt_dat_gaps : process
    begin
      snk_out_arr(i).xon <= '1';
      snk_out_arr(i).ready  <= '0';
      wait for c_clk_period * (ceil_div(g_nof_in, g_nof_out) - 1);
      snk_out_arr(i).ready  <= '1';
      wait for c_clk_period;
    end process;

  end generate;

  -----------------------------------------------------------------------------
  -- DUT
  -----------------------------------------------------------------------------
  u_reinterleave : entity work.dp_reinterleave
  generic map (
    g_nof_in         => g_nof_in,
    g_deint_block_size  => g_deint_block_size,
    g_nof_out        => g_nof_out,
    g_inter_block_size => g_inter_block_size,
    g_dat_w          => g_dat_w,
    g_use_complex    => g_use_complex,
    g_align_out => g_align_out
  )
  port map (
    rst        => rst,
    clk        => clk,

    snk_in_arr  => snk_in_arr,
    src_out_arr => src_out_arr
  );
end rtl;
