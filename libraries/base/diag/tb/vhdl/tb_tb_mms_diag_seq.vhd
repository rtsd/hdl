-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, dp_lib;
use IEEE.std_logic_1164.all;
use dp_lib.tb_dp_pkg.all;

entity tb_tb_mms_diag_seq is
end tb_tb_mms_diag_seq;

architecture tb of tb_tb_mms_diag_seq is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- g_flow_control_verify  : t_dp_flow_control_enum := e_active;  -- always active or random flow control
  -- g_nof_streams          : NATURAL := 2;
  -- g_use_steps            : BOOLEAN := FALSE;
  -- g_mm_broadcast_tx      : BOOLEAN := TRUE;
  -- g_data_w               : NATURAL := 40;  -- >= g_seq_dat_w
  -- g_seq_dat_w            : NATURAL := 32

  u_1_stream_mm_equal_dat_w    : entity work.tb_mms_diag_seq generic map (e_active, 1, false, false, 32, 32);
  u_1_stream_mm_random         : entity work.tb_mms_diag_seq generic map (e_random, 1, false, false, 40, 15);
  u_2_streams_mm_broadcast     : entity work.tb_mms_diag_seq generic map (e_active, 2, false, true,  40, 32);
  u_2_streams_mm_multiplex     : entity work.tb_mms_diag_seq generic map (e_active, 2, false, false, 40, 32);

  u_1_stream_mm_steps_w        : entity work.tb_mms_diag_seq generic map (e_random, 1, true,  false, 40, 15);
end tb;
