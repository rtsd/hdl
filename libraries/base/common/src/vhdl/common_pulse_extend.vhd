-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose: Extend the active high time of a pulse
-- Description:
--   Extend an input pulse p_in to an output pulse ep_out that is delayed by 1
--   clock cycle and lasts for 2**g_extend_w number of clock cycles longer
--   after that p_in went low.

entity common_pulse_extend is
  generic (
    g_rst_level    : std_logic := '0';
    g_p_in_level   : std_logic := '1';
    g_ep_out_level : std_logic := '1';
    g_extend_w     : natural := 1;
    g_nof_cycles   : natural := 1  -- 1 = use maximum determined by g_extend_w
  );
  port (
    rst     : in  std_logic := '0';
    clk     : in  std_logic;
    clken   : in  std_logic := '1';
    p_in    : in  std_logic;
    ep_out  : out std_logic
  );
end common_pulse_extend;

architecture rtl of common_pulse_extend is
  constant c_cnt_w  : natural := sel_a_b(g_nof_cycles > 1, ceil_log2(g_nof_cycles), g_extend_w);

  signal cnt        : std_logic_vector(c_cnt_w - 1 downto 0) := (others => '0');
  signal nxt_cnt    : std_logic_vector(cnt'range);
  signal cnt_is_0   : std_logic;
  signal i_ep_out   : std_logic := g_rst_level;
  signal nxt_ep_out : std_logic;
begin
  -- Extend ep_out active for 2**g_extend_w cycles longer than p_in active
  -- Inactive p_in for less than 2**g_extend_w cycles will get lost in ep_out

  ep_out <= i_ep_out;

  registers_proc : process (clk, rst)
  begin
    if rst = '1' then
      cnt      <= (others => '0');
      i_ep_out <= g_rst_level;
    elsif rising_edge(clk) then
      if clken = '1' then
        cnt      <= nxt_cnt;
        i_ep_out <= nxt_ep_out;
      end if;
    end if;
  end process;

  nxt_ep_out <= g_ep_out_level when p_in = g_p_in_level or cnt_is_0 = '0' else not g_ep_out_level;

  cnt_is_0 <= '1' when unsigned(cnt) = 0 else '0';

  p_cnt : process(p_in, cnt_is_0, cnt)
  begin
    nxt_cnt <= (others => '0');
    if p_in = g_p_in_level then
      nxt_cnt <= std_logic_vector(to_unsigned(1, cnt'length));
    elsif cnt_is_0 = '0' then
      nxt_cnt <= std_logic_vector(unsigned(cnt) + 1);
      if g_nof_cycles > 1 and unsigned(cnt) = g_nof_cycles - 1 then
        nxt_cnt <= (others => '0');
      end if;
    end if;
  end process;
end architecture;
