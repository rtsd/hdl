--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

-- Purpose:
-- Description:

entity corr_unfolder is
  generic (
    g_nof_inputs  : natural;  -- Number of inputs
    g_nof_unfolds : natural := 0  -- Number of times to unfold
  );
  port (
    rst            : in  std_logic;
    clk            : in  std_logic;

    snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

    src_out_arr    : out t_dp_sosi_arr(g_nof_inputs * pow2(g_nof_unfolds) - 1 downto 0)
  );
end corr_unfolder;

architecture str of corr_unfolder is
  component corr_unfolder is
    generic (
      g_nof_inputs  : natural;
      g_nof_unfolds : natural := 0
    );
    port (
      rst            : in  std_logic;
      clk            : in  std_logic;

      snk_in_arr     : in  t_dp_sosi_arr(g_nof_inputs - 1 downto 0);

      src_out_arr    : out t_dp_sosi_arr(g_nof_inputs * pow2(g_nof_unfolds) - 1 downto 0)
    );
  end component;

  constant c_nof_demuxes : natural := g_nof_inputs;
  constant c_nof_outputs : natural := g_nof_inputs * pow2(g_nof_unfolds);

  signal output_sel_arr           : std_logic_vector(c_nof_demuxes - 1 downto 0);
  signal nxt_output_sel_arr       : std_logic_vector(c_nof_demuxes - 1 downto 0);

  signal demux_src_out_2arr_2     : t_dp_sosi_2arr_2(c_nof_demuxes - 1 downto 0);
  signal nxt_demux_src_out_2arr_2 : t_dp_sosi_2arr_2(c_nof_demuxes - 1 downto 0);
  signal demux_src_out_arr        : t_dp_sosi_arr(2 * c_nof_demuxes - 1 downto 0);

  signal dp_pipeline_snk_in_arr   : t_dp_sosi_arr(c_nof_outputs - 1 downto 0);
begin
  gen_arch: if g_nof_unfolds /= 0 generate
    -----------------------------------------------------------------------------
    -- Simple 2-output demux logic
    -----------------------------------------------------------------------------
    gen_demux_comb: for i in 0 to c_nof_demuxes - 1 generate
      nxt_output_sel_arr(i) <= not output_sel_arr(i) when snk_in_arr(i).valid = '1' else output_sel_arr(i);

      nxt_demux_src_out_2arr_2(i)(0) <= snk_in_arr(i) when output_sel_arr(i) = '0' and snk_in_arr(i).valid = '1' else c_dp_sosi_rst;
      nxt_demux_src_out_2arr_2(i)(1) <= snk_in_arr(i) when output_sel_arr(i) = '1' and snk_in_arr(i).valid = '1' else c_dp_sosi_rst;
    end generate;

    -----------------------------------------------------------------------------
    -- Wire the 2D demux output array to 1D array to match entity I/O type
    -----------------------------------------------------------------------------
    gen_demux_inputs_0: for i in 0 to c_nof_demuxes - 1 generate
       demux_src_out_arr(2 * i)   <= demux_src_out_2arr_2(i)(0);
       demux_src_out_arr(2 * i + 1) <= demux_src_out_2arr_2(i)(1);
    end generate;

    -----------------------------------------------------------------------------
    -- If g_nof_unfolds=1, this is the last stage. Otherwise, add a stage.
    -----------------------------------------------------------------------------
    gen_corr_folder: if g_nof_unfolds > 1 generate
      u_corr_folder : corr_unfolder
      generic map (
        g_nof_inputs  => c_nof_demuxes * 2,  -- Next stage has all our demux outputs as inputs
        g_nof_unfolds => g_nof_unfolds - 1
      )
      port map (
        rst         => rst,
        clk         => clk,

        snk_in_arr  => demux_src_out_arr,
        src_out_arr => src_out_arr
      );
    end generate;

    gen_src_out_arr: if g_nof_unfolds = 1 generate
      dp_pipeline_snk_in_arr <= demux_src_out_arr;

      -----------------------------------------------------------------------------
      -- Pipeline to re-align the unfolder output
      -----------------------------------------------------------------------------
      gen_dp_pipeline : for i in 0 to c_nof_outputs - 1 generate
        u_dp_pipeline : entity dp_lib.dp_pipeline
        generic map (
          g_pipeline => 0 + (pow2(g_nof_unfolds) - i rem pow2(g_nof_unfolds) - 1)
        )
        port map (
          rst         => rst,
          clk         => clk,

          snk_in      => dp_pipeline_snk_in_arr(i),
          src_out     => src_out_arr(i)
        );
      end generate;
    end generate;

    -----------------------------------------------------------------------------
    -- Registers
    -----------------------------------------------------------------------------
    p_clk: process(clk, rst)
    begin
      if rst = '1' then
        demux_src_out_2arr_2 <= (others => (others => c_dp_sosi_rst));
        output_sel_arr       <= (others => '0');
      elsif rising_edge(clk) then
        demux_src_out_2arr_2 <= nxt_demux_src_out_2arr_2;
        output_sel_arr       <= nxt_output_sel_arr;
      end if;
    end process;
  end generate;

  -----------------------------------------------------------------------------
  -- Wire output to input if g_nof_unfolds=0
  -----------------------------------------------------------------------------
  gen_wire_out_to_in: if g_nof_unfolds = 0 generate
    src_out_arr <= snk_in_arr;
  end generate;
end str;
