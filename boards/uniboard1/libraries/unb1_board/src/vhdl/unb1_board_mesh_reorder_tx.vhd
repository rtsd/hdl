-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Compensate for the mesh reorder between FN and BN on UniBoard
-- Description: See unb1_board_mesh_reorder_bidir.vhd
--   This unb1_board_mesh_reorder_tx performs mesh reordering for user transmit,
--   so for the tx_usr_sosi and the rx_usr_siso. Note that these belong to
--   different streams. The tx_usr_sosi carries the user Tx data output stream
--   and the rx_usr_siso belongs to the user Rx data stream and carries the
--   output flow control for the Rx data stream.
-- Remark:
-- . Indexing for *_2arr = (node id 0,1,2,3)(tr lane 3,2,1,0)

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.unb1_board_pkg.all;

entity unb1_board_mesh_reorder_tx is
  generic (
    g_node_type : t_e_unb1_board_node := e_any;
    g_reorder   : boolean := true
  );
  port (
    chip_id          : in  std_logic_vector(c_unb1_board_aux.chip_id_w - 1 downto 0);  -- [2:0]
    clk              : in  std_logic;
    tx_usr_sosi_2arr : in  t_unb1_board_mesh_sosi_2arr;  -- _2arr = (node id 0,1,2,3)(tr lane 3,2,1,0)
    rx_usr_siso_2arr : in  t_unb1_board_mesh_siso_2arr := c_unb1_board_mesh_siso_2arr_rst;
    tx_phy_sosi_2arr : out t_unb1_board_mesh_sosi_2arr;
    rx_phy_siso_2arr : out t_unb1_board_mesh_siso_2arr
  );
end unb1_board_mesh_reorder_tx;

architecture rtl of unb1_board_mesh_reorder_tx is
  signal chip_id_reg : std_logic_vector(chip_id'range);
  signal chip_id_i   : std_logic_vector(chip_id'range);
begin
  -- Register the chip_id from FPGA pins to ease timing closure.
  -- . Alternatively these registers may better be removed and pin input chip_id[] set as false path for timing closure
  u_chip_id : entity common_lib.common_pipeline
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => c_meta_delay_len,
    g_reset_value    => 0,
    g_in_dat_w       => chip_id'LENGTH,
    g_out_dat_w      => chip_id'length
  )
  port map (
    rst     => '0',
    clk     => clk,
    in_dat  => chip_id,
    out_dat => chip_id_reg
  );

  -- force chip_id(2) to '0' or '1' to reduce the case options in p_comb if the design will run only on a FN or only on a BN
  chip_id_i <= func_unb1_board_chip_id(chip_id_reg, g_node_type);

  p_comb : process(chip_id_i, tx_usr_sosi_2arr, rx_usr_siso_2arr)
  begin
    -- default connect to node 0,1,2,3 via port 0,1,2,3
    tx_phy_sosi_2arr <= tx_usr_sosi_2arr;
    rx_phy_siso_2arr <= rx_usr_siso_2arr;
    if g_reorder = true then
      -- make the reordering to compensate for the reordering of the UniBoard mesh
      case TO_UINT(chip_id_i) is
        when 0 =>  -- this is FN0, connect usr bus 0,1,2,3 to phy bus 0,3,2,1
          -- sosi
          tx_phy_sosi_2arr(3) <= tx_usr_sosi_2arr(1);
          tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(2);
          tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(3);
          -- siso
          rx_phy_siso_2arr(3) <= rx_usr_siso_2arr(1);
          rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(2);
          rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(3);
        when 1 =>  -- this is FN1, connect usr bus 0,1,2,3 to phy bus 0,1,3,2
          -- sosi
          tx_phy_sosi_2arr(3) <= tx_usr_sosi_2arr(2);
          tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(3);
          -- siso
          rx_phy_siso_2arr(3) <= rx_usr_siso_2arr(2);
          rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(3);
        when 2 =>  -- this is FN2, connect usr bus 0,1,2,3 to phy bus 0,1,2,3
        when 3 =>  -- this is FN3, connect usr bus 0,1,2,3 to phy bus 1,0,2,3
          -- sosi
          tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(0);
          tx_phy_sosi_2arr(0) <= tx_usr_sosi_2arr(1);
          -- siso
          rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(0);
          rx_phy_siso_2arr(0) <= rx_usr_siso_2arr(1);
        when 4 |
             5 =>  -- this is BN0
                  --      or BN1, connect usr bus 0,1,2,3 to phy bus 3,2,1,0
          -- sosi
          tx_phy_sosi_2arr(3) <= tx_usr_sosi_2arr(0);
          tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(1);
          tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(2);
          tx_phy_sosi_2arr(0) <= tx_usr_sosi_2arr(3);
          -- siso
          rx_phy_siso_2arr(3) <= rx_usr_siso_2arr(0);
          rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(1);
          rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(2);
          rx_phy_siso_2arr(0) <= rx_usr_siso_2arr(3);
        when 6 =>  -- this is BN2, connect usr bus 0,1,2,3 to phy bus 2,3,1,0
          -- sosi
          tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(0);
          tx_phy_sosi_2arr(3) <= tx_usr_sosi_2arr(1);
          tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(2);
          tx_phy_sosi_2arr(0) <= tx_usr_sosi_2arr(3);
          -- siso
          rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(0);
          rx_phy_siso_2arr(3) <= rx_usr_siso_2arr(1);
          rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(2);
          rx_phy_siso_2arr(0) <= rx_usr_siso_2arr(3);
        when 7 =>  -- this is BN3, connect usr bus 0,1,2,3 to phy bus 1,3,2,0
          -- sosi
          tx_phy_sosi_2arr(1) <= tx_usr_sosi_2arr(0);
          tx_phy_sosi_2arr(3) <= tx_usr_sosi_2arr(1);
          tx_phy_sosi_2arr(2) <= tx_usr_sosi_2arr(2);
          tx_phy_sosi_2arr(0) <= tx_usr_sosi_2arr(3);
          -- siso
          rx_phy_siso_2arr(1) <= rx_usr_siso_2arr(0);
          rx_phy_siso_2arr(3) <= rx_usr_siso_2arr(1);
          rx_phy_siso_2arr(2) <= rx_usr_siso_2arr(2);
          rx_phy_siso_2arr(0) <= rx_usr_siso_2arr(3);
        when others => null;
      end case;
    end if;
  end process;
end rtl;
