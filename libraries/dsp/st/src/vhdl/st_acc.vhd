-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

-- Purpose:
--   Accumulate input data to an accumulator that is stored externally. In this
--   way blocks of input samples (e.g. subband products) can be accumulated to
--   a set of external accumulators. At the in_load the accumulator input value
--   is ignored so that the accumulation restarts with the in_dat.
--
-- Description:
--   if in_load = '1' then
--     out_acc = in_dat + 0         -- restart accumulation
--   else
--     out_acc = in_dat + in_acc    -- accumulate
--
-- Remarks:
-- . in_val propagates to out_val after the pipeline latency but does not
--   affect the sum

entity st_acc is
  generic (
    g_dat_w            : natural;
    g_acc_w            : natural;  -- g_acc_w >= g_dat_w
    g_hold_load        : boolean := true;
    g_pipeline_input   : natural;  -- 0 no input registers, else register input after in_load
    g_pipeline_output  : natural  -- pipeline for the adder
  );
  port (
    clk         : in  std_logic;
    clken       : in  std_logic := '1';
    in_load     : in  std_logic;
    in_dat      : in  std_logic_vector(g_dat_w - 1 downto 0);
    in_acc      : in  std_logic_vector(g_acc_w - 1 downto 0);
    in_val      : in  std_logic := '1';
    out_acc     : out std_logic_vector(g_acc_w - 1 downto 0);
    out_val     : out std_logic
  );
end st_acc;

architecture rtl of st_acc is
  constant c_pipeline  : natural := g_pipeline_input + g_pipeline_output;

  -- Input signals
  signal hld_load        : std_logic := '0';
  signal nxt_hld_load    : std_logic;
  signal acc_clr        : std_logic;

  signal reg_dat        : std_logic_vector(g_acc_w - 1 downto 0) := (others => '0');
  signal nxt_reg_dat    : std_logic_vector(g_acc_w - 1 downto 0);
  signal reg_acc        : std_logic_vector(g_acc_w - 1 downto 0) := (others => '0');
  signal nxt_reg_acc    : std_logic_vector(g_acc_w - 1 downto 0);

  -- Pipeline control signals, map to slv to be able to use common_pipeline
  signal in_val_slv     : std_logic_vector(0 downto 0);
  signal out_val_slv    : std_logic_vector(0 downto 0);
begin
  assert not(g_acc_w < g_dat_w)
    report "st_acc: output accumulator width must be >= input data width"
    severity FAILURE;

  ------------------------------------------------------------------------------
  -- Input load control
  ------------------------------------------------------------------------------

  p_clk : process(clk)
  begin
    if rising_edge(clk) then
      if clken = '1' then
        hld_load <= nxt_hld_load;
      end if;
    end if;
  end process;

  nxt_hld_load <= in_load when in_val = '1' else hld_load;

  -- Hold in_load to save power by avoiding unneccessary out_acc toggling when in_val goes low
  -- . For g_pipeline_input>0 this is fine
  -- . For g_pipeline_input=0 this may cause difficulty in achieving timing closure for synthesis
  use_in_load : if g_hold_load = false generate
    acc_clr <= in_load;  -- the in_load may already be extended during in_val
  end generate;

  use_hld_load : if g_hold_load = true generate
    acc_clr <= in_load or (hld_load and not in_val);
  end generate;

  -- Do not use g_pipeline_input of u_adder, to allow registered acc clear if g_pipeline_input=1
  nxt_reg_dat <= RESIZE_SVEC(in_dat, g_acc_w);
  nxt_reg_acc <= in_acc when acc_clr = '0' else (others => '0');

  no_input_reg : if g_pipeline_input = 0 generate
    reg_dat <= nxt_reg_dat;
    reg_acc <= nxt_reg_acc;
  end generate;

  gen_input_reg : if g_pipeline_input > 0 generate
    p_reg : process(clk)
    begin
      if rising_edge(clk) then
        if clken = '1' then
          reg_dat <= nxt_reg_dat;
          reg_acc <= nxt_reg_acc;
        end if;
      end if;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- Adder for the external accumulator
  ------------------------------------------------------------------------------

  u_adder : entity common_lib.common_add_sub
  generic map (
    g_direction       => "ADD",
    g_representation  => "SIGNED",  -- not relevant because g_out_dat_w = g_in_dat_w
    g_pipeline_input  => 0,
    g_pipeline_output => g_pipeline_output,
    g_in_dat_w        => g_acc_w,
    g_out_dat_w       => g_acc_w
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_a    => reg_dat,
    in_b    => reg_acc,
    result  => out_acc
  );

  ------------------------------------------------------------------------------
  -- Parallel output control pipeline
  ------------------------------------------------------------------------------

  in_val_slv(0) <= in_val;
  out_val       <= out_val_slv(0);

  u_out_val : entity common_lib.common_pipeline
  generic map (
    g_representation => "UNSIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => 1,
    g_out_dat_w      => 1
  )
  port map (
    clk     => clk,
    clken   => clken,
    in_dat  => slv(in_val),
    out_dat => out_val_slv
  );
end rtl;
