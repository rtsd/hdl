-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Multiplex the 4 signal streams into 1 wide stream

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity bn_capture_mux is
  generic (
    g_nof_input    : natural := 4;
    g_in_fifo_size : natural := 512;  -- 2 * 1024/4, at least 1 frame of block size 1024 8b samples, so 1024/4 32b samples words
    g_in_data_w    : natural := 32;
    g_mux_data_w   : natural := 256
  );
  port (
    in_rst         : in  std_logic;
    in_clk         : in  std_logic;
    mux_wide_rst   : in  std_logic;
    mux_wide_clk   : in  std_logic;

    -- ST sinks (input signal paths)
    in_siso_arr    : out t_dp_siso_arr(0 to g_nof_input - 1);
    in_sosi_arr    : in  t_dp_sosi_arr(0 to g_nof_input - 1);  -- = [0:3] = Signal Paths [A[31:0], B[31:0], C[31:0], D[31:0]]
                                                             -- and e.g. A[31:0] = [A(t0), A(t1), A(t2), A(t3)], so 4 8b samples in time per one 32b word
    -- ST source (multiplexed output signal paths)
    mux_wide_siso  : in  t_dp_siso := c_dp_siso_rdy;
    mux_wide_sosi  : out t_dp_sosi  -- = Signal Paths A[255:0], B[255:0], C[255:0], D[255:0] multiplexed in time
                                                             -- and e.g. A[255:0] = [A(t0), A(t1), ..., A(t31)], so 4*8=32 8b samples in time per one 256b word
  );
end bn_capture_mux;

architecture str of bn_capture_mux is
  signal wide_siso_arr  : t_dp_siso_arr(0 to g_nof_input - 1);
  signal wide_sosi_arr  : t_dp_sosi_arr(0 to g_nof_input - 1);  -- = [0:3] = Signal Paths [A,B,C,D]
                                                              -- and e.g. A[255:0] = [A(t0), A(t1), ..., A(t31)], so 4*8=32 800M samples in time per one 256b word
begin
  gen_fifo : for I in 0 to g_nof_input - 1 generate
    u_n2w : entity dp_lib.dp_fifo_dc_mixed_widths
    generic map (
      g_wr_data_w    => g_in_data_w,
      g_rd_data_w    => g_mux_data_w,
      g_use_ctrl     => true,
      g_wr_fifo_size => g_in_fifo_size,
      g_rd_fifo_rl   => 1
    )
    port map (
      wr_rst         => in_rst,
      wr_clk         => in_clk,
      rd_rst         => mux_wide_rst,
      rd_clk         => mux_wide_clk,
      -- ST sink
      snk_out        => in_siso_arr(I),
      snk_in         => in_sosi_arr(I),
      -- Monitor FIFO filling
      wr_usedw       => OPEN,
      rd_usedw       => OPEN,
      rd_emp         => OPEN,
      -- ST source
      src_in         => wide_siso_arr(I),
      src_out        => wide_sosi_arr(I)
    );
  end generate;

  gen_mux : entity dp_lib.dp_mux
  generic map (
    g_data_w          => g_mux_data_w,
    g_empty_w         => 1,
    g_in_channel_w    => 1,
    g_error_w         => 1,
    g_use_empty       => false,
    g_use_in_channel  => false,
    g_use_error       => false,
    g_mode            => 1,
    g_nof_input       => g_nof_input,
    g_use_fifo        => false,
    g_fifo_size       => array_init(1024, g_nof_input),  -- dummy value must match g_nof_input
    g_fifo_fill       => array_init(   0, g_nof_input)  -- dummy value must match g_nof_input
  )
  port map (
    rst         => mux_wide_rst,
    clk         => mux_wide_clk,
    -- ST sinks
    snk_out_arr => wide_siso_arr,
    snk_in_arr  => wide_sosi_arr,
    -- ST source
    src_in      => mux_wide_siso,
    src_out     => mux_wide_sosi
  );
end str;
