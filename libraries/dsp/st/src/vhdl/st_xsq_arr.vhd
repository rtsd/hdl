-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- Author : R. vd Walle
-- Purpose:
-- Multiple instances of st_xsq.vhd
-- Description:
--   . See st_xsq.vhd
-- Remarks:
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib, technology_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use dp_lib.dp_stream_pkg.all;

entity st_xsq_arr is
  generic (
    g_nof_streams       : natural := 1;
    g_nof_crosslets     : natural := 1;
    g_nof_signal_inputs : natural := 2;
    g_in_data_w         : natural := 18;  -- width of the data to be accumulated
    g_stat_data_w       : natural := 54;  -- statistics accumulator width
    g_stat_data_sz      : natural := 2  -- statistics word width >= statistics accumulator width and fit in a power of 2 multiple 32b MM words
  );
  port (
    mm_rst : in  std_logic;
    mm_clk : in  std_logic;
    dp_rst : in  std_logic;
    dp_clk : in  std_logic;

    -- Streaming
    in_a_arr : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);  -- Complex input data
    in_b_arr : in  t_dp_sosi_arr(g_nof_streams - 1 downto 0);  -- Complex input data

    -- Memory Mapped
    ram_st_xsq_mosi : in  t_mem_mosi;
    ram_st_xsq_miso : out t_mem_miso
  );
end st_xsq_arr;

architecture str of st_xsq_arr is
  constant c_xsq : natural := g_nof_signal_inputs * g_nof_signal_inputs;
  constant c_nof_statistics : natural := g_nof_crosslets * c_xsq;
  constant c_nof_word     : natural := g_stat_data_sz * c_nof_statistics * c_nof_complex;
  constant c_nof_word_w   : natural := ceil_log2(c_nof_word);

  signal ram_st_xsq_mosi_arr : t_mem_mosi_arr(g_nof_streams - 1 downto 0) := (others => c_mem_mosi_rst);
  signal ram_st_xsq_miso_arr : t_mem_miso_arr(g_nof_streams - 1 downto 0) := (others => c_mem_miso_rst);
begin
  -- st_xsq instances
  gen_xsq : for I in 0 to g_nof_streams - 1 generate
    st_xsq : entity work.st_xsq
    generic map (
      g_nof_signal_inputs => g_nof_signal_inputs,
      g_nof_crosslets     => g_nof_crosslets,
      g_in_data_w         => g_in_data_w,
      g_stat_data_w       => g_stat_data_w,
      g_stat_data_sz      => g_stat_data_sz
    )
    port map (

      mm_rst => mm_rst,
      mm_clk => mm_clk,
      dp_rst => dp_rst,
      dp_clk => dp_clk,

      -- Streaming
      in_a => in_a_arr(I),
      in_b => in_b_arr(I),

      -- Memory Mapped
      ram_st_xsq_mosi => ram_st_xsq_mosi_arr(I),
      ram_st_xsq_miso => ram_st_xsq_miso_arr(I)
    );
  end generate;

  ---------------------------------------------------------------
  -- COMBINE MEMORY MAPPED INTERFACES
  ---------------------------------------------------------------
  -- Combine the internal array of mm interfaces.
  u_mem_mux_select : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_nof_streams,
    g_mult_addr_w => c_nof_word_w
  )
  port map (
    mosi     => ram_st_xsq_mosi,
    miso     => ram_st_xsq_miso,
    mosi_arr => ram_st_xsq_mosi_arr,
    miso_arr => ram_st_xsq_miso_arr
  );
end str;
