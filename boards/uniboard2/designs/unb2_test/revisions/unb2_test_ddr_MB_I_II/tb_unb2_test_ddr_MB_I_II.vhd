-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, unb2_test_lib;
use IEEE.std_logic_1164.all;

entity tb_unb2_test_ddr_MB_I_II is
end tb_unb2_test_ddr_MB_I_II;

architecture tb of tb_unb2_test_ddr_MB_I_II is
begin
  u_tb_unb2_test : entity unb2_test_lib.tb_unb2_test
  generic map (
    g_design_name   => "unb2_test_ddr_MB_I_II",
    g_sim_model_ddr => false
  );
end tb;
