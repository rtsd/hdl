%% Test R2SDF output for given input signal
% 
%-------------------------------------------------------------------------%
%   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
%   Copyright (C) 2009-2010
%   ASTRON (Netherlands Institute for Radio Astronomy)
%   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
% 
%   This file is part of the UniBoard software suite.
%   The file is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%-------------------------------------------------------------------------%
%%
%function testFFT
if(1)
  clear all; close all; clc;

  %--------------------
  j=  1i; 
  K=  12;  
  N=  2.^K;
  B=  16; 
  Bt= 16; 

  NOISE    = 1;
  IMPULSE  = 0;
  TWO_REAL = 1;
  REORDER  = 0;  % optional Reorder enable for the vhdl output. Use only in case reordering in VHDL is not done! 
  SEPARATE = 1;  % Separate function only applicable when TWO_REAL inputs are used. 
  FIGURE   = 1;
  
  SYN=1;
  VAL=2;
  RE= 3;
  IM= 4;
  
  snr=zeros(K,B);
  for kk=10:10
    for bb=16:1:16
%   for kk=2:K
%     for bb=4:B

      n=2.^kk;
      
      x=zeros(n,4); %in_syn,  in_val,   in_re,  in_img
      y=zeros(n,4); %out_syn, out_val,  out_re, out_img
      if TWO_REAL
        if NOISE        
          inFileName=   strcat('../data/test/in/uniNoise_p',num2str(n),'_b',num2str(bb),'_in.txt');
          outFileName=  strcat('../data/test/out/uniNoise_2xreal_p',num2str(n),'_b',num2str(bb),'_tb',num2str(Bt),'_out.txt');
        elseif IMPULSE
          inFileName=   '../data/2xreal_impulse_p16_b16_in.txt'
          outFileName=  '../data/2xreal_impulse_p16_b16_out.txt'  % Golden reference file
  %        outFileName=  '../data/2xreal_impulse_out.txt'
        end;
      else                                            
        if NOISE        
          inFileName=   strcat('../data/test/in/uniNoise_p',num2str(n),'_b',num2str(bb),'_in.txt');
          outFileName=  strcat('../data/test/out/uniNoise_p',num2str(n),'_b',num2str(bb),'_tb',num2str(Bt),'_out.txt');
        elseif IMPULSE
          inFileName=   '../data/impulse_p16_b16_in.txt'
          outFileName=  '../data/impulse_p16_b16_out.txt'
        end;
      end;   
        
      %-------------------
      'twiddle defintion'
        w=  exp(-j*(2*pi/n));
        W=  w.^([0:n-1]'*[0:n-1]);  % W matrix for DFT      
      
%       N=0:n-1; K=0:n-1;
%       w=exp(-j*(2*pi/n));
%       NK= N'*K;     % index
%       W=  w.^NK;    % W matrix for DFT

      'reading Model sim inputs and outputs'
      inFileName
      outFileName
      X= dlmread(inFileName,',');
      Y= dlmread(outFileName,',');

      xSize= size(X);
      ySize= size(Y);
      
      % read valid inputs
      new=1;
      for cnt=1: xSize(1)
        if(X(cnt,VAL)==1) %reading in_val
          x(new,:)=X(cnt,:);
          new=new+1;
        end;
      end;
      size(x);
      
      % read valid outputs
      new=1;
      for cnt=1: ySize(1)
        if(Y(cnt,VAL)==1)
          y(new,:)=Y(cnt,:);
          new=new+1;
        end;
      end;
      size(y);
      
      'the number of relevant input frames are'
      nOfFrames= round(size(x,1)/n)
      
      x=x(1:nOfFrames*n,:);
      y=y(1:nOfFrames*n,:);
      
      if(REORDER)
        % re-order y
        r=bitrevorder([0:n-1]);
        for ff=0:n:(nOfFrames-1)*n
          for jj=1:size(r,2)
            y1(ff+jj,:)=y(ff+r(jj)+1,:);
          end;
        end;
      else
        % retain fft
        y1=y;
      end;
      size(y1);

      % FFT
      for ff=0:n:(nOfFrames-1)*n
        tmp(ff+1:ff+n,:)=fft(x(ff+1:ff+n,RE)+i*x(ff+1:ff+n,IM),n);
      end;
      expFFT(:,RE)=real(tmp); 
      expFFT(:,IM)=imag(tmp);
           
      % DFT
      for ff=0:n:(nOfFrames-1)*n
        tmp(ff+1:ff+n,:)= W*(x(ff+1:ff+n,RE)+i*x(ff+1:ff+n,IM));
      end;
      expDFT(:,RE)=real(tmp); 
      expDFT(:,IM)=imag(tmp);
      
      % Separate
      if(SEPARATE)  
        for ff=0:n:(nOfFrames-1)*n
          newaFFT(1+ff,RE) = expFFT(1+ff,RE);
          newbFFT(1+ff,RE) = expFFT(1+ff,IM);
          for jj=1:1:n/2
            newaFFT(jj+ff+1,RE) = (expFFT(ff+n-jj+1,RE) + expFFT(ff+jj+1,RE))/2;
            newbFFT(jj+ff+1,RE) = (expFFT(ff+n-jj+1,IM) + expFFT(ff+jj+1,IM))/2;
            newaFFT(jj+ff+1,IM) = (expFFT(ff+jj+1,IM)   - expFFT(ff+n-jj+1,IM))/2;
            newbFFT(jj+ff+1,IM) = (expFFT(ff+n-jj+1,RE) - expFFT(ff+jj+1,RE))/2;
          end;          
          for jj=0:1:n/2-1          
            expFFT(ff+2*jj+1,RE) = newaFFT(ff+jj+1,RE); 
            expFFT(ff+2*jj+2,RE) = newbFFT(ff+jj+1,RE);
            expFFT(ff+2*jj+1,IM) = newaFFT(ff+jj+1,IM); 
            expFFT(ff+2*jj+2,IM) = newbFFT(ff+jj+1,IM);
          end;
        end;
      end;    
      expFFT;

      % Error
      z=    expFFT(:,RE)+i*expFFT(:,IM);
      zQ=   y1(:,RE)+i*y1(:,IM);
      qErr= z-zQ;
      snr(kk,bb)= 10*log10((zQ'*zQ)/(qErr'*qErr));
      % snr(kk,bb)= 10*log10((z'*z)/(qErr'*qErr)); % both yield same SQNR
      
      % %bitgrowth
      % Np=2.^(1:16)
      % bg=ceil((1+log2(Np))/2)
      
      if(FIGURE)
        subplot(4,1,1);
        plot(x(1:nOfFrames*n,RE),'B*-'); grid on; hold on;
        plot(x(1:nOfFrames*n,IM),'r*-'); hold off;
        legend ('Input real','Input imaginary');

        subplot(4,1,2); grid on;
        plot(expFFT(:,RE),'B*-'); grid on; hold on;
        plot(expFFT(:,IM),'r*-'); hold off;
        legend ('MATLAB FFT output real','MATLAB FFT output imaginary');

%         subplot(5,1,3); grid on;
%         plot(expDFT(:,2),'B*-'); grid on; hold on;
%         plot(expDFT(:,3),'r*-'); hold off;
%         legend ('DFT output real','DFT output imaginary');

        subplot(4,1,3); grid on;
        plot(y1(:,RE),'B*-'); grid on; hold on;
        plot(y1(:,IM),'r*-'); hold off;
        legend ('VHDL output real','VHDL output imaginary');

        subplot(4,1,4); grid on;
        %figure;
        plot(real(qErr),'B*-'); grid on; hold on;
        plot(imag(qErr),'r*-'); hold off;
        legend ('error real','error imaginary');
      end;
    end;
  clear tmp expFFT expDFT x y Y z;    
  end;
  snr(kk,:).'
end;

inputDB=  kron((6.020599 * [1:16]),ones(kk,1));

% reordering snr table, row wise
for ii=2:10
  snr_row(ii,:)= snr(12-ii,:);
end;

if(0)
  'imagesc'
  imagesc(inputDB(2:10,4:16)-snr_row(2:10,4:16)); shading flat; colorbar;
  
  set(gca,'XTick',[1:16])
  set(gca,'XTickLabel',[4:16])
  set(gca,'YTickLabel',2.^[10:-1:2])
  zlabel('SQNR (db)')

%   set(gca,'XTick',[1:16])
%   xlabel('Number of Input bits (B)'); set(gca,'XTickLabel',[4:16])
%   ylabel('Number of points (P)'); set(gca,'YTickLabel',2.^[10:-1:2])
%   zlabel('SQNR (db)')
%   title({ 'SQNR (db) plots of VHDL-R2SDF FFT core';
%     [' Twiddle Bits= ',num2str(Bt),...
%     ', Output Bits= ','B+ 0.5(log_2P +2)']...
%     });
  %title(strcat('SQNR(db) plot for R2SDF FFT : Twiddles are coded in ',num2str(Bt),'bits, Output is coded in (B+ 0.5*log_2P+ 2) bits'))
  %set(gca,'YDir','normal');
  
  for ii=K:-1:2
    for jj=B:-1:4
      text(1.0*(jj-4)+1,1.0*(ii-2)+1,[num2str(round(snr_row(ii,jj)))],...
            'BackgroundColor',[.7 .7 .7]);
    end;
  end;
end;

if(0)
  
  [C,h]=contour(snr(3:K,4:B)); shading flat; colorbar;
  set(h,'ShowText','on','TextStep',get(h,'LevelStep')*1)
  xlabel('Number of Input bits (B)'); set(gca,'XTickLabel',[4:2:B])
  ylabel('Number of points (P)'); set(gca,'YTickLabel',2.^[3:K])
  zlabel('SQNR (db)')
  title(strcat('SQNR(db) plot for R2SDF FFT : Twiddles are coded in ',num2str(Bt),'bits, Output is coded in (B+ 0.5*log_2P+ 2) bits'))
  set(gca,'YDir','normal');  
end;

%return;