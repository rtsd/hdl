--------------------------------------------------------------------------------
--   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
--   Copyright (C) 2009-2010
--   ASTRON (Netherlands Institute for Radio Astronomy)
--   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
--   This file is part of the UniBoard software suite.
--   The file is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

library ieee;
use IEEE.std_logic_1164.all;

package rTwoSDFPkg is
  -- Internal pipeline latencies and Z^(-1) pipeline settings for a stage in the rTwoSDF FFT
  -- Also used for other preallele and wideband FFT implementations (fft_lib)
  type t_fft_pipeline is record
    -- generics for rTwoSDFStage
    stage_lat      : natural;  -- = 1
    weight_lat     : natural;  -- = 1
    mul_lat        : natural;  -- = 3+1
    -- generics for rTwoBFStage
    bf_lat         : natural;  -- = 1
    -- generics for rTwoBF
    bf_use_zdly    : natural;  -- = 1
    bf_in_a_zdly   : natural;  -- = 0
    bf_out_d_zdly  : natural;  -- = 0
  end record;

  constant c_fft_pipeline   : t_fft_pipeline := (1, 1, 4, 1, 1, 0, 0);
end package rTwoSDFPkg;

package body rTwoSDFPkg is
end rTwoSDFPkg;
