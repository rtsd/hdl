-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
--
-- Purpose: Test bench for common_zip
-- Features:
--
-- Usage:
-- > as 10
-- > run -all
-- Observe manually in Wave Window that the values of the in_dat_arr are zipped
-- to the out_dat vector.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;
use work.common_lfsr_sequences_pkg.all;
use work.tb_common_pkg.all;

entity tb_common_zip is
  generic (
    g_nof_streams : natural := 3;  -- Number of input streams to be zipped
    g_dat_w       : natural := 8
  );
end tb_common_zip;

architecture tb of tb_common_zip is
  constant clk_period   : time      := 10 ns;
  constant c_rl         : natural   := 1;  -- Read Latency = 1

  signal rst         : std_logic;
  signal clk         : std_logic := '0';
  signal tb_end      : std_logic := '0';

  signal ready       : std_logic := '1';  -- Ready is always '1'
  signal in_dat_arr  : t_slv_64_arr(g_nof_streams - 1 downto 0);
  signal in_val      : std_logic := '1';
  signal out_dat     : std_logic_vector(g_dat_w - 1 downto 0);
  signal out_val     : std_logic;
  signal ena         : std_logic := '1';
  signal ena_mask    : std_logic := '1';
  signal enable      : std_logic := '1';
begin
  clk    <= not clk or tb_end after clk_period / 2;
  rst    <= '1', '0' after 7 * clk_period;
  tb_end <= '0', '1' after 1 us;

  gen_data : for I in 0 to g_nof_streams - 1 generate
    proc_common_gen_data(c_rl, I * 10, rst, clk, enable, ready, in_dat_arr(I), in_val);
  end generate;

  -- The "ena" forms the dutu cycle for the in_val signal
  proc_common_gen_pulse(1, g_nof_streams, '1', clk, ena);

  -- The "ena_mask" creates a gap between series of incoming packets in order
  -- to simulate the starting and stopping of the incoming streams.
  proc_common_gen_pulse(g_nof_streams * 10, g_nof_streams * 15, '1', clk, ena_mask);
  enable <= ena and ena_mask;

  u_dut : entity work.common_zip
  generic map (
    g_nof_streams => g_nof_streams,
    g_dat_w       => g_dat_w
  )
  port map (
    rst        => rst,
    clk        => clk,
    in_val     => in_val,
    in_dat_arr => in_dat_arr,
    out_val    => out_val,
    out_dat    => out_dat
  );
end tb;
