This directory contains the following files:

unb2a_minimal.rbf
unb2a_minimal.sof

-> The .rbf is generated from the .sof file.
-> This .rbf also contains the bootloader, so it can only be used as a Factory image in the EPCQ flash (starting @ address 0)
