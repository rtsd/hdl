-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Instantiate FIFO IP with generics
-- Description:
--   Copied component declaration and instance example from generated/fifo_140/sim/ip_arria10_e1sg_fifo_sc_fifo_140_pkqwcbi.vhd

library ieee;
use ieee.std_logic_1164.all;

library technology_lib;
use technology_lib.technology_pkg.all;

library altera_mf;
use altera_mf.all;

entity ip_arria10_e1sg_fifo_sc is
  generic (
    g_use_eab   : string := "ON";
    g_dat_w     : natural := 20;
    g_nof_words : natural := 1024
  );
  port (
    aclr    : in std_logic;
    clock   : in std_logic;
    data    : in std_logic_vector(g_dat_w - 1 downto 0);
    rdreq   : in std_logic;
    wrreq   : in std_logic;
    empty   : out std_logic;
    full    : out std_logic;
    q       : out std_logic_vector(g_dat_w - 1 downto 0);
    usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
end ip_arria10_e1sg_fifo_sc;

architecture SYN of ip_arria10_e1sg_fifo_sc is
  component  scfifo
  generic (
      add_ram_output_register  : string;
      intended_device_family  : string;
      lpm_numwords  : natural;
      lpm_showahead  : string;
      lpm_type  : string;
      lpm_width  : natural;
      lpm_widthu  : natural;
      overflow_checking  : string;
      underflow_checking  : string;
      use_eab  : string
  );
  port (
      aclr   : in std_logic;
      clock   : in std_logic;
      data   : in std_logic_vector(g_dat_w - 1 downto 0);
      rdreq   : in std_logic;
      wrreq   : in std_logic;
      empty   : out std_logic;
      full   : out std_logic;
      q   : out std_logic_vector(g_dat_w - 1 downto 0);
      usedw   : out std_logic_vector(tech_ceil_log2(g_nof_words) - 1 downto 0)
  );
  end component;
begin
  u_scfifo : scfifo
  generic map (
    add_ram_output_register  => "ON",
    intended_device_family  => "Arria 10",
    lpm_numwords  => g_nof_words,
    lpm_showahead  => "OFF",
    lpm_type  => "scfifo",
    lpm_width  => g_dat_w,
    lpm_widthu  => tech_ceil_log2(g_nof_words),
    overflow_checking  => "ON",
    underflow_checking  => "ON",
    use_eab  => g_use_eab
  )
  port map (
    aclr => aclr,
    clock => clock,
    data => data,
    rdreq => rdreq,
    wrreq => wrreq,
    empty => empty,
    full => full,
    q => q,
    usedw => usedw
  );
end SYN;
