// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
p63aUr3HyWgiU2JcWQkoZoUp6Qng2oLeKl643gBhqPm5W9FRMfLIBHGQMfLHYbjS
RthbvFcjEI+xdrgCzDbE2Aqe8QyRPZ3YC9NYNtlNWYM9noYjVcBHEWDxw1G0rFhO
TGgZW208uyKeFwfZSSb8AGcD7p40jCu1xnQmQ9AHOeI=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 1600 )
`pragma protect data_block
qbAW9dhuMrdteQDeVkSye2Xw4fq4d6JsJhc7vAzJUIVPyv8SX4DInu4sfIgE4FqO
4kqf9chLysQHI0EAQcNfcr4c8tuiGHvaUVsK/bpqF3pXaEJfzhIiOO6EEbYiKGJO
I79jCGsW3mcFU7BOIyari2k6YIbIbsFqus8SVA3KwalPAUkDQoLwg85KWr3suNCN
QIJILm7dyKhJHrUjFfX71L7ZM5j2eKoVkd4yWYvzGulEH1HHSvV0tU9RMPDuRvXA
uoA3oX+f3ozWFhRvxOkkLxllo/lZiiXD3luNxcBtGb6HLb1vPMimslZzcCXIsqNf
Tl/vxN2j4JsA3040Pj5Q52IF5AWWfTfZI1J5IVftBVJHnwSCXOb3IdmhH0KRjTme
ALcJgYeeNq2zSagNM3XI+E2GONYPA56gNZ+6/bOyNx/NRr5dPnl/9CJ5w3To0onV
Yf/NdB3HBubZSaQXdZpmsAa21VbU9hckhezYBNX6CyFB1cXq1ltKljJ/KlWCfUFw
S5WtwJqu6hnr1UhYSfBfkQgOEU3hpTDzayC1hT6TZKF47LTUCXgjAYc37hzCquAw
HDnRqyKKeewvgdspR6p2QZv9++R4Lauzd1k80lK1y0Jza0szeg6q/tLFQvTOeZcj
NtfDKItsKLGXTayT8p4emvAI5Z9fX1+/qX/rn7FSI3RPa10e16eZcIwyalrnUmBQ
uOtbPeX4sSdn0yUeR9h45JCQMwbYfjayFai3j2fpv5DcdkFbK88JRz+zYv6DuePc
EkM6IZ/p6s1Cs5i+lg+ZjYH4vNoegu9pbTUQU+TNuAa1RcttiRxPXvuSmWBdaYDq
CentURAAk9R4kVbuun70xXaePyHyOwRB4+/9M3d3xiNr+DaYB/nds+erEXxP/gPN
S0zlWpmCVmc6SpEff4+MFPT65kMzy5Z403sfIVMnV3zhqO3MAAZSq/59C1q8/yST
cwJ5u/ugppHU4ygsdH8LtAc1ua3irgZLZoNkWf0S/HUE+zinDYu23dew+dIIrG/+
nWzLVCWolM9qOy6ctjFrDSYQRlZSGWGBoCGp37JPrgVsE/URzhgPP6lUG8+Bmprs
KY5oHqoowYwSdwlJ0OaQaV6ldrC1CIoaakss5P0jXn2veQEvmjbdZyd+B0fPYclb
H2Cpy6XTHXO+rbVdfTGxAdDP71wOBqG8lQVIN8bb/UTbrDV4ee2rHZMpfIV9esl8
Rkrk1QVHafojv58gtAV0tbWoFxIC+/gYRdRXIsCEdDl/VsvL8r5ioLtnI3oaQoM+
sA6GqeUqwAfd2kXl0Jgg38V2Gg+GuZXMpz8xyIpHXQKFtC+P4PasguqoIr0KhovL
Vpkz4IImS/2eRe9S9RLziOoE7u19kF1aG9DREdcmKvNRCH1pgby6WeZLmD6RbZTk
hex/Nj2YgUOuKHkSM67erGjMioKKEX3EvSak5WdW7q13A6WIKNccnvF5Vj9rhQer
spvg89VIUltbEEJP+7ckiAee3r2SX7lJM4fqmx4/r8RwAc6KPsfKCmxkNQzESZ6d
iweJ+BcA8nBv0VDTU4NIdEKERQuDOFiIil0YT8UYXeTbPC/OP6FUt6AYXDFJbBfu
JFt+pDgEvNms++6OOSHxlvxruDmsNhCrjR0LZakdVMrynCdNkV8RjwwZ+RQkjeAM
II+JJeunPLRxWaStSDbDE8JLD+AFgglsJosSqzeuw+9e22lmlKQxMvNUOjnu4gcd
sUznHoQwdmwv6HkgDqjSP+z2Qrjy9/uIdqB7TIt6/jLhDdZUyUvMjXBL8beEb/ss
kB1Yrc5C73+weYPWp9hGMILbbKyZwK4rlo7fynQ/zcMKxone0kISdMtdpJWjzUWR
04ZHgXkz0AmLUElV6NNbU28dl7x1J2gKDqJ4fRbeKzgzDTsd9XPKGvA5aiEKB5JR
+YDWtPpVaYLCZmDCIodtQJIqbmyBQCzi9tebwDgXlP/4i2iYcj6XAW1yATFMtLGX
/Ff73mHNrSW6IuTcThurfB6K49E6CDvGfo5HKfYl5HQfRqMV2eO4BXMuxcQ8qRA4
slKJ/g2RuFPU5GN0oLn6O7J36gI+Ij06FSQ3ynmsiNo9iEhr0WuuXugv+fGk0z40
enOcno9g0Tl//WICFUZw7A==

`pragma protect end_protected
