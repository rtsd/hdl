-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.eth_pkg.all;

-- Purpose:
--   Provide the total Ethernet header information for the user to decide upon.
-- Description:
--   Extract the first c_network_total_header_32b_nof_words=11 words from an
--   Ethernet packet and make them available via as a word array and as records
--   fields. The component has three output formats:
--   1) hdr_words_arr, the 11 word header store
--   2) hdr_data, the currently active header word
--   3) hdr_fields, combinatorial mapping of the 11 word header store to
--      various Ethernet packet header fields.
--   Which format to use depends on the application. The logic for words or
--   header fields that are not used, will get optimized away during synthesis.
--   The snk_in word count is also output for the header words. The first valid
--   word marked by the snk_in.sop has count 0, the last header word has count
--   10 and the payload words have count 11.
-- Remarks:
-- . This component acts as a stream monitor, therefore it does not output a
--   snk_out.ready.
-- . The word indices from eth_pkg together with hdr_words_arr_val[] can be
--   used to determine which word of the Ethernet header words is valid.
-- . The word indices from eth_pkg together with hdr_fields_val[] can be used
--   to determine which fields of the Ethernet header records are valid. The
--   difference with hdr_words_arr_val is that hdr_words_arr_val is a single
--   cycle pulse, whereas hdr_fields_val keeps its state until the next sop.
--

entity eth_hdr_store is
  port (
    rst               : in  std_logic;
    clk               : in  std_logic;

    -- Streaming Sink
    snk_in            : in  t_dp_sosi;
    snk_in_word_cnt   : out natural range 0 to c_network_total_header_32b_nof_words;

    -- Total header
    hdr_words_arr     : out t_network_total_header_32b_arr;
    hdr_words_arr_val : out std_logic_vector(0 to c_network_total_header_32b_nof_words - 1);
    -- Combinatorial map of the 11 header words on to the Ethernet header records
    hdr_fields        : out t_network_total_header;
    hdr_fields_val    : out std_logic_vector(0 to c_network_total_header_32b_nof_words - 1);
    -- Support also outputting only the currently valid header data
    hdr_data          : out std_logic_vector(c_word_w - 1 downto 0);
    hdr_data_val      : out std_logic
  );
end eth_hdr_store;

architecture rtl of eth_hdr_store is
  signal word_cnt              : natural range 0 to c_network_total_header_32b_nof_words;
  signal nxt_word_cnt          : natural;

  signal i_hdr_words_arr       : t_network_total_header_32b_arr := (others => (others => '0'));  -- init to avoid numeric_std warning, no need to rst
  signal nxt_hdr_words_arr     : t_network_total_header_32b_arr;
  signal nxt_hdr_words_arr_val : std_logic_vector(hdr_fields_val'range);
  signal i_hdr_fields_val      : std_logic_vector(hdr_fields_val'range);
  signal nxt_hdr_fields_val    : std_logic_vector(hdr_fields_val'range);
  signal i_hdr_data            : std_logic_vector(c_word_w - 1 downto 0) := (others => '0');  -- init to avoid numeric_std warning, no need to rst
  signal nxt_hdr_data          : std_logic_vector(c_word_w - 1 downto 0);
  signal nxt_hdr_data_val      : std_logic;
begin
  snk_in_word_cnt <= word_cnt;

  hdr_words_arr  <= i_hdr_words_arr;
  hdr_fields_val <= i_hdr_fields_val;
  hdr_data       <= i_hdr_data;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      -- input
      -- internal
      word_cnt          <= 0;
      -- output
      hdr_words_arr_val <= (others => '0');
      i_hdr_fields_val  <= (others => '0');
      hdr_data_val      <= '0';
    elsif rising_edge(clk) then
      -- input
      -- internal
      word_cnt          <= nxt_word_cnt;
      -- output
      i_hdr_words_arr   <= nxt_hdr_words_arr;  -- stored array of 11 words
      hdr_words_arr_val <= nxt_hdr_words_arr_val;
      i_hdr_fields_val  <= nxt_hdr_fields_val;
      i_hdr_data        <= nxt_hdr_data;  -- current word
      hdr_data_val      <= nxt_hdr_data_val;
    end if;
  end process;

  -- Extract the 11 words from the Ethernet header
  p_words : process(word_cnt, i_hdr_words_arr, i_hdr_fields_val, i_hdr_data, snk_in)
  begin
    nxt_word_cnt          <= word_cnt;
    nxt_hdr_words_arr     <= i_hdr_words_arr;
    nxt_hdr_words_arr_val <= (others => '0');
    nxt_hdr_fields_val    <= i_hdr_fields_val;
    nxt_hdr_data          <= i_hdr_data;
    nxt_hdr_data_val      <= '0';
    if snk_in.sop = '1' then
      nxt_hdr_fields_val <= (others => '0');
    end if;
    if snk_in.valid = '1' and word_cnt < c_network_total_header_32b_nof_words then
      -- new Ethernet header is arriving
      nxt_word_cnt                     <= word_cnt + 1;
      nxt_hdr_words_arr(     word_cnt) <= snk_in.data(c_eth_data_w - 1 downto 0);
      nxt_hdr_words_arr_val( word_cnt) <= '1';
      nxt_hdr_fields_val(word_cnt)     <= '1';
      nxt_hdr_data                     <= snk_in.data(c_eth_data_w - 1 downto 0);
      nxt_hdr_data_val                 <= '1';
    end if;
    if snk_in.eop = '1' then
      nxt_word_cnt <= 0;
    end if;
  end process;

  -- Combinatorial map of the total header on to the Ethernet header records
  hdr_fields.eth  <= func_network_total_header_extract_eth( i_hdr_words_arr);
  hdr_fields.arp  <= func_network_total_header_extract_arp( i_hdr_words_arr);
  hdr_fields.ip   <= func_network_total_header_extract_ip(  i_hdr_words_arr);
  hdr_fields.icmp <= func_network_total_header_extract_icmp(i_hdr_words_arr);
  hdr_fields.udp  <= func_network_total_header_extract_udp( i_hdr_words_arr);
end rtl;
