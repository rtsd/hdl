-----------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: The bf unit is a structure that places multiple single bf_unit
--          modules in parallel. It can be regarded as a hierarchical level in the design.
--
--          The bf entity takes the input data from the in_sosi_arr and feeds it to
--          all bf_unit modules.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use dp_lib.dp_stream_pkg.all;
use work.bf_pkg.all;

entity bf is
  generic (
    g_bf                    : t_c_bf  := c_bf;
    g_bf_weights_file_name  : string  := "UNUSED";
    g_ss_wide_file_prefix   : string  := "UNUSED";  -- path_to_file
    g_weights_write_only    : boolean := false  -- When set to TRUE the M9K blocks are forced to Simple Dual Port mode. When FALSE it is True Dual Port.
  );
  port (
    -- System
    dp_rst                  : in  std_logic := '0';
    dp_clk                  : in  std_logic;
    mm_rst                  : in  std_logic;
    mm_clk                  : in  std_logic;

    -- MM interface
    ram_ss_ss_wide_mosi     : in  t_mem_mosi;
    ram_ss_ss_wide_miso     : out t_mem_miso := c_mem_miso_rst;
    ram_bf_weights_mosi     : in  t_mem_mosi;
    ram_bf_weights_miso     : out t_mem_miso := c_mem_miso_rst;
    ram_st_sst_mosi         : in  t_mem_mosi;  -- Power statistics memory
    ram_st_sst_miso         : out t_mem_miso := c_mem_miso_rst;
    reg_st_sst_mosi         : in  t_mem_mosi;  -- Power statistics register
    reg_st_sst_miso         : out t_mem_miso := c_mem_miso_rst;

    -- ST interface
    in_sosi_arr             : in  t_dp_sosi_arr(g_bf.nof_input_streams - 1 downto 0);  -- subbands
    in_siso_arr             : out t_dp_siso_arr(g_bf.nof_input_streams - 1 downto 0);
    out_raw_sosi_arr        : out t_dp_sosi_arr(g_bf.nof_bf_units - 1 downto 0);  -- raw beamlets
    out_bst_sosi_arr        : out t_dp_sosi_arr(g_bf.nof_bf_units - 1 downto 0);  -- 16b beamlets
    out_qua_sosi_arr        : out t_dp_sosi_arr(g_bf.nof_bf_units - 1 downto 0)  -- 8b beamlets
  );
end bf;

architecture str of bf is
  constant c_frame_size_in       : positive := g_bf.nof_subbands * g_bf.nof_signal_paths / g_bf.nof_input_streams;
  constant c_reg_st_addr_w       : positive := ceil_log2(2);
  constant c_ram_st_addr_w       : positive := ceil_log2(g_bf.stat_data_sz * g_bf.nof_weights * c_nof_complex);  -- The "c_nof_complex" is due to the crosslets capability.

  type t_in_siso_2arr is array (integer range <>) of t_dp_siso_arr(g_bf.nof_input_streams - 1 downto 0);

  signal in_siso_2arr            : t_in_siso_2arr(g_bf.nof_bf_units - 1 downto 0);

  signal ram_bf_weights_mosi_arr : t_mem_mosi_arr(g_bf.nof_bf_units - 1 downto 0);
  signal ram_bf_weights_miso_arr : t_mem_miso_arr(g_bf.nof_bf_units - 1 downto 0);
  signal ram_st_sst_mosi_arr     : t_mem_mosi_arr(g_bf.nof_bf_units - 1 downto 0);
  signal ram_st_sst_miso_arr     : t_mem_miso_arr(g_bf.nof_bf_units - 1 downto 0);
  signal reg_st_sst_mosi_arr     : t_mem_mosi_arr(g_bf.nof_bf_units - 1 downto 0);
  signal reg_st_sst_miso_arr     : t_mem_miso_arr(g_bf.nof_bf_units - 1 downto 0);
  signal ram_ss_ss_wide_mosi_arr : t_mem_mosi_arr(g_bf.nof_bf_units - 1 downto 0);
  signal ram_ss_ss_wide_miso_arr : t_mem_miso_arr(g_bf.nof_bf_units - 1 downto 0);
begin
  ------------------------------------------------------------------------------
  -- The MM multiplexers
  ------------------------------------------------------------------------------

  -- Combine the internal array of mm interfaces for the weight factors to one array that is connected to the port of bf
  u_mem_mux_weight : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_bf.nof_bf_units,
    g_mult_addr_w => ceil_log2(g_bf.nof_signal_paths * g_bf.nof_weights)
  )
  port map (
    mosi     => ram_bf_weights_mosi,
    miso     => ram_bf_weights_miso,
    mosi_arr => ram_bf_weights_mosi_arr,
    miso_arr => ram_bf_weights_miso_arr
  );

  -- Combine the internal array of mm interfaces for the beamlet statistics to one array that is connected to the port of bf
  u_mem_mux_ram_bst : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_bf.nof_bf_units,
    g_mult_addr_w => c_ram_st_addr_w
  )
  port map (
    mosi     => ram_st_sst_mosi,
    miso     => ram_st_sst_miso,
    mosi_arr => ram_st_sst_mosi_arr,
    miso_arr => ram_st_sst_miso_arr
  );

  -- Combine the internal array of mm interfaces for the beamlet statistics to one array that is connected to the port of bf
  u_mem_mux_reg_bst : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_bf.nof_bf_units,
    g_mult_addr_w => c_reg_st_addr_w
  )
  port map (
    mosi     => reg_st_sst_mosi,
    miso     => reg_st_sst_miso,
    mosi_arr => reg_st_sst_mosi_arr,
    miso_arr => reg_st_sst_miso_arr
  );

  -- Combine the internal array of mm interfaces for the ss_wide to one array that is connected to the port of bf
  u_mem_mux_ss_wide : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => g_bf.nof_bf_units,
    g_mult_addr_w => ceil_log2(g_bf.nof_weights * g_bf.nof_signal_paths)
  )
  port map (
    mosi     => ram_ss_ss_wide_mosi,
    miso     => ram_ss_ss_wide_miso,
    mosi_arr => ram_ss_ss_wide_mosi_arr,
    miso_arr => ram_ss_ss_wide_miso_arr
  );

  ------------------------------------------------------------------------------
  -- The BF units
  ------------------------------------------------------------------------------
  -- Instantiate multiple BF units.
  gen_bf_units : for J in 0 to  g_bf.nof_bf_units - 1 generate
    u_bf_unit : entity work.bf_unit
    generic map (
      g_bf                   => g_bf,
      g_bf_weights_file_name => sel_a_b(g_bf_weights_file_name = "UNUSED", g_bf_weights_file_name, g_bf_weights_file_name & "_" & natural'image(J)),
      g_ss_wide_file_prefix  => sel_a_b(g_ss_wide_file_prefix = "UNUSED", g_ss_wide_file_prefix, g_ss_wide_file_prefix & "_" & natural'image(J)),
      g_weights_write_only   => g_weights_write_only
    )
    port map (
      -- System
      dp_rst                  =>  dp_rst,
      dp_clk                  =>  dp_clk,
      mm_rst                  =>  mm_rst,
      mm_clk                  =>  mm_clk,

      -- MM interface
      ram_ss_ss_wide_mosi     => ram_ss_ss_wide_mosi_arr(J),
      ram_ss_ss_wide_miso     => ram_ss_ss_wide_miso_arr(J),
      ram_bf_weights_mosi     => ram_bf_weights_mosi_arr(J),
      ram_bf_weights_miso     => ram_bf_weights_miso_arr(J),
      ram_st_sst_mosi         => ram_st_sst_mosi_arr(J),
      ram_st_sst_miso         => ram_st_sst_miso_arr(J),
      reg_st_sst_mosi         => reg_st_sst_mosi_arr(J),
      reg_st_sst_miso         => reg_st_sst_miso_arr(J),

      -- ST interface
      in_sosi_arr             => in_sosi_arr,
      in_siso_arr             => in_siso_2arr(J),
      out_raw_sosi            => out_raw_sosi_arr(J),
      out_bst_sosi            => out_bst_sosi_arr(J),
      out_qua_sosi            => out_qua_sosi_arr(J)
    );
  end generate gen_bf_units;

  in_siso_arr <= in_siso_2arr(0);
end str;
