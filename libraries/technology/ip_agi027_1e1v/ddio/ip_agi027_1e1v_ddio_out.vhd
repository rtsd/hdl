-- -----------------------------------------------------------------------------
--
-- Copyright 2023
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- Author:
--   D.F. Brouwer
-- Purpose:
--   RadioHDL wrapper for ip_agi027_1e1v_ddio_out_1 to support g_width >= 1
-- Reference:
--   Copied component declaration and instance example from
--   ip_arria10_e2sg/ddio/ip_arria10_e2sg_ddio_out.vhd and verified them against
--   generated/altera_gpio_2100/sim/ip_agi027_1e1v_ddio_out_1_altera_gpio_2100_e4tgwdq

library ieee;
use ieee.std_logic_1164.all;

entity ip_agi027_1e1v_ddio_out is
  generic(
    g_width : natural := 1
  );
  port (
    rst        : in   std_logic := '0';
    in_clk     : in   std_logic;
    in_clk_en  : in   std_logic := '1';  -- Not Connected
    in_dat_hi  : in   std_logic_vector(g_width - 1 downto 0);
    in_dat_lo  : in   std_logic_vector(g_width - 1 downto 0);
    out_dat    : out  std_logic_vector(g_width - 1 downto 0)
  );
end ip_agi027_1e1v_ddio_out;

architecture str of ip_agi027_1e1v_ddio_out is
  component ip_agi027_1e1v_ddio_out_1 is
        port (
                dataout  : out std_logic_vector(0 downto 0);  -- pad_out.export
                outclock : in  std_logic                    := '0';  -- ck.export
                aclr     : in  std_logic                    := '0';  -- aclr.export
                datain_h : in  std_logic_vector(0 downto 0) := (others => '0');  -- datain_h.fragment
                datain_l : in  std_logic_vector(0 downto 0) := (others => '0')  -- datain_l.fragment
        );
  end component;
begin
  gen_w : for I in g_width - 1 downto 0 generate
    u_ip_agi027_1e1v_ddio_out_1 : ip_agi027_1e1v_ddio_out_1
    port map (
      dataout  => out_dat(I downto I),
      outclock => in_clk,
      aclr     => rst,
      datain_h => in_dat_hi(I downto I),
      datain_l => in_dat_lo(I downto I)
    );
  end generate;
end str;
