--------------------------------------------------------------------------------
--
-- Copyright (C) 2017
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Simple counter with start, stop, step; used in dp_counter_func.
-- Description:
-- . range(0,2,1) = [0, 1] is the smallest count range allowed
-- Usage:
-- . Not for standalone use; part of dp_counter_func.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.dp_stream_pkg.all;

entity dp_counter_func_single is  -- FIXME move this to common
  generic (
    g_range_start : natural;  -- (start,stop,step) like python range(start, stop, step)
    g_range_stop  : natural;
    g_range_step  : natural
  );
  port (
    clk        : in  std_logic;
    rst        : in  std_logic;

    count_en   : in std_logic;
    check_max  : in std_logic := '1';
    count_offset : natural := 0;

    count      : out std_logic_vector(31 downto 0);
    count_init : out std_logic;  -- Pulses at first init
    count_min  : out std_logic;  -- Pulses when count=start
    count_max  : out std_logic  -- Pulses when count=max
  );
end dp_counter_func_single;

architecture rtl of dp_counter_func_single is
 -- The user defines the counters like a Python range(start,stop,step) in which the stop value
  -- is never actually reached. Calculate the actual maximum values here.
  -- . Example:
  --   . range(0,4,2) = [0, 2]
  --   . range(0,5,2) = [0, 2, 4]
  --   . range(0,6,2) = [0, 2, 4]
  --   . range(0,7,2) = [0, 2, 4, 6]
  --   . range(1,7,2) = [1, 3, 5]
  -- . The maximum value is: start+((stop-1-start)/step)*step
  constant c_nof_count : natural := (g_range_stop - 1 - g_range_start) / g_range_step + 1;
  constant c_count_max : natural := g_range_start + (c_nof_count - 1) * g_range_step;
  constant c_count_w   : natural := ceil_log2(c_count_max + 1);

  type t_reg is record
    count_en  : std_logic;
    count     : std_logic_vector(c_count_w - 1 downto 0);
    count_min : std_logic;
    count_max : std_logic;
    count_init : std_logic;
  end record;

  signal r, nxt_r : t_reg;
begin
  --------------------------------------------------------------------------------
  -- Combinational logic
  --------------------------------------------------------------------------------
  p_comb : process(rst, r, count_en, check_max, count_offset)
    variable v : t_reg;
  begin
    v           := r;
    v.count_min := '0';
    v.count_max := '0';
    v.count_init := '0';

    if count_en = '1' then

      -- Start counting / init
      if r.count_en = '0' then
        v.count_en := '1';
        v.count := TO_UVEC(g_range_start + count_offset, c_count_w);
        v.count_min := '1';
        v.count_init := '1';
      -- keep counting
      else
        v.count := INCR_UVEC(r.count, g_range_step);
        if c_count_max > 0 and check_max = '1' and r.count = TO_UVEC(c_count_max - g_range_step, c_count_w) then  -- count max almost reached
          v.count_max := '1';
        elsif r.count = TO_UVEC(c_count_max, c_count_w) then  -- count max reached
          -- Reset count to start value
          v.count := TO_UVEC(g_range_start, c_count_w);
          v.count_min := '1';
        end if;
      end if;

      -- If the maximum count is 0, count_max is always high.
      if c_count_max = 0 then
        v.count_max := '1';
      end if;

    elsif check_max = '1' and r.count = TO_UVEC(c_count_max, c_count_w) then  -- count max reached
      v.count_max := '1';
    end if;

    if rst = '1' then
      v.count_en  := '0';
      v.count     := (others => '0');
      v.count_min := '0';
      v.count_max := '0';
      v.count_init := '0';
    end if;

    nxt_r <= v;
  end process;

  --------------------------------------------------------------------------------
  -- Register stage
  --------------------------------------------------------------------------------
  r <= nxt_r when rising_edge(clk);

  --------------------------------------------------------------------------------
  -- Outputs
  --------------------------------------------------------------------------------
  count(31 downto c_count_w)  <= (others => '0');
  count(c_count_w - 1 downto 0) <= nxt_r.count;
  count_init <= nxt_r.count_init;
  count_min  <= nxt_r.count_min;
  count_max  <= nxt_r.count_max;
end rtl;
