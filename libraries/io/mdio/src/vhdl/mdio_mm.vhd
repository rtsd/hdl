-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.mdio_pkg.all;

entity mdio_mm is
  port (
    rst                 : in  std_logic;
    clk                 : in  std_logic;

    ---------------------------------------------------------------------------
    -- Avalon Memory Mapped Slave interface with Interrupt
    ---------------------------------------------------------------------------
    -- MM slave mdio header register
    mms_header_address  : in  std_logic_vector(0 downto 0);
    mms_header_write    : in  std_logic;
    mms_header_read     : in  std_logic;
    mms_header_writedata: in  std_logic_vector(c_halfword_w - 1 downto 0);  -- 16-bit
    mms_header_readdata : out std_logic_vector(c_halfword_w - 1 downto 0);
    -- MM slave mdio data register
    mms_data_address    : in  std_logic_vector(0 downto 0);
    mms_data_write      : in  std_logic;
    mms_data_read       : in  std_logic;
    mms_data_writedata  : in  std_logic_vector(c_halfword_w - 1 downto 0);  -- 16-bit
    mms_data_readdata   : out std_logic_vector(c_halfword_w - 1 downto 0);
    -- Interrupt
    ins_mdio_rdy        : out std_logic;

    ---------------------------------------------------------------------------
    -- MDIO PHY control interface
    ---------------------------------------------------------------------------
    -- MDIO access control
    mdio_en_evt         : out std_logic;  -- pulse to start new MDIO access
    mdio_done_evt       : in  std_logic;  -- pulse from MDIO access finished
    -- MDIO header and tx/rx data
    mdio_hdr            : out std_logic_vector(c_halfword_w - 1 downto 0);
    mdio_tx_dat         : out std_logic_vector(c_halfword_w - 1 downto 0);
    mdio_rx_dat         : in  std_logic_vector(c_halfword_w - 1 downto 0)
  );
end mdio_mm;

architecture str of mdio_mm is
  -- Use MM bus data width = c_halfword_w
  constant c_reg_mdio  : t_c_mem := (latency  => 1,
                                     adr_w    => 1,
                                     dat_w    => c_halfword_w,
                                     nof_dat  => 1,
                                     init_sl  => 'X');

  signal header_write              : std_logic;  -- used to issue mdio access enable
  signal data_read                 : std_logic;  -- used to clear mdio access ready interrupt

  signal reg_header                : std_logic_vector(c_halfword_w - 1 downto 0);
  signal reg_data                  : std_logic_vector(c_halfword_w - 1 downto 0);
  signal reg_data_rd               : std_logic_vector(reg_data'range);
begin
  -- MDIO access enable when the MDIO header register is written.
  -- Hence for a MDIO write the MDIO data register needs to have been written first.
  header_write <= mms_header_write;  -- only one reg, so no need to check mms_header_address

  u_mdio_en : entity common_lib.common_request
  port map (
    rst         => rst,
    clk         => clk,
    in_req      => header_write,
    out_req_evt => mdio_en_evt
  );

  -- MDIO ready interrupt when the MDIO PHY access has been done
  -- Clear the interrupt by reading the MDIO data register
  data_read <= mms_data_read;  -- only one reg, so no need to check mms_data_address

  u_mdio_done : entity common_lib.common_switch
  port map (
    clk         => clk,
    rst         => rst,
    switch_high => mdio_done_evt,
    switch_low  => data_read,
    out_level   => ins_mdio_rdy
  );

  -- MDIO register
  mdio_hdr    <= reg_header;
  mdio_tx_dat <= reg_data;

  reg_data_rd <= mdio_rx_dat;  -- always read rx_dat

  u_reg_header : entity common_lib.common_reg_r_w
  generic map (
    g_reg       => c_reg_mdio
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- control side
    wr_en       => mms_header_write,
    wr_adr      => mms_header_address,
    wr_dat      => mms_header_writedata,
    rd_en       => mms_header_read,
    rd_adr      => mms_header_address,
    rd_dat      => mms_header_readdata,
    rd_val      => OPEN,
    -- data side
    out_reg     => reg_header,
    in_reg      => reg_header
  );

  u_reg_data : entity common_lib.common_reg_r_w
  generic map (
    g_reg       => c_reg_mdio
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- control side
    wr_en       => mms_data_write,
    wr_adr      => mms_data_address,
    wr_dat      => mms_data_writedata,
    rd_en       => mms_data_read,
    rd_adr      => mms_data_address,
    rd_dat      => mms_data_readdata,
    rd_val      => OPEN,
    -- data side
    out_reg     => reg_data,
    in_reg      => reg_data_rd
  );
end str;
