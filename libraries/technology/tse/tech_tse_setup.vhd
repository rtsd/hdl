-------------------------------------------------------------------------------
--
-- Copyright 2022
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
-- AUthor: E. Kooistra
-- Purpose: Set up TSE via MM
-- Description:
-- . TSE set up as in tb_tech_tse_pkg, unb_osy/unbos_eth.c and
--   eth1g_master.vhd. Cannot use proc_mem_mm_bus_*() because a synthesis
--   process can only have one rising_edge(mm_clk) statement
-- . After tse_init is done, then connect to external MM controller, to allow
--   external  monitoring of the TSE.

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use work.tech_tse_pkg.all;

entity tech_tse_setup is
  generic (
    g_sim : boolean;
    -- Nios 1GbE-I uses ETH_FRAME_LENGTH = 1518 in inbos_eth.h. Use g_jumbo_en
    -- = FALSE for frame_len <= 1500 octets. If frame is longer then this
    -- yields invalid length flag in rx_sosi.err, but data is still received.
    -- Use g_jumbo_en = TRUE for frame_len <= 9000 octets (jumbo frames).
    g_jumbo_en : boolean := true
  );
  port (
    -- Clocks and reset
    mm_rst         : in  std_logic;
    mm_clk         : in  std_logic;

    -- TSE setup
    src_mac        : in std_logic_vector(c_48 - 1 downto 0);
    setup_done     : out std_logic;

    -- Memory Mapped Peripheral
    -- . Controller side
    mm_ctlr_copi   : in  t_mem_copi;
    mm_ctlr_cipo   : out t_mem_cipo;
    -- . Peripheral side
    mm_peri_copi   : out t_mem_copi;
    mm_peri_cipo   : in  t_mem_cipo
  );
end tech_tse_setup;

architecture rtl of tech_tse_setup is
  -- FALSE receive only frames for this src_mac and broadcast, TRUE receive all
  constant c_promis_en  : boolean := false;

  -- Access the MM bus
  type t_state is (s_rd_pcs_rev, s_wr_if_mode, s_rd_control, s_rd_status, s_wr_control,
                   s_rd_mac_rev, s_wr_promis_en, s_wr_mac_0, s_wr_mac_1, s_wr_tx_ipg_len, s_wr_frm_len,
                   s_wr_rx_section_empty, s_wr_rx_section_full, s_wr_tx_section_empty, s_wr_tx_section_full,
                   s_wr_rx_almost_empty, s_wr_rx_almost_full, s_wr_tx_almost_empty, s_wr_tx_almost_full,
                   s_rd_tx_cmd_stat, s_rd_rx_cmd_stat,
                   s_done);

  signal state           : t_state;
  signal next_state      : t_state;
  signal psc_access      : std_logic;  -- active during PCS registers access, for view in Wave window
  signal fifo_access     : std_logic;  -- active during FIFO registers access, for view in Wave window

  -- Memory Mapped Slave
  signal tse_init        : std_logic := '1';
  signal tse_ctlr_copi   : t_mem_copi;
  signal tse_ctlr_cipo   : t_mem_cipo;
  signal tse_waitrequest : std_logic;
  signal tse_wrdata      : std_logic_vector(c_word_w - 1 downto 0);  -- for view in Wave window
  signal tse_rddata      : std_logic_vector(c_word_w - 1 downto 0);  -- for view in Wave window

  signal src_mac_0       : std_logic_vector(c_32 - 1 downto 0);
  signal src_mac_1       : std_logic_vector(c_16 - 1 downto 0);
begin
  setup_done <= not tse_init;

  src_mac_0 <= hton(src_mac(c_48 - 1 downto c_16), 4);
  src_mac_1 <= hton(src_mac(c_16 - 1 downto  0), 2);

  -- Select MM interface controller
  --               ___
  --              |   |
  --  mm_ctlr ----| 0 |
  --              |   |---- mm_peri
  -- tse_ctlr ----| 1 |
  --              |___|
  --                |
  -- tse_init ------/
  --
  mm_peri_copi <= tse_ctlr_copi  when tse_init = '1' else mm_ctlr_copi;
  mm_ctlr_cipo <= c_mem_cipo_rst when tse_init = '1' else mm_peri_cipo;
  tse_ctlr_cipo <= mm_peri_cipo;
  tse_waitrequest <= tse_ctlr_cipo.waitrequest;
  tse_wrdata <= tse_ctlr_copi.wrdata(c_word_w - 1 downto 0);
  tse_rddata <= tse_ctlr_cipo.rddata(c_word_w - 1 downto 0);

  p_state : process(mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      state <= s_rd_pcs_rev;
      next_state <= s_rd_pcs_rev;
      tse_init <= '1';
      tse_ctlr_copi <= c_mem_copi_rst;
      psc_access <= '0';
      fifo_access <= '0';
    elsif rising_edge(mm_clk) then
      tse_init <= '1';
      psc_access <= '0';
      fifo_access <= '0';

      -- Issue MM access
      case state is
        -- PSC control
        when s_rd_pcs_rev =>
          psc_access <= '1';
          proc_mem_bus_rd(func_tech_tse_map_pcs_addr(16#22#), tse_ctlr_copi);  -- REV --> 0x0901, 0x1304
          next_state <= s_wr_if_mode;

        when s_wr_if_mode =>
          psc_access <= '1';
          proc_mem_bus_wr(func_tech_tse_map_pcs_addr(16#28#), 16#0008#, tse_ctlr_copi);  -- IF_MODE <-- Force 1GbE,
          next_state <= s_rd_control;

        when s_rd_control =>
          psc_access <= '1';
          proc_mem_bus_rd(func_tech_tse_map_pcs_addr(16#00#), tse_ctlr_copi);  -- CONTROL --> 0x1140
          next_state <= s_rd_status;

        when s_rd_status =>
          psc_access <= '1';
          proc_mem_bus_rd(func_tech_tse_map_pcs_addr(16#02#), tse_ctlr_copi);  -- STATUS --> 0x000D
          next_state <= s_wr_control;

        when s_wr_control =>
          psc_access <= '1';
          if g_sim = false then
            proc_mem_bus_wr(func_tech_tse_map_pcs_addr(16#00#), 16#1140#, tse_ctlr_copi);  -- CONTROL <-- Keep auto negotiate enabled (is reset default)
          else
            proc_mem_bus_wr(func_tech_tse_map_pcs_addr(16#00#), 16#0140#, tse_ctlr_copi);  -- CONTROL <-- In simulation disable auto negotiate
          end if;
          next_state <= s_rd_mac_rev;

        -- MAC control
        when s_rd_mac_rev =>
          proc_mem_bus_rd(16#000#, tse_ctlr_copi);  -- REV --> CUST_VERSION & 0x0901
          next_state <= s_wr_promis_en;

        when s_wr_promis_en =>
          if c_promis_en = false then
            proc_mem_bus_wr(16#008#, 16#0100004B#, tse_ctlr_copi);  -- COMMAND_CONFIG
          else
            proc_mem_bus_wr(16#008#, 16#0100005B#, tse_ctlr_copi);
          end if;
          next_state <= s_wr_mac_0;

        when s_wr_mac_0 =>
          proc_mem_bus_wr(16#00C#, src_mac_0, tse_ctlr_copi);  -- MAC_0
          next_state <= s_wr_mac_1;

        when s_wr_mac_1 =>
          proc_mem_bus_wr(16#010#, src_mac_1, tse_ctlr_copi);  -- MAC_1 <-- SRC_MAC
          next_state <= s_wr_tx_ipg_len;

        when s_wr_tx_ipg_len =>
          proc_mem_bus_wr(16#05C#, 16#0000000C#, tse_ctlr_copi);  -- TX_IPG_LENGTH <-- interpacket gap = 12
          next_state <= s_wr_frm_len;

        when s_wr_frm_len =>
          if g_jumbo_en = false then
            proc_mem_bus_wr(16#014#, 16#000005EE#, tse_ctlr_copi);  -- FRM_LENGTH <-- receive max frame length = 1518
          else
            proc_mem_bus_wr(16#014#, 16#0000233A#, tse_ctlr_copi);  -- FRM_LENGTH <-- receive max frame length = 9018
          end if;
          next_state <= s_wr_rx_section_empty;

        -- MAC FIFO
        when s_wr_rx_section_empty =>
          fifo_access <= '1';
          proc_mem_bus_wr(16#01C#, c_tech_tse_rx_fifo_depth - 16, tse_ctlr_copi);  -- RX_SECTION_EMPTY <-- default FIFO depth - 16, >3
          next_state <= s_wr_rx_section_full;

        when s_wr_rx_section_full =>
          fifo_access <= '1';
          proc_mem_bus_wr(16#020#, 16, tse_ctlr_copi);  -- RX_SECTION_FULL <-- default 16
          next_state <= s_wr_tx_section_empty;

        when s_wr_tx_section_empty =>
          fifo_access <= '1';
          proc_mem_bus_wr(16#024#, c_tech_tse_tx_fifo_depth - 16, tse_ctlr_copi);  -- TX_SECTION_EMPTY <-- default FIFO depth - 16, >3
          next_state <= s_wr_tx_section_full;

        when s_wr_tx_section_full =>
          fifo_access <= '1';
          proc_mem_bus_wr(16#028#, 16, tse_ctlr_copi);  -- TX_SECTION_FULL <-- default 16, >~ 8 otherwise no tx
          next_state <= s_wr_rx_almost_empty;

        when s_wr_rx_almost_empty =>
          fifo_access <= '1';
          proc_mem_bus_wr(16#02C#, 8, tse_ctlr_copi);  -- RX_ALMOST_EMPTY <-- default 8
          next_state <= s_wr_rx_almost_full;

        when s_wr_rx_almost_full =>
          fifo_access <= '1';
          proc_mem_bus_wr(16#030#, 8, tse_ctlr_copi);  -- RX_ALMOST_FULL <-- default 8
          next_state <= s_wr_tx_almost_empty;

        when s_wr_tx_almost_empty =>
          fifo_access <= '1';
          proc_mem_bus_wr(16#034#, 8, tse_ctlr_copi);  -- TX_ALMOST_EMPTY  <-- default 8
          next_state <= s_wr_tx_almost_full;

        when s_wr_tx_almost_full =>
          fifo_access <= '1';
          proc_mem_bus_wr(16#038#, c_tech_tse_tx_ready_latency + 3, tse_ctlr_copi);  -- TX_ALMOST_FULL   <-- default 3
          next_state <= s_rd_tx_cmd_stat;

        -- MAC status
        when s_rd_tx_cmd_stat =>
          proc_mem_bus_rd(16#0E8#, tse_ctlr_copi);  -- TX_CMD_STAT --> 0x00040000 : [18]=1 TX_SHIFT16, [17]=0 OMIT_CRC
          next_state <= s_rd_rx_cmd_stat;

        when s_rd_rx_cmd_stat =>
          proc_mem_bus_rd(16#0EC#, tse_ctlr_copi);  -- RX_CMD_STAT --> 0x02000000 : [25]=1 RX_SHIFT16
          next_state <= s_done;

        when others =>  -- s_done
          tse_init <= '0';
      end case;

      -- Go to next state when MM access was accepted
      if state /= next_state and tse_waitrequest = '0' then
        tse_ctlr_copi.wr <= '0';
        tse_ctlr_copi.rd <= '0';
        state <= next_state;
      end if;
    end if;
  end process;
end architecture;
