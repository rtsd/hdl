-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: P. Donker, R van der Walle, E. Kooistra

-- Purpose:
-- . SDP statistics offload
-- Description:
-- [1] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+filterbank
--     . See figure 4.3
-- [2] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+Correlator
--     . See Figure 3.7
-- [3] https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD
--     . See 2.9.4 Station Control (L3-SC) - SDP Firmware (L4-SDPFW)
--
-- . endianess
--   Within a 32bit MM word the values are stored with LSByte at lowest byte
--   address (so relative byte address 0) and MSByte at highest byte address
--   (so relative byte address 3). Internally in the RAM the MM words are thus
--   stored with LSByte first, which is called little endian. However,
--   externally the MM words are send with MSByte first, so in the statistics
--   offload header and offload payload the 32bit words are send with MSByte
--   first, which is called big endian.
--   The dp_offload_tx_v3 sends all multi byte header fields in big endian
--   order, hence also the 16bit, 24bit and 64bit values are send MSByte
--   first. The internet packet headers also use big endian, which is
--   therefore also called network order. Hence the entire statistics offload
--   packet is in big endian order.
--   If values are shown with first byte received left and last byte received
--   right, then big endian hex values can directly be interpreted when read
--   by a human, because they are shown in the normal order.
--
-- . g_reverse_word_order
--   The statistics consist of c_sdp_W_statistic_sz = 2 MM words that are
--   with LSWord at low (even) word address and MSWord at high (odd) word
--   address. Default the statistics offload reads the low address first, but
--   the statistics have to be send with MSWord first. Therefore the read
--   order needs to be reversed per statistic value, so g_reverse_word_order
--   = TRUE. The combination of sending the MSWord first and the MSByte first
--   results that the entire 64b statistics words are send in big endian
--   order.
--
--   Stored order:
--   word address:   0,   1,   2,   3
--   SST            Ul,  Uh
--   BST            Xl,  Xh,  Yl,  Yh
--   XST            Rl,  Rh,  Il,  Ih
--
--   The g_reverse_word_order = TRUE is needed to achieve have MSWord first
--   and 64bit big endian.
--   The g_user_size defines the number of words that get reversed:
--   . For the SST there is only one uint64 part, so g_user_size = 2.
--   . For the BST the X and Y polarization parts are treated as an array of
--     [N_pol_bf], so index 0 = X is send first and therefore g_user_size = 2
--     to preserve the polarization X, Y order.
--   . For the XST the Re and Im complex parts of cint64 are treated as an
--     array of [N_complex], so index 0 = Re is send first and therefore
--     g_user_size = 2 to preserve the complex Re, Im order.
--
--   The () show the parts that are contained in g_user_size and that got
--   reversed by g_reverse_word_order = TRUE compared to the stored order:
--
--   Transport order:                   (g_user_size)
--   SST           (Uh,  Ul),                2
--   BST           (Xh,  Xl), (Yh,  Yl),     2   keep X, Y parts order
--   XST           (Rh,  Rl), (Ih,  Il),     2   keep Re, Im parts order
--
-- . g_P_sq and p.nof_used_P_sq
--   The g_P_sq defines the number of correlator cells that is available in
--   the SDPFW. Use generic to support P_sq = 1 for one node and P_sq =
--   c_sdp_P_sq for multiple nodes (with ring).
--   The p.nof_used_P_sq is the number of correlator cells that is actually
--   used and that will output XST packets. Unused correlator cells yield
--   zero data that should not be output. The p.nof_used_P_sq is the smallest
--   of g_P_sq and ring_info.N_rn/2 + 1. In this way the XST offload can work
--   with g_P_sq = 1 when N_rn > 1 and also in a ring with N_rn < N_pn when
--   g_P_sq = 9.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib, dp_lib, ring_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_layers_pkg.all;
use dp_lib.dp_stream_pkg.all;
use ring_lib.ring_pkg.all;
use work.sdp_pkg.all;

entity sdp_statistics_offload is
  generic (
    g_statistics_type          : string  := "SST";
    g_offload_node_time        : natural := c_sdp_offload_node_time;
    g_offload_packet_time      : natural := c_sdp_offload_packet_time;
    g_offload_scale_w          : natural := 0;  -- 0 = default
    g_ctrl_interval_size_min   : natural := 1;  -- 1 = default
    g_beamset_id               : natural := 0;
    g_P_sq                     : natural := c_sdp_P_sq;  -- number of available correlator cells,
    g_crosslets_direction      : natural := 1;  -- > 0 for crosslet transport in positive direction (incrementing RN),
                                                -- else 0 for negative direction
    g_reverse_word_order       : boolean := true;  -- default word order is MSB after LSB, we need to stream LSB
                                                   -- after MSB.
    g_bsn_monitor_sync_timeout : natural := c_sdp_N_clk_sync_timeout
  );
  port (
    -- Clocks and reset
    mm_rst : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk : in  std_logic;  -- memory-mapped bus clock

    dp_clk : in  std_logic;
    dp_rst : in  std_logic;

    -- Memory access to statistics values
    master_mosi      : out  t_mem_mosi;  -- := c_mem_mosi_rst;
    master_miso      : in t_mem_miso;

    -- Memory access to read/write settings
    reg_enable_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_enable_miso  : out t_mem_miso;

    reg_hdr_dat_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    reg_hdr_dat_miso : out t_mem_miso;

    -- Memory access bsn monitor udp offload
    reg_bsn_monitor_v2_offload_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_offload_cipo : out t_mem_cipo;

    -- Input timing regarding the integration interval of the statistics
    in_sosi            : in t_dp_sosi;
    new_interval       : in std_logic;
    ctrl_interval_size : in natural := 1;  -- 1 = default

    -- Streaming output of offloaded statistics packets
    out_sosi         : out t_dp_sosi;
    out_siso         : in t_dp_siso;

    -- inputs from other blocks
    eth_src_mac             : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    udp_src_port            : in std_logic_vector(c_network_udp_port_w - 1 downto 0);
    ip_src_addr             : in std_logic_vector(c_network_ip_addr_w - 1 downto 0);

    gn_index                : in natural;
    ring_info               : in t_ring_info := c_ring_info_rst;  -- only needed for XST
    sdp_info                : in t_sdp_info;
    weighted_subbands_flag  : in std_logic := '0';

    nof_crosslets           : in std_logic_vector(c_sdp_nof_crosslets_reg_w - 1 downto 0) :=
                                                  (others => '0');  -- from MM
    prev_crosslets_info_rec : in t_sdp_crosslets_info := c_sdp_crosslets_info_rst
  );
end sdp_statistics_offload;

architecture str of sdp_statistics_offload is
  constant c_nof_streams               : natural := 1;

  -- Offload timing
  constant c_offload_node_time_scaled : natural := (g_offload_node_time * 2**g_offload_scale_w) /
                                                    g_ctrl_interval_size_min;
  constant c_offload_packet_time_scaled : natural := (g_offload_packet_time * 2**g_offload_scale_w) /
                                                      g_ctrl_interval_size_min;
  constant c_offload_packet_time_max : natural := g_offload_packet_time *
                                                  (c_sdp_N_clk_per_sync_max / c_sdp_N_clk_per_sync_min + 1) + 1;
                                                  -- +1 for margin and to ensure > 0

  -- header fields
  constant c_hfa                       : t_common_field_arr(c_sdp_stat_hdr_field_arr'range) := c_sdp_stat_hdr_field_arr;
  constant c_marker                    : natural := func_sdp_get_stat_marker(g_statistics_type);
  constant c_nof_signal_inputs         : natural := func_sdp_get_stat_nof_signal_inputs(g_statistics_type);
  constant c_nof_statistics_per_packet : natural := func_sdp_get_stat_nof_statistics_per_packet(g_statistics_type);
  constant c_udp_total_length          : natural := func_sdp_get_stat_udp_total_length(g_statistics_type);
  constant c_ip_total_length           : natural := func_sdp_get_stat_ip_total_length(g_statistics_type);
  constant c_nof_packets_max           : natural := func_sdp_get_stat_nof_packets(g_statistics_type,
                                                                             c_sdp_S_pn, g_P_sq, c_sdp_N_crosslets_max);

  constant c_beamlet_id                : natural := g_beamset_id * c_sdp_S_sub_bf;

  -- MM access settings per packet for u_dp_block_from_mm_dc
  constant c_mm_user_size              : natural := func_sdp_get_stat_from_mm_user_size(g_statistics_type);
  constant c_mm_data_size              : natural := func_sdp_get_stat_from_mm_data_size(g_statistics_type);
  constant c_mm_step_size              : natural := func_sdp_get_stat_from_mm_step_size(g_statistics_type);
  constant c_mm_nof_data               : natural := func_sdp_get_stat_from_mm_nof_data(g_statistics_type);
  constant c_mm_ram_size               : natural := c_mm_nof_data * c_mm_data_size * c_nof_packets_max;

  -- Parameters that are fixed per node
  type t_parameters is record
    gn_index             : natural;  -- index of this global node
    pn_index             : natural;  -- index of this node in antenna band
    rn_index             : natural;  -- index of this ring node
    local_si_offset      : natural;  -- index of first signal input on this node
    remote_rn            : natural;  -- index of remote ring node
    remote_gn            : natural;  -- index of remote global node
    remote_pn            : natural;  -- index of remote node in antenna band
    remote_si_offset     : natural;  -- index of first signal input on remote node
    offload_node_time    : natural;  -- unit offload delay between nodes
    offload_packet_time  : natural;  -- unit offload delay between packets per node
    base_dly             : natural;  -- same base offload delay for nof_cycles_dly per node
    nodes_dly            : natural;  -- incremental offload delay for nof_cycles_dly per node
    nof_cycles_dly       : natural;  -- trigger_offload delay for this node
    offset_rn            : natural;  -- = ring_info.O_rn, GN index of first ring node
    nof_rn               : natural;  -- = ring_info.N_rn, number of GN in the ring
    nof_used_P_sq        : natural;  -- number of used correlator cells <= g_P_sq (is number of available correlator
                                     -- cells)
  end record;

  -- Input capture per sync interval
  type t_input is record
    nof_crosslets        : natural range 0 to c_sdp_N_crosslets_max;  -- nof_crosslets from MM
    nof_packets          : natural;  -- nof XST offload packets per integration interval dependend on nof_crosslets
    bsn_at_sync          : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
    integration_interval : natural;
    crosslets_info_rec   : t_sdp_crosslets_info;
    sop_cnt              : natural;
    payload_err          : std_logic;
  end record;

  constant c_input_rst : t_input := (0, 0, (others => '0'), 0, c_sdp_crosslets_info_rst, 0, '0');  -- to avoid initial
                                                                                                   -- 'X' in sim

  -- Offload control
  type t_reg is record
    packet_count         : natural range 0 to c_nof_packets_max;
    start_address        : natural range 0 to c_mm_ram_size;
    start_timer          : natural range 0 to c_offload_packet_time_max;
    start_pulse          : std_logic;
    start_sync           : std_logic;
    dp_header_info       : std_logic_vector(1023 downto 0);
    interleave_count     : natural range 0 to c_sdp_Q_fft;
    interleave_address   : natural range 0 to c_mm_ram_size;
    crosslet_count       : natural range 0 to c_sdp_N_crosslets_max;
    instance_count       : natural range 0 to c_sdp_P_sq;
    instance_address     : natural range 0 to c_mm_ram_size;
  end record;

  constant c_reg_rst : t_reg := (0, 0, 0, '0', '0', (others => '0'), 0, 0, 0, 0, 0);

  signal p                        : t_parameters;
  signal p_gn_id                  : std_logic_vector(c_byte_w - 1 downto 0);
  signal pp_gn_id                 : std_logic_vector(c_byte_w - 1 downto 0);

  signal reg_input                : t_input := c_input_rst;
  signal prev_input               : t_input := c_input_rst;
  signal hdr_input                : t_input := c_input_rst;

  signal r                        : t_reg;
  signal nxt_r                    : t_reg;

  signal reg_new_interval         : std_logic;
  signal ctrl_interval_scaled     : natural;

  signal data_id_rec              : t_sdp_stat_data_id;
  signal data_id_slv              : std_logic_vector(31 downto 0) := (others => '0');

  signal in_trigger               : std_logic;
  signal trigger_en               : std_logic := '0';
  signal trigger_offload          : std_logic := '0';
  signal mm_done                  : std_logic := '0';
  signal dp_sop                   : std_logic := '0';
  signal dp_block_from_mm_src_out : t_dp_sosi;
  signal dp_block_from_mm_src_in  : t_dp_siso;

  signal dp_offload_snk_in        : t_dp_sosi;
  signal dp_offload_snk_out       : t_dp_siso;

  signal udp_sosi                 : t_dp_sosi;

  signal dp_header_info           : std_logic_vector(1023 downto 0) := (others => '0');
  signal r_dp_header_sop          : std_logic;
  signal r_dp_header_rec          : t_sdp_stat_header;
  signal fsub_type                : std_logic := '0';
  signal station_info             : std_logic_vector(15 downto 0) := (others => '0');

  -- Debug signals for view in Wave window
  signal dbg_g_offload_node_time         : natural := g_offload_node_time;
  signal dbg_g_offload_packet_time       : natural := g_offload_packet_time;
  signal dbg_c_marker                    : natural := c_marker;
  signal dbg_c_nof_signal_inputs         : natural := c_nof_signal_inputs;
  signal dbg_c_nof_statistics_per_packet : natural := c_nof_statistics_per_packet;
  signal dbg_c_udp_total_length          : natural := c_udp_total_length;
  signal dbg_c_ip_total_length           : natural := c_ip_total_length;
  signal dbg_c_nof_packets_max           : natural := c_nof_packets_max;
  signal dbg_c_beamlet_id                : natural := c_beamlet_id;
  signal dbg_c_mm_data_size              : natural := c_mm_data_size;
  signal dbg_c_mm_step_size              : natural := c_mm_step_size;
  signal dbg_c_mm_nof_data               : natural := c_mm_nof_data;
begin
  -------------------------------------------------------------------------------
  -- Assemble offload header info
  -------------------------------------------------------------------------------
  -- Whether the dp_offload_tx_hdr_fields value is actually used in the Tx header depends on c_sdp_stat_hdr_field_sel
  --   c_sdp_stat_hdr_field_sel = "1" & "101" & "111011111001" & "0100" & "0100" & "00000000" & "1000000" & "0"
  --                                     eth     ip               udp      app
  --   where 0 = data path, 1 = MM controlled. The '0' fields are assigned here in dp_header_info
  --   in order:
  --     access   field
  --     MM       word_align
  --
  --     MM       eth_dst_mac
  --        DP    eth_src_mac
  --     MM       eth_type
  --
  --     MM       ip_version
  --     MM       ip_header_length
  --     MM       ip_services
  --        DP    ip_total_length
  --     MM       ip_identification
  --     MM       ip_flags
  --     MM       ip_fragment_offset
  --     MM       ip_time_to_live
  --     MM       ip_protocol
  --        DP    ip_header_checksum --> not here, will be filled in by 1GbE eth component
  --        DP    ip_src_addr
  --     MM       ip_dst_addr
  --
  --        DP    udp_src_port
  --     MM       udp_dst_port
  --        DP    udp_total_length
  --        DP    udp_checksum --> default fixed 0, so not used, not calculated here or in 1GbE
  --                               eth component because would require store and forward
  --
  --        DP    sdp_marker
  --     MM       sdp_version_id
  --        DP    sdp_observation_id
  --        DP    sdp_station_info
  --
  --        DP    sdp_source_info_antenna_band_id
  --        DP    sdp_source_info_nyquist_zone_id
  --        DP    sdp_source_info_f_adc
  --        DP    sdp_source_info_fsub_type
  --        DP    sdp_source_info_payload_error
  --        DP    sdp_source_info_beam_repositioning_flag
  --        DP    sdp_source_info_weighted_subbands_flag
  --     MM DP    sdp_source_info_gn_id
  --
  --     MM       sdp_reserved
  --        DP    sdp_integration_interval
  --        DP    sdp_data_id
  --        DP    sdp_nof_signal_inputs
  --        DP    sdp_nof_bytes_per_statistic
  --        DP    sdp_nof_statistics_per_packet
  --        DP    sdp_block_period
  --
  --        DP    dp_bsn

  station_info <= sdp_info.antenna_field_index & sdp_info.station_id;

  dp_header_info(field_hi(c_hfa, "eth_src_mac"                            ) downto
                 field_lo(c_hfa, "eth_src_mac"                            )) <= eth_src_mac;
  dp_header_info(field_hi(c_hfa, "ip_total_length"                        ) downto
                 field_lo(c_hfa, "ip_total_length"                        )) <= TO_UVEC(c_ip_total_length, 16);
  dp_header_info(field_hi(c_hfa, "ip_src_addr"                            ) downto
                 field_lo(c_hfa, "ip_src_addr"                            )) <= ip_src_addr;
  dp_header_info(field_hi(c_hfa, "udp_src_port"                           ) downto
                 field_lo(c_hfa, "udp_src_port"                           )) <= udp_src_port;
  dp_header_info(field_hi(c_hfa, "udp_total_length"                       ) downto
                 field_lo(c_hfa, "udp_total_length"                       )) <= TO_UVEC(c_udp_total_length, 16);
  dp_header_info(field_hi(c_hfa, "sdp_marker"                             ) downto
                 field_lo(c_hfa, "sdp_marker"                             )) <= TO_UVEC(c_marker, 8);
  dp_header_info(field_hi(c_hfa, "sdp_observation_id"                     ) downto
                 field_lo(c_hfa, "sdp_observation_id"                     )) <= sdp_info.observation_id;
  dp_header_info(field_hi(c_hfa, "sdp_station_info"                       ) downto
                 field_lo(c_hfa, "sdp_station_info"                       )) <= station_info;
  dp_header_info(field_hi(c_hfa, "sdp_source_info_antenna_band_id"        ) downto
                 field_lo(c_hfa, "sdp_source_info_antenna_band_id"        )) <= SLV(sdp_info.antenna_band_index);
  dp_header_info(field_hi(c_hfa, "sdp_source_info_nyquist_zone_id"        ) downto
                 field_lo(c_hfa, "sdp_source_info_nyquist_zone_id"        )) <= sdp_info.nyquist_zone_index;
  dp_header_info(field_hi(c_hfa, "sdp_source_info_f_adc"                  ) downto
                 field_lo(c_hfa, "sdp_source_info_f_adc"                  )) <= SLV(sdp_info.f_adc);
  dp_header_info(field_hi(c_hfa, "sdp_source_info_fsub_type"              ) downto
                 field_lo(c_hfa, "sdp_source_info_fsub_type"              )) <= SLV(fsub_type);
  dp_header_info(field_hi(c_hfa, "sdp_source_info_payload_error"          ) downto
                 field_lo(c_hfa, "sdp_source_info_payload_error"          )) <= SLV(hdr_input.payload_err);
  dp_header_info(field_hi(c_hfa, "sdp_source_info_beam_repositioning_flag") downto
                 field_lo(c_hfa, "sdp_source_info_beam_repositioning_flag")) <= SLV(sdp_info.beam_repositioning_flag);
  dp_header_info(field_hi(c_hfa, "sdp_source_info_weighted_subbands_flag" ) downto
                 field_lo(c_hfa, "sdp_source_info_weighted_subbands_flag" )) <= SLV(weighted_subbands_flag);
  dp_header_info(field_hi(c_hfa, "sdp_source_info_gn_id"                  ) downto
                 field_lo(c_hfa, "sdp_source_info_gn_id"                  )) <= pp_gn_id;
  dp_header_info(field_hi(c_hfa, "sdp_integration_interval"     ) downto
                 field_lo(c_hfa, "sdp_integration_interval"     )) <= TO_UVEC(hdr_input.integration_interval, 24);
  dp_header_info(field_hi(c_hfa, "sdp_data_id"                  ) downto
                 field_lo(c_hfa, "sdp_data_id"                  )) <= data_id_slv;
  dp_header_info(field_hi(c_hfa, "sdp_nof_signal_inputs"        ) downto
                 field_lo(c_hfa, "sdp_nof_signal_inputs"        )) <= TO_UVEC(c_nof_signal_inputs, 8);
  dp_header_info(field_hi(c_hfa, "sdp_nof_bytes_per_statistic"  ) downto
                 field_lo(c_hfa, "sdp_nof_bytes_per_statistic"  )) <= TO_UVEC(c_sdp_nof_bytes_per_statistic, 8);
  dp_header_info(field_hi(c_hfa, "sdp_nof_statistics_per_packet") downto
                 field_lo(c_hfa, "sdp_nof_statistics_per_packet")) <= TO_UVEC(c_nof_statistics_per_packet, 16);
  dp_header_info(field_hi(c_hfa, "sdp_block_period"             ) downto
                 field_lo(c_hfa, "sdp_block_period"             )) <= sdp_info.block_period;
  dp_header_info(field_hi(c_hfa, "dp_bsn"                       ) downto
                 field_lo(c_hfa, "dp_bsn"                       )) <= hdr_input.bsn_at_sync;

  p_reg : process(dp_rst, dp_clk)
  begin
    if dp_rst = '1' then
      r <= c_reg_rst;
    elsif rising_edge(dp_clk) then
      r <= nxt_r;
    end if;
  end process;

  -- Derive and pipeline dynamic parameters that depend on node id and MM, but are otherwise fixed
  -- . gn_index and ring_info.O_rn are in full GN = ID = 0:255 range defined by c_sdp_W_gn_id = 8
  -- . pn_index = gn_index MOD c_sdp_N_pn_max = gn_index[3:0] = 0:15, with c_sdp_N_pn_max = 16
  -- . O_rn is first GN index in ring, so O_rn <= gn_index
  -- . Avoid offload bursts between nodes via g_offload_node_time:
  --   - The nof_cycles_dly delays the start of statistics offload per node. The nof_cycles_dly
  --     increases as function of the GN index, so that the multiple nodes do their offload after
  --     eachother, to avoid bursts from multiple nodes.
  --   - c_sdp_N_band * c_sdp_N_pn_max = 32 nodes is maximum for a LOFAR2 Station.
  --   - p.nodes_dly: g_offload_node_time = c_sdp_offload_node_time = 600000 * 5 ns = 3 ms, so for max
  --     gn_index[4:0] = 31 the offload starts after 93 ms, to just fit within XST T_int min is 100 ms.
  --   - p.base_dly: use gn_index[7:5] to add a small extra dly g_offload_node_time / 8 of per group of
  --     32 nodes, so 0:g_offload_node_time in 8 steps of g_offload_node_time / 8, to remain within
  --     32 * 3 = 96 ms < 100 ms for any set of 32 nodes within full GN range.
  --   - use +1 for nof_cycles_dly to ensure that hdr_input.integration_interval gets the correct
  --     value also for node 0 with zero delay. Otherwise node 0 will read an integration_interval
  --     value that depends on when the remaining sop_cnt of the last interval in case of a XST
  --     processing restart.
  --   - g_offload_node_time = c_sdp_offload_node_time = 600000 suits g_ctrl_interval_size_min. For
  --     larger ctrl_interval_size the p.offload_node_time can be > g_offload_node_time by factor
  --     ctrl_interval_size / g_ctrl_interval_size_min. Use g_offload_scale_w to be able to implement
  --     this scaling by using right shift instead of integer divide:
  --       p.offload_node_time = (g_offload_node_time * 2**g_offload_scale_w / g_ctrl_interval_size_min) *
  --                             (ctrl_interval_size / 2**g_offload_scale_w)
  --                          ~= g_offload_node_time * ctrl_interval_size / g_ctrl_interval_size_min
  -- . Avoid offload bursts per node via g_offload_packet_time:
  --   - Each node offloads multiple packets per integration interval. For SST nof_packets = S_pn = 12,
  --     for BST nof_packets = 1 (per beamset), and for XST maximum nof_packets = P_sq *
  --     c_sdp_N_crosslets_max = 9 * 7 = 63.
  --   - g_offload_packet_time = c_sdp_offload_packet_time = 3100 suits g_ctrl_interval_size_min. For
  --     larger ctrl_interval_size the p.offload_packet_time can be > g_offload_packet_time by factor
  --     ctrl_interval_size / g_ctrl_interval_size_min. Use g_offload_scale_w to be able to implement
  --     this scaling by using right shift instead of integer divide:
  --       p.offload_packet_time = (g_offload_packet_time * 2**g_offload_scale_w / g_ctrl_interval_size_min) *
  --                               (ctrl_interval_size / 2**g_offload_scale_w)
  --                            ~= g_offload_packet_time * ctrl_interval_size / g_ctrl_interval_size_min
  -- . Fit g_offload_scale_w for offload_node_time and offload_packet_time.
  --   - For c_sdp_offload_node_time / g_ctrl_interval_size_min = 600000 / 20000000 = 0.03 choose
  --     g_offload_scale_w = 15 to have c_offload_node_time_scaled = 0.03 * 2**15 = 983.04 --> 983 and
  --     ctrl_interval_scaled = 20000000 / 2**15 = 610.35 --> 610, and 983 * 610 = 599630, which is
  --     close enough to c_sdp_offload_node_time = 600000.
  --   - For c_sdp_offload_packet_time / g_ctrl_interval_size_min = 3100 / 20000000 = 0.000155 choose
  --     g_offload_scale_w = 15 to have c_offload_node_time_scaled = 0.000155 * 2**15 = 5.08 --> 5 and
  --     ctrl_interval_scaled = 20000000 / 2**15 = 610.35 --> 610, and 5 * 610 = 3050, which is close
  --     enough to c_sdp_offload_packet_time = 3100.
  --   - Default use g_offload_scale_w = 0, ctrl_interval_size = g_ctrl_interval_size_min = 1 to have
  --     p.offload_node_time = g_offload_node_time
  --     p.offload_packet_time = g_offload_packet_time

  ctrl_interval_scaled <= SHIFT_UINT(ctrl_interval_size, g_offload_scale_w);

  p_reg_parameters : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      p.gn_index            <= gn_index;  -- gn_index[7:0] full GN range
      p.pn_index            <= func_sdp_gn_index_to_pn_index(p.gn_index);  -- pn_index = 0:15
      p.offset_rn           <= TO_UINT(ring_info.O_rn);
      p.rn_index            <= p.gn_index - p.offset_rn;
      p.local_si_offset     <= p.pn_index * c_sdp_S_pn;
      p.offload_node_time   <= c_offload_node_time_scaled * ctrl_interval_scaled;
      p.offload_packet_time <= c_offload_packet_time_scaled * ctrl_interval_scaled + 1;  -- + 1 to ensure > 0
      p.base_dly            <= TO_UINT(p_gn_id(7 downto 5)) * SHIFT_UINT(p.offload_node_time, 3);  -- divide by 2**3 = 8
      p.nodes_dly           <= TO_UINT(p_gn_id(4 downto 0)) * p.offload_node_time;
      p.nof_cycles_dly      <= p.base_dly + p.nodes_dly + 1;  -- + 1 to ensure > 0, to have proper
                                                              -- hdr_input.integration_interval also on node 0
      p.nof_rn              <= TO_UINT(ring_info.N_rn);
      p.nof_used_P_sq       <= smallest(p.nof_rn / 2 + 1, g_P_sq);
      p.remote_rn           <= func_ring_nof_hops_to_source_rn(
                                                         r.instance_count, p.rn_index, p.nof_rn, g_crosslets_direction);
      p.remote_gn           <= p.offset_rn + p.remote_rn;
      p.remote_pn           <= func_sdp_gn_index_to_pn_index(p.remote_gn);
      p.remote_si_offset    <= p.remote_pn * c_sdp_S_pn;
    end if;
  end process;

  p_gn_id <= TO_UVEC(p.gn_index, 8);  -- in slv format to ease slicing bit ranges
  pp_gn_id <= p_gn_id when rising_edge(dp_clk);  -- pipeline again to ease timing closure

  -- Determine info from previous sync interval in which the statistics were
  -- measured.
  -- . hold nof_crosslets from MM (and related nof_packets) per sync interval
  -- . capture bsn_at_sync at start of a sync interval
  -- . count number of sop that occur in a sync interval, to determine the
  --   actual number of sop for the previous integration_interval
  -- . track whether payload errors occur in a sync interval, to determine
  --   the payload_err flag for the previous integration_interval
  p_input_capture : process(dp_clk)
  begin
    if rising_edge(dp_clk) then
      -- Capture input
      if in_sosi.sync = '1' then
        reg_input.nof_crosslets <= TO_UINT(nof_crosslets);
        reg_input.bsn_at_sync <= in_sosi.bsn;
        reg_input.integration_interval <= reg_input.sop_cnt + 1;  -- size = index + 1
        reg_input.crosslets_info_rec <= prev_crosslets_info_rec;
        reg_input.sop_cnt <= 0;
        reg_input.payload_err <= '0';
      elsif in_sosi.sop = '1' then
        reg_input.sop_cnt <= reg_input.sop_cnt + 1;
      elsif in_sosi.eop = '1' then
        reg_input.payload_err <= reg_input.payload_err or in_sosi.err(0);
      end if;
      reg_input.nof_packets <= func_sdp_get_stat_nof_packets(g_statistics_type,
                                                             c_sdp_S_pn, p.nof_used_P_sq, reg_input.nof_crosslets);

      -- Determine previous sync interval info
      -- . just register when the value suits any sync interval
      prev_input.nof_crosslets <= reg_input.nof_crosslets;
      prev_input.nof_packets <= reg_input.nof_packets;
      -- . register at sync to get value for previous sync interval
      if in_sosi.sync = '1' then
        prev_input.bsn_at_sync <= reg_input.bsn_at_sync;
      end if;
      -- . just register when the value already is for the previous sync interval
      prev_input.integration_interval <= reg_input.integration_interval;
      prev_input.crosslets_info_rec <= reg_input.crosslets_info_rec;
      prev_input.sop_cnt <= reg_input.sop_cnt;
      prev_input.payload_err <= reg_input.payload_err;

      -- Hold previous sync interval info at trigger_offload, so that
      -- hdr_input can be used in p_control_packet_offload and
      -- dp_header_info.
      if trigger_offload = '1' then
        hdr_input <= prev_input;
      end if;
    end if;
  end process;

  -- Assign application header data_id for different statistic types, use
  -- GENERATE to keep unused fields at 0 (all fields are NATURAL, so default to 0).
  gen_data_id_sst : if g_statistics_type = "SST" generate
    data_id_rec.sst_signal_input_index <= r.packet_count + p.local_si_offset;
  end generate;

  gen_data_id_sst_os : if g_statistics_type = "SST_OS" generate
    -- Signal input index is repeated, first 0:11 (r.packet_count 0:11) with fsub_type = 0.
    -- Then for r.packet_count 12:23, signal input index is again 0:11 but with fsub_type = 1.
    fsub_type <= '0' when r.packet_count < c_sdp_S_pn else '1';  -- Set fsub_type = 0 for unshifted bands, '1'
                                                                 -- for shifted bands
    data_id_rec.sst_signal_input_index <= r.packet_count + p.local_si_offset when
                                      r.packet_count < c_sdp_S_pn else r.packet_count - c_sdp_S_pn + p.local_si_offset;
  end generate;

  gen_no_os : if g_statistics_type /= "SST_OS" generate
    -- Set fsub_type to sdp_info.fsub_type when g_statistics_type is not SST_OS.
    fsub_type <= sdp_info.fsub_type;
  end generate;

  gen_data_id_bst : if g_statistics_type = "BST" generate
    data_id_rec.bst_beamlet_index <= c_beamlet_id;
  end generate;

  gen_data_id_xst : if g_statistics_type = "XST" generate
    data_id_rec.xst_subband_index <= func_sdp_modulo_N_sub(hdr_input.crosslets_info_rec.offset_arr(r.crosslet_count));
    data_id_rec.xst_signal_input_A_index <= p.local_si_offset;
    data_id_rec.xst_signal_input_B_index <= p.remote_si_offset;
  end generate;

  data_id_slv <= func_sdp_map_stat_data_id(g_statistics_type, data_id_rec);

  -- sensitivity list in order of appearance
  p_control_packet_offload : process(r, p, trigger_offload, dp_sop, dp_header_info, reg_input)
    variable v       : t_reg;
    variable v_index : natural;
  begin
    v := r;
    v.start_pulse := '0';
    v.start_sync := '0';

    -- The trigger_offload occurs p.nof_cycles_dly after the in_sosi.sync and
    -- the offload will have finished before the next in_sosi.sync, because
    -- c_sdp_offload_node_time is such that all offload will finish within 100 ms
    -- and the integration interval (= sync interval) is 1 s for SST and BST
    -- and minimal 0.1s (= c_sdp_N_clk_per_sync_min) for XST.
    -- The trigger_offload initializes the control for the first packet offload
    -- in every sync interval.
    -- . Issue a start_pulse per packet offload. The start_pulse is used by
    --   u_dp_block_from_mm_dc to read the packet from statistics memory.
    if trigger_offload = '1' then
      -- Use trigger_offload to start first packet offload, all
      -- g_statistics_type start from start address 0
      v.start_pulse        := '1';
      v.start_sync         := '1';
      v.start_address      := 0;
      v.packet_count       := 0;
      v.interleave_count   := 0;  -- only used for SST
      v.interleave_address := 0;  -- only used for SST
      v.crosslet_count     := 0;  -- only used for XST
      v.instance_count     := 0;  -- only used for XST
      v.instance_address   := 0;  -- only used for XST

    -- The dp_sop = '1' when the packet has been read from statistics memory
    -- and is about to get out of the dp_fifo_fill_eop in
    -- u_dp_block_from_mm_dc. This ensures that the dp_sop identifies the
    -- sop of the offload packet. At the dp_sop:
    -- . the dp_header_info per packet offload can be released
    -- . the next packet offload with inter packet gap can be prepared
    elsif dp_sop = '1' then
      -- Release dp_header_info for current packet offload
      v.dp_header_info := dp_header_info;

      -- Start next packets offload.
      if r.packet_count < reg_input.nof_packets - 1 then
        if g_statistics_type = "SST" or g_statistics_type = "SST_OS" then
          --                 step        step        step        step        step        step
          -- start_address :    0,    2, 2048, 2050, 4096, 4098, 6144, 6146, 8192, 8194, 10240, 10242
          v.start_address := r.start_address + c_mm_data_size;  -- default step to next packet in this step
          v.interleave_count := r.interleave_count + 1;
          if r.interleave_count = c_sdp_Q_fft - 1 then
            -- jump to first packet for next step,
            v.start_address := r.interleave_address + c_sdp_N_sub * c_sdp_Q_fft * c_sdp_W_statistic_sz;  -- =
                                                                                              -- + 512 * 2 * 2 = + 2048
            v.interleave_count := 0;
            v.interleave_address := v.start_address;
          end if;
          v.start_timer := p.offload_packet_time;
          v.packet_count := r.packet_count + 1;

        elsif g_statistics_type = "BST" then
          null;  -- there is only one BST packet, so no more packets to offload here.

        elsif g_statistics_type = "XST" then
          -- start_address:
          --   reg_input.nof_crosslets:
          --                      1,     2,     3,     4,     5,     6,     7
          --   X_sq instance:
          --           0          0,   576,  1152,  1728,  2304,  2880,  3456
          --           1       4096,  4672,  5248,  5824,  6400,  6976,  7552
          --           2       8192,  8768,  9344,  9920, 10496, 11072, 11648
          --           3      12288, 12864, 13440, 14016, 14592, 15168, 15744
          --           4      16384, 16960, 17536, 18112, 18688, 19264, 19840
          --           5      20480, 21056, 21632, 22208, 22784, 23360, 23936
          --           6      24576, 25152, 25728, 26304, 26880, 27456, 28032
          --           7      28672, 29248, 29824, 30400, 30976, 31552, 32128
          --           8      32768, 33344, 33920, 34496, 35072, 35648, 36224
          v.start_address := r.start_address + c_sdp_X_sq * c_nof_complex * c_sdp_W_statistic_sz;  -- continue with
                                                                                        -- next packet in this instance
          v.crosslet_count := r.crosslet_count + 1;
          if r.crosslet_count = reg_input.nof_crosslets - 1 then
            v.start_address := r.instance_address + 2**c_sdp_ram_st_xsq_addr_w;  -- jump to first packet in
                                                                                 -- next instance
            v.crosslet_count := 0;
            v.instance_count := r.instance_count + 1;
            v.instance_address := v.start_address;  -- use v.start_address to avoid multipier needed in
                                                    -- (r.instance_count + 1) * 2**c_sdp_ram_st_xsq_addr_w
          end if;
          v.start_timer := p.offload_packet_time;
          v.packet_count := r.packet_count + 1;

        else
          null;  -- do nothing in case of unknown g_statistics_type
        end if;
      end if;

    -- Use start_timer to have p.offload_packet_time gap between packets, and issue start_pulse when
    -- the start_timer expires
    elsif r.start_timer > 0 then
      v.start_timer := r.start_timer - 1;
      if v.start_timer = 0 then
        v.start_pulse := '1';
      end if;
    end if;

    nxt_r <= v;
  end process;

  -- Register new_interval to ease timing closure. The new_interval will already be active
  -- somewhat before the first in_sosi.sync arrives at the sdp_statistics_offload, due to
  -- latency in the data path.
  reg_new_interval <= new_interval when rising_edge(dp_clk);

  -- The in_trigger can skip the first in_sosi.sync. This is necessary if the
  -- in_sosi input can be restarted, because then at every restart there is
  -- no valid previous in_sosi.sync interval yet.
  in_trigger <= in_sosi.sync and not reg_new_interval;

  u_mms_common_variable_delay : entity common_lib.mms_common_variable_delay
  port map (
    mm_rst => mm_rst,
    mm_clk => mm_clk,
    dp_rst => dp_rst,
    dp_clk => dp_clk,

    -- MM interface
    reg_enable_mosi => reg_enable_mosi,
    reg_enable_miso => reg_enable_miso,

    delay           => p.nof_cycles_dly,
    trigger         => in_trigger,
    trigger_en      => trigger_en,
    trigger_dly     => trigger_offload
  );

  u_dp_block_from_mm_dc : entity dp_lib.dp_block_from_mm_dc
  generic map (
    g_user_size          => c_mm_user_size,
    g_data_size          => c_mm_data_size,
    g_step_size          => c_mm_step_size,
    g_nof_data           => c_mm_nof_data,
    g_word_w             => c_word_w,
    g_reverse_word_order => g_reverse_word_order,
    g_bsn_w              => c_dp_stream_bsn_w,
    g_bsn_incr_enable    => false  -- all offload block have same bsn_at_sync
  )
  port map(
    dp_rst        => dp_rst,
    dp_clk        => dp_clk,
    mm_rst        => mm_rst,
    mm_clk        => mm_clk,
    start_pulse   => r.start_pulse,
    sync_in       => r.start_sync,
    bsn_at_sync   => hdr_input.bsn_at_sync,
    start_address => r.start_address,
    mm_mosi       => master_mosi,
    mm_miso       => master_miso,
    out_sop       => dp_sop,  -- = dp_block_from_mm_src_out.sop
    out_sosi      => dp_block_from_mm_src_out,
    out_siso      => dp_block_from_mm_src_in
  );

  -- The dp_sop is the sop of the packet that is about to be offloaded by
  -- u_dp_offload_tx_v3. The r.dp_header_info must be available at the
  -- dp_offload_snk_in.sop. This is guaranteed because:
  -- . r.dp_header_info is available one clock cycle after dp_sop in
  --   p_control_packet_offload.
  -- . The dp_offload_snk_in is delayed also by at least one clock cycle by
  --   u_dp_pipeline_ready.

  u_dp_pipeline_ready : entity dp_lib.dp_pipeline_ready
  port map(
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_out      => dp_block_from_mm_src_in,
    snk_in       => dp_block_from_mm_src_out,
    -- ST source
    src_in       => dp_offload_snk_out,
    src_out      => dp_offload_snk_in
  );

  -- The hdr_input.bsn_at_sync is passed on via r.dp_header_info so that
  -- u_dp_offload_tx_v3 can put it in the udp_sosi header.
  -- The dp_offload_snk_in.bsn that is passed on by u_dp_block_from_mm_dc is
  -- in fact not used, but useful to see in udp_sosi.bsn in the Wave Window.
  -- Similar dp_offload_snk_in.sync that is passed on by u_dp_block_from_mm_dc
  -- is in fact not used, but useful to have in udp_sosi.sync (e.g. for the
  -- tb).
  u_dp_offload_tx_v3: entity dp_lib.dp_offload_tx_v3
  generic map (
    g_nof_streams    => c_nof_streams,
    g_data_w         => c_word_w,
    g_symbol_w       => c_word_w,
    g_hdr_field_arr  => c_sdp_stat_hdr_field_arr,
    g_hdr_field_sel  => c_sdp_stat_hdr_field_sel,
    g_pipeline_ready => true
  )
  port map(
    mm_rst               => mm_rst,
    mm_clk               => mm_clk,
    dp_rst               => dp_rst,
    dp_clk               => dp_clk,
    reg_hdr_dat_mosi     => reg_hdr_dat_mosi,
    reg_hdr_dat_miso     => reg_hdr_dat_miso,
    snk_in_arr(0)        => dp_offload_snk_in,
    snk_out_arr(0)       => dp_offload_snk_out,
    src_out_arr(0)       => udp_sosi,
    src_in_arr(0)        => out_siso,
    hdr_fields_in_arr(0) => r.dp_header_info
  );

  -- Debug signal, r_dp_header_rec must be available at the r_dp_header_sop
  r_dp_header_sop <= dp_offload_snk_in.sop;
  r_dp_header_rec <= func_sdp_map_stat_header(r.dp_header_info);

  out_sosi <= udp_sosi;

  u_bsn_mon_udp : entity dp_lib.mms_dp_bsn_monitor_v2
  generic map (
    g_nof_streams        => 1,
    g_cross_clock_domain => true,
    g_sync_timeout       => g_bsn_monitor_sync_timeout,
    g_bsn_w              => c_dp_stream_bsn_w,
    g_error_bi           => 0,
    g_cnt_sop_w          => c_word_w,
    g_cnt_valid_w        => c_word_w,
    g_cnt_latency_w      => c_word_w
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst         => mm_rst,
    mm_clk         => mm_clk,
    reg_mosi       => reg_bsn_monitor_v2_offload_copi,
    reg_miso       => reg_bsn_monitor_v2_offload_cipo,

    -- Streaming clock domain
    dp_rst         => dp_rst,
    dp_clk         => dp_clk,
    ref_sync       => in_sosi.sync,

    in_sosi_arr(0) => udp_sosi
  );
end str;
