-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose: Test multiple instances of tb_dp_fifo_dc_arr.

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_tb_dp_fifo_dc_arr is
end tb_tb_dp_fifo_dc_arr;

architecture tb of tb_tb_dp_fifo_dc_arr is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- > as 10
  -- > run g_dut_rd_clk_freq * 330 us                 --> OK

  -- Try FIFO settings : GENERIC MAP (g_dut_wr_clk_freq, g_dut_rd_clk_freq, g_dut_use_bsn, g_dut_use_empty, g_dut_use_channel, g_dut_use_sync, g_dut_use_ctrl, g_dut_out_latency)

  u_use_all_rl_0          : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 1, true,  true,  true,  true,   true,  true,  0);
  u_use_all_rl_0_clk_2_1  : entity work.tb_dp_fifo_dc_arr generic map (3, 2, 1, true,  true,  true,  true,   true,  true,  0);
  u_use_all_rl_0_clk_1_2  : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 2, true,  true,  true,  true,   true,  true,  0);
  u_use_all_rl_0_clk_3_2  : entity work.tb_dp_fifo_dc_arr generic map (3, 3, 2, true,  true,  true,  true,   true,  true,  0);
  u_use_all_rl_0_clk_2_3  : entity work.tb_dp_fifo_dc_arr generic map (3, 2, 3, true,  true,  true,  true,   true,  true,  0);

  u_use_all               : entity work.tb_dp_fifo_dc_arr generic map (1, 1, 1, true,  true,  true,  true,   true,  true,  1);
  u_use_all_clk_3_1       : entity work.tb_dp_fifo_dc_arr generic map (3, 3, 1, true,  true,  true,  true,   true,  true,  1);
  u_use_all_clk_1_3       : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 3, true,  true,  true,  true,   true,  true,  1);

  u_use_ctrl_rl_0         : entity work.tb_dp_fifo_dc_arr generic map (1, 1, 1, false, false, false, false,  true,  true,  0);
  u_use_ctrl_rl_0_clk_1_3 : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 3, false, false, false, false,  true,  true,  0);
  u_use_ctrl_rl_0_clk_3_1 : entity work.tb_dp_fifo_dc_arr generic map (3, 3, 1, false, false, false, false,  true,  true,  0);
  u_use_ctrl              : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 1, false, false, false, false,  true,  true,  1);
  u_use_ctrl_clk_1_2      : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 2, false, false, false, false,  true,  true,  1);
  u_use_ctrl_clk_2_1      : entity work.tb_dp_fifo_dc_arr generic map (3, 2, 1, false, false, false, false,  true,  true,  1);

  u_no_bsn                : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 1, false, true,  true,  true,   true,  true,  1);
  u_no_empty              : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 1, true,  false, true,  true,   true,  true,  1);
  u_no_channel            : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 1, true,  true,  false, true,   true,  true,  1);
  u_no_sync               : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 1, true,  true,  true,  false,  true,  true,  1);
  u_no_ctrl               : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 1, true,  true,  true,  true,   false, true,  1);
  u_no_aux                : entity work.tb_dp_fifo_dc_arr generic map (3, 1, 1, true,  true,  true,  true,   true,  false, 1);
end tb;
