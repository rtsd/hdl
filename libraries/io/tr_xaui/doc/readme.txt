How to compile the IP used by the tr_xaui module:
=================================================
1) Run the megawizard on the variation files, by:
   . running 'unb_mgw tr_xaui' or
   . running the MegaWizard GUI on /src/ip/megawizard/*vhd files
2) In ModelSim, first compile the simulation libraries:
   . lp tr_xaui
   . In the project window, right-click the 'compile_ip' TCL scripts and
     select 'execute'.
3) Now the tr_xaui module itself can be compiled:
   . mk compile
