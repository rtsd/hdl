-------------------------------------------------------------------------------
--
-- Copyright (C) 2009
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

entity tb_common_add_sub is
  generic (
    g_direction    : string := "SUB";  -- "SUB", "ADD" or "BOTH"
    g_sel_add      : std_logic := '1';  -- '0' = sub, '1' = add, only valid for g_direction = "BOTH"
    g_pipeline_in  : natural := 0;  -- input pipelining 0 or 1
    g_pipeline_out : natural := 2;  -- output pipelining >= 0
    g_in_dat_w     : natural := 5;
    g_out_dat_w    : natural := 5  -- g_in_dat_w or g_in_dat_w+1
  );
end tb_common_add_sub;

architecture tb of tb_common_add_sub is
  constant clk_period    : time := 10 ns;
  constant c_pipeline    : natural := g_pipeline_in + g_pipeline_out;

  function func_result(in_a, in_b : std_logic_vector) return std_logic_vector is
    variable v_a, v_b, v_result : integer;
  begin
    -- Calculate expected result
    v_a := TO_SINT(in_a);
    v_b := TO_SINT(in_b);
    if g_direction = "ADD"                    then v_result := v_a + v_b; end if;
    if g_direction = "SUB"                    then v_result := v_a - v_b; end if;
    if g_direction = "BOTH" and g_sel_add = '1' then v_result := v_a + v_b; end if;
    if g_direction = "BOTH" and g_sel_add = '0' then v_result := v_a - v_b; end if;
    -- Wrap to avoid warning: NUMERIC_STD.TO_SIGNED: vector truncated
    if v_result >  2**(g_out_dat_w - 1) - 1 then v_result := v_result - 2**g_out_dat_w; end if;
    if v_result < - 2**(g_out_dat_w - 1)   then v_result := v_result + 2**g_out_dat_w; end if;
    return TO_SVEC(v_result, g_out_dat_w);
  end;

  signal tb_end          : std_logic := '0';
  signal rst             : std_logic;
  signal clk             : std_logic := '0';
  signal in_a            : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal in_b            : std_logic_vector(g_in_dat_w - 1 downto 0);
  signal out_result      : std_logic_vector(g_out_dat_w - 1 downto 0);  -- combinatorial result
  signal result_expected : std_logic_vector(g_out_dat_w - 1 downto 0);  -- pipelined results
  signal result_rtl      : std_logic_vector(g_out_dat_w - 1 downto 0);
begin
  clk  <= not clk or tb_end after clk_period / 2;

  -- run 1 us or -all
  p_in_stimuli : process
  begin
    rst <= '1';
    in_a <= TO_SVEC(0, g_in_dat_w);
    in_b <= TO_SVEC(0, g_in_dat_w);
    wait until rising_edge(clk);
    for I in 0 to 9 loop
      wait until rising_edge(clk);
    end loop;
    rst <= '0';
    for I in 0 to 9 loop
      wait until rising_edge(clk);
    end loop;

    -- Some special combinations
    in_a <= TO_SVEC(2, g_in_dat_w);
    in_b <= TO_SVEC(5, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(2, g_in_dat_w);
    in_b <= TO_SVEC(-5, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(-3, g_in_dat_w);
    in_b <= TO_SVEC(-9, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(-3, g_in_dat_w);
    in_b <= TO_SVEC(9, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(11, g_in_dat_w);
    in_b <= TO_SVEC(15, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(11, g_in_dat_w);
    in_b <= TO_SVEC(-15, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(-11, g_in_dat_w);
    in_b <= TO_SVEC(15, g_in_dat_w);
    wait until rising_edge(clk);
    in_a <= TO_SVEC(-11, g_in_dat_w);
    in_b <= TO_SVEC(-15, g_in_dat_w);
    wait until rising_edge(clk);

    for I in 0 to 49 loop
      wait until rising_edge(clk);
    end loop;

    -- All combinations
    for I in - 2**(g_in_dat_w - 1) to 2**(g_in_dat_w - 1) - 1 loop
      for J in - 2**(g_in_dat_w - 1) to 2**(g_in_dat_w - 1) - 1 loop
        in_a <= TO_SVEC(I, g_in_dat_w);
        in_b <= TO_SVEC(J, g_in_dat_w);
        wait until rising_edge(clk);
      end loop;
    end loop;
    wait until rising_edge(clk);
    tb_end <= '1';
    wait;
  end process;

  out_result <= func_result(in_a, in_b);

  u_result : entity work.common_pipeline
  generic map (
    g_representation => "SIGNED",
    g_pipeline       => c_pipeline,
    g_reset_value    => 0,
    g_in_dat_w       => g_out_dat_w,
    g_out_dat_w      => g_out_dat_w
  )
  port map (
    rst     => rst,
    clk     => clk,
    clken   => '1',
    in_dat  => out_result,
    out_dat => result_expected
  );

  u_dut_rtl : entity work.common_add_sub
  generic map (
    g_direction       => g_direction,
    g_representation  => "SIGNED",
    g_pipeline_input  => g_pipeline_in,
    g_pipeline_output => g_pipeline_out,
    g_in_dat_w        => g_in_dat_w,
    g_out_dat_w       => g_out_dat_w
  )
  port map (
    clk     => clk,
    clken   => '1',
    sel_add => g_sel_add,
    in_a    => in_a,
    in_b    => in_b,
    result  => result_rtl
  );

  p_verify : process(rst, clk)
  begin
    if rst = '0' then
      if rising_edge(clk) then
        assert result_rtl = result_expected
          report "Error: wrong RTL result"
          severity ERROR;
      end if;
    end if;
  end process;
end tb;
