###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from common import *
from eth import ip_hdr_checksum
from mem_init_file import list_to_hex

# Purpose:
# . Generate a HEX file with a default header for the RAM in tr_10GbE.vhd
# Description:
# . In unb_tr_10GbE, each diagnostics data block is packetized using the
#   header in the RAM.
# . The HEX file generated here makes up the initial RAM contents.
# . The RAM contents can be changed during run-time using 
#   $UPE_GEAR/peripherals/pi_dp_ram_from_mm.py.

###############################################################################
# Constants
###############################################################################

FILENAME = "../hex/default_eth_header.hex"
WORD_WIDTH = 64 # With of the 10GbE MAC's user interface
NOF_HDR_WORDS = 8 # 512 bits = 64 bytes (c_nof_header_words in tr_10GbE.vhd)
NOF_PAYLOAD_WORDS = 1118 # c_block_len in node_unb_tr_10GbE.vhd
NOF_PAYLOAD_BYTES = NOF_PAYLOAD_WORDS * c_word_w / c_byte_w # 8944 bytes
MEM_WIDTH = WORD_WIDTH
MEM_DEPTH = NOF_HDR_WORDS

# Header lengths in bytes
ETH_HDR_LENGTH = 14
IP_HDR_LENGTH  = 20
UDP_HDR_LENGTH =  8
USR_HDR_LENGTH = 22
TOT_HDR_LENGTH = ETH_HDR_LENGTH+IP_HDR_LENGTH+UDP_HDR_LENGTH+USR_HDR_LENGTH

print 'Creating Ethernet header'

###############################################################################
# Ethernet header
###############################################################################
eth_dst_mac = 0x00074306C700  # capture5
eth_src_mac = 0x002286080000
eth_type    = 0x0800

eth_hdr_bytes = CommonBytes(eth_dst_mac, 6) & \
                CommonBytes(eth_src_mac, 6) & \
                CommonBytes(eth_type   , 2)

###############################################################################
# IP header
###############################################################################
ip_version         = 4 
ip_header_length   = 5 # 5 32b words
ip_services        = 0 
ip_total_length    = IP_HDR_LENGTH + UDP_HDR_LENGTH + USR_HDR_LENGTH+ \
                     NOF_PAYLOAD_BYTES
print 'ip_total_length=',ip_total_length
ip_total_length = 8994
print 'ip_total_length=',ip_total_length

ip_identification  = 0 
ip_flags           = 2 
ip_fragment_offset = 0 
ip_time_to_live    = 127 
ip_protocol        = 17 
ip_header_checksum = 0 # to be calculated
ip_src_addr        = 0x0a630001 # 10.99.0.1
ip_dst_addr        = 0x0a0a0a0a # 10.10.10.10 # capture5

ip_hdr_bits = CommonBits(ip_version         ,  4) & \
              CommonBits(ip_header_length   ,  4) & \
              CommonBits(ip_services        ,  8) & \
              CommonBits(ip_total_length    , 16) & \
              CommonBits(ip_identification  , 16) & \
              CommonBits(ip_flags           ,  3) & \
              CommonBits(ip_fragment_offset , 13) & \
              CommonBits(ip_time_to_live    ,  8) & \
              CommonBits(ip_protocol        ,  8) & \
              CommonBits(ip_header_checksum , 16) & \
              CommonBits(ip_src_addr        , 32) & \
              CommonBits(ip_dst_addr        , 32)

ip_hdr_bytes = CommonBytes(ip_hdr_bits.data, IP_HDR_LENGTH)

# Calculate and insert the IP header checksum
calced_checksum = ip_hdr_checksum(ip_hdr_bytes)
print 'Inserting IP header checksum:', calced_checksum, '=', hex(calced_checksum)
ip_hdr_bytes[9:8] = calced_checksum

###############################################################################
# UDP header
###############################################################################
#udp_src_port     = 0x89AB
#udp_dst_port     = 0xCDEF
udp_src_port     = 4000
udp_dst_port     = 4000
udp_total_length = USR_HDR_LENGTH + NOF_PAYLOAD_BYTES
udp_total_length = 8974
udp_checksum     = 0 # Zero is fine

udp_hdr_bytes = CommonBytes(udp_src_port    , 2) & \
                CommonBytes(udp_dst_port    , 2) & \
                CommonBytes(udp_total_length, 2) & \
                CommonBytes(udp_checksum    , 2)

###############################################################################
# USR header
###############################################################################
usr_hdr = 0
usr_hdr_bytes = CommonBytes(usr_hdr, USR_HDR_LENGTH)

###############################################################################
# Total header
###############################################################################
tot_hdr_bytes = eth_hdr_bytes & ip_hdr_bytes & udp_hdr_bytes & usr_hdr_bytes

###############################################################################
# Convert header bytes to 64b word list
# . The LS word of the RAM is released first. We want the MS part of the header
#   to be released into the MAC first, so we must fill the 64b word list in 
#   reverse.
###############################################################################
tot_hdr_words = CommonWords64(tot_hdr_bytes.data, NOF_HDR_WORDS)

word_list = []
for w in reversed(range(NOF_HDR_WORDS)):
    word_list.append(tot_hdr_words[w])
    print 'w=',w,' data=',hex(tot_hdr_words[w])

###############################################################################
# Generate the HEX file
###############################################################################
print 'Generating hex file:', FILENAME
list_to_hex(word_list, FILENAME, MEM_WIDTH, MEM_DEPTH)
print 'Done.'
