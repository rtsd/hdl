-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Get in_pulse from in_clk to out_pulse in the out_clk domain.
-- Description:
--   The in_pulse is captured in the in_clk domain and then transfered to the
--   out_clk domain. The out_pulse is also only one cycle wide and transfered
--   back to the in_clk domain to serve as an acknowledge signal to ensure
--   that the in_pulse was recognized also in case the in_clk is faster than
--   the out_clk. The in_busy is active during the entire transfer. Hence the
--   rate of pulses that can be transfered is limited by g_delay_len and by
--   the out_clk rate.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_spulse is
  generic (
    g_delay_len : natural := c_meta_delay_len
  );
  port (
    in_rst       : in  std_logic := '0';
    in_clk       : in  std_logic;
    in_clken     : in  std_logic := '1';
    in_pulse     : in  std_logic;
    in_busy      : out std_logic;
    out_rst      : in  std_logic := '0';
    out_clk      : in  std_logic;
    out_clken    : in  std_logic := '1';
    out_pulse    : out std_logic
  );
end;

architecture rtl of common_spulse is
  signal in_level       : std_logic;
  signal meta_level     : std_logic_vector(0 to g_delay_len - 1);
  signal out_level      : std_logic;
  signal prev_out_level : std_logic;
  signal meta_ack       : std_logic_vector(0 to g_delay_len - 1);
  signal pulse_ack      : std_logic;
  signal nxt_out_pulse  : std_logic;
begin
  capture_in_pulse : entity work.common_switch
  port map (
    clk         => in_clk,
    clken       => in_clken,
    rst         => in_rst,
    switch_high => in_pulse,
    switch_low  => pulse_ack,
    out_level   => in_level
  );

  in_busy <= in_level or pulse_ack;

  p_out_clk : process(out_rst, out_clk)
  begin
    if out_rst = '1' then
      meta_level     <= (others => '0');
      out_level      <= '0';
      prev_out_level <= '0';
      out_pulse      <= '0';
    elsif rising_edge(out_clk) then
      if out_clken = '1' then
        meta_level     <= in_level & meta_level(0 to meta_level'high - 1);
        out_level      <= meta_level(meta_level'high);
        prev_out_level <= out_level;
        out_pulse      <= nxt_out_pulse;
      end if;
    end if;
  end process;

  p_in_clk : process(in_rst, in_clk)
  begin
    if in_rst = '1' then
      meta_ack  <= (others => '0');
      pulse_ack <= '0';
    elsif rising_edge(in_clk) then
      if in_clken = '1' then
        meta_ack  <= out_level & meta_ack(0 to meta_ack'high - 1);
        pulse_ack <= meta_ack(meta_ack'high);
      end if;
    end if;
  end process;

  nxt_out_pulse <= out_level and not prev_out_level;
end rtl;
