-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb2c_board_lib, mm_lib, eth_lib, technology_lib, tech_tse_lib, tech_mac_10g_lib, io_ddr_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.common_field_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.common_network_layers_pkg.all;
use unb2c_board_lib.unb2c_board_pkg.all;
use unb2c_board_lib.unb2c_board_peripherals_pkg.all;
use mm_lib.mm_file_pkg.all;
use mm_lib.mm_file_unb_pkg.all;
use eth_lib.eth_pkg.all;
use technology_lib.technology_pkg.all;
use tech_tse_lib.tech_tse_pkg.all;
use tech_tse_lib.tb_tech_tse_pkg.all;
use work.qsys_unb2c_test_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;
use work.unb2c_test_pkg.all;

entity mmm_unb2c_test is
  generic (
    g_sim               : boolean := false;  -- FALSE: use SOPC; TRUE: use mm_file I/O
    g_sim_unb_nr        : natural := 0;
    g_sim_node_nr       : natural := 0;
    g_technology        : natural := c_tech_arria10_e1sg;
    g_bg_block_size     : natural;
    g_hdr_field_arr     : t_common_field_arr;
    g_nof_streams_qsfp  : natural;
    g_nof_streams_ring  : natural;
    g_nof_streams_back0 : natural
  );
  port (
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    pout_wdi                 : out std_logic;

    -- Manual WDI override
    reg_wdi_mosi             : out t_mem_mosi;
    reg_wdi_miso             : in  t_mem_miso;

    -- system_info
    reg_unb_system_info_mosi : out t_mem_mosi;
    reg_unb_system_info_miso : in  t_mem_miso;
    rom_unb_system_info_mosi : out t_mem_mosi;
    rom_unb_system_info_miso : in  t_mem_miso;

    reg_fpga_temp_sens_mosi   : out t_mem_mosi;
    reg_fpga_temp_sens_miso   : in  t_mem_miso;
    reg_fpga_voltage_sens_mosi: out t_mem_mosi;
    reg_fpga_voltage_sens_miso: in  t_mem_miso;

    -- PPSH
    reg_ppsh_mosi            : out t_mem_mosi;
    reg_ppsh_miso            : in  t_mem_miso;

    -- eth1g ch0
    eth_0_mm_rst        : out std_logic;
    eth_0_tse_mosi      : out t_mem_mosi;
    eth_0_tse_miso      : in  t_mem_miso;
    eth_0_reg_mosi      : out t_mem_mosi;
    eth_0_reg_miso      : in  t_mem_miso;
    eth_0_reg_interrupt : in  std_logic;
    eth_0_ram_mosi      : out t_mem_mosi;
    eth_0_ram_miso      : in  t_mem_miso;

    reg_diag_bg_eth_0_copi                : out t_mem_copi;
    reg_diag_bg_eth_0_cipo                : in  t_mem_cipo;
    reg_hdr_dat_eth_0_copi                : out t_mem_copi;
    reg_hdr_dat_eth_0_cipo                : in  t_mem_cipo;
    reg_bsn_monitor_v2_tx_eth_0_copi      : out t_mem_copi;
    reg_bsn_monitor_v2_tx_eth_0_cipo      : in  t_mem_cipo;
    reg_strobe_total_count_tx_eth_0_copi  : out t_mem_copi;
    reg_strobe_total_count_tx_eth_0_cipo  : in  t_mem_cipo;

    reg_bsn_monitor_v2_rx_eth_0_copi      : out t_mem_copi;
    reg_bsn_monitor_v2_rx_eth_0_cipo      : in  t_mem_cipo;
    reg_strobe_total_count_rx_eth_0_copi  : out t_mem_copi;
    reg_strobe_total_count_rx_eth_0_cipo  : in  t_mem_cipo;

    -- eth1g ch1
    eth_1_mm_rst        : out std_logic;
    eth_1_tse_mosi      : out t_mem_mosi;
    eth_1_tse_miso      : in  t_mem_miso;
    eth_1_reg_mosi      : out t_mem_mosi;
    eth_1_reg_miso      : in  t_mem_miso;
    eth_1_reg_interrupt : in  std_logic;
    eth_1_ram_mosi      : out t_mem_mosi;
    eth_1_ram_miso      : in  t_mem_miso;

    reg_diag_bg_eth_1_copi                : out t_mem_copi;
    reg_diag_bg_eth_1_cipo                : in  t_mem_cipo;
    reg_hdr_dat_eth_1_copi                : out t_mem_copi;
    reg_hdr_dat_eth_1_cipo                : in  t_mem_cipo;
    reg_bsn_monitor_v2_tx_eth_1_copi      : out t_mem_copi;
    reg_bsn_monitor_v2_tx_eth_1_cipo      : in  t_mem_cipo;
    reg_strobe_total_count_tx_eth_1_copi  : out t_mem_copi;
    reg_strobe_total_count_tx_eth_1_cipo  : in  t_mem_cipo;

    reg_bsn_monitor_v2_rx_eth_1_copi      : out t_mem_copi;
    reg_bsn_monitor_v2_rx_eth_1_cipo      : in  t_mem_cipo;
    reg_strobe_total_count_rx_eth_1_copi  : out t_mem_copi;
    reg_strobe_total_count_rx_eth_1_cipo  : in  t_mem_cipo;

    -- EPCS read
    reg_dpmm_data_mosi       : out t_mem_mosi;
    reg_dpmm_data_miso       : in  t_mem_miso;
    reg_dpmm_ctrl_mosi       : out t_mem_mosi;
    reg_dpmm_ctrl_miso       : in  t_mem_miso;

    -- EPCS write
    reg_mmdp_data_mosi       : out t_mem_mosi;
    reg_mmdp_data_miso       : in  t_mem_miso;
    reg_mmdp_ctrl_mosi       : out t_mem_mosi;
    reg_mmdp_ctrl_miso       : in  t_mem_miso;

    -- EPCS status/control
    reg_epcs_mosi            : out t_mem_mosi;
    reg_epcs_miso            : in  t_mem_miso;

    -- Remote Update
    reg_remu_mosi            : out t_mem_mosi;
    reg_remu_miso            : in  t_mem_miso;

    -- Heater
    reg_heater_mosi          : out t_mem_mosi;
    reg_heater_miso          : in  t_mem_miso;

    -- block gen
    ram_diag_bg_10GbE_mosi         : out t_mem_mosi;
    ram_diag_bg_10GbE_miso         : in  t_mem_miso;
    reg_diag_bg_10GbE_mosi         : out t_mem_mosi;
    reg_diag_bg_10GbE_miso         : in  t_mem_miso;
    reg_diag_tx_seq_10GbE_mosi     : out t_mem_mosi;
    reg_diag_tx_seq_10GbE_miso     : in  t_mem_miso;

    -- bsn
    reg_bsn_monitor_10GbE_mosi     : out t_mem_mosi;
    reg_bsn_monitor_10GbE_miso     : in  t_mem_miso;

    -- databuffer
    ram_diag_data_buf_10GbE_mosi   : out t_mem_mosi;
    ram_diag_data_buf_10GbE_miso   : in  t_mem_miso;
    reg_diag_data_buf_10GbE_mosi   : out t_mem_mosi;
    reg_diag_data_buf_10GbE_miso   : in  t_mem_miso;
    reg_diag_rx_seq_10GbE_mosi     : out t_mem_mosi;
    reg_diag_rx_seq_10GbE_miso     : in  t_mem_miso;

    -- 10GbE
    reg_tr_10GbE_qsfp_ring_mosi    : out t_mem_mosi;
    reg_tr_10GbE_qsfp_ring_miso    : in  t_mem_miso;
    reg_tr_10GbE_back0_mosi        : out t_mem_mosi;
    reg_tr_10GbE_back0_miso        : in  t_mem_miso;

    reg_eth10g_qsfp_ring_mosi      : out t_mem_mosi;
    reg_eth10g_qsfp_ring_miso      : in  t_mem_miso;
    reg_eth10g_back0_mosi          : out t_mem_mosi;
    reg_eth10g_back0_miso          : in  t_mem_miso;

    -- DDR4 : MB I
    reg_io_ddr_MB_I_mosi                : out t_mem_mosi;
    reg_io_ddr_MB_I_miso                : in  t_mem_miso;

    reg_diag_tx_seq_ddr_MB_I_mosi       : out t_mem_mosi;
    reg_diag_tx_seq_ddr_MB_I_miso       : in  t_mem_miso;

    reg_diag_rx_seq_ddr_MB_I_mosi       : out t_mem_mosi;
    reg_diag_rx_seq_ddr_MB_I_miso       : in  t_mem_miso;

    reg_diag_data_buf_ddr_MB_I_mosi     : out t_mem_mosi;
    reg_diag_data_buf_ddr_MB_I_miso     : in  t_mem_miso;
    ram_diag_data_buf_ddr_MB_I_mosi     : out t_mem_mosi;
    ram_diag_data_buf_ddr_MB_I_miso     : in  t_mem_miso;

    -- DDR4 : MB II
    reg_io_ddr_MB_II_mosi                : out t_mem_mosi;
    reg_io_ddr_MB_II_miso                : in  t_mem_miso;

    reg_diag_tx_seq_ddr_MB_II_mosi       : out t_mem_mosi;
    reg_diag_tx_seq_ddr_MB_II_miso       : in  t_mem_miso;

    reg_diag_rx_seq_ddr_MB_II_mosi       : out t_mem_mosi;
    reg_diag_rx_seq_ddr_MB_II_miso       : in  t_mem_miso;

    reg_diag_data_buf_ddr_MB_II_mosi     : out t_mem_mosi;
    reg_diag_data_buf_ddr_MB_II_miso     : in  t_mem_miso;
    ram_diag_data_buf_ddr_MB_II_mosi     : out t_mem_mosi;
    ram_diag_data_buf_ddr_MB_II_miso     : in  t_mem_miso;

    -- Jesd reset control
    jesd_ctrl_mosi            : out t_mem_mosi;
    jesd_ctrl_miso            : in  t_mem_miso;
    -- Jesd ip status/control
    jesd204b_mosi               : out t_mem_mosi;
    jesd204b_miso               : in  t_mem_miso;
    reg_bsn_source_mosi         : out t_mem_mosi;
    reg_bsn_source_miso         : in  t_mem_miso;
    reg_bsn_monitor_input_mosi  : out t_mem_mosi;
    reg_bsn_monitor_input_miso  : in  t_mem_miso;
    ram_diag_data_buf_bsn_mosi  : out t_mem_mosi;
    ram_diag_data_buf_bsn_miso  : in  t_mem_miso;
    reg_diag_data_buf_bsn_mosi  : out t_mem_mosi;
    reg_diag_data_buf_bsn_miso  : in  t_mem_miso;

    -- Scrap RAM
    ram_scrap_mosi           : out t_mem_mosi;
    ram_scrap_miso           : in  t_mem_miso
  );
end mmm_unb2c_test;

architecture str of mmm_unb2c_test is
  constant c_sim_node_nr   : natural := g_sim_node_nr;
  constant c_sim_node_type : string(1 to 2) := "FN";

  constant g_nof_streams_10GbE                     : natural := g_nof_streams_qsfp + g_nof_streams_ring + g_nof_streams_back0;

  -- Block generator
  -- check with python: from common import *
  --                    ceil_log2(48 * 2**ceil_log2(900))
  constant c_ram_diag_bg_10GbE_addr_w              : natural := ceil_log2(g_nof_streams_10GbE * pow2(ceil_log2(g_bg_block_size)));
  constant c_ram_diag_databuffer_10GbE_addr_w      : natural := ceil_log2(g_nof_streams_10GbE * pow2(ceil_log2(g_bg_block_size)));
  constant c_ram_diag_databuffer_ddr_addr_w        : natural := ceil_log2(2                   * pow2(ceil_log2(g_bg_block_size)));

  -- tr_10GbE
  constant c_reg_tr_10GbE_adr_w                    : natural := func_tech_mac_10g_csr_addr_w(g_technology);
  constant c_reg_tr_10GbE_qsfp_ring_multi_adr_w    : natural := ceil_log2((g_nof_streams_qsfp + g_nof_streams_ring) * pow2(c_reg_tr_10GbE_adr_w));
  constant c_reg_tr_10GbE_back0_multi_adr_w        : natural := ceil_log2(g_nof_streams_back0 * pow2(c_reg_tr_10GbE_adr_w));

  -- reg_eth10g
  constant c_reg_eth10g_adr_w                      : natural := 1;
  constant c_reg_eth10g_qsfp_ring_multi_adr_w      : natural := ceil_log2((g_nof_streams_qsfp + g_nof_streams_ring) * pow2(c_reg_eth10g_adr_w));
  constant c_reg_eth10g_back0_multi_adr_w          : natural := ceil_log2(g_nof_streams_back0 * pow2(c_reg_eth10g_adr_w));

  -- BSN monitors
  constant c_reg_rsp_bsn_monitor_10GbE_adr_w       : natural := ceil_log2(g_nof_streams_10GbE * pow2(c_unb2c_board_peripherals_mm_reg_default.reg_bsn_monitor_adr_w));

  -- Simulation
  constant c_sim_eth_src_mac                       : std_logic_vector(c_network_eth_mac_slv'range) := X"00228608" & TO_UVEC(g_sim_unb_nr, c_byte_w) & TO_UVEC(g_sim_node_nr, c_byte_w);
  constant c_sim_eth_control_rx_en                 : natural := 2**c_eth_mm_reg_control_bi.rx_en;

  signal sim_eth_mm_bus_switch                     : std_logic;
  signal sim_eth_psc_access                        : std_logic;

  signal i_eth_0_reg_mosi                     : t_mem_mosi;
  signal i_eth_0_reg_miso                     : t_mem_miso;
  signal i_eth_1_reg_mosi                     : t_mem_mosi;
  signal i_eth_1_reg_miso                     : t_mem_miso;

  signal sim_eth_0_reg_mosi                   : t_mem_mosi;
  signal sim_eth_1_reg_mosi                   : t_mem_mosi;
  signal i_reset_n                                 : std_logic;
begin
  ----------------------------------------------------------------------------
  -- MM <-> file I/O for simulation. The files are created in $UPE/sim.
  ----------------------------------------------------------------------------
  gen_mm_file_io : if g_sim = true generate
    eth_0_mm_rst <= mm_rst;
    eth_1_mm_rst <= mm_rst;

    u_mm_file_reg_unb_system_info   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_SYSTEM_INFO")
                                                 port map(mm_rst, mm_clk, reg_unb_system_info_mosi, reg_unb_system_info_miso );

    u_mm_file_rom_unb_system_info   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "ROM_SYSTEM_INFO")
                                                 port map(mm_rst, mm_clk, rom_unb_system_info_mosi, rom_unb_system_info_miso );

    u_mm_file_reg_wdi               : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_WDI")
                                                 port map(mm_rst, mm_clk, reg_wdi_mosi, reg_wdi_miso );

    u_mm_file_reg_fpga_temp_sens  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_TEMP_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_temp_sens_mosi, reg_fpga_temp_sens_miso );

    u_mm_file_reg_fpga_voltage_sens :  mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_FPGA_VOLTAGE_SENS")
                                               port map(mm_rst, mm_clk, reg_fpga_voltage_sens_mosi, reg_fpga_voltage_sens_miso );

    u_mm_file_reg_ppsh              : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "PIO_PPS")
                                                 port map(mm_rst, mm_clk, reg_ppsh_mosi, reg_ppsh_miso );

    u_mm_file_reg_diag_bg_10GbE     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG_10GBE")
                                                 port map(mm_rst, mm_clk, reg_diag_bg_10GbE_mosi, reg_diag_bg_10GbE_miso);
    u_mm_file_ram_diag_bg_10GbE     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_BG_10GBE")
                                                 port map(mm_rst, mm_clk, ram_diag_bg_10GbE_mosi, ram_diag_bg_10GbE_miso);
    u_mm_file_reg_diag_tx_seq_10GbE : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_10GBE")
                                                 port map(mm_rst, mm_clk, reg_diag_tx_seq_10GbE_mosi, reg_diag_tx_seq_10GbE_miso);

    u_mm_file_reg_bsn_monitor_10GbE      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_10GBE")
                                                      port map(mm_rst, mm_clk, reg_bsn_monitor_10GbE_mosi, reg_bsn_monitor_10GbE_miso);

    u_mm_file_reg_diag_data_buffer_10GbE : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_10GBE")
                                                      port map(mm_rst, mm_clk, reg_diag_data_buf_10GbE_mosi, reg_diag_data_buf_10GbE_miso);
    u_mm_file_ram_diag_data_buffer_10GbE : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_10GBE")
                                                      port map(mm_rst, mm_clk, ram_diag_data_buf_10GbE_mosi, ram_diag_data_buf_10GbE_miso);
    u_mm_file_reg_diag_rx_seq_10GbE      : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_10GBE")
                                                      port map(mm_rst, mm_clk, reg_diag_rx_seq_10GbE_mosi, reg_diag_rx_seq_10GbE_miso);

    u_mm_file_reg_io_ddr_MB_I                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_IO_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_io_ddr_MB_I_mosi, reg_io_ddr_MB_I_miso);
    u_mm_file_reg_diag_tx_seq_ddr_MB_I        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_tx_seq_ddr_MB_I_mosi, reg_diag_tx_seq_ddr_MB_I_miso);
    u_mm_file_reg_diag_rx_seq_ddr_MB_I        : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_rx_seq_ddr_MB_I_mosi, reg_diag_rx_seq_ddr_MB_I_miso);
    u_mm_file_reg_diag_data_buffer_ddr_MB_I   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, reg_diag_data_buf_ddr_MB_I_mosi, reg_diag_data_buf_ddr_MB_I_miso);
    u_mm_file_ram_diag_data_buffer_ddr_MB_I   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_DDR_MB_I")
                                                           port map(mm_rst, mm_clk, ram_diag_data_buf_ddr_MB_I_mosi, ram_diag_data_buf_ddr_MB_I_miso);

    u_mm_file_reg_io_ddr_MB_II                : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_IO_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_io_ddr_MB_II_mosi, reg_io_ddr_MB_II_miso);
    u_mm_file_reg_diag_tx_seq_ddr_MB_II       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_TX_SEQ_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_tx_seq_ddr_MB_II_mosi, reg_diag_tx_seq_ddr_MB_II_miso);
    u_mm_file_reg_diag_rx_seq_ddr_MB_II       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_RX_SEQ_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_rx_seq_ddr_MB_II_mosi, reg_diag_rx_seq_ddr_MB_II_miso);
    u_mm_file_reg_diag_data_buffer_ddr_MB_II  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_DATA_BUFFER_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, reg_diag_data_buf_ddr_MB_II_mosi, reg_diag_data_buf_ddr_MB_II_miso);
    u_mm_file_ram_diag_data_buffer_ddr_MB_II  : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_DIAG_DATA_BUFFER_DDR_MB_II")
                                                           port map(mm_rst, mm_clk, ram_diag_data_buf_ddr_MB_II_mosi, ram_diag_data_buf_ddr_MB_II_miso);

    -- Note: the eth1g RAM and TSE buses are only required by unb_osy on the NIOS as they provide the ethernet<->MM gateway.
    -- . 1GbE_I with TSE setup by NiosII
    u_mm_file_reg_eth_0_tse       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_TSE")
                                               port map(mm_rst, mm_clk, eth_0_tse_mosi, eth_0_tse_miso);
    u_mm_file_reg_eth_0_reg       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_REG")
                                               port map(mm_rst, mm_clk, i_eth_0_reg_mosi, eth_0_reg_miso);
    u_mm_file_reg_eth_0_ram       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_0_RAM")
                                               port map(mm_rst, mm_clk, eth_0_ram_mosi, eth_0_ram_miso);
    -- . 1GbE_II with TSE setup in VHDL
    u_mm_file_reg_eth_1_tse       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "AVS_ETH_1_TSE")
                                               port map(mm_rst, mm_clk, eth_1_tse_mosi, eth_1_tse_miso);

    u_mm_file_reg_tr_10GbE_qsfp_ring : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_QSFP_RING")
                                                  port map(mm_rst, mm_clk, reg_tr_10GbE_qsfp_ring_mosi, reg_tr_10GbE_qsfp_ring_miso);
    u_mm_file_reg_tr_10GbE_back0     : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_TR_10GBE_BACK0")
                                                  port map(mm_rst, mm_clk, reg_tr_10GbE_back0_mosi, reg_tr_10GbE_back0_miso);

    u_mm_file_reg_eth10g_qsfp_ring   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_ETH10G_QSFP_RING")
                                                  port map(mm_rst, mm_clk, reg_eth10g_qsfp_ring_mosi, reg_eth10g_qsfp_ring_miso);
    u_mm_file_reg_eth10g_back0       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_ETH10G_BACK0")
                                                  port map(mm_rst, mm_clk, reg_eth10g_back0_mosi, reg_eth10g_back0_miso);

    u_mm_file_reg_heater          : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_HEATER")
                                               port map(mm_rst, mm_clk, reg_heater_mosi, reg_heater_miso );
    u_mm_file_ram_scrap           : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "RAM_SCRAP")
                                               port map(mm_rst, mm_clk, ram_scrap_mosi, ram_scrap_miso );

    u_mm_file_reg_reg_diag_bg_eth_0             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG_ETH_0")
                                                             port map(mm_rst, mm_clk, reg_diag_bg_eth_0_copi, reg_diag_bg_eth_0_cipo );
    u_mm_file_reg_hdr_dat_eth_0                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_HDR_DAT_ETH_0")
                                                             port map(mm_rst, mm_clk, reg_hdr_dat_eth_0_copi, reg_hdr_dat_eth_0_cipo );
    u_mm_file_reg_bsn_monitor_v2_tx_eth_0       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_TX_ETH_0")
                                                             port map(mm_rst, mm_clk, reg_bsn_monitor_v2_tx_eth_0_copi, reg_bsn_monitor_v2_tx_eth_0_cipo );
    u_mm_file_reg_strobe_total_count_tx_eth_0   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STROBE_TOTAL_COUNT_TX_ETH_0")
                                                             port map(mm_rst, mm_clk, reg_strobe_total_count_tx_eth_0_copi, reg_strobe_total_count_tx_eth_0_cipo );
    u_mm_file_reg_bsn_monitor_v2_rx_eth_0       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RX_ETH_0")
                                                             port map(mm_rst, mm_clk, reg_bsn_monitor_v2_rx_eth_0_copi, reg_bsn_monitor_v2_rx_eth_0_cipo );
    u_mm_file_reg_strobe_total_count_rx_eth_0   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STROBE_TOTAL_COUNT_RX_ETH_0")
                                                             port map(mm_rst, mm_clk, reg_strobe_total_count_rx_eth_0_copi, reg_strobe_total_count_rx_eth_0_cipo );

    u_mm_file_reg_reg_diag_bg_eth_1             : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_DIAG_BG_ETH_1")
                                                             port map(mm_rst, mm_clk, reg_diag_bg_eth_1_copi, reg_diag_bg_eth_1_cipo );
    u_mm_file_reg_hdr_dat_eth_1                 : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_HDR_DAT_ETH_1")
                                                             port map(mm_rst, mm_clk, reg_hdr_dat_eth_1_copi, reg_hdr_dat_eth_1_cipo );
    u_mm_file_reg_bsn_monitor_v2_tx_eth_1       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_TX_ETH_1")
                                                             port map(mm_rst, mm_clk, reg_bsn_monitor_v2_tx_eth_1_copi, reg_bsn_monitor_v2_tx_eth_1_cipo );
    u_mm_file_reg_strobe_total_count_tx_eth_1   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STROBE_TOTAL_COUNT_TX_ETH_1")
                                                             port map(mm_rst, mm_clk, reg_strobe_total_count_tx_eth_1_copi, reg_strobe_total_count_tx_eth_1_cipo );
    u_mm_file_reg_bsn_monitor_v2_rx_eth_1       : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_BSN_MONITOR_V2_RX_ETH_1")
                                                             port map(mm_rst, mm_clk, reg_bsn_monitor_v2_rx_eth_1_copi, reg_bsn_monitor_v2_rx_eth_1_cipo );
    u_mm_file_reg_strobe_total_count_rx_eth_1   : mm_file generic map(mmf_unb_file_prefix(g_sim_unb_nr, c_sim_node_nr, c_sim_node_type) & "REG_STROBE_TOTAL_COUNT_RX_ETH_1")
                                                             port map(mm_rst, mm_clk, reg_strobe_total_count_rx_eth_1_copi, reg_strobe_total_count_rx_eth_1_cipo );

    ----------------------------------------------------------------------------
    -- 1GbE setup sequence normally performed by unb_os@NIOS
    ----------------------------------------------------------------------------
    p_eth_setup : process
    begin
      sim_eth_mm_bus_switch <= '1';

      eth_0_tse_mosi.wr <= '0';
      eth_0_tse_mosi.rd <= '0';
      wait for 400 ns;
      wait until rising_edge(mm_clk);
      proc_tech_tse_setup(c_tech_arria10_e1sg, false, c_tech_tse_tx_fifo_depth, c_tech_tse_rx_fifo_depth, c_tech_tse_tx_ready_latency, c_sim_eth_src_mac, sim_eth_psc_access, mm_clk, eth_0_tse_miso, eth_0_tse_mosi);

      -- Enable RX
      proc_mem_mm_bus_wr(c_eth_reg_control_wi + 0, c_sim_eth_control_rx_en, mm_clk, eth_0_reg_miso, sim_eth_0_reg_mosi);  -- control rx en
      sim_eth_mm_bus_switch <= '0';

      wait;
    end process;

    p_switch : process(sim_eth_mm_bus_switch, sim_eth_0_reg_mosi, i_eth_0_reg_mosi)
    begin
      if sim_eth_mm_bus_switch = '1' then
          eth_0_reg_mosi <= sim_eth_0_reg_mosi;
        else
          eth_0_reg_mosi <= i_eth_0_reg_mosi;
        end if;
    end process;

    ----------------------------------------------------------------------------
    -- Procedure that polls a sim control file that can be used to e.g. get
    -- the simulation time in ns
    ----------------------------------------------------------------------------
    mmf_poll_sim_ctrl_file(mm_clk, c_mmf_unb_file_path & "sim.ctrl", c_mmf_unb_file_path & "sim.stat");
  end generate;

  i_reset_n <= not mm_rst;

  ----------------------------------------------------------------------------
  -- QSYS for synthesis
  ----------------------------------------------------------------------------
  gen_qsys : if g_sim = false generate
    u_qsys : qsys_unb2c_test
    port map (

      clk_clk                                   => mm_clk,
      reset_reset_n                             => i_reset_n,

      -- the_pio_wdi: toggled by NIOS II application unb_osy. Connects to WDI via ctrl_unb2c_board.
      pio_wdi_external_connection_export        => pout_wdi,

      avs_eth_0_reset_export                    => eth_0_mm_rst,
      avs_eth_0_clk_export                      => OPEN,
      avs_eth_0_tse_address_export              => eth_0_tse_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_0_tse_write_export                => eth_0_tse_mosi.wr,
      avs_eth_0_tse_read_export                 => eth_0_tse_mosi.rd,
      avs_eth_0_tse_writedata_export            => eth_0_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_tse_readdata_export             => eth_0_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_tse_waitrequest_export          => eth_0_tse_miso.waitrequest,
      avs_eth_0_reg_address_export              => eth_0_reg_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_0_reg_write_export                => eth_0_reg_mosi.wr,
      avs_eth_0_reg_read_export                 => eth_0_reg_mosi.rd,
      avs_eth_0_reg_writedata_export            => eth_0_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_reg_readdata_export             => eth_0_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_ram_address_export              => eth_0_ram_mosi.address(c_unb2c_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_0_ram_write_export                => eth_0_ram_mosi.wr,
      avs_eth_0_ram_read_export                 => eth_0_ram_mosi.rd,
      avs_eth_0_ram_writedata_export            => eth_0_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_0_ram_readdata_export             => eth_0_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_0_irq_export                      => eth_0_reg_interrupt,

      avs_eth_1_reset_export               => eth_1_mm_rst,
      avs_eth_1_clk_export                 => OPEN,
      avs_eth_1_tse_address_export         => eth_1_tse_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_tse_adr_w - 1 downto 0),
      avs_eth_1_tse_write_export           => eth_1_tse_mosi.wr,
      avs_eth_1_tse_read_export            => eth_1_tse_mosi.rd,
      avs_eth_1_tse_writedata_export       => eth_1_tse_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_1_tse_readdata_export        => eth_1_tse_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_1_tse_waitrequest_export     => eth_1_tse_miso.waitrequest,
      avs_eth_1_reg_address_export         => eth_1_reg_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_eth_adr_w - 1 downto 0),
      avs_eth_1_reg_write_export           => eth_1_reg_mosi.wr,
      avs_eth_1_reg_read_export            => eth_1_reg_mosi.rd,
      avs_eth_1_reg_writedata_export       => eth_1_reg_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_1_reg_readdata_export        => eth_1_reg_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_1_ram_address_export         => eth_1_ram_mosi.address(c_unb2c_board_peripherals_mm_reg_default.ram_eth_adr_w - 1 downto 0),
      avs_eth_1_ram_write_export           => eth_1_ram_mosi.wr,
      avs_eth_1_ram_read_export            => eth_1_ram_mosi.rd,
      avs_eth_1_ram_writedata_export       => eth_1_ram_mosi.wrdata(c_word_w - 1 downto 0),
      avs_eth_1_ram_readdata_export        => eth_1_ram_miso.rddata(c_word_w - 1 downto 0),
      avs_eth_1_irq_export                 => eth_1_reg_interrupt,

      reg_fpga_temp_sens_reset_export           => OPEN,
      reg_fpga_temp_sens_clk_export             => OPEN,
      reg_fpga_temp_sens_address_export         => reg_fpga_temp_sens_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_fpga_temp_sens_adr_w - 1 downto 0),
      reg_fpga_temp_sens_write_export           => reg_fpga_temp_sens_mosi.wr,
      reg_fpga_temp_sens_writedata_export       => reg_fpga_temp_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_temp_sens_read_export            => reg_fpga_temp_sens_mosi.rd,
      reg_fpga_temp_sens_readdata_export        => reg_fpga_temp_sens_miso.rddata(c_word_w - 1 downto 0),

      reg_fpga_voltage_sens_reset_export        => OPEN,
      reg_fpga_voltage_sens_clk_export          => OPEN,
      reg_fpga_voltage_sens_address_export      => reg_fpga_voltage_sens_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_fpga_voltage_sens_adr_w - 1 downto 0),
      reg_fpga_voltage_sens_write_export        => reg_fpga_voltage_sens_mosi.wr,
      reg_fpga_voltage_sens_writedata_export    => reg_fpga_voltage_sens_mosi.wrdata(c_word_w - 1 downto 0),
      reg_fpga_voltage_sens_read_export         => reg_fpga_voltage_sens_mosi.rd,
      reg_fpga_voltage_sens_readdata_export     => reg_fpga_voltage_sens_miso.rddata(c_word_w - 1 downto 0),

      rom_system_info_reset_export              => OPEN,
      rom_system_info_clk_export                => OPEN,
      rom_system_info_address_export            => rom_unb_system_info_mosi.address(c_unb2c_board_peripherals_mm_reg_default.rom_unb_system_info_adr_w - 1 downto 0),
      rom_system_info_write_export              => rom_unb_system_info_mosi.wr,
      rom_system_info_writedata_export          => rom_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      rom_system_info_read_export               => rom_unb_system_info_mosi.rd,
      rom_system_info_readdata_export           => rom_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_system_info_reset_export              => OPEN,
      pio_system_info_clk_export                => OPEN,
      pio_system_info_address_export            => reg_unb_system_info_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_unb_system_info_adr_w - 1 downto 0),
      pio_system_info_write_export              => reg_unb_system_info_mosi.wr,
      pio_system_info_writedata_export          => reg_unb_system_info_mosi.wrdata(c_word_w - 1 downto 0),
      pio_system_info_read_export               => reg_unb_system_info_mosi.rd,
      pio_system_info_readdata_export           => reg_unb_system_info_miso.rddata(c_word_w - 1 downto 0),

      pio_pps_reset_export                      => OPEN,
      pio_pps_clk_export                        => OPEN,
      pio_pps_address_export                    => reg_ppsh_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_ppsh_adr_w - 1 downto 0),
      pio_pps_write_export                      => reg_ppsh_mosi.wr,
      pio_pps_writedata_export                  => reg_ppsh_mosi.wrdata(c_word_w - 1 downto 0),
      pio_pps_read_export                       => reg_ppsh_mosi.rd,
      pio_pps_readdata_export                   => reg_ppsh_miso.rddata(c_word_w - 1 downto 0),

      reg_wdi_reset_export                      => OPEN,
      reg_wdi_clk_export                        => OPEN,
      reg_wdi_address_export                    => reg_wdi_mosi.address(0 downto 0),
      reg_wdi_write_export                      => reg_wdi_mosi.wr,
      reg_wdi_writedata_export                  => reg_wdi_mosi.wrdata(c_word_w - 1 downto 0),
      reg_wdi_read_export                       => reg_wdi_mosi.rd,
      reg_wdi_readdata_export                   => reg_wdi_miso.rddata(c_word_w - 1 downto 0),

      reg_remu_reset_export                     => OPEN,
      reg_remu_clk_export                       => OPEN,
      reg_remu_address_export                   => reg_remu_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_remu_adr_w - 1 downto 0),
      reg_remu_write_export                     => reg_remu_mosi.wr,
      reg_remu_writedata_export                 => reg_remu_mosi.wrdata(c_word_w - 1 downto 0),
      reg_remu_read_export                      => reg_remu_mosi.rd,
      reg_remu_readdata_export                  => reg_remu_miso.rddata(c_word_w - 1 downto 0),

      reg_epcs_reset_export                     => OPEN,
      reg_epcs_clk_export                       => OPEN,
      reg_epcs_address_export                   => reg_epcs_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_epcs_adr_w - 1 downto 0),
      reg_epcs_write_export                     => reg_epcs_mosi.wr,
      reg_epcs_writedata_export                 => reg_epcs_mosi.wrdata(c_word_w - 1 downto 0),
      reg_epcs_read_export                      => reg_epcs_mosi.rd,
      reg_epcs_readdata_export                  => reg_epcs_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_ctrl_reset_export                => OPEN,
      reg_dpmm_ctrl_clk_export                  => OPEN,
      reg_dpmm_ctrl_address_export              => reg_dpmm_ctrl_mosi.address(0 downto 0),
      reg_dpmm_ctrl_write_export                => reg_dpmm_ctrl_mosi.wr,
      reg_dpmm_ctrl_writedata_export            => reg_dpmm_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      reg_dpmm_ctrl_read_export                 => reg_dpmm_ctrl_mosi.rd,
      reg_dpmm_ctrl_readdata_export             => reg_dpmm_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_mmdp_data_reset_export                => OPEN,
      reg_mmdp_data_clk_export                  => OPEN,
      reg_mmdp_data_address_export              => reg_mmdp_data_mosi.address(0 downto 0),
      reg_mmdp_data_write_export                => reg_mmdp_data_mosi.wr,
      reg_mmdp_data_writedata_export            => reg_mmdp_data_mosi.wrdata(c_word_w - 1 downto 0),
      reg_mmdp_data_read_export                 => reg_mmdp_data_mosi.rd,
      reg_mmdp_data_readdata_export             => reg_mmdp_data_miso.rddata(c_word_w - 1 downto 0),

      reg_dpmm_data_reset_export                => OPEN,
      reg_dpmm_data_clk_export                  => OPEN,
      reg_dpmm_data_address_export              => reg_dpmm_data_mosi.address(0 downto 0),
      reg_dpmm_data_read_export                 => reg_dpmm_data_mosi.rd,
      reg_dpmm_data_readdata_export             => reg_dpmm_data_miso.rddata(c_word_w - 1 downto 0),
      reg_dpmm_data_write_export                => reg_dpmm_data_mosi.wr,
      reg_dpmm_data_writedata_export            => reg_dpmm_data_mosi.wrdata(c_word_w - 1 downto 0),

      reg_mmdp_ctrl_reset_export                => OPEN,
      reg_mmdp_ctrl_clk_export                  => OPEN,
      reg_mmdp_ctrl_address_export              => reg_mmdp_ctrl_mosi.address(0 downto 0),
      reg_mmdp_ctrl_read_export                 => reg_mmdp_ctrl_mosi.rd,
      reg_mmdp_ctrl_readdata_export             => reg_mmdp_ctrl_miso.rddata(c_word_w - 1 downto 0),
      reg_mmdp_ctrl_write_export                => reg_mmdp_ctrl_mosi.wr,
      reg_mmdp_ctrl_writedata_export            => reg_mmdp_ctrl_mosi.wrdata(c_word_w - 1 downto 0),

      reg_tr_10gbe_qsfp_ring_reset_export       => OPEN,
      reg_tr_10gbe_qsfp_ring_clk_export         => OPEN,
      reg_tr_10gbe_qsfp_ring_address_export     => reg_tr_10GbE_qsfp_ring_mosi.address(c_reg_tr_10GbE_qsfp_ring_multi_adr_w - 1 downto 0),
      reg_tr_10gbe_qsfp_ring_write_export       => reg_tr_10GbE_qsfp_ring_mosi.wr,
      reg_tr_10gbe_qsfp_ring_writedata_export   => reg_tr_10GbE_qsfp_ring_mosi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10gbe_qsfp_ring_read_export        => reg_tr_10GbE_qsfp_ring_mosi.rd,
      reg_tr_10gbe_qsfp_ring_readdata_export    => reg_tr_10GbE_qsfp_ring_miso.rddata(c_word_w - 1 downto 0),
      reg_tr_10gbe_qsfp_ring_waitrequest_export => reg_tr_10GbE_qsfp_ring_miso.waitrequest,

      reg_tr_10gbe_back0_reset_export           => OPEN,
      reg_tr_10gbe_back0_clk_export             => OPEN,
      reg_tr_10gbe_back0_address_export         => reg_tr_10GbE_back0_mosi.address(c_reg_tr_10GbE_back0_multi_adr_w - 1 downto 0),
      reg_tr_10gbe_back0_write_export           => reg_tr_10GbE_back0_mosi.wr,
      reg_tr_10gbe_back0_writedata_export       => reg_tr_10GbE_back0_mosi.wrdata(c_word_w - 1 downto 0),
      reg_tr_10gbe_back0_read_export            => reg_tr_10GbE_back0_mosi.rd,
      reg_tr_10gbe_back0_readdata_export        => reg_tr_10GbE_back0_miso.rddata(c_word_w - 1 downto 0),
      reg_tr_10gbe_back0_waitrequest_export     => reg_tr_10GbE_back0_miso.waitrequest,

      reg_eth10g_qsfp_ring_reset_export         => OPEN,
      reg_eth10g_qsfp_ring_clk_export           => OPEN,
      reg_eth10g_qsfp_ring_address_export       => reg_eth10g_qsfp_ring_mosi.address(c_reg_eth10g_qsfp_ring_multi_adr_w - 1 downto 0),
      reg_eth10g_qsfp_ring_write_export         => reg_eth10g_qsfp_ring_mosi.wr,
      reg_eth10g_qsfp_ring_writedata_export     => reg_eth10g_qsfp_ring_mosi.wrdata(c_word_w - 1 downto 0),
      reg_eth10g_qsfp_ring_read_export          => reg_eth10g_qsfp_ring_mosi.rd,
      reg_eth10g_qsfp_ring_readdata_export      => reg_eth10g_qsfp_ring_miso.rddata(c_word_w - 1 downto 0),

      reg_eth10g_back0_reset_export             => OPEN,
      reg_eth10g_back0_clk_export               => OPEN,
      reg_eth10g_back0_address_export           => reg_eth10g_back0_mosi.address(c_reg_eth10g_back0_multi_adr_w - 1 downto 0),
      reg_eth10g_back0_write_export             => reg_eth10g_back0_mosi.wr,
      reg_eth10g_back0_writedata_export         => reg_eth10g_back0_mosi.wrdata(c_word_w - 1 downto 0),
      reg_eth10g_back0_read_export              => reg_eth10g_back0_mosi.rd,
      reg_eth10g_back0_readdata_export          => reg_eth10g_back0_miso.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_10gbe_reset_export        => OPEN,
      reg_bsn_monitor_10gbe_clk_export          => OPEN,
      reg_bsn_monitor_10gbe_address_export      => reg_bsn_monitor_10GbE_mosi.address(c_reg_rsp_bsn_monitor_10GbE_adr_w - 1 downto 0),
      reg_bsn_monitor_10gbe_write_export        => reg_bsn_monitor_10GbE_mosi.wr,
      reg_bsn_monitor_10gbe_writedata_export    => reg_bsn_monitor_10GbE_mosi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_10gbe_read_export         => reg_bsn_monitor_10GbE_mosi.rd,
      reg_bsn_monitor_10gbe_readdata_export     => reg_bsn_monitor_10GbE_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_10gbe_reset_export     => OPEN,
      reg_diag_data_buffer_10gbe_clk_export       => OPEN,
      reg_diag_data_buffer_10gbe_address_export   => reg_diag_data_buf_10gbe_mosi.address(5 downto 0),
      reg_diag_data_buffer_10gbe_write_export     => reg_diag_data_buf_10gbe_mosi.wr,
      reg_diag_data_buffer_10gbe_writedata_export => reg_diag_data_buf_10gbe_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_10gbe_read_export      => reg_diag_data_buf_10gbe_mosi.rd,
      reg_diag_data_buffer_10gbe_readdata_export  => reg_diag_data_buf_10gbe_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_10gbe_clk_export       => OPEN,
      ram_diag_data_buffer_10gbe_reset_export     => OPEN,
      ram_diag_data_buffer_10gbe_address_export   => ram_diag_data_buf_10gbe_mosi.address(c_ram_diag_databuffer_10GbE_addr_w - 1 downto 0),
      ram_diag_data_buffer_10gbe_write_export     => ram_diag_data_buf_10gbe_mosi.wr,
      ram_diag_data_buffer_10gbe_writedata_export => ram_diag_data_buf_10gbe_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_10gbe_read_export      => ram_diag_data_buf_10gbe_mosi.rd,
      ram_diag_data_buffer_10gbe_readdata_export  => ram_diag_data_buf_10gbe_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_bg_10GbE_reset_export              => OPEN,
      reg_diag_bg_10GbE_clk_export                => OPEN,
      reg_diag_bg_10GbE_address_export            => reg_diag_bg_10GbE_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_diag_bg_adr_w - 1 downto 0),
      reg_diag_bg_10GbE_write_export              => reg_diag_bg_10GbE_mosi.wr,
      reg_diag_bg_10GbE_writedata_export          => reg_diag_bg_10GbE_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_bg_10GbE_read_export               => reg_diag_bg_10GbE_mosi.rd,
      reg_diag_bg_10GbE_readdata_export           => reg_diag_bg_10GbE_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_bg_10GbE_reset_export              => OPEN,
      ram_diag_bg_10GbE_clk_export                => OPEN,
      ram_diag_bg_10GbE_address_export            => ram_diag_bg_10GbE_mosi.address(c_ram_diag_bg_10GbE_addr_w - 1 downto 0),
      ram_diag_bg_10GbE_write_export              => ram_diag_bg_10GbE_mosi.wr,
      ram_diag_bg_10GbE_writedata_export          => ram_diag_bg_10GbE_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_bg_10GbE_read_export               => ram_diag_bg_10GbE_mosi.rd,
      ram_diag_bg_10GbE_readdata_export           => ram_diag_bg_10GbE_miso.rddata(c_word_w - 1 downto 0),

      reg_io_ddr_MB_I_address_export                  => reg_io_ddr_MB_I_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_io_ddr_adr_w - 1 downto 0),
      reg_io_ddr_MB_I_clk_export                      => OPEN,
      reg_io_ddr_MB_I_read_export                     => reg_io_ddr_MB_I_mosi.rd,
      reg_io_ddr_MB_I_readdata_export                 => reg_io_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),
      reg_io_ddr_MB_I_reset_export                    => OPEN,
      reg_io_ddr_MB_I_write_export                    => reg_io_ddr_MB_I_mosi.wr,
      reg_io_ddr_MB_I_writedata_export                => reg_io_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),

      reg_io_ddr_MB_II_address_export                 => reg_io_ddr_MB_II_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_io_ddr_adr_w - 1 downto 0),
      reg_io_ddr_MB_II_clk_export                     => OPEN,
      reg_io_ddr_MB_II_read_export                    => reg_io_ddr_MB_II_mosi.rd,
      reg_io_ddr_MB_II_readdata_export                => reg_io_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),
      reg_io_ddr_MB_II_reset_export                   => OPEN,
      reg_io_ddr_MB_II_write_export                   => reg_io_ddr_MB_II_mosi.wr,
      reg_io_ddr_MB_II_writedata_export               => reg_io_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),

      reg_diag_tx_seq_ddr_MB_I_reset_export           => OPEN,
      reg_diag_tx_seq_ddr_MB_I_clk_export             => OPEN,
      reg_diag_tx_seq_ddr_MB_I_address_export         => reg_diag_tx_seq_ddr_MB_I_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_diag_tx_seq_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_I_write_export           => reg_diag_tx_seq_ddr_MB_I_mosi.wr,
      reg_diag_tx_seq_ddr_MB_I_writedata_export       => reg_diag_tx_seq_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_I_read_export            => reg_diag_tx_seq_ddr_MB_I_mosi.rd,
      reg_diag_tx_seq_ddr_MB_I_readdata_export        => reg_diag_tx_seq_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_tx_seq_ddr_MB_II_reset_export          => OPEN,
      reg_diag_tx_seq_ddr_MB_II_clk_export            => OPEN,
      reg_diag_tx_seq_ddr_MB_II_address_export        => reg_diag_tx_seq_ddr_MB_II_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_diag_tx_seq_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_II_write_export          => reg_diag_tx_seq_ddr_MB_II_mosi.wr,
      reg_diag_tx_seq_ddr_MB_II_writedata_export      => reg_diag_tx_seq_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_tx_seq_ddr_MB_II_read_export           => reg_diag_tx_seq_ddr_MB_II_mosi.rd,
      reg_diag_tx_seq_ddr_MB_II_readdata_export       => reg_diag_tx_seq_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_rx_seq_ddr_MB_I_reset_export           => OPEN,
      reg_diag_rx_seq_ddr_MB_I_clk_export             => OPEN,
      reg_diag_rx_seq_ddr_MB_I_address_export         => reg_diag_rx_seq_ddr_MB_I_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_diag_rx_seq_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_I_write_export           => reg_diag_rx_seq_ddr_MB_I_mosi.wr,
      reg_diag_rx_seq_ddr_MB_I_writedata_export       => reg_diag_rx_seq_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_I_read_export            => reg_diag_rx_seq_ddr_MB_I_mosi.rd,
      reg_diag_rx_seq_ddr_MB_I_readdata_export        => reg_diag_rx_seq_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_rx_seq_ddr_MB_II_reset_export          => OPEN,
      reg_diag_rx_seq_ddr_MB_II_clk_export            => OPEN,
      reg_diag_rx_seq_ddr_MB_II_address_export        => reg_diag_rx_seq_ddr_MB_II_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_diag_rx_seq_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_II_write_export          => reg_diag_rx_seq_ddr_MB_II_mosi.wr,
      reg_diag_rx_seq_ddr_MB_II_writedata_export      => reg_diag_rx_seq_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_rx_seq_ddr_MB_II_read_export           => reg_diag_rx_seq_ddr_MB_II_mosi.rd,
      reg_diag_rx_seq_ddr_MB_II_readdata_export       => reg_diag_rx_seq_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_ddr_MB_I_reset_export      => OPEN,
      reg_diag_data_buffer_ddr_MB_I_clk_export        => OPEN,
      reg_diag_data_buffer_ddr_MB_I_address_export    => reg_diag_data_buf_ddr_MB_I_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_I_write_export      => reg_diag_data_buf_ddr_MB_I_mosi.wr,
      reg_diag_data_buffer_ddr_MB_I_writedata_export  => reg_diag_data_buf_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_I_read_export       => reg_diag_data_buf_ddr_MB_I_mosi.rd,
      reg_diag_data_buffer_ddr_MB_I_readdata_export   => reg_diag_data_buf_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_ddr_MB_II_reset_export     => OPEN,
      reg_diag_data_buffer_ddr_MB_II_clk_export       => OPEN,
      reg_diag_data_buffer_ddr_MB_II_address_export   => reg_diag_data_buf_ddr_MB_II_mosi.address(c_unb2c_board_peripherals_mm_reg_default.reg_diag_db_adr_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_II_write_export     => reg_diag_data_buf_ddr_MB_II_mosi.wr,
      reg_diag_data_buffer_ddr_MB_II_writedata_export => reg_diag_data_buf_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_ddr_MB_II_read_export      => reg_diag_data_buf_ddr_MB_II_mosi.rd,
      reg_diag_data_buffer_ddr_MB_II_readdata_export  => reg_diag_data_buf_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_ddr_MB_I_clk_export        => OPEN,
      ram_diag_data_buffer_ddr_MB_I_reset_export      => OPEN,
      ram_diag_data_buffer_ddr_MB_I_address_export    => ram_diag_data_buf_ddr_MB_I_mosi.address(c_ram_diag_databuffer_ddr_addr_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_I_write_export      => ram_diag_data_buf_ddr_MB_I_mosi.wr,
      ram_diag_data_buffer_ddr_MB_I_writedata_export  => ram_diag_data_buf_ddr_MB_I_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_I_read_export       => ram_diag_data_buf_ddr_MB_I_mosi.rd,
      ram_diag_data_buffer_ddr_MB_I_readdata_export   => ram_diag_data_buf_ddr_MB_I_miso.rddata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_ddr_MB_II_clk_export       => OPEN,
      ram_diag_data_buffer_ddr_MB_II_reset_export     => OPEN,
      ram_diag_data_buffer_ddr_MB_II_address_export   => ram_diag_data_buf_ddr_MB_II_mosi.address(c_ram_diag_databuffer_ddr_addr_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_II_write_export     => ram_diag_data_buf_ddr_MB_II_mosi.wr,
      ram_diag_data_buffer_ddr_MB_II_writedata_export => ram_diag_data_buf_ddr_MB_II_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_ddr_MB_II_read_export      => ram_diag_data_buf_ddr_MB_II_mosi.rd,
      ram_diag_data_buffer_ddr_MB_II_readdata_export  => ram_diag_data_buf_ddr_MB_II_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_bg_eth_0_reset_export                    => OPEN,
      reg_diag_bg_eth_0_clk_export                      => OPEN,
      reg_diag_bg_eth_0_address_export                  => reg_diag_bg_eth_0_copi.address(4 downto 0),
      reg_diag_bg_eth_0_write_export                    => reg_diag_bg_eth_0_copi.wr,
      reg_diag_bg_eth_0_writedata_export                => reg_diag_bg_eth_0_copi.wrdata(c_word_w - 1 downto 0),
      reg_diag_bg_eth_0_read_export                     => reg_diag_bg_eth_0_copi.rd,
      reg_diag_bg_eth_0_readdata_export                 => reg_diag_bg_eth_0_cipo.rddata(c_word_w - 1 downto 0),

      reg_hdr_dat_eth_0_reset_export                    => OPEN,
      reg_hdr_dat_eth_0_clk_export                      => OPEN,
      reg_hdr_dat_eth_0_address_export                  => reg_hdr_dat_eth_0_copi.address(6 downto 0),
      reg_hdr_dat_eth_0_write_export                    => reg_hdr_dat_eth_0_copi.wr,
      reg_hdr_dat_eth_0_writedata_export                => reg_hdr_dat_eth_0_copi.wrdata(c_word_w - 1 downto 0),
      reg_hdr_dat_eth_0_read_export                     => reg_hdr_dat_eth_0_copi.rd,
      reg_hdr_dat_eth_0_readdata_export                 => reg_hdr_dat_eth_0_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_tx_eth_0_reset_export          => OPEN,
      reg_bsn_monitor_v2_tx_eth_0_clk_export            => OPEN,
      reg_bsn_monitor_v2_tx_eth_0_address_export        => reg_bsn_monitor_v2_tx_eth_0_copi.address(4 downto 0),
      reg_bsn_monitor_v2_tx_eth_0_write_export          => reg_bsn_monitor_v2_tx_eth_0_copi.wr,
      reg_bsn_monitor_v2_tx_eth_0_writedata_export      => reg_bsn_monitor_v2_tx_eth_0_copi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_tx_eth_0_read_export           => reg_bsn_monitor_v2_tx_eth_0_copi.rd,
      reg_bsn_monitor_v2_tx_eth_0_readdata_export       => reg_bsn_monitor_v2_tx_eth_0_cipo.rddata(c_word_w - 1 downto 0),

      reg_strobe_total_count_tx_eth_0_reset_export      => OPEN,
      reg_strobe_total_count_tx_eth_0_clk_export        => OPEN,
      reg_strobe_total_count_tx_eth_0_address_export    => reg_strobe_total_count_tx_eth_0_copi.address(6 downto 0),
      reg_strobe_total_count_tx_eth_0_write_export      => reg_strobe_total_count_tx_eth_0_copi.wr,
      reg_strobe_total_count_tx_eth_0_writedata_export  => reg_strobe_total_count_tx_eth_0_copi.wrdata(c_word_w - 1 downto 0),
      reg_strobe_total_count_tx_eth_0_read_export       => reg_strobe_total_count_tx_eth_0_copi.rd,
      reg_strobe_total_count_tx_eth_0_readdata_export   => reg_strobe_total_count_tx_eth_0_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_rx_eth_0_reset_export          => OPEN,
      reg_bsn_monitor_v2_rx_eth_0_clk_export            => OPEN,
      reg_bsn_monitor_v2_rx_eth_0_address_export        => reg_bsn_monitor_v2_rx_eth_0_copi.address(4 downto 0),
      reg_bsn_monitor_v2_rx_eth_0_write_export          => reg_bsn_monitor_v2_rx_eth_0_copi.wr,
      reg_bsn_monitor_v2_rx_eth_0_writedata_export      => reg_bsn_monitor_v2_rx_eth_0_copi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_rx_eth_0_read_export           => reg_bsn_monitor_v2_rx_eth_0_copi.rd,
      reg_bsn_monitor_v2_rx_eth_0_readdata_export       => reg_bsn_monitor_v2_rx_eth_0_cipo.rddata(c_word_w - 1 downto 0),

      reg_strobe_total_count_rx_eth_0_reset_export      => OPEN,
      reg_strobe_total_count_rx_eth_0_clk_export        => OPEN,
      reg_strobe_total_count_rx_eth_0_address_export    => reg_strobe_total_count_rx_eth_0_copi.address(6 downto 0),
      reg_strobe_total_count_rx_eth_0_write_export      => reg_strobe_total_count_rx_eth_0_copi.wr,
      reg_strobe_total_count_rx_eth_0_writedata_export  => reg_strobe_total_count_rx_eth_0_copi.wrdata(c_word_w - 1 downto 0),
      reg_strobe_total_count_rx_eth_0_read_export       => reg_strobe_total_count_rx_eth_0_copi.rd,
      reg_strobe_total_count_rx_eth_0_readdata_export   => reg_strobe_total_count_rx_eth_0_cipo.rddata(c_word_w - 1 downto 0),

      reg_diag_bg_eth_1_reset_export                   => OPEN,
      reg_diag_bg_eth_1_clk_export                     => OPEN,
      reg_diag_bg_eth_1_address_export                 => reg_diag_bg_eth_1_copi.address(2 downto 0),
      reg_diag_bg_eth_1_write_export                   => reg_diag_bg_eth_1_copi.wr,
      reg_diag_bg_eth_1_writedata_export               => reg_diag_bg_eth_1_copi.wrdata(c_word_w - 1 downto 0),
      reg_diag_bg_eth_1_read_export                    => reg_diag_bg_eth_1_copi.rd,
      reg_diag_bg_eth_1_readdata_export                => reg_diag_bg_eth_1_cipo.rddata(c_word_w - 1 downto 0),

      reg_hdr_dat_eth_1_reset_export                   => OPEN,
      reg_hdr_dat_eth_1_clk_export                     => OPEN,
      reg_hdr_dat_eth_1_address_export                 => reg_hdr_dat_eth_1_copi.address(4 downto 0),
      reg_hdr_dat_eth_1_write_export                   => reg_hdr_dat_eth_1_copi.wr,
      reg_hdr_dat_eth_1_writedata_export               => reg_hdr_dat_eth_1_copi.wrdata(c_word_w - 1 downto 0),
      reg_hdr_dat_eth_1_read_export                    => reg_hdr_dat_eth_1_copi.rd,
      reg_hdr_dat_eth_1_readdata_export                => reg_hdr_dat_eth_1_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_tx_eth_1_reset_export         => OPEN,
      reg_bsn_monitor_v2_tx_eth_1_clk_export           => OPEN,
      reg_bsn_monitor_v2_tx_eth_1_address_export       => reg_bsn_monitor_v2_tx_eth_1_copi.address(2 downto 0),
      reg_bsn_monitor_v2_tx_eth_1_write_export         => reg_bsn_monitor_v2_tx_eth_1_copi.wr,
      reg_bsn_monitor_v2_tx_eth_1_writedata_export     => reg_bsn_monitor_v2_tx_eth_1_copi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_tx_eth_1_read_export          => reg_bsn_monitor_v2_tx_eth_1_copi.rd,
      reg_bsn_monitor_v2_tx_eth_1_readdata_export      => reg_bsn_monitor_v2_tx_eth_1_cipo.rddata(c_word_w - 1 downto 0),

      reg_strobe_total_count_tx_eth_1_reset_export     => OPEN,
      reg_strobe_total_count_tx_eth_1_clk_export       => OPEN,
      reg_strobe_total_count_tx_eth_1_address_export   => reg_strobe_total_count_tx_eth_1_copi.address(4 downto 0),
      reg_strobe_total_count_tx_eth_1_write_export     => reg_strobe_total_count_tx_eth_1_copi.wr,
      reg_strobe_total_count_tx_eth_1_writedata_export => reg_strobe_total_count_tx_eth_1_copi.wrdata(c_word_w - 1 downto 0),
      reg_strobe_total_count_tx_eth_1_read_export      => reg_strobe_total_count_tx_eth_1_copi.rd,
      reg_strobe_total_count_tx_eth_1_readdata_export  => reg_strobe_total_count_tx_eth_1_cipo.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_v2_rx_eth_1_reset_export         => OPEN,
      reg_bsn_monitor_v2_rx_eth_1_clk_export           => OPEN,
      reg_bsn_monitor_v2_rx_eth_1_address_export       => reg_bsn_monitor_v2_rx_eth_1_copi.address(2 downto 0),
      reg_bsn_monitor_v2_rx_eth_1_write_export         => reg_bsn_monitor_v2_rx_eth_1_copi.wr,
      reg_bsn_monitor_v2_rx_eth_1_writedata_export     => reg_bsn_monitor_v2_rx_eth_1_copi.wrdata(c_word_w - 1 downto 0),
      reg_bsn_monitor_v2_rx_eth_1_read_export          => reg_bsn_monitor_v2_rx_eth_1_copi.rd,
      reg_bsn_monitor_v2_rx_eth_1_readdata_export      => reg_bsn_monitor_v2_rx_eth_1_cipo.rddata(c_word_w - 1 downto 0),

      reg_strobe_total_count_rx_eth_1_reset_export     => OPEN,
      reg_strobe_total_count_rx_eth_1_clk_export       => OPEN,
      reg_strobe_total_count_rx_eth_1_address_export   => reg_strobe_total_count_rx_eth_1_copi.address(4 downto 0),
      reg_strobe_total_count_rx_eth_1_write_export     => reg_strobe_total_count_rx_eth_1_copi.wr,
      reg_strobe_total_count_rx_eth_1_writedata_export => reg_strobe_total_count_rx_eth_1_copi.wrdata(c_word_w - 1 downto 0),
      reg_strobe_total_count_rx_eth_1_read_export      => reg_strobe_total_count_rx_eth_1_copi.rd,
      reg_strobe_total_count_rx_eth_1_readdata_export  => reg_strobe_total_count_rx_eth_1_cipo.rddata(c_word_w - 1 downto 0),

      reg_heater_reset_export                   => OPEN,
      reg_heater_clk_export                     => OPEN,
      reg_heater_address_export                 => reg_heater_mosi.address(4 downto 0),
      reg_heater_read_export                    => reg_heater_mosi.rd,
      reg_heater_readdata_export                => reg_heater_miso.rddata(c_word_w - 1 downto 0),
      reg_heater_write_export                   => reg_heater_mosi.wr,
      reg_heater_writedata_export               => reg_heater_mosi.wrdata(c_word_w - 1 downto 0),

      jesd204b_reset_export                     => OPEN,
      jesd204b_clk_export                       => OPEN,
      jesd204b_address_export                   => jesd204b_mosi.address(11 downto 0),
      jesd204b_write_export                     => jesd204b_mosi.wr,
      jesd204b_writedata_export                 => jesd204b_mosi.wrdata(c_word_w - 1 downto 0),
      jesd204b_read_export                      => jesd204b_mosi.rd,
      jesd204b_readdata_export                  => jesd204b_miso.rddata(c_word_w - 1 downto 0),

      pio_jesd_ctrl_reset_export               => OPEN,
      pio_jesd_ctrl_clk_export                 => OPEN,
      pio_jesd_ctrl_address_export             => jesd_ctrl_mosi.address(0 downto 0),
      pio_jesd_ctrl_write_export               => jesd_ctrl_mosi.wr,
      pio_jesd_ctrl_writedata_export           => jesd_ctrl_mosi.wrdata(c_word_w - 1 downto 0),
      pio_jesd_ctrl_read_export                => jesd_ctrl_mosi.rd,
      pio_jesd_ctrl_readdata_export            => jesd_ctrl_miso.rddata(c_word_w - 1 downto 0),

      reg_bsn_monitor_input_address_export      => reg_bsn_monitor_input_mosi.address(7 downto 0),
      reg_bsn_monitor_input_clk_export          => OPEN,
      reg_bsn_monitor_input_read_export         => reg_bsn_monitor_input_mosi.rd,
      reg_bsn_monitor_input_readdata_export     => reg_bsn_monitor_input_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_monitor_input_reset_export        => OPEN,
      reg_bsn_monitor_input_write_export        => reg_bsn_monitor_input_mosi.wr,
      reg_bsn_monitor_input_writedata_export    => reg_bsn_monitor_input_mosi.wrdata(c_word_w - 1 downto 0),

      reg_bsn_source_clk_export                 => OPEN,
      reg_bsn_source_reset_export               => OPEN,
      reg_bsn_source_address_export             => reg_bsn_source_mosi.address(1 downto 0),
      reg_bsn_source_read_export                => reg_bsn_source_mosi.rd,
      reg_bsn_source_readdata_export            => reg_bsn_source_miso.rddata(c_word_w - 1 downto 0),
      reg_bsn_source_write_export               => reg_bsn_source_mosi.wr,
      reg_bsn_source_writedata_export           => reg_bsn_source_mosi.wrdata(c_word_w - 1 downto 0),

      ram_diag_data_buffer_bsn_clk_export          => OPEN,
      ram_diag_data_buffer_bsn_reset_export        => OPEN,
      ram_diag_data_buffer_bsn_address_export      => ram_diag_data_buf_bsn_mosi.address(21 - 1 downto 0),  -- 22 = ceil_log2(12  * 256k), so maximum possible data buffer size is 256 kSamples
      ram_diag_data_buffer_bsn_write_export        => ram_diag_data_buf_bsn_mosi.wr,
      ram_diag_data_buffer_bsn_writedata_export    => ram_diag_data_buf_bsn_mosi.wrdata(c_word_w - 1 downto 0),
      ram_diag_data_buffer_bsn_read_export         => ram_diag_data_buf_bsn_mosi.rd,
      ram_diag_data_buffer_bsn_readdata_export     => ram_diag_data_buf_bsn_miso.rddata(c_word_w - 1 downto 0),

      reg_diag_data_buffer_bsn_reset_export        => OPEN,
      reg_diag_data_buffer_bsn_clk_export          => OPEN,
      reg_diag_data_buffer_bsn_address_export      => reg_diag_data_buf_bsn_mosi.address(12 - 1 downto 0),
      reg_diag_data_buffer_bsn_write_export        => reg_diag_data_buf_bsn_mosi.wr,
      reg_diag_data_buffer_bsn_writedata_export    => reg_diag_data_buf_bsn_mosi.wrdata(c_word_w - 1 downto 0),
      reg_diag_data_buffer_bsn_read_export         => reg_diag_data_buf_bsn_mosi.rd,
      reg_diag_data_buffer_bsn_readdata_export     => reg_diag_data_buf_bsn_miso.rddata(c_word_w - 1 downto 0),

      ram_scrap_reset_export                    => OPEN,
      ram_scrap_clk_export                      => OPEN,
      ram_scrap_address_export                  => ram_scrap_mosi.address(8 downto 0),
      ram_scrap_write_export                    => ram_scrap_mosi.wr,
      ram_scrap_writedata_export                => ram_scrap_mosi.wrdata(c_word_w - 1 downto 0),
      ram_scrap_read_export                     => ram_scrap_mosi.rd,
      ram_scrap_readdata_export                 => ram_scrap_miso.rddata(c_word_w - 1 downto 0)
    );
  end generate;
end str;
