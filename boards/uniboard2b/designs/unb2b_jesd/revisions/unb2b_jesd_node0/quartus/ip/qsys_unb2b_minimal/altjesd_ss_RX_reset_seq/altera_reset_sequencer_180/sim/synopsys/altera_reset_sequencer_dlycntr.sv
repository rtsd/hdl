// (C) 2001-2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files from any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel FPGA IP License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0

`pragma protect begin_protected
`pragma protect version=1
`pragma protect encrypt_agent="VCS"
`pragma protect encrypt_agent_info="J-2014.12-SP1 Build Date Feb 26 2015 20:50:25"
`pragma protect author="IP Provider"
`pragma protect key_keyowner="Synopsys"
`pragma protect key_keyname="SNPS-VCS-RSA-1"
`pragma protect key_method="rsa"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 128 )
`pragma protect key_block
xWrp2t9GSfS+pdWPZ/s56vuC0DS08im7o7amjb3AZSMvzjCuVaBoOEHLoQQtiHdl
YqcSweegpqL/CmYzG8PHZdnZwv/bNQor5xNoU2gcy4kEQ8qCFmLjm9V5Aractp9q
a1F8WHz8SxzaK7vvL6XbDrFLrSVH7WxbSisL7BYClMI=

`pragma protect data_method="aes128-cbc"
`pragma protect encoding = (enctype = "base64", line_length = 76, bytes = 832 )
`pragma protect data_block
61xH6gzVIxGsoT/9NJnwHK75hVwbCAccTmrq9+u7nX+O4MOmWAWry1THQ8MRxf1m
wbnFlEueUWuUNr+1ivilTlT4PgIStQGyEc0xVhC+Mih6fMeiVwpMFMG5GJUaS9v0
oMDRKuTIqeB+Oh3a2rAKjux9ez4p8OwJ38Hm/GZda7HrsCvvFmUxNdNfzcK04dFp
1FOx4T8bLkcWQ3DUzbqmJ3zGtC1mQT6zephon16i0LUBOmMcTrGizTOuPPxnNF3N
ebjqxSPL0fsXLDA/C0P2+tSektciwL3Q6NZLgSXCyM+Lu1M0EidELTll4JrOF8j6
XaxblPQGdKTZMbtIXpVp4ROax9iV1SLtwwe6pkZSn3Q1HpA3T4ri220OyPbYAZRv
u3yRzkRERC9+bc8u9vSBvAUrR2ixZp8XbidHg5qfTmWpQmSu3wE5O/kvY0zyZpZq
Mn9dzzvLloD1FI36CSFEk9fg9DecqKsMXSo840M6WeXTu6ATsjExOkNKwsylgJor
nW7vqR0eGqIuPCrihFGrEs4PKQXoWjQrpW/VYKVLu0radEptu11mWkC8zwmqp4oL
Alr3+xXnWH45LvzmnV3mvTz5Ow0cKHIPIDtYDoTAI2n2ulEO5fTRqilTY4ZVyJIe
TWz30I0qEfWe0rq/qlWwyJ0AL5nFzMb/wxjSotujEmxbN37t2QXFjZWKfEMqFnAR
ndgcmjCakkLbko5Upom4ghrqh6+pibrP+aBB6P74GAeda2JUJcp29Ztswfz9TrQT
nP38GPg3MIpd0ic9vygWYlBbymV7E1kZMs17qoksHUiSBkjZSuewxcHjBGB2bGSF
g4ejCjUQHgxAK2ypwtttz/FpvrMPCSJ7ylqP+YKZV4TFjFtJR0yutBUDDdeQVkZ8
Amwal6feQlX5Ngw6lBiewF2os6+UMJP582lF029ll53joldrFia8UHDZNu3ZNkpQ
g7OvkS9/701w27Lx/aAn7la9bPKKLroUvUDZr3wIx3CWnynS/itjdWZ2utnkLwu6
rI/KcxJofaHaMqPRKVu2aeHatSZ4qM+zbRYnPI1bBYSwj0y8xBiTaA4+ak0rSqz5
OGlXDPUBWVbmKcvvTUOYsg==

`pragma protect end_protected
