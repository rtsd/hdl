###############################################################################
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from common import *
from mem_init_file import list_to_hex

# Purpose:
# . Generate HEX files with weights 
# Description:
# . Simple weights: complex(1,0) so multiplier out = multiplier in. 

PATH = "../hex"
FILENAME = "beamformer_weights"

NOF_WEIGHTS_RAMS = 2
COMPLEX_WIDTH = 16
NOF_WEIGHTS = 32

MEM_WIDTH = 2*COMPLEX_WIDTH
MEM_DEPTH = NOF_WEIGHTS

###############################################################################
# Create the data list
# . 176 words = 2 (X,Y pols) interleaved streams of 88 complex numbers
# . X-pol = complex( 1, 1)
# . Y-pol = complex(-1,-1)
###############################################################################
complex_weights = [complex(i, 0) for i in range(NOF_WEIGHTS)]
concat_weights = concat_complex(complex_weights, COMPLEX_WIDTH)

print complex_weights
print concat_weights

###############################################################################
# Write the HEX file
###############################################################################
for i in range(NOF_WEIGHTS_RAMS):
    list_to_hex(concat_weights, PATH+"/"+FILENAME+"_"+str(i)+".hex", MEM_WIDTH, MEM_DEPTH)

