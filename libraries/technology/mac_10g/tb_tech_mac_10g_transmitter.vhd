-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Reusable test bench stimuli for tech_mac_10g transmitter
-- Description:
--   The packet transmission starts after mm_init goes low. The
--   g_pkt_length_arr1 contains the array of packet lengths for the packets
--   that will be transmitted. The packet data depends on g_data_type and can
--   be counter data that counts per word or per symbol.
--   If g_verify_link_recovery=TRUE then link_fault will be active for some
--   time during which no packets are transmitted. This link_fault can be used
--   to disable the rx path.
--   After link_fault is deasserted again the second series of packets defined
--   by g_pkt_length_arr2 will be transmitted to verify that the link has
--   recovered from the link_fault.
--   Some time after all packets have been transmitted the tx_end will go high.
--

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use WORK.tech_mac_10g_component_pkg.all;
use WORK.tb_tech_mac_10g_pkg.all;

entity tb_tech_mac_10g_transmitter is
  generic (
    --   g_data_type = c_tb_tech_mac_10g_data_type_symbols  = 0
    --   g_data_type = c_tb_tech_mac_10g_data_type_counter  = 1
    g_data_type             : natural := c_tb_tech_mac_10g_data_type_counter;
    g_pkt_length_arr1       : t_nat_natural_arr := array_init(0, 50, 1) & (1472, 1473) & 9000;  -- frame longer than 1518-46 = 1472 is received with rx_sosi.err = 8
                                                                                                -- jumbo frame is 9018-46 = 8972
    g_pkt_length_arr2       : t_nat_natural_arr := array_init(46, 10, 139) & 1472;
    g_verify_link_recovery  : boolean := false
  );
  port (
    mm_init        : in  std_logic;
    total_header   : in  t_network_total_header;
    tx_clk         : in  std_logic;
    tx_siso        : in  t_dp_siso;
    tx_sosi        : out t_dp_sosi;
    link_fault     : out std_logic;
    tx_end         : out std_logic
  );
end tb_tech_mac_10g_transmitter;

architecture tb of tb_tech_mac_10g_transmitter is
  constant c_rl                 : natural := 1;
  constant c_nof_tx_not_valid   : natural := 0;  -- when > 0 then pull tx valid low for c_nof_tx_not_valid beats during tx

  constant c_nof_pkt1           : natural := g_pkt_length_arr1'length;
  constant c_nof_pkt2           : natural := g_pkt_length_arr2'length;

  signal tx_en                  : std_logic := '1';
begin
  p_ff_transmitter : process
  begin
    tx_end <= '0';
    link_fault <= '0';
    tx_sosi <= c_dp_sosi_rst;

    proc_common_wait_until_low(tx_clk, mm_init);
    proc_common_wait_some_cycles(tx_clk, 10);

    -- Loopback txp->rxp so use promiscuous mode or use DST_MAC = c_src_mac to send to itself

    -- TX frame:g_pkt_length_arr1
    -- . I=0 is empty payload, so only 4 words of the ETH header with 46 padding zeros, so empty = 2
    -- . For I=1 to 46 the payload length remains 46 with padding zeros, so empty = 2
    -- . For I>46 the payload length is I and empty = 4 - (I mod 4)

    for I in 0 to c_nof_pkt1 - 1 loop
      proc_tech_mac_10g_tx_packet(total_header, g_pkt_length_arr1(I), g_data_type, c_rl, c_nof_tx_not_valid, tx_clk, tx_en, tx_siso, tx_sosi);
      proc_common_wait_some_cycles(tx_clk, 0);
    end loop;

    proc_common_wait_some_cycles(tx_clk, g_pkt_length_arr1(c_nof_pkt1 - 1) / c_tech_mac_10g_symbols_per_beat);
    proc_common_wait_some_cycles(tx_clk, 100);

    if g_verify_link_recovery = true then
      -- Model a link fault to verify Rx recovery
      link_fault <= '1';
      proc_common_wait_some_cycles(tx_clk, 1000);
      -- Remove the link fault and wait for tx_siso.xon to recover
      link_fault <= '0';
      proc_common_wait_some_cycles(tx_clk, 500);

      for I in 0 to c_nof_pkt2 - 1 loop
        proc_tech_mac_10g_tx_packet(total_header, g_pkt_length_arr2(I), g_data_type, c_rl, c_nof_tx_not_valid, tx_clk, tx_en, tx_siso, tx_sosi);
        proc_common_wait_some_cycles(tx_clk, 0);
      end loop;

      proc_common_wait_some_cycles(tx_clk, g_pkt_length_arr2(c_nof_pkt2 - 1) / c_tech_mac_10g_symbols_per_beat);
      proc_common_wait_some_cycles(tx_clk, 100);
    end if;

    tx_end <= '1';
    wait;
  end process;
end tb;
