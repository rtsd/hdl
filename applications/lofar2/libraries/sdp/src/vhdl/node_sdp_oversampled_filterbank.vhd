-------------------------------------------------------------------------------
--
-- Copyright 2020
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author: R. van der Walle
-- Purpose:
-- . Implements the functionality of the complex subband filterbank (Fsub complex) in the
--   DISTURB SDPFW design.
-- Description:
-- . See https://support.astron.nl/confluence/display/DISTURB/DISTURB-2+Station+Digital+Processor+Design+Document
-- . The subband filterbank seperates the incoming timestamped ADC samples into
--   512 frequency bands called subbands.
-- . It implements a critically sampled poly-phase filterbank (PFB). The PFB consists of a
--   poly-phase finite impulse response (PFIR) filter per complex input and a
--   complex fast fourier transform (FFT) per complex input.
-- . The number of points of the FFT is 1024.
-- Remark:
-- .
-------------------------------------------------------------------------------

library IEEE, common_lib, dp_lib, mm_lib, diag_lib, common_mult_lib;
library rTwoSDF_lib, fft_lib, wpfb_lib, filter_lib, si_lib, st_lib;
  use IEEE.std_logic_1164.all;
  use common_lib.common_pkg.all;
  use common_lib.common_mem_pkg.all;
  use common_lib.common_network_layers_pkg.all;
  use dp_lib.dp_stream_pkg.all;
  use diag_lib.diag_pkg.all;
  use rTwoSDF_lib.rTwoSDFPkg.all;
  use filter_lib.fil_pkg.all;
  use fft_lib.fft_pkg.all;
  use wpfb_lib.wpfb_pkg.all;
  use work.sdp_pkg.all;

entity node_sdp_oversampled_filterbank is
  generic (
    g_sim                    : boolean := false;
    g_sim_sdp                : t_sdp_sim := c_sdp_sim;
    g_wpfb                   : t_wpfb := c_sdp_wpfb_subbands;
    g_wpfb_complex           : t_wpfb := c_sdp_wpfb_complex_subbands;
    g_scope_selected_subband : natural := 0
  );
  port (
    dp_clk        : in  std_logic;
    dp_rst        : in  std_logic;

    dp_bsn_source_restart      : in std_logic;
    dp_bsn_source_new_interval : in std_logic;

    in_sosi_arr        : in  t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0);
    fsub_quant_sosi_arr: out t_dp_sosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0);
    fsub_raw_sosi_arr  : out t_dp_sosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0);
    sst_udp_sosi       : out t_dp_sosi;
    sst_udp_siso       : in  t_dp_siso := c_dp_siso_rst;

    mm_rst             : in  std_logic;
    mm_clk             : in  std_logic;

    reg_si_mosi        : in  t_mem_mosi := c_mem_mosi_rst;
    reg_si_miso        : out t_mem_miso;
    ram_st_sst_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    ram_st_sst_miso    : out t_mem_miso;
    ram_fil_coefs_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_fil_coefs_miso : out t_mem_miso;
    ram_gains_mosi     : in  t_mem_mosi := c_mem_mosi_rst;
    ram_gains_miso     : out t_mem_miso;
    ram_gains_cross_mosi : in  t_mem_mosi := c_mem_mosi_rst;
    ram_gains_cross_miso : out t_mem_miso;
    reg_selector_mosi  : in  t_mem_mosi := c_mem_mosi_rst;
    reg_selector_miso  : out t_mem_miso;
    reg_enable_mosi    : in  t_mem_mosi := c_mem_mosi_rst;
    reg_enable_miso    : out t_mem_miso;
    reg_hdr_dat_mosi   : in  t_mem_mosi := c_mem_mosi_rst;
    reg_hdr_dat_miso   : out t_mem_miso;
    reg_bsn_monitor_v2_sst_offload_copi : in  t_mem_copi := c_mem_copi_rst;
    reg_bsn_monitor_v2_sst_offload_cipo : out t_mem_cipo;

    sdp_info : in t_sdp_info;
    gn_id    : in std_logic_vector(c_sdp_W_gn_id - 1 downto 0);

    eth_src_mac  : in std_logic_vector(c_network_eth_mac_addr_w - 1 downto 0);
    ip_src_addr  : in std_logic_vector(c_network_ip_addr_w - 1 downto 0);
    udp_src_port : in std_logic_vector(c_network_udp_port_w - 1 downto 0)
  );
end node_sdp_oversampled_filterbank;

architecture str of node_sdp_oversampled_filterbank is
  -- See node_sdp_filterbank for location and genetion of hex files
  constant c_coefs_file_prefix : string := "data/Coeffs16384Kaiser-quant_1wb";
  constant c_gains_file_name : string := "data/gains_1024_complex_" &
                                         natural'image(c_sdp_W_sub_weight) & "b" &
                                         natural'image(c_sdp_W_sub_weight_fraction) & "f_unit";

  constant c_fft                            : t_fft := func_wpfb_map_wpfb_parameters_to_fft(g_wpfb);
  constant c_fft_complex                    : t_fft := func_wpfb_map_wpfb_parameters_to_fft(g_wpfb_complex);
  constant c_subband_raw_dat_w              : natural := func_fft_raw_dat_w(c_fft);
  constant c_subband_raw_fraction_w         : natural := func_fft_raw_fraction_w(c_fft);
  constant c_complex_subband_raw_dat_w      : natural := func_fft_raw_dat_w(c_fft_complex);
  constant c_complex_subband_raw_fraction_w : natural := func_fft_raw_fraction_w(c_fft_complex);
  constant c_dat_w_diff                     : integer := c_complex_subband_raw_dat_w - c_subband_raw_dat_w;  -- = -1
                                                         -- which is used to shift 1 bit to the left.

  constant c_nof_masters : positive := 2;

  constant c_si_pipeline : natural := 1;
  constant c_complex_mult_pipeline : natural := 3;
  constant c_complex_pfb_pipeline  : natural := 2;
  constant c_pipeline_remove_lsb   : natural := 1;  -- to easy timing closure

  -- Use WG as local oscillator, buf contains 16b sin and 16b cos
  -- . c_sdp_W_local_oscillator = 16b
  -- . c_sdp_W_local_oscillator_fraction = 16b - 1 sign bit = 15b
  constant c_buf            : t_c_mem := (latency  => 1,
                                          adr_w    => ceil_log2(2 * c_sdp_N_fft),
                                          dat_w    => c_nof_complex * c_sdp_W_local_oscillator,
                                          nof_dat  => c_sdp_R_os * c_sdp_N_fft,
                                          init_sl  => '0');

  constant c_buf_file       : string := "data/freq_shift_half_subband_2048x16_im_re.hex";

  constant c_wg_ctrl : t_diag_wg := (TO_UVEC(c_diag_wg_mode_repeat, c_diag_wg_mode_w),
                                     TO_UVEC(c_buf.nof_dat, c_diag_wg_nofsamples_w),
                                     (others => '0'),
                                     (others => '0'),
                                     (others => '0'));
  constant c_wg_phase_offset : natural := 6;  -- Compensate for WG start latency. In nof samples.

  constant c_fil_coefs_mem_addr_w : natural := ceil_log2(c_sdp_N_fft * c_sdp_N_taps);

  signal ram_fil_coefs_mosi_arr : t_mem_mosi_arr(c_sdp_R_os - 1 downto 0) := (others => c_mem_mosi_rst);
  signal ram_fil_coefs_miso_arr : t_mem_miso_arr(c_sdp_R_os - 1 downto 0) := (others => c_mem_miso_rst);

  signal ram_st_sst_mosi_arr : t_mem_mosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0) := (others => c_mem_mosi_rst);
  signal ram_st_sst_miso_arr : t_mem_miso_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0) := (others => c_mem_miso_rst);

  -- Subband statistics
  signal ram_st_offload_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal ram_st_offload_miso : t_mem_miso := c_mem_miso_rst;

  signal master_mem_mux_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal master_mem_mux_miso : t_mem_miso := c_mem_miso_rst;
  signal master_mosi_arr     : t_mem_mosi_arr(0 to c_nof_masters - 1) := (others => c_mem_mosi_rst);
  signal master_miso_arr     : t_mem_miso_arr(0 to c_nof_masters - 1) := (others => c_mem_miso_rst);

  -- WG as local oscillator (LO)
  signal wg_rddata  : std_logic_vector(c_buf.dat_w - 1 downto 0);
  signal wg_rdval   : std_logic;
  signal wg_address : std_logic_vector(c_buf.adr_w - 1 downto 0);
  signal wg_rom_address : std_logic_vector(c_buf.adr_w - 1 downto 0);
  signal wg_rd      : std_logic;
  signal wg_out_dat : std_logic_vector(c_buf.dat_w - 1 downto 0);

  -- Spectral Inversion (SI)
  signal si_sosi_arr                    : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal si_sosi_0_piped                : t_dp_sosi := c_dp_sosi_rst;

  -- Real input FFT
  signal wpfb_unit_in_sosi_arr          : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal wpfb_unit_fil_sosi_arr         : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal wpfb_unit_out_quant_sosi_arr   : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0)  := (others => c_dp_sosi_rst);
  signal wpfb_unit_out_raw_sosi_arr     : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0)  := (others => c_dp_sosi_rst);

  -- Mixer to shift f_sub/2
  signal mixer_complex_mult_src_out_arr       : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);
  signal mixer_complex_requantize_src_out_arr : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0) := (others => c_dp_sosi_rst);

  -- Complex input FFT (from LO mixer)
  signal wpfb_unit_complex_in_sosi_arr        : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)   := (others => c_dp_sosi_rst);
  signal wpfb_unit_complex_fil_sosi_arr       : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)   := (others => c_dp_sosi_rst);
  signal wpfb_unit_complex_out_quant_sosi_arr : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)   := (others => c_dp_sosi_rst);
  signal wpfb_unit_complex_out_raw_sosi_arr   : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)   := (others => c_dp_sosi_rst);

  -- Remove negative frequencies
  signal wpfb_complex_out_resized_sosi_arr  : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)   := (others => c_dp_sosi_rst);

  -- Interleave positive frequencies per factor Q_fft = 2, like with output of real input FFT
  signal wpfb_complex_out_fifo_sosi_arr     : t_dp_sosi_arr(c_sdp_S_pn - 1 downto 0)   := (others => c_dp_sosi_rst);
  signal wpfb_complex_out_fifo_siso_arr     : t_dp_siso_arr(c_sdp_S_pn - 1 downto 0)   := (others => c_dp_siso_rst);
  signal wpfb_complex_out_resized_sosi_2arr : t_dp_sosi_2arr_2(c_sdp_P_pfb - 1 downto 0) :=
                                                  (others => (others => c_dp_sosi_rst));
  signal wpfb_complex_out_resized_siso_2arr : t_dp_siso_2arr_2(c_sdp_P_pfb - 1 downto 0) :=
                                                  (others => (others => c_dp_siso_rst));
  signal wpfb_complex_out_interleaved_sosi_arr : t_dp_sosi_arr(c_sdp_P_pfb - 1 downto 0) := (others => c_dp_sosi_rst);

  signal subband_equalizer_in_sosi_arr    : t_dp_sosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0) :=
                                                (others => c_dp_sosi_rst);
  signal subband_equalizer_quant_sosi_arr : t_dp_sosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0) :=
                                                (others => c_dp_sosi_rst);
  signal subband_equalizer_raw_sosi_arr   : t_dp_sosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0) :=
                                                (others => c_dp_sosi_rst);
  signal dp_selector_out_sosi_arr         : t_dp_sosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0) :=
                                                (others => c_dp_sosi_rst);
  signal dp_selector_pipe_sosi_arr        : t_dp_sosi_arr(c_sdp_R_os * c_sdp_P_pfb - 1 downto 0) :=
                                                (others => c_dp_sosi_rst);

  signal scope_equalizer_in_sosi_arr      : t_dp_sosi_integer_arr(c_sdp_R_os * c_sdp_S_pn - 1 downto 0);
  signal scope_equalizer_out_sosi_arr     : t_dp_sosi_integer_arr(c_sdp_R_os * c_sdp_S_pn - 1 downto 0);

  signal selector_en                        : std_logic;
  signal weighted_subbands_flag             : std_logic;

  signal dp_bsn_source_restart_pipe         : std_logic;
  signal dp_bsn_source_restart_delayed      : std_logic;
  signal dp_bsn_source_restart_wg           : std_logic;
  signal dp_bsn_source_restart_pipe_complex : std_logic;
begin
  ---------------------------------------------------------------
  -- SPECTRAL INVERSION
  ---------------------------------------------------------------
  u_si_arr : entity si_lib.si_arr
  generic map (
    g_nof_streams => c_sdp_S_pn,
    g_pipeline    => c_si_pipeline,
    g_dat_w       => c_sdp_W_adc
  )
  port map(
    in_sosi_arr  => in_sosi_arr,
    out_sosi_arr => si_sosi_arr,

    reg_si_mosi  => reg_si_mosi,
    reg_si_miso  => reg_si_miso,

    mm_rst       => mm_rst,
    mm_clk       => mm_clk,
    dp_clk       => dp_clk,
    dp_rst       => dp_rst
  );

  ---------------------------------------------------------------
  -- POLY-PHASE FILTERBANK
  ---------------------------------------------------------------

  -- Connect the 12 ADC streams to the re and im fields of the PFB input.
  p_pfb_streams : process(si_sosi_arr)
  begin
    for I in 0 to c_sdp_P_pfb - 1 loop
      wpfb_unit_in_sosi_arr(I) <= si_sosi_arr(2 * I);
      wpfb_unit_in_sosi_arr(I).re <= RESIZE_DP_DSP_DATA(si_sosi_arr(2 * I).data(c_sdp_W_adc - 1 downto 0));
      wpfb_unit_in_sosi_arr(I).im <= RESIZE_DP_DSP_DATA(si_sosi_arr(2 * I + 1).data(c_sdp_W_adc - 1 downto 0));
    end loop;
  end process;

  -- pipeline bsn restart signal to keep dp_bsn_source_restart aligned with si_sosi_arr
  u_common_pipeline_sl : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline  => c_si_pipeline
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    in_dat     => dp_bsn_source_restart,
    out_dat    => dp_bsn_source_restart_pipe
  );

  -- PFB
  u_wpfb_unit_dev : entity wpfb_lib.wpfb_unit_dev
  generic map (
    g_wpfb                   => g_wpfb,
    g_use_prefilter          => true,
    g_stats_ena              => false,
    g_use_bg                 => false,
    g_coefs_file_prefix      => c_coefs_file_prefix,
    g_restart_on_valid       => false
  )
  port map (
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    ram_fil_coefs_mosi => ram_fil_coefs_mosi_arr(0),
    ram_fil_coefs_miso => ram_fil_coefs_miso_arr(0),

    in_sosi_arr        => wpfb_unit_in_sosi_arr,
    fil_sosi_arr       => wpfb_unit_fil_sosi_arr,
    out_quant_sosi_arr => wpfb_unit_out_quant_sosi_arr,
    out_raw_sosi_arr   => wpfb_unit_out_raw_sosi_arr,

    dp_bsn_source_restart => dp_bsn_source_restart_pipe
  );

  ---------------------------------------------------------------
  -- POLY-PHASE COMPLEX FILTERBANK
  ---------------------------------------------------------------
  -- WG for frequency shift by half a subband
  -- real part is in LSB and imaginary part in MSB.
  -- Waveform buffer
  u_buf : entity common_lib.common_rom
  generic map (
    g_ram       => c_buf,
    g_init_file => c_buf_file
  )
  port map (
    rst       => dp_rst,
    clk       => dp_clk,
    rd_adr    => wg_address,
    rd_en     => wg_rd,
    rd_val    => wg_rdval,
    rd_dat    => wg_rddata
  );

  -- Waveform generator as local oscillator.
  p_lo_restart : process(dp_clk, dp_rst)
  begin
    if rising_edge(dp_clk) then
      dp_bsn_source_restart_wg <= '0';
      if dp_rst = '1' then
        dp_bsn_source_restart_delayed <= '0';
      elsif si_sosi_arr(0).sop = '1' then
        dp_bsn_source_restart_delayed <= dp_bsn_source_restart_pipe;
        if dp_bsn_source_restart_pipe = '1' and si_sosi_arr(0).bsn(0) = '0' then  -- even bsn, start now.
          dp_bsn_source_restart_wg <= '1';
        else  -- Odd bsn, start 1 block later.
          dp_bsn_source_restart_wg <= dp_bsn_source_restart_delayed;
        end if;
      end if;
    end if;
  end process;

  u_lo_wg : entity diag_lib.diag_wg
  generic map (
    g_buf_dat_w    => c_buf.dat_w,
    g_buf_addr_w   => c_buf.adr_w,
    g_rate_offset  => c_wg_phase_offset,
    g_calc_support => false
  )
  port map (
    rst            => dp_rst,
    clk            => dp_clk,
    restart        => dp_bsn_source_restart_wg,

    buf_rddat      => wg_rddata,
    buf_rdval      => wg_rdval,
    buf_addr       => wg_address,
    buf_rden       => wg_rd,

    ctrl           => c_wg_ctrl,

    out_dat        => wg_out_dat,
    out_val        => open
  );

  -- Complex mult
  gen_complex_mult: for I in 0 to c_sdp_S_pn - 1 generate
    u_common_complex_mult : entity common_mult_lib.common_complex_mult
    generic map (
      g_in_a_w           => c_sdp_W_local_oscillator,  -- = 16
      g_in_b_w           => c_sdp_W_adc,  -- = 14
      g_out_p_w          => c_sdp_W_local_oscillator + c_sdp_W_adc,  -- = 16 + 14 = 30
      g_conjugate_b      => false
    )
    port map (
      clk        => dp_clk,
      clken      => '1',
      rst        => dp_rst,
      in_ar      => wg_out_dat(c_sdp_W_local_oscillator - 1 downto 0),
      in_ai      => wg_out_dat(2 * c_sdp_W_local_oscillator - 1 downto c_sdp_W_local_oscillator),
      in_br      => si_sosi_arr(I).data(c_sdp_W_adc - 1 downto 0),
      in_bi      => (others => '0'),
      in_val     => si_sosi_arr(I).valid,
      out_pr     => mixer_complex_mult_src_out_arr(I).re(c_sdp_W_local_oscillator + c_sdp_W_adc - 1 downto 0),
      out_pi     => mixer_complex_mult_src_out_arr(I).im(c_sdp_W_local_oscillator + c_sdp_W_adc - 1 downto 0),
      out_val    => mixer_complex_mult_src_out_arr(I).valid
    );

    --requantize
    u_dp_requantize : entity dp_lib.dp_requantize
    generic map (
      g_complex             => true,
      g_representation      => "SIGNED",
      g_lsb_w               => c_sdp_W_local_oscillator_fraction,
      g_lsb_round           => true,
      g_lsb_round_clip      => false,
      g_msb_clip            => true,
      g_msb_clip_symmetric  => false,
      g_pipeline_remove_lsb => 0,
      g_pipeline_remove_msb => 0,
      g_in_dat_w            => c_sdp_W_local_oscillator + c_sdp_W_adc,
      g_out_dat_w           => c_sdp_W_adc
    )
    port map (
      rst          => dp_rst,
      clk          => dp_clk,
      -- ST sink
      snk_in       => mixer_complex_mult_src_out_arr(I),
      -- ST source
      src_out      => mixer_complex_requantize_src_out_arr(I)
    );
  end generate;

  -- Pipeline to compensate for complex mult and dp_requantize.
  u_dp_pipeline : entity dp_lib.dp_pipeline
  generic map (
    g_pipeline   => c_complex_mult_pipeline
  )
  port map (
    rst          => dp_rst,
    clk          => dp_clk,
    -- ST sink
    snk_in       => si_sosi_arr(0),
    -- ST source
    src_out      => si_sosi_0_piped
  );

  -- pipeline bsn restart signal to keep dp_bsn_source_restart aligned with si_sosi_arr
  u_common_pipeline_sl_cplx : entity common_lib.common_pipeline_sl
  generic map (
    g_pipeline  => c_complex_mult_pipeline
  )
  port map (
    rst        => dp_rst,
    clk        => dp_clk,
    in_dat     => dp_bsn_source_restart_pipe,
    out_dat    => dp_bsn_source_restart_pipe_complex
  );

  p_align : process(mixer_complex_requantize_src_out_arr, si_sosi_0_piped)
  begin
    for I in 0 to c_sdp_S_pn - 1 loop
      wpfb_unit_complex_in_sosi_arr(I) <= si_sosi_0_piped;
      wpfb_unit_complex_in_sosi_arr(I).re <= mixer_complex_requantize_src_out_arr(I).re;
      wpfb_unit_complex_in_sosi_arr(I).im <= mixer_complex_requantize_src_out_arr(I).im;
    end loop;
  end process;

  -- PFB complex
  u_wpfb_unit_dev_complex : entity wpfb_lib.wpfb_unit_dev
  generic map (
    g_wpfb                   => g_wpfb_complex,
    g_use_prefilter          => true,
    g_stats_ena              => false,
    g_use_bg                 => false,
    g_coefs_file_prefix      => c_coefs_file_prefix,
    g_restart_on_valid       => false
  )
  port map (
    dp_rst             => dp_rst,
    dp_clk             => dp_clk,
    mm_rst             => mm_rst,
    mm_clk             => mm_clk,

    ram_fil_coefs_mosi => ram_fil_coefs_mosi_arr(1),
    ram_fil_coefs_miso => ram_fil_coefs_miso_arr(1),

    in_sosi_arr        => wpfb_unit_complex_in_sosi_arr,
    fil_sosi_arr       => wpfb_unit_complex_fil_sosi_arr,
    out_quant_sosi_arr => wpfb_unit_complex_out_quant_sosi_arr,
    out_raw_sosi_arr   => wpfb_unit_complex_out_raw_sosi_arr,

    dp_bsn_source_restart => dp_bsn_source_restart_pipe_complex
  );

  ---------------------------------------------------------------
  -- Interleave for PFB complex
  ---------------------------------------------------------------
  -- Remove the part that is in the negative frequency domain by reducing the size to N_sub = 512.
  gen_dp_block_resize: for I in 0 to c_sdp_S_pn - 1 generate
    u_dp_block_resize : entity dp_lib.dp_block_resize
      generic map(
        g_input_block_size  => c_sdp_N_fft,
        g_out_eop_index     => c_sdp_N_sub - 1
      )
      port map (
        rst          => dp_rst,
        clk          => dp_clk,
        snk_in       => wpfb_unit_complex_out_raw_sosi_arr(I),
        src_out      => wpfb_complex_out_resized_sosi_arr(I)
      );
  end generate;

  -- buffer for interleaving
  gen_dp_fifo: for I in 0 to c_sdp_S_pn - 1 generate
    u_dp_fifo_sc : entity dp_lib.dp_fifo_sc
      generic map(
        g_data_w      => c_nof_complex * c_complex_subband_raw_dat_w,
        g_bsn_w       => c_dp_stream_bsn_w,
        g_use_bsn     => true,
        g_use_sync    => true,
        g_use_complex => true,
        g_fifo_size   => c_sdp_N_sub
      )
      port map (
        rst          => dp_rst,
        clk          => dp_clk,
        snk_in       => wpfb_complex_out_resized_sosi_arr(I),
        src_out      => wpfb_complex_out_fifo_sosi_arr(I),
        src_in       => wpfb_complex_out_fifo_siso_arr(I)
    );
  end generate;

  -- rewire 1d array of 1 X S_pn to 2d array of 2 X P_pfb
  gen_rewire: for I in 0 to c_sdp_P_pfb - 1 generate
    wpfb_complex_out_resized_sosi_2arr(I)(0) <= wpfb_complex_out_fifo_sosi_arr(2 * I);
    wpfb_complex_out_resized_sosi_2arr(I)(1) <= wpfb_complex_out_fifo_sosi_arr(2 * I + 1);
    wpfb_complex_out_fifo_siso_arr(2 * I)      <= wpfb_complex_out_resized_siso_2arr(I)(0);
    wpfb_complex_out_fifo_siso_arr(2 * I + 1)  <= wpfb_complex_out_resized_siso_2arr(I)(1);
  end generate;

  gen_align: for I in 0 to c_sdp_P_pfb - 1 generate
    -- Interleave 2 to 1 for all S_pn signals.
    u_dp_interleave_n_to_one : entity dp_lib.dp_interleave_n_to_one
      generic map(
        g_nof_inputs => c_sdp_Q_fft
      )
      port map (
        rst          => dp_rst,
        clk          => dp_clk,
        snk_in_arr   => wpfb_complex_out_resized_sosi_2arr(I),
        snk_out_arr  => wpfb_complex_out_resized_siso_2arr(I),
        src_out      => wpfb_complex_out_interleaved_sosi_arr(I)
    );

    -- Align data width of wpfb_complex output with wpfb_real output as the real wpfb
    -- has an extra bit that is used for the FFT seperate function which the complex FFT
    -- does not have. Here we add that extra bit to the output of the complex FFT aswell
    -- to maintain the same scale and data width.
    p_add_bit : process(wpfb_complex_out_interleaved_sosi_arr)
    begin
      subband_equalizer_in_sosi_arr(c_sdp_P_pfb + I) <= wpfb_complex_out_interleaved_sosi_arr(I);
      subband_equalizer_in_sosi_arr(c_sdp_P_pfb + I).re <= SHIFT_SVEC(wpfb_complex_out_interleaved_sosi_arr(I).re,
                                                                      c_dat_w_diff);
      subband_equalizer_in_sosi_arr(c_sdp_P_pfb + I).im <= SHIFT_SVEC(wpfb_complex_out_interleaved_sosi_arr(I).im,
                                                                      c_dat_w_diff);
    end process;
  end generate;

  -- Pipeline to compensate for longer latency of the complex PFB.
  u_dp_pipeline_arr : entity dp_lib.dp_pipeline_arr
    generic map (
      g_nof_streams => c_sdp_P_pfb,
      g_pipeline    => c_complex_pfb_pipeline
    )
    port map (
      rst          => dp_rst,
      clk          => dp_clk,
      -- ST sink
      snk_in_arr   => wpfb_unit_out_raw_sosi_arr,
      -- ST source
      src_out_arr  => subband_equalizer_in_sosi_arr(c_sdp_P_pfb - 1 downto 0)
    );

  ---------------------------------------------------------------
  -- COMBINE MEMORY MAPPED INTERFACES OF RAM_FIL_COEFS
  ---------------------------------------------------------------
  u_mem_mux_coef : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_R_os,
    g_mult_addr_w => c_fil_coefs_mem_addr_w
  )
  port map (
    mosi     => ram_fil_coefs_mosi,
    miso     => ram_fil_coefs_miso,
    mosi_arr => ram_fil_coefs_mosi_arr,
    miso_arr => ram_fil_coefs_miso_arr
  );

  ---------------------------------------------------------------
  -- SUBBAND EQUALIZER
  ---------------------------------------------------------------
  u_sdp_subband_equalizer : entity work.sdp_subband_equalizer
    generic map (
      g_gains_file_name => c_gains_file_name,
      g_nof_streams     => c_sdp_R_os * c_sdp_P_pfb,
      g_raw_dat_w       => c_subband_raw_dat_w,
      g_raw_fraction_w  => c_subband_raw_fraction_w
    )
    port map(
      dp_clk         => dp_clk,
      dp_rst         => dp_rst,

      in_raw_sosi_arr    => subband_equalizer_in_sosi_arr,
      out_raw_sosi_arr   => subband_equalizer_raw_sosi_arr,
      out_quant_sosi_arr => subband_equalizer_quant_sosi_arr,

      mm_rst         => mm_rst,
      mm_clk         => mm_clk,

      ram_gains_mosi => ram_gains_mosi,
      ram_gains_miso => ram_gains_miso,
      ram_gains_cross_mosi => ram_gains_cross_mosi,
      ram_gains_cross_miso => ram_gains_cross_miso
    );

  -- Output fsub streams
  fsub_quant_sosi_arr <= subband_equalizer_quant_sosi_arr;
  fsub_raw_sosi_arr <= subband_equalizer_raw_sosi_arr;

  ---------------------------------------------------------------
  -- DP REQUANTIZE for SST
  ---------------------------------------------------------------
  gen_dp_requantize : for I in 0 to c_sdp_R_os * c_sdp_P_pfb - 1 generate
    u_dp_requantize : entity dp_lib.dp_requantize
      generic map (
        g_complex             => true,
        g_lsb_w               => c_subband_raw_fraction_w,
        g_msb_clip            => false,
        g_pipeline_remove_lsb => c_pipeline_remove_lsb,
        g_in_dat_w            => c_subband_raw_dat_w,
        g_out_dat_w           => c_sdp_W_subband
      )
      port map (
        rst => dp_rst,
        clk => dp_clk,

        snk_in  => subband_equalizer_in_sosi_arr(I),
        src_out => dp_selector_pipe_sosi_arr(I)
      );
  end generate;

  ---------------------------------------------------------------
  -- DP SELECTOR for SST input
  ---------------------------------------------------------------
  u_dp_selector_arr : entity dp_lib.dp_selector_arr
    generic map (
      g_nof_arr => c_sdp_R_os * c_sdp_P_pfb,
      g_pipeline => c_sdp_subband_equalizer_latency - c_pipeline_remove_lsb
    )
    port map (
      mm_rst => mm_rst,
      mm_clk => mm_clk,
      dp_rst => dp_rst,
      dp_clk => dp_clk,

      reg_selector_mosi => reg_selector_mosi,
      reg_selector_miso => reg_selector_miso,

      pipe_sosi_arr  => dp_selector_pipe_sosi_arr,
      ref_sosi_arr   => subband_equalizer_quant_sosi_arr,
      out_sosi_arr   => dp_selector_out_sosi_arr,

      selector_en    => selector_en
    );

  ---------------------------------------------------------------
  -- SIGNAL SCOPE
  ---------------------------------------------------------------
  -- synthesis translate_off
  u_sdp_scope_equalizer_in : entity work.sdp_scope
    generic map (
      g_sim            => g_sim,
      g_selection      => g_scope_selected_subband,
      g_nof_input      => c_sdp_R_os * c_sdp_P_pfb,
      g_n_deinterleave => c_sdp_Q_fft,
      g_dat_w          => c_sdp_W_subband
    )
    port map (
      clk            => dp_clk,
      rst            => dp_rst,
      sp_sosi_arr    => subband_equalizer_in_sosi_arr,
      scope_sosi_arr => scope_equalizer_in_sosi_arr
    );

  u_sdp_scope_equalizer_out : entity work.sdp_scope
    generic map (
      g_sim            => g_sim,
      g_selection      => g_scope_selected_subband,
      g_nof_input      => c_sdp_R_os * c_sdp_P_pfb,
      g_n_deinterleave => c_sdp_Q_fft,
      g_dat_w          => c_sdp_W_subband
    )
    port map (
      clk            => dp_clk,
      rst            => dp_rst,
      sp_sosi_arr    => subband_equalizer_quant_sosi_arr,
      scope_sosi_arr => scope_equalizer_out_sosi_arr
    );
  -- synthesis translate_on

  ---------------------------------------------------------------
  -- SUBBAND STATISTICS
  ---------------------------------------------------------------
  gen_stats_streams: for I in 0 to c_sdp_R_os * c_sdp_P_pfb - 1 generate
    u_subband_stats : entity st_lib.st_sst
      generic map(
        g_nof_stat       => c_sdp_N_sub * c_sdp_Q_fft,
        g_in_data_w      => c_sdp_W_subband,
        g_stat_data_w    => g_wpfb.stat_data_w,
        g_stat_data_sz   => g_wpfb.stat_data_sz,
        g_stat_multiplex => c_sdp_Q_fft
      )
      port map (
        mm_rst          => mm_rst,
        mm_clk          => mm_clk,
        dp_rst          => dp_rst,
        dp_clk          => dp_clk,
        in_complex      => dp_selector_out_sosi_arr(I),
        ram_st_sst_mosi => ram_st_sst_mosi_arr(I),
        ram_st_sst_miso => ram_st_sst_miso_arr(I)
      );
  end generate;

  ---------------------------------------------------------------
  -- COMBINE MEMORY MAPPED INTERFACES OF SST
  ---------------------------------------------------------------
  -- Combine the internal array of mm interfaces for the subband
  -- statistics to one array.
  u_mem_mux_sst : entity common_lib.common_mem_mux
  generic map (
    g_nof_mosi    => c_sdp_R_os * c_sdp_P_pfb,
    g_mult_addr_w => ceil_log2(c_sdp_N_sub * c_sdp_Q_fft * g_wpfb.stat_data_sz)
  )
  port map (
    mosi     => master_mem_mux_mosi,
    miso     => master_mem_mux_miso,
    mosi_arr => ram_st_sst_mosi_arr,
    miso_arr => ram_st_sst_miso_arr
  );

  -- Connect 2 mm_masters to the common_mem_mux output
  master_mosi_arr(0)  <= ram_st_sst_mosi;  -- MM access via QSYS MM bus
  ram_st_sst_miso     <= master_miso_arr(0);
  master_mosi_arr(1)  <= ram_st_offload_mosi;  -- MM access by SST offload
  ram_st_offload_miso <= master_miso_arr(1);

  u_mem_master_mux : entity mm_lib.mm_master_mux
  generic map (
    g_nof_masters    => c_nof_masters,
    g_rd_latency_min => 1  -- read latency of statistics RAM is 1
  )
  port map (
    mm_clk => mm_clk,

    master_mosi_arr => master_mosi_arr,
    master_miso_arr => master_miso_arr,
    mux_mosi        => master_mem_mux_mosi,
    mux_miso        => master_mem_mux_miso
  );

  ---------------------------------------------------------------
  -- STATISTICS OFFLOAD
  ---------------------------------------------------------------
  weighted_subbands_flag <= not selector_en when rising_edge(dp_clk);

  u_sdp_sst_udp_offload: entity work.sdp_statistics_offload
  generic map (
    g_statistics_type   => "SST_OS",
    g_offload_node_time => sel_a_b(g_sim, g_sim_sdp.offload_node_time, c_sdp_offload_node_time)
  )
  port map (
    mm_clk    => mm_clk,
    mm_rst    => mm_rst,

    dp_clk    => dp_clk,
    dp_rst    => dp_rst,

    master_mosi => ram_st_offload_mosi,
    master_miso => ram_st_offload_miso,

    reg_enable_mosi  => reg_enable_mosi,
    reg_enable_miso  => reg_enable_miso,

    reg_hdr_dat_mosi  => reg_hdr_dat_mosi,
    reg_hdr_dat_miso  => reg_hdr_dat_miso,

    reg_bsn_monitor_v2_offload_copi => reg_bsn_monitor_v2_sst_offload_copi,
    reg_bsn_monitor_v2_offload_cipo => reg_bsn_monitor_v2_sst_offload_cipo,

    in_sosi      => dp_selector_out_sosi_arr(0),
    new_interval => dp_bsn_source_new_interval,

    out_sosi  => sst_udp_sosi,
    out_siso  => sst_udp_siso,

    eth_src_mac  => eth_src_mac,
    udp_src_port => udp_src_port,
    ip_src_addr  => ip_src_addr,

    gn_index                => TO_UINT(gn_id),
    sdp_info                => sdp_info,
    weighted_subbands_flag  => weighted_subbands_flag
  );
end str;
