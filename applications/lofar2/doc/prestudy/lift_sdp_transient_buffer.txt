


1) LIFT 27 June 2022 meeting

https://support.astron.nl/confluence/display/L2M/2022-06-27+LIFT+system+implementation+meeting+notes

a) Transient buffer:
- dual pol
- only raw, no subband data
- sciences:
  * comic ray
    - uses all antennas of one station, superterp
  * lightning
    - uses ~ 6 antennas but all Dutch stations
    - 6 dual pol antennas per station with long baselines
    - 96 LBA --> 8 - 16 possible useful combinations of 6 antennas --> dependent on RCU allocation
    - external trigger delay < 2 s
  - cosmic ray is commensal, lightning is dedicated mode
    - typical thunderstorm duration is 1 - 4 hours
    - in NL there are about 30 thunderstoms / year
    - capture > 10 events / 4 hours
  - cosmic ray 2 ms event, lightning 0.1 - 1 s events

- LIFT HW budget 182 kEuro
- individually controlled antenna buffers
- read out via 10GbE via LCU (with local storage, later TCP to CEP) or directly to CEP (UDP)
  . one signal input for 1 s takes 200M * 16b = 0.4 GByte = 3.2 Gbit, so 0.3 s
  . 192 signal inputs for 1 s takes 3.2 Gbit * 192 ~= 0.6 Tbit, so 60 s
- 2 * 8 GB / (12 * 16b / 8b * 0.2 GHz) =  3.33 s seems enough storage
  . 3.33 * 16b / 14b = 3.81 s
  . header overhead 5 % --> 3.81 * 0.95 = 3.61 s

