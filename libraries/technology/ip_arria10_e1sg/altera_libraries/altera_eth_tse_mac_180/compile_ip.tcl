#------------------------------------------------------------------------------
#
# Copyright (C) 2017
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------

# This file is based on generated file mentor/msim_setup.tcl.
# - the values for modelsim_search_libraries key in the hdllib.cfg follow from altera libraries vmap section in the msim_setup.tcl
# - vmap for the IP specific libraries and compile all IP source files into those libraries similar as in the msim_setup.tcl
# - replace QSYS_SIMDIR by IP_DIR
# - if the testbench is also generated with QSYS then only the IP_TBDIR files are needed, because these also contain the source files.

#vlib ./work/         ;# Assume library work already exist                                                                                        

set IP_DIR   "$env(HDL_BUILD_DIR)/$env(BUILDSET)/qsys-generate/ip_arria10_e1sg_tse_sgmii_gx/sim"

vmap  altera_eth_tse_mac_180                 ./work/

  vlog -sv  "$IP_DIR/../altera_xcvr_native_a10_180/sim/altera_xcvr_native_a10_functions_h.sv"                                     -work altera_common_sv_packages  
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_eth_tse_mac.v"                                                   -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_clk_cntl.v"                                                  -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_crc328checker.v"                                             -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_crc328generator.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_crc32ctl8.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_crc32galois8.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_gmii_io.v"                                                   -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_lb_read_cntl.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_lb_wrt_cntl.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_hashing.v"                                                   -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_host_control.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_host_control_small.v"                                        -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_mac_control.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_register_map.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_register_map_small.v"                                        -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rx_counter_cntl.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_shared_mac_control.v"                                        -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_shared_register_map.v"                                       -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_counter_cntl.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_lfsr_10.v"                                                   -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_loopback_ff.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_altshifttaps.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_fifoless_mac_rx.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_mac_rx.v"                                                    -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_fifoless_mac_tx.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_mac_tx.v"                                                    -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_magic_detection.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_mdio.v"                                                      -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_mdio_clk_gen.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_mdio_cntl.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_top_mdio.v"                                                  -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_mii_rx_if.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_mii_tx_if.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_pipeline_base.v"                                             -work altera_eth_tse_mac_180                
  vlog -sv  "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_pipeline_stage.sv"              -L altera_common_sv_packages -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_dpram_16x32.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_dpram_8x32.v"                                                -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_dpram_ecc_16x32.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_fifoless_retransmit_cntl.v"                                  -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_retransmit_cntl.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rgmii_in1.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rgmii_in4.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_nf_rgmii_module.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rgmii_module.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rgmii_out1.v"                                                -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rgmii_out4.v"                                                -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rx_ff.v"                                                     -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rx_min_ff.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rx_ff_cntrl.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rx_ff_cntrl_32.v"                                            -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rx_ff_cntrl_32_shift16.v"                                    -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rx_ff_length.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_rx_stat_extract.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_timing_adapter32.v"                                          -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_timing_adapter8.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_timing_adapter_fifo32.v"                                     -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_timing_adapter_fifo8.v"                                      -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_top_1geth.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_top_fifoless_1geth.v"                                        -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_top_w_fifo.v"                                                -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_top_w_fifo_10_100_1000.v"                                    -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_top_wo_fifo.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_top_wo_fifo_10_100_1000.v"                                   -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_top_gen_host.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_ff.v"                                                     -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_min_ff.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_ff_cntrl.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_ff_cntrl_32.v"                                            -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_ff_cntrl_32_shift16.v"                                    -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_ff_length.v"                                              -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_ff_read_cntl.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_tx_stat_extract.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_eth_tse_std_synchronizer.v"                                      -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_eth_tse_std_synchronizer_bundle.v"                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_eth_tse_ptp_std_synchronizer.v"                                  -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_false_path_marker.v"                                         -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_reset_synchronizer.v"                                        -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_clock_crosser.v"                                             -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_a_fifo_13.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_a_fifo_24.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_a_fifo_34.v"                                                 -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_a_fifo_opt_1246.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_a_fifo_opt_14_44.v"                                          -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_a_fifo_opt_36_10.v"                                          -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_gray_cnt.v"                                                  -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_sdpm_altsyncram.v"                                           -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_altsyncram_dpm_fifo.v"                                       -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_bin_cnt.v"                                                   -work altera_eth_tse_mac_180                
  vlog -sv  "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ph_calculator.sv"               -L altera_common_sv_packages -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_sdpm_gen.v"                                                  -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_dec_x10.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x10.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x10_wrapper.v"                                       -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_dec_x14.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x14.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x14_wrapper.v"                                       -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_dec_x2.v"                                                -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x2.v"                                                -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x2_wrapper.v"                                        -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_dec_x23.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x23.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x23_wrapper.v"                                       -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_dec_x36.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x36.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x36_wrapper.v"                                       -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_dec_x40.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x40.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x40_wrapper.v"                                       -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_dec_x30.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x30.v"                                               -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_enc_x30_wrapper.v"                                       -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/mentor/altera_tse_ecc_status_crosser.v"                                        -work altera_eth_tse_mac_180                
  vlog      "$IP_DIR/../altera_eth_tse_mac_180/sim/altera_std_synchronizer_nocut.v"                                               -work altera_eth_tse_mac_180              
