-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity tb_tb_common_paged_ram_ww_rr is
end tb_tb_common_paged_ram_ww_rr;

architecture tb of tb_tb_common_paged_ram_ww_rr is
  signal tb_end : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Usage:
  -- > as 3
  -- > run -all

  -- DUT settings:
  --   g_pipeline_in     : NATURAL := 0;   -- >= 0
  --   g_pipeline_out    : NATURAL := 0;   -- >= 0
  --   g_page_sz         : NATURAL := 10   -- >= 1

  u_1             : entity work.tb_common_paged_ram_ww_rr generic map (0, 0, 1);
  u_2             : entity work.tb_common_paged_ram_ww_rr generic map (0, 0, 2);
  u_8             : entity work.tb_common_paged_ram_ww_rr generic map (0, 0, 8);

  u_even          : entity work.tb_common_paged_ram_ww_rr generic map (0, 0, 10);
  u_odd           : entity work.tb_common_paged_ram_ww_rr generic map (0, 0, 11);

  u_even_pipe_0_1 : entity work.tb_common_paged_ram_ww_rr generic map (0, 1, 10);
  u_even_pipe_1_1 : entity work.tb_common_paged_ram_ww_rr generic map (1, 1, 10);
  u_odd_pipe_0_1  : entity work.tb_common_paged_ram_ww_rr generic map (0, 1, 11);
  u_odd_pipe_1_1  : entity work.tb_common_paged_ram_ww_rr generic map (1, 1, 11);
end tb;
