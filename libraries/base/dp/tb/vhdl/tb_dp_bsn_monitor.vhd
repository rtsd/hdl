-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify dp_bsn_monitor for different RL
-- Description:
-- Usage:
-- > as 10
-- > run -all  -- signal tb_end will stop the simulation by stopping the clk
-- . The verify procedures check the correct input and monitor results

library IEEE, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_dp_bsn_monitor is
  generic (
    g_in_en      : t_dp_flow_control_enum := e_active;  -- always e_active, e_random or e_pulse flow control
    g_nof_sync   : natural := 11
  );
end tb_dp_bsn_monitor;

architecture tb of tb_dp_bsn_monitor is
  constant c_rl                  : natural := 1;
  constant c_data_w              : natural := 16;
  constant c_data_init           : integer := 0;
  constant c_frame_len           : natural := 20;
  constant c_pulse_active        : natural := 1;
  constant c_pulse_period        : natural := 7;
  constant c_sync_period         : natural := 17;
  constant c_sync_offset         : natural := 0;
  constant c_sync_timeout        : natural := c_frame_len * c_sync_period;
  constant c_nof_repeat          : natural := g_nof_sync * c_sync_period + 1;

  -- Error control
  constant c_skip_sync_nr        : integer := -1;  -- use e.g. 5 >= 0 to introduce a sync timeout at that sync interval 5 (causes missing sinc error by proc_dp_verify_sync), use -1 to disable skipping a sync
  constant c_nof_err             : natural := 2;  -- <= c_sync_period   -- introduce frame errors

  signal tb_end                  : std_logic := '0';
  signal clk                     : std_logic := '1';
  signal rst                     : std_logic := '1';

  -- Flow control
  signal random_0                : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0                 : std_logic;
  signal pulse_en                : std_logic := '1';

  -- Stimuli
  signal in_en                   : std_logic := '1';
  signal in_siso                 : t_dp_siso := c_dp_siso_rdy;
  signal in_sosi                 : t_dp_sosi;
  signal sync_in                 : std_logic := '0';

  -- Output
  signal out_siso                : t_dp_siso;

  -- Monitor in_sosi and out_siso
  signal mon_evt                 : std_logic;  -- pulses when new monitor output is available regarding the previous sync interval
  signal mon_sync                : std_logic;  -- pulses every in_sync interval
  signal mon_sync_timeout        : std_logic;
  signal mon_ready_stable        : std_logic;
  signal mon_xon_stable          : std_logic;
  signal mon_bsn_at_sync         : std_logic_vector(c_word_w - 1 downto 0);
  signal mon_nof_sop             : std_logic_vector(c_word_w - 1 downto 0);
  signal mon_nof_err             : std_logic_vector(c_word_w - 1 downto 0);
  signal mon_nof_valid           : std_logic_vector(c_word_w - 1 downto 0);
  signal mon_bsn_first           : std_logic_vector(c_word_w - 1 downto 0);
  signal mon_bsn_first_cycle_cnt : std_logic_vector(c_word_w - 1 downto 0);

  -- Verification
  signal verify_en               : std_logic := '0';
  signal verify_done             : std_logic := '0';
  signal count_eop               : natural := 0;

  signal prev_in_ready           : std_logic_vector(0 to c_rl);
  signal prev_in_data            : std_logic_vector(c_data_w - 1 downto 0) := TO_SVEC(c_data_init - 1, c_data_w);
  signal in_bsn                  : std_logic_vector(c_word_w - 1 downto 0);
  signal in_data                 : std_logic_vector(c_data_w - 1 downto 0);
  signal in_sync                 : std_logic;
  signal in_val                  : std_logic;
  signal in_sop                  : std_logic;
  signal in_eop                  : std_logic;
  signal expected_in_data        : std_logic_vector(c_data_w - 1 downto 0);

  signal expected_bsn_at_sync  : std_logic_vector(c_word_w - 1 downto 0);
  signal expected_nof_sop      : std_logic_vector(c_word_w - 1 downto 0);
  signal expected_nof_err      : std_logic_vector(c_word_w - 1 downto 0);
  signal expected_nof_valid    : std_logic_vector(c_word_w - 1 downto 0);
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);

  ------------------------------------------------------------------------------
  -- SOSI DATA GENERATION
  ------------------------------------------------------------------------------

  in_en   <= '1'                     when g_in_en = e_active      else
             random_0(random_0'high) when g_in_en = e_random      else
             pulse_0                 when g_in_en = e_pulse;

  in_siso <= c_dp_siso_rdy;

  -- Generate data path input data
  p_sosi_stimuli : process
    variable v_data_init   : natural;
    variable v_nof_err     : natural;
    variable v_err         : natural;
    variable v_sync_cnt    : natural;
    variable v_sync        : std_logic;
  begin
    v_data_init := c_data_init;
    v_sync_cnt := 0;
    v_nof_err := 0;
    in_sosi <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Begin of stimuli
    for R in 0 to c_nof_repeat - 1 loop
      -- control the sync intervals
      -- . introduce sync timeout by skipping a sync
      -- . introduce frame errors via sosi.err
      v_sync := sel_a_b(R mod c_sync_period = c_sync_offset, '1', '0');
      if v_sync = '1' then
        v_sync_cnt := v_sync_cnt + 1;
        if v_sync_cnt = c_skip_sync_nr then
          v_sync := '0';
        end if;
        v_nof_err := 0;
      else
        v_nof_err := v_nof_err + 1;
      end if;
      v_err := 0;
      if v_nof_err < c_nof_err then
        v_err := 1;
      end if;

      proc_dp_gen_block_data(c_rl, true, c_data_w, c_data_w, v_data_init, 0, 0, c_frame_len, 0, v_err, v_sync, TO_DP_BSN(R), clk, in_en, in_siso, in_sosi);
      --proc_common_wait_some_cycles(clk, 10);
      v_data_init := v_data_init + c_frame_len;
    end loop;

    -- End of stimuli
    expected_in_data <= TO_UVEC(v_data_init - 1, c_data_w);

    proc_common_wait_until_high(clk, verify_done);
    proc_common_wait_some_cycles(clk, 10);
    tb_end <= '1';
    wait;
  end process;

  expected_bsn_at_sync <= TO_UVEC(           c_nof_repeat - 1, c_word_w);
  expected_nof_sop     <= TO_UVEC(            c_sync_period, c_word_w);
  expected_nof_err     <= TO_UVEC(                c_nof_err, c_word_w);
  expected_nof_valid   <= TO_UVEC(c_frame_len * c_sync_period, c_word_w);

  ------------------------------------------------------------------------------
  -- SISO FLOW CONTROL GENERATION
  ------------------------------------------------------------------------------

  p_siso_stimuli : process
  begin
    out_siso <= c_dp_siso_rdy;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Pulse ready low
    proc_common_wait_until_hi_lo(clk, in_sosi.sync);
    proc_common_wait_until_hi_lo(clk, in_sosi.sop);
    proc_common_wait_until_hi_lo(clk, in_sosi.sop);
    proc_common_wait_some_cycles(clk, 3);
    out_siso.ready <= '0';
    proc_common_wait_some_cycles(clk, 1);
    out_siso.ready <= '1';

    -- Pulse xon low
    proc_common_wait_until_hi_lo(clk, in_sosi.sop);
    proc_common_wait_until_hi_lo(clk, in_sosi.sop);
    proc_common_wait_some_cycles(clk, 3);
    out_siso.xon <= '0';
    proc_common_wait_some_cycles(clk, 1);
    out_siso.xon <= '1';

    -- Keep ready active
    -- Keep xon active
    proc_common_wait_until_hi_lo(clk, in_sosi.sync);
    proc_common_wait_until_hi_lo(clk, in_sosi.sync);

    -- Make xon low for whole interval
    out_siso.xon <= '0';
    proc_common_wait_until_hi_lo(clk, in_sosi.sync);
    -- Make xon high during the interval
    proc_common_wait_some_cycles(clk, 1);
    out_siso.xon <= '1';

    wait;
  end process;

  p_siso_verify : process
  begin
    -- The assert conditions must manually be set such to fit the stimuli from p_siso_stimuli
    proc_common_wait_until_hi_lo(clk, mon_sync);
    assert mon_ready_stable = '0'
      report "Wrong mon_ready_stable at sync 1"
      severity ERROR;
    assert mon_xon_stable = '0'
      report "Wrong mon_xon_stable at sync 1"
      severity ERROR;
    proc_common_wait_until_hi_lo(clk, mon_sync);
    assert mon_ready_stable = '0'
      report "Wrong mon_ready_stable at sync 2"
      severity ERROR;
    assert mon_xon_stable = '0'
      report "Wrong mon_xon_stable at sync 2"
      severity ERROR;
    proc_common_wait_until_hi_lo(clk, mon_sync);
    assert mon_ready_stable = '1'
      report "Wrong mon_ready_stable at sync 3"
      severity ERROR;
    assert mon_xon_stable = '1'
      report "Wrong mon_xon_stable at sync 3"
      severity ERROR;
    proc_common_wait_until_hi_lo(clk, mon_sync);
    assert mon_ready_stable = '1'
      report "Wrong mon_ready_stable at sync 4"
      severity ERROR;
    assert mon_xon_stable = '0'
      report "Wrong mon_xon_stable at sync 4"
      severity ERROR;
    proc_common_wait_until_hi_lo(clk, mon_sync);
    assert mon_ready_stable = '1'
      report "Wrong mon_ready_stable at sync 5"
      severity ERROR;
    assert mon_xon_stable = '0'
      report "Wrong mon_xon_stable at sync 5"
      severity ERROR;
    proc_common_wait_until_hi_lo(clk, mon_sync);
    assert mon_ready_stable = '1'
      report "Wrong mon_ready_stable at sync 6"
      severity ERROR;
    assert mon_xon_stable = '1'
      report "Wrong mon_xon_stable at sync 6"
      severity ERROR;
    wait;
  end process;

  p_sync_in_stimuli : process
  begin
    sync_in <= '0';
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 25);  -- on SOP
    sync_in <= '1';
    proc_common_wait_some_cycles(clk, 1);
    sync_in <= '0';
    proc_common_wait_some_cycles(clk, 38);  -- on EOP
    sync_in <= '1';
    proc_common_wait_some_cycles(clk, 1);
    sync_in <= '0';
    proc_common_wait_some_cycles(clk, 67);  -- random
    sync_in <= '1';
    proc_common_wait_some_cycles(clk, 1);
    sync_in <= '0';

    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Verification logistics
  verify_en <= '1'          when rising_edge(clk) and in_sosi.sop = '1';  -- enable verify after first input sop
  count_eop <= count_eop + 1  when rising_edge(clk) and in_sosi.eop = '1';  -- count number of input eop
  verify_done <= '1'        when rising_edge(clk) and count_eop = c_nof_repeat;  -- signal verify done after c_nof_repeat frames

  -- Actual verification of the input streams
  proc_dp_verify_data("in_sosi.data", c_rl, clk, verify_en, in_siso.ready, in_sosi.valid, in_data, prev_in_data);  -- Verify that the input is incrementing data, like the input stimuli
  proc_dp_verify_valid(c_rl, clk, verify_en, in_siso.ready, prev_in_ready, in_sosi.valid);  -- Verify that the input valid fits with the input ready latency
  proc_dp_verify_value(e_equal, clk, verify_done, expected_in_data, prev_in_data);  -- Verify that the stimuli have been applied at all
  proc_dp_verify_sync(c_sync_period, c_sync_offset, clk, verify_en, in_sosi.sync, in_sosi.sop, in_sosi.bsn);

  -- Monitoring
  in_bsn  <= in_sosi.bsn(c_word_w - 1 downto 0);
  in_data <= in_sosi.data(c_data_w - 1 downto 0);
  in_sync <= in_sosi.sync;
  in_val  <= in_sosi.valid;
  in_sop  <= in_sosi.sop;
  in_eop  <= in_sosi.eop;

  ------------------------------------------------------------------------------
  -- MONITOR VERIFICATION
  ------------------------------------------------------------------------------

  proc_dp_verify_value(e_equal, clk, verify_done, expected_bsn_at_sync, mon_bsn_at_sync);
  proc_dp_verify_value(e_equal, clk, verify_done, expected_nof_sop,   mon_nof_sop);
  proc_dp_verify_value(e_equal, clk, verify_done, expected_nof_err,   mon_nof_err);
  proc_dp_verify_value(e_equal, clk, verify_done, expected_nof_valid, mon_nof_valid);

  ------------------------------------------------------------------------------
  -- DUT dp_bsn_monitor
  ------------------------------------------------------------------------------

  -- Tap the stream to the monitor
  dut : entity work.dp_bsn_monitor
  generic map (
    g_log_first_bsn => false,
    g_sync_timeout  => c_sync_timeout
  )
  port map (
    rst                     => rst,
    clk                     => clk,

    -- ST interface
    in_siso                 => out_siso,
    in_sosi                 => in_sosi,
    sync_in                 => sync_in,

    -- MM interface
    -- . control
    mon_evt                 => mon_evt,
    mon_sync                => mon_sync,
    mon_sync_timeout        => mon_sync_timeout,
    -- . siso
    mon_ready_stable        => mon_ready_stable,
    mon_xon_stable          => mon_xon_stable,
    -- . sosi
    mon_bsn_at_sync         => mon_bsn_at_sync,
    mon_nof_sop             => mon_nof_sop,
    mon_nof_err             => mon_nof_err,
    mon_nof_valid           => mon_nof_valid,

    mon_bsn_first           => mon_bsn_first,
    mon_bsn_first_cycle_cnt => mon_bsn_first_cycle_cnt
  );
end tb;
