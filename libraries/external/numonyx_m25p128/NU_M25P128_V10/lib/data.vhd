--
--        _/_/           _/_/_/
--      _/_/_/_/         _/_/_/
--      _/_/_/_/_/       _/_/_/              ____________________________________________
--      _/_/_/_/_/       _/_/_/             /                                           /
--      _/_/_/_/_/       _/_/_/            /                                 M25P128   /
--      _/_/_/_/_/       _/_/_/           /                                           /
--      _/_/_/_/_/_/     _/_/_/          /                                  128Mbit  /
--      _/_/_/_/_/_/     _/_/_/         /                   Low Voltage, SPI, 50Mhz /
--      _/_/_/ _/_/_/    _/_/_/        /                                           /
--      _/_/_/  _/_/_/   _/_/_/       /                     VHDL Behavioral Model /
--      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.0 /
--      _/_/_/    _/_/_/ _/_/_/     /                                           /
--      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2008 Numonyx B.V. /
--      _/_/_/      _/_/_/_/_/    /___________________________________________/
--      _/_/_/       _/_/_/_/
--      _/_/          _/_/_/
--
--
--             NUMONYX
--
--
-- common parameter and data types 
LIBRARY IEEE;
    Use IEEE.std_logic_1164.all;
LIBRARY Work;
    Use work.Def.all;
    use work.CUIcommandData.all;

package Data is 
  
  -------------------------------------------------
            ------  JEDEC id   --------
  -------------------------------------------------
  Constant ManufCode_id      : Std_Logic_vector(Byte_range) := x"20";
  Constant MemoryType_id     : Std_Logic_vector(Byte_range) := x"20";
  Constant MemoryCapacity_id : Std_Logic_vector(Byte_range) := x"18";
  
  -------------------------------------------------
  ------  Voltage Range  -- Tables 11, 13  --------
  -------------------------------------------------
  
  Constant Vcc_low      : real  :=  2.7;
  Constant Vcc_High     : real  :=  3.6;

  Constant Vwi_low       : real  := 1.5;
  Constant Vwi_High      : real  := 2.5;

  Constant Vpp_low       : real  := 8.5;
  Constant Vpp_High      : real  := 9.5;
  ------------------------------------------------------
  --------  Address and Data Bus definitions  ---------- 
  ------------------------------------------------------

  Constant AddrBus_dim     : natural := 24;
  Constant DataBus_dim     : natural := 8;                             -- data bus dimension 
 
  Subtype  AddrBus_range   is Integer range AddrBus_Dim - 1  downto 0;
  Subtype  DataBus_range   is Integer range DataBus_Dim - 1  downto 0;
  
        -- Address and Data Bus types are std_logic_vector because 
        -- the address and data busses can contain numeric values, but also 
        -- Hi-Z or XXX values
  
  SubType  AddrBus_type    is Std_Logic_Vector(AddrBus_range);
  SubType  DataBus_type    is Std_Logic_Vector(DataBus_range);  

        -- Address and Data Memory types are integer and bit_vector, 
        -- because a memory address is a number, and also you can 
        -- write only numeric values to the memory cells, 
        -- and not Hi-z or XXX values 
  
  Constant AddrMem_dim     : natural := 24;
  Constant DataMem_dim     : natural := 8;
  Constant Memory_dim      : natural := 2 ** AddrMem_Dim;                 -- memory dimension
  Subtype  AddrMem_type    is integer range 0 to Memory_Dim - 1;
  Subtype  DataMem_type    is bit_vector(DataMem_dim - 1 downto 0);
  Subtype  AddrMem_range   is Integer range AddrMem_Dim - 1  downto 0;
  Subtype  DataMem_range   is Integer range DataMem_Dim - 1  downto 0;

  Constant Last_Addr       : natural := Memory_Dim - 1;
   
  Constant DataLatch_dim    : natural := 8;
  Subtype  DataLatch_type   is Std_Logic_vector(DataLatch_dim - 1 downto 0);

  Constant AddrLatch_dim    : natural := 24;
  Subtype  AddrLatch_type   is Std_Logic_vector(AddrLatch_dim - 1 downto 0);

  Constant DataStatusReg_dim    : natural := 8;                        -- Status Register Dim
  Subtype  DataStatusReg_type   is bit_vector(DataStatusReg_dim - 1 downto 0); 
  
  Constant Page_dim : Natural := 256;
  Subtype  Page_range is Integer range Page_dim - 1 downto 0;
  
Constant ElectronicSignature_start  : Natural := 0;
Constant ElectronicSignature_end    : natural := 1;


  Type AddrBoundary_type is record 
       addrStart: AddrMem_type;
       addrEnd: AddrMem_type;
  end record;
  

-------------------------------------------------------------------------
  Constant Block_Dim : Natural := 64;  
  Constant BlockSize_Dim : Natural := 262144;  

-------------------------------------------------------------------------


type message_type is ( NoError_msg, none_msg, 
                     setSRWDbit_msg, resetSRWDbit_msg, setWELbit_msg, resetWELbit_msg,
                     noWRSR_msg, NoWrite_msg, WIP_msg, setWRSR_msg,
                     CmdAbtByRst_msg,
                     BkEraseProt_msg,
BadSeq_msg, BlockOverbound_msg, CmdSeq_msg, SuspCmd_msg, SuspAcc_msg , AddrRange_msg,
                       AddrTog_msg, BuffSize_msg, SuspAccWarn_msg, InvVDD_msg, InvVpp1_msg, InvVppH_msg,
                       BlockLock_msg, IncProgSeq_msg, IncEraseSeq_msg,
                       ByteToggle_msg, BlkBuffer_msg, AddrCFI_msg, PreProg_msg, NoBusy_msg,
                       NoSusp_msg, Suspend_msg, OTPlock_msg, UPlock_msg,
                       Busy_msg, PRegPreProg_msg, 
                       NoOPallowed_msg, NoOPSameBlock_msg, NoOPguaranteed_msg, -- for Multiple Bank
                       GENERIC_ERROR_msg -- Errore Generico
                     );
 
type Report_type is record 
       message : message_type;
       eventTime : time;
end record;
type vectorReport_type is array (Integer range <>) of Report_type;


  Type Status_type is record
        Busy      : Boolean;
        Suspended : Boolean;
        message : Message_type;
        address : AddrMem_type; -- Address which Program or Erase Operation
  end record;

  SubType Event is boolean;

  SubType ErrorEvent is Std_Logic;
  Type vectorErrorEvent_type is array (Integer range <>) of ErrorEvent;


  SubType TimeEvent is Time;
  Type vectorTimeEvent_type is array (Integer range <>) of TimeEvent;
 
  SubType IndexCommand_type is Integer;
  Type vectorIndexCommand_type is array (Integer range <>) of IndexCommand_type;


  Type DataOutBuffer_type is record
       data       : Std_Logic_vector(Byte_range);
       eventTime  : time;
  end record;
  Type vectorDataOutBuffer_type is array (Integer range <>) of DataOutBuffer_type;
  
-------------------------------------------------------------------------------------------------------------------
-- ---------- CUI Status --------------- --
 
-- ----- Read Bus Status Operation ----  --
type ReadStatusMode_type is (ReadArray_status, FastReadArray_status, ReadID_status, ReadStatusReg_status);
type ReadStatus_type is record
        Mode: ReadStatusMode_type;
        eventTime  : time;
end record;
Type vectorReadStatus_type is array (Integer range <>) of ReadStatus_type;

-------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- === Memory === --
Constant MemBufferSize_dim : Natural := 4; -- Function not avaible
type MemTaskName_type is (none, InitPage, addByteInPage, putPage, init, put, get, BlockErase, getBlock, putBlock, ReadIFall1, load, save, printList, printData);
     type MemoryTask_type is record
         task     : MemTaskName_type;
         data     : DataMem_type;
         address  : AddrMem_type;
       addressEnd : AddrMem_type;
         isAll1   : Boolean;
       eventTime  : time;
     end record;
Type vectorMemoryTask_type is array (Integer range <>) of MemoryTask_type;
--================================================================================        
type ProgTaskName_type is (none, isBusy, isSuspended, isAddrSuspended, Suspend, Resume,Program, resetStatus);
     type ProgramTask_type is record
        task            : ProgTaskName_type;
        status          : message_type;
        address         : AddrMem_type;
        data            : DataMem_type;
        eventTime       : time;
     end record;    
Type vectorProgramTask_type is array (Integer range <>) of ProgramTask_type;
--================================================================================        
--================================================================================        
type EraseTaskName_type is (none, BlockErase, BulkErase);
     type EraseTask_type is record
        task            : EraseTaskName_type;
        status          : message_type;
        address         : AddrMem_type;
        eventTime       : time;
     end record;
Type vectorEraseTask_type is array (Integer range <>) of EraseTask_type;

---------------------------
----  Status Register  ----
---------------------------

Type StatusRegisterTaskName_type is (none, Read, Write, ReadProtectAreaSizes, WriteProtectAreaSizes, set_SRWD, reset_SRWD, Read_SRWD, set_WEL, reset_WEL, Read_WEL);
     Type StatusRegisterTask_type is record
          task          : StatusRegisterTaskName_type;
          SR            : DataStatusReg_type;
          eventTime     : time;
     End Record;
Type vectorStatusRegisterTask_type is array (Integer range <>) of StatusRegisterTask_type;

end;
