-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Testbench for tech_mac_10g the 10G Ethernet IP technology wrapper.
-- Description:
--   The tb is self checking based on:
--   . proc_tech_mac_10g_rx_packet() for expected header and data type
--   . tx_pkt_cnt=rx_pkt_cnt > 0 must be true at the tb_end.
--
--   The tb finishes using severity failure because the simulation can not
--   finish by stopping the clocks and applying the resets due to that the IP
--   keeps on toggling internally.
-- Usage:
--   > as 10
--   > run -all
--
-- Remarks:
-- . c_tx_rx_loopback:
--   The tb uses a single DUT and c_tx_rx_loopback to model the link.
--   The g_direction = "TX_RX" is verified properly by c_tx_rx_loopback =
--   TRUE. Internally "RX_ONLY" is equivalent to "TX_RX" so there
--   c_tx_rx_loopback = TRUE is also suitable. For TX_ONLY the MAC Tx is
--   internally loopbacked to the MAC Rx, so there the external loopback is
--   not used and set to c_tx_rx_loopback = FALSE.

library IEEE, technology_lib, tech_pll_lib, tech_mac_10g_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.common_interface_layers_pkg.all;
use common_lib.common_network_layers_pkg.all;
use common_lib.common_network_total_header_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use technology_lib.technology_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_mac_10g_lib.tech_mac_10g_component_pkg.all;
use tech_mac_10g_lib.tb_tech_mac_10g_pkg.all;
use tech_pll_lib.tech_pll_component_pkg.all;

entity tb_tr_10GbE is
  -- Test bench control parameters
  generic (
    g_technology              : natural := c_tech_select_default;
    g_tb_end                  : boolean := true;  -- when TRUE then tb_end ends this simulation, else a higher multi-testbench will end the simulation
    g_no_dut                  : boolean := false;  -- default FALSE to verify the DUT, else use TRUE to verify the tb itself without the DUT
    g_dp_clk_period           : time := 5 ns;  -- must be ~< 9000/(9000-c_tx_fifo_fill) * g_ref_clk_156_period
    g_sim_level               : natural := 0;  -- 0 = use IP; 1 = use fast serdes model
    g_nof_channels            : natural := 1;
    g_direction               : string := "TX_RX";  -- "TX_RX", "TX_ONLY", "RX_ONLY"
    g_ref_clk_644_period      : time := tech_pll_clk_644_period;  -- for 10GBASE-R
    g_ref_clk_156_period      : time := 6.4 ns;  -- for XAUI
    g_data_type               : natural := c_tb_tech_mac_10g_data_type_symbols;
    g_verify_link_recovery    : boolean := true
  );
  port (
    tb_end : out std_logic
  );
end tb_tr_10GbE;

architecture tb of tb_tr_10GbE is
  constant c_sim                : boolean := true;  -- tr_xaui_align_dly has time delay ~ 1 sec, so not suitable for simulation

  constant cal_clk_period       : time := 25 ns;  -- 40 MHz

  constant phy_delay            : time := sel_a_b(g_sim_level = 0, 0 ns, 0 ns);
  constant c_tx_rx_loopback     : boolean := g_direction /= "TX_ONLY";

  constant c_tx_fifo_fill       : natural := 100;

  constant c_pkt_length_arr1    : t_nat_natural_arr := array_init(0, 50, 1) & (1472, 1473) & 9000;  -- frame longer than 1518-46 = 1472 is received with rx_sosi.err = 8
                                                                                                    -- jumbo frame is 9018-46 = 8972
  constant c_pkt_length_arr2    : t_nat_natural_arr := array_init(46, 10, 139) & 1472;
  constant c_pkt_length_arr     : t_nat_natural_arr := c_pkt_length_arr1 & c_pkt_length_arr2;
  constant c_nof_pkt1           : natural := c_pkt_length_arr1'length;
  constant c_nof_pkt2           : natural := c_pkt_length_arr2'length;
  constant c_nof_pkt            : natural := sel_a_b(g_verify_link_recovery, c_nof_pkt1 + c_nof_pkt2, c_nof_pkt1);

  constant c_dst_mac            : std_logic_vector(c_network_eth_mac_slv'range) := X"10FA01020300";
  constant c_src_mac            : std_logic_vector(c_network_eth_mac_slv'range) := X"123456789ABC";  -- = 12-34-56-78-9A-BC
  constant c_src_mac_tx         : std_logic_vector(c_network_eth_mac_slv'range) := c_src_mac;
  --CONSTANT c_src_mac_tx         : STD_LOGIC_VECTOR(c_network_eth_mac_slv'RANGE) := X"100056789ABC";  -- = 10-00-56-78-9A-BC
  constant c_ethertype          : std_logic_vector(c_network_eth_type_slv'range) := X"10FA";
  constant c_etherlen           : std_logic_vector(c_network_eth_type_slv'range) := "0000000000010000";

  -- Packet headers
  constant c_eth_header_ethertype : t_network_eth_header := (c_dst_mac, c_src_mac_tx, c_ethertype);
  constant c_eth_header_etherlen  : t_network_eth_header := (c_dst_mac, c_src_mac_tx, c_etherlen);

  signal total_header      : t_network_total_header := c_network_total_header_ones;  -- default fill all fields with value 1

  -- Clocks and reset
  signal tx_end_arr        : std_logic_vector(g_nof_channels - 1 downto 0);
  signal tx_end            : std_logic;
  signal rx_end            : std_logic;
  signal cal_clk           : std_logic := '1';  -- calibration clock
  signal mm_clk            : std_logic;  -- memory-mapped bus clock
  signal mm_rst            : std_logic;  -- reset synchronous with mm_clk
  signal dp_clk            : std_logic := '1';  -- data path clock
  signal dp_rst            : std_logic;  -- reset synchronous with dp_clk

  -- External reference clocks
  signal tr_ref_clk_644    : std_logic := '1';  -- 10GBASE-R
  signal tr_ref_clk_312    : std_logic;  -- 10GBASE-R
  signal tr_ref_clk_156    : std_logic := '1';  -- 10GBASE-R or XAUI
  signal tr_ref_rst_156    : std_logic;  -- 10GBASE-R or XAUI

  -- MAC 10G control interface
  signal mm_init           : std_logic;
  signal mac_mosi_wrdata   : std_logic_vector(c_word_w - 1 downto 0);  -- for channel 0, 32 bit
  signal mac_mosi          : t_mem_mosi;
  signal mac_miso          : t_mem_miso;
  signal mac_miso_rdval    : std_logic;  -- for channel 0
  signal mac_miso_rddata   : std_logic_vector(c_word_w - 1 downto 0);  -- for channel 0, 32 bit

  -- MAC 10G transmit interface
  signal tx_en             : std_logic := '1';
  signal tx_siso_arr       : t_dp_siso_arr(g_nof_channels - 1 downto 0);
  signal tx_sosi_arr       : t_dp_sosi_arr(g_nof_channels - 1 downto 0);
  signal tx_sosi_data      : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);  -- for channel 0, 64 bit

  -- MAC 10G receive interface
  signal rx_siso_arr       : t_dp_siso_arr(g_nof_channels - 1 downto 0);
  signal rx_sosi_arr       : t_dp_sosi_arr(g_nof_channels - 1 downto 0);
  signal rx_sosi_data      : std_logic_vector(c_tech_mac_10g_data_w - 1 downto 0);  -- for channel 0, 64 bit

  -- PHY XAUI serial I/O
  signal xaui_tx_arr       : t_xaui_arr(g_nof_channels - 1 downto 0);
  signal xaui_rx_arr       : t_xaui_arr(g_nof_channels - 1 downto 0) := (others => (others => '0'));

  -- PHY 10GBASE-R serial IO
  signal serial_tx_arr     : std_logic_vector(g_nof_channels - 1 downto 0);
  signal serial_rx_arr     : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');

  -- Model a serial link fault
  signal link_fault_arr    : std_logic_vector(g_nof_channels - 1 downto 0);

  -- Verification
  signal tx_pkt_cnt_arr    : t_natural_arr(g_nof_channels - 1 downto 0) := (others => 0);
  signal rx_pkt_cnt_arr    : t_natural_arr(g_nof_channels - 1 downto 0) := (others => 0);
  signal rx_toggle_arr     : std_logic_vector(g_nof_channels - 1 downto 0) := (others => '0');  -- toggle after every received packet
begin
  cal_clk <= not cal_clk after cal_clk_period / 2;  -- Calibration clock

  dp_clk  <= not dp_clk  after g_dp_clk_period / 2;  -- DP clock
  dp_rst  <= '1', '0' after g_dp_clk_period * 10;

  -- debug signals to ease monitoring in wave window
  mac_mosi_wrdata <= mac_mosi.wrdata(c_word_w - 1 downto 0);
  mac_miso_rddata <= mac_miso.rddata(c_word_w - 1 downto 0);
  mac_miso_rdval <= '1' when mac_mosi.rd = '1' and mac_miso.waitrequest = '0' else '0';  -- c_rd_latency = 1

  tx_sosi_data <= tx_sosi_arr(0).data(c_tech_mac_10g_data_w - 1 downto 0);
  rx_sosi_data <= rx_sosi_arr(0).data(c_tech_mac_10g_data_w - 1 downto 0);

  -- Use signal to leave unused fields 'X'
  total_header.eth <= c_eth_header_ethertype;

  -- Generate reference clocks
  gen_ref_clocks_xaui : if g_technology = c_tech_stratixiv generate
    tr_ref_clk_644 <= 'X';
    tr_ref_clk_312 <= 'X';
    tr_ref_clk_156 <= not tr_ref_clk_156 after g_ref_clk_156_period / 2;
    tr_ref_rst_156 <= '1', '0' after g_ref_clk_156_period * 5;
  end generate;

  gen_ref_clocks_10gbase_r : if g_technology /= c_tech_stratixiv generate
    tr_ref_clk_644 <= not tr_ref_clk_644 after g_ref_clk_644_period / 2;

    pll : entity tech_pll_lib.tech_pll_xgmii_mac_clocks
    generic map (
      g_technology => g_technology
    )
    port map (
      refclk_644 => tr_ref_clk_644,
      rst_in     => mm_rst,
      clk_156    => tr_ref_clk_156,
      clk_312    => tr_ref_clk_312,
      rst_156    => tr_ref_rst_156,
      rst_312    => open
    );
  end generate;

  -- Setup all MACs in series
  u_mm_setup : entity tech_mac_10g_lib.tb_tech_mac_10g_setup
  generic map (
    g_technology    => g_technology,
    g_nof_macs      => g_nof_channels,
    g_src_mac       => c_src_mac
  )
  port map (
    tb_end    => rx_end,
    mm_clk    => mm_clk,
    mm_rst    => mm_rst,
    mm_init   => mm_init,
    mac_mosi  => mac_mosi,
    mac_miso  => mac_miso
  );

  gen_transmitter : for I in 0 to g_nof_channels - 1 generate
    -- Packet transmitter
    u_transmitter : entity tech_mac_10g_lib.tb_tech_mac_10g_transmitter
    generic map (
      g_data_type            => g_data_type,
      g_pkt_length_arr1      => c_pkt_length_arr1,
      g_pkt_length_arr2      => c_pkt_length_arr2,
      g_verify_link_recovery => g_verify_link_recovery
    )
    port map (
      mm_init        => mm_init,
      total_header   => total_header,
      tx_clk         => dp_clk,
      tx_siso        => tx_siso_arr(I),
      tx_sosi        => tx_sosi_arr(I),
      link_fault     => link_fault_arr(I),
      tx_end         => tx_end_arr(I)
    );
  end generate;

  no_dut : if g_no_dut = true generate
    rx_sosi_arr <= tx_sosi_arr;
    tx_siso_arr <= rx_siso_arr;
  end generate;

  gen_dut : if g_no_dut = false generate
    u_tr_10GbE : entity work.tr_10GbE
    generic map (
      g_technology             => g_technology,
      g_sim                    => c_sim,
      g_sim_level              => g_sim_level,  -- 0 = use IP; 1 = use fast serdes model
      g_nof_macs               => g_nof_channels,
      g_direction              => g_direction,
      g_use_mdio               => true,
      g_mdio_epcs_dis          => true,  -- TRUE disables Enhanced PCS on init; e.g. to target a 10GbE card in PC that does not support it
      g_tx_fifo_fill           => c_tx_fifo_fill,
      g_tx_fifo_size           => 256,
      g_word_alignment_padding => true
    )
    port map (
      -- Transceiver PLL reference clock
      tr_ref_clk_644      => tr_ref_clk_644,  -- 644.531250 MHz for 10GBASE-R
      tr_ref_clk_312      => tr_ref_clk_312,  -- 312.5      MHz for 10GBASE-R
      tr_ref_clk_156      => tr_ref_clk_156,  -- 156.25     MHz for 10GBASE-R or for XAUI
      tr_ref_rst_156      => tr_ref_rst_156,  -- for 10GBASE-R or for XAUI

      -- Calibration & reconfig clock
      cal_rec_clk         => cal_clk,

      -- MM interface
      mm_rst              => mm_rst,
      mm_clk              => mm_clk,

      reg_mac_mosi        => mac_mosi,
      reg_mac_miso        => mac_miso,

      xaui_mosi           => c_mem_mosi_rst,
      xaui_miso           => OPEN,

      mdio_mosi_arr       => (others => c_mem_mosi_rst),
      mdio_miso_arr       => OPEN,

      -- DP interface
      dp_rst              => dp_rst,
      dp_clk              => dp_clk,

      snk_out_arr         => tx_siso_arr,
      snk_in_arr          => tx_sosi_arr,

      src_in_arr          => rx_siso_arr,
      src_out_arr         => rx_sosi_arr,

      -- Serial XAUI interface
      xaui_tx_arr         => xaui_tx_arr,
      xaui_rx_arr         => xaui_rx_arr,

      -- Serial IO
      serial_tx_arr       => serial_tx_arr,
      serial_rx_arr       => serial_rx_arr,

      -- MDIO interface
      mdio_rst            => OPEN,
      mdio_mdc_arr        => OPEN,
      mdio_mdat_in_arr    => (others => '0'),
      mdio_mdat_oen_arr   => open
    );
  end generate;

  gen_link_connect : for I in 0 to g_nof_channels - 1 generate
    u_link_connect : entity tech_mac_10g_lib.tb_tech_mac_10g_link_connect
    generic map (
      g_loopback    => c_tx_rx_loopback,
      g_link_delay  => phy_delay
    )
    port map (
      link_fault   => link_fault_arr(I),  -- when '1' then forces rx_serial_arr(0)='0'

      -- 10GBASE-R serial layer connect
      serial_tx    => serial_tx_arr(I),
      serial_rx    => serial_rx_arr(I),  -- connects to delayed tx_serial when g_loopback=TRUE

      -- XAUI serial layer connect
      xaui_tx      => xaui_tx_arr(I),
      xaui_rx      => xaui_rx_arr(I)  -- connects to delayed xaui_tx when g_loopback=TRUE
    );
  end generate;

  gen_receiver : for I in 0 to g_nof_channels - 1 generate
    u_receiver : entity tech_mac_10g_lib.tb_tech_mac_10_receiver
    generic map (
      g_data_type  => g_data_type
    )
    port map (
      mm_init        => mm_init,
      total_header   => total_header,
      rx_clk         => dp_clk,
      rx_sosi        => rx_sosi_arr(I),
      rx_siso        => rx_siso_arr(I),
      rx_toggle      => rx_toggle_arr(I)
    );
  end generate;

  gen_verify_rx_at_eop : for I in 0 to g_nof_channels - 1 generate
    u_verify_rx_at_eop : entity tech_mac_10g_lib.tb_tech_mac_10_verify_rx_at_eop
    generic map (
      g_no_padding     => g_no_dut,
      g_pkt_length_arr => c_pkt_length_arr
    )
    port map (
      tx_clk      => dp_clk,
      tx_sosi     => tx_sosi_arr(I),
      rx_clk      => dp_clk,
      rx_sosi     => rx_sosi_arr(I)
    );
  end generate;

  gen_verify_rx_pkt_cnt : for I in 0 to g_nof_channels - 1 generate
    u_verify_rx_pkt_cnt : entity tech_mac_10g_lib.tb_tech_mac_10g_verify_rx_pkt_cnt
    generic map (
      g_nof_pkt   => c_nof_pkt
    )
    port map (
      tx_clk      => dp_clk,
      tx_sosi     => tx_sosi_arr(I),
      rx_clk      => dp_clk,
      rx_sosi     => rx_sosi_arr(I),
      tx_pkt_cnt  => tx_pkt_cnt_arr(I),
      rx_pkt_cnt  => rx_pkt_cnt_arr(I),
      rx_end      => rx_end
    );
  end generate;

  -- Stop the simulation
  tx_end <= andv(tx_end_arr);

  u_simulation_end : entity tech_mac_10g_lib.tb_tech_mac_10g_simulation_end
  generic map (
    g_tb_end            => g_tb_end,
    g_nof_clk_to_rx_end => 1000
  )
  port map (
    clk       => dp_clk,
    tx_end    => tx_end,
    rx_end    => rx_end,
    tb_end    => tb_end
  );
end tb;
