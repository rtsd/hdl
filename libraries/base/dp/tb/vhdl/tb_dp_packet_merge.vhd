-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
-- . Test bench for dp_packet_merge
-- Description:
-- . The p_stimuli_st uses proc_dp_gen_block_data to generate g_nof_repeat packets for the DUT. The output of the DUT needs
--   to be similar e.g. by means of an inverse DUT component so that the proc_dp_verify_* procedures can be used to verify
--   that the counter data in the sosi data fields is passed on correctly. Furthermore the proc_dp_verify_* procedures
--   verify that the test bench has yielded output results at all and that the output valid, sop, eop, and ready fits the
--   streaming interface specification.
-- . A seperate p_stimuli_mm process may be used to verify the nof_pkt input of the DUT.
-- . Use separate tb_dp_packet_merge_unmerge to verify dp_packet_unmerge, to have easier control
--   over expected output channel, err and empty of dp_packet_unmerge.
-- Usage:
-- > as 10
-- > run -all
--
-- Design steps:
-- . One or multiple instances of the p_stimuli_st and the set of proc_dp_verify_* fit almost any DP component. Verification
--   steps:
--   - First make the tb correct for e_active flow control and typical generic values e.g. g_nof_pkt=3 and g_pkt_len=10.
--   - After that make it work for the boundary generic values, e.g. g_nof_pkt=0, 1 and g_pkt_len=1.
--   - Then try the tb for e_random flow control and if useful also for e_pulse flow control of the stimuli input enable.
--   - Finally try e_random flow control for the verify ready
-- . For DP components that have MM control a seperate p_stimuli_mm can be added in parallel.
-- . Combine several instances of tb_dp_packet_merge in tb_tb_dp_packet_merge.
-- . Add tb_tb_dp_packet_merge to the DP lib regression testbench tb_tb_tb_backpressure.vhd

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_packet_merge is
  generic (
    -- general
    g_flow_control_stimuli   : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    g_flow_control_verify    : t_dp_flow_control_enum := e_active;  -- always active, random or pulse flow control
    -- specific
    g_pipeline_ready         : boolean := true;
    g_data_w                 : natural := 4;
    g_nof_repeat             : natural := 24;
    g_nof_pkt                : natural := 3;
    g_pkt_len                : natural := 10;
    g_pkt_gap                : natural := 0;
    g_align_at_sync          : boolean := false;
    g_verify_bsn_err         : boolean := false;
    g_bsn_increment          : natural := 2;
    g_bsn_err_at_pkt_index   : natural := 6  -- force wrong snk_in.bsn for packet with this index, in range(g_nof_repeat)
  );
end tb_dp_packet_merge;

architecture tb of tb_dp_packet_merge is
  constant c_rl                       : natural := 1;
  constant c_use_ready                : boolean := g_flow_control_verify /= e_active;

  constant c_pulse_active             : natural := 1;
  constant c_pulse_period             : natural := 7;

  constant c_sync_period              : natural := 10;
  constant c_sync_offset              : natural := 7;

  constant c_data_max                 : unsigned(g_data_w - 1 downto 0) := (others => '1');
  constant c_data_init                : integer := -1;
  constant c_bsn_init                 : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := X"0000000000000000";  -- X"0877665544332211"
  constant c_bsn_error                : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0) := (others => '1');  -- use -1 as bsn error value
  constant c_err_init                 : natural := 247;
  constant c_bsn_err_bi               : natural := 31;  -- use sufficiently high bsn error bit index, that is outside counter range of c_err_init
  constant c_channel_init             : integer := 5;  -- fixed

  constant c_nof_pkt_not_zero         : natural := sel_a_b(g_nof_pkt = 0, 1, g_nof_pkt);
  constant c_nof_merged_sop           : natural := sel_a_b(g_nof_pkt = 0, 0, ceil_div(g_nof_repeat, c_nof_pkt_not_zero));
  constant c_verify_at_least          : natural := largest(1, c_nof_merged_sop / 2);  -- verify that at least some packets have been merged, not exact to allow variation by p_stimuli_mm
  constant c_verify_data_gap          : natural := g_nof_pkt;
  constant c_verify_bsn_gap           : natural := g_nof_pkt * g_bsn_increment;
  constant c_exp_err_at_pkt_index     : natural := g_bsn_err_at_pkt_index / sel_a_b(g_nof_pkt = 0, 1, g_nof_pkt);

  signal tb_end                     : std_logic := '0';
  signal clk                        : std_logic := '1';
  signal rst                        : std_logic := '1';
  signal sl1                        : std_logic := '1';

  signal random_0                   : std_logic_vector(14 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal random_1                   : std_logic_vector(15 downto 0) := (others => '0');  -- use different lengths to have different random sequences
  signal pulse_0                    : std_logic;
  signal pulse_1                    : std_logic;
  signal pulse_en                   : std_logic := '1';

  signal stimuli_en                 : std_logic := '1';
  signal stimuli_src_in             : t_dp_siso;
  signal stimuli_src_out            : t_dp_sosi;
  signal stimuli_data               : std_logic_vector(g_data_w - 1 downto 0);

  signal dp_packet_merge_snk_out    : t_dp_siso;
  signal dp_packet_merge_snk_in     : t_dp_sosi;
  signal dp_packet_merge_src_in     : t_dp_siso;
  signal dp_packet_merge_src_out    : t_dp_sosi;

  signal dp_packet_unmerge_snk_out  : t_dp_siso;
  signal dp_packet_unmerge_snk_in   : t_dp_sosi;
  signal dp_packet_unmerge_src_in   : t_dp_siso;
  signal dp_packet_unmerge_src_out  : t_dp_sosi;

  signal prev_verify_snk_out        : t_dp_siso;
  signal verify_snk_out             : t_dp_siso := c_dp_siso_rdy;
  signal verify_snk_in              : t_dp_sosi;
  signal verify_data                : std_logic_vector(g_data_w - 1 downto 0);
  signal prev_verify_snk_in         : t_dp_sosi;
  signal merged_pkt_cnt             : natural := 0;
  signal merged_pkt_err             : std_logic;

  signal verify_hold_sop            : std_logic := '0';
  signal verify_en_valid            : std_logic := '0';
  signal verify_en_sop              : std_logic := '0';
  signal verify_en_eop              : std_logic := '0';
  signal verify_done                : std_logic := '0';
  signal verify_value_en            : std_logic := sel_a_b(g_nof_pkt = 0, '0', '1');

  signal expected_verify_snk_in     : t_dp_sosi;
begin
  clk <= (not clk) or tb_end after clk_period / 2;
  rst <= '1', '0' after clk_period * 7;

  random_0 <= func_common_random(random_0) when rising_edge(clk);
  random_1 <= func_common_random(random_1) when rising_edge(clk);

  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period,   '1', rst, clk, pulse_en, pulse_0);
  proc_common_gen_duty_pulse(c_pulse_active, c_pulse_period + 1, '1', rst, clk, pulse_en, pulse_1);

  ------------------------------------------------------------------------------
  -- STREAM CONTROL
  ------------------------------------------------------------------------------

  stimuli_en           <= '1'                     when g_flow_control_stimuli = e_active else
                          random_0(random_0'high) when g_flow_control_stimuli = e_random else
                          pulse_0                 when g_flow_control_stimuli = e_pulse;

  verify_snk_out.ready <= '1'                     when g_flow_control_verify = e_active  else
                          random_1(random_1'high) when g_flow_control_verify = e_random  else
                          pulse_1                 when g_flow_control_verify = e_pulse;

  ------------------------------------------------------------------------------
  -- DATA GENERATION
  ------------------------------------------------------------------------------

  -- Generate data path input data
  p_stimuli_st : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
    variable v_bsn  : std_logic_vector(c_dp_stream_bsn_w - 1 downto 0);
  begin
    -- Adjust initial sosi field values by -1 to compensate for auto increment
    v_bsn          := INCR_UVEC(c_bsn_init,                    -1);
    v_sosi.channel := INCR_UVEC(TO_DP_CHANNEL(c_channel_init), -1);
    v_sosi.data    := INCR_UVEC(TO_DP_DATA(c_data_init),       -1);
    v_sosi.err     := INCR_UVEC(TO_DP_ERROR(c_err_init),       -1);

    stimuli_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(clk, rst);
    proc_common_wait_some_cycles(clk, 5);

    -- Generate c_nof_repeat packets
    for I in 0 to g_nof_repeat - 1 loop
      -- Auto increment v_sosi field values for this packet
      v_bsn          := INCR_UVEC(v_bsn, g_bsn_increment);
      v_sosi.bsn     := v_bsn;
      v_sosi.sync    := sel_a_b((unsigned(v_sosi.bsn) mod c_sync_period) = c_sync_offset, '1', '0');  -- insert sync starting at BSN=c_sync_offset and with period c_sync_period
      v_sosi.channel := INCR_UVEC(v_sosi.channel, 1);
      v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len);
      v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_data_w - 1 downto 0));  -- wrap when >= 2**g_data_w
      v_sosi.err     := INCR_UVEC(v_sosi.err, 1);

      -- Force bsn error in one snk_in block, to verify src_out.err(g_bsn_err_bi) bit
      if g_verify_bsn_err = true then
        if I = g_bsn_err_at_pkt_index then
          v_sosi.bsn := c_bsn_error;
        end if;
      end if;

      -- Send packet
      proc_dp_gen_block_data(g_data_w, TO_UINT(v_sosi.data), g_pkt_len, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn, clk, stimuli_en, stimuli_src_in, stimuli_src_out);

      -- Insert optional gap between the packets
      proc_common_wait_some_cycles(clk, g_pkt_gap);
    end loop;

    -- Determine expected sosi field values after end of stimuli
    -- . e_qual (account for merge size g_nof_pkt)
    -- . e_at_least
    v_sosi.bsn     := std_logic_vector( unsigned(c_bsn_init) + c_verify_at_least * g_nof_pkt);
    v_sosi.channel := TO_DP_CHANNEL(c_channel_init           + c_verify_at_least * g_nof_pkt);
    v_sosi.err     := TO_DP_ERROR(c_err_init                 + c_verify_at_least * g_nof_pkt);

    -- . account for g_pkt_len
    v_sosi.data    := INCR_UVEC(v_sosi.data, g_pkt_len - 1);
    v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_data_w - 1 downto 0));  -- wrap when >= 2**g_data_w
    expected_verify_snk_in <= v_sosi;

    -- Signal end of stimuli
    proc_common_wait_some_cycles(clk, 100);  -- latency from stimuli to verify depends on the flow control, so wait sufficiently long for last packet to have passed through
    if verify_value_en = '1' then
      proc_common_gen_pulse(clk, verify_done);
    end if;
    proc_common_wait_some_cycles(clk, 50);
    tb_end <= '1';
    wait;
  end process;

  ------------------------------------------------------------------------------
  -- DATA VERIFICATION
  ------------------------------------------------------------------------------

  -- Start verify after first valid, sop or eop
  verify_en_valid <= '1' when verify_snk_in.valid = '1' and rising_edge(clk);
  verify_en_sop   <= '1' when verify_snk_in.sop = '1'   and rising_edge(clk);
  verify_en_eop   <= '1' when verify_snk_in.eop = '1'   and rising_edge(clk);

  -- Verify that the stimuli have been applied at all
  proc_dp_verify_value("verify_snk_in.valid",               clk, verify_done, sl1, verify_en_valid);
  proc_dp_verify_value("verify_snk_in.sop",                 clk, verify_done, sl1, verify_en_sop);
  proc_dp_verify_value("verify_snk_in.eop",                 clk, verify_done, sl1, verify_en_eop);
  proc_dp_verify_value("verify_snk_in.data",    e_equal,    clk, verify_done, expected_verify_snk_in.data,    verify_snk_in.data);
  proc_dp_verify_value("verify_snk_in.bsn",     e_at_least, clk, verify_done, expected_verify_snk_in.bsn,     verify_snk_in.bsn);
  proc_dp_verify_value("verify_snk_in.channel", e_at_least, clk, verify_done, expected_verify_snk_in.channel, verify_snk_in.channel);
  proc_dp_verify_value("verify_snk_in.err",     e_at_least, clk, verify_done, expected_verify_snk_in.err,     verify_snk_in.err);

  -- Verify that the output is incrementing data, like the input stimuli
  proc_dp_verify_data("verify_snk_in.data",    c_rl, c_data_max,   c_unsigned_1,                      clk, verify_en_valid, verify_snk_out.ready, verify_snk_in.valid, verify_snk_in.data,    prev_verify_snk_in.data);

  gen_verify_bsn_increment : if g_verify_bsn_err = false generate
    proc_dp_verify_data("verify_snk_in.bsn",   c_rl, c_unsigned_0, to_unsigned(c_verify_bsn_gap, 32), clk, verify_en_sop,   verify_snk_out.ready, verify_snk_in.sop,   verify_snk_in.bsn,     prev_verify_snk_in.bsn);

    gen_verify_err : if g_nof_pkt = 1 generate
      -- Assume verifying g_nof_pkt = 1 is sufficient. Verifing g_nof_pkt > 1 is more difficult,
      -- because the merged output error field is the bitwise OR of the input error fields
      proc_dp_verify_data("verify_snk_in.err", c_rl, c_unsigned_0, to_unsigned(c_verify_data_gap, 32), clk, verify_en_eop,   verify_snk_out.ready, verify_snk_in.eop,   verify_snk_in.err,     prev_verify_snk_in.err);
    end generate;
  end generate;
  proc_dp_verify_data("verify_snk_in.channel", c_rl, c_unsigned_0, to_unsigned(c_verify_data_gap, 32), clk, verify_en_sop,   verify_snk_out.ready, verify_snk_in.sop,   verify_snk_in.channel, prev_verify_snk_in.channel);

  -- Verify that the output bsn error bit is set if an input block was missed in a merge
  merged_pkt_err <= verify_snk_in.err(c_bsn_err_bi);

  gen_verify_bsn_err : if g_verify_bsn_err = true generate
    p_verify_bsn_err : process(clk)
    begin
      if rising_edge(clk) then
        -- count number of merged packets
        if verify_snk_in.sop = '1' then
          merged_pkt_cnt <= merged_pkt_cnt + 1;
        end if;
        -- verify err field for merged packet with input bsn error
        if verify_snk_in.sop = '1' then
          if merged_pkt_cnt = c_exp_err_at_pkt_index + 1 and g_bsn_increment > 0 then
            assert merged_pkt_err = '1'
              report "Unexpected sosi.err = 0"
              severity ERROR;
          else
            assert merged_pkt_err = '0'
              report "Unexpected sosi.err = 1"
              severity ERROR;
          end if;
        end if;
      end if;
    end process;
  end generate;

  -- Verify output packet ctrl
  proc_dp_verify_sop_and_eop(clk, verify_snk_in.valid, verify_snk_in.sop, verify_snk_in.eop, verify_hold_sop);

  -- Verify output ready latency
  proc_dp_verify_valid(clk, verify_en_valid, verify_snk_out.ready, prev_verify_snk_out.ready, verify_snk_in.valid);

  ------------------------------------------------------------------------------
  -- DUT dp_packet_merge
  ------------------------------------------------------------------------------

  stimuli_src_in         <= dp_packet_merge_snk_out;
  dp_packet_merge_snk_in <= stimuli_src_out;

  -- Merge every g_nof_pkt incomming packets into output packets
  u_dp_packet_merge : entity work.dp_packet_merge
  generic map (
    g_use_ready      => c_use_ready,
    g_pipeline_ready => g_pipeline_ready,
    g_nof_pkt        => g_nof_pkt,
    g_align_at_sync  => g_align_at_sync,
    g_bsn_increment  => g_bsn_increment,
    g_bsn_err_bi     => c_bsn_err_bi
  )
  port map (
    rst       => rst,
    clk       => clk,

    snk_out   => dp_packet_merge_snk_out,
    snk_in    => dp_packet_merge_snk_in,

    src_in    => dp_packet_merge_src_in,
    src_out   => dp_packet_merge_src_out
  );

  ------------------------------------------------------------------------------
  -- Optional reverse DUT dp_packet_unmerge
  ------------------------------------------------------------------------------
  dp_packet_merge_src_in <= verify_snk_out;
  verify_snk_in          <= dp_packet_merge_src_out;

  -- Map to slv to ease monitoring in wave window
  stimuli_data <= stimuli_src_out.data(g_data_w - 1 downto 0);
  verify_data  <= verify_snk_in.data(g_data_w - 1 downto 0);
end tb;
