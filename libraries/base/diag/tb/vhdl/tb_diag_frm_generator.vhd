--------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Test bench for diag_frm_generator
-- Usage:
--   > as 5
--   > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;

entity tb_diag_frm_generator is
end tb_diag_frm_generator;

architecture tb of tb_diag_frm_generator is
  constant c_period       : time    := 100 ns;
  constant c_nof_cycles   : natural := 200;

  constant c_sel          : std_logic := '1';  -- select COUNTER sequence
  constant c_frame_len    : natural := 20;  -- >= 2, test frame length
  constant c_frame_len_w  : natural := ceil_log2(c_frame_len);
  constant c_sof_period   : natural := 35;
  constant c_sof_period_w : natural := ceil_log2(c_sof_period);
  constant c_frame_cnt_w  : natural := 32;
  constant c_dat_w        : natural := c_frame_len_w;
  constant c_symbol_w     : natural := c_dat_w;
  constant c_empty_w      : natural := ceil_log2(c_dat_w / c_symbol_w);
  constant c_empty        : natural := 1;

  signal tb_end          : std_logic := '0';
  signal rst             : std_logic;
  signal clk             : std_logic := '1';

  signal diag_en         : std_logic;
  signal diag_sel        : std_logic := c_sel;
  signal diag_frame_len  : std_logic_vector(c_frame_len_w - 1 downto 0) := TO_UVEC(c_frame_len, c_frame_len_w);
  signal diag_sof_period : std_logic_vector(c_sof_period_w - 1 downto 0) := TO_UVEC(c_sof_period, c_sof_period_w);
  signal diag_frame_cnt  : std_logic_vector(c_frame_cnt_w - 1 downto 0);

  signal seq_req         : std_logic;
  signal seq_dat         : std_logic_vector(c_dat_w - 1 downto 0);
  signal prev_seq_dat    : std_logic_vector(c_dat_w - 1 downto 0) := TO_UVEC(c_frame_len - 1, c_dat_w);
  signal seq_val         : std_logic;
  signal seq_sop         : std_logic;
  signal seq_eop         : std_logic;
  signal seq_empty       : std_logic_vector(c_empty_w - 1 downto 0);  -- nof empty symbols in last out_dat word marked by out_eop

  signal init_dat        : std_logic_vector(c_dat_w - 1 downto 0);
begin
  rst <= '1', '0' after c_period / 10;
  clk <= not clk or tb_end after c_period / 2;

  stimuli: process
  -- run 200 us
  begin
    diag_sel       <= c_sel;
    diag_frame_len <= TO_UVEC(c_frame_len, c_frame_len_w);
    diag_en        <= '0';
    seq_req        <= '1';
    wait for 10 * c_period;

    -- Keep seq_req='1'
    seq_req  <= '1';
    wait for c_period;
    diag_en  <= '1';  -- Enable frames generator
    wait for c_nof_cycles * c_period;
    wait for 10 * c_period;
    diag_en  <= '0';  -- Disable frames generator
    wait for c_nof_cycles * c_period;

    -- Toggle seq_req='1'
    seq_req  <= '1';
    wait for c_period;
    diag_en  <= '1';  -- Enable frames generator
    wait for 20 * c_period;
    for I in 0 to c_sof_period loop
      seq_req  <= '0';
      wait for c_period;
      seq_req  <= '1';
      wait for c_period;
    end loop;
    for I in 0 to c_sof_period loop
      seq_req  <= '0';
      wait for c_period;
      wait for c_period;
      seq_req  <= '1';
      wait for c_period;
    end loop;
    for I in 0 to c_sof_period loop
      seq_req  <= '0';
      wait for c_period;
      seq_req  <= '1';
      wait for c_period;
      wait for c_period;
    end loop;
    wait for c_nof_cycles * c_period;
    wait for 10 * c_period;
    diag_en  <= '0';  -- Disable frames generator

    wait for c_nof_cycles * c_period;
    tb_end <= '1';
    wait;
  end process;

  u_frm_gen : entity work.diag_frm_generator
  generic map (
    g_sel         => c_sel,
    g_frame_len   => c_frame_len,
    g_sof_period  => c_sof_period,
    g_frame_cnt_w => c_frame_cnt_w,
    g_dat_w       => c_dat_w,
    g_symbol_w    => c_symbol_w,
    g_empty       => c_empty
  )
  port map (
    rst             => rst,
    clk             => clk,
    clken           => '1',

    -- Static control input (connect via MM or leave open to use default)
    diag_en         => diag_en,
    diag_sel        => diag_sel,
    diag_frame_len  => diag_frame_len,
    diag_sof_period => diag_sof_period,
    diag_frame_cnt  => diag_frame_cnt,

    -- ST output
    out_ready       => seq_req,
    out_dat         => seq_dat,
    out_val         => seq_val,
    out_sop         => seq_sop,
    out_eop         => seq_eop,
    out_empty       => seq_empty
  );

  prev_seq_dat <= seq_dat when rising_edge(clk) and seq_val = '1';

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if diag_en = '0' and seq_val = '0' then
        init_dat <= (others => '0');
      elsif seq_eop = '1' then
        init_dat <= INCR_UVEC(init_dat, 1);
      end if;

      if seq_sop = '1' then assert seq_val = '1' and unsigned(seq_dat) = unsigned(init_dat)               report "Unexpected seq_sop";  end if;
      if seq_eop = '1' then assert seq_val = '1' and unsigned(seq_dat) = unsigned(init_dat) + c_frame_len - 1 report "Unexpected seq_eop";  end if;

      if seq_val = '1' and unsigned(seq_dat) = unsigned(init_dat)               then assert seq_sop = '1' report "Missing seq_sop";  end if;
      if seq_val = '1' and unsigned(seq_dat) = unsigned(init_dat) + c_frame_len - 1 then assert seq_eop = '1' report "Missing seq_eop";  end if;

      if seq_sop = '1'                                          then assert unsigned(seq_dat) = unsigned(init_dat)       report "Wrong first seq_dat"; end if;
      if seq_val = '1' and unsigned(seq_dat) > unsigned(init_dat) then assert unsigned(prev_seq_dat) = unsigned(seq_dat) - 1 report "Wrong seq_dat";       end if;
    end if;
  end process;
end tb;
