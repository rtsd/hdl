README.txt for $HDL_WORK/libraries/technology/ip_agi027_1e1v/complex_mult

1) Porting
2) IP component
3) Compilation, simulation and verification
4) Synthesis
5) Remarks


1) Porting

The complex_mult IP was ported manually from Quartus v19.4 for Arria10_e2sg to Quartus 23.2 for Agi027_1e1v by creating it in Quartus (Qsys) using
the same parameter settings.


2) IP component

The generated IPs are not kept in git repository, only the ip source files:

  ip_agi027_1e1v_complex_mult.ip
  ip_agi027_1e1v_complex_mult_27b.ip

Therefore first the IP needs to be generated using:

  generate_ip_libs iwave
  

3) Compilation, simulation and verification

The generated IP also contains a msim_setup.tcl file that was used to manually create:

  compile_ip.tcl
  
This compile_ip.tcl is in the hdllib.cfg and gets compiled before the other code.


4) Synthesis

No synthesis trials were done, because this will implicitely be done when the IP is used in a design. The QIP file:

  ip_agi027_1e1v_complex_mult.qip
  ip_agi027_1e1v_complex_mult_27b.qip

is included in the hdllib.cfg and contains what is needed to synthesize the IP.


5) Remarks

a) Use generated IP specific library clause name and IP specific lib uses sim

  The generated ip_agi027_1e1v_<lib_name>.vhd uses an IP specific library name. Therefore the hdllib.cfg uses the IP
  specific library as library clause name and, in addition, uses lib uses sim to make it known:
  
    hdl_lib_name = ip_agi027_1e1v_<lib_name>
    hdl_library_clause_name = ip_agi027_1e1v_<lib_name>_<ip_specific>
    hdl_lib_uses_sim = ip_agi027_1e1v_<ip_specific>

b) When multiple IPs are generated, each utilizing the same IP function but with different settings, it results in the generation of the same 
   library name, containing a different .vhd file, as opposed to the previously used unique library names. This leads to issues. To address 
   this, shared libraries are combined within a single library with the IP-specific library name in the build directory when 'generate_ip_libs' 
   is used. Therefore, a directory is manually created in 'altera_libraries' with the IP-specific library name, containing three files: 
   'compile_ip.tcl', 'hdllib.cfg' and 'liborder'.
