-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Same function as avs_common_mm.vhd, but with the read latency set
--          to 2 instead of 1 in the hardware description TCL file.
-- Description:
--   The avs_common_mm_readlatency2_hw.tcl determines the read latency. This
--   component wraps the default avs_common_mm which has readlatency 1 in its
--   avs_common_mm_hw.tcl.

library IEEE;
use IEEE.std_logic_1164.all;

entity avs_common_mm_readlatency2 is
  generic (
    g_adr_w     : natural := 5;
    g_dat_w     : natural := 32
  );
  port (
    -- MM side
    csi_system_reset       : in  std_logic;
    csi_system_clk         : in  std_logic;

    avs_mem_address        : in  std_logic_vector(g_adr_w - 1 downto 0);
    avs_mem_write          : in  std_logic;
    avs_mem_writedata      : in  std_logic_vector(g_dat_w - 1 downto 0);
    avs_mem_read           : in  std_logic;
    avs_mem_readdata       : out std_logic_vector(g_dat_w - 1 downto 0);

    -- User side
    coe_reset_export       : out std_logic;
    coe_clk_export         : out std_logic;

    coe_address_export     : out std_logic_vector(g_adr_w - 1 downto 0);
    coe_write_export       : out std_logic;
    coe_writedata_export   : out std_logic_vector(g_dat_w - 1 downto 0);
    coe_read_export        : out std_logic;
    coe_readdata_export    : in  std_logic_vector(g_dat_w - 1 downto 0)
  );
end avs_common_mm_readlatency2;

architecture wrap of avs_common_mm_readlatency2 is
begin
  -- wires
  coe_reset_export     <= csi_system_reset;
  coe_clk_export       <= csi_system_clk;

  coe_address_export   <= avs_mem_address;
  coe_write_export     <= avs_mem_write;
  coe_writedata_export <= avs_mem_writedata;
  coe_read_export      <= avs_mem_read;
  avs_mem_readdata     <= coe_readdata_export;
end wrap;
