-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common_pkg.all;

-- Purpose: Interleave g_nof_in inputs into one output stream based on g_block_size.
-- Description:
--   The input streams are concatenated into one SLV. The incoming streams are
--   sequentially multiplexed onto the output, starting from input 0. During
--   multiplexing of the current input, the other input(s) are buffered and
--   will be put on the output after g_block_size valid words on the current
--   input.
-- Remarks:
-- . One valid input applies to all input data streams;
-- . The user must take care of the correct valid/gap ratio on the inputs.

entity common_interleave is
  generic (
    g_nof_in     : natural;  -- >= 2
    g_dat_w      : natural;
    g_block_size : natural
 );
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;

    in_dat      : in  std_logic_vector(g_nof_in * g_dat_w - 1 downto 0);
    in_val      : in  std_logic;

    out_dat     : out std_logic_vector(g_dat_w - 1 downto 0);
    out_val     : out std_logic
  );
end;

architecture rtl of common_interleave is
  type t_dat_arr is array (integer range <>) of std_logic_vector(g_dat_w - 1 downto 0);
  type t_val_arr is array (integer range <>) of std_logic;

  -----------------------------------------------------------------------------
  -- Array of block register inputs
  -----------------------------------------------------------------------------
  signal bkr_in_dat_arr : t_dat_arr(g_nof_in - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Array of block register outputs
  -----------------------------------------------------------------------------
  signal bkr_out_dat_arr : t_dat_arr(g_nof_in - 1 downto 0);
  signal bkr_out_val_arr : std_logic_vector(g_nof_in - 1 downto 0);

  signal piped_bkr_out_dat_arr : t_dat_arr(g_nof_in - 1 downto 0);
  signal piped_bkr_out_val_arr : std_logic_vector(g_nof_in - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Array of multiplexer inputs
  -----------------------------------------------------------------------------
  signal mux_in_dat_arr : t_dat_arr(g_nof_in - 1 downto 0);
  signal nxt_mux_in_dat_arr : t_dat_arr(g_nof_in - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Concatenated multiplexer inputs
  -----------------------------------------------------------------------------
  signal mux_in_concat_dat_arr : std_logic_vector(g_nof_in * g_dat_w - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Multiplexer input selection control
  -----------------------------------------------------------------------------
  constant c_mux_in_sel_w : natural := ceil_log2(g_nof_in);

  signal mux_in_sel       : std_logic_vector(c_mux_in_sel_w - 1 downto 0);
  signal nxt_mux_in_sel   : std_logic_vector(c_mux_in_sel_w - 1 downto 0);
  signal sch_mux_in_sel   : std_logic_vector(c_mux_in_sel_w - 1 downto 0);

  constant c_mux_val_cnt_w  : natural := ceil_log2(g_block_size+1);
  signal mux_val_cnt        : std_logic_vector(c_mux_val_cnt_w - 1 downto 0);
  signal nxt_mux_val_cnt    : std_logic_vector(c_mux_val_cnt_w - 1 downto 0);

  signal mux_in_val       : std_logic;
  signal nxt_mux_in_val   : std_logic;
begin
  -----------------------------------------------------------------------------
  -- Wire SLV -> Array
  -----------------------------------------------------------------------------
  gen_wire_in_dat: for i in 0 to g_nof_in - 1 generate
    bkr_in_dat_arr(i) <= in_dat( i * g_dat_w + g_dat_w - 1 downto i * g_dat_w);
  end generate;

  -----------------------------------------------------------------------------
  -- Block registers output bursts of g_block_size data words at a time. To
  -- offset these blocks  in time with respect to each other the outputs are
  -- incrementally pipelined.
  -----------------------------------------------------------------------------
  gen_blockregs: for i in 0 to g_nof_in - 1 generate
    u_blockreg : entity work.common_blockreg
    generic map (
      g_block_size => g_block_size,
      g_dat_w     => g_dat_w
    )
    port map (
      rst          => rst,
      clk          => clk,

      in_dat       => bkr_in_dat_arr(i),
      in_val       => in_val,

      out_dat      => bkr_out_dat_arr(i),
      out_val      => bkr_out_val_arr(i)
      );

    u_dat_block_offset_pipe : entity work.common_pipeline
    generic map (
      g_pipeline  => i * g_block_size,
      g_in_dat_w  => g_dat_w,
      g_out_dat_w => g_dat_w
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => bkr_out_dat_arr(i),
      out_dat => piped_bkr_out_dat_arr(i)
    );

    u_val_block_offset_pipe : entity work.common_pipeline
    generic map (
      g_pipeline  => i * g_block_size,
      g_in_dat_w  => 1,
      g_out_dat_w => 1
    )
    port map (
      rst     => rst,
      clk     => clk,
      in_dat  => slv(bkr_out_val_arr(i)),
      sl(out_dat) => piped_bkr_out_val_arr(i)
    );
  end generate;

  -----------------------------------------------------------------------------
  -- Array -> concatenated SLV conversion
  -----------------------------------------------------------------------------
  gen_arr_to_concat : for i in 0 to g_nof_in - 1 generate
    mux_in_concat_dat_arr(i * g_dat_w + g_dat_w - 1 downto i * g_dat_w) <= mux_in_dat_arr(i);
  end generate;

  -----------------------------------------------------------------------------
  -- The multiplexer
  -----------------------------------------------------------------------------
  u_mux : entity work.common_multiplexer
  generic map (
    g_nof_in => g_nof_in,
    g_dat_w  => g_dat_w
  )
  port map (
    rst        => rst,
    clk        => clk,

    in_sel     => mux_in_sel,
    in_dat     => mux_in_concat_dat_arr,
    in_val     => mux_in_val,

    out_dat    => out_dat,
    out_val    => out_val
  );

  -----------------------------------------------------------------------------
  -- Multiplexer input selection
  -----------------------------------------------------------------------------
  -- Scheduled input
  sch_mux_in_sel <= INCR_UVEC(mux_in_sel, 1) when unsigned(mux_in_sel) < g_nof_in - 1 else (others => '0');

  p_nxt_mux_in_sel : process(mux_in_sel, mux_val_cnt, sch_mux_in_sel, piped_bkr_out_val_arr, piped_bkr_out_dat_arr)
  begin
    nxt_mux_in_sel     <= mux_in_sel;
    nxt_mux_val_cnt    <= mux_val_cnt;

    nxt_mux_in_dat_arr <= piped_bkr_out_dat_arr;
    nxt_mux_in_val     <= orv(piped_bkr_out_val_arr);

    if orv(piped_bkr_out_val_arr) = '1' then
      if unsigned(mux_val_cnt) = g_block_size then
        nxt_mux_val_cnt <= TO_UVEC(1, c_mux_val_cnt_w);
        nxt_mux_in_sel  <= sch_mux_in_sel;
      else
        nxt_mux_val_cnt <= INCR_UVEC(mux_val_cnt, 1);
      end if;
    else
      if unsigned(mux_val_cnt) = g_block_size then
        nxt_mux_val_cnt <= TO_UVEC(0, c_mux_val_cnt_w);
        nxt_mux_in_sel  <= sch_mux_in_sel;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------
  p_reg : process(rst, clk)
  begin
    if rst = '1' then
      mux_in_sel     <= (others => '0');
      mux_in_val     <= '0';
      mux_in_dat_arr  <= (others => (others => '0'));
      mux_val_cnt    <= (others => '0');
    elsif rising_edge(clk) then
      mux_in_sel     <= nxt_mux_in_sel;
      mux_in_val     <= nxt_mux_in_val;
      mux_in_dat_arr <= nxt_mux_in_dat_arr;
      mux_val_cnt    <= nxt_mux_val_cnt;
    end if;
  end process;
end rtl;
