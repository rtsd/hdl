--------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose:  Component declarations for jesd204b ip blocks

library IEEE, technology_lib, common_lib, dp_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use technology_lib.technology_pkg.all;
use dp_lib.dp_stream_pkg.all;

package ip_arria10_e2sg_jesd204b_component_pkg is
  ------------------------------------------------------------------------------
  -- Main IP, TX ONLY, 1 channel
  ------------------------------------------------------------------------------

    component ip_arria10_e2sg_jesd204b_tx is
    port (
      csr_cf                     : out std_logic_vector(4 downto 0);  -- export
      csr_cs                     : out std_logic_vector(1 downto 0);  -- export
      csr_f                      : out std_logic_vector(7 downto 0);  -- export
      csr_hd                     : out std_logic;  -- export
      csr_k                      : out std_logic_vector(4 downto 0);  -- export
      csr_l                      : out std_logic_vector(4 downto 0);  -- export
      csr_lane_powerdown         : out std_logic_vector(0 downto 0);  -- export
      csr_m                      : out std_logic_vector(7 downto 0);  -- export
      csr_n                      : out std_logic_vector(4 downto 0);  -- export
      csr_np                     : out std_logic_vector(4 downto 0);  -- export
      csr_s                      : out std_logic_vector(4 downto 0);  -- export
      csr_tx_testmode            : out std_logic_vector(3 downto 0);  -- export
      csr_tx_testpattern_a       : out std_logic_vector(31 downto 0);  -- export
      csr_tx_testpattern_b       : out std_logic_vector(31 downto 0);  -- export
      csr_tx_testpattern_c       : out std_logic_vector(31 downto 0);  -- export
      csr_tx_testpattern_d       : out std_logic_vector(31 downto 0);  -- export
      dev_sync_n                 : out std_logic;  -- export
      jesd204_tx_avs_chipselect  : in  std_logic                     := 'X';  -- chipselect
      jesd204_tx_avs_address     : in  std_logic_vector(7 downto 0)  := (others => 'X');  -- address
      jesd204_tx_avs_read        : in  std_logic                     := 'X';  -- read
      jesd204_tx_avs_readdata    : out std_logic_vector(31 downto 0);  -- readdata
      jesd204_tx_avs_waitrequest : out std_logic;  -- waitrequest
      jesd204_tx_avs_write       : in  std_logic                     := 'X';  -- write
      jesd204_tx_avs_writedata   : in  std_logic_vector(31 downto 0) := (others => 'X');  -- writedata
      jesd204_tx_avs_clk         : in  std_logic                     := 'X';  -- clk
      jesd204_tx_avs_rst_n       : in  std_logic                     := 'X';  -- reset_n
      jesd204_tx_dlb_data        : out std_logic_vector(31 downto 0);  -- export
      jesd204_tx_dlb_kchar_data  : out std_logic_vector(3 downto 0);  -- export
      jesd204_tx_frame_error     : in  std_logic                     := 'X';  -- export
      jesd204_tx_frame_ready     : out std_logic;  -- export
      jesd204_tx_int             : out std_logic;  -- irq
      jesd204_tx_link_data       : in  std_logic_vector(31 downto 0) := (others => 'X');  -- data
      jesd204_tx_link_valid      : in  std_logic                     := 'X';  -- valid
      jesd204_tx_link_ready      : out std_logic;  -- ready
      mdev_sync_n                : in  std_logic                     := 'X';  -- export
      pll_locked                 : in  std_logic_vector(0 downto 0)  := (others => 'X');  -- pll_locked
      somf                       : out std_logic_vector(3 downto 0);  -- export
      sync_n                     : in  std_logic                     := 'X';  -- export
      sysref                     : in  std_logic                     := 'X';  -- export
      tx_analogreset             : in  std_logic_vector(0 downto 0)  := (others => 'X');  -- tx_analogreset
      tx_bonding_clocks          : in  std_logic_vector(5 downto 0)  := (others => 'X');  -- clk
      tx_cal_busy                : out std_logic_vector(0 downto 0);  -- tx_cal_busy
      tx_digitalreset            : in  std_logic_vector(0 downto 0)  := (others => 'X');  -- tx_digitalreset
      tx_serial_data             : out std_logic_vector(0 downto 0);  -- tx_serial_data
      txlink_clk                 : in  std_logic                     := 'X';  -- clk
      txlink_rst_n_reset_n       : in  std_logic                     := 'X';  -- reset_n
      txphy_clk                  : out std_logic_vector(0 downto 0)  -- export
    );
    end component ip_arria10_e2sg_jesd204b_tx;
end ip_arria10_e2sg_jesd204b_component_pkg;

package body ip_arria10_e2sg_jesd204b_component_pkg is
end ip_arria10_e2sg_jesd204b_component_pkg;
