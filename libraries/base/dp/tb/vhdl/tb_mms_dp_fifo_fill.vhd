-------------------------------------------------------------------------------
--
-- Copyright (C) 2010
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose:
--   Test bench for mms_dp_fifo_fill
-- Description:
--   The tb creates a data stream and checks if the register can be readline
--   The value that is read via MM should be checked in the wave window.
--
-- Remark:
--   It is not possible to check if the wr_full bit is set. That is because of the
--   assert in de common_fifo_sc definition.
--
-- Usage:
-- > as 10
-- > run -all
-- . signal tb_end will stop the simulation by stopping the clk
--
library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_mms_dp_fifo_fill is
  generic (
    -- Try FIFO settings
    g_nof_streams     : positive := 3;
    g_dut_use_bsn     : boolean  := false;
    g_dut_use_empty   : boolean  := false;
    g_dut_use_channel : boolean  := false;
    g_dut_use_sync    : boolean  := false;
    g_dut_fifo_rl     : natural  := 1;  -- internal RL,  use 0 for look ahead FIFO, default 1 for normal FIFO
    g_dut_fifo_size   : natural  := 64;
    g_dut_fifo_fill   : natural  := 40  -- selectable >= 0 for dp_fifo_fill
  );
end tb_mms_dp_fifo_fill;

architecture tb of tb_mms_dp_fifo_fill is
  constant c_nof_regs_per_stream       : natural := 3;
  constant c_reg_used_words_offset     : natural := 0;
  constant c_reg_fifo_flags            : natural := 1;
  constant c_reg_max_used_words_offset : natural := 2;

  ----------------------------------------------------------------------------
  -- Clocks and resets
  ----------------------------------------------------------------------------
  constant c_mm_clk_period     : time := 20 ns;
  constant c_dp_clk_period     : time := 5 ns;

  signal mm_rst                : std_logic := '1';
  signal mm_clk                : std_logic := '0';

  signal dp_rst                : std_logic;
  signal dp_clk                : std_logic := '0';

  signal tb_end                : std_logic := '0';

  signal sim_used_words        : std_logic := '0';
  signal sim_rd_empty          : std_logic := '0';
  signal sim_wr_full           : std_logic := '0';

  signal reg_diag_bg_mosi      : t_mem_mosi;
  signal reg_diag_bg_miso      : t_mem_miso;
  signal ram_diag_bg_mosi      : t_mem_mosi;
  signal ram_diag_bg_miso      : t_mem_miso;
  signal reg_dp_fifo_fill_mosi : t_mem_mosi := c_mem_mosi_rst;
  signal reg_dp_fifo_fill_miso : t_mem_miso := c_mem_miso_rst;

  signal in_sosi               : t_dp_sosi  := c_dp_sosi_rst;
  signal in_sosi_arr           : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal out_siso_arr          : t_dp_siso_arr(g_nof_streams - 1 downto 0);
  signal out_sosi_arr          : t_dp_sosi_arr(g_nof_streams - 1 downto 0);
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 5;

  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 5;

  p_stimuli : process
  begin
    proc_common_wait_some_cycles(dp_clk, 10);
    -- Generate a datastream.
    while true loop
      in_sosi.valid <= '1';
      in_sosi.sop <= '1';
      proc_common_wait_some_cycles(dp_clk, 1);
      in_sosi.sop <= '0';
      proc_common_wait_some_cycles(dp_clk, 63);
      in_sosi.eop <= '1';
      proc_common_wait_some_cycles(dp_clk, 1);
      in_sosi.eop <= '0';
      in_sosi.valid <= '0';
      proc_common_wait_some_cycles(dp_clk, 1);
      while (sim_used_words = '0' and sim_wr_full = '0') loop
        proc_common_wait_some_cycles(dp_clk, 1);
      end loop;
    end loop;
    wait;  -- add void WAIT statement to avoid (vcom-1090) Possible infinite loop: Process contains no WAIT statement.
  end process;

  gen_connect : for I in 0 to g_nof_streams - 1 generate
    in_sosi_arr(I)  <= in_sosi;
    out_siso_arr(I) <= c_dp_siso_rdy when sim_wr_full = '0' else c_dp_siso_rst;
  end generate;

  p_special_stimuli : process
  begin
    -- Read out the used_w register
    sim_used_words <= '1';
    proc_common_wait_some_cycles(mm_clk, 200);
    for I in 0 to g_nof_streams - 1 loop
      proc_mem_mm_bus_rd(I * c_nof_regs_per_stream + c_reg_used_words_offset, mm_clk, reg_dp_fifo_fill_mosi);
    end loop;
    sim_used_words <= '0';

    -- Read maximum used fifo words register
    sim_rd_empty <= '1';
    proc_common_wait_some_cycles(mm_clk, 30);
    for I in 0 to g_nof_streams - 1 loop
      proc_mem_mm_bus_rd(I * c_nof_regs_per_stream + c_reg_max_used_words_offset, mm_clk, reg_dp_fifo_fill_mosi);
    end loop;

--    proc_mem_mm_bus_rd(2, mm_clk, reg_dp_fifo_fill_mosi);

    -- Read out the rd_empty bit
    proc_common_wait_some_cycles(mm_clk, 100);
--    proc_mem_mm_bus_rd(1, mm_clk, reg_dp_fifo_fill_mosi);
    for I in 0 to g_nof_streams - 1 loop
      proc_mem_mm_bus_rd(I * c_nof_regs_per_stream + c_reg_fifo_flags, mm_clk, reg_dp_fifo_fill_mosi);
    end loop;
    sim_rd_empty <= '0';

    -- Read out the wr_full bit
    sim_wr_full <= '1';
    proc_common_wait_some_cycles(mm_clk, 100);
    for I in 0 to g_nof_streams - 1 loop
      proc_mem_mm_bus_rd(I * c_nof_regs_per_stream + c_reg_fifo_flags, mm_clk, reg_dp_fifo_fill_mosi);
    end loop;
--    proc_mem_mm_bus_rd(1, mm_clk, reg_dp_fifo_fill_mosi);

    proc_common_wait_some_cycles(mm_clk, 10);
    tb_end <= '1';
    wait;
  end process;

  dut : entity work.mms_dp_fifo_fill
  generic map (
    g_nof_streams        => g_nof_streams,
    g_cross_clock_domain => true,
    g_data_w             => c_dp_data_w,
    g_bsn_w              => c_dp_bsn_w,
    g_empty_w            => c_dp_empty_w,
    g_channel_w          => c_dp_channel_w,
    g_error_w            => 1,
    g_use_bsn            => g_dut_use_bsn,
    g_use_empty          => g_dut_use_empty,
    g_use_channel        => g_dut_use_channel,
    g_use_error          => false,
    g_use_sync           => g_dut_use_sync,
    g_use_complex        => true,
    g_fifo_fill          => g_dut_fifo_fill,
    g_fifo_size          => g_dut_fifo_size,
    g_fifo_af_margin     => 4,
    g_fifo_rl            => g_dut_fifo_rl
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    reg_mosi    => reg_dp_fifo_fill_mosi,
    reg_miso    => reg_dp_fifo_fill_miso,

    -- Streaming clock domain
    dp_rst      => dp_rst,
    dp_clk      => dp_clk,

    -- ST sink
    snk_out_arr => OPEN,
    snk_in_arr  => in_sosi_arr,
    -- ST source
    src_in_arr  => out_siso_arr,
    src_out_arr => open
  );
end tb;
