--------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use work.ddr3_pkg.all;

entity ddr3_driver is
  generic (
    g_wr_fifo_depth    : natural := 128;
    g_ddr              : t_c_ddr3_phy
   );
  port (
    clk                : in  std_logic;
    rst                : in  std_logic;

    ctlr_rdy           : in  std_logic;
    ctlr_init_done     : in  std_logic;
    ctlr_burst         : out std_logic;
    ctlr_burst_size    : out std_logic_vector(c_ddr3_ctlr_maxburstsize_w - 1 downto 0);
    ctlr_wr_req        : out std_logic;
    ctlr_rd_req        : out std_logic;

    dvr_en             : in  std_logic := '1';
    dvr_wr_not_rd      : in  std_logic;
    dvr_done           : out std_logic;  -- Requested wr or rd sequence is done.

    wr_val             : in  std_logic;
    wr_rdy             : out std_logic;
    rd_rdy             : in  std_logic;

    cur_addr           : out t_ddr3_addr;
    start_addr         : in  t_ddr3_addr;
    end_addr           : in  t_ddr3_addr;

    wr_fifo_usedw      : in  std_logic_vector
   );
end ddr3_driver;

architecture str of ddr3_driver is
  constant c_chip_addr_w        : natural := ceil_log2(g_ddr.cs_w);  -- Chip sel lines converted to logical address
  constant c_address_w          : natural := c_chip_addr_w + g_ddr.ba_w + g_ddr.a_w + g_ddr.a_col_w + 1;  -- 1 bit added to detect overflow

  constant c_margin             : natural := 2;  -- wr_burst_size is updated one cycle after reading actual nof available words.
                                                -- Subtract two (wr_fifo_usedw and wr_burst_size are both registered) so we cannot
                                                -- post a request for a too large burst size, which could cause the wr_burst state
                                                -- to be two valid words short.

  signal req_burst_cycles       : std_logic_vector(c_ddr3_ctlr_maxburstsize_w - 1 downto 0);
  signal nxt_req_burst_cycles   : std_logic_vector(c_ddr3_ctlr_maxburstsize_w - 1 downto 0);

  type t_state_enum is (s_init, s_idle, s_wait1, s_wait2, s_wait3, s_rd_request, s_wr_request, s_wr_burst);

  signal state                  : t_state_enum;
  signal nxt_state              : t_state_enum;
  signal prev_state             : t_state_enum;

  signal wr_burst_size          : natural;
  signal rd_burst_size          : natural;

  signal nxt_wr_burst_size      : natural;
  signal nxt_rd_burst_size      : natural;

  signal i_ctlr_burst_size      : std_logic_vector(c_ddr3_ctlr_maxburstsize_w - 1 downto 0);

  signal i_dvr_done             : std_logic;
  signal nxt_dvr_done           : std_logic;

  signal start_address          : std_logic_vector(c_address_w - 1 downto 0);
  signal end_address            : std_logic_vector(c_address_w - 1 downto 0);

  signal cur_address            : std_logic_vector(c_address_w - 1 downto 0);
  signal nxt_cur_address        : std_logic_vector(c_address_w - 1 downto 0);
  signal diff_address           : std_logic_vector(c_address_w - 1 downto 0);

  signal addresses_rem          : std_logic_vector(31 downto 0);  -- nof words (on the user side interface) to rd/wr until end addr is reached
  signal reg_addresses_rem      : std_logic_vector(31 downto 0);  -- nof words (on the user side interface) to rd/wr until end addr is reached

  signal reg_wr_fifo_usedw      : std_logic_vector(ceil_log2(g_wr_fifo_depth) - 1 downto 0);  -- read side depth of the write FIFO
begin
  ctlr_burst_size <= i_ctlr_burst_size;
  dvr_done        <= i_dvr_done;

  p_clk : process(rst, clk)
  begin
    if rst = '1' then
      state                <= s_init;
      req_burst_cycles     <= (others => '0');
      i_dvr_done           <= '0';
      cur_address          <= (others => '0');
      wr_burst_size        <= 0;
      rd_burst_size        <= 0;
      reg_addresses_rem    <= (others => '0');
      reg_wr_fifo_usedw    <= (others => '0');
      prev_state           <= s_idle;
    elsif rising_edge(clk) then
      state                <= nxt_state;
      req_burst_cycles     <= nxt_req_burst_cycles;
      i_dvr_done           <= nxt_dvr_done;
      cur_address          <= nxt_cur_address;
      wr_burst_size        <= nxt_wr_burst_size;
      rd_burst_size        <= nxt_rd_burst_size;
      reg_addresses_rem    <= addresses_rem;
      reg_wr_fifo_usedw    <= wr_fifo_usedw;
      prev_state           <= state;
    end if;
  end process;

  -- Add 1 address (accounting for address resulotion) to diff_address: we also want to write the last address. Shift the result right to provide the correct resolution.
  addresses_rem <= RESIZE_UVEC( SHIFT_UVEC( INCR_UVEC(diff_address, c_ddr3_ctlr_rsl), c_ddr3_ctlr_rsl_w), addresses_rem'length);

  -- End address - current address
  diff_address  <= SUB_UVEC(end_address, cur_address, c_address_w);

  p_burst_size : process (reg_addresses_rem, reg_wr_fifo_usedw)
    variable v_burst_size : natural;
  begin
    -- Write burst size is smallest of c_ddr3_ctlr_maxburstsize, addresses_rem and wr_fifo_usedw
    v_burst_size := c_ddr3_ctlr_maxburstsize + c_margin;
    if unsigned(reg_wr_fifo_usedw) >= c_margin and unsigned(reg_addresses_rem) >= c_margin then
      if v_burst_size > signed('0' & reg_addresses_rem) then v_burst_size := TO_UINT(reg_addresses_rem); end if;
      if v_burst_size > signed('0' & reg_wr_fifo_usedw) then v_burst_size := TO_UINT(reg_wr_fifo_usedw); end if;
      v_burst_size := v_burst_size - c_margin;
    else
      v_burst_size := 0;
    end if;
    nxt_wr_burst_size <= v_burst_size;

    -- Read burst size is smallest of c_ddr3_ctlr_maxburstsize and addresses_rem
    v_burst_size := c_ddr3_ctlr_maxburstsize;
    if unsigned(reg_addresses_rem) >= 1 then  -- prevent assigning <0 value to natural
      if v_burst_size > signed('0' & reg_addresses_rem) then v_burst_size := TO_UINT(reg_addresses_rem); end if;
    else
      v_burst_size := 0;
    end if;
    nxt_rd_burst_size <= v_burst_size;
  end process;

  p_state : process(prev_state, state, i_dvr_done, ctlr_rdy, req_burst_cycles, dvr_wr_not_rd, wr_val, wr_fifo_usedw, wr_burst_size, rd_burst_size, dvr_en, ctlr_init_done, reg_addresses_rem, rd_rdy, i_ctlr_burst_size, start_address, cur_address)
  begin
    nxt_state              <= state;
    ctlr_wr_req            <= '0';
    ctlr_rd_req            <= '0';
    ctlr_burst             <= '0';
    i_ctlr_burst_size      <= (others => '0');
    nxt_req_burst_cycles   <= req_burst_cycles;
    wr_rdy                 <= '0';
    nxt_dvr_done           <= i_dvr_done;
    nxt_cur_address        <= cur_address;

    case state is

      when s_wr_burst =>  -- Performs the burst portion (word 2+)
        ctlr_wr_req <= '1';
        if ctlr_rdy = '1' then  -- when local_ready goes low, that cycle does not count as a burst cycle
          nxt_req_burst_cycles <= INCR_UVEC(req_burst_cycles, -1);
          wr_rdy               <= '1';  -- wr side uses latency of 0, so wr_rdy<='1' acknowledges a successful write request.
          if unsigned(req_burst_cycles) = 1 then  -- Then we're in the last cycle of this burst sequence
            nxt_state <= s_wr_request;  -- We can only initiate a burst through the wr_request state
          end if;
        end if;

      when s_wr_request =>  -- Performs 1 write or read and goes into s_wr_burst when requested write words >1
        nxt_state <= s_wait3;
        if unsigned(reg_addresses_rem) = 0 then  -- end address reached
          nxt_dvr_done  <= '1';
          nxt_state     <= s_idle;
        elsif ctlr_rdy = '1' then
          if wr_val = '1' then
            -- Always perform 1st write here
            ctlr_burst        <= '1';  -- assert burst begin: strictly this is a burst of 1.
            ctlr_wr_req       <= '1';
            wr_rdy            <= '1';
            i_ctlr_burst_size <= TO_UVEC(1, c_ddr3_ctlr_maxburstsize_w);  -- Set ctlr_burst_size to 1 by default
            if wr_burst_size > 1 then
              -- Perform any remaining writes in a burst
              nxt_state            <= s_wr_burst;
              nxt_req_burst_cycles <= TO_UVEC(wr_burst_size-1, c_ddr3_ctlr_maxburstsize_w);  -- Forward the required nof burst cycles (-1 as we've done the 1st in this state already) to burst state
              i_ctlr_burst_size    <= TO_UVEC(wr_burst_size,   c_ddr3_ctlr_maxburstsize_w);
            end if;  -- ELSE: there is only 1 word, so no need for remaining burst
            nxt_cur_address   <= INCR_UVEC(cur_address, unsigned(i_ctlr_burst_size) * c_ddr3_ctlr_rsl);
--            IF UNSIGNED(i_ctlr_burst_size) = 1 THEN -- Prevents FSM from going into this state again too soon (reg_addresses_rem invalid)
--            nxt_state <= s_wait3;
--            END IF;
          end if;
        end if;

      when s_rd_request =>  -- Posts a read request for a burst (0...c_ddr3_ctlr_maxburstsize)
        nxt_state <= s_wait3;
        if unsigned(reg_addresses_rem) = 0 then  -- end address reached
          nxt_dvr_done  <= '1';
          nxt_state     <= s_idle;
        else
          if rd_rdy = '1' then  -- Fifo uses its internal almost_full signal to toggle its snk_out.rdy
            if ctlr_rdy = '1' then
              ctlr_rd_req       <= '1';
              ctlr_burst        <= '1';  -- assert burst begin: strictly this is a burst of 1.
              i_ctlr_burst_size <= TO_UVEC(rd_burst_size, c_ddr3_ctlr_maxburstsize_w);
              if rd_burst_size = 0 then i_ctlr_burst_size <= TO_UVEC(1, c_ddr3_ctlr_maxburstsize_w); end if;
              nxt_cur_address   <= INCR_UVEC(cur_address, unsigned(i_ctlr_burst_size) * c_ddr3_ctlr_rsl);
--              IF UNSIGNED(i_ctlr_burst_size) = 1 THEN -- Prevents FSM from going into this state again too soon (reg_addresses_rem invalid)
--              nxt_state <= s_wait3;
--              END IF;
            end if;
          end if;
        end if;

      -- This wait state is inserted between two requests when necessary, e.g. when FSM enters wr_request
      -- from the state wr_request, an extra cycle is needed for reg_addresses_rem to be valid.
      when s_wait3 =>
        if prev_state = s_wr_request then nxt_state <= s_wr_request; end if;
        if prev_state = s_rd_request then nxt_state <= s_rd_request; end if;

     -- In this cycle reg_addresses_rem is valid. This cycle is added so wr_burst_size and rd_burst_size
     -- (derived from reg_addresses_rem) are valid the next cycle.
     when s_wait2 =>
       if dvr_wr_not_rd = '1' then
         nxt_state <= s_wr_request;
       else
         nxt_state <= s_rd_request;
       end if;

      -- Wait a cycle so reg_addresses_rem is valid the next cyle.
      when s_wait1 =>
        nxt_state <= s_wait2;

      when s_idle =>
        if dvr_en = '1' then
          nxt_cur_address <= start_address;
          nxt_dvr_done    <= '0';
          nxt_state       <= s_wait1;
        end if;

      when others =>  -- s_init
        if ctlr_init_done = '1' then
          nxt_state <= s_idle;
        end if;
    end case;
  end process;
  end_address   <= RESIZE_UVEC(  end_addr.chip &  end_addr.bank  &   end_addr.row(g_ddr.a_w - 1 downto 0) &   end_addr.column(g_ddr.a_col_w - 1 downto 0), c_address_w);
  start_address <= RESIZE_UVEC(start_addr.chip & start_addr.bank & start_addr.row(g_ddr.a_w - 1 downto 0) & start_addr.column(g_ddr.a_col_w - 1 downto 0), c_address_w);

  cur_addr.chip(  c_chip_addr_w     - 1 downto 0) <= cur_address(c_chip_addr_w + g_ddr.ba_w + g_ddr.a_w + g_ddr.a_col_w - 1 downto g_ddr.ba_w + g_ddr.a_w + g_ddr.a_col_w);
  cur_addr.bank(  g_ddr.ba_w        - 1 downto 0) <= cur_address(                g_ddr.ba_w + g_ddr.a_w + g_ddr.a_col_w - 1 downto              g_ddr.a_w + g_ddr.a_col_w);
  cur_addr.row(   g_ddr.a_w         - 1 downto 0) <= cur_address(                             g_ddr.a_w + g_ddr.a_col_w - 1 downto                          g_ddr.a_col_w);
  cur_addr.column(g_ddr.a_col_w     - 1 downto 0) <= cur_address(                                         g_ddr.a_col_w - 1 downto                                      0);
end str;
