-------------------------------------------------------------------------------
--
-- Copyright (C) 2014
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Measure the phase between two clocks
-- Description:
--   The in_clk is sampled by clk. One clk is sufficient to detect the phase
--   of the in_clk. E.g. for in_clk = 2x clk:
--
--            phase  0    180   360
--                   . 90  . 270 .
--                   .  .  .  .  .
--                   .  .  .  .  .
--               _   .__.  .__.  .__    __    __    __
--   in_clk  :    |__|  |__|  |__|  |__|  |__|  |__|
--                   ._____       _____       _____
--   clk     :   ____|     |_____|     |_____|     |__
--
--   Measure in_clk at rising edge of clk or falling edge dependent on
--   g_rising_edge. When in_clk = 2x clk then only half of the in_clk cycles
--   are sampled which is sufficient:
--
--                   ._____       _____       __     in_phs
--   clk   0- 90: ___|     |_____|     |_____|    --> 1
--                   .   _____       _____
--   clk  90-180: |_____|     |_____|     |_____  --> 0
--                ___.      _____       _____
--   clk 180-270:    |_____|     |_____|     |__  --> 1
--                 _____       _____       _____
--   clk 270-360: |  .  |_____|     |_____|       --> 0
--
--
--   Define phase as the value measured by clk. The phase is detected
--   when it is not changing so in_phs = in_phs_dly(0) (is previous in_phs).
--
--   The clock phase detection works when in_clk = M x clk, because then M-1
--   of the in_clk cycles are skipped. When clk = N x in_clk then the in_clk
--   high and low are detected N times, which needs to be accounted for in
--   the determining of phs_evt. E.g. for clk = 2x in_clk:
--
--               phase  0    180   360
--                      . 90  . 270 .
--                      .  .  .  .  .
--                      .  .  .  .  .
--                      ._____.  .  ._____       _____
--   in_clk :       ____|  .  |_____|     |_____|     |__
--                  _   .__.  .__.  .__    __    __    __
--   clk    :        |__|  |__|  |__|  |__|  |__|  |__|
--
--                      ._____       _____       __     in_phs
--   in_clk   0- 90: ___|     |_____|     |_____|    --> 0 1
--                      .   _____       _____
--   in_clk  90-180: |_____|     |_____|     |_____  --> 0 1
--                   ___.      _____       _____
--   in_clk 180-270:    |_____|     |_____|     |__  --> 1 0
--                    _____       _____       _____
--   in_clk 270-360: |  .  |_____|     |_____|       --> 1 0
--
-- Remark:
-- . For in_clk faster than clk the detected phase = in_phs is constant. For
--   clk faster than in_clk so N>1 the detected phase toggles, because then
--   the clk is fast enough to follow the transitions of the in_clk. In both
--   cases the phase_det monitors whether there occurs an unexpected transition
--   event.
-- . The clock tree logic in the FPGA ensures that clk captures the input
--   in_clk independent of how it is layout. The short routing from in_clk
--   to the D-input of the flipflop in common_async will have a small data
--   path delay and therefore this data path delay will be nearly independent
--   of the size of the design in which it is synthesised. Therefore it is
--   not necessary or appropriate to put a small logic lock region of 1 ALM
--   on common_clock_phase_detector.
-- . The g_meta_delay_len is increased to be a multiple of c_period_len and
--   then +1 such that the clock phase relation is detected independent of
--   g_meta_delay_len. Default g_offset_delay_len=0, however e.g. by using
--   g_offset_delay_len=-1 it is possible to compensate for an external
--   pipeline stage.

library IEEE;
use IEEE.std_logic_1164.all;
use work.common_pkg.all;

entity common_clock_phase_detector is
  generic (
    g_rising_edge      : boolean := true;
    g_phase_rst_level  : std_logic := '0';
    g_meta_delay_len   : positive := c_meta_delay_len;
    g_offset_delay_len : integer  := 0;
    g_clk_factor       : positive := 1  -- = N
  );
  port (
    in_clk    : in  std_logic;  -- used as data input for clk domain
    rst       : in  std_logic := '0';
    clk       : in  std_logic;
    phase     : out std_logic;
    phase_det : out std_logic
  );
end common_clock_phase_detector;

architecture str of common_clock_phase_detector is
  constant c_period_len : natural := 2 * g_clk_factor;
  constant c_delay_len  : natural := ceil_div(g_meta_delay_len, c_period_len) * c_period_len + 1 + g_offset_delay_len;  -- detect clock phase relation independent of g_meta_delay_len

  signal dbg_g_meta_delay_len : natural := g_meta_delay_len;  -- to view it in the wave window
  signal dbg_c_period_len     : natural := c_period_len;  -- to view it in the wave window
  signal dbg_c_delay_len      : natural := c_delay_len;  -- to view it in the wave window

  signal in_phs_cap    : std_logic;
  signal in_phs        : std_logic;
  signal in_phs_dly    : std_logic_vector(g_clk_factor - 1 downto 0);
  signal phs_evt       : std_logic;
  signal nxt_phase_det : std_logic;
begin
  -- Capture the in_clk in the clk domain
  u_async : entity work.common_async
  generic map (
    g_rising_edge => g_rising_edge,
    g_rst_level   => g_phase_rst_level,
    g_delay_len   => c_delay_len
  )
  port map (
    rst  => rst,
    clk  => clk,
    din  => in_clk,
    dout => in_phs_cap
  );

  -- Process the registers in the rising edge clk domain
  gen_r_wire : if g_rising_edge = true generate
    in_phs <= in_phs_cap;
  end generate;

  gen_fr_reg : if g_rising_edge = false generate
    in_phs <= g_phase_rst_level when rst = '1' else in_phs_cap when rising_edge(clk);  -- get from f to r
  end generate;

  in_phs_dly(0)                       <= in_phs                              when rising_edge(clk);  -- when N=1 or M>1
  in_phs_dly(g_clk_factor - 1 downto 1) <= in_phs_dly(g_clk_factor - 2 downto 0) when rising_edge(clk);  -- when N>1

  phase_det <= '0' when rst = '1' else nxt_phase_det when rising_edge(clk);

  -- Combinatorial function
  phs_evt <= '1' when in_phs_dly(g_clk_factor - 1) /= in_phs else '0';

  nxt_phase_det <= not phs_evt;

  phase <= in_phs;
end str;
