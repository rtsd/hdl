---------------------------------------------------------------------------
Background
---------------------------------------------------------------------------
The Lattice ECP3 FPGA is used to implement a JTAG scanbrid, the interface between the JTAG Test Access Point on UNB2c and the FPGA's and Ethernet PHY. With dipswitches the bridge can be set in mode:
- Bridge mode, all FPGA's and Ethernet phy can be accessed using JTAG boundary scan software
- stitch mode, a selection of parts (like all 4 FPGA) can be stitched in one JTAG chain. This is used for most of our applications

FPGA: LCMXO2-640HC-5TG100C

---------------------------------------------------------------------------
Tools needed to build JTAG Scanbridge image
---------------------------------------------------------------------------
- Diamond Diamond 3.12 
  https://www.latticesemi.com/latticediamond#windows
- A license is needed. This license is free for 1 year 
  https://www.latticesemi.com/Support/Licensing/DiamondAndiCEcube2SoftwareLicensing/DiamondFree

---------------------------------------------------------------------------
How to build an Lattice JTAG image
---------------------------------------------------------------------------
- Open UNB2_JTAG_SCANBRIDGE.ldf project file with Lattice Diamond.
- Open the "Process" tab in the left window.
- In Process tab , tick "JEDEC File" to make bit file.
- Double click "Export Files".

---------------------------------------------------------------------------
Make SVF file (used in JTAG Provision to configure the FPGA)
---------------------------------------------------------------------------
- Start Programmer
- Design -> Utilities -> Deployment Tool
- Function: Files Conversion, Output File Type IEEE 1532 ISC Data File --> OK
- Click on "SVF" button
- Add the jed file (hdl/boards/uniboard2c/lattice_jtag/UNB2_JTAG_SCANBRIDGE/UNB2_JTAG_SCANBRIDGE_UNB2_JTAG_SCANBRIDGE.jed)
- Click Next
- Set:
  - Flash Erase, Program, Verify
  - Write Header and Comments
  - Rev D Standaard SVF
  - RUNTEST from Rev C
- Click Next
- Click Generate






