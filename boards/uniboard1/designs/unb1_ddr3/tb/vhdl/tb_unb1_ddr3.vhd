-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Verify DDR3 module using stimuli that are applied via python script

-- Description:

-- Usage:
--   Modelsim:
--   > load simulation configuration: tb_unb_ddr3
--   > run 100 us
--   Terminal:
--   > python tc_unb1_ddr3_seq.py --sim --unb 0 --fn 3 --rep 3

library IEEE, common_lib, dp_lib, unb1_board_lib, diag_lib, tech_ddr_lib, technology_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use unb1_board_lib.unb1_board_peripherals_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use tech_ddr_lib.tech_ddr_mem_model_component_pkg.all;
use technology_lib.technology_select_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;

entity tb_unb1_ddr3 is
  generic (
    g_design_name : string  := "unb1_ddr3"
  );
end tb_unb1_ddr3;

architecture tb of tb_unb1_ddr3 is
  -- UniBoard
  constant c_sim             : boolean := true;
  constant c_unb_nr          : natural := 0;  -- UniBoard 0
  constant c_node_nr         : natural := 3;  -- Front node 3
  constant c_id              : std_logic_vector(7 downto 0) := TO_UVEC(c_unb_nr, c_unb1_board_nof_uniboard_w) & TO_UVEC(c_node_nr, c_unb1_board_nof_chip_w);

  constant c_version         : std_logic_vector(1 downto 0) := "00";
  constant c_fw_version      : t_unb1_board_fw_version := (1, 0);

  constant c_ddr             : t_c_tech_ddr := c_tech_ddr3_4g_800m_master;

  constant c_st_dat_w        : natural := 64;  -- Any power of two 8..256

  constant c_cable_delay     : time := 12 ns;
  constant c_eth_clk_period  : time := 40 ns;  -- 25 MHz XO on UniBoard
  constant c_ext_clk_period  : time := 5 ns;
  constant c_pps_period      : natural := 1000;

   -- SO-DIMM Memory Bank I = ddr3_I
  signal MB_I_in             : t_tech_ddr3_phy_in;
  signal MB_I_io             : t_tech_ddr3_phy_io;
  signal MB_I_ou             : t_tech_ddr3_phy_ou;

  -- DUT
  signal clk                 : std_logic := '0';
  signal pps                 : std_logic := '0';
  signal pps_rst             : std_logic := '0';

  signal WDI                 : std_logic;
  signal INTA                : std_logic;
  signal INTB                : std_logic;

  signal eth_clk             : std_logic := '0';
  signal eth_txp             : std_logic;
  signal eth_rxp             : std_logic;

  signal VERSION             : std_logic_vector(c_unb1_board_aux.version_w - 1 downto 0) := c_version;
  signal ID                  : std_logic_vector(c_unb1_board_aux.id_w - 1 downto 0)      := c_id;
  signal TESTIO              : std_logic_vector(c_unb1_board_aux.testio_w - 1 downto 0);

  signal sens_scl            : std_logic;
  signal sens_sda            : std_logic;
begin
   ----------------------------------------------------------------------------
  -- System setup
  ----------------------------------------------------------------------------
  clk     <= not clk after c_ext_clk_period / 2;  -- External clock (200 MHz)
  eth_clk <= not eth_clk after c_eth_clk_period / 2;  -- Ethernet ref clock (25 MHz)

  INTA <= 'H';  -- pull up
  INTB <= 'H';  -- pull up

  sens_scl <= 'H';  -- pull up
  sens_sda <= 'H';  -- pull up

  ------------------------------------------------------------------------------
  -- External PPS
  ------------------------------------------------------------------------------
  proc_common_gen_pulse(1, c_pps_period, '1', pps_rst, clk, pps);

  ------------------------------------------------------------------------------
  -- 1GbE Loopback model
  ------------------------------------------------------------------------------
  eth_rxp <= transport eth_txp after c_cable_delay;

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------

  dut : entity work.unb1_ddr3
  generic map (
    g_sim           => c_sim,
    g_sim_unb_nr    => c_unb_nr,
    g_sim_node_nr   => c_node_nr,
    -- Stamps are passed via QIP at compile start if $UNB_COMPILE_STAMPS is set
    g_st_dat_w      => c_st_dat_w

  )
  port map (
    -- GENERAL
    CLK         => clk,
    PPS         => pps,
    WDI         => WDI,
    INTA        => INTA,
    INTB        => INTB,

    sens_sc     => sens_scl,
    sens_sd     => sens_sda,

    -- Others
    VERSION     => VERSION,
    ID          => ID,
    TESTIO      => TESTIO,

    -- 1GbE Control Interface
    ETH_clk     => eth_clk,
    ETH_SGIN    => eth_rxp,
    ETH_SGOUT   => eth_txp,

    -- SO-DIMM Memory Bank I = ddr3_I
    MB_I_IN     => MB_I_in,
    MB_I_IO     => MB_I_io,
    MB_I_OU     => MB_I_ou
   );

  ------------------------------------------------------------------------------
  -- DDR3 memory model
  ------------------------------------------------------------------------------
  u_tech_ddr_memory_model : entity tech_ddr_lib.tech_ddr_memory_model
  generic map (
    g_tech_ddr => c_ddr
  )
  port map (
    mem3_in => MB_I_ou,
    mem3_io => MB_I_io,
    mem3_ou => MB_I_in
  );
end tb;
