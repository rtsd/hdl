-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Author: R vd Walle
-- Purpose:
-- Test bench for dp_block_validate_length.
-- Description:
-- Verifies the output sosi of the DUT with the expected sosi.
-- Usage:
-- . as 5
-- . run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_str_pkg.all;
use common_lib.common_lfsr_sequences_pkg.all;
use common_lib.tb_common_pkg.all;
use work.dp_stream_pkg.all;
use work.tb_dp_pkg.all;

entity tb_dp_block_validate_length is
  generic (
    g_nof_blocks_per_sync  : natural := 5;
    g_nof_data_per_blk     : natural := 9;
    g_expected_length      : natural := 3;
    g_err_bi               : natural := 3
  );
end tb_dp_block_validate_length;

architecture tb of tb_dp_block_validate_length is
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  constant c_clk_period  : time := 5 ns;

  constant c_dut_pipeline             : natural := 1;
  constant c_nof_sync                 : natural := 5;
  constant c_gap_size                 : natural := 4;
  constant c_exp_err                  : std_logic_vector(c_dp_stream_error_w - 1 downto 0) := TO_UVEC(2**g_err_bi, c_dp_stream_error_w);

  signal clk            : std_logic := '1';
  signal rst            : std_logic := '1';
  signal tb_end         : std_logic := '0';

  signal stimuli_end         : std_logic;
  signal stimuli_sosi        : t_dp_sosi;
  signal stimuli_siso        : t_dp_siso;
  signal stimuli_cnt_reg     : natural;
  signal stimuli_cnt         : natural;
  signal verify_sosi         : t_dp_sosi;
  signal verify_siso         : t_dp_siso := c_dp_siso_rdy;
  signal reference_cnt       : natural;
  signal reference_sosi      : t_dp_sosi;
  signal reference_siso      : t_dp_siso := c_dp_siso_rdy;
begin
  ------------------------------------------------------------------------------
  -- Clock & reset
  ------------------------------------------------------------------------------
  clk <= (not clk) or tb_end after c_clk_period / 2;
  rst <= '1', '0' after c_clk_period * 7;

  ------------------------------------------------------------------------------
  -- Stimuli:
  ------------------------------------------------------------------------------

  -- Generate snk_in with data frames
  u_stimuli : entity work.dp_stream_stimuli
  generic map (
    g_sync_period => g_nof_blocks_per_sync,
    g_nof_repeat  => g_nof_blocks_per_sync * c_nof_sync,
    g_pkt_len     => g_nof_data_per_blk,
    g_pkt_gap     => c_gap_size,
    g_err_init    => 0,
    g_err_incr    => 0
  )
  port map (
    rst               => rst,
    clk               => clk,

    -- Generate stimuli
    src_in            => stimuli_siso,
    src_out           => stimuli_sosi,

    -- End of stimuli
    tb_end            => stimuli_end
  );

  ------------------------------------------------------------------------------
  -- DUT
  ------------------------------------------------------------------------------
  u_dut : entity work.dp_block_validate_length
  generic map (
    g_err_bi           => g_err_bi,
    g_expected_length  => g_expected_length
  )
  port map (
    rst          => rst,
    clk          => clk,
    -- ST sink
    snk_out      => stimuli_siso,
    snk_in       => stimuli_sosi,
    -- ST source
    src_in       => verify_siso,
    src_out      => verify_sosi
  );

  ------------------------------------------------------------------------------
  -- Verification
  ------------------------------------------------------------------------------

  u_pipeline : entity work.dp_pipeline
  generic map (
    g_pipeline   => c_dut_pipeline
  )
  port map (
    rst         => rst,
    clk         => clk,
    -- ST sink
    snk_out     => OPEN,
    snk_in      => stimuli_sosi,
    -- ST source
    src_in      => reference_siso,
    src_out     => reference_sosi
  );

  stimuli_cnt_reg <= stimuli_cnt when rising_edge(clk);
  stimuli_cnt     <= 0           when stimuli_sosi.sop = '1' else
             stimuli_cnt_reg + 1 when stimuli_sosi.valid = '1' else
             stimuli_cnt_reg;

  reference_cnt <= stimuli_cnt when rising_edge(clk);

  p_verify : process(clk)
  begin
    if rising_edge(clk) then
      if reference_sosi.valid = '1' then
        if g_expected_length = g_nof_data_per_blk then  -- in sosi should be identical to out sosi
          assert verify_sosi = reference_sosi
            report "Unexpected difference between in / out sosi"
            severity ERROR;

        elsif g_expected_length < g_nof_data_per_blk then  -- expect an err bit to be set and the block length to be limited to g_expected_length
          if reference_cnt < g_expected_length - 1 then
            assert verify_sosi = reference_sosi
              report "Wrong block while reference_cnt < g_expected_length-1"
              severity ERROR;
          elsif reference_cnt = g_expected_length - 1 then
            assert verify_sosi.sync = reference_sosi.sync
              report "Wrong sync, while reference_cnt = g_expected_length-1"
              severity ERROR;
            assert verify_sosi.bsn = reference_sosi.bsn
              report "Wrong bsn, while reference_cnt = g_expected_length-1"
              severity ERROR;
            assert verify_sosi.channel = reference_sosi.channel
              report "Wrong valid, while reference_cnt = g_expected_length-1"
              severity ERROR;
            assert verify_sosi.data = reference_sosi.data
              report "Wrong data, while reference_cnt = g_expected_length-1"
              severity ERROR;
            assert verify_sosi.valid = reference_sosi.valid
              report "Wrong valid, while reference_cnt = g_expected_length-1"
              severity ERROR;
            assert verify_sosi.sop = reference_sosi.sop
              report "Wrong sop, while reference_cnt = g_expected_length-1"
              severity ERROR;
            assert verify_sosi.eop = '1'
              report "Wrong eop, while reference_cnt = g_expected_length-1"
              severity ERROR;
            assert verify_sosi.err = c_exp_err
              report "Wrong err, while reference_cnt = g_expected_length-1"
              severity ERROR;
          else  -- reference_cnt > g_expected_length-1
            assert verify_sosi.valid = '0'
              report "Wrong, valid should be '0' when reference_cnt > g_expected_length-1"
              severity ERROR;
          end if;

        else  -- g_expected_length > g_nof_data_per_blk
          -- Expected an err bit to be set at eop
          if reference_cnt = g_nof_data_per_blk - 1 then
            assert verify_sosi.sync = reference_sosi.sync
              report "Wrong sync, while reference_cnt = g_nof_data_per_blk-1"
              severity ERROR;
            assert verify_sosi.bsn = reference_sosi.bsn
              report "Wrong bsn, while reference_cnt = g_nof_data_per_blk-1"
              severity ERROR;
            assert verify_sosi.channel = reference_sosi.channel
              report "Wrong valid, while reference_cnt = g_nof_data_per_blk-1"
              severity ERROR;
            assert verify_sosi.data = reference_sosi.data
              report "Wrong data, while reference_cnt = g_nof_data_per_blk-1"
              severity ERROR;
            assert verify_sosi.valid = reference_sosi.valid
              report "Wrong valid, while reference_cnt = g_nof_data_per_blk-1"
              severity ERROR;
            assert verify_sosi.sop = reference_sosi.sop
              report "Wrong sop, while reference_cnt = g_nof_data_per_blk-1"
              severity ERROR;
            assert verify_sosi.eop = reference_sosi.eop
              report "Wrong eop, while reference_cnt = g_nof_data_per_blk-1"
              severity ERROR;
            assert verify_sosi.err = c_exp_err
              report "Wrong err, while reference_cnt = g_nof_data_per_blk-1"
              severity ERROR;
          else
            assert verify_sosi = reference_sosi
              report "Wrong block while g_expected_length > g_nof_data_per_blk"
              severity ERROR;
          end if;
        end if;
      end if;
    end if;
  end process;

  tb_end <= '0', stimuli_end after (1 + 10 * c_dut_pipeline) * c_clk_period;
end tb;
