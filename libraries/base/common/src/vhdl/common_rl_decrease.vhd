-------------------------------------------------------------------------------
--
-- Copyright (C) 2013
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- >>> Ported from UniBoard dp_latency_adapter for fixed RL 0 --> 1

library IEEE;
use IEEE.std_logic_1164.all;

-- Purpose: Adapt from ready latency 1 to 0 to make a look ahead FIFO
-- Description: -
-- Remark:
-- . A show ahead FIFO with RL=0 does not need a rd_emp output signal, because
--   with RL=0 the rd_val='0' when it is empty (so emp <= NOT rd_val).

entity common_rl_decrease is
  generic (
    g_adapt       : boolean := true;  -- default when TRUE then decrease sink RL 1 to source RL 0, else then implement wires
    g_dat_w       : natural := 18
  );
  port (
    rst           : in  std_logic;
    clk           : in  std_logic;
    -- ST sink: RL = 1
    snk_out_ready : out std_logic;
    snk_in_dat    : in  std_logic_vector(g_dat_w - 1 downto 0);
    snk_in_val    : in  std_logic := 'X';
    -- ST source: RL = 0
    src_in_ready  : in  std_logic;
    src_out_dat   : out std_logic_vector(g_dat_w - 1 downto 0);
    src_out_val   : out std_logic
  );
end common_rl_decrease;

architecture rtl of common_rl_decrease is
  -- Internally use streaming record for the SOSI, for the SISO.ready directly use src_in_ready
  type t_sosi is record  -- Source Out or Sink In
    data     : std_logic_vector(g_dat_w - 1 downto 0);
    valid    : std_logic;
  end record;

  type t_sosi_arr is array (integer range <>) of t_sosi;

  constant c_sosi_rst : t_sosi := ((others => '0'), '0');

  -- SOSI IO
  signal snk_in       : t_sosi;
  signal src_out      : t_sosi;

  -- The default FIFO has ready latency RL = 1, need to use input RL + 1 words for the buf array, to go to output RL = 0 for show ahead FIFO
  signal buf          : t_sosi_arr(1 downto 0);
  signal nxt_buf      : t_sosi_arr(1 downto 0);
begin
  gen_wires : if g_adapt = false generate
    snk_out_ready <= src_in_ready;

    src_out_dat   <= snk_in_dat;
    src_out_val   <= snk_in_val;
  end generate;

  gen_adapt : if g_adapt = true generate
    snk_in.data  <= snk_in_dat;
    snk_in.valid <= snk_in_val;

    src_out_dat <= src_out.data;
    src_out_val <= src_out.valid;

    -- Buf[0] contains the FIFO output with zero ready latency
    src_out <= buf(0);

    p_clk : process(rst, clk)
    begin
      if rst = '1' then
        buf <= (others => c_sosi_rst);
      elsif rising_edge(clk) then
        buf <= nxt_buf;
      end if;
    end process;

    p_snk_out_ready : process(buf, src_in_ready, snk_in)
    begin
      snk_out_ready <= '0';
      if src_in_ready = '1' then
        -- Default snk_out_ready when src_in_ready.
        snk_out_ready <= '1';
      else
        -- Extra snk_out_ready to look ahead for RL = 0.
        if buf(0).valid = '0' then
          snk_out_ready <= '1';
        elsif buf(1).valid = '0' then
          snk_out_ready <= not(snk_in.valid);
        end if;
      end if;
    end process;

    p_buf : process(buf, src_in_ready, snk_in)
    begin
      -- Keep or shift the buf dependent on src_in_ready, no need to explicitly check buf().valid
      nxt_buf <= buf;
      if src_in_ready = '1' then
        nxt_buf(0) <= buf(1);
        nxt_buf(1).valid <= '0';  -- not strictly necessary, but robust
      end if;

      -- Put input data at the first available location dependent on src_in_ready, no need to explicitly check snk_in_val
      if buf(0).valid = '0' then
        nxt_buf(0) <= snk_in;
      else
        if buf(1).valid = '0' then
          if src_in_ready = '0' then
            nxt_buf(1) <= snk_in;
          else
            nxt_buf(0) <= snk_in;
          end if;
        end if;
      end if;
    end process;
  end generate;
end rtl;
