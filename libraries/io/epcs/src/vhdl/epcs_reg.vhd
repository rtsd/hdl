-------------------------------------------------------------------------------
--
-- Copyright (C) 2011
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

--  RO  read only  (no VHDL present to access HW in write mode)
--  WO  write only (no VHDL present to access HW in read mode)
--  WE  write event (=WO)
--  WR  write control, read control
--  RW  read status, write control
--  RC  read, clear on read
--  FR  FIFO read
--  FW  FIFO write
--
--  wi  Bits    R/W Name              Default  Description             |REG_EPCS|
--  =============================================================================
--  0   [23..0] WO  addr              0x0      Address to write to/read from
--  1   [0]     WO  rden              0x0      Read enable
--  2   [0]     WE  read              0x0      Read
--  3   [0]     WE  write             0x0      Write
--  4   [0]     WO  sector_erase      0x0      Sector erase
--  5   [0]     RO  busy              0x0      Busy
--  =============================================================================
--
--  Refer to the user guide of Altera's ALTASMI_PARALLEL megafunction for more
--  information.

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity epcs_reg is
  generic (
    g_epcs_addr_w : natural := 24
  );
  port (
    -- Clocks and reset
    mm_rst                   : in  std_logic;
    mm_clk                   : in  std_logic;

    epcs_rst                 : in  std_logic;
    epcs_clk                 : in  std_logic;

    -- Memory Mapped Slave
    sla_in                   : in  t_mem_mosi;
    sla_out                  : out t_mem_miso;

    epcs_in_addr             : out std_logic_vector(g_epcs_addr_w - 1 downto 0);
    epcs_in_read_evt         : out std_logic;
    epcs_in_rden             : out std_logic;
    epcs_in_write_evt        : out std_logic;
    epcs_in_sector_erase_evt : out std_logic;

    epcs_out_busy            : in std_logic;

    unprotect_address_range  : out std_logic := '0'
    );
end epcs_reg;

architecture rtl of epcs_reg is
  -- For safety, address range unprotection requires the following word to be written:
  constant c_unprotect_passphrase : std_logic_vector(c_word_w - 1 downto 0 ) := x"BEDA221E";  -- "Bedazzle"

  constant c_mm_reg        : t_c_mem := (latency  => 1,
                                         adr_w    => ceil_log2(6),
                                         dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                         nof_dat  => 6,
                                         init_sl  => '0');

  signal mm_epcs_in_addr             : std_logic_vector(g_epcs_addr_w - 1 downto 0);
  signal mm_epcs_in_rden             : std_logic;
  signal mm_epcs_in_read_evt         : std_logic;
  signal mm_epcs_in_write_evt        : std_logic;
  signal mm_epcs_in_sector_erase_evt : std_logic;

  signal mm_busy                     : std_logic;

  signal mm_unprotect_address_range  : std_logic := '0';
begin
  p_mm_reg : process (mm_rst, mm_clk)
  begin
    if mm_rst = '1' then
      -- Read access
      sla_out   <= c_mem_miso_rst;
      -- Write access, register values
      mm_epcs_in_addr <= (others => '0');
      mm_epcs_in_rden <= '0';
    elsif rising_edge(mm_clk) then
      -- Read access defaults
      sla_out.rdval <= '0';

      -- Write event defaults
      mm_epcs_in_read_evt         <= '0';
      mm_epcs_in_write_evt        <= '0';
      mm_epcs_in_sector_erase_evt <= '0';

      -- Write access: set register value
      if sla_in.wr = '1' then
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          -- Write Block Sync
          when 0 =>
            mm_epcs_in_addr <= sla_in.wrdata(g_epcs_addr_w - 1 downto 0);
          when 1 =>
            mm_epcs_in_rden <= sla_in.wrdata(0);
          when 2 =>
            mm_epcs_in_read_evt <= sla_in.wrdata(0);
          when 3 =>
            mm_epcs_in_write_evt <= sla_in.wrdata(0);
          when 4 =>
            mm_epcs_in_sector_erase_evt <= sla_in.wrdata(0);
          when 6 =>
            if sla_in.wrdata(c_word_w - 1 downto 0) = c_unprotect_passphrase then
              mm_unprotect_address_range <= '1';
            else
              mm_unprotect_address_range <= '0';  -- Writing anything else protects the range again
            end if;
          when others => null;  -- unused MM addresses
        end case;

      -- Read access: get register value
      elsif sla_in.rd = '1' then
        sla_out       <= c_mem_miso_rst;  -- set unused rddata bits to '0' when read
        sla_out.rdval <= '1';  -- c_mm_reg.latency = 1
        case TO_UINT(sla_in.address(c_mm_reg.adr_w - 1 downto 0)) is
          when 5 =>
            sla_out.rddata(0) <= epcs_out_busy;
          when others => null;  -- unused MM addresses
        end case;
      end if;
    end if;
  end process;

  u_spulse_epcs_read : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_epcs_in_read_evt,
    in_busy   => OPEN,
    out_rst   => epcs_rst,
    out_clk   => epcs_clk,
    out_pulse => epcs_in_read_evt
  );

  u_spulse_epcs_write : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_epcs_in_write_evt,
    in_busy   => OPEN,
    out_rst   => epcs_rst,
    out_clk   => epcs_clk,
    out_pulse => epcs_in_write_evt
  );

  u_spulse_epcs_sector_erase : entity common_lib.common_spulse
  port map (
    in_rst    => mm_rst,
    in_clk    => mm_clk,
    in_pulse  => mm_epcs_in_sector_erase_evt,
    in_busy   => OPEN,
    out_rst   => epcs_rst,
    out_clk   => epcs_clk,
    out_pulse => epcs_in_sector_erase_evt
  );

  epcs_in_addr            <= mm_epcs_in_addr;
  epcs_in_rden            <= mm_epcs_in_rden;
  unprotect_address_range <= mm_unprotect_address_range;
end rtl;
