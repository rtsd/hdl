###############################################################################
#
# Copyright (C) 2013
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

set_time_format -unit ns -decimal_places 3

create_clock -period 125Mhz [get_ports {ETH_CLK}]
create_clock -period 200Mhz [get_ports {CLK}]
create_clock -period 100Mhz [get_ports {CLKUSR}]
create_clock -period 644.53125Mhz [get_ports {SA_CLK}]
create_clock -period 644.53125Mhz [get_ports {SB_CLK}]
create_clock -period 1.552 -name {BCK_REF_CLK} { BCK_REF_CLK }

derive_pll_clocks
derive_clock_uncertainty

set_clock_groups -asynchronous -group {CLK}
set_clock_groups -asynchronous -group {ETH_CLK}
set_clock_groups -asynchronous -group {BCK_REF_CLK}
set_clock_groups -asynchronous -group {CLK_USR}
set_clock_groups -asynchronous -group {CLKUSR}
set_clock_groups -asynchronous -group {SA_CLK}
set_clock_groups -asynchronous -group {SB_CLK}

# IOPLL outputs (which have global names defined in the IP qsys settings)
set_clock_groups -asynchronous -group [get_clocks pll_clk20]
set_clock_groups -asynchronous -group [get_clocks pll_clk50]
set_clock_groups -asynchronous -group [get_clocks pll_clk100]
set_clock_groups -asynchronous -group [get_clocks pll_clk125]
set_clock_groups -asynchronous -group [get_clocks pll_clk200]
set_clock_groups -asynchronous -group [get_clocks pll_clk200p]
set_clock_groups -asynchronous -group [get_clocks pll_clk400]


# FPLL outputs
set_clock_groups -asynchronous -group [get_clocks {*u_ctrl|\gen_mm_clk_hardware:u_unb2_board_clk125_pll|\gen_fractional_pll:u_pll|\gen_ip_arria10:u0|xcvr_fpll_a10_0|outclk1}]
set_clock_groups -asynchronous -group [get_clocks {*u_ctrl|\gen_mm_clk_hardware:u_unb2_board_clk125_pll|\gen_fractional_pll:u_pll|\gen_ip_arria10:u0|xcvr_fpll_a10_0|outclk3}]
set_clock_groups -asynchronous -group [get_clocks {*u_ctrl|\gen_dp_clk_hardware:gen_pll:u_unb2_board_clk200_pll|\gen_st_fractional_pll:u_st_fractional_pll|\gen_ip_arria10:u0|xcvr_fpll_a10_0|outclk0}]


set_clock_groups -asynchronous -group [get_clocks {u_revision|\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|u_tr_10GbE_0|u_tech_eth_10g|\gen_ip_arria10:u0|u_tech_10gbase_r|\gen_ip_arria10:u0|\gen_phy_12:u_ip_arria10_phy_10gbase_r_12|xcvr_native_a10_0|g_xcvr_native_insts[*]|rx_pma_clk}]
set_clock_groups -asynchronous -group [get_clocks {u_revision|\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|u_unb2_board_clk644_pll|\gen_ip_arria10:u0|xcvr_fpll_a10_0|outclk0}]
set_clock_groups -asynchronous -group [get_clocks {u_revision|\gen_udp_stream_10GbE:u_tr_10GbE_qsfp_and_ring|u_unb2_board_clk644_pll|\gen_ip_arria10:u0|xcvr_fpll_a10_0|outclk1}]


#set_clock_groups -asynchronous \
#-group [get_clocks {inst2|xcvr_4ch_native_phy_inst|xcvr_native_a10_0|g_xcvr_native_insts[?]|rx_pma_clk}] \
#-group [get_clocks {inst2|xcvr_pll_inst|xcvr_fpll_a10_0|tx_bonding_clocks[0]}]


#JTAG Signal Constraints
#constrain the TDI TMS and TDO ports  -- (modified from timequest SDC cookbook)
#set_input_delay  -clock altera_reserved_tck 5 [get_ports altera_reserved_tdi]
#set_input_delay  -clock altera_reserved_tck 5 [get_ports altera_reserved_tms]
#set_output_delay -clock altera_reserved_tck -clock_fall -fall -max 5 [get_ports altera_reserved_tdo]

