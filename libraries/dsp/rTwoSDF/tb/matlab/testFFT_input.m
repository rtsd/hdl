%% writes R2SDF input stimulis, full range uniform  for {B} bits input
%% signal and {N} point FFT
% 
%-------------------------------------------------------------------------%
%   Author: Raj Thilak Rajan : rajan at astron.nl: Nov 2009
%   Copyright (C) 2009-2011
%   ASTRON (Netherlands Institute for Radio Astronomy)
%   P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
% 
%   This file is part of the UniBoard software suite.
%   The file is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%-------------------------------------------------------------------------%
%%

clear all; close all; clc;

j=1i; 
K=12;       % log2(# of points)
B=16;       % # of input bits

P=2.^(1:K); 

nOfFrames= 8;

NOISE=      0;
REARRANGE = 1;

SYN=1;
VAL=2;
RE= 3;
IM= 4;

H=  1;  %ON
L=  0;  %OFF

for kk=2:K
  for bb=4:1:B
    inFileName= strcat('../data/test/in/uniNoise_p',num2str(P(kk)),'_b',num2str(bb),'_in.txt');

    'test signal'
    x=zeros(nOfFrames*P(kk),3);
    high=2^(bb-1)-1;
    
    x(:,SYN)= 0;
    %x(:,VAL)= 1; %round(rand(P(kk),1)); 
    x(:,RE)=  round(high*(2*rand(nOfFrames*P(kk),1)-1));
    x(:,IM)=  round(high*(2*rand(nOfFrames*P(kk),1)-1));
       
    
    
    'VALID IN signal- mask template'
    % 8 frames
    % H_L_H_L_L_H_H_L
    seq= [H,L,H,L,L,H,H,L];
    %seq= [H,H,H,H,H,H,H,H];
    for ii=0:nOfFrames-1
      x(ii*P(kk)+1:(ii+1)*P(kk),VAL)  = seq(ii+1);
    end;
    
    'writing test signal to file'
    inFileName
    dlmwrite(inFileName,x(1:nOfFrames*P(kk),:),'delimiter',',');
    
  end;
end;
