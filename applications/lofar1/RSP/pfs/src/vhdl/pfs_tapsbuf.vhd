-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: ported by E. Kooistra, original 2004 by W. Lubberhuizen / W. Poeisz
-- Purpose: Polyphase FIR filter
-- Description: Ported from LOFAR1, see readme_lofar1.txt
-- Remark: Put entity and architecture in same file without () in file name.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity pfs_tapsbuf is
  generic (
    g_data_w      : natural;
    g_nof_words   : natural;
    g_addr_w      : natural
  );
  port (
    wrdata        : in  std_logic_vector(g_data_w - 1 downto 0);
    wren          : in  std_logic;
    wraddr        : in  std_logic_vector(g_addr_w - 1 downto 0);
    rdaddr        : in  std_logic_vector(g_addr_w - 1 downto 0);
    rddata        : out std_logic_vector(g_data_w - 1 downto 0);
    clk           : in  std_logic;
    rst           : in  std_logic
  );
end pfs_tapsbuf;

architecture rtl of pfs_tapsbuf is
  type RamType is array(0 to 2**g_addr_w) of std_logic_vector(g_data_w - 1 downto 0);

  -- pfs_tapsbuf_1024.hex is empty (all zeros)
  signal RAM : RamType := (others => (others => '0'));

  signal read_addrb : std_logic_vector(g_addr_w - 1 downto 0);
begin
  process (clk)
  begin
    if rising_edge(clk) then
      if (wren = '1') then
        RAM (conv_integer(wraddr)) <= wrdata;
      end if;
      read_addrb <= rdaddr;
      rddata <= RAM(conv_integer(read_addrb));
    end if;
  end process;
end rtl;
