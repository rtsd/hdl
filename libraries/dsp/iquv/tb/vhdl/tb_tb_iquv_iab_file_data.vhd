--------------------------------------------------------------------------------
--
-- Copyright (C) 2016
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--------------------------------------------------------------------------------

-- Purpose: Multi-testbench for iquv_iab_file_data
-- Description:
--   Verify iquv_iab
-- Usage:
--   > as 4
--   > run -all

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;

entity tb_tb_iquv_iab_file_data is
end tb_tb_iquv_iab_file_data;

architecture tb of tb_tb_iquv_iab_file_data is
  constant c_file_name           : string  := "iquv_stimuli_1.dat";
  constant c_nof_streams         : natural := 3;
  signal tb_end                  : std_logic := '0';  -- declare tb_end to avoid 'No objects found' error on 'when -label tb_end'
begin
  -- Parameter list (random valid,  stimuli file, enable IAB accumulator, number of streams  samples to integrate,  in data width, out data width);
  u_tc0            : entity work.tb_iquv_iab_file_data generic map (true,  c_file_name, false, c_nof_streams,  8,  12, 18);
  u_tc1            : entity work.tb_iquv_iab_file_data generic map (true,  c_file_name, false, c_nof_streams, 16,  12, 18);
  u_tc2            : entity work.tb_iquv_iab_file_data generic map (true,  c_file_name, false, c_nof_streams,  8,  12,  8);
  u_tc3            : entity work.tb_iquv_iab_file_data generic map (true,  c_file_name, false, c_nof_streams, 16,  12,  8);
  u_tc4            : entity work.tb_iquv_iab_file_data generic map (true,  c_file_name,  true, c_nof_streams,  8,  12, 18);
  u_tc5            : entity work.tb_iquv_iab_file_data generic map (true,  c_file_name,  true, c_nof_streams, 16,  12, 18);
  u_tc6            : entity work.tb_iquv_iab_file_data generic map (true,  c_file_name,  true, c_nof_streams,  8,  12,  8);
  u_tc7            : entity work.tb_iquv_iab_file_data generic map (true,  c_file_name,  true, c_nof_streams, 16,  12,  8);
end tb;
