-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Author:
-- . Daniel van der Schuur
-- Purpose:
-- . Read and verify histogram RAM of instance 0
-- ModelSim usage:
-- . (open project, compile)
-- . (load simulation config)
-- . as 8
-- . run -a
-- Description:
-- . This TB is self checking and stops after g_nof_sync test iterations.
-- . This TB only checks the MM aspects of mmp_st_histogram with limited (counter
--   data) stimuli and verification. Details of st_histogram are thoroughly
--   checked in tb_tb_st_histogram.
-------------------------------------------------------------------------------

library IEEE, common_lib, mm_lib, dp_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use common_lib.tb_common_mem_pkg.all;
use common_lib.tb_common_pkg.all;
use dp_lib.dp_stream_pkg.all;
use dp_lib.tb_dp_pkg.all;

entity tb_mmp_st_histogram is
  generic(
    g_nof_sync               : natural := 4;
    g_nof_instances          : natural := 12;
    g_data_w                 : natural := 14;
    g_nof_bins               : natural := 512;
    g_nof_data_per_sync      : natural := 16384;  -- g_nof_data_per_sync/g_nof_bins should be integer so counter data yields the same histogram in each bin
    g_nof_data_per_sync_diff : natural := 32  -- Use non-constant g_nof_data_per_sync: longer (+g_nof_data_per_sync_diff), shorter (-g_nof_data_per_sync_diff),
  );  -- longer, shorter, etc. E.g. LOFAR2 uses 200M+-512 samples per sync.
end tb_mmp_st_histogram;

architecture tb of tb_mmp_st_histogram is
  ---------------------------------------------------------------------------
  -- Constants derived from generics
  ---------------------------------------------------------------------------
  constant c_expected_ram_content_counter : natural := g_nof_data_per_sync / g_nof_bins;
  constant c_ram_dat_w                    : natural := ceil_log2(g_nof_data_per_sync + 1);
  constant c_ram_adr_w                    : natural := ceil_log2(g_nof_bins);

  ---------------------------------------------------------------------------
  -- Clocks and resets
  ---------------------------------------------------------------------------
  constant c_dp_clk_period : time := 5 ns;
  constant c_mm_clk_period : time := 20 ns;

  signal dp_clk            : std_logic := '1';
  signal dp_rst            : std_logic;
  signal mm_clk            : std_logic := '1';
  signal mm_rst            : std_logic;
  signal tb_end            : std_logic := '0';

  ----------------------------------------------------------------------------
  -- stimuli
  ----------------------------------------------------------------------------
  signal stimuli_en      : std_logic := '1';
  signal stimuli_src_out : t_dp_sosi;
  signal stimuli_src_in  : t_dp_siso;
  signal stimuli_done    : std_logic;

  ----------------------------------------------------------------------------
  -- st_histogram
  ----------------------------------------------------------------------------
  signal st_histogram_snk_in_arr   : t_dp_sosi_arr(g_nof_instances - 1 downto 0);
  signal st_histogram_ram_copi     : t_mem_copi;
  signal prv_st_histogram_ram_copi : t_mem_copi;
  signal st_histogram_ram_cipo     : t_mem_cipo;

  ----------------------------------------------------------------------------
  -- Automatic verification of RAM readout
  ----------------------------------------------------------------------------
  -- Use these 4 signals to verify histogram by eye in the wave window
  signal histogram_data         : natural;  -- QuestaSim: Format->Analog
  signal histogram_bin_unsigned : natural;
  signal histogram_bin_signed   : integer;  -- QuestaSim: Radix->Decimal
  signal histogram_valid        : std_logic;

  signal sum_of_bins            : natural;
  signal verification_done      : std_logic;

  signal ver_long_sync_interval : boolean;
begin
  ----------------------------------------------------------------------------
  -- Clock and reset generation
  ----------------------------------------------------------------------------
  dp_clk <= not dp_clk or tb_end after c_dp_clk_period / 2;
  dp_rst <= '1', '0' after c_dp_clk_period * 10;

  mm_clk <= not mm_clk or tb_end after c_mm_clk_period / 2;
  mm_rst <= '1', '0' after c_mm_clk_period * 10;

  ----------------------------------------------------------------------------
  -- DP Stimuli: generate st_histogram input (counter) data
  ----------------------------------------------------------------------------
  stimuli_src_in <= c_dp_siso_rdy;

  -- Generate g_nof_sync packets of g_nof_data_per_sync words
  p_generate_packets : process
    variable v_sosi : t_dp_sosi := c_dp_sosi_rst;
    variable v_nof_data_per_sync : natural;
    variable v_long_sync_interval : boolean;
  begin
    stimuli_done    <= '0';
    stimuli_src_out <= c_dp_sosi_rst;
    proc_common_wait_until_low(dp_clk, dp_rst);
    proc_common_wait_some_cycles(dp_clk, 5);
    v_long_sync_interval := false;

    for I in 0 to g_nof_sync - 1 loop
      -- Optionally replace equal period lengths with: shorter, longer, shorter, longer, ...
      v_nof_data_per_sync := g_nof_data_per_sync;
      if v_long_sync_interval then
        v_nof_data_per_sync := g_nof_data_per_sync + g_nof_data_per_sync_diff;
      else  -- Short interval
        v_nof_data_per_sync := g_nof_data_per_sync - g_nof_data_per_sync_diff;
      end if;

      v_sosi.sync    := '1';
      v_sosi.data    := RESIZE_DP_DATA(v_sosi.data(g_data_w - 1 downto 0));  -- wrap when >= 2**g_data_w
      -- Generate a block of counter data
      proc_dp_gen_block_data(g_data_w, TO_UINT(v_sosi.data), v_nof_data_per_sync, TO_UINT(v_sosi.channel), TO_UINT(v_sosi.err), v_sosi.sync, v_sosi.bsn, dp_clk, stimuli_en, stimuli_src_in, stimuli_src_out);
      v_long_sync_interval := not v_long_sync_interval;
    end loop;

    stimuli_done <= '1';
    proc_common_wait_some_cycles(dp_clk, 50);
    tb_end <= '1';
    wait;
  end process;

  ----------------------------------------------------------------------------
  -- mmp_st_histogram
  ----------------------------------------------------------------------------
  gen_snk_in_arr: for i in 0 to g_nof_instances - 1 generate
    st_histogram_snk_in_arr(i) <= stimuli_src_out;
  end generate;

  u_mmp_st_histogram : entity work.mmp_st_histogram
  generic map(
    g_nof_instances     => g_nof_instances,
    g_data_w            => g_data_w,
    g_nof_bins          => g_nof_bins,
    g_nof_data_per_sync => g_nof_data_per_sync,
    g_nof_data_per_sync_diff => g_nof_data_per_sync_diff
  )
  port map (
    dp_clk       => dp_clk,
    dp_rst       => dp_rst,

    mm_clk       => mm_clk,
    mm_rst       => mm_rst,

    snk_in_arr   => st_histogram_snk_in_arr,

    ram_copi     => st_histogram_ram_copi,
    ram_cipo     => st_histogram_ram_cipo
  );

  ----------------------------------------------------------------------------
  -- Readout and verification of RAM contents
  -- . The table below shows what RAM we are reading here ('RAM read') via the
  --   ram_copi/cipo interface, and what the expected RAM contents are.
  --
  -- Counter data (the same every sync excl. sync 0):
  ---+-------------+-------------+----------+-----------------------+
  -- | Sync period | RAM written | RAM read | RAM contents          |
  -- +-------------+-------------+----------+-----------------------+
  -- | 0           | 0           | 1        | 256 addresses *  0    |
  -- | 1           | 1           | 0        | 256 addresses * 12    |
  -- | 2           | 0           | 1        | 256 addresses * 12    |
  -- | 3           | 1           | 0        | 256 addresses * 12    |
  -- +-------------+-------------+----------+-----------------------+
  ----------------------------------------------------------------------------
  -- Perform MM read and put result in ram_rd_word
  p_verify_mm_read : process
  begin
    st_histogram_ram_copi.wr <= '0';
    for i in 0 to g_nof_sync - 1 loop
      -- mmp_st_histogram will start copying DP RAM contents to MM RAM 10 cyles after sync
      proc_common_wait_until_high(dp_clk, stimuli_src_out.sync);
      proc_common_wait_some_cycles(dp_clk, 10);

      -- Wait until copying the bins completes
      proc_common_wait_some_cycles(dp_clk, g_nof_bins);

      -- Start MM reading the bins after some safety cycles
      proc_common_wait_some_cycles(mm_clk, 20);
      for j in 0 to g_nof_bins - 1 loop
        proc_mem_mm_bus_rd(j, mm_clk, st_histogram_ram_copi);
      end loop;
    end loop;
    wait;
  end process;

  -- Help signals that contain the histogram bins+data
  histogram_bin_unsigned <= TO_UINT(              prv_st_histogram_ram_copi.address(c_ram_adr_w - 1 downto 0));
  histogram_bin_signed   <= TO_SINT(offset_binary(prv_st_histogram_ram_copi.address(c_ram_adr_w - 1 downto 0)));
  histogram_data         <= TO_UINT(st_histogram_ram_cipo.rddata(c_ram_dat_w - 1 downto 0)) when st_histogram_ram_cipo.rdval = '1'ELSE 0;
  histogram_valid        <= st_histogram_ram_cipo.rdval;

  -- Perform verification of ram_rd_word when ram_rd_word_valid
  p_verify_assert : process
    variable v_expected_ram_content_counter : natural;
    variable v_sum_of_bins : natural;
  begin
    verification_done <= '0';
    ver_long_sync_interval <= true;
    for i in 0 to g_nof_sync - 1 loop
      sum_of_bins <= 0;
      proc_common_wait_until_high(dp_clk, stimuli_src_out.sync);
      for j in 0 to g_nof_bins - 1 loop
        proc_common_wait_until_high(mm_clk, histogram_valid);
        if i = 0 then  -- Sync period 0: we expect RAM to contain zeros
          assert histogram_data = 0
            report "RAM contains wrong bin count (expected 0, actual " & integer'image(histogram_data) & ")"
            severity ERROR;
        else  -- Sync period 1 onwards
         v_expected_ram_content_counter := c_expected_ram_content_counter;
         if ver_long_sync_interval and j = 0 then
          -- Long sync interval: more counter values (counter wraps) so lowest bin has double the amount
           v_expected_ram_content_counter := 2 * c_expected_ram_content_counter;
         elsif ver_long_sync_interval = false and j = g_nof_bins - 1 then
          -- Short sync interval: less counter values (counter does not reach max) so highest bin remains zero
           v_expected_ram_content_counter := 0;
         end if;
          -- Check counter data: bin values remain the same every sync
          assert histogram_data = v_expected_ram_content_counter
            report "RAM contains wrong bin count (expected " & integer'image(v_expected_ram_content_counter) & ", actual " & integer'image(histogram_data) & ")"
            severity ERROR;
        end if;
        sum_of_bins <= sum_of_bins + histogram_data;  -- Keep the sum of all bins
        proc_common_wait_some_cycles(mm_clk, 1);
      end loop;

      -- Check the sum of all bins
      if i > 0 then  -- Skip sync 0 (histogram still all zeros)
        -- Account for g_nof_data_per_sync_diff
        v_sum_of_bins := g_nof_data_per_sync - g_nof_data_per_sync_diff;
        if ver_long_sync_interval then
          v_sum_of_bins := g_nof_data_per_sync + g_nof_data_per_sync_diff;
        end if;
      assert sum_of_bins = v_sum_of_bins
        report "Sum of bins not equal to g_nof_data_per_sync (expected " & integer'image(v_sum_of_bins) & ", actual " & integer'image(sum_of_bins) & ")"
        severity ERROR;
      end if;
      ver_long_sync_interval <= not ver_long_sync_interval;
    end loop;
    verification_done <= '1';  -- We have blocking proc_common_wait_until_high procedures above so we need to know if we make it here.
    wait;
  end process;

  -- Check if verification was done at all
  p_check_verification_done : process
  begin
    proc_common_wait_until_high(dp_clk, stimuli_done);
    proc_common_wait_some_cycles(dp_clk, 50);
    assert verification_done = '1'
      report "Verification failed"
      severity ERROR;
    wait;
  end process;

  -- Register MOSI to store the read address
  p_clk: process(mm_rst, mm_clk) is
  begin
    if mm_rst = '1' then
      prv_st_histogram_ram_copi <= c_mem_copi_rst;
    elsif rising_edge(mm_clk) then
      prv_st_histogram_ram_copi <= st_histogram_ram_copi;
    end if;
  end process;
end tb;
