-------------------------------------------------------------------------------
--
-- Copyright (C) 2015
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

library IEEE, common_lib, unb1_board_lib, dp_lib, diag_lib, technology_lib, tech_ddr_lib, io_ddr_lib, reorder_lib;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;
use unb1_board_lib.unb1_board_pkg.all;
use dp_lib.dp_stream_pkg.all;
use diag_lib.diag_pkg.all;
use technology_lib.technology_select_pkg.all;
use tech_ddr_lib.tech_ddr_pkg.all;
use reorder_lib.reorder_pkg.all;

entity node_unb1_ddr3_reorder is
  generic (
    g_sim            : boolean       := true;
    g_use_MB_I       : natural       := 1;
    g_tech_ddr       : t_c_tech_ddr;
    g_nof_streams    : positive      := 4;
    g_in_dat_w       : positive      := 8;
    g_ena_pre_transp : boolean       := true;
    g_frame_size_in  : natural       := 256;
    g_frame_size_out : natural       := 176;
    g_reorder_seq    : t_reorder_seq := c_reorder_seq
  );
port (
    -- System
    mm_rst                : in    std_logic;
    mm_clk                : in    std_logic;

    dp_rst                : in    std_logic;
    dp_clk                : in    std_logic;

    ddr_ref_clk           : in    std_logic;
    ddr_ref_rst           : in    std_logic;

    -- Clock outputs
    ddr_out_clk           : out   std_logic;
    ddr_out_rst           : out   std_logic;

    -- IO DDR register map
    reg_io_ddr_mosi       : in    t_mem_mosi;
    reg_io_ddr_miso       : out   t_mem_miso;

    -- Reorder transpose
    ram_ss_ss_transp_mosi : in    t_mem_mosi;
    ram_ss_ss_transp_miso : out   t_mem_miso;

    -- BSN Monitor
    reg_bsn_monitor_mosi  : in    t_mem_mosi;
    reg_bsn_monitor_miso  : out   t_mem_miso;

    -- Data Buffer Control
    reg_diag_data_buf_mosi : in   t_mem_mosi;
    reg_diag_data_buf_miso : out  t_mem_miso;

    -- Data Buffer Data
    ram_diag_data_buf_mosi : in   t_mem_mosi;
    ram_diag_data_buf_miso : out  t_mem_miso;

    -- Block generator Control
    reg_diag_bg_mosi      : in    t_mem_mosi;
    reg_diag_bg_miso      : out   t_mem_miso;

    -- Block generator Data
    ram_diag_bg_mosi      : in    t_mem_mosi;
    ram_diag_bg_miso      : out   t_mem_miso;

    -- TX Sequencer
    reg_diag_tx_seq_mosi  : in    t_mem_mosi;
    reg_diag_tx_seq_miso  : out   t_mem_miso;

    -- RX Sequencer IM
    reg_diag_rx_seq_mosi  : in    t_mem_mosi;
    reg_diag_rx_seq_miso  : out   t_mem_miso;

    -- SO-DIMM Memory Bank I
    MB_I_IN               : in    t_tech_ddr3_phy_in;
    MB_I_IO               : inout t_tech_ddr3_phy_io;
    MB_I_OU               : out   t_tech_ddr3_phy_ou
  );
end node_unb1_ddr3_reorder;

architecture str of node_unb1_ddr3_reorder is
  constant g_tech_select_default    : natural  := c_tech_select_default;
  constant c_cross_domain_dvr_ctlr  : boolean  := true;

  constant c_use_complex            : boolean  := false;
  constant c_total_data_w           : natural  := g_nof_streams * g_in_dat_w;
  constant c_complex_data_w         : natural  := c_total_data_w * c_nof_complex;
  constant c_data_w                 : natural  := sel_a_b(c_use_complex, c_complex_data_w, c_total_data_w);
  constant c_fifo_depth             : natural  := 256;  -- >=16                             , defined at DDR side of the FIFO.
  constant c_use_db                 : boolean  := false;
  constant c_use_rx_seq             : boolean  := true;
  constant c_use_steps              : boolean  := true;
  constant c_buf_use_sync           : boolean  := false;
  constant c_buf_nof_data           : natural  := 256;
  constant c_nof_bsn_streams        : natural  := 4;

  -- Signals to interface with the DDR conroller and memory model.
  signal ctlr_dvr_miso             : t_mem_ctlr_miso;
  signal ctlr_dvr_mosi             : t_mem_ctlr_mosi;

  signal to_mem_siso               : t_dp_siso := c_dp_siso_rdy;
  signal to_mem_sosi               : t_dp_sosi;

  signal from_mem_siso             : t_dp_siso := c_dp_siso_rdy;
  signal from_mem_sosi             : t_dp_sosi;

  signal ddr_out_clk_i             : std_logic;
  signal ddr_out_rst_i             : std_logic;

  signal bg_sosi_arr               : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal bg_siso_arr               : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

  signal db_sosi_arr               : t_dp_sosi_arr(g_nof_streams - 1 downto 0) := (others => c_dp_sosi_rst);
  signal db_siso_arr               : t_dp_siso_arr(g_nof_streams - 1 downto 0) := (others => c_dp_siso_rdy);

  signal bsn_sosi_arr              : t_dp_sosi_arr(3 downto 0) := (others => c_dp_sosi_rst);
begin
  u_bsn_monitor : entity dp_lib.mms_dp_bsn_monitor
  generic map (
    g_nof_streams        => c_nof_bsn_streams,  -- Check one input and one output stream
    g_cross_clock_domain => true,
    g_bsn_w              => c_dp_stream_bsn_w,
    g_cnt_sop_w          => c_word_w,
    g_cnt_valid_w        => c_word_w,
    g_log_first_bsn      => true
  )
  port map (
    -- Memory-mapped clock domain
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    reg_mosi    => reg_bsn_monitor_mosi,
    reg_miso    => reg_bsn_monitor_miso,

    -- Streaming clock domain
    dp_rst      => ddr_out_rst_i,
    dp_clk      => ddr_out_clk_i,
    in_siso_arr => (others => c_dp_siso_rdy),
    in_sosi_arr => bsn_sosi_arr
  );

  bsn_sosi_arr(0) <= bg_sosi_arr(0);
  bsn_sosi_arr(1) <= to_mem_sosi;
  bsn_sosi_arr(2) <= from_mem_sosi;
  bsn_sosi_arr(3) <= db_sosi_arr(0);

  ------------------------------------------------------------------------------
  -- TRANSPOSE UNIT
  ------------------------------------------------------------------------------
  u_transpose: entity reorder_lib.reorder_transpose
  generic map(
    g_nof_streams    => g_nof_streams,
    g_in_dat_w       => g_in_dat_w,
    g_frame_size_in  => g_frame_size_in,
    g_frame_size_out => g_frame_size_out,
    g_use_complex    => c_use_complex,
    g_ena_pre_transp => g_ena_pre_transp,
    g_reorder_seq    => g_reorder_seq
  )
  port map (
    mm_rst                => mm_rst,
    mm_clk                => mm_clk,

    dp_rst                => ddr_out_rst_i,
    dp_clk                => ddr_out_clk_i,

    -- ST sink
    snk_out_arr           => bg_siso_arr,
    snk_in_arr            => bg_sosi_arr,

    -- ST source
    src_in_arr            => db_siso_arr,
    src_out_arr           => db_sosi_arr,

    ram_ss_ss_transp_mosi => ram_ss_ss_transp_mosi,
    ram_ss_ss_transp_miso => ram_ss_ss_transp_miso,

    -- Control interface to the external memory
    dvr_miso              => ctlr_dvr_miso,
    dvr_mosi              => ctlr_dvr_mosi,

    -- Data interface to the external memory
    to_mem_src_out        => to_mem_sosi,
    to_mem_src_in         => to_mem_siso,

    from_mem_snk_in       => from_mem_sosi,
    from_mem_snk_out      => from_mem_siso

  );

  ------------------------------------------------------------------------------
  -- DDR3 MODULE 0, MB_I
  ------------------------------------------------------------------------------
  u_ddr_mem_ctrl : entity io_ddr_lib.io_ddr
  generic map(
    g_technology             => g_tech_select_default,
    g_tech_ddr               => g_tech_ddr,
    g_cross_domain_dvr_ctlr  => false,
    g_wr_data_w              => c_data_w,
    g_wr_fifo_depth          => c_fifo_depth,
    g_rd_fifo_depth          => c_fifo_depth,
    g_rd_data_w              => c_data_w,
    g_wr_flush_mode          => "SYN",
    g_wr_flush_use_channel   => false,
    g_wr_flush_start_channel => 0,
    g_wr_flush_nof_channels  => 1
  )
  port map (
    -- DDR reference clock
    ctlr_ref_clk  => ddr_ref_clk,
    ctlr_ref_rst  => ddr_ref_rst,

    -- DDR controller clock domain
    ctlr_clk_out  => ddr_out_clk_i,  -- output clock of the ddr controller is used as DP clk.
    ctlr_rst_out  => ddr_out_rst_i,

    ctlr_clk_in   => ddr_out_clk_i,
    ctlr_rst_in   => ddr_out_rst_i,

    -- MM clock + reset
    mm_rst        => mm_rst,
    mm_clk        => mm_clk,

    -- MM register map for DDR controller status info
    reg_io_ddr_mosi => reg_io_ddr_mosi,
    reg_io_ddr_miso => reg_io_ddr_miso,

    -- Driver clock domain
    dvr_clk       => ddr_out_clk_i,
    dvr_rst       => ddr_out_rst_i,

    dvr_miso      => ctlr_dvr_miso,
    dvr_mosi      => ctlr_dvr_mosi,

    -- Write FIFO clock domain
    wr_clk        => ddr_out_clk_i,
    wr_rst        => ddr_out_rst_i,

    wr_fifo_usedw => OPEN,
    wr_sosi       => to_mem_sosi,
    wr_siso       => to_mem_siso,

    -- Read FIFO clock domain
    rd_clk        => ddr_out_clk_i,
    rd_rst        => ddr_out_rst_i,

    rd_fifo_usedw => OPEN,
    rd_sosi       => from_mem_sosi,
    rd_siso       => from_mem_siso,

    term_ctrl_out => OPEN,
    term_ctrl_in  => OPEN,

    phy3_in       => MB_I_IN,
    phy3_io       => MB_I_IO,
    phy3_ou       => MB_I_OU
  );

  ddr_out_clk <= ddr_out_clk_i;
  ddr_out_rst <= ddr_out_rst_i;

  -----------------------------------------------------------------------------
  -- DIAG Block Generator
  -----------------------------------------------------------------------------
  u_mms_diag_block_gen: entity diag_lib.mms_diag_block_gen
  generic map (
    -- Generate configurations
    g_use_usr_input      => false,
    g_use_bg             => true,
    g_use_tx_seq         => true,
    -- General
    g_nof_streams        => g_nof_streams,
    -- BG settings
    g_use_bg_buffer_ram  => false,
    -- Tx_seq
    g_seq_dat_w          => c_data_w
  )
  port map (
    -- System
    mm_rst           => mm_rst,
    mm_clk           => mm_clk,
    dp_rst           => ddr_out_rst_i,
    dp_clk           => ddr_out_clk_i,
    -- MM interface
    reg_bg_ctrl_mosi => reg_diag_bg_mosi,
    reg_bg_ctrl_miso => reg_diag_bg_miso,
    reg_tx_seq_mosi  => reg_diag_tx_seq_mosi,
    reg_tx_seq_miso  => reg_diag_tx_seq_miso,
    -- ST interface
    out_siso_arr     => bg_siso_arr,
    out_sosi_arr     => bg_sosi_arr
  );

  -----------------------------------------------------------------------------
  -- DIAG Rx seq with optional Data Buffer
  -----------------------------------------------------------------------------
  u_mms_diag_data_buffer: entity diag_lib.mms_diag_data_buffer
  generic map (
    -- Generate configurations
    g_use_db       => c_use_db,
    g_use_rx_seq   => c_use_rx_seq,
    -- General
    g_nof_streams  => g_nof_streams,
    -- DB settings
    g_data_type    => e_data,  -- define the sosi field that gets stored: e_data=data, e_complex=im&re, e_real=re, e_imag=im,
    g_data_w       => c_data_w,
    g_buf_nof_data => c_buf_nof_data,
    g_buf_use_sync => c_buf_use_sync,  -- when TRUE start filling the buffer at the in_sync, else after the last word was read,
    -- Rx_seq
    g_use_steps    => c_use_steps,
    g_seq_dat_w    => c_data_w
  )
  port map (
    -- System
    mm_rst            => mm_rst,
    mm_clk            => mm_clk,
    dp_rst            => ddr_out_rst_i,
    dp_clk            => ddr_out_clk_i,
    -- MM interface
    reg_data_buf_mosi => reg_diag_data_buf_mosi,
    reg_data_buf_miso => reg_diag_data_buf_miso,
    ram_data_buf_mosi => ram_diag_data_buf_mosi,
    ram_data_buf_miso => ram_diag_data_buf_miso,
    reg_rx_seq_mosi   => reg_diag_rx_seq_mosi,
    reg_rx_seq_miso   => reg_diag_rx_seq_miso,

    -- ST interface
    in_sosi_arr       => db_sosi_arr
  );
end str;
