-------------------------------------------------------------------------------
--
-- Copyright (C) 2012
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------

-- Purpose: Provide MM slave register for dp_fifo_fill
-- Description:
--
--  Read/Write registers for all streams. The total number of registers depends on the
--  g_nof_streams that are used. Each stream has c_nof_regs_per_stream = 3 registers.
--
--   31             24 23             16 15              8 7               0  wi
--  |-----------------|-----------------|-----------------|-----------------|
--  |                                             fifo_used_words stream 0  |  0
--  |-----------------------------------------------------------------------|
--  |                      wr_fifo_full = [1] rd_fifo_empty = [0] stream 0  |  1
--  |-----------------------------------------------------------------------|
--  |                                         max_fifo_used_words stream 0  |  2
--  |-----------------------------------------------------------------------|
--  |                                             fifo_used_words stream 1  |  3
--  |-----------------------------------------------------------------------|
--  |                      wr_fifo_full = [1] rd_fifo_empty = [0] stream 1  |  4
--  |-----------------------------------------------------------------------|
--  |                                         max_fifo_used_words stream 1  |  5
--  |-----------------------------------------------------------------------|
--                                      |
--                                      |
--  |-----------------------------------------------------------------------|
--  |                                             fifo_used_words stream x  |
--  |-----------------------------------------------------------------------|
--  |                      wr_fifo_full = [1] rd_fifo_empty = [0] stream x  |
--  |-----------------------------------------------------------------------|
--  |                                         max_fifo_used_words stream x  |
--  |-----------------------------------------------------------------------|

library IEEE, common_lib;
use IEEE.std_logic_1164.all;
use common_lib.common_pkg.all;
use common_lib.common_mem_pkg.all;

entity dp_fifo_fill_reg is
  generic (
    g_nof_streams        : positive := 3;
    g_cross_clock_domain : boolean  := true  -- use FALSE when mm_clk and st_clk are the same, else use TRUE to cross the clock domain
  );
  port (
    -- Clocks and reset
    mm_rst     : in  std_logic;  -- reset synchronous with mm_clk
    mm_clk     : in  std_logic;  -- memory-mapped bus clock
    st_rst     : in  std_logic;  -- reset synchronous with st_clk
    st_clk     : in  std_logic;  -- other clock domain clock

    -- Memory Mapped Slave in mm_clk domain
    sla_in     : in  t_mem_mosi;  -- actual ranges defined by c_mm_reg
    sla_out    : out t_mem_miso;  -- actual ranges defined by c_mm_reg

    -- MM registers in st_clk domain
    used_w     : in  std_logic_vector;  -- Actual used word of the fifo
    rd_emp     : in  std_logic_vector;
    wr_ful     : in  std_logic_vector
  );
end dp_fifo_fill_reg;

architecture str of dp_fifo_fill_reg is
  constant c_reg_max_used_words_offset : natural := 2;
  constant c_nof_regs_per_stream       : natural := 4;  -- Must always be a power of 2 in order to meet the python register definition.

  -- Define the actual size of the MM slave register
  constant c_mm_reg : t_c_mem := (latency  => 1,
                                  adr_w    => ceil_log2(g_nof_streams * c_nof_regs_per_stream),
                                  dat_w    => c_word_w,  -- Use MM bus data width = c_word_w = 32 for all MM registers
                                  nof_dat  => g_nof_streams * c_nof_regs_per_stream,
                                  init_sl  => '0');

  -- Registers in st_clk domain
  signal in_arr_reg  : std_logic_vector(g_nof_streams * c_nof_regs_per_stream * c_word_w - 1 downto 0) := (others => '0');
  signal reg_wr_arr  : std_logic_vector(g_nof_streams * c_nof_regs_per_stream - 1          downto 0) := (others => '0');
  signal reg_rd_arr  : std_logic_vector(g_nof_streams * c_nof_regs_per_stream - 1          downto 0) := (others => '0');
  signal peak_used_w : std_logic_vector(g_nof_streams * c_word_w - 1                       downto 0) := (others => '0');
begin
  gen_in_arr_reg : for I in 0 to g_nof_streams - 1 generate
    in_arr_reg((c_nof_regs_per_stream * I + 1) * c_word_w - 1  downto  c_nof_regs_per_stream * I   * c_word_w) <= used_w((I + 1) * c_word_w - 1 downto I * c_word_w);
    in_arr_reg((c_nof_regs_per_stream * I + 2) * c_word_w - 31 downto (c_nof_regs_per_stream * I + 1) * c_word_w) <= wr_ful(I) & rd_emp(I);
    in_arr_reg((c_nof_regs_per_stream * I + 3) * c_word_w - 1  downto (c_nof_regs_per_stream * I + 2) * c_word_w) <= peak_used_w((I + 1) * c_word_w - 1 downto I * c_word_w);
  end generate;

  u_reg : entity common_lib.common_reg_r_w_dc
  generic map (
    g_cross_clock_domain => g_cross_clock_domain,
    g_in_new_latency     => 1,
    g_readback           => true,
    g_reg                => c_mm_reg,
    g_init_reg           => (others => '1')
  )
  port map (
    -- Clocks and reset
    mm_rst      => mm_rst,
    mm_clk      => mm_clk,
    st_rst      => st_rst,
    st_clk      => st_clk,

    -- Memory Mapped Slave in mm_clk domain
    sla_in      => sla_in,
    sla_out     => sla_out,

    -- MM registers in st_clk domain
    reg_wr_arr  => reg_wr_arr,
    reg_rd_arr  => reg_rd_arr,
    in_new      => OPEN,
    in_reg      => in_arr_reg,  -- read
    out_reg     => OPEN,  -- write
    out_new     => open
  );

  gen_peak_meters : for I in 0 to g_nof_streams - 1 generate
    u_peak_meter : entity common_lib.common_peak
    generic map(
      g_dat_w => c_word_w
    )
    port map (
      rst       => st_rst,
      clk       => st_clk,
      in_dat    => used_w((I + 1) * c_word_w - 1 downto I * c_word_w),
      in_val    => '1',
      in_clear  => reg_rd_arr(I * c_nof_regs_per_stream + c_reg_max_used_words_offset),
      out_dat   => peak_used_w((I + 1) * c_word_w - 1 downto I * c_word_w),
      out_val   => open
    );
  end generate;
end str;
